ALTER TABLE TASY.ROP_LOTE_MOVTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_LOTE_MOVTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_LOTE_MOVTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_REGISTRO             DATE,
  NR_SEQ_OPERACAO         NUMBER(10)            NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  NR_SEQ_LOCAL            NUMBER(10)            NOT NULL,
  NR_SEQ_MOTIVO_BAIXA     NUMBER(10),
  NR_SEQ_LOCAL_ORIG_DEST  NUMBER(10),
  DT_LIBERACAO            DATE,
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  NR_SEQ_LAVANDERIA       NUMBER(10),
  CD_PACIENTE             VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPLOMO_ESTABEL_FK_I ON TASY.ROP_LOTE_MOVTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOMO_I2 ON TASY.ROP_LOTE_MOVTO
(DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_I2
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOMO_I3 ON TASY.ROP_LOTE_MOVTO
(DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPLOMO_PESFISI_FK_I ON TASY.ROP_LOTE_MOVTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPLOMO_PESFISI_FK2_I ON TASY.ROP_LOTE_MOVTO
(CD_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ROPLOMO_PK ON TASY.ROP_LOTE_MOVTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPLOMO_ROPLAVA_FK_I ON TASY.ROP_LOTE_MOVTO
(NR_SEQ_LAVANDERIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_ROPLAVA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOMO_ROPLOCA_FK_I ON TASY.ROP_LOTE_MOVTO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_ROPLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOMO_ROPLOCA_FK2_I ON TASY.ROP_LOTE_MOVTO
(NR_SEQ_LOCAL_ORIG_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_ROPLOCA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOMO_ROPMOBA_FK_I ON TASY.ROP_LOTE_MOVTO
(NR_SEQ_MOTIVO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_ROPMOBA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOMO_ROPOPER_FK_I ON TASY.ROP_LOTE_MOVTO
(NR_SEQ_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOMO_ROPOPER_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_LOTE_MOVTO_ATUAL
before insert or update ON TASY.ROP_LOTE_MOVTO for each row
declare

ie_tipo_operacao_w		varchar2(1);
ie_entrada_saida_w			varchar2(1);
qt_saldo_w			number(13,4);
nr_seq_roupa_w			number(10);
qt_roupa_w			number(13,4);
qt_saldo_ww			number(13,4) := 0;
qt_saldo_www			number(13,4) := 0;
qt_existe_w			number(10);
qt_existe_destino_w		number(10);
qt_roupa_cadastro_w		number(13,4);
ie_operacao_w			varchar2(80);

cursor c01 is
select	nr_seq_roupa,
	qt_roupa,
	rop_obter_dados_operacao(:new.nr_seq_operacao, 'T')
from	rop_movto_roupa
where	nr_seq_lote = :new.nr_Sequencia
and	qt_roupa is not null;

begin

select	ie_tipo_operacao,
	ie_entrada_saida
into	ie_tipo_operacao_w,
	ie_entrada_saida_w
from	rop_operacao
where	nr_sequencia = :new.nr_seq_operacao;

/* so atualiza quando libera o lote e se a operacao do movimento tiver algum tipo informado*/
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) and
	(ie_tipo_operacao_w is not null) then
	begin

	open C01;
	loop
	fetch C01 into
		nr_seq_roupa_w,
		qt_roupa_w,
		ie_operacao_w;
	exit when C01%notfound;
		begin

		select	count(*)
		into	qt_existe_w
		from	rop_saldo_roupa
		where	nr_seq_local = :new.nr_seq_local
		and	dt_mesano_referencia = trunc(sysdate,'mm')
		and	nr_seq_roupa = nr_seq_roupa_w;

		select	sum(qt_saldo)
		into	qt_existe_destino_w
		from	rop_saldo_roupa
		where	nr_seq_local = :new.nr_seq_local_orig_dest
		and	dt_mesano_referencia = trunc(sysdate,'mm')
		and	nr_seq_roupa = nr_seq_roupa_w;

		select	nvl(sum(qt_saldo),0)
		into	qt_saldo_w
		from	rop_saldo_roupa
		where	nr_seq_local = :new.nr_seq_local
		and	dt_mesano_referencia = trunc(sysdate,'mm')
		and	nr_seq_roupa = nr_seq_roupa_w;

		select	nvl(sum(qt_roupa),0)
		into	qt_roupa_cadastro_w
		from	rop_roupa
		where	nr_sequencia = nr_seq_roupa_w;

		/*Se nao existe na tabela de saldo, faz insert*/
		if	(qt_existe_w = 0) then
			begin
			if	(ie_entrada_saida_w = 'E') then

				if	(ie_tipo_operacao_w in(0,1,4)) then
					qt_saldo_ww := qt_roupa_w;
				elsif	(ie_tipo_operacao_w  = 2) then
					qt_saldo_ww := qt_roupa_cadastro_w;
				end if;

				insert into rop_saldo_roupa(
					nr_seq_roupa,
					nr_seq_local,
					dt_mesano_referencia,
					qt_saldo)
				values(	nr_seq_roupa_w,
					:new.nr_seq_local,
					trunc(sysdate,'mm'),
					qt_saldo_ww);

			elsif	(ie_entrada_saida_w = 'S') then

				if	(ie_tipo_operacao_w in(0,1,3)) then
					qt_saldo_ww := qt_roupa_w * -1;
				elsif	(ie_tipo_operacao_w = 2) then
					qt_saldo_ww := qt_roupa_cadastro_w;
				end if;

				insert into rop_saldo_roupa(
					nr_seq_roupa,
					nr_seq_local,
					dt_mesano_referencia,
					qt_saldo)
				values(	nr_seq_roupa_w,
					:new.nr_seq_local,
					trunc(sysdate,'mm'),
					qt_saldo_ww);

				if	(ie_operacao_w = 'TS') and
					(:new.nr_seq_local_orig_dest > 0) then
					begin

					select	count(*)
					into	qt_existe_w
					from	rop_saldo_roupa
					where	nr_seq_local = :new.nr_seq_local_orig_dest
					and	dt_mesano_referencia = trunc(sysdate,'mm')
					and	nr_seq_roupa = nr_seq_roupa_w;

					if	(qt_existe_w = 0) then
						insert into rop_saldo_roupa(
							nr_seq_roupa,
							nr_seq_local,
							dt_mesano_referencia,
							qt_saldo)
						values(	nr_seq_roupa_w,
							:new.nr_seq_local_orig_dest,
							trunc(sysdate,'mm'),
							qt_roupa_w);
					else
						qt_saldo_www := qt_roupa_w + qt_existe_destino_w;

						update	rop_saldo_roupa
						set	qt_saldo = qt_saldo_www
						where	nr_seq_local = :new.nr_seq_local_orig_dest
						and	dt_mesano_referencia = trunc(sysdate,'mm')
						and	nr_seq_roupa = nr_seq_roupa_w;
					end if;

					end;
				end if;
			end if;
			end;
		/*Senao, faz update*/
		else
			begin
			if	(ie_entrada_saida_w = 'E') then

				if	(ie_tipo_operacao_w in(0,1,4)) then
					qt_saldo_ww := qt_saldo_w + qt_roupa_w;
				elsif	(ie_tipo_operacao_w  = 2) then
					qt_saldo_ww := qt_roupa_cadastro_w;
				end if;

				update	rop_saldo_roupa
				set	qt_saldo = qt_saldo_ww
				where	nr_seq_local = :new.nr_seq_local
				and	dt_mesano_referencia = trunc(sysdate,'mm')
				and	nr_seq_roupa = nr_seq_roupa_w;

			elsif	(ie_entrada_saida_w = 'S') then

				if	(ie_tipo_operacao_w in(0,1,3)) then
					qt_saldo_ww := qt_saldo_w - qt_roupa_w;
				elsif	(ie_tipo_operacao_w  = 2) then
					qt_saldo_ww := qt_roupa_cadastro_w;
				end if;

				update	rop_saldo_roupa
				set	qt_saldo = qt_saldo_ww
				where	nr_seq_local = :new.nr_seq_local
				and	dt_mesano_referencia = trunc(sysdate,'mm')
				and	nr_seq_roupa = nr_seq_roupa_w;

				if	(ie_operacao_w = 'TS') and
					(:new.nr_seq_local_orig_dest > 0) then
					begin

					select	count(*)
					into	qt_existe_w
					from	rop_saldo_roupa
					where	nr_seq_local = :new.nr_seq_local_orig_dest
					and	dt_mesano_referencia = trunc(sysdate,'mm')
					and	nr_seq_roupa = nr_seq_roupa_w;

					if	(qt_existe_w = 0) then
						insert into rop_saldo_roupa(
							nr_seq_roupa,
							nr_seq_local,
							dt_mesano_referencia,
							qt_saldo)
						values(	nr_seq_roupa_w,
							:new.nr_seq_local_orig_dest,
							trunc(sysdate,'mm'),
							qt_roupa_w);
					else
						qt_saldo_www := qt_roupa_w + qt_existe_destino_w;

						update	rop_saldo_roupa
						set	qt_saldo = qt_saldo_www
						where	nr_seq_local = :new.nr_seq_local_orig_dest
						and	dt_mesano_referencia = trunc(sysdate,'mm')
						and	nr_seq_roupa = nr_seq_roupa_w;
					end if;

					end;
				end if;
			end if;
			end;
		end if;
		end;
	end loop;
	close C01;

	end;
end if;
end;
/


ALTER TABLE TASY.ROP_LOTE_MOVTO ADD (
  CONSTRAINT ROPLOMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_LOTE_MOVTO ADD (
  CONSTRAINT ROPLOMO_PESFISI_FK2 
 FOREIGN KEY (CD_PACIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ROPLOMO_ROPLOCA_FK2 
 FOREIGN KEY (NR_SEQ_LOCAL_ORIG_DEST) 
 REFERENCES TASY.ROP_LOCAL (NR_SEQUENCIA),
  CONSTRAINT ROPLOMO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ROPLOMO_ROPLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.ROP_LOCAL (NR_SEQUENCIA),
  CONSTRAINT ROPLOMO_ROPOPER_FK 
 FOREIGN KEY (NR_SEQ_OPERACAO) 
 REFERENCES TASY.ROP_OPERACAO (NR_SEQUENCIA),
  CONSTRAINT ROPLOMO_ROPMOBA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_BAIXA) 
 REFERENCES TASY.ROP_MOTIVO_BAIXA (NR_SEQUENCIA),
  CONSTRAINT ROPLOMO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPLOMO_ROPLAVA_FK 
 FOREIGN KEY (NR_SEQ_LAVANDERIA) 
 REFERENCES TASY.ROP_LAVANDERIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_LOTE_MOVTO TO NIVEL_1;

