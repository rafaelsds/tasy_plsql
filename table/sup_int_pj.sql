ALTER TABLE TASY.SUP_INT_PJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_PJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_PJ
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  IE_FORMA_INTEGRACAO         VARCHAR2(15 BYTE) NOT NULL,
  DT_LIBERACAO                DATE,
  DT_LEITURA                  DATE,
  DT_CONFIRMA_INTEGRACAO      DATE,
  CD_CGC                      VARCHAR2(14 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO          NUMBER(4),
  DS_RAZAO_SOCIAL             VARCHAR2(80 BYTE) NOT NULL,
  NM_FANTASIA                 VARCHAR2(80 BYTE) NOT NULL,
  DS_NOME_ABREV               VARCHAR2(18 BYTE),
  CD_MUNICIPIO_IBGE           VARCHAR2(6 BYTE),
  CD_CEP                      VARCHAR2(15 BYTE) NOT NULL,
  DS_ENDERECO                 VARCHAR2(40 BYTE) NOT NULL,
  NR_ENDERECO                 VARCHAR2(10 BYTE),
  DS_COMPLEMENTO              VARCHAR2(255 BYTE),
  DS_BAIRRO                   VARCHAR2(40 BYTE),
  DS_MUNICIPIO                VARCHAR2(40 BYTE) NOT NULL,
  SG_ESTADO                   VARCHAR2(2 BYTE)  NOT NULL,
  NR_TELEFONE                 VARCHAR2(15 BYTE),
  NR_FAX                      VARCHAR2(15 BYTE),
  NR_INSCRICAO_ESTADUAL       VARCHAR2(20 BYTE),
  NR_INSCRICAO_MUNICIPAL      VARCHAR2(20 BYTE),
  CD_INTERNACIONAL            VARCHAR2(20 BYTE),
  DS_SITE_INTERNET            VARCHAR2(40 BYTE),
  CD_PF_RESP_TECNICO          VARCHAR2(10 BYTE),
  DS_ORGAO_REG_RESP_TECNICO   VARCHAR2(10 BYTE),
  NR_AUTOR_FUNC               VARCHAR2(20 BYTE),
  NR_ALVARA_SANITARIO         VARCHAR2(20 BYTE),
  NR_ALVARA_SANITARIO_MUNIC   VARCHAR2(20 BYTE),
  NR_CERTIFICADO_BOAS_PRAT    VARCHAR2(20 BYTE),
  CD_ANS                      VARCHAR2(20 BYTE),
  DT_VALIDADE_AUTOR_FUNC      DATE,
  DT_VALIDADE_ALVARA_SANIT    DATE,
  DT_VALIDADE_CERT_BOAS_PRAT  DATE,
  CD_CNES                     VARCHAR2(20 BYTE),
  CD_REFERENCIA_FORNEC        VARCHAR2(20 BYTE),
  CD_SISTEMA_ANT              VARCHAR2(20 BYTE),
  DS_EMAIL                    VARCHAR2(255 BYTE),
  DT_VALIDADE_RESP_TECNICO    DATE,
  NM_PESSOA_CONTATO           VARCHAR2(255 BYTE),
  NR_RAMAL_CONTATO            NUMBER(5),
  NR_REGISTRO_RESP_TECNICO    VARCHAR2(20 BYTE),
  QT_DIA_PRAZO_ENTREGA        NUMBER(3),
  VL_MINIMO_NF                NUMBER(15,2),
  DT_VALIDADE_ALVARA_MUNIC    DATE,
  DS_RESP_TECNICO             VARCHAR2(255 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  IE_STATUS_APENADO           VARCHAR2(15 BYTE),
  NR_DDI_TELEFONE             VARCHAR2(3 BYTE),
  NR_DDI_FAX                  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUINPEJ_ESTABEL_FK_I ON TASY.SUP_INT_PJ
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINPEJ_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUINPEJ_PK ON TASY.SUP_INT_PJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINPEJ_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_pj_atual
before update or insert ON TASY.SUP_INT_PJ for each row
declare
qt_existe_erros_w				number(10);
qt_existe_w				number(10);
begin

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pj b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PJ'
and	a.ie_forma = 'R'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.dt_liberacao is not null) and
	(:new.dt_confirma_integracao is null) and
	(:new.ie_forma_integracao = 'R') then

	:new.dt_leitura := sysdate;

	consiste_sup_int_pj(
		:new.nr_sequencia,
		:new.cd_cgc,
		:new.cd_estabelecimento,
		:new.cd_municipio_ibge,
		:new.cd_pf_resp_tecnico,
		:new.cd_cnes);

	select	count(*)
	into	qt_existe_erros_w
	from	sup_int_pj_consist
	where	nr_sequencia = :new.nr_sequencia;

	if	(qt_existe_erros_w = 0) then

		gerar_pessoa_jur_integracao(
			:new.cd_cgc,
			:new.cd_estabelecimento,
			'INTEGR_TASY',
			:new.ds_razao_social,
			:new.nm_fantasia,
			:new.ds_nome_abrev,
			:new.cd_municipio_ibge,
			:new.cd_cep,
			:new.ds_endereco,
			:new.nr_endereco,
			:new.ds_complemento,
			:new.ds_bairro,
			:new.ds_municipio,
			:new.sg_estado,
			:new.nr_telefone,
			:new.nr_fax,
			:new.nr_inscricao_estadual,
			:new.nr_inscricao_municipal,
			:new.cd_internacional,
			:new.ds_site_internet,
			:new.cd_pf_resp_tecnico,
			:new.ds_orgao_reg_resp_tecnico,
			:new.nr_autor_func,
			:new.nr_alvara_sanitario,
			:new.nr_alvara_sanitario_munic,
			:new.nr_certificado_boas_prat,
			:new.cd_ans,
			:new.dt_validade_autor_func,
			:new.dt_validade_alvara_sanit,
			:new.dt_validade_cert_boas_prat,
			:new.cd_cnes,
			:new.cd_referencia_fornec,
			:new.cd_sistema_ant,
			:new.ds_email,
			:new.dt_validade_resp_tecnico,
			:new.nm_pessoa_contato,
			:new.nr_ramal_contato,
			:new.nr_registro_resp_tecnico,
			:new.qt_dia_prazo_entrega,
			:new.vl_minimo_nf,
			:new.dt_validade_alvara_munic,
			:new.ds_resp_tecnico,
			:new.ie_situacao,
			:new.ie_status_apenado);

		:new.dt_confirma_integracao 	:= sysdate;
	end if;
end if;
end;
/


ALTER TABLE TASY.SUP_INT_PJ ADD (
  CONSTRAINT SUINPEJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_INT_PJ ADD (
  CONSTRAINT SUINPEJ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.SUP_INT_PJ TO NIVEL_1;

