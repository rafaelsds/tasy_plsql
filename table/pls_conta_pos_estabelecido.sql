ALTER TABLE TASY.PLS_CONTA_POS_ESTABELECIDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_POS_ESTABELECIDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_POS_ESTABELECIDO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_CONTA                 NUMBER(10)       NOT NULL,
  VL_BENEFICIARIO              NUMBER(15,2)     NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_PROC            NUMBER(10),
  NR_SEQ_CONTA_MAT             NUMBER(10),
  IE_CALCULA_PRECO_BENEF       VARCHAR2(1 BYTE),
  IE_PRECO_PLANO               VARCHAR2(2 BYTE),
  NR_SEQ_REGRA_POS_ESTAB       NUMBER(10),
  IE_COBRAR_MENSALIDADE        VARCHAR2(1 BYTE),
  NR_SEQ_MENSALIDADE_SEG       NUMBER(10),
  CD_CONTA_CRED                VARCHAR2(20 BYTE),
  CD_CONTA_DEB                 VARCHAR2(20 BYTE),
  CD_CLASSIF_CRED              VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB               VARCHAR2(40 BYTE),
  NR_SEQ_ESQUEMA               NUMBER(10),
  IE_GLOSA                     VARCHAR2(1 BYTE),
  NR_SEQ_ANALISE               NUMBER(10),
  NR_SEQ_SCA                   NUMBER(10),
  NR_SEQ_EVENTO_FAT            NUMBER(10),
  NR_SEQ_REGRA_EVENTO_FAT      NUMBER(10),
  NR_SEQ_COOPERATIVA           NUMBER(10),
  NR_SEQ_LOTE_FAT              NUMBER(10),
  CD_PROC_CONVERTIDO           NUMBER(15),
  CD_CONTA_CRED_PROVISAO       VARCHAR2(20 BYTE),
  CD_CONTA_DEB_PROVISAO        VARCHAR2(20 BYTE),
  CD_CLASSIF_CRED_PROVISAO     VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB_PROVISAO      VARCHAR2(40 BYTE),
  VL_PROVISAO                  NUMBER(15,2),
  NR_LOTE_CONTABIL_PROV        NUMBER(10),
  NR_SEQ_ESQUEMA_PROV          NUMBER(10),
  CD_CLASSIF_CRED_FATURAMENTO  VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB_FATURAMENTO   VARCHAR2(40 BYTE),
  NR_SEQ_ESQUEMA_FATURAMENTO   NUMBER(10),
  NR_LOTE_CONTAB_FATURAMENTO   NUMBER(10),
  CD_HISTORICO_PROVISAO        NUMBER(10),
  CD_HISTORICO_FATURAMENTO     NUMBER(10),
  IE_STATUS_FATURAMENTO        VARCHAR2(2 BYTE),
  CD_HISTORICO_REV_FAT         NUMBER(10),
  NR_SEQ_POS_ESTAB_INTERC      NUMBER(10),
  VL_CUSTO_OPERACIONAL         NUMBER(15,2),
  VL_MATERIAIS                 NUMBER(15,2),
  CD_ITEM_CONVERTIDO           VARCHAR2(30 BYTE),
  DS_ITEM_CONVERTIDO           VARCHAR2(255 BYTE),
  VL_ADMINISTRACAO             NUMBER(15,2),
  TX_ADMINISTRACAO             NUMBER(7,4),
  QT_ITEM                      NUMBER(12,4),
  VL_MEDICO                    NUMBER(15,2),
  NR_SEQ_LOTE_DISC             NUMBER(10),
  CD_HISTORICO                 NUMBER(10),
  CD_CENTRO_CUSTO              NUMBER(8),
  IE_ORIGEM_VALOR_POS          VARCHAR2(2 BYTE),
  QT_ORIGINAL                  NUMBER(12,4),
  VL_CALCULADO                 NUMBER(15,2),
  VL_MEDICO_CALC               NUMBER(15,2),
  VL_MATERIAIS_CALC            NUMBER(15,2),
  VL_CUSTO_OPERACIONAL_CALC    NUMBER(15,2),
  CD_SISTEMA_ANT               VARCHAR2(255 BYTE),
  NR_SEQ_REGRA_CTB_DEB         NUMBER(10),
  NR_SEQ_REGRA_CTB_CRED        NUMBER(10),
  NR_SEQ_REGRA_CONV_PROC       NUMBER(10),
  VL_LIBERADO_MATERIAL_FAT     NUMBER(15,2),
  VL_LIBERADO_CO_FAT           NUMBER(15,2),
  VL_LIBERADO_HI_FAT           NUMBER(15,2),
  VL_GLOSA_MATERIAL_FAT        NUMBER(15,2),
  VL_GLOSA_HI_FAT              NUMBER(15,2),
  VL_GLOSA_CO_FAT              NUMBER(15,2),
  NR_SEQ_CONV_MAT              NUMBER(10),
  CD_PROCEDIMENTO              NUMBER(15),
  IE_ORIGEM_PROCED             NUMBER(10),
  DT_ITEM                      DATE,
  DT_INICIO_ITEM               DATE,
  DT_FIM_ITEM                  DATE,
  NR_SEQ_MATERIAL              NUMBER(10),
  CD_CONTA_CRED_TAXA           VARCHAR2(20 BYTE),
  CD_CONTA_DEB_TAXA            VARCHAR2(20 BYTE),
  CD_CLASSIF_CRED_TAXA         VARCHAR2(40 BYTE),
  CD_CLASSIF_DEB_TAXA          VARCHAR2(40 BYTE),
  CD_HISTORICO_PROVISAO_TAXA   NUMBER(10),
  CD_HISTORICO_REV_FAT_TAXA    NUMBER(10),
  NR_SEQ_ESQUEMA_PROV_TAXA     NUMBER(10),
  CD_ITEM_CONVERTIDO_XML       VARCHAR2(30 BYTE),
  NR_SEQ_TISS_TAB_CONVERSAO    NUMBER(10),
  NR_SEQ_DISC_PROC             NUMBER(10),
  NR_SEQ_DISC_MAT              NUMBER(10),
  NR_SEQ_CABECALHO             NUMBER(10),
  VL_TAXA_SERVICO              NUMBER(12,4),
  VL_TAXA_CO                   NUMBER(12,4),
  VL_TAXA_MATERIAL             NUMBER(12,4),
  VL_LIB_TAXA_CO               NUMBER(15,2),
  VL_LIB_TAXA_MATERIAL         NUMBER(15,2),
  VL_LIB_TAXA_SERVICO          NUMBER(15,2),
  VL_GLOSA_TAXA_CO             NUMBER(15,2),
  VL_GLOSA_TAXA_MATERIAL       NUMBER(15,2),
  VL_GLOSA_TAXA_SERVICO        NUMBER(15,2),
  NR_SEQ_REC_FUTURA            NUMBER(10),
  VL_TABELA_PRECO              NUMBER(15,4),
  VL_COTACAO_MOEDA             NUMBER(15,4),
  CD_MOEDA_CALCULO             NUMBER(3),
  QT_FILME                     NUMBER(15,4),
  VL_CUSTO_OPERACIONAL_TAB     NUMBER(15,2),
  QT_FILME_TAB                 NUMBER(15,4),
  DS_ITEM_PTU                  VARCHAR2(255 BYTE),
  VL_MATERIAL_TAB              NUMBER(15,2),
  TX_ITEM                      NUMBER(9,3),
  IE_SITUACAO                  VARCHAR2(1 BYTE),
  NR_SEQ_PROC_REC              NUMBER(10),
  NR_SEQ_MAT_REC               NUMBER(10),
  NR_SEQ_CONTA_REC             NUMBER(10),
  IE_VL_PAG_PRESTADOR          VARCHAR2(1 BYTE),
  IE_TIPO_GUIA                 VARCHAR2(2 BYTE),
  IE_TIPO_PROTOCOLO            VARCHAR2(3 BYTE),
  IE_TIPO_SEGURADO             VARCHAR2(3 BYTE),
  NR_SEQ_PRESTADOR_ATEND       NUMBER(10),
  NR_SEQ_PRESTADOR_EXEC        NUMBER(10),
  IE_TIPO_PRESTADOR_ATEND      VARCHAR2(2 BYTE),
  IE_TIPO_PRESTADOR_EXEC       VARCHAR2(2 BYTE),
  NR_SEQ_SEGURADO              NUMBER(10),
  DT_MES_COMPETENCIA           DATE,
  NR_SEQ_PROTOCOLO             NUMBER(10),
  DT_COMPETENCIA_MENS          DATE,
  NR_SEQ_PAGADOR               NUMBER(10),
  NR_SEQ_REGRA_HORARIO         NUMBER(10),
  NR_SEQ_REGRA_TX_OPME         NUMBER(10),
  NR_SEQ_CONTA_POS_ORIG        NUMBER(10),
  NR_REGISTRO_ANVISA           VARCHAR2(15 BYTE),
  CD_REF_FABRICANTE            VARCHAR2(60 BYTE),
  NM_FORNECEDOR_OPME           VARCHAR2(40 BYTE),
  DET_REG_ANVISA_OPME          VARCHAR2(50 BYTE),
  NR_NOTA_FISCAL_FORN_OPME     VARCHAR2(20 BYTE),
  CD_REF_MATERIAL_FAB_OPME     VARCHAR2(60 BYTE),
  CD_PORTE_ANESTESICO          VARCHAR2(10 BYTE),
  IE_GERAR_CO                  VARCHAR2(5 BYTE),
  IE_GERAR_FILME               VARCHAR2(5 BYTE),
  VL_AJUSTE                    NUMBER(12,4),
  NR_SEQ_REGRA_TP_POS          NUMBER(10),
  IE_TIPO_LIBERACAO            VARCHAR2(2 BYTE),
  CD_ITEM_ALT                  VARCHAR2(30 BYTE),
  NR_SEQ_REGRA_LIMITE_MENS     NUMBER(10),
  CD_UNIDADE_MEDIDA            VARCHAR2(30 BYTE),
  IE_TX_ITEM_MANUAL            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOVB_CENCUST_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_CONCONT_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CONTA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_CONCONT_FK2_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CONTA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_CONCONT_FK5_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CONTA_CRED_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CONCONT_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_CONCONT_FK6_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CONTA_DEB_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CONCONT_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_CONCONT_FK7_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CONTA_DEB_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CONCONT_FK7_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_CONCONT_FK8_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_CONTA_CRED_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_CONCONT_FK8_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_HISPADR_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_HISTORICO_FATURAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_HISPADR_FK2_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_HISPADR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_HISPADR_FK3_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_HISTORICO_PROVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_HISPADR_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_HISPADR_FK4_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_HISTORICO_REV_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_HISPADR_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_HISPADR_FK5_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_HISTORICO_PROVISAO_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_HISPADR_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_HISPADR_FK6_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_HISTORICO_REV_FAT_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_HISPADR_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_I1 ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CONTA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_I2 ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_MENSALIDADE_SEG, IE_COBRAR_MENSALIDADE, IE_TIPO_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_I3 ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CONTA_POS_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_LOTCONT_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_LOTE_CONTAB_FATURAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_LOTCONT_FK2_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_LOTE_CONTABIL_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_MOEDA_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_MOEDA_CALCULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCOVB_PK ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLCONPAG_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSANCO_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSCMAL_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CONV_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSCMAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSCOMAT_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSCOME_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSCONG_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_COOPERATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSCONP_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_REGRA_CONV_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSCONP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSCOPRO_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSCORF_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_REC_FUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSCPCA_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSCPCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSDIMA_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_DISC_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSDIMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSDISP_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_DISC_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSDISP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSESCO_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSESCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSESCO_FK2_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_ESQUEMA_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSESCO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSESCO_FK3_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_ESQUEMA_FATURAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSESCO_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSESCO_FK4_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_ESQUEMA_PROV_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSESCO_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSEVFA_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_EVENTO_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSEVFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSLODI_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_LOTE_DISC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_PLSLOFA_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_LOTE_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSMESE_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_MENSALIDADE_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSPRCO_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSPRES_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_PRESTADOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSPRES_FK2_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_PRESTADOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSRGLC_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_REGRA_LIMITE_MENS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSSEGU_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PLSTTXO_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_REGRA_TX_OPME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PRGLMAT_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_MAT_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PRGPROC_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_PROC_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOVB_PROCEDI_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOVB_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOVB_TISSTTA_FK_I ON TASY.PLS_CONTA_POS_ESTABELECIDO
(NR_SEQ_TISS_TAB_CONVERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_conta_pos_estab_update
before update ON TASY.PLS_CONTA_POS_ESTABELECIDO for each row
declare

qt_registro_w	number(10);
nr_seq_lote_w	number(10);

begin

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	select	max(s.nr_seq_lote)
	into	nr_seq_lote_w
	from	pls_fatura_proc x,
		pls_fatura_conta c,
		pls_fatura_evento z,
		pls_fatura s,
		pls_lote_faturamento a
	where	s.nr_sequencia	= z.nr_seq_fatura
	and	z.nr_sequencia	= c.nr_seq_fatura_evento
	and	c.nr_sequencia	= x.nr_seq_fatura_conta
	and	a.nr_sequencia	= s.nr_seq_lote
	and	nvl(s.ie_cancelamento,'X') not in ('C','E')
	and	a.nr_seq_lote_origem is null
	and	x.nr_seq_conta_pos_estab = :new.nr_sequencia
	and	x.ie_tipo_cobranca in ('1','2');

	if	(nr_seq_lote_w is null) then
		select	max(s.nr_seq_lote)
		into	nr_seq_lote_w
		from	pls_fatura_mat x,
			pls_fatura_conta c,
			pls_fatura_evento z,
			pls_fatura s,
			pls_lote_faturamento a
		where	s.nr_sequencia	= z.nr_seq_fatura
		and	z.nr_sequencia	= c.nr_seq_fatura_evento
		and	c.nr_sequencia	= x.nr_seq_fatura_conta
		and	a.nr_sequencia	= s.nr_seq_lote
		and	a.nr_seq_lote_origem is null
		and	nvl(s.ie_cancelamento,'X') not in ('C','E')
		and	x.nr_seq_conta_pos_estab = :new.nr_sequencia
		and	x.ie_tipo_cobranca in ('1','2');
	end if;

	if	(nr_seq_lote_w is not null) then

		if	(:old.nr_seq_lote_fat is not null) and
			(:new.nr_seq_lote_fat is null) then
			pls_gerar_fatura_log(	nr_seq_lote_w, null, :new.nr_seq_conta, 'PLS_CONTA_POS_ESTAB_UPDATE -'||
											' Conta: '||:new.nr_seq_conta||
											' Lote incluso: '||nr_seq_lote_w||
											' Lote old: '||:old.nr_seq_lote_fat||
											' Lote new: '||:new.nr_seq_lote_fat||
											' Evento old: '||:old.nr_seq_evento_fat||
											' Evento new: '||:new.nr_seq_evento_fat,'CX', 'N', :new.nm_usuario);

			:new.nr_seq_lote_fat := nr_seq_lote_w;
			:new.nr_seq_evento_fat := :old.nr_seq_evento_fat;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_conta_pos_estab_alt_pag
before insert ON TASY.PLS_CONTA_POS_ESTABELECIDO for each row
begin
	if	(:new.nr_seq_pagador is null) then
		:new.nr_seq_pagador := pls_obter_pagador_benef(	:new.nr_seq_segurado,
								:new.dt_mes_competencia,
								'6');
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_conta_pos_estab_atual
before insert or update ON TASY.PLS_CONTA_POS_ESTABELECIDO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

ds_programa_w		varchar2(4000);
qt_registro_w		pls_integer := 0;
ie_status_analise_w	pls_analise_conta.ie_status%type;
ds_log_w		varchar2(4000);
ds_log_call_w		varchar2(2000);
machine_w		varchar2(100);
osuser_w		varchar2(100);
qt_apresentada_w	pls_conta_mat.qt_material_imp%type;
qt_liberada_w		pls_conta_mat.qt_material_imp%type;
ie_concil_contab_w	pls_visible_false.ie_concil_contab%type;
nr_seq_log_pos_estab_w	pls_log_pos_estabelecido.nr_sequencia%type;

begin

select	osuser,
	machine
into	osuser_w,
	machine_w
from  	v$session
where	audsid = userenv('SESSIONID');

ds_log_w := substr(	' M'||chr(225)||'quina: ' || machine_w || chr(13) ||chr(10)||
				' OS User: ' || osuser_w || chr(13) ||chr(10)||
				' Fun'||chr(231)||chr(227)||'o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
				' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack || chr(13) ||chr(10) ,1,1500);

if (:new.nr_seq_analise is null and :old.nr_seq_analise is not null) then


	if	(pls_se_aplicacao_tasy = 'N') then
			ds_log_w := ds_log_w || ' old.An'||chr(225)||'lise. '||:old.nr_seq_analise ||' Altera'||chr(231)||chr(227)||'o efetuada via update fora do Tasy.';

	else
		ds_log_w :=  ds_log_w || ' new.An'||chr(225)||'lise. '||:new.nr_seq_analise  ||' Altera'||chr(231)||chr(227)||'o efetuada via Tasy.';
	end if;


end if;

if	(:new.nr_seq_disc_proc is not null) then
	select	qt_procedimento_imp,
		qt_procedimento
	into	qt_apresentada_w,
		qt_liberada_w
	from	pls_conta_proc
	where	nr_sequencia = :new.nr_seq_conta_proc;

elsif	(:new.nr_seq_disc_mat is not null) then

	select	qt_material_imp,
		qt_material
	into	qt_apresentada_w,
		qt_liberada_w
	from	pls_conta_mat
	where	nr_sequencia = :new.nr_seq_conta_mat;
end if;

if	((:new.qt_item + qt_liberada_w) > qt_apresentada_w) and
	((:new.qt_item <> :old.qt_item) or
	(:new.qt_item is not null and :old.qt_item is null)) then

	ds_log_w := 'Quantidade gerada + quantidade liberada do item '||chr(233)||' maior que a quantidade apresentada. ' || pls_util_pck.enter_w || ds_log_w;

	insert	into	pls_conta_pos_estab_log
		(	nr_sequencia, vl_beneficiario, vl_beneficiario_old,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, nr_seq_conta, nr_seq_conta_mat,
			nr_seq_conta_proc, ds_log, vl_provisao_old,
			vl_provisao, vl_custo_operacional_old, vl_custo_operacional,
			vl_materiais_old, vl_materiais, vl_administracao,
			tx_administracao, vl_medico_old, vl_medico,
			vl_calculado_old, vl_calculado, vl_medico_calc,
			vl_medico_calc_old, vl_materiais_calc, vl_materiais_calc_old,
			vl_custo_operacional_calc_old, vl_custo_operacional_calc, vl_liberado_material_fat,
			vl_liberado_material_fat_old, vl_liberado_co_fat_old, vl_liberado_co_fat,
			vl_liberado_hi_fat, vl_liberado_hi_fat_old, vl_glosa_material_fat,
			vl_glosa_material_fat_old, vl_glosa_hi_fat, vl_glosa_hi_fat_old,
			vl_glosa_co_fat_old, vl_glosa_co_fat, vl_taxa_servico,
			vl_taxa_servico_old, vl_taxa_co, vl_taxa_co_old,
			vl_taxa_material_old, vl_taxa_material, vl_lib_taxa_co,
			vl_lib_taxa_co_old, vl_lib_taxa_material_old, vl_lib_taxa_material,
			vl_lib_taxa_servico, vl_lib_taxa_servico_old, vl_glosa_taxa_co_old,
			vl_glosa_taxa_co, vl_glosa_taxa_material_old, vl_glosa_taxa_material,
			vl_glosa_taxa_servico, vl_glosa_taxa_servico_old, nr_seq_lote_fat,
			nr_seq_lote_fat_old, nr_seq_evento_fat, nr_seq_evento_fat_old)
	values	(	pls_conta_pos_estab_log_seq.nextval, :new.vl_beneficiario, :old.vl_beneficiario,
			sysdate, :new.nm_usuario, sysdate,
			:new.nm_usuario, :new.nr_seq_conta, :new.nr_seq_conta_mat,
			:new.nr_seq_conta_proc, ds_log_w, :old.vl_provisao,
			:new.vl_provisao, :old.vl_custo_operacional, :new.vl_custo_operacional,
			:old.vl_materiais, :new.vl_materiais, :new.vl_administracao,
			:new.tx_administracao, :old.vl_medico, :new.vl_medico,
			:old.vl_calculado, :new.vl_calculado, :new.vl_medico_calc,
			:old.vl_medico_calc, :new.vl_materiais_calc, :old.vl_materiais_calc,
			:old.vl_custo_operacional_calc, :new.vl_custo_operacional_calc, :new.vl_liberado_material_fat,
			:old.vl_liberado_material_fat, :old.vl_liberado_co_fat, :new.vl_liberado_co_fat,
			:new.vl_liberado_hi_fat, :old.vl_liberado_hi_fat, :new.vl_glosa_material_fat,
			:old.vl_glosa_material_fat, :new.vl_glosa_hi_fat, :old.vl_glosa_hi_fat,
			:old.vl_glosa_co_fat, :new.vl_glosa_co_fat, :new.vl_taxa_servico,
			:old.vl_taxa_servico, :new.vl_taxa_co, :old.vl_taxa_co,
			:old.vl_taxa_material, :new.vl_taxa_material, :new.vl_lib_taxa_co,
			:old.vl_lib_taxa_co, :old.vl_lib_taxa_material, :new.vl_lib_taxa_material,
			:new.vl_lib_taxa_servico, :old.vl_lib_taxa_servico, :old.vl_glosa_taxa_co,
			:new.vl_glosa_taxa_co, :old.vl_glosa_taxa_material, :new.vl_glosa_taxa_material,
			:new.vl_glosa_taxa_servico, :old.vl_glosa_taxa_servico, :new.nr_seq_lote_fat,
			:old.nr_seq_lote_fat, :new.nr_seq_evento_fat, :old.nr_seq_evento_fat);
end if;

--Quando status faturamento da conta pos mudar para liberado. Tentar pegar os casos onde as contas ficam liberadas para faturamento e a analise pendente.(Ex: OS958963)
if	(:new.ie_status_faturamento = 'L' and (:old.ie_status_faturamento <> 'L' or :old.ie_status_faturamento is null) ) then

	--Tentar pegar os casos onde
	select 	max(ie_status)
	into	ie_status_analise_w
	from	pls_analise_conta
	where	nr_sequencia = :new.nr_seq_analise;

	if	(ie_status_analise_w = 'G') then

		if	(pls_se_aplicacao_tasy = 'N') then
			ds_log_w := ds_log_w || 'status da an'||chr(255)||'lise = G. Altera'||chr(231)||chr(227)||'o efetuada via update fora do Tasy.';

		else
			ds_log_w :=  ds_log_w || 'ie_status_analise_w = G.';
		end if;
	end if;

end if;

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	((nvl(:new.vl_beneficiario,0) < 0) and (nvl(:new.vl_beneficiario,0) <> nvl(:old.vl_beneficiario,0))) and
		(:new.ie_status_faturamento <> 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort( 281537, 'NR_SEQ_CONTA=' || :new.nr_seq_conta || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_sequencia );
	end if;
end if;

if	(:old.ie_status_faturamento = 'L') and
	(:new.ie_status_faturamento = 'P') then

	if	(:new.nr_seq_conta_proc is not null) then
		select  count(1)
		into	qt_registro_w
		from    pls_fatura_proc a,
			pls_fatura_conta b,
			pls_fatura_evento c,
			pls_fatura d
		where   a.nr_seq_conta_pos_estab = :new.nr_sequencia
		and     a.nr_seq_fatura_conta = b.nr_sequencia
		and     b.nr_seq_fatura_evento = c.nr_sequencia
		and     c.nr_seq_fatura = d.nr_sequencia
		and     d.ie_cancelamento is null;

	elsif	(:new.nr_seq_conta_mat is not null) then
		select  count(1)
		into	qt_registro_w
		from    pls_fatura_mat a,
			pls_fatura_conta b,
			pls_fatura_evento c,
			pls_fatura d
		where   a.nr_seq_conta_pos_estab = :new.nr_sequencia
		and     a.nr_seq_fatura_conta = b.nr_sequencia
		and     b.nr_seq_fatura_evento = c.nr_sequencia
		and     c.nr_seq_fatura = d.nr_sequencia
		and     d.ie_cancelamento is null;

	end if;
	if	(qt_registro_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 289301, 'NR_SEQ_CONTA=' || :new.nr_seq_conta || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_sequencia );
	end if;
end if;

if	(:new.nr_seq_evento_fat is not null) and
	(:new.nr_seq_lote_fat is null) and
	(:old.nr_seq_evento_fat is not null) and
	(:old.nr_seq_lote_fat is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort( 322232, 'NR_SEQ_CONTA=' || :new.nr_seq_conta || ';' || 'NR_SEQ_CONTA_POS=' || :new.nr_sequencia );
end if;

if	(:old.ie_status_faturamento	= 'C') and
	(:new.ie_status_faturamento	!= 'C') then
	:new.ie_status_faturamento	:= 'C';
	:new.ie_cobrar_mensalidade	:= 'N';
	:new.vl_beneficiario		:= 0;
	:new.qt_item			:= 0;
end if;

if	( updating ) and
	(:new.ie_situacao = 'I') and
	(:old.nr_seq_lote_fat is not null) then

	if	(pls_se_aplicacao_tasy = 'N') then
		ds_log_w := ds_log_w || 'Situa'||chr(231)||chr(227)||'o do item(pos) alterada para inativo. Altera'||chr(231)||chr(227)||'o efetuada via update fora do Tasy.';

	else
		ds_log_w :=  ds_log_w || 'Situa'||chr(231)||chr(227)||'o do item(pos) alterada para inativo.';
	end if;

end if;

if	( updating ) and
	( (pls_se_aplicacao_tasy = 'N') or (ie_status_analise_w = 'G')) then

	insert	into pls_conta_pos_estab_log
		(nr_sequencia,
		vl_beneficiario,
		vl_beneficiario_old,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_conta,
		nr_seq_conta_mat,
		nr_seq_conta_proc,
		ds_log,
		vl_provisao_old,
		vl_provisao,
		vl_custo_operacional_old,
		vl_custo_operacional,
		vl_materiais_old,
		vl_materiais,
		vl_administracao,
		tx_administracao,
		vl_medico_old,
		vl_medico,
		vl_calculado_old,
		vl_calculado,
		vl_medico_calc,
		vl_medico_calc_old,
		vl_materiais_calc,
		vl_materiais_calc_old,
		vl_custo_operacional_calc_old,
		vl_custo_operacional_calc,
		vl_liberado_material_fat,
		vl_liberado_material_fat_old,
		vl_liberado_co_fat_old,
		vl_liberado_co_fat,
		vl_liberado_hi_fat,
		vl_liberado_hi_fat_old,
		vl_glosa_material_fat,
		vl_glosa_material_fat_old,
		vl_glosa_hi_fat,
		vl_glosa_hi_fat_old,
		vl_glosa_co_fat_old,
		vl_glosa_co_fat,
		vl_taxa_servico,
		vl_taxa_servico_old,
		vl_taxa_co,
		vl_taxa_co_old,
		vl_taxa_material_old,
		vl_taxa_material,
		vl_lib_taxa_co,
		vl_lib_taxa_co_old,
		vl_lib_taxa_material_old,
		vl_lib_taxa_material,
		vl_lib_taxa_servico,
		vl_lib_taxa_servico_old,
		vl_glosa_taxa_co_old,
		vl_glosa_taxa_co,
		vl_glosa_taxa_material_old,
		vl_glosa_taxa_material,
		vl_glosa_taxa_servico,
		vl_glosa_taxa_servico_old,
		nr_seq_lote_fat,
		nr_seq_lote_fat_old,
		nr_seq_evento_fat,
		nr_seq_evento_fat_old)
	select	pls_conta_pos_estab_log_seq.nextval,
		:new.vl_beneficiario,
		:old.vl_beneficiario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_seq_conta,
		:new.nr_seq_conta_mat,
		:new.nr_seq_conta_proc,
		nvl(ds_log_w, 'Altera'||chr(231)||chr(227)||'o efetuada via update fora do Tasy.'),
		:old.vl_provisao,
		:new.vl_provisao,
		:old.vl_custo_operacional,
		:new.vl_custo_operacional,
		:old.vl_materiais,
		:new.vl_materiais,
		:new.vl_administracao,
		:new.tx_administracao,
		:old.vl_medico,
		:new.vl_medico,
		:old.vl_calculado,
		:new.vl_calculado,
		:new.vl_medico_calc,
		:old.vl_medico_calc,
		:new.vl_materiais_calc,
		:old.vl_materiais_calc,
		:old.vl_custo_operacional_calc,
		:new.vl_custo_operacional_calc,
		:new.vl_liberado_material_fat,
		:old.vl_liberado_material_fat,
		:old.vl_liberado_co_fat,
		:new.vl_liberado_co_fat,
		:new.vl_liberado_hi_fat,
		:old.vl_liberado_hi_fat,
		:new.vl_glosa_material_fat,
		:old.vl_glosa_material_fat,
		:new.vl_glosa_hi_fat,
		:old.vl_glosa_hi_fat,
		:old.vl_glosa_co_fat,
		:new.vl_glosa_co_fat,
		:new.vl_taxa_servico,
		:old.vl_taxa_servico,
		:new.vl_taxa_co,
		:old.vl_taxa_co,
		:old.vl_taxa_material,
		:new.vl_taxa_material,
		:new.vl_lib_taxa_co,
		:old.vl_lib_taxa_co,
		:old.vl_lib_taxa_material,
		:new.vl_lib_taxa_material,
		:new.vl_lib_taxa_servico,
		:old.vl_lib_taxa_servico,
		:old.vl_glosa_taxa_co,
		:new.vl_glosa_taxa_co,
		:old.vl_glosa_taxa_material,
		:new.vl_glosa_taxa_material,
		:new.vl_glosa_taxa_servico,
		:old.vl_glosa_taxa_servico,
		:new.nr_seq_lote_fat,
		:old.nr_seq_lote_fat,
		:new.nr_seq_evento_fat,
		:old.nr_seq_evento_fat
	from	dual;

end if;

if	(:new.vl_beneficiario != (nvl(:new.vl_medico,0) + nvl(:new.vl_custo_operacional,0) + nvl(:new.vl_materiais,0) + nvl(:new.vl_lib_taxa_servico,0) + nvl(:new.vl_lib_taxa_co,0) + nvl(:new.vl_lib_taxa_material,0))) and
	((:new.vl_beneficiario != :old.vl_beneficiario) or
	((nvl(:new.vl_medico,0) + nvl(:new.vl_custo_operacional,0) + nvl(:new.vl_materiais,0) + nvl(:new.vl_lib_taxa_servico,0) + nvl(:new.vl_lib_taxa_co,0) + nvl(:new.vl_lib_taxa_material,0)) !=
	(nvl(:old.vl_medico,0) + nvl(:old.vl_custo_operacional,0) + nvl(:old.vl_materiais,0) + nvl(:old.vl_lib_taxa_servico,0) + nvl(:old.vl_lib_taxa_co,0) + nvl(:old.vl_lib_taxa_material,0)))) and
	(:new.vl_beneficiario 		> 0)   and
	(:new.ie_status_faturamento	= 'L') then
	begin
	insert	into pls_conta_pos_estab_log
		(nr_sequencia,
		vl_beneficiario,
		vl_beneficiario_old,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_conta,
		nr_seq_conta_mat,
		nr_seq_conta_proc,
		ds_log,
		vl_provisao_old,
		vl_provisao,
		vl_custo_operacional_old,
		vl_custo_operacional,
		vl_materiais_old,
		vl_materiais,
		vl_administracao,
		tx_administracao,
		vl_medico_old,
		vl_medico,
		vl_calculado_old,
		vl_calculado,
		vl_medico_calc,
		vl_medico_calc_old,
		vl_materiais_calc,
		vl_materiais_calc_old,
		vl_custo_operacional_calc_old,
		vl_custo_operacional_calc,
		vl_liberado_material_fat,
		vl_liberado_material_fat_old,
		vl_liberado_co_fat_old,
		vl_liberado_co_fat,
		vl_liberado_hi_fat,
		vl_liberado_hi_fat_old,
		vl_glosa_material_fat,
		vl_glosa_material_fat_old,
		vl_glosa_hi_fat,
		vl_glosa_hi_fat_old,
		vl_glosa_co_fat_old,
		vl_glosa_co_fat,
		vl_taxa_servico,
		vl_taxa_servico_old,
		vl_taxa_co,
		vl_taxa_co_old,
		vl_taxa_material_old,
		vl_taxa_material,
		vl_lib_taxa_co,
		vl_lib_taxa_co_old,
		vl_lib_taxa_material_old,
		vl_lib_taxa_material,
		vl_lib_taxa_servico,
		vl_lib_taxa_servico_old,
		vl_glosa_taxa_co_old,
		vl_glosa_taxa_co,
		vl_glosa_taxa_material_old,
		vl_glosa_taxa_material,
		vl_glosa_taxa_servico,
		vl_glosa_taxa_servico_old,
		nr_seq_lote_fat,
		nr_seq_lote_fat_old,
		nr_seq_evento_fat,
		nr_seq_evento_fat_old)
	select	pls_conta_pos_estab_log_seq.nextval,
		:new.vl_beneficiario,
		:old.vl_beneficiario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_seq_conta,
		:new.nr_seq_conta_mat,
		:new.nr_seq_conta_proc,
		'Valor divergente '||dbms_utility.format_call_stack,
		:old.vl_provisao,
		:new.vl_provisao,
		:old.vl_custo_operacional,
		:new.vl_custo_operacional,
		:old.vl_materiais,
		:new.vl_materiais,
		:new.vl_administracao,
		:new.tx_administracao,
		:old.vl_medico,
		:new.vl_medico,
		:old.vl_calculado,
		:new.vl_calculado,
		:new.vl_medico_calc,
		:old.vl_medico_calc,
		:new.vl_materiais_calc,
		:old.vl_materiais_calc,
		:old.vl_custo_operacional_calc,
		:new.vl_custo_operacional_calc,
		:new.vl_liberado_material_fat,
		:old.vl_liberado_material_fat,
		:old.vl_liberado_co_fat,
		:new.vl_liberado_co_fat,
		:new.vl_liberado_hi_fat,
		:old.vl_liberado_hi_fat,
		:new.vl_glosa_material_fat,
		:old.vl_glosa_material_fat,
		:new.vl_glosa_hi_fat,
		:old.vl_glosa_hi_fat,
		:old.vl_glosa_co_fat,
		:new.vl_glosa_co_fat,
		:new.vl_taxa_servico,
		:old.vl_taxa_servico,
		:new.vl_taxa_co,
		:old.vl_taxa_co,
		:old.vl_taxa_material,
		:new.vl_taxa_material,
		:new.vl_lib_taxa_co,
		:old.vl_lib_taxa_co,
		:old.vl_lib_taxa_material,
		:new.vl_lib_taxa_material,
		:new.vl_lib_taxa_servico,
		:old.vl_lib_taxa_servico,
		:old.vl_glosa_taxa_co,
		:new.vl_glosa_taxa_co,
		:old.vl_glosa_taxa_material,
		:new.vl_glosa_taxa_material,
		:new.vl_glosa_taxa_servico,
		:old.vl_glosa_taxa_servico,
		:new.nr_seq_lote_fat,
		:old.nr_seq_lote_fat,
		:new.nr_seq_evento_fat,
		:old.nr_seq_evento_fat
	from	dual;
	end;
end if;


if	(nvl(:new.qt_item,0) = 0) then
	begin
	insert	into pls_conta_pos_estab_log
		(nr_sequencia,
		vl_beneficiario,
		vl_beneficiario_old,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_conta,
		nr_seq_conta_mat,
		nr_seq_conta_proc,
		ds_log,
		vl_provisao_old,
		vl_provisao,
		vl_custo_operacional_old,
		vl_custo_operacional,
		vl_materiais_old,
		vl_materiais,
		vl_administracao,
		tx_administracao,
		vl_medico_old,
		vl_medico,
		vl_calculado_old,
		vl_calculado,
		vl_medico_calc,
		vl_medico_calc_old,
		vl_materiais_calc,
		vl_materiais_calc_old,
		vl_custo_operacional_calc_old,
		vl_custo_operacional_calc,
		vl_liberado_material_fat,
		vl_liberado_material_fat_old,
		vl_liberado_co_fat_old,
		vl_liberado_co_fat,
		vl_liberado_hi_fat,
		vl_liberado_hi_fat_old,
		vl_glosa_material_fat,
		vl_glosa_material_fat_old,
		vl_glosa_hi_fat,
		vl_glosa_hi_fat_old,
		vl_glosa_co_fat_old,
		vl_glosa_co_fat,
		vl_taxa_servico,
		vl_taxa_servico_old,
		vl_taxa_co,
		vl_taxa_co_old,
		vl_taxa_material_old,
		vl_taxa_material,
		vl_lib_taxa_co,
		vl_lib_taxa_co_old,
		vl_lib_taxa_material_old,
		vl_lib_taxa_material,
		vl_lib_taxa_servico,
		vl_lib_taxa_servico_old,
		vl_glosa_taxa_co_old,
		vl_glosa_taxa_co,
		vl_glosa_taxa_material_old,
		vl_glosa_taxa_material,
		vl_glosa_taxa_servico,
		vl_glosa_taxa_servico_old,
		nr_seq_lote_fat,
		nr_seq_lote_fat_old,
		nr_seq_evento_fat,
		nr_seq_evento_fat_old)
	select	pls_conta_pos_estab_log_seq.nextval,
		:new.vl_beneficiario,
		:old.vl_beneficiario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_seq_conta,
		:new.nr_seq_conta_mat,
		:new.nr_seq_conta_proc,
		'Quantidade do item zerado (qt_item anterior: '||:old.qt_item||') - '||dbms_utility.format_call_stack,
		:old.vl_provisao,
		:new.vl_provisao,
		:old.vl_custo_operacional,
		:new.vl_custo_operacional,
		:old.vl_materiais,
		:new.vl_materiais,
		:new.vl_administracao,
		:new.tx_administracao,
		:old.vl_medico,
		:new.vl_medico,
		:old.vl_calculado,
		:new.vl_calculado,
		:new.vl_medico_calc,
		:old.vl_medico_calc,
		:new.vl_materiais_calc,
		:old.vl_materiais_calc,
		:old.vl_custo_operacional_calc,
		:new.vl_custo_operacional_calc,
		:new.vl_liberado_material_fat,
		:old.vl_liberado_material_fat,
		:old.vl_liberado_co_fat,
		:new.vl_liberado_co_fat,
		:new.vl_liberado_hi_fat,
		:old.vl_liberado_hi_fat,
		:new.vl_glosa_material_fat,
		:old.vl_glosa_material_fat,
		:new.vl_glosa_hi_fat,
		:old.vl_glosa_hi_fat,
		:old.vl_glosa_co_fat,
		:new.vl_glosa_co_fat,
		:new.vl_taxa_servico,
		:old.vl_taxa_servico,
		:new.vl_taxa_co,
		:old.vl_taxa_co,
		:old.vl_taxa_material,
		:new.vl_taxa_material,
		:new.vl_lib_taxa_co,
		:old.vl_lib_taxa_co,
		:old.vl_lib_taxa_material,
		:new.vl_lib_taxa_material,
		:new.vl_lib_taxa_servico,
		:old.vl_lib_taxa_servico,
		:old.vl_glosa_taxa_co,
		:new.vl_glosa_taxa_co,
		:old.vl_glosa_taxa_material,
		:new.vl_glosa_taxa_material,
		:new.vl_glosa_taxa_servico,
		:old.vl_glosa_taxa_servico,
		:new.nr_seq_lote_fat,
		:old.nr_seq_lote_fat,
		:new.nr_seq_evento_fat,
		:old.nr_seq_evento_fat
	from	dual;
	end;
end if;



if	(nvl(:new.vl_beneficiario,0) = 0) and
	(nvl(:new.ie_status_faturamento,'L') = 'L') then
	begin
	:new.ie_status_faturamento	:= 'N';
	end;
end if;

-- tratamento para o pos estabelecido com base no A520. Quando for gerado.
-- se estiver atualizado e o antigo e 'A', deve permanecer 'A'.
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') and
	(updating) and
	(nvl(:old.ie_status_faturamento,'L')	= 'A') then

	:new.ie_status_faturamento := 'A'; -- Fixo 'A', para nao emitir cobranca no A520
end if;


if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') and
	(updating) and
	(nvl(:old.ie_cobrar_mensalidade,'L')	= 'A') then

	:new.ie_cobrar_mensalidade := 'A'; -- Fixo 'A', para nao emitir cobranca no A520
end if;

if	(updating) and
	(nvl(:old.ie_status_faturamento,'L')	!= nvl(:new.ie_status_faturamento,'L')) then
	begin
	insert 	into	pls_log_pos_estabelecido (nr_sequencia, dt_atualizacao,	nm_usuario,
						  dt_atualizacao_nrec, nm_usuario_nrec, ie_status_faturamento_ant,
						  ie_status_faturamento_new, nr_seq_conta, nr_seq_conta_pos, ie_tipo_registro,
						  ds_log)
		values				(pls_log_pos_estabelecido_seq.nextval,sysdate,:new.nm_usuario,
						 sysdate, :new.nm_usuario, :old.ie_status_faturamento,
						  :new.ie_status_faturamento, :new.nr_seq_conta, :new.nr_sequencia, 'H',
						  dbms_utility.format_call_stack) returning nr_sequencia into nr_seq_log_pos_estab_w;

	-- Contabilidade instantanea
	select	nvl(max(ie_concil_contab), 'N')
	into	ie_concil_contab_w
	from	pls_visible_false;

	if 	(ie_concil_contab_w = 'S') then
		pls_ctb_onl_gravar_movto_pck.gravar_movto_pos_estab_update(	:new.nr_sequencia,
										:new.nr_seq_conta,
										nr_seq_log_pos_estab_w,
										nvl(:new.nr_seq_conta_rec, 0),
										:new.ie_situacao,
										:new.ie_status_faturamento,
										nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
	end if;

	end;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.before_update_conta_pos_estab
before update ON TASY.PLS_CONTA_POS_ESTABELECIDO for each row
declare

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(:new.nr_seq_lote_fat is not null) and
		(:new.nr_seq_lote_disc is null) and /*aaschlote 13/05/2014 - Quando o item est� na discuss�o, pode fazer o refaturamento*/
		(nvl(:new.ie_status_faturamento,'X') <> nvl(:old.ie_status_faturamento,'X')) then

		wheb_mensagem_pck.exibir_mensagem_abort(190151);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_consistir_evento_lote_fat
after insert or update ON TASY.PLS_CONTA_POS_ESTABELECIDO for each row
declare

begin
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then

	if	(:new.nr_seq_lote_fat is not null) and
		(:new.nr_seq_evento_fat is null) then
		-- N�o � permitido conta p�s-estabelecido com lote de faturamento e sem evento informado.
		wheb_mensagem_pck.exibir_mensagem_abort(323392);
	end if;

	if	(:new.nr_seq_lote_fat is null) and
		(:new.nr_seq_evento_fat is not null) then
		-- N�o � permitido conta p�s-estabelecido sem lote de faturamento e com evento informado.
		wheb_mensagem_pck.exibir_mensagem_abort(323393);
	end if;

	if	(inserting) then
		insert 	into	pls_log_pos_estabelecido
			(nr_sequencia, dt_atualizacao,	nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, ie_status_faturamento_ant,
			ie_status_faturamento_new, nr_seq_conta, nr_seq_conta_pos,
			ie_tipo_registro, ds_log)
		values	(pls_log_pos_estabelecido_seq.nextval,sysdate,:new.nm_usuario,
			sysdate, :new.nm_usuario, :old.ie_status_faturamento,
			:new.ie_status_faturamento, :new.nr_seq_conta, :new.nr_sequencia,
			'H',dbms_utility.format_call_stack);
	end if;
end if;
end;
/


ALTER TABLE TASY.PLS_CONTA_POS_ESTABELECIDO ADD (
  CONSTRAINT PLSCPE_CK
 CHECK (((nr_seq_lote_fat is null and nr_seq_evento_fat is null)
					or (nr_seq_lote_fat is not null and nr_seq_evento_fat is not null))),
  CONSTRAINT PLSCOVB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_POS_ESTABELECIDO ADD (
  CONSTRAINT PLSCOVB_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_ATEND) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_EXEC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSTTXO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TX_OPME) 
 REFERENCES TASY.PLS_REGRA_TX_OPME (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSRGLC_FK 
 FOREIGN KEY (NR_SEQ_REGRA_LIMITE_MENS) 
 REFERENCES TASY.PLS_REGRA_LIMITE_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSCOVB_PLSANCO_FK 
 FOREIGN KEY (NR_SEQ_ANALISE) 
 REFERENCES TASY.PLS_ANALISE_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCONP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONV_PROC) 
 REFERENCES TASY.PLS_CONVERSAO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCMAL_FK 
 FOREIGN KEY (NR_SEQ_CONV_MAT) 
 REFERENCES TASY.PLS_CONVERSAO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PLSCOVB_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOVB_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_DISC) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSMESE_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE_SEG) 
 REFERENCES TASY.PLS_MENSALIDADE_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOVB_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOVB_PLSESCO_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_COOPERATIVA) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSEVFA_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_FAT) 
 REFERENCES TASY.PLS_EVENTO_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSLOFA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FAT) 
 REFERENCES TASY.PLS_LOTE_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_CONCONT_FK5 
 FOREIGN KEY (CD_CONTA_CRED_PROVISAO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOVB_CONCONT_FK6 
 FOREIGN KEY (CD_CONTA_DEB_PROVISAO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOVB_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO_FATURAMENTO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOVB_HISPADR_FK3 
 FOREIGN KEY (CD_HISTORICO_PROVISAO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOVB_PLSESCO_FK2 
 FOREIGN KEY (NR_SEQ_ESQUEMA_PROV) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSESCO_FK3 
 FOREIGN KEY (NR_SEQ_ESQUEMA_FATURAMENTO) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_HISPADR_FK4 
 FOREIGN KEY (CD_HISTORICO_REV_FAT) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOVB_PRGPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_HISPADR_FK5 
 FOREIGN KEY (CD_HISTORICO_PROVISAO_TAXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOVB_HISPADR_FK6 
 FOREIGN KEY (CD_HISTORICO_REV_FAT_TAXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOVB_PLSESCO_FK4 
 FOREIGN KEY (NR_SEQ_ESQUEMA_PROV_TAXA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_TISSTTA_FK 
 FOREIGN KEY (NR_SEQ_TISS_TAB_CONVERSAO) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSDIMA_FK 
 FOREIGN KEY (NR_SEQ_DISC_MAT) 
 REFERENCES TASY.PLS_DISCUSSAO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSDISP_FK 
 FOREIGN KEY (NR_SEQ_DISC_PROC) 
 REFERENCES TASY.PLS_DISCUSSAO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_PLSCPCA_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.PLS_CONTA_POS_CABECALHO (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA_CALCULO) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSCOVB_PLSCORF_FK 
 FOREIGN KEY (NR_SEQ_REC_FUTURA) 
 REFERENCES TASY.PLS_CONTA_RECEITA_FUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSCOVB_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTAB_FATURAMENTO) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOVB_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_PROV) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOVB_CONCONT_FK7 
 FOREIGN KEY (CD_CONTA_DEB_TAXA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOVB_CONCONT_FK8 
 FOREIGN KEY (CD_CONTA_CRED_TAXA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOVB_PRGLMAT_FK 
 FOREIGN KEY (NR_SEQ_MAT_REC) 
 REFERENCES TASY.PLS_REC_GLOSA_MAT (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_POS_ESTABELECIDO TO NIVEL_1;

