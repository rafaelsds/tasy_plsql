ALTER TABLE TASY.FIS_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_LOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_LOTE         VARCHAR2(2 BYTE)         NOT NULL,
  IE_STATUS_LOTE       VARCHAR2(2 BYTE),
  DT_INICIAL           DATE                     NOT NULL,
  DT_FINAL             DATE                     NOT NULL,
  DT_GERACAO           DATE,
  DT_FINALIZACAO       DATE,
  DS_LOTE              VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISLOTE_ESTABEL_FK_I ON TASY.FIS_LOTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISLOTE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FISLOTE_PK ON TASY.FIS_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISLOTE_PK
  MONITORING USAGE;


ALTER TABLE TASY.FIS_LOTE ADD (
  CONSTRAINT FISLOTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_LOTE ADD (
  CONSTRAINT FISLOTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.FIS_LOTE TO NIVEL_1;

