DROP TABLE TASY.EIS_COBRANCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_COBRANCA
(
  DT_REF_CONTA        DATE                      NOT NULL,
  DT_EMISSAO          DATE                      NOT NULL,
  DT_BAIXA            DATE                      NOT NULL,
  DT_VENCIMENTO       DATE                      NOT NULL,
  CD_CONVENIO         NUMBER(5)                 NOT NULL,
  QT_TITULO           NUMBER(7)                 NOT NULL,
  VL_TITULO           NUMBER(15,2)              NOT NULL,
  VL_RECEBIDO         NUMBER(15,2)              NOT NULL,
  VL_GLOSADO          NUMBER(15,2)              NOT NULL,
  VL_REC_MAIOR        NUMBER(15,2)              NOT NULL,
  VL_JURO_MULTA       NUMBER(15,2)              NOT NULL,
  VL_DESCONTO         NUMBER(15,2)              NOT NULL,
  VL_SALDO            NUMBER(15,2)              NOT NULL,
  VL_SEM_RETORNO      NUMBER(15,2)              NOT NULL,
  VL_A_MENOR          NUMBER(15,2)              NOT NULL,
  VL_ESTORNO          NUMBER(15,2)              NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  VL_DIA_ATRASO       NUMBER(15,2)              NOT NULL,
  VL_DIA_RECEB        NUMBER(15,2),
  DS_TIPO_PROTOCOLO   VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISCOBR_I1 ON TASY.EIS_COBRANCA
(DT_REF_CONTA, DT_EMISSAO, DT_BAIXA, DT_VENCIMENTO, CD_CONVENIO, 
CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


GRANT SELECT ON TASY.EIS_COBRANCA TO NIVEL_1;

