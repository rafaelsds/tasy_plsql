ALTER TABLE TASY.PLS_OC_CTA_VAL_NGUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_NGUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_NGUIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO_GUIA         VARCHAR2(2 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_NGUIA   NUMBER(10),
  QT_MIN_CARACTERES    NUMBER(7),
  DS_CARAC_VALIDO      VARCHAR2(255 BYTE),
  CD_GUIA_INVALIDO     VARCHAR2(20 BYTE),
  NR_SEQ_OC_CTA_COMB   NUMBER(10)               NOT NULL,
  IE_GUIA_DIFERENTE    VARCHAR2(3 BYTE),
  IE_GUIA_DIF_NULO     VARCHAR2(1 BYTE),
  QT_MAX_CARACTERES    NUMBER(7)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSNGUIA_PK ON TASY.PLS_OC_CTA_VAL_NGUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSNGUIA_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_NGUIA
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSNGUIA_PLSRNGUI_FK_I ON TASY.PLS_OC_CTA_VAL_NGUIA
(NR_SEQ_REGRA_NGUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_oc_cta_val_nguia_atual
before insert or update ON TASY.PLS_OC_CTA_VAL_NGUIA for each row
declare

begin
if	(:new.ie_guia_diferente = :new.ie_tipo_guia) then
	wheb_mensagem_pck.exibir_mensagem_abort(402642);
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_NGUIA_tp  after update ON TASY.PLS_OC_CTA_VAL_NGUIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_GUIA_DIF_NULO,1,4000),substr(:new.IE_GUIA_DIF_NULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_DIF_NULO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GUIA_DIFERENTE,1,4000),substr(:new.IE_GUIA_DIFERENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_DIFERENTE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_NGUIA,1,4000),substr(:new.NR_SEQ_REGRA_NGUIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_NGUIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CARAC_VALIDO,1,4000),substr(:new.DS_CARAC_VALIDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CARAC_VALIDO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GUIA_INVALIDO,1,4000),substr(:new.CD_GUIA_INVALIDO,1,4000),:new.nm_usuario,nr_seq_w,'CD_GUIA_INVALIDO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_CARACTERES,1,4000),substr(:new.QT_MIN_CARACTERES,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_CARACTERES',ie_log_w,ds_w,'PLS_OC_CTA_VAL_NGUIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_NGUIA ADD (
  CONSTRAINT PLSNGUIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_NGUIA ADD (
  CONSTRAINT PLSNGUIA_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSNGUIA_PLSRNGUI_FK 
 FOREIGN KEY (NR_SEQ_REGRA_NGUIA) 
 REFERENCES TASY.PLS_REGRA_NGUIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_NGUIA TO NIVEL_1;

