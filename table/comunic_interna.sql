ALTER TABLE TASY.COMUNIC_INTERNA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMUNIC_INTERNA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMUNIC_INTERNA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_COMUNICADO         DATE                    NOT NULL,
  DS_TITULO             VARCHAR2(255 BYTE)      NOT NULL,
  DS_COMUNICADO         LONG                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  IE_GERAL              VARCHAR2(1 BYTE),
  NM_USUARIO_DESTINO    VARCHAR2(4000 BYTE),
  CD_PERFIL             NUMBER(5),
  IE_GERENCIAL          VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_CLASSIF        NUMBER(10),
  DS_PERFIL_ADICIONAL   VARCHAR2(4000 BYTE),
  CD_SETOR_DESTINO      NUMBER(5),
  CD_ESTAB_DESTINO      NUMBER(4),
  DS_SETOR_ADICIONAL    VARCHAR2(2000 BYTE),
  DT_LIBERACAO          DATE,
  DS_GRUPO              VARCHAR2(255 BYTE),
  NM_USUARIO_OCULTO     VARCHAR2(100 BYTE),
  NR_ATENDIMENTO        NUMBER(10),
  DS_GRUPO_PERFIL       VARCHAR2(4000 BYTE),
  DS_USUARIOS_OCULTOS   VARCHAR2(2000 BYTE),
  NR_SEQ_AGENDA         NUMBER(10),
  DS_ESTAB_ADICIONAL    VARCHAR2(2000 BYTE),
  NR_SEQ_RESULTADO      NUMBER(10),
  NR_SEQ_EV_PAC_DEST    NUMBER(10),
  CD_PERFIL_ORIGEM      NUMBER(5),
  NR_COMUNIC_SETOR      NUMBER(10),
  DT_AGENDAMENTO        DATE,
  CD_SETOR_ORIGEM       NUMBER(5),
  IE_ENVIO_TERCEIRO     VARCHAR2(2 BYTE),
  CD_CARGO              NUMBER(10),
  CD_ESPECIALIDADE      NUMBER(5),
  IE_FUNCIONARIO        VARCHAR2(1 BYTE),
  DT_CANCELAMENTO       DATE,
  NR_SEQ_MOTIVO_CANCEL  NUMBER(10),
  IE_PLATAFORMA         VARCHAR2(1 BYTE)        DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          96M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMINTE_AGETASY_FK_I ON TASY.COMUNIC_INTERNA
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_AGETASY_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_ATEPACI_FK_I ON TASY.COMUNIC_INTERNA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMINTE_AVFRESU_FK_I ON TASY.COMUNIC_INTERNA
(NR_SEQ_RESULTADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_AVFRESU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_CARGO_FK_I ON TASY.COMUNIC_INTERNA
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_COMINCL_FK_I ON TASY.COMUNIC_INTERNA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_COMINCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_ESPMEDI_FK_I ON TASY.COMUNIC_INTERNA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_ESTABEL_FK_I ON TASY.COMUNIC_INTERNA
(CD_ESTAB_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_EVEVNPD_FK_I ON TASY.COMUNIC_INTERNA
(NR_SEQ_EV_PAC_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_EVEVNPD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_I1 ON TASY.COMUNIC_INTERNA
(DT_COMUNICADO, NM_USUARIO, CD_PERFIL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMINTE_I2 ON TASY.COMUNIC_INTERNA
(NR_SEQ_CLASSIF, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMINTE_I3 ON TASY.COMUNIC_INTERNA
(DT_AGENDAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMINTE_I5 ON TASY.COMUNIC_INTERNA
(DT_COMUNICADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMINTE_PK ON TASY.COMUNIC_INTERNA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMINTE_SETATEN_FK_I ON TASY.COMUNIC_INTERNA
(CD_SETOR_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMINTE_SETATEN_FK1_I ON TASY.COMUNIC_INTERNA
(CD_SETOR_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMINTE_SETATEN_FK1_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.COMUNIC_INTERNA_tp  after update ON TASY.COMUNIC_INTERNA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PERFIL_ORIGEM,1,4000),substr(:new.CD_PERFIL_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL_ORIGEM',ie_log_w,ds_w,'COMUNIC_INTERNA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Comunic_interna_insert
BEFORE INSERT ON TASY.COMUNIC_INTERNA FOR EACH ROW
DECLARE

nr_sequencia_w			number(10,0);
nm_usuario_oculto_w		Varchar2(255);
ie_tipo_w				Varchar2(255);
ie_usuario_oculto_destino_w		number(10,0);
ds_usuarios_copia_w		VARCHAR2(2000);
perfil_ativo_w			number(20);
begin

if	(:new.nr_sequencia is null) then
	select	comunic_interna_seq.nextval
	into	:new.nr_sequencia
	from	dual;
	:new.ie_geral	:= 'N';
	:new.ie_gerencial	:= 'N';
	:new.dt_liberacao	:= :new.dt_comunicado;
	:new.dt_atualizacao	:= sysdate;
end if;

select	max(nvl(vl_parametro, vl_parametro_padrao))
into	nm_usuario_oculto_w
from	funcao_parametro
where	cd_funcao		= 87
and	nr_sequencia		= 12;

select	nvl(max(ie_tipo),'C'),
	substr(max(ds_usuarios_copia) ||',',1,2000)
into	ie_tipo_w,
		ds_usuarios_copia_w
from	comunic_interna_classif
where	nr_sequencia		= :new.nr_seq_classif;

if	(nm_usuario_oculto_w is not null) and
	(ie_tipo_w 		<> 'G') and
	(:new.ie_gerencial	<> 'S') then
	/* Francisco - OS 60685 - Quando o usu�rio oculto estiver na lista destino deve aparecer entre os que leram */
	ie_usuario_oculto_destino_w := instr(:new.nm_usuario_destino,nm_usuario_oculto_w);
	if	( ie_usuario_oculto_destino_w = 0 ) then
		:new.nm_usuario_oculto	:= nm_usuario_oculto_w;
	end if;
else
	:new.nm_usuario_oculto	:= null;
end if;

if	(ds_usuarios_copia_w is not null) and
	(ds_usuarios_copia_w <> ',') then
	:new.ds_usuarios_ocultos  := substr(:new.ds_usuarios_ocultos|| ds_usuarios_copia_w,1,2000);
end if;

perfil_ativo_w := obter_perfil_ativo;

if (nvl(perfil_ativo_w,0) <> 0) then
	:new.cd_perfil_origem := perfil_ativo_w;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Comunic_interna_update
BEFORE update ON TASY.COMUNIC_INTERNA FOR EACH ROW
DECLARE

nr_sequencia_w		number(10,0);
nm_usuario_oculto_w		Varchar2(255);
ie_tipo_w			Varchar2(255);
ie_usuario_oculto_destino_w	varchar2(255);

begin

if	(:old.nm_usuario <> :new.nm_usuario) then
	-- N�o � poss�vel alterar uma comunica��o interna de outro usu�rio!
	wheb_mensagem_pck.exibir_mensagem_abort(266912);
end if;

select	max(nvl(vl_parametro, vl_parametro_padrao))
into	nm_usuario_oculto_w
from	funcao_parametro
where	cd_funcao		= 87
and	nr_sequencia		= 12;

select	nvl(max(ie_tipo),'C')
into	ie_tipo_w
from	comunic_interna_classif
where	nr_sequencia		= :new.nr_seq_classif;

if	(nm_usuario_oculto_w is not null) and
	(ie_tipo_w 		<> 'G') and
	(:new.ie_gerencial	<> 'S') then
	/* Francisco - OS 60685 - Quando o usu�rio oculto estiver na lista destino deve aparecer entre os que leram */
	ie_usuario_oculto_destino_w := instr(:new.nm_usuario_destino,nm_usuario_oculto_w);
	if	( ie_usuario_oculto_destino_w = 0 ) then
		:new.nm_usuario_oculto	:= nm_usuario_oculto_w;
	end if;
else
	:new.nm_usuario_oculto	:= null;
end if;

end;
/


ALTER TABLE TASY.COMUNIC_INTERNA ADD (
  CONSTRAINT COMINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMUNIC_INTERNA ADD (
  CONSTRAINT COMINTE_AVFRESU_FK 
 FOREIGN KEY (NR_SEQ_RESULTADO) 
 REFERENCES TASY.AVF_RESULTADO (NR_SEQUENCIA),
  CONSTRAINT COMINTE_COMINCL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.COMUNIC_INTERNA_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT COMINTE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_DESTINO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT COMINTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_DESTINO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COMINTE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT COMINTE_AGETASY_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_TASY (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COMINTE_EVEVNPD_FK 
 FOREIGN KEY (NR_SEQ_EV_PAC_DEST) 
 REFERENCES TASY.EV_EVENTO_PAC_DESTINO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COMINTE_SETATEN_FK1 
 FOREIGN KEY (CD_SETOR_ORIGEM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT COMINTE_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT COMINTE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.COMUNIC_INTERNA TO NIVEL_1;

