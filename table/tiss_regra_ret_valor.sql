ALTER TABLE TASY.TISS_REGRA_RET_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_RET_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_RET_VALOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  NR_SEQ_TISS_TABELA   NUMBER(10),
  IE_ORIGEM_VALOR      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRRV_CONVENI_FK_I ON TASY.TISS_REGRA_RET_VALOR
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRRV_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRRV_ESTABEL_FK_I ON TASY.TISS_REGRA_RET_VALOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRRV_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRRV_PK ON TASY.TISS_REGRA_RET_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRRV_TISSTTA_FK_I ON TASY.TISS_REGRA_RET_VALOR
(NR_SEQ_TISS_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_RET_VALOR_tp  after update ON TASY.TISS_REGRA_RET_VALOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_ORIGEM_VALOR,1,500);gravar_log_alteracao(substr(:old.IE_ORIGEM_VALOR,1,4000),substr(:new.IE_ORIGEM_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_VALOR',ie_log_w,ds_w,'TISS_REGRA_RET_VALOR',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TISS_TABELA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TISS_TABELA,1,4000),substr(:new.NR_SEQ_TISS_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TISS_TABELA',ie_log_w,ds_w,'TISS_REGRA_RET_VALOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_RET_VALOR ADD (
  CONSTRAINT TISSRRV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_RET_VALOR ADD (
  CONSTRAINT TISSRRV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRRV_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT TISSRRV_TISSTTA_FK 
 FOREIGN KEY (NR_SEQ_TISS_TABELA) 
 REFERENCES TASY.TISS_TIPO_TABELA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TISS_REGRA_RET_VALOR TO NIVEL_1;

