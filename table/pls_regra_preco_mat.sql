ALTER TABLE TASY.PLS_REGRA_PRECO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_MAT
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR             NUMBER(10),
  DT_INICIO_VIGENCIA           DATE             NOT NULL,
  DT_FIM_VIGENCIA              DATE,
  TX_AJUSTE                    NUMBER(15,4),
  VL_NEGOCIADO                 NUMBER(15,4),
  TX_AJUSTE_PFB                NUMBER(15,4),
  TX_AJUSTE_PMC_NEUT           NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_PMC_POS            NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_PMC_NEG            NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_SIMPRO_PFB         NUMBER(15,4),
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  TX_AJUSTE_SIMPRO_PMC         NUMBER(15,4)     NOT NULL,
  IE_ORIGEM_PRECO              VARCHAR2(5 BYTE) NOT NULL,
  NR_SEQ_MATERIAL_PRECO        NUMBER(10),
  IE_TIPO_DESPESA              VARCHAR2(2 BYTE),
  TX_AJUSTE_TAB_PROPRIA        NUMBER(15,4)     NOT NULL,
  IE_TIPO_TABELA               VARCHAR2(2 BYTE),
  NR_SEQ_OUTORGANTE            NUMBER(10),
  NR_SEQ_CONTRATO              NUMBER(10),
  TX_AJUSTE_BENEF_LIB          NUMBER(15,4),
  NR_SEQ_CONGENERE             NUMBER(10),
  NR_SEQ_REGRA_ANT             NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(4000 BYTE),
  NR_SEQ_PLANO                 NUMBER(10),
  IE_TIPO_VINCULO              VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO         NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR       NUMBER(10),
  IE_TIPO_CONTRATACAO          VARCHAR2(2 BYTE),
  IE_PRECO                     VARCHAR2(2 BYTE),
  IE_TIPO_SEGURADO             VARCHAR2(3 BYTE),
  NR_SEQ_GRUPO_CONTRATO        NUMBER(10),
  NR_SEQ_GRUPO_PRODUTO         NUMBER(10),
  NR_SEQ_MATERIAL              NUMBER(10),
  NR_SEQ_ESTRUTURA_MAT         NUMBER(10),
  IE_TABELA_ADICIONAL          NUMBER(10),
  CD_MATERIAL                  NUMBER(6),
  SG_UF_OPERADORA_INTERCAMBIO  VARCHAR2(2 BYTE),
  IE_TIPO_INTERCAMBIO          VARCHAR2(10 BYTE),
  CD_CONVENIO                  NUMBER(4),
  CD_CATEGORIA                 VARCHAR2(10 BYTE),
  NR_SEQ_CATEGORIA             NUMBER(10),
  NR_SEQ_TIPO_ACOMODACAO       NUMBER(10),
  NR_SEQ_TIPO_ATENDIMENTO      NUMBER(10),
  NR_SEQ_CLINICA               NUMBER(10),
  NR_SEQ_TIPO_PRESTADOR        NUMBER(10),
  IE_TIPO_GUIA                 VARCHAR2(2 BYTE),
  DT_BASE_FIXO                 DATE,
  IE_GENERICO_UNIMED           VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_UF              NUMBER(10),
  NR_SEQ_TIPO_USO              NUMBER(10),
  IE_MAT_AUTORIZACAO_ESP       VARCHAR2(1 BYTE),
  CD_PRESTADOR                 VARCHAR2(30 BYTE),
  NR_SEQ_GRUPO_MATERIAL        NUMBER(10),
  IE_INTERNADO                 VARCHAR2(1 BYTE),
  NR_CONTRATO                  NUMBER(10),
  NR_SEQ_CONGENERE_PROT        NUMBER(10),
  IE_PCMSO                     VARCHAR2(1 BYTE),
  IE_TIPO_PRECO_SIMPRO         VARCHAR2(1 BYTE),
  IE_TIPO_PRECO_BRASINDICE     VARCHAR2(3 BYTE),
  NR_SEQ_REGRA_MAT_REF         NUMBER(10),
  IE_TIPO_ACOMODACAO_PTU       VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_OPERADORA       NUMBER(10),
  IE_TIPO_AJUSTE_PFB           VARCHAR2(1 BYTE),
  VL_PFB_NEUTRA_SIMPRO         NUMBER(15,4),
  VL_PFB_POSITIVA_SIMPRO       NUMBER(15,4),
  VL_PFB_NEGATIVA_SIMPRO       NUMBER(15,4),
  VL_PFB_NAO_APLICAVEL_SIMPRO  NUMBER(15,4),
  VL_PFB_NEUTRA_BRASINDICE     NUMBER(15,4),
  VL_PFB_POSITIVA_BRASINDICE   NUMBER(15,4),
  VL_PFB_NEGATIVA_BRASINDICE   NUMBER(15,4),
  NR_SEQ_INTERCAMBIO           NUMBER(10),
  CD_ESPECIALIDADE_PREST       NUMBER(5),
  IE_COOPERADO                 VARCHAR2(1 BYTE),
  DS_REGRA                     VARCHAR2(255 BYTE),
  IE_NAO_GERA_TX_INTER         VARCHAR2(1 BYTE),
  IE_TIPO_PRESTADOR            VARCHAR2(2 BYTE),
  CD_PRESTADOR_PROT            VARCHAR2(30 BYTE),
  NR_SEQ_TIPO_PRESTADOR_PROT   NUMBER(10),
  NR_SEQ_REGRA_ATEND_CART      NUMBER(10),
  NR_SEQ_CP_COMBINADA          NUMBER(10),
  IE_ORIGEM_PROTOCOLO          VARCHAR2(1 BYTE),
  IE_ATEND_PCMSO               VARCHAR2(1 BYTE),
  IE_REF_GUIA_INTERNACAO       VARCHAR2(1 BYTE),
  IE_TIPO_PRECO_TAB_ADIC       VARCHAR2(3 BYTE),
  NR_SEQ_PRESTADOR_INTER       NUMBER(10),
  CD_VERSAO_TISS               VARCHAR2(20 BYTE),
  CD_PRESTADOR_SOLIC           VARCHAR2(30 BYTE),
  DT_INICIO_VIGENCIA_REF       DATE,
  DT_FIM_VIGENCIA_REF          DATE,
  IE_RESTRINGE_HOSP            VARCHAR2(1 BYTE),
  IE_ACOMODACAO_AUTORIZADA     VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_ATEND_PRINC      NUMBER(10),
  IE_TIPO_ATENDIMENTO          VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_AJUSTE          NUMBER(10),
  NR_SEQ_GRUPO_PREST_INT       NUMBER(10),
  PR_ICMS                      NUMBER(12,4),
  NR_SEQ_MOT_REEMBOLSO         NUMBER(10),
  IE_MED_ONCOLOGICO            VARCHAR2(3 BYTE),
  QT_IDADE_INICIAL             NUMBER(5),
  QT_IDADE_FINAL               NUMBER(5),
  IE_UTILIZAR_DUAS_CASAS       VARCHAR2(3 BYTE),
  IE_ULT_VALOR_FABRICA         VARCHAR2(3 BYTE),
  DS_NEGOCIACAO                VARCHAR2(4000 BYTE),
  IE_GERAR_REMIDO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREPM_CATCONV_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_CONVENI_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_ESPMEDI_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(CD_ESPECIALIDADE_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_ESTABEL_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_I1 ON TASY.PLS_REGRA_PRECO_MAT
(IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_MATERIA_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSREPM_PK ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_PLSCATE_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSCATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSCLIN_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSCLIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSCLPR_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSCONG_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSCONG_FK1_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CONGENERE_PROT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSCONG_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSCONT_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSCPCC_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_CP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_PLSESMAT_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_ESTRUTURA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSESMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSGAMT_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_PLSINCA_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSMAPR_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_MATERIAL_PRECO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSMAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSMAT_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSMORE_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_MOT_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_PLSOACA_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_REGRA_ATEND_CART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_PLSOUTO_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_OUTORGANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSOUTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPGOP_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPGOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPGPI_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_PREST_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPM_PLSPGRP_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPGRP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPLAN_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPRES_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPRGC_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPRGC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPRGM_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPRGM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPRGP_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSPRGU_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_GRUPO_UF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSPRGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSTIAC_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSTIAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSTIAT_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSTIAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSTIPR_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSTIPR_FK1_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_TIPO_PRESTADOR_PROT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSTIPR_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPM_PLSTUMA_FK_I ON TASY.PLS_REGRA_PRECO_MAT
(NR_SEQ_TIPO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPM_PLSTUMA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_regra_preco_mat_delete
after delete ON TASY.PLS_REGRA_PRECO_MAT for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_REGRA_PRECO_MAT',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_PRECO_MAT_tp  after update ON TASY.PLS_REGRA_PRECO_MAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_GRUPO_AJUSTE,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_AJUSTE,1,4000),substr(:new.NR_SEQ_GRUPO_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_AJUSTE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRESTADOR,1,4000),substr(:new.IE_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_COMBINADA,1,4000),substr(:new.NR_SEQ_CP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_COMBINADA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATEND_PCMSO,1,4000),substr(:new.IE_ATEND_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATEND_PCMSO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_ATEND_CART,1,4000),substr(:new.NR_SEQ_REGRA_ATEND_CART,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_ATEND_CART',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COOPERADO,1,4000),substr(:new.IE_COOPERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COOPERADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_PROT,1,4000),substr(:new.CD_PRESTADOR_PROT,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_PROT',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR_PROT,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR_PROT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR_PROT',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_GERA_TX_INTER,1,4000),substr(:new.IE_NAO_GERA_TX_INTER,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_GERA_TX_INTER',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROTOCOLO,1,4000),substr(:new.IE_ORIGEM_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROTOCOLO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REF_GUIA_INTERNACAO,1,4000),substr(:new.IE_REF_GUIA_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REF_GUIA_INTERNACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_VERSAO_TISS,1,4000),substr(:new.CD_VERSAO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO_TISS',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO_TAB_ADIC,1,4000),substr(:new.IE_TIPO_PRECO_TAB_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO_TAB_ADIC',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR_INTER,1,4000),substr(:new.NR_SEQ_PRESTADOR_INTER,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR_INTER',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE_PREST,1,4000),substr(:new.CD_ESPECIALIDADE_PREST,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE_PREST',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE,1,4000),substr(:new.TX_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_NEGOCIADO,1,4000),substr(:new.VL_NEGOCIADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_NEGOCIADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PRECO,1,4000),substr(:new.IE_ORIGEM_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PRECO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PFB,1,4000),substr(:new.TX_AJUSTE_PFB,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PFB',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PMC_NEUT,1,4000),substr(:new.TX_AJUSTE_PMC_NEUT,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PMC_NEUT',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PMC_POS,1,4000),substr(:new.TX_AJUSTE_PMC_POS,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PMC_POS',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PMC_NEG,1,4000),substr(:new.TX_AJUSTE_PMC_NEG,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PMC_NEG',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_SIMPRO_PFB,1,4000),substr(:new.TX_AJUSTE_SIMPRO_PFB,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_SIMPRO_PFB',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_SIMPRO_PMC,1,4000),substr(:new.TX_AJUSTE_SIMPRO_PMC,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_SIMPRO_PMC',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL_PRECO,1,4000),substr(:new.NR_SEQ_MATERIAL_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL_PRECO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_TAB_PROPRIA,1,4000),substr(:new.TX_AJUSTE_TAB_PROPRIA,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_TAB_PROPRIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESPESA,1,4000),substr(:new.IE_TIPO_DESPESA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESPESA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_TABELA,1,4000),substr(:new.IE_TIPO_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_TABELA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATEND_PRINC,1,4000),substr(:new.NR_SEQ_TIPO_ATEND_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATEND_PRINC',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_ICMS,1,4000),substr(:new.PR_ICMS,1,4000),:new.nm_usuario,nr_seq_w,'PR_ICMS',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_SOLIC,1,4000),substr(:new.CD_PRESTADOR_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_SOLIC',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACOMODACAO_AUTORIZADA,1,4000),substr(:new.IE_ACOMODACAO_AUTORIZADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACOMODACAO_AUTORIZADA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTRINGE_HOSP,1,4000),substr(:new.IE_RESTRINGE_HOSP,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTRINGE_HOSP',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOT_REEMBOLSO,1,4000),substr(:new.NR_SEQ_MOT_REEMBOLSO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOT_REEMBOLSO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ULT_VALOR_FABRICA,1,4000),substr(:new.IE_ULT_VALOR_FABRICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ULT_VALOR_FABRICA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UTILIZAR_DUAS_CASAS,1,4000),substr(:new.IE_UTILIZAR_DUAS_CASAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_UTILIZAR_DUAS_CASAS',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_INICIAL,1,4000),substr(:new.QT_IDADE_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_INICIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MED_ONCOLOGICO,1,4000),substr(:new.IE_MED_ONCOLOGICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MED_ONCOLOGICO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_FINAL,1,4000),substr(:new.QT_IDADE_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_FINAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OUTORGANTE,1,4000),substr(:new.NR_SEQ_OUTORGANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OUTORGANTE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTRATO,1,4000),substr(:new.NR_SEQ_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_BENEF_LIB,1,4000),substr(:new.TX_AJUSTE_BENEF_LIB,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_BENEF_LIB',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE,1,4000),substr(:new.NR_SEQ_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TABELA_ADICIONAL,1,4000),substr(:new.IE_TABELA_ADICIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TABELA_ADICIONAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_VINCULO,1,4000),substr(:new.IE_TIPO_VINCULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VINCULO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_GRUPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONTRATACAO,1,4000),substr(:new.IE_TIPO_CONTRATACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONTRATACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO,1,4000),substr(:new.IE_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SEGURADO,1,4000),substr(:new.IE_TIPO_SEGURADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SEGURADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_CONTRATO,1,4000),substr(:new.NR_SEQ_GRUPO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRODUTO,1,4000),substr(:new.NR_SEQ_GRUPO_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRODUTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PLANO,1,4000),substr(:new.NR_SEQ_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PLANO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA_MAT,1,4000),substr(:new.NR_SEQ_ESTRUTURA_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA_MAT',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_UF_OPERADORA_INTERCAMBIO,1,4000),substr(:new.SG_UF_OPERADORA_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'SG_UF_OPERADORA_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_INTERCAMBIO,1,4000),substr(:new.IE_TIPO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_BASE_FIXO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_BASE_FIXO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_BASE_FIXO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ACOMODACAO,1,4000),substr(:new.NR_SEQ_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ACOMODACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATENDIMENTO,1,4000),substr(:new.NR_SEQ_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR,1,4000),substr(:new.CD_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLINICA,1,4000),substr(:new.NR_SEQ_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLINICA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MAT_AUTORIZACAO_ESP,1,4000),substr(:new.IE_MAT_AUTORIZACAO_ESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_MAT_AUTORIZACAO_ESP',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_USO,1,4000),substr(:new.NR_SEQ_TIPO_USO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_USO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GENERICO_UNIMED,1,4000),substr(:new.IE_GENERICO_UNIMED,1,4000),:new.nm_usuario,nr_seq_w,'IE_GENERICO_UNIMED',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_UF,1,4000),substr(:new.NR_SEQ_GRUPO_UF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_UF',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CATEGORIA,1,4000),substr(:new.NR_SEQ_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CATEGORIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_MATERIAL,1,4000),substr(:new.NR_SEQ_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_MATERIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTERNADO,1,4000),substr(:new.IE_INTERNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTERNADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTRATO,1,4000),substr(:new.NR_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO_SIMPRO,1,4000),substr(:new.IE_TIPO_PRECO_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO_SIMPRO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO_BRASINDICE,1,4000),substr(:new.IE_TIPO_PRECO_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO_BRASINDICE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PCMSO,1,4000),substr(:new.IE_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PCMSO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE_PROT,1,4000),substr(:new.NR_SEQ_CONGENERE_PROT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE_PROT',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_MAT_REF,1,4000),substr(:new.NR_SEQ_REGRA_MAT_REF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_MAT_REF',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ACOMODACAO_PTU,1,4000),substr(:new.IE_TIPO_ACOMODACAO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ACOMODACAO_PTU',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_OPERADORA,1,4000),substr(:new.NR_SEQ_GRUPO_OPERADORA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_OPERADORA',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_AJUSTE_PFB,1,4000),substr(:new.IE_TIPO_AJUSTE_PFB,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_AJUSTE_PFB',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEUTRA_SIMPRO,1,4000),substr(:new.VL_PFB_NEUTRA_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEUTRA_SIMPRO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_POSITIVA_SIMPRO,1,4000),substr(:new.VL_PFB_POSITIVA_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_POSITIVA_SIMPRO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEGATIVA_SIMPRO,1,4000),substr(:new.VL_PFB_NEGATIVA_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEGATIVA_SIMPRO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NAO_APLICAVEL_SIMPRO,1,4000),substr(:new.VL_PFB_NAO_APLICAVEL_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NAO_APLICAVEL_SIMPRO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEUTRA_BRASINDICE,1,4000),substr(:new.VL_PFB_NEUTRA_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEUTRA_BRASINDICE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_POSITIVA_BRASINDICE,1,4000),substr(:new.VL_PFB_POSITIVA_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_POSITIVA_BRASINDICE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PFB_NEGATIVA_BRASINDICE,1,4000),substr(:new.VL_PFB_NEGATIVA_BRASINDICE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PFB_NEGATIVA_BRASINDICE',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INTERCAMBIO,1,4000),substr(:new.NR_SEQ_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_MAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_PRECO_MAT_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_REGRA_PRECO_MAT FOR EACH ROW
declare

cd_material_w	number(6);

begin

-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		select	max(cd_material)
		into	cd_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;

		:new.cd_material	:= cd_material_w;
	end if;
end if;

END;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_MAT ADD (
  CONSTRAINT PLSREPM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_MAT ADD (
  CONSTRAINT PLSREPM_PLSOACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ATEND_CART) 
 REFERENCES TASY.PLS_OC_ATEND_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMBINADA) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSGAMT_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_AJUSTE) 
 REFERENCES TASY.PLS_GRUPO_AJUSTE_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPGPI_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PREST_INT) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PREST_INT (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSMORE_FK 
 FOREIGN KEY (NR_SEQ_MOT_REEMBOLSO) 
 REFERENCES TASY.PLS_MOTIVO_REEMBOLSO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSTIPR_FK1 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR_PROT) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_PREST) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PLSREPM_PLSPGOP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_OPERADORA) 
 REFERENCES TASY.PLS_PRECO_GRUPO_OPERADORA (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSCONG_FK1 
 FOREIGN KEY (NR_SEQ_CONGENERE_PROT) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPGRP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRODUTO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRODUTO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA_MAT) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PLSREPM_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT PLSREPM_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PLSREPM_PLSCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.PLS_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPRGU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_UF) 
 REFERENCES TASY.PLS_PRECO_GRUPO_UF (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSTUMA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_USO) 
 REFERENCES TASY.PLS_TIPO_USO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSPRGM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MATERIAL) 
 REFERENCES TASY.PLS_PRECO_GRUPO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSREPM_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSMAPR_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_PRECO) 
 REFERENCES TASY.PLS_MATERIAL_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSOUTO_FK 
 FOREIGN KEY (NR_SEQ_OUTORGANTE) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSREPM_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_MAT TO NIVEL_1;

