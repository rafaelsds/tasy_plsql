DROP TABLE TASY.REG_CUSTOMER_REQ_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_CUSTOMER_REQ_HIST
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_CODE                 VARCHAR2(255 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_TITLE                VARCHAR2(255 BYTE)    NOT NULL,
  DT_APROVACAO            DATE,
  NR_SEQ_APRESENTACAO     NUMBER(10,3),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_VERSAO               VARCHAR2(255 BYTE),
  NR_SEQ_REG_VERSION_REV  NUMBER(10),
  IE_TIPO_ALTERACAO       VARCHAR2(1 BYTE),
  NR_SEQ_FEATURES         NUMBER(10)            NOT NULL,
  NR_SEQ_CUSTOMER_REQ     NUMBER(10),
  DS_RATIONALE            VARCHAR2(4000 BYTE)   NOT NULL,
  NR_SEQ_ESTAGIO          NUMBER(10),
  CD_CRS_ID               VARCHAR2(35 BYTE),
  NM_CURTO                VARCHAR2(255 BYTE),
  NM_LIBERACAO_VV         VARCHAR2(15 BYTE),
  DT_LIBERACAO_VV         DATE,
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_MOTIVO_INATIVACAO    VARCHAR2(255 BYTE),
  DS_DESCRICAO_CR         VARCHAR2(4000 BYTE),
  IE_INTERNAL_FEATURE     VARCHAR2(1 BYTE),
  DS_DESCRIPTION          CLOB,
  DT_REVISAO              DATE,
  IE_TIPO_REVISAO         VARCHAR2(2 BYTE),
  NR_SEQ_REVISAO          NUMBER(10),
  IE_CLINICO              VARCHAR2(1 BYTE),
  DS_RACIONAL_CLINICO     VARCHAR2(4000 BYTE),
  NM_USUARIO_APROVACAO    VARCHAR2(15 BYTE),
  DT_LIBERACAO            DATE,
  NM_USUARIO_LIBERACAO    VARCHAR2(15 BYTE),
  DT_LANCAMENTO           DATE,
  NM_USUARIO_LANCAMENTO   VARCHAR2(15 BYTE),
  NR_SEQ_ANALISE_IMPACTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_DESCRIPTION) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCRQ_MANOSIM_FK_I ON TASY.REG_CUSTOMER_REQ_HIST
(NR_SEQ_ANALISE_IMPACTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCRQ_REGCR_FK_I ON TASY.REG_CUSTOMER_REQ_HIST
(NR_SEQ_CUSTOMER_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCRQ_REGEST_FK_I ON TASY.REG_CUSTOMER_REQ_HIST
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCRQ_REGFEC_FK_I ON TASY.REG_CUSTOMER_REQ_HIST
(NR_SEQ_FEATURES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCRQ_REGVERREV_FK_I ON TASY.REG_CUSTOMER_REQ_HIST
(NR_SEQ_REG_VERSION_REV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_CUSTOMER_REQ_HIST ADD (
  CONSTRAINT REGCRQ_MANOSIM_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_IMPACTO) 
 REFERENCES TASY.MAN_ORDEM_SERV_IMPACTO (NR_SEQUENCIA),
  CONSTRAINT REGCRQ_REGCR_FK 
 FOREIGN KEY (NR_SEQ_CUSTOMER_REQ) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGCRQ_REGEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.REG_ESTAGIO_CR (NR_SEQUENCIA),
  CONSTRAINT REGCRQ_REGFEC_FK 
 FOREIGN KEY (NR_SEQ_FEATURES) 
 REFERENCES TASY.REG_FEATURES_CUSTOMER (NR_SEQUENCIA),
  CONSTRAINT REGCRQ_REGVERREV_FK 
 FOREIGN KEY (NR_SEQ_REG_VERSION_REV) 
 REFERENCES TASY.REG_VERSION_REVISION (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REG_CUSTOMER_REQ_HIST TO NIVEL_1;

