ALTER TABLE TASY.QUA_AUDITORIA_CAP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_AUDITORIA_CAP CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_AUDITORIA_CAP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_CAPITULO          VARCHAR2(80 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_APRES         NUMBER(5),
  CD_EXP_CAPITULO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUAAUCA_DICEXPR_FK_I ON TASY.QUA_AUDITORIA_CAP
(CD_EXP_CAPITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUAAUCA_PK ON TASY.QUA_AUDITORIA_CAP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAUCA_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUAAUCA_QUAAUTI_FK_I ON TASY.QUA_AUDITORIA_CAP
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QUA_AUDITORIA_CAP ADD (
  CONSTRAINT QUAAUCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_AUDITORIA_CAP ADD (
  CONSTRAINT QUAAUCA_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CAPITULO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT QUAAUCA_QUAAUTI_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.QUA_AUDITORIA_TIPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_AUDITORIA_CAP TO NIVEL_1;

