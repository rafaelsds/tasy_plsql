ALTER TABLE TASY.ESCALA_ABCD2
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ABCD2 CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ABCD2
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  QT_ABCD2               NUMBER(2),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  IE_AGE                 NUMBER(2),
  IE_BLOOD_PRESSURE      NUMBER(2),
  IE_CLINICAL_FEATURES   NUMBER(2),
  IE_SYMPTOMS_DURATION   NUMBER(2),
  IE_DIABETES            NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCABCD2_ATEPACI_FK_I ON TASY.ESCALA_ABCD2
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCABCD2_PESFISI_FK_I ON TASY.ESCALA_ABCD2
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCABCD2_PK ON TASY.ESCALA_ABCD2
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_abcd2_atual

before insert or update ON TASY.ESCALA_ABCD2 
for each row
declare



begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then



	:new.qt_abcd2 :=      :new.ie_age +

				:new.ie_blood_pressure +

				:new.ie_clinical_features +

				:new.ie_symptoms_duration +

				:new.ie_diabetes ;


end if;

end;
/


ALTER TABLE TASY.ESCALA_ABCD2 ADD (
  CONSTRAINT ESCABCD2_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_ABCD2 ADD (
  CONSTRAINT ESCABCD2_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCABCD2_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_ABCD2 TO NIVEL_1;

