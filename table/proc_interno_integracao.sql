ALTER TABLE TASY.PROC_INTERNO_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INTERNO_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INTERNO_INTEGRACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO      NUMBER(10),
  NR_SEQ_SISTEMA_INTEG     NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4),
  CD_INTEGRACAO            VARCHAR2(60 BYTE),
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  CD_TIPO_PROCEDIMENTO     NUMBER(3),
  IE_UTILIZA_ESTAB         VARCHAR2(1 BYTE),
  DS_INTERFACE_IDENTIFIER  VARCHAR2(20 BYTE),
  IE_N_ENVIA_ASSOCIADO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROINTI_ESTABEL_FK_I ON TASY.PROC_INTERNO_INTEGRACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROINTI_PK ON TASY.PROC_INTERNO_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROINTI_PROCEDI_FK_I ON TASY.PROC_INTERNO_INTEGRACAO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINTI_PROINTE_FK_I ON TASY.PROC_INTERNO_INTEGRACAO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTI_SISINTE_FK_I ON TASY.PROC_INTERNO_INTEGRACAO
(NR_SEQ_SISTEMA_INTEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROC_INTERNO_INTEGRACAO ADD (
  CONSTRAINT PROINTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INTERNO_INTEGRACAO ADD (
  CONSTRAINT PROINTI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PROINTI_SISINTE_FK 
 FOREIGN KEY (NR_SEQ_SISTEMA_INTEG) 
 REFERENCES TASY.SISTEMA_INTEGRACAO (NR_SEQUENCIA),
  CONSTRAINT PROINTI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PROINTI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PROC_INTERNO_INTEGRACAO TO NIVEL_1;

