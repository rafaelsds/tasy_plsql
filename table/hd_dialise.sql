ALTER TABLE TASY.HD_DIALISE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISE CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISE
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE) NOT NULL,
  DT_DIALISE                    DATE            NOT NULL,
  DT_INICIO_DIALISE             DATE,
  DT_FIM_DIALISE                DATE,
  CD_PF_INICIO_DIALISE          VARCHAR2(10 BYTE),
  CD_PF_FIM_DIALISE             VARCHAR2(10 BYTE),
  QT_PA_SIST_PRE_DEITADO        NUMBER(3),
  QT_PA_DIAST_PRE_DEITADO       NUMBER(3),
  QT_PA_SIST_PRE_PE             NUMBER(3),
  QT_PA_DIAST_PRE_PE            NUMBER(3),
  QT_PA_SIST_POS_DEITADO        NUMBER(3),
  QT_PA_DIAST_POS_DEITADO       NUMBER(3),
  QT_PA_SIST_POS_PE             NUMBER(3),
  QT_PA_DIAST_POS_PE            NUMBER(3),
  QT_PESO_PRE                   NUMBER(6,3),
  QT_PESO_POS                   NUMBER(6,3),
  NR_SEQ_PACIENTE               NUMBER(10)      NOT NULL,
  DT_INSTALACAO                 DATE,
  CD_PF_INSTALACAO              VARCHAR2(10 BYTE),
  QT_GPID                       NUMBER(15,7),
  DS_MOTIVO_CANCELAMENTO        VARCHAR2(255 BYTE),
  DT_CANCELAMENTO               DATE,
  CD_PF_CANCELAMENTO            VARCHAR2(10 BYTE),
  NR_SEQ_UNID_DIALISE           NUMBER(10)      NOT NULL,
  NR_ATENDIMENTO                NUMBER(10),
  DS_OBSERVACAO                 VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_FIM             NUMBER(10),
  NR_SEQ_MOTIVO_PESO_PRE        NUMBER(10),
  NR_SEQ_MOTIVO_PA_DEITADO_PRE  NUMBER(10),
  NR_SEQ_MOTIVO_PA_PE_PRE       NUMBER(10),
  QT_VALOR_GPID                 NUMBER(15,7),
  NR_SEQ_MOTIVO_PA_DEITADO_POS  NUMBER(10),
  NR_SEQ_MOTIVO_PA_PE_POS       NUMBER(10),
  NR_SEQ_MOTIVO_PESO_POS        NUMBER(10),
  QT_PESO_IDEAL                 NUMBER(6,3),
  QT_SORO_REPOSICAO             NUMBER(10),
  QT_SORO_DEVOLUCAO             NUMBER(10),
  NR_DIALISE_ATEND_PAC          NUMBER(10),
  NR_SEQ_TIPO_SORO              NUMBER(10),
  IE_TIPO_DIALISE               VARCHAR2(15 BYTE),
  QT_FREQ_CARDIACA              NUMBER(3),
  QT_ULTRAFILTRACAO             NUMBER(15,4),
  QT_TEMP_AXILIAR_POS           NUMBER(15,4),
  QT_TEMPO_DIALISE              NUMBER(5),
  IE_PACIENTE_AGUDO             VARCHAR2(1 BYTE),
  NR_PRESCRICAO                 NUMBER(14),
  QT_FREQ_RESP_PRE              NUMBER(3),
  QT_GLICEMIA_CAPILAR_PRE       NUMBER(15,1),
  QT_INSULINA_PRE               NUMBER(15,4),
  QT_GLICOSE_ADM_PRE            NUMBER(5,1),
  QT_SATURACAO_O2_PRE           NUMBER(3),
  QT_FREQ_RESP_POS              NUMBER(3),
  QT_GLICEMIA_CAPILAR_POS       NUMBER(15,1),
  QT_INSULINA_POS               NUMBER(15,4),
  QT_GLICOSE_ADM_POS            NUMBER(5,1),
  QT_SATURACAO_O2_POS           NUMBER(3),
  QT_TEMP_AUXILIAR_PRE          NUMBER(4,1),
  QT_FREQ_CARDIACA_PRE          NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDIALE_ATEPACI_FK_I ON TASY.HD_DIALISE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_HDMOTDAPA_FK_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_PESO_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDMOTDAPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_HDMOTDAPA_FK2_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_PA_DEITADO_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDMOTDAPA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_HDMOTDAPA_FK3_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_PA_PE_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDMOTDAPA_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_HDMOTDAPA_FK4_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_PA_DEITADO_POS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDMOTDAPA_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_HDMOTDAPA_FK5_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_PA_PE_POS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDMOTDAPA_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_HDMOTDAPA_FK6_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_PESO_POS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDMOTDAPA_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_HDPAREC_FK_I ON TASY.HD_DIALISE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_HDUNIDD_FK1_I ON TASY.HD_DIALISE
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_HDUNIDD_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_I1 ON TASY.HD_DIALISE
(NR_ATENDIMENTO, DT_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_I2 ON TASY.HD_DIALISE
(DT_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_MOTIFIM_FK_I ON TASY.HD_DIALISE
(NR_SEQ_MOTIVO_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALE_MOTIFIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALE_PESFISI_FK_I ON TASY.HD_DIALISE
(CD_PF_FIM_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_PESFISI_FK2_I ON TASY.HD_DIALISE
(CD_PF_INICIO_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_PESFISI_FK3_I ON TASY.HD_DIALISE
(CD_PF_INSTALACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_PESFISI_FK4_I ON TASY.HD_DIALISE
(CD_PF_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDDIALE_PK ON TASY.HD_DIALISE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_PRESMED_FK_I ON TASY.HD_DIALISE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALE_3 ON TASY.HD_DIALISE
(NR_ATENDIMENTO, DT_CANCELAMENTO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_dialise_bef_insert_update
before insert or update ON TASY.HD_DIALISE for each row
declare

begin

if (nvl(:new.qt_peso_pre,0) <> nvl(:old.qt_peso_pre,0)) then
	cpoe_update_peso_atual_dialise( :new.nr_atendimento, :new.dt_dialise, :new.qt_peso_pre);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hd_dialise_biu_agenda
    AFTER INSERT OR UPDATE ON TASY.HD_DIALISE     FOR EACH ROW
DECLARE
    cd_pessoa_fisica_w VARCHAR2(10) := nvl(:new.cd_pessoa_fisica, :old.cd_pessoa_fisica);
    dt_agenda_w        DATE := ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_dialise, :old.dt_dialise));
    nr_seq_dialise_w   NUMBER := nvl(:new.nr_sequencia, :old.nr_sequencia);
    ie_status_agenda_w   VARCHAR2(2);
    nr_seq_agenda_w    NUMBER;
    nm_usuario_w       VARCHAR2(20) := :new.nm_usuario;
	nr_seq_ag_dial_w	hd_agenda_dialise.nr_sequencia%type;
BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

    IF updating('DT_FIM_DIALISE') OR
       updating('DT_CANCELAMENTO') OR
       updating('DT_INICIO_DIALISE') OR
       inserting THEN
        SELECT MAX(a.nr_seq_agenda)
          INTO nr_seq_agenda_w
          FROM hd_agenda_dialise a
         WHERE a.cd_pessoa_fisica = cd_pessoa_fisica_w
           AND a.dt_agenda = dt_agenda_w
           AND a.nr_seq_dialise = nr_seq_dialise_w;

        IF nr_seq_agenda_w IS NULL THEN

            SELECT MAX(a.nr_seq_agenda),
					max(a.nr_sequencia)
              INTO nr_seq_agenda_w,
					nr_seq_ag_dial_w
              FROM hd_agenda_dialise a
             WHERE a.cd_pessoa_fisica = cd_pessoa_fisica_w
               AND a.dt_agenda = dt_agenda_w
               AND a.nr_seq_dialise IS NULL
			   and	a.ie_situacao = 'A';

            UPDATE hd_agenda_dialise
               SET nr_seq_dialise = nr_seq_dialise_w,
                   dt_atualizacao = SYSDATE,
                   nm_usuario     = nm_usuario_w
             WHERE nr_sequencia = nr_seq_ag_dial_w;

        END IF;
		if	(updating) then
			IF updating('DT_INICIO_DIALISE') THEN
				ie_status_agenda_w := 'IN';
			END IF;

			IF updating('DT_FIM_DIALISE') THEN
				ie_status_agenda_w := 'FI';
			END IF;
			IF updating('DT_CANCELAMENTO') THEN
				ie_status_agenda_w := 'C';
			END IF;

			if	(ie_status_agenda_w is not null) then
				UPDATE agenda_consulta ac
				   SET ac.ie_status_agenda = ie_status_agenda_w,
					   ac.dt_atualizacao   = SYSDATE,
					   ac.nm_usuario       = nm_usuario_w
				WHERE ac.nr_sequencia = nr_seq_agenda_w;
			end if;
		end if;
    END IF;

<<final>>
null;

END;
/


CREATE OR REPLACE TRIGGER TASY.hd_dialise_befup_sign
before update ON TASY.HD_DIALISE for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'C'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_cancelamento		<> :new.dt_cancelamento
		or :old.ds_observacao		<> :new.ds_observacao
		or :old.nr_seq_motivo_fim	<> :new.nr_seq_motivo_fim
		or :old.cd_pf_cancelamento	<> :new.cd_pf_cancelamento
		or (:old.dt_cancelamento 	is null		and :new.dt_cancelamento 	is not null)
		or (:old.ds_observacao 		is null		and :new.ds_observacao 		is not null)
		or (:old.nr_seq_motivo_fim 	is null		and :new.nr_seq_motivo_fim 	is not null)
		or (:old.cd_pf_cancelamento 	is null		and :new.cd_pf_cancelamento 	is not null)
		or (:old.dt_cancelamento 	is not null	and :new.dt_cancelamento 	is null)
		or (:old.ds_observacao 		is not null	and :new.ds_observacao 		is null)
		or (:old.nr_seq_motivo_fim 	is not null	and :new.nr_seq_motivo_fim 	is null)
		or (:old.cd_pf_cancelamento 	is not null	and :new.cd_pf_cancelamento 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'CP'
and	dt_liberacao is not null
and	dt_inativacao is null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.qt_pa_sist_pre_deitado		<> :new.qt_pa_sist_pre_deitado
		or :old.qt_pa_diast_pre_deitado         <> :new.qt_pa_diast_pre_deitado
		or :old.qt_pa_sist_pre_pe               <> :new.qt_pa_sist_pre_pe
		or :old.qt_pa_diast_pre_pe              <> :new.qt_pa_diast_pre_pe
		or :old.qt_peso_pre                     <> :new.qt_peso_pre
		or :old.qt_peso_ideal                   <> :new.qt_peso_ideal
		or :old.dt_instalacao                   <> :new.dt_instalacao
		or :old.cd_pf_instalacao                <> :new.cd_pf_instalacao
		or :old.nr_seq_motivo_peso_pre          <> :new.nr_seq_motivo_peso_pre
		or :old.nr_seq_motivo_pa_deitado_pre    <> :new.nr_seq_motivo_pa_deitado_pre
		or :old.nr_seq_motivo_pa_pe_pre         <> :new.nr_seq_motivo_pa_pe_pre
		or :old.qt_soro_reposicao               <> :new.qt_soro_reposicao
		or :old.qt_soro_devolucao               <> :new.qt_soro_devolucao
		or :old.nr_seq_tipo_soro                <> :new.nr_seq_tipo_soro
		or :old.qt_gpid                         <> :new.qt_gpid
		or :old.qt_valor_gpid                   <> :new.qt_valor_gpid
		or (:old.qt_pa_sist_pre_deitado 	is null		and :new.qt_pa_sist_pre_deitado 	is not null)
		or (:old.qt_pa_diast_pre_deitado 	is null		and :new.qt_pa_diast_pre_deitado 	is not null)
		or (:old.qt_pa_sist_pre_pe 		is null		and :new.qt_pa_sist_pre_pe 		is not null)
		or (:old.qt_pa_diast_pre_pe 		is null		and :new.qt_pa_diast_pre_pe 		is not null)
		or (:old.qt_peso_pre 			is null		and :new.qt_peso_pre 			is not null)
		or (:old.qt_peso_ideal 			is null		and :new.qt_peso_ideal 			is not null)
		or (:old.dt_instalacao 			is null		and :new.dt_instalacao 			is not null)
		or (:old.cd_pf_instalacao 		is null		and :new.cd_pf_instalacao 		is not null)
		or (:old.nr_seq_motivo_peso_pre 	is null		and :new.nr_seq_motivo_peso_pre 	is not null)
		or (:old.nr_seq_motivo_pa_deitado_pre 	is null		and :new.nr_seq_motivo_pa_deitado_pre 	is not null)
		or (:old.nr_seq_motivo_pa_pe_pre 	is null		and :new.nr_seq_motivo_pa_pe_pre 	is not null)
		or (:old.qt_soro_reposicao 		is null		and :new.qt_soro_reposicao 		is not null)
		or (:old.qt_soro_devolucao 		is null		and :new.qt_soro_devolucao 		is not null)
		or (:old.nr_seq_tipo_soro 		is null		and :new.nr_seq_tipo_soro 		is not null)
		or (:old.qt_gpid 			is null		and :new.qt_gpid 			is not null)
		or (:old.qt_valor_gpid 			is null		and :new.qt_valor_gpid 			is not null)
		or (:old.qt_pa_sist_pre_deitado 	is not null	and :new.qt_pa_sist_pre_deitado 	is null)
		or (:old.qt_pa_diast_pre_deitado 	is not null	and :new.qt_pa_diast_pre_deitado 	is null)
		or (:old.qt_pa_sist_pre_pe 		is not null	and :new.qt_pa_sist_pre_pe 		is null)
		or (:old.qt_pa_diast_pre_pe 		is not null	and :new.qt_pa_diast_pre_pe 		is null)
		or (:old.qt_peso_pre 			is not null	and :new.qt_peso_pre 			is null)
		or (:old.qt_peso_ideal 			is not null	and :new.qt_peso_ideal 			is null)
		or (:old.dt_instalacao 			is not null	and :new.dt_instalacao 			is null)
		or (:old.cd_pf_instalacao 		is not null	and :new.cd_pf_instalacao 		is null)
		or (:old.nr_seq_motivo_peso_pre 	is not null	and :new.nr_seq_motivo_peso_pre 	is null)
		or (:old.nr_seq_motivo_pa_deitado_pre 	is not null	and :new.nr_seq_motivo_pa_deitado_pre 	is null)
		or (:old.nr_seq_motivo_pa_pe_pre 	is not null	and :new.nr_seq_motivo_pa_pe_pre 	is null)
		or (:old.qt_soro_reposicao 		is not null	and :new.qt_soro_reposicao 		is null)
		or (:old.qt_soro_devolucao 		is not null	and :new.qt_soro_devolucao 		is null)
		or (:old.nr_seq_tipo_soro 		is not null	and :new.nr_seq_tipo_soro 		is null)
		or (:old.qt_gpid 			is not null	and :new.qt_gpid 			is null)
		or (:old.qt_valor_gpid 			is not null	and :new.qt_valor_gpid 			is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'ID'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_inicio_dialise		<> :new.dt_inicio_dialise
		or :old.cd_pf_inicio_dialise    <> :new.cd_pf_inicio_dialise
		or (:old.dt_inicio_dialise 	is null		and :new.dt_inicio_dialise 	is not null)
		or (:old.cd_pf_inicio_dialise 	is null		and :new.cd_pf_inicio_dialise 	is not null)
		or (:old.dt_inicio_dialise 	is not null	and :new.dt_inicio_dialise 	is null)
		or (:old.cd_pf_inicio_dialise 	is not null	and :new.cd_pf_inicio_dialise 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'FD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.qt_pa_sist_pos_pe			<> :new.qt_pa_sist_pos_pe
		or :old.qt_pa_diast_pos_pe              <> :new.qt_pa_diast_pos_pe
		or :old.qt_pa_sist_pos_deitado          <> :new.qt_pa_sist_pos_deitado
		or :old.qt_pa_diast_pos_deitado         <> :new.qt_pa_diast_pos_deitado
		or :old.qt_peso_pos                     <> :new.qt_peso_pos
		or :old.dt_fim_dialise                  <> :new.dt_fim_dialise
		or :old.cd_pf_fim_dialise               <> :new.cd_pf_fim_dialise
		or :old.nr_seq_motivo_peso_pos          <> :new.nr_seq_motivo_peso_pos
		or :old.nr_seq_motivo_pa_deitado_pos    <> :new.nr_seq_motivo_pa_deitado_pos
		or :old.nr_seq_motivo_pa_pe_pos         <> :new.nr_seq_motivo_pa_pe_pos
		or :old.qt_freq_cardiaca                <> :new.qt_freq_cardiaca
		or :old.qt_temp_axiliar_pos             <> :new.qt_temp_axiliar_pos
		or :old.qt_ultrafiltracao               <> :new.qt_ultrafiltracao
		or :old.qt_tempo_dialise                <> :new.qt_tempo_dialise
		or (:old.qt_pa_sist_pos_pe		is null		and :new.qt_pa_sist_pos_pe		is not null)
		or (:old.qt_pa_diast_pos_pe		is null		and :new.qt_pa_diast_pos_pe		is not null)
		or (:old.qt_pa_sist_pos_deitado		is null		and :new.qt_pa_sist_pos_deitado		is not null)
		or (:old.qt_pa_diast_pos_deitado	is null		and :new.qt_pa_diast_pos_deitado	is not null)
		or (:old.qt_peso_pos			is null		and :new.qt_peso_pos			is not null)
		or (:old.dt_fim_dialise			is null		and :new.dt_fim_dialise			is not null)
		or (:old.cd_pf_fim_dialise		is null		and :new.cd_pf_fim_dialise		is not null)
		or (:old.nr_seq_motivo_peso_pos		is null		and :new.nr_seq_motivo_peso_pos		is not null)
		or (:old.nr_seq_motivo_pa_deitado_pos	is null		and :new.nr_seq_motivo_pa_deitado_pos	is not null)
		or (:old.nr_seq_motivo_pa_pe_pos	is null		and :new.nr_seq_motivo_pa_pe_pos	is not null)
		or (:old.qt_freq_cardiaca		is null		and :new.qt_freq_cardiaca		is not null)
		or (:old.qt_temp_axiliar_pos		is null		and :new.qt_temp_axiliar_pos		is not null)
		or (:old.qt_ultrafiltracao		is null		and :new.qt_ultrafiltracao		is not null)
		or (:old.qt_tempo_dialise		is null		and :new.qt_tempo_dialise		is not null)
		or (:old.qt_pa_sist_pos_pe		is not null	and :new.qt_pa_sist_pos_pe		is null)
		or (:old.qt_pa_diast_pos_pe		is not null	and :new.qt_pa_diast_pos_pe		is null)
		or (:old.qt_pa_sist_pos_deitado		is not null	and :new.qt_pa_sist_pos_deitado		is null)
		or (:old.qt_pa_diast_pos_deitado	is not null	and :new.qt_pa_diast_pos_deitado	is null)
		or (:old.qt_peso_pos			is not null	and :new.qt_peso_pos			is null)
		or (:old.dt_fim_dialise			is not null	and :new.dt_fim_dialise			is null)
		or (:old.cd_pf_fim_dialise		is not null	and :new.cd_pf_fim_dialise		is null)
		or (:old.nr_seq_motivo_peso_pos		is not null	and :new.nr_seq_motivo_peso_pos		is null)
		or (:old.nr_seq_motivo_pa_deitado_pos	is not null	and :new.nr_seq_motivo_pa_deitado_pos	is null)
		or (:old.nr_seq_motivo_pa_pe_pos	is not null	and :new.nr_seq_motivo_pa_pe_pos	is null)
		or (:old.qt_freq_cardiaca		is not null	and :new.qt_freq_cardiaca		is null)
		or (:old.qt_temp_axiliar_pos		is not null	and :new.qt_temp_axiliar_pos		is null)
		or (:old.qt_ultrafiltracao		is not null	and :new.qt_ultrafiltracao		is null)
		or (:old.qt_tempo_dialise		is not null	and :new.qt_tempo_dialise		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'GH'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
		or :old.cd_pessoa_fisica	<> :new.cd_pessoa_fisica
		or :old.dt_dialise		<> :new.dt_dialise
		or :old.nr_seq_unid_dialise	<> :new.nr_seq_unid_dialise
		or :old.nr_atendimento		<> :new.nr_atendimento
		or :old.nr_dialise_atend_pac	<> :new.nr_dialise_atend_pac
		or :old.ie_tipo_dialise		<> :new.ie_tipo_dialise
		or :old.ie_paciente_agudo	<> :new.ie_paciente_agudo
		or (:old.nr_sequencia		is null		and :new.nr_sequencia		is not null)
		or (:old.dt_atualizacao_nrec	is null		and :new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec	is null		and :new.nm_usuario_nrec	is not null)
		or (:old.cd_pessoa_fisica	is null		and :new.cd_pessoa_fisica	is not null)
		or (:old.dt_dialise		is null		and :new.dt_dialise		is not null)
		or (:old.nr_seq_unid_dialise	is null		and :new.nr_seq_unid_dialise	is not null)
		or (:old.nr_atendimento		is null		and :new.nr_atendimento		is not null)
		or (:old.nr_dialise_atend_pac	is null		and :new.nr_dialise_atend_pac	is not null)
		or (:old.ie_tipo_dialise	is null		and :new.ie_tipo_dialise	is not null)
		or (:old.ie_paciente_agudo	is null		and :new.ie_paciente_agudo	is not null)
		or (:old.nr_sequencia		is not null	and :new.nr_sequencia		is null)
		or (:old.dt_atualizacao_nrec	is not null	and :new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec	is not null	and :new.nm_usuario_nrec	is null)
		or (:old.cd_pessoa_fisica	is not null	and :new.cd_pessoa_fisica	is null)
		or (:old.dt_dialise		is not null	and :new.dt_dialise		is null)
		or (:old.nr_seq_unid_dialise	is not null	and :new.nr_seq_unid_dialise	is null)
		or (:old.nr_atendimento		is not null	and :new.nr_atendimento		is null)
		or (:old.nr_dialise_atend_pac	is not null	and :new.nr_dialise_atend_pac	is null)
		or (:old.ie_tipo_dialise	is not null	and :new.ie_tipo_dialise	is null)
		or (:old.ie_paciente_agudo	is not null	and :new.ie_paciente_agudo	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_sequencia
and 	ie_opcao = 'GM'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
		or :old.cd_pessoa_fisica	<> :new.cd_pessoa_fisica
		or :old.dt_dialise		<> :new.dt_dialise
		or :old.nr_seq_unid_dialise	<> :new.nr_seq_unid_dialise
		or :old.nr_atendimento		<> :new.nr_atendimento
		or :old.nr_dialise_atend_pac	<> :new.nr_dialise_atend_pac
		or :old.ie_tipo_dialise		<> :new.ie_tipo_dialise
		or :old.ie_paciente_agudo	<> :new.ie_paciente_agudo
		or (:old.nr_sequencia		is null		and :new.nr_sequencia		is not null)
		or (:old.dt_atualizacao_nrec	is null		and :new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec	is null		and :new.nm_usuario_nrec	is not null)
		or (:old.cd_pessoa_fisica	is null		and :new.cd_pessoa_fisica	is not null)
		or (:old.dt_dialise		is null		and :new.dt_dialise		is not null)
		or (:old.nr_seq_unid_dialise	is null		and :new.nr_seq_unid_dialise	is not null)
		or (:old.nr_atendimento		is null		and :new.nr_atendimento		is not null)
		or (:old.nr_dialise_atend_pac	is null		and :new.nr_dialise_atend_pac	is not null)
		or (:old.ie_tipo_dialise	is null		and :new.ie_tipo_dialise	is not null)
		or (:old.ie_paciente_agudo	is null		and :new.ie_paciente_agudo	is not null)
		or (:old.nr_sequencia		is not null	and :new.nr_sequencia		is null)
		or (:old.dt_atualizacao_nrec	is not null	and :new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec	is not null	and :new.nm_usuario_nrec	is null)
		or (:old.cd_pessoa_fisica	is not null	and :new.cd_pessoa_fisica	is null)
		or (:old.dt_dialise		is not null	and :new.dt_dialise		is null)
		or (:old.nr_seq_unid_dialise	is not null	and :new.nr_seq_unid_dialise	is null)
		or (:old.nr_atendimento		is not null	and :new.nr_atendimento		is null)
		or (:old.nr_dialise_atend_pac	is not null	and :new.nr_dialise_atend_pac	is null)
		or (:old.ie_tipo_dialise	is not null	and :new.ie_tipo_dialise	is null)
		or (:old.ie_paciente_agudo	is not null	and :new.ie_paciente_agudo	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_DIALISE ADD (
  CONSTRAINT HDDIALE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISE ADD (
  CONSTRAINT HDDIALE_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT HDDIALE_HDPAREC_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.HD_PAC_RENAL_CRONICO (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALE_PESFISI_FK 
 FOREIGN KEY (CD_PF_FIM_DIALISE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALE_PESFISI_FK2 
 FOREIGN KEY (CD_PF_INICIO_DIALISE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALE_PESFISI_FK3 
 FOREIGN KEY (CD_PF_INSTALACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALE_PESFISI_FK4 
 FOREIGN KEY (CD_PF_CANCELAMENTO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALE_HDUNIDD_FK1 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT HDDIALE_MOTIFIM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FIM) 
 REFERENCES TASY.MOTIVO_FIM (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_HDMOTDAPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PESO_PRE) 
 REFERENCES TASY.HD_MOTIVO_DADOS_PAC (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_HDMOTDAPA_FK2 
 FOREIGN KEY (NR_SEQ_MOTIVO_PA_DEITADO_PRE) 
 REFERENCES TASY.HD_MOTIVO_DADOS_PAC (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_HDMOTDAPA_FK3 
 FOREIGN KEY (NR_SEQ_MOTIVO_PA_PE_PRE) 
 REFERENCES TASY.HD_MOTIVO_DADOS_PAC (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_HDMOTDAPA_FK4 
 FOREIGN KEY (NR_SEQ_MOTIVO_PA_DEITADO_POS) 
 REFERENCES TASY.HD_MOTIVO_DADOS_PAC (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_HDMOTDAPA_FK5 
 FOREIGN KEY (NR_SEQ_MOTIVO_PA_PE_POS) 
 REFERENCES TASY.HD_MOTIVO_DADOS_PAC (NR_SEQUENCIA),
  CONSTRAINT HDDIALE_HDMOTDAPA_FK6 
 FOREIGN KEY (NR_SEQ_MOTIVO_PESO_POS) 
 REFERENCES TASY.HD_MOTIVO_DADOS_PAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISE TO NIVEL_1;

