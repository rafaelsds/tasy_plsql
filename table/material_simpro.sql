ALTER TABLE TASY.MATERIAL_SIMPRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_SIMPRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_SIMPRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  CD_SIMPRO            NUMBER(15)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  QT_CONVERSAO         NUMBER(15,4)             NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4),
  CD_CONVENIO          NUMBER(5),
  DT_VIGENCIA          DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_FINAL_VIGENCIA    DATE,
  IE_TIPO_CONVENIO     NUMBER(2),
  NR_SEQ_MARCA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATSIMP_CONVENI_FK_I ON TASY.MATERIAL_SIMPRO
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATSIMP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATSIMP_ESTABEL_FK_I ON TASY.MATERIAL_SIMPRO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATSIMP_MARCA_FK_I ON TASY.MATERIAL_SIMPRO
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATSIMP_MATERIA_FK_I ON TASY.MATERIAL_SIMPRO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATSIMP_PK ON TASY.MATERIAL_SIMPRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATSIMP_SIMMATE_FK_I ON TASY.MATERIAL_SIMPRO
(CD_SIMPRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATSIMP_SIMMATE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATSIMP_UK ON TASY.MATERIAL_SIMPRO
(CD_MATERIAL, CD_SIMPRO, CD_ESTABELECIMENTO, CD_CONVENIO, DT_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_simpro_delete
before delete ON TASY.MATERIAL_SIMPRO for each row
declare

qt_existe_regra_w		varchar2(1);
ds_erro_w			varchar2(255) := '0';

begin

select	nvl(max('S'),'N')
into	qt_existe_regra_w
from	conv_regra_vinc_mat;

if	(qt_existe_regra_w = 'S') then

	obter_parametro_vinculo_mat(:old.cd_material, wheb_usuario_pck.get_nm_usuario,	ds_erro_w, 'S', 3);

	if	(ds_erro_w <> '0') then
		--R.aise_application_error(-20011,ds_erro_w);
		wheb_mensagem_pck.exibir_mensagem_abort(263411,'ds_erro_w='||ds_erro_w);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_simpro_afterinsert
after insert ON TASY.MATERIAL_SIMPRO for each row
declare

ds_titulo_w             varchar2(80);
ds_historico_w          varchar2(4000);

begin

ds_titulo_w     := wheb_mensagem_pck.get_texto(802954);

ds_historico_w  := wheb_mensagem_pck.get_texto(802958,'ds_simpro=' || substr(obter_desc_simpro(:new.cd_simpro),1,255) || ';' || 'qt_conversao=' || :new.qt_conversao);

insert into material_historico(
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        ds_historico,
        ds_historico_curto,
        cd_material,
        ds_titulo,
        cd_estabelecimento,
        ie_tipo)
select  material_historico_seq.nextval,
        sysdate,
        :new.nm_usuario,
        sysdate,
        :new.nm_usuario,
        ds_historico_w,
        substr(ds_historico_w,1,4000),
        :new.cd_material,
        ds_titulo_w,
        :new.cd_estabelecimento,
        'S'
from    dual;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_simpro_insert
before insert ON TASY.MATERIAL_SIMPRO for each row
declare

qt_existe_regra_w		varchar2(1);
ds_erro_w			varchar2(255) := '0';

begin

select	nvl(max('S'),'N')
into	qt_existe_regra_w
from	conv_regra_vinc_mat;

if	(qt_existe_regra_w = 'S') then

	obter_parametro_vinculo_mat(:new.cd_material, :new.nm_usuario,	ds_erro_w, 'S', 1);

	if	(ds_erro_w <> '0') then
		--R.aise_application_error(-20011,ds_erro_w);
		wheb_mensagem_pck.exibir_mensagem_abort(263411,'ds_erro_w='||ds_erro_w);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_simpro_update
before update ON TASY.MATERIAL_SIMPRO for each row
declare

qt_existe_regra_w		varchar2(1);
ds_erro_w			varchar2(255) := '0';

begin

select	nvl(max('S'),'N')
into	qt_existe_regra_w
from	conv_regra_vinc_mat;

if	(qt_existe_regra_w = 'S') then

	obter_parametro_vinculo_mat(:new.cd_material, :new.nm_usuario,	ds_erro_w, 'S', 2);

	if	(ds_erro_w <> '0') then
		--R.aise_application_error(-20011,ds_erro_w);
		wheb_mensagem_pck.exibir_mensagem_abort(263411,'ds_erro_w='||ds_erro_w);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MATERIAL_SIMPRO_tp  after update ON TASY.MATERIAL_SIMPRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SIMPRO,1,4000),substr(:new.CD_SIMPRO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SIMPRO',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONVERSAO,1,4000),substr(:new.QT_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONVENIO,1,4000),substr(:new.IE_TIPO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONVENIO',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA,1,4000),substr(:new.DT_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FINAL_VIGENCIA,1,4000),substr(:new.DT_FINAL_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'MATERIAL_SIMPRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.material_simpro_after
after insert or update or delete ON TASY.MATERIAL_SIMPRO for each row
declare

ds_retorno_integracao_w clob;
event_w					varchar2(25);
event_class_w			varchar2(90);
nr_sequencia_w 			material_simpro.nr_sequencia%type;
qt_retorno_w        varchar2(1);

begin

select	obter_valor_param_usuario(9041,
                                  10,
                                  obter_perfil_ativo,
                                  wheb_usuario_pck.get_nm_usuario(),
                                  obter_estabelecimento_ativo())
into	qt_retorno_w
from	dual;

if (qt_retorno_w = 'S')then
  /*Quando Incluir, alterar e excluir as informa��es da material_tuss chama a integra��o padr�o*/
  if	(inserting) then
    nr_sequencia_w			:= :new.nr_sequencia;
    event_w					:= 'materialsimpro.added'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialsimpro.outbound.MaterialSimproAddedCallback'; -- event class created in tasy-interfaces(material module).
  elsif	(updating) then
    nr_sequencia_w			:= :new.nr_sequencia;
    event_w					:= 'materialsimpro.updated'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialsimpro.outbound.MaterialSimproUpdatedCallback'; -- event class created in tasy-interfaces(material module).
  else
    nr_sequencia_w			:= :old.nr_sequencia;
    event_w					:= 'materialsimpro.deleted'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialsimpro.outbound.MaterialSimproDeletedCallback'; -- event class created in tasy-interfaces(material module).
  end if;

  SELECT BIFROST.SEND_INTEGRATION(
      event_w,
      event_class_w,
      '{"code" : '||nr_sequencia_w||'}',
      'integration')
  INTO ds_retorno_integracao_w
  FROM dual;
end if;

end;
/


ALTER TABLE TASY.MATERIAL_SIMPRO ADD (
  CONSTRAINT MATSIMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT MATSIMP_UK
 UNIQUE (CD_MATERIAL, CD_SIMPRO, CD_ESTABELECIMENTO, CD_CONVENIO, DT_VIGENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_SIMPRO ADD (
  CONSTRAINT MATSIMP_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA),
  CONSTRAINT MATSIMP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATSIMP_SIMMATE_FK 
 FOREIGN KEY (CD_SIMPRO) 
 REFERENCES TASY.SIMPRO_CADASTRO (CD_SIMPRO)
    ON DELETE CASCADE,
  CONSTRAINT MATSIMP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATSIMP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.MATERIAL_SIMPRO TO NIVEL_1;

