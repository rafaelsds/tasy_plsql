ALTER TABLE TASY.HISTORICO_SAUDE_MULHER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HISTORICO_SAUDE_MULHER CASCADE CONSTRAINTS;

CREATE TABLE TASY.HISTORICO_SAUDE_MULHER
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  QT_GESTACOES               NUMBER(2),
  QT_PARTO_NORMAL            NUMBER(2),
  QT_FILHOS_VIVOS            NUMBER(2),
  QT_FILHOS_MORTOS           NUMBER(2),
  QT_PARTO_CESARIO           NUMBER(2),
  QT_ABORTOS                 NUMBER(2),
  DT_ATUALIZACAO             DATE               NOT NULL,
  QT_PARTO_FORCEPS           NUMBER(2),
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_ULT_MENSTRUACAO         DATE,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_NASC_MORTOS             NUMBER(2)          NOT NULL,
  IE_ALT_MENSTRUACAO         VARCHAR2(1 BYTE),
  DT_REGISTRO                DATE               NOT NULL,
  IE_CIRURGIA_MAMA           VARCHAR2(1 BYTE),
  DS_CIRURGIA_MAMA           VARCHAR2(255 BYTE),
  DS_ALT_MENSTRUACAO         VARCHAR2(255 BYTE),
  IE_PAPANICOLAOU            VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DT_ULT_PAPANICOLAOU        DATE,
  DS_DOENCAS_GINECOLOGICAS   VARCHAR2(255 BYTE),
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  DT_REVISAO                 DATE,
  QT_IG_SEM_US               NUMBER(2),
  QT_IG_DIA_US               NUMBER(2),
  IE_PAC_GRAVIDA             VARCHAR2(1 BYTE)   NOT NULL,
  IE_IDADE_FERTIL            VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  QT_SEM_IG_CRONOLOGICA      NUMBER(5,2),
  QT_DIA_IG_CRONOLOGICA      NUMBER(5,2),
  DT_ULT_PARTO               DATE,
  QT_ABORTOS_PROV            NUMBER(2),
  DT_PRIM_PARTO              DATE,
  IE_FLUXO                   VARCHAR2(1 BYTE),
  QT_IDADE_PRIM_GRAV         NUMBER(2),
  IE_RITMO_MENSTRUAL         VARCHAR2(1 BYTE),
  QT_DIAS_CICLO              NUMBER(2),
  QT_DIAS_DURACAO            NUMBER(2),
  QT_PARCEIROS               NUMBER(2),
  QT_ABORTOS_ESP             NUMBER(2),
  QT_MENARCA                 NUMBER(2),
  QT_SEXARCA                 NUMBER(2),
  QT_IDADE_ULT_PARTO         NUMBER(2),
  DS_METODOS_CONTRAC         VARCHAR2(255 BYTE),
  DT_INICIO_AMAMENTACAO      DATE,
  DT_FIM_AMAMENTACAO         DATE,
  DT_INICIO_GESTACAO         DATE,
  DT_FIM_GESTACAO            DATE,
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_ESCUTA              NUMBER(10),
  DT_CIRURGIA_MAMA           DATE,
  QT_ABORTOS_TERAP           NUMBER(2),
  IE_PARENTESCO_AGRESSOR     NUMBER(2),
  DT_PROC_CONTRAC            DATE,
  DS_COMPL_CONTRAC           VARCHAR2(255 BYTE),
  NR_CONTRAC_REALIZADO       NUMBER(3),
  IE_ORIENT_MET_CONTRAC      VARCHAR2(1 BYTE),
  DS_INF_MENOPAUSA           VARCHAR2(2000 BYTE),
  DS_REPOSICAO_HORMONAL      VARCHAR2(255 BYTE),
  IE_REPOSICAO_HORMONAL      VARCHAR2(1 BYTE),
  NR_IDADE_MENOPAUSA         NUMBER(10),
  IE_MAMOGRAFIA              VARCHAR2(1 BYTE),
  DT_MAMOGRAFIA              DATE,
  IE_PELVICO                 VARCHAR2(1 BYTE),
  DT_PELVICO                 DATE,
  IE_REP_HORMONAL            VARCHAR2(1 BYTE),
  DS_REP_HORMONAL            VARCHAR2(255 BYTE),
  DT_ULT_MAMOGRAFIA          DATE,
  DT_ULT_PELVICO             DATE,
  IE_EXAM_MAMOGRAFIA         VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_GRAVIDEZ_PLAN           VARCHAR2(1 BYTE)   DEFAULT null,
  IE_PREGNANCY               VARCHAR2(1 BYTE),
  DT_START_LAST_MENSTRUAL    DATE,
  DT_END_LAST_MENSTRUAL      DATE,
  DT_MISCARRIAGE             DATE,
  IE_POSSIBILITY_PREGNANCY   VARCHAR2(1 BYTE),
  IE_LACTATION               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HISAMUL_ATCONSPEPA_FK_I ON TASY.HISTORICO_SAUDE_MULHER
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISAMUL_ATESINI_FK_I ON TASY.HISTORICO_SAUDE_MULHER
(NR_SEQ_ESCUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISAMUL_EHRREEL_FK_I ON TASY.HISTORICO_SAUDE_MULHER
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAMUL_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISAMUL_PESFISI_FK_I ON TASY.HISTORICO_SAUDE_MULHER
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISAMUL_PESFISI_FK2_I ON TASY.HISTORICO_SAUDE_MULHER
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HISAMUL_PK ON TASY.HISTORICO_SAUDE_MULHER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAMUL_PK
  MONITORING USAGE;


CREATE INDEX TASY.HISAMUL_TASASDI_FK_I ON TASY.HISTORICO_SAUDE_MULHER
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAMUL_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISAMUL_TASASDI_FK2_I ON TASY.HISTORICO_SAUDE_MULHER
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAMUL_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.HISTORICO_SAUDE_MULHER_ATUAL
before insert or update ON TASY.HISTORICO_SAUDE_MULHER for each row
declare


begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hist_saude_mulher_pend_atual 
after insert or update ON TASY.HISTORICO_SAUDE_MULHER 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSM'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSM'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.historico_saude_mulher_aftup
after update ON TASY.HISTORICO_SAUDE_MULHER for each row
declare
ds_guampa_w		varchar2(10)	:= chr(39);

begin

  if (((:new.dt_liberacao is not null) and (:old.dt_liberacao is null)) and
      ((:new.ie_pac_gravida = 'S') or (:new.qt_sem_ig_cronologica is not null))) then

		call_interface_file(927, 'itech_pck.send_patient_attribute_info('||ds_guampa_w|| :new.cd_pessoa_fisica ||ds_guampa_w||','||ds_guampa_w|| :new.nm_usuario ||ds_guampa_w||','||0||','||ds_guampa_w||null||ds_guampa_w||','||wheb_usuario_pck.get_cd_estabelecimento||');', :new.nm_usuario);

    call_interface_file(933, 'carestream_ris_japan_l10n_pck.patient_change_info( ' || :new.cd_pessoa_fisica || ', ''' || :new.nm_usuario || ''' , 0 , null, ' || wheb_usuario_pck.get_cd_estabelecimento || ');' , :new.nm_usuario);

  end if;

end;
/


ALTER TABLE TASY.HISTORICO_SAUDE_MULHER ADD (
  CONSTRAINT HISAMUL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HISTORICO_SAUDE_MULHER ADD (
  CONSTRAINT HISAMUL_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HISAMUL_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HISAMUL_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HISAMUL_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HISAMUL_ATESINI_FK 
 FOREIGN KEY (NR_SEQ_ESCUTA) 
 REFERENCES TASY.ATEND_ESCUTA_INICIAL (NR_SEQUENCIA),
  CONSTRAINT HISAMUL_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT HISAMUL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.HISTORICO_SAUDE_MULHER TO NIVEL_1;

