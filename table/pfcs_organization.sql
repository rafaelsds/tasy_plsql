ALTER TABLE TASY.PFCS_ORGANIZATION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_ORGANIZATION CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_ORGANIZATION
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  ID_ORGANIZATION          VARCHAR2(255 BYTE),
  SI_STATUS                VARCHAR2(30 BYTE),
  DS_ORGANIZATION          VARCHAR2(255 BYTE),
  CD_TYPE                  VARCHAR2(30 BYTE),
  NR_SEQ_ORGANIZATION_SUP  NUMBER(10),
  NAME                     VARCHAR2(255 BYTE),
  NR_SEQ_ADDRESS           NUMBER(10,1),
  CD_ESTABELECIMENTO       NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCSORGA_ESTABEL_FK_I ON TASY.PFCS_ORGANIZATION
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSORGA_PFCSADDR_FK_I ON TASY.PFCS_ORGANIZATION
(NR_SEQ_ADDRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSORGA_PFCSORGA_FK_I ON TASY.PFCS_ORGANIZATION
(NR_SEQ_ORGANIZATION_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCSORGA_PK ON TASY.PFCS_ORGANIZATION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PFCS_UPDATE_ESTABELECIMENTO
BEFORE INSERT OR UPDATE ON TASY.PFCS_ORGANIZATION FOR EACH ROW
DECLARE
-- Constants
nm_usuario_w            pessoa_juridica.nm_usuario%type     := 'PFCS';

-- Dynamic variables
tasy_licenca_seq_w      tasy_licenca.nr_sequencia%type;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;
cd_empresa_w            empresa.cd_empresa%type;
cd_cgc_base_w           empresa.cd_base_cgc%type;
cd_cgc_w                pessoa_juridica.cd_cgc%type;
cd_tipo_pessoa_w        pessoa_juridica.cd_tipo_pessoa%type;
ds_log_w                pfcs_org_structure_log.ds_log%type;
ie_situacao_w           VARCHAR2(1) := 'A';
old_ie_situacao_w       VARCHAR2(1) := null;
qt_reg_w				number(1);

/*
	This trigger is responsible for inserting a new establishment on table "ESTABELECIMENTO" or updating an existing one.
    It will also insert or update, when needed, data on table "EMPRESA", since "ESTABELECIMENTO" is linked to it.
	The necessary information will come from the integration table PFCS_ORGANIZATION.
*/

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    wheb_usuario_pck.set_nm_usuario(nm_usuario_w);
    /* Needed configuration
        select vl_parametro
        from funcao_param_usuario
        where upper(nm_usuario_param) = 'PFCS'
            and cd_funcao = 6
            and nr_sequencia = 9;
    */

    if (:new.si_status is not null) then
        ie_situacao_w :=
        CASE
            WHEN :new.si_status = '1' THEN 'A'
            WHEN :new.si_status = '0' THEN 'I'
        END;
    end if;

    cd_cgc_base_w := to_char(:new.nr_sequencia);
    cd_cgc_w := to_char(:new.nr_sequencia);

    IF INSERTING THEN
        -- Check if it is a company (there is no superior organization)
        if (:new.nr_seq_organization_sup is null) then
            /************* EMPRESA *************/
            select (nvl(max(cd_empresa),0)+1)
            into cd_empresa_w
            from empresa;

            insert into
            empresa (
                CD_BASE_CGC,
                CD_EMPRESA,
                DS_NOME_CURTO,
                IE_CLASSIF_CONTA,
                IE_CONSISTE_MASCARA_CONTABIL,
                IE_CONTRAPARTIDA,
                IE_FECHA_ESTAB,
                IE_SISTEMA_CTB,
                IE_SITUACAO,
                NM_RAZAO_SOCIAL,
                QT_MES_FIM_EXERCICIO,
                QT_NIVEL_FLUXO,
                NM_USUARIO,
                NM_USUARIO_NREC,
                DT_ATUALIZACAO,
                DT_ATUALIZACAO_NREC
            ) values (
                cd_cgc_base_w,
                cd_empresa_w,
                substr(:new.id_organization,1,50),
                'N',
                'N',
                'N',
                'N',
                'N',
                ie_situacao_w,
                :new.name,
                12,
                4,
                nm_usuario_w,
                nm_usuario_w,
                SYSDATE,
                SYSDATE
            );

            ds_log_w := (
                concat('cd_base_cgc: ',             cd_cgc_base_w || CHR(10))
                || concat('cd_empresa: ',           cd_empresa_w || CHR(10))
                || concat('nm_razao_social: ',      :new.name || CHR(10))
                || concat('ds_nome_curto: ',        substr(:new.id_organization,1,50) || CHR(10))
                || concat('ie_situacao: ',          ie_situacao_w)
            );
            pfcs_log_pck.pfcs_insert_org_struc_log(
                ie_type_p => 'INSERT',
                nm_table_p => 'EMPRESA',
                ds_log_p => ds_log_w,
                nm_usuario_p => nm_usuario_w
            );
        else
        -- else if it is an establishment (it is inside a company)
            select max(cd_empresa)
            into cd_empresa_w
            from empresa
            where cd_base_cgc = to_char(:new.nr_seq_organization_sup);

            -- Check if there is a company
            if (cd_empresa_w is not null) then
                /************* TIPO_PESSOA_JURIDICA *************/
                select nvl(max(cd_tipo_pessoa),0)
                into cd_tipo_pessoa_w
                from tipo_pessoa_juridica;

                if (cd_tipo_pessoa_w = 0) then
                    cd_tipo_pessoa_w := cd_tipo_pessoa_w + 1;

                    insert into
                    tipo_pessoa_juridica (
                        IE_ATUALIZA_PJ,
                        DT_ATUALIZACAO,
                        DT_ATUALIZACAO_NREC,
                        IE_SITUACAO,
                        DS_TIPO_PESSOA,
                        CD_TIPO_PESSOA,
                        NM_USUARIO,
                        NM_USUARIO_NREC
                    ) values (
                        'S',
                        SYSDATE,
                        SYSDATE,
                        'A',
                        'Hospital',
                        cd_tipo_pessoa_w,
                        nm_usuario_w,
                        nm_usuario_w
                    );
                end if;

                /************* PESSOA_JURIDICA *************/
                insert into
                pessoa_juridica (
                    CD_CGC,
                    DS_RAZAO_SOCIAL,
                    NM_FANTASIA,
                    CD_CEP,
                    DS_ENDERECO,
                    IE_SITUACAO,
                    SG_ESTADO, -- Domain
                    DT_ATUALIZACAO,
                    DT_ATUALIZACAO_NREC,
                    NM_USUARIO,
                    NM_USUARIO_NREC,
                    CD_TIPO_PESSOA, -- FK TIPO_PESSOA_JURIDICA
                    IE_PROD_FABRIC,
                    DS_MUNICIPIO
                ) values (
                    cd_cgc_w,
                    :new.name,
                    :new.name,
                    ' ',
                    ' ',
                    ie_situacao_w,
                    ' ', --> ToDo
                    SYSDATE,
                    SYSDATE,
                    nm_usuario_w,
                    nm_usuario_w,
                    cd_tipo_pessoa_w,
                    'N',
                    ' '
                );

                /************* TASY_LICENCA *************/
                select nvl(max(nr_sequencia),0)
                into tasy_licenca_seq_w
                from tasy_licenca;

                if (tasy_licenca_seq_w = 0) then
                    select
                    tasy_licenca_seq.nextval
                    into tasy_licenca_seq_w
                    from dual;

                    insert into
                    tasy_licenca (
                        NR_SEQUENCIA,
                        DS_LICENCA,
                        DT_ATUALIZACAO,
                        NM_USUARIO,
                        NM_USUARIO_BANCO,
                        NM_SENHA_BANCO,
                        NM_USUARIO_AVISO,
                        IE_ACAO_EXCESSO,
                        QT_USUARIO,
                        CD_LICENCA
                    ) values (
                        tasy_licenca_seq_w,
                        'PFCS',
                        SYSDATE,
                        nm_usuario_w,
                        ' ',
                        ' ',
                        nm_usuario_w,
                        'A',
                        99999,
                        '99999'
                    );
                end if;

                /************* ESTABELECIMENTO *************/
                select (nvl(max(cd_estabelecimento),0)+1)
                into cd_estabelecimento_w
                from estabelecimento;

                insert into
                estabelecimento (
                    CD_CGC, -- FK PESSOA_JURIDICA
                    CD_ESTABELECIMENTO,
                    CD_EMPRESA,
                    NM_FANTASIA_ESTAB,
                    DT_ATUALIZACAO,
                    DT_ATUALIZACAO_NREC,
                    DT_REF_CONTROLE_TERC,
                    IE_ANS,
                    IE_RAZAO_FANTASIA,
                    IE_SERV_LEITO,
                    IE_SITUACAO,
                    NM_USUARIO,
                    NM_USUARIO_NREC,
                    NR_SEQ_LICENCA, -- FK TASY_LICENCA
                    PR_ICMS_ESTADO
                ) values (
                    cd_cgc_w,
                    cd_estabelecimento_w,
                    cd_empresa_w,
                    :new.name,
                    SYSDATE,
                    SYSDATE,
                    SYSDATE,
                    'N',
                    'R',
                    'N',
                    ie_situacao_w,
                    nm_usuario_w,
                    nm_usuario_w,
                    tasy_licenca_seq_w,
                    17
                );

                ds_log_w := (
                    concat('cd_cgc: ',                  cd_cgc_w || CHR(10))
                    || concat('cd_estabelecimento: ',   cd_estabelecimento_w || CHR(10))
                    || concat('cd_empresa: ',           cd_empresa_w || CHR(10))
                    || concat('nm_fantasia_estab: ',    :new.name || CHR(10))
                    || concat('ie_situacao: ',          ie_situacao_w)
                );
                pfcs_log_pck.pfcs_insert_org_struc_log(
                    ie_type_p => 'INSERT',
                    nm_table_p => 'ESTABELECIMENTO',
                    ds_log_p => ds_log_w,
                    nm_usuario_p => nm_usuario_w
                );

                :new.cd_estabelecimento := cd_estabelecimento_w; --> Link Between ESTABELECIMENTO and PFCS_ORGANIZATION

            end if;
        end if;
    END IF;


    IF UPDATING THEN
        if ((:old.nr_seq_organization_sup is not null) and (:new.nr_seq_organization_sup is null)) then
            :new.nr_seq_organization_sup := :old.nr_seq_organization_sup;
        end if;

        old_ie_situacao_w :=
        CASE
            WHEN :old.si_status = '1' THEN 'A'
            WHEN :old.si_status = '0' THEN 'I'
            ELSE null
        END;

        if (:new.nr_seq_organization_sup is null) then
            update empresa set
                IE_SITUACAO = ie_situacao_w,
                DT_ATUALIZACAO = SYSDATE,
                NM_RAZAO_SOCIAL = :new.name
            where cd_base_cgc = cd_cgc_base_w;

            ds_log_w := (
                concat('old nm_razao_social: ',         :old.name)
                || concat(' | new nm_razao_social: ',   :new.name || CHR(10))
                || concat('old ie_situacao: ',          nvl(old_ie_situacao_w,'null'))
                || concat(' | new ie_situacao: ',       ie_situacao_w )
            );
            pfcs_log_pck.pfcs_insert_org_struc_log(
                ie_type_p => 'UPDATE',
                nm_table_p => 'EMPRESA',
                ds_log_p => ds_log_w,
                nm_usuario_p => nm_usuario_w
            );
        else
            update pessoa_juridica set
                DS_RAZAO_SOCIAL = :new.name,
                NM_FANTASIA = :new.name,
                IE_SITUACAO = ie_situacao_w,
                DT_ATUALIZACAO = SYSDATE
            where cd_cgc = cd_cgc_w;

            update estabelecimento set
                NM_FANTASIA_ESTAB = :new.name,
                IE_SITUACAO = ie_situacao_w,
                DT_ATUALIZACAO = SYSDATE
            where cd_cgc = cd_cgc_w;

             ds_log_w := (
                concat('old nm_fantasia_estab: ',       :old.name)
                || concat(' | new nm_fantasia_estab: ', :new.name || CHR(10))
                || concat('old ie_situacao: ',          nvl(old_ie_situacao_w,'null'))
                || concat(' | new ie_situacao: ',       ie_situacao_w)
            );
            pfcs_log_pck.pfcs_insert_org_struc_log(
                ie_type_p => 'UPDATE',
                nm_table_p => 'ESTABELECIMENTO',
                ds_log_p => ds_log_w,
                nm_usuario_p => nm_usuario_w
            );
        end if;
    END IF;
<<Final>>
qt_reg_w := 0;
END;
/


ALTER TABLE TASY.PFCS_ORGANIZATION ADD (
  CONSTRAINT PFCSORGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PFCS_ORGANIZATION ADD (
  CONSTRAINT PFCSORGA_PFCSADDR_FK 
 FOREIGN KEY (NR_SEQ_ADDRESS) 
 REFERENCES TASY.PFCS_ADDRESS (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSORGA_PFCSORGA_FK 
 FOREIGN KEY (NR_SEQ_ORGANIZATION_SUP) 
 REFERENCES TASY.PFCS_ORGANIZATION (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSORGA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PFCS_ORGANIZATION TO NIVEL_1;

