ALTER TABLE TASY.PESSOA_CLIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_CLIENTE
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  CD_TIPO_CONTROLE  NUMBER(5)                   NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  CD_PESSOA_FISICA  VARCHAR2(10 BYTE),
  CD_CGC            VARCHAR2(14 BYTE),
  CD_CLASSIF_PRINC  VARCHAR2(20 BYTE),
  CD_CLASSIF_SEC    VARCHAR2(20 BYTE),
  NR_SEQ_CLIENTE    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESSCLI_COMCLIE_FK_I ON TASY.PESSOA_CLIENTE
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESSCLI_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESSCLI_PESFISI_FK_I ON TASY.PESSOA_CLIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESSCLI_PESFISI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESSCLI_PESJURI_FK_I ON TASY.PESSOA_CLIENTE
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESSCLI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESSCLI_PK ON TASY.PESSOA_CLIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESSCLI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PESSCLI_TIPCOPE_FK_I ON TASY.PESSOA_CLIENTE
(CD_TIPO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESSCLI_TIPCOPE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PESSOA_CLIENTE ADD (
  CONSTRAINT PESSCLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_CLIENTE ADD (
  CONSTRAINT PESSCLI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT PESSCLI_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PESSCLI_TIPCOPE_FK 
 FOREIGN KEY (CD_TIPO_CONTROLE) 
 REFERENCES TASY.TIPO_CONTROLE_PESSOA (CD_TIPO_CONTROLE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_CLIENTE TO NIVEL_1;

