ALTER TABLE TASY.MEDIC_CONTROLADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDIC_CONTROLADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDIC_CONTROLADO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_MATERIAL               NUMBER(6),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_CLASSIFICACAO          VARCHAR2(20 BYTE),
  NR_SEQ_DCB                NUMBER(10),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_TIPO               NUMBER(10),
  CD_ESTABELECIMENTO        NUMBER(4),
  CD_MEDICAMENTO            VARCHAR2(17 BYTE),
  CD_CLASSIFICACAO_RECEITA  VARCHAR2(20 BYTE),
  IE_RECEITA                VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDCONT_DCBMECO_FK_I ON TASY.MEDIC_CONTROLADO
(NR_SEQ_DCB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONT_DCBMECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCONT_ESTABEL_FK_I ON TASY.MEDIC_CONTROLADO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCONT_MATERIA_FK_I ON TASY.MEDIC_CONTROLADO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDCONT_PK ON TASY.MEDIC_CONTROLADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDCONT_TIPMECO_FK_I ON TASY.MEDIC_CONTROLADO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCONT_TIPMECO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MEDIC_CONTROLADO_tp  after update ON TASY.MEDIC_CONTROLADO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIFICACAO,1,4000),substr(:new.CD_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIFICACAO',ie_log_w,ds_w,'MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DCB,1,4000),substr(:new.NR_SEQ_DCB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DCB',ie_log_w,ds_w,'MEDIC_CONTROLADO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MEDIC_CONTROLADO ADD (
  CONSTRAINT MEDCONT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDIC_CONTROLADO ADD (
  CONSTRAINT MEDCONT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MEDCONT_DCBMECO_FK 
 FOREIGN KEY (NR_SEQ_DCB) 
 REFERENCES TASY.DCB_MEDIC_CONTROLADO (NR_SEQUENCIA),
  CONSTRAINT MEDCONT_TIPMECO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_MEDIC_CONTROLADO (NR_SEQUENCIA),
  CONSTRAINT MEDCONT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MEDIC_CONTROLADO TO NIVEL_1;

