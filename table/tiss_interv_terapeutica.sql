ALTER TABLE TASY.TISS_INTERV_TERAPEUTICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_INTERV_TERAPEUTICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_INTERV_TERAPEUTICA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_ATENDIMENTO               NUMBER(10)       NOT NULL,
  DT_AVALIACAO                 DATE             NOT NULL,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE) NOT NULL,
  IE_AB_MONIT_PADRAO           VARCHAR2(1 BYTE) NOT NULL,
  IE_AB_LABORATORIO            VARCHAR2(1 BYTE) NOT NULL,
  IE_AB_MEDIC_UNICA            VARCHAR2(1 BYTE) NOT NULL,
  IE_AB_MEDIC_ENDOVENOSA       VARCHAR2(1 BYTE) NOT NULL,
  IE_AB_CURATIVO_ROTINA        VARCHAR2(1 BYTE) NOT NULL,
  IE_AB_CURATIVO_FREQUENTE     VARCHAR2(1 BYTE) NOT NULL,
  IE_AB_DRENO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_SV_VENT_MECANICA          VARCHAR2(1 BYTE) NOT NULL,
  IE_SV_SUP_VENT_SUPL          VARCHAR2(1 BYTE) NOT NULL,
  IE_SV_VIAS_AERIAS            VARCHAR2(1 BYTE) NOT NULL,
  IE_SV_TRAT_FUNCAO_PULMONAR   VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_MEDIC_VASOATIVA_UNICA  VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_MEDIC_VASOATIVA_MULT   VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_REP_VOLEMICA           VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_CATETER_ART_PERIF      VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_MONIT_ATRIO_ESQ        VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_VIA_VENOSA_CENTRAL     VARCHAR2(1 BYTE) NOT NULL,
  IE_SC_RESSUSCIT_CARDIO       VARCHAR2(1 BYTE) NOT NULL,
  IE_SR_TEC_HEMOFILTRACAO      VARCHAR2(1 BYTE) NOT NULL,
  IE_SR_MED_DEBITO_URINA       VARCHAR2(1 BYTE) NOT NULL,
  IE_SR_DIURESE_ATIVA          VARCHAR2(1 BYTE) NOT NULL,
  IE_SN_PRESSAO_INTRACRANIANA  VARCHAR2(1 BYTE) NOT NULL,
  IE_SM_ACIDOSE_ALCALOSE       VARCHAR2(1 BYTE) NOT NULL,
  IE_SM_NUT_PARENT             VARCHAR2(1 BYTE) NOT NULL,
  IE_SM_NUT_ENTERAL            VARCHAR2(1 BYTE) NOT NULL,
  IE_IE_INTER_UNICA_UTI        VARCHAR2(1 BYTE) NOT NULL,
  IE_IE_INTER_MULT_UTI         VARCHAR2(1 BYTE) NOT NULL,
  IE_IE_INTER_FORA_UTI         VARCHAR2(1 BYTE) NOT NULL,
  QT_PONTUACAO                 NUMBER(3)        NOT NULL,
  DT_LIBERACAO                 DATE,
  CD_PERFIL_ATIVO              NUMBER(5),
  IE_SITUACAO                  VARCHAR2(1 BYTE),
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  NR_HORA                      NUMBER(2),
  NR_SEQ_PRESCR                NUMBER(10),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISINTE_ATEPACI_FK_I ON TASY.TISS_INTERV_TERAPEUTICA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISINTE_I1 ON TASY.TISS_INTERV_TERAPEUTICA
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISINTE_PEPRESC_FK_I ON TASY.TISS_INTERV_TERAPEUTICA
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISINTE_PERFIL_FK_I ON TASY.TISS_INTERV_TERAPEUTICA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISINTE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISINTE_PESFISI_FK_I ON TASY.TISS_INTERV_TERAPEUTICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISINTE_PK ON TASY.TISS_INTERV_TERAPEUTICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISINTE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Tiss_Interv_Terapeutica_Update
BEFORE INSERT OR UPDATE ON TASY.TISS_INTERV_TERAPEUTICA FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.qt_pontuacao	:= 0;

if	(:new.IE_AB_MONIT_PADRAO = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 5;
end if;

if	(:new.IE_AB_LABORATORIO = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

if	(:new.IE_AB_MEDIC_ENDOVENOSA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;

elsif (:new.IE_AB_MEDIC_UNICA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 2;
end if;

if	(:new.IE_AB_CURATIVO_ROTINA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

if  (:new.IE_AB_CURATIVO_FREQUENTE = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

if	(:new.IE_AB_DRENO = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_SV_VENT_MECANICA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 5;

elsif (:new.IE_SV_SUP_VENT_SUPL = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 2;
end if;

if	(:new.IE_SV_VIAS_AERIAS = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

if	(:new.IE_SV_TRAT_FUNCAO_PULMONAR = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

if	(:new.IE_SC_MEDIC_VASOATIVA_MULT = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 4;

elsif (:new.IE_SC_MEDIC_VASOATIVA_UNICA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_SC_REP_VOLEMICA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 4;
end if;

if	(:new.IE_SC_CATETER_ART_PERIF = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 5;
end if;

if	(:new.IE_SC_MONIT_ATRIO_ESQ = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 8;
end if;

if	(:new.IE_SC_VIA_VENOSA_CENTRAL = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 2;
end if;

if	(:new.IE_SC_RESSUSCIT_CARDIO = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_SR_TEC_HEMOFILTRACAO = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_SR_MED_DEBITO_URINA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 2;
end if;

if	(:new.IE_SR_DIURESE_ATIVA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_SN_PRESSAO_INTRACRANIANA = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 4;
end if;

if	(:new.IE_SM_ACIDOSE_ALCALOSE = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 4;
end if;

if	(:new.IE_SM_NUT_PARENT = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_SM_NUT_ENTERAL = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 2;
end if;

if	(:new.IE_IE_INTER_MULT_UTI = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 5;

elsif (:new.IE_IE_INTER_UNICA_UTI = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 3;
end if;

if	(:new.IE_IE_INTER_FORA_UTI = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 5;
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/


ALTER TABLE TASY.TISS_INTERV_TERAPEUTICA ADD (
  CONSTRAINT TISINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_INTERV_TERAPEUTICA ADD (
  CONSTRAINT TISINTE_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT TISINTE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TISINTE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TISINTE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.TISS_INTERV_TERAPEUTICA TO NIVEL_1;

