ALTER TABLE TASY.PLS_MONITOR_TISS_PARAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MONITOR_TISS_PARAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MONITOR_TISS_PARAM
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  QT_CONTA_ARQUIVO              NUMBER(10),
  IE_UTILIZA_GUIA_OPERADORA     VARCHAR2(1 BYTE),
  IE_ENVIA_CTA_INTER            VARCHAR2(1 BYTE),
  IE_ORIGEM_REGRA_CONV_TAB      VARCHAR2(1 BYTE),
  IE_PERMITE_REC_GLOSA          VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO            NUMBER(4),
  IE_CBO_PREST_EXECUTOR         VARCHAR2(3 BYTE),
  IE_CNES_PREST_INTER           VARCHAR2(1 BYTE),
  IE_CONVERTE_ITEM              VARCHAR2(1 BYTE) DEFAULT null,
  IE_GERAR_NOVO_ARQUIVO         VARCHAR2(1 BYTE),
  IE_DATA_PROTOCOLO             VARCHAR2(1 BYTE),
  IE_AGRUPAR_PAGTOS_MESMA_COMP  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMTIP_ESTABEL_FK_I ON TASY.PLS_MONITOR_TISS_PARAM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMTIP_PK ON TASY.PLS_MONITOR_TISS_PARAM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_monitor_tiss_param_alt
before update ON TASY.PLS_MONITOR_TISS_PARAM for each row
declare

ds_observacao_w		pls_monitor_tiss_param_log.ds_observacao%type;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S')  then

	if	(:new.qt_conta_arquivo is null and :old.qt_conta_arquivo is not null) or
		(:new.qt_conta_arquivo is not null and :old.qt_conta_arquivo is null) or
		(:new.qt_conta_arquivo <> :old.qt_conta_arquivo) then
		ds_observacao_w := 	ds_observacao_w 			|| pls_util_pck.enter_w ||
					'Campo: QT_CONTA_ARQUIVO '  		|| pls_util_pck.enter_w ||
					'Antes: ' || :old.qt_conta_arquivo	|| pls_util_pck.enter_w ||
					'Novo: ' || :new.qt_conta_arquivo 	|| pls_util_pck.enter_w;
	end if;

	if	(:new.ie_utiliza_guia_operadora is null and :old.ie_utiliza_guia_operadora is not null) or
		(:new.ie_utiliza_guia_operadora is not null and :old.ie_utiliza_guia_operadora is null) or
		(:new.ie_utiliza_guia_operadora <> :old.ie_utiliza_guia_operadora) then
		ds_observacao_w := 	ds_observacao_w 				|| pls_util_pck.enter_w ||
					'Campo: IE_UTILIZA_GUIA_OPERADORA '		|| pls_util_pck.enter_w ||
					'Antes: ' || :old.ie_utiliza_guia_operadora	|| pls_util_pck.enter_w ||
					'Novo: ' || :new.ie_utiliza_guia_operadora	|| pls_util_pck.enter_w;
	end if;

	if	(:new.ie_envia_cta_inter is null and :old.ie_envia_cta_inter is not null) or
		(:new.ie_envia_cta_inter is not null and :old.ie_envia_cta_inter is null) or
		(:new.ie_envia_cta_inter <> :old.ie_envia_cta_inter) then
		ds_observacao_w := 	ds_observacao_w 			|| pls_util_pck.enter_w ||
					'Campo: IE_ENVIA_CTA_INTER '		|| pls_util_pck.enter_w ||
					'Antes: ' || :old.ie_envia_cta_inter	|| pls_util_pck.enter_w ||
					'Novo: ' || :new.ie_envia_cta_inter	|| pls_util_pck.enter_w;
	end if;

	if	(:new.ie_permite_rec_glosa is null and :old.ie_permite_rec_glosa is not null) or
		(:new.ie_permite_rec_glosa is not null and :old.ie_permite_rec_glosa is null) or
		(:new.ie_permite_rec_glosa <> :old.ie_permite_rec_glosa) then
		ds_observacao_w := 	ds_observacao_w 			|| pls_util_pck.enter_w ||
					'Campo: IE_PERMITE_REC_GLOSA '		|| pls_util_pck.enter_w ||
					'Antes: ' || :old.ie_permite_rec_glosa 	|| pls_util_pck.enter_w ||
					'Novo: ' || :new.ie_permite_rec_glosa	|| pls_util_pck.enter_w;
	end if;

	if	(:new.ie_origem_regra_conv_tab is null and :old.ie_origem_regra_conv_tab is not null) or
		(:new.ie_origem_regra_conv_tab is not null and :old.ie_origem_regra_conv_tab is null) or
		(:new.ie_origem_regra_conv_tab <> :old.ie_origem_regra_conv_tab) then
		ds_observacao_w := 	ds_observacao_w 				|| pls_util_pck.enter_w ||
					'Campo: IE_ORIGEM_REGRA_CONV_TAB '		|| pls_util_pck.enter_w ||
					'Antes: ' || :old.ie_origem_regra_conv_tab	|| pls_util_pck.enter_w ||
					'Novo: ' || :new.ie_origem_regra_conv_tab	|| pls_util_pck.enter_w;
	end if;

	if	(ds_observacao_w is not null) then
		insert	into	pls_monitor_tiss_param_log
			(	dt_atualizacao, dt_atualizacao_nrec, nm_usuario,
				nm_usuario_nrec, nr_seq_tiss_param, ds_observacao,
				nr_sequencia)
		values	(	sysdate, sysdate, :new.nm_usuario,
				:new.nm_usuario, :new.nr_sequencia, ds_observacao_w,
				pls_monitor_tiss_param_log_seq.nextval);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_MONITOR_TISS_PARAM_tp  after update ON TASY.PLS_MONITOR_TISS_PARAM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CBO_PREST_EXECUTOR,1,4000),substr(:new.IE_CBO_PREST_EXECUTOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CBO_PREST_EXECUTOR',ie_log_w,ds_w,'PLS_MONITOR_TISS_PARAM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MONITOR_TISS_PARAM ADD (
  CONSTRAINT PLSMTIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MONITOR_TISS_PARAM ADD (
  CONSTRAINT PLSMTIP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_MONITOR_TISS_PARAM TO NIVEL_1;

