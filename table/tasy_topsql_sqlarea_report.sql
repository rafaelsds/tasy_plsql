ALTER TABLE TASY.TASY_TOPSQL_SQLAREA_REPORT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_TOPSQL_SQLAREA_REPORT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_TOPSQL_SQLAREA_REPORT
(
  SQL_ID                   VARCHAR2(13 BYTE)    NOT NULL,
  COMMAND_TYPE             NUMBER(10),
  OCCURENCES_TOP_N         NUMBER(10),
  OBJECT_NAME              VARCHAR2(128 BYTE),
  MODULE                   VARCHAR2(64 BYTE),
  ACTION                   VARCHAR2(64 BYTE),
  FIRST_SAMPLE             DATE,
  LAST_SAMPLE              DATE,
  LAST_EXECUTIONS          NUMBER(10),
  LAST_ELAPSED_TIME        NUMBER(10),
  LAST_CPU_TIME            NUMBER(10),
  LAST_DISK_READS          NUMBER(10),
  LAST_BUFFER_GETS         NUMBER(10),
  LAST_USER_IO_WAIT_TIME   NUMBER(10),
  LAST_ROWS_PROCESSED      NUMBER(10),
  TOTAL_EXECUTIONS         NUMBER(10),
  TOTAL_ELAPSED_TIME       NUMBER(10),
  TOTAL_CPU_TIME           NUMBER(10),
  TOTAL_DISK_READS         NUMBER(10),
  TOTAL_BUFFER_GETS        NUMBER(10),
  TOTAL_USER_IO_WAIT_TIME  NUMBER(10),
  TOTAL_ROWS_PROCESSED     NUMBER(10),
  SQL_TEXT                 VARCHAR2(1000 BYTE),
  SQL_FULLTEXT             CLOB,
  LAST_SQL_PLANS           CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (LAST_SQL_PLANS) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (SQL_FULLTEXT) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TOPSQLREP_PK ON TASY.TASY_TOPSQL_SQLAREA_REPORT
(SQL_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TASY_TOPSQL_SQLAREA_REPORT ADD (
  CONSTRAINT TOPSQLREP_PK
 PRIMARY KEY
 (SQL_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TASY_TOPSQL_SQLAREA_REPORT TO NIVEL_1;

