ALTER TABLE TASY.PLS_BENEF_PROP_CANAL_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_BENEF_PROP_CANAL_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_BENEF_PROP_CANAL_COMPL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_BENEF_PROPOSTA      NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_VENDEDOR_CANAL      NUMBER(10),
  NR_SEQ_VENDEDOR_VINCULADO  NUMBER(10),
  NR_SEQ_PROPOSTA            NUMBER(10)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSBPCC_ESTABEL_FK_I ON TASY.PLS_BENEF_PROP_CANAL_COMPL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBPCC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSBPCC_PK ON TASY.PLS_BENEF_PROP_CANAL_COMPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBPCC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSBPCC_PLSPROB_FK_I ON TASY.PLS_BENEF_PROP_CANAL_COMPL
(NR_SEQ_BENEF_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBPCC_PLSPROB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBPCC_PLSVEND_FK_I ON TASY.PLS_BENEF_PROP_CANAL_COMPL
(NR_SEQ_VENDEDOR_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBPCC_PLSVEND_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBPCC_PLSVEVI_FK_I ON TASY.PLS_BENEF_PROP_CANAL_COMPL
(NR_SEQ_VENDEDOR_VINCULADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBPCC_PLSVEVI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_BENEF_PROP_CANAL_COMPL ADD (
  CONSTRAINT PLSBPCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_BENEF_PROP_CANAL_COMPL ADD (
  CONSTRAINT PLSBPCC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSBPCC_PLSPROB_FK 
 FOREIGN KEY (NR_SEQ_BENEF_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSBPCC_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_CANAL) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSBPCC_PLSVEVI_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_VINCULADO) 
 REFERENCES TASY.PLS_VENDEDOR_VINCULADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_BENEF_PROP_CANAL_COMPL TO NIVEL_1;

