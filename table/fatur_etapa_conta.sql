ALTER TABLE TASY.FATUR_ETAPA_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FATUR_ETAPA_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FATUR_ETAPA_CONTA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_ETAPA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CONVENIO             NUMBER(5),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4),
  IE_TIPO_ATENDIMENTO     NUMBER(3),
  NR_SEQ_CLASSIFICACAO    NUMBER(10),
  CD_CATEGORIA            VARCHAR2(10 BYTE),
  IE_CLINICA              NUMBER(5),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  NR_SEQ_MOTIVO_DEV       NUMBER(10),
  IE_CONTA_AJUSTE_CANCEL  VARCHAR2(1 BYTE),
  CD_SETOR_ATEND_ETAPA    NUMBER(10),
  IE_PESSOA_ETAPA         VARCHAR2(3 BYTE),
  IE_ETAPA_CRITICA        VARCHAR2(1 BYTE),
  IE_CONTA_DESDOBRAMENTO  VARCHAR2(1 BYTE),
  CD_PERFIL               NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FATETAC_CATCONV_FK_I ON TASY.FATUR_ETAPA_CONTA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_CLAATEN_FK_I ON TASY.FATUR_ETAPA_CONTA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_CONVENI_FK_I ON TASY.FATUR_ETAPA_CONTA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_ESTABEL_FK_I ON TASY.FATUR_ETAPA_CONTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_FATETAP_FK_I ON TASY.FATUR_ETAPA_CONTA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_FATETAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_FATMODE_FK_I ON TASY.FATUR_ETAPA_CONTA
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_FATMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_PERFIL_FK_I ON TASY.FATUR_ETAPA_CONTA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FATETAC_PK ON TASY.FATUR_ETAPA_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FATETAC_SETATEN_FK_I ON TASY.FATUR_ETAPA_CONTA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAC_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAC_SETATEN_FK2_I ON TASY.FATUR_ETAPA_CONTA
(CD_SETOR_ATEND_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FATUR_ETAPA_CONTA_tp  after update ON TASY.FATUR_ETAPA_CONTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ETAPA_CRITICA,1,4000),substr(:new.IE_ETAPA_CRITICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ETAPA_CRITICA',ie_log_w,ds_w,'FATUR_ETAPA_CONTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FATUR_ETAPA_CONTA ADD (
  CONSTRAINT FATETAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FATUR_ETAPA_CONTA ADD (
  CONSTRAINT FATETAC_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND_ETAPA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT FATETAC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT FATETAC_FATMODE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.FATUR_MOTIVO_DEVOL (NR_SEQUENCIA),
  CONSTRAINT FATETAC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT FATETAC_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT FATETAC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT FATETAC_FATETAP_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.FATUR_ETAPA (NR_SEQUENCIA),
  CONSTRAINT FATETAC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FATETAC_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.FATUR_ETAPA_CONTA TO NIVEL_1;

