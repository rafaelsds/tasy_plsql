ALTER TABLE TASY.BANDA_RELAT_CAMPO_LONGO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANDA_RELAT_CAMPO_LONGO CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANDA_RELAT_CAMPO_LONGO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_BANDA_RELAT_CAMPO  NUMBER(10)          NOT NULL,
  DS_CONTEUDO               CLOB,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_CONTEUDO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BANRECALO_BANRECA_FK_I ON TASY.BANDA_RELAT_CAMPO_LONGO
(NR_SEQ_BANDA_RELAT_CAMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BANRECALO_PK ON TASY.BANDA_RELAT_CAMPO_LONGO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.BANDA_RELAT_CAMPO_LONGO
	BEFORE INSERT OR DELETE OR UPDATE ON TASY.BANDA_RELAT_CAMPO_LONGO 	FOR EACH ROW
BEGIN
	IF(WHEB_USUARIO_PCK.GET_IE_EXECUTAR_TRIGGER = 'N')THEN
		RETURN;
	END IF;

	exec_sql_dinamico(  '',
						' UPDATE RELATORIO '||
						' SET 	IE_GERAR_RELATORIO = ''S'' ,'||
						' DT_LAST_MODIFICATION     = SYSDATE,'||
						' DT_ATUALIZACAO     = SYSDATE,'||
						' NM_USUARIO         = ' || CHR(39) ||NVL(:NEW.NM_USUARIO,:OLD.NM_USUARIO) || CHR(39) ||
						' WHERE 	NR_SEQUENCIA 	   = (SELECT	b.nr_seq_relatorio ' ||
						' FROM	BANDA_RELAT_CAMPO a, ' ||
						' 		BANDA_RELATORIO b ' ||
						' WHERE	a.nr_seq_banda = b.nr_sequencia ' ||
						' AND 	a.nr_Sequencia = ' || NVL(:NEW.nr_Seq_banda_relat_campo, :OLD.nr_Seq_banda_relat_campo) || ')');


END;
/


ALTER TABLE TASY.BANDA_RELAT_CAMPO_LONGO ADD (
  CONSTRAINT BANRECALO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANDA_RELAT_CAMPO_LONGO ADD (
  CONSTRAINT BANRECALO_BANRECA_FK 
 FOREIGN KEY (NR_SEQ_BANDA_RELAT_CAMPO) 
 REFERENCES TASY.BANDA_RELAT_CAMPO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.BANDA_RELAT_CAMPO_LONGO TO NIVEL_1;

