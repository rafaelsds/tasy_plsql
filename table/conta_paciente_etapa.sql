ALTER TABLE TASY.CONTA_PACIENTE_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_PACIENTE_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_PACIENTE_ETAPA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_INTERNO_CONTA        NUMBER(15)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ETAPA                DATE                  NOT NULL,
  NR_SEQ_ETAPA            NUMBER(10)            NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_DEV       NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_FIM_ETAPA            DATE,
  DT_PERIODO_INICIAL      DATE,
  DT_PERIODO_FINAL        DATE,
  NR_LOTE_BARRAS          NUMBER(10),
  CD_PESSOA_EXEC          VARCHAR2(10 BYTE),
  IE_ETAPA_CRITICA        VARCHAR2(1 BYTE),
  NR_SEQ_PROT_DOCUMENTO   NUMBER(10),
  DT_RECEBIMENTO          DATE,
  NM_USUARIO_RECEBIMENTO  VARCHAR2(15 BYTE),
  VL_CONTA                NUMBER(15,2),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  IE_ETAPA_PROTOCOLO      VARCHAR2(1 BYTE),
  NR_SEQ_CONTA_ORIGEM     NUMBER(10),
  NR_SEQ_LOTE             NUMBER(10),
  IE_STATUS               VARCHAR2(3 BYTE),
  NR_SEQ_LOTE_DEV         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONPAET_CONPACI_FK_I ON TASY.CONTA_PACIENTE_ETAPA
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_FATETAP_FK_I ON TASY.CONTA_PACIENTE_ETAPA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_FATMODE_FK_I ON TASY.CONTA_PACIENTE_ETAPA
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPAET_FATMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPAET_I1 ON TASY.CONTA_PACIENTE_ETAPA
(NR_LOTE_BARRAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_I2 ON TASY.CONTA_PACIENTE_ETAPA
(NR_INTERNO_CONTA, DT_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_I3 ON TASY.CONTA_PACIENTE_ETAPA
(NR_INTERNO_CONTA, NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_LOTEETA_FK_I ON TASY.CONTA_PACIENTE_ETAPA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_LOTEETA_FK2_I ON TASY.CONTA_PACIENTE_ETAPA
(NR_SEQ_LOTE_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_PESFISI_FK_I ON TASY.CONTA_PACIENTE_ETAPA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_PESFISI_FK2_I ON TASY.CONTA_PACIENTE_ETAPA
(CD_PESSOA_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONPAET_PK ON TASY.CONTA_PACIENTE_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPAET_SETATEN_FK_I ON TASY.CONTA_PACIENTE_ETAPA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPAET_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CONTA_PACIENTE_ETAPA_tp  after update ON TASY.CONTA_PACIENTE_ETAPA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_DEV,1,4000),substr(:new.NR_SEQ_MOTIVO_DEV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_DEV',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_ORIGEM,1,4000),substr(:new.NR_SEQ_CONTA_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_ORIGEM',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ETAPA,1,4000),substr(:new.DT_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'DT_ETAPA',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_ETAPA,1,4000),substr(:new.DT_FIM_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_ETAPA',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PERIODO_INICIAL,1,4000),substr(:new.DT_PERIODO_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_PERIODO_INICIAL',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PERIODO_FINAL,1,4000),substr(:new.DT_PERIODO_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_PERIODO_FINAL',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LOTE_BARRAS,1,4000),substr(:new.NR_LOTE_BARRAS,1,4000),:new.nm_usuario,nr_seq_w,'NR_LOTE_BARRAS',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_EXEC,1,4000),substr(:new.CD_PESSOA_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_EXEC',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ETAPA_CRITICA,1,4000),substr(:new.IE_ETAPA_CRITICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ETAPA_CRITICA',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROT_DOCUMENTO,1,4000),substr(:new.NR_SEQ_PROT_DOCUMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROT_DOCUMENTO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_RECEBIMENTO,1,4000),substr(:new.DT_RECEBIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_RECEBIMENTO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_RECEBIMENTO,1,4000),substr(:new.NM_USUARIO_RECEBIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_RECEBIMENTO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CONTA,1,4000),substr(:new.VL_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_CONTA',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROTOCOLO,1,4000),substr(:new.NR_SEQ_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROTOCOLO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ETAPA_PROTOCOLO,1,4000),substr(:new.IE_ETAPA_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ETAPA_PROTOCOLO',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ETAPA,1,4000),substr(:new.NR_SEQ_ETAPA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ETAPA',ie_log_w,ds_w,'CONTA_PACIENTE_ETAPA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Conta_Paciente_Etapa_Insert
BEFORE INSERT ON TASY.CONTA_PACIENTE_ETAPA FOR EACH ROW
DECLARE

dt_periodo_inicial_w	DATE;
ie_receb_automatico_w   VARCHAR2(3);
dt_periodo_final_w	DATE;
nr_seq_evento_w		number(10);
nr_atendimento_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ie_receb_mesma_etapa_w	varchar2(1);
nr_seq_ultima_etapa_w	conta_paciente_etapa.nr_seq_etapa%type;

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	ie_evento_disp = 'IEC'
	and	(nvl(nr_seq_etapa,:NEW.NR_SEQ_ETAPA) = :NEW.NR_SEQ_ETAPA)
	and	(nvl(nr_seq_motivo_dev, nvl(:new.nr_seq_motivo_dev,0)) = nvl(:new.nr_seq_motivo_dev,0))
	and	nvl(ie_situacao,'A') = 'A'
	order by
		nvl(nr_seq_etapa,0),
		nvl(nr_seq_motivo_dev,0);

PRAGMA autonomous_transaction;

BEGIN

:NEW.ie_status := 'P';

select	nvl(max(ie_receb_automatico),'N'),
	nvl(max(ie_receb_mesma_etapa),'N')
into	ie_receb_automatico_w,
	ie_receb_mesma_etapa_w
from	fatur_etapa
where	nr_sequencia = :new.nr_seq_etapa;

SELECT	MAX(dt_periodo_inicial),
   	MAX(dt_periodo_final),
	MAX(cd_estabelecimento),
	max(nr_atendimento)
INTO	dt_periodo_inicial_w,
    	dt_periodo_final_w,
	cd_estabelecimento_w,
	nr_atendimento_w
FROM	conta_paciente
WHERE 	nr_interno_conta	= :NEW.nr_interno_conta;

select 	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_w;

nr_seq_ultima_etapa_w	:= 0;

if	(ie_receb_mesma_etapa_w = 'S') then
	nr_seq_ultima_etapa_w	:= somente_numero(obter_conta_paciente_etapa(:new.nr_interno_conta,'C'));
end if;

IF ((ie_receb_automatico_w = 'S') or (nvl(nr_seq_ultima_etapa_w,0) = :new.nr_seq_etapa)) THEN
    BEGIN
	:NEW.dt_recebimento := SYSDATE;
	:NEW.nm_usuario_recebimento := NVL(wheb_usuario_pck.get_nm_usuario,'Tasy');
	:NEW.ie_status := 'R';
	END;
END IF;

IF	(dt_periodo_inicial_w IS NOT NULL) THEN
	:NEW.dt_periodo_inicial	:= dt_periodo_inicial_w;
END IF;

IF	(dt_periodo_final_w IS NOT NULL) THEN
	:NEW.dt_periodo_final	:= dt_periodo_final_w;
END IF;

IF	(:NEW.nr_interno_conta IS NOT NULL) THEN
	BEGIN
	:NEW.vl_conta	:=  NVL(obter_valor_conta(:NEW.nr_interno_conta,0),0);
	EXCEPTION
		WHEN OTHERS THEN
		:NEW.vl_conta	:= 0;
	END;
END IF;

open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,wheb_usuario_pck.get_nm_usuario,null);
		end;
	end loop;
close C01;

COMMIT;

END;
/


CREATE OR REPLACE TRIGGER TASY.Conta_Paciente_Etapa_Update
BEFORE UPDATE ON TASY.CONTA_PACIENTE_ETAPA FOR EACH ROW
DECLARE

nr_seq_evento_w		number(10);
nr_atendimento_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	ie_evento_disp = 'FEC'
	and	(nvl(nr_seq_etapa,:NEW.NR_SEQ_ETAPA) = :NEW.NR_SEQ_ETAPA)
	and	(nvl(nr_seq_motivo_dev, nvl(:new.nr_seq_motivo_dev,0)) = nvl(:new.nr_seq_motivo_dev,0))
	and	nvl(ie_situacao,'A') = 'A'
	order by
		nvl(nr_seq_etapa,0),
		nvl(nr_seq_motivo_dev,0);

PRAGMA autonomous_transaction;

BEGIN

if	(:old.DT_FIM_ETAPA is null) and
	(:new.DT_FIM_ETAPA is not null) then

	SELECT	MAX(cd_estabelecimento),
		max(nr_atendimento)
	INTO	cd_estabelecimento_w,
		nr_atendimento_w
	FROM	conta_paciente
	WHERE 	nr_interno_conta	= :NEW.nr_interno_conta;

	select 	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_w;

	open C01;
		loop
		fetch C01 into
			nr_seq_evento_w;
		exit when C01%notfound;
			begin
			gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,wheb_usuario_pck.get_nm_usuario,null);
			end;
		end loop;
	close C01;

end if;

COMMIT;

END;
/


ALTER TABLE TASY.CONTA_PACIENTE_ETAPA ADD (
  CONSTRAINT CONPAET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_PACIENTE_ETAPA ADD (
  CONSTRAINT CONPAET_LOTEETA_FK2 
 FOREIGN KEY (NR_SEQ_LOTE_DEV) 
 REFERENCES TASY.LOTE_ETAPA (NR_SEQUENCIA),
  CONSTRAINT CONPAET_LOTEETA_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.LOTE_ETAPA (NR_SEQUENCIA),
  CONSTRAINT CONPAET_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONPAET_FATETAP_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.FATUR_ETAPA (NR_SEQUENCIA),
  CONSTRAINT CONPAET_FATMODE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.FATUR_MOTIVO_DEVOL (NR_SEQUENCIA),
  CONSTRAINT CONPAET_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT CONPAET_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_EXEC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONPAET_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CONTA_PACIENTE_ETAPA TO NIVEL_1;

