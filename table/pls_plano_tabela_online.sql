ALTER TABLE TASY.PLS_PLANO_TABELA_ONLINE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PLANO_TABELA_ONLINE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PLANO_TABELA_ONLINE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_PLANO_ONLINE   NUMBER(10)              NOT NULL,
  NR_SEQ_TABELA         NUMBER(10)              NOT NULL,
  DT_INICIO_VIGENCIA    DATE                    NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA       DATE,
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE),
  QT_VIDAS_INICIAL      NUMBER(10),
  QT_VIDAS_FINAL        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPLTO_PK ON TASY.PLS_PLANO_TABELA_ONLINE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLTO_PLSPLON_FK_I ON TASY.PLS_PLANO_TABELA_ONLINE
(NR_SEQ_PLANO_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLTO_PLSTAPR_FK_I ON TASY.PLS_PLANO_TABELA_ONLINE
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_plano_tabela_online_atual
before insert or update ON TASY.PLS_PLANO_TABELA_ONLINE for each row
declare

nr_seq_plano_regra_w	pls_plano.nr_sequencia%type;
nr_seq_plano_tabela_w	pls_plano.nr_sequencia%type;
nr_contrato_w		pls_tabela_preco.nr_contrato%type;
nr_segurado_w		pls_tabela_preco.nr_segurado%type;

begin

select	max(nr_seq_plano)
into	nr_seq_plano_regra_w
from	pls_plano_online
where	nr_sequencia	= :new.nr_seq_plano_online;

select	max(nr_seq_plano),
	max(nr_contrato),
	max(nr_segurado)
into	nr_seq_plano_tabela_w,
	nr_contrato_w,
	nr_segurado_w
from	pls_tabela_preco
where	nr_sequencia	= :new.nr_seq_tabela;

if	(nr_seq_plano_regra_w <> nr_seq_plano_tabela_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(1093636);
end if;

if	(nr_contrato_w is not null) or (nr_segurado_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1093678);
end if;

end;
/


ALTER TABLE TASY.PLS_PLANO_TABELA_ONLINE ADD (
  CONSTRAINT PLSPLTO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_PLANO_TABELA_ONLINE ADD (
  CONSTRAINT PLSPLTO_PLSPLON_FK 
 FOREIGN KEY (NR_SEQ_PLANO_ONLINE) 
 REFERENCES TASY.PLS_PLANO_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSPLTO_PLSTAPR_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PLANO_TABELA_ONLINE TO NIVEL_1;

