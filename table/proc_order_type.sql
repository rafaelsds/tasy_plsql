ALTER TABLE TASY.PROC_ORDER_TYPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_ORDER_TYPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_ORDER_TYPE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO  NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_ORDER_TYPE    NUMBER(10)               NOT NULL,
  IE_PERMITE_OBS       VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRORTYPE_CPTIPE_FK_I ON TASY.PROC_ORDER_TYPE
(NR_SEQ_ORDER_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRORTYPE_ESTABEL_FK_I ON TASY.PROC_ORDER_TYPE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRORTYPE_PK ON TASY.PROC_ORDER_TYPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRORTYPE_PROINTE_FK_I ON TASY.PROC_ORDER_TYPE
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROC_ORDER_TYPE ADD (
  CONSTRAINT PRORTYPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PROC_ORDER_TYPE ADD (
  CONSTRAINT PRORTYPE_CPTIPE_FK 
 FOREIGN KEY (NR_SEQ_ORDER_TYPE) 
 REFERENCES TASY.CPOE_TIPO_PEDIDO (NR_SEQUENCIA),
  CONSTRAINT PRORTYPE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PRORTYPE_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROC_ORDER_TYPE TO NIVEL_1;

