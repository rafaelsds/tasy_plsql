ALTER TABLE TASY.ATEND_PAC_UNID_CLASSIF_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PAC_UNID_CLASSIF_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PAC_UNID_CLASSIF_ESP
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_PAC_UNIDADE  NUMBER(10)          NOT NULL,
  NR_SEQ_CLASSIF_ESP        NUMBER(10),
  DT_INICIAL                DATE                NOT NULL,
  DT_FINAL                  DATE,
  NR_ATENDIMENTO            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPAUNCE_ATEPACU_FK_I ON TASY.ATEND_PAC_UNID_CLASSIF_ESP
(NR_SEQ_ATEND_PAC_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPAUNCE_CLAEPAC_FK_I ON TASY.ATEND_PAC_UNID_CLASSIF_ESP
(NR_SEQ_CLASSIF_ESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPAUNCE_PK ON TASY.ATEND_PAC_UNID_CLASSIF_ESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_pac_unid_classif_esp_aft
after insert ON TASY.ATEND_PAC_UNID_CLASSIF_ESP for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w			number(10);

pragma autonomous_transaction;
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	begin
	select	count(1)
	into	qt_reg_w
	from	atend_paciente_unidade
	where	nr_atendimento = :new.nr_atendimento
	and	nr_seq_interno < :new.nr_seq_atend_pac_unidade;

	if	(qt_reg_w > 0) then
		begin
		select	count(1)
		into	qt_reg_w
		from	atend_pac_unid_classif_esp
		where	nr_atendimento = :new.nr_atendimento
		and	nr_seq_atend_pac_unidade = :new.nr_seq_atend_pac_unidade
		and	dt_inicial < :new.dt_inicial;

		if	(qt_reg_w > 0) then
			reg_integracao_p.nr_atendimento 	:= :new.nr_atendimento;
			reg_integracao_p.ie_saida_unidade	:= 'N';
			reg_integracao_p.ie_tipo_movimentacao	:= 'T';
			reg_integracao_p.ie_operacao		:= 'A';

			gerar_int_padrao.gravar_integracao('118', :new.nr_seq_atend_pac_unidade, :new.nm_usuario, reg_integracao_p);
		end if;
		end;
	end if;
	end;
end if;
commit;
end;
/


ALTER TABLE TASY.ATEND_PAC_UNID_CLASSIF_ESP ADD (
  CONSTRAINT ATEPAUNCE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PAC_UNID_CLASSIF_ESP ADD (
  CONSTRAINT ATEPAUNCE_ATEPACU_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PAC_UNIDADE) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO)
    ON DELETE CASCADE,
  CONSTRAINT ATEPAUNCE_CLAEPAC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ESP) 
 REFERENCES TASY.CLASSIF_ESPECIAL_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_PAC_UNID_CLASSIF_ESP TO NIVEL_1;

