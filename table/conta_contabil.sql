ALTER TABLE TASY.CONTA_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_CONTABIL
(
  CD_CONTA_CONTABIL          VARCHAR2(20 BYTE)  NOT NULL,
  DS_CONTA_CONTABIL          VARCHAR2(255 BYTE) NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  IE_CENTRO_CUSTO            VARCHAR2(1 BYTE),
  CD_GRUPO                   NUMBER(10),
  IE_TIPO                    VARCHAR2(1 BYTE),
  CD_CLASSIFICACAO           VARCHAR2(40 BYTE)  NOT NULL,
  CD_EMPRESA                 NUMBER(4)          NOT NULL,
  CD_CENTRO_CUSTO            NUMBER(8),
  CD_SISTEMA_CONTABIL        VARCHAR2(140 BYTE),
  NR_SEQ_CRIT_RATEIO         NUMBER(10),
  CD_PLANO_ANS               NUMBER(10),
  DS_ORIENTACAO              VARCHAR2(2000 BYTE),
  IE_COMPENSACAO             VARCHAR2(1 BYTE)   NOT NULL,
  IE_VERSAO_ANS              VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA         DATE,
  DT_FIM_VIGENCIA            DATE,
  CD_CLASSIF_SUPERIOR        VARCHAR2(40 BYTE),
  CD_CLASSIF_ECD             VARCHAR2(40 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_ECD_REG_DRE             VARCHAR2(1 BYTE),
  IE_ECD_REG_BP              VARCHAR2(1 BYTE),
  IE_DIOPS                   VARCHAR2(1 BYTE),
  CD_CLASSIFICACAO_ATUAL     VARCHAR2(40 BYTE),
  CD_CLASSIF_SUPERIOR_ATUAL  VARCHAR2(40 BYTE),
  IE_NATUREZA_SPED           VARCHAR2(2 BYTE),
  IE_DEDUCAO_ACOMP_ORC       VARCHAR2(1 BYTE),
  CD_CONTA_REFERENCIA        VARCHAR2(20 BYTE),
  IE_INTERCOMPANY            VARCHAR2(1 BYTE),
  IE_ELIMINACAO_LANCTO       VARCHAR2(1 BYTE),
  DT_ELIMINACAO_LANCTO       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONCONT_CENCONT_FK_I ON TASY.CONTA_CONTABIL
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCONT_CENCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONCONT_CONCONT_FK_I ON TASY.CONTA_CONTABIL
(CD_CONTA_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONCONT_CRITRAT_FK_I ON TASY.CONTA_CONTABIL
(NR_SEQ_CRIT_RATEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCONT_CRITRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONCONT_CTBGRCO_FK_I ON TASY.CONTA_CONTABIL
(CD_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONCONT_CTBPLAN_FK_I ON TASY.CONTA_CONTABIL
(CD_EMPRESA, CD_PLANO_ANS, IE_VERSAO_ANS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONCONT_EMPRESA_FK_I ON TASY.CONTA_CONTABIL
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONCONT_I1 ON TASY.CONTA_CONTABIL
(CD_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCONT_I1
  MONITORING USAGE;


CREATE INDEX TASY.CONCONT_I2 ON TASY.CONTA_CONTABIL
(CD_EMPRESA, CD_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONCONT_I3 ON TASY.CONTA_CONTABIL
(CD_CLASSIFICACAO_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCONT_I3
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONCONT_PK ON TASY.CONTA_CONTABIL
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CONTA_CONTABIL_tp  after update ON TASY.CONTA_CONTABIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CONTA_CONTABIL);  ds_c_w:=null; ds_w:=substr(:new.IE_ELIMINACAO_LANCTO,1,500);gravar_log_alteracao(substr(:old.IE_ELIMINACAO_LANCTO,1,4000),substr(:new.IE_ELIMINACAO_LANCTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ELIMINACAO_LANCTO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONTA_CONTABIL,1,4000),substr(:new.DS_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONTA_CONTABIL',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIFICACAO,1,4000),substr(:new.CD_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIFICACAO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ELIMINACAO_LANCTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ELIMINACAO_LANCTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ELIMINACAO_LANCTO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_CONTABIL,1,4000),substr(:new.CD_SISTEMA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_CONTABIL',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIF_SUPERIOR,1,4000),substr(:new.CD_CLASSIF_SUPERIOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIF_SUPERIOR',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CENTRO_CUSTO,1,4000),substr(:new.IE_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CENTRO_CUSTO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO,1,4000),substr(:new.CD_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO',ie_log_w,ds_w,'CONTA_CONTABIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.conta_contabil_insert
before insert or update ON TASY.CONTA_CONTABIL for each row
begin

if	(wheb_usuario_pck.GET_IE_EXECUTAR_TRIGGER = 'S') then
	begin
	if	(:new.dt_inicio_vigencia is not null) and
		(:new.dt_fim_vigencia is not null) and
		(:new.dt_inicio_vigencia > :new.dt_fim_vigencia) then
		wheb_mensagem_pck.exibir_mensagem_abort(266389);
		/*A data de in�cio, n�o pode ser maior que a data final da vig�ncia.');*/
	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.conta_contabil_update
before update ON TASY.CONTA_CONTABIL for each row
declare

ds_log_w	varchar2(2000);
qt_registro_w	number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if	not (:new.ie_centro_custo in ('S','N')) then
		/* O campo Exige Centro de Custo deve ter valor S, N */
		wheb_mensagem_pck.exibir_mensagem_abort(266390);
	end if;

	if	(obter_pais_sistema(wheb_usuario_pck.get_cd_perfil,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento) = 1) and
		(nvl(:new.ie_centro_custo,'X') <> nvl(:old.ie_centro_custo,'X')) then
		begin
		select	count(*)
		into	qt_registro_w
		from	(select	1
			from	ctb_movimento a
			where	a.cd_conta_credito = :new.cd_conta_contabil
			and	rownum = 1
			union all
			select	1
			from	ctb_movimento a
			where	a.cd_conta_debito = :new.cd_conta_contabil
			and	rownum = 1);

		if	(qt_registro_w > 0) then
			begin
			/* Esta conta contabil ja possui movimentacao na contabilidade, desta forma o detalhamento por centro de custo nao pode ser alterado */
			wheb_mensagem_pck.exibir_mensagem_abort(1009703);
			end;
		end if;

		end;
	end if;

	if	((:new.cd_classificacao <> :old.cd_classificacao) OR (:new.cd_classif_superior <> :old.cd_classif_superior)) then
		begin
		select	count(*)
		into	qt_registro_w
		from	(select	1
			from	ctb_saldo c
			where	c.cd_conta_contabil = :new.cd_conta_contabil
			and	rownum = 1);

		if	(qt_registro_w > 0) then
			begin
			/*Nao eh permitido alterar a classificacao e classificacao superior da conta contabil #@CD_CONTA_CONTABIL#@ - #@DS_CONTA_CONTABIL#@, porque ela ja   possui movimentacao na contabilidade.
			Utilize a regra de classificacao por periodo para modificar a classificacao e classificacao superior desta conta.*/
			wheb_mensagem_pck.exibir_mensagem_abort(1141110, substr('CD_CONTA_CONTABIL=' || :new.cd_conta_contabil || ';' ||
										'DS_CONTA_CONTABIL=' || :new.ds_conta_contabil, 1, 2000));
			end;
		end if;

		end;
	end if;

	if	(:new.ie_tipo <> :old.ie_tipo) then
	/* Alteracao do TIPO da conta. De: #@OLD_IE_TIPO#@ Para: #@NEW_IE_TIPO#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818830, 'OLD_IE_TIPO=' || :old.ie_tipo || ';NEW_IE_TIPO=' || :new.ie_tipo),1,500);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	if	(:new.cd_classificacao <> :old.cd_classificacao) then
	/* Alteracao da CLASSIFICACAO da conta. De: #@OLD_CD_CLASSIFICACAO#@ Para: #@NEW_CD_CLASSIFICACAO#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818831, 'OLD_CD_CLASSIFICACAO=' || :old.cd_classificacao || ';NEW_CD_CLASSIFICACAO=' || :new.cd_classificacao),1,2000);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	if	(:new.cd_classif_superior <> :old.cd_classif_superior) then
	/* Alteracao da CLASSIFICACAO SUPERIOR da conta. De: #@OLD_CD_CLASSIF_SUPERIOR#@ Para: #@NEW_CD_CLASSIF_SUPERIOR#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818832, 'OLD_CD_CLASSIF_SUPERIOR=' || :old.cd_classif_superior || ';NEW_CD_CLASSIF_SUPERIOR=' || :new.cd_classif_superior),1,2000);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	if	(:new.cd_grupo <> :old.cd_grupo) then
	/* Alteracao do GRUPO da conta. De: #@OLD_CD_GRUPO#@ Para: #@NEW_CD_GRUPO#@ */
		ds_log_w	:= substr(wheb_mensagem_pck.get_texto(818833, 'OLD_CD_GRUPO=' || :old.cd_grupo || ';NEW_CD_GRUPO=' || :new.cd_grupo),1,2000);
		ctb_gravar_log_conta(:new.cd_conta_contabil, ds_log_w, :new.nm_usuario);
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.CONTA_CONTABIL ADD (
  CONSTRAINT CONCONT_PK
 PRIMARY KEY
 (CD_CONTA_CONTABIL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          448K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_CONTABIL ADD (
  CONSTRAINT CONCONT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_REFERENCIA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CONCONT_CTBGRCO_FK 
 FOREIGN KEY (CD_GRUPO) 
 REFERENCES TASY.CTB_GRUPO_CONTA (CD_GRUPO),
  CONSTRAINT CONCONT_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CONCONT_CENCONT_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CONCONT_CRITRAT_FK 
 FOREIGN KEY (NR_SEQ_CRIT_RATEIO) 
 REFERENCES TASY.CTB_CRITERIO_RATEIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONTA_CONTABIL TO NIVEL_1;

