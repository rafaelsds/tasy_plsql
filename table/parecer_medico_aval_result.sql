ALTER TABLE TASY.PARECER_MEDICO_AVAL_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARECER_MEDICO_AVAL_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARECER_MEDICO_AVAL_RESULT
(
  NR_SEQ_ITEM             NUMBER(10)            NOT NULL,
  QT_RESULTADO            NUMBER(15,4),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_RESULTADO            CLOB,
  NR_SEQ_INTERNO          NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA  NUMBER(10),
  DT_LIBERACAO            DATE,
  NR_SEQUENCIA            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_RESULTADO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARMEDAVRS_ATCONSPEPA_FK_I ON TASY.PARECER_MEDICO_AVAL_RESULT
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDAVRS_MEDITAV_FK_I ON TASY.PARECER_MEDICO_AVAL_RESULT
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDAVRS_PARMEDI_FK_I ON TASY.PARECER_MEDICO_AVAL_RESULT
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARMEDAVRS_PK ON TASY.PARECER_MEDICO_AVAL_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.parecer_medico_aval_result_up
before insert ON TASY.PARECER_MEDICO_AVAL_RESULT for each row
declare

nr_sequencia_w		parecer_medico_aval_result.nr_sequencia%type;

begin

if	(:new.nr_sequencia is null) then

	select	parecer_medico_aval_result_seq.nextval
	into	nr_sequencia_w
	from	dual;

	:new.nr_sequencia := nr_sequencia_w;
end if;

end;
/


ALTER TABLE TASY.PARECER_MEDICO_AVAL_RESULT ADD (
  CONSTRAINT PARMEDAVRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARECER_MEDICO_AVAL_RESULT ADD (
  CONSTRAINT PARMEDAVRS_MEDITAV_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.MED_ITEM_AVALIAR (NR_SEQUENCIA),
  CONSTRAINT PARMEDAVRS_PARMEDI_FK 
 FOREIGN KEY (NR_SEQ_INTERNO) 
 REFERENCES TASY.PARECER_MEDICO (NR_SEQ_INTERNO),
  CONSTRAINT PARMEDAVRS_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PARECER_MEDICO_AVAL_RESULT TO NIVEL_1;

