ALTER TABLE TASY.MATERIAL_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_ESTAB
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  IE_BAIXA_ESTOQ_PAC         VARCHAR2(1 BYTE),
  IE_MATERIAL_ESTOQUE        VARCHAR2(1 BYTE),
  QT_ESTOQUE_MINIMO          NUMBER(13,4),
  QT_PONTO_PEDIDO            NUMBER(13,4),
  QT_ESTOQUE_MAXIMO          NUMBER(15,4),
  QT_DIA_INTERV_RESSUP       NUMBER(3),
  QT_DIA_RESSUP_FORN         NUMBER(3),
  QT_DIA_ESTOQUE_MINIMO      NUMBER(3),
  NR_MINIMO_COTACAO          NUMBER(1),
  QT_CONSUMO_MENSAL          NUMBER(15,4),
  CD_MATERIAL_CONTA          NUMBER(6),
  CD_KIT_MATERIAL            NUMBER(5),
  QT_PESO_KG                 NUMBER(15,4),
  DT_ATUAL_CONSUMO           DATE,
  QT_MES_CONSUMO             NUMBER(15),
  IE_RESSUPRIMENTO           VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_CLASSIF_CUSTO           VARCHAR2(1 BYTE),
  NR_REGISTRO_ANVISA         VARCHAR2(60 BYTE),
  IE_ESTOQUE_LOTE            VARCHAR2(1 BYTE),
  IE_PRESCRICAO              VARCHAR2(1 BYTE)   NOT NULL,
  IE_PADRONIZADO             VARCHAR2(1 BYTE)   NOT NULL,
  IE_REQUISICAO              VARCHAR2(1 BYTE),
  IE_CURVA_XYZ               VARCHAR2(1 BYTE),
  IE_CONTROLA_SERIE          VARCHAR2(1 BYTE),
  DT_VALIDADE_REG_ANVISA     DATE,
  IE_UNID_CONSUMO_PRESCR     VARCHAR2(1 BYTE),
  QT_DESVIO_PADRAO_CONS      NUMBER(22,4),
  IE_FARMACIA_COM            VARCHAR2(1 BYTE),
  CD_UNIDADE_VENDA           VARCHAR2(30 BYTE),
  QT_CONV_ESTOQUE_VENDA      NUMBER(13,4),
  NR_PROTOCOLO_ANVISA        VARCHAR2(60 BYTE),
  DT_VALIDADE_PROT_ANVISA    DATE,
  CD_SISTEMA_ANT             VARCHAR2(20 BYTE),
  CD_UNIDADE_MEDIDA_COMPRA   VARCHAR2(15 BYTE),
  CD_UNIDADE_MEDIDA_CONSUMO  VARCHAR2(15 BYTE),
  CD_UNIDADE_MEDIDA_ESTOQUE  VARCHAR2(15 BYTE),
  QT_CONV_COMPRA_ESTOQUE     NUMBER(13,4),
  QT_CONV_ESTOQUE_CONSUMO    NUMBER(13,4),
  IE_REVISAR                 VARCHAR2(5 BYTE),
  NR_ICMS                    NUMBER(10),
  NR_ISS                     NUMBER(10),
  IE_SITUACAO_TRIBUTARIA     VARCHAR2(10 BYTE),
  IE_TIPO_CONSIGNADO         VARCHAR2(15 BYTE),
  IE_TIPO_IMPOSTO            VARCHAR2(10 BYTE),
  QT_CONSUMO_MENSAL_RESSUP   NUMBER(15,4),
  DT_SAZONALIDADE_INI        DATE,
  DT_SAZONALIDADE_FIN        DATE,
  IE_VIGENTE_ANVISA          VARCHAR2(1 BYTE)   DEFAULT null,
  IE_REG_ANVISA_ISENTO       VARCHAR2(1 BYTE),
  DS_MOTIVO_ISENCAO_ANVISA   VARCHAR2(255 BYTE),
  IE_TRIBUTACAO_ICMS         VARCHAR2(15 BYTE)  DEFAULT null,
  IE_ORIGEM_MERCADORIA       VARCHAR2(15 BYTE)  DEFAULT null,
  IE_OUT_OF_HOSPITAL         VARCHAR2(3 BYTE),
  IE_IN_HOSPITAL             VARCHAR2(3 BYTE),
  DT_IN_PERIODO_USO          DATE,
  DT_OUT_PERIODO_USO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATESTA_ESTABEL_FK_I ON TASY.MATERIAL_ESTAB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESTA_KITMATE_FK_I ON TASY.MATERIAL_ESTAB
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESTA_MATERIA_FK_I ON TASY.MATERIAL_ESTAB
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESTA_MATERIA_FK2_I ON TASY.MATERIAL_ESTAB
(CD_MATERIAL_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATESTA_PK ON TASY.MATERIAL_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATESTA_UK ON TASY.MATERIAL_ESTAB
(CD_ESTABELECIMENTO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESTA_UNIMEDI_FK_I ON TASY.MATERIAL_ESTAB
(CD_UNIDADE_VENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATESTA_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATESTA_UNIMEDI_FK2_I ON TASY.MATERIAL_ESTAB
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESTA_UNIMEDI_FK3_I ON TASY.MATERIAL_ESTAB
(CD_UNIDADE_MEDIDA_CONSUMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATESTA_UNIMEDI_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.MATESTA_UNIMEDI_FK4_I ON TASY.MATERIAL_ESTAB
(CD_UNIDADE_MEDIDA_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_estab_after
after insert or update or delete ON TASY.MATERIAL_ESTAB for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_operacao_w			varchar2(1);
ie_material_estoque_w		varchar2(1);
ie_padronizado_w		varchar2(1);
cd_material_w			material.cd_material%type;
nm_usuario_w			usuario.nm_usuario%type;
cd_estabelecimento_w	material_estab.cd_estabelecimento%type;
ds_retorno_integracao_w clob;
event_w					varchar2(35);
event_class_w			varchar2(100);
ie_integrar_w			varchar2(1) := 'S';
qt_retorno_w        varchar2(1);

pragma autonomous_transaction;

begin

select	obter_valor_param_usuario(9041,
                                  10,
                                  obter_perfil_ativo,
                                  wheb_usuario_pck.get_nm_usuario(),
                                  obter_estabelecimento_ativo())
into	qt_retorno_w
from	dual;

if (qt_retorno_w = 'S')then
  /* Inclus�o do if wheb_usuario_pck.get_ie_executar_trigger devido ao grande volume de dados  do cliente RedeDor, ao dar d isable e e nable na  trigger , causava  extrema lentid�o OS 1945059*/

  if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
    /*Quando Incluir, alterar e excluir as informa��es da material,_estab chama a integra��o padr�o*/
    if	(inserting) then
      ie_operacao_w			:= 'A';
      ie_material_estoque_w		:= :new.ie_material_estoque;
      ie_padronizado_w		:= :new.ie_padronizado;
      cd_material_w			:= :new.cd_material;
      nm_usuario_w			:= :new.nm_usuario;
      cd_estabelecimento_w	:= :new.cd_estabelecimento;
      event_w					:= 'establishmentmaterial.added'; -- event register in bifrost
      event_class_w			:= 'com.philips.tasy.integration.establishmentmaterial.outbound.EstablishmentMaterialAddedCallback'; -- event class created in tasy-interfaces(material module).
      ie_integrar_w			:= 'S';
    elsif	(updating) then
      ie_operacao_w			:= 'A';
      ie_material_estoque_w		:= :new.ie_material_estoque;
      ie_padronizado_w		:= :new.ie_padronizado	;
      cd_material_w			:= :new.cd_material;
      nm_usuario_w			:= :new.nm_usuario;
      cd_estabelecimento_w	:= :new.cd_estabelecimento;
      event_w					:= 'establishmentmaterial.updated'; -- event register in bifrost
      event_class_w			:= 'com.philips.tasy.integration.establishmentmaterial.outbound.EstablishmentMaterialUpdatedCallback'; -- event class created in tasy-interfaces(material module).
      ie_integrar_w			:= 'S';
      if	(:new.dt_atual_consumo <> :old.dt_atual_consumo) then
        ie_integrar_w			:= 'N';
      end if;
    else
      ie_operacao_w			:= 'A';
      ie_material_estoque_w		:= :old.ie_material_estoque;
      ie_padronizado_w		:= :old.ie_padronizado;
      cd_material_w			:= :old.cd_material;
      nm_usuario_w			:= :old.nm_usuario;
      cd_estabelecimento_w	:= :old.cd_estabelecimento;
      event_w					:= 'establishmentmaterial.deleted'; -- event register in bifrost
      event_class_w			:= 'com.philips.tasy.integration.establishmentmaterial.outbound.EstablishmentMaterialDeletedCallback'; -- event class created in tasy-interfaces(material module).
      if	(:new.dt_atual_consumo <> :old.dt_atual_consumo) then
        ie_integrar_w			:= 'N';
      end if;
    end if;

    reg_integracao_p.ie_operacao		:=	ie_operacao_w;
    reg_integracao_p.ie_material_estoque	:=	ie_material_estoque_w;
    reg_integracao_p.ie_padronizado		:=	ie_padronizado_w;

    if	(ie_integrar_w = 'S') then
      reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
      gerar_int_padrao.gravar_integracao('1', cd_material_w,nm_usuario_w,reg_integracao_p);
    end if;

    SELECT BIFROST.SEND_INTEGRATION(
        event_w,
        event_class_w,
        '{"material" : '||cd_material_w||', "establishment":' || cd_estabelecimento_w||'}',
        'integration')
    INTO ds_retorno_integracao_w
    FROM dual;

    commit;

  end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.material_estab_update
before update ON TASY.MATERIAL_ESTAB for each row
declare
ie_possui_reg_w		number(5);

begin

select	count(*)
into	ie_possui_reg_w
from	material_tipo_local_est
where	cd_material = :old.cd_material;

if	(ie_possui_reg_w > 0) then
	:new.ie_revisar	:= 'R';
	gerar_comunic_revisao_mat(:old.cd_material, null, :new.dt_atualizacao, :old.cd_estabelecimento, :new.nm_usuario );
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_estab_delete
before delete ON TASY.MATERIAL_ESTAB for each row
declare
qt_reg_prestador_w		number(5);
qt_reg_farmacia_w		number(5);


begin
if	(:old.ie_estoque_lote = 'S') then
	-- Este registro n�o pode ser exclu�do pois o material � controlado por lote.
	wheb_mensagem_pck.exibir_mensagem_abort(267087);
end if;


/*select	count(*)
into	qt_reg_prestador_w
from	material_tipo_local_est
where	cd_material = :old.cd_material
and	exists (select	1
		from	parametro_gestao_material y,
			estabelecimento x
		where	x.nr_seq_unid_neg	= y.nr_seq_unidade_hosp
		and	x.cd_estabelecimento	= :old.cd_estabelecimento);

select	count(*)
into	qt_reg_farmacia_w
from	material_tipo_local_est
where	cd_material = :old.cd_material
and	exists (select	1
		from	parametro_gestao_material y,
			estabelecimento x
		where	x.nr_seq_unid_neg	= y.nr_seq_unidade_farm
		and	x.cd_estabelecimento	= :old.cd_estabelecimento);

if	(qt_reg_prestador_w > 0) then
	update	material_tipo_local_est
	set	ie_prestador	= 'X'
	where	cd_material	= :old.cd_material;
elsif	(qt_reg_farmacia_w > 0) then
	update	material_tipo_local_est
	set	ie_farmacia	= 'X'
	where	cd_material	= :old.cd_material;
end if;*/

end;
/


CREATE OR REPLACE TRIGGER TASY.material_estab_atual
before insert or update ON TASY.MATERIAL_ESTAB for each row
declare

cd_material_estoque_w		number(6);
qt_itens_w			number(15,0);

qt_disp_estoque_w		number(15,4);
qt_estoque_w			number(15,4);
qt_estoque_lote_w		number(15,4);
qt_emprestimo_w			number(15,4);

qt_disp_estoque_normal_w	number(15,4);
qt_estoque_normal_w		number(15,4);
qt_estoque_lote_normal_w	number(15,4);

qt_disp_estoque_consig_w	number(15,4);
qt_estoque_consig_w		number(15,4);
qt_estoque_lote_consig_w	number(15,4);

qt_dia_interv_ressup_w		number(15,0);
qt_dia_ressup_forn_w		number(15,0);
qt_dia_estoque_minimo_w		number(15,0);

qt_reg_saldo_lote_normal_w	number(10);
qt_reg_saldo_lote_consig_w	number(10);
qt_reg_saldo_w			number(10);

ie_abc_w			varchar2(1);
ie_atualizar_w			varchar2(1);
ie_consignado_w			varchar2(1);
ie_obg_estoque_lote_w		varchar2(1);

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_registro_anvisa_w		material_estab.nr_registro_anvisa%type;
dt_validade_reg_anvisa_w	material_estab.dt_validade_reg_anvisa%type;
ie_parametro_17_w		varchar2(255) := 'N';

begin
if	((updating) and (:old.ie_estoque_lote <> :new.ie_estoque_lote)) or
	(inserting) and (:new.ie_estoque_lote = 'S') then
	begin
	qt_disp_estoque_consig_w	:= 0;
	qt_estoque_consig_w		:= 0;
	qt_estoque_lote_consig_w	:= 0;
	qt_disp_estoque_normal_w	:= 0;
	qt_estoque_normal_w		:= 0;
	qt_estoque_lote_normal_w	:= 0;

	select	ie_consignado,
		cd_material_estoque
	into	ie_consignado_w,
		cd_material_estoque_w
	from	material
	where	cd_material = :new.cd_material;

	select	sum(b.qt_material)
	into	qt_emprestimo_w
	from	emprestimo c,
		emprestimo_material b
	where	b.nr_emprestimo		= c.nr_emprestimo
	and	b.qt_material		> 0
	and	c.ie_situacao		<> 'I'
	and	c.cd_estabelecimento = :new.cd_estabelecimento
	and	exists (
		select	1
		from	material a
		where	a.cd_material_estoque	= cd_material_estoque_w
		and	a.cd_material 		= b.cd_material);

	if	(ie_consignado_w <> '1') then
		begin
		obter_saldo_estoque( :new.cd_estabelecimento, cd_material_estoque_w, null, PKG_DATE_UTILS.START_OF(sysdate,'month'), qt_estoque_normal_w);

		select	count(1),
			nvl(sum(a.qt_estoque), 0)
		into	qt_reg_saldo_lote_normal_w,
			qt_estoque_lote_normal_w
		from	saldo_estoque_lote a
		where	a.cd_estabelecimento = :new.cd_estabelecimento
		and	a.cd_material = cd_material_estoque_w
		and	a.dt_mesano_referencia = (
				select	max(x.dt_mesano_referencia)
				from	saldo_estoque_lote x
				where	x.cd_estabelecimento = a.cd_estabelecimento
				and	x.cd_local_estoque = a.cd_local_estoque
				and	x.cd_material = a.cd_material
				and	x.nr_seq_lote = a.nr_seq_lote)
		and	a.qt_estoque <> 0;
		end;
	end if;

	if	(ie_consignado_w <> '0') then
		begin
		qt_estoque_consig_w := obter_saldo_estoque_consig(:new.cd_estabelecimento, null, cd_material_estoque_w, null);

		select	count(1),
			nvl(sum(qt_estoque), 0)
		into	qt_reg_saldo_lote_consig_w,
			qt_estoque_lote_consig_w
		from	fornecedor_mat_consig_lote a
		where	a.cd_estabelecimento = :new.cd_estabelecimento
		and	a.cd_material = cd_material_estoque_w
		and	a.dt_mesano_referencia = (
				select	max(x.dt_mesano_referencia)
				from	fornecedor_mat_consig_lote x
				where	x.cd_estabelecimento = a.cd_estabelecimento
				and	x.cd_local_estoque = a.cd_local_estoque
				and	x.cd_material = a.cd_material
				and	x.cd_fornecedor = a.cd_fornecedor
				and	x.nr_seq_lote = a.nr_seq_lote)
		and	a.qt_estoque <> 0;
		end;
	end if;

	qt_reg_saldo_w		:=	nvl(qt_reg_saldo_lote_consig_w,0) + nvl(qt_reg_saldo_lote_normal_w,0);
	qt_disp_estoque_w	:=	nvl(qt_disp_estoque_normal_w,0) + nvl(qt_disp_estoque_consig_w,0) + qt_emprestimo_w;
	qt_estoque_w		:=	nvl(qt_estoque_normal_w,0) + nvl(qt_estoque_consig_w,0);
	qt_estoque_lote_w	:=	nvl(qt_estoque_lote_normal_w,0) + nvl(qt_estoque_lote_consig_w,0);

	if	(:new.ie_estoque_lote = 'S') then
		begin
		if	((qt_estoque_lote_w = 0) and ((qt_emprestimo_w <> 0 or qt_estoque_w <> 0))) then
			wheb_mensagem_pck.exibir_mensagem_abort(187664, 'QT_DISPONIVEL=' || to_char(qt_disp_estoque_w) || ';'||
									'QT_ESTOQUE=' || to_char(qt_estoque_w));
			/*r.aise_application_error(-20011,
					'Para iniciar controle de estoque por lote, deve ser zerado o estoque atual do material.' || chr(13) || chr(10) ||
					'Saldo dispon�vel: ' || qt_disp_estoque_w || chr(13) || chr(10) ||
					'Saldo em estoque: ' || qt_estoque_w);*/
		elsif	(qt_estoque_lote_w <> qt_estoque_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(187681, 'QT_SALDO_LOTE=' || to_char(qt_estoque_lote_w) || ';'||
									'QT_ESTOQUE=' || to_char(qt_estoque_w));
			/*r.aise_application_error(-20011,
					'Para iniciar o controle de estoque por lote, a soma dos saldos dos lotes deve ser igual ao saldo do material.' || chr(13) || chr(10) ||
					'Saldo lotes: ' || qt_estoque_lote_w || chr(13) || chr(10) ||
					'Saldo em estoque: ' || qt_estoque_w);*/
		end if;
		end;
	else
		begin
		if	((qt_estoque_lote_w <> 0) or (qt_estoque_w <> 0) or (qt_reg_saldo_w > 0)) then
			wheb_mensagem_pck.exibir_mensagem_abort(187684, 'QT_SALDO_LOTE=' || to_char(qt_estoque_lote_w) || ';'||
									'QT_ESTOQUE=' || to_char(qt_estoque_w));
			/*r.aise_application_error(-20011,
					'Para finalizar o controle de estoque por lote, o saldo do material e dos lotes devem estar zerados.' || chr(13) || chr(10) ||
					'Saldo lotes: ' || qt_estoque_lote_w || chr(13) || chr(10) ||
					'Saldo em estoque: ' || qt_estoque_w);*/
		end if;
		end;
	end if;

	ie_obg_estoque_lote_w := substr(OBTER_SE_ESTOQUE_LOTE_OBG(:new.cd_material, :new.cd_estabelecimento),1,1);

	if	(ie_obg_estoque_lote_w = 'S') and
		(:new.ie_estoque_lote = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(293727);
		--r.aise_application_error(-20011,'� obrigat�rio o controle de estoque por lote para materiais com essa estrutura!');
	end if;
	end;
end if;

if	(updating) then
	begin
	if	(:new.ie_curva_xyz is not null) and (:new.ie_curva_xyz not in ('X','Y','Z')) then
		wheb_mensagem_pck.exibir_mensagem_abort(187686);
		/*r.aise_application_error(-20011, 'A Classifica��o XYZ, n�o pode ter informa��o diferente de X,Y ou Z');*/
	end if;

	if	(:new.ie_curva_xyz is not null) and (:new.ie_curva_xyz <> :old.ie_curva_xyz) then
		ie_abc_w			:= null;
		man_obter_dias_ressup(	:new.cd_estabelecimento,
						:new.cd_material,
						ie_abc_w,
						:new.ie_curva_xyz,
						qt_dia_interv_ressup_w,
						qt_dia_ressup_forn_w,
						qt_dia_estoque_minimo_w,
						ie_atualizar_w);
		if	(ie_atualizar_w	= 'S') then
			:new.qt_dia_interv_ressup		:= qt_dia_interv_ressup_w;
			:new.qt_dia_ressup_forn		:= qt_dia_ressup_forn_w;
			:new.qt_dia_estoque_minimo		:= qt_dia_estoque_minimo_w;
		end if;
	end if;
	end;
elsif	(inserting) then
	begin
	ie_obg_estoque_lote_w := substr(OBTER_SE_ESTOQUE_LOTE_OBG(:new.cd_material, :new.cd_estabelecimento),1,1);

	if	(ie_obg_estoque_lote_w = 'S') and
		(:new.ie_estoque_lote = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(293727);
		--r.aise_application_error(-20011,'� obrigat�rio o controle de estoque por lote para materiais com essa estrutura!');
	end if;

	Obter_Param_Usuario(9900, 17, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_parametro_17_w);

	if	(nvl(ie_parametro_17_w,'N') = 'S') then
		select	max(nr_registro_anvisa),
			max(dt_validade_reg_anvisa)
		into	nr_registro_anvisa_w,
			dt_validade_reg_anvisa_w
		from	material
		where	cd_material	= :new.cd_material;

		:new.nr_registro_anvisa		:= nvl(:new.nr_registro_anvisa,nr_registro_anvisa_w);
		:new.dt_validade_reg_anvisa	:= nvl(:new.dt_validade_reg_anvisa,dt_validade_reg_anvisa_w);
	end if;

	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_estab_insert
before update or insert ON TASY.MATERIAL_ESTAB for each row
declare
ie_estoque_lote_w   material_estab.ie_estoque_lote%type;
cd_material_estoque_w   material.cd_material_estoque%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

    if (:new.ie_estoque_lote <> :old.ie_estoque_lote or :old.ie_estoque_lote is null) then

        begin
        select	cd_material_estoque
        into	cd_material_estoque_w
        from	material
        where	cd_material = :new.cd_material;
        exception
                when others then
            cd_material_estoque_w := '';
        end;
        if (cd_material_estoque_w <> :new.cd_material) then
            begin
            select ie_estoque_lote
            into ie_estoque_lote_w
            from material_estab
            where cd_material = cd_material_estoque_w
            and cd_estabelecimento = :new.cd_estabelecimento;
            exception
                    when others then
                ie_estoque_lote_w := 'N';
            end;
            if (ie_estoque_lote_w <> :new.ie_estoque_lote) then
                Wheb_mensagem_pck.exibir_mensagem_abort(1140301);
            end if;

        end if;

    end if;

end if;

end;
/


ALTER TABLE TASY.MATERIAL_ESTAB ADD (
  CONSTRAINT MATESTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          768K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT MATESTA_UK
 UNIQUE (CD_ESTABELECIMENTO, CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_ESTAB ADD (
  CONSTRAINT MATESTA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATESTA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATESTA_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_CONTA) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATESTA_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT MATESTA_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_VENDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATESTA_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATESTA_UNIMEDI_FK3 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_CONSUMO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATESTA_UNIMEDI_FK4 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_ESTOQUE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.MATERIAL_ESTAB TO NIVEL_1;

