ALTER TABLE TASY.CTB_ORC_CEN_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_ORC_CEN_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_ORC_CEN_REGRA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_CENARIO          NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_REGRA            NUMBER(15)            NOT NULL,
  CD_CENTRO_CUSTO         NUMBER(15),
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE),
  CD_CLASSIF_CONTA        VARCHAR2(40 BYTE),
  CD_CLASSIF_CENTRO       VARCHAR2(40 BYTE),
  DT_MES_ORIG             DATE,
  DT_MES_INIC             DATE,
  DT_MES_FIM              DATE,
  CD_CENTRO_ORIGEM        NUMBER(15),
  CD_CONTA_ORIGEM         VARCHAR2(20 BYTE),
  IE_SOBREPOR             VARCHAR2(1 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  NR_SEQ_MES_REF_ORIG     NUMBER(10),
  IE_REGRA_VALOR          VARCHAR2(15 BYTE)     NOT NULL,
  PR_APLICAR              NUMBER(15,4),
  VL_FIXO                 NUMBER(15,2),
  IE_STATUS_ORIGEM        VARCHAR2(2 BYTE),
  NR_SEQ_CRITERIO_RATEIO  NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBORCR_CENCUST_FK_I ON TASY.CTB_ORC_CEN_REGRA
(CD_CENTRO_CUSTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_CENCUST_FK2_I ON TASY.CTB_ORC_CEN_REGRA
(CD_CENTRO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CENCUST_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_CONCONT_FK_I ON TASY.CTB_ORC_CEN_REGRA
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_CONCONT_FK2_I ON TASY.CTB_ORC_CEN_REGRA
(CD_CONTA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_CRITRAT_FK_I ON TASY.CTB_ORC_CEN_REGRA
(NR_SEQ_CRITERIO_RATEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CRITRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_CTBMERE_FK_I ON TASY.CTB_ORC_CEN_REGRA
(NR_SEQ_MES_REF_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_CTBORCE_FK_I ON TASY.CTB_ORC_CEN_REGRA
(NR_SEQ_CENARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_CTBORCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_ESTABEL_FK_I ON TASY.CTB_ORC_CEN_REGRA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCR_I1 ON TASY.CTB_ORC_CEN_REGRA
(NR_SEQ_CENARIO, NR_SEQ_REGRA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_I1
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBORCR_PK ON TASY.CTB_ORC_CEN_REGRA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ctb_orc_cen_regra_insert
before insert or update ON TASY.CTB_ORC_CEN_REGRA for each row
declare

ie_conta_vigente_w		Varchar2(1);
ie_tipo_w			varchar2(1);
ie_situacao_w			varchar2(1);
ie_permite_w			varchar2(1);
ie_resultado_w		varchar2(1);

BEGIN
if	(:new.ie_regra_valor = 'VF') then
	begin
	:new.pr_aplicar			:= null;
	:new.nr_seq_mes_ref_orig		:= null;
	if	(:new.vl_fixo is null) then
		/*Para regra de valor fixo deve haver um valor informado, mesmo que seja zero!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266569);
	end if;
	if	(:new.nr_seq_mes_ref_orig is not null) then
		/*'Para valores fixos informar a data inicial e final e n�o o m�s de origem!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266570);
	end if;
	if	(:new.dt_mes_inic is null) or
		(:new.dt_mes_fim is null) then
		/*'Para valores fixos informar a data inicial e final!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266571);
	end if;
	if	(:new.cd_centro_custo is null) and
		(:new.nr_seq_criterio_rateio is null) then
		/*Para valores fixos deve-se informar um crit�rio de rateio ou um Centro de custo!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266572);
	end if;
	if	(:new.cd_conta_contabil is null) then
		/*Para valores fixos informar a conta contabil!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266573);
	end if;
	if	(:new.cd_centro_custo is not null) and
		(:new.nr_seq_criterio_rateio is not null) then
		/*Para valores fixos deve-se informar somente um crit�rio de rateio ou um Centro de custo!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266574);
	end if;
	end;
elsif	(:new.ie_regra_valor = 'PV') or
	(:new.ie_regra_valor = 'PAV') then
	begin
	if	(:new.dt_mes_inic is null) or
		(:new.dt_mes_fim is null) then
		/*Para este tipo de regra deve ser informado m�s inicial e final!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266575);
	end if;
	if	(:new.pr_aplicar is null) then
		/*Para este tipo de regra deve ser informado uma % APLICAR v�lida!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266576);
	end if;
	end;
end if;

if	(:new.ie_regra_valor = 'PV') then
	:new.vl_fixo			:= null;
	:new.nr_seq_mes_ref_orig	:= null;

	select	nvl(max(obter_valor_param_usuario(925, 30, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento)), 'N')
	into	ie_permite_w
	from	dual;
	/* Matheus OS 84429 28/02/2008*/
	if	(ie_permite_w = 'N') and
		(nvl(:new.cd_centro_origem,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(266577);
	end if;
elsif	(:new.ie_regra_valor = 'PAV') then
	:new.vl_fixo			:= null;
elsif	(:new.ie_regra_valor = 'CM') then
	:new.vl_fixo			:= null;
	:new.pr_aplicar			:= null;
	:new.nr_seq_mes_ref_orig	:= null;
end if;

/* Consist�ncia centro destino*/
if	(:new.cd_centro_custo is not null) then
	select	nvl(max(ie_tipo),'X'),
		nvl(max(ie_situacao),'I')
	into	ie_tipo_w,
		ie_situacao_w
	from	centro_custo
	where	cd_centro_custo = :new.cd_centro_custo;

	if	(ie_tipo_w <> 'A') then
		wheb_mensagem_pck.exibir_mensagem_abort(266580);
		/*S� pode ser informado centro de custo do tipo anal�tico!');*/
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266581);
		/*O centro de custo informado est� inativo!');*/
	end if;

end if;
/* Consist�ncia centro origem*/
if	(:new.cd_centro_origem is not null) then
	select	nvl(max(ie_tipo),'X'),
		nvl(max(ie_situacao),'I')
	into	ie_tipo_w,
		ie_situacao_w
	from	centro_custo
	where	cd_centro_custo = :new.cd_centro_origem;

	if	(ie_tipo_w <> 'A') then
		/*S� pode ser informado centro de custo Origem do tipo anal�tico!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266582);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266583);
		/*O centro de custo origem informado est� inativo!');*/
	end if;
end if;

if	(:new.cd_conta_contabil is not null) then
	select	nvl(max(a.ie_tipo),'X'),
		nvl(max(a.ie_situacao),'I'),
		max(b.ie_tipo)
	into	ie_tipo_w,
		ie_situacao_w,
		ie_resultado_w
	from	ctb_grupo_conta b,
		conta_contabil a
	where	a.cd_grupo		= b.cd_grupo
	and	a.cd_conta_contabil	= :new.cd_conta_contabil;

	if	(ie_tipo_w <> 'A') then
		/*S� pode ser informado Conta cont�bil anal�tica!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(266584);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266585);
		/*A conta cont�bil destino informada est� inativa!');*/
	end if;
	if	(ie_resultado_w not in ('R','C','D')) then
		wheb_mensagem_pck.exibir_mensagem_abort(266586);
		/*A conta cont�bil destino informada n�o � do tipo resultado!');*/
	end if;

end if;

/* Consistencia conta origem*/
if	(:new.cd_conta_origem is not null) then
	select	nvl(max(a.ie_tipo),'X'),
		nvl(max(a.ie_situacao),'I'),
		max(b.ie_tipo)
	into	ie_tipo_w,
		ie_situacao_w,
		ie_resultado_w
	from	ctb_grupo_conta b,
		conta_contabil a
	where	a.cd_grupo		= b.cd_grupo
	and	a.cd_conta_contabil	= :new.cd_conta_origem;

	if	(ie_tipo_w <> 'A') then
		wheb_mensagem_pck.exibir_mensagem_abort(266584);
	end if;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(266587);
	end if;
	if	(ie_resultado_w not in ('R','C','D')) then
		wheb_mensagem_pck.exibir_mensagem_abort(266588);
	end if;

end if;
END;
/


ALTER TABLE TASY.CTB_ORC_CEN_REGRA ADD (
  CONSTRAINT CTBORCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_ORC_CEN_REGRA ADD (
  CONSTRAINT CTBORCR_CTBORCE_FK 
 FOREIGN KEY (NR_SEQ_CENARIO) 
 REFERENCES TASY.CTB_ORC_CENARIO (NR_SEQUENCIA),
  CONSTRAINT CTBORCR_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBORCR_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CTBORCR_CENCUST_FK2 
 FOREIGN KEY (CD_CENTRO_ORIGEM) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CTBORCR_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_ORIGEM) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBORCR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBORCR_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF_ORIG) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBORCR_CRITRAT_FK 
 FOREIGN KEY (NR_SEQ_CRITERIO_RATEIO) 
 REFERENCES TASY.CTB_CRITERIO_RATEIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_ORC_CEN_REGRA TO NIVEL_1;

