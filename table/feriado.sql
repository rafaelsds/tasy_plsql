ALTER TABLE TASY.FERIADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FERIADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FERIADO
(
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_FERIADO           DATE                     NOT NULL,
  DS_MOTIVO_FERIADO    VARCHAR2(40 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_FERIADO      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FERIADO_ESTABEL_FK_I ON TASY.FERIADO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FERIADO_PK ON TASY.FERIADO
(CD_ESTABELECIMENTO, DT_FERIADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Feriado_Delete
before delete ON TASY.FERIADO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= trunc(sysdate)
and	dt_agenda between trunc(:old.dt_feriado) and trunc(:old.dt_feriado) + 86399 / 86400;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.Feriado_Atual
before insert or update ON TASY.FERIADO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from agenda_controle_horario
where	dt_agenda >= trunc(sysdate)
and	dt_agenda between trunc(:new.dt_feriado) and trunc(:new.dt_feriado) + 86399 / 86400;

:new.dt_feriado := trunc(:new.dt_feriado);

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.FERIADO ADD (
  CONSTRAINT FERIADO_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, DT_FERIADO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.FERIADO TO NIVEL_1;

