DROP TABLE TASY.W_REL_COSTO_CUENTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_REL_COSTO_CUENTA
(
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  NR_SEMANA          NUMBER(5),
  NR_ATENCION        NUMBER(10),
  NR_INTERNO_CUENTA  NUMBER(10),
  CD_ITEM            NUMBER(15),
  DS_ITEM            VARCHAR2(255 BYTE),
  DS_CLASE           VARCHAR2(255 BYTE),
  QT_ITEM            NUMBER(9,3),
  VL_ITEM            NUMBER(15,2),
  VL_DESCUENTO       NUMBER(15,4),
  VL_DEDUCIBLE       NUMBER(15,4),
  VL_IMPUESTO        NUMBER(15,4),
  DT_ITEM            DATE,
  VL_COSTO_MEDIO     NUMBER(15,4),
  CD_PACIENTE        VARCHAR2(10 BYTE),
  CD_CONVENIO        NUMBER(5),
  VL_ITEM_ORIGINAL   NUMBER(15,4),
  NR_SEQ_ITEM        NUMBER(10),
  IE_PROC_MAT        NUMBER(2),
  VL_PRECIO_TOTAL    NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_REL_COSTO_CUENTA TO NIVEL_1;

