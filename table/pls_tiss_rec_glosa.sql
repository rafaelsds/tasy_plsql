ALTER TABLE TASY.PLS_TISS_REC_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TISS_REC_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TISS_REC_GLOSA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_RECURSO_GLOSA     VARCHAR2(1 BYTE)         NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  NR_SEQ_VERSAO        NUMBER(10)               NOT NULL,
  DT_FIM_VIGENCIA_REF  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTRECG_PK ON TASY.PLS_TISS_REC_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTRECG_PLSVETI_FK_I ON TASY.PLS_TISS_REC_GLOSA
(NR_SEQ_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_tiss_rec_glosa_atual
before insert or update ON TASY.PLS_TISS_REC_GLOSA for each row
declare

begin
-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_tiss_rec_glosa_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_TISS_REC_GLOSA_tp  after update ON TASY.PLS_TISS_REC_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_TISS_REC_GLOSA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECURSO_GLOSA,1,4000),substr(:new.IE_RECURSO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECURSO_GLOSA',ie_log_w,ds_w,'PLS_TISS_REC_GLOSA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_TISS_REC_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TISS_REC_GLOSA ADD (
  CONSTRAINT PLSTRECG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TISS_REC_GLOSA ADD (
  CONSTRAINT PLSTRECG_PLSVETI_FK 
 FOREIGN KEY (NR_SEQ_VERSAO) 
 REFERENCES TASY.PLS_VERSAO_TISS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_TISS_REC_GLOSA TO NIVEL_1;

