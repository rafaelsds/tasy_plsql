ALTER TABLE TASY.ESCALA_STROKE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_STROKE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_STROKE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  QT_IDADE               NUMBER(3)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_MASCULINO           VARCHAR2(1 BYTE)       NOT NULL,
  IE_D                   VARCHAR2(1 BYTE)       NOT NULL,
  IE_AVC_ATI_PREVIO      VARCHAR2(1 BYTE)       NOT NULL,
  QT_PONTUACAO           NUMBER(5,1),
  IE_ANGINA              VARCHAR2(1 BYTE)       NOT NULL,
  IE_DIABETE             VARCHAR2(1 BYTE)       NOT NULL,
  IE_DEFICIT_FOCAL       VARCHAR2(1 BYTE)       NOT NULL,
  IE_CEFALEIA            VARCHAR2(1 BYTE)       NOT NULL,
  IE_VOMITO              VARCHAR2(1 BYTE)       NOT NULL,
  IE_PA_MAIOR            VARCHAR2(1 BYTE)       NOT NULL,
  IE_NAO_ALERTA          VARCHAR2(1 BYTE)       NOT NULL,
  IE_DEFICIT_ACORDAR     VARCHAR2(1 BYTE)       NOT NULL,
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSTRO_ATEPACI_FK_I ON TASY.ESCALA_STROKE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSTRO_PESFISI_FK_I ON TASY.ESCALA_STROKE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSTRO_PK ON TASY.ESCALA_STROKE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSTRO_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_STROKE_ATUAL
before insert or  update ON TASY.ESCALA_STROKE for each row
declare

qt_pontos_w		number(10,2) := 0;


begin

if	(:new.qt_idade > 65 ) then
	qt_pontos_w := qt_pontos_w + 1;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_MASCULINO = 'S') then
	qt_pontos_w  := qt_pontos_w - 1;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_AVC_ATI_PREVIO = 'S') then
	qt_pontos_w  := qt_pontos_w + 2;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_ANGINA = 'S') then
	qt_pontos_w  := qt_pontos_w + 2;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_DIABETE = 'S') then
	qt_pontos_w  := qt_pontos_w + 1.5;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_DEFICIT_FOCAL = 'S') then
	qt_pontos_w  := qt_pontos_w + 2.5;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_CEFALEIA = 'S') then
	qt_pontos_w  := qt_pontos_w - 2;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_VOMITO = 'S') then
	qt_pontos_w  := qt_pontos_w - 1.5;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_PA_MAIOR = 'S') then
	qt_pontos_w  := qt_pontos_w - 1.5;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;


if	(:new.IE_NAO_ALERTA = 'S') then
	qt_pontos_w  := qt_pontos_w - 3;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_DEFICIT_ACORDAR = 'S') then
	qt_pontos_w  := qt_pontos_w + 1;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;

if	(:new.IE_D = 'S') then
	qt_pontos_w  := qt_pontos_w - 1;
else
	qt_pontos_w := qt_pontos_w + 0;
end if;


 :new.QT_PONTUACAO := qt_pontos_w;

end;
/


ALTER TABLE TASY.ESCALA_STROKE ADD (
  CONSTRAINT ESCSTRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_STROKE ADD (
  CONSTRAINT ESCSTRO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSTRO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_STROKE TO NIVEL_1;

