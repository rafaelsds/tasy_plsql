ALTER TABLE TASY.REPASSE_TERC_VENC_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REPASSE_TERC_VENC_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.REPASSE_TERC_VENC_TRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REP_VENC      NUMBER(10)               NOT NULL,
  CD_TRIBUTO           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_IMPOSTO           DATE                     NOT NULL,
  VL_BASE_CALCULO      NUMBER(15,2)             NOT NULL,
  PR_TRIBUTO           NUMBER(7,4)              NOT NULL,
  VL_IMPOSTO           NUMBER(15,2)             NOT NULL,
  CD_CONTA_FINANC      NUMBER(10),
  NR_SEQ_TRANS_REG     NUMBER(10),
  NR_SEQ_TRANS_BAIXA   NUMBER(10),
  CD_BENEFICIARIO      VARCHAR2(14 BYTE),
  VL_NAO_RETIDO        NUMBER(15,2)             NOT NULL,
  VL_BASE_NAO_RETIDO   NUMBER(15,2)             NOT NULL,
  VL_TRIB_ADIC         NUMBER(15,2)             NOT NULL,
  VL_BASE_ADIC         NUMBER(15,2)             NOT NULL,
  IE_PAGO_PREV         VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VL_REDUCAO           NUMBER(15,2),
  VL_DESC_BASE         NUMBER(15,2),
  CD_DARF              VARCHAR2(10 BYTE),
  CD_VARIACAO          VARCHAR2(2 BYTE),
  IE_PERIODICIDADE     VARCHAR2(1 BYTE),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  CD_CGC_RETENCAO      VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPTEVT_CONFINA_FK_I ON TASY.REPASSE_TERC_VENC_TRIB
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEVT_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEVT_I1 ON TASY.REPASSE_TERC_VENC_TRIB
(NR_SEQ_REP_VENC, CD_TRIBUTO, IE_PAGO_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEVT_PESJURI_FK_I ON TASY.REPASSE_TERC_VENC_TRIB
(CD_BENEFICIARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEVT_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEVT_PESJURI_FK2_I ON TASY.REPASSE_TERC_VENC_TRIB
(CD_CGC_RETENCAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEVT_PESJURI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REPTEVT_PK ON TASY.REPASSE_TERC_VENC_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEVT_REPTEVE_FK_I ON TASY.REPASSE_TERC_VENC_TRIB
(NR_SEQ_REP_VENC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEVT_TRAFINA_FK_I ON TASY.REPASSE_TERC_VENC_TRIB
(NR_SEQ_TRANS_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEVT_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEVT_TRAFINA_FK2_I ON TASY.REPASSE_TERC_VENC_TRIB
(NR_SEQ_TRANS_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEVT_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEVT_TRIBUTO_FK_I ON TASY.REPASSE_TERC_VENC_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.repasse_terc_venc_trib_insert
BEFORE INSERT ON TASY.REPASSE_TERC_VENC_TRIB FOR EACH ROW
DECLARE
ds_tributo_w 	varchar2(255);

BEGIN

if	(:new.vl_imposto < 0) then
	select	ds_tributo
	into	ds_tributo_w
	from	tributo
	where	cd_tributo	= :new.cd_tributo;

	/* O valor do tributo #@DS_TRIBUTO n�o pode ser negativo!
	Vl_tributo = #@VL_TRIBUTO#@ */
	wheb_mensagem_pck.exibir_mensagem_abort(266955, 'DS_TRIBUTO=' || ds_tributo_w || ';VL_TRIBUTO=' || :new.vl_imposto);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.repasse_terc_venc_trib_delete
before delete ON TASY.REPASSE_TERC_VENC_TRIB for each row
declare
ds_benefic_w			varchar2(80);
dt_vencimento_w			date;
dt_emissao_w			date;
vl_vencimento_w			number(15,2);
ds_tributo_w			varchar2(40);
ds_log_w			varchar2(4000);
qt_registro_w			pls_integer;
begin


select	substr(max(obter_nome_terceiro(b.nr_seq_terceiro)),1,80),
	max(a.dt_vencimento),
	max(a.vl_vencimento)
into	ds_benefic_w,
	dt_vencimento_w,
	vl_vencimento_w
from	repasse_terceiro b,
	repasse_terceiro_venc a
where	a.nr_repasse_terceiro	= b.nr_repasse_terceiro
and	nr_sequencia		= :old.nr_seq_rep_venc;

select	max(ds_tributo)
into	ds_tributo_w
from	tributo
where	cd_tributo = :old.cd_tributo;

--ds_log_w	:= substr(	'Terceiro repasse: ' || ds_benefic_w || chr(13) ||
--				'Vencimento: ' || dt_vencimento_w || chr(13) ||
--				'Cria��o tributo: ' || to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
--				'Valor venc: ' || vl_vencimento_w || chr(13) ||
--				'Tributo: ' || ds_tributo_w || chr(13) ||
--				'Valor tributo ' || :old.vl_imposto,1,4000);

ds_log_w	:= substr(	wheb_mensagem_pck.get_texto(304660) || ds_benefic_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304662) || dt_vencimento_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304663) || to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
				wheb_mensagem_pck.get_texto(304664) || vl_vencimento_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304665) || ds_tributo_w || chr(13) ||
				wheb_mensagem_pck.get_texto(304667) || :old.vl_imposto,1,4000);

fin_gerar_log_controle_banco(100, ds_log_w, 'Tasy','N');

select	count(1)
into	qt_registro_w
from	pls_pp_base_acum_trib
where	nr_seq_vl_repasse = :old.nr_sequencia;

if	(qt_registro_w > 0) then
	-- Existem registros dependentes
	wheb_mensagem_pck.exibir_mensagem_abort(457570);
end if;

select	count(1)
into	qt_registro_w
from	pls_pp_lr_base_trib
where	nr_seq_vl_repasse = :old.nr_sequencia;

if	(qt_registro_w > 0) then
	-- Existem registros dependentes
	wheb_mensagem_pck.exibir_mensagem_abort(457570);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.repasse_terc_venc_trib_update
after update ON TASY.REPASSE_TERC_VENC_TRIB FOR EACH ROW
declare

begin

if	(:new.VL_IMPOSTO <> :old.VL_IMPOSTO) or
	(:new.VL_BASE_CALCULO <> :old.VL_BASE_CALCULO) or
	(:new.PR_TRIBUTO <> :old.PR_TRIBUTO) or
	(:new.VL_DESC_BASE <> :old.VL_DESC_BASE) or
	(:new.VL_NAO_RETIDO <> :old.VL_NAO_RETIDO) or
	(:new.VL_BASE_NAO_RETIDO <> :old.VL_BASE_NAO_RETIDO) or
	(:new.VL_TRIB_ADIC <> :old.VL_TRIB_ADIC) or
	(:new.VL_BASE_ADIC <> :old.VL_BASE_ADIC) or
	(:new.VL_REDUCAO <> :old.VL_REDUCAO) then
	null;
	/*insert into logxxxx_tasy
			(DT_ATUALIZACAO,
			NM_USUARIO,
			CD_LOG,
			DS_LOG)
	values		(sysdate,
			 'SQLPLUS',
			 55712,
			'Seq. trib: ' || :new.nr_sequencia || chr(13) ||
			'Tributo: ' || :new.CD_TRIBUTO || chr(13) ||
			':new.VL_IMPOSTO = ' || 	:new.VL_IMPOSTO 	|| ' :old.VL_IMPOSTO = ' 	|| :old.VL_IMPOSTO || chr(13) ||
			':new.VL_BASE_CALCULO = ' || 	:new.VL_BASE_CALCULO 	|| ' :old.VL_BASE_CALCULO = ' 	|| :old.VL_BASE_CALCULO || chr(13) ||
			':new.PR_TRIBUTO = ' || 	:new.PR_TRIBUTO		|| ' :old.PR_TRIBUTO = ' 	|| :old.PR_TRIBUTO || chr(13) ||
			':new.VL_DESC_BASE = ' || 	:new.VL_DESC_BASE 	|| ' :old.VL_DESC_BASE = ' 	|| :old.VL_DESC_BASE || chr(13) ||
			':new.VL_NAO_RETIDO = ' || :new.VL_NAO_RETIDO	|| ' :old.VL_NAO_RETIDO = '|| :old.VL_NAO_RETIDO || chr(13) ||
			':new.VL_BASE_NAO_RETIDO = ' || :new.VL_BASE_NAO_RETIDO	|| ' :old.VL_BASE_NAO_RETIDO = '|| :old.VL_BASE_NAO_RETIDO || chr(13) ||
			':new.VL_TRIB_ADIC = ' || 	:new.VL_TRIB_ADIC	|| ' :old.VL_TRIB_ADIC = ' 	|| :old.VL_TRIB_ADIC || chr(13) ||
			':new.VL_BASE_ADIC = ' ||	:new.VL_BASE_ADIC	|| ' :old.VL_BASE_ADIC = ' 	|| :old.VL_BASE_ADIC || chr(13) ||
			':new.VL_REDUCAO = ' ||		:new.VL_REDUCAO		|| ' :old.VL_REDUCAO = ' 	|| :old.VL_REDUCAO);*/

end if;

end;
/


ALTER TABLE TASY.REPASSE_TERC_VENC_TRIB ADD (
  CONSTRAINT REPTEVT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REPASSE_TERC_VENC_TRIB ADD (
  CONSTRAINT REPTEVT_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT REPTEVT_REPTEVE_FK 
 FOREIGN KEY (NR_SEQ_REP_VENC) 
 REFERENCES TASY.REPASSE_TERCEIRO_VENC (NR_SEQUENCIA),
  CONSTRAINT REPTEVT_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT REPTEVT_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT REPTEVT_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_REG) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT REPTEVT_PESJURI_FK 
 FOREIGN KEY (CD_BENEFICIARIO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT REPTEVT_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_RETENCAO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.REPASSE_TERC_VENC_TRIB TO NIVEL_1;

