ALTER TABLE TASY.PLS_AME_LOTE_REMESSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AME_LOTE_REMESSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AME_LOTE_REMESSA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_LOTE               DATE                    NOT NULL,
  DT_REF_MENSALIDADE    DATE                    NOT NULL,
  NR_SEQ_REGRA_GERACAO  NUMBER(10)              NOT NULL,
  DT_GERACAO_LOTE       DATE,
  DT_GERACAO_ARQUIVOS   DATE,
  DT_ENVIO              DATE,
  VL_TOTAL              NUMBER(15,2)            NOT NULL,
  IE_VISUALIZAR_PORTAL  VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_DESTINO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAMLR_PK ON TASY.PLS_AME_LOTE_REMESSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAMLR_PLSAMRG_FK_I ON TASY.PLS_AME_LOTE_REMESSA
(NR_SEQ_REGRA_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_AME_LOTE_REMESSA ADD (
  CONSTRAINT PLSAMLR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AME_LOTE_REMESSA ADD (
  CONSTRAINT PLSAMLR_PLSAMRG_FK 
 FOREIGN KEY (NR_SEQ_REGRA_GERACAO) 
 REFERENCES TASY.PLS_AME_REGRA_GERACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AME_LOTE_REMESSA TO NIVEL_1;

