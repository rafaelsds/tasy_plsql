ALTER TABLE TASY.EIS_PACIENTE_UTI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EIS_PACIENTE_UTI CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_PACIENTE_UTI
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_REFERENCIA         DATE                    NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_ATENDIMENTO        NUMBER(10)              NOT NULL,
  IE_SEXO               VARCHAR2(1 BYTE)        NOT NULL,
  QT_IDADE              NUMBER(3)               NOT NULL,
  QT_OBITO              NUMBER(1)               NOT NULL,
  QT_CIRURGIA_ELETIVA   NUMBER(3)               NOT NULL,
  QT_CIRURGIA_URGENCIA  NUMBER(3)               NOT NULL,
  DT_ENTRADA            DATE                    NOT NULL,
  DT_SAIDA              DATE                    NOT NULL,
  QT_MIN_PERM           NUMBER(7)               NOT NULL,
  QT_APACHE             NUMBER(7,3),
  PR_RISCO              NUMBER(7,3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISPAUT_ATEPACI_FK_I ON TASY.EIS_PACIENTE_UTI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EISPAUT_I1 ON TASY.EIS_PACIENTE_UTI
(DT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EISPAUT_PK ON TASY.EIS_PACIENTE_UTI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISPAUT_PK
  MONITORING USAGE;


CREATE INDEX TASY.EISPAUT_SETATEN_FK_I ON TASY.EIS_PACIENTE_UTI
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISPAUT_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.EIS_PACIENTE_UTI ADD (
  CONSTRAINT EISPAUT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EIS_PACIENTE_UTI ADD (
  CONSTRAINT EISPAUT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT EISPAUT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.EIS_PACIENTE_UTI TO NIVEL_1;

