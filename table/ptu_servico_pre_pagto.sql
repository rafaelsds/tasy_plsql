ALTER TABLE TASY.PTU_SERVICO_PRE_PAGTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_SERVICO_PRE_PAGTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_SERVICO_PRE_PAGTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_UNIMED_DESTINO        VARCHAR2(4 BYTE)     NOT NULL,
  CD_UNIMED_ORIGEM         VARCHAR2(4 BYTE)     NOT NULL,
  DT_GERACAO               DATE                 NOT NULL,
  DT_INICIO_PAGTO          DATE                 NOT NULL,
  DT_FIM_PAGTO             DATE                 NOT NULL,
  NR_VERSAO_TRANSACAO      NUMBER(2)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_GERACAO           NUMBER(5),
  IE_ENVIO_RECEBIMENTO     VARCHAR2(2 BYTE),
  NR_SEQ_LOTE              NUMBER(10),
  NM_ARQUIVO               VARCHAR2(255 BYTE),
  DT_IMPORTACAO_ARQUIVO    DATE,
  NM_USUARIO_IMPORTACAO    VARCHAR2(15 BYTE),
  DT_GERACAO_ARQUIVO       DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE,
  NR_SEQ_MOTIVO_CANCEL     NUMBER(10),
  DS_JUSTIFICATIVA         VARCHAR2(4000 BYTE),
  IE_STATUS                VARCHAR2(255 BYTE),
  DS_HASH                  VARCHAR2(255 BYTE),
  DS_SERIAL_PROCESSO       VARCHAR2(255 BYTE),
  DS_SID_PROCESSO          VARCHAR2(255 BYTE),
  VL_TOT_SERV              NUMBER(15,2),
  VL_TOT_CO                NUMBER(15,2),
  VL_TOT_FILME             NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTUSEPP_ESTABEL_FK_I ON TASY.PTU_SERVICO_PRE_PAGTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUSEPP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PTUSEPP_PK ON TASY.PTU_SERVICO_PRE_PAGTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSEPP_PLSMOCC_FK_I ON TASY.PTU_SERVICO_PRE_PAGTO
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSEPP_PTULSPP_FK_I ON TASY.PTU_SERVICO_PRE_PAGTO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUSEPP_PTULSPP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_SERVICO_PRE_PAGTO ADD (
  CONSTRAINT PTUSEPP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_SERVICO_PRE_PAGTO ADD (
  CONSTRAINT PTUSEPP_PTULSPP_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PTU_LOTE_SERV_PRE_PGTO (NR_SEQUENCIA),
  CONSTRAINT PTUSEPP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PTUSEPP_PLSMOCC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.PLS_MOTIVO_CANCEL_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_SERVICO_PRE_PAGTO TO NIVEL_1;

