ALTER TABLE TASY.CONTA_CONTABIL_CLASSIF_ECD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_CONTABIL_CLASSIF_ECD CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_CONTABIL_CLASSIF_ECD
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE)        NOT NULL,
  CD_CLASSIF_ECD       VARCHAR2(40 BYTE)        NOT NULL,
  CD_CENTRO_CUSTO      NUMBER(8),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  CD_VERSAO            VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONCLECD_CENCUST_FK_I ON TASY.CONTA_CONTABIL_CLASSIF_ECD
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCLECD_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONCLECD_CONCONT_FK_I ON TASY.CONTA_CONTABIL_CLASSIF_ECD
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCLECD_CONCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONCLECD_PK ON TASY.CONTA_CONTABIL_CLASSIF_ECD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONCLECD_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.conta_contabil_classif_ecd_del
	before delete ON TASY.CONTA_CONTABIL_CLASSIF_ECD 	for each row
declare
	PRAGMA AUTONOMOUS_TRANSACTION;
	-- local variables here
	cd_conta_ref_w        conta_contabil.cd_conta_contabil%type;

	cursor c_conta_referencia(cd_conta_ref_p in conta_contabil.cd_conta_contabil%type) is
	select  a.cd_conta_contabil
	from  conta_contabil a
	where  a.cd_conta_referencia = cd_conta_ref_p
	order by 1;

begin
open c_conta_referencia(:old.cd_conta_Contabil);
loop
fetch c_conta_referencia into  cd_conta_ref_w;
exit when c_conta_referencia%notfound;
	begin

	delete  conta_contabil_classif_ecd
	where  cd_classif_ecd = :old.cd_classif_ecd
	and  cd_conta_contabil = cd_conta_ref_w;

	end;
end loop;
close c_conta_referencia;

commit;

end conta_contabil_classif_ecd_del;
/


ALTER TABLE TASY.CONTA_CONTABIL_CLASSIF_ECD ADD (
  CONSTRAINT CONCLECD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_CONTABIL_CLASSIF_ECD ADD (
  CONSTRAINT CONCLECD_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CONCLECD_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO));

GRANT SELECT ON TASY.CONTA_CONTABIL_CLASSIF_ECD TO NIVEL_1;

