ALTER TABLE TASY.SAN_TEST_ESP_GER_COD_ISBT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_TEST_ESP_GER_COD_ISBT CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_TEST_ESP_GER_COD_ISBT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_ISBT                VARCHAR2(5 BYTE)       NOT NULL,
  DS_INTERPRETACAO       VARCHAR2(200 BYTE)     NOT NULL,
  DS_DEFINICAO           VARCHAR2(255 BYTE),
  NR_SEQ_VERSAO_TEST     NUMBER(10)             NOT NULL,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANTEGC_PK ON TASY.SAN_TEST_ESP_GER_COD_ISBT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANTEGC_SANTEGV_FK_I ON TASY.SAN_TEST_ESP_GER_COD_ISBT
(NR_SEQ_VERSAO_TEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_TEST_ESP_GER_COD_ISBT ADD (
  CONSTRAINT SANTEGC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.SAN_TEST_ESP_GER_COD_ISBT ADD (
  CONSTRAINT SANTEGC_SANTEGV_FK 
 FOREIGN KEY (NR_SEQ_VERSAO_TEST) 
 REFERENCES TASY.SAN_TEST_ESP_GER_VER_ISBT (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_TEST_ESP_GER_COD_ISBT TO NIVEL_1;

