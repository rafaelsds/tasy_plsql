ALTER TABLE TASY.CONTRATO_USUARIO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTRATO_USUARIO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTRATO_USUARIO_LIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONTRATO      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  IE_AVISO_VENCIMENTO  VARCHAR2(1 BYTE)         NOT NULL,
  CD_PERFIL            NUMBER(5),
  CD_SETOR             NUMBER(5),
  IE_PERMITE           VARCHAR2(1 BYTE),
  IE_AVISO_REVISAO     VARCHAR2(1 BYTE),
  IE_GERACAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONTULI_CONTRAT_FK_I ON TASY.CONTRATO_USUARIO_LIB
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTULI_PERFIL_FK_I ON TASY.CONTRATO_USUARIO_LIB
(CD_PERFIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTULI_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONTULI_PK ON TASY.CONTRATO_USUARIO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTULI_SETATEN_FK_I ON TASY.CONTRATO_USUARIO_LIB
(CD_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTULI_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.OnUpdate_ContratoUserLib
AFTER UPDATE ON TASY.CONTRATO_USUARIO_LIB FOR EACH ROW
DECLARE

nr_sequencia_w        contrato_historico.nr_sequencia%type;
ds_historico_w        contrato_historico.ds_historico%type;
ds_titulo_w           contrato_historico.ds_titulo%type;
nm_usuario_w          contrato_historico.nm_usuario%type := :new.nm_usuario;
nr_seq_contrato_w     contrato_historico.nr_seq_contrato%type;
nm_usuario_comp_old_w varchar2(60);
nm_usuario_comp_w     varchar2(60);
ds_setor_old_w        varchar2(60);
ds_setor_w            varchar2(60);
ds_perfil_old_w       varchar2(60);
ds_perfil_w           varchar2(60);

BEGIN

if (:new.cd_perfil <> :old.cd_perfil) then
    begin

       ds_perfil_old_w := substr(obter_desc_perfil(:old.cd_perfil),1,255);
       ds_perfil_w := substr(obter_desc_perfil(:new.cd_perfil),1,255);

       select contrato_historico_seq.nextval
       into nr_sequencia_w
       from dual;

       ds_titulo_w := wheb_mensagem_pck.get_texto(1095623);
       ds_historico_w := substr(wheb_mensagem_pck.get_texto(1095449) || ' ' || ds_perfil_old_w || chr(10) || chr(13)
                        || wheb_mensagem_pck.get_texto(1095450) || ' ' || ds_perfil_w,1,1200);
       nr_seq_contrato_w    := :new.nr_seq_contrato;
       insert into contrato_historico(nr_sequencia,
                                      nr_seq_contrato,
                                      dt_historico,
                                      ds_historico,
                                      dt_atualizacao,
                                      nm_usuario,
                                      ds_titulo,
                                      dt_liberacao,
                                      nm_usuario_lib,
                                      ie_efetivado)
       values(nr_sequencia_w,
              nr_seq_contrato_w,
              sysdate,
              ds_historico_w,
              sysdate,
              nm_usuario_w,
              ds_titulo_w,
              sysdate,
              nm_usuario_w,
             'N');
    end;
end if;
if (:new.cd_setor <> :old.cd_setor) then
    begin

       ds_setor_old_w := substr(obter_ds_descricao_setor(:old.cd_setor),1,255);
       ds_setor_w := substr(obter_ds_descricao_setor(:new.cd_setor),1,255);

       select contrato_historico_seq.nextval
       into nr_sequencia_w
       from dual;

       ds_titulo_w := wheb_mensagem_pck.get_texto(1095622);
       ds_historico_w := substr(wheb_mensagem_pck.get_texto(1095449) || ' ' || ds_setor_old_w || chr(10) || chr(13)
                        || wheb_mensagem_pck.get_texto(1095450) || ' ' || ds_setor_w,1,1200);
       nr_seq_contrato_w    := :new.nr_seq_contrato;
       insert into contrato_historico(nr_sequencia,
                                      nr_seq_contrato,
                                      dt_historico,
                                      ds_historico,
                                      dt_atualizacao,
                                      nm_usuario,
                                      ds_titulo,
                                      dt_liberacao,
                                      nm_usuario_lib,
                                      ie_efetivado)
       values(nr_sequencia_w,
              nr_seq_contrato_w,
              sysdate,
              ds_historico_w,
              sysdate,
              nm_usuario_w,
              ds_titulo_w,
              sysdate,
              nm_usuario_w,
             'N');
    end;
end if;
if (:new.nm_usuario_lib <> :old.nm_usuario_lib) then
    begin

       nm_usuario_comp_old_w := substr(obter_nome_usuario(:old.nm_usuario_lib),1,100);
       nm_usuario_comp_w :=  substr(obter_nome_usuario(:new.nm_usuario_lib),1,100);

       select contrato_historico_seq.nextval
       into nr_sequencia_w
       from dual;

       ds_titulo_w := wheb_mensagem_pck.get_texto(1095621);
       ds_historico_w := substr(wheb_mensagem_pck.get_texto(1095449) || ' ' || :old.nm_usuario_lib || ' - ' || nm_usuario_comp_old_w || chr(10) || chr(13)
                        || wheb_mensagem_pck.get_texto(1095450) || ' ' || :new.nm_usuario_lib || ' - ' || nm_usuario_comp_w,1,1200);
       nr_seq_contrato_w    := :new.nr_seq_contrato;
       insert into contrato_historico(nr_sequencia,
                                      nr_seq_contrato,
                                      dt_historico,
                                      ds_historico,
                                      dt_atualizacao,
                                      nm_usuario,
                                      ds_titulo,
                                      dt_liberacao,
                                      nm_usuario_lib,
                                      ie_efetivado)
       values(nr_sequencia_w,
              nr_seq_contrato_w,
              sysdate,
              ds_historico_w,
              sysdate,
              nm_usuario_w,
              ds_titulo_w,
              sysdate,
              nm_usuario_w,
             'N');
    end;
end if;

END;
/


ALTER TABLE TASY.CONTRATO_USUARIO_LIB ADD (
  CONSTRAINT CONTULI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTRATO_USUARIO_LIB ADD (
  CONSTRAINT CONTULI_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONTULI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CONTULI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CONTRATO_USUARIO_LIB TO NIVEL_1;

