ALTER TABLE TASY.DIS_ENTRADA_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIS_ENTRADA_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIS_ENTRADA_PACIENTE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NM_PESSOA_FISICA        VARCHAR2(60 BYTE),
  NR_ATENDIMENTO          NUMBER(10),
  DS_SETOR_ATENDIMENTO    VARCHAR2(100 BYTE),
  DS_UNIDADE_ATENDIMENTO  VARCHAR2(100 BYTE),
  DT_ENTRADA              DATE,
  IE_SEXO                 VARCHAR2(1 BYTE),
  DT_NASCIMENTO           DATE,
  IE_STATUS               VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DIENPAC_PK ON TASY.DIS_ENTRADA_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIENPAC_PK
  MONITORING USAGE;


ALTER TABLE TASY.DIS_ENTRADA_PACIENTE ADD (
  CONSTRAINT DIENPAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.DIS_ENTRADA_PACIENTE TO NIVEL_1;

