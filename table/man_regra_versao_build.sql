ALTER TABLE TASY.MAN_REGRA_VERSAO_BUILD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_REGRA_VERSAO_BUILD CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_REGRA_VERSAO_BUILD
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_REGRA             VARCHAR2(3 BYTE)         NOT NULL,
  CD_VERSAO            VARCHAR2(15 BYTE),
  QT_VERSOES           NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MANREVB_PK ON TASY.MAN_REGRA_VERSAO_BUILD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_regra_versao_build_before
before insert or update ON TASY.MAN_REGRA_VERSAO_BUILD for each row
declare

begin

if (:new.ie_regra  = 'F' and :new.cd_versao is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1017739, null);
end if;

if (:new.ie_regra  = 'V' and :new.qt_versoes is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1017739, null);
end if;

end;
/


ALTER TABLE TASY.MAN_REGRA_VERSAO_BUILD ADD (
  CONSTRAINT MANREVB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MAN_REGRA_VERSAO_BUILD TO NIVEL_1;

