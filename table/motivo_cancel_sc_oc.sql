ALTER TABLE TASY.MOTIVO_CANCEL_SC_OC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_CANCEL_SC_OC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_CANCEL_SC_OC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_MOTIVO                  VARCHAR2(80 BYTE)  NOT NULL,
  CD_EMPRESA                 NUMBER(4)          NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_MOSTRA_INTERNET         VARCHAR2(1 BYTE)   NOT NULL,
  IE_NOTA_FISCAL             VARCHAR2(1 BYTE)   NOT NULL,
  IE_ORDEM_COMPRA            VARCHAR2(1 BYTE)   NOT NULL,
  IE_SOLIC_COMPRA            VARCHAR2(1 BYTE)   NOT NULL,
  IE_COTACAO                 VARCHAR2(1 BYTE)   NOT NULL,
  IE_EXIGE_DESCRICAO_MOTIVO  VARCHAR2(15 BYTE)  NOT NULL,
  IE_NF_SAIDA                VARCHAR2(1 BYTE)   NOT NULL,
  IE_TRANSF_ESTAB            VARCHAR2(1 BYTE),
  IE_REG_LIC_COMPRA          VARCHAR2(1 BYTE)   NOT NULL,
  IE_NF_PROTOCOLO            VARCHAR2(1 BYTE)   NOT NULL,
  IE_TIPO_ESTORNO            VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOTCASO_EMPRESA_FK_I ON TASY.MOTIVO_CANCEL_SC_OC
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MOTCASO_PK ON TASY.MOTIVO_CANCEL_SC_OC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MOTIVO_CANCEL_SC_OC_tp  after update ON TASY.MOTIVO_CANCEL_SC_OC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_NOTA_FISCAL,1,4000),substr(:new.IE_NOTA_FISCAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOTA_FISCAL',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MOTIVO,1,4000),substr(:new.DS_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOSTRA_INTERNET,1,4000),substr(:new.IE_MOSTRA_INTERNET,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOSTRA_INTERNET',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDEM_COMPRA,1,4000),substr(:new.IE_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDEM_COMPRA',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NF_PROTOCOLO,1,4000),substr(:new.IE_NF_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NF_PROTOCOLO',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_DESCRICAO_MOTIVO,1,4000),substr(:new.IE_EXIGE_DESCRICAO_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_DESCRICAO_MOTIVO',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COTACAO,1,4000),substr(:new.IE_COTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COTACAO',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NF_SAIDA,1,4000),substr(:new.IE_NF_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_NF_SAIDA',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TRANSF_ESTAB,1,4000),substr(:new.IE_TRANSF_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'IE_TRANSF_ESTAB',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REG_LIC_COMPRA,1,4000),substr(:new.IE_REG_LIC_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REG_LIC_COMPRA',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLIC_COMPRA,1,4000),substr(:new.IE_SOLIC_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLIC_COMPRA',ie_log_w,ds_w,'MOTIVO_CANCEL_SC_OC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MOTIVO_CANCEL_SC_OC ADD (
  CONSTRAINT MOTCASO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOTIVO_CANCEL_SC_OC ADD (
  CONSTRAINT MOTCASO_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.MOTIVO_CANCEL_SC_OC TO NIVEL_1;

