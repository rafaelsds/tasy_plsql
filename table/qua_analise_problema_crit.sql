ALTER TABLE TASY.QUA_ANALISE_PROBLEMA_CRIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_ANALISE_PROBLEMA_CRIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_ANALISE_PROBLEMA_CRIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ANALISE       NUMBER(10)               NOT NULL,
  NR_SEQ_CRIT_VALOR    NUMBER(10)               NOT NULL,
  QT_VALOR             NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QUAAPCR_PK ON TASY.QUA_ANALISE_PROBLEMA_CRIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAPCR_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUAAPCR_QUAAPRO_FK_I ON TASY.QUA_ANALISE_PROBLEMA_CRIT
(NR_SEQ_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAPCR_QUAAPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAAPCR_QUACRPR_FK_I ON TASY.QUA_ANALISE_PROBLEMA_CRIT
(NR_SEQ_CRIT_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAAPCR_QUACRPR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUA_ANALISE_PROBLEMA_CRIT ADD (
  CONSTRAINT QUAAPCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_ANALISE_PROBLEMA_CRIT ADD (
  CONSTRAINT QUAAPCR_QUAAPRO_FK 
 FOREIGN KEY (NR_SEQ_ANALISE) 
 REFERENCES TASY.QUA_ANALISE_PROBLEMA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT QUAAPCR_QUACRPR_FK 
 FOREIGN KEY (NR_SEQ_CRIT_VALOR) 
 REFERENCES TASY.QUA_CRIT_PROBLEMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_ANALISE_PROBLEMA_CRIT TO NIVEL_1;

