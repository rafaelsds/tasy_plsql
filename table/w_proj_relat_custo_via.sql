DROP TABLE TASY.W_PROJ_RELAT_CUSTO_VIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PROJ_RELAT_CUSTO_VIA
(
  NR_SEQ_VIAGEM    NUMBER(10),
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  CD_SETOR         NUMBER(5),
  IE_RESP_CUSTO    VARCHAR2(1 BYTE),
  NR_CLASSIF       NUMBER(10),
  NR_SEQ_PROJ      NUMBER(10),
  IE_TIPO_RECURSO  NUMBER(1),
  IE_TIPO_DESPESA  VARCHAR2(1 BYTE),
  VL_MES01         NUMBER(15,2),
  VL_MES02         NUMBER(15,2),
  VL_MES03         NUMBER(15,2),
  VL_MES04         NUMBER(15,2),
  VL_MES05         NUMBER(15,2),
  VL_MES06         NUMBER(15,2),
  VL_MES07         NUMBER(15,2),
  VL_MES08         NUMBER(15,2),
  VL_MES09         NUMBER(15,2),
  VL_MES10         NUMBER(15,2),
  VL_MES11         NUMBER(15,2),
  VL_MES12         NUMBER(15,2),
  VL_TOTAL         NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_PROJ_RELAT_CUSTO_VIA TO NIVEL_1;

