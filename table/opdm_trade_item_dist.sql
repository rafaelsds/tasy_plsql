ALTER TABLE TASY.OPDM_TRADE_ITEM_DIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPDM_TRADE_ITEM_DIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPDM_TRADE_ITEM_DIST
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DS_DISTRIBUTION_COUNTRY  VARCHAR2(200 BYTE),
  NR_SEQ_OPDM_TASY         NUMBER(10),
  NR_SEQ_TRADE_ITEM        NUMBER(10),
  IE_COUNTRY_DIST_STATUS   VARCHAR2(1 BYTE),
  DT_START_DISTRIBUTION    DATE,
  DT_END_DISTRIBUTION      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OPDMTRITEM_OPDMTASY_FK_I ON TASY.OPDM_TRADE_ITEM_DIST
(NR_SEQ_OPDM_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OPDMTRITEM_OPDMTRAIT_FK_I ON TASY.OPDM_TRADE_ITEM_DIST
(NR_SEQ_TRADE_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OPDMTRITEM_PK ON TASY.OPDM_TRADE_ITEM_DIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPDM_TRADE_ITEM_DIST_tp  after update ON TASY.OPDM_TRADE_ITEM_DIST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPDM_TASY,1,4000),substr(:new.NR_SEQ_OPDM_TASY,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPDM_TASY',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRADE_ITEM,1,4000),substr(:new.NR_SEQ_TRADE_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRADE_ITEM',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_END_DISTRIBUTION,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_END_DISTRIBUTION,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_END_DISTRIBUTION',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COUNTRY_DIST_STATUS,1,4000),substr(:new.IE_COUNTRY_DIST_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_COUNTRY_DIST_STATUS',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_START_DISTRIBUTION,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_START_DISTRIBUTION,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_START_DISTRIBUTION',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DISTRIBUTION_COUNTRY,1,4000),substr(:new.DS_DISTRIBUTION_COUNTRY,1,4000),:new.nm_usuario,nr_seq_w,'DS_DISTRIBUTION_COUNTRY',ie_log_w,ds_w,'OPDM_TRADE_ITEM_DIST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.OPDM_TRADE_ITEM_DIST ADD (
  CONSTRAINT OPDMTRITEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OPDM_TRADE_ITEM_DIST ADD (
  CONSTRAINT OPDMTRITEM_OPDMTASY_FK 
 FOREIGN KEY (NR_SEQ_OPDM_TASY) 
 REFERENCES TASY.OPDM_TASY (NR_SEQUENCIA),
  CONSTRAINT OPDMTRITEM_OPDMTRAIT_FK 
 FOREIGN KEY (NR_SEQ_TRADE_ITEM) 
 REFERENCES TASY.OPDM_TRADE_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.OPDM_TRADE_ITEM_DIST TO NIVEL_1;

