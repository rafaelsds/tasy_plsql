ALTER TABLE TASY.SAN_EXAME_ANTICORPOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_EXAME_ANTICORPOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_EXAME_ANTICORPOS
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_ANTICORPO          NUMBER(10),
  NR_SEQ_EXAME              NUMBER(10)          NOT NULL,
  IE_RESULTADO              VARCHAR2(1 BYTE),
  NR_SEQ_EXAME_LOTE         NUMBER(10),
  NR_SEQ_METODOLOGIA        VARCHAR2(1 BYTE),
  IE_ANTI_NAO_IDENTIFICADO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANEXAN_PK ON TASY.SAN_EXAME_ANTICORPOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEXAN_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANEXAN_SANANCO_FK_I ON TASY.SAN_EXAME_ANTICORPOS
(NR_SEQ_ANTICORPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEXAN_SANANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANEXAN_SANEXRE_FK_I ON TASY.SAN_EXAME_ANTICORPOS
(NR_SEQ_EXAME_LOTE, NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEXAN_SANEXRE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_EXAME_ANTICORPOS ADD (
  CONSTRAINT SANEXAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_EXAME_ANTICORPOS ADD (
  CONSTRAINT SANEXAN_SANANCO_FK 
 FOREIGN KEY (NR_SEQ_ANTICORPO) 
 REFERENCES TASY.SAN_ANTICORPOS (NR_SEQUENCIA),
  CONSTRAINT SANEXAN_SANEXRE_FK 
 FOREIGN KEY (NR_SEQ_EXAME_LOTE, NR_SEQ_EXAME) 
 REFERENCES TASY.SAN_EXAME_REALIZADO (NR_SEQ_EXAME_LOTE,NR_SEQ_EXAME));

GRANT SELECT ON TASY.SAN_EXAME_ANTICORPOS TO NIVEL_1;

