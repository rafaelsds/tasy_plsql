ALTER TABLE TASY.IND_LEG_CATEGORIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IND_LEG_CATEGORIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.IND_LEG_CATEGORIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LEGENDA       NUMBER(10)               NOT NULL,
  NR_SEQ_DIMENSAO      NUMBER(10)               NOT NULL,
  NR_SEQ_DATA          NUMBER(10),
  QT_PERIODO           NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INDLEGCAT_INDDATA_FK_I ON TASY.IND_LEG_CATEGORIA
(NR_SEQ_DATA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INDLEGCAT_INDDIM_FK_I ON TASY.IND_LEG_CATEGORIA
(NR_SEQ_DIMENSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INDLEGCAT_INDLEG_FK_I ON TASY.IND_LEG_CATEGORIA
(NR_SEQ_LEGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INDLEGCAT_PK ON TASY.IND_LEG_CATEGORIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IND_LEG_CATEGORIA ADD (
  CONSTRAINT INDLEGCAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.IND_LEG_CATEGORIA ADD (
  CONSTRAINT INDLEGCAT_INDDATA_FK 
 FOREIGN KEY (NR_SEQ_DATA) 
 REFERENCES TASY.IND_DATA (NR_SEQUENCIA),
  CONSTRAINT INDLEGCAT_INDDIM_FK 
 FOREIGN KEY (NR_SEQ_DIMENSAO) 
 REFERENCES TASY.IND_DIMENSAO (NR_SEQUENCIA),
  CONSTRAINT INDLEGCAT_INDLEG_FK 
 FOREIGN KEY (NR_SEQ_LEGENDA) 
 REFERENCES TASY.IND_LEGENDA (NR_SEQUENCIA));

GRANT SELECT ON TASY.IND_LEG_CATEGORIA TO NIVEL_1;

