ALTER TABLE TASY.HFP_TESTE_CAM_MIN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HFP_TESTE_CAM_MIN CASCADE CONSTRAINTS;

CREATE TABLE TASY.HFP_TESTE_CAM_MIN
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_MIN               NUMBER(5)                NOT NULL,
  QT_FREQ_CARDIACA     NUMBER(3),
  QT_SATURACAO_O2      NUMBER(3),
  QT_DISTANCIA         NUMBER(5)                NOT NULL,
  QT_BORG              NUMBER(3),
  QT_PA_DIASTOLICA     NUMBER(3),
  QT_PA_SISTOLICA      NUMBER(3),
  NR_SEQ_CAMINHADA     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HFPTECM_HFPTECA_FK_I ON TASY.HFP_TESTE_CAM_MIN
(NR_SEQ_CAMINHADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HFPTECM_HFPTECA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HFPTECM_PK ON TASY.HFP_TESTE_CAM_MIN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HFPTECM_PK
  MONITORING USAGE;


ALTER TABLE TASY.HFP_TESTE_CAM_MIN ADD (
  CONSTRAINT HFPTECM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HFP_TESTE_CAM_MIN ADD (
  CONSTRAINT HFPTECM_HFPTECA_FK 
 FOREIGN KEY (NR_SEQ_CAMINHADA) 
 REFERENCES TASY.HFP_TESTE_CAMINHADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.HFP_TESTE_CAM_MIN TO NIVEL_1;

