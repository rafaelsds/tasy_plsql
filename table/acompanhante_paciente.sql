ALTER TABLE TASY.ACOMPANHANTE_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ACOMPANHANTE_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ACOMPANHANTE_PACIENTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_SAIDA_REAL        DATE,
  DT_ENTRADA_REAL      DATE,
  DT_LIBERACAO_ACESSO  DATE,
  NR_IDENTIDADE        VARCHAR2(15 BYTE),
  NR_CONTROLE_ACESSO   NUMBER(10),
  NR_SEQ_PACIENTE      NUMBER(10)               NOT NULL,
  NM_VISITANTE         VARCHAR2(40 BYTE),
  NR_CONTROLE          VARCHAR2(20 BYTE),
  NR_SEQ_TIPO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ACOMPAC_ATEVISI_FK_I ON TASY.ACOMPANHANTE_PACIENTE
(NR_SEQ_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ACOMPAC_ATEVISI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ACOMPAC_PESFISI_FK_I ON TASY.ACOMPANHANTE_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ACOMPAC_PK ON TASY.ACOMPANHANTE_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ACOMPAC_PK
  MONITORING USAGE;


CREATE INDEX TASY.ACOMPAC_VISITAN_FK_I ON TASY.ACOMPANHANTE_PACIENTE
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.acompanhante_paciente_insert
before INSERT ON TASY.ACOMPANHANTE_PACIENTE FOR EACH ROW
DECLARE
ie_gerar_controle_acomp_w	varchar2(1);
nr_controle_novo_w		acompanhante_paciente.nr_controle%type;
nr_controle_unico_w		varchar(2);
ds_origem_w			varchar2(1800);
pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	Obter_Param_Usuario(8014, 36,  obter_perfil_ativo, :new.nm_usuario, 0, ie_gerar_controle_acomp_w);
	Obter_Param_Usuario(8014, 119, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_controle_unico_w);

	begin

		if	(ie_gerar_controle_acomp_w = 'S') then

			if (nr_controle_unico_w = 'S') then
				select	CONTROLE_VISITA_UNICO_SEQ.nextval
				into	nr_controle_novo_w
				from	dual;
			else
				select	nvl(max(somente_numero(nr_controle)),0) + 1
				into	nr_controle_novo_w
				from	acompanhante_paciente;
			end if;
			:new.nr_controle := nr_controle_novo_w;
		end if;

		ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

		insert into log_mov(
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(	sysdate,
			'Tasy',
			131712,
			'obter_perfil_ativo: '||obter_perfil_ativo||
			' - :new.nm_usuario: '||:new.nm_usuario||
			' - :new.nr_controle: '||:new.nr_controle||
			' - Stack: ' || ds_origem_w);

		select	controle_visita_seq.nextval
		into	:new.nr_controle_acesso
		from	dual;

	exception
		when	others then
			null;
	end;

	commit;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ACOMPANHANTE_PACIENTE_ATUAL
BEFORE UPDATE ON TASY.ACOMPANHANTE_PACIENTE FOR EACH ROW
DECLARE

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
    IF 	(:NEW.NR_CONTROLE IS NOT NULL) AND
        ((:OLD.NR_CONTROLE IS NULL) OR (:NEW.NR_CONTROLE != :OLD.NR_CONTROLE)) THEN
        VALIDA_CRACHA_INTEGRACAO(:NEW.NM_USUARIO, SOMENTE_NUMERO(:NEW.NR_CONTROLE));
    END IF;
end if;
END;
/


ALTER TABLE TASY.ACOMPANHANTE_PACIENTE ADD (
  CONSTRAINT ACOMPAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ACOMPANHANTE_PACIENTE ADD (
  CONSTRAINT ACOMPAC_VISITAN_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.VISITANTE (NR_SEQUENCIA),
  CONSTRAINT ACOMPAC_ATEVISI_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE) 
 REFERENCES TASY.ATENDIMENTO_VISITA (NR_SEQUENCIA),
  CONSTRAINT ACOMPAC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ACOMPANHANTE_PACIENTE TO NIVEL_1;

