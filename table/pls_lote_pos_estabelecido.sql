ALTER TABLE TASY.PLS_LOTE_POS_ESTABELECIDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_POS_ESTABELECIDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_POS_ESTABELECIDO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA          NUMBER(10)              NOT NULL,
  DT_COMPETENCIA        DATE                    NOT NULL,
  IE_STATUS             VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_GERACAO            DATE,
  NM_USUARIO_GERACAO    VARCHAR2(15 BYTE),
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE),
  DT_COMPETENCIA_MENS   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLPES_PK ON TASY.PLS_LOTE_POS_ESTABELECIDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPES_PLSRLPE_FK_I ON TASY.PLS_LOTE_POS_ESTABELECIDO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LOTE_POS_ESTABELECIDO ADD (
  CONSTRAINT PLSLPES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_POS_ESTABELECIDO ADD (
  CONSTRAINT PLSLPES_PLSRLPE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_LIB_POS_ESTAB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_POS_ESTABELECIDO TO NIVEL_1;

