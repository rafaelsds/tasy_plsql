ALTER TABLE TASY.TISS_REGRA_GUIA_PRINC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_GUIA_PRINC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_GUIA_PRINC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  IE_TISS_TIPO_GUIA    VARCHAR2(2 BYTE)         NOT NULL,
  IE_REGRA             VARCHAR2(3 BYTE)         NOT NULL,
  IE_TIPO_ATENDIMENTO  NUMBER(3),
  IE_MESMA_GUIA        VARCHAR2(1 BYTE)         NOT NULL,
  IE_UNICA_GUIA        VARCHAR2(1 BYTE),
  IE_MESMA_GUIA_PREST  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSGPR_CONVENI_FK_I ON TASY.TISS_REGRA_GUIA_PRINC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSGPR_ESTABEL_FK_I ON TASY.TISS_REGRA_GUIA_PRINC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSGPR_PK ON TASY.TISS_REGRA_GUIA_PRINC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_GUIA_PRINC_tp  after update ON TASY.TISS_REGRA_GUIA_PRINC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TISS_TIPO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_TISS_TIPO_GUIA,1,4000),substr(:new.IE_TISS_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TISS_TIPO_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA_PRINC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA,1,500);gravar_log_alteracao(substr(:old.IE_REGRA,1,4000),substr(:new.IE_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA',ie_log_w,ds_w,'TISS_REGRA_GUIA_PRINC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_UNICA_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_UNICA_GUIA,1,4000),substr(:new.IE_UNICA_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_UNICA_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA_PRINC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MESMA_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_MESMA_GUIA,1,4000),substr(:new.IE_MESMA_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMA_GUIA',ie_log_w,ds_w,'TISS_REGRA_GUIA_PRINC',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_GUIA_PRINC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_GUIA_PRINC ADD (
  CONSTRAINT TISSGPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_GUIA_PRINC ADD (
  CONSTRAINT TISSGPR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSGPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.TISS_REGRA_GUIA_PRINC TO NIVEL_1;

