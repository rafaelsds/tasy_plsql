ALTER TABLE TASY.SALDO_ESTOQUE_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SALDO_ESTOQUE_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SALDO_ESTOQUE_LOTE
(
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_LOCAL_ESTOQUE      NUMBER(4)               NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  DT_MESANO_REFERENCIA  DATE                    NOT NULL,
  NR_SEQ_LOTE           NUMBER(10)              NOT NULL,
  QT_ESTOQUE            NUMBER(13,4)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SAESLOT_I2 ON TASY.SALDO_ESTOQUE_LOTE
(CD_ESTABELECIMENTO, CD_MATERIAL, DT_MESANO_REFERENCIA, CD_LOCAL_ESTOQUE, NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAESLOT_LOCESTO_FK_I ON TASY.SALDO_ESTOQUE_LOTE
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAESLOT_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAESLOT_MATERIA_FK_I ON TASY.SALDO_ESTOQUE_LOTE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAESLOT_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAESLOT_MATLOFO_FK_I ON TASY.SALDO_ESTOQUE_LOTE
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SAESLOT_PK ON TASY.SALDO_ESTOQUE_LOTE
(CD_ESTABELECIMENTO, CD_LOCAL_ESTOQUE, CD_MATERIAL, DT_MESANO_REFERENCIA, NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAESLOT_VALOR_I ON TASY.SALDO_ESTOQUE_LOTE
(DT_MESANO_REFERENCIA, CD_MATERIAL, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.saldo_estoque_lote_log
before update or insert or delete ON TASY.SALDO_ESTOQUE_LOTE 
for each row
begin

if	(inserting) then
	begin
	insert into saldo_estoque_lote_log (
			 CD_ESTABELECIMENTO,     
			 CD_LOCAL_ESTOQUE,      
			 CD_MATERIAL,           
			 DT_MESANO_REFERENCIA,  
			 NR_SEQ_LOTE,           
			 QT_ESTOQUE,            
			 DT_ATUALIZACAO,        
			 NM_USUARIO,            
			 IE_ACAO,
			 DS_STACK)                
	values (
			 :new.CD_ESTABELECIMENTO,     
			 :new.CD_LOCAL_ESTOQUE,      
			 :new.CD_MATERIAL,           
			 :new.DT_MESANO_REFERENCIA,  
			 :new.NR_SEQ_LOTE,           
			 :new.QT_ESTOQUE,            
			 :new.DT_ATUALIZACAO,        
			 :new.NM_USUARIO,            
			'I',
			substr(dbms_utility.format_call_stack,1,4000));
	
	end;
elsif	(updating) then
	begin
	
	insert into saldo_estoque_lote_log (
			 CD_ESTABELECIMENTO,     
			 CD_LOCAL_ESTOQUE,      
			 CD_MATERIAL,           
			 DT_MESANO_REFERENCIA,  
			 NR_SEQ_LOTE,           
			 QT_ESTOQUE,            
			 DT_ATUALIZACAO,        
			 NM_USUARIO,            
			 IE_ACAO,
			 DS_STACK)                
	values (
			 :old.CD_ESTABELECIMENTO,     
			 :old.CD_LOCAL_ESTOQUE,      
			 :old.CD_MATERIAL,           
			 :old.DT_MESANO_REFERENCIA,  
			 :old.NR_SEQ_LOTE,           
			 :old.QT_ESTOQUE,            
			 :old.DT_ATUALIZACAO,        
			 :old.NM_USUARIO,            
			'U',
			substr(dbms_utility.format_call_stack,1,4000));
	
	end;
else
	begin
	
	insert into saldo_estoque_lote_log (
			 CD_ESTABELECIMENTO,     
			 CD_LOCAL_ESTOQUE,      
			 CD_MATERIAL,           
			 DT_MESANO_REFERENCIA,  
			 NR_SEQ_LOTE,           
			 QT_ESTOQUE,            
			 DT_ATUALIZACAO,        
			 NM_USUARIO,            
			 IE_ACAO,
			 DS_STACK)                
	values (
			 :old.CD_ESTABELECIMENTO,     
			 :old.CD_LOCAL_ESTOQUE,      
			 :old.CD_MATERIAL,           
			 :old.DT_MESANO_REFERENCIA,  
			 :old.NR_SEQ_LOTE,           
			 :old.QT_ESTOQUE,            
			 :old.DT_ATUALIZACAO,        
			 :old.NM_USUARIO,            
			'D',
			substr(dbms_utility.format_call_stack,1,4000));
	
	end;
end if;

end;
/


ALTER TABLE TASY.SALDO_ESTOQUE_LOTE ADD (
  CONSTRAINT SAESLOT_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_LOCAL_ESTOQUE, CD_MATERIAL, DT_MESANO_REFERENCIA, NR_SEQ_LOTE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SALDO_ESTOQUE_LOTE ADD (
  CONSTRAINT SAESLOT_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT SAESLOT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SAESLOT_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA));

GRANT SELECT ON TASY.SALDO_ESTOQUE_LOTE TO NIVEL_1;

