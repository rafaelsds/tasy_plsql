ALTER TABLE TASY.ATEND_PACIENTE_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PACIENTE_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PACIENTE_CONTROLE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10),
  NR_SEQ_EPISODIO        NUMBER(10),
  IE_STATUS_ATENDIMENTO  VARCHAR2(1 BYTE)       NOT NULL,
  IE_STATUS_EPISODIO     VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPACCO_ATEPACI_FK_I ON TASY.ATEND_PACIENTE_CONTROLE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACCO_CAEPPA_FK_I ON TASY.ATEND_PACIENTE_CONTROLE
(NR_SEQ_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACCO_PESFISI_FK_I ON TASY.ATEND_PACIENTE_CONTROLE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPACCO_PK ON TASY.ATEND_PACIENTE_CONTROLE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_pac_cont_aft
after update ON TASY.ATEND_PACIENTE_CONTROLE for each row
declare
nr_seq_episodio_w	atendimento_paciente.nr_seq_episodio%type;
nr_seq_episodio_ww	atendimento_paciente.nr_seq_episodio%type;
nr_atendimento_ww	atendimento_paciente.nr_atendimento%type;
nr_seq_agrupador_w	intpd_fila_transmissao.nr_seq_agrupador%type;
qt_reg_w		number(10);
ds_separador_w		varchar2(10)	:= ish_param_pck.get_separador;

cursor C01 is
select	a.nr_sequencia,
	a.nr_seq_agrupador,
	a.ie_evento,
	a.nr_seq_documento
from	intpd_fila_transmissao a
where	a.nr_seq_agrupador	= nr_seq_agrupador_w
and 	nr_seq_agrupador_w is not null
and	a.ie_status		= 'A'
order by nr_sequencia asc;

c01_w	c01%rowtype;

	procedure excluir_fila(
		nr_seq_fila_p	number,
		nr_seq_agrupador_p	number)	is

	begin

	update	intpd_fila_transmissao
	set	nr_seq_dependencia	= null
	where	nr_seq_agrupador	= nr_seq_agrupador_p
	and	nr_seq_dependencia	= nr_seq_fila_p;

	delete	from	intpd_fila_transmissao
	where	nr_sequencia	= nr_seq_fila_p;
	end excluir_fila;

begin


if	(:old.ie_status_atendimento <> :new.ie_status_atendimento) then
	begin
	select	a.nr_seq_episodio
	into	nr_seq_episodio_w
	from	atendimento_paciente a
	where	a.nr_atendimento	= :new.nr_atendimento;

	begin
		select	a.nr_agrupador
		into	nr_seq_agrupador_w
		from	ish_agrupador a,
			episodio_paciente b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	b.nr_sequencia		= nr_seq_episodio_w;
	exception
	when others then
		nr_seq_agrupador_w := null;
	end;

	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin
		nr_seq_episodio_ww	:= ish_utils_pck.obter_dados_fila(c01_w.nr_seq_documento, c01_w.ie_evento, 'E');
		nr_atendimento_ww	:= ish_utils_pck.obter_dados_fila(c01_w.nr_seq_documento, c01_w.ie_evento, 'A');

		if	((nr_atendimento_ww is not null) and (:new.nr_atendimento = nr_atendimento_ww)) or
			((nr_atendimento_ww is null) and (nr_seq_episodio_w = nr_seq_episodio_ww)) then
			if	(:new.ie_status_atendimento = 'T')	then
				begin
				update	intpd_fila_transmissao
				set	ie_status	= 'P'
				where	nr_sequencia	= c01_w.nr_sequencia;
				end;
			elsif	(:new.ie_status_atendimento = 'C') then
				begin
				excluir_fila(c01_w.nr_sequencia, c01_w.nr_seq_agrupador);
				end;
			end if;
		end if;
		end;
	end loop;
	close C01;
	end;
end if;

end;
/


ALTER TABLE TASY.ATEND_PACIENTE_CONTROLE ADD (
  CONSTRAINT ATEPACCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ATEND_PACIENTE_CONTROLE ADD (
  CONSTRAINT ATEPACCO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEPACCO_CAEPPA_FK 
 FOREIGN KEY (NR_SEQ_EPISODIO) 
 REFERENCES TASY.EPISODIO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT ATEPACCO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ATEND_PACIENTE_CONTROLE TO NIVEL_1;

