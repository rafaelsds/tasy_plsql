ALTER TABLE TASY.HEADER_DETAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEADER_DETAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEADER_DETAIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FACILITY_NUMBER   VARCHAR2(5 BYTE)         NOT NULL,
  NR_CAD_RECORDS       VARCHAR2(5 BYTE)         NOT NULL,
  NR_CAN_RECORDS       VARCHAR2(5 BYTE)         NOT NULL,
  NR_FAN_RECORDS       VARCHAR2(5 BYTE)         NOT NULL,
  NR_CDX_RECORDS       VARCHAR2(5 BYTE)         NOT NULL,
  DT_EXPORT_FROM       DATE                     NOT NULL,
  DT_EXPORT_TO         DATE                     NOT NULL,
  NR_REPORT_SEQUENCE   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.HEADET_PK ON TASY.HEADER_DETAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEADET_REDOWCO_FK_I ON TASY.HEADER_DETAIL
(NR_REPORT_SEQUENCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HEADER_DETAIL ADD (
  CONSTRAINT HEADET_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.HEADER_DETAIL ADD (
  CONSTRAINT HEADET_REDOWCO_FK 
 FOREIGN KEY (NR_REPORT_SEQUENCE) 
 REFERENCES TASY.REPORT_DOWNLOADED_COUNT (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEADER_DETAIL TO NIVEL_1;

