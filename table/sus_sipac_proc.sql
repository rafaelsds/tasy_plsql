ALTER TABLE TASY.SUS_SIPAC_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_SIPAC_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_SIPAC_PROC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SIPAC         NUMBER(10)               NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSSIPR_PK ON TASY.SUS_SIPAC_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSSIPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUSSIPR_PROCEDI_FK_I ON TASY.SUS_SIPAC_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSSIPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSSIPR_SUSSIPA_FK_I ON TASY.SUS_SIPAC_PROC
(NR_SEQ_SIPAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSSIPR_SUSSIPA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUS_SIPAC_PROC ADD (
  CONSTRAINT SUSSIPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_SIPAC_PROC ADD (
  CONSTRAINT SUSSIPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSSIPR_SUSSIPA_FK 
 FOREIGN KEY (NR_SEQ_SIPAC) 
 REFERENCES TASY.SUS_SIPAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUS_SIPAC_PROC TO NIVEL_1;

