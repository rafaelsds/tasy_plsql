ALTER TABLE TASY.TABELA_EVOLUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_EVOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_EVOLUCAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_TABELA              VARCHAR2(80 BYTE)      NOT NULL,
  IE_UNIDADE_MEDIDA      VARCHAR2(15 BYTE)      NOT NULL,
  IE_UNIDADE_TEMPO       VARCHAR2(15 BYTE)      NOT NULL,
  DS_FONTE               VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TIPO_TABELA         VARCHAR2(100 BYTE),
  IE_ATRIBUTO            NUMBER(3),
  IE_ATRIBUTO_PRE        NUMBER(3),
  IE_ATRIBUTO_NASC       NUMBER(3),
  IE_ATRIBUTO_AVAL_NUTR  NUMBER(3),
  IE_SEXO                VARCHAR2(1 BYTE),
  QT_IDADE_MIN           NUMBER(3),
  QT_IDADE_MAX           NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TABEVOL_PK ON TASY.TABELA_EVOLUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TABELA_EVOLUCAO_tp  after update ON TASY.TABELA_EVOLUCAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'TABELA_EVOLUCAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TABELA_EVOLUCAO ADD (
  CONSTRAINT TABEVOL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TABELA_EVOLUCAO TO NIVEL_1;

