ALTER TABLE TASY.NOTA_FISCAL_VENC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_VENC CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_VENC
(
  NR_SEQUENCIA         NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_CGC_EMITENTE      VARCHAR2(14 BYTE),
  CD_SERIE_NF          VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQUENCIA_NF      NUMBER(10)               NOT NULL,
  DT_VENCIMENTO        DATE                     NOT NULL,
  VL_VENCIMENTO        NUMBER(13,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_BASE_VENC         NUMBER(15,2),
  VL_DESCONTO          NUMBER(15,2),
  VL_DESC_FINANC       NUMBER(15,2),
  NR_TITULO_PAGAR      NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NR_NOTA_FISCAL       VARCHAR2(255 BYTE)       NOT NULL,
  IE_VENC_ALT_SISTEMA  VARCHAR2(1 BYTE),
  IE_CONFERIDO         VARCHAR2(1 BYTE),
  IE_ORIGEM            VARCHAR2(1 BYTE)         NOT NULL,
  NR_TITULO_VINC       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTFIVE_I ON TASY.NOTA_FISCAL_VENC
(CD_ESTABELECIMENTO, CD_CGC_EMITENTE, CD_SERIE_NF, NR_NOTA_FISCAL, NR_SEQUENCIA_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIVE_NOTFISC_FK_I ON TASY.NOTA_FISCAL_VENC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NOTFIVE_PK ON TASY.NOTA_FISCAL_VENC
(NR_SEQUENCIA, DT_VENCIMENTO, IE_ORIGEM)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIVE_TITPAGA_FK_I ON TASY.NOTA_FISCAL_VENC
(NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_log
before insert or update or delete on  nota_fiscal_venc
for each row
declare
ds_log_w	log_tasy.ds_log%type;
ds_perfil_w	perfil.ds_perfil%type;

begin

select	max(substr(OBTER_DESC_PERFIL(obter_perfil_ativo),1,30))
into	ds_perfil_w
from	dual;

if	(updating) then

	ds_log_w	:=  'Altera��o de vencimento' || chr(13) || chr(10) ||
						'Sequencia da NF: ' || :new.nr_sequencia || chr(13) || chr(10) ||
						'Data venc antiga: ' || to_char(:old.dt_vencimento,'dd/mm/yyyy') || chr(13) || chr(10) ||
						'Data venc nova: ' || to_char(:new.dt_vencimento,'dd/mm/yyyy') || chr(13) || chr(10) ||
						'Data altera��o: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || chr(13) || chr(10) ||
						'Valor antigo: ' || :old.vl_vencimento  || chr(13) || chr(10) ||
						'Valor novo: ' || :new.vl_vencimento  || chr(13) || chr(10) ||
						'T�tulo:' || :new.nr_titulo_pagar  || chr(13) || chr(10) ||
						'Usu�rio: ' || wheb_usuario_pck.get_nm_usuario || chr(13) || chr(10) ||
						'Perfil: ' || ds_perfil_w || chr(13) || chr(10) ||
						'Fun��o: ' || substr(OBTER_DESC_FUNCAO(Obter_Funcao_Ativa),1,20) ||
						'Call stack: ' || substr(dbms_utility.format_call_stack,1,1000);

	gravar_log_tasy(55911,ds_log_w,nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));

elsif (inserting) then

	ds_log_w	:=  'Inser��o de vencimento' || chr(13) || chr(10) ||
						'Sequencia da NF: ' || :new.nr_sequencia || chr(13) || chr(10) ||
						'Data venc: ' || to_char(:new.dt_vencimento,'dd/mm/yyyy') || chr(13) || chr(10) ||
						'Data inser��o: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || chr(13) || chr(10) ||
						'Valor: ' || :new.vl_vencimento  || chr(13) || chr(10) ||
						'T�tulo:' || :new.nr_titulo_pagar  || chr(13) || chr(10) ||
						'Usu�rio: ' || wheb_usuario_pck.get_nm_usuario || chr(13) || chr(10) ||
						'Perfil: ' || ds_perfil_w || chr(13) || chr(10) ||
						'Fun��o: ' || substr(OBTER_DESC_FUNCAO(Obter_Funcao_Ativa),1,20) ||
						'Call stack: ' || substr(dbms_utility.format_call_stack,1,1000);

	gravar_log_tasy(55912,ds_log_w,nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));


elsif (deleting) then

	ds_log_w	:=  'Exclus�o de vencimento' || chr(13) || chr(10) ||
						'Sequencia da NF: ' || :old.nr_sequencia || chr(13) || chr(10) ||
						'Data venc: ' || to_char(:old.dt_vencimento,'dd/mm/yyyy') || chr(13) || chr(10) ||
						'Data exclus�o: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || chr(13) || chr(10) ||
						'Valor: ' || :old.vl_vencimento  || chr(13) || chr(10) ||
						'T�tulo:' || :old.nr_titulo_pagar  || chr(13) || chr(10) ||
						'Usu�rio: ' || wheb_usuario_pck.get_nm_usuario || chr(13) || chr(10) ||
						'Perfil: ' || ds_perfil_w || chr(13) || chr(10) ||
						'Fun��o: ' || substr(OBTER_DESC_FUNCAO(Obter_Funcao_Ativa),1,20) ||
						'Call stack: ' || substr(dbms_utility.format_call_stack,1,1000);

	gravar_log_tasy(55913,ds_log_w,nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_atual
before insert or update ON TASY.NOTA_FISCAL_VENC for each row
declare

begin
--feito este tratamento para contabiliza��o dos vencimentos, pois estava buscando a data no formato HH:MM:SS, e n�o entrava na contabilidade.
:new.dt_vencimento := trunc(:new.dt_vencimento);

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_insert
BEFORE INSERT ON TASY.NOTA_FISCAL_VENC FOR EACH ROW
BEGIN

if	(:new.ie_origem is null) then
	:new.ie_origem := 'N';
end if;

END;
/


ALTER TABLE TASY.NOTA_FISCAL_VENC ADD (
  CONSTRAINT NOTFIVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA, DT_VENCIMENTO, IE_ORIGEM)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_VENC ADD (
  CONSTRAINT NOTFIVE_NOTFISC_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NOTFIVE_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.NOTA_FISCAL_VENC TO NIVEL_1;

