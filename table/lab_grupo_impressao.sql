ALTER TABLE TASY.LAB_GRUPO_IMPRESSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_GRUPO_IMPRESSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_GRUPO_IMPRESSAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_GRUPO                VARCHAR2(50 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQ_APRESENT         NUMBER(5),
  IE_REGRA_SEQUENCIA      VARCHAR2(5 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_INIC             NUMBER(20),
  IE_CONS_SEQ_INIC_REGRA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LABGRIM_PK ON TASY.LAB_GRUPO_IMPRESSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Lab_Grupo_Impressao_Update
AFTER UPDATE ON TASY.LAB_GRUPO_IMPRESSAO FOR EACH ROW
declare
nr_sequencia_w	number(10);
dt_sequencia_w	date;

BEGIN

if (:new.ie_regra_sequencia is not null) then
	if 	(substr(:new.ie_regra_sequencia,1,1) = 'D') then
		dt_sequencia_w := trunc(sysdate,'dd');
	elsif 	(substr(:new.ie_regra_sequencia,1,1) = 'S') then
		dt_sequencia_w := trunc(sysdate,'day');
	elsif 	(substr(:new.ie_regra_sequencia,1,1) = 'M') then
		dt_sequencia_w := trunc(sysdate,'month');
	elsif 	(substr(:new.ie_regra_sequencia,1,1) = 'A') then
		dt_sequencia_w := trunc(sysdate,'year');
	elsif 	(substr(:new.ie_regra_sequencia,1,1) = 'G') then
		dt_sequencia_w := trunc(sysdate,'year');
	end if;
	insert into lab_seq_grupo_imp (	nr_sequencia, nr_seq_grupo_imp, dt_sequencia,
					nr_valor_seq, dt_atualizacao, nm_usuario)
	values (lab_seq_grupo_imp_seq.nextval, :new.nr_sequencia, dt_sequencia_w,
		nvl(:new.nr_seq_inic,'0'), sysdate, :new.nm_usuario);
end if;

END;
/


ALTER TABLE TASY.LAB_GRUPO_IMPRESSAO ADD (
  CONSTRAINT LABGRIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LAB_GRUPO_IMPRESSAO TO NIVEL_1;

