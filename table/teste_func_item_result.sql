ALTER TABLE TASY.TESTE_FUNC_ITEM_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TESTE_FUNC_ITEM_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TESTE_FUNC_ITEM_RESULT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_RESULTADO         VARCHAR2(2000 BYTE)      NOT NULL,
  NR_SEQ_ACAO          NUMBER(10)               NOT NULL,
  NR_SEQ_APRES         NUMBER(3),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TEFUITR_PK ON TASY.TESTE_FUNC_ITEM_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TEFUITR_PK
  MONITORING USAGE;


CREATE INDEX TASY.TEFUITR_TEFUITA_FK_I ON TASY.TESTE_FUNC_ITEM_RESULT
(NR_SEQ_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TEFUITR_TEFUITA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TESTE_FUNC_ITEM_RESULT_atual
after update or insert ON TASY.TESTE_FUNC_ITEM_RESULT for each row
begin

update	teste_funcao a
set	dt_atualizacao	= sysdate,
	nm_usuario	= :new.nm_usuario
where	a.nr_sequencia	= (	select	nr_seq_teste
				from	TESTE_FUNCAO_ITEM x
				where	x.nr_sequencia = (	select	NR_SEQ_ITEM
								from	TESTE_FUNCAO_ITEM_ACAO y
								where	y.nr_sequencia = :new.NR_SEQ_ACAO));

end;
/


CREATE OR REPLACE TRIGGER TASY.TESTE_FUNC_ITEM_RESULT_tp  after update ON TASY.TESTE_FUNC_ITEM_RESULT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_RESULTADO,1,4000),substr(:new.DS_RESULTADO,1,4000),:new.nm_usuario,nr_seq_w,'DS_RESULTADO',ie_log_w,ds_w,'TESTE_FUNC_ITEM_RESULT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'TESTE_FUNC_ITEM_RESULT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TESTE_FUNC_ITEM_RESULT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TESTE_FUNC_ITEM_RESULT ADD (
  CONSTRAINT TEFUITR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TESTE_FUNC_ITEM_RESULT ADD (
  CONSTRAINT TEFUITR_TEFUITA_FK 
 FOREIGN KEY (NR_SEQ_ACAO) 
 REFERENCES TASY.TESTE_FUNCAO_ITEM_ACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TESTE_FUNC_ITEM_RESULT TO NIVEL_1;

