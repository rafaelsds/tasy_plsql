ALTER TABLE TASY.PLS_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTRATO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_CGC_ESTIPULANTE            VARCHAR2(14 BYTE),
  CD_PF_ESTIPULANTE             VARCHAR2(10 BYTE),
  CD_COD_ANTERIOR               VARCHAR2(20 BYTE),
  DT_CONTRATO                   DATE            NOT NULL,
  DT_APROVACAO                  DATE,
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  QT_INTERVALO                  NUMBER(10),
  IE_RENOVACAO_AUTOMATICA       VARCHAR2(1 BYTE) NOT NULL,
  IE_PERMITE_PROD_DIF           VARCHAR2(1 BYTE) NOT NULL,
  DT_RESCISAO_CONTRATO          DATE,
  DT_REATIVACAO                 DATE,
  CD_CONTRATO                   VARCHAR2(20 BYTE),
  IE_REAJUSTE                   VARCHAR2(1 BYTE) NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  IE_GERACAO_VALORES            VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_EMISSOR                NUMBER(10),
  IE_TIPO_BENEFICIARIO          VARCHAR2(3 BYTE),
  NR_SEQ_CONGENERE              NUMBER(10),
  QT_DIAS_VALID_CART            NUMBER(5),
  QT_IDADE_LIMITE_REAJ          NUMBER(3),
  QT_ANOS_LIMITE_REAJ           NUMBER(3),
  DT_CANCELAMENTO               DATE,
  NR_SEQ_MOTIVO_RESCISAO        NUMBER(10),
  NR_SEQ_PROPOSTA               NUMBER(10),
  NR_CONTRATO_PRINCIPAL         NUMBER(10),
  IE_TIPO_OPERACAO              VARCHAR2(3 BYTE) NOT NULL,
  IE_PRECO_CO_OPERADORA         VARCHAR2(1 BYTE),
  IE_ITENS_NAO_COBERTOS         VARCHAR2(1 BYTE),
  DT_LIMITE_MOVIMENTACAO        NUMBER(2),
  IE_COBRANCA_MENSALIDADE       VARCHAR2(1 BYTE),
  QT_INTERV_MES_MENSALIDADE     NUMBER(5),
  DT_ULTIMA_MENS_GERADA         DATE,
  DT_LIMITE_UTILIZACAO          DATE,
  IE_CONTROLE_CARTEIRA          VARCHAR2(1 BYTE) NOT NULL,
  NR_CONTRATO                   NUMBER(10)      NOT NULL,
  NR_SEQ_ASSINATURA             NUMBER(10),
  CD_OPERADORA_EMPRESA          NUMBER(10),
  NR_SEQ_OPERADORA              NUMBER(10),
  NR_SEQ_COOPERATIVA            NUMBER(10),
  NR_SEQ_INDICE_REAJUSTE        NUMBER(10),
  NR_SEQ_CONTRATO_ANT           NUMBER(10),
  CD_COOPERATIVA                VARCHAR2(10 BYTE),
  IE_NOVO_BENEFICIARIO          VARCHAR2(1 BYTE),
  DT_REAJUSTE                   DATE,
  CD_CONTRATO_PRINCIPAL         NUMBER(10),
  IE_EXIGE_VINC_OPERADORA       VARCHAR2(1 BYTE),
  DT_BASE_VALIDADE_CARTEIRA     VARCHAR2(2 BYTE),
  NR_PROPOSTA_ANT               VARCHAR2(10 BYTE),
  NR_SEQ_COMPL_PJ               NUMBER(10),
  IE_PERMITE_MATRICULA_DIF      VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO          NUMBER(10),
  CD_CLASSIF_CONTRATO           VARCHAR2(10 BYTE),
  IE_CONSISTIR_CARENCIA_REDE    VARCHAR2(1 BYTE),
  CD_SCPA                       VARCHAR2(30 BYTE),
  IE_MES_COBRANCA_REAJ          VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_COMISSAO          NUMBER(10),
  DT_BASE_CARTEIRA              DATE,
  DS_CONTRATO                   VARCHAR2(255 BYTE),
  IE_CONTRATO_EX_FUNCIONARIO    VARCHAR2(1 BYTE),
  IE_PARTICIPACAO               VARCHAR2(1 BYTE),
  CD_CGC_ADMINISTRADORA         VARCHAR2(14 BYTE),
  DS_CONTRATO_CARTEIRA          VARCHAR2(255 BYTE),
  IE_CONSIDERAR_CONTAGEM_VIDAS  VARCHAR2(1 BYTE),
  NR_SEQ_INDICE_REAJ            NUMBER(10),
  NR_MES_REAJUSTE               NUMBER(2),
  NR_SEQ_GRUPO_REAJUSTE         NUMBER(10),
  NR_SEQ_PROP_ONLINE            NUMBER(10),
  IE_EMPRESARIO_INDIVIDUAL      VARCHAR2(1 BYTE),
  CD_CAEPF                      VARCHAR2(14 BYTE),
  NR_SEQ_CAUSA_RESCISAO         NUMBER(10),
  DT_RECEBIMENTO                DATE,
  IE_ENVIAR_ALERTA_RESC_BENEF   VARCHAR2(1 BYTE),
  IE_UTILIZAR_FECHAM_CONTA      VARCHAR2(1 BYTE),
  IE_EXCLUSIVO_BENEF_REMIDO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCONT_CGC_ADM_FK_I ON TASY.PLS_CONTRATO
(CD_CGC_ADMINISTRADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_ESTABEL_FK_I ON TASY.PLS_CONTRATO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_I1 ON TASY.PLS_CONTRATO
(DT_RESCISAO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_MOEDA_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_INDICE_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PESFISI_FK_I ON TASY.PLS_CONTRATO
(CD_PF_ESTIPULANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PESJUCP_FK_I ON TASY.PLS_CONTRATO
(CD_CGC_ESTIPULANTE, NR_SEQ_COMPL_PJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PESJURI_FK_I ON TASY.PLS_CONTRATO
(CD_CGC_ESTIPULANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCONT_PK ON TASY.PLS_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLINDREAJ_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_INDICE_REAJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLSCARS_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_CAUSA_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLSCONG_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PLSCONG_FK1_I ON TASY.PLS_CONTRATO
(NR_SEQ_COOPERATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSCONG_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PLSCONT_FK_I ON TASY.PLS_CONTRATO
(NR_CONTRATO_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLSCSSC_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSCSSC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PLSEMC_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_EMISSOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSEMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PLSGRCO_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_GRUPO_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLSMOCA_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_MOTIVO_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSMOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PLSOUTO_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSOUTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_PLSPRAD_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLSPRON_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_PROP_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCONT_PLSTPCM_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_TIPO_COMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_PLSTPCM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCONT_TASASDI_FK_I ON TASY.PLS_CONTRATO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCONT_TASASDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCONT_UK ON TASY.PLS_CONTRATO
(CD_COD_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCONT_UK2 ON TASY.PLS_CONTRATO
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ssj_contratos_inova
before insert ON TASY.PLS_CONTRATO 
for each row
declare

/*Trigger criada para colocar os contratos da inova no parametro 11 do OPSW - Mensalidade, para que os benefici�rios da INOVA n�o consiga ver os valores de mensalidade, somente o pagador*/
    

ds_email_origem      varchar2(255);
ds_assunto           varchar2(255);
body_clob_w          clob;
observacao           varchar(255);

   
begin

observacao:='Par�metro adicionado a pedido do Clodoaldo(INOVA) / Trigger ssj_contratos_inova';
ds_email_origem:='wesllen.medeiros@ssjose.com.br' ;
ds_assunto:='Contratos Inova - Plano de Sa�de Sa�de S�o Jos�'; 


 body_clob_w :='<email background="#fff" lang="pt-BR">
 <row style="background: #009900; ">
         <col style="background: #009900; color: #009900;">
             <p2>
                 <text italic="true" bolder="true" foreground="#212529" size="3"></text>
             </p2>
         </col>
     </row>
     <row>
         <col>
             <p1>
                 <center>
                     <text align="center" size="4" >Prazado(a),<br/><br/>
                         Um Novo contrato da Inova '||:new.nr_contrato||' foi adicionado ao parametro 11<br/><br/>
                         do OPSW - Mensalidades.<br/><br/>
                     </text>
                  </center>   
             </p1>
             <info type="AUTOMATIC_EMAIL"></info>
         </col>
     </row>
 </email>'; 
    
    if (:new.CD_CGC_ESTIPULANTE = 25291830000146 and :new.IE_SITUACAO = 1)  then 
         
          insert into PLS_WEB_PARAM_CONTRATO(
               nr_sequencia,
               cd_estabelecimento,
               nr_seq_param,
               nr_seq_contrato,
               dt_atualizacao,
               nm_usuario,
               dt_atualizacao_nrec,
               nm_usuario_nrec,
               vl_parametro,
               cd_funcao,
               nr_seq_funcao_param,
               ds_observacao
          )values(
                pls_web_param_contrato_seq.nextval,
                2,
                null,
                :new.nr_contrato,
                sysdate,
                'Trigger.ssj',
                sysdate,
                'Trigger.ssj',
                2,
                1252,
                11,
                observacao
          );
          
          
          ssj_enviar_email
          (
            'wesllen.ssj',
            ds_assunto,
            null,
            ds_email_origem,
            'wesllen.medeiros@ssjose.com.br;tecnologia@ssjose.com.br',
            null,
            'M',
            body_clob_w,
            null,
            null,
            null,
            null,
            'N',
            'S',
            null
          );
          
    end if;
    


end ssj_contratos_inova;
/


CREATE OR REPLACE TRIGGER TASY.ssj_gerar_multa_rescisao
after update
ON TASY.PLS_CONTRATO for each row
declare 

Pragma Autonomous_Transaction;
nr_seq_contrato_w int;

begin

    nr_seq_contrato_w := :new.nr_sequencia;

    ssj_inserir_multa_rescisao(nr_seq_contrato_w);

end ssj_gerar_multa_rescisao;
/


CREATE OR REPLACE TRIGGER TASY.ssj_atualiza_part_fin_contrato
before insert ON TASY.PLS_CONTRATO for each row
declare
                
/* 
    Quando um novo contrato for criado e n�o existir o atributo de participa��o financeira, ser� setado automaticamente 'S' Sem participa��o,
    pois visto que esse atributo ser� necess�rio para o funcionamento de alguns lotes cont�beis e livros auxiliares.
    
    obs: quando exite proposta de ades�o, � inserido automaticamente null, ent�o essa trigger se faz necess�rio para inserir Sem participa��o como padr�o.
    Caso o informa��o seja outra, o usu�rio ter� que trocar a informa��o da participa��o financeira no contrato ap�s gera��o.
*/
begin

    if(:new.ie_participacao is null)then
     :new.ie_participacao := 'S';
    end if;
    
end ssj_atualiza_part_fin_contrato;
/


ALTER TABLE TASY.PLS_CONTRATO ADD (
  CONSTRAINT PLSCONT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSCONT_UK
 UNIQUE (CD_COD_ANTERIOR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSCONT_UK2
 UNIQUE (NR_CONTRATO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTRATO ADD (
  CONSTRAINT PLSCONT_PLINDREAJ_FK 
 FOREIGN KEY (NR_SEQ_INDICE_REAJ) 
 REFERENCES TASY.PLS_INDICE_REAJUSTE (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_CGC_ADM_FK 
 FOREIGN KEY (CD_CGC_ADMINISTRADORA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSCONT_PLSGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REAJUSTE) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSPRON_FK 
 FOREIGN KEY (NR_SEQ_PROP_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSCARS_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_RESCISAO) 
 REFERENCES TASY.PLS_CAUSA_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSTPCM_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMISSAO) 
 REFERENCES TASY.PLS_TIPO_COMISSAO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PESJURI_FK 
 FOREIGN KEY (CD_CGC_ESTIPULANTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSCONT_PESFISI_FK 
 FOREIGN KEY (CD_PF_ESTIPULANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSCONT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCONT_PLSEMC_FK 
 FOREIGN KEY (NR_SEQ_EMISSOR) 
 REFERENCES TASY.PLS_EMISSOR_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RESCISAO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSCONT_FK 
 FOREIGN KEY (NR_CONTRATO_PRINCIPAL) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSOUTO_FK 
 FOREIGN KEY (NR_SEQ_OPERADORA) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSCONG_FK1 
 FOREIGN KEY (NR_SEQ_COOPERATIVA) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSCONT_MOEDA_FK 
 FOREIGN KEY (NR_SEQ_INDICE_REAJUSTE) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSCONT_PESJUCP_FK 
 FOREIGN KEY (CD_CGC_ESTIPULANTE, NR_SEQ_COMPL_PJ) 
 REFERENCES TASY.PESSOA_JURIDICA_COMPL (CD_CGC,NR_SEQUENCIA),
  CONSTRAINT PLSCONT_PLSCSSC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIFICACAO_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTRATO TO NIVEL_1;

