ALTER TABLE TASY.PLS_REQUISICAO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REQUISICAO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REQUISICAO_PROC
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_REQUISICAO           NUMBER(10)        NOT NULL,
  CD_PROCEDIMENTO             NUMBER(15)        NOT NULL,
  IE_ORIGEM_PROCED            NUMBER(10)        NOT NULL,
  QT_PROCEDIMENTO             NUMBER(9,3),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_COBERTURA            NUMBER(10),
  IE_TIPO_COBERTURA           VARCHAR2(1 BYTE),
  IE_STATUS                   VARCHAR2(2 BYTE)  NOT NULL,
  NR_SEQ_GUIA                 NUMBER(10),
  NR_SEQ_GRUPO_EXEC           NUMBER(10),
  NR_SEQ_MOTIVO_EXC           NUMBER(10),
  IE_ESTAGIO                  VARCHAR2(3 BYTE)  NOT NULL,
  NR_SEQ_TIPO_LIMITACAO       NUMBER(10),
  QT_PROC_EXECUTADO           NUMBER(9,3),
  NR_SEQ_PACOTE               NUMBER(10),
  VL_PROCEDIMENTO             NUMBER(15,2),
  CD_PORTE_ANESTESICO         VARCHAR2(10 BYTE),
  NR_SEQ_REGRA                NUMBER(10),
  NR_SEQ_COMPL_PTU            NUMBER(10),
  QT_SOLICITADO               NUMBER(9,3)       NOT NULL,
  NR_SEQ_TRATAMENTO           NUMBER(10),
  IE_UTILIZA_OPME             VARCHAR2(1 BYTE),
  IE_PACOTE_ABERTO            VARCHAR2(1 BYTE),
  CD_PACOTE_ITEM              NUMBER(15),
  NR_SEQ_PRECO_PACOTE         NUMBER(10),
  NR_SEQ_CONVERSAO_PACOTE     NUMBER(10),
  NR_SEQ_MOTIVO_CANCEL        NUMBER(10),
  IE_COBRANCA_PREVIA_SERVICO  VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO_PTU         NUMBER(15),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  IE_ORIGEM_INCLUSAO          VARCHAR2(2 BYTE),
  DS_PROCEDIMENTO_PTU         VARCHAR2(80 BYTE),
  CD_PROC_INEXISTENTE         NUMBER(15),
  IE_ESTAGIO_EXECUCAO         VARCHAR2(3 BYTE),
  VL_TOTAL_PACOTE             NUMBER(15,2),
  IE_TIPO_ANEXO               VARCHAR2(2 BYTE),
  IE_PACOTE_PTU               VARCHAR2(1 BYTE),
  IE_ORIGEM_PACOTE_ITEM       NUMBER(10),
  NR_SEQ_PROC_PRINC           NUMBER(10),
  IE_COBRANCA_PREVISTA        VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_CARENCIA        NUMBER(10),
  IE_TIPO_TABELA              VARCHAR2(3 BYTE),
  NR_SEQ_MAT_PRINC            NUMBER(10),
  NR_SEQ_PRIORIDADE           NUMBER(10),
  NR_SEQ_ITEM_TISS            NUMBER(4),
  NR_SEQ_SCA_COBERTURA        NUMBER(10),
  IE_SERVICO_PROPRIO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRPRO_I1 ON TASY.PLS_REQUISICAO_PROC
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPRO_I2 ON TASY.PLS_REQUISICAO_PROC
(IE_COBRANCA_PREVIA_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_I2
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRPRO_PK ON TASY.PLS_REQUISICAO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPRO_PLSGUMC_FK_I ON TASY.PLS_REQUISICAO_PROC
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_PLSGUMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPRO_PLSMOEX_FK_I ON TASY.PLS_REQUISICAO_PROC
(NR_SEQ_MOTIVO_EXC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_PLSMOEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPRO_PLSPACO_FK_I ON TASY.PLS_REQUISICAO_PROC
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_PLSPACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPRO_PLSREQU_FK_I ON TASY.PLS_REQUISICAO_PROC
(NR_SEQ_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPRO_PLSTRAB_FK_I ON TASY.PLS_REQUISICAO_PROC
(NR_SEQ_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_PLSTRAB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRPRO_PROCEDI_FK_I ON TASY.PLS_REQUISICAO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPRO_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_requisicao_proc_update
before update ON TASY.PLS_REQUISICAO_PROC for each row
declare

ds_mensagem_w			varchar2(1000);
cd_estabelecimento_w		pls_requisicao.cd_estabelecimento%type;
qt_solicitada_w			number(10);
qt_permitida_w			number(10);
nr_seq_pacote_w			pls_pacote.nr_sequencia%type;
ie_regra_preco_w		varchar2(3);
nr_seq_regra_pacote_out_w	number(10);
cd_proc_pacote_out_w		number(15)	:= null;
ie_origem_pacote_out_w		number(15)	:= null;
vl_pacote_out_w			number(15,2)	:= 0;
vl_medico_out_w			number(15,2)	:= 0;
vl_anestesista_out_w		number(15,2)	:= 0;
vl_auxiliares_out_w		number(15,2)	:= 0;
vl_custo_operacional_out_w	number(15,2)	:= 0;
vl_materiais_out_w		number(15,2)	:= 0;
nr_seq_intercambio_w		number(10);
nr_seq_classificacao_prest_w	number(10);
nr_seq_protocolo_w		number(10);
dt_conta_guia_w			date;
nr_seq_segurado_w		number(10);
cd_guia_w			varchar2(30);
sg_estado_w			pessoa_juridica.sg_estado%type;
sg_estado_int_w			pessoa_juridica.sg_estado%type;
nr_seq_prestador_prot_w		number(10);
nr_seq_tipo_acomod_w		number(10);
dt_pacote_w			date;
ie_internado_w			varchar2(3);
nr_seq_plano_w			number(10);
nr_contrato_w			number(10);
nr_seq_congenere_w		number(10);
ie_origem_conta_w		varchar2(5)	:= null;
ie_tipo_intercambio_w		varchar2(5);
nr_seq_conta_w			number(10);
nr_seq_prestador_conta_w	number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(15);
nr_seq_prestador_w		number(10);
nr_seq_requisicao_w		number(10);
ie_tipo_processo_w		varchar2(2);
ie_tipo_intercambio_ww		pls_requisicao.ie_tipo_intercambio%type;
ie_ident_pacote_ptu_w		pls_param_intercambio_scs.ie_ident_pacote_ptu%type;
qt_pacote_w			number(10)	:= 0;
ie_pacote_maior_vl_w		pls_param_requisicao.ie_pacote_maior_vl%type;
ie_tipo_guia_w			pls_requisicao.ie_tipo_guia%type;
ie_nacional_w			pls_congenere.ie_nacional%type;

pragma autonomous_transaction;

begin

select	max(nr_sequencia)
into	nr_seq_pacote_w
from	pls_pacote
where	cd_procedimento		= :new.cd_procedimento
and	ie_origem_proced	= :new.ie_origem_proced
and	ie_situacao		= 'A';

if	(nr_seq_pacote_w	is not null) then
	select	nvl(ie_regra_preco,'N')
	into	ie_regra_preco_w
	from	pls_pacote a
	where	a.nr_sequencia = nr_seq_pacote_w;

	nr_seq_requisicao_w	:= :new.nr_seq_requisicao;

	begin
		/*Obter dados da guia*/
		select	nvl(nr_seq_prestador_exec,nr_seq_prestador),
			cd_estabelecimento,
			dt_requisicao,
			nr_seq_tipo_acomodacao,
			nr_seq_plano,
			pls_obter_internado_guia(nr_sequencia,'R'),
			nr_seq_segurado,
			ie_tipo_processo,
			nvl(ie_tipo_intercambio, 'N'),
			ie_tipo_guia
		into	nr_seq_prestador_w,
			cd_estabelecimento_w,
			dt_pacote_w,
			nr_seq_tipo_acomod_w,
			nr_seq_plano_w,
			ie_internado_w,
			nr_seq_segurado_w,
			ie_tipo_processo_w,
			ie_tipo_intercambio_ww,
			ie_tipo_guia_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
	exception
	when others then
		goto final;
	end;


	if	(ie_regra_preco_w = 'S') then
		if (nr_seq_tipo_acomod_w is null) then
			nr_seq_tipo_acomod_w := pls_obter_dados_produto(nr_seq_plano_w,'AP');
		end if;

		if	(nvl(nr_seq_plano_w,0) = 0) then
			begin
			select	a.nr_seq_plano
			into	nr_seq_plano_w
			from	pls_segurado			a
			where	a.nr_sequencia = nr_seq_segurado_w;
			exception
			when others then
				nr_seq_plano_w := null;
			end;
		end if;

		begin
		select	nvl(a.nr_contrato,0),
			b.nr_seq_congenere
		into	nr_contrato_w,
			nr_seq_congenere_w
		from	pls_contrato	a,
			pls_segurado	b
		where	a.nr_sequencia	= b.nr_seq_contrato
		and	b.nr_sequencia	= nr_seq_segurado_w;
		exception
		when others then
			nr_contrato_w	:= 0;
		end;

		if	(nr_seq_congenere_w is null) then
			select	b.nr_seq_congenere
			into	nr_seq_congenere_w
			from	pls_segurado	b
			where	b.nr_sequencia	= nr_seq_segurado_w;
		end if;

		select	max(nr_seq_intercambio)
		into	nr_seq_intercambio_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;

		select	max(nr_seq_classificacao)
		into	nr_seq_classificacao_prest_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prestador_w;

		select	nvl(max(sg_estado),'X')
		into	sg_estado_w
		from	pessoa_juridica
		where	cd_cgc	= (	select	max(cd_cgc_outorgante)
					from	pls_outorgante
					where	cd_estabelecimento	= cd_estabelecimento_w);
		select	nvl(max(a.sg_estado),'X'),
			nvl(max(ie_nacional),'N')
		into	sg_estado_int_w,
			ie_nacional_w
		from	pessoa_juridica	a,
			pls_congenere	b
		where	a.cd_cgc	= b.cd_cgc
		and	b.nr_sequencia	= nr_seq_congenere_w;

		if	(ie_nacional_w = 'S') then
			ie_tipo_intercambio_w := 'N';
		elsif	(sg_estado_w <> 'X') and
			(sg_estado_int_w <> 'X') then
			if	(sg_estado_w	= sg_estado_int_w) then
				ie_tipo_intercambio_w	:= 'E';
			else
				ie_tipo_intercambio_w	:= 'N';
			end if;
		else
			ie_tipo_intercambio_w	:= 'A';
		end if;

		pls_define_preco_pacote(cd_estabelecimento_w,
					nr_seq_prestador_w,
					nr_seq_tipo_acomod_w,
					dt_pacote_w,
					:new.cd_procedimento,
					:new.ie_origem_proced,
					ie_internado_w,
					nr_seq_plano_w,
					nr_contrato_w,
					nr_seq_congenere_w,
					:new.nm_usuario,
					ie_origem_conta_w,
					ie_tipo_intercambio_w,
					nr_seq_pacote_w,
					nr_seq_regra_pacote_out_w,
					cd_proc_pacote_out_w,
					ie_origem_pacote_out_w,
					vl_pacote_out_w,
					vl_medico_out_w,
					vl_anestesista_out_w,
					vl_auxiliares_out_w,
					vl_custo_operacional_out_w,
					vl_materiais_out_w,
					nr_seq_intercambio_w,
					nr_seq_classificacao_prest_w,
					null,
					nr_seq_segurado_w,
					null,
					'N',
					null,
					1);
	end if;

	begin
		select	nvl(ie_pacote_maior_vl,'N')
		into	ie_pacote_maior_vl_w
		from	pls_param_requisicao;
	exception
	when others then
		ie_pacote_maior_vl_w	:= 'N';
	end;

	if	(ie_pacote_maior_vl_w	= 'S') then

		select	max(nr_sequencia)
		into	nr_seq_pacote_w
		from	pls_pacote
		where	cd_procedimento		= :new.cd_procedimento
		and	ie_origem_proced	= :new.ie_origem_proced
		and	ie_situacao		= 'A';

		if	(nr_seq_pacote_w is not null) then
			select	max(a.nr_sequencia)
			into	nr_seq_regra_pacote_out_w
			from	pls_pacote_tipo_acomodacao a
			where 	a.nr_seq_pacote = nr_seq_pacote_w
			and   	a.vl_pacote       = (	select  max(x.vl_pacote)
							from    pls_pacote_tipo_acomodacao x
							where   x.nr_seq_pacote = nr_seq_pacote_w
							and	((x.dt_fim_vigencia is null) or (trunc(x.dt_fim_vigencia) >= trunc(sysdate))));

			begin
				select	vl_pacote
				into	vl_pacote_out_w
				from	pls_pacote_tipo_acomodacao
				where	nr_sequencia	= nr_seq_regra_pacote_out_w;
			exception
			when others then
				vl_pacote_out_w := null;
			end;
		end if;
	end if;

	if	(nr_seq_pacote_w is not null) then
		:new.vl_total_pacote		:= (vl_pacote_out_w * :new.qt_solicitado);
	end if;

	if (nr_seq_regra_pacote_out_w is not null) then
		:new.vl_procedimento		:= vl_pacote_out_w;
		:new.nr_seq_pacote		:= nr_seq_pacote_w;
		:new.nr_seq_preco_pacote	:= nr_seq_regra_pacote_out_w;
	end if;

	select 	max(nvl(ie_ident_pacote_ptu, 'N'))
	into	ie_ident_pacote_ptu_w
	from	pls_param_intercambio_scs;

	if	((nr_seq_pacote_w	is not null) and
		(ie_tipo_processo_w	= 'I' and ie_tipo_intercambio_ww = 'I') and
		(ie_ident_pacote_ptu_w = 'S'))then
		:new.ie_pacote_ptu	:= 'S';
	end if;

	if	((nr_seq_pacote_w	is not null) and
		(ie_tipo_processo_w	= 'I' and ie_tipo_intercambio_ww = 'I') )then
		:new.vl_procedimento	:= 0;
		:new.vl_total_pacote	:= 0;
	end if;
end if;

begin
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	pls_requisicao
	where	nr_sequencia = :old.nr_seq_requisicao;
exception
when others then
	cd_estabelecimento_w	:= obter_estabelecimento_ativo;
end;

if	(:new.qt_solicitado <> :old.qt_solicitado) and
	(:new.cd_procedimento = :old.cd_procedimento) and
	(:new.ie_origem_proced = :old.ie_origem_proced) then
	pls_consistir_qtde_proc_req(:new.nr_seq_requisicao,
				:old.cd_procedimento,
				:old.ie_origem_proced,
				:new.qt_solicitado,
				cd_estabelecimento_w,
				:new.nm_usuario,
				ds_mensagem_w,
				qt_solicitada_w,
				qt_permitida_w);

	if	(ds_mensagem_w is not null) and
		(obter_valor_param_usuario(1271, 2, Obter_Perfil_Ativo, :new.nm_usuario, 0) = 'I') and
		((qt_solicitada_w - :old.qt_solicitado) > qt_permitida_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(319010, 'QT_SOLICITADA=' || (qt_solicitada_w - :old.qt_solicitado) || ';QT_PERMITIDA=' || qt_permitida_w);
	end if;
	null;
end if;
-- Tratamento realizado devido ao processo de Duplicar requisicao, onde a rotina de valorizacao de pacote e chamada no  pls_requisicao_proc_insert e nao e mais necessario chamar novamente nesta rotina.
<<final>>
	qt_pacote_w	:= qt_pacote_w;
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_requisicao_proc_Insert
before insert ON TASY.PLS_REQUISICAO_PROC for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realiza a atualizacao do procedimento quanto ao cadastro de pacote. Seta no procedimento a sequencia do pacote e marca o campo IE_PACOTE_PTU no caso de requisicoes de intercambio
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
1 - Tomar muito cuidado com a performance
2 - AO ALTERAR ALGO NESTA ROTINA REFERENTE A PACOTE, DEVE SER AJUSTADO TAMBEM NA TRIGGER PLS_REQUISICAO_PROC_UPDATE
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

nr_seq_pacote_w			Number(10);
ie_regra_preco_w		varchar2(3);
nr_seq_regra_pacote_out_w	number(10);
cd_proc_pacote_out_w		number(15)	:= null;
ie_origem_pacote_out_w		number(15)	:= null;
vl_pacote_out_w			number(15,2)	:= 0;
vl_medico_out_w			number(15,2)	:= 0;
vl_anestesista_out_w		number(15,2)	:= 0;
vl_auxiliares_out_w		number(15,2)	:= 0;
vl_custo_operacional_out_w	number(15,2)	:= 0;
vl_materiais_out_w		number(15,2)	:= 0;
nr_seq_intercambio_w		number(10);
nr_seq_classificacao_prest_w	number(10);
nr_seq_protocolo_w		number(10);
dt_conta_guia_w			date;
nr_seq_segurado_w		number(10);
cd_guia_w			varchar2(30);
sg_estado_w			pessoa_juridica.sg_estado%type;
sg_estado_int_w			pessoa_juridica.sg_estado%type;
nr_seq_prestador_prot_w		number(10);
cd_estabelecimento_w		number(4);
nr_seq_tipo_acomod_w		number(10);
dt_pacote_w			date;
ie_internado_w			varchar2(3);
nr_seq_plano_w			number(10);
nr_contrato_w			number(10);
nr_seq_congenere_w		number(10);
ie_origem_conta_w		varchar2(5)	:= null;
ie_tipo_intercambio_w		varchar2(5);
nr_seq_conta_w			number(10);
nr_seq_prestador_conta_w	number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(15);
nr_seq_prestador_w		number(10);
nr_seq_requisicao_w		number(10);
ie_tipo_processo_w		varchar2(2);
ie_tipo_intercambio_ww		pls_requisicao.ie_tipo_intercambio%type;
ie_ident_pacote_ptu_w		pls_param_intercambio_scs.ie_ident_pacote_ptu%type;
ie_pacote_maior_vl_w		pls_param_requisicao.ie_pacote_maior_vl%type;
ie_tipo_guia_w			pls_requisicao.ie_tipo_guia%type;

begin

select	max(nr_sequencia)
into	nr_seq_pacote_w
from	pls_pacote
where	cd_procedimento		= :new.cd_procedimento
and	ie_origem_proced	= :new.ie_origem_proced
and	ie_situacao		= 'A';

if	(nr_seq_pacote_w	is not null) then
	select	nvl(ie_regra_preco,'N')
	into	ie_regra_preco_w
	from	pls_pacote a
	where	a.nr_sequencia = nr_seq_pacote_w;

	nr_seq_requisicao_w	:= :new.nr_seq_requisicao;

	/*Obter dados da guia*/
	select	nvl(nr_seq_prestador_exec,nr_seq_prestador),
		cd_estabelecimento,
		dt_requisicao,
		nr_seq_tipo_acomodacao,
		nr_seq_plano,
		pls_obter_internado_guia(nr_sequencia,'R'),
		nr_seq_segurado,
		ie_tipo_processo,
		nvl(ie_tipo_intercambio, 'N'),
		ie_tipo_guia
	into	nr_seq_prestador_w,
		cd_estabelecimento_w,
		dt_pacote_w,
		nr_seq_tipo_acomod_w,
		nr_seq_plano_w,
		ie_internado_w,
		nr_seq_segurado_w,
		ie_tipo_processo_w,
		ie_tipo_intercambio_ww,
		ie_tipo_guia_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_w;

	if	(ie_regra_preco_w = 'S') then
		if (nr_seq_tipo_acomod_w is null) then
			nr_seq_tipo_acomod_w := pls_obter_dados_produto(nr_seq_plano_w,'AP');
		end if;

		if	(nvl(nr_seq_plano_w,0) = 0) then
			begin
			select	a.nr_seq_plano
			into	nr_seq_plano_w
			from	pls_segurado			a
			where	a.nr_sequencia = nr_seq_segurado_w;
			exception
			when others then
				nr_seq_plano_w := null;
			end;
		end if;

		begin
		select	nvl(a.nr_contrato,0),
			b.nr_seq_congenere
		into	nr_contrato_w,
			nr_seq_congenere_w
		from	pls_contrato	a,
			pls_segurado	b
		where	a.nr_sequencia	= b.nr_seq_contrato
		and	b.nr_sequencia	= nr_seq_segurado_w;
		exception
		when others then
			nr_contrato_w		:= null;
			nr_seq_congenere_w	:= null;
		end;

		if	(nr_contrato_w is null) then
			nr_contrato_w := 0;
		end if;

		if	(nr_seq_congenere_w is null) then
			begin
				select	b.nr_seq_congenere
				into	nr_seq_congenere_w
				from	pls_segurado	b
				where	b.nr_sequencia	= nr_seq_segurado_w;
			exception
			when others then
				nr_seq_congenere_w	:= null;
			end;
		end if;

		select	max(nr_seq_intercambio)
		into	nr_seq_intercambio_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;

		select	max(nr_seq_classificacao)
		into	nr_seq_classificacao_prest_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prestador_w;

		select	nvl(max(sg_estado),'X')
		into	sg_estado_w
		from	pessoa_juridica
		where	cd_cgc	= (	select	max(cd_cgc_outorgante)
					from	pls_outorgante
					where	cd_estabelecimento	= cd_estabelecimento_w);
		select	nvl(max(a.sg_estado),'X')
		into	sg_estado_int_w
		from	pessoa_juridica	a,
			pls_congenere	b
		where	a.cd_cgc	= b.cd_cgc
		and	b.nr_sequencia	= nr_seq_congenere_w;

		if	(sg_estado_w <> 'X') and
			(sg_estado_int_w <> 'X') then
			if	(sg_estado_w	= sg_estado_int_w) then
				ie_tipo_intercambio_w	:= 'E';
			else
				ie_tipo_intercambio_w	:= 'N';
			end if;
		else
			ie_tipo_intercambio_w	:= 'A';
		end if;

		pls_define_preco_pacote(cd_estabelecimento_w,
					nr_seq_prestador_w,
					nr_seq_tipo_acomod_w,
					dt_pacote_w,
					:new.cd_procedimento,
					:new.ie_origem_proced,
					ie_internado_w,
					nr_seq_plano_w,
					nr_contrato_w,
					nr_seq_congenere_w,
					:new.nm_usuario,
					ie_origem_conta_w,
					ie_tipo_intercambio_w,
					nr_seq_pacote_w,
					nr_seq_regra_pacote_out_w,
					cd_proc_pacote_out_w,
					ie_origem_pacote_out_w,
					vl_pacote_out_w,
					vl_medico_out_w,
					vl_anestesista_out_w,
					vl_auxiliares_out_w,
					vl_custo_operacional_out_w,
					vl_materiais_out_w,
					nr_seq_intercambio_w,
					nr_seq_classificacao_prest_w,
					null,
					nr_seq_segurado_w,
					null,
					'N',
					null,
					1);
	end if;

	begin
		select	nvl(ie_pacote_maior_vl,'N')
		into	ie_pacote_maior_vl_w
		from	pls_param_requisicao;
	exception
	when others then
		ie_pacote_maior_vl_w	:= 'N';
	end;

	if	(ie_pacote_maior_vl_w	= 'S') then

		select	max(nr_sequencia)
		into	nr_seq_pacote_w
		from	pls_pacote
		where	cd_procedimento		= :new.cd_procedimento
		and	ie_origem_proced	= :new.ie_origem_proced
		and	ie_situacao		= 'A';

		if	(nr_seq_pacote_w is not null) then
			select	max(a.nr_sequencia)
			into	nr_seq_regra_pacote_out_w
			from	pls_pacote_tipo_acomodacao a
			where 	a.nr_seq_pacote = nr_seq_pacote_w
			and   	a.vl_pacote       = (	select  max(x.vl_pacote)
							from    pls_pacote_tipo_acomodacao x
							where   x.nr_seq_pacote = nr_seq_pacote_w
							and	((x.dt_fim_vigencia is null) or (trunc(x.dt_fim_vigencia) >= trunc(sysdate))));

			begin
				select	vl_pacote
				into	vl_pacote_out_w
				from	pls_pacote_tipo_acomodacao
				where	nr_sequencia	= nr_seq_regra_pacote_out_w;
			exception
			when others then
				vl_pacote_out_w := null;
			end;

		end if;
	end if;


	if (nr_seq_regra_pacote_out_w is not null) then
		:new.vl_procedimento		:= vl_pacote_out_w;
		:new.nr_seq_pacote		:= nr_seq_pacote_w;
		:new.nr_seq_preco_pacote	:= nr_seq_regra_pacote_out_w;
	end if;

	select 	max(nvl(ie_ident_pacote_ptu, 'N'))
	into	ie_ident_pacote_ptu_w
	from	pls_param_intercambio_scs;

	if	((nr_seq_pacote_w	is not null) and
		(ie_tipo_processo_w	= 'I' and ie_tipo_intercambio_ww = 'I') and
		(ie_ident_pacote_ptu_w = 'S'))then
		:new.ie_pacote_ptu	:= 'S';
	end if;

	if	((nr_seq_pacote_w	is not null) and
		(ie_tipo_processo_w	= 'I' and ie_tipo_intercambio_ww = 'I'))then
		:new.vl_procedimento	:= 0;
		:new.vl_total_pacote	:= 0;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ssj_requisicao_sem_origem_proc
before insert ON TASY.PLS_REQUISICAO_PROC for each row
declare

ie_origem_proced_w      number(10):=0;
ie_tipo_processo_w      varchar2(10);
dados_tipo_conv_tiss_w  pls_cta_valorizacao_pck.dados_tipo_conv_tiss;
nr_seq_guia_plano_imp_w number(10);
nr_seq_guia_plano_pc_w  number(10);
cd_tabela_w             varchar2(10);
cd_procedimento_w       varchar2(10); 
nr_seq_prestador_w      number(10); 
                
/* 
    Caso o sistema tente inserir o registro null na requisil�ao, ent�o ir� automaticamente inserir como CBHPM caso existir
    OBS: Resolver o erro de convers�o
*/
begin

select ie_tipo_processo
into ie_tipo_processo_w
from pls_requisicao
where nr_sequencia = :new.nr_seq_requisicao;

if(ie_tipo_processo_w = 'E')then

    if(:new.ie_origem_proced is null)then

        select  max(a.nr_sequencia)nr_seq_guia_plano_imp,
                max(nr_seq_guia_plano)nr_seq_guia_plano_pc
        into    nr_seq_guia_plano_imp_w,
                nr_seq_guia_plano_pc_w      
        from pls_guia_plano_imp a
        join pls_guia_plano_proc_imp b on(a.nr_sequencia = b.nr_seq_guia_plano_imp)
        where a.nr_seq_requisicao = :new.nr_seq_requisicao
        and b.cd_procedimento = :new.cd_procedimento;


        select  max(cd_tabela),
                max(cd_procedimento),
                max((select a.nr_seq_prestador
                from    pls_guia_plano a
                where   a.nr_sequencia = nr_seq_guia_plano_pc_w))nr_seq_prestador
        into    cd_tabela_w,
                cd_procedimento_w,
                nr_seq_prestador_w
        from    pls_guia_plano_proc_imp
        where   nr_seq_guia_plano_imp = nr_seq_guia_plano_imp_w
        and     cd_procedimento = :new.cd_procedimento;


        dados_tipo_conv_tiss_w := pls_obter_conversao_tab_tiss(cd_tabela_w, 2, cd_procedimento_w, '', 'A', nr_seq_prestador_w, null);


        if(dados_tipo_conv_tiss_w.ie_tipo_tabela in ('06')) then
            ie_origem_proced_w := 5; --CBHPM
        end if;


        if(nvl(ie_origem_proced_w,0) != 0)then
            :new.ie_origem_proced := ie_origem_proced_w;
            insert into temp(A) values ('Inserido: '||dados_tipo_conv_tiss_w.ie_tipo_tabela||'  '||sysdate);
        end if;


    end if;   
    
end if;

end ssj_requisicao_sem_origem_proc;
/


ALTER TABLE TASY.PLS_REQUISICAO_PROC ADD (
  CONSTRAINT PLSRPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REQUISICAO_PROC ADD (
  CONSTRAINT PLSRPRO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSRPRO_PLSREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.PLS_REQUISICAO (NR_SEQUENCIA),
  CONSTRAINT PLSRPRO_PLSMOEX_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_EXC) 
 REFERENCES TASY.PLS_MOTIVO_EXCLUSAO (NR_SEQUENCIA),
  CONSTRAINT PLSRPRO_PLSPACO_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.PLS_PACOTE (NR_SEQUENCIA),
  CONSTRAINT PLSRPRO_PLSTRAB_FK 
 FOREIGN KEY (NR_SEQ_TRATAMENTO) 
 REFERENCES TASY.PLS_TRATAMENTO_BENEF (NR_SEQUENCIA),
  CONSTRAINT PLSRPRO_PLSGUMC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.PLS_GUIA_MOTIVO_CANCEL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REQUISICAO_PROC TO NIVEL_1;

