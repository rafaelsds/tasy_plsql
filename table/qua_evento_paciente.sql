ALTER TABLE TASY.QUA_EVENTO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_EVENTO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_EVENTO_PACIENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_EVENTO              NUMBER(10)         NOT NULL,
  DT_EVENTO                  DATE               NOT NULL,
  IE_CLASSIFICACAO           VARCHAR2(10 BYTE),
  DS_EVENTO                  VARCHAR2(4000 BYTE) NOT NULL,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  DT_CADASTRO                DATE               NOT NULL,
  NM_USUARIO_ORIGEM          VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF_EVENTO      NUMBER(10),
  DT_LIBERACAO               DATE,
  DT_ANALISE                 DATE,
  NM_USUARIO_REG             VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_DISP_PAC            NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_CIRURGIA                NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_PEPO                NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  IE_STATUS                  VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_GRAVIDADE           NUMBER(10),
  IE_ORIGEM                  VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_EVENTO         NUMBER(10),
  NR_SEQ_MOTIVO_CANCEL       NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_RESTRITO_USUARIO        VARCHAR2(1 BYTE),
  IE_TIPO_EVENTO             VARCHAR2(15 BYTE),
  NR_SEQ_MODELO              NUMBER(10),
  NM_USUARIO_ANALISE         VARCHAR2(15 BYTE),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_SEQ_TURNO               NUMBER(10),
  DS_DISPOSICAO_IMEDIATA     VARCHAR2(4000 BYTE),
  NR_SEQ_SAE                 NUMBER(10),
  CD_SETOR_ANALISE           NUMBER(5),
  NR_SEQ_MOTIVO_SEM_RNC      NUMBER(10),
  NR_SEQ_MOTIVO_CONCLUSAO    NUMBER(10),
  CD_EXP_EVENTO              NUMBER(10),
  IE_RN                      VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_TIPO_COMPLICACAO_MX     NUMBER(1),
  CD_FUNCAO_ATIVA            NUMBER(5),
  CD_SETOR_NOTIFICADO        NUMBER(5),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUAEVPA_ATEPACI_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_ATEPADI_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_DISP_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_CATCOMPL_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_TIPO_COMPLICACAO_MX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_CIRURGI_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_DICEXPR_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_EXP_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_ESTABEL_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_FUNCAO_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_FUNCAO_ATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_HDTIPEV_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_TIPO_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_HDTIPEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_I1 ON TASY.QUA_EVENTO_PACIENTE
(DT_EVENTO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_MODELOT_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_MODELOT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_PEPOCIR_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_PEPRESC_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_SAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_PEPRESC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_PERFIL_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_PESFISI_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_PESFISI_FK2_I ON TASY.QUA_EVENTO_PACIENTE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUAEVPA_PK ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_QUAEVEN_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_EVENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_QUAEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_QUAGREV_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_GRAVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_QUAGREV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_QUAMCEP_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_QUAMCEP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_QUAMSNC_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_MOTIVO_SEM_RNC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_QUAMSNC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_RETUEVT_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_RETUEVT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_SETATEN_FK_I ON TASY.QUA_EVENTO_PACIENTE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_SETATEN_FK2_I ON TASY.QUA_EVENTO_PACIENTE
(CD_SETOR_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_SETATEN_FK3_I ON TASY.QUA_EVENTO_PACIENTE
(CD_SETOR_NOTIFICADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUAEVPA_TASASDI_FK_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUAEVPA_TASASDI_FK2_I ON TASY.QUA_EVENTO_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUAEVPA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.QUA_EVENTO_PACIENTE_ATUAL
before insert or update ON TASY.QUA_EVENTO_PACIENTE for each row
declare
ie_anonimo_w	varchar2(1);
qt_reg_w	    number(10);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.cd_pessoa_fisica is null)  then
	:new.cd_pessoa_fisica	:= obter_pessoa_Atendimento(:new.nr_Atendimento,'C');
end if;

if	(nvl(:old.DT_EVENTO,sysdate+10) <> :new.DT_EVENTO) and
	(:new.DT_EVENTO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_EVENTO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.ie_tipo_evento is null) then
	:new.ie_tipo_evento:= 'E';
end if;

if	(:new.ie_tipo_evento in ('E','R')) and
	(:new.cd_pessoa_fisica is null) then
	-- Obrigatorio informar a pessoa!
	Wheb_mensagem_pck.exibir_mensagem_abort(264646);
end if;

if	(:new.ie_tipo_evento = 'S') and
	(:new.cd_setor_atendimento is null) then
	-- Obrigatorio informar o setor!
	Wheb_mensagem_pck.exibir_mensagem_abort(264647);
end if;

if	(:new.nr_seq_evento is not null) then
	select	count(*)
	into	qt_reg_w
	from	qua_evento
	where	nr_sequencia = :new.NR_SEQ_EVENTO
	and	ie_situacao = 'A';
	-- Obrigatorio informar evento ativo!
	if	(qt_reg_w = 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(405672);
	end if;
end if;

if	(:new.cd_setor_analise is null) then
	begin
	select	cd_setor_analise
	into	:new.cd_setor_analise
	from	qua_evento
	where	nr_sequencia	= :new.nr_seq_evento;
	exception
	when others then
	null;
	end;
end if;

select	max(ie_anonimo)
into	ie_anonimo_w
from	qua_evento
where	nr_sequencia = :new.nr_seq_evento;

if	(ie_anonimo_w = 'S' and (:old.dt_liberacao is null and :new.dt_liberacao is not null)) then
	:new.nm_usuario := obter_desc_expressao(710876);
	:new.nm_usuario_nrec := obter_desc_expressao(710876);
	:new.nm_usuario_origem := obter_desc_expressao(710876);
	:new.cd_profissional := null;
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	 Enviar_Comunic_Evento_trigger(	:new.nr_sequencia,
					:new.nr_seq_evento,
					:new.ds_evento,
					:new.cd_pessoa_fisica,
					:new.nr_atendimento,
					:new.IE_CLASSIFICACAO,
					:new.cd_estabelecimento,
					:new.cd_setor_atendimento,
					:new.nm_usuario,
					:new.nr_seq_classif_evento);
end if;

if	(:new.nr_seq_turno is null) then
	:new.nr_seq_turno := Obter_Turno_evento(:new.DT_EVENTO,:new.cd_estabelecimento,'SEQ','EQ');
end if;

if	(inserting) and
	(:new.cd_funcao_ativa is null) then
	:new.cd_funcao_ativa := wheb_usuario_pck.get_cd_funcao;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.qua_evento_pac_pend_atual
after insert or update ON TASY.QUA_EVENTO_PACIENTE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_evento_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Begin
	if	(inserting) or
		(updating) then

        select	max(ie_lib_evento)
        into	ie_lib_evento_w
        from	parametro_medico
        where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

        if	(nvl(ie_lib_evento_w,'N') = 'S') then
            if	(:new.dt_liberacao is null) then
                ie_tipo_w := 'EVT';
            elsif	(:old.dt_liberacao is null) and
                    (:new.dt_liberacao is not null) then
                ie_tipo_w := 'XEVT';
            end if;
            if	(ie_tipo_w	is not null) then
                Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
            end if;
        end if;

     elsif	(deleting) then

		delete	from pep_item_pendente
		where 	IE_TIPO_REGISTRO = 'EVT'
		and	    nr_seq_registro = :old.nr_sequencia
		and	    nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

	end if;


exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.QUA_EVENTO_PACIENTE_tp  after update ON TASY.QUA_EVENTO_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DISPOSICAO_IMEDIATA,1,4000),substr(:new.DS_DISPOSICAO_IMEDIATA,1,4000),:new.nm_usuario,nr_seq_w,'DS_DISPOSICAO_IMEDIATA',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EVENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EVENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EVENTO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO,1,4000),substr(:new.IE_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EVENTO,1,4000),substr(:new.DS_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_EVENTO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_EVENTO,1,4000),substr(:new.NR_SEQ_CLASSIF_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_EVENTO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DISP_PAC,1,4000),substr(:new.NR_SEQ_DISP_PAC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DISP_PAC',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ANALISE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ANALISE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ANALISE',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO_ATIVA,1,4000),substr(:new.CD_FUNCAO_ATIVA,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO_ATIVA',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INATIVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INATIVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INATIVACAO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA,1,4000),substr(:new.DS_JUSTIFICATIVA,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRAVIDADE,1,4000),substr(:new.NR_SEQ_GRAVIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRAVIDADE',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EVENTO,1,4000),substr(:new.NR_SEQ_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EVENTO',ie_log_w,ds_w,'QUA_EVENTO_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.QUA_EVENTO_PACIENTE ADD (
  CONSTRAINT QUAEVPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_EVENTO_PACIENTE ADD (
  CONSTRAINT QUAEVPA_RETUEVT_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.REGRA_TURNO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_SAE) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT QUAEVPA_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ANALISE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT QUAEVPA_QUAMSNC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SEM_RNC) 
 REFERENCES TASY.QUA_MOTIVO_SEM_NC (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_EVENTO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT QUAEVPA_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO_ATIVA) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT QUAEVPA_CATCOMPL_FK 
 FOREIGN KEY (CD_TIPO_COMPLICACAO_MX) 
 REFERENCES TASY.CAT_COMPLICACOES (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_NOTIFICADO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT QUAEVPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUAEVPA_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT QUAEVPA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT QUAEVPA_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT QUAEVPA_QUAGREV_FK 
 FOREIGN KEY (NR_SEQ_GRAVIDADE) 
 REFERENCES TASY.QUA_GRAVIDADE_EVENTO (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_HDTIPEV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EVENTO) 
 REFERENCES TASY.HD_TIPO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_QUAMCEP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.QUA_MOTIVO_CANCEL_EV_PAC (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_MODELOT_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MODELO (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QUAEVPA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT QUAEVPA_QUAEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.QUA_EVENTO (NR_SEQUENCIA),
  CONSTRAINT QUAEVPA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT QUAEVPA_ATEPADI_FK 
 FOREIGN KEY (NR_SEQ_DISP_PAC) 
 REFERENCES TASY.ATEND_PAC_DISPOSITIVO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT QUAEVPA_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA));

GRANT SELECT ON TASY.QUA_EVENTO_PACIENTE TO NIVEL_1;

