ALTER TABLE TASY.PLS_MAT_UNI_SC_PRE_INCONS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MAT_UNI_SC_PRE_INCONS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MAT_UNI_SC_PRE_INCONS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MAT_IMP       NUMBER(10)               NOT NULL,
  DS_INCONSISTENCIA    VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMUSI_PK ON TASY.PLS_MAT_UNI_SC_PRE_INCONS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUSI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUSI_PLSMUPI_FK_I ON TASY.PLS_MAT_UNI_SC_PRE_INCONS
(NR_SEQ_MAT_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUSI_PLSMUPI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_MAT_UNI_SC_PRE_INCONS ADD (
  CONSTRAINT PLSMUSI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MAT_UNI_SC_PRE_INCONS ADD (
  CONSTRAINT PLSMUSI_PLSMUPI_FK 
 FOREIGN KEY (NR_SEQ_MAT_IMP) 
 REFERENCES TASY.PLS_MAT_UNI_SC_PRE_IMP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_MAT_UNI_SC_PRE_INCONS TO NIVEL_1;

