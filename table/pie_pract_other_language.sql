ALTER TABLE TASY.PIE_PRACT_OTHER_LANGUAGE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PIE_PRACT_OTHER_LANGUAGE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PIE_PRACT_OTHER_LANGUAGE
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DS_LANGUAGE       VARCHAR2(40 BYTE)           NOT NULL,
  NR_SEQ_PIE_PRACT  NUMBER(10)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PIEPRALANG_PIEPRACTNR_FK_I ON TASY.PIE_PRACT_OTHER_LANGUAGE
(NR_SEQ_PIE_PRACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PIEPRALANG_PK ON TASY.PIE_PRACT_OTHER_LANGUAGE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PIE_PRACT_OTHER_LANGUAGE ADD (
  CONSTRAINT PIEPRALANG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PIE_PRACT_OTHER_LANGUAGE ADD (
  CONSTRAINT PIEPRALANG_PIEPRACTNR_FK 
 FOREIGN KEY (NR_SEQ_PIE_PRACT) 
 REFERENCES TASY.PIE_PRACTITIONER (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PIE_PRACT_OTHER_LANGUAGE TO NIVEL_1;

