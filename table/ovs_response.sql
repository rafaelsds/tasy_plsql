ALTER TABLE TASY.OVS_RESPONSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OVS_RESPONSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.OVS_RESPONSE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CLAIM         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TRANSACTION       VARCHAR2(40 BYTE),
  CD_STATUS            VARCHAR2(10 BYTE),
  NR_ACCOUNT           VARCHAR2(20 BYTE),
  CD_CLM_ASSES_FUND    VARCHAR2(10 BYTE),
  CD_CLAIM             VARCHAR2(10 BYTE),
  CD_FUND_STATUS       NUMBER(10),
  NM_FAMILY            VARCHAR2(40 BYTE),
  NM_FIRST             VARCHAR2(40 BYTE),
  CD_PROCESS_STATUS    VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OVSRESP_OVSCLAIM_FK_I ON TASY.OVS_RESPONSE
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OVSRESP_PK ON TASY.OVS_RESPONSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OVS_RESPONSE ADD (
  CONSTRAINT OVSRESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.OVS_RESPONSE ADD (
  CONSTRAINT OVSRESP_OVSCLAIM_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.OVS_CLAIM (NR_SEQUENCIA));

GRANT SELECT ON TASY.OVS_RESPONSE TO NIVEL_1;

