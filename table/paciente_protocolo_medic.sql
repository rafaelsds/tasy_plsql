ALTER TABLE TASY.PACIENTE_PROTOCOLO_MEDIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_PROTOCOLO_MEDIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_PROTOCOLO_MEDIC
(
  NR_SEQ_PACIENTE            NUMBER(10)         NOT NULL,
  NR_SEQ_MATERIAL            NUMBER(6)          NOT NULL,
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  QT_DOSE                    NUMBER(15,4)       NOT NULL,
  CD_UNIDADE_MEDIDA          VARCHAR2(30 BYTE)  NOT NULL,
  DS_DIAS_APLICACAO          VARCHAR2(4000 BYTE) NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_AGRUPAMENTO             NUMBER(6)          NOT NULL,
  DS_RECOMENDACAO            VARCHAR2(2000 BYTE),
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE),
  NR_SEQ_DILUICAO            NUMBER(6),
  QT_MIN_APLICACAO           NUMBER(4),
  IE_BOMBA_INFUSAO           VARCHAR2(1 BYTE)   NOT NULL,
  CD_INTERVALO               VARCHAR2(7 BYTE),
  QT_HORA_APLICACAO          NUMBER(3),
  QT_DIAS_UTIL               NUMBER(3),
  NR_SEQ_INTERNO             NUMBER(10)         NOT NULL,
  IE_SE_NECESSARIO           VARCHAR2(1 BYTE),
  IE_APLIC_LENTA             VARCHAR2(1 BYTE),
  IE_URGENCIA                VARCHAR2(1 BYTE),
  IE_APLIC_BOLUS             VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  CD_UNID_MED_PRESCR         VARCHAR2(30 BYTE),
  QT_DOSE_PRESCR             NUMBER(15,4),
  CD_PROTOCOLO               NUMBER(10),
  NR_SEQ_MEDICACAO           NUMBER(6),
  IE_PRE_MEDICACAO           VARCHAR2(1 BYTE),
  IE_ALERTA_VIA_ADM          VARCHAR2(1 BYTE),
  DS_CICLOS_APLICACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_SOLUCAO             NUMBER(10),
  CD_PROTOCOLO_ADIC          NUMBER(10),
  NR_SEQ_MEDICACAO_ADIC      NUMBER(6),
  IE_AGRUPADOR               NUMBER(2),
  PR_REDUCAO                 VARCHAR2(3 BYTE),
  IE_GERAR_SOLUCAO           VARCHAR2(1 BYTE),
  IE_TIPO_DOSAGEM            VARCHAR2(3 BYTE),
  QT_DOSAGEM                 NUMBER(15,4),
  NR_SEQ_RECONSTITUINTE      NUMBER(10),
  IE_APLICA_REDUCAO          VARCHAR2(1 BYTE),
  IE_ZERADO                  VARCHAR2(1 BYTE),
  IE_REGRA_DILUICAO_CAD_MAT  VARCHAR2(1 BYTE),
  NR_SEQ_PROCEDIMENTO        NUMBER(6),
  IE_LOCAL_ADM               VARCHAR2(1 BYTE),
  NR_SEQ_PROT_MED            NUMBER(10),
  NR_SEQ_ORDEM_ADEP          NUMBER(10),
  IE_USO_CONTINUO            VARCHAR2(1 BYTE),
  NR_SEQ_MEDIC_MATERIAL      NUMBER(6),
  DS_DOSE_DIFERENCIADA       VARCHAR2(50 BYTE),
  NR_SEQ_MAT_DILUICAO        NUMBER(10),
  DS_JUSTIF_INCONS_QUIMIO    VARCHAR2(255 BYTE),
  IE_ACM                     VARCHAR2(1 BYTE),
  QT_DIAS_RECEITA            NUMBER(5),
  IE_OBJETIVO                VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(2000 BYTE),
  CD_TOPOGRAFIA_CIH          NUMBER(10),
  CD_MICROORGANISMO_CIH      NUMBER(10),
  CD_AMOSTRA_CIH             NUMBER(10),
  IE_ORIGEM_INFECCAO         VARCHAR2(1 BYTE),
  IE_MEDICACAO_PACIENTE      VARCHAR2(1 BYTE),
  IE_INF_RELEVANTE           NUMBER(4),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  DT_LIBERACAO               DATE,
  QT_FATOR_CORRECAO          NUMBER(10,3),
  IE_PESO_CONSIDERAR         VARCHAR2(1 BYTE),
  QT_DOSE_REF                NUMBER(15,4),
  CD_UNID_MEDIDA_REF         VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACPRME_ATCONSPEPA_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_CIHAMCU_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_AMOSTRA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_CIHMICR_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_MICROORGANISMO_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_CIHTOPO_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_TOPOGRAFIA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_INTERNO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_INTPRES_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_INTERVALO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_I2 ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_RECONSTITUINTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_I2
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_I3 ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PROT_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_I3
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_MATERIA_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_PACPRME_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PACIENTE, NR_SEQ_DILUICAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_PACPRME_FK2_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PACIENTE, NR_SEQ_MEDIC_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_PACPRPR_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PACIENTE, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_PACPRPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_PACPRSO_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PACIENTE, NR_SEQ_SOLUCAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_PACPRSO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_PACSETO_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PACIENTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_PACSETO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PACPRME_PK ON TASY.PACIENTE_PROTOCOLO_MEDIC
(NR_SEQ_PACIENTE, NR_SEQ_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPRME_PROMEDI_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_PROMEDI_FK2_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_PROTOCOLO_ADIC, NR_SEQ_MEDICACAO_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_PROMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_UNIMEDI_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_UNIDADE_MEDIDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_UNIMEDI_FK2_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(CD_UNID_MED_PRESCR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPRME_VIAAPLI_FK_I ON TASY.PACIENTE_PROTOCOLO_MEDIC
(IE_VIA_APLICACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPRME_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.paciente_protocolo_medic_refs
before insert ON TASY.PACIENTE_PROTOCOLO_MEDIC for each row
declare

begin
	if (:new.cd_unid_medida_ref is null) then
		:new.cd_unid_medida_ref := :new.cd_unid_med_prescr;
	end if;

	if (:new.qt_dose_ref is null) then
		:new.qt_dose_ref := :new.qt_dose_prescr;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_PROTOCOLO_MEDIC_tp  after update ON TASY.PACIENTE_PROTOCOLO_MEDIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_SEQ_PACIENTE='||to_char(:old.NR_SEQ_PACIENTE)||'#@#@NR_SEQ_MATERIAL='||to_char(:old.NR_SEQ_MATERIAL); ds_w:=substr(:new.QT_DOSE_PRESCR,1,500);gravar_log_alteracao(substr(:old.QT_DOSE_PRESCR,1,4000),substr(:new.QT_DOSE_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_PRESCR',ie_log_w,ds_w,'PACIENTE_PROTOCOLO_MEDIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_protocolo_medic_atual
before insert or update ON TASY.PACIENTE_PROTOCOLO_MEDIC for each row
declare
qt_minuto_aplicacao_w	number(10);
qt_hora_aplicacao_w	number(10);
ds_restricao_medic_onc_w 	varchar(100);
ie_acao_w  varchar(255);
ie_param_usuario_w varchar2(1) := 'N';

begin
Abortar_Se_Protoc_Inativo(:new.nr_seq_paciente);

if (:new.IE_VIA_APLICACAO <> :old.IE_VIA_APLICACAO) then
	:new.IE_ALERTA_VIA_ADM	:= 'N';
end if;

if (inserting)  or (:new.cd_material <> :old.cd_material) then
	Consiste_Regra_Mat_quimio(:new.cd_material);
end if;

Obter_Param_Usuario(281, 1553, Obter_Perfil_Ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_param_usuario_w);
    if (ie_param_usuario_w <>'N') then
         if(inserting) then
                    Gravar_Log_Alt_Prot_Onc(
                  :new.nr_seq_paciente,
                  :new.cd_material,
                  :new.nm_usuario,
                  'PACIENTE_PROTOCOLO_MEDIC',
                  'QT_DOSE',
                  nvl(:old.QT_DOSE,0),
                  :new.QT_DOSE,
                  'I');

     elsif (updating) then

if (:OLD.cd_material <> :NEW.cd_material) THEN
        Gravar_Log_Alt_Prot_Onc(
          :new.nr_seq_paciente,
          :new.cd_material,
          :new.nm_usuario,
          'PACIENTE_PROTOCOLO_MEDIC',
          'CD_MATERIAL',
          :old.cd_material,
          :new.cd_material,
           'A');
End if;

if (:OLD.QT_DOSE <> :NEW.QT_DOSE) THEN
     Gravar_Log_Alt_Prot_Onc(
          :new.nr_seq_paciente,
          :new.cd_material,
          :new.nm_usuario,
          'PACIENTE_PROTOCOLO_MEDIC',
          'QT_DOSE',
          :old.QT_DOSE,
          :new.QT_DOSE,
          'A');
End if;
if (:OLD.QT_DOSE_PRESCR <> :NEW.QT_DOSE_PRESCR) THEN
      Gravar_Log_Alt_Prot_Onc(
          :new.nr_seq_paciente,
          :new.cd_material,
          :new.nm_usuario,
          'PACIENTE_PROTOCOLO_MEDIC',
           'QT_DOSE_PRESCR',
           :old.QT_DOSE_PRESCR,
           :new.QT_DOSE_PRESCR,
           'A');
End if;

if (:OLD.DS_DIAS_APLICACAO <> :NEW.DS_DIAS_APLICACAO) THEN
Gravar_Log_Alt_Prot_Onc(
              :new.nr_seq_paciente,
              :new.cd_material,
              :new.nm_usuario,
              'PACIENTE_PROTOCOLO_MEDIC',
              'DS_DIAS_APLICACAO',
              :old.DS_DIAS_APLICACAO,
              :new.DS_DIAS_APLICACAO,
               'A');
 End if;
        end if;
        end if;
if ((inserting) or
	(:new.cd_material <> :old.cd_material)) and
	(:new.cd_material > 0) and
	(:new.QT_HORA_APLICACAO is null) and
	(:new.QT_MIN_APLICACAO is null) then

	select	nvl(max(QT_MIN_APLICACAO),0)
	into	qt_minuto_aplicacao_w
	from	material
	where	cd_material	= :new.cd_material;

	if (qt_minuto_aplicacao_w > 0) then
		if (qt_minuto_aplicacao_w < 60) then
			qt_hora_aplicacao_w	:= null;
		elsif (qt_minuto_aplicacao_w = 60) then
			qt_hora_aplicacao_w	:= 1;
			qt_minuto_aplicacao_w	:= null;
		else
			qt_hora_aplicacao_w	:= trunc(dividir(qt_minuto_aplicacao_w,60));
			qt_minuto_aplicacao_w	:= (qt_minuto_aplicacao_w - (qt_hora_aplicacao_w * 60));
		end if;
	else
		if (:new.IE_VIA_APLICACAO is not null) then
			select	max(qt_hora_aplicacao),
				max(qt_minuto_aplicacao)
			into	qt_hora_aplicacao_w,
				qt_minuto_aplicacao_w
			from	material_prescr
			where	cd_material = :new.cd_material
			and	nvl(ie_via_aplicacao, nvl(:new.ie_via_aplicacao,'0')) = nvl(:new.ie_via_aplicacao,'0');
		end if;
	end if;

	if (qt_minuto_aplicacao_w = 0) then
		qt_minuto_aplicacao_w := null;
	end if;

	if (nvl(qt_minuto_aplicacao_w,0) > 0) or
		(nvl(qt_hora_aplicacao_w,0) > 0) then
		:new.QT_HORA_APLICACAO	:= qt_hora_aplicacao_w;
		:new.QT_MIN_APLICACAO	:= qt_minuto_aplicacao_w;
	end if;
end if;

if (:new.pr_reducao	is not null) and
	((NVL(:new.pr_reducao,0)	<> NVL(:old.pr_reducao,0))) and
	(nvl(:new.ie_aplica_reducao,'S') = 'S') then
	:new.QT_DOSE_PRESCR	:= (((nvl(:new.pr_reducao,100) * nvl(:new.QT_DOSE_PRESCR,0)) / nvl(:old.pr_reducao,100)));
end if;

if (:new.cd_unid_medida_ref is null) then
	:new.cd_unid_medida_ref := :new.cd_unid_med_prescr;
end if;

if (:new.qt_dose_ref is null) then
	:new.qt_dose_ref := :new.qt_dose_prescr;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_protocolo_medic_del
after delete ON TASY.PACIENTE_PROTOCOLO_MEDIC for each row
declare

begin
if (1 = 2) then
	delete from 	PACIENTE_ATENDIMENTO_ERRO;
end if;
end;
/


ALTER TABLE TASY.PACIENTE_PROTOCOLO_MEDIC ADD (
  CONSTRAINT PACPRME_PK
 PRIMARY KEY
 (NR_SEQ_PACIENTE, NR_SEQ_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_PROTOCOLO_MEDIC ADD (
  CONSTRAINT PACPRME_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACPRME_CIHMICR_FK 
 FOREIGN KEY (CD_MICROORGANISMO_CIH) 
 REFERENCES TASY.CIH_MICROORGANISMO (CD_MICROORGANISMO),
  CONSTRAINT PACPRME_CIHTOPO_FK 
 FOREIGN KEY (CD_TOPOGRAFIA_CIH) 
 REFERENCES TASY.CIH_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT PACPRME_CIHAMCU_FK 
 FOREIGN KEY (CD_AMOSTRA_CIH) 
 REFERENCES TASY.CIH_AMOSTRA_CULTURA (CD_AMOSTRA_CULTURA),
  CONSTRAINT PACPRME_PACPRME_FK2 
 FOREIGN KEY (NR_SEQ_PACIENTE, NR_SEQ_MEDIC_MATERIAL) 
 REFERENCES TASY.PACIENTE_PROTOCOLO_MEDIC (NR_SEQ_PACIENTE,NR_SEQ_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACPRME_PACPRSO_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE, NR_SEQ_SOLUCAO) 
 REFERENCES TASY.PACIENTE_PROTOCOLO_SOLUC (NR_SEQ_PACIENTE,NR_SEQ_SOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT PACPRME_PROMEDI_FK2 
 FOREIGN KEY (CD_PROTOCOLO_ADIC, NR_SEQ_MEDICACAO_ADIC) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT PACPRME_PACPRME_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE, NR_SEQ_DILUICAO) 
 REFERENCES TASY.PACIENTE_PROTOCOLO_MEDIC (NR_SEQ_PACIENTE,NR_SEQ_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACPRME_PACSETO_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE) 
 REFERENCES TASY.PACIENTE_SETOR (NR_SEQ_PACIENTE)
    ON DELETE CASCADE,
  CONSTRAINT PACPRME_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PACPRME_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PACPRME_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MED_PRESCR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PACPRME_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT PACPRME_PACPRPR_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE, NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PACIENTE_PROTOCOLO_PROC (NR_SEQ_PACIENTE,NR_SEQ_PROCEDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PACPRME_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT PACPRME_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PACIENTE_PROTOCOLO_MEDIC TO NIVEL_1;

