ALTER TABLE TASY.PLS_PARAMETRO_ESCRIT_QUOTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PARAMETRO_ESCRIT_QUOTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PARAMETRO_ESCRIT_QUOTA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TRANS_FIN_REAJ  NUMBER(10)             NOT NULL,
  NR_SEQ_RELATORIO       NUMBER(10),
  NR_SEQ_CLASSE          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPEQU_CLATIRE_FK_I ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA
(NR_SEQ_CLASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPEQU_ESTABEL_FK_I ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPEQU_PK ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPEQU_RELATOR_FK_I ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPEQU_TRAFINA_FK_I ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA
(NR_SEQ_TRANS_FIN_REAJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.controla_registros_transacao
before insert ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OPS - Escritura��es de Quotas de Cooperados
Finalidade: N�o permitir que tenha mais de um registro na tabela pls_parametro_escrit_quota
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
{X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: OS 693236.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare

cont_w   number(2) := 0;

begin

 select nvl(count(*),0)
 into   cont_w
 from  pls_parametro_escrit_quota;

 if	(cont_w >= 1) then
	 begin
 	 wheb_mensagem_pck.exibir_mensagem_abort(281755);
	 end;
 end if;

end;
/


ALTER TABLE TASY.PLS_PARAMETRO_ESCRIT_QUOTA ADD (
  CONSTRAINT PLSPEQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PARAMETRO_ESCRIT_QUOTA ADD (
  CONSTRAINT PLSPEQU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPEQU_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_REAJ) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSPEQU_CLATIRE_FK 
 FOREIGN KEY (NR_SEQ_CLASSE) 
 REFERENCES TASY.CLASSE_TITULO_RECEBER (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PARAMETRO_ESCRIT_QUOTA TO NIVEL_1;

