ALTER TABLE TASY.PE_PRESCR_DIAG_FAT_RIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_PRESCR_DIAG_FAT_RIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_PRESCR_DIAG_FAT_RIS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DIAG          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FAT_RIS       NUMBER(10)               NOT NULL,
  IE_FREE_ITEM         VARCHAR2(1 BYTE),
  DS_NOTES             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPRDFRI_PECADFRI_FK_I ON TASY.PE_PRESCR_DIAG_FAT_RIS
(NR_SEQ_FAT_RIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRDFRI_PEPREDI_FK_I ON TASY.PE_PRESCR_DIAG_FAT_RIS
(NR_SEQ_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPRDFRI_PK ON TASY.PE_PRESCR_DIAG_FAT_RIS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PE_PRESCR_DIAG_FAT_RIS ADD (
  CONSTRAINT PEPRDFRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PE_PRESCR_DIAG_FAT_RIS ADD (
  CONSTRAINT PEPRDFRI_PECADFRI_FK 
 FOREIGN KEY (NR_SEQ_FAT_RIS) 
 REFERENCES TASY.PE_CAD_FATOR_RISCO (NR_SEQUENCIA),
  CONSTRAINT PEPRDFRI_PEPREDI_FK 
 FOREIGN KEY (NR_SEQ_DIAG) 
 REFERENCES TASY.PE_PRESCR_DIAG (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PE_PRESCR_DIAG_FAT_RIS TO NIVEL_1;

