ALTER TABLE TASY.TRE_EVENTO_MODULO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRE_EVENTO_MODULO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRE_EVENTO_MODULO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_EVENTO        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MODULO        NUMBER(10)               NOT NULL,
  QT_CARGA_HORARIA     NUMBER(13,2),
  DT_INICIO            DATE,
  DT_TERMINO           DATE,
  CD_PALESTRANTE       VARCHAR2(10 BYTE),
  IE_STATUS_MODULO     VARCHAR2(3 BYTE),
  PR_REAL              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TREEVMO_PESFISI_FK_I ON TASY.TRE_EVENTO_MODULO
(CD_PALESTRANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TREEVMO_PK ON TASY.TRE_EVENTO_MODULO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TREEVMO_TRECUMO_FK_I ON TASY.TRE_EVENTO_MODULO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TREEVMO_TREEVEN_FK_I ON TASY.TRE_EVENTO_MODULO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TREEVMO_TREEVEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TRE_EVENTO_MODULO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.TRE_EVENTO_MODULO FOR EACH ROW
DECLARE

dt_inicio_w				date;
dt_final_w				date;
dt_inicio_real_w			date;
dt_fim_real_w				date;
dt_consist_modulo_w			varchar(1);

begin

obter_param_usuario(7004,32,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, dt_consist_modulo_w);

select	max(dt_inicio),
	max(dt_fim),
	max(dt_inicio_real),
	max(dt_fim_real)
into	dt_inicio_w,
	dt_final_w,
	dt_inicio_real_w,
	dt_fim_real_w
from	tre_evento
where	nr_sequencia		= :new.nr_seq_evento;

if	(dt_consist_modulo_w = 'P') then
	begin
		if	(:new.dt_inicio  < dt_inicio_w)  or
			(:new.dt_inicio  > dt_final_w) then
				Wheb_mensagem_pck.exibir_mensagem_abort(262012); -- A data do conte�do deve estar dentro do per�odo definido no m�dulo!
		end if;

		if	(:new.dt_termino  < dt_inicio_w)  or
			(:new.dt_termino  > dt_final_w) then
			Wheb_mensagem_pck.exibir_mensagem_abort(262013); -- A data do conte�do deve estar dentro do per�odo definido no m�dulo!
		end if;
	end;
elsif (dt_consist_modulo_w = 'R') then
		if	(:new.dt_inicio  < dt_inicio_real_w)  or
			(:new.dt_inicio  > dt_fim_real_w) then
				Wheb_mensagem_pck.exibir_mensagem_abort(262012); -- A data do conte�do deve estar dentro do per�odo definido no m�dulo!
		end if;

		if	(:new.dt_termino  < dt_inicio_real_w)  or
			(:new.dt_termino  > dt_fim_real_w) then
			Wheb_mensagem_pck.exibir_mensagem_abort(262013); -- A data do conte�do deve estar dentro do per�odo definido no m�dulo!
		end if;
end if;
end;
/


ALTER TABLE TASY.TRE_EVENTO_MODULO ADD (
  CONSTRAINT TREEVMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRE_EVENTO_MODULO ADD (
  CONSTRAINT TREEVMO_TREEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.TRE_EVENTO (NR_SEQUENCIA),
  CONSTRAINT TREEVMO_TRECUMO_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.TRE_CURSO_MODULO (NR_SEQUENCIA),
  CONSTRAINT TREEVMO_PESFISI_FK 
 FOREIGN KEY (CD_PALESTRANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.TRE_EVENTO_MODULO TO NIVEL_1;

