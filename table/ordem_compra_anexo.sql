ALTER TABLE TASY.ORDEM_COMPRA_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_ORDEM_COMPRA      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  DS_TITULO            VARCHAR2(255 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  IE_ANEXAR_EMAIL      VARCHAR2(1 BYTE)         NOT NULL,
  DS_ARQUIVO_BANCO     BLOB,
  IE_DOC_ORIGEM        VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_ARQUIVO_BANCO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORDCANE_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA_ANEXO
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORDCANE_PK ON TASY.ORDEM_COMPRA_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ANEXO_tp  after update ON TASY.ORDEM_COMPRA_ANEXO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ANEXAR_EMAIL,1,4000),substr(:new.IE_ANEXAR_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ANEXAR_EMAIL',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ARQUIVO,1,4000),substr(:new.DS_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ANEXO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Ordem_Compra_Anexo_Atual
BEFORE UPDATE ON TASY.ORDEM_COMPRA_ANEXO FOR EACH ROW
DECLARE

dt_aprovacao_w			ordem_compra.dt_aprovacao%type;
cd_estabelecimento_w		ordem_compra.cd_estabelecimento%type;
ie_altera_anexo_w		varchar2(15);

begin

select	dt_aprovacao,
	cd_estabelecimento
into	dt_aprovacao_w,
	cd_estabelecimento_w
from	ordem_compra
where	nr_ordem_compra = :old.nr_ordem_compra;

select	(max(obter_valor_param_usuario(917, 231, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w)))
into	ie_altera_anexo_w
from	dual;

if	(dt_aprovacao_w is not null) and
	(nvl(ie_altera_anexo_w,'N') = 'N') and
	((:old.DS_ARQUIVO <> :new.DS_ARQUIVO) or (:old.DS_TITULO <> :new.DS_TITULO) or (:old.DS_OBSERVACAO <> :new.DS_OBSERVACAO)) then

	wheb_mensagem_pck.exibir_mensagem_abort(352022);
end if;

END;
/


ALTER TABLE TASY.ORDEM_COMPRA_ANEXO ADD (
  CONSTRAINT ORDCANE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_ANEXO ADD (
  CONSTRAINT ORDCANE_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_COMPRA_ANEXO TO NIVEL_1;

