ALTER TABLE TASY.PLS_EQUIPAMENTO_PREST_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EQUIPAMENTO_PREST_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EQUIPAMENTO_PREST_ARQ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EQUIP_PREST   NUMBER(10),
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSEPAR_PK ON TASY.PLS_EQUIPAMENTO_PREST_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEPAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSEPAR_PLSEQPR_FK_I ON TASY.PLS_EQUIPAMENTO_PREST_ARQ
(NR_SEQ_EQUIP_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEPAR_PLSEQPR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_EQUIPAMENTO_PREST_ARQ ADD (
  CONSTRAINT PLSEPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EQUIPAMENTO_PREST_ARQ ADD (
  CONSTRAINT PLSEPAR_PLSEQPR_FK 
 FOREIGN KEY (NR_SEQ_EQUIP_PREST) 
 REFERENCES TASY.PLS_EQUIPAMENTO_PRESTADOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_EQUIPAMENTO_PREST_ARQ TO NIVEL_1;

