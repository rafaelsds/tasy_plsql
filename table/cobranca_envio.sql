ALTER TABLE TASY.COBRANCA_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COBRANCA_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COBRANCA_ENVIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_COBRANCA      NUMBER(10)               NOT NULL,
  DT_ENVIO             DATE                     NOT NULL,
  DS_DESTINO           VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COBRENV_COBRANC_FK_I ON TASY.COBRANCA_ENVIO
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRENV_COBRANC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COBRENV_PK ON TASY.COBRANCA_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cobranca_envio_insert
after insert ON TASY.COBRANCA_ENVIO for each row
declare

nr_seq_notif_pagador_w		pls_notificacao_pagador.nr_sequencia%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  nvl(max(b.nr_sequencia),0)
into	nr_seq_notif_pagador_w
from	cobranca	d,
	pls_notificacao_item    c,
	pls_notificacao_pagador	b,
	pls_notificacao_lote    a
where	a.nr_sequencia = b.nr_seq_lote
and	b.nr_sequencia = c.nr_seq_notific_pagador
and	c.nr_sequencia = d.nr_seq_notific_item
and	d.nr_sequencia = :new.nr_seq_cobranca;

if (nr_seq_notif_pagador_w > 0) then
	pls_registrar_receb_notif(nr_seq_notif_pagador_w,
			:new.ds_destino,
			sysdate,
			'E');
end if;

end if;

end;
/


ALTER TABLE TASY.COBRANCA_ENVIO ADD (
  CONSTRAINT COBRENV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COBRANCA_ENVIO ADD (
  CONSTRAINT COBRENV_COBRANC_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.COBRANCA_ENVIO TO NIVEL_1;

