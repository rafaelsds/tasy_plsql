ALTER TABLE TASY.NUT_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_PACIENTE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_ATENDIMENTO            NUMBER(10),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_PESO                   NUMBER(6,3)         NOT NULL,
  QT_IDADE_ANO              NUMBER(3)           NOT NULL,
  QT_ALTURA_CM              NUMBER(3)           NOT NULL,
  NR_SEQ_FATOR_ATIV         NUMBER(10)          NOT NULL,
  NR_SEQ_FATOR_STRESS       NUMBER(10)          NOT NULL,
  PR_PROTEINA               NUMBER(3)           NOT NULL,
  PR_LIPIDIO                NUMBER(3)           NOT NULL,
  PR_CARBOIDRATO            NUMBER(3)           NOT NULL,
  QT_KCAL_PROT              NUMBER(5)           NOT NULL,
  QT_KCAL_LIPIDIO           NUMBER(5)           NOT NULL,
  QT_KCAL_CARBOIDRATO       NUMBER(5)           NOT NULL,
  QT_KCAL_TOTAL             NUMBER(5)           NOT NULL,
  IE_NPT                    VARCHAR2(1 BYTE)    NOT NULL,
  IE_PARENTERAL             VARCHAR2(1 BYTE)    NOT NULL,
  QT_KCAL_KG                NUMBER(5,1)         NOT NULL,
  QT_GRAMA_PROT_KG_DIA      NUMBER(5,1)         NOT NULL,
  PR_NPT                    NUMBER(5,2)         NOT NULL,
  QT_FASE_NPT               NUMBER(2)           NOT NULL,
  QT_GOTEJAMENTO_NPT        NUMBER(4),
  NR_PRESCRICAO             NUMBER(14)          NOT NULL,
  QT_DIA_NPT                NUMBER(5),
  QT_IDADE_MES              NUMBER(15),
  QT_IDADE_DIA              NUMBER(15),
  QT_NEC_HIDRICA_DIARIA     NUMBER(5),
  QT_APORTE_HIDRICO_DIARIO  NUMBER(5,2),
  QT_G_LIP_KG_DIA           NUMBER(5,1),
  QT_G_CH_KG_DIA            NUMBER(5,1),
  QT_VEL_INF_GLICOSE        NUMBER(5,2),
  QT_NEC_KCAL_KG_DIA        NUMBER(5,1),
  PR_CONC_GLIC_SOLUCAO      NUMBER(5,1),
  QT_REL_CAL_NIT            NUMBER(5),
  IE_BOMBA_INFUSAO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_CALC_AUTOMATICO        VARCHAR2(1 BYTE),
  DT_STATUS                 DATE,
  IE_STATUS                 VARCHAR2(3 BYTE),
  IE_SUSPENSO               VARCHAR2(1 BYTE),
  QT_VOLUME_ADEP            NUMBER(15,3),
  DT_SUSPENSAO              DATE,
  NM_USUARIO_SUSP           VARCHAR2(15 BYTE),
  IE_FORMA_INFUSAO          VARCHAR2(1 BYTE),
  CD_SETOR_EXEC_INIC        NUMBER(5),
  CD_SETOR_EXEC_FIM         NUMBER(5),
  IE_NPT_ADULTA             VARCHAR2(3 BYTE),
  CD_PERFIL_ATIVO           NUMBER(5),
  NR_SEQ_ASSINATURA         NUMBER(10),
  IE_PERMITE_ALTERACAO      VARCHAR2(1 BYTE),
  NR_SEQ_INTERF_FARM        NUMBER(10),
  QT_FATOR_ATIV             NUMBER(15,4),
  QT_FATOR_STRESS           NUMBER(15,4),
  QT_IMC                    NUMBER(4),
  NR_PRESCRICAO_ORIGINAL    NUMBER(15),
  HR_PRIM_HORARIO           VARCHAR2(5 BYTE),
  QT_GR_PROT                NUMBER(4),
  QT_GR_LIPIDIO             NUMBER(4),
  QT_GR_CARBOIDRATO         NUMBER(4),
  DT_EMISSAO                DATE,
  NR_SEQ_ASSINATURA_SUSP    NUMBER(10),
  QT_VOLUME_TOTAL           NUMBER(15,2),
  CD_PROTOCOLO              NUMBER(10),
  NR_SEQ_PROTOCOLO          NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUTPACI_ATEPACI_FK_I ON TASY.NUT_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACI_NUTFAAT_FK_I ON TASY.NUT_PACIENTE
(NR_SEQ_FATOR_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACI_NUTFAAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACI_NUTFAST_FK_I ON TASY.NUT_PACIENTE
(NR_SEQ_FATOR_STRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACI_NUTFAST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACI_PERFIL_FK_I ON TASY.NUT_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACI_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NUTPACI_PK ON TASY.NUT_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACI_PRESMED_FK_I ON TASY.NUT_PACIENTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTPACI_SETATEN_FK_I ON TASY.NUT_PACIENTE
(CD_SETOR_EXEC_INIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACI_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACI_SETATEN_FK2_I ON TASY.NUT_PACIENTE
(CD_SETOR_EXEC_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACI_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTPACI_TASASDI_FK_I ON TASY.NUT_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTPACI_TASASDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.nut_paciente_update_hl7_dixtal
AFTER UPDATE ON TASY.NUT_PACIENTE FOR EACH ROW
declare

nr_seq_interno_w		number(10);
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	number(10);
ie_leito_monit		varchar2(1);

BEGIN

if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') then

	ds_sep_bv_w := obter_separador_bv;

	select	nr_atendimento, cd_pessoa_fisica
	into	nr_atendimento_w, cd_pessoa_fisica_w
	from 	prescr_medica
	where 	nr_prescricao = :new.nr_prescricao;

	select	max(obter_atepacu_paciente(nr_atendimento_w ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(nr_atendimento_w),'N')
	into	ie_leito_monit
	from 	dual;

	if (ie_leito_monit = 'S') then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
					'nr_atendimento='   || nr_atendimento_w      || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
					'nr_seq_nut_pac='   || :new.nr_sequencia     || ds_sep_bv_w;
		gravar_agend_integracao(50, ds_param_integ_hl7_w);
		end;
	end if;

end if;

END;
/


ALTER TABLE TASY.NUT_PACIENTE ADD (
  CONSTRAINT NUTPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_PACIENTE ADD (
  CONSTRAINT NUTPACI_NUTFAAT_FK 
 FOREIGN KEY (NR_SEQ_FATOR_ATIV) 
 REFERENCES TASY.NUT_FATOR_ATIV (NR_SEQUENCIA),
  CONSTRAINT NUTPACI_NUTFAST_FK 
 FOREIGN KEY (NR_SEQ_FATOR_STRESS) 
 REFERENCES TASY.NUT_FATOR_STRESS (NR_SEQUENCIA),
  CONSTRAINT NUTPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT NUTPACI_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT NUTPACI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXEC_INIC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NUTPACI_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_EXEC_FIM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NUTPACI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT NUTPACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.NUT_PACIENTE TO NIVEL_1;

