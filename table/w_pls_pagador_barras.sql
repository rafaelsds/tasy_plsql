DROP TABLE TASY.W_PLS_PAGADOR_BARRAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_PAGADOR_BARRAS
(
  NR_SEQ_NOTIFICACAO      NUMBER(10),
  NM_PESSOA_RECEB         VARCHAR2(255 BYTE),
  DT_RECEBIMENTO          DATE,
  NR_SEQ_VINCULO_PAGADOR  NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPLSPGB_PLSNOPA_FK_I ON TASY.W_PLS_PAGADOR_BARRAS
(NR_SEQ_NOTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSPGB_PLSVIPA_FK_I ON TASY.W_PLS_PAGADOR_BARRAS
(NR_SEQ_VINCULO_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PLS_PAGADOR_BARRAS ADD (
  CONSTRAINT WPLSPGB_PLSNOPA_FK 
 FOREIGN KEY (NR_SEQ_NOTIFICACAO) 
 REFERENCES TASY.PLS_NOTIFICACAO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT WPLSPGB_PLSVIPA_FK 
 FOREIGN KEY (NR_SEQ_VINCULO_PAGADOR) 
 REFERENCES TASY.PLS_VINCULO_PAGADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PLS_PAGADOR_BARRAS TO NIVEL_1;

