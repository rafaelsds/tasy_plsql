ALTER TABLE TASY.TITULO_PAGAR_ESCRIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_ESCRIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_ESCRIT
(
  NR_TITULO              NUMBER(10)             NOT NULL,
  NR_SEQUENCIA           NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_REMESSA         NUMBER(10),
  NR_SEQ_RETORNO         NUMBER(10),
  CD_INSTRUCAO           VARCHAR2(5 BYTE),
  VL_ESCRITURAL          NUMBER(15,2),
  NR_SEQ_ESCRIT          NUMBER(10)             NOT NULL,
  VL_DESCONTO            NUMBER(15,2),
  CD_BANCO               NUMBER(5),
  CD_AGENCIA_BANCARIA    VARCHAR2(8 BYTE),
  NR_CONTA               VARCHAR2(20 BYTE),
  VL_ACRESCIMO           NUMBER(15,2),
  CD_OCORRENCIA          VARCHAR2(2 BYTE),
  DS_ERRO                VARCHAR2(120 BYTE),
  NR_SEQ_ORIGEM          NUMBER(10),
  IE_TIPO_DOCUMENTO      VARCHAR2(3 BYTE),
  IE_TIPO_SERVICO        VARCHAR2(5 BYTE),
  IE_TIPO_PAGAMENTO      VARCHAR2(3 BYTE),
  IE_MOEDA               NUMBER(2),
  IE_DIGITO_CONTA        VARCHAR2(5 BYTE),
  CD_CAMARA_COMPENSACAO  NUMBER(3),
  CD_OCORRENCIA_RET      NUMBER(2),
  CD_OCORRENCIA_RET_FAV  NUMBER(2),
  DT_LIQUIDACAO          DATE,
  VL_LIQUIDACAO          NUMBER(15,2),
  IE_DIGITO_AGENCIA      VARCHAR2(5 BYTE),
  VL_JUROS               NUMBER(15,2),
  VL_MULTA               NUMBER(15,2),
  NR_PAGAMENTO           VARCHAR2(30 BYTE),
  CD_CODIGO_BARRAS       VARCHAR2(255 BYTE),
  VL_DESPESA             NUMBER(15,2),
  NR_SEQ_REGRA_TIPO_PAG  NUMBER(10),
  IE_SELECIONADO         VARCHAR2(1 BYTE),
  CD_CENTRO_CUSTO        NUMBER(8),
  HR_PAGAMENTO           VARCHAR2(4 BYTE),
  DT_PAGAMENTO           DATE,
  HR_PAGAMENTO_HTML5     DATE,
  NR_SEQ_TIPO_PAGTO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPAES_AGEBANC_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(CD_AGENCIA_BANCARIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAES_AGEBANC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAES_BANCO_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(CD_BANCO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAES_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAES_BANESCR_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(NR_SEQ_ESCRIT)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAES_BCOTPPGES_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(NR_SEQ_TIPO_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAES_CENCUST_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITPAES_PK ON TASY.TITULO_PAGAR_ESCRIT
(NR_SEQ_ESCRIT, NR_TITULO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAES_REGTIPE_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(NR_SEQ_REGRA_TIPO_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAES_REGTIPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAES_TITPAGA_FK_I ON TASY.TITULO_PAGAR_ESCRIT
(NR_TITULO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_escrit_delete
before delete ON TASY.TITULO_PAGAR_ESCRIT for each row
begin

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TP',:old.dt_atualizacao,'E',:old.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_escrit_update
BEFORE UPDATE ON TASY.TITULO_PAGAR_ESCRIT FOR EACH ROW
DECLARE

cd_estabelecimento_w	banco_escritural.cd_estabelecimento%type;
cd_estab_titulo_w		titulo_pagar.cd_estabelecimento%type;
cd_estab_financ_w		titulo_pagar.cd_estab_financeiro%type;
cd_estab_ativo_w		estabelecimento.cd_estabelecimento%type;
ie_permite_estab_w		varchar2(1);

begin

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	banco_escritural
where	nr_sequencia	= :new.nr_seq_escrit;

cd_estab_ativo_w := obter_estabelecimento_ativo;

obter_param_usuario(857,68,obter_perfil_ativo,:new.nm_usuario,cd_estab_ativo_w,ie_permite_estab_w);

if	(nvl(ie_permite_estab_w,'S') = 'N') then
	select	max(cd_estabelecimento),
		max(cd_estab_financeiro)
	into	cd_estab_titulo_w,
		cd_estab_financ_w
	from	titulo_pagar
	where	nr_titulo = :new.nr_titulo;
	if ((cd_estab_titulo_w <> nvl(cd_estab_ativo_w,cd_estabelecimento_w)) and (cd_estab_financ_w <> nvl(cd_estab_ativo_w,cd_estabelecimento_w))) then
		-- N�o � permitido incluir t�tulos de outro estabelecimento. Par�metro [68]
		wheb_mensagem_pck.exibir_mensagem_abort(446925);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_ESCRIT_tp  after update ON TASY.TITULO_PAGAR_ESCRIT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_SEQ_ESCRIT='||to_char(:old.NR_SEQ_ESCRIT)||'#@#@NR_TITULO='||to_char(:old.NR_TITULO); ds_w:=substr(:new.VL_ACRESCIMO,1,500);gravar_log_alteracao(substr(:old.VL_ACRESCIMO,1,4000),substr(:new.VL_ACRESCIMO,1,4000),:new.nm_usuario,nr_seq_w,'VL_ACRESCIMO',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_ESCRITURAL,1,500);gravar_log_alteracao(substr(:old.VL_ESCRITURAL,1,4000),substr(:new.VL_ESCRITURAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_ESCRITURAL',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_TITULO,1,500);gravar_log_alteracao(substr(:old.NR_TITULO,1,4000),substr(:new.NR_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'NR_TITULO',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_DESCONTO,1,500);gravar_log_alteracao(substr(:old.VL_DESCONTO,1,4000),substr(:new.VL_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_DESPESA,1,500);gravar_log_alteracao(substr(:old.VL_DESPESA,1,4000),substr(:new.VL_DESPESA,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESPESA',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_LIQUIDACAO,1,500);gravar_log_alteracao(substr(:old.VL_LIQUIDACAO,1,4000),substr(:new.VL_LIQUIDACAO,1,4000),:new.nm_usuario,nr_seq_w,'VL_LIQUIDACAO',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_JUROS,1,500);gravar_log_alteracao(substr(:old.VL_JUROS,1,4000),substr(:new.VL_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'VL_JUROS',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_MULTA,1,500);gravar_log_alteracao(substr(:old.VL_MULTA,1,4000),substr(:new.VL_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_MULTA',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESCRIT,1,4000),substr(:new.NR_SEQ_ESCRIT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESCRIT',ie_log_w,ds_w,'TITULO_PAGAR_ESCRIT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PAYMENT_HOUR_UPDATE_HTML BEFORE
  INSERT OR
  UPDATE ON TASY.TITULO_PAGAR_ESCRIT FOR EACH ROW
DECLARE cd_estabelecimento_w estabelecimento.cd_estabelecimento%type;
  nm_usuario_w usuario.nm_usuario%type;
  BEGIN
    /*rotina espec�fica para a plataforma HTML5 */
    /* campos ds_horarios date (para utiliza��o do dateTimePickerr) em HTML x ds_horarios String em Java*/
    IF (:new.hr_pagamento_html5 IS NOT NULL AND (:new.hr_pagamento_html5 <> :old.hr_pagamento_html5 OR :old.hr_pagamento_html5 IS NULL)) THEN
      BEGIN
        cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
        nm_usuario_w         := wheb_usuario_pck.get_nm_usuario;
       :new.hr_pagamento    := REPLACE(SUBSTR(TO_CHAR(:new.hr_pagamento_html5, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5),':','');
      END;
    END IF;
  END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_escrit_after
after insert or update ON TASY.TITULO_PAGAR_ESCRIT for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'A',:new.nm_usuario);
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.atual_titulo_pagar_escrit
before insert or update ON TASY.TITULO_PAGAR_ESCRIT for each row
declare

begin

if (:new.hr_pagamento is not null) and
   ((:new.hr_pagamento <> :old.hr_pagamento) or (:old.dt_pagamento is null)) then
    /*Tem que validar se no hr_pagamento tem horas e minutos validos, igual ocorre na DEFINIR_HORA_PAGAMENTO*/
	begin
	if (length(:new.hr_pagamento) = 4) and  /*Tem que ter 4 digitos, 2 para hora e 2 para mintuos*/
	   (substr(:new.hr_pagamento,1,2) >= 00 and substr(:new.hr_pagamento,1,2) < 24) and /*Horas tem que ser entre 00 at� 23*/
	   (substr(:new.hr_pagamento,3,2) >= 00 and substr(:new.hr_pagamento,3,2) < 60) then /*Mintuos tem que ser entre  01 e 59*/
		:new.dt_pagamento := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_pagamento || '00', 'dd/mm/yyyy hh24:mi:ss');
	end if;
	exception when others then
		:new.dt_pagamento := null;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_escrit_INSERT
BEFORE INSERT ON TASY.TITULO_PAGAR_ESCRIT FOR EACH ROW
DECLARE

ie_remessa_retorno_w	varchar2(255);
vl_saldo_titulo_w		number(15,2);
dt_baixa_w				date;
qt_bordero_w			number(15);
qt_bordero_tit_pagar_w	number(15);
qt_baixa_titulo_w		number(15);
ie_baixa_titulo_w 		varchar2(1);
cd_estabelecimento_w	banco_escritural.cd_estabelecimento%type;
cd_estab_titulo_w		titulo_pagar.cd_estabelecimento%type;
cd_estab_financ_w		titulo_pagar.cd_estab_financeiro%type;
cd_estab_ativo_w		estabelecimento.cd_estabelecimento%type;
ie_permite_estab_w		varchar2(1);

begin

select	max(vl_saldo_titulo)
into	vl_saldo_titulo_w
from	titulo_pagar
where	nr_titulo		= :new.nr_titulo;

select	max(ie_remessa_retorno),
		max(dt_baixa),
		max(cd_estabelecimento)
into	ie_remessa_retorno_w,
		dt_baixa_w,
		cd_estabelecimento_w
from	banco_escritural
where	nr_sequencia	= :new.nr_seq_escrit;

cd_estab_ativo_w := obter_estabelecimento_ativo;

obter_param_usuario(857,45,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_baixa_titulo_w);
obter_param_usuario(857,68,obter_perfil_ativo,:new.nm_usuario,cd_estab_ativo_w,ie_permite_estab_w);

if	(nvl(ie_baixa_titulo_w,'N')	= 'S') then

	select	count(*)
	into	qt_baixa_titulo_w
	from	titulo_pagar_baixa a,
			bordero_tit_pagar b
	where	b.nr_titulo 	= :new.nr_titulo
	and		a.nr_titulo		= b.nr_titulo
	and		a.nr_bordero 	= b.nr_bordero
	and		exists ( select 1 -- Verificar se existe uma baixa de estorno OS 1140612
					 from 	titulo_pagar_baixa x
					 where	x.nr_titulo = a.nr_titulo
					 and	x.nr_seq_baixa_origem = a.nr_sequencia);

	if (qt_baixa_titulo_w = 0) then
		select	count(*)
		into	qt_baixa_titulo_w
		from	titulo_pagar_baixa a,
				titulo_pagar b
		where	b.nr_titulo 	= :new.nr_titulo
		and		a.nr_titulo		= b.nr_titulo
		and		a.nr_bordero 	= b.nr_bordero
		and		exists ( select 1 -- Verificar se existe uma baixa de estorno OS 1140612
						 from 	titulo_pagar_baixa x
						 where	x.nr_titulo = a.nr_titulo
						 and	x.nr_seq_baixa_origem = a.nr_sequencia);
	end if;

end if;

select	count(*)
into	qt_bordero_w
from	titulo_pagar a
where	a.nr_bordero	is not null
and	a.nr_titulo	= :new.nr_titulo;

select	count(*)
into	qt_bordero_tit_pagar_w
from	bordero_tit_pagar a
where	a.nr_titulo	= :new.nr_titulo;


if	(((qt_bordero_w > 0) or (qt_bordero_tit_pagar_w > 0)) and ((qt_baixa_titulo_w = 0) or ('N' = nvl(ie_baixa_titulo_w,'N')))) then
--'N�o � poss�vel incluir um t�tulo que est� em border�!'
	wheb_mensagem_pck.exibir_mensagem_abort(224271);
end if;


if	(vl_saldo_titulo_w = 0) and (ie_remessa_retorno_w = 'R') then
-- N�o � poss�vel incluir um t�tulo a pagar com saldo zerado no pagamento escritural!
-- T�tulo: #@NR_TITULO#@
	wheb_mensagem_pck.exibir_mensagem_abort(224273, 'NR_TITULO=' || :new.nr_titulo);
end if;

if	(dt_baixa_w is not null) then
--N�o � poss�vel incluir um t�tulo em um pagamento escritural j� baixado!
	wheb_mensagem_pck.exibir_mensagem_abort(224275);
end if;

if	(nvl(ie_permite_estab_w,'S') = 'N') then
	select	max(cd_estabelecimento),
		max(cd_estab_financeiro)
	into	cd_estab_titulo_w,
		cd_estab_financ_w
	from	titulo_pagar
	where	nr_titulo = :new.nr_titulo;
	if ((cd_estab_titulo_w <> nvl(cd_estab_ativo_w,cd_estabelecimento_w)) and (cd_estab_financ_w <> nvl(cd_estab_ativo_w,cd_estabelecimento_w))) then
		-- N�o � permitido incluir t�tulos de outro estabelecimento. Par�metro [68]
		wheb_mensagem_pck.exibir_mensagem_abort(446925);
	end if;
end if;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_ESCRIT ADD (
  CONSTRAINT TITPAES_PK
 PRIMARY KEY
 (NR_SEQ_ESCRIT, NR_TITULO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_ESCRIT ADD (
  CONSTRAINT TITPAES_REGTIPE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TIPO_PAG) 
 REFERENCES TASY.REGRA_TIPO_PAG_ESCRIT (NR_SEQUENCIA),
  CONSTRAINT TITPAES_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITPAES_BCOTPPGES_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PAGTO) 
 REFERENCES TASY.BANCO_TIPO_PAGTO_ESCRIT (NR_SEQUENCIA),
  CONSTRAINT TITPAES_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT TITPAES_BANESCR_FK 
 FOREIGN KEY (NR_SEQ_ESCRIT) 
 REFERENCES TASY.BANCO_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT TITPAES_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TITULO_PAGAR_ESCRIT TO NIVEL_1;

