ALTER TABLE TASY.CTB_MOVIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_MOVIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_MOVIMENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_LOTE_CONTABIL         NUMBER(10)           NOT NULL,
  NR_SEQ_MES_REF           NUMBER(10)           NOT NULL,
  DT_MOVIMENTO             DATE                 NOT NULL,
  VL_MOVIMENTO             NUMBER(15,2)         NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_HISTORICO             NUMBER(10)           NOT NULL,
  CD_CONTA_DEBITO          VARCHAR2(20 BYTE),
  CD_CONTA_CREDITO         VARCHAR2(20 BYTE),
  DS_COMPL_HISTORICO       VARCHAR2(255 BYTE),
  NR_SEQ_AGRUPAMENTO       NUMBER(15),
  IE_REVISADO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(15),
  DS_CONSISTENCIA          VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_CLASSIF_DEBITO        VARCHAR2(40 BYTE),
  CD_CLASSIF_CREDITO       VARCHAR2(40 BYTE),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  NR_SEQ_MOVTO_CORRESP     NUMBER(10),
  DT_REVISAO               DATE,
  NM_USUARIO_REVISAO       VARCHAR2(15 BYTE),
  NR_SEQ_APRES             NUMBER(10),
  NR_SEQ_CONTA_ANS_DEB     NUMBER(10),
  NR_SEQ_CONTA_ANS_CRED    NUMBER(10),
  NR_AGRUP_SEQUENCIAL      NUMBER(15),
  NR_DOCUMENTO             VARCHAR2(255 BYTE),
  NR_SEQ_REGRA_LANC        NUMBER(10),
  IE_ORIGEM_DOCUMENTO      NUMBER(5),
  NR_SEQ_MOVTO_PARTIDA     NUMBER(10),
  NR_SEQ_REG_CONCIL        NUMBER(10),
  IE_STATUS_CONCIL         VARCHAR2(15 BYTE),
  IE_STATUS_ORIGEM         VARCHAR2(15 BYTE),
  NR_SEQ_MUTACAO_PL        NUMBER(10),
  DS_JUSTIFICATIVA         VARCHAR2(2000 BYTE),
  IE_TRANSITORIO           VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_SALDO     DATE,
  DT_LANCTO_EXT            DATE,
  NR_SEQ_CLASSIF_MOVTO     NUMBER(10),
  IE_INTERCOMPANY          VARCHAR2(1 BYTE),
  IE_ELIMINACAO_LANCTO     VARCHAR2(1 BYTE),
  NR_SEQ_MOVTO_ELIMINACAO  NUMBER(10),
  CD_ESTAB_INTERCOMPANY    NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBMOVI_CONCONT_FK_I ON TASY.CTB_MOVIMENTO
(CD_CONTA_DEBITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CONCONT_FK2_I ON TASY.CTB_MOVIMENTO
(CD_CONTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBCLSMOV_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_CLASSIF_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBMERE_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBMOVI_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_MOVTO_CORRESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBMOVI_CTBMOVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBMOVI_CTBMOVI_FK2_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_MOVTO_PARTIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBMOVI_FK3_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_MOVTO_ELIMINACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBMUTAPL_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_MUTACAO_PL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBPLAN_D_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_CONTA_ANS_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBMOVI_CTBPLAN_D_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBMOVI_CTBPLAN_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_CONTA_ANS_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBMOVI_CTBPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBMOVI_CTBRECON_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_REG_CONCIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_CTBREMP_FK_I ON TASY.CTB_MOVIMENTO
(NR_SEQ_REGRA_LANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBMOVI_CTBREMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBMOVI_ESTABEL_FK_I ON TASY.CTB_MOVIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBMOVI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBMOVI_ESTABEL_FK2_I ON TASY.CTB_MOVIMENTO
(CD_ESTAB_INTERCOMPANY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_HISPADR_FK_I ON TASY.CTB_MOVIMENTO
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_I1 ON TASY.CTB_MOVIMENTO
(DT_MOVIMENTO, CD_CONTA_DEBITO, CD_CONTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          20M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_I2 ON TASY.CTB_MOVIMENTO
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_I3 ON TASY.CTB_MOVIMENTO
(NR_AGRUP_SEQUENCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_I4 ON TASY.CTB_MOVIMENTO
(NR_LOTE_CONTABIL, NR_SEQ_MES_REF, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBMOVI_LOTCONT_FK_I ON TASY.CTB_MOVIMENTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBMOVI_PK ON TASY.CTB_MOVIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_movimento_delete
after delete ON TASY.CTB_MOVIMENTO for each row
begin
/*
O movimento foi gerado a partir de um documento contabil,
por isso se a contabilizacao for desfeita, o documento precisa ser marcado como Nao contabilizado.
*/
if (:old.ie_status_origem = 'SO') then
	update	ctb_documento
	set		ie_situacao_ctb = 'P',
			nr_lote_contabil= null
	where	nr_sequencia in (
				select	y.nr_seq_ctb_documento
				from 	movimento_contabil_doc y
				where	y.nr_seq_ctb_movto = :old.nr_sequencia
				and		y.nr_lote_contabil = :old.nr_lote_contabil);

	delete	movimento_contabil_doc
	where	nr_seq_ctb_movto = :old.nr_sequencia
	and		nr_seq_ctb_documento is not null;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CTB_MOVIMENTO_tp  after update ON TASY.CTB_MOVIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_ELIMINACAO_LANCTO,1,500);gravar_log_alteracao(substr(:old.IE_ELIMINACAO_LANCTO,1,4000),substr(:new.IE_ELIMINACAO_LANCTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ELIMINACAO_LANCTO',ie_log_w,ds_w,'CTB_MOVIMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'CTB_MOVIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ctb_movimento_atual
before insert or update or delete ON TASY.CTB_MOVIMENTO for each row
declare

nr_seq_conta_ans_cred_w		number(10);
nr_seq_conta_ans_deb_w		number(10);
nr_versao_w					number(10);
qtd_reg_w					number(10);
cd_empresa_w			ctb_mes_ref.cd_empresa%type;

begin
if	(philips_contabil_pck.get_ie_consistindo_lote = 'N') then
	begin
	if	(philips_contabil_pck.get_ie_integrando_lote = 'N') then
		begin
		if	(:new.nr_lote_contabil is not null) and
			(:new.nm_usuario <> wheb_mensagem_pck.get_texto(798482)) then
			begin
			update	lote_contabil
			set	dt_consistencia	= null
			where	nr_lote_contabil	= :new.nr_lote_contabil;
			end;
		end if;
		end;
	end if;

	if	(:new.nr_lote_contabil is not null) and
		((inserting) or (updating)) then
		begin

		if	(nvl(:new.ie_status_origem, '0') <> 'SO' ) and
            		(nvl(:new.cd_conta_credito,'0') = '0') and
			(nvl(:new.cd_conta_debito,'0') = '0') then
			wheb_mensagem_pck.exibir_mensagem_abort(225337,'CD_CONTA_CREDITO_NOVA=' || :new.cd_conta_credito || ';CD_CONTA_DEBITO_NOVA=' || :new.cd_conta_debito || ';CD_CONTA_CREDITO_ANT=' || :old.cd_conta_credito|| ';CD_CONTA_DEBITO_ANT=' || :old.cd_conta_debito);
		end if;

		select	nvl(max(nr_sequencia),0)
		into 	nr_versao_w
		from	ctb_versao_plano_ans
		where	:new.dt_movimento between dt_inicio_vigencia and nvl(dt_fim_vigencia, sysdate);

		begin
		select	cd_empresa
		into	cd_empresa_w
		from	ctb_mes_ref
		where	nr_sequencia = :new.nr_seq_mes_ref;
		exception when others then
			if	(:new.cd_estabelecimento is not null) then
				cd_empresa_w	:= obter_empresa_estab(:new.cd_estabelecimento);
			elsif	(nvl(wheb_usuario_pck.get_cd_estabelecimento,0) != 0) then
				cd_empresa_w	:= obter_empresa_estab(wheb_usuario_pck.get_cd_estabelecimento);
			end if;
		end;

		PHILIPS_CONTABIL_PCK.valida_se_dia_fechado(cd_empresa_w, :new.dt_movimento);

		if	(nr_versao_w > 0) then
			begin
			select 	count(*)
			into 	qtd_reg_w
			from 	conta_contabil_ans c,
				ctb_plano_ans p
			where 	p.nr_sequencia 			= c.nr_seq_conta_ans
			and	p.nr_seq_versao_plano	= nr_versao_w
			and	((c.cd_conta_contabil	= :new.cd_conta_debito) or
				(c.cd_conta_contabil	= :new.cd_conta_credito));

			if	(qtd_reg_w > 0) then
				begin
				/* pega a sequencia do plano ans para gravar a conta debito*/
				select 	nvl(max(c.nr_seq_conta_ans),0)
				into 	nr_seq_conta_ans_deb_w
				from 	conta_contabil_ans c,
						ctb_plano_ans p
				where 	p.nr_sequencia 	= c.nr_seq_conta_ans
				and		c.cd_conta_contabil = :new.cd_conta_debito
				and		p.nr_seq_versao_plano	=  nr_versao_w;

				if	(nr_seq_conta_ans_deb_w <> 0) then
					begin
					:new.nr_seq_conta_ans_deb := nr_seq_conta_ans_deb_w;
					end;
				end if;

				/* pega a sequencia do plano ans para gravar a conta cr�dito*/
				select 	nvl(max(c.nr_seq_conta_ans),0)
				into 	nr_seq_conta_ans_cred_w
				from 	conta_contabil_ans c,
					ctb_plano_ans p
				where 	p.nr_sequencia = c.nr_seq_conta_ans
				and	c.cd_conta_contabil = :new.cd_conta_credito
				and	p.nr_seq_versao_plano = nr_versao_w;

				if	(nr_seq_conta_ans_cred_w <> 0) then
					begin
					:new.nr_seq_conta_ans_cred := nr_seq_conta_ans_cred_w;
					end;
				end if;
				end;
			end if;
			end;
		end if;
	end;
	end if;

	begin
	if	(deleting) and
		(:old.nr_lote_contabil is not null) and
		(nvl(:old.nr_seq_regra_lanc,0) <> 0)then
		begin

		update 	ctb_regra_movto_prog
		set	vl_saldo = vl_saldo + :old.vl_movimento
		where 	nr_sequencia = :old.nr_seq_regra_lanc;

		end;
	end if;
	exception when others then
		null;
	end;

	end;
end if;
end;
/


ALTER TABLE TASY.CTB_MOVIMENTO ADD (
  CONSTRAINT CTBMOVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          12M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_MOVIMENTO ADD (
  CONSTRAINT CTBMOVI_CTBMOVI_FK3 
 FOREIGN KEY (NR_SEQ_MOVTO_ELIMINACAO) 
 REFERENCES TASY.CTB_MOVIMENTO (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_INTERCOMPANY) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBMOVI_CTBPLAN_FK 
 FOREIGN KEY (NR_SEQ_CONTA_ANS_CRED) 
 REFERENCES TASY.CTB_PLANO_ANS (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CTBREMP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_LANC) 
 REFERENCES TASY.CTB_REGRA_MOVTO_PROG (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CTBMOVI_FK2 
 FOREIGN KEY (NR_SEQ_MOVTO_PARTIDA) 
 REFERENCES TASY.CTB_MOVIMENTO (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CTBRECON_FK 
 FOREIGN KEY (NR_SEQ_REG_CONCIL) 
 REFERENCES TASY.CTB_REGISTRO_CONCIL (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CTBMUTAPL_FK 
 FOREIGN KEY (NR_SEQ_MUTACAO_PL) 
 REFERENCES TASY.CTB_MUTACAO_PL (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CTBCLSMOV_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_MOVTO) 
 REFERENCES TASY.CTB_CLASSIF_MOVIMENTO (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CTBPLAN_D_FK 
 FOREIGN KEY (NR_SEQ_CONTA_ANS_DEB) 
 REFERENCES TASY.CTB_PLANO_ANS (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_CREDITO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBMOVI_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBMOVI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CTBMOVI_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT CTBMOVI_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_DEBITO) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBMOVI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBMOVI_CTBMOVI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_CORRESP) 
 REFERENCES TASY.CTB_MOVIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_MOVIMENTO TO NIVEL_1;

