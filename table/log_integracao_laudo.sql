ALTER TABLE TASY.LOG_INTEGRACAO_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_INTEGRACAO_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_INTEGRACAO_LAUDO
(
  NR_SEQUENCIA     NUMBER(10)                   NOT NULL,
  NR_ACESSO_DICOM  VARCHAR2(20 BYTE),
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  DS_LOG           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LOGLAUDO_PK ON TASY.LOG_INTEGRACAO_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOGLAUDO_PK
  MONITORING USAGE;


ALTER TABLE TASY.LOG_INTEGRACAO_LAUDO ADD (
  CONSTRAINT LOGLAUDO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LOG_INTEGRACAO_LAUDO TO NIVEL_1;

