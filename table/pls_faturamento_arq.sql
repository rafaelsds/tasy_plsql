ALTER TABLE TASY.PLS_FATURAMENTO_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FATURAMENTO_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FATURAMENTO_ARQ
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_XML                  CLOB,
  DS_ERRO                 VARCHAR2(2000 BYTE),
  DT_INICIO_IMPORTACAO    DATE,
  DT_FINALIZACAO_ARQUIVO  DATE,
  IE_TIPO_FATURAMENTO     VARCHAR2(3 BYTE),
  NR_TITULO_PAGAR         NUMBER(10),
  IE_ENVIO_RECEBIMENTO    VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSFATAR_PK ON TASY.PLS_FATURAMENTO_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFATAR_TITPAGA_FK_I ON TASY.PLS_FATURAMENTO_ARQ
(NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_FATURAMENTO_ARQ ADD (
  CONSTRAINT PLSFATAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_FATURAMENTO_ARQ ADD (
  CONSTRAINT PLSFATAR_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.PLS_FATURAMENTO_ARQ TO NIVEL_1;

