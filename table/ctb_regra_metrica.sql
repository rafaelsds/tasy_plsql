ALTER TABLE TASY.CTB_REGRA_METRICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGRA_METRICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGRA_METRICA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CENTRO_CUSTO      NUMBER(15)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_METRICA       NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_CENARIO       NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA         NUMBER(15)               NOT NULL,
  NR_SEQ_MES_REF       NUMBER(10),
  DT_MES_INIC          DATE,
  DT_MES_FIM           DATE,
  IE_REGRA             VARCHAR2(15 BYTE)        NOT NULL,
  PR_APLICAR           NUMBER(15,4),
  IE_SOBREPOR          VARCHAR2(1 BYTE)         NOT NULL,
  QT_FIXA              NUMBER(15,2),
  QT_LEITO             NUMBER(10),
  TX_OCUPACAO          NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGMETR_CENCUST_FK_I ON TASY.CTB_REGRA_METRICA
(CD_CENTRO_CUSTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMETR_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGMETR_CTBMERE_FK_I ON TASY.CTB_REGRA_METRICA
(NR_SEQ_MES_REF)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMETR_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGMETR_CTBMETR_FK_I ON TASY.CTB_REGRA_METRICA
(NR_SEQ_METRICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMETR_CTBMETR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGMETR_CTBORCE_FK_I ON TASY.CTB_REGRA_METRICA
(NR_SEQ_CENARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMETR_CTBORCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGMETR_ESTABEL_FK_I ON TASY.CTB_REGRA_METRICA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMETR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGMETR_PK ON TASY.CTB_REGRA_METRICA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMETR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ctb_regra_metrica_insert
before insert or update ON TASY.CTB_REGRA_METRICA for each row
declare

ds_centro_custo_w		varchar2(255);
ds_metrica_w		varchar2(255);
qt_limite_w		number(15,4);

BEGIN

if	(:new.ie_regra = 'QF') then
	:new.pr_aplicar	:= null;
	:new.nr_seq_mes_ref	:= null;
elsif	(:new.ie_regra = 'PO') then
	:new.qt_fixa		:= null;
elsif	(:new.ie_regra = 'PQ') then
	:new.qt_fixa		:= null;
	:new.nr_seq_mes_ref	:= null;
end if;

if	(:new.cd_centro_custo is not null) and
	(:new.nr_seq_metrica is not null) and
	(nvl(:new.qt_fixa,0) <> 0) then
	begin
	select	nvl(max(qt_limite),0)
	into	qt_limite_w
	from	ctb_orc_metrica
	where	nr_seq_metrica	= :new.nr_seq_metrica
	and	cd_centro_custo	= :new.cd_centro_custo;

	if	(qt_limite_w <> 0) and
		(:new.qt_fixa > qt_limite_w) then

		ds_centro_custo_w	:= substr(obter_desc_centro_custo(:new.cd_centro_custo),1,255);
		ds_metrica_w		:= substr(ctb_obter_desc_metrica(:new.nr_seq_metrica),1,255);
		wheb_mensagem_pck.exibir_mensagem_abort(266590,'QT_FIXA=' || :new.qt_fixa 	|| ';' ||
							'DS_METRICA='	  || ds_metrica_w 	|| ';' ||
							'DS_CENTRO='	  || ds_centro_custo_w  || ';' ||
							'QT_LIMITE='	  || qt_limite_w);
	end if;
	end;
end if;

END ctb_regra_metrica_insert;
/


ALTER TABLE TASY.CTB_REGRA_METRICA ADD (
  CONSTRAINT REGMETR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_REGRA_METRICA ADD (
  CONSTRAINT REGMETR_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT REGMETR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGMETR_CTBORCE_FK 
 FOREIGN KEY (NR_SEQ_CENARIO) 
 REFERENCES TASY.CTB_ORC_CENARIO (NR_SEQUENCIA),
  CONSTRAINT REGMETR_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT REGMETR_CTBMETR_FK 
 FOREIGN KEY (NR_SEQ_METRICA) 
 REFERENCES TASY.CTB_METRICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_REGRA_METRICA TO NIVEL_1;

