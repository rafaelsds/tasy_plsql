ALTER TABLE TASY.HDM_INDIC_FT_MEDICATION_PR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HDM_INDIC_FT_MEDICATION_PR CASCADE CONSTRAINTS;

CREATE TABLE TASY.HDM_INDIC_FT_MEDICATION_PR
(
  NR_SEQ_DIMENSAO  NUMBER(10)                   NOT NULL,
  NR_SEQ_FACT      NUMBER(10)                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDMIFMP_HDMIDPR_FK_I ON TASY.HDM_INDIC_FT_MEDICATION_PR
(NR_SEQ_DIMENSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.HDMIFMP_HDMIFME_FK_I ON TASY.HDM_INDIC_FT_MEDICATION_PR
(NR_SEQ_FACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDMIFMP_PK ON TASY.HDM_INDIC_FT_MEDICATION_PR
(NR_SEQ_DIMENSAO, NR_SEQ_FACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HDM_INDIC_FT_MEDICATION_PR ADD (
  CONSTRAINT HDMIFMP_PK
 PRIMARY KEY
 (NR_SEQ_DIMENSAO, NR_SEQ_FACT)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HDM_INDIC_FT_MEDICATION_PR ADD (
  CONSTRAINT HDMIFMP_HDMIDPR_FK 
 FOREIGN KEY (NR_SEQ_DIMENSAO) 
 REFERENCES TASY.HDM_INDIC_DM_PROGRAM (NR_SEQUENCIA),
  CONSTRAINT HDMIFMP_HDMIFME_FK 
 FOREIGN KEY (NR_SEQ_FACT) 
 REFERENCES TASY.HDM_INDIC_FT_MEDICATION (NR_SEQUENCIA));

GRANT SELECT ON TASY.HDM_INDIC_FT_MEDICATION_PR TO NIVEL_1;

