ALTER TABLE TASY.FIS_DIM_ARQUIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_DIM_ARQUIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_DIM_ARQUIVO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTROLE_DIM  NUMBER(10)               NOT NULL,
  NR_LINHA             NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(4000 BYTE)      NOT NULL,
  DS_ARQUIVO_COMPL     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISDIMARQ_FISDIMCON_FK_I ON TASY.FIS_DIM_ARQUIVO
(NR_SEQ_CONTROLE_DIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FISDIMARQ_PK ON TASY.FIS_DIM_ARQUIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_DIM_ARQUIVO ADD (
  CONSTRAINT FISDIMARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_DIM_ARQUIVO ADD (
  CONSTRAINT FISDIMARQ_FISDIMCON_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE_DIM) 
 REFERENCES TASY.FIS_DIM_CONTROLE (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_DIM_ARQUIVO TO NIVEL_1;

