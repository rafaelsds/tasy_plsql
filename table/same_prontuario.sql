ALTER TABLE TASY.SAME_PRONTUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAME_PRONTUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAME_PRONTUARIO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  IE_TIPO                   VARCHAR2(5 BYTE)    NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_STATUS                 VARCHAR2(5 BYTE)    NOT NULL,
  NR_SEQ_LOCAL              NUMBER(10),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  NR_ATENDIMENTO            NUMBER(10),
  NR_SEQ_AGRUP              NUMBER(10),
  NR_SEQ_CAIXA              NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  IE_DIGITALIZADO           VARCHAR2(1 BYTE)    NOT NULL,
  IE_MICROFILMADO           VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_FILME                  VARCHAR2(15 BYTE),
  CD_FOTOGRAMA              VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO      NUMBER(5)           NOT NULL,
  DT_PERIODO_INICIAL        DATE,
  DT_PERIODO_FINAL          DATE,
  NR_SEQ_VOLUME             NUMBER(6),
  IE_TIPO_PRONTUARIO        VARCHAR2(1 BYTE),
  NR_PRONTUARIO_ANT         NUMBER(10),
  NR_PRONTUARIO_ANTERIOR    VARCHAR2(20 BYTE),
  CD_FUNCAO                 NUMBER(5),
  IE_FORMA_GERACAO_PRONT    VARCHAR2(1 BYTE),
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  IE_TIPO_ATEND_PRONTUARIO  NUMBER(3),
  QT_PAGINAS                NUMBER(5),
  NR_INTERNO_CONTA          NUMBER(10),
  NR_SEQ_LOCAL_ANT          NUMBER(10),
  CD_MACO                   VARCHAR2(20 BYTE),
  NR_SEQ_TIPO               NUMBER(10),
  CD_TRAMIX                 VARCHAR2(4000 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SAMPRON_ATEPACI_FK_I ON TASY.SAME_PRONTUARIO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAMPRON_CONPACI_FK_I ON TASY.SAME_PRONTUARIO
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMPRON_CONPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMPRON_ESTABEL_FK_I ON TASY.SAME_PRONTUARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMPRON_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMPRON_PESFISI_FK_I ON TASY.SAME_PRONTUARIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAMPRON_SAMCAIX_FK_I ON TASY.SAME_PRONTUARIO
(NR_SEQ_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAMPRON_SAMETIPPRO_FK_I ON TASY.SAME_PRONTUARIO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAMPRON_SAMLOCA_FK_I ON TASY.SAME_PRONTUARIO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMPRON_SAMLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMPRON_SAMLOCA_FK2_I ON TASY.SAME_PRONTUARIO
(NR_SEQ_LOCAL_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMPRON_SAMLOCA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMPRON_SAMPRON_FK_I ON TASY.SAME_PRONTUARIO
(NR_SEQ_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMPRON_SAMPRON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMPRON_SETATEN_FK_I ON TASY.SAME_PRONTUARIO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMPRON_SETATEN_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SAMPRO_PK ON TASY.SAME_PRONTUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Same_Prontuario_BefInsUpd
before insert or update ON TASY.SAME_PRONTUARIO for each row
declare
qt_reg_w		number(1);
qt_reg_etapa_w		number(10);
ie_tipo_atend_pront_w	varchar2(1);
ie_atend_internado_w	varchar2(1);
ie_tipo_atendimento_w	number(3);
ie_exige_dtinicio_w	varchar2(1);

pragma autonomous_transaction;
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

obter_param_usuario(941, 236, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_exige_dtinicio_w);

if 	(:new.dt_periodo_inicial is not null) or
	(:new.dt_periodo_final is not null) then
	if (ie_exige_dtinicio_w = 'S') then   					--Exige somente data de inicio
		if (:new.dt_periodo_inicial is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(283874);
		end if;								--Exige data de in�cio e data de fim	Caso tenha sido informada somente uma ou outra gera um erro, pois neste caso � necess�rio informar as duas.
	elsif	(:new.dt_periodo_inicial is not null and :new.dt_periodo_final is null) or
		(:new.dt_periodo_final is not null and :new.dt_periodo_inicial is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(264175);
	end if;
end if;

select	count(*)
into	qt_reg_etapa_w
from	fatur_etapa_alta
where	ie_evento in ('G','S');

if	(:new.nr_atendimento is not null) and
	(qt_reg_etapa_w > 0) then

	if	(inserting) then
		gerar_etapa_prontuario(:new.nr_atendimento, :new.ie_status, 'G', :new.nm_usuario);
	end if;

	if	(updating) and
		(:new.ie_status <> :old.ie_status) then
		gerar_etapa_prontuario(:new.nr_atendimento, :new.ie_status, 'S', :new.nm_usuario);
	end if;

end if;

if	(inserting) then

	obter_param_usuario(941, 162, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_tipo_atend_pront_w);

	if (ie_tipo_atend_pront_w = 'S') then

		select	decode(count(*),0,'N','S')
		into	ie_atend_internado_w
		from	atendimento_paciente
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica
		and	ie_tipo_atendimento = 1;

		if (ie_atend_internado_w = 'S') then
			:new.ie_tipo_atend_prontuario := 1;
		elsif (:new.nr_atendimento is not null) then

			select	nvl(max(ie_tipo_atendimento),8)
			into	ie_tipo_atendimento_w
			from	atendimento_paciente
			where	nr_atendimento = :new.nr_atendimento;

			:new.ie_tipo_atend_prontuario := ie_tipo_atendimento_w;

		else
			:new.ie_tipo_atend_prontuario := 8;
		end if;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.SAME_PRONTUARIO_tp  after update ON TASY.SAME_PRONTUARIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'SAME_PRONTUARIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SAME_PRONTUARIO ADD (
  CONSTRAINT SAMPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAME_PRONTUARIO ADD (
  CONSTRAINT SAMPRON_SAMETIPPRO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.SAME_TIPO_PRONTUARIO (NR_SEQUENCIA),
  CONSTRAINT SAMPRON_SAMLOCA_FK2 
 FOREIGN KEY (NR_SEQ_LOCAL_ANT) 
 REFERENCES TASY.SAME_LOCAL (NR_SEQUENCIA),
  CONSTRAINT SAMPRON_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT SAMPRON_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SAMPRON_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SAMPRON_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SAMPRON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SAMPRON_SAMPRON_FK 
 FOREIGN KEY (NR_SEQ_AGRUP) 
 REFERENCES TASY.SAME_PRONTUARIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SAMPRON_SAMCAIX_FK 
 FOREIGN KEY (NR_SEQ_CAIXA) 
 REFERENCES TASY.SAME_CAIXA (NR_SEQUENCIA),
  CONSTRAINT SAMPRON_SAMLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.SAME_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAME_PRONTUARIO TO NIVEL_1;

