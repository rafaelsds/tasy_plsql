ALTER TABLE TASY.PLS_TAXA_ATENDIMENTO_EXCE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TAXA_ATENDIMENTO_EXCE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TAXA_ATENDIMENTO_EXCE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TX_ADM  NUMBER(10)               NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  NR_SEQ_GRAU_PARTIC   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTXATE_PK ON TASY.PLS_TAXA_ATENDIMENTO_EXCE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTXATE_PLSGRPA_FK_I ON TASY.PLS_TAXA_ATENDIMENTO_EXCE
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTXATE_PLSTAAT_FK_I ON TASY.PLS_TAXA_ATENDIMENTO_EXCE
(NR_SEQ_REGRA_TX_ADM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TAXA_ATENDIMENTO_EXCE ADD (
  CONSTRAINT PLSTXATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TAXA_ATENDIMENTO_EXCE ADD (
  CONSTRAINT PLSTXATE_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSTXATE_PLSTAAT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TX_ADM) 
 REFERENCES TASY.PLS_TAXA_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TAXA_ATENDIMENTO_EXCE TO NIVEL_1;

