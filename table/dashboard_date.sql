ALTER TABLE TASY.DASHBOARD_DATE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DASHBOARD_DATE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DASHBOARD_DATE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DASHBOARD     NUMBER(10)               NOT NULL,
  NR_SEQ_INDICATOR     NUMBER(10)               NOT NULL,
  NR_SEQ_DATE          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DASHDAT_DASHBASE_FK_I ON TASY.DASHBOARD_DATE
(NR_SEQ_DASHBOARD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DASHDAT_DASHIND_FK_I ON TASY.DASHBOARD_DATE
(NR_SEQ_INDICATOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DASHDAT_INDDATA_FK_I ON TASY.DASHBOARD_DATE
(NR_SEQ_DATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DASHDAT_PK ON TASY.DASHBOARD_DATE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DASHBOARD_DATE ADD (
  CONSTRAINT DASHDAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DASHBOARD_DATE ADD (
  CONSTRAINT DASHDAT_DASHBASE_FK 
 FOREIGN KEY (NR_SEQ_DASHBOARD) 
 REFERENCES TASY.DASHBOARD_BASE (NR_SEQUENCIA),
  CONSTRAINT DASHDAT_DASHIND_FK 
 FOREIGN KEY (NR_SEQ_INDICATOR) 
 REFERENCES TASY.DASHBOARD_INDICATOR (NR_SEQUENCIA),
  CONSTRAINT DASHDAT_INDDATA_FK 
 FOREIGN KEY (NR_SEQ_DATE) 
 REFERENCES TASY.IND_DATA (NR_SEQUENCIA));

GRANT SELECT ON TASY.DASHBOARD_DATE TO NIVEL_1;

