ALTER TABLE TASY.FIS_LIVROS_ESTR_PADRAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_LIVROS_ESTR_PADRAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_LIVROS_ESTR_PADRAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ESTRUTURA         VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_ORDEM         NUMBER(10),
  IE_LALUR_LACS        VARCHAR2(2 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(2 BYTE)         NOT NULL,
  IE_TIPO_ESTRUTURA    VARCHAR2(2 BYTE),
  NR_SEQ_SUPERIOR      NUMBER(10),
  IE_FORMA_APURACAO    VARCHAR2(10 BYTE),
  IE_GERAR_ECF         VARCHAR2(10 BYTE),
  IE_PERMITE_EDITAR    VARCHAR2(2 BYTE),
  CD_CODIGO_ECF        VARCHAR2(255 BYTE),
  CD_CODIGO_BLOCO_N    VARCHAR2(255 BYTE),
  IE_GERAR_ECF_N       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISLIVESTR_FISLIVESTR_FK_I ON TASY.FIS_LIVROS_ESTR_PADRAO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FISLIVESTR_PK ON TASY.FIS_LIVROS_ESTR_PADRAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOG_FIS_LIVROS_ESTR_PADRAO
before insert or update or delete ON TASY.FIS_LIVROS_ESTR_PADRAO for each row
declare

ds_historico_w		varchar2(2000);
ds_erro_w		varchar2(4000);
ds_operacao		varchar2(10);

begin
	begin
		if (inserting) then
			ds_operacao := 'Insert';
		else 	if (updating) then
				ds_operacao := 'Update';
			else
				ds_operacao := 'Delete';
			end if;
		end if;

		select 	substr(ds_operacao || ' na estrutura dos livros (LALUR/LACS): '|| chr(13) || chr(10)||
			'Sequ�ncia anterior: ' || :old.nr_sequencia || ' | Sequ�ncia atual: ' || :new.nr_sequencia || chr(13) || chr(10)||
			'Descri��o anterior: ' || :old.ds_estrutura || ' | Descri��o atual: ' || :new.ds_estrutura || chr(13) || chr(10)||
			'Livro anterior: ' || :old.ie_lalur_lacs || ' | Livro atual: ' || :new.ie_lalur_lacs || chr(13) || chr(10)||
			'Tipo anterior: ' || :old.ie_tipo_estrutura || ' | tipo atual: ' || :new.ie_tipo_estrutura || chr(13) || chr(10)||
			decode(:old.nr_seq_superior, :new.nr_seq_superior, null,
			'Seq superior anterior: ' || :old.nr_seq_superior || ' | Seq superior atual: ' || :new.nr_seq_superior || chr(13) || chr(10)) ||
			decode(:old.nr_seq_ordem, :new.nr_seq_ordem, null,
			'Seq ordem anterior: ' || :old.nr_seq_ordem || ' | Seq ordem atual: ' || :new.nr_seq_ordem || chr(13) || chr(10)) ||
			decode(:old.ie_situacao, :new.ie_situacao, null,
			'Situa��o anterior: ' || :old.ie_situacao || ' | Situa��o atual: ' || :new.ie_situacao|| chr(13) || chr(10)) ||
			decode(:old.ie_forma_apuracao, :new.ie_forma_apuracao, null,
			'Forma apura��o anterior: ' || :old.ie_forma_apuracao || ' | Forma apura��o atual: ' || :new.ie_forma_apuracao || chr(13) || chr(10)) ||
			decode(:old.ie_permite_editar, :new.ie_permite_editar, null,
			'Permite edi��o anterior: ' || :old.ie_permite_editar || ' | Permite edi��o atual: ' || :new.ie_permite_editar || chr(13) || chr(10)) ||
			decode(:old.ie_gerar_ecf, :new.ie_gerar_ecf, null,
			'Registros ECF anterior: ' || :old.ie_gerar_ecf || ' | Registros ECF atual: ' || :new.ie_gerar_ecf || chr(13) || chr(10)) ||
			decode(:old.cd_codigo_ecf, :new.cd_codigo_ecf, null,
			'C�digo ECF anterior: ' || :old.cd_codigo_ecf || ' | C�digo ECF atual: ' || :new.cd_codigo_ecf || chr(13) || chr(10)) ||
			decode(:old.ie_gerar_ecf_n, :new.ie_gerar_ecf_n, null,
			'Registros ECF(Bloco N anual) anterior: ' || :old.ie_gerar_ecf_n || ' | Registros ECF(Bloco N anual) atual: ' || :new.ie_gerar_ecf_n || chr(13) || chr(10)) ||
			decode(:old.cd_codigo_bloco_n, :new.cd_codigo_bloco_n, null,
			'C�digo ECF (Bloco N anual) anterior: ' || :old.cd_codigo_bloco_n || ' | C�digo ECF (Bloco N anual) atual: ' || :new.cd_codigo_bloco_n),1,2000)
		into	ds_historico_w
		from	dual;

		gravar_log_tasy(1908, ds_historico_w, wheb_usuario_pck.get_nm_usuario);

	exception
		when others then
			ds_erro_w := sqlerrm(sqlcode);
			gravar_log_tasy(1908, 'Erro ao gravar log de Altera��o da estrutura dos livros (LALUR/LACS): '|| chr(13) || chr(10)|| ds_erro_w, wheb_usuario_pck.get_nm_usuario);
	end;
end;
/


ALTER TABLE TASY.FIS_LIVROS_ESTR_PADRAO ADD (
  CONSTRAINT FISLIVESTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_LIVROS_ESTR_PADRAO ADD (
  CONSTRAINT FISLIVESTR_FISLIVESTR_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.FIS_LIVROS_ESTR_PADRAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_LIVROS_ESTR_PADRAO TO NIVEL_1;

