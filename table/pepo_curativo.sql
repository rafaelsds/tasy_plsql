ALTER TABLE TASY.PEPO_CURATIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_CURATIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_CURATIVO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_TIPO_CURATIVO       NUMBER(10),
  IE_TAMANHO_CURATIVO        VARCHAR2(10 BYTE)  DEFAULT null,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_TOPOGRAFIA          NUMBER(10),
  DT_LANCAMENTO_AUTOMATICO   DATE,
  NR_SEQ_PEPO                NUMBER(10),
  QT_CURATIVO                NUMBER(15,3),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NAO_REALIZA             VARCHAR2(1 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DT_EXECUCAO                DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPOCUR_CIRURGI_FK_I ON TASY.PEPO_CURATIVO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCUR_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCUR_PEPOCIR_FK_I ON TASY.PEPO_CURATIVO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOCUR_PESFISI_FK_I ON TASY.PEPO_CURATIVO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPOCUR_PK ON TASY.PEPO_CURATIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOCUR_TASASDI_FK_I ON TASY.PEPO_CURATIVO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCUR_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCUR_TASASDI_FK2_I ON TASY.PEPO_CURATIVO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCUR_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCUR_TIPCURA_FK_I ON TASY.PEPO_CURATIVO
(NR_SEQ_TIPO_CURATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCUR_TIPCURA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCUR_TOPDOR_FK_I ON TASY.PEPO_CURATIVO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCUR_TOPDOR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pepo_curativo_pend_atual 
after insert or update ON TASY.PEPO_CURATIVO 
for each row
declare 
 
qt_reg_w			number(1); 
ie_tipo_w			varchar2(10); 
ie_pepo_curativo_w			varchar2(10); 
cd_pessoa_fisica_w		varchar2(10); 
nr_atendimento_w		number(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_libera_curativo) 
into	ie_pepo_curativo_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
select	max(c.nr_atendimento), 
	max(c.cd_pessoa_fisica) 
into	nr_atendimento_w, 
	cd_pessoa_fisica_w 
from	cirurgia c 
where	c.nr_cirurgia = :new.nr_cirurgia; 
 
if 	(cd_pessoa_fisica_w is null) then 
	select	max(c.nr_atendimento), 
		max(c.cd_pessoa_fisica) 
	into	nr_atendimento_w, 
		cd_pessoa_fisica_w 
	from	pepo_cirurgia c 
	where	c.nr_sequencia = :new.nr_seq_pepo; 
end if; 
 
if	(nvl(ie_pepo_curativo_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'CR'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
 
		 
		ie_tipo_w := 'XCR'; 
	end if; 
			 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario); 
	end if; 
end if; 
 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


ALTER TABLE TASY.PEPO_CURATIVO ADD (
  CONSTRAINT PEPOCUR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEPO_CURATIVO ADD (
  CONSTRAINT PEPOCUR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPOCUR_TIPCURA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CURATIVO) 
 REFERENCES TASY.TIPO_CURATIVO (NR_SEQUENCIA),
  CONSTRAINT PEPOCUR_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPOCUR_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT PEPOCUR_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPOCUR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPOCUR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PEPO_CURATIVO TO NIVEL_1;

