ALTER TABLE TASY.AGEINT_REGRA_TEXTO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_REGRA_TEXTO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_REGRA_TEXTO_PROC
(
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  CD_PROCEDIMENTO       NUMBER(15),
  DS_TEXTO              LONG                    NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  IE_ORIGEM_PROCED      NUMBER(10),
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_AGEINT   NUMBER(10),
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  QT_IDADE_MIN          NUMBER(3),
  QT_IDADE_MAX          NUMBER(3),
  IE_SEXO               VARCHAR2(1 BYTE),
  CD_MEDICO             VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGRTXPR_AGEINGR_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(NR_SEQ_GRUPO_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_AREPROC_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_ESPPROC_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_ESTABEL_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_GRUPROC_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_MEDICO_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGRTXPR_PK ON TASY.AGEINT_REGRA_TEXTO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_PROCEDI_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGRTXPR_PROINTE_FK_I ON TASY.AGEINT_REGRA_TEXTO_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGEINT_REGRA_TEXTO_PROC ADD (
  CONSTRAINT AGRTXPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_REGRA_TEXTO_PROC ADD (
  CONSTRAINT AGRTXPR_AGEINGR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_AGEINT) 
 REFERENCES TASY.AGENDA_INT_GRUPO (NR_SEQUENCIA),
  CONSTRAINT AGRTXPR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT AGRTXPR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT AGRTXPR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT AGRTXPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGRTXPR_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGRTXPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGRTXPR_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGEINT_REGRA_TEXTO_PROC TO NIVEL_1;

