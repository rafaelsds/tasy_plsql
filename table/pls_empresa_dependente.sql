ALTER TABLE TASY.PLS_EMPRESA_DEPENDENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EMPRESA_DEPENDENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EMPRESA_DEPENDENTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_OPERADORA     NUMBER(10)               NOT NULL,
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  IE_TIPO_EMPRESA      VARCHAR2(2 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSEMDE_PESJURI_FK_I ON TASY.PLS_EMPRESA_DEPENDENTE
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEMDE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSEMDE_PK ON TASY.PLS_EMPRESA_DEPENDENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMDE_PLSACPF_FK_I ON TASY.PLS_EMPRESA_DEPENDENTE
(NR_SEQ_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_EMPRESA_DEPENDENTE ADD (
  CONSTRAINT PLSEMDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EMPRESA_DEPENDENTE ADD (
  CONSTRAINT PLSEMDE_PLSACPF_FK 
 FOREIGN KEY (NR_SEQ_OPERADORA) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSEMDE_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.PLS_EMPRESA_DEPENDENTE TO NIVEL_1;

