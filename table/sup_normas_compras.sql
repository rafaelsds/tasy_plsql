ALTER TABLE TASY.SUP_NORMAS_COMPRAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_NORMAS_COMPRAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_NORMAS_COMPRAS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_MATERIAL           NUMBER(10),
  NR_SEQ_FAMILIA        NUMBER(10),
  IE_MATERIAL_ESTOQUE   VARCHAR2(1 BYTE)        NOT NULL,
  IE_PADRONIZADO        VARCHAR2(1 BYTE)        NOT NULL,
  IE_URGENTE            VARCHAR2(1 BYTE)        NOT NULL,
  IE_TIPO_SERVICO       VARCHAR2(2 BYTE),
  CD_PERFIL             NUMBER(5),
  CD_CENTRO_CUSTO       NUMBER(8),
  QT_DIAS               NUMBER(5)               NOT NULL,
  IE_DIAS_UTEIS         VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUPNOCO_CENCUST_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_CLAMATE_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_ESTABEL_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_GRUMATE_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_MATERIA_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_MATFAMI_FK_I ON TASY.SUP_NORMAS_COMPRAS
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_MATFAMI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_PERFIL_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUPNOCO_PK ON TASY.SUP_NORMAS_COMPRAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUPNOCO_SUBMATE_FK_I ON TASY.SUP_NORMAS_COMPRAS
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPNOCO_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SUP_NORMAS_COMPRAS_INSERT
after insert or update ON TASY.SUP_NORMAS_COMPRAS for each row
declare
nr_sequencia_w		number(10);

PRAGMA AUTONOMOUS_TRANSACTION;

begin

select	nvl(max(a.nr_sequencia),0)
into	nr_sequencia_w
from	sup_normas_compras a
where	a.ie_situacao = 'A'
and	a.cd_estabelecimento	= :new.cd_estabelecimento
and	a.nr_sequencia		<> :new.nr_sequencia
and	nvl(a.cd_grupo_material,0)	= nvl(:new.cd_grupo_material,0)
and	nvl(a.cd_subgrupo_material,0)	= nvl(:new.cd_subgrupo_material,0)
and	nvl(a.cd_classe_material,0)	= nvl(:new.cd_classe_material,0)
and	nvl(a.cd_material,0)		= nvl(:new.cd_material,0)
and	nvl(a.nr_seq_familia,0)	= nvl(:new.nr_seq_familia,0)
and	nvl(a.cd_perfil,0)		= nvl(:new.cd_perfil,0)
and	nvl(a.cd_centro_custo,0)	= nvl(:new.cd_centro_custo,0)
and	nvl(a.ie_tipo_servico,'XX')	= nvl(:new.ie_tipo_servico,'XX')
and	((a.ie_material_estoque	= :new.ie_material_estoque) or
	((a.ie_material_estoque	= 'A') and (:new.ie_material_estoque in ('S','N'))) or
	((a.ie_material_estoque	in ('S','N')) and (:new.ie_material_estoque = 'A')))
and	((a.ie_padronizado = :new.ie_padronizado) or
	((a.ie_padronizado = 'A') and (:new.ie_padronizado in ('S','N'))) or
	((a.ie_padronizado in ('S','N')) and (:new.ie_padronizado = 'A')))
and	((a.ie_urgente = :new.ie_urgente) or
	((a.ie_urgente = 'A') and (:new.ie_urgente in ('S','N'))) or
	((a.ie_urgente in ('S','N')) and (:new.ie_urgente = 'A')));

if	(nr_sequencia_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(217597,'NR_SEQUENCIA_W='||nr_sequencia_w);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SUP_NORMAS_COMPRAS_tp  after update ON TASY.SUP_NORMAS_COMPRAS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_DIAS_UTEIS,1,4000),substr(:new.IE_DIAS_UTEIS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIAS_UTEIS',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS,1,4000),substr(:new.QT_DIAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FAMILIA,1,4000),substr(:new.NR_SEQ_FAMILIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FAMILIA',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MATERIAL_ESTOQUE,1,4000),substr(:new.IE_MATERIAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL_ESTOQUE',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_URGENTE,1,4000),substr(:new.IE_URGENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_URGENTE',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SERVICO,1,4000),substr(:new.IE_TIPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SERVICO',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRONIZADO,1,4000),substr(:new.IE_PADRONIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRONIZADO',ie_log_w,ds_w,'SUP_NORMAS_COMPRAS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_NORMAS_COMPRAS ADD (
  CONSTRAINT SUPNOCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_NORMAS_COMPRAS ADD (
  CONSTRAINT SUPNOCO_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT SUPNOCO_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT SUPNOCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SUPNOCO_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT SUPNOCO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SUPNOCO_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT SUPNOCO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT SUPNOCO_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.SUP_NORMAS_COMPRAS TO NIVEL_1;

