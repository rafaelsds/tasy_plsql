ALTER TABLE TASY.ATENDIMENTO_VISITA_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_VISITA_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_VISITA_GRUPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_VISITA  NUMBER(10),
  NR_SEQ_GRUPO_ACESSO  NUMBER(10)               NOT NULL,
  NR_ATENDIMENTO       NUMBER(10),
  DT_ACOMPANHANTE      DATE,
  DT_ENTRADA           DATE,
  DT_SAIDA             DATE,
  QT_ACESSOS           NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEVISG_ATEVISI_FK_I ON TASY.ATENDIMENTO_VISITA_GRUPO
(NR_SEQ_ATEND_VISITA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEVISG_ATEVISI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEVISG_CONACEV_FK_I ON TASY.ATENDIMENTO_VISITA_GRUPO
(NR_SEQ_GRUPO_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEVISG_CONACEV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATEVISG_PK ON TASY.ATENDIMENTO_VISITA_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATENDIMENTO_VISITA_GRUPO ADD (
  CONSTRAINT ATEVISG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_VISITA_GRUPO ADD (
  CONSTRAINT ATEVISG_ATEVISI_FK 
 FOREIGN KEY (NR_SEQ_ATEND_VISITA) 
 REFERENCES TASY.ATENDIMENTO_VISITA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEVISG_CONACEV_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_ACESSO) 
 REFERENCES TASY.CONTROLE_ACESSO_VISITA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_VISITA_GRUPO TO NIVEL_1;

