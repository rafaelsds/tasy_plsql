ALTER TABLE TASY.PRM_FICHA_AREA_ORIGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRM_FICHA_AREA_ORIGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRM_FICHA_AREA_ORIGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_FICHA         NUMBER(10)               NOT NULL,
  NR_SEQ_AREA          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRMFIAO_PK ON TASY.PRM_FICHA_AREA_ORIGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMFIAO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRMFIAO_PRMAROR_FK_I ON TASY.PRM_FICHA_AREA_ORIGEM
(NR_SEQ_AREA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMFIAO_PRMAROR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRMFIAO_PRMFIOCO_FK_I ON TASY.PRM_FICHA_AREA_ORIGEM
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMFIAO_PRMFIOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRM_FICHA_AREA_ORIGEM ADD (
  CONSTRAINT PRMFIAO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRM_FICHA_AREA_ORIGEM ADD (
  CONSTRAINT PRMFIAO_PRMAROR_FK 
 FOREIGN KEY (NR_SEQ_AREA) 
 REFERENCES TASY.PRM_AREA_ORIGEM (NR_SEQUENCIA),
  CONSTRAINT PRMFIAO_PRMFIOCO_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.PRM_FICHA_OCORRENCIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRM_FICHA_AREA_ORIGEM TO NIVEL_1;

