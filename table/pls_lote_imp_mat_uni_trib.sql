ALTER TABLE TASY.PLS_LOTE_IMP_MAT_UNI_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_IMP_MAT_UNI_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_IMP_MAT_UNI_TRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MAT_UNIMED    NUMBER(10)               NOT NULL,
  VL_PERC_ICMS         NUMBER(7,2),
  VL_PMC               NUMBER(15,4),
  VL_TCL               NUMBER(2),
  ID_ALC               VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSIMUT_PK ON TASY.PLS_LOTE_IMP_MAT_UNI_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSIMUT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSIMUT_PLSLIMR_FK_I ON TASY.PLS_LOTE_IMP_MAT_UNI_TRIB
(NR_SEQ_MAT_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSIMUT_PLSLIMR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOTE_IMP_MAT_UNI_TRIB ADD (
  CONSTRAINT PLSIMUT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_IMP_MAT_UNI_TRIB ADD (
  CONSTRAINT PLSIMUT_PLSLIMR_FK 
 FOREIGN KEY (NR_SEQ_MAT_UNIMED) 
 REFERENCES TASY.PLS_LOTE_IMP_MAT_UNI_REG (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_LOTE_IMP_MAT_UNI_TRIB TO NIVEL_1;

