ALTER TABLE TASY.PLS_TIPO_GUIA_MED_TP_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_GUIA_MED_TP_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_GUIA_MED_TP_PREST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_GUIA       NUMBER(10)             NOT NULL,
  NR_SEQ_PRESTADOR       NUMBER(10),
  IE_ENTRA_GUIA_MEDICO   VARCHAR2(1 BYTE)       NOT NULL,
  IE_ENTRA_PORTAL        VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_TIPO_PRESTADOR  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTGTP_PK ON TASY.PLS_TIPO_GUIA_MED_TP_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTGTP_PLSPRES_FK_I ON TASY.PLS_TIPO_GUIA_MED_TP_PREST
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTGTP_PLSTGME_FK_I ON TASY.PLS_TIPO_GUIA_MED_TP_PREST
(NR_SEQ_TIPO_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTGTP_PLSTIPR_FK_I ON TASY.PLS_TIPO_GUIA_MED_TP_PREST
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TIPO_GUIA_MED_TP_PREST ADD (
  CONSTRAINT PLSTGTP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_GUIA_MED_TP_PREST ADD (
  CONSTRAINT PLSTGTP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTGTP_PLSTGME_FK 
 FOREIGN KEY (NR_SEQ_TIPO_GUIA) 
 REFERENCES TASY.PLS_TIPO_GUIA_MEDICO (NR_SEQUENCIA),
  CONSTRAINT PLSTGTP_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TIPO_GUIA_MED_TP_PREST TO NIVEL_1;

