ALTER TABLE TASY.SAN_PARAMETRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_PARAMETRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_PARAMETRO
(
  CD_MEDICO_RESP               VARCHAR2(10 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DS_ENTIDADE                  VARCHAR2(60 BYTE),
  DS_END_ENTIDADE              VARCHAR2(60 BYTE),
  DS_CEP_ENTIDADE              VARCHAR2(8 BYTE),
  DS_MUNIC_ENTIDADE            VARCHAR2(30 BYTE),
  SG_ESTADO_ENTIDADE           VARCHAR2(2 BYTE),
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_HEMOGLOBINA           NUMBER(10),
  NR_SEQ_HEMATOCRITO           NUMBER(10),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  IE_GERA_RESERVA_PRESCR       VARCHAR2(1 BYTE) NOT NULL,
  IE_DATA_EXAME_EMP            VARCHAR2(1 BYTE) NOT NULL,
  IE_FINALIZAR_TRANS_CONTA     VARCHAR2(1 BYTE),
  IE_RESERVA_NAO_UTILIZADA     VARCHAR2(1 BYTE),
  IE_GERAR_CONTA_RES_NAO_UTIL  VARCHAR2(1 BYTE),
  PR_HEMAT_MIN_FEM             NUMBER(10,4),
  PR_HEMAT_MAX_FEM             NUMBER(10,4),
  PR_HEMAT_MIN_MAS             NUMBER(10,4),
  PR_HEMAT_MAX_MAS             NUMBER(10,4),
  IE_REGRA_USO                 VARCHAR2(1 BYTE) NOT NULL,
  NR_SANGUE_INICIAL            VARCHAR2(20 BYTE),
  IE_GERAR_NR_SANGUE           VARCHAR2(1 BYTE),
  NR_SEQ_ENTIDADE              NUMBER(10),
  CD_SETOR_CONTA               NUMBER(5),
  NR_SEQ_EXAME_RH              NUMBER(10),
  NR_SEQ_EXAME_TIPO            NUMBER(10),
  CD_REGIONAL                  NUMBER(10),
  IE_UTILIZA_FISICA_CLINICA    VARCHAR2(1 BYTE),
  IE_GERAR_BOLSA_NAO_UTIL      VARCHAR2(1 BYTE),
  DS_MENSAGEM_AUTO_EXC         LONG,
  IE_ATUALIZAR_TIPO_SANGUE     VARCHAR2(1 BYTE),
  NR_SEQ_FORMA_CONTATO         NUMBER(10),
  NR_SEQ_TIPO_DOC              NUMBER(10),
  NR_SEQ_FORMA_CONTATO_SORO    NUMBER(10),
  NR_SEQ_TIPO_DOC_SORO         NUMBER(10),
  QT_HEMOGLOBINA_MIN_MAS       NUMBER(10,3),
  QT_HEMOGLOBINA_MAX_MAS       NUMBER(10,3),
  QT_HEMOGLOBINA_MIN_FEM       NUMBER(10,3),
  QT_HEMOGLOBINA_MAX_FEM       NUMBER(10,3),
  IE_DATA_PROCEDIMENTO_CONTA   VARCHAR2(1 BYTE),
  IE_TIPO_DATA_VIGENCIA        VARCHAR2(1 BYTE),
  IE_ALTERA_VOLUME             VARCHAR2(1 BYTE),
  IE_CONTAR_DOACAO             VARCHAR2(1 BYTE),
  DS_MENSAGEM_EMAIL_APTO       VARCHAR2(255 BYTE),
  IE_NOVO_QUESTIONARIO         VARCHAR2(1 BYTE),
  IE_UTILIZA_LEOCOMETRIA       VARCHAR2(1 BYTE),
  QT_VALIDADE_SOLIC_RESERVA    NUMBER(5),
  CD_OPERACAO_ESTOQUE          NUMBER(3),
  DS_REMETENTE_PADRAO          VARCHAR2(255 BYTE),
  NR_SEQ_TIPO_BAIXA            NUMBER(10),
  IE_NUMERO_BOLSA              VARCHAR2(1 BYTE),
  IE_ATUALIZAR_EXAMES          VARCHAR2(1 BYTE),
  IE_GERAR_CONTA_ITENS_SOLIC   VARCHAR2(1 BYTE),
  IE_ATUALIZA_NULO             VARCHAR2(1 BYTE),
  IE_PF_REALIZOU_RESERVA       VARCHAR2(1 BYTE),
  CD_LOCAL_ISBT                VARCHAR2(5 BYTE),
  IE_SETOR_CONTA               VARCHAR2(1 BYTE),
  IE_PADRAO_ISBT_128           VARCHAR2(1 BYTE),
  QT_PESO_MINIMO_DOACAO        NUMBER(5),
  QT_IDADE_MINIMA              NUMBER(5),
  QT_IDADE_MAXIMA              NUMBER(5),
  NR_SEQ_EXAME_SUB_ABO         NUMBER(10),
  QT_DIAS_INAPTO_HEMAT_M       NUMBER(10),
  QT_DIAS_INAPTO_HEMAT_F       NUMBER(10),
  QT_DIAS_INAPTO_HEMOG_M       NUMBER(10),
  QT_DIAS_INAPTO_HEMOG_F       NUMBER(10),
  IE_EXIGE_LIB_RESERVA         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANPARA_ESTABEL_FK_I ON TASY.SAN_PARAMETRO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPARA_FORCONT_FK_I ON TASY.SAN_PARAMETRO
(NR_SEQ_FORMA_CONTATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_FORCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_FORCONT_FK1_I ON TASY.SAN_PARAMETRO
(NR_SEQ_FORMA_CONTATO_SORO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_FORCONT_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_MEDICO_FK_I ON TASY.SAN_PARAMETRO
(CD_MEDICO_RESP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPARA_OPEESTO_FK_I ON TASY.SAN_PARAMETRO
(CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_OPEESTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SANPARA_PK ON TASY.SAN_PARAMETRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_SANENTI_FK_I ON TASY.SAN_PARAMETRO
(NR_SEQ_ENTIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_SANENTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_SANEXAM_FK_I ON TASY.SAN_PARAMETRO
(NR_SEQ_HEMOGLOBINA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_SANEXAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_SANEXAM_FK2_I ON TASY.SAN_PARAMETRO
(NR_SEQ_HEMATOCRITO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_SANEXAM_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_SANEXAM_FK3_I ON TASY.SAN_PARAMETRO
(NR_SEQ_EXAME_RH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_SANEXAM_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_SANEXAM_FK4_I ON TASY.SAN_PARAMETRO
(NR_SEQ_EXAME_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_SANEXAM_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_SANEXAM_FK5_I ON TASY.SAN_PARAMETRO
(NR_SEQ_EXAME_SUB_ABO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANPARA_SETATEN_FK_I ON TASY.SAN_PARAMETRO
(CD_SETOR_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_TIPBAPR_FK_I ON TASY.SAN_PARAMETRO
(NR_SEQ_TIPO_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_TIPBAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_TPDOCCO_FK_I ON TASY.SAN_PARAMETRO
(NR_SEQ_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_TPDOCCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPARA_TPDOCCO_FK1_I ON TASY.SAN_PARAMETRO
(NR_SEQ_TIPO_DOC_SORO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPARA_TPDOCCO_FK1_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_PARAMETRO ADD (
  CONSTRAINT SANPARA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_PARAMETRO ADD (
  CONSTRAINT SANPARA_SANEXAM_FK5 
 FOREIGN KEY (NR_SEQ_EXAME_SUB_ABO) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANPARA_TPDOCCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOC) 
 REFERENCES TASY.TIPO_DOC_CONTATO (NR_SEQUENCIA),
  CONSTRAINT SANPARA_FORCONT_FK 
 FOREIGN KEY (NR_SEQ_FORMA_CONTATO) 
 REFERENCES TASY.FORMA_CONTATO (NR_SEQUENCIA),
  CONSTRAINT SANPARA_FORCONT_FK1 
 FOREIGN KEY (NR_SEQ_FORMA_CONTATO_SORO) 
 REFERENCES TASY.FORMA_CONTATO (NR_SEQUENCIA),
  CONSTRAINT SANPARA_TPDOCCO_FK1 
 FOREIGN KEY (NR_SEQ_TIPO_DOC_SORO) 
 REFERENCES TASY.TIPO_DOC_CONTATO (NR_SEQUENCIA),
  CONSTRAINT SANPARA_OPEESTO_FK 
 FOREIGN KEY (CD_OPERACAO_ESTOQUE) 
 REFERENCES TASY.OPERACAO_ESTOQUE (CD_OPERACAO_ESTOQUE),
  CONSTRAINT SANPARA_TIPBAPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_BAIXA) 
 REFERENCES TASY.TIPO_BAIXA_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT SANPARA_SANEXAM_FK 
 FOREIGN KEY (NR_SEQ_HEMOGLOBINA) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANPARA_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT SANPARA_SANEXAM_FK2 
 FOREIGN KEY (NR_SEQ_HEMATOCRITO) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANPARA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SANPARA_SANENTI_FK 
 FOREIGN KEY (NR_SEQ_ENTIDADE) 
 REFERENCES TASY.SAN_ENTIDADE (NR_SEQUENCIA),
  CONSTRAINT SANPARA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_CONTA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SANPARA_SANEXAM_FK3 
 FOREIGN KEY (NR_SEQ_EXAME_RH) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANPARA_SANEXAM_FK4 
 FOREIGN KEY (NR_SEQ_EXAME_TIPO) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_PARAMETRO TO NIVEL_1;

