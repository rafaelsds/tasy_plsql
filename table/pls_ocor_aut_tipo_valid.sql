ALTER TABLE TASY.PLS_OCOR_AUT_TIPO_VALID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCOR_AUT_TIPO_VALID CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCOR_AUT_TIPO_VALID
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_VALIDACAO         NUMBER(5)                NOT NULL,
  NM_VALIDACAO         VARCHAR2(255 BYTE)       NOT NULL,
  DS_FUNCIONAMENTO     VARCHAR2(4000 BYTE)      NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOATV_PK ON TASY.PLS_OCOR_AUT_TIPO_VALID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_OCOR_AUT_TIPO_VALID ADD (
  CONSTRAINT PLSOATV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_OCOR_AUT_TIPO_VALID TO NIVEL_1;

