ALTER TABLE TASY.PLS_OC_CTA_REGRA_INTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_REGRA_INTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_REGRA_INTER
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  QT_MININA             NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  QT_MAXIMA             NUMBER(10),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NM_REGRA              VARCHAR2(255 BYTE),
  IE_REGIME_INTERNACAO  VARCHAR2(1 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRINTE_ESTABEL_FK_I ON TASY.PLS_OC_CTA_REGRA_INTER
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRINTE_PK ON TASY.PLS_OC_CTA_REGRA_INTER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_REGRA_INTER_tp  after update ON TASY.PLS_OC_CTA_REGRA_INTER FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_REGRA,1,500);gravar_log_alteracao(substr(:old.NM_REGRA,1,4000),substr(:new.NM_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_REGRA',ie_log_w,ds_w,'PLS_OC_CTA_REGRA_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAXIMA,1,4000),substr(:new.QT_MAXIMA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAXIMA',ie_log_w,ds_w,'PLS_OC_CTA_REGRA_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_OC_CTA_REGRA_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGIME_INTERNACAO,1,4000),substr(:new.IE_REGIME_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGIME_INTERNACAO',ie_log_w,ds_w,'PLS_OC_CTA_REGRA_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MININA,1,4000),substr(:new.QT_MININA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MININA',ie_log_w,ds_w,'PLS_OC_CTA_REGRA_INTER',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_REGRA_INTER ADD (
  CONSTRAINT PLSRINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_REGRA_INTER ADD (
  CONSTRAINT PLSRINTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_OC_CTA_REGRA_INTER TO NIVEL_1;

