ALTER TABLE TASY.AGENDA_HC_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_HC_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_HC_PACIENTE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA           NUMBER(10),
  DT_AGENDA               DATE                  NOT NULL,
  IE_STATUS_AGENDA        VARCHAR2(15 BYTE)     NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  NR_SEQ_PAC_COMO_CHEGAR  NUMBER(10),
  NR_SEQ_CLASSIFICACAO    NUMBER(10),
  NR_SEQ_PACIENTE_HC      NUMBER(10),
  NR_SEQ_MOTIVO           NUMBER(10),
  DS_MOTIVO_CANC          VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_CANC      NUMBER(10),
  NR_SEQ_MOTIVO_TRANSF    NUMBER(10),
  IE_PRESCRICAO           VARCHAR2(1 BYTE),
  NR_PRESCRICAO           NUMBER(14),
  NR_MINUTO_DURACAO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEHCPA_AGEHOCA_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_AGEHOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_AGHOCAC_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_AGHOCAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_HCCOCHP_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_PAC_COMO_CHEGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_HCCOCHP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_HCMOTATRA_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_HCMOTATRA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_HCMOTRANSF_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_MOTIVO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_HCMOTRANSF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_HDMOTCANC_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_HDMOTCANC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_PACHOCA_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_SEQ_PACIENTE_HC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_PACHOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_PESFISI_FK_I ON TASY.AGENDA_HC_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEHCPA_PK ON TASY.AGENDA_HC_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGEHCPA_PRESMED_FK_I ON TASY.AGENDA_HC_PACIENTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEHCPA_PRESMED_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AGENDA_HC_PACIENTE_tp  after update ON TASY.AGENDA_HC_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA,1,4000),substr(:new.NR_SEQ_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_AGENDA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_AGENDA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_AGENDA',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_AGENDA,1,4000),substr(:new.IE_STATUS_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_AGENDA',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_CANC,1,4000),substr(:new.NR_SEQ_MOTIVO_CANC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_CANC',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PACIENTE_HC,1,4000),substr(:new.NR_SEQ_PACIENTE_HC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PACIENTE_HC',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO,1,4000),substr(:new.NR_SEQ_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MOTIVO_CANC,1,4000),substr(:new.DS_MOTIVO_CANC,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_CANC',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAC_COMO_CHEGAR,1,4000),substr(:new.NR_SEQ_PAC_COMO_CHEGAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAC_COMO_CHEGAR',ie_log_w,ds_w,'AGENDA_HC_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_hc_paciente_Atual
BEFORE Insert or update ON TASY.AGENDA_HC_PACIENTE FOR EACH ROW
DECLARE

nr_controle_w			number(10);
ie_perm_dt_ret_w 		varchar2(1);
cd_estabelecimento_w	number(4);
ie_cons_pac_hc_w	varchar2(1);

begin
obter_param_usuario(867, 43, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_cons_pac_hc_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	Count(*)
	into	nr_controle_w
	from	Paciente_home_care
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	if	(nr_controle_w = 0) and
		(ie_cons_pac_hc_w = 'S') then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(262781);
	end if;

	obter_param_usuario(867, 6, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_dt_ret_w);

	If	(ie_perm_dt_ret_w = 'N') and
	    (:new.dt_agenda < sysdate - 240/84600 ) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(262780);
	end if;

end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_hc_paciente_AfterPost
AFTER INSERT or update ON TASY.AGENDA_HC_PACIENTE FOR EACH ROW
DECLARE

ie_gerar_solic_pront_w	varchar2(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then


	select 	max(ie_gerar_solic_pront)
	into 	ie_gerar_solic_pront_w
	from 	agenda_home_care
	where 	nr_sequencia = :new.nr_seq_agenda;


	if (ie_gerar_solic_pront_w = 'S') and
	   ((:old.cd_pessoa_fisica is null) or
	   (:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica)) then
	   hc_Gerar_Solic_Pront_Agenda(:new.cd_pessoa_fisica,
				       :new.nr_seq_agenda,
				       :new.nr_sequencia,
				       :new.dt_agenda,
				       :new.nm_usuario
				      );
	end if;
end if;
END;
/


ALTER TABLE TASY.AGENDA_HC_PACIENTE ADD (
  CONSTRAINT AGEHCPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_HC_PACIENTE ADD (
  CONSTRAINT AGEHCPA_HCMOTRANSF_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TRANSF) 
 REFERENCES TASY.HC_MOTIVO_TRANSFERENCIA (NR_SEQUENCIA),
  CONSTRAINT AGEHCPA_PACHOCA_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE_HC) 
 REFERENCES TASY.PACIENTE_HOME_CARE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEHCPA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT AGEHCPA_HCMOTATRA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.HC_MOTIVO_ATRASO (NR_SEQUENCIA),
  CONSTRAINT AGEHCPA_HDMOTCANC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.HC_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEHCPA_AGEHOCA_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_HOME_CARE (NR_SEQUENCIA),
  CONSTRAINT AGEHCPA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEHCPA_HCCOCHP_FK 
 FOREIGN KEY (NR_SEQ_PAC_COMO_CHEGAR) 
 REFERENCES TASY.HC_COMO_CHEGAR_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT AGEHCPA_AGHOCAC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.AGENDA_HOME_CARE_CLASSIF (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_HC_PACIENTE TO NIVEL_1;

