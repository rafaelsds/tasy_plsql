ALTER TABLE TASY.PLS_COMUNIC_EXT_ANEXO_WEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMUNIC_EXT_ANEXO_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMUNIC_EXT_ANEXO_WEB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_COMUNICADO    NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  NM_ARQUIVO           VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCEAW_PK ON TASY.PLS_COMUNIC_EXT_ANEXO_WEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCEAW_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCEAW_PLSCEXW_FK_I ON TASY.PLS_COMUNIC_EXT_ANEXO_WEB
(NR_SEQ_COMUNICADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCEAW_PLSCEXW_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_COMUNIC_EXT_ANEXO_WEB ADD (
  CONSTRAINT PLSCEAW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMUNIC_EXT_ANEXO_WEB ADD (
  CONSTRAINT PLSCEAW_PLSCEXW_FK 
 FOREIGN KEY (NR_SEQ_COMUNICADO) 
 REFERENCES TASY.PLS_COMUNIC_EXTERNA_WEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COMUNIC_EXT_ANEXO_WEB TO NIVEL_1;

