ALTER TABLE TASY.CIH_MICROORG_MEDIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIH_MICROORG_MEDIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIH_MICROORG_MEDIC
(
  CD_MICROORGANISMO     NUMBER(10)              NOT NULL,
  CD_MEDICAMENTO        NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_MATERIAL       NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_MICRO    NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIHMIME_CIHMEDI_FK_I ON TASY.CIH_MICROORG_MEDIC
(CD_MEDICAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHMIME_CIHMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHMIME_CIHMICR_FK_I ON TASY.CIH_MICROORG_MEDIC
(CD_MICROORGANISMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIHMIME_MATEXLA_FK_I ON TASY.CIH_MICROORG_MEDIC
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIHMIME_PK ON TASY.CIH_MICROORG_MEDIC
(CD_MICROORGANISMO, NR_SEQ_MATERIAL, CD_MEDICAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIHMIME_SETATEN_FK_I ON TASY.CIH_MICROORG_MEDIC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHMIME_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CIH_MICROORG_MEDIC ADD (
  CONSTRAINT CIHMIME_PK
 PRIMARY KEY
 (CD_MICROORGANISMO, NR_SEQ_MATERIAL, CD_MEDICAMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIH_MICROORG_MEDIC ADD (
  CONSTRAINT CIHMIME_CIHMICR_FK 
 FOREIGN KEY (CD_MICROORGANISMO) 
 REFERENCES TASY.CIH_MICROORGANISMO (CD_MICROORGANISMO)
    ON DELETE CASCADE,
  CONSTRAINT CIHMIME_CIHMEDI_FK 
 FOREIGN KEY (CD_MEDICAMENTO) 
 REFERENCES TASY.CIH_MEDICAMENTO (CD_MEDICAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT CIHMIME_MATEXLA_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIHMIME_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CIH_MICROORG_MEDIC TO NIVEL_1;

