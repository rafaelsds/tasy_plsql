ALTER TABLE TASY.OFT_CONSULTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_CONSULTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_CONSULTA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_ATENDIMENTO        NUMBER(10),
  NR_ATEND_CONSULTORIO  NUMBER(10),
  DT_CONSULTA           DATE                    NOT NULL,
  CD_MEDICO             VARCHAR2(10 BYTE)       NOT NULL,
  DT_FIM_CONSULTA       DATE,
  DS_RESUMO             LONG,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_CONSULTA  NUMBER(10),
  NR_SEQ_STATUS         NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  DT_CANCELAMENTO       DATE,
  DT_INICIO_CONSULTA    DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTCONS_ATEPACI_FK_I ON TASY.OFT_CONSULTA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCONS_ESTABEL_FK_I ON TASY.OFT_CONSULTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCONS_MEDATEN_FK_I ON TASY.OFT_CONSULTA
(NR_ATEND_CONSULTORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONS_MEDATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONS_OFTICON_FK_I ON TASY.OFT_CONSULTA
(NR_SEQ_TIPO_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONS_OFTICON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONS_OFTSTCO_FK_I ON TASY.OFT_CONSULTA
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTCONS_OFTSTCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTCONS_PESFISI_FK_I ON TASY.OFT_CONSULTA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTCONS_PESFISI_FK2_I ON TASY.OFT_CONSULTA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFTCONS_PK ON TASY.OFT_CONSULTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.oft_consulta_delete
before delete ON TASY.OFT_CONSULTA FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

update   atendimento_paciente
set      nr_seq_oftalmo = null
where    nr_seq_oftalmo = :old.nr_sequencia;

update   agenda_paciente
set      nr_seq_oftalmo = null
where    nr_seq_oftalmo = :old.nr_sequencia;

update   agenda_consulta
set      nr_seq_oftalmo = null
where    nr_seq_oftalmo = :old.nr_sequencia;

update   atestado_paciente
set      nr_seq_consulta = null
where    nr_seq_consulta = :old.nr_sequencia;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.OFT_CONSULTA_ATUAL
BEFORE INSERT ON TASY.OFT_CONSULTA FOR EACH ROW
DECLARE

ie_gera_sem_atend_w		varchar2(1) := 'S';

BEGIN

Obter_Param_Usuario(3010,95,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_gera_sem_atend_w);

if	(:new.cd_estabelecimento is null) then
	:new.cd_estabelecimento := wheb_usuario_pck.get_cd_estabelecimento;
end if;

if	(ie_gera_sem_atend_w = 'N') and
	(:new.nr_atendimento is null) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(203246);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.oft_consulta_insert
after INSERT ON TASY.OFT_CONSULTA FOR EACH ROW
declare

nr_sequencia_w 	number(10);
ie_atualiza_triagem_w   varchar2(1);
cd_perfil_w          perfil.cd_perfil%type                     := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w         usuario.nm_usuario%type                   := wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w estabelecimento.cd_estabelecimento%type   := wheb_usuario_pck.get_cd_estabelecimento;

begin
obter_param_usuario(3010, 150, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_atualiza_triagem_w);

begin
if (ie_atualiza_triagem_w = 'S') and
   (:new.nr_atendimento is not null) then
   update   atendimento_paciente
   set      dt_inicio_atendimento   = :new.dt_consulta,
            dt_fim_triagem          = :new.dt_consulta
   where    nr_atendimento          = :new.nr_atendimento
   and      dt_inicio_atendimento   is null
   and      dt_fim_triagem          is null;
end if;
exception
when others then
	null;
end;

select	oft_log_status_consulta_seq.nextval
into	nr_sequencia_w
from 	dual;

if (nvl(:new.nr_seq_status,0) > 0) then
	insert into oft_log_status_consulta(
					nr_sequencia,
					nr_seq_status,
					nr_seq_consulta,
					dt_atualizacao,
					nm_usuario)
	values 			(nr_sequencia_w,
					:new.nr_seq_status,
					:new.nr_sequencia, --antes era :old.nr_sequencia
					sysdate,
					:new.nm_usuario);

	insert into	oft_status_consulta_hist(
					nr_sequencia,
					nr_seq_consulta,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_status,
					dt_status)
	values			(oft_status_consulta_hist_seq.nextval,
					:new.nr_sequencia,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.nr_seq_status,
					sysdate);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.oft_consulta_update
after UPDATE ON TASY.OFT_CONSULTA FOR EACH ROW
declare

ie_modifica_fim_cons_w	varchar2(1);
nr_sequencia_w 	number(10);
ie_liberar_avaliacao_w varchar2(1);
ie_atualiza_triagem_w   varchar2(1);
cd_perfil_w          perfil.cd_perfil%type                     := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w         usuario.nm_usuario%type                   := wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w estabelecimento.cd_estabelecimento%type   := wheb_usuario_pck.get_cd_estabelecimento;

begin
obter_param_usuario(3010, 150, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_atualiza_triagem_w);

begin
if (ie_atualiza_triagem_w = 'S') and
   (:old.nr_atendimento is null) and
   (:new.nr_atendimento is not null) then
   update   atendimento_paciente
   set      dt_inicio_atendimento   = :new.dt_consulta,
            dt_fim_triagem          = :new.dt_consulta
   where    nr_atendimento          = :new.nr_atendimento
   and      dt_inicio_atendimento   is null
   and      dt_fim_triagem          is null;
end if;
exception
when others then
	null;
end;


if ((nvl(:old.nr_seq_status,0)) <> nvl(:new.nr_seq_status,0)) then
	select	max(IE_LIBERAR_AVALIACAO)
	into	ie_liberar_avaliacao_w
	from	oft_status_consulta
	where 	nr_sequencia = :new.nr_seq_status;

	if (ie_liberar_avaliacao_w = 'S') then
		update med_avaliacao_paciente
		set    DT_LIBERACAO = sysdate
		where  nr_atendimento = :new.nr_atendimento
		and    DT_LIBERACAO is null
		and    nr_seq_consulta = :new.nr_sequencia;
	end if;
end if;

if 	(:new.nr_seq_status is not null) and
	((nvl(:old.nr_seq_status,0)) <> nvl(:new.nr_seq_status,0)) then

	select	oft_log_status_consulta_seq.nextval
	into	nr_sequencia_w
	from 	dual;

	insert into oft_log_status_consulta (
		nr_sequencia,
		nr_seq_status,
		nr_seq_consulta,
		dt_atualizacao,
		nm_usuario)
	values (
		nr_sequencia_w,
		:new.nr_seq_status,
		:old.nr_sequencia,
		sysdate,
		:new.nm_usuario);

	insert	into	oft_status_consulta_hist(nr_sequencia,
						nr_seq_consulta,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_status,
						dt_status)
	values					(oft_status_consulta_hist_seq.nextval,
						:new.nr_sequencia,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						:new.nr_seq_status,
						sysdate);
end if;

end;
/


ALTER TABLE TASY.OFT_CONSULTA ADD (
  CONSTRAINT OFTCONS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_CONSULTA ADD (
  CONSTRAINT OFTCONS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT OFTCONS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT OFTCONS_MEDATEN_FK 
 FOREIGN KEY (NR_ATEND_CONSULTORIO) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO),
  CONSTRAINT OFTCONS_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT OFTCONS_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT OFTCONS_OFTICON_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CONSULTA) 
 REFERENCES TASY.OFT_TIPO_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT OFTCONS_OFTSTCO_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.OFT_STATUS_CONSULTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.OFT_CONSULTA TO NIVEL_1;

