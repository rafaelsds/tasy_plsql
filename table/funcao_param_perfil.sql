ALTER TABLE TASY.FUNCAO_PARAM_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_PARAM_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_PARAM_PERFIL
(
  CD_FUNCAO           NUMBER(5)                 NOT NULL,
  NR_SEQUENCIA        NUMBER(5)                 NOT NULL,
  CD_PERFIL           NUMBER(5)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  VL_PARAMETRO        VARCHAR2(255 BYTE),
  DS_OBSERVACAO       VARCHAR2(4000 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  NR_SEQ_INTERNO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNPAPE_DOMINIO_FK_I ON TASY.FUNCAO_PARAM_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPAPE_ESTABEL_FK_I ON TASY.FUNCAO_PARAM_PERFIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FUNPAPE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FUNPAPE_PK ON TASY.FUNCAO_PARAM_PERFIL
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNPAPE_UK ON TASY.FUNCAO_PARAM_PERFIL
(CD_FUNCAO, NR_SEQUENCIA, CD_PERFIL, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.funcao_param_perfil_dt_param
after INSERT OR DELETE OR UPDATE ON TASY.FUNCAO_PARAM_PERFIL for each row
declare
	ie_consistir_valor_w	varchar2(1);
	vl_parametro_padrao_w	varchar2(255);
begin

update	funcao
set	dt_atualizacao_param = sysdate
where	cd_funcao = :new.cd_funcao;

if (inserting or updating) then
	select	ie_consistir_valor,
		vl_parametro_padrao
	into	ie_consistir_valor_w,
		vl_parametro_padrao_w
	from	funcao_parametro
	where	cd_funcao = nvl(:new.cd_funcao, :old.cd_funcao)
	and	nr_sequencia = nvl(:new.nr_sequencia, :old.nr_sequencia);

	if	(ie_consistir_valor_w = 'S') and
		(somente_numero(:new.vl_parametro) < somente_numero(vl_parametro_padrao_w)) then
		wheb_mensagem_pck.exibir_mensagem_abort(55208);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_perfil_subject FOR
  DELETE OR INSERT OR UPDATE ON TASY.FUNCAO_PARAM_PERFIL 
COMPOUND TRIGGER
  apply_sync   BOOLEAN := false;
  AFTER EACH ROW IS BEGIN
    apply_sync := psa_is_auth_parameter(
      :new.cd_funcao,
      :new.nr_sequencia
    ) = 'TRUE' OR apply_sync;
  END AFTER EACH ROW;
  AFTER STATEMENT IS BEGIN
    IF
      apply_sync
    THEN
      psa_synchronize_subject;
    END IF;
  END AFTER STATEMENT;
END;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_perfil_insert
BEFORE INSERT ON TASY.FUNCAO_PARAM_PERFIL FOR EACH ROW
DECLARE
nr_seq_interno_w NUMBER(10);

BEGIN

IF (:NEW.nr_seq_interno IS NULL) THEN
 SELECT funcao_param_perfil_seq.NEXTVAL
 INTO nr_seq_interno_w
 FROM dual;

 :NEW.nr_seq_interno := nr_seq_interno_w;
END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_perfil_ins_upd
before insert or update ON TASY.FUNCAO_PARAM_PERFIL for each row
declare

ie_protocolo_vig_w	varchar2(1);

Cursor C01 is
	select 	distinct
			nr_seq_protocolo
	from   	concorde_perfil
	where  	cd_perfil = :new.cd_perfil
	order by 1;

c01_w				C01%rowtype;

begin

if 	(:old.vl_parametro is null) and (:new.vl_parametro is not null) or
	(:new.vl_parametro <> :old.vl_parametro) then
	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin
			select 	decode(count(*),0,'N','S')
			into   	ie_protocolo_vig_w
			from	concorde_protocolo
			where   nr_sequencia = c01_w.nr_seq_protocolo
			and		sysdate between dt_inicio_vigencia and dt_fim_vigencia;

			if (ie_protocolo_vig_w = 'S') then
				update 	concorde_protocolo
				set		dt_alteracao = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = :new.nm_usuario
				where 	nr_sequencia = c01_w.nr_seq_protocolo;

				update 	perfil
				set		dt_alteracao = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = :new.nm_usuario
				where 	cd_perfil = :new.cd_perfil;
			end if;
		end;
	end loop;
	close C01;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_perfil_delete
before delete ON TASY.FUNCAO_PARAM_PERFIL for each row
declare

ie_protocolo_vig_w	varchar2(1);

Cursor C01 is
	select 	distinct
			nr_seq_protocolo
	from   	concorde_perfil
	where  	cd_perfil = :old.cd_perfil
	order by 1;

c01_w				C01%rowtype;

begin

if 	(:old.vl_parametro is not null) then
	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin
			select 	decode(count(*),0,'N','S')
			into   	ie_protocolo_vig_w
			from	concorde_protocolo
			where   nr_sequencia = c01_w.nr_seq_protocolo
			and		sysdate between dt_inicio_vigencia and dt_fim_vigencia;

			if (ie_protocolo_vig_w = 'S') then
				update 	concorde_protocolo
				set		dt_alteracao = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = :old.nm_usuario
				where 	nr_sequencia = c01_w.nr_seq_protocolo;

				update 	perfil
				set		dt_alteracao = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = :old.nm_usuario
				where 	cd_perfil = :old.cd_perfil;

			end if;
		end;
	end loop;
	close C01;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAM_PERFIL_tp  after update ON TASY.FUNCAO_PARAM_PERFIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_INTERNO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCAO_PARAM_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'FUNCAO_PARAM_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'FUNCAO_PARAM_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'FUNCAO_PARAM_PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO,1,4000),substr(:new.VL_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO',ie_log_w,ds_w,'FUNCAO_PARAM_PERFIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.update_param_perfil
before update or insert ON TASY.FUNCAO_PARAM_PERFIL for each row
declare

pragma autonomous_transaction;

begin

IF INSTR(:NEW.VL_PARAMETRO, '#@') > 0 THEN
  wheb_mensagem_pck.exibir_mensagem_abort(1040016);
END IF;

atualizar_log_alt_parametros(
			:new.nm_usuario,
			:new.cd_estabelecimento,
			null,
			null,
			:new.cd_perfil,
			:old.cd_perfil,
			null,
			null,
			:new.nr_sequencia,
			:new.vl_parametro,
			:old.vl_parametro,
			'P',
			:new.cd_funcao);

commit;

end;
/


ALTER TABLE TASY.FUNCAO_PARAM_PERFIL ADD (
  CONSTRAINT FUNPAPE_PK
 PRIMARY KEY
 (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT FUNPAPE_UK
 UNIQUE (CD_FUNCAO, NR_SEQUENCIA, CD_PERFIL, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_PARAM_PERFIL ADD (
  CONSTRAINT FUNPAPE_FUNPARA_FK 
 FOREIGN KEY (CD_FUNCAO, NR_SEQUENCIA) 
 REFERENCES TASY.FUNCAO_PARAMETRO (CD_FUNCAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT FUNPAPE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT FUNPAPE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.FUNCAO_PARAM_PERFIL TO NIVEL_1;

