ALTER TABLE TASY.TRADUCAO_INCOSIST_OBJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRADUCAO_INCOSIST_OBJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRADUCAO_INCOSIST_OBJ
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_OBJETO           NUMBER(10)            NOT NULL,
  IE_TIPO_INCONSISTENCIA  VARCHAR2(15 BYTE),
  NM_USUARIO_AJUSTE       VARCHAR2(15 BYTE),
  DT_AJUSTE               DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TRAIOBJ_PK ON TASY.TRADUCAO_INCOSIST_OBJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAIOBJ_TRAAOBJ_FK_I ON TASY.TRADUCAO_INCOSIST_OBJ
(NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TRADUCAO_INCOSIST_OBJ ADD (
  CONSTRAINT TRAIOBJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRADUCAO_INCOSIST_OBJ ADD (
  CONSTRAINT TRAIOBJ_TRAAOBJ_FK 
 FOREIGN KEY (NR_SEQ_OBJETO) 
 REFERENCES TASY.TRADUCAO_AJUSTE_OBJETO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TRADUCAO_INCOSIST_OBJ TO NIVEL_1;

