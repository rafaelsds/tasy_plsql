ALTER TABLE TASY.SL_SERVICO_REGRA_CANC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SL_SERVICO_REGRA_CANC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SL_SERVICO_REGRA_CANC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SERVICO       NUMBER(10)               NOT NULL,
  NR_SEQ_SERVICO_CANC  NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SLSRECA_PK ON TASY.SL_SERVICO_REGRA_CANC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLSRECA_PK
  MONITORING USAGE;


CREATE INDEX TASY.SLSRECA_SLSERVI_FK_I ON TASY.SL_SERVICO_REGRA_CANC
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLSRECA_SLSERVI_FK2_I ON TASY.SL_SERVICO_REGRA_CANC
(NR_SEQ_SERVICO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SLSRECA_SLSERVI_FK2_I
  MONITORING USAGE;


ALTER TABLE TASY.SL_SERVICO_REGRA_CANC ADD (
  CONSTRAINT SLSRECA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SL_SERVICO_REGRA_CANC ADD (
  CONSTRAINT SLSRECA_SLSERVI_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.SL_SERVICO (NR_SEQUENCIA),
  CONSTRAINT SLSRECA_SLSERVI_FK2 
 FOREIGN KEY (NR_SEQ_SERVICO_CANC) 
 REFERENCES TASY.SL_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SL_SERVICO_REGRA_CANC TO NIVEL_1;

