ALTER TABLE TASY.PLS_SCA_TIPO_MEDICAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SCA_TIPO_MEDICAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SCA_TIPO_MEDICAMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_TIPO_MEDICAMENTO  VARCHAR2(255 BYTE)       NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_SUBSIDIO          VARCHAR2(30 BYTE),
  PR_SUBSIDIO          NUMBER(7,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTPMD_ESTABEL_FK_I ON TASY.PLS_SCA_TIPO_MEDICAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSTPMD_PK ON TASY.PLS_SCA_TIPO_MEDICAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SCA_TIPO_MEDICAMENTO ADD (
  CONSTRAINT PLSTPMD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_SCA_TIPO_MEDICAMENTO ADD (
  CONSTRAINT PLSTPMD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_SCA_TIPO_MEDICAMENTO TO NIVEL_1;

