ALTER TABLE TASY.QHAPDC_FIELD_MAPPING
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QHAPDC_FIELD_MAPPING CASCADE CONSTRAINTS;

CREATE TABLE TASY.QHAPDC_FIELD_MAPPING
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SEGMENT       NUMBER(10),
  NM_TABLE             VARCHAR2(50 BYTE),
  NM_ATTRIBUTE         VARCHAR2(50 BYTE),
  DS_FIELD             VARCHAR2(150 BYTE),
  IE_MANDATORY         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QHPDFIELD_PK ON TASY.QHAPDC_FIELD_MAPPING
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QHPDFIELD_QHPSEG_FK_I ON TASY.QHAPDC_FIELD_MAPPING
(NR_SEQ_SEGMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QHAPDC_FIELD_MAPPING ADD (
  CONSTRAINT QHPDFIELD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QHAPDC_FIELD_MAPPING ADD (
  CONSTRAINT QHPDFIELD_QHPSEG_FK 
 FOREIGN KEY (NR_SEQ_SEGMENT) 
 REFERENCES TASY.QHAPDC_SEGMENT_STRUCT (NR_SEQUENCIA));

GRANT SELECT ON TASY.QHAPDC_FIELD_MAPPING TO NIVEL_1;

