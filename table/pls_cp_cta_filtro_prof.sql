ALTER TABLE TASY.PLS_CP_CTA_FILTRO_PROF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_FILTRO_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_FILTRO_PROF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_CP_CTA_FILTRO  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_MEDICO             VARCHAR2(10 BYTE)       NOT NULL,
  CD_MEDICO             VARCHAR2(10 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCPCFPF_MEDICO_FK_I ON TASY.PLS_CP_CTA_FILTRO_PROF
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCPCFPF_PK ON TASY.PLS_CP_CTA_FILTRO_PROF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCPCFPF_PLCPCTF_FK_I ON TASY.PLS_CP_CTA_FILTRO_PROF
(NR_SEQ_CP_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_FILTRO_PROF_tp  after update ON TASY.PLS_CP_CTA_FILTRO_PROF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_CP_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_CTA_FILTRO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDICO,1,4000),substr(:new.IE_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_FILTRO_PROF ADD (
  CONSTRAINT PCPCFPF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_FILTRO_PROF ADD (
  CONSTRAINT PCPCFPF_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PCPCFPF_PLCPCTF_FK 
 FOREIGN KEY (NR_SEQ_CP_CTA_FILTRO) 
 REFERENCES TASY.PLS_CP_CTA_FILTRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_FILTRO_PROF TO NIVEL_1;

