ALTER TABLE TASY.SPA_REGRA_EMAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_REGRA_EMAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_REGRA_EMAIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_MENSAGEM     VARCHAR2(3 BYTE)         NOT NULL,
  DS_ASSUNTO           VARCHAR2(255 BYTE)       NOT NULL,
  DS_MENSAGEM_PADRAO   VARCHAR2(2000 BYTE)      NOT NULL,
  IE_FORMA_ENVIO       VARCHAR2(2 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO_ENVIO        VARCHAR2(1 BYTE)         NOT NULL,
  DS_EMAIL_ORIGEM      VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SPREEM_PK ON TASY.SPA_REGRA_EMAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SPA_REGRA_EMAIL_tp  after update ON TASY.SPA_REGRA_EMAIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_ENVIO,1,4000),substr(:new.IE_TIPO_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ENVIO',ie_log_w,ds_w,'SPA_REGRA_EMAIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_MENSAGEM,1,4000),substr(:new.IE_TIPO_MENSAGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MENSAGEM',ie_log_w,ds_w,'SPA_REGRA_EMAIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SPA_REGRA_EMAIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MENSAGEM_PADRAO,1,4000),substr(:new.DS_MENSAGEM_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MENSAGEM_PADRAO',ie_log_w,ds_w,'SPA_REGRA_EMAIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_ENVIO,1,4000),substr(:new.IE_FORMA_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ENVIO',ie_log_w,ds_w,'SPA_REGRA_EMAIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ASSUNTO,1,4000),substr(:new.DS_ASSUNTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ASSUNTO',ie_log_w,ds_w,'SPA_REGRA_EMAIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SPA_REGRA_EMAIL ADD (
  CONSTRAINT SPREEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SPA_REGRA_EMAIL TO NIVEL_1;

