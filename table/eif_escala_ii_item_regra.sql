ALTER TABLE TASY.EIF_ESCALA_II_ITEM_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EIF_ESCALA_II_ITEM_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIF_ESCALA_II_ITEM_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM_SUP      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RESULT_ITEM   NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM_INF      NUMBER(10)               NOT NULL,
  NR_SEQ_ESCALA        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EIESIRE_EIEITRE_FK_I ON TASY.EIF_ESCALA_II_ITEM_REGRA
(NR_SEQ_RESULT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EIESIRE_EIESIII_FK_I ON TASY.EIF_ESCALA_II_ITEM_REGRA
(NR_SEQ_ITEM_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EIESIRE_EIESIII_FK2_I ON TASY.EIF_ESCALA_II_ITEM_REGRA
(NR_SEQ_ITEM_INF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EIESIRE_EIFESII_FK_I ON TASY.EIF_ESCALA_II_ITEM_REGRA
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EIESIRE_PK ON TASY.EIF_ESCALA_II_ITEM_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EIF_ESCALA_II_ITEM_REGRA ADD (
  CONSTRAINT EIESIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EIF_ESCALA_II_ITEM_REGRA ADD (
  CONSTRAINT EIESIRE_EIEITRE_FK 
 FOREIGN KEY (NR_SEQ_RESULT_ITEM) 
 REFERENCES TASY.EIF_ESCALA_II_ITEM_RESULT (NR_SEQUENCIA),
  CONSTRAINT EIESIRE_EIESIII_FK 
 FOREIGN KEY (NR_SEQ_ITEM_SUP) 
 REFERENCES TASY.EIF_ESCALA_II_ITEM (NR_SEQUENCIA),
  CONSTRAINT EIESIRE_EIESIII_FK2 
 FOREIGN KEY (NR_SEQ_ITEM_INF) 
 REFERENCES TASY.EIF_ESCALA_II_ITEM (NR_SEQUENCIA),
  CONSTRAINT EIESIRE_EIFESII_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.EIF_ESCALA_II (NR_SEQUENCIA));

GRANT SELECT ON TASY.EIF_ESCALA_II_ITEM_REGRA TO NIVEL_1;

