ALTER TABLE TASY.W_ANALISE_PEND_OS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ANALISE_PEND_OS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ANALISE_PEND_OS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GERENCIA      NUMBER(10),
  NR_SEQ_GRUPO         NUMBER(10),
  QT_TOTAL_OS          NUMBER(10),
  QT_MIN_PREV          NUMBER(15),
  DT_ULT_PREV          DATE,
  DT_ULT_TEMPO         DATE,
  DT_OS_ANT            DATE,
  IE_TIPO              NUMBER(15),
  NM_USUARIO_EXEC      VARCHAR2(15 BYTE),
  DS_USUARIO_EXEC      VARCHAR2(255 BYTE),
  IE_FORA_PERIODO      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WANPEOS_PK ON TASY.W_ANALISE_PEND_OS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WANPEOS_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_ANALISE_PEND_OS ADD (
  CONSTRAINT WANPEOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_ANALISE_PEND_OS TO NIVEL_1;

