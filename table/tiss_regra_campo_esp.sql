ALTER TABLE TASY.TISS_REGRA_CAMPO_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_CAMPO_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_CAMPO_ESP
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  DS_CAMPO                VARCHAR2(255 BYTE)    NOT NULL,
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_AREA_PROCEDIMENTO    NUMBER(15),
  CD_ESPECIALIDADE        NUMBER(15),
  CD_GRUPO_PROC           NUMBER(15),
  IE_GUIA                 VARCHAR2(15 BYTE)     NOT NULL,
  IE_TIPO_GUIA            VARCHAR2(2 BYTE),
  IE_TIPO_ATENDIMENTO     NUMBER(3),
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  IE_RESPONSAVEL_CREDITO  VARCHAR2(5 BYTE),
  CD_MEDICO_EXECUTOR      VARCHAR2(10 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  CD_GRUPO_MATERIAL       NUMBER(3),
  CD_SUBGRUPO_MATERIAL    NUMBER(3),
  CD_CLASSE_MATERIAL      NUMBER(5),
  CD_MATERIAL             NUMBER(10),
  CD_SETOR_ATENDIMENTO    NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRCE_AREPROC_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCE_CLAMATE_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_CONVENI_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_ESPPROC_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCE_ESTABEL_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCE_GRUMATE_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_GRUPROC_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCE_MATERIA_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_PESFISI_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_PESJURI_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRCE_PK ON TASY.TISS_REGRA_CAMPO_ESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_PROCEDI_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCE_REGHONO_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(IE_RESPONSAVEL_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRCE_REGHONO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRCE_SETATEN_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_SUBMATE_FK_I ON TASY.TISS_REGRA_CAMPO_ESP
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSRCE_UK ON TASY.TISS_REGRA_CAMPO_ESP
(CD_ESTABELECIMENTO, CD_CONVENIO, DS_CAMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_CAMPO_ESP_tp  after update ON TASY.TISS_REGRA_CAMPO_ESP FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_CAMPO,1,500);gravar_log_alteracao(substr(:old.DS_CAMPO,1,4000),substr(:new.DS_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CAMPO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_AREA_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_ESPECIALIDADE,1,500);gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_GRUPO_PROC,1,500);gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_RESPONSAVEL_CREDITO,1,500);gravar_log_alteracao(substr(:old.IE_RESPONSAVEL_CREDITO,1,4000),substr(:new.IE_RESPONSAVEL_CREDITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESPONSAVEL_CREDITO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_GUIA,1,4000),substr(:new.IE_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CGC_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.CD_CGC_PRESTADOR,1,4000),substr(:new.CD_CGC_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_PRESTADOR',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'TISS_REGRA_CAMPO_ESP',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_CAMPO_ESP ADD (
  CONSTRAINT TISSRCE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_CAMPO_ESP ADD (
  CONSTRAINT TISSRCE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT TISSRCE_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT TISSRCE_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT TISSRCE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT TISSRCE_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT TISSRCE_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TISSRCE_REGHONO_FK 
 FOREIGN KEY (IE_RESPONSAVEL_CREDITO) 
 REFERENCES TASY.REGRA_HONORARIO (CD_REGRA),
  CONSTRAINT TISSRCE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRCE_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT TISSRCE_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT TISSRCE_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT TISSRCE_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT TISSRCE_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT TISSRCE_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.TISS_REGRA_CAMPO_ESP TO NIVEL_1;

