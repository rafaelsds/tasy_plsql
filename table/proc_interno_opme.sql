ALTER TABLE TASY.PROC_INTERNO_OPME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INTERNO_OPME CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INTERNO_OPME
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_MATERIAL           NUMBER(10)              NOT NULL,
  QT_MATERIAL           NUMBER(15,3)            NOT NULL,
  IE_AUTORIZACAO        VARCHAR2(1 BYTE)        NOT NULL,
  IE_PADRAO             VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_APRES          NUMBER(15)              NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MEDICO             VARCHAR2(10 BYTE),
  CD_CONVENIO           NUMBER(5),
  CD_CGC                VARCHAR2(14 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  IE_PACOTE             VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE      NUMBER(10),
  IE_SOMENTE_EXCLUSIVO  VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO          NUMBER(10),
  QT_IDADE_MINIMA       NUMBER(3),
  QT_IDADE_MAXIMA       NUMBER(3),
  IE_SEXO               VARCHAR2(1 BYTE),
  IE_TIPO_CONVENIO      NUMBER(2),
  CD_ESTABELECIMENTO    NUMBER(4),
  CD_PLANO              VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.PROINOP_CONPLAN_FK_I ON TASY.PROC_INTERNO_OPME
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PROINOP_CONVENI_FK_I ON TASY.PROC_INTERNO_OPME
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINOP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINOP_ESPMEDI_FK_I ON TASY.PROC_INTERNO_OPME
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINOP_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINOP_ESTABEL_FK_I ON TASY.PROC_INTERNO_OPME
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PROINOP_MATERIA_FK_I ON TASY.PROC_INTERNO_OPME
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINOP_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINOP_PESFISI_FK_I ON TASY.PROC_INTERNO_OPME
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINOP_PESJURI_FK_I ON TASY.PROC_INTERNO_OPME
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROINOP_PK ON TASY.PROC_INTERNO_OPME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINOP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROINOP_PRINOPG_FK_I ON TASY.PROC_INTERNO_OPME
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINOP_PROINTE_FK_I ON TASY.PROC_INTERNO_OPME
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROC_INTERNO_OPME_tp  after update ON TASY.PROC_INTERNO_OPME FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTORIZACAO,1,4000),substr(:new.IE_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTORIZACAO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRAO,1,4000),substr(:new.IE_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PACOTE,1,4000),substr(:new.IE_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PACOTE',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_EXCLUSIVO,1,4000),substr(:new.IE_SOMENTE_EXCLUSIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_EXCLUSIVO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO,1,4000),substr(:new.NR_SEQ_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MAXIMA,1,4000),substr(:new.QT_IDADE_MAXIMA,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MAXIMA',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MINIMA,1,4000),substr(:new.QT_IDADE_MINIMA,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MINIMA',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'PROC_INTERNO_OPME',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROC_INTERNO_OPME ADD (
  CONSTRAINT PROINOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INTERNO_OPME ADD (
  CONSTRAINT PROINOP_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT PROINOP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PROINOP_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PROINOP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PROINOP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PROINOP_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PROINOP_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PROINOP_PRINOPG_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PROC_INTERNO_OPME_GRUPO (NR_SEQUENCIA),
  CONSTRAINT PROINOP_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PROC_INTERNO_OPME TO NIVEL_1;

