ALTER TABLE TASY.PLS_PRECO_BRASINDICE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRECO_BRASINDICE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRECO_BRASINDICE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR     NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  TX_AJUSTE_PFB        NUMBER(15,4)             NOT NULL,
  TX_PMC_NEUT          NUMBER(15,4),
  TX_PMC_POS           NUMBER(15,4),
  TX_PMC_NEG           NUMBER(15,4),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRBR_ESTABEL_FK_I ON TASY.PLS_PRECO_BRASINDICE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRBR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPRBR_PK ON TASY.PLS_PRECO_BRASINDICE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRBR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRBR_PLSPRES_FK_I ON TASY.PLS_PRECO_BRASINDICE
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRBR_PLSPRES_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PRECO_BRASINDICE ADD (
  CONSTRAINT PLSPRBR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRECO_BRASINDICE ADD (
  CONSTRAINT PLSPRBR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPRBR_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRECO_BRASINDICE TO NIVEL_1;

