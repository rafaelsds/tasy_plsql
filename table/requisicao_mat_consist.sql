ALTER TABLE TASY.REQUISICAO_MAT_CONSIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REQUISICAO_MAT_CONSIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.REQUISICAO_MAT_CONSIST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_REQUISICAO          NUMBER(10)             NOT NULL,
  IE_TIPO_CONSISTENCIA   VARCHAR2(1 BYTE)       NOT NULL,
  DS_CONSISTENCIA        VARCHAR2(255 BYTE)     NOT NULL,
  IE_FORMA_CONSISTENCIA  VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  CD_MATERIAL            NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REMACON_MATERIA_FK_I ON TASY.REQUISICAO_MAT_CONSIST
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMACON_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REMACON_PK ON TASY.REQUISICAO_MAT_CONSIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMACON_PK
  MONITORING USAGE;


CREATE INDEX TASY.REMACON_REQMATE_FK_I ON TASY.REQUISICAO_MAT_CONSIST
(NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REQUISICAO_MAT_CONSIST ADD (
  CONSTRAINT REMACON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REQUISICAO_MAT_CONSIST ADD (
  CONSTRAINT REMACON_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REMACON_REQMATE_FK 
 FOREIGN KEY (NR_REQUISICAO) 
 REFERENCES TASY.REQUISICAO_MATERIAL (NR_REQUISICAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REQUISICAO_MAT_CONSIST TO NIVEL_1;

