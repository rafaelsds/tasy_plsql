ALTER TABLE TASY.ATESTADO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATESTADO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATESTADO_PACIENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATESTADO                DATE               NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_MEDICO                  VARCHAR2(10 BYTE),
  DS_ATESTADO_OLD            VARCHAR2(4000 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  DS_ATESTADO                LONG,
  NR_SEQ_TIPO_ATESTADO       NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_CONSULTA            NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_RESTRICAO_VISUALIZACAO  VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  QT_CARACTERES              NUMBER(15),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  QT_DIAS_ATESTADO           NUMBER(15,2),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_ACIDENTE_MILITAR        VARCHAR2(1 BYTE),
  NR_SEQ_SOAP                NUMBER(10),
  IE_ACIDENTE_TRABALHO       VARCHAR2(1 BYTE),
  DT_DIAGNOSTICADO           DATE,
  DT_AUSENCIA_TRABALHO       DATE,
  DS_OUTRO                   VARCHAR2(255 BYTE),
  IE_OUTRO_ACIDENTE          VARCHAR2(1 BYTE),
  IE_OUTROS                  VARCHAR2(1 BYTE),
  IE_PASSOS_REINTEGRACAO     VARCHAR2(1 BYTE),
  IE_REABILITACAO_MEDICA     VARCHAR2(1 BYTE),
  IE_TIPO_ATESTADO           VARCHAR2(1 BYTE),
  IE_TIPO_FINAL              VARCHAR2(1 BYTE),
  IE_SEQUELA_ACIDENTE        VARCHAR2(1 BYTE),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  DT_FIM                     DATE,
  DS_OBS_DIAGNOSTICO         VARCHAR2(60 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_RQE                     VARCHAR2(20 BYTE),
  NR_BSNR                    VARCHAR2(25 BYTE),
  IE_ACAO                    VARCHAR2(1 BYTE),
  NR_SEQ_FORMULARIO          NUMBER(10),
  NR_SEQ_DEPTO_MEDICO        NUMBER(10),
  NR_SEQ_NAIS_INSURANCE      NUMBER(10),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  CD_DEPARTAMENTO            NUMBER(10),
  CD_EVOLUCAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          25M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPACT_ATCONSPEPA_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_ATEPACI_FK_I ON TASY.ATESTADO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_DEPMED_FK_I ON TASY.ATESTADO_PACIENTE
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_DEPMEDI_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_DEPTO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_EHRREGI_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_EVOPACI_FK_I ON TASY.ATESTADO_PACIENTE
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_I1 ON TASY.ATESTADO_PACIENTE
(DT_ATESTADO, NR_ATENDIMENTO, CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACT_I1
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACT_NAISINS_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_OFTCONS_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACT_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACT_PERFIL_FK_I ON TASY.ATESTADO_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACT_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACT_PESFISI_FK_I ON TASY.ATESTADO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_PESFISI_FK2_I ON TASY.ATESTADO_PACIENTE
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPACT_PK ON TASY.ATESTADO_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_PROFISS_FK_I ON TASY.ATESTADO_PACIENTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_SETATEN_FK_I ON TASY.ATESTADO_PACIENTE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACT_TASASDI_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACT_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACT_TASASDI_FK2_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACT_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACT_TIPATES_FK_I ON TASY.ATESTADO_PACIENTE
(NR_SEQ_TIPO_ATESTADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACT_TIPATES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atestado_paciente_pend_atual
after insert or update ON TASY.ATESTADO_PACIENTE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_atestado_w	varchar2(10);
nm_usuario_w        varchar2(15);

qt_tempo_ma_w				wl_regra_item.qt_tempo_normal%type;
nr_seq_regra_wl_ma_w		wl_regra_item.nr_sequencia%type;
is_rule_tasklist_ma_w		wl_item.nr_sequencia%type;

cursor c01 is
	select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'MA'
							and		x.ie_situacao = 'A');


begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (:new.dt_liberacao is null) then

     if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
	   (:new.DT_LIBERACAO_AUX is not null)then


		select 	obter_usuario_pf(:new.cd_medico)
		into	nm_usuario_w
		from	dual;

		Update    pep_item_pendente
		set		nm_usuario = nm_usuario_w
		where	NR_SEQ_ATESTADO = :new.nr_sequencia;

	end if;

end if;

select	max(ie_lib_atestado)
into	ie_lib_atestado_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_lib_atestado_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'AT';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XAT';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
	end if;
end if;

if (pkg_i18n.get_user_locale = 'ja_JP') then
begin
select	count(a.nr_sequencia)
into	is_rule_tasklist_ma_w
from	wl_item a,
		wl_regra_worklist b,
		wl_regra_item c
where	a.nr_sequencia = b.nr_seq_item
and	b.nr_sequencia = c.nr_seq_regra
and	a.cd_categoria = 'MA'
and	a.ie_situacao = 'A'
and	c.ie_situacao = 'A';

if (:old.dt_liberacao is null and :new.dt_liberacao is not null and is_rule_tasklist_ma_w > 0 ) then

	open c01;
	loop
	fetch c01 into
		qt_tempo_ma_w,
		nr_seq_regra_wl_ma_w;
	exit when c01%notfound;
		begin
			if	(nvl(qt_tempo_ma_w,0) > 0) then

				wl_gerar_finalizar_tarefa('MA','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_ma_w/24),'N',
										null,null,null,null,null,null,null,null,null,nr_seq_regra_wl_ma_w,null,null,null,null,null,null,null,sysdate,null);
			end if;
		end;
	end loop;
	close c01;
  end if;
end;
end if;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

update evolucao_paciente
set dt_inativacao = sysdate,
ie_situacao ='I',
dt_atualizacao = sysdate ,
nm_usuario = wheb_usuario_pck.get_nm_usuario,
nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
ds_justificativa = :new.ds_justificativa
where cd_evolucao = :new.cd_evolucao;
delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'MED_CERT'  and ie_stage = 1  and ie_soap_type = 'P' and nr_seq_med_item = :new.nr_sequencia;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.atestado_paciente_insert
before insert ON TASY.ATESTADO_PACIENTE for each row
declare

begin

if	(:new.nr_atendimento	is not null) and
	(:new.cd_pessoa_fisica is null) then

	:new.cd_pessoa_fisica	:= obter_pessoa_atendimento(:new.nr_atendimento,'C');

end if;

if	(:new.DT_AUSENCIA_TRABALHO is null) and
	(:new.QT_DIAS_ATESTADO is not null) then
	:new.DT_AUSENCIA_TRABALHO := nvl(:new.DT_ATESTADO, sysdate) + :new.QT_DIAS_ATESTADO;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATESTADO_PACIENTE_ATUAL
before insert or update ON TASY.ATESTADO_PACIENTE for each row
declare

/* envio de email de acordo com OS 1772780 ser� via codigo fonte pois ter� um relat�rio
nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w		number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento = cd_estabelecimento_w))
	and	ie_evento_disp = 'LATE'
	and	qt_idade_w between nvl(qt_idade_min,0) and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';*/

begin
--cd_estabelecimento_w := obter_estabelecimento_ativo;

if	(nvl(:old.DT_ATESTADO,sysdate+10) <> :new.DT_ATESTADO) and
	(:new.DT_ATESTADO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATESTADO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

/*envio de email de acordo com OS 1772780 ser� via codigo fonte pois ter� um relat�rio
if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	cd_pessoa_fisica_w := obter_pessoa_atendimento(:new.nr_atendimento,'C');
	qt_idade_w := nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);

	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
			--gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario,null,:new.dt_liberacao);
		end;
	end loop;
	close C01;

	exception
		when others then
		null;
	end;
end if;*/
end;
/


CREATE OR REPLACE TRIGGER TASY.ATESTADO_PACIENTE_UPDATE
before update ON TASY.ATESTADO_PACIENTE for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';
begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into 	is_rule_exists_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV1'
    and   	obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV1'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
    end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ATESTADO_PACIENTE_SBIS_DEL
before delete ON TASY.ATESTADO_PACIENTE 
for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual; 

insert into log_alteracao_prontuario (nr_sequencia, 
									 dt_atualizacao, 
									 ds_evento, 
									 ds_maquina, 
									 cd_pessoa_fisica, 
									 ds_item, 
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values 
									 (nr_log_seq_w, 
									 sysdate, 
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina, 
									 nvl(obter_pessoa_atendimento(:old.nr_atendimento,'C'), :old.cd_pessoa_fisica), 
									 substr(obter_desc_expressao(283879), 1, 30), 
									  :old.nr_atendimento,
									 :old.dt_liberacao, 
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));
end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_ATESTADO_PACIENTE
before insert or update ON TASY.ATESTADO_PACIENTE for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ATESTADO_PACIENTE_SBIS_IN
before insert or update ON TASY.ATESTADO_PACIENTE for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'pt_BR') then
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  if (INSERTING) then
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665),
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica),
                       obter_desc_expressao(283879),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica),
                       obter_desc_expressao(283879),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.GER_ATESTADO_PACIENTE_ATUAL
before insert or update ON TASY.ATESTADO_PACIENTE 
for each row
declare

begin

	if	(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_DE') then
		if(:new.CD_MEDICO is null) then
			:new.CD_MEDICO	:= Obter_Pf_Usuario(:new.NM_USUARIO, 'C');
		end if;
	end if;

end;
/


ALTER TABLE TASY.ATESTADO_PACIENTE ADD (
  CONSTRAINT ATEPACT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          640K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATESTADO_PACIENTE ADD (
  CONSTRAINT ATEPACT_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPACT_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEPACT_DEPMEDI_FK 
 FOREIGN KEY (NR_SEQ_DEPTO_MEDICO) 
 REFERENCES TASY.DEPTO_MEDICO (NR_SEQUENCIA),
  CONSTRAINT ATEPACT_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT ATEPACT_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT ATEPACT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEPACT_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT ATEPACT_PESFISI_FK2 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPACT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEPACT_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPACT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPACT_TIPATES_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATESTADO) 
 REFERENCES TASY.TIPO_ATESTADO (NR_SEQUENCIA),
  CONSTRAINT ATEPACT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEPACT_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT ATEPACT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEPACT_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATESTADO_PACIENTE TO NIVEL_1;

