ALTER TABLE TASY.ESCALA_GOLD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_GOLD CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_GOLD
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_TOTAL_CAT           NUMBER(10),
  IE_GOLD                VARCHAR2(3 BYTE)       NOT NULL,
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  IE_MMRC                VARCHAR2(1 BYTE),
  QT_CAT_01              VARCHAR2(10 BYTE),
  IE_IRRITADO_ADM        VARCHAR2(1 BYTE),
  QT_CAT_02              VARCHAR2(10 BYTE),
  QT_CAT_03              VARCHAR2(10 BYTE),
  QT_CAT_04              VARCHAR2(10 BYTE),
  QT_CAT_05              VARCHAR2(10 BYTE),
  QT_CAT_06              VARCHAR2(10 BYTE),
  QT_CAT_07              VARCHAR2(10 BYTE),
  QT_CAT_08              VARCHAR2(10 BYTE),
  QT_IRRITADO            NUMBER(10),
  IE_GRUPO               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAGOL_ATEPACI_FK_I ON TASY.ESCALA_GOLD
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAGOL_PESFISI_FK_I ON TASY.ESCALA_GOLD
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAGOL_PK ON TASY.ESCALA_GOLD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Escala_gold_atual
before insert or update ON TASY.ESCALA_GOLD for each row
declare

qt_reg_w		number(1);
qt_cat_w		number(5);
ie_grupo_w		varchar2(1);
ie_linha_w		number(1);
ie_coluna_w		number(1);

begin


if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;



if	(:new.qt_irritado >= 2) then

	ie_linha_w := 1;

elsif	(:new.qt_irritado >= 1) then

	ie_linha_w := 1;

	if (:new.ie_irritado_adm = 'S') then

		ie_linha_w := 2;

	end if;

else

	ie_linha_w := 2;

end if;

qt_cat_w := :new.QT_CAT_01 + :new.QT_CAT_02 + :new.QT_CAT_03 + :new.QT_CAT_04 + :new.QT_CAT_05 + :new.QT_CAT_06 + :new.QT_CAT_07 + :new.QT_CAT_08;

:new.QT_TOTAL_CAT:= qt_cat_w;


if ( :new.ie_mmrc >= 2) or
   ( qt_cat_w >= 10) then

   ie_coluna_w	:= 2;

else

	ie_coluna_w	:= 1;

end if;



if (ie_linha_w = 1)  then

	ie_grupo_w := 'C';

	if (ie_coluna_w = 2) then

		ie_grupo_w := 'D';

	end if;


elsif (ie_linha_w = 2) then

	ie_grupo_w := 'A';

	if (ie_coluna_w = 2) then

		ie_grupo_w := 'B';

	end if;



end if;




<<Final>>
qt_reg_w	:= 0;

:new.ie_grupo := ie_grupo_w;

end Escala_gold_atual;
/


ALTER TABLE TASY.ESCALA_GOLD ADD (
  CONSTRAINT ESCAGOL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_GOLD ADD (
  CONSTRAINT ESCAGOL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAGOL_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_GOLD TO NIVEL_1;

