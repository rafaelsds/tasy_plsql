ALTER TABLE TASY.PTU_CLAS_REDE_PREST_DTH
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_CLAS_REDE_PREST_DTH CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_CLAS_REDE_PREST_DTH
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CLAS_REDE_PREST  NUMBER(10)            NOT NULL,
  CD_DTH                  VARCHAR2(8 BYTE)      NOT NULL,
  VL_DTH                  NUMBER(15,2)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUCRPD_PK ON TASY.PTU_CLAS_REDE_PREST_DTH
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUCRPD_PTUCRPR_FK_I ON TASY.PTU_CLAS_REDE_PREST_DTH
(NR_SEQ_CLAS_REDE_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_CLAS_REDE_PREST_DTH ADD (
  CONSTRAINT PTUCRPD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PTU_CLAS_REDE_PREST_DTH ADD (
  CONSTRAINT PTUCRPD_PTUCRPR_FK 
 FOREIGN KEY (NR_SEQ_CLAS_REDE_PREST) 
 REFERENCES TASY.PTU_CLAS_REDE_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_CLAS_REDE_PREST_DTH TO NIVEL_1;

