ALTER TABLE TASY.SAC_BOLETIM_ALT_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAC_BOLETIM_ALT_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAC_BOLETIM_ALT_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RESP_BO       NUMBER(10),
  IE_TIPO_ALTERACAO    VARCHAR2(2 BYTE),
  DS_VALOR_ANTERIOR    VARCHAR2(255 BYTE),
  DS_VALOR_NOVO        VARCHAR2(255 BYTE),
  NR_SEQ_BOLETIM_OCOR  NUMBER(10),
  DS_MOTIVO            VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SACBOLALT_PK ON TASY.SAC_BOLETIM_ALT_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SACBOLALT_PK
  MONITORING USAGE;


CREATE INDEX TASY.SACBOLALT_SACBOOC_FK_I ON TASY.SAC_BOLETIM_ALT_HIST
(NR_SEQ_BOLETIM_OCOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SACBOLALT_SACBOOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SACBOLALT_SACREBO_FK_I ON TASY.SAC_BOLETIM_ALT_HIST
(NR_SEQ_RESP_BO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAC_BOLETIM_ALT_HIST ADD (
  CONSTRAINT SACBOLALT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAC_BOLETIM_ALT_HIST ADD (
  CONSTRAINT SACBOLALT_SACBOOC_FK 
 FOREIGN KEY (NR_SEQ_BOLETIM_OCOR) 
 REFERENCES TASY.SAC_BOLETIM_OCORRENCIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SACBOLALT_SACREBO_FK 
 FOREIGN KEY (NR_SEQ_RESP_BO) 
 REFERENCES TASY.SAC_RESP_BOL_OCOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SAC_BOLETIM_ALT_HIST TO NIVEL_1;

