ALTER TABLE TASY.PLS_COOPERADO_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COOPERADO_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COOPERADO_DOCUMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_DOCUMENTO         DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE),
  NR_SEQ_COOP_TIPO     NUMBER(10)               NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCDOC_PK ON TASY.PLS_COOPERADO_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCDOC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCDOC_PLSCOTD_FK_I ON TASY.PLS_COOPERADO_DOCUMENTO
(NR_SEQ_COOP_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCDOC_PLSCOTD_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_COOPERADO_DOCUMENTO_tp  after update ON TASY.PLS_COOPERADO_DOCUMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_ARQUIVO,1,500);gravar_log_alteracao(substr(:old.DS_ARQUIVO,1,4000),substr(:new.DS_ARQUIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARQUIVO',ie_log_w,ds_w,'PLS_COOPERADO_DOCUMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_COOPERADO_DOCUMENTO ADD (
  CONSTRAINT PLSCDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COOPERADO_DOCUMENTO ADD (
  CONSTRAINT PLSCDOC_PLSCOTD_FK 
 FOREIGN KEY (NR_SEQ_COOP_TIPO) 
 REFERENCES TASY.PLS_COOPERADO_TIPO_DOC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COOPERADO_DOCUMENTO TO NIVEL_1;

