ALTER TABLE TASY.CORPO_HUMANO_REF_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CORPO_HUMANO_REF_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CORPO_HUMANO_REF_LIB
(
  NR_SEQ_REF_LIB         NUMBER(10)             NOT NULL,
  NR_SEQ_CORPO_HUMANO    NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_IDADE_ANO_MIN       NUMBER(10),
  QT_IDADE_ANO_MAX       NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  CD_PERFIL              NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COHUREL_COHUREL_FK_I ON TASY.CORPO_HUMANO_REF_LIB
(NR_SEQ_CORPO_HUMANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUREL_ESTABEL_FK_I ON TASY.CORPO_HUMANO_REF_LIB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUREL_PERFIL_FK_I ON TASY.CORPO_HUMANO_REF_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COHUREL_PK ON TASY.CORPO_HUMANO_REF_LIB
(NR_SEQ_REF_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUREL_SETATEN_FK_I ON TASY.CORPO_HUMANO_REF_LIB
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CORPO_HUMANO_REF_LIB ADD (
  CONSTRAINT COHUREL_PK
 PRIMARY KEY
 (NR_SEQ_REF_LIB));

ALTER TABLE TASY.CORPO_HUMANO_REF_LIB ADD (
  CONSTRAINT COHUREL_COHUREL_FK 
 FOREIGN KEY (NR_SEQ_CORPO_HUMANO) 
 REFERENCES TASY.CORPO_HUMANO_REFERENCIA (NR_SEQ_CORPO_HUMANO),
  CONSTRAINT COHUREL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COHUREL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT COHUREL_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CORPO_HUMANO_REF_LIB TO NIVEL_1;

