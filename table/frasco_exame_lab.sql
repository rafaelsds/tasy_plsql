ALTER TABLE TASY.FRASCO_EXAME_LAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FRASCO_EXAME_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.FRASCO_EXAME_LAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_FRASCO            VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FRASCO            VARCHAR2(10 BYTE),
  QT_VOLUME            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.FRAEXLA_PK ON TASY.FRASCO_EXAME_LAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FRASCO_EXAME_LAB_tp  after update ON TASY.FRASCO_EXAME_LAB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_VOLUME,1,4000),substr(:new.QT_VOLUME,1,4000),:new.nm_usuario,nr_seq_w,'QT_VOLUME',ie_log_w,ds_w,'FRASCO_EXAME_LAB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.FRASCO_EXAME_LAB_INS_UP

before insert or update ON TASY.FRASCO_EXAME_LAB for each row
begin

if ((:new.QT_VOLUME < 1) and (:new.QT_VOLUME is not null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(791040);
end if;

end;
/


ALTER TABLE TASY.FRASCO_EXAME_LAB ADD (
  CONSTRAINT FRAEXLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.FRASCO_EXAME_LAB TO NIVEL_1;

