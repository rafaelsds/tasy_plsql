ALTER TABLE TASY.COMPL_PESSOA_FISICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPL_PESSOA_FISICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPL_PESSOA_FISICA
(
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  NR_SEQUENCIA            NUMBER(3),
  IE_TIPO_COMPLEMENTO     NUMBER(2)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NM_CONTATO              VARCHAR2(60 BYTE),
  DS_ENDERECO             VARCHAR2(100 BYTE),
  CD_CEP                  VARCHAR2(15 BYTE),
  NR_ENDERECO             NUMBER(5),
  DS_COMPLEMENTO          VARCHAR2(40 BYTE),
  DS_BAIRRO               VARCHAR2(80 BYTE),
  DS_MUNICIPIO            VARCHAR2(40 BYTE),
  SG_ESTADO               VARCHAR2(15 BYTE),
  NR_TELEFONE             VARCHAR2(15 BYTE),
  NR_RAMAL                NUMBER(5),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  DS_EMAIL                VARCHAR2(255 BYTE),
  CD_EMPRESA_REFER        NUMBER(10),
  CD_PROFISSAO            NUMBER(10),
  NR_IDENTIDADE           VARCHAR2(15 BYTE),
  NR_CPF                  VARCHAR2(11 BYTE),
  CD_MUNICIPIO_IBGE       VARCHAR2(6 BYTE),
  DS_SETOR_TRABALHO       VARCHAR2(30 BYTE),
  DS_HORARIO_TRABALHO     VARCHAR2(30 BYTE),
  NR_MATRICULA_TRABALHO   VARCHAR2(20 BYTE),
  NR_SEQ_PARENTESCO       NUMBER(10),
  DS_FONE_ADIC            VARCHAR2(80 BYTE),
  DS_FAX                  VARCHAR2(80 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA_REF    VARCHAR2(10 BYTE),
  NR_SEQ_PAIS             NUMBER(10),
  DS_FONETICA             VARCHAR2(60 BYTE),
  CD_TIPO_LOGRADOURO      VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE         VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE         VARCHAR2(3 BYTE),
  NR_DDI_FAX              VARCHAR2(3 BYTE),
  NR_DDD_FAX              VARCHAR2(3 BYTE),
  DS_WEBSITE              VARCHAR2(255 BYTE),
  NM_CONTATO_PESQUISA     VARCHAR2(60 BYTE),
  IE_NF_CORREIO           VARCHAR2(1 BYTE),
  QT_DEPENDENTE           NUMBER(2),
  CD_ZONA_PROCEDENCIA     VARCHAR2(3 BYTE),
  IE_OBRIGA_EMAIL         VARCHAR2(1 BYTE),
  NR_SEQ_IDENT_CNES       NUMBER(10),
  IE_MALA_DIRETA          VARCHAR2(1 BYTE),
  IE_TIPO_SANGUE          VARCHAR2(2 BYTE),
  IE_FATOR_RH             VARCHAR2(1 BYTE),
  NR_SEQ_LOCAL_ATEND_MED  NUMBER(10),
  CD_PESSOA_END_REF       VARCHAR2(10 BYTE),
  NR_SEQ_END_REF          NUMBER(3),
  IE_EMPRESA_PAGADORA     VARCHAR2(1 BYTE),
  NR_SEQ_REGIAO           NUMBER(10),
  IE_CORRESPONDENCIA      VARCHAR2(1 BYTE),
  NR_DDI_FONE_ADIC        VARCHAR2(3 BYTE),
  NR_DDD_FONE_ADIC        VARCHAR2(3 BYTE),
  NR_SEQ_TIPO_COMPL_ADIC  NUMBER(10),
  NR_TELEFONE_CELULAR     VARCHAR2(40 BYTE),
  NR_DDD_CELULAR          VARCHAR2(3 BYTE),
  NR_DDI_CELULAR          VARCHAR2(3 BYTE),
  IE_NAO_POSSUI_EMAIL     VARCHAR2(15 BYTE),
  IE_REGIME_CASAMENTO     VARCHAR2(3 BYTE),
  IE_RESP_LEGAL           VARCHAR2(1 BYTE),
  NR_SEQ_ASSENTAMENTO_MX  NUMBER(10),
  NR_SEQ_LOCALIZACAO_MX   NUMBER(10),
  DS_COMPL_END            VARCHAR2(20 BYTE),
  NR_SEQ_TIPO_CUSTODIA    NUMBER(10),
  IE_CUSTODIANTE          VARCHAR2(1 BYTE),
  IE_CONTATO_PRINCIPAL    VARCHAR2(1 BYTE),
  NR_SEQ_PESSOA_ENDERECO  NUMBER(10),
  CD_CGC                  VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          112M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.CD_PESSOA_FISICA IS 'Codigo da Pessoa fisica';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.NR_SEQUENCIA IS 'Numero de Sequencia do endereco';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.IE_TIPO_COMPLEMENTO IS 'Indicador do Tipo de Complemento da Pessoa';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.NM_CONTATO IS 'Nome da Pessoa de Contato';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.DS_ENDERECO IS 'Endereco da pessoa Fisica';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.NR_ENDERECO IS 'Numero do Endereco';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.DS_COMPLEMENTO IS 'Complemento do Endere�o';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.DS_BAIRRO IS 'Descricao do Bairro';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.DS_MUNICIPIO IS 'Descricao do Municipio';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.SG_ESTADO IS 'Sigla do Estado';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.NR_TELEFONE IS 'Numero do Telefone';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.NR_RAMAL IS 'Numero do Ramal';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.DS_OBSERVACAO IS 'Observa�oes';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.DS_EMAIL IS 'Endere�o na Internet';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.CD_EMPRESA_REFER IS 'Codigo da Empresa de Referencia';

COMMENT ON COLUMN TASY.COMPL_PESSOA_FISICA.CD_PROFISSAO IS 'Codigo da Profissao';


CREATE INDEX TASY.COMPEFI_CATIASS_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_ASSENTAMENTO_MX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_CATLOL_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_LOCALIZACAO_MX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_CNESIDE_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_COMPEFI_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_PESSOA_END_REF, NR_SEQ_END_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          536K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_EMPREFE_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_EMPRESA_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_GRAUPA_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_GRAUPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_I ON TASY.COMPL_PESSOA_FISICA
(IE_TIPO_COMPLEMENTO, NR_TELEFONE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_I2 ON TASY.COMPL_PESSOA_FISICA
(DS_FONETICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_I2
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_I3 ON TASY.COMPL_PESSOA_FISICA
(NM_CONTATO_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          22M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_LOCATME_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_LOCAL_ATEND_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_LOCATME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_PAIS_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_PAIS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_PESFISI_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          26M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_PESFISI_FK2_I ON TASY.COMPL_PESSOA_FISICA
(CD_PESSOA_FISICA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_PESJURI_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_PESSEND_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMPEFI_PK ON TASY.COMPL_PESSOA_FISICA
(CD_PESSOA_FISICA, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          29M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPEFI_PROFISS_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_PROFISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_PROFISS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_SUSMUNI_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_SUSMURE_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_SUSMURE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_SUSTILO_FK_I ON TASY.COMPL_PESSOA_FISICA
(CD_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_SUSTILO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_TIPCOAD_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_TIPO_COMPL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPEFI_TIPCOAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMPEFI_TIPOCUST_FK_I ON TASY.COMPL_PESSOA_FISICA
(NR_SEQ_TIPO_CUSTODIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMPEFI_UK ON TASY.COMPL_PESSOA_FISICA
(CD_PESSOA_FISICA, IE_TIPO_COMPLEMENTO, NR_SEQ_TIPO_COMPL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.COMPL_PESSOA_FISICA_INSERT
AFTER INSERT OR UPDATE ON TASY.COMPL_PESSOA_FISICA FOR EACH ROW
DECLARE

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:old.nm_usuario is not null) then
		insert into compl_pessoa_fisica_hist
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				ie_tipo_complemento,
				ds_endereco,
				cd_cep,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				sg_estado,
				cd_municipio_ibge,
				nr_seq_pais,
				cd_tipo_logradouro,
				nm_contato,
				nr_telefone,
				nr_ramal,
				ds_observacao,
				ds_email,
				cd_empresa_refer,
				cd_profissao,
				nr_identidade,
				nr_cpf,
				ds_setor_trabalho,
				ds_horario_trabalho,

				ds_website,
				nr_matricula_trabalho,
				ds_fone_adic,
				ds_fax,
				nr_seq_parentesco,
				cd_pessoa_fisica_ref,
				nr_ddd_telefone,
				nr_ddi_telefone,
				nr_ddi_fax,
				nr_ddd_fax,
				nm_contato_pesquisa,
				qt_dependente,
				ds_fonetica,
				ie_nf_correio,
				cd_zona_procedencia,
				ie_obriga_email,
				nr_seq_ident_cnes,
				ie_mala_direta,
				ie_fator_rh,
				ie_tipo_sangue,
				nr_seq_local_atend_med,
				cd_pessoa_end_ref,
				nr_seq_end_ref,
				ie_empresa_pagadora,
				nr_seq_regiao,
				ie_correspondencia,
				nr_ddi_fone_adic,
				nr_ddd_fone_adic)
		values	(	compl_pessoa_fisica_hist_seq.nextval,
				sysdate,
				nvl(:new.nm_usuario,:old.nm_usuario),
				sysdate,
				:old.nm_usuario_nrec,
				:old.cd_pessoa_fisica,
				:old.ie_tipo_complemento,
				:old.ds_endereco,
				:old.cd_cep,
				:old.nr_endereco,
				:old.ds_complemento,
				:old.ds_bairro,
				:old.ds_municipio,
				:old.sg_estado,
				:old.cd_municipio_ibge,
				:old.nr_seq_pais,
				:old.cd_tipo_logradouro,
				:old.nm_contato,
				:old.nr_telefone,
				:old.nr_ramal,
				:old.ds_observacao,
				:old.ds_email,
				:old.cd_empresa_refer,
				:old.cd_profissao,
				:old.nr_identidade,
				:old.nr_cpf,
				:old.ds_setor_trabalho,
				:old.ds_horario_trabalho,
				:old.ds_website,
				:old.nr_matricula_trabalho,
				:old.ds_fone_adic,
				:old.ds_fax,
				:old.nr_seq_parentesco,
				:old.cd_pessoa_fisica_ref,
				:old.nr_ddd_telefone,
				:old.nr_ddi_telefone,
				:old.nr_ddi_fax,
				:old.nr_ddd_fax,
				:old.nm_contato_pesquisa,
				:old.qt_dependente,
				:old.ds_fonetica,
				:old.ie_nf_correio,
				:old.cd_zona_procedencia,
				:old.ie_obriga_email,
				:old.nr_seq_ident_cnes,
				:old.ie_mala_direta,
				:old.ie_fator_rh,
				:old.ie_tipo_sangue,
				:old.nr_seq_local_atend_med,
				:old.cd_pessoa_end_ref,
				:old.nr_seq_end_ref,
				:old.ie_empresa_pagadora,
				:old.nr_seq_regiao,
				:old.ie_correspondencia,
				:old.nr_ddi_fone_adic,
				:old.nr_ddd_fone_adic);
	end if;
end if;

/* Projeto MXM (7077)  - Exportar cadastro pessoa fisica */
if (:new.sg_estado is not null and :new.ie_tipo_complemento = 1) then
	gravar_agend_integracao(556,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';CD_PESSOA_JURIDICA='||null||';'); --Fornecedor
	gravar_agend_integracao(562,'CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||';'); --Cliente
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_COMPL_PESSOA_FISICA_UPDATE
AFTER INSERT OR UPDATE ON TASY.COMPL_PESSOA_FISICA FOR EACH ROW
DECLARE

ie_insere_registro_w		varchar2(10) := 'N';
qt_beneficiarios_repasse_w	number(10);
nr_seq_segurado_w		number(10);

begin

ie_insere_registro_w		:= 'N';
qt_beneficiarios_repasse_w	:= 0;

/*Essa trigger ir� verificar apenas benefici�rios de repasse com responsabilidade transferida*/
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:old.nm_usuario is not null) and
		(:new.ie_tipo_complemento in (1,2)) then

		select	count(*)
		into	qt_beneficiarios_repasse_w
		from	pls_segurado
		where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
		and	ie_tipo_segurado	= 'R';

		if	(qt_beneficiarios_repasse_w	> 0) then

			select	max(nr_sequencia)
			into	nr_seq_segurado_w
			from	pls_segurado
			where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
			and	ie_tipo_segurado	= 'R';

			if	(nvl(:old.ds_endereco,'0') <> nvl(:new.ds_endereco,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.nr_endereco,'0') <> nvl(:new.nr_endereco,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.ds_bairro,'0') <> nvl(:new.ds_bairro,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.cd_municipio_ibge,'0') <> nvl(:new.cd_municipio_ibge,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.sg_estado,'0') <> nvl(:new.sg_estado,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(nvl(:old.cd_cep,'0') <> nvl(:new.cd_cep,'0')) then
				ie_insere_registro_w := 'S';
			end if;

			if	(ie_insere_registro_w	= 'S') then
				begin
				insert into pls_segurado_alteracao
					(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						nr_seq_segurado)
				values	(	pls_segurado_alteracao_seq.nextval,trunc(sysdate,'dd'),:new.nm_usuario,trunc(sysdate,'dd'),:new.nm_usuario,
						nr_seq_segurado_w);
				exception
				when others then
					ie_insere_registro_w	:= 'N';
				end;

			end if;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.COMPL_PESSOA_FISICA_tp  after update ON TASY.COMPL_PESSOA_FISICA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_PESSOA_FISICA='||to_char(:old.CD_PESSOA_FISICA)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA); ds_w:=substr(:new.NM_CONTATO,1,500);gravar_log_alteracao(substr(:old.NM_CONTATO,1,4000),substr(:new.NM_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NM_CONTATO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SANGUE,1,4000),substr(:new.IE_TIPO_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SANGUE',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL,1,4000),substr(:new.NR_RAMAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_RAMAL',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA_REFER,1,4000),substr(:new.CD_EMPRESA_REFER,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA_REFER',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROFISSAO,1,4000),substr(:new.CD_PROFISSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROFISSAO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SETOR_TRABALHO,1,4000),substr(:new.DS_SETOR_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SETOR_TRABALHO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MATRICULA_TRABALHO,1,4000),substr(:new.NR_MATRICULA_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_MATRICULA_TRABALHO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HORARIO_TRABALHO,1,4000),substr(:new.DS_HORARIO_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_HORARIO_TRABALHO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PARENTESCO,1,4000),substr(:new.NR_SEQ_PARENTESCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PARENTESCO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONE_ADIC,1,4000),substr(:new.DS_FONE_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONE_ADIC',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FAX,1,4000),substr(:new.DS_FAX,1,4000),:new.nm_usuario,nr_seq_w,'DS_FAX',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA_REF,1,4000),substr(:new.CD_PESSOA_FISICA_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA_REF',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOGRADOURO,1,4000),substr(:new.CD_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOGRADOURO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPL_END,1,4000),substr(:new.DS_COMPL_END,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPL_END',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FATOR_RH,1,4000),substr(:new.IE_FATOR_RH,1,4000),:new.nm_usuario,nr_seq_w,'IE_FATOR_RH',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONETICA,1,4000),substr(:new.DS_FONETICA,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONETICA',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'COMPL_PESSOA_FISICA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.fis_efd_icmsipi_pf_compl_atual
after update of cd_municipio_ibge, ds_endereco, nr_endereco, ds_complemento, ds_bairro ON TASY.COMPL_PESSOA_FISICA for each row
WHEN (
new.ie_tipo_complemento = 1
      )
declare
ie_efd_icmsipi_w	varchar2(1);

begin
obter_param_usuario(5500, 61, obter_perfil_ativo, obter_usuario_ativo , obter_estabelecimento_ativo, ie_efd_icmsipi_w);

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if	(nvl(ie_efd_icmsipi_w,'N') = 'S') then
		-- REGISTRO 0150.CD_MUN
		if  (nvl(:new.cd_municipio_ibge,'XPTO') <> nvl(:old.cd_municipio_ibge,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'08', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				elimina_caracteres_especiais(substr(:old.cd_municipio_ibge || substr(calcula_digito('MODULO10', :old.cd_municipio_ibge),1,1),1,7)),
				:new.cd_pessoa_fisica);
		end if;

		-- REGISTRO 0150.END
		if  (nvl(:new.ds_endereco,'XPTO') <> nvl(:old.ds_endereco,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'10', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_endereco,1,100),
				:new.cd_pessoa_fisica);
		end if;

		-- REGISTRO 0150.NUM
		if  (nvl(:new.nr_endereco,0) <> nvl(:old.nr_endereco,0)) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'11', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				to_char(:old.nr_endereco),
				:new.cd_pessoa_fisica);
		end if;

		-- REGISTRO 0150.COMPL
		if  (nvl(:new.ds_complemento,'XPTO') <> nvl(:old.ds_complemento,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'12', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_complemento,1,100),
				:new.cd_pessoa_fisica);
		end if;

		-- REGISTRO 0150.BAIRRO
		if  (nvl(:new.ds_bairro,'XPTO') <> nvl(:old.ds_bairro,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_pessoa_fisica)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'13', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_bairro,1,100),
				:new.cd_pessoa_fisica);
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ISCV_COMPL_PESSOA_FISICA_UPDAT
after update or insert ON TASY.COMPL_PESSOA_FISICA for each row
declare
ds_sep_bv_w			varchar2(100)	:= obter_separador_bv;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;

	/* Verifica se existe alguma integra��o cadastrada para o dom�nio 17, para certificar que h� o que ser procurado, para a� ent�o executar todas as verifica��es. */
	function permiteIntegrarISCV
	return boolean is
	qt_resultado_w	number;

	begin
	select	count(*) qt_resultado
	into	qt_resultado_w
	from 	regra_proc_interno_integra
	where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

	return qt_resultado_w > 0;
	end;

	function permiteIntegrar return boolean is	--Verificar se h� algum procedimento interno que deva integrar
	ds_retorno_w		varchar2(1);
	begin
	if	permiteIntegrarISCV then
		nr_atendimento_w := obter_atendimento_paciente(:new.cd_pessoa_fisica, obter_estabelecimento_ativo);

		select	nvl(max(Obter_Se_Integr_Proc_Interno(pp.nr_seq_proc_interno, 17,null,pp.ie_lado,nvl(pm.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento))),'N') ie_permite_proc_integ
		into	ds_retorno_w
		from	prescr_procedimento pp,
				prescr_medica pm
		where	pm.nr_atendimento = nr_atendimento_w
		and		pp.nr_seq_proc_interno is not null
		and		pp.nr_sequencia = pm.nr_prescricao;

		if	(ds_retorno_w = 'N') then

			select	nvl(max(Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,wheb_usuario_pck.get_cd_estabelecimento)),'N') ie_permite_proc_integ
			into	ds_retorno_w
			from	agenda_paciente
			where	nr_seq_proc_interno is not null
			and		nr_atendimento = nr_atendimento_w;

			if	(ds_retorno_w = 'N') then
				select	nvl(max(Obter_Se_Integr_Proc_Interno(app.nr_seq_proc_interno, 17,null,app.ie_lado,wheb_usuario_pck.get_cd_estabelecimento)),'N') ie_permite_proc_integ
				into	ds_retorno_w
				from	agenda_paciente ap,
						agenda_paciente_proc app
				where	ap.nr_seq_proc_interno is not null
				and		ap.nr_atendimento = nr_atendimento_w
				and		ap.nr_sequencia = app.nr_sequencia;

				if	(ds_retorno_w = 'N') then
					select	decode(count(*),0,'N','S')
					into	ds_retorno_w
					from	prescr_medica a,
							prescr_procedimento b,
							atendimento_paciente c
					where	a.nr_prescricao = b.nr_prescricao
					and		a.nr_atendimento = c.nr_atendimento
					and		c.cd_pessoa_fisica = :new.cd_pessoa_fisica
					and exists(select	1
							from	regra_proc_interno_integra x
							where	x.nr_seq_proc_interno = b.nr_seq_proc_interno
							and	x.ie_tipo_integracao = 17
							and	nvl(x.ie_tipo_proc_integr,0) = 0
							and	NVL(x.cd_estabelecimento, nvl(wheb_usuario_pck.get_cd_estabelecimento,0)) = NVL(wheb_usuario_pck.get_cd_estabelecimento,0));

				end if;
			end if;
		end if;

		return ds_retorno_w = 'S';
	else
		return false;
	end if;

	end;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	((:old.ds_endereco <> :new.ds_endereco)
	or	(:old.nr_endereco <> :new.nr_endereco)
	or	(:old.ds_complemento <> :new.ds_complemento)
	or	(:old.cd_cep <> :new.cd_cep)
	or	(:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica)
	or	(:old.ie_tipo_complemento <> :new.ie_tipo_complemento)
	or	(:old.sg_estado <> :new.sg_estado)
	or	(:old.nr_seq_pais <> :new.nr_seq_pais))
	and	(:new.ie_tipo_complemento = 1)
	and	permiteIntegrar() then
		gravar_agend_integracao(746, 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w, Obter_Setor_Atendimento(nr_atendimento_w));
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_update 
 before update ON TASY.COMPL_PESSOA_FISICA 
 FOR EACH ROW
DECLARE
 nm_coluna_w		varchar2(50);
 ie_alterar_w		varchar2(10);
 ds_w			varchar2(50);
 vl_parametro_w	varchar2(20);
 cd_estabelecimento_w	number(10);
 qt_registros_w	number(10);
 ie_tipo_segurado_w	varchar2(10);
 ie_estipulante_w	varchar2(10);
 ie_pagador_w		varchar2(10);
 begin
 if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  and	
	(pls_usuario_pck.get_ie_exec_trigger_solic_pf	= 'S') then 	
	pls_usuario_pck.set_ie_lancar_mensagem_alt('N'); 
	pls_usuario_pck.set_ie_commit('S');
	select	cd_estabelecimento 					
	into	cd_estabelecimento_w 					
	from	pessoa_fisica						
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;	
 	if	(wheb_usuario_pck.get_cd_funcao = 5) then 
		Obter_Param_Usuario(5, 177, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
	elsif	(wheb_usuario_pck.get_cd_funcao = 32) then 
		Obter_Param_Usuario(32, 46, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
	elsif	(wheb_usuario_pck.get_cd_funcao = 1220) then 
		Obter_Param_Usuario(1220, 26, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, vl_parametro_w);
	else 
		vl_parametro_w := 'N';
	end if;
	if	(vl_parametro_w = 'S')  then	
      gerar_solicitacao_alteracao(:old.NR_SEQ_TIPO_COMPL_ADIC,:new.NR_SEQ_TIPO_COMPL_ADIC,'NR_SEQ_TIPO_COMPL_ADIC',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_TELEFONE_CELULAR,:new.NR_TELEFONE_CELULAR,'NR_TELEFONE_CELULAR',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_NAO_POSSUI_EMAIL,:new.IE_NAO_POSSUI_EMAIL,'IE_NAO_POSSUI_EMAIL',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_RESP_LEGAL,:new.IE_RESP_LEGAL,'IE_RESP_LEGAL',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDD_CELULAR,:new.NR_DDD_CELULAR,'NR_DDD_CELULAR',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDI_CELULAR,:new.NR_DDI_CELULAR,'NR_DDI_CELULAR',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_REGIME_CASAMENTO,:new.IE_REGIME_CASAMENTO,'IE_REGIME_CASAMENTO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NM_CONTATO,:new.NM_CONTATO,'NM_CONTATO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_ENDERECO,:new.DS_ENDERECO,'DS_ENDERECO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_CEP,:new.CD_CEP,'CD_CEP',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_ENDERECO,:new.NR_ENDERECO,'NR_ENDERECO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_COMPLEMENTO,:new.DS_COMPLEMENTO,'DS_COMPLEMENTO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_BAIRRO,:new.DS_BAIRRO,'DS_BAIRRO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_MUNICIPIO,:new.DS_MUNICIPIO,'DS_MUNICIPIO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.SG_ESTADO,:new.SG_ESTADO,'SG_ESTADO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_TELEFONE,:new.NR_TELEFONE,'NR_TELEFONE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_RAMAL,:new.NR_RAMAL,'NR_RAMAL',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_EMAIL,:new.DS_EMAIL,'DS_EMAIL',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_EMPRESA_REFER,:new.CD_EMPRESA_REFER,'CD_EMPRESA_REFER',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_PROFISSAO,:new.CD_PROFISSAO,'CD_PROFISSAO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_IDENTIDADE,:new.NR_IDENTIDADE,'NR_IDENTIDADE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_CPF,:new.NR_CPF,'NR_CPF',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_MUNICIPIO_IBGE,:new.CD_MUNICIPIO_IBGE,'CD_MUNICIPIO_IBGE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_SETOR_TRABALHO,:new.DS_SETOR_TRABALHO,'DS_SETOR_TRABALHO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_HORARIO_TRABALHO,:new.DS_HORARIO_TRABALHO,'DS_HORARIO_TRABALHO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_MATRICULA_TRABALHO,:new.NR_MATRICULA_TRABALHO,'NR_MATRICULA_TRABALHO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_SEQ_PARENTESCO,:new.NR_SEQ_PARENTESCO,'NR_SEQ_PARENTESCO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_FONE_ADIC,:new.DS_FONE_ADIC,'DS_FONE_ADIC',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_FAX,:new.DS_FAX,'DS_FAX',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DT_ATUALIZACAO_NREC,:new.DT_ATUALIZACAO_NREC,'DT_ATUALIZACAO_NREC',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NM_USUARIO_NREC,:new.NM_USUARIO_NREC,'NM_USUARIO_NREC',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_PESSOA_FISICA_REF,:new.CD_PESSOA_FISICA_REF,'CD_PESSOA_FISICA_REF',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_SEQ_PAIS,:new.NR_SEQ_PAIS,'NR_SEQ_PAIS',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_FONETICA,:new.DS_FONETICA,'DS_FONETICA',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_TIPO_LOGRADOURO,:new.CD_TIPO_LOGRADOURO,'CD_TIPO_LOGRADOURO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDD_TELEFONE,:new.NR_DDD_TELEFONE,'NR_DDD_TELEFONE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDI_TELEFONE,:new.NR_DDI_TELEFONE,'NR_DDI_TELEFONE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDI_FAX,:new.NR_DDI_FAX,'NR_DDI_FAX',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDD_FAX,:new.NR_DDD_FAX,'NR_DDD_FAX',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.DS_WEBSITE,:new.DS_WEBSITE,'DS_WEBSITE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NM_CONTATO_PESQUISA,:new.NM_CONTATO_PESQUISA,'NM_CONTATO_PESQUISA',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_NF_CORREIO,:new.IE_NF_CORREIO,'IE_NF_CORREIO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.QT_DEPENDENTE,:new.QT_DEPENDENTE,'QT_DEPENDENTE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_ZONA_PROCEDENCIA,:new.CD_ZONA_PROCEDENCIA,'CD_ZONA_PROCEDENCIA',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_OBRIGA_EMAIL,:new.IE_OBRIGA_EMAIL,'IE_OBRIGA_EMAIL',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_SEQ_IDENT_CNES,:new.NR_SEQ_IDENT_CNES,'NR_SEQ_IDENT_CNES',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_MALA_DIRETA,:new.IE_MALA_DIRETA,'IE_MALA_DIRETA',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_TIPO_SANGUE,:new.IE_TIPO_SANGUE,'IE_TIPO_SANGUE',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_FATOR_RH,:new.IE_FATOR_RH,'IE_FATOR_RH',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_SEQ_LOCAL_ATEND_MED,:new.NR_SEQ_LOCAL_ATEND_MED,'NR_SEQ_LOCAL_ATEND_MED',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.CD_PESSOA_END_REF,:new.CD_PESSOA_END_REF,'CD_PESSOA_END_REF',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_SEQ_END_REF,:new.NR_SEQ_END_REF,'NR_SEQ_END_REF',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_EMPRESA_PAGADORA,:new.IE_EMPRESA_PAGADORA,'IE_EMPRESA_PAGADORA',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_SEQ_REGIAO,:new.NR_SEQ_REGIAO,'NR_SEQ_REGIAO',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.IE_CORRESPONDENCIA,:new.IE_CORRESPONDENCIA,'IE_CORRESPONDENCIA',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDI_FONE_ADIC,:new.NR_DDI_FONE_ADIC,'NR_DDI_FONE_ADIC',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
      gerar_solicitacao_alteracao(:old.NR_DDD_FONE_ADIC,:new.NR_DDD_FONE_ADIC,'NR_DDD_FONE_ADIC',:new.cd_pessoa_fisica,cd_estabelecimento_w, :new.nm_usuario);
	elsif	(vl_parametro_w = 'D') then	
	select	max(a.ie_tipo_segurado)	 
	into	ie_tipo_segurado_w	 
	from	pls_segurado a,		 
	     	pls_regra_solic_alt_pf b 
	where	a.cd_pessoa_fisica	= :new.cd_pessoa_fisica
	and	b.nm_tabela = 'COMPL_PESSOA_FISICA'
	and 	a.ie_tipo_segurado = b.ie_tipo_segurado
	and 	((b.cd_funcao is null) or (b.cd_funcao = wheb_usuario_pck.get_cd_funcao))
	and	((a.dt_rescisao is null)  or (a.dt_rescisao > sysdate));

	select	decode(count(1),0,'N','S')	
	into	ie_estipulante_w	
	from	pls_contrato		
	where	cd_pf_estipulante	= :new.cd_pessoa_fisica
	and	((dt_rescisao_contrato is null)  or (dt_rescisao_contrato > sysdate));

	select	decode(count(1),0,'N','S')	
	into	ie_pagador_w	
	from	pls_contrato_pagador		
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
	and	((dt_rescisao is null)  or (dt_rescisao > sysdate));

      gerar_solicitacao_alt_regra(:old.NR_SEQ_TIPO_COMPL_ADIC,:new.NR_SEQ_TIPO_COMPL_ADIC,'NR_SEQ_TIPO_COMPL_ADIC',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_TIPO_COMPL_ADIC := :old.NR_SEQ_TIPO_COMPL_ADIC;
	end if;      gerar_solicitacao_alt_regra(:old.NR_TELEFONE_CELULAR,:new.NR_TELEFONE_CELULAR,'NR_TELEFONE_CELULAR',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_TELEFONE_CELULAR := :old.NR_TELEFONE_CELULAR;
	end if;      gerar_solicitacao_alt_regra(:old.IE_NAO_POSSUI_EMAIL,:new.IE_NAO_POSSUI_EMAIL,'IE_NAO_POSSUI_EMAIL',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_NAO_POSSUI_EMAIL := :old.IE_NAO_POSSUI_EMAIL;
	end if;      gerar_solicitacao_alt_regra(:old.IE_RESP_LEGAL,:new.IE_RESP_LEGAL,'IE_RESP_LEGAL',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_RESP_LEGAL := :old.IE_RESP_LEGAL;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDD_CELULAR,:new.NR_DDD_CELULAR,'NR_DDD_CELULAR',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDD_CELULAR := :old.NR_DDD_CELULAR;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDI_CELULAR,:new.NR_DDI_CELULAR,'NR_DDI_CELULAR',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDI_CELULAR := :old.NR_DDI_CELULAR;
	end if;      gerar_solicitacao_alt_regra(:old.IE_REGIME_CASAMENTO,:new.IE_REGIME_CASAMENTO,'IE_REGIME_CASAMENTO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_REGIME_CASAMENTO := :old.IE_REGIME_CASAMENTO;
	end if;      gerar_solicitacao_alt_regra(:old.NM_CONTATO,:new.NM_CONTATO,'NM_CONTATO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NM_CONTATO := :old.NM_CONTATO;
	end if;      gerar_solicitacao_alt_regra(:old.DS_ENDERECO,:new.DS_ENDERECO,'DS_ENDERECO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_ENDERECO := :old.DS_ENDERECO;
	end if;      gerar_solicitacao_alt_regra(:old.CD_CEP,:new.CD_CEP,'CD_CEP',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_CEP := :old.CD_CEP;
	end if;      gerar_solicitacao_alt_regra(:old.NR_ENDERECO,:new.NR_ENDERECO,'NR_ENDERECO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_ENDERECO := :old.NR_ENDERECO;
	end if;      gerar_solicitacao_alt_regra(:old.DS_COMPLEMENTO,:new.DS_COMPLEMENTO,'DS_COMPLEMENTO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_COMPLEMENTO := :old.DS_COMPLEMENTO;
	end if;      gerar_solicitacao_alt_regra(:old.DS_BAIRRO,:new.DS_BAIRRO,'DS_BAIRRO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_BAIRRO := :old.DS_BAIRRO;
	end if;      gerar_solicitacao_alt_regra(:old.DS_MUNICIPIO,:new.DS_MUNICIPIO,'DS_MUNICIPIO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_MUNICIPIO := :old.DS_MUNICIPIO;
	end if;      gerar_solicitacao_alt_regra(:old.SG_ESTADO,:new.SG_ESTADO,'SG_ESTADO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.SG_ESTADO := :old.SG_ESTADO;
	end if;      gerar_solicitacao_alt_regra(:old.NR_TELEFONE,:new.NR_TELEFONE,'NR_TELEFONE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_TELEFONE := :old.NR_TELEFONE;
	end if;      gerar_solicitacao_alt_regra(:old.NR_RAMAL,:new.NR_RAMAL,'NR_RAMAL',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_RAMAL := :old.NR_RAMAL;
	end if;      gerar_solicitacao_alt_regra(:old.DS_EMAIL,:new.DS_EMAIL,'DS_EMAIL',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_EMAIL := :old.DS_EMAIL;
	end if;      gerar_solicitacao_alt_regra(:old.CD_EMPRESA_REFER,:new.CD_EMPRESA_REFER,'CD_EMPRESA_REFER',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_EMPRESA_REFER := :old.CD_EMPRESA_REFER;
	end if;      gerar_solicitacao_alt_regra(:old.CD_PROFISSAO,:new.CD_PROFISSAO,'CD_PROFISSAO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_PROFISSAO := :old.CD_PROFISSAO;
	end if;      gerar_solicitacao_alt_regra(:old.NR_IDENTIDADE,:new.NR_IDENTIDADE,'NR_IDENTIDADE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_IDENTIDADE := :old.NR_IDENTIDADE;
	end if;      gerar_solicitacao_alt_regra(:old.NR_CPF,:new.NR_CPF,'NR_CPF',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_CPF := :old.NR_CPF;
	end if;      gerar_solicitacao_alt_regra(:old.CD_MUNICIPIO_IBGE,:new.CD_MUNICIPIO_IBGE,'CD_MUNICIPIO_IBGE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_MUNICIPIO_IBGE := :old.CD_MUNICIPIO_IBGE;
	end if;      gerar_solicitacao_alt_regra(:old.DS_SETOR_TRABALHO,:new.DS_SETOR_TRABALHO,'DS_SETOR_TRABALHO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_SETOR_TRABALHO := :old.DS_SETOR_TRABALHO;
	end if;      gerar_solicitacao_alt_regra(:old.DS_HORARIO_TRABALHO,:new.DS_HORARIO_TRABALHO,'DS_HORARIO_TRABALHO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_HORARIO_TRABALHO := :old.DS_HORARIO_TRABALHO;
	end if;      gerar_solicitacao_alt_regra(:old.NR_MATRICULA_TRABALHO,:new.NR_MATRICULA_TRABALHO,'NR_MATRICULA_TRABALHO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_MATRICULA_TRABALHO := :old.NR_MATRICULA_TRABALHO;
	end if;      gerar_solicitacao_alt_regra(:old.NR_SEQ_PARENTESCO,:new.NR_SEQ_PARENTESCO,'NR_SEQ_PARENTESCO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_PARENTESCO := :old.NR_SEQ_PARENTESCO;
	end if;      gerar_solicitacao_alt_regra(:old.DS_FONE_ADIC,:new.DS_FONE_ADIC,'DS_FONE_ADIC',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_FONE_ADIC := :old.DS_FONE_ADIC;
	end if;      gerar_solicitacao_alt_regra(:old.DS_FAX,:new.DS_FAX,'DS_FAX',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_FAX := :old.DS_FAX;
	end if;      gerar_solicitacao_alt_regra(:old.DT_ATUALIZACAO_NREC,:new.DT_ATUALIZACAO_NREC,'DT_ATUALIZACAO_NREC',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DT_ATUALIZACAO_NREC := :old.DT_ATUALIZACAO_NREC;
	end if;      gerar_solicitacao_alt_regra(:old.NM_USUARIO_NREC,:new.NM_USUARIO_NREC,'NM_USUARIO_NREC',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NM_USUARIO_NREC := :old.NM_USUARIO_NREC;
	end if;      gerar_solicitacao_alt_regra(:old.CD_PESSOA_FISICA_REF,:new.CD_PESSOA_FISICA_REF,'CD_PESSOA_FISICA_REF',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_PESSOA_FISICA_REF := :old.CD_PESSOA_FISICA_REF;
	end if;      gerar_solicitacao_alt_regra(:old.NR_SEQ_PAIS,:new.NR_SEQ_PAIS,'NR_SEQ_PAIS',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_PAIS := :old.NR_SEQ_PAIS;
	end if;      gerar_solicitacao_alt_regra(:old.DS_FONETICA,:new.DS_FONETICA,'DS_FONETICA',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_FONETICA := :old.DS_FONETICA;
	end if;      gerar_solicitacao_alt_regra(:old.CD_TIPO_LOGRADOURO,:new.CD_TIPO_LOGRADOURO,'CD_TIPO_LOGRADOURO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_TIPO_LOGRADOURO := :old.CD_TIPO_LOGRADOURO;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDD_TELEFONE,:new.NR_DDD_TELEFONE,'NR_DDD_TELEFONE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDD_TELEFONE := :old.NR_DDD_TELEFONE;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDI_TELEFONE,:new.NR_DDI_TELEFONE,'NR_DDI_TELEFONE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDI_TELEFONE := :old.NR_DDI_TELEFONE;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDI_FAX,:new.NR_DDI_FAX,'NR_DDI_FAX',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDI_FAX := :old.NR_DDI_FAX;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDD_FAX,:new.NR_DDD_FAX,'NR_DDD_FAX',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDD_FAX := :old.NR_DDD_FAX;
	end if;      gerar_solicitacao_alt_regra(:old.DS_WEBSITE,:new.DS_WEBSITE,'DS_WEBSITE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.DS_WEBSITE := :old.DS_WEBSITE;
	end if;      gerar_solicitacao_alt_regra(:old.NM_CONTATO_PESQUISA,:new.NM_CONTATO_PESQUISA,'NM_CONTATO_PESQUISA',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NM_CONTATO_PESQUISA := :old.NM_CONTATO_PESQUISA;
	end if;      gerar_solicitacao_alt_regra(:old.IE_NF_CORREIO,:new.IE_NF_CORREIO,'IE_NF_CORREIO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_NF_CORREIO := :old.IE_NF_CORREIO;
	end if;      gerar_solicitacao_alt_regra(:old.QT_DEPENDENTE,:new.QT_DEPENDENTE,'QT_DEPENDENTE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.QT_DEPENDENTE := :old.QT_DEPENDENTE;
	end if;      gerar_solicitacao_alt_regra(:old.CD_ZONA_PROCEDENCIA,:new.CD_ZONA_PROCEDENCIA,'CD_ZONA_PROCEDENCIA',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_ZONA_PROCEDENCIA := :old.CD_ZONA_PROCEDENCIA;
	end if;      gerar_solicitacao_alt_regra(:old.IE_OBRIGA_EMAIL,:new.IE_OBRIGA_EMAIL,'IE_OBRIGA_EMAIL',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_OBRIGA_EMAIL := :old.IE_OBRIGA_EMAIL;
	end if;      gerar_solicitacao_alt_regra(:old.NR_SEQ_IDENT_CNES,:new.NR_SEQ_IDENT_CNES,'NR_SEQ_IDENT_CNES',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_IDENT_CNES := :old.NR_SEQ_IDENT_CNES;
	end if;      gerar_solicitacao_alt_regra(:old.IE_MALA_DIRETA,:new.IE_MALA_DIRETA,'IE_MALA_DIRETA',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_MALA_DIRETA := :old.IE_MALA_DIRETA;
	end if;      gerar_solicitacao_alt_regra(:old.IE_TIPO_SANGUE,:new.IE_TIPO_SANGUE,'IE_TIPO_SANGUE',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_TIPO_SANGUE := :old.IE_TIPO_SANGUE;
	end if;      gerar_solicitacao_alt_regra(:old.IE_FATOR_RH,:new.IE_FATOR_RH,'IE_FATOR_RH',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_FATOR_RH := :old.IE_FATOR_RH;
	end if;      gerar_solicitacao_alt_regra(:old.NR_SEQ_LOCAL_ATEND_MED,:new.NR_SEQ_LOCAL_ATEND_MED,'NR_SEQ_LOCAL_ATEND_MED',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_LOCAL_ATEND_MED := :old.NR_SEQ_LOCAL_ATEND_MED;
	end if;      gerar_solicitacao_alt_regra(:old.CD_PESSOA_END_REF,:new.CD_PESSOA_END_REF,'CD_PESSOA_END_REF',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.CD_PESSOA_END_REF := :old.CD_PESSOA_END_REF;
	end if;      gerar_solicitacao_alt_regra(:old.NR_SEQ_END_REF,:new.NR_SEQ_END_REF,'NR_SEQ_END_REF',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_END_REF := :old.NR_SEQ_END_REF;
	end if;      gerar_solicitacao_alt_regra(:old.IE_EMPRESA_PAGADORA,:new.IE_EMPRESA_PAGADORA,'IE_EMPRESA_PAGADORA',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_EMPRESA_PAGADORA := :old.IE_EMPRESA_PAGADORA;
	end if;      gerar_solicitacao_alt_regra(:old.NR_SEQ_REGIAO,:new.NR_SEQ_REGIAO,'NR_SEQ_REGIAO',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_SEQ_REGIAO := :old.NR_SEQ_REGIAO;
	end if;      gerar_solicitacao_alt_regra(:old.IE_CORRESPONDENCIA,:new.IE_CORRESPONDENCIA,'IE_CORRESPONDENCIA',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.IE_CORRESPONDENCIA := :old.IE_CORRESPONDENCIA;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDI_FONE_ADIC,:new.NR_DDI_FONE_ADIC,'NR_DDI_FONE_ADIC',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDI_FONE_ADIC := :old.NR_DDI_FONE_ADIC;
	end if;      gerar_solicitacao_alt_regra(:old.NR_DDD_FONE_ADIC,:new.NR_DDD_FONE_ADIC,'NR_DDD_FONE_ADIC',:new.cd_pessoa_fisica,'COMPL_PESSOA_FISICA',wheb_usuario_pck.get_cd_funcao,:new.ie_tipo_complemento,ie_tipo_segurado_w,ie_estipulante_w,ie_pagador_w,cd_estabelecimento_w, :new.nm_usuario,ie_alterar_w);
	if	(ie_alterar_w	= 'S') then
		:new.NR_DDD_FONE_ADIC := :old.NR_DDD_FONE_ADIC;
	end if;
end if;
if	(vl_parametro_w <> 'N') then	
	select	count(1)		
	into	qt_registros_w		
	from	pessoa_fisica_solic_alt	
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	if	(qt_registros_w > 0) then 
		tasy_gerar_solicitacao_compl(:new.cd_pessoa_fisica,:new.ie_tipo_complemento);
		if	(vl_parametro_w = 'S') then	
			wheb_mensagem_pck.exibir_mensagem_abort(231273);
		end if;
		pls_usuario_pck.set_ie_lancar_mensagem_alt('S'); 
	end if;
end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_pls_rps
after update ON TASY.COMPL_PESSOA_FISICA for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
ie_alterou_w 	varchar2(1) := 'N';

begin
begin
	if	(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) or
		(nvl(:new.sg_estado,'0')	!= nvl(:old.sg_estado,'0')) or
		(nvl(:new.ds_bairro,'0')	!= nvl(:old.ds_bairro,'0')) or
		(nvl(:new.ds_endereco,'0')	!= nvl(:old.ds_endereco,'0')) or
		(nvl(:new.ds_municipio,'0')	!= nvl(:old.ds_municipio,'0')) or
		(nvl(:new.nr_cpf,'0')		!= nvl(:old.nr_cpf,'0')) or
		(nvl(:new.nr_endereco,0)	!= nvl(:old.nr_endereco,0)) or
		(nvl(:new.cd_cep,'0')		!= nvl(:old.cd_cep,'0')) then
		ie_alterou_w := 'S';
	end if;

	if	(ie_alterou_w = 'S') then
		pls_gerar_alt_prest_rps(null, :new.cd_pessoa_fisica, null, :old.nr_cpf, :old.cd_municipio_ibge, null, :new.nm_usuario, null, :new.nr_seq_tipo_compl_adic, null);
	end if;
exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_a400_up
before update ON TASY.COMPL_PESSOA_FISICA for each row
declare
nr_seq_prestador_w	pls_prestador.nr_sequencia%type;
ie_divulga_email_w	pls_prestador.ie_divulga_email%type;
qt_email_w		pls_integer;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ie_param_6_w		varchar2(1);
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if	(:new.ie_tipo_complemento in(1, 2, 9)) and
		((:new.ds_email <> :old.ds_email) or
		 (:new.ds_email is null and :old.ds_email is not null))then

		select	max(ie_divulga_email),
			min(nr_sequencia)
		into	ie_divulga_email_w,
			nr_seq_prestador_w
		from	pls_prestador
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
		and	ie_divulga_email	= 'S'
		and	ie_ptu_a400		= 'S';

		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

		obter_param_usuario( 1323,  6, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_W, ie_param_6_w);

		if	(nr_seq_prestador_w is not null) and
			(nvl(ie_divulga_email_w, 'N') = 'S') and
			(nvl(ie_param_6_w, 'S') = 'N') then

			case 	(:new.ie_tipo_complemento)
				when 	1 then -- Endere�o Residencial

					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email is not null
					and	b.ie_tipo_endereco_original = 'PFR';
				when	2	then --Endere�o comercial

					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email is not null
					and	b.ie_tipo_endereco_original = 'PFC';
				when	9	then -- Endere�o adicional
					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email is not null
					and	b.ie_tipo_endereco_original = 'PFA';
				else
					qt_email_w := 0;
			end case;

			if	(qt_email_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(387909);
			end if;
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_atual_hl7
after insert or update ON TASY.COMPL_PESSOA_FISICA for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
ie_existe_atend_w 	number(10);
ds_param_integration_w	varchar2(500);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	ds_sep_bv_w := obter_separador_bv;

	ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
				'nr_atendimento=0' || ds_sep_bv_w ||
				'nr_seq_interno=0' || ds_sep_bv_w;
	gravar_agend_integracao(18, ds_param_integ_hl7_w);


	select 	count(*)
	into 	ie_existe_atend_w
	from	atendimento_paciente
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	if	(ie_existe_atend_w > 0) then
		gravar_agend_integracao(426, ds_param_integ_hl7_w);
	end if;

	if (wheb_usuario_pck.is_evento_ativo(853) = 'S') then
		enviar_pf_medico_hl7(:new.cd_pessoa_fisica,'MUP');
	end if;

	call_bifrost_content('patient.address.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Compl_Pessoa_Fisica_Atual
BEFORE INSERT OR UPDATE ON TASY.COMPL_PESSOA_FISICA FOR EACH ROW
DECLARE
nr_seq_cbos_tiss_w				number(10);
qt_reg_w						number(1);
nm_usuario_ww					varchar2(20);
qt_registro_w					number(10);
ie_atualizar_email_usuario_w	varchar2(1);
cd_estabelecimento_w			number(4);
ie_revisar_alt_w				varchar2(10);
ie_lancar_mensagem_alt_w		varchar2(10);
nr_seq_prestador_w				pls_prestador.nr_sequencia%type;
qt_valid_mail_w					number(10);

esocial_altera_cad_det_w	gerar_esocial_dados_pck.t_esocial_altera_cad_det;

cursor c_prestadores is
	select	a.nr_sequencia nr_seq_prestador
	from	pls_prestador a
	where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica;

BEGIN

select	nvl(max(nm_usuario), 'X'),
	max(cd_estabelecimento)
into	nm_usuario_ww,
	cd_estabelecimento_w
from	usuario
where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

Obter_Param_Usuario(0,166,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_atualizar_email_usuario_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

/*aaschlote 25/04/2013 - OS 574467 -Tratamento para a apresenta��o da mensagem de altera��o enviada a an�lise*/
ie_lancar_mensagem_alt_w	:= pls_usuario_pck.get_ie_lancar_mensagem_alt;

	/* Coelho em 03/01/07 	OS45273 */
if	((:new.nm_contato <> :old.nm_contato) or
	(:old.ds_fonetica is null )) then
	:new.ds_fonetica := gera_fonetica(:new.nm_contato,'N');
end if;

if	(:new.cd_profissao is not null) and
	(:old.cd_profissao is null) then
	begin
	select	nr_seq_cbos_tiss
	into	nr_seq_cbos_tiss_w
	from	profissao
	where	cd_profissao	= :new.cd_profissao;
	exception
	when others then
		nr_seq_cbos_tiss_w	:= null;
	end;

	update	pessoa_fisica
	set	nr_seq_cbo_saude	= nr_seq_cbos_tiss_w,
		nm_usuario 		= :new.nm_usuario,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica
	and	nr_seq_cbo_saude is null
	and	nr_seq_cbos_tiss_w is not null;
end if;

/*aaschlote 27/12/2012 OS 524353*/
Obter_Param_Usuario(0,189,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_revisar_alt_w);

ie_revisar_alt_w	:= nvl(ie_revisar_alt_w,'R');

/* Francisco em 11/03/2008  - OS 82683 - Se � do plano e foi alterado, deve mudar o campo IE_REVISAR */
if	(ie_revisar_alt_w <> 'M') then
	select	count(*)
	into	qt_registro_w
	from	pls_contrato
	where	cd_pf_estipulante	= :new.cd_pessoa_fisica;

	if	(qt_registro_w > 0) then
		update	pessoa_fisica
		set	ie_revisar	= ie_revisar_alt_w,
			nm_usuario 	= :new.nm_usuario,
			dt_atualizacao	= sysdate
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	else
		select	count(*)
		into	qt_registro_w
		from	pls_contrato_pagador
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

		if	(qt_registro_w > 0) then
			update	pessoa_fisica
			set	ie_revisar	= ie_revisar_alt_w,
				nm_usuario 	= :new.nm_usuario,
				dt_atualizacao	= sysdate
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
		else
			select	count(*)
			into	qt_registro_w
			from	pls_segurado
			where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

			if	(qt_registro_w > 0) then
				update	pessoa_fisica
				set	ie_revisar	= ie_revisar_alt_w,
					nm_usuario 	= :new.nm_usuario,
					dt_atualizacao	= sysdate
				where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
			else
				select	count(*)
				into	qt_registro_w
				from	pls_prestador
				where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

				if	(qt_registro_w > 0) then
					update	pessoa_fisica
					set	ie_revisar	= ie_revisar_alt_w,
						nm_usuario 	= :new.nm_usuario,
						dt_atualizacao	= sysdate
					where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
				end if;
			end if;
		end if;
	end if;
end if;
/* Fim Francisco em 11/03/2008 */

/*       Coelho 03/07/2008 OS95248 */
if	(:new.nm_contato	<> :old.nm_contato) or
	(:new.nm_contato_pesquisa is null) then
	:new.nm_contato_pesquisa	:= padronizar_nome(:new.nm_contato);
end if;

if	(nm_usuario_ww <> 'X') and
	(:new.ie_tipo_complemento = 1) and
	(:new.ds_email <> :old.ds_email) and
	(ie_atualizar_email_usuario_w = 'S') then
	update	usuario
	set	ds_email	= :new.ds_email
	where	nm_usuario	= nm_usuario_ww;
end if;

begin
if	(:new.ie_tipo_complemento = 1) and
	(updating) then

	open c_prestadores;
	loop
	fetch c_prestadores into
		nr_seq_prestador_w;
	exit when c_prestadores%notfound;
		begin

		/* Verifica se houve altera��o em algum dos campos utilizados no E-Social */
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('CD_CEP',			:old.cd_cep,		:new.cd_cep,		esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_CPF',			:old.nr_cpf,		:new.nr_cpf,		esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_DDD_TELEFONE',		:old.nr_ddd_telefone,	:new.nr_ddd_telefone,	esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_DDI_TELEFONE',		:old.nr_ddi_telefone,	:new.nr_ddi_telefone,	esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_ENDERECO',		:old.nr_endereco, 	:new.nr_endereco,	esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_IDENTIDADE',		:old.nr_identidade,	:new.nr_identidade,	esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('NR_TELEFONE',		:old.nr_telefone,	:new.nr_telefone,	esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('SG_ESTADO',		:old.sg_estado, 	:new.sg_estado,		esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('CD_TIPO_LOGRADOURO',	:old.cd_tipo_logradouro,:new.cd_tipo_logradouro,esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('DS_BAIRRO',		:old.ds_bairro,		:new.ds_bairro,		esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('DS_COMPLEMENTO',		:old.ds_complemento,	:new.ds_complemento,	esocial_altera_cad_det_w);
		gerar_esocial_dados_pck.adicionar_alteracao_cad_det('DS_ENDERECO',		:old.ds_endereco,	:new.ds_endereco,	esocial_altera_cad_det_w);

		if	(esocial_altera_cad_det_w.count > 0) then
			gerar_esocial_dados_pck.gravar_alteracao_cad_det(nr_seq_prestador_w, :new.nm_usuario, esocial_altera_cad_det_w);
		end if;

		end;
	end loop;
	close c_prestadores;
end if;
exception
when others then
	nr_seq_prestador_w	:= null;
end;

--Validar se e-mail informado est� correto
if	(:new.ds_email is not null) or (:old.ds_email is not null) then

	select  count(*)
	into    qt_valid_mail_w
	from    dual
	where   REGEXP_LIKE (:new.ds_email,'^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,8}$');

	if	((qt_valid_mail_w = 0) and (nvl((pkg_i18n.get_user_locale), 'pt_BR') = 'es_MX')) then
		wheb_mensagem_pck.exibir_mensagem_abort(139551);
	end if;

end if;


pls_usuario_pck.set_ie_lancar_mensagem_alt(ie_lancar_mensagem_alt_w);

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_delete
after delete ON TASY.COMPL_PESSOA_FISICA for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
nr_prontuario_w			pessoa_fisica.nr_prontuario%type;
ie_funcionario_w		pessoa_fisica.ie_funcionario%type;
nr_seq_conselho_w		pessoa_fisica.nr_seq_conselho%type;
ds_sep_bv_w				varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

begin
ds_sep_bv_w := obter_separador_bv;
select nr_prontuario,
		ie_funcionario,
		nr_seq_conselho
into nr_prontuario_w,
	ie_funcionario_w,
	nr_seq_conselho_w
from pessoa_fisica
where cd_pessoa_fisica = :old.cd_pessoa_fisica;
exception
when others then
	nr_prontuario_w	:= null;
end;

reg_integracao_p.ie_operacao		:=	'A';
reg_integracao_p.nr_prontuario      :=   nr_prontuario_w;
reg_integracao_p.cd_pessoa_fisica      :=   :old.cd_pessoa_fisica;
reg_integracao_p.ie_funcionario 	:= ie_funcionario_w;
reg_integracao_p.nr_seq_conselho	:= nr_seq_conselho_w;

SELECT decode(COUNT(*), 0, 'N', 'S')
  INTO reg_integracao_p.ie_pessoa_atend
  FROM pessoa_fisica_aux
 WHERE cd_pessoa_fisica = :old.cd_pessoa_fisica
   AND nr_primeiro_atend IS NOT NULL;

if :old.nr_cpf is not null then
   reg_integracao_p.ie_possui_cpf := 'S';
end if;

if	(wheb_usuario_pck.get_ie_lote_contabil = 'N') then
	gerar_int_padrao.gravar_integracao('12',:old.cd_pessoa_fisica,:old.nm_usuario, reg_integracao_p);
end if;

send_physician_intpd(:old.cd_pessoa_fisica, :old.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');
if (wheb_usuario_pck.is_evento_ativo(853) = 'S') then
  enviar_pf_medico_hl7(:old.cd_pessoa_fisica,'MUP');
end if;

delete from compl_pessoa_fisica_setor where nr_seq_compl_pessoa_fisica = :old.nr_sequencia and cd_pessoa_fisica = :old.cd_pessoa_fisica;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_update_hl7
before update ON TASY.COMPL_PESSOA_FISICA for each row
declare

ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
qt_atendimento_w     number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then

	ds_sep_bv_w := obter_separador_bv;

	if	(nvl(:old.ds_endereco,'X') 	<> nvl(:new.ds_endereco,'X')) 	or
		(nvl(:old.ds_municipio,'X') <> nvl(:new.ds_municipio,'X')) 	or
		(nvl(:old.sg_estado,'X') 	<> nvl(:new.sg_estado,'X')) 	or
		(nvl(:old.cd_cep,'X') 		<> nvl(:new.cd_cep,'X')) 		or
		(nvl(:old.nr_ddi_telefone,'X') 	<> nvl(:new.nr_ddi_telefone,'X')) or
		(nvl(:old.nr_ddd_telefone,'X') 	<> nvl(:new.nr_ddd_telefone,'X')) or
		(nvl(:old.nr_telefone,'X') 	<> nvl(:new.nr_telefone,'X')) 	then
		begin

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
		gravar_agend_integracao(145, ds_param_integ_hl7_w);

	--HCOR ADT A08

		if (wheb_usuario_pck.is_evento_ativo(783) = 'S') then

			qt_atendimento_w	:= obter_qtd_atendimentos(:new.cd_pessoa_fisica);

			if (qt_atendimento_w > 0) then
				ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ;
				gravar_agend_integracao(783, ds_param_integ_hl7_w);
			end if;
		end if;

		end;
	end if;

end if;

    --Outbound ADT_A08
	call_bifrost_content('patient.registration.update','person_json_pck.get_patient_message_clob('||:new.cd_pessoa_fisica||')', :new.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_fisica_AfterUp
after update or insert ON TASY.COMPL_PESSOA_FISICA for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_existe_medico_p		number(10) := 0;
nr_identidade_w				varchar2(15);
nm_pessoa_fisica_w			varchar2(60);
nr_telefone_celular_w			varchar2(40);
ie_grau_instrucao_w			number(2);
nr_cep_cidade_nasc_w			varchar2(15);
nr_prontuario_w				number(10);
cd_religiao_w				number(5);
nr_pis_pasep_w				varchar2(11);
cd_nacionalidade_w			varchar2(8);
ie_dependencia_sus_w			varchar2(1);
qt_altura_cm_w				number(4,1);
ie_tipo_sangue_w				varchar2(2);
ie_fator_rh_w				varchar2(1);
dt_nascimento_w				date;
dt_obito_w				date;
ie_sexo_w				varchar2(1);
nr_iss_w					varchar2(20);
ie_estado_civil_w				varchar2(2);
nr_inss_w					varchar2(20);
nr_cpf_w					varchar2(11);
nr_cert_nasc_w				varchar2(255);
cd_cargo_w				number(10);
nr_cert_casamento_w			varchar2(255);
ds_codigo_prof_w				varchar2(15);
cd_empresa_w				number(4);
ie_funcionario_w				varchar2(1);
nr_seq_cor_pele_w				number(10);
ds_orgao_emissor_ci_w			pessoa_fisica.ds_orgao_emissor_ci%type;
nr_cartao_nac_sus_w			varchar2(20);
cd_cbo_sus_w				number(6);
cd_atividade_sus_w			number(3);
ie_vinculo_sus_w				number(1);
cd_estabelecimento_w			number(4);
cd_sistema_ant_w				varchar2(20);
ie_frequenta_escola_w			varchar2(1);
cd_funcionario_w        varchar2(15);
nr_pager_bip_w        varchar2(25);
nr_transacao_sus_w      varchar2(20);
cd_medico_w        varchar2(10);
ie_tipo_prontuario_w      number(3);
dt_emissao_ci_w        date;
nr_seq_conselho_w        number(10);
dt_admissao_hosp_w      date;
ie_fluencia_portugues_w      varchar2(5);
nr_titulo_eleitor_w        varchar2(20);
nr_zona_w        varchar2(5);
nr_secao_w        varchar2(15);
nr_cartao_estrangeiro_w      varchar2(30);
nr_reg_geral_estrang_w      varchar2(30);
dt_chegada_brasil_w      date;
sg_emissora_ci_w        varchar2(2);
dt_naturalizacao_pf_w      date;
nr_ctps_w        pessoa_fisica.nr_ctps%type;
nr_serie_ctps_w        pessoa_fisica.nr_serie_ctps%type;
uf_emissora_ctps_w      pessoa_fisica.uf_emissora_ctps%type;
dt_emissao_ctps_w        date;
nr_portaria_nat_w        varchar2(16);
nr_seq_cartorio_nasc_w      number(10);
nr_seq_cartorio_casamento_w    number(10);
dt_emissao_cert_nasc_w      date;
dt_emissao_cert_casamento_w    date;
nr_livro_cert_nasc_w      number(10);
nr_livro_cert_casamento_w      number(10);
dt_cadastro_original_w      date;
nr_folha_cert_nasc_w      number(10);
nr_folha_cert_casamento_w      number(10);
nr_same_w        number(10);
ds_observacao_w        varchar2(2000);
qt_dependente_w        number(2);
nr_transplante_w        number(10);
dt_validade_rg_w        date;
dt_revisao_w        date;
dt_demissao_hosp_w      date;
cd_puericultura_w        number(6);
cd_cnes_w        varchar2(20);
ds_apelido_w        varchar2(60);
ie_endereco_correspondencia_w    number(2);
qt_peso_nasc_w        number(6,3);
uf_conselho_w        pessoa_fisica.uf_conselho%type;
nm_abreviado_w        varchar2(80);
nr_ccm_w        number(10);
dt_fim_experiencia_w      date;
dt_validade_conselho_w      date;
ds_profissao_w        varchar2(255);
ds_empresa_pf_w        varchar2(255);
nr_passaporte_w        varchar2(255);
nr_cnh_w          varchar2(255);
nr_cert_militar_w        varchar2(255);
nr_pront_ext_w        varchar2(100);
ie_escolaridade_cns_w      varchar2(10);
ie_situacao_conj_cns_w      varchar2(10);
nr_folha_cert_div_w      number(10);
nr_cert_divorcio_w        varchar2(20);
nr_seq_cartorio_divorcio_w      number(10);
dt_emissao_cert_divorcio_w      date;
nr_livro_cert_divorcio_w      number(10);
dt_alta_institucional_w      date;
dt_transplante_w        date;
nr_matricula_nasc_w      varchar2(32);
ie_doador_w        varchar2(15);
dt_vencimento_cnh_w      date;
ds_categoria_cnh_w      varchar2(30);
qt_existe_w        number(10);
ie_opcao_w        varchar2(1) := 'A';
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

select  count(*)
into  qt_existe_w
from  sup_parametro_integracao a,
  sup_int_regra_pf b
where  a.nr_sequencia = b.nr_seq_integracao
and  a.ie_evento = 'PF'
and  a.ie_forma = 'E'
and  a.ie_situacao = 'A'
and  b.ie_situacao = 'A';

if  (qt_existe_w > 0) and
  (:new.nm_usuario <> 'INTEGR_TASY') then

  select  nr_identidade,
    substr(obter_nome_pf(:new.cd_pessoa_fisica),1,60),
    nr_telefone_celular,
    ie_grau_instrucao,
    nr_cep_cidade_nasc,
    nr_prontuario,
    cd_religiao,
    nr_pis_pasep,
    cd_nacionalidade,
    ie_dependencia_sus,
    qt_altura_cm,
    ie_tipo_sangue,
    ie_fator_rh,
    dt_nascimento,
    dt_obito,
    ie_sexo,
    nr_iss,
    ie_estado_civil,
    nr_inss,
    nr_cpf,
    nr_cert_nasc,
    cd_cargo,
    nr_cert_casamento,
    ds_codigo_prof,
    cd_empresa,
    ie_funcionario,
    nr_seq_cor_pele,
    ds_orgao_emissor_ci,
    nr_cartao_nac_sus,
    cd_cbo_sus,
    cd_atividade_sus,
    ie_vinculo_sus,
    cd_sistema_ant,
    ie_frequenta_escola,
    cd_funcionario,
    nr_pager_bip,
    nr_transacao_sus,
    cd_medico,
    ie_tipo_prontuario,
    dt_emissao_ci,
    nr_seq_conselho,
    dt_admissao_hosp,
    ie_fluencia_portugues,
    nr_titulo_eleitor,
    nr_zona,
    nr_secao,
    nr_cartao_estrangeiro,
    nr_reg_geral_estrang,
    dt_chegada_brasil,
    sg_emissora_ci,
    dt_naturalizacao_pf,
    nr_ctps,
    nr_serie_ctps,
    uf_emissora_ctps,
    dt_emissao_ctps,
    nr_portaria_nat,
    nr_seq_cartorio_nasc,
    nr_seq_cartorio_casamento,
    dt_emissao_cert_nasc,
    dt_emissao_cert_casamento,
    nr_livro_cert_nasc,
    nr_livro_cert_casamento,
    dt_cadastro_original,
    nr_folha_cert_nasc,
    nr_folha_cert_casamento,
    nr_same,
    ds_observacao,
    qt_dependente,
    nr_transplante,
    dt_validade_rg,
    dt_revisao,
    dt_demissao_hosp,
    cd_puericultura,
    cd_cnes,
    ds_apelido,
    ie_endereco_correspondencia,
    qt_peso_nasc,
    uf_conselho,
    nm_abreviado,
    nr_ccm,
    dt_fim_experiencia,
    dt_validade_conselho,
    ds_profissao,
    ds_empresa_pf,
    nr_passaporte,
    nr_cnh,
    nr_cert_militar,
    nr_pront_ext,
    ie_escolaridade_cns,
    ie_situacao_conj_cns,
    nr_folha_cert_div,
    nr_cert_divorcio,
    nr_seq_cartorio_divorcio,
    dt_emissao_cert_divorcio,
    nr_livro_cert_divorcio,
    dt_alta_institucional,
    dt_transplante,
    nr_matricula_nasc,
    ie_doador,
    dt_vencimento_cnh,
    ds_categoria_cnh
  into  nr_identidade_w,
    nm_pessoa_fisica_w,
    nr_telefone_celular_w,
    ie_grau_instrucao_w,
    nr_cep_cidade_nasc_w,
    nr_prontuario_w,
    cd_religiao_w,
    nr_pis_pasep_w,
    cd_nacionalidade_w,
    ie_dependencia_sus_w,
    qt_altura_cm_w,
    ie_tipo_sangue_w,
    ie_fator_rh_w,
    dt_nascimento_w,
    dt_obito_w,
    ie_sexo_w,
    nr_iss_w,
    ie_estado_civil_w,
    nr_inss_w,
    nr_cpf_w,
    nr_cert_nasc_w,
    cd_cargo_w,
    nr_cert_casamento_w,
    ds_codigo_prof_w,
    cd_empresa_w,
    ie_funcionario_w,
    nr_seq_cor_pele_w,
    ds_orgao_emissor_ci_w,
    nr_cartao_nac_sus_w,
    cd_cbo_sus_w,
    cd_atividade_sus_w,
    ie_vinculo_sus_w,
    cd_sistema_ant_w,
    ie_frequenta_escola_w,
    cd_funcionario_w,
    nr_pager_bip_w,
    nr_transacao_sus_w,
    cd_medico_w,
    ie_tipo_prontuario_w,
    dt_emissao_ci_w,
    nr_seq_conselho_w,
    dt_admissao_hosp_w,
    ie_fluencia_portugues_w,
    nr_titulo_eleitor_w,
    nr_zona_w,
    nr_secao_w,
    nr_cartao_estrangeiro_w,
    nr_reg_geral_estrang_w,
    dt_chegada_brasil_w,
    sg_emissora_ci_w,
    dt_naturalizacao_pf_w,
    nr_ctps_w,
    nr_serie_ctps_w,
    uf_emissora_ctps_w,
    dt_emissao_ctps_w,
    nr_portaria_nat_w,
    nr_seq_cartorio_nasc_w,
    nr_seq_cartorio_casamento_w,
    dt_emissao_cert_nasc_w,
    dt_emissao_cert_casamento_w,
    nr_livro_cert_nasc_w,
    nr_livro_cert_casamento_w,
    dt_cadastro_original_w,
    nr_folha_cert_nasc_w,
    nr_folha_cert_casamento_w,
    nr_same_w,
    ds_observacao_w,
    qt_dependente_w,
    nr_transplante_w,
    dt_validade_rg_w,
    dt_revisao_w,
    dt_demissao_hosp_w,
    cd_puericultura_w,
    cd_cnes_w,
    ds_apelido_w,
    ie_endereco_correspondencia_w,
    qt_peso_nasc_w,
    uf_conselho_w,
    nm_abreviado_w,
    nr_ccm_w,
    dt_fim_experiencia_w,
    dt_validade_conselho_w,
    ds_profissao_w,
    ds_empresa_pf_w,
    nr_passaporte_w,
    nr_cnh_w,
    nr_cert_militar_w,
    nr_pront_ext_w,
    ie_escolaridade_cns_w,
    ie_situacao_conj_cns_w,
    nr_folha_cert_div_w,
    nr_cert_divorcio_w,
    nr_seq_cartorio_divorcio_w,
    dt_emissao_cert_divorcio_w,
    nr_livro_cert_divorcio_w,
    dt_alta_institucional_w,
    dt_transplante_w,
    nr_matricula_nasc_w,
    ie_doador_w,
    dt_vencimento_cnh_w,
    ds_categoria_cnh_w
  from  pessoa_fisica
  where  cd_pessoa_fisica = :new.cd_pessoa_fisica;


  envia_sup_int_pf(
    :new.cd_pessoa_fisica,
    nr_identidade_w,
    nm_pessoa_fisica_w,
    nr_telefone_celular_w,
    ie_grau_instrucao_w,
    nr_cep_cidade_nasc_w,
    nr_prontuario_w,
    :new.nm_usuario,
    cd_religiao_w,
    nr_pis_pasep_w,
    cd_nacionalidade_w,
    ie_dependencia_sus_w,
    qt_altura_cm_w,
    ie_tipo_sangue_w,
    ie_fator_rh_w,
    dt_nascimento_w,
    dt_obito_w,
    ie_sexo_w,
    nr_iss_w,
    ie_estado_civil_w,
    nr_inss_w,
    nr_cpf_w,
    nr_cert_nasc_w,
    cd_cargo_w,
    nr_cert_casamento_w,
    ds_codigo_prof_w,
    cd_empresa_w,
    ie_funcionario_w,
    nr_seq_cor_pele_w,
    ds_orgao_emissor_ci_w,
    nr_cartao_nac_sus_w,
    cd_cbo_sus_w,
    cd_atividade_sus_w,
    ie_vinculo_sus_w,
    cd_sistema_ant_w,
    ie_frequenta_escola_w,
    cd_funcionario_w,
    nr_pager_bip_w,
    nr_transacao_sus_w,
    cd_medico_w,
    ie_tipo_prontuario_w,
    dt_emissao_ci_w,
    nr_seq_conselho_w,
    dt_admissao_hosp_w,
    ie_fluencia_portugues_w,
    nr_titulo_eleitor_w,
    nr_zona_w,
    nr_secao_w,
    nr_cartao_estrangeiro_w,
    nr_reg_geral_estrang_w,
    dt_chegada_brasil_w,
    sg_emissora_ci_w,
    dt_naturalizacao_pf_w,
    nr_ctps_w,
    nr_serie_ctps_w,
    uf_emissora_ctps_w,
    dt_emissao_ctps_w,
    nr_portaria_nat_w,
    nr_seq_cartorio_nasc_w,
    nr_seq_cartorio_casamento_w,
    dt_emissao_cert_nasc_w,
    dt_emissao_cert_casamento_w,
    nr_livro_cert_nasc_w,
    nr_livro_cert_casamento_w,
    dt_cadastro_original_w,
    nr_folha_cert_nasc_w,
    nr_folha_cert_casamento_w,
    nr_same_w,
    ds_observacao_w,
    qt_dependente_w,
    nr_transplante_w,
    dt_validade_rg_w,
    dt_revisao_w,
    dt_demissao_hosp_w,
    cd_puericultura_w,
    cd_cnes_w,
    ds_apelido_w,
    ie_endereco_correspondencia_w,
    qt_peso_nasc_w,
    uf_conselho_w,
    nm_abreviado_w,
    nr_ccm_w,
    dt_fim_experiencia_w,
    dt_validade_conselho_w,
    ds_profissao_w,
    ds_empresa_pf_w,
    nr_passaporte_w,
    nr_cnh_w,
    nr_cert_militar_w,
    nr_pront_ext_w,
    ie_escolaridade_cns_w,
    ie_situacao_conj_cns_w,
    nr_folha_cert_div_w,
    nr_cert_divorcio_w,
    nr_seq_cartorio_divorcio_w,
    dt_emissao_cert_divorcio_w,
    nr_livro_cert_divorcio_w,
    dt_alta_institucional_w,
    dt_transplante_w,
    nr_matricula_nasc_w,
    ie_doador_w,
    dt_vencimento_cnh_w,
    ds_categoria_cnh_w,

    :new.nm_contato,
    :new.ds_endereco,
    :new.cd_cep,
    :new.nr_endereco,
    :new.ds_complemento,
    :new.ds_municipio,
    substr(:new.ds_bairro,1,40),
    :new.sg_estado,
    :new.nr_telefone,
    :new.nr_ramal,
    :new.ds_fone_adic,
    :new.ds_email,
    :new.cd_profissao,
    :new.cd_empresa_refer,
    :new.ds_setor_trabalho,
    :new.ds_horario_trabalho,
    :new.nr_matricula_trabalho,
    :new.cd_municipio_ibge,
    :new.ds_fax,
    :new.cd_tipo_logradouro,
    :new.nr_ddd_telefone,
    :new.nr_ddi_telefone,
    :new.nr_ddi_fax,
    :new.nr_ddd_fax,
    :new.ds_website,
    :new.nm_contato_pesquisa,
    :new.ie_tipo_complemento);
end if;

reg_integracao_p.ie_operacao    :=  'A';
reg_integracao_p.nr_prontuario       :=   nr_prontuario_w;
reg_integracao_p.cd_pessoa_fisica       :=   :new.cd_pessoa_fisica;
reg_integracao_p.ie_funcionario    :=  nvl(ie_funcionario_w,'N');
reg_integracao_p.nr_seq_conselho  :=  nr_seq_conselho_w;

SELECT decode(COUNT(*), 0, 'N', 'S')
  INTO reg_integracao_p.ie_pessoa_atend
  FROM pessoa_fisica_aux
 WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
   AND nr_primeiro_atend IS NOT NULL;

if nr_cpf_w is not null then
   reg_integracao_p.ie_possui_cpf := 'S';
end if;

if  (wheb_usuario_pck.get_ie_lote_contabil = 'N') then
  gerar_int_padrao.gravar_integracao('12', :new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

if  (ie_funcionario_w = 'S') then
  gerar_int_padrao.gravar_integracao('304',:new.cd_pessoa_fisica,:new.nm_usuario, reg_integracao_p);
end if;

send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.COMPL_PESSOA_FISICA_SIB_INSERT
AFTER INSERT OR UPDATE ON TASY.COMPL_PESSOA_FISICA FOR EACH ROW
DECLARE

qt_beneficiarios_plano_w	number(10);
cd_estabelecimento_w		number(10);

/* IE_ATRIBUTO :

9   - DS_ENDERECO
10 - NR_ENDERECO
11 - DS_BAIRRO
12 - CD_MUNICIPIO_IBGE
13 - SG_ESTADO
14 - CD_CEP
15 - CD_PESSOA_FISICA_REF
16 - NM_CONTATO

*/

begin

qt_beneficiarios_plano_w	:= 0;
cd_estabelecimento_w		:= wheb_usuario_pck.get_cd_estabelecimento;

--if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then -- OS 1250067 / Unimed Sao Jose do Rio Preto
	if	(:old.nm_usuario is not null) and (cd_estabelecimento_w is not null) then

		select	count(1)
		into	qt_beneficiarios_plano_w
		from	pls_segurado
		where	cd_pessoa_fisica	= :old.cd_pessoa_fisica
		and	ie_tipo_segurado	in ('A','B','R');

		if	(qt_beneficiarios_plano_w > 0) then
			if	(:old.ie_tipo_complemento in (1,2)) then -- 1 = Residencial / 2 = Comercial
				--Endereco
				if	(nvl(:old.ds_endereco,'0') <> nvl(:new.ds_endereco,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'9');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--Numero do endereco
				if	(nvl(:old.nr_endereco,'0') <> nvl(:new.nr_endereco,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'10');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--Bairro
				if	(nvl(:old.ds_bairro,'0') <> nvl(:new.ds_bairro,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'11');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--Codigo de IBGE
				if	(nvl(:old.cd_municipio_ibge,'0') <> nvl(:new.cd_municipio_ibge,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'12');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--Estado UF
				if	(nvl(:old.sg_estado,'0') <> nvl(:new.sg_estado,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'13');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--CEP
				if	(nvl(:old.cd_cep,'0') <> nvl(:new.cd_cep,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'14');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--Complemento
				if	(nvl(:old.ds_complemento,'0') <> nvl(:new.ds_complemento,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'17');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
			elsif	(:old.ie_tipo_complemento = 5) then -- 5 = da Mae
				--Pessoa responsavel
				if	(nvl(:old.cd_pessoa_fisica_ref,0) <> nvl(:new.cd_pessoa_fisica_ref,0)) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'15');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
				--Contato
				if	(nvl(:old.nm_contato,'0') <> nvl(:new.nm_contato,'0')) then
					begin
					insert into PLS_PESSOA_FISICA_SIB
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							cd_estabelecimento,cd_pessoa_fisica,dt_ocorrencia_sib,ie_atributo)
					values	(	pls_pessoa_fisica_sib_seq.nextval,sysdate,:new.nm_usuario,sysdate,:new.nm_usuario,
							cd_estabelecimento_w,:old.cd_pessoa_fisica,ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate),'16');
					exception
					when others then
						cd_estabelecimento_w := cd_estabelecimento_w;
					end;
				end if;
			end if;
		end if;
	end if;
--end if;

end;
/


ALTER TABLE TASY.COMPL_PESSOA_FISICA ADD (
  CONSTRAINT COMPEFI_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          29M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT COMPEFI_UK
 UNIQUE (CD_PESSOA_FISICA, IE_TIPO_COMPLEMENTO, NR_SEQ_TIPO_COMPL_ADIC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMPL_PESSOA_FISICA ADD (
  CONSTRAINT COMPEFI_CATLOL_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO_MX) 
 REFERENCES TASY.CAT_LOCALIDADE (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_TIPOCUST_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CUSTODIA) 
 REFERENCES TASY.TIPO_CUSTODIA (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT COMPEFI_CATIASS_FK 
 FOREIGN KEY (NR_SEQ_ASSENTAMENTO_MX) 
 REFERENCES TASY.CAT_TIPO_ASSENTAMENTO (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_TIPCOAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMPL_ADIC) 
 REFERENCES TASY.TIPO_COMPLEMENTO_ADICIONAL (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REFER) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT COMPEFI_PROFISS_FK 
 FOREIGN KEY (CD_PROFISSAO) 
 REFERENCES TASY.PROFISSAO (CD_PROFISSAO),
  CONSTRAINT COMPEFI_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT COMPEFI_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT COMPEFI_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA_REF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COMPEFI_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_SUSTILO_FK 
 FOREIGN KEY (CD_TIPO_LOGRADOURO) 
 REFERENCES TASY.SUS_TIPO_LOGRADOURO (CD_TIPO_LOGRADOURO),
  CONSTRAINT COMPEFI_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_LOCATME_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_ATEND_MED) 
 REFERENCES TASY.LOCAL_ATENDIMENTO_MEDICO (NR_SEQUENCIA),
  CONSTRAINT COMPEFI_COMPEFI_FK 
 FOREIGN KEY (CD_PESSOA_END_REF, NR_SEQ_END_REF) 
 REFERENCES TASY.COMPL_PESSOA_FISICA (CD_PESSOA_FISICA,NR_SEQUENCIA),
  CONSTRAINT COMPEFI_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COMPL_PESSOA_FISICA TO NIVEL_1;

GRANT INSERT, SELECT ON TASY.COMPL_PESSOA_FISICA TO ROBOCMTECNOLOGIA;

GRANT INSERT, SELECT, UPDATE ON TASY.COMPL_PESSOA_FISICA TO SAOJOSE;

