ALTER TABLE TASY.MED_PAC_SINAL_VITAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_PAC_SINAL_VITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_PAC_SINAL_VITAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CLIENTE         NUMBER(10)             NOT NULL,
  NR_SEQ_AVAL            NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_CONSCIENCIA         VARCHAR2(1 BYTE)       NOT NULL,
  QT_TEMP                NUMBER(4,1),
  QT_ALTURA_CM           NUMBER(5,2),
  QT_PESO                NUMBER(6,3),
  QT_IMC                 NUMBER(3,1),
  QT_SUPERF_CORPORIA     NUMBER(15,2),
  IE_OBESO               VARCHAR2(1 BYTE)       NOT NULL,
  IE_POSICAO             VARCHAR2(1 BYTE)       NOT NULL,
  IE_BRACO               VARCHAR2(1 BYTE),
  QT_PA_SISTOLICA        NUMBER(3),
  QT_PA_DIASTOLICA       NUMBER(3),
  QT_FREQ_CARDIACA       NUMBER(3),
  IE_BRACO_SEG           VARCHAR2(1 BYTE),
  QT_PA_SISTOLICA_SEG    NUMBER(3),
  QT_PA_DIASTOLICA_SEG   NUMBER(3),
  QT_FREQ_CARDIACA_SEG   NUMBER(3),
  QT_FREQ_RESP           NUMBER(3),
  IE_RITMO_FR            VARCHAR2(1 BYTE),
  IE_RITMO_PULSO_RADIAL  VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  IE_PACIENTE_ACAMADO    VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDPASV_MEDAVCP_FK_I ON TASY.MED_PAC_SINAL_VITAL
(NR_SEQ_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDPASV_MEDAVCP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDPASV_MEDCLIE_FK_I ON TASY.MED_PAC_SINAL_VITAL
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDPASV_MEDCLIE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDPASV_PK ON TASY.MED_PAC_SINAL_VITAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDPASV_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_PAC_SINAL_VITAL ADD (
  CONSTRAINT MEDPASV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_PAC_SINAL_VITAL ADD (
  CONSTRAINT MEDPASV_MEDAVCP_FK 
 FOREIGN KEY (NR_SEQ_AVAL) 
 REFERENCES TASY.MED_AVAL_COMPLETA_PAC (NR_SEQUENCIA),
  CONSTRAINT MEDPASV_MEDCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MED_CLIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_PAC_SINAL_VITAL TO NIVEL_1;

