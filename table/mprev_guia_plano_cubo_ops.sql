ALTER TABLE TASY.MPREV_GUIA_PLANO_CUBO_OPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_GUIA_PLANO_CUBO_OPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_GUIA_PLANO_CUBO_OPS
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  IE_TIPO_GUIA             VARCHAR2(2 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_BENEF_CUBO        NUMBER(10)           NOT NULL,
  IE_CARATER_INTERNACAO    VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_ATENDIMENTO  NUMBER(10),
  CD_ESPECIALIDADE         NUMBER(5),
  NR_SEQ_CLINICA           NUMBER(10),
  DT_ATENDIMENTO           DATE                 NOT NULL,
  DT_MES_COMPETENCIA       DATE                 NOT NULL,
  DS_ESPECIALIDADE_SOLIC   VARCHAR2(255 BYTE),
  DS_TIPO_INTERNACAO       VARCHAR2(255 BYTE),
  DS_TIPO_ATENDIMENTO      VARCHAR2(255 BYTE),
  CD_GUIA                  VARCHAR2(20 BYTE),
  CD_GUIA_PRINCIPAL        VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRGPOP_MPRBECO_FK_I ON TASY.MPREV_GUIA_PLANO_CUBO_OPS
(NR_SEQ_BENEF_CUBO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRGPOP_PK ON TASY.MPREV_GUIA_PLANO_CUBO_OPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_GUIA_PLANO_CUBO_OPS ADD (
  CONSTRAINT MPRGPOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_GUIA_PLANO_CUBO_OPS ADD (
  CONSTRAINT MPRGPOP_MPRBECO_FK 
 FOREIGN KEY (NR_SEQ_BENEF_CUBO) 
 REFERENCES TASY.MPREV_BENEF_CUBO_OPS (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_GUIA_PLANO_CUBO_OPS TO NIVEL_1;

