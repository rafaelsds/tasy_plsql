ALTER TABLE TASY.OFT_RITUAL_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_RITUAL_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_RITUAL_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RITUAL        NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTRIIT_OFTAITE_FK_I ON TASY.OFT_RITUAL_ITEM
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTRIIT_OFTRITU_FK_I ON TASY.OFT_RITUAL_ITEM
(NR_SEQ_RITUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTRIIT_OFTRITU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.OFTRIIT_PK ON TASY.OFT_RITUAL_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTRIIT_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.OFTRIIT_UK ON TASY.OFT_RITUAL_ITEM
(NR_SEQ_RITUAL, NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTRIIT_UK
  MONITORING USAGE;


ALTER TABLE TASY.OFT_RITUAL_ITEM ADD (
  CONSTRAINT OFTRIIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT OFTRIIT_UK
 UNIQUE (NR_SEQ_RITUAL, NR_SEQ_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_RITUAL_ITEM ADD (
  CONSTRAINT OFTRIIT_OFTAITE_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.OFTALMOLOGIA_ITEM (NR_SEQUENCIA),
  CONSTRAINT OFTRIIT_OFTRITU_FK 
 FOREIGN KEY (NR_SEQ_RITUAL) 
 REFERENCES TASY.OFT_RITUAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.OFT_RITUAL_ITEM TO NIVEL_1;

