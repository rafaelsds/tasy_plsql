ALTER TABLE TASY.PLS_PROTOCOLO_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROTOCOLO_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROTOCOLO_CONTA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_PROTOCOLO_PRESTADOR    VARCHAR2(20 BYTE),
  NR_SEQ_PRESTADOR          NUMBER(10),
  DT_PROTOCOLO              DATE,
  IE_STATUS                 VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_PRESTADOR_IMP      VARCHAR2(20 BYTE),
  DT_PROTOCOLO_IMP          DATE,
  NM_PRESTADOR_IMP          VARCHAR2(255 BYTE),
  IE_SITUACAO               VARCHAR2(3 BYTE)    NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_CGC_PRESTADOR_IMP      VARCHAR2(14 BYTE),
  NR_CPF_PRESTADOR_IMP      VARCHAR2(11 BYTE),
  DT_MES_COMPETENCIA        DATE                NOT NULL,
  DT_BASE_VENC              DATE,
  CD_CONDICAO_PAGAMENTO     NUMBER(10),
  DT_INTEGRACAO             DATE,
  NR_SEQ_TRANSACAO_NUMBER   NUMBER(12),
  DS_HASH                   VARCHAR2(255 BYTE),
  NR_SEQ_OUTORGANTE         NUMBER(10),
  NM_USUARIO_INTEGRACAO     VARCHAR2(15 BYTE),
  IE_TIPO_GUIA              VARCHAR2(2 BYTE),
  IE_FORMA_IMP              VARCHAR2(1 BYTE),
  NR_LOTE_CONTABIL          NUMBER(10),
  CD_PROFISSIONAL_EXECUTOR  VARCHAR2(10 BYTE),
  NR_SEQ_GRAU_PARTIC        NUMBER(10),
  NR_SEQ_PROT_REFERENCIA    NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  NR_SEQ_LOTE               NUMBER(10),
  NR_SEQ_RESUMO_LOTE        NUMBER(10),
  IE_TIPO_PROTOCOLO         VARCHAR2(3 BYTE)    NOT NULL,
  IE_APRESENTACAO           VARCHAR2(3 BYTE),
  NR_SEQ_SEGURADO           NUMBER(10),
  IE_TIPO_RESP_CREDITO      VARCHAR2(2 BYTE),
  IE_FORMA_PAGAMENTO        VARCHAR2(2 BYTE),
  CD_BANCO                  NUMBER(5),
  NR_SEQ_CONTA_BANCO        NUMBER(10),
  CD_AGENCIA_BANCARIA       VARCHAR2(8 BYTE),
  IE_DIGITO_AGENCIA         VARCHAR2(1 BYTE),
  CD_CONTA                  VARCHAR2(20 BYTE),
  IE_DIGITO_CONTA           VARCHAR2(2 BYTE),
  CD_VERSAO                 VARCHAR2(70 BYTE),
  DS_APLICATIVO             VARCHAR2(70 BYTE),
  DS_FABRICANTE             VARCHAR2(70 BYTE),
  NR_SEQ_LOTE_CONTA         NUMBER(10),
  QT_CONTAS_INFORMADAS      NUMBER(10),
  NR_SEQ_CONGENERE          NUMBER(10),
  IE_PROTOCOLO_COMPL        VARCHAR2(1 BYTE),
  VL_COBRADO                NUMBER(15,2),
  VL_COPARTICIPACAO         NUMBER(15,2),
  VL_GLOSA                  NUMBER(15,2),
  VL_TOTAL                  NUMBER(15,2),
  VL_TOTAL_BENEFICIARIO     NUMBER(15,2),
  VL_TOTAL_IMP              NUMBER(15,2),
  VL_CONTABIL               NUMBER(15,2),
  VL_PENDENTE               NUMBER(15,2),
  VL_LIB_SISTEMA            NUMBER(15,2),
  VL_LIB_USUARIO            NUMBER(15,2),
  QT_OCORRENCIAS            NUMBER(10),
  CD_PRESTADOR_IMP          VARCHAR2(30 BYTE),
  CD_PRESTADOR              VARCHAR2(30 BYTE),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  DT_LIB_PAGAMENTO          DATE,
  NR_SEQ_PERIODO_PGTO       NUMBER(10),
  DT_FECHAMENTO_CONTAS      DATE,
  NR_SEQ_MOT_REEMBOLSO      NUMBER(10),
  VL_COBRADO_MANUAL         NUMBER(15,2),
  CD_VERSAO_TISS            VARCHAR2(20 BYTE),
  IE_GUIA_FISICA            VARCHAR2(1 BYTE),
  NR_SEQ_USU_PRESTADOR      NUMBER(10),
  IE_ORIGEM_PROTOCOLO       VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_CANCEL      NUMBER(10),
  NR_LOTE_CONTABIL_PROV     NUMBER(10),
  NR_LOTE_PROV_COPARTIC     NUMBER(10),
  NR_LOTE_CONTAB_PAG        NUMBER(10),
  NR_SEQ_LOTE_APRES_AUTOM   NUMBER(10),
  DT_RECEBIMENTO            DATE,
  DT_FIM_ANALISE            DATE,
  NR_SEQ_XML_ARQUIVO        NUMBER(10),
  IE_TIPO_IMPORTACAO        VARCHAR2(2 BYTE),
  DT_CANCELAMENTO           DATE,
  NR_SEQ_SERV_PRE_PAGTO     NUMBER(10),
  CD_SISTEMA_ANT            VARCHAR2(255 BYTE),
  DS_MOTIVO_CANC            VARCHAR2(4000 BYTE),
  NR_SEQ_LOTE_REEMB_CRED    NUMBER(10),
  NR_SEQ_PRESTADOR_IMP_REF  NUMBER(10),
  NR_SEQ_TRANSACAO          VARCHAR2(12 BYTE),
  NR_SEQ_IMP                NUMBER(10),
  CD_MEDICO_SOLICITANTE     VARCHAR2(10 BYTE),
  NR_SEQ_ATEND_CALL         NUMBER(10),
  DS_ARQUIVO_PORTAL         VARCHAR2(255 BYTE),
  IE_REFATURAMENTO          VARCHAR2(2 BYTE),
  IE_TIPO_ATENDIMENTO       VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_PLS          NUMBER(10),
  DS_INF_ADIC_REEMBOLSO     VARCHAR2(4000 BYTE),
  IE_FORNEC_DIRETO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRCO_BANCO_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_BANESTA_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_CONPAGA_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_ESTABEL_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I1 ON TASY.PLS_PROTOCOLO_CONTA
(DT_MES_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I2 ON TASY.PLS_PROTOCOLO_CONTA
(IE_SITUACAO, DT_MES_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I3 ON TASY.PLS_PROTOCOLO_CONTA
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I4 ON TASY.PLS_PROTOCOLO_CONTA
(IE_SITUACAO, CD_ESTABELECIMENTO, IE_STATUS, DT_MES_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I5 ON TASY.PLS_PROTOCOLO_CONTA
(DT_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I6 ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_LOTE_CONTA, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_I7 ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQUENCIA, IE_STATUS, IE_TIPO_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_LOTCONT_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_LOTE_CONTABIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_LOTCONT_FK4_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_LOTE_CONTAB_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_LOTCONT_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_LOTCONT_FK5_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_LOTE_CONTABIL_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_LOTCONT_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_LOTCONT_FK6_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_LOTE_PROV_COPARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_LOTCONT_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_MEDICO_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(CD_MEDICO_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PESFISI_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(CD_PROFISSIONAL_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PESFISI_FK2_I ON TASY.PLS_PROTOCOLO_CONTA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRCO_PK ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSATEN_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_ATEND_CALL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSATEN_FK2_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_ATEND_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSCONG_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSGRPA_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSGRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSLAAU_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_LOTE_APRES_AUTOM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSLAAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSLOPC_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_LOTE_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSLOPO_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSLOPT_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_RESUMO_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSLOPT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSLRCR_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_LOTE_REEMB_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSMOCC_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSMORE_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_MOT_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSMORE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSOUTO_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_OUTORGANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSOUTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSPEPA_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_PERIODO_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSPEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PLSPRCO_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_PROT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSPRES_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSPRES_FK3_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_PRESTADOR_IMP_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSSEGU_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PLSXMAR_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_XML_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PLSXMAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRCO_PPCTIMP_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRCO_PTUSEPP_FK_I ON TASY.PLS_PROTOCOLO_CONTA
(NR_SEQ_SERV_PRE_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRCO_PTUSEPP_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_protocolo_conta_alt
before update ON TASY.PLS_PROTOCOLO_CONTA for each row
declare

ds_log_call_w			Varchar2(1500);
ds_observacao_w			Varchar2(4000);

begin
/* Permitir desfazer contas originadas do A700 - ebcabral - OS 609162 - 03/02/2014
if	(:old.ie_status != :new.ie_status) and
	(:old.ie_status	= '7') then
	wheb_mensagem_pck.exibir_mensagem_abort(250624);
end if; */
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S')  then
		if	(( pls_se_aplicacao_tasy = 'N') and
			(nvl(wheb_usuario_pck.get_nm_usuario,'X') = 'X')) or
			(:new.ie_tipo_protocolo is null) then

			ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
			ds_observacao_w := 'Protocolo: '||:old.nr_sequencia;

			if	(nvl(:old.nr_seq_prestador,0) <> nvl(:new.nr_seq_prestador,0)) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Prestador: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_prestador||' - Modificada: '||:new.nr_seq_prestador||chr(13)||chr(10);
			end if;

			if	(nvl(:old.ie_tipo_protocolo,'X') <> nvl(:new.ie_tipo_protocolo,'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Tipo protocolo: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_tipo_protocolo||' - Modificada: '||:new.ie_tipo_protocolo||chr(13)||chr(10);
			end if;

			if	(nvl(:old.nr_seq_lote,0) <> nvl(:new.nr_seq_lote,0)) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Lote: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_lote||' - Modificada: '||:new.nr_seq_lote||chr(13)||chr(10);
			end if;

			if	(nvl(:old.nr_seq_lote_conta,0) <> nvl(:new.nr_seq_lote_conta,0)) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Lote de contas: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_lote_conta||' - Modificada: '||:new.nr_seq_lote_conta||chr(13)||chr(10);
			end if;

			if	(nvl(:old.ie_status,'X') <> nvl(:new.ie_status,'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Status: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_status||' - Modificada: '||:new.ie_status||chr(13)||chr(10);
			end if;

			if	(nvl(:old.ie_situacao,'X') <> nvl(:new.ie_situacao,'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Situa��o: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_situacao||' - Modificada: '||:new.ie_situacao||chr(13)||chr(10);
			end if;

			if	(nvl(to_char(:old.dt_protocolo),'X') <> nvl(to_char(:new.dt_protocolo),'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Dt. protocolo: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||to_char(:old.dt_protocolo, 'dd/mm/yyyy')||' - Modificada: '||to_char(:new.dt_protocolo, 'dd/mm/yyyy')||chr(13)||chr(10);
			end if;

			if	(nvl(to_char(:old.dt_recebimento),'X') <> nvl(to_char(:new.dt_recebimento),'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Dt. recebimento: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||to_char(:old.dt_recebimento, 'dd/mm/yyyy')||' - Modificada: '||to_char(:new.dt_recebimento, 'dd/mm/yyyy')||chr(13)||chr(10);
			end if;

			if	(nvl(to_char(:old.dt_mes_competencia),'X') <> nvl(to_char(:new.dt_mes_competencia),'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Dt. m�s compet�ncia: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||to_char(:old.dt_mes_competencia, 'dd/mm/yyyy')||' - Modificada: '||to_char(:new.dt_mes_competencia, 'dd/mm/yyyy')||chr(13)||chr(10);
			end if;

			insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
								dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
								ds_log, ds_log_call, ds_funcao_ativa,
								ie_aplicacao_tasy, nm_maquina, nr_seq_protocolo, ie_opcao )
					values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
								sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROTOCOLO_CONTA',
								ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
								pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, :old.nr_sequencia, '0');
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_protocolo_conta_update
before update ON TASY.PLS_PROTOCOLO_CONTA for each row
declare

qt_titulo_w			number(10);
vl_saldo_titulos_w		number(15,2);
ds_log_call_w			varchar2(1500);
ds_observacao_w			varchar2(4000);
ds_string_redundancia_w		pls_conta_proc.ds_redundancia%type;
qt_sem_tit_w			pls_integer;

cursor C01 (	nr_seq_protocolo_pc	pls_protocolo_conta.nr_sequencia%type) is
	select	a.dt_atendimento_imp_referencia,
		a.dt_atendimento_referencia,
		a.nr_seq_prestador_exec,
		a.nr_sequencia nr_seq_conta
	from	pls_conta a
	where	a.nr_seq_protocolo = nr_seq_protocolo_pc;

begin

if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then

	if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S')  then

		if	(:new.ie_tipo_protocolo = 'R') and
			(:new.nr_seq_segurado <> :old.nr_seq_segurado) then

			--aaschlote 22/12/2014 OS 824746 - Tratamento para n�o dar erro quando chamar a trigger PLS_CONTA_ATUAL
			wheb_usuario_pck.set_ie_executar_trigger('N');
			update	pls_conta
			set	nr_seq_segurado = :new.nr_seq_segurado
			where	nr_seq_protocolo = :new.nr_sequencia;
			wheb_usuario_pck.set_ie_executar_trigger('S');
		end if;

		if	(:new.dt_integracao is not null) and
			(:old.dt_integracao is null) and
			(:new.ie_situacao = 'RE') then

			wheb_mensagem_pck.exibir_mensagem_abort(273258);
		end if;

		/*Consist�ncia para verificar se existe t�tulos j� quitados para o protocolo, este caso n�o pode ser alterado o status dos itens*/
		if	(:old.ie_status = '6') and
			(:old.ie_status <> :new.ie_status) then

			select	sum(qt)
			into	qt_titulo_w
			from	(
				select	count(1) qt
				from	pls_pag_prest_vencimento a,
					pls_pagamento_prestador b,
					pls_conta_medica_resumo c
				where	a.nr_seq_pag_prestador = b.nr_sequencia
				and	c.nr_seq_prestador_pgto = b.nr_seq_prestador
				and	b.nr_seq_lote = c.nr_seq_lote_pgto
				and	c.nr_seq_protocolo = :new.nr_sequencia
				and	a.nr_titulo is not null
				and	c.ie_situacao = 'A'
				and	rownum = 1
				union all
				select	count(1) qt
				from	pls_conta_medica_resumo a,
					pls_pp_prestador b
				where	a.nr_seq_protocolo = :new.nr_sequencia
				and	a.ie_situacao = 'A'
				and	b.nr_seq_lote = a.nr_seq_pp_lote
				and	b.nr_seq_prestador = a.nr_seq_prestador_pgto
				and	b.nr_titulo_pagar is not null
				and	rownum = 1);

			if	(qt_titulo_w > 0) then

				select	sum(vl_saldo)
				into	vl_saldo_titulos_w
				from	(
					select	nvl(sum(decode(d.ie_situacao, 'C', 1, d.vl_saldo_titulo)),0) vl_saldo	-- Edgar 03/05/2014, OS 743393, coloquei o decode "1", para considerar que os t�tulos cancelados tem saldo, para n�o entrar na consist�ncia abaixo
					from	pls_pag_prest_vencimento a,
						pls_pagamento_prestador b,
						pls_conta_medica_resumo c,
						titulo_pagar d
					where	a.nr_seq_pag_prestador	= b.nr_sequencia
					and	c.nr_seq_prestador_pgto	= b.nr_seq_prestador
					and	a.nr_titulo = d.nr_titulo
					and	b.nr_seq_lote = c.nr_seq_lote_pgto
					and	c.nr_seq_protocolo = :new.nr_sequencia
					and	c.ie_situacao = 'A'
					union all
					select	sum(decode(c.ie_situacao, 'C', 1, nvl(c.vl_saldo_titulo,0))) vl_saldo
					from	pls_conta_medica_resumo a,
						pls_pp_prestador b,
						titulo_pagar c
					where	a.nr_seq_protocolo = :new.nr_sequencia
					and	a.ie_situacao = 'A'
					and	b.nr_seq_lote = a.nr_seq_pp_lote
					and	b.nr_seq_prestador = a.nr_seq_prestador_pgto
					and	c.nr_titulo = b.nr_titulo_pagar);

				-- tratamento criado para quando n�o houver titulo ainda gerado para os itens
				-- alterei este tratamento para verificar se os lotes j� est�o todos com data de gera��o dos t�tulos
				-- pois n�o necess�riamente um prestador ir� ter um titulo a pagar, pode ser que ele tenha ficado com valores
				-- negativos e estes valores foram apropriados ou gerado um t�tulo a receber
				select	sum(qt)
				into	qt_sem_tit_w
				from	(
					select	count(1) qt
					from	pls_conta_medica_resumo a,
						pls_lote_pagamento b
					where	a.nr_seq_protocolo = :new.nr_sequencia
					and	a.ie_situacao = 'A'
					and	b.nr_sequencia = a.nr_seq_lote_pgto
					and	b.dt_geracao_titulos is null
					and	rownum = 1
					union all
					select	count(1) qt
					from	pls_conta_medica_resumo a,
						pls_pp_lote b
					where	a.nr_seq_protocolo = :new.nr_sequencia
					and	a.ie_situacao = 'A'
					and	b.nr_sequencia = a.nr_seq_pp_lote
					and	b.dt_geracao_titulo is null
					and	rownum = 1);

				if	(vl_saldo_titulos_w = 0) and
					(qt_sem_tit_w = 0) then

					--'Os t�tulos do protocolo est�o liquidados n�o � poss�vel alterar o status do mesmo !  #@#@');
					wheb_mensagem_pck.exibir_mensagem_abort(226722);
				end if;
			end if;
		end if;

		if	(pls_se_aplicacao_tasy = 'N') then

			ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);/*substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
					' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);*/
					--' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
			ds_observacao_w := 'Protocolo: '||:old.nr_sequencia;

			if	(nvl(:old.nr_seq_prestador,0) <> nvl(:new.nr_seq_prestador,0)) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Prestador: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_prestador||' - Modificada: '||:new.nr_seq_prestador||chr(13)||chr(10);
			end if;

			if	(nvl(:old.ie_tipo_protocolo,'X') <> nvl(:new.ie_tipo_protocolo,'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Tipo protocolo: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_tipo_protocolo||' - Modificada: '||:new.ie_tipo_protocolo||chr(13)||chr(10);
			end if;

			if	(nvl(:old.nr_seq_lote,0) <> nvl(:new.nr_seq_lote,0)) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Lote: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_lote||' - Modificada: '||:new.nr_seq_lote||chr(13)||chr(10);
			end if;

			if	(nvl(:old.nr_seq_lote_conta,0) <> nvl(:new.nr_seq_lote_conta,0)) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Lote de contas: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.nr_seq_lote_conta||' - Modificada: '||:new.nr_seq_lote_conta||chr(13)||chr(10);
			end if;

			if	(nvl(:old.ie_status,'X') <> nvl(:new.ie_status,'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Status: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||:old.ie_status||' - Modificada: '||:new.ie_status||chr(13)||chr(10);
			end if;

			if	(nvl(to_char(:old.dt_protocolo),'X') <> nvl(to_char(:new.dt_protocolo),'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Dt. protocolo: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||to_char(:old.dt_protocolo, 'dd/mm/yyyy')||' - Modificada: '||to_char(:new.dt_protocolo, 'dd/mm/yyyy')||chr(13)||chr(10);
			end if;

			if	(nvl(to_char(:old.dt_recebimento),'X') <> nvl(to_char(:new.dt_recebimento),'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Dt. recebimento: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||to_char(:old.dt_recebimento, 'dd/mm/yyyy')||' - Modificada: '||to_char(:new.dt_recebimento, 'dd/mm/yyyy')||chr(13)||chr(10);
			end if;

			if	(nvl(to_char(:old.dt_mes_competencia),'X') <> nvl(to_char(:new.dt_mes_competencia),'X')) then
				ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
							'Dt. m�s compet�ncia: '||chr(13)||chr(10)||
							chr(9)||'Anterior: '||to_char(:old.dt_mes_competencia, 'dd/mm/yyyy')||' - Modificada: '||to_char(:new.dt_mes_competencia, 'dd/mm/yyyy')||chr(13)||chr(10);
			end if;

			if (:new.ie_apresentacao is null and :old.ie_apresentacao is not null) then
				ds_observacao_w := ds_observacao_w||chr(13)||chr(10)||
								chr(9)||'  sem informa��o de tipo de apresenta��o'||chr(13)||chr(10)||
								chr(9)||'Anterior: '||:old.ie_apresentacao||' - Modificada: '||:new.ie_apresentacao||chr(13)||chr(10);
			end if;

			insert into plsprco_cta(
				nr_sequencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
				ds_log, ds_log_call, ds_funcao_ativa,
				ie_aplicacao_tasy, nm_maquina, ie_opcao,
				nr_seq_protocolo
			) values (
				plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
				sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROTOCOLO_CONTA',
				ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
				'N', wheb_usuario_pck.get_machine, '0',
				:new.nr_sequencia
			);
		else
			if (:new.ie_apresentacao is null and :old.ie_apresentacao is not null) then

				ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
				ds_observacao_w := ds_observacao_w||chr(13)||chr(10)||
								chr(9)||'  sem informa��o de tipo de apresenta��o'||chr(13)||chr(10)||
								chr(9)||'Anterior: '||:old.ie_apresentacao||' - Modificada: '||:new.ie_apresentacao||chr(13)||chr(10);

				insert into plsprco_cta(
					nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
					ds_log, ds_log_call, ds_funcao_ativa,
					ie_aplicacao_tasy, nm_maquina, ie_opcao,
					nr_seq_protocolo
				) values (
					plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROTOCOLO_CONTA',
					ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
					'N', wheb_usuario_pck.get_machine, '0',
					:new.nr_sequencia
				);

			end if;
		end if;

		if	(:new.dt_integracao is not null) and
			(:new.ie_situacao = 'RE') then

			ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

			insert into plsprco_cta(
				nr_sequencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
				ds_log, ds_log_call, ds_funcao_ativa,
				ie_aplicacao_tasy, nm_maquina, ie_opcao,
				nr_seq_protocolo
			) values (
				plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
				sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROTOCOLO_CONTA',
				'Rejeitou um protocolo integrado', ds_log_call_w, obter_funcao_ativa,
				pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '0',
				:new.nr_sequencia
			);
		end if;
	end if;

	-- controle de redund�ncias para tratamento de procedimentos e materiais
	-- se for atualiza��o somente, pois no insert ainda n�o existem procedimentos ou materiais
	if	(updating) then

		-- se mudou alguma destas datas na conta significa que � necess�rio atualizar as redund�ncias que existem nos itens
		-- isso � feito por motivos de performance e principalmente para atender as regras de neg�cio de forma mais simples
		-- possibilita que exista no item a data correta do atendimento sem precisar efetuar c�lculos
		-- centraliza a regra de neg�cio da atualiza��o dos itens na trigger dos itens
		if	((nvl(:new.dt_protocolo, to_date('31/12/1899', 'DD/MM/YYYY')) != nvl(:old.dt_protocolo, to_date('31/12/1899','DD/MM/YYYY'))) or
			 (nvl(:new.dt_mes_competencia, to_date('31/12/1899', 'DD/MM/YYYY')) != nvl(:old.dt_mes_competencia, to_date('31/12/1899', 'DD/MM/YYYY')))) then

			-- seta para controlar para que partes das triggers dos itens n�o rode
			pls_util_pck.get_ie_executar_redundancia_w := 'S';

			for r_C01_w in C01(:new.nr_sequencia) loop

				ds_string_redundancia_w := 'dt_atendimento_imp_referencia=' || to_char(r_C01_w.dt_atendimento_imp_referencia) ||
							   ';dt_atendimento_referencia=' || to_char(r_C01_w.dt_atendimento_referencia) ||
							   ';nr_seq_prestador_exec=' || to_char(r_C01_w.nr_seq_prestador_exec) ||
							   ';dt_protocolo=' || to_char(:new.dt_protocolo) ||
							   ';dt_mes_competencia=' || to_char(:new.dt_mes_competencia);

				-- atualiza os procedimentos
				update	pls_conta_proc set
					ds_redundancia = ds_string_redundancia_w
				where	nr_seq_conta = r_C01_w.nr_seq_conta;

				-- atualiza os materiais
				update	pls_conta_mat set
					ds_redundancia = ds_string_redundancia_w
				where	nr_seq_conta = r_C01_w.nr_seq_conta;
			end loop;

			-- volta para o valor padr�o
			pls_util_pck.get_ie_executar_redundancia_w := 'N';
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PROTOCOLO_CONTA_tp  after update ON TASY.PLS_PROTOCOLO_CONTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_PROTOCOLO_CONTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_protocolo_conta_insert
before insert ON TASY.PLS_PROTOCOLO_CONTA for each row
declare

ds_log_call_w	varchar2(1500);
ds_observacao_w	varchar2(4000);

begin
/* A vers�o TISS vem no XML, PTU ou pode ser informada em tela,
por�m temos que garantir que caso n�o informada, busque a vers�o vigente
pois h� v�rias vis�es de telas tratadas pela vers�o TISS */
if	(:new.cd_versao_tiss is null) then
	:new.cd_versao_tiss	:= pls_obter_versao_tiss;
end if;


if (:new.ie_apresentacao is null) then

	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
	ds_observacao_w := 'Protocolo: '||:old.nr_sequencia||' sendo inserido sem informa��o de tipo de apresenta��o';

	insert into plsprco_cta(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
		ds_log, ds_log_call, ds_funcao_ativa,
		ie_aplicacao_tasy, nm_maquina, ie_opcao
	) values (
		plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
		sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROTOCOLO_CONTA',
		ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
		'N', wheb_usuario_pck.get_machine, '0'
	);

end if;

end;
/


ALTER TABLE TASY.PLS_PROTOCOLO_CONTA ADD (
  CONSTRAINT PLSPRCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROTOCOLO_CONTA ADD (
  CONSTRAINT PLSPRCO_PPCTIMP_FK 
 FOREIGN KEY (NR_SEQ_IMP) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA_IMP (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRCO_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_SOLICITANTE) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRCO_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CALL) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSATEN_FK2 
 FOREIGN KEY (NR_SEQ_ATEND_PLS) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSPRES_FK3 
 FOREIGN KEY (NR_SEQ_PRESTADOR_IMP_REF) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRCO_PTUSEPP_FK 
 FOREIGN KEY (NR_SEQ_SERV_PRE_PAGTO) 
 REFERENCES TASY.PTU_SERVICO_PRE_PAGTO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSLRCR_FK 
 FOREIGN KEY (NR_SEQ_LOTE_REEMB_CRED) 
 REFERENCES TASY.PLS_LOTE_REEMB_CREDITO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRCO_PLSPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERIODO_PGTO) 
 REFERENCES TASY.PLS_PERIODO_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSMORE_FK 
 FOREIGN KEY (NR_SEQ_MOT_REEMBOLSO) 
 REFERENCES TASY.PLS_MOTIVO_REEMBOLSO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSMOCC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.PLS_MOTIVO_CANCEL_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_LOTCONT_FK4 
 FOREIGN KEY (NR_LOTE_CONTAB_PAG) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSPRCO_PLSLAAU_FK 
 FOREIGN KEY (NR_SEQ_LOTE_APRES_AUTOM) 
 REFERENCES TASY.PLS_LOTE_APRES_AUTOMATICA (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_LOTCONT_FK5 
 FOREIGN KEY (NR_LOTE_CONTABIL_PROV) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSPRCO_LOTCONT_FK6 
 FOREIGN KEY (NR_LOTE_PROV_COPARTIC) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSPRCO_PLSXMAR_FK 
 FOREIGN KEY (NR_SEQ_XML_ARQUIVO) 
 REFERENCES TASY.PLS_XML_ARQUIVO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT PLSPRCO_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSLOPC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CONTA) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPRCO_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PLSPRCO_PLSOUTO_FK 
 FOREIGN KEY (NR_SEQ_OUTORGANTE) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSPRCO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRCO_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROT_REFERENCIA) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSLOPO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT PLSPRCO_PLSLOPT_FK 
 FOREIGN KEY (NR_SEQ_RESUMO_LOTE) 
 REFERENCES TASY.PLS_LOTE_PROT_TITULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PROTOCOLO_CONTA TO NIVEL_1;

