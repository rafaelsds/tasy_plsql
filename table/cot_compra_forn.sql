ALTER TABLE TASY.COT_COMPRA_FORN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_COMPRA_FORN CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_COMPRA_FORN
(
  NR_COT_COMPRA              NUMBER(10)         NOT NULL,
  CD_CGC_FORNECEDOR          VARCHAR2(14 BYTE),
  CD_CONDICAO_PAGAMENTO      NUMBER(10)         NOT NULL,
  CD_MOEDA                   NUMBER(5)          NOT NULL,
  IE_FRETE                   VARCHAR2(1 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  VL_PREVISTO_FRETE          NUMBER(15,2),
  NR_DOCUMENTO_FORNEC        VARCHAR2(10 BYTE),
  DT_DOCUMENTO               DATE,
  IE_TIPO_DOCUMENTO          VARCHAR2(1 BYTE),
  QT_DIAS_VALIDADE           NUMBER(5),
  QT_DIAS_ENTREGA            NUMBER(5),
  PR_DESCONTO                NUMBER(15,4),
  PR_DESCONTO_PGTO_ANTEC     NUMBER(15,4),
  PR_JUROS_NEGOCIADO         NUMBER(13,4),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_SEQUENCIA               NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  IE_STATUS                  VARCHAR2(5 BYTE),
  DT_FECHAMENTO              DATE,
  NM_USUARIO_FECHAMENTO      VARCHAR2(15 BYTE),
  VL_DESPESA_ACESSORIA       NUMBER(13,2),
  IE_GERADO_BIONEXO          VARCHAR2(1 BYTE),
  VL_DESCONTO                NUMBER(13,2),
  IE_EXCLUSIVO               VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_EXCLUSIVO    NUMBER(10),
  IE_FORMA_PAGTO             VARCHAR2(15 BYTE),
  IE_LIBERADA_INTERNET       VARCHAR2(1 BYTE),
  IE_STATUS_ENVIO_EMAIL_LIB  VARCHAR2(1 BYTE),
  VL_COTACAO_MOEDA           NUMBER(15,4),
  VL_DESPESA_DOC             NUMBER(13,2),
  NR_DOCUMENTO_EXTERNO       VARCHAR2(100 BYTE),
  DS_EMAIL_REGRA_PJ          VARCHAR2(4000 BYTE),
  DS_CONTATO_REGRA_PJ        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COCOFOR_CONPAGA_FK_I ON TASY.COT_COMPRA_FORN
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOFOR_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COCOFOR_MOEDA_FK_I ON TASY.COT_COMPRA_FORN
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOFOR_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COCOFOR_PK ON TASY.COT_COMPRA_FORN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFO_COTCOMP_FK_I ON TASY.COT_COMPRA_FORN
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFO_MOFOEXC_FK_I ON TASY.COT_COMPRA_FORN
(NR_SEQ_MOTIVO_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFO_MOFOEXC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFO_PESFISI_FK_I ON TASY.COT_COMPRA_FORN
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFO_PESJURI_FK1_I ON TASY.COT_COMPRA_FORN
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFO_PESJURI_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFO_STATUS_I ON TASY.COT_COMPRA_FORN
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFO_STATUS_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cot_compra_forn_Insert
BEFORE INSERT OR UPDATE ON TASY.COT_COMPRA_FORN FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.cd_cgc_fornecedor is not null) and
	(:new.cd_pessoa_fisica is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(278762);
end if;

if	(:new.cd_cgc_fornecedor is null) and
	(:new.cd_pessoa_fisica is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(278763);
end if;
<<Final>>
qt_reg_w	:= 0;


END;
/


CREATE OR REPLACE TRIGGER TASY.COT_COMPRA_FORN_UPDATE
BEFORE UPDATE ON TASY.COT_COMPRA_FORN FOR EACH ROW
DECLARE
qt_registro_w		number(05,0);
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
select	count(*)
into	qt_registro_w
from	cot_compra_forn_item
where	nr_seq_cot_forn = :new.nr_sequencia;

if	(qt_registro_w > 0) and
	(:new.cd_cgc_fornecedor <> :old.cd_cgc_fornecedor) then
	update	cot_compra_forn_item
	set	cd_cgc_fornecedor = :new.cd_cgc_fornecedor
	where	nr_seq_cot_forn = :new.nr_sequencia;
end if;

if	(:new.ie_status <> :old.ie_status) and
	(:new.ie_status in ('FF','FH')) then
	confirmar_cotacao_eletronica(:new.nr_cot_compra,:new.nr_sequencia);
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/


ALTER TABLE TASY.COT_COMPRA_FORN ADD (
  CONSTRAINT COCOFOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          896K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_COMPRA_FORN ADD (
  CONSTRAINT COTCOFO_PESJURI_FK1 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT COTCOFO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COTCOFO_MOFOEXC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_EXCLUSIVO) 
 REFERENCES TASY.MOTIVO_FORNEC_EXCLUSIVO (NR_SEQUENCIA),
  CONSTRAINT COTCOFO_COTCOMP_FK 
 FOREIGN KEY (NR_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA (NR_COT_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT COCOFOR_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT COCOFOR_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.COT_COMPRA_FORN TO NIVEL_1;

