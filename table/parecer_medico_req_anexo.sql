ALTER TABLE TASY.PARECER_MEDICO_REQ_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARECER_MEDICO_REQ_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARECER_MEDICO_REQ_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PAR_MED_REQ   NUMBER(10)               NOT NULL,
  DS_TITULO            VARCHAR2(255 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAMERAN_PARMERE_FK_I ON TASY.PARECER_MEDICO_REQ_ANEXO
(NR_SEQ_PAR_MED_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAMERAN_PK ON TASY.PARECER_MEDICO_REQ_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PARECER_MEDICO_REQ_ANEXO ADD (
  CONSTRAINT PAMERAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PARECER_MEDICO_REQ_ANEXO ADD (
  CONSTRAINT PAMERAN_PARMERE_FK 
 FOREIGN KEY (NR_SEQ_PAR_MED_REQ) 
 REFERENCES TASY.PARECER_MEDICO_REQ (NR_PARECER)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PARECER_MEDICO_REQ_ANEXO TO NIVEL_1;

