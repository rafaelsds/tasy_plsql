ALTER TABLE TASY.PF_CARTAO_FIDELIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_CARTAO_FIDELIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_CARTAO_FIDELIDADE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CARTAO          NUMBER(10)             NOT NULL,
  DT_INICIO_VALIDADE     DATE                   NOT NULL,
  DT_FIM_VALIDADE        DATE,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_CARTAO_FIDELIDADE   VARCHAR2(16 BYTE)      NOT NULL,
  DT_EMISSAO_CARTAO      DATE                   NOT NULL,
  CD_PESSOA_TITULAR      VARCHAR2(10 BYTE),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  CD_PESSOA_VENDEDOR     VARCHAR2(10 BYTE),
  QT_PONTUACAO_CM        NUMBER(10),
  CD_EMPRESA_REFERENCIA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCARFI_CARFIDE_FK_I ON TASY.PF_CARTAO_FIDELIDADE
(NR_SEQ_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFCARFI_CARFIDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PFCARFI_EMPREFE_FK_I ON TASY.PF_CARTAO_FIDELIDADE
(CD_EMPRESA_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PFCARFI_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PFCARFI_PESFISI_FK_I ON TASY.PF_CARTAO_FIDELIDADE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCARFI_PESFISI_FK2_I ON TASY.PF_CARTAO_FIDELIDADE
(CD_PESSOA_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCARFI_PESFISI_FK3_I ON TASY.PF_CARTAO_FIDELIDADE
(CD_PESSOA_VENDEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCARFI_PK ON TASY.PF_CARTAO_FIDELIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pf_cartao_befupdate_log
before update ON TASY.PF_CARTAO_FIDELIDADE for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into pf_cartao_fidelidade_log(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_cartao,
		dt_inicio_validade,
		dt_fim_validade,
		cd_pessoa_fisica,
		nr_cartao_fidelidade,
		dt_emissao_cartao,
		cd_pessoa_titular,
		ds_observacao,
		cd_pessoa_vendedor,
		qt_pontuacao_cm)
	values( pf_cartao_fidelidade_log_seq.NextVal,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.nr_seq_cartao,
		:old.dt_inicio_validade,
		:old.dt_fim_validade,
		:new.cd_pessoa_fisica,
		:old.nr_cartao_fidelidade,
		:old.dt_emissao_cartao,
		:old.cd_pessoa_titular,
		:old.ds_observacao,
		:old.cd_pessoa_vendedor,
		:old.qt_pontuacao_cm);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PF_CARTAO_FIDELIDADE_tp  after update ON TASY.PF_CARTAO_FIDELIDADE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTAO,1,4000),substr(:new.NR_SEQ_CARTAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTAO',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VALIDADE,1,4000),substr(:new.DT_INICIO_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VALIDADE',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VALIDADE,1,4000),substr(:new.DT_FIM_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VALIDADE',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PONTUACAO_CM,1,4000),substr(:new.QT_PONTUACAO_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_PONTUACAO_CM',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_EMISSAO_CARTAO,1,4000),substr(:new.DT_EMISSAO_CARTAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CARTAO',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_TITULAR,1,4000),substr(:new.CD_PESSOA_TITULAR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_TITULAR',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_VENDEDOR,1,4000),substr(:new.CD_PESSOA_VENDEDOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_VENDEDOR',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CARTAO_FIDELIDADE,1,4000),substr(:new.NR_CARTAO_FIDELIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_FIDELIDADE',ie_log_w,ds_w,'PF_CARTAO_FIDELIDADE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PF_CARTAO_FIDELIDADE ADD (
  CONSTRAINT PFCARFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_CARTAO_FIDELIDADE ADD (
  CONSTRAINT PFCARFI_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REFERENCIA) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT PFCARFI_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_TITULAR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFCARFI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PFCARFI_CARFIDE_FK 
 FOREIGN KEY (NR_SEQ_CARTAO) 
 REFERENCES TASY.CARTAO_FIDELIDADE (NR_SEQUENCIA),
  CONSTRAINT PFCARFI_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_VENDEDOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PF_CARTAO_FIDELIDADE TO NIVEL_1;

