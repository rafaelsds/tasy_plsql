DROP TABLE TASY.W_PLS_ANALISE_ITEM CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_PLS_ANALISE_ITEM
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NR_SEQ_ANALISE                 NUMBER(10)     NOT NULL,
  NR_SEQ_CONTA                   NUMBER(10),
  NR_SEQ_CONTA_PROC              NUMBER(10),
  NR_SEQ_CONTA_MAT               NUMBER(10),
  NR_SEQ_PROC_PARTIC             NUMBER(10),
  IE_TIPO_LINHA                  VARCHAR2(3 BYTE),
  IE_TIPO_DESPESA                VARCHAR2(10 BYTE),
  CD_ITEM                        VARCHAR2(30 BYTE),
  DT_ITEM                        DATE,
  DS_ITEM                        VARCHAR2(255 BYTE),
  IE_STATUS_ANALISE              VARCHAR2(1 BYTE),
  IE_PAGAMENTO                   VARCHAR2(3 BYTE),
  IE_VALOR_BASE                  VARCHAR2(1 BYTE),
  VL_APRESENTADO                 NUMBER(15,2),
  QT_APRESENTADA                 NUMBER(12,4),
  VL_CALCULADO_UNITARIO          NUMBER(15,2),
  VL_CALCULADO                   NUMBER(15,2),
  NR_ORDEM_1                     NUMBER(10),
  NR_ORDEM_2                     NUMBER(10),
  NR_IDENTIFICADOR               NUMBER(10),
  IE_TIPO_ITEM                   VARCHAR2(3 BYTE),
  IE_TIPO_TITULO                 VARCHAR2(3 BYTE),
  NR_SEQ_PROC_PAI                NUMBER(10),
  IE_PARTIC_HONORARIO            VARCHAR2(1 BYTE),
  CD_GRAU_PARTIC_TISS            VARCHAR2(20 BYTE),
  DS_GRAU_PARTIC                 VARCHAR2(255 BYTE),
  CD_PROCEDIMENTO                NUMBER(15),
  IE_ORIGEM_PROCED               NUMBER(10),
  NR_SEQ_MATERIAL                NUMBER(10),
  IE_VIA_ACESSO                  VARCHAR2(10 BYTE),
  DS_VIA_ACESSO                  VARCHAR2(255 BYTE),
  NR_AUXILIARES                  NUMBER(10),
  NM_PRESTADOR_SOLIC             VARCHAR2(255 BYTE),
  CD_PORTE_ANESTESICO            VARCHAR2(10 BYTE),
  DS_TIPO_GUIA                   VARCHAR2(255 BYTE),
  NM_PRESTADOR_EXEC              VARCHAR2(255 BYTE),
  NM_PRESTADOR_PAG               VARCHAR2(255 BYTE),
  DS_ESPEC_CBO                   VARCHAR2(255 BYTE),
  NR_SEQ_PROTOCOLO               NUMBER(10),
  IE_AUTORIZ_PREVIA              VARCHAR2(5 BYTE),
  DS_EXIGE_NF                    VARCHAR2(255 BYTE),
  CD_GUIA                        VARCHAR2(30 BYTE),
  DS_FORNECEDOR                  VARCHAR2(255 BYTE),
  DS_SETOR_ATEND                 VARCHAR2(255 BYTE),
  DS_UNIDADE_MEDIDA              VARCHAR2(255 BYTE),
  DS_ITEM_IMPORTACAO             VARCHAR2(255 BYTE),
  DS_MEDICO_EXECUTOR             VARCHAR2(80 BYTE),
  IE_TIPO_GUIA                   VARCHAR2(2 BYTE),
  VL_GLOSA                       NUMBER(15,2),
  TX_ITEM                        NUMBER(9,3),
  QT_LIBERADO                    NUMBER(12,4),
  VL_TAXA_INTERCAMBIO_IMP        NUMBER(15,2),
  VL_TAXA_INTERCAMBIO            NUMBER(15,2),
  TX_INTERCAMBIO_IMP             NUMBER(7,4),
  TX_INTERCAMBIO                 NUMBER(7,4),
  VL_CALCULADO_HI                NUMBER(15,2),
  VL_CALCULADO_CO                NUMBER(15,2),
  VL_CALCULADO_MATERIAL          NUMBER(15,2),
  VL_LIBERADO_HI                 NUMBER(15,2),
  VL_LIBERADO_CO                 NUMBER(15,2),
  VL_LIBERADO_MATERIAL           NUMBER(15,2),
  VL_CUSTO_OPERACIONAL           NUMBER(15,2),
  VL_GLOSA_CO                    NUMBER(15,2),
  VL_GLOSA_HI                    NUMBER(15,2),
  VL_GLOSA_MATERIAL              NUMBER(15,2),
  VL_LIBERADO                    NUMBER(15,2),
  VL_UNITARIO_APRES              NUMBER(15,2),
  NR_SEQ_PRESTADOR_EXEC          NUMBER(10),
  NR_SEQ_GUIA                    NUMBER(10),
  IE_EXPANDIDO                   VARCHAR2(1 BYTE),
  QT_LIBERAR                     NUMBER(12,4),
  IE_STATUS_ITEM                 VARCHAR2(2 BYTE),
  DS_STATUS_ITEM                 VARCHAR2(255 BYTE),
  IE_PEND_GRUPO                  VARCHAR2(3 BYTE),
  IE_ITEM_NAO_ENCONTRADO         VARCHAR2(1 BYTE),
  IE_SELECIONADO                 VARCHAR2(1 BYTE),
  IE_SEM_FLUXO                   VARCHAR2(1 BYTE),
  VL_LIB_TAXA_CO                 NUMBER(15,2),
  VL_LIB_TAXA_MATERIAL           NUMBER(15,2),
  VL_LIB_TAXA_SERVICO            NUMBER(15,2),
  VL_GLOSA_TAXA_CO               NUMBER(15,2),
  VL_GLOSA_TAXA_MATERIAL         NUMBER(15,2),
  VL_GLOSA_TAXA_SERVICO          NUMBER(15,2),
  VL_TAXA_SERVICO                NUMBER(12,4),
  VL_TAXA_SERVICO_IMP            NUMBER(12,4),
  VL_TAXA_CO                     NUMBER(12,4),
  VL_TAXA_CO_IMP                 NUMBER(12,4),
  VL_TAXA_MATERIAL               NUMBER(12,4),
  VL_TAXA_MATERIAL_IMP           NUMBER(12,4),
  NR_SEQ_CONTA_POS_ESTAB         NUMBER(10),
  IE_FATURAMENTO                 VARCHAR2(3 BYTE),
  IE_STATUS_FATURAMENTO          VARCHAR2(3 BYTE),
  VL_FATURADO_ORIG               NUMBER(15,2),
  QT_FATURADA_ORIG               NUMBER(12,4),
  VL_UNITARIO_FATURADO           NUMBER(15,2),
  TX_MEDICO                      NUMBER(15,4),
  TX_MATERIAL                    NUMBER(15,4),
  TX_CUSTO_OPERACIONAL           NUMBER(15,4),
  NR_SEQ_AUTOGERADO              NUMBER(10),
  VL_PROCEDIMENTO_PTU_IMP        NUMBER(15,2),
  VL_CO_PTU_IMP                  NUMBER(15,2),
  VL_MATERIAL_PTU_IMP            NUMBER(15,2),
  CD_CLASSIFICACAO               VARCHAR2(30 BYTE),
  VL_MEDICO_CALC_FAT             NUMBER(15,2),
  VL_MATERIAIS_CALC_FAT          NUMBER(15,2),
  VL_CUSTO_OPERACIONAL_CALC_FAT  NUMBER(15,2),
  CD_MATERIAL_A900               VARCHAR2(30 BYTE),
  DS_MATERIAL_A900               VARCHAR2(255 BYTE),
  IE_PACOTE_PTU                  VARCHAR2(1 BYTE),
  VL_LIBERADO_MATERIAL_FAT       NUMBER(15,2),
  VL_LIBERADO_CO_FAT             NUMBER(15,2),
  VL_LIBERADO_HI_FAT             NUMBER(15,2),
  VL_GLOSA_MATERIAL_FAT          NUMBER(15,2),
  VL_GLOSA_HI_FAT                NUMBER(15,2),
  VL_GLOSA_CO_FAT                NUMBER(15,2),
  QT_COBRANCA                    NUMBER(12,4),
  VL_LIBERADO_PAG                NUMBER(15,2),
  IE_INFORMATIVO                 VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_QTDE_EXEC         NUMBER(10),
  NR_SEQ_TUSS_MAT_ITEM           NUMBER(10),
  CD_TUSS_MAT_ITEM               VARCHAR2(255 BYTE),
  DS_TUSS_MAT_ITEM               VARCHAR2(255 BYTE),
  CD_MOEDA_AUTOGERADO            NUMBER(3),
  CD_UNIDADE_MEDIDA_A900         VARCHAR2(10 BYTE),
  VL_GLOSA_PAG                   NUMBER(15,2),
  VL_LIB_TAXA_CO_FAT             NUMBER(15,2),
  VL_LIB_TAXA_MATERIAL_FAT       NUMBER(15,2),
  VL_LIB_TAXA_HM_FAT             NUMBER(15,2),
  VL_GLOSA_TAXA_CO_FAT           NUMBER(15,2),
  VL_GLOSA_TAXA_MATERIAL_FAT     NUMBER(15,2),
  VL_GLOSA_TAXA_HM_FAT           NUMBER(15,2),
  VL_TAXA_HM_FAT                 NUMBER(12,4),
  VL_TAXA_CO_FAT                 NUMBER(12,4),
  VL_TAXA_MATERIAL_FAT           NUMBER(12,4),
  NR_SEQ_EVENTO_FAT              NUMBER(10),
  NR_SEQ_LOTE_FAT                NUMBER(10),
  IE_PCMSO                       VARCHAR2(1 BYTE),
  TX_ADMINISTRACAO_POS           NUMBER(7,4),
  CD_DENTE                       VARCHAR2(20 BYTE),
  CD_REGIAO_BOCA                 VARCHAR2(20 BYTE),
  CD_FACE_DENTE                  VARCHAR2(20 BYTE),
  DS_DENTE                       VARCHAR2(255 BYTE),
  DS_REGIAO_BOCA                 VARCHAR2(255 BYTE),
  DS_FACE_DENTE                  VARCHAR2(255 BYTE),
  NR_REGISTRO_ANVISA             VARCHAR2(15 BYTE),
  CD_REF_FABRICANTE_IMP          VARCHAR2(60 BYTE),
  DS_AUT_FUNCIONAMENTO_IMP       VARCHAR2(30 BYTE),
  CD_UNIDADE_MEDIDA              VARCHAR2(3 BYTE),
  IE_ALTO_CUSTO                  VARCHAR2(1 BYTE),
  CD_REF_FABRICANTE              VARCHAR2(60 BYTE),
  DS_COMPLEMENTO                 VARCHAR2(240 BYTE),
  NR_SEQ_CONTA_POS_PROC          NUMBER(10),
  NR_SEQ_CONTA_POS_MAT           NUMBER(10),
  CD_CBO_SAUDE                   VARCHAR2(30 BYTE),
  NR_ID_TRANSACAO                NUMBER(10),
  TP_REDE_MIN                    NUMBER(1),
  IE_A520                        VARCHAR2(3 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.WPLSAIT_I1 ON TASY.W_PLS_ANALISE_ITEM
(NM_USUARIO, NR_SEQUENCIA);

ALTER INDEX TASY.WPLSAIT_I1
  MONITORING USAGE;


CREATE INDEX TASY.WPLSAIT_I15 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_ANALISE);


CREATE INDEX TASY.WPLSAIT_I2 ON TASY.W_PLS_ANALISE_ITEM
(IE_TIPO_ITEM, IE_TIPO_LINHA);

ALTER INDEX TASY.WPLSAIT_I2
  MONITORING USAGE;


CREATE INDEX TASY.WPLSAIT_I3 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_CONTA, NR_SEQ_CONTA_PROC);

ALTER INDEX TASY.WPLSAIT_I3
  MONITORING USAGE;


CREATE INDEX TASY.WPLSAIT_I4 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_CONTA, NR_SEQ_CONTA_MAT);

ALTER INDEX TASY.WPLSAIT_I4
  MONITORING USAGE;


CREATE INDEX TASY.WPLSAIT_I5 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_CONTA_PROC);

ALTER INDEX TASY.WPLSAIT_I5
  MONITORING USAGE;


CREATE INDEX TASY.WPLSAIT_I6 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_CONTA_MAT);

ALTER INDEX TASY.WPLSAIT_I6
  MONITORING USAGE;


CREATE INDEX TASY.WPLSAIT_I7 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_ANALISE, IE_TIPO_LINHA);


CREATE INDEX TASY.WPLSAIT_I8 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_ANALISE, NR_ID_TRANSACAO, IE_TIPO_ITEM, IE_TIPO_LINHA);


CREATE INDEX TASY.WPLSAIT_I9 ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQUENCIA, NR_ID_TRANSACAO);


CREATE INDEX TASY.WPLSAIT_PLSEVFA_FK_I ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_EVENTO_FAT);


CREATE INDEX TASY.WPLSAIT_PLSLOFA_FK_I ON TASY.W_PLS_ANALISE_ITEM
(NR_SEQ_LOTE_FAT);


CREATE OR REPLACE TRIGGER TASY.LOG_RFOLIVEIRA
before insert or update ON TASY.W_PLS_ANALISE_ITEM for each row
declare
ds_stack_w            varchar2(1800);
nr_sequencia_w            number(10);
ie_acao_w            varchar2(1);
ie_grava_w            varchar2(1);
ds_log_w            varchar2(2000);

begin

ie_grava_w    := 'N';

if    (INSERTING) then
    ie_acao_w    := 'I';
    ie_grava_w    := 'S';
elsif
    (UPDATING) then
    ie_acao_w    := 'U';
    ie_grava_w    := 'S';
end if;

if    (ie_grava_w    = 'S') then
    begin

    nr_sequencia_w        := :new.nr_sequencia;
    ds_stack_w        := substr(dbms_utility.format_call_stack,1,1800);
    
    ds_log_w        := substr(ie_acao_w || ';SEQ=' || nr_sequencia_w || ';STACK=' || ds_stack_w,1,1900);

    gravar_log_tasy(15102,ds_log_w,:new.nm_usuario);
    
    end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.w_pls_analise_item_atual
before insert or update ON TASY.W_PLS_ANALISE_ITEM for each row
declare

begin

if	(((:old.nr_identificador is not null) and (:new.nr_identificador is null)) or
	((:old.nr_identificador is not null)  and (:old.nr_identificador != :new.nr_identificador)) or
	((:new.nr_identificador is null)and (:new.ie_tipo_linha not in ('T','C','A')) and((:new.ie_informativo is null) or (:new.ie_informativo = 'N')))) then

	wheb_mensagem_pck.exibir_mensagem_abort(247472);
end if;

end;
/


GRANT SELECT ON TASY.W_PLS_ANALISE_ITEM TO NIVEL_1;

