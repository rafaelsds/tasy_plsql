ALTER TABLE TASY.ESCALA_ANSIEDADE_DEPRESSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ANSIEDADE_DEPRESSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ANSIEDADE_DEPRESSAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_SAEP                NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_LIBERACAO               DATE,
  IE_TENSO                   NUMBER(1)          NOT NULL,
  IE_GOSTO                   NUMBER(1)          NOT NULL,
  IE_MEDO                    NUMBER(1)          NOT NULL,
  IE_RISADA                  NUMBER(1)          NOT NULL,
  IE_PREOCUPACOES            NUMBER(1)          NOT NULL,
  IE_ALEGRE                  NUMBER(1)          NOT NULL,
  IE_RELAXADO                NUMBER(1)          NOT NULL,
  IE_LENTO                   NUMBER(1)          NOT NULL,
  IE_SENSACAO                NUMBER(1)          NOT NULL,
  IE_APARENCIA               NUMBER(1)          NOT NULL,
  IE_INQUIETO                NUMBER(1)          NOT NULL,
  IE_ANIMADO                 NUMBER(1)          NOT NULL,
  IE_PANICO                  NUMBER(1)          NOT NULL,
  IE_PRAZER                  NUMBER(1)          NOT NULL,
  QT_ANSIEDADE               NUMBER(10)         NOT NULL,
  QT_DEPRESSAO               NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESANDEP_ATEPACI_FK_I ON TASY.ESCALA_ANSIEDADE_DEPRESSAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESANDEP_PESFISI_FK_I ON TASY.ESCALA_ANSIEDADE_DEPRESSAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESANDEP_PK ON TASY.ESCALA_ANSIEDADE_DEPRESSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESANDEP_SAEPERO_FK_I ON TASY.ESCALA_ANSIEDADE_DEPRESSAO
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESANDEP_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESANDEP_TASASDI_FK_I ON TASY.ESCALA_ANSIEDADE_DEPRESSAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESANDEP_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESANDEP_TASASDI_FK2_I ON TASY.ESCALA_ANSIEDADE_DEPRESSAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESANDEP_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_ansiedade_depre_atual
BEFORE INSERT OR UPDATE ON TASY.ESCALA_ANSIEDADE_DEPRESSAO FOR EACH ROW
DECLARE
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_liberacao is null)  then
	begin

	:new.qt_ansiedade := 				nvl(:new.ie_tenso,0)		+
							nvl(:new.ie_medo,0) 	+
							nvl(:new.ie_preocupacoes,0) 	+
							nvl(:new.ie_relaxado,0)	+
							nvl(:new.ie_sensacao,0) 	+
							nvl(:new.ie_inquieto,0) 	+
							nvl(:new.ie_panico,0);

	:new.qt_depressao := 				nvl(:new.ie_gosto,0)	 	+
							nvl(:new.ie_risada,0)	+
							nvl(:new.ie_alegre,0) 	+
							nvl(:new.ie_lento,0)	+
							nvl(:new.ie_aparencia,0) 	+
							nvl(:new.ie_animado,0)    	+
							nvl(:new.ie_prazer,0);

	end;
end if;
<<Final>>
qt_reg_w	:= 0;

end escala_ansiedade_depre_atual;
/


CREATE OR REPLACE TRIGGER TASY.esc_ans_depres_pend_atual
after insert or update ON TASY.ESCALA_ANSIEDADE_DEPRESSAO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'41');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.escala_ans_depress_delete
after delete ON TASY.ESCALA_ANSIEDADE_DEPRESSAO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '41'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_ANSIEDADE_DEPRESSAO ADD (
  CONSTRAINT ESANDEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ANSIEDADE_DEPRESSAO ADD (
  CONSTRAINT ESANDEP_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESANDEP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESANDEP_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESANDEP_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESANDEP_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_ANSIEDADE_DEPRESSAO TO NIVEL_1;

