ALTER TABLE TASY.W_TISS_DADOS_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_TISS_DADOS_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TISS_DADOS_ATENDIMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_GUIA          NUMBER(10)               NOT NULL,
  IE_TIPO_ATENDIMENTO  VARCHAR2(5 BYTE),
  IE_TIPO_SAIDA        VARCHAR2(5 BYTE),
  IE_TIPO_ACIDENTE     VARCHAR2(10 BYTE),
  NR_SEQ_CONTA         NUMBER(10),
  IE_TIPO_CONSULTA     VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WTDAATE_PK ON TASY.W_TISS_DADOS_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WTDAATE_PK
  MONITORING USAGE;


CREATE INDEX TASY.WTDAATE_USER_I ON TASY.W_TISS_DADOS_ATENDIMENTO
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_TISS_DADOS_ATENDIMENTO ADD (
  CONSTRAINT WTDAATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_TISS_DADOS_ATENDIMENTO TO NIVEL_1;

