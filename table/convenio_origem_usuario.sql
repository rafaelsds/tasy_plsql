ALTER TABLE TASY.CONVENIO_ORIGEM_USUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_ORIGEM_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_ORIGEM_USUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  DS_ORIGEM            VARCHAR2(60 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ORIGEM            VARCHAR2(30 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONORUS_CONVENI_FK_I ON TASY.CONVENIO_ORIGEM_USUARIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONORUS_LOATINT_FK_I ON TASY.CONVENIO_ORIGEM_USUARIO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONORUS_LOATINT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONORUS_PK ON TASY.CONVENIO_ORIGEM_USUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONVENIO_ORIGEM_USUARIO ADD (
  CONSTRAINT CONORUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_ORIGEM_USUARIO ADD (
  CONSTRAINT CONORUS_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT CONORUS_LOATINT_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.LOCAL_ATEND_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONVENIO_ORIGEM_USUARIO TO NIVEL_1;

