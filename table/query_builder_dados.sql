ALTER TABLE TASY.QUERY_BUILDER_DADOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUERY_BUILDER_DADOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUERY_BUILDER_DADOS
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NM_TABELA                VARCHAR2(80 BYTE),
  NM_CAMPO                 VARCHAR2(80 BYTE),
  CD_EXP_TABELA            NUMBER(15),
  CD_EXP_CAMPO             NUMBER(15),
  NR_SEQ_FILTER            NUMBER(10),
  DS_FUNCAO                VARCHAR2(80 BYTE),
  NR_SEQ_OBJETO_SCHEMATIC  NUMBER(10),
  CD_FUNCAO                NUMBER(5),
  NR_SEQ_ELEMENTO          NUMBER(10),
  CD_EXP_TITLE             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUERY_DADO_FUNCAO_FK_I ON TASY.QUERY_BUILDER_DADOS
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUERY_DADO_PK ON TASY.QUERY_BUILDER_DADOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUERY_DADO_QUERY_DAR_FK_I ON TASY.QUERY_BUILDER_DADOS
(NR_SEQ_FILTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QUERY_BUILDER_DADOS ADD (
  CONSTRAINT QUERY_DADO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.QUERY_BUILDER_DADOS ADD (
  CONSTRAINT QUERY_DADO_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT QUERY_DADO_QUERY_DAR_FK 
 FOREIGN KEY (NR_SEQ_FILTER) 
 REFERENCES TASY.FILTER_QUERY_DAR (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUERY_BUILDER_DADOS TO NIVEL_1;

