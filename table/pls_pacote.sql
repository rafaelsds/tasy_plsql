ALTER TABLE TASY.PLS_PACOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PACOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PACOTE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_PRESTADOR       NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO        NUMBER(15)             NOT NULL,
  IE_ORIGEM_PROCED       NUMBER(10)             NOT NULL,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  VL_PACOTE              NUMBER(15,2),
  IE_REGRA_PRECO         VARCHAR2(1 BYTE),
  IE_SITUACAO_LIBERACAO  VARCHAR2(5 BYTE)       NOT NULL,
  IE_BAIXO_RISCO         VARCHAR2(1 BYTE),
  IE_PACOTE_LOCAL        VARCHAR2(2 BYTE),
  IE_PACOTE_GENETICA     VARCHAR2(1 BYTE),
  IE_PACOTE_ESTADUAL     VARCHAR2(1 BYTE),
  IE_STATUS_SISPAC       NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPACO_ESTABEL_FK_I ON TASY.PLS_PACOTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPACO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPACO_PK ON TASY.PLS_PACOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPACO_PLSPRES_FK_I ON TASY.PLS_PACOTE
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPACO_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPACO_PROCEDI_FK_I ON TASY.PLS_PACOTE
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_pacote_delete
after delete ON TASY.PLS_PACOTE for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PACOTE',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.ssj_inse_proc_princ_pacot
after insert
ON TASY.PLS_PACOTE for each row
declare

nr_seq_pacote_p int;
cd_codigo_pacote_p int; 
ie_origem_proc_p int;
jobno number;

begin
       
    nr_seq_pacote_p := :new.nr_sequencia; /* Pega a sequence do pacote */
    cd_codigo_pacote_p := :new.cd_procedimento; /* Pega o c�digo do procedimento sendo inserido no pacote */
    
    /*
        Pega a origem do procedimento na tabela PROCEDIMENTO
    */
    select ie_origem_proced
    into ie_origem_proc_p
    from procedimento
    where cd_procedimento = :new.cd_procedimento
    and ie_origem_proced not in(3,2,7);

    /*
        Verifica se as vari�veis n�o est�o nulas, caso n�o estejam elas s�o tranferidas uma JOB tempor�rio que ir� realizar a execu��o em paralelo com a procedure para n�o dar problema de tabela mutante.
        Caso alguma das vari�veis n�o esteja de acordo um erro em banco � mostrado.
    */
    if (nr_seq_pacote_p is not null and cd_codigo_pacote_p is not null and ie_origem_proc_p is not null) then

        dbms_job.submit(jobno,'ssj_inserir_proc_princ_pacote('||nr_seq_pacote_p||','||cd_codigo_pacote_p||','||ie_origem_proc_p||');');
    else
        raise_application_error(-20011,'Pacote n�o encontrado');
    end if;

end ssj_inse_proc_princ_pacot;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PACOTE_tp  after update ON TASY.PLS_PACOTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_LIBERACAO,1,4000),substr(:new.IE_SITUACAO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_LIBERACAO',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PACOTE,1,4000),substr(:new.VL_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PACOTE',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_PRECO,1,4000),substr(:new.IE_REGRA_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_PRECO',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_PACOTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PACOTE ADD (
  CONSTRAINT PLSPACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PACOTE ADD (
  CONSTRAINT PLSPACO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPACO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSPACO_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PACOTE TO NIVEL_1;

