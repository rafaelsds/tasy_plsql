ALTER TABLE TASY.TIT_CHEQUE_ORGAO_COBR_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIT_CHEQUE_ORGAO_COBR_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIT_CHEQUE_ORGAO_COBR_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TITULO        NUMBER(10),
  NR_SEQ_CHEQUE_CR     NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_HISTORICO         VARCHAR2(255 BYTE)       NOT NULL,
  DT_HISTORICO         DATE                     NOT NULL,
  IE_ORIGEM            VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITCHEHIS_CHEQUES_FK_I ON TASY.TIT_CHEQUE_ORGAO_COBR_HIST
(NR_SEQ_CHEQUE_CR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITCHEHIS_PK ON TASY.TIT_CHEQUE_ORGAO_COBR_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITCHEHIS_TITRECE_FK_I ON TASY.TIT_CHEQUE_ORGAO_COBR_HIST
(NR_SEQ_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIT_CHEQUE_ORGAO_COBR_HIST ADD (
  CONSTRAINT TITCHEHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIT_CHEQUE_ORGAO_COBR_HIST ADD (
  CONSTRAINT TITCHEHIS_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE_CR) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE),
  CONSTRAINT TITCHEHIS_TITRECE_FK 
 FOREIGN KEY (NR_SEQ_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO));

GRANT SELECT ON TASY.TIT_CHEQUE_ORGAO_COBR_HIST TO NIVEL_1;

