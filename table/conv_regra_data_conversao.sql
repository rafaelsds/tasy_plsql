ALTER TABLE TASY.CONV_REGRA_DATA_CONVERSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONV_REGRA_DATA_CONVERSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONV_REGRA_DATA_CONVERSAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO              VARCHAR2(1 BYTE),
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_DATA              VARCHAR2(3 BYTE)         NOT NULL,
  DT_VIGENCIA_INICIAL  DATE                     NOT NULL,
  DT_VIGENCIA_FINAL    DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COREDAC_CONVENI_FK_I ON TASY.CONV_REGRA_DATA_CONVERSAO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREDAC_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COREDAC_PK ON TASY.CONV_REGRA_DATA_CONVERSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CONV_REGRA_DATA_CONVERSAO_tp  after update ON TASY.CONV_REGRA_DATA_CONVERSAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CONV_REGRA_DATA_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'CONV_REGRA_DATA_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_FINAL,1,4000),substr(:new.DT_VIGENCIA_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_FINAL',ie_log_w,ds_w,'CONV_REGRA_DATA_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_INICIAL,1,4000),substr(:new.DT_VIGENCIA_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_INICIAL',ie_log_w,ds_w,'CONV_REGRA_DATA_CONVERSAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DATA,1,4000),substr(:new.IE_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA',ie_log_w,ds_w,'CONV_REGRA_DATA_CONVERSAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CONV_REGRA_DATA_CONVERSAO ADD (
  CONSTRAINT COREDAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONV_REGRA_DATA_CONVERSAO ADD (
  CONSTRAINT COREDAC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.CONV_REGRA_DATA_CONVERSAO TO NIVEL_1;

