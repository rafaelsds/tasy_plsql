ALTER TABLE TASY.QR_CHECKLIST_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QR_CHECKLIST_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.QR_CHECKLIST_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CHECKLIST     NUMBER(10)               NOT NULL,
  DS_CHECKLIST_ITEM    VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_DETALHAMENTO      VARCHAR2(4000 BYTE)      NOT NULL,
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  NR_SEQ_APRESENT      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QRCKLIT_PK ON TASY.QR_CHECKLIST_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QRCKLIT_QRCHKLS_FK_I ON TASY.QR_CHECKLIST_ITEM
(NR_SEQ_CHECKLIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QR_CHECKLIST_ITEM ADD (
  CONSTRAINT QRCKLIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QR_CHECKLIST_ITEM ADD (
  CONSTRAINT QRCKLIT_QRCHKLS_FK 
 FOREIGN KEY (NR_SEQ_CHECKLIST) 
 REFERENCES TASY.QR_CHECKLIST (NR_SEQUENCIA));

GRANT SELECT ON TASY.QR_CHECKLIST_ITEM TO NIVEL_1;

