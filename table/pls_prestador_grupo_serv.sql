ALTER TABLE TASY.PLS_PRESTADOR_GRUPO_SERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_GRUPO_SERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_GRUPO_SERV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR     NUMBER(10)               NOT NULL,
  CD_GRUPO_SERVICO     NUMBER(3)                NOT NULL,
  NR_SEQ_PREST_END     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPGRS_PK ON TASY.PLS_PRESTADOR_GRUPO_SERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPGRS_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPGRS_PLSPREN_FK_I ON TASY.PLS_PRESTADOR_GRUPO_SERV
(NR_SEQ_PREST_END)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPGRS_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_GRUPO_SERV
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPGRS_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPGRS_PTUGRSE_FK_I ON TASY.PLS_PRESTADOR_GRUPO_SERV
(CD_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPGRS_PTUGRSE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_prest_grupo_serv_delete
after delete ON TASY.PLS_PRESTADOR_GRUPO_SERV for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PRESTADOR_GRUPO_SERV',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


ALTER TABLE TASY.PLS_PRESTADOR_GRUPO_SERV ADD (
  CONSTRAINT PLSPGRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_GRUPO_SERV ADD (
  CONSTRAINT PLSPGRS_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPGRS_PTUGRSE_FK 
 FOREIGN KEY (CD_GRUPO_SERVICO) 
 REFERENCES TASY.PTU_GRUPO_SERVICO (CD_GRUPO_SERVICO),
  CONSTRAINT PLSPGRS_PLSPREN_FK 
 FOREIGN KEY (NR_SEQ_PREST_END) 
 REFERENCES TASY.PLS_PRESTADOR_ENDERECO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_GRUPO_SERV TO NIVEL_1;

