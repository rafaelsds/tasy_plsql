ALTER TABLE TASY.MAN_ENC_DIA_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ENC_DIA_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ENC_DIA_REGRA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_ENCERRA  NUMBER(10),
  QT_DIA_COMUNICADO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANENDR_MANENOSES_FK_I ON TASY.MAN_ENC_DIA_REGRA
(NR_SEQ_REGRA_ENCERRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANENDR_PK ON TASY.MAN_ENC_DIA_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_enc_dia_regra_before
before insert or update ON TASY.MAN_ENC_DIA_REGRA for each row
declare

qt_dias_regra_w	man_encerrar_os_estag.qt_dias%type;

begin
	if	(phi_is_base_philips = 'S') then
		begin
			begin
				select	nvl(meoe.qt_dias, 0)
				into	qt_dias_regra_w
				from	man_encerrar_os_estag meoe
				where	meoe.nr_sequencia = :new.nr_seq_regra_encerra;
			exception
			when others then
				qt_dias_regra_w := 0;
			end;

			if	(qt_dias_regra_w = 0) then
				begin
					wheb_mensagem_pck.exibir_mensagem_abort(1143436);
				end;
			elsif 	(qt_dias_regra_w < :new.qt_dia_comunicado) then
				begin
					wheb_mensagem_pck.exibir_mensagem_abort(1143441);
				end;
			end if;
		end;
	end if;
end;
/


ALTER TABLE TASY.MAN_ENC_DIA_REGRA ADD (
  CONSTRAINT MANENDR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MAN_ENC_DIA_REGRA ADD (
  CONSTRAINT MANENDR_MANENOSES_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ENCERRA) 
 REFERENCES TASY.MAN_ENCERRAR_OS_ESTAG (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ENC_DIA_REGRA TO NIVEL_1;

