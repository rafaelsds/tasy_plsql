ALTER TABLE TASY.ENVIO_LACRE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ENVIO_LACRE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENVIO_LACRE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_LACRE                VARCHAR2(255 BYTE)    NOT NULL,
  QT_TEMPERATURA_PREPARO  NUMBER(5,2),
  DT_AGENDAMENTO          DATE,
  NM_USUARIO_AGENDAMENTO  VARCHAR2(15 BYTE),
  QT_TEMPERATURA_RECEB    NUMBER(4,1),
  NM_USUARIO_RETIRADA     VARCHAR2(15 BYTE),
  NM_USUARIO_RECEB        VARCHAR2(15 BYTE),
  DT_RETIRADA             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ENVILACR_I ON TASY.ENVIO_LACRE
(DS_LACRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ENVILACR_PK ON TASY.ENVIO_LACRE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ENVIO_LACRE ADD (
  CONSTRAINT ENVILACR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT ENVILACR_I
 UNIQUE (DS_LACRE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          32K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ENVIO_LACRE TO NIVEL_1;

