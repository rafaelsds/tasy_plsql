ALTER TABLE TASY.AGEINT_EXAME_LAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_EXAME_LAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_EXAME_LAB
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_PROC_INTERNO      NUMBER(10),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_AGEINT            NUMBER(10)           NOT NULL,
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  NR_SEQ_EXAME             NUMBER(10),
  NR_SEQ_REGRA             NUMBER(10),
  IE_GLOSA                 VARCHAR2(1 BYTE),
  IE_AUTORIZACAO           VARCHAR2(3 BYTE),
  IE_REGRA                 NUMBER(5),
  VL_CUSTO_OPERACIONAL     NUMBER(15,2),
  VL_ANESTESISTA           NUMBER(15,2),
  VL_MEDICO                NUMBER(15,2),
  VL_AUXILIARES            NUMBER(15,2),
  VL_MATERIAIS             NUMBER(15,2),
  VL_ITEM                  NUMBER(15,2),
  NR_SEQ_ORIGEM            NUMBER(10),
  DT_PREVISTA              DATE,
  NR_SEQ_INTERNO_PRESCR    NUMBER(10),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  NR_PRESCRICAO            NUMBER(14)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIEXLB_AGEINTE_FK_I ON TASY.AGEINT_EXAME_LAB
(NR_SEQ_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXLB_AGEINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXLB_EXALABO_FK_I ON TASY.AGEINT_EXAME_LAB
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXLB_EXALABO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGIEXLB_PK ON TASY.AGEINT_EXAME_LAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXLB_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXLB_PRESPRO_FK_I ON TASY.AGEINT_EXAME_LAB
(NR_SEQ_INTERNO_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXLB_PRESPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXLB_PROCEDI_FK_I ON TASY.AGEINT_EXAME_LAB
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1032K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXLB_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXLB_PROINTE_FK_I ON TASY.AGEINT_EXAME_LAB
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXLB_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AGEINT_EXAME_LAB_tp  after update ON TASY.AGEINT_EXAME_LAB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_EXAME,1,4000),substr(:new.NR_SEQ_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EXAME',ie_log_w,ds_w,'AGEINT_EXAME_LAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'AGEINT_EXAME_LAB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ageint_exame_lab_atual
before insert or update ON TASY.AGEINT_EXAME_LAB for each row
declare

qt_pontos_w			preco_amb.qt_pontuacao%type;
cd_estabelecimento_w	Number(4);
cd_convenio_w		Number(5);
cd_categoria_w		Varchar2(10);
dt_inicio_agendamento_w	Date;
cd_plano_w		Varchar2(10);
vl_procedimento_w	Number(15,2);
vl_aux_w		Number(15,4);
ds_aux_w		Varchar2(10);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
cd_convenio_ww		Number(5);
cd_categoria_ww		Varchar2(10);
cd_plano_ww		Varchar2(10);
ie_tipo_convenio_w	Number(3);
ie_regra_w		Number(5);
ie_glosa_w		Varchar2(1);
cd_usuario_convenio_w	Varchar2(30);
qt_idade_w		Number(3);
dt_nascimento_w		Date;
ie_Sexo_w		Varchar2(1);
cd_pessoa_fisica_w	Varchar2(10);
CD_EDICAO_AMB_w         Number(6);
VL_LANC_AUTOM_W         Number(15,2);
vl_custo_operacional_w	Number(15,2);
vl_anestesista_w	Number(15,2);
vl_medico_w		Number(15,2);
vl_auxiliares_w		Number(15,2);
vl_materiais_w		Number(15,2);
cd_medico_item_w	Varchar2(10);
ie_tipo_Atendimento_w	Number(3);
nr_seq_ageint_w		number(10);
cd_conv_item_w		number(5);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
nr_seq_regra_w		number(10);
nr_minuto_duracao_w	number(10);
ie_resp_autor_w		varchar2(10);
cd_medico_solicitante_w varchar2(10);

ie_edicao_w             varchar2(1);
cd_edicao_ajuste_w      number(10);
qt_item_edicao_w         number(10);
cd_estab_agenda_w	number(5);
nm_paciente_w		varchar2(60);
cd_paciente_agenda_w	varchar2(10);
nm_paciente_agenda_w	varchar2(60);
ie_pacote_w		varchar2(1);
ie_inserir_prof_w	varchar2(1);
ds_erro_w		varchar2(255);
dt_nascimento_Ww	date;
nr_minuto_duracao_ww	number(10);
ie_bloq_glosa_part_w	varchar2(1);
nr_seq_cobertura_w	number(10,0);
cd_paciente_w		varchar2(10);
ie_calc_glosa_atend_w	Varchar2(1);
cd_convenio_glosa_w	Number(10);
cd_categoria_glosa_w	Varchar2(10);
ie_conv_cat_regra_w	varchar2(1);
nr_seq_exame_w		number(10,0);
cd_setor_w		number(10,0);
cd_se3tor_w		number(10,0);
nr_seq_proc_interno_aux_w	number(10,0);
nr_seq_ajuste_proc_w	number(10,0);
ie_situacao_exame_w		varchar2(1);
pr_glosa_w				number(15,4);
vl_glosa_w				number(15,4);
ie_regra_arredondamento_tx_w	varchar2(1):= 'N';
ie_tipo_rounded_w		varchar2(1);
ie_regra_arred_IPE_w		varchar2(1):= 'N';
IE_CALCULA_GLOSA_w			parametro_Agenda_integrada.ie_calcula_glosa%type;

ie_param_444_w               VARCHAR2(1) := '';
ie_excluir_valor             VARCHAR2(1) := 'N';

begin

select	nvl(max(IE_CALCULA_GLOSA),'N')
into	IE_CALCULA_GLOSA_w
from	parametro_Agenda_integrada
where	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento)	= wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(869, 187, obter_perfil_ativo, :new.nm_usuario, 0, ie_bloq_glosa_part_w);
obter_param_usuario(869, 262, obter_perfil_ativo, :new.nm_usuario, 0, ie_calc_glosa_atend_w);
obter_param_usuario(869, 444, obter_perfil_ativo, :new.nm_usuario, 0, ie_param_444_w);

begin
select	cd_estabelecimento,
	cd_convenio,
	cd_categoria,
	dt_inicio_agendamento,
	cd_plano,
	cd_usuario_convenio,
	cd_pessoa_fisica,
	ie_tipo_Atendimento,
	dt_nascimento,
	nr_sequencia,
	nm_paciente,
	cd_medico_solicitante,
	nr_seq_cobertura,
	cd_pessoa_fisica
into	cd_estabelecimento_w,
	cd_convenio_ww,
	cd_categoria_ww,
	dt_inicio_agendamento_w,
	cd_plano_ww,
	cd_usuario_convenio_w,
	cd_pessoa_fisica_w,
	ie_tipo_Atendimento_w,
	dt_nascimento_w,
	nr_seq_ageint_w,
	nm_paciente_w,
	cd_medico_solicitante_w,
	nr_seq_cobertura_w,
	cd_paciente_w
from	agenda_integrada
where	nr_sequencia	= :new.nr_seq_ageint;
exception
when others then
	nr_seq_ageint_w	:= null;
end;

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio = cd_convenio_ww;

select	max(ie_Sexo)
into	ie_Sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

if	(((:new.nr_seq_proc_interno is not null) and
	((:old.nr_seq_proc_interno is null) or
	((:old.nr_seq_proc_interno is not null) and
	(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno)))) or
	((:new.nr_seq_exame is not null) and
	((:old.nr_seq_exame is null) or
	((:old.nr_seq_exame is not null) and
	(:old.nr_seq_exame <> :new.nr_seq_exame))))) then

	select	max(nvl(ie_situacao, 'A'))
	into	ie_situacao_exame_w
	from	exame_laboratorio
	where	nr_seq_exame = :new.nr_seq_exame;

	if	(ie_situacao_exame_w = 'I') then
		--N�o � poss�vel cadastrar um exame inativo!
		wheb_mensagem_pck.exibir_mensagem_abort(322751);
	end if;

	if	(((:new.nr_seq_exame is not null) and
		(:new.cd_procedimento is null)) or
		(:new.nr_seq_exame <> :old.nr_seq_exame )) then


		obter_exame_lab_convenio(
					:new.nr_seq_exame,
					cd_convenio_ww,
					cd_categoria_ww,
					ie_tipo_atendimento_w,
					cd_estabelecimento_w,
					ie_tipo_convenio_w,
					null,
					null,
					cd_plano_ww,
					cd_se3tor_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					ds_erro_w,
					nr_seq_proc_interno_aux_w,
					sysdate);

		:new.cd_procedimento 	:=  cd_procedimento_w;
		:new.ie_origem_proced	:= ie_origem_proced_w;
	elsif (:new.nr_seq_proc_interno is not null) and
		((:old.nr_seq_proc_interno is null) or
		((:old.nr_seq_proc_interno is not null) and
		(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno))) then

		obter_proc_tab_interno_conv(
						:new.nr_seq_proc_interno,
						cd_estabelecimento_w,
						cd_convenio_ww,
						cd_categoria_ww,
						cd_plano_ww,
						null,
						cd_procedimento_w,
						ie_origem_proced_w,
						null,
						sysdate,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);

		:new.cd_procedimento	:= cd_procedimento_w;
		:new.ie_origem_proced	:= ie_origem_proced_w;

	end if;



	ageint_consiste_plano_conv(
				null,
				cd_convenio_ww,
				:new.cd_procedimento,
				:new.ie_origem_proced,
				sysdate,
				1,
				nvl(ie_tipo_Atendimento_w,0),
				cd_plano_ww,
				null,
				ds_erro_w,
				0,
				:new.nr_seq_exame,
				ie_regra_w,
				null,
				nr_seq_regra_w,
				:new.nr_Seq_proc_interno,
				cd_categoria_ww,
				cd_estabelecimento_w,
				null,
				ie_Sexo_w,
				ie_glosa_w,
				cd_edicao_ajuste_w,
				nr_seq_cobertura_w,
				cd_convenio_glosa_w,
				cd_categoria_glosa_w,
				cd_paciente_w,
				null,
				pr_glosa_w,
				vl_glosa_w);


		ie_edicao_w	:= ageint_obter_se_proc_conv(cd_estabelecimento_w, cd_convenio_ww, cd_categoria_ww, sysdate, :new.cd_procedimento, :new.ie_origem_proced, :new.nr_Seq_proc_interno, ie_tipo_atendimento_w);

		ie_pacote_w	:= obter_Se_pacote_convenio(:new.cd_procedimento, :new.ie_origem_proced, cd_convenio_ww, cd_estabelecimento_w);


		if	(ie_edicao_w 				= 'N') and
			(nvl(cd_edicao_ajuste_w,0) 	= 0) and
			(nvl(ie_glosa_w,'L') 		= 'L') and
			(ie_pacote_w				= 'N') then
			ie_glosa_w        			:= 'T';
		end if;

		if 	(ie_glosa_w = 'E') and
			(nvl(ie_param_444_w,'N') = 'N') and
			(ie_tipo_convenio_w = '1') then
			ie_excluir_valor := 'S';
		end if;

		if	(ie_edicao_w 				= 'N') and
			(nvl(cd_edicao_ajuste_w,0) 	> 0) and
			(nvl(ie_glosa_w,'L') 		= 'L') and
			(ie_pacote_w				= 'N') then

			select   count(*)
			into     qt_item_edicao_w
			from     preco_amb
			where    cd_edicao_amb = cd_edicao_ajuste_w
			and      cd_procedimento = :new.cd_procedimento
			and      ie_origem_proced = :new.ie_origem_proced;

			if	(qt_item_edicao_w = 0) then
				    ie_glosa_w :=    'G';
			end if;

		end if;

		:new.ie_autorizacao	:= 'L';
		if	((ie_Regra_w in (1,2,5)) or
			((ie_Regra_w = 8) and (ie_bloq_glosa_part_w = 'N'))) then
			:new.ie_autorizacao	:= 'B';
		elsif	(ie_Regra_w in (3,6,7)) then
			select 	nvl(max(ie_resp_autor),'H')
			into	ie_resp_autor_w
			from 	regra_convenio_plano
			where 	nr_sequencia = nr_seq_regra_w;
			if	(ie_resp_autor_w	= 'H') then
				:new.ie_autorizacao	:= 'PAH';
			elsif	(ie_resp_autor_w	= 'P') then
				:new.ie_autorizacao	:= 'PAP';
			end if;
		end if;

		if	(ie_glosa_w in ('G','T','D','F')) then
			:new.ie_autorizacao	:= 'B';
		end if;

		IF (ie_excluir_valor = 'S' ) or
			(((((nvl(ie_Regra_w,0) not in (1,2,5)) or
			(nvl(ie_glosa_w,'') not in ('T','E','R','B','H','Z','P',''))) and
			(ie_tipo_convenio_w <> 1) and
			(ie_calc_glosa_atend_w = 'N')) or
			((ie_calc_glosa_atend_w = 'S') 	and
			 (ie_tipo_convenio_w <> 1) 	and
			 ((nvl(ie_glosa_w,'') not in ('T','G','P','R')) or (ie_glosa_w is null)))) and
			(IE_CALCULA_GLOSA_w = 'N' or
			(IE_CALCULA_GLOSA_w = 'S' and
			(nvl(ie_glosa_w,'') not in ('P','R') or (ie_glosa_w is null))))) then
			vl_procedimento_w	:= 0;
		else
			if	(ie_glosa_w	in ('P','R') and
				IE_CALCULA_GLOSA_w = 'S') then
				Define_Preco_Procedimento(
					CD_ESTABELECIMENTO_w,
					cd_convenio_ww,
					cd_categoria_ww,
					dt_inicio_agendamento_w,
					:new.cd_procedimento,
					0,
					nvl(ie_tipo_Atendimento_w,0),
					0,
					null,--medico
					0,
					0,
					0,
					:new.nr_seq_proc_interno,
					null,--usuario convenio
					cd_plano_w,
					0,
					0,
					null,
					VL_PROCEDIMENTO_w,
					vl_custo_operacional_w,
					vl_anestesista_w,
					vl_medico_w,
					vl_auxiliares_w,
					vl_materiais_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					qt_pontos_w,
					CD_EDICAO_AMB_w,
					ds_aux_w,
					nr_seq_ajuste_proc_w,
					0,
					null,
					0,
					'N',
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);

				if	(ie_glosa_w = 'P') then
					vl_glosa_w:= vl_procedimento_w * pr_glosa_w / 100;

					/* ROTINA DE ARREDONDAMENTO, USADO PELO CONV�NIO IPE   --->>    INICIO  <<----- */
					begin
					select 	nvl(max(ie_regra_arredondamento_tx),'N')
					into	ie_regra_arredondamento_tx_w
					from 	parametro_faturamento
					where 	cd_estabelecimento = cd_estabelecimento_w;
					exception
						when others then
							ie_regra_arredondamento_tx_w:= 'N';
					end;

					if	(ie_regra_arredondamento_tx_w = 'S')then

						select	max(ie_arredondamento)
						into	ie_tipo_rounded_w
						from	convenio_estabelecimento
						where	cd_convenio	  	= cd_convenio_ww
						and	cd_estabelecimento	= cd_estabelecimento_w;

						if	(ie_tipo_rounded_w = 'R') then

							select 	obter_regra_arredondamento(cd_convenio_ww, cd_categoria_w, :new.cd_procedimento, :new.ie_origem_proced, cd_estabelecimento_w,
									nvl(sysdate,sysdate), 'P', 1)
							into	ie_tipo_rounded_w
							from 	dual;

							ie_regra_arred_IPE_w:= 'S';

						end if;

						if	(ie_tipo_rounded_w is not null) and
							(ie_regra_arred_IPE_w = 'S') then

							arredondamento(vl_glosa_w, 2, ie_tipo_rounded_w);

						else
							ie_regra_arred_IPE_w:= 'N';
						end if;

					end if;
					if	(vl_glosa_w	> 0)  then
						vl_procedimento_w	:= vl_procedimento_w - vl_glosa_w;
					end if;
				else
					vl_procedimento_w:= vl_glosa_w;
				end if;
			else
				if	(ie_tipo_convenio_w <> 1) then
					select	max(cd_convenio_partic),
						max(cd_categoria_partic)
					into	cd_convenio_w,
						cd_categoria_w
					from	parametro_faturamento
					where	cd_estabelecimento	= cd_estabelecimento_w;

					if (ie_conv_cat_regra_w = 'S') then
						cd_convenio_w	:= nvl(cd_convenio_glosa_w,cd_convenio_w);
						cd_categoria_w	:= nvl(cd_categoria_glosa_w,cd_categoria_w);
					end if;

					if	(cd_convenio_ww is not null) then
						select	max(cd_plano)
						into	cd_plano_w
						from	convenio_plano
						where	cd_convenio	= cd_convenio_w
						and	ie_situacao	= 'A';
					end if;
				end if;

				if	(cd_convenio_w is null) then
					cd_convenio_w	:= cd_convenio_ww;
					cd_Categoria_w	:= cd_categoria_ww;
					cd_plano_w	:= cd_plano_ww;
				end if;

				Define_Preco_Procedimento(
					CD_ESTABELECIMENTO_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_inicio_agendamento_w,
					:new.cd_procedimento,
					0,
					nvl(ie_tipo_Atendimento_w,0),
					0,
					null,--medico
					0,
					0,
					0,
					:new.nr_seq_proc_interno,
					null,--usuario convenio
					cd_plano_w,
					0,
					0,
					null,
					VL_PROCEDIMENTO_w,
					vl_custo_operacional_w,
					vl_anestesista_w,
					vl_medico_w,
					vl_auxiliares_w,
					vl_materiais_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					vl_aux_w,
					qt_pontos_w,
					CD_EDICAO_AMB_w,
					ds_aux_w,
					nr_seq_ajuste_proc_w,
					0,
					null,
					0,
					'N',
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);
			end if;
		end if;

		:new.ie_Regra			:= ie_Regra_w;
		:new.ie_glosa			:= ie_glosa_w;
		:new.vl_item			:= vl_procedimento_w;
		:new.vl_custo_operacional	:= vl_custo_operacional_w;
		:new.vl_anestesista		:= vl_anestesista_w;
		:new.vl_medico			:= vl_medico_w;
		:new.vl_auxiliares		:= vl_auxiliares_w;
		:new.vl_materiais		:= vl_materiais_w;
		:new.nr_seq_regra		:= nr_seq_regra_w;
end if;


end;
/


ALTER TABLE TASY.AGEINT_EXAME_LAB ADD (
  CONSTRAINT AGIEXLB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_EXAME_LAB ADD (
  CONSTRAINT AGIEXLB_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGEINT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA),
  CONSTRAINT AGIEXLB_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGIEXLB_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGIEXLB_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT AGIEXLB_PRESPRO_FK 
 FOREIGN KEY (NR_SEQ_INTERNO_PRESCR) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_SEQ_INTERNO));

GRANT SELECT ON TASY.AGEINT_EXAME_LAB TO NIVEL_1;

