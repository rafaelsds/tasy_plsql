ALTER TABLE TASY.MODELO_NOTA_FISCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MODELO_NOTA_FISCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.MODELO_NOTA_FISCAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MODELO_NF         VARCHAR2(80 BYTE)        NOT NULL,
  DS_MODELO_NF         VARCHAR2(80 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  SG_MODELO            VARCHAR2(6 BYTE),
  IE_ENTRADA_SAIDA     VARCHAR2(1 BYTE)         NOT NULL,
  IE_EXIGE_DANFE       VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MODNOFI_PK ON TASY.MODELO_NOTA_FISCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MODELO_NOTA_FISCAL_tp  after update ON TASY.MODELO_NOTA_FISCAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MODELO_NF,1,4000),substr(:new.CD_MODELO_NF,1,4000),:new.nm_usuario,nr_seq_w,'CD_MODELO_NF',ie_log_w,ds_w,'MODELO_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MODELO_NF,1,4000),substr(:new.DS_MODELO_NF,1,4000),:new.nm_usuario,nr_seq_w,'DS_MODELO_NF',ie_log_w,ds_w,'MODELO_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_DANFE,1,4000),substr(:new.IE_EXIGE_DANFE,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_DANFE',ie_log_w,ds_w,'MODELO_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_MODELO,1,4000),substr(:new.SG_MODELO,1,4000),:new.nm_usuario,nr_seq_w,'SG_MODELO',ie_log_w,ds_w,'MODELO_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENTRADA_SAIDA,1,4000),substr(:new.IE_ENTRADA_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENTRADA_SAIDA',ie_log_w,ds_w,'MODELO_NOTA_FISCAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MODELO_NOTA_FISCAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MODELO_NOTA_FISCAL ADD (
  CONSTRAINT MODNOFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MODELO_NOTA_FISCAL TO NIVEL_1;

