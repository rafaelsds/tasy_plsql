ALTER TABLE TASY.DUV_ASS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DUV_ASS CASCADE CONSTRAINTS;

CREATE TABLE TASY.DUV_ASS
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_MENSAGEM              NUMBER(10)       NOT NULL,
  IE_SEXO                      NUMBER(1),
  NR_FAIXA_ETARIA              NUMBER(1),
  IE_TRAUMA_INALACAO           NUMBER(1),
  IE_QUEIMADURA_TERCEIRO_GRAU  NUMBER(1),
  IE_PERCENT_CORPO_QUEIMADO    NUMBER(2),
  QT_SCORE_TOTAL               NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DUVASS_DUVMENS_FK_I ON TASY.DUV_ASS
(NR_SEQ_MENSAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DUVASS_PK ON TASY.DUV_ASS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DUV_ASS_BEFORE_UPDATE
before insert or update ON TASY.DUV_ASS for each row
declare

vl_soma_w	number(2);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	return;
end if;

vl_soma_w	:=	nvl(:new.IE_SEXO,0) + nvl(:new.NR_FAIXA_ETARIA,0) + nvl(:new.IE_TRAUMA_INALACAO,0) + nvl(:new.IE_QUEIMADURA_TERCEIRO_GRAU,0) + nvl(:new.IE_PERCENT_CORPO_QUEIMADO,0);

:new.QT_SCORE_TOTAL := vl_soma_w;

end;
/


ALTER TABLE TASY.DUV_ASS ADD (
  CONSTRAINT DUVASS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DUV_ASS ADD (
  CONSTRAINT DUVASS_DUVMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSAGEM) 
 REFERENCES TASY.DUV_MENSAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DUV_ASS TO NIVEL_1;

