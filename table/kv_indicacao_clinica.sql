ALTER TABLE TASY.KV_INDICACAO_CLINICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.KV_INDICACAO_CLINICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.KV_INDICACAO_CLINICA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_HOSPITAL            VARCHAR2(255 BYTE),
  DS_RESULTADO_MEDICO    VARCHAR2(255 BYTE),
  DS_DIAGNOSTICO         VARCHAR2(255 BYTE),
  DT_REGISTRO            DATE,
  DS_RESULTADO_EXAME     VARCHAR2(255 BYTE),
  IE_ACIDENTE            VARCHAR2(1 BYTE),
  IE_LEI                 VARCHAR2(1 BYTE),
  IE_MEDICO_HOSPITAL     VARCHAR2(1 BYTE),
  IE_EMERGENCIA          VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  DS_MEDICAMENTO         VARCHAR2(255 BYTE),
  DS_INDICACAO           VARCHAR2(255 BYTE),
  CD_MEDICO              VARCHAR2(10 BYTE),
  NR_RQE                 VARCHAR2(20 BYTE),
  NR_BSNR                VARCHAR2(25 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.KVINDCL_ATEPACI_FK_I ON TASY.KV_INDICACAO_CLINICA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVINDCL_PESFISI_FK_I ON TASY.KV_INDICACAO_CLINICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.KVINDCL_PK ON TASY.KV_INDICACAO_CLINICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVINDCL_PROFISS_FK_I ON TASY.KV_INDICACAO_CLINICA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.KV_INDICACAO_CLINICA_UPDATE
before update ON TASY.KV_INDICACAO_CLINICA for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into is_rule_exists_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO  = 'KV2'
    and   obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
		nvl(b.nr_sequencia, 0)
    into qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO  = 'KV2'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


		wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
	end if;
  end if;
end;
/


ALTER TABLE TASY.KV_INDICACAO_CLINICA ADD (
  CONSTRAINT KVINDCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.KV_INDICACAO_CLINICA ADD (
  CONSTRAINT KVINDCL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT KVINDCL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT KVINDCL_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.KV_INDICACAO_CLINICA TO NIVEL_1;

