ALTER TABLE TASY.ESCALA_NTISS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NTISS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NTISS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  DT_LIBERACAO               DATE,
  DT_AVALIACAO               DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  IE_VENTILACAO              VARCHAR2(1 BYTE),
  QT_PTO_VENT                NUMBER(2)          NOT NULL,
  IE_SURFACTANTE             VARCHAR2(1 BYTE),
  QT_PTO_SURFACTANTE         NUMBER(2)          NOT NULL,
  IE_INTUBACAO_TRAQ          VARCHAR2(1 BYTE),
  QT_PTO_INTUBACAO           NUMBER(2)          NOT NULL,
  IE_TRAQUEOSTOMIA           VARCHAR2(1 BYTE),
  QT_PTO_TRAQUEOSTOMIA       NUMBER(2)          NOT NULL,
  IE_ECMO                    VARCHAR2(1 BYTE),
  QT_PTO_ECMO                NUMBER(2)          NOT NULL,
  IE_USO_ATB                 VARCHAR2(1 BYTE),
  QT_PTO_ATB                 NUMBER(2)          NOT NULL,
  IE_USO_DIURETICO           VARCHAR2(1 BYTE),
  QT_PTO_DIURETICO           NUMBER(2)          NOT NULL,
  IE_ANTICONVULSIVANTE       VARCHAR2(1 BYTE),
  QT_PTO_ANTICONVULSIVANTE   NUMBER(2)          NOT NULL,
  IE_AMINOFILINA             VARCHAR2(1 BYTE),
  QT_PTO_AMINOFILINA         NUMBER(2)          NOT NULL,
  IE_MEDIC_NAO_PROG          VARCHAR2(1 BYTE),
  QT_PTO_MEDIC_NAO_PROG      NUMBER(2)          NOT NULL,
  IE_CORTICOIDE              VARCHAR2(1 BYTE),
  QT_PTO_CORTICOIDE          NUMBER(2)          NOT NULL,
  IE_RESINA                  VARCHAR2(1 BYTE),
  QT_PTO_RESINA              NUMBER(2)          NOT NULL,
  IE_TRAT_METABOLICO         VARCHAR2(1 BYTE),
  QT_PTO_TRAT_META           NUMBER(2)          NOT NULL,
  IE_ALIMENT_GAVAGEM         VARCHAR2(1 BYTE),
  QT_PTO_ALIMENT_GAVAGEM     NUMBER(2)          NOT NULL,
  IE_FOTOTERAPIA             VARCHAR2(1 BYTE),
  QT_PTO_FOTOTERAPIA         NUMBER(2)          NOT NULL,
  IE_EMULSAO_LIPIDICA        VARCHAR2(1 BYTE),
  QT_PTO_EMULSAO             NUMBER(2)          NOT NULL,
  IE_INFUSAO_AMINOACIDO      VARCHAR2(1 BYTE),
  QT_PTO_INFUSAO_AMINO       NUMBER(2)          NOT NULL,
  IE_ADM_INSULINA            VARCHAR2(1 BYTE),
  QT_PTO_ADM_INSULINA        NUMBER(2)          NOT NULL,
  IE_INFUSAO_POTASSIO        VARCHAR2(1 BYTE),
  QT_PTO_INFUSAO_POT         NUMBER(2)          NOT NULL,
  IE_TRANSPORTE              VARCHAR2(1 BYTE),
  QT_PTO_TRANSPORTE          NUMBER(2)          NOT NULL,
  IE_DIALISE                 VARCHAR2(1 BYTE),
  QT_PTO_DIALISE             NUMBER(2)          NOT NULL,
  IE_DRENAGEM                VARCHAR2(1 BYTE),
  QT_PTO_DRENAGEM            NUMBER(2)          NOT NULL,
  IE_TORACOCENTESE           VARCHAR2(1 BYTE),
  QT_PTO_TORACOCENTESE       NUMBER(2)          NOT NULL,
  IE_PERICARDIO              VARCHAR2(1 BYTE),
  QT_PTO_PERICARDIO          NUMBER(2)          NOT NULL,
  IE_OPERACAO                VARCHAR2(1 BYTE),
  QT_PTO_OPERACAO            NUMBER(2)          NOT NULL,
  IE_INDOMETACINA            VARCHAR2(1 BYTE),
  QT_PTO_INDOMETACINA        NUMBER(2)          NOT NULL,
  IE_EXPANSAO                VARCHAR2(1 BYTE),
  QT_PTO_EXPANSAO            NUMBER(2)          NOT NULL,
  IE_VASOPRESSOR             VARCHAR2(1 BYTE),
  QT_PTO_VASOPRESSOR         NUMBER(2)          NOT NULL,
  IE_RESSUCITACAO            VARCHAR2(1 BYTE),
  QT_PTO_RESSUCITACAO        NUMBER(2)          NOT NULL,
  IE_MARCAPASSO              VARCHAR2(1 BYTE),
  QT_PTO_MARCAPASSO          NUMBER(2)          NOT NULL,
  IE_SINAL_VITAL             VARCHAR2(1 BYTE),
  QT_PTO_SINAL_VITAL         NUMBER(2)          NOT NULL,
  IE_COLETA_SANGUE           VARCHAR2(1 BYTE),
  QT_PTO_COLETA              NUMBER(2)          NOT NULL,
  IE_MONITOR_CARDIO          VARCHAR2(1 BYTE),
  QT_PTO_MONIT_CARDIO        NUMBER(2)          NOT NULL,
  IE_AMB_TERMOREGULADO       VARCHAR2(1 BYTE),
  QT_PTO_TERMOREGULADO       NUMBER(2)          NOT NULL,
  IE_OXIMETRIA               VARCHAR2(1 BYTE),
  QT_PTO_OXIMETRIA           NUMBER(2)          NOT NULL,
  IE_MONIT_PA                VARCHAR2(1 BYTE),
  QT_PTO_MONIT_PA            NUMBER(2)          NOT NULL,
  IE_MONIT_PVC               VARCHAR2(1 BYTE),
  QT_PTO_MONIT_PVC           NUMBER(2)          NOT NULL,
  IE_CATETER                 VARCHAR2(1 BYTE),
  QT_PTO_CATETER             NUMBER(2)          NOT NULL,
  IE_BALANCO_HIDRICO         VARCHAR2(1 BYTE),
  QT_PTO_BALANCO_HIDRICO     NUMBER(2)          NOT NULL,
  IE_GAMAGLOBULINA           VARCHAR2(1 BYTE),
  QT_PTO_GAMAGLOBULINA       NUMBER(2)          NOT NULL,
  IE_EXSANGUINEOTRA          VARCHAR2(1 BYTE),
  QT_PTO_EXSANGUINEOTRA      NUMBER(2)          NOT NULL,
  IE_EXSANGUINEOTRA_PARCIAL  VARCHAR2(1 BYTE),
  QT_PTO_EXSANGUINEO_PAR     NUMBER(2)          NOT NULL,
  IE_TRANSFUSAO_PLAQ         VARCHAR2(1 BYTE),
  QT_PTO_TRANSF_PLAQ         NUMBER(2)          NOT NULL,
  IE_TRANSFUSAO_PLASMA       VARCHAR2(1 BYTE),
  QT_PTO_TRANSF_PLASMA       NUMBER(2)          NOT NULL,
  IE_ACESSO_ARTERIAL         VARCHAR2(1 BYTE),
  QT_PTO_ACESSO_ARTERIAL     NUMBER(2)          NOT NULL,
  IE_ACESSO_VENOSO_PER       VARCHAR2(1 BYTE),
  QT_PTO_ACESSO_VENOSO_PER   NUMBER(2)          NOT NULL,
  IE_ACESSO_VENOSO_CENTRAL   VARCHAR2(1 BYTE),
  QT_PTO_ACESSO_VENOSO_CEN   NUMBER(2)          NOT NULL,
  IE_HEMOTRANSF_MENOR        VARCHAR2(1 BYTE),
  QT_PTO_HEMOTRANSF_MENOR    NUMBER(2)          NOT NULL,
  QT_PTO_TOTAL               NUMBER(10)         NOT NULL,
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCNTIS_ATEPACI_FK_I ON TASY.ESCALA_NTISS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNTIS_I1 ON TASY.ESCALA_NTISS
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNTIS_PERFIL_FK_I ON TASY.ESCALA_NTISS
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCNTIS_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCNTIS_PESFISI_FK_I ON TASY.ESCALA_NTISS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCNTIS_PK ON TASY.ESCALA_NTISS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_ntiss_atual
before insert or update ON TASY.ESCALA_NTISS for each row
declare
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Obter pontua��o da ventila��o */
if	(:new.ie_ventilacao = '1') then
	:new.qt_pto_vent := 1;
elsif	(:new.ie_ventilacao = '2') then
	:new.qt_pto_vent := 2;
elsif	(:new.ie_ventilacao = '3') then
	:new.qt_pto_vent := 3;
elsif	(:new.ie_ventilacao in ('4','5')) then
	:new.qt_pto_vent := 4;
else
	:new.qt_pto_vent := 0;
end if;

if	(:new.ie_surfactante = 'S') then
	:new.qt_pto_surfactante := 1;
elsif	(:new.ie_surfactante = 'N') then
	:new.qt_pto_surfactante := 0;
end if;

if	(:new.ie_intubacao_traq = 'S') then
	:new.qt_pto_intubacao := 2;
elsif	(:new.ie_intubacao_traq = 'N') then
	:new.qt_pto_intubacao := 0;
end if;

if	(:new.ie_traqueostomia in ('1','2')) then
	:new.qt_pto_traqueostomia := 1;
else
	:new.qt_pto_traqueostomia := 0;
end if;

if	(:new.ie_ecmo = 'S') then
	:new.qt_pto_ecmo := 4;
elsif	(:new.ie_ecmo = 'N') then
	:new.qt_pto_ecmo := 0;
end if;

/* Obter pontua��o dos antibi�ticos */
if	(:new.ie_uso_atb = '1') then
	:new.qt_pto_atb := 1;
elsif	(:new.ie_uso_atb = '2') then
	:new.qt_pto_atb := 2;
else
	:new.qt_pto_atb := 0;
end if;

/* Obter pontua��o dos diur�ticos */
if	(:new.ie_uso_diuretico = '1') then
	:new.qt_pto_diuretico := 1;
elsif	(:new.ie_uso_diuretico = '2') then
	:new.qt_pto_diuretico := 2;
end if;

if	(:new.ie_anticonvulsivante = 'S') then
	:new.qt_pto_anticonvulsivante := 1;
elsif	(:new.ie_anticonvulsivante = 'N') then
	:new.qt_pto_anticonvulsivante := 0;
end if;

if	(:new.ie_aminofilina = 'S') then
	:new.qt_pto_aminofilina := 1;
elsif	(:new.ie_aminofilina = 'N') then
	:new.qt_pto_aminofilina := 0;
end if;

if	(:new.ie_medic_nao_prog = 'S') then
	:new.qt_pto_medic_nao_prog := 1;
elsif	(:new.ie_medic_nao_prog = 'N') then
	:new.qt_pto_medic_nao_prog := 0;
end if;

if	(:new.ie_corticoide = 'S') then
	:new.qt_pto_corticoide := 1;
elsif	(:new.ie_corticoide = 'N') then
	:new.qt_pto_corticoide := 0;
end if;

if	(:new.ie_resina = 'S') then
	:new.qt_pto_resina := 3;
elsif	(:new.ie_resina = 'N') then
	:new.qt_pto_resina := 0;
end if;

if	(:new.ie_trat_metabolico = 'S') then
	:new.qt_pto_trat_meta := 3;
elsif	(:new.ie_trat_metabolico = 'N') then
	:new.qt_pto_trat_meta := 0;
end if;

if	(:new.ie_aliment_gavagem = 'S') then
	:new.qt_pto_aliment_gavagem := 1;
elsif	(:new.ie_aliment_gavagem = 'N') then
	:new.qt_pto_aliment_gavagem := 0;
end if;

if	(:new.ie_fototerapia = 'S') then
	:new.qt_pto_fototerapia := 1;
elsif	(:new.ie_fototerapia = 'N') then
	:new.qt_pto_fototerapia := 0;
end if;

if	(:new.ie_emulsao_lipidica = 'S') then
	:new.qt_pto_emulsao := 1;
elsif	(:new.ie_emulsao_lipidica = 'N') then
	:new.qt_pto_emulsao := 0;
end if;

if	(:new.ie_infusao_aminoacido = 'S') then
	:new.qt_pto_infusao_amino := 1;
elsif	(:new.ie_infusao_aminoacido = 'N') then
	:new.qt_pto_infusao_amino := 0;
end if;

if	(:new.ie_adm_insulina = 'S') then
	:new.qt_pto_adm_insulina := 2;
elsif	(:new.ie_adm_insulina = 'N') then
	:new.qt_pto_adm_insulina := 0;
end if;

if	(:new.ie_infusao_potassio = 'S') then
	:new.qt_pto_infusao_pot := 3;
elsif	(:new.ie_infusao_potassio = 'N') then
	:new.qt_pto_infusao_pot := 0;
end if;

if	(:new.ie_transporte = 'S') then
	:new.qt_pto_transporte := 2;
elsif	(:new.ie_transporte = 'N') then
	:new.qt_pto_transporte := 0;
end if;

if	(:new.ie_dialise = 'S') then
	:new.qt_pto_dialise := 4;
elsif	(:new.ie_dialise = 'N') then
	:new.qt_pto_dialise := 0;
end if;

/* Obter pontua��o drenagem */
if	(:new.ie_drenagem = '1') then
	:new.qt_pto_drenagem := 2;
elsif	(:new.ie_drenagem = '2') then
	:new.qt_pto_drenagem := 3;
else
	:new.qt_pto_drenagem := 0;
end if;

if	(:new.ie_toracocentese = 'S') then
	:new.qt_pto_toracocentese := 3;
elsif	(:new.ie_toracocentese = 'N') then
	:new.qt_pto_toracocentese := 0;
end if;

/* Obter pontua��o do peric�rdio */
if	(:new.ie_pericardio in ('1','2')) then
	:new.qt_pto_pericardio := 4;
else
	:new.qt_pto_pericardio := 0;
end if;


/* Obter pontua��o da opera��o */
if	(:new.ie_operacao = '1') then
	:new.qt_pto_operacao := 2;
elsif	(:new.ie_operacao = '2') then
	:new.qt_pto_operacao := 4;
end if;

if	(:new.ie_indometacina = 'S') then
	:new.qt_pto_indometacina := 1;
elsif	(:new.ie_indometacina = 'N') then
	:new.qt_pto_indometacina := 0;
end if;


/* Obter pontua��o da expans�o de vol */
if	(:new.ie_expansao = '1') then
	:new.qt_pto_expansao := 1;
elsif	(:new.ie_expansao = '2') then
	:new.qt_pto_expansao := 3;
else
	:new.qt_pto_expansao := 0;
end if;

/* Obter pontua��o de vasopressor */
if	(:new.ie_vasopressor = '1') then
	:new.qt_pto_vasopressor := 2;
elsif	(:new.ie_vasopressor = '2') then
	:new.qt_pto_vasopressor := 3;
else
	:new.qt_pto_vasopressor := 0;
end if;

if	(:new.ie_ressucitacao = 'S') then
	:new.qt_pto_ressucitacao := 4;
elsif	(:new.ie_ressucitacao = 'N') then
	:new.qt_pto_ressucitacao := 0;
end if;

/* Obter pontua��o marcapasso */
if	(:new.ie_marcapasso = '1') then
	:new.qt_pto_marcapasso := 3;
elsif	(:new.ie_marcapasso = '2') then
	:new.qt_pto_marcapasso := 4;
else
	:new.qt_pto_marcapasso := 0;
end if;

if	(:new.ie_sinal_vital = 'S') then
	:new.qt_pto_sinal_vital := 1;
elsif	(:new.ie_sinal_vital = 'N') then
	:new.qt_pto_sinal_vital := 0;
end if;

/* Obter pontua��o coleta de sague */
if	(:new.ie_coleta_sangue = '1') then
	:new.qt_pto_coleta := 1;
elsif	(:new.ie_coleta_sangue = '2') then
	:new.qt_pto_coleta := 2;
end if;

if	(:new.ie_monitor_cardio = 'S') then
	:new.qt_pto_monit_cardio := 1;
elsif	(:new.ie_monitor_cardio = 'N') then
	:new.qt_pto_monit_cardio := 0;
end if;

if	(:new.ie_amb_termoregulado = 'S') then
	:new.qt_pto_termoregulado := 1;
elsif	(:new.ie_amb_termoregulado = 'N') then
	:new.qt_pto_termoregulado := 0;
end if;

if	(:new.ie_oximetria = 'S') then
	:new.qt_pto_oximetria := 1;
elsif	(:new.ie_oximetria = 'N') then
	:new.qt_pto_oximetria := 0;
end if;

if	(:new.ie_monit_pa = 'S') then
	:new.qt_pto_monit_pa := 1;
elsif	(:new.ie_monit_pa = 'N') then
	:new.qt_pto_monit_pa := 0;
end if;

if	(:new.ie_monit_pvc = 'S') then
	:new.qt_pto_monit_pvc := 1;
elsif	(:new.ie_monit_pvc = 'N') then
	:new.qt_pto_monit_pvc := 0;
end if;

if	(:new.ie_cateter = 'S') then
	:new.qt_pto_cateter := 1;
elsif	(:new.ie_cateter = 'N') then
	:new.qt_pto_cateter := 0;
end if;

if	(:new.ie_balanco_hidrico = 'S') then
	:new.qt_pto_balanco_hidrico := 1;
elsif	(:new.ie_balanco_hidrico = 'N') then
	:new.qt_pto_balanco_hidrico := 0;
end if;

if	(:new.ie_gamaglobulina = 'S') then
	:new.qt_pto_gamaglobulina := 1;
elsif	(:new.ie_gamaglobulina = 'N') then
	:new.qt_pto_gamaglobulina := 0;
end if;

if	(:new.ie_exsanguineotra = 'S') then
	:new.qt_pto_exsanguineotra := 3;
elsif	(:new.ie_exsanguineotra = 'N') then
	:new.qt_pto_exsanguineotra := 0;
end if;

if	(:new.ie_exsanguineotra_parcial = 'S') then
	:new.qt_pto_exsanguineo_par := 2;
elsif	(:new.ie_exsanguineotra_parcial = 'N') then
	:new.qt_pto_exsanguineo_par := 0;
end if;

if	(:new.ie_hemotransf_menor = '1') then
	:new.qt_pto_hemotransf_menor := 2;
elsif	(:new.ie_hemotransf_menor = '2') then
	:new.qt_pto_hemotransf_menor := 3;
else
	:new.qt_pto_hemotransf_menor := 0;
end if;


if	(:new.ie_transfusao_plaq = 'S') then
	:new.qt_pto_transf_plaq := 3;
elsif	(:new.ie_transfusao_plaq = 'N') then
	:new.qt_pto_transf_plaq := 0;
end if;

if	(:new.ie_transfusao_plasma = 'S') then
	:new.qt_pto_transf_plasma := 3;
elsif	(:new.ie_transfusao_plasma = 'N') then
	:new.qt_pto_transf_plasma := 0;
end if;

if	(:new.ie_acesso_venoso_per = 'S') then
	:new.qt_pto_acesso_venoso_per := 1;
elsif	(:new.ie_acesso_venoso_per = 'N') then
	:new.qt_pto_acesso_venoso_per := 0;
end if;

if	(:new.ie_acesso_arterial = 'S') then
	:new.qt_pto_acesso_arterial := 2;
elsif	(:new.ie_acesso_arterial = 'N') then
	:new.qt_pto_acesso_arterial := 0;
end if;

if	(:new.ie_acesso_venoso_central = 'S') then
	:new.qt_pto_acesso_venoso_cen := 2;
elsif	(:new.ie_acesso_venoso_central = 'N') then
	:new.qt_pto_acesso_venoso_cen := 0;
end if;

:new.qt_pto_total :=	nvl(:new.qt_pto_vent,0)			+	nvl(:new.qt_pto_surfactante,0)	+	nvl(:new.qt_pto_intubacao,0)		+
			nvl(:new.qt_pto_traqueostomia,0)	+	nvl(:new.qt_pto_ecmo,0)		+
			nvl(:new.qt_pto_atb,0)			+	nvl(:new.qt_pto_diuretico,0)	+	nvl(:new.qt_pto_anticonvulsivante,0)	+
			nvl(:new.qt_pto_aminofilina,0)		+	nvl(:new.qt_pto_medic_nao_prog,0)+	nvl(:new.qt_pto_corticoide,0)		+
			nvl(:new.qt_pto_resina,0)		+	nvl(:new.qt_pto_trat_meta,0)	+	nvl(:new.qt_pto_aliment_gavagem,0)	+
			nvl(:new.qt_pto_fototerapia,0)		+	nvl(:new.qt_pto_emulsao,0)	+	nvl(:new.qt_pto_infusao_amino,0)	+
			nvl(:new.qt_pto_adm_insulina,0)		+	nvl(:new.qt_pto_infusao_pot,0)	+	nvl(:new.qt_pto_transporte,0)		+
			nvl(:new.qt_pto_dialise,0)		+	nvl(:new.qt_pto_drenagem,0)	+	nvl(:new.qt_pto_toracocentese,0)	+
			nvl(:new.qt_pto_pericardio,0)		+	nvl(:new.qt_pto_operacao,0)	+	nvl(:new.qt_pto_indometacina,0)		+
			nvl(:new.qt_pto_expansao,0)		+	nvl(:new.qt_pto_vasopressor,0)	+	nvl(:new.qt_pto_ressucitacao,0)		+
			nvl(:new.qt_pto_marcapasso,0)		+	nvl(:new.qt_pto_sinal_vital,0)	+	nvl(:new.qt_pto_coleta,0)		+
			nvl(:new.qt_pto_monit_cardio,0)		+	nvl(:new.qt_pto_termoregulado,0)+	nvl(:new.qt_pto_oximetria,0)		+
			nvl(:new.qt_pto_monit_pa,0)		+	nvl(:new.qt_pto_monit_pvc,0)	+	nvl(:new.qt_pto_cateter,0)		+
			nvl(:new.qt_pto_balanco_hidrico,0)	+	nvl(:new.qt_pto_gamaglobulina,0)+	nvl(:new.qt_pto_exsanguineotra,0)	+
			nvl(:new.qt_pto_exsanguineo_par,0)	+	nvl(:new.qt_pto_hemotransf_menor,0)+	nvl(:new.qt_pto_transf_plaq,0)		+	nvl(:new.qt_pto_transf_plasma,0)+	nvl(:new.qt_pto_acesso_venoso_per,0)	+
			nvl(:new.qt_pto_acesso_arterial,0)	+	nvl(:new.qt_pto_acesso_venoso_cen,0);

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_NTISS ADD (
  CONSTRAINT ESCNTIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NTISS ADD (
  CONSTRAINT ESCNTIS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCNTIS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCNTIS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_NTISS TO NIVEL_1;

