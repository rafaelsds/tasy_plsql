ALTER TABLE TASY.PACIENTE_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_SETOR
(
  NR_SEQ_PACIENTE          NUMBER(10)           NOT NULL,
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  CD_SETOR_ATENDIMENTO     NUMBER(5)            NOT NULL,
  CD_PROTOCOLO             NUMBER(10),
  NR_SEQ_MEDICACAO         NUMBER(6),
  IE_STATUS                VARCHAR2(1 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_MEDICO_RESP           VARCHAR2(10 BYTE),
  IE_TERAPIA_RENAL_SUBST   NUMBER(1),
  CD_CONVENIO              NUMBER(5),
  CD_MOTIVO_INATIV         NUMBER(10),
  QT_DIAS_INTERVALO        NUMBER(3),
  NR_CICLOS                NUMBER(3),
  DT_PROTOCOLO             DATE,
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  QT_PESO                  NUMBER(6,3),
  QT_ALTURA                NUMBER(3),
  QT_REDUTOR_SC            NUMBER(15,2),
  QT_AUC                   NUMBER(4,1),
  QT_CREATININA            NUMBER(5,2),
  IE_L_DL_CREATININA       NUMBER(1),
  QT_CLEARANCE_CREATININA  NUMBER(14,2),
  QT_MG_CARBOPLATINA       NUMBER(15,4),
  DT_INICIO_TRAT           DATE,
  DT_FINAL_TRAT            DATE,
  IE_PROTOCOLO_LIVRE       VARCHAR2(1 BYTE),
  DS_PROTOCOLO_LIVRE       VARCHAR2(255 BYTE),
  NR_SEQ_LOCAL             NUMBER(10),
  NR_SEQ_ACESSO            NUMBER(10),
  IE_SUPORTE               VARCHAR2(1 BYTE),
  CD_PERFIL_ATIVO          NUMBER(5),
  QT_TEMPO_MEDIC           NUMBER(10),
  IE_MODALIDADE_TRAT       VARCHAR2(3 BYTE),
  IE_DEFINICAO_TRAT        VARCHAR2(3 BYTE),
  PR_REDUCAO               VARCHAR2(3 BYTE),
  IE_FINALIDADE            VARCHAR2(3 BYTE),
  NR_SEQ_LAUDO_SUS         NUMBER(10),
  NR_SEQ_REFERENCIA        NUMBER(10),
  NR_SEQ_LOCO_REGIONAL     NUMBER(10),
  DT_LIB_AUTORIZADOR       DATE,
  NR_SEQ_DEFINIDOR         NUMBER(10),
  IE_FORMULA_SUP_CORPOREA  VARCHAR2(1 BYTE),
  IE_REGRA_DISP            VARCHAR2(1 BYTE),
  QT_REDUTOR_CLCR          NUMBER(15,2),
  DS_MOTIVO_INATIVACAO     VARCHAR2(255 BYTE),
  IE_ALT_MEDICAMENTO       VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_UTC                   VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO         VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO       VARCHAR2(50 BYTE),
  IE_INF_RELEVANTE         NUMBER(4),
  IE_AGENDAMENTO_MULTIPLO  VARCHAR2(5 BYTE),
  NR_GRUPO_AGENDAMENTO     NUMBER(5),
  NR_SEQ_ATEND_CONS_PEPA   NUMBER(10),
  IE_PROCEDIMENTO_APOIO    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACSETO_ATCONSPEPA_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSETO_CANLORE_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_LOCO_REGIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSETO_CONVENI_FK_I ON TASY.PACIENTE_SETOR
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_DEFONCO_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_DEFINIDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_DEFONCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_ESTABEL_FK_I ON TASY.PACIENTE_SETOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_I1 ON TASY.PACIENTE_SETOR
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSETO_MEDICO_FK_I ON TASY.PACIENTE_SETOR
(CD_MEDICO_RESP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSETO_MOTPACON_FK_I ON TASY.PACIENTE_SETOR
(CD_MOTIVO_INATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_MOTPACON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_PERFIL_FK_I ON TASY.PACIENTE_SETOR
(CD_PERFIL_ATIVO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_PESFISI_FK_I ON TASY.PACIENTE_SETOR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACSETO_PK ON TASY.PACIENTE_SETOR
(NR_SEQ_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSETO_PROMEDI_FK_I ON TASY.PACIENTE_SETOR
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_PROTOCO_FK_I ON TASY.PACIENTE_SETOR
(CD_PROTOCOLO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_QTACESS_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_QTACESS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_QTLOCAL_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_QTLOCAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_REFBIBL_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_REFBIBL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSETO_SETATEND_FK_I ON TASY.PACIENTE_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSETO_SUSLAPA_FK_I ON TASY.PACIENTE_SETOR
(NR_SEQ_LAUDO_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSETO_SUSLAPA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_SETOR_ATUAL
before insert or update ON TASY.PACIENTE_SETOR for each row
declare
qt_tempo_medic_w	number(10);
nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(10);
nr_atendimento_W        number(10);
ie_cancelar_agend_w	varchar2(1);
nm_usuario_w		varchar2(15);

cursor C01 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	= 'IPO'
	and	nvl(cd_protocolo,nvl(:new.cd_protocolo,0)) = nvl(:new.cd_protocolo,0)
	and	nvl(nr_seq_protocolo,nvl(:new.nr_seq_medicacao,0)) = nvl(:new.nr_seq_medicacao,0)
	and	nvl(ie_situacao,'A') = 'A';

cursor C02 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	= 'APO'
	and	nvl(cd_protocolo,nvl(:new.cd_protocolo,0)) = nvl(:new.cd_protocolo,0)
	and	nvl(nr_seq_protocolo,nvl(:new.nr_seq_medicacao,0)) = nvl(:new.nr_seq_medicacao,0)
	and	nvl(ie_situacao,'A') = 'A';

cursor C03 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	= 'EPT'
	and	nvl(cd_protocolo,nvl(:new.cd_protocolo,0)) = nvl(:new.cd_protocolo,0)
	and	nvl(nr_seq_protocolo,nvl(:new.nr_seq_medicacao,0)) = nvl(:new.nr_seq_medicacao,0)
	and	nvl(ie_situacao,'A') = 'A';

begin
If	(:new.ie_status is not null) and
	(:old.ie_status <>  :new.ie_status) and
	(:new.ie_status <> 'I') and
	(:new.ie_status <> 'F') then
	Abortar_Se_Protoc_Inativo(:new.nr_seq_paciente);
end if;

if	(:old.ie_status is not null) and
	(:old.ie_status <>  :new.ie_status) and
	(:new.ie_status = 'F') then
	Abortar_se_dia_ciclo_ativo(:new.nr_seq_paciente,:new.cd_pessoa_fisica);
end if;

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(:old.DT_PROTOCOLO,sysdate+10) <> :new.DT_PROTOCOLO) and
	(:new.DT_PROTOCOLO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_PROTOCOLO, 'HV');
end if;

if	(nvl(:old.DT_LIB_AUTORIZADOR,sysdate+10) <> :new.DT_LIB_AUTORIZADOR) and
	(:new.DT_LIB_AUTORIZADOR is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIB_AUTORIZADOR,'HV');
end if;

if	(:new.nr_seq_medicacao is not null) and
	((:old.nr_seq_medicacao is null) or
	 (:new.nr_seq_medicacao <> :old.nr_seq_medicacao)) and
	(:new.qt_tempo_medic	is null )then
	begin
	select	max(qt_tempo_medic)
	into	:new.qt_tempo_medic
	from	protocolo_medicacao
	where	cd_protocolo	= :new.cd_protocolo
	and	nr_sequencia	= :new.nr_seq_medicacao;
	end;
end if;

nm_usuario_w :=wheb_usuario_pck.get_nm_usuario;
Obter_Param_Usuario(281,1255,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_cancelar_agend_w );

if	(:old.ie_status is not null) and
	(:old.ie_status <>  :new.ie_status) and
	((:new.ie_status = 'I') or
	((:new.ie_status = 'F') and
	 (:old.ie_status <> 'I')))then
		if	(ie_cancelar_agend_w = 'S') or
			((:new.ie_status = 'I') and (ie_cancelar_agend_w = 'I')) or
			((:new.ie_status = 'F') and (ie_cancelar_agend_w = 'F')) then
				inativar_dia_tratamento_onco(:new.nr_seq_paciente, :new.nm_usuario);
		end if;
end if;

if	(inserting) then

	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_protocolo_onc( 	nr_seq_evento_w,
						:new.cd_pessoa_fisica,
						:new.cd_protocolo,
						:new.nr_seq_medicacao,
						:new.nm_usuario);
		end;
	end loop;
	close C01;
	exception
		when others then
		null;
	end;
end if;

if	(updating) then

	begin
	open C02;
	loop
	fetch C02 into
		nr_seq_evento_w;
	exit when C02%notfound;
		begin
		gerar_evento_protocolo_onc( 	nr_seq_evento_w,
						:new.cd_pessoa_fisica,
						:new.cd_protocolo,
						:new.nr_seq_medicacao,
						:new.nm_usuario);
		end;
	end loop;
	close C02;
	exception
		when others then
		null;
	end;

end if;

if	(inserting) then
	begin
	SELECT MAX(NR_ATENDIMENTO)
	INTO nr_atendimento_W
	FROM ATENDIMENTO_PACIENTE
	WHERE CD_PESSOA_FISICA = :NEW.cd_PESSOA_FISICA;
	open C03;
	loop
	fetch C03 into
		nr_seq_evento_w;
	exit when C03%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,nr_atendimento_W,:new.cd_pessoa_fisica,null,:new.nm_usuario,null,:new.DT_PROTOCOLO,NULL);
		end;
	end loop;
	close C03;
	end;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_setor_update
before update ON TASY.PACIENTE_SETOR for each row
declare

qt_reg_w	number(10);

pragma autonomous_transaction;

begin

if	(:new.ie_status = 'I') and
	(:old.ie_status = 'A') then

	select	count(*)
	into	qt_reg_w
	from	paciente_atendimento a
	where	dt_chegada is not null
	and		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_chegada) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate)
	and		dt_fim_adm is null
	and		a.nr_seq_paciente = :new.nr_seq_paciente;

	if	(qt_reg_w	> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1029223);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_SETOR_tp  after update ON TASY.PACIENTE_SETOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_PACIENTE);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_ALTURA,1,4000),substr(:new.QT_ALTURA,1,4000),:new.nm_usuario,nr_seq_w,'QT_ALTURA',ie_log_w,ds_w,'PACIENTE_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO,1,4000),substr(:new.QT_PESO,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO',ie_log_w,ds_w,'PACIENTE_SETOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PACIENTE_SETOR ADD (
  CONSTRAINT PACSETO_PK
 PRIMARY KEY
 (NR_SEQ_PACIENTE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_SETOR ADD (
  CONSTRAINT PACSETO_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACSETO_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT PACSETO_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT PACSETO_SETATEND_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACSETO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PACSETO_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PACSETO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACSETO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PACSETO_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT PACSETO_QTACESS_FK 
 FOREIGN KEY (NR_SEQ_ACESSO) 
 REFERENCES TASY.QT_ACESSO (NR_SEQUENCIA),
  CONSTRAINT PACSETO_SUSLAPA_FK 
 FOREIGN KEY (NR_SEQ_LAUDO_SUS) 
 REFERENCES TASY.SUS_LAUDO_PACIENTE (NR_SEQ_INTERNO),
  CONSTRAINT PACSETO_MOTPACON_FK 
 FOREIGN KEY (CD_MOTIVO_INATIV) 
 REFERENCES TASY.MOTIVO_INATIV_PAC_ONCO (CD_MOTIVO),
  CONSTRAINT PACSETO_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PACSETO_CANLORE_FK 
 FOREIGN KEY (NR_SEQ_LOCO_REGIONAL) 
 REFERENCES TASY.CAN_LOCO_REGIONAL (NR_SEQUENCIA),
  CONSTRAINT PACSETO_REFBIBL_FK 
 FOREIGN KEY (NR_SEQ_REFERENCIA) 
 REFERENCES TASY.REFERENCIA_BIBLIOGRAFICA (NR_SEQUENCIA),
  CONSTRAINT PACSETO_DEFONCO_FK 
 FOREIGN KEY (NR_SEQ_DEFINIDOR) 
 REFERENCES TASY.DEFINIDOR_ONCOLOCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_SETOR TO NIVEL_1;

