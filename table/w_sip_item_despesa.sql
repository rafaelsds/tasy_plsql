ALTER TABLE TASY.W_SIP_ITEM_DESPESA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_SIP_ITEM_DESPESA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_SIP_ITEM_DESPESA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_SEGURADO       NUMBER(10),
  NR_SEQ_CONTRATO       NUMBER(10),
  NR_SEQ_PLANO          NUMBER(10),
  CD_ESTRUTURA          VARCHAR2(40 BYTE),
  NR_EXPOSTOS           NUMBER(10),
  NR_EVENTOS            NUMBER(10),
  VL_TOTAL_DESPESA      NUMBER(15,2),
  VL_PARTICIPACAO       NUMBER(15,2),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_SIP       NUMBER(10),
  VL_RECUPERACAO        NUMBER(15,2),
  IE_TIPO_ITEM_DESPESA  VARCHAR2(3 BYTE),
  DS_ESTRUTURA          VARCHAR2(80 BYTE),
  IE_EXPOSTOS           VARCHAR2(1 BYTE),
  IE_EVENTOS            VARCHAR2(1 BYTE),
  IE_TOTAL_DESPESA      VARCHAR2(1 BYTE),
  IE_COPARTICIPACAO     VARCHAR2(1 BYTE),
  IE_SEGUROS            VARCHAR2(1 BYTE),
  IE_TIPO_BENEFICIARIO  VARCHAR2(3 BYTE),
  QT_BENEFICIARIOS      NUMBER(10),
  IE_TIPO_DESPESA       VARCHAR2(3 BYTE),
  VL_EVENTOS            NUMBER(15,2),
  VL_GLOSA              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WSIPDES_PK ON TASY.W_SIP_ITEM_DESPESA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WSIPDES_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.w_sip_item_despesa_delete
before delete ON TASY.W_SIP_ITEM_DESPESA for each row
declare

dt_envio_w	Date;

begin

select	nvl(max(dt_envio),'')
into	dt_envio_w
from	pls_lote_sip
where	nr_sequencia	= :old.nr_seq_lote_sip;

if	(dt_envio_w is not null) then
	-- O lote SIP j� foi enviado para a ANS, n�o � permitido a exclus�o de registros desse per�odo!
	wheb_mensagem_pck.exibir_mensagem_abort(267079);
end if;

end;
/


ALTER TABLE TASY.W_SIP_ITEM_DESPESA ADD (
  CONSTRAINT WSIPDES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_SIP_ITEM_DESPESA TO NIVEL_1;

