ALTER TABLE TASY.CTB_SALDO_ANS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_SALDO_ANS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_SALDO_ANS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MES_REF       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_EMPRESA           NUMBER(5)                NOT NULL,
  CD_CONTA_ANS         NUMBER(10),
  VL_DEBITO            NUMBER(15,2)             NOT NULL,
  VL_CREDITO           NUMBER(15,2)             NOT NULL,
  VL_SALDO             NUMBER(15,2)             NOT NULL,
  VL_SALDO_ANT         NUMBER(15,2)             NOT NULL,
  CD_GRUPO             NUMBER(10),
  IE_COMPENSACAO       VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_VERSAO_ANS        VARCHAR2(1 BYTE),
  VL_ENCERRAMENTO      NUMBER(15,2),
  NR_SEQ_CONTA_ANS     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBSAAN_CTBGRCO_FK_I ON TASY.CTB_SALDO_ANS
(CD_GRUPO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBSAAN_CTBGRCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBSAAN_CTBMERE_FK_I ON TASY.CTB_SALDO_ANS
(NR_SEQ_MES_REF)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBSAAN_CTBPLAN_FK_I ON TASY.CTB_SALDO_ANS
(NR_SEQ_CONTA_ANS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBSAAN_EMPRESA_FK_I ON TASY.CTB_SALDO_ANS
(CD_EMPRESA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBSAAN_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBSAAN_ESTABEL_FK_I ON TASY.CTB_SALDO_ANS
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBSAAN_PK ON TASY.CTB_SALDO_ANS
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBSAAN_PK
  MONITORING USAGE;


ALTER TABLE TASY.CTB_SALDO_ANS ADD (
  CONSTRAINT CTBSAAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          832K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_SALDO_ANS ADD (
  CONSTRAINT CTBSAAN_CTBPLAN_FK 
 FOREIGN KEY (NR_SEQ_CONTA_ANS) 
 REFERENCES TASY.CTB_PLANO_ANS (NR_SEQUENCIA),
  CONSTRAINT CTBSAAN_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBSAAN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBSAAN_CTBGRCO_FK 
 FOREIGN KEY (CD_GRUPO) 
 REFERENCES TASY.CTB_GRUPO_CONTA (CD_GRUPO),
  CONSTRAINT CTBSAAN_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.CTB_SALDO_ANS TO NIVEL_1;

