ALTER TABLE TASY.CHEQUE_CR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHEQUE_CR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHEQUE_CR
(
  NR_SEQ_CHEQUE             NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_BANCO                  NUMBER(5)           NOT NULL,
  CD_AGENCIA_BANCARIA       VARCHAR2(8 BYTE)    NOT NULL,
  NR_CONTA                  VARCHAR2(20 BYTE)   NOT NULL,
  NR_CHEQUE                 VARCHAR2(20 BYTE)   NOT NULL,
  VL_CHEQUE                 NUMBER(15,2)        NOT NULL,
  CD_MOEDA                  NUMBER(5)           NOT NULL,
  DT_VENCIMENTO             DATE,
  NR_ADIANTAMENTO           NUMBER(10),
  NR_TITULO                 NUMBER(10),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  CD_CGC                    VARCHAR2(14 BYTE),
  DT_DEPOSITO               DATE,
  DT_REAPRESENTACAO         DATE,
  DT_DEVOLUCAO              DATE,
  NM_USUARIO_DEVOLUCAO      VARCHAR2(15 BYTE),
  CD_MOTIVO_DEVOLUCAO       NUMBER(5),
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  DT_DEVOLUCAO_BANCO        DATE,
  DT_SEG_DEVOLUCAO          DATE,
  IE_ORIGEM_CHEQUE          VARCHAR2(3 BYTE),
  DT_REGISTRO               DATE,
  CD_PORTADOR               NUMBER(10),
  CD_TIPO_PORTADOR          NUMBER(5),
  VL_TERCEIRO               NUMBER(15,2)        NOT NULL,
  DT_CONTABIL               DATE                NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  NR_SEQ_LOTE               NUMBER(10),
  NR_SEQ_SALDO_CAIXA        NUMBER(10),
  DT_VENDA_TERC             DATE,
  DT_SAQUE                  DATE,
  NR_SEQ_CAIXA_REC          NUMBER(10),
  NR_SEQ_MOTIVO_DEV         NUMBER(10),
  IE_LIB_CAIXA              VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_MOTIVO_SEG_DEV     NUMBER(10),
  NR_SEQ_TRANS_CAIXA        NUMBER(10),
  DT_VENCIMENTO_ATUAL       DATE,
  DT_COMPENSACAO            DATE,
  DT_TERC_DEVOLUCAO         DATE,
  NR_SEQ_MOTIVO_TERC_DEV    NUMBER(10),
  DT_SEG_REAPRESENTACAO     DATE,
  IE_DEPOSITO_PACIENTE      VARCHAR2(1 BYTE),
  NR_SEQ_MOVTO_DEP          NUMBER(10),
  VL_SALDO_NEGOCIADO        NUMBER(15,2),
  NR_SEQ_CAMARA             NUMBER(10),
  DT_PREV_DESBLOQUEIO       DATE,
  DT_PREV_SEG_DESBLOQUEIO   DATE,
  DT_PREV_TERC_DESBLOQUEIO  DATE,
  DS_CMC7                   VARCHAR2(40 BYTE),
  DS_BENEFICIARIO           VARCHAR2(255 BYTE),
  DS_AUTORIZACAO_TEF        VARCHAR2(255 BYTE),
  CD_RETORNO_TEF            VARCHAR2(255 BYTE),
  CD_TIPO_TAXA_JUROS        NUMBER(10),
  CD_TIPO_TAXA_MULTA        NUMBER(10),
  TX_JUROS_COBRANCA         NUMBER(7,4),
  TX_MULTA_COBRANCA         NUMBER(7,4),
  NR_SEQ_GRUPO_PROD         NUMBER(10),
  NR_SEQ_PRODUTO            NUMBER(10),
  DT_CUSTODIA               DATE,
  DT_RESGATE_CUSTODIA       DATE,
  IE_FORMA_PAGTO            VARCHAR2(2 BYTE),
  CD_PF_PACIENTE            VARCHAR2(10 BYTE),
  VL_COTACAO                NUMBER(21,10),
  CD_MOEDA_ORIGINAL         NUMBER(5),
  VL_CHEQUE_ESTRANG         NUMBER(15,2),
  VL_COMPLEMENTO            NUMBER(15,2),
  NR_PROTEMEX               VARCHAR2(255 BYTE),
  NR_SEQ_ATEND_ADIANT       NUMBER(10),
  CD_CEP                    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHEQUCR_AGEBANC_FK_I ON TASY.CHEQUE_CR
(CD_BANCO, CD_AGENCIA_BANCARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUCR_MOEDA_FK_I ON TASY.CHEQUE_CR
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUCR_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUCR_PESFISI_FK_I ON TASY.CHEQUE_CR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUCR_PESJURI_FK_I ON TASY.CHEQUE_CR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHEQUCR_PK ON TASY.CHEQUE_CR
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUCR_TITRECE_FK ON TASY.CHEQUE_CR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHEQUCR_UK ON TASY.CHEQUE_CR
(CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA, NR_CHEQUE, DT_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_ADIANTA_FK_I ON TASY.CHEQUE_CR
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_ATEPACI_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_ATEND_ADIANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_CAIREC_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_CAMCOPM_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_CAMCOPM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_ESTABEL_FK_I ON TASY.CHEQUE_CR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_GRPRFIN_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_GRUPO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_GRPRFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_I1 ON TASY.CHEQUE_CR
(DT_VENCIMENTO_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_MODEVCH_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_MODEVCH_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_MODEVCH_FK2_I ON TASY.CHEQUE_CR
(NR_SEQ_MOTIVO_SEG_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_MODEVCH_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_MODEVCH_FK3_I ON TASY.CHEQUE_CR
(NR_SEQ_MOTIVO_TERC_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_MODEVCH_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_MOEDA_FK2_I ON TASY.CHEQUE_CR
(CD_MOEDA_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_MOVTRFI_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_MOVTO_DEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_MOVTRFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_PESFISI_FK1_I ON TASY.CHEQUE_CR
(CD_PF_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_PORTADO_FK_I ON TASY.CHEQUE_CR
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUES_PRODFIN_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_PRODFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_TIPTAXA_FK_I ON TASY.CHEQUE_CR
(CD_TIPO_TAXA_JUROS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_TIPTAXA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_TIPTAXA_FK1_I ON TASY.CHEQUE_CR
(CD_TIPO_TAXA_MULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_TIPTAXA_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUES_TRAFINA_FK_I ON TASY.CHEQUE_CR
(NR_SEQ_TRANS_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUES_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CHEQUE_CR_UPDATE
before update ON TASY.CHEQUE_CR for each row
declare

ie_tipo_devolucao_w		varchar2(5);
ie_repasse_cheque_comp_w	varchar2(1);

dt_fechamento_w			date	:= null;
nr_recibo_w			number(10);
qt_registro_w			number(10);
ie_pessoa_w		varchar2(1);

ie_cheque_deposito_w		varchar2(1)	:= 'S';
qt_reg_w			number(1);
ie_lib_rep_tit_cheque_w		varchar2(1);
ds_moeda_w			moeda.ds_moeda%type;
cd_moeda_unica_w		banco_estabelecimento.cd_moeda%type;
ds_conta_w			banco_estabelecimento_v.ds_conta%type;
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_pessoa_cheque_cr),'N')
into	ie_pessoa_w
from	parametro_contas_receber
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(ie_pessoa_w = 'S') and
	(:new.cd_pessoa_fisica is null and :new.cd_cgc is null) then
	/* O cheque deve ser de uma pessoa f�sica ou jur�dica! */
	wheb_mensagem_pck.exibir_mensagem_abort(223996);
end if;

if	(nvl(:new.dt_deposito, to_date('01/01/1900','dd/mm/yyyy')) 		<> nvl(:old.dt_deposito, to_date('01/01/1900','dd/mm/yyyy'))) or
	(nvl(:new.dt_devolucao_banco, to_date('01/01/1900','dd/mm/yyyy')) 	<> nvl(:old.dt_devolucao_banco, to_date('01/01/1900','dd/mm/yyyy'))) or
	(nvl(:new.dt_reapresentacao, to_date('01/01/1900','dd/mm/yyyy')) 	<> nvl(:old.dt_reapresentacao, to_date('01/01/1900','dd/mm/yyyy'))) or
	(nvl(:new.dt_seg_devolucao, to_date('01/01/1900','dd/mm/yyyy')) 	<> nvl(:old.dt_seg_devolucao, to_date('01/01/1900','dd/mm/yyyy'))) or
	(nvl(:new.dt_seg_reapresentacao, to_date('01/01/1900','dd/mm/yyyy'))	<> nvl(:old.dt_seg_reapresentacao, to_date('01/01/1900','dd/mm/yyyy'))) or
	(nvl(:new.dt_terc_devolucao, to_date('01/01/1900','dd/mm/yyyy')) 	<> nvl(:old.dt_terc_devolucao, to_date('01/01/1900','dd/mm/yyyy'))) then

	select	nvl(max(ie_tipo_devolucao),'N')
	into	ie_tipo_devolucao_w
	from 	motivo_dev_cheque
	where 	nr_sequencia = :new.nr_seq_motivo_dev;

	if	((:new.dt_terc_devolucao is not null) and (ie_tipo_devolucao_w = 'B')) then
		if	(:new.dt_seg_reapresentacao is null) then
			/* O cheque nr_cheque n�o pode ter terceira devolu��o, pois n�o tem terceira reapresenta��o! */
			wheb_mensagem_pck.exibir_mensagem_abort(191669,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_seg_devolucao is null) then
			/* O cheque nr_cheque n�o pode ter terceira devolu��o, pois n�o tem segunda devolu��o! */
			wheb_mensagem_pck.exibir_mensagem_abort(191670,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_reapresentacao is null) then
			/* O cheque nr_cheque n�o pode ter terceira devolu��o, pois n�o foi reapresentado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191673,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_devolucao_banco is null) then
			/* O cheque nr_cheque n�o pode ter terceira devolu��o, pois n�o foi devolvido pelo banco! */
			wheb_mensagem_pck.exibir_mensagem_abort(191674,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_deposito is null) then
			/* O cheque nr_cheque n�o pode ter terceira devolu��o, pois n�o foi depositado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191675,	'NR_CHEQUE=' || :new.nr_cheque);
		end if;
	elsif	((:new.dt_seg_reapresentacao is not null) and (ie_tipo_devolucao_w = 'B')) then
		if	(:new.dt_seg_devolucao is null) then
			/* O cheque nr_cheque n�o pode ter terceira reapresenta��o, pois n�o tem segunda devolu��o! */
			wheb_mensagem_pck.exibir_mensagem_abort(191676,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_reapresentacao is null) then
			/* O cheque nr_cheque n�o pode ter terceira reapresenta��o, pois n�o foi reapresentado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191677,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_devolucao_banco is null) then
			/* O cheque nr_cheque n�o pode ter terceira reapresenta��o, pois n�o foi devolvido pelo banco! */
			wheb_mensagem_pck.exibir_mensagem_abort(191678,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_deposito is null) then
			/* O cheque nr_cheque n�o pode ter terceira reapresenta��o, pois n�o foi depositado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191679,	'NR_CHEQUE=' || :new.nr_cheque);
		end if;
	elsif	(:new.dt_seg_devolucao is not null) then
		if	(:new.dt_reapresentacao is null) then
			/* O cheque nr_cheque n�o pode ter segunda devolu��o, pois n�o foi reapresentado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191680,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_devolucao_banco is null) then
			/* O cheque nr_cheque n�o pode ter segunda devolu��o, pois n�o foi devolvido pelo banco! */
			wheb_mensagem_pck.exibir_mensagem_abort(191681,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_deposito is null) then
			/* O cheque nr_cheque n�o pode ter segunda devolu��o, pois n�o foi depositado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191682,	'NR_CHEQUE=' || :new.nr_cheque);
		end if;
	elsif	(:new.dt_reapresentacao is not null) then
		if	(:new.dt_devolucao_banco is null) then
			/* O cheque nr_cheque n�o pode ser reapresentado, pois n�o foi devolvido pelo banco! */
			wheb_mensagem_pck.exibir_mensagem_abort(191683,	'NR_CHEQUE=' || :new.nr_cheque);
		elsif	(:new.dt_deposito is null) then
			/* O cheque nr_cheque n�o pode ser reapresentado, pois n�o foi depositado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191684,	'NR_CHEQUE=' || :new.nr_cheque);
		end if;
	elsif	(:new.dt_devolucao_banco is not null) then
		if	(:new.dt_deposito is null) then
			/* O cheque nr_cheque n�o pode ser devolvido pelo banco, pois n�o foi depositado! */
			wheb_mensagem_pck.exibir_mensagem_abort(191685,	'NR_CHEQUE=' || :new.nr_cheque);
		end if;
	end if;

	if	(:old.dt_deposito is null and :new.dt_deposito is not null) then

		select	nvl(max(ie_cheque_deposito),'S')
		into	ie_cheque_deposito_w
		from	parametro_contas_receber
		where	cd_estabelecimento	= :new.cd_estabelecimento;

		if	(ie_cheque_deposito_w = 'S') then

			select	count(*)
			into	qt_registro_w
			from	deposito_cheque
			where	nr_seq_cheque	= :new.nr_seq_cheque;

			if	(qt_registro_w = 0) then
				/* N�o � poss�vel colocar a data de dep�sito no cheque pois o cheque n�o est� em nenhum lote de dep�sito. */
				wheb_mensagem_pck.exibir_mensagem_abort(191686);
			end if;
		end if;
	end if;
end if;

/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
if	(:old.dt_contabil <> :new.dt_contabil) then
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_contabil);
end if;


select 	nvl(max(ie_repasse_cheque_comp),'N'),
	nvl(max(ie_lib_rep_tit_cheque),'N')
into	ie_repasse_cheque_comp_w,
	ie_lib_rep_tit_cheque_w
from	parametro_faturamento
where	cd_estabelecimento = :new.cd_estabelecimento;

if 	((:old.dt_compensacao is null) and
	(:new.dt_compensacao is not null) and
	(ie_repasse_cheque_comp_w = 'S') and
	((:new.nr_titulo is not null) or
	(:new.NR_SEQ_CAIXA_REC is not null))) and
	(nvl(ie_lib_rep_tit_cheque_w,'N') = 'N') then

	atualizar_repasse_cheque_comp (:new.nr_titulo,:new.DT_COMPENSACAO, :new.vl_cheque, :new.nm_usuario, :new.cd_estabelecimento,:new.NR_SEQ_CAIXA_REC, :new.nr_seq_cheque);

end if;

if	(:old.nr_seq_caixa_rec is not null) and
	(:old.vl_cheque <> :new.vl_cheque) then

	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* N�o � poss�vel alterar esse cheque! O recibo nr_recibo_w j� foi fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(191687,	'NR_RECIBO_W=' || nr_recibo_w);
	end if;
end if;

if 	(:old.dt_deposito is null) and
	(:new.dt_deposito is not null) then
begin

	select	max(c.cd_moeda),
		max(c.ds_conta)
	into	cd_moeda_unica_w,
		ds_conta_w
	from	movto_trans_financ b,
		banco_estabelecimento_v c
	where	b.nr_seq_banco = c.nr_sequencia
	and	b.nr_seq_cheque = :new.nr_seq_cheque;

	if ( cd_moeda_unica_w is null) then
		begin
		select	max(e.cd_moeda),
			max(e.ds_conta)
		into	cd_moeda_unica_w,
			ds_conta_w
		from	deposito_cheque b,
			deposito c,
			movto_trans_financ d,
			banco_estabelecimento_v e
		where	b.nr_seq_deposito = c.nr_sequencia
		and	c.nr_sequencia = d.nr_seq_deposito
		and	d.nr_seq_banco = e.nr_sequencia
		and	b.nr_seq_cheque = :new.nr_seq_cheque;
		end;
	end if;

	if	(:new.cd_moeda <> cd_moeda_unica_w ) and
		(cd_moeda_unica_w is not null)	     then
		begin

		select	max(a.ds_moeda)
		into	ds_moeda_w
		from	moeda a
		where	a.cd_moeda = cd_moeda_unica_w;

		/* A conta banc�ria cd_conta_w permite apenas movimenta��es em ds_moeda_w!*/
		wheb_mensagem_pck.exibir_mensagem_abort(301078,'CD_CONTA_W='|| ds_conta_w ||';DS_MOEDA_W='||ds_moeda_w);
		end;
	end if;
end;
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.cheque_cr_after_insert
after insert or update ON TASY.CHEQUE_CR for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_cheque,null,'CR',nvl(:new.dt_vencimento_atual,:new.dt_vencimento),'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_cheque,null,'CR',nvl(:new.dt_vencimento_atual,:new.dt_vencimento),'A',:new.nm_usuario);
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CHEQUE_CR_DELETE
before delete ON TASY.CHEQUE_CR for each row
declare

dt_fechamento_w	date	:= null;
nr_recibo_w	number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:old.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* Nao e possivel excluir esse cheque!
		O recibo #@NR_RECIBO#@ ja foi fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(267126, 'NR_RECIBO=' || nr_recibo_w);
	end if;
end if;

/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_seq_cheque,null,'CR',nvl(:old.dt_vencimento_atual,:old.dt_vencimento),'I',:old.nm_usuario);

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.CHEQUE_CR_INSERT
before insert ON TASY.CHEQUE_CR for each row
declare
ie_pessoa_w	varchar2(1);
dt_fechamento_w	date	:= null;
nr_recibo_w	number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	nvl(max(ie_pessoa_cheque_cr),'N')
into	ie_pessoa_w
from	parametro_contas_receber
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(ie_pessoa_w = 'S') and
	(:new.cd_pessoa_fisica is null and :new.cd_cgc is null) then
	/* O cheque deve ser de uma pessoa fisica ou juridica! */
	wheb_mensagem_pck.exibir_mensagem_abort(223996);
end if;

if	(:new.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :new.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* Nao e possivel inserir um novo cheque!
		O recibo nr_recibo_w ja foi fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(223997,'NR_RECIBO_W='||nr_recibo_w);
	end if;
end if;

if	(:new.ie_forma_pagto is null) then

	if	(trunc(nvl(:new.dt_registro,sysdate),'dd') = trunc(nvl(:new.dt_vencimento,sysdate),'dd')) then
		:new.ie_forma_pagto	:= 'AV';
	else
		:new.ie_forma_pagto	:= 'PD';
	end if;

end if;

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
if	( :new.dt_contabil is not null ) then
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_contabil);
end if;

:new.vl_saldo_negociado	:= :new.vl_cheque;

end if;
end;
/


ALTER TABLE TASY.CHEQUE_CR ADD (
  CONSTRAINT CHEQUCR_PK
 PRIMARY KEY
 (NR_SEQ_CHEQUE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CHEQUCR_UK
 UNIQUE (CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA, NR_CHEQUE, DT_DEVOLUCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          960K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHEQUE_CR ADD (
  CONSTRAINT CHEQUES_PESFISI_FK1 
 FOREIGN KEY (CD_PF_PACIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHEQUES_MOEDA_FK2 
 FOREIGN KEY (CD_MOEDA_ORIGINAL) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT CHEQUES_ATEPACI_FK 
 FOREIGN KEY (NR_SEQ_ATEND_ADIANT) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CHEQUCR_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT CHEQUCR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHEQUCR_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CHEQUCR_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT CHEQUES_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT CHEQUES_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT CHEQUES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CHEQUES_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_MODEVCH_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.MOTIVO_DEV_CHEQUE (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_MODEVCH_FK2 
 FOREIGN KEY (NR_SEQ_MOTIVO_SEG_DEV) 
 REFERENCES TASY.MOTIVO_DEV_CHEQUE (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_CAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_MODEVCH_FK3 
 FOREIGN KEY (NR_SEQ_MOTIVO_TERC_DEV) 
 REFERENCES TASY.MOTIVO_DEV_CHEQUE (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_MOVTRFI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_DEP) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_CAMCOPM_FK 
 FOREIGN KEY (NR_SEQ_CAMARA) 
 REFERENCES TASY.CAMARA_COMPENSACAO (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_TIPTAXA_FK 
 FOREIGN KEY (CD_TIPO_TAXA_JUROS) 
 REFERENCES TASY.TIPO_TAXA (CD_TIPO_TAXA),
  CONSTRAINT CHEQUES_TIPTAXA_FK1 
 FOREIGN KEY (CD_TIPO_TAXA_MULTA) 
 REFERENCES TASY.TIPO_TAXA (CD_TIPO_TAXA),
  CONSTRAINT CHEQUES_GRPRFIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PROD) 
 REFERENCES TASY.GRUPO_PROD_FINANC (NR_SEQUENCIA),
  CONSTRAINT CHEQUES_PRODFIN_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.PRODUTO_FINANCEIRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CHEQUE_CR TO NIVEL_1;

