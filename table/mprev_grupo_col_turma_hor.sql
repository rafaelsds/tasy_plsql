ALTER TABLE TASY.MPREV_GRUPO_COL_TURMA_HOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_GRUPO_COL_TURMA_HOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_GRUPO_COL_TURMA_HOR
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_TURMA                NUMBER(10)        NOT NULL,
  NR_SEQ_LOCAL_ATEND          NUMBER(10),
  IE_DIA_SEMANA               NUMBER(1)         NOT NULL,
  IE_SEMANA                   NUMBER(5)         NOT NULL,
  IE_FREQUENCIA               VARCHAR2(15 BYTE) NOT NULL,
  DT_INICIO                   DATE              NOT NULL,
  HR_INICIO                   DATE              NOT NULL,
  HR_TERMINO                  DATE              NOT NULL,
  QT_REPETICAO                NUMBER(5),
  DT_FIM_REPETICAO            DATE,
  DT_GERACAO_HORARIO          DATE,
  NM_USUARIO_GERACAO          VARCHAR2(15 BYTE),
  DT_CANCELAMENTO             DATE,
  NM_USUARIO_CANC             VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_TEMA           NUMBER(10),
  IE_FORMA_ATENDIMENTO_MPREV  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRGCTH_MPRGCTU_FK_I ON TASY.MPREV_GRUPO_COL_TURMA_HOR
(NR_SEQ_TURMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRGCTH_MPRGCTU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRGCTH_MPRGTEN_FK_I ON TASY.MPREV_GRUPO_COL_TURMA_HOR
(NR_SEQ_GRUPO_TEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRGCTH_PK ON TASY.MPREV_GRUPO_COL_TURMA_HOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MPREV_GRUPO_COL_TURMA_HOR_tp  after update ON TASY.MPREV_GRUPO_COL_TURMA_HOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_FORMA_ATENDIMENTO_MPREV,1,500);gravar_log_alteracao(substr(:old.IE_FORMA_ATENDIMENTO_MPREV,1,4000),substr(:new.IE_FORMA_ATENDIMENTO_MPREV,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ATENDIMENTO_MPREV',ie_log_w,ds_w,'MPREV_GRUPO_COL_TURMA_HOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MPREV_GRUPO_COL_TURMA_HOR ADD (
  CONSTRAINT MPRGCTH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_GRUPO_COL_TURMA_HOR ADD (
  CONSTRAINT MPRGCTH_MPRGCTU_FK 
 FOREIGN KEY (NR_SEQ_TURMA) 
 REFERENCES TASY.MPREV_GRUPO_COL_TURMA (NR_SEQUENCIA),
  CONSTRAINT MPRGCTH_MPRGTEN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TEMA) 
 REFERENCES TASY.MPREV_GRUPO_TEMA_ENCONTRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_GRUPO_COL_TURMA_HOR TO NIVEL_1;

