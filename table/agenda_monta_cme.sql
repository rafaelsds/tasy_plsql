ALTER TABLE TASY.AGENDA_MONTA_CME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_MONTA_CME CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_MONTA_CME
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_AGENDA         NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  DT_APROVACAO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGMOCME_AGEPACI_FK_I ON TASY.AGENDA_MONTA_CME
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGMOCME_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGMOCME_CMCLCON_FK_I ON TASY.AGENDA_MONTA_CME
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGMOCME_CMCLCON_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGMOCME_PK ON TASY.AGENDA_MONTA_CME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGMOCME_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AGENDA_MONTA_CME_tp  after update ON TASY.AGENDA_MONTA_CME FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'AGENDA_MONTA_CME',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_monta_cme_delete
before delete ON TASY.AGENDA_MONTA_CME for each row
declare
ds_alteracao_w		varchar2(4000) := null;
begin

if	(nvl(:old.nr_seq_classificacao,0) > 0) then
	ds_alteracao_w	:=	substr(obter_desc_expressao(499891)||' '||substr(cme_obter_desc_classif_conj(:old.nr_seq_classificacao),1,125),1,4000);
end if;

if	(ds_alteracao_w is not null) then
	gravar_historico_montagem(:old.nr_seq_agenda,'EC',ds_alteracao_w,:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_monta_cme_atual
before insert or update ON TASY.AGENDA_MONTA_CME for each row
declare
ds_alteracao_w		varchar2(4000) 	:= null;
ds_equipamento_w	varchar2(255) 	:= null;

begin

if	(updating) then
	if	(nvl(:old.nr_seq_classificacao,0) <> nvl(:new.nr_seq_classificacao,0)) then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791491,
							'DS_CLASSIF_OLD='||substr(cme_obter_desc_classif_conj(:old.nr_seq_classificacao),1,125)||
							';DS_CLASSIF_NEW='||substr(cme_obter_desc_classif_conj(:new.nr_seq_classificacao),1,125)),1,4000);

	end if;

	if	(nvl(:old.ds_observacao,'X') <> nvl(:new.ds_observacao,'X')) then
		ds_alteracao_w	:=	substr(ds_alteracao_w || CHR(10) || CHR(13) ||
					wheb_mensagem_pck.get_texto(791471,
							'DS_OBSERVACAO_OLD='||:old.ds_observacao||
							';DS_OBSERVACAO_NEW='||:new.ds_observacao),1,4000);

	end if;

	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'AC',ds_alteracao_w,:new.nm_usuario);
	end if;
else

	ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(791490,
							'DS_CLASSIF='||substr(cme_obter_desc_classif_conj(:new.nr_seq_classificacao),1,125)),1,4000);

	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'IC',ds_alteracao_w,:new.nm_usuario);
	end if;
end if;


end;
/


ALTER TABLE TASY.AGENDA_MONTA_CME ADD (
  CONSTRAINT AGMOCME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_MONTA_CME ADD (
  CONSTRAINT AGMOCME_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGMOCME_CMCLCON_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CM_CLASSIF_CONJUNTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_MONTA_CME TO NIVEL_1;

