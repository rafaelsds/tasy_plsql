ALTER TABLE TASY.AGENDA_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_CLASSIF
(
  CD_CLASSIFICACAO         VARCHAR2(5 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DS_CLASSIFICACAO         VARCHAR2(40 BYTE)    NOT NULL,
  IE_TIPO_CLASSIF          VARCHAR2(1 BYTE)     NOT NULL,
  DS_COR_FUNDO             VARCHAR2(10 BYTE),
  DS_COR_FONTE             VARCHAR2(10 BYTE),
  CD_CLASSIF_TASY          VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF_ATEND     NUMBER(10),
  IE_AGENDA                VARCHAR2(1 BYTE)     NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  IE_TIPO_CONSULTA         NUMBER(3),
  IE_TIPO_SAIDA_CONSULTA   NUMBER(3),
  DS_NOME_CURTO            VARCHAR2(7 BYTE),
  IE_TIPO_ATEND_TISS       VARCHAR2(2 BYTE),
  IE_TIPO_GUIA             VARCHAR2(2 BYTE),
  IE_NAO_CONSIDERA_MENSAL  VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_CONSULTA     NUMBER(10),
  CD_CLASSIF_EXTERNO       VARCHAR2(20 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4),
  NR_SEQ_GRUPO_CLASSIF     NUMBER(10),
  IE_CHEGADA_QUIMIO        VARCHAR2(1 BYTE),
  IE_CLINICA               NUMBER(5),
  IE_TIPO_ATENDIMENTO      NUMBER(3),
  CD_PERFIL                NUMBER(5),
  IE_EMAIL_CONFIRMACAO     VARCHAR2(1 BYTE),
  IE_CARATER_INTER_SUS     VARCHAR2(2 BYTE),
  IE_VALIDAR_POLTRONA      VARCHAR2(1 BYTE),
  QT_TEMPO_AGENDAMENTO     NUMBER(10),
  IE_UTILIZA_TELEMEDICINA  VARCHAR2(1 BYTE)     DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGECLAS_AGGRCLA_FK_I ON TASY.AGENDA_CLASSIF
(NR_SEQ_GRUPO_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECLAS_AGGRCLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGECLAS_CLAATEN_FK_I ON TASY.AGENDA_CLASSIF
(NR_SEQ_CLASSIF_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECLAS_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGECLAS_ESTABEL_FK_I ON TASY.AGENDA_CLASSIF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECLAS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGECLAS_OFTICON_FK_I ON TASY.AGENDA_CLASSIF
(NR_SEQ_TIPO_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECLAS_OFTICON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGECLAS_PERFIL_FK_I ON TASY.AGENDA_CLASSIF
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECLAS_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGECLAS_PK ON TASY.AGENDA_CLASSIF
(CD_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGECLAS_SUSCAIN_FK_I ON TASY.AGENDA_CLASSIF
(IE_CARATER_INTER_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECLAS_SUSCAIN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_classif_delete
before delete ON TASY.AGENDA_CLASSIF for each row
declare
qt_registros_w		number(20) 		:= 0;
ds_consistencia_w	varchar2(255)	:= null;

begin
if	(qt_registros_w = 0) and
	(ds_consistencia_w is null)then
	begin
	select	count(*)
	into	qt_registros_w
	from	agenda_turno_classif
	where	cd_classificacao = :old.cd_classificacao;

	if	(qt_registros_w > 0)then
		ds_consistencia_w	:= 'AGENDA_TURNO_CLASSIF';
	end if;

	if	(qt_registros_w = 0) and
		(ds_consistencia_w is null)then
		select	count(*)
		into	qt_registros_w
		from	agenda_servico_turno_esp
		where	ie_classif_agenda = :old.cd_classificacao;

		if	(qt_registros_w > 0)then
			ds_consistencia_w	:= 'AGENDA_SERVICO_TURNO_ESP';
		end if;

	end if;

	if	(qt_registros_w = 0) and
		(ds_consistencia_w is null)then
		select	count(*)
		into	qt_registros_w
		from	agenda_classif_regra
		where	cd_classificacao = :old.cd_classificacao;

		if	(qt_registros_w > 0)then
			ds_consistencia_w	:= 'AGENDA_CLASSIF_REGRA';
		end if;

	end if;

	if	(qt_registros_w = 0) and
		(ds_consistencia_w is null)then
		select	count(*)
		into	qt_registros_w
		from	med_classif_agenda
		where	ie_classif_agenda = :old.cd_classificacao;

		if	(qt_registros_w > 0)then
			ds_consistencia_w	:= 'MED_CLASSIF_AGENDA';
		end if;

	end if;

	if	(qt_registros_w = 0) and
		(ds_consistencia_w is null)then
		select	count(*)
		into	qt_registros_w
		from	agenda_consulta
		where	ie_classif_agenda = :old.cd_classificacao
		and		trunc(dt_agenda) >= trunc(sysdate)
		and		ie_status_agenda not in ('L', 'C', 'B', 'F', 'I', 'II', 'R');

		if	(qt_registros_w > 0)then
			ds_consistencia_w	:= 'AGENDA_CONSULTA';
		end if;

	end if;

	if	(qt_registros_w > 0) and
		(ds_consistencia_w is not null)then
		--N�o � permitido excluir esta classifica��o, pois a mesma est� sendo utilizada por algum agendamento!
		Wheb_mensagem_pck.exibir_mensagem_abort(293389);
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.AGENDA_CLASSIF ADD (
  CONSTRAINT AGECLAS_PK
 PRIMARY KEY
 (CD_CLASSIFICACAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_CLASSIF ADD (
  CONSTRAINT AGECLAS_SUSCAIN_FK 
 FOREIGN KEY (IE_CARATER_INTER_SUS) 
 REFERENCES TASY.SUS_CARATER_INTERNACAO (CD_CARATER_INTERNACAO),
  CONSTRAINT AGECLAS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT AGECLAS_AGGRCLA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CLASSIF) 
 REFERENCES TASY.AGENDA_GRUPO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT AGECLAS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGECLAS_OFTICON_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CONSULTA) 
 REFERENCES TASY.OFT_TIPO_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGECLAS_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ATEND) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_CLASSIF TO NIVEL_1;

GRANT SELECT ON TASY.AGENDA_CLASSIF TO ROBOCMTECNOLOGIA;

