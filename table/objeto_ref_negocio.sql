ALTER TABLE TASY.OBJETO_REF_NEGOCIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OBJETO_REF_NEGOCIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.OBJETO_REF_NEGOCIO
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NM_OBJETO       VARCHAR2(50 BYTE)             NOT NULL,
  NM_OBJETO_REF   VARCHAR2(50 BYTE)             NOT NULL,
  DS_OBSERVACAO   VARCHAR2(255 BYTE),
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  IE_BANCO        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OBRENEG_OBJSIST_FK_I ON TASY.OBJETO_REF_NEGOCIO
(NM_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OBRENEG_PK ON TASY.OBJETO_REF_NEGOCIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OBJETO_REF_NEGOCIO ADD (
  CONSTRAINT OBRENEG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OBJETO_REF_NEGOCIO ADD (
  CONSTRAINT OBRENEG_OBJSIST_FK 
 FOREIGN KEY (NM_OBJETO, IE_BANCO) 
 REFERENCES TASY.OBJETO_SISTEMA (NM_OBJETO,IE_BANCO));

GRANT SELECT ON TASY.OBJETO_REF_NEGOCIO TO NIVEL_1;

