ALTER TABLE TASY.QHAPDC_SEGMENT_HEADER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QHAPDC_SEGMENT_HEADER CASCADE CONSTRAINTS;

CREATE TABLE TASY.QHAPDC_SEGMENT_HEADER
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_SYNTAX               VARCHAR2(10 BYTE),
  NR_SEQ_FILE             NUMBER(10),
  NR_SEQ_FACILITY         NUMBER(10),
  DT_EXTRACT_START        DATE,
  DT_EXTRACT_END          DATE,
  NUMBER_OF_RECORDS       NUMBER(5),
  EXTRACTION_SOFTWARE_ID  VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QHAPDCHEAD_PK ON TASY.QHAPDC_SEGMENT_HEADER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QHAPDCHEAD_QHAPDC_FIL_FK_I ON TASY.QHAPDC_SEGMENT_HEADER
(NR_SEQ_FILE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QHAPDC_SEGMENT_HEADER ADD (
  CONSTRAINT QHAPDCHEAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.QHAPDC_SEGMENT_HEADER ADD (
  CONSTRAINT QHAPDCHEAD_QHAPDC_FIL_FK 
 FOREIGN KEY (NR_SEQ_FILE) 
 REFERENCES TASY.QHAPDC_FILE (NR_SEQUENCIA));

GRANT SELECT ON TASY.QHAPDC_SEGMENT_HEADER TO NIVEL_1;

