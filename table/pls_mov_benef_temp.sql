DROP TABLE TASY.PLS_MOV_BENEF_TEMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_MOV_BENEF_TEMP
(
  NR_SEQ_SEGURADO_REPASSE  NUMBER(10),
  NR_SEQ_SEGURADO          NUMBER(10),
  NR_SEQ_PLANO             NUMBER(10),
  NR_SEQ_CONTRATO          NUMBER(10),
  NR_SEQ_INTERCAMBIO       NUMBER(10),
  NR_SEQ_OPERADORA         NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.PLSMBTP_I1 ON TASY.PLS_MOV_BENEF_TEMP
(NR_SEQ_OPERADORA);


CREATE INDEX TASY.PLSMBTP_I2 ON TASY.PLS_MOV_BENEF_TEMP
(NR_SEQ_OPERADORA, NR_SEQ_CONTRATO);


CREATE INDEX TASY.PLSMBTP_I3 ON TASY.PLS_MOV_BENEF_TEMP
(NR_SEQ_OPERADORA, NR_SEQ_INTERCAMBIO);


GRANT SELECT ON TASY.PLS_MOV_BENEF_TEMP TO NIVEL_1;

