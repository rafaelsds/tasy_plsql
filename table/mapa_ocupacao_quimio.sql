ALTER TABLE TASY.MAPA_OCUPACAO_QUIMIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAPA_OCUPACAO_QUIMIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAPA_OCUPACAO_QUIMIO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_LOCAL             NUMBER(10),
  DT_MAPA                  DATE,
  NR_DURACAO               NUMBER(10),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  NR_SEQ_ATENDIMENTO       NUMBER(10),
  IE_EXIBIR_MAPA           VARCHAR2(1 BYTE),
  IE_SAC                   VARCHAR2(1 BYTE),
  CD_ENFERMAGEM            VARCHAR2(10 BYTE),
  IE_PACIENTE_PESQUISA     VARCHAR2(1 BYTE),
  IE_PRIMEIRO_ATENDIMENTO  VARCHAR2(1 BYTE),
  IE_PADRONIZADO           VARCHAR2(1 BYTE),
  DS_PROTOCOLO             VARCHAR2(255 BYTE),
  IE_A_SABER               VARCHAR2(1 BYTE),
  NR_SEQ_AGENDA_QUIMIO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MACOQUI_AGEQUIM_FK_I ON TASY.MAPA_OCUPACAO_QUIMIO
(NR_SEQ_AGENDA_QUIMIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MACOQUI_PACATEN_FK_I ON TASY.MAPA_OCUPACAO_QUIMIO
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACOQUI_PACATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MACOQUI_PESFISI_FK_I ON TASY.MAPA_OCUPACAO_QUIMIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MACOQUI_PESFISI_FK2_I ON TASY.MAPA_OCUPACAO_QUIMIO
(CD_ENFERMAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MACOQUI_PK ON TASY.MAPA_OCUPACAO_QUIMIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACOQUI_PK
  MONITORING USAGE;


CREATE INDEX TASY.MACOQUI_QTLOCAL_FK_I ON TASY.MAPA_OCUPACAO_QUIMIO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MACOQUI_QTLOCAL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.mapa_ocupacao_quimio_insert
before update ON TASY.MAPA_OCUPACAO_QUIMIO for each row
declare

nr_sequencia_w		number(10);
nr_seq_mapa_onco_w	number(10);
begin

nr_seq_mapa_onco_w := :new.nr_sequencia;

if 	(:new.nr_seq_local <> :old.nr_seq_local) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_local,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.nr_seq_local,
		nr_seq_mapa_onco_w);
end if;

if	(:new.dt_mapa <> :old.dt_mapa) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		dt_mapa,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_mapa,
		nr_seq_mapa_onco_w);
end if;

if	(:new.dt_mapa <> :old.dt_mapa) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		dt_mapa,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.dt_mapa,
		nr_seq_mapa_onco_w);
end if;

if	(:new.ie_exibir_mapa <> :old.ie_exibir_mapa) then
	select	paciente_atendimento_hist_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into paciente_atendimento_hist (
		nr_sequencia,
		nr_seq_atendimento,
		dt_atualizacao,
		nm_usuario,
		ie_exibir_mapa,
		nr_seq_mapa_ocup)
	values	(nr_sequencia_w,
		:new.nr_seq_atendimento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		:new.ie_exibir_mapa,
		nr_seq_mapa_onco_w);
end if;

end;
/


ALTER TABLE TASY.MAPA_OCUPACAO_QUIMIO ADD (
  CONSTRAINT MACOQUI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAPA_OCUPACAO_QUIMIO ADD (
  CONSTRAINT MACOQUI_AGEQUIM_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_QUIMIO) 
 REFERENCES TASY.AGENDA_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT MACOQUI_PESFISI_FK2 
 FOREIGN KEY (CD_ENFERMAGEM) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MACOQUI_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT MACOQUI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MACOQUI_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAPA_OCUPACAO_QUIMIO TO NIVEL_1;

