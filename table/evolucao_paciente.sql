ALTER TABLE TASY.EVOLUCAO_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EVOLUCAO_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EVOLUCAO_PACIENTE
(
  CD_EVOLUCAO                NUMBER(10)         NOT NULL,
  DT_EVOLUCAO                DATE               NOT NULL,
  IE_TIPO_EVOLUCAO           VARCHAR2(3 BYTE)   NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  DS_EVOLUCAO                LONG,
  CD_MEDICO                  VARCHAR2(10 BYTE),
  DT_LIBERACAO               DATE,
  IE_EVOLUCAO_CLINICA        VARCHAR2(3 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  CD_ESPECIALIDADE           NUMBER(5),
  CD_MEDICO_PARECER          VARCHAR2(10 BYTE),
  QT_PESO                    NUMBER(6,3),
  QT_ALTURA                  NUMBER(3),
  QT_SUPERF_CORPORIA         NUMBER(15,2),
  CD_TOPOGRAFIA              VARCHAR2(10 BYTE),
  CD_TUMOR_PRIM_PAT          VARCHAR2(15 BYTE)  DEFAULT null,
  CD_LINFONODO_REG_PAT       VARCHAR2(15 BYTE)  DEFAULT null,
  CD_METASTASE_DIST_PAT      VARCHAR2(15 BYTE)  DEFAULT null,
  CD_ESTADIO_OUTRO           VARCHAR2(20 BYTE),
  DT_INICIO_TRAT_PROPOSTO    DATE,
  DT_FINAL_TRAT_PROPOSTO     DATE,
  CD_ESTADO_DOENCA           NUMBER(10),
  IE_ESTADO_PAC_FIM_TRAT     NUMBER(10),
  IE_RECEM_NATO              VARCHAR2(1 BYTE),
  NR_CIRURGIA                NUMBER(10),
  CD_ESPECIALIDADE_MEDICO    NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_SAEP                NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  IE_EVOLUCAO_DOR            VARCHAR2(1 BYTE),
  NR_SEQ_PEPO                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_ATEND_ALTA              NUMBER(10),
  NR_SEQ_APROV               NUMBER(10),
  NR_SEQ_RP_TRATAMENTO       NUMBER(10),
  IE_RESTRICAO_VISUALIZACAO  VARCHAR2(1 BYTE),
  NR_RECEM_NATO              NUMBER(3),
  QT_CARACTERES              NUMBER(15),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_UNIDADE                 VARCHAR2(100 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_PEND_PAC_ACAO       NUMBER(10),
  QT_PERIODO                 NUMBER(10),
  IE_PERIODO                 VARCHAR2(15 BYTE),
  DT_RETORNO_PREV            DATE,
  NR_SEQ_REGULACAO           NUMBER(10),
  NR_SEQ_AVALIACAO           NUMBER(10),
  NR_SEQ_INT_PRESCR          NUMBER(10),
  NR_SEQ_REMOCAO             NUMBER(10),
  IE_PERFOR_STAUS_ZUBROD     NUMBER(3),
  DT_ULTIMA_QUIMIO           DATE,
  NR_SEQ_SAE                 NUMBER(10),
  IE_RELEV_RESUMO_ALTA       VARCHAR2(1 BYTE),
  NR_SEQ_FORMULARIO          NUMBER(10),
  IE_PALM                    VARCHAR2(1 BYTE),
  DS_LISTA_PROBLEMAS         VARCHAR2(4000 BYTE),
  NR_SEQ_EHR_REG             NUMBER(10),
  DS_IMPRESSAO               VARCHAR2(255 BYTE),
  NR_SEQ_EVOL_GRAV           NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  CD_HANDOVER_NURSE          VARCHAR2(10 BYTE),
  DT_HANDOVER_ACCEPTED       DATE,
  IE_ACAO                    VARCHAR2(1 BYTE),
  CD_DEPARTAMENTO            NUMBER(10),
  DT_CLI_START               DATE,
  DT_CLI_END                 DATE,
  CD_DIS_CATEGORY            NUMBER(2),
  CD_DOENCA                  VARCHAR2(10 BYTE),
  IE_TIPO_ATENDIMENTO        NUMBER(3),
  CD_INSURANCE               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EVOPACI_ATCONSPEPA_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_ATEPACI_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_ATEPACI_FK2_I ON TASY.EVOLUCAO_PACIENTE
(NR_ATEND_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_CANESDO_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_ESTADO_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_CANESDO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_CANESPA_FK_I ON TASY.EVOLUCAO_PACIENTE
(IE_ESTADO_PAC_FIM_TRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_CANESPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_CIDOTOP_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_CIDOTOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_CIRURGI_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_EHRREGI_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_EHR_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_EMEREEV_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_REGULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_EMEREEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_EMEREGU_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_REMOCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_EMEREGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_ESPMEDI_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_ESPMEDI_FK2_I ON TASY.EVOLUCAO_PACIENTE
(CD_ESPECIALIDADE_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_EVOPAGR_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_EVOL_GRAV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_GQAPPAC_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_PEND_PAC_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_GQAPPAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_I1 ON TASY.EVOLUCAO_PACIENTE
(IE_TIPO_EVOLUCAO, DT_EVOLUCAO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I10 ON TASY.EVOLUCAO_PACIENTE
(DT_EVOLUCAO, CD_MEDICO, NM_USUARIO, NM_USUARIO_NREC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I11 ON TASY.EVOLUCAO_PACIENTE
(NM_USUARIO, DT_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          984K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I12 ON TASY.EVOLUCAO_PACIENTE
(NR_ATENDIMENTO, DT_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I13 ON TASY.EVOLUCAO_PACIENTE
(CD_PESSOA_FISICA, DT_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I14 ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I15 ON TASY.EVOLUCAO_PACIENTE
(NR_ATENDIMENTO, IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I2 ON TASY.EVOLUCAO_PACIENTE
(NM_USUARIO, DT_EVOLUCAO, DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I3 ON TASY.EVOLUCAO_PACIENTE
(IE_TIPO_EVOLUCAO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I4 ON TASY.EVOLUCAO_PACIENTE
(DT_EVOLUCAO, CD_MEDICO, CD_MEDICO_PARECER, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I5 ON TASY.EVOLUCAO_PACIENTE
(DT_EVOLUCAO, IE_EVOLUCAO_CLINICA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I6 ON TASY.EVOLUCAO_PACIENTE
(DT_EVOLUCAO, CD_MEDICO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I7 ON TASY.EVOLUCAO_PACIENTE
(DT_EVOLUCAO, DT_LIBERACAO, IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I8 ON TASY.EVOLUCAO_PACIENTE
(IE_EVOLUCAO_CLINICA, CD_PESSOA_FISICA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_I9 ON TASY.EVOLUCAO_PACIENTE
(IE_TIPO_EVOLUCAO, CD_PESSOA_FISICA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_MEDAVPA_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_MEDPARE_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_MEDICO_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_NAISINS_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_PEPOCIR_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_PEPRESC_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_SAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_PEPRESC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_PERFIL_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_PESFISI_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_PESFISI_FK1_I ON TASY.EVOLUCAO_PACIENTE
(CD_HANDOVER_NURSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EVOPACI_PK ON TASY.EVOLUCAO_PACIENTE
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_PRESPRO_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_INT_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_PROFISS_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_RPAPRTR_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_RPAPRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_RPTRATA_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_RP_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_RPTRATA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_SAEPERO_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_SETATEN_FK_I ON TASY.EVOLUCAO_PACIENTE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVOPACI_TASASDI_FK_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_TASASDI_FK2_I ON TASY.EVOLUCAO_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_TIPEVOL_FK_I ON TASY.EVOLUCAO_PACIENTE
(IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVOPACI_TIPEVOL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVOPACI_10 ON TASY.EVOLUCAO_PACIENTE
(IE_TIPO_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.evolucao_paciente_pend_atual
after insert or update ON TASY.EVOLUCAO_PACIENTE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
nm_usuario_w		varchar2(15) := :new.nm_usuario;


begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (:new.dt_liberacao is null) then

     if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
	   (:new.DT_LIBERACAO_AUX is not null)then

		select 	obter_usuario_pf(:new.cd_medico)
		into	nm_usuario_w
		from	dual;

		Update    pep_item_pendente
		set		nm_usuario = nm_usuario_w
		where	CD_EVOLUCAO = :new.cd_evolucao;

	end if;

end if;

if	(Obter_Valor_Param_Usuario(0,37, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) = 'S') then
	if	(nvl(:new.ie_avaliador_aux,'N') = 'S') and
		(:new.dt_liberacao_aux is null) then
		ie_tipo_w := 'E';
		nm_usuario_w := :new.nm_usuario_nrec;
	elsif	(nvl(:new.ie_avaliador_aux,'N') = 'S') and
		(:new.dt_liberacao_aux is not null) and
		(:new.dt_liberacao is null) then
		ie_tipo_w := 'XE';
		nm_usuario_w := :new.nm_usuario_nrec;
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.cd_evolucao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w);
		ie_tipo_w := 'E';
		nm_usuario_w := substr(obter_usuario_pessoa(:new.cd_medico),1,15);
	elsif	(:new.dt_liberacao is null) then
		ie_tipo_w := 'E';
		nm_usuario_w := :new.nm_usuario;
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XE';
		nm_usuario_w := :new.nm_usuario;
	end if;

	if	(ie_tipo_w	is not null)then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.cd_evolucao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.EVOLUCAO_PACIENTE_tp  after update ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_EVOLUCAO);  ds_c_w:=null;  exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.BLOQUEIO_EVOLUCAO_PACIENTE
BEFORE INSERT OR UPDATE ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE
  IE_SOAP_W       VARCHAR2(1) := 'N';
  IE_INTEGRACAO_W VARCHAR2(1) := 'N';
  QT_REG_W		    NUMBER(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

  SELECT NVL(MAX('S'),'N')
    INTO IE_INTEGRACAO_W
    FROM EVOLUCAO_PACIENTE_INT EPI
  WHERE EPI.NR_SEQ_EVOLUCAO = :NEW.CD_EVOLUCAO;

Select 	nvl(max('S'),'N')
into	ie_soap_w
from 	evolucao_paciente_vinculo
where	cd_evolucao = :new.cd_evolucao
and	ie_tipo_item = 'SOAP';

	if ((ie_soap_w = 'N') and
    (IE_INTEGRACAO_W = 'N') AND
		(:old.nm_usuario is not null) and
	    (:new.dt_liberacao < :new.dt_atualizacao) and
	    (:new.dt_inativacao is null) and
	    (nvl(:old.nr_seq_assinatura,0) = nvl(:new.nr_seq_assinatura,0))) then
		if (inserting) then
			insert into log_tasy (
				cd_log,
				ds_log,
				dt_atualizacao,
				nm_usuario,
				nr_sequencia)
			values (
				54993,
				substr(' ;Insert ;cd_evolucao = ' || :new.cd_evolucao ||
				' ;nr_atendimento = ' || :new.nr_atendimento ||
				' ;dt_atualizacao_nrec = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_atualizacao_nrec, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;dt_atualizacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_atualizacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;dt_evolucao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_evolucao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;dt_liberacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;nm_usuario = ' || :new.nm_usuario ||
				' ;nm_usuario_nrec = ' || :new.nm_usuario_nrec ||
				' ;Usuario = ' || OBTER_USUARIO_ATIVO ||
				' ;Perfil = ' || OBTER_PERFIL_ATIVO ||
				' ;Funcao = ' || OBTER_FUNCAO_ATIVA ||
				' ;new.cd_medico = '||:new.cd_medico ||
				' ;old.cd_medico = '||:old.cd_medico ||
				' ;old.nm_usuario = '||:old.nm_usuario ||
				' ;old.dt_atualizacao_nrec = '||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_atualizacao_nrec, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;old.dt_atualizacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_atualizacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;old.dt_evolucao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_evolucao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;new.qt_caracteres = ' || :new.qt_caracteres ||
				' ;old.qt_caracteres = ' || :old.qt_caracteres ||
				' ;Stack: ' || substr(dbms_utility.format_call_stack,1,1900),1,1990),
				sysdate,
				OBTER_USUARIO_ATIVO,
				log_tasy_seq.nextval);

		else
			insert into log_tasy (
				cd_log,
				ds_log,
				dt_atualizacao,
				nm_usuario,
				nr_sequencia)
			values (
				54993,
				substr(' ;Update ;cd_evolucao = ' || :new.cd_evolucao ||
				' ;nr_atendimento = ' || :new.nr_atendimento ||
				' ;dt_atualizacao_nrec = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_atualizacao_nrec, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;dt_atualizacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_atualizacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;dt_evolucao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_evolucao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;dt_liberacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;nm_usuario = ' || :new.nm_usuario ||
				' ;nm_usuario_nrec = ' || :new.nm_usuario_nrec ||
				' ;Usuario = ' || OBTER_USUARIO_ATIVO ||
				' ;Perfil = ' || OBTER_PERFIL_ATIVO ||
				' ;Funcao = ' || OBTER_FUNCAO_ATIVA ||
				' ;new.cd_medico = '||:new.cd_medico ||
				' ;old.cd_medico = '||:old.cd_medico ||
				' ;old.nm_usuario = '||:old.nm_usuario ||
				' ;old.dt_atualizacao_nrec = '||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_atualizacao_nrec, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;old.dt_atualizacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_atualizacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;old.dt_evolucao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_evolucao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;old.dt_liberacao = ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||
				' ;new.qt_caracteres = ' || :new.qt_caracteres ||
				' ;old.qt_caracteres = ' || :old.qt_caracteres ||
				' ;Stack: ' || substr(dbms_utility.format_call_stack,1,1900),1,1990),
				sysdate,
				OBTER_USUARIO_ATIVO,
				log_tasy_seq.nextval);
		end if;

		wheb_mensagem_pck.exibir_mensagem_abort(1098265);--Esta nota clinica nao pode ser alterada! Ja existe data de liberacao.
	end if;

	if	((nvl(:new.nr_atendimento, 0) > 0) and
		(nvl(:new.cd_pessoa_fisica, '0') <> '0') and
		(:new.cd_pessoa_fisica <> obter_pessoa_atendimento(:new.nr_atendimento,'C'))) then
			wheb_mensagem_pck.exibir_mensagem_abort(15270);--Nao foi possivel enviar a sua solicitacaoo. Favor contactar o Administrador.
	end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.EVOLUCAO_PACIENTE_ATUAL
BEFORE INSERT OR UPDATE ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE

cd_perfil_ativo_w	NUMBER(5);
ie_relev_resumo_alta_w	varchar2(10);

BEGIN

cd_perfil_ativo_w	:=	Obter_perfil_Ativo;

IF	(:NEW.cd_perfil_ativo IS NULL) AND
	(cd_perfil_ativo_w <> 0) THEN
	:NEW.cd_perfil_ativo	:= cd_perfil_ativo_w;
END IF;

if	(:new.DT_EVOLUCAO is not null) then
	:new.ds_utc		:= substr(obter_data_utc(:new.DT_EVOLUCAO, 'HV'),1,50);
end if;

if	((:old.DT_LIBERACAO is null) or (:old.DT_LIBERACAO <> :new.DT_LIBERACAO)) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= substr(obter_data_utc(:new.DT_LIBERACAO,'HV'),1,50);
end if;

if ((:old.DT_LIBERACAO_AUX is null) or (:old.DT_LIBERACAO_AUX <> :new.DT_LIBERACAO_AUX)) and
	(:new.DT_LIBERACAO_AUX is not null) and
	(:new.IE_AVALIADOR_AUX = 'S') and (:new.CD_AVALIADOR_AUX is null) then
	:new.CD_AVALIADOR_AUX :=  obter_pf_usuario(:new.nm_usuario,'C');
end if;

begin

if	(:new.cd_setor_atendimento is null) and
	(:new.nr_atendimento is not null) then
	:new.cd_setor_atendimento	:= obter_setor_atendimento(:new.nr_atendimento);
end if;

exception
when others then
	null;
end;

END;
/


CREATE OR REPLACE TRIGGER TASY.EVOLUCAO_PACIENTE_AFTUPDATE
AFTER UPDATE ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE

qt_reg_w				number(1);
qt_idade_w				number(10);
cd_estabelecimento_w	number(10);
nr_seq_evento_w			number(10);
ie_evolucao_clinica_w	varchar2(3);
nm_usuario_w			varchar2(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_evolucao_w			number(10);
ie_receipt_symptom_w  varchar2(10);
json_data_w clob;
ds_param_integration_w  varchar2(500);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	ie_evento_disp = 'IEV'
	and	(nvl(cd_tipo_evolucao,ie_evolucao_clinica_w)	= ie_evolucao_clinica_w)
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

	select	nvl(max(ie_receipt_symptom),'N')
	into	ie_receipt_symptom_w
	from	tipo_evolucao
	where	cd_tipo_evolucao = :new.IE_EVOLUCAO_CLINICA;

if	(:old.dt_inativacao is null) and
	(:new.dt_inativacao is not null) then
	BEGIN

	qt_idade_w	:= nvl(obter_idade_pf(:old.cd_pessoa_fisica,sysdate,'A'),0);
	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
	nm_usuario_w:= wheb_usuario_pck.get_nm_usuario;
	ie_evolucao_clinica_w	:= :old.ie_evolucao_clinica;
	nr_atendimento_w		:= :old.nr_atendimento;
	cd_pessoa_fisica_w		:= :old.cd_pessoa_fisica;
	cd_evolucao_w			:= :old.cd_evolucao;
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,wheb_usuario_pck.get_nm_usuario,null);
		end;
	end loop;
	close C01;

	exception
	when others then
		null;
	end;
end if;

if ((:old.dt_liberacao is null) and (:new.dt_liberacao is not null) and (ie_receipt_symptom_w = 'S')) then
 begin
    ds_param_integration_w :=  '{"recordId" : "' || :new.cd_evolucao|| '"' || '}';
    json_data_w := bifrost.send_integration_content('nais.receipt.symptom', ds_param_integration_w, wheb_usuario_pck.get_nm_usuario);
  end;
end if;

if ((:old.dt_inativacao is null) and (:new.dt_inativacao is not null) and (ie_receipt_symptom_w = 'S')) then
 begin
    ds_param_integration_w :=  '{"recordId" : "' || :new.cd_evolucao|| '"' || '}';
    json_data_w := bifrost.send_integration_content('nais.receipt.symptom', ds_param_integration_w, wheb_usuario_pck.get_nm_usuario);
  end;
 end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.Evolucao_Paciente_Insert
AFTER INSERT ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE

dt_entrada_w			Date;
ie_medico_w			varchar2(1);
cd_estabelecimento_w		Number(4);
qt_hora_w			Number(15,4);
qt_hora_med_w			Number(15,4);
qt_hora_retroativa_w		Number(15,4);
ie_gera_lancamento_w		varchar2(1);
ie_regra_libera_evolucao_w	parametro_faturamento.ie_regra_libera_evolucao%type;
BEGIN

select 	nvl(max('S'),'N')
into	ie_medico_w
from	Medico
where 	cd_pessoa_fisica	= :new.cd_medico;

if	(:new.dt_evolucao > sysdate) and
	(:new.nr_atendimento is not null) then
	--A data da evolu��o n�o pode ser maior que a data atual.#@#@');
	Wheb_mensagem_pck.exibir_mensagem_abort(262188);
end if;

if	(:new.nr_atendimento is not null) and (:new.nr_atendimento > 0) then
	select	dt_entrada,
		cd_estabelecimento
	into	dt_entrada_w,
		cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;
	qt_hora_retroativa_w		:= Obter_Valor_Param_Usuario(281,592,obter_Perfil_Ativo,:new.nm_usuario,0);
	select	nvl(max(nvl(to_number(qt_hora_retroativa_w),qt_horas_passado_evo)),-1),
		nvl(max(qt_horas_passado_evo_med),-1)
	into	qt_hora_w,
		qt_hora_med_w
	from	parametro_medico
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(qt_hora_w >= 0) and
		(ie_medico_w = 'N') and
		(:new.dt_evolucao < (sysdate - qt_hora_w/24)) then
		--A data da evolu��o n�o pode ser menor que '|| to_char(qt_hora_w)||' hora(s) em rela��o a data atual.#@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(262189, 'QT_HORA='||to_char(qt_hora_w));
	end if;

	if	(qt_hora_med_w >= 0) and
		(ie_medico_w = 'S') and
		(:new.dt_evolucao < (sysdate - qt_hora_med_w/24)) then
		--A data da evolu��o n�o pode ser menor que '|| to_char(qt_hora_med_w)||' hora(s) em rela��o a data atual.#@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(262190, 'QT_HORA='||to_char(qt_hora_med_w));
	end if;

	if	(dt_entrada_w > :new.dt_evolucao) then
		--A data da evolu��o n�o pode ser menor que a data de entrada do atendimento.#@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(262191);
	end if;
end if;


select 	nvl(max(ie_regra_libera_evolucao),'M')
into	ie_regra_libera_evolucao_w
from 	parametro_faturamento
where 	cd_estabelecimento = cd_estabelecimento_w;

if	(nvl(:new.ie_evolucao_clinica,'0') <> '0') then

	select 	nvl(max(ie_gera_lancto_auto),'N')
	into	ie_gera_lancamento_w
	from 	tipo_evolucao
	where 	cd_tipo_evolucao = :new.ie_evolucao_clinica;
else
	ie_gera_lancamento_w:= 'N';
end if;
wheb_usuario_pck.set_ie_commit('N');
if	(((ie_regra_libera_evolucao_w = 'M') and (ie_medico_w = 'S')) or
	 ((ie_regra_libera_evolucao_w = 'R') and (ie_gera_lancamento_w = 'S'))) then
	gerar_lancamento_automatico(:new.nr_atendimento, null, 12, :new.nm_usuario, :new.cd_evolucao, :new.cd_medico,to_char(:new.dt_evolucao,'dd/mm/yyyy hh24:mi:ss'), :new.ie_evolucao_clinica, :new.cd_medico_parecer, null);
end if;
wheb_usuario_pck.set_ie_commit('S');

END;
/


CREATE OR REPLACE TRIGGER TASY.Evolucao_Paciente_update
BEFORE UPDATE ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE

dt_entrada_w		Date;
dt_alta_w		date;
cd_estabelecimento_w	Number(4);
qt_hora_w		Number(15,4);
qt_hora_med_w		Number(15,4);
ie_medico_w		varchar2(1);
qt_reg_w	number(1);
ie_evolucao_clinica_alta_w	varchar2(10);
qt_hora_retroativa_w	Number(15,4);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

begin
	if (:old.dt_inativacao is null and :new.dt_inativacao is not null) then
		inativar_registro_template(:new.nr_seq_ehr_reg, :new.nm_usuario_inativacao, :new.ds_justificativa);
	end if;
exception
  when others then
  null;
end;

if	(nvl(:new.ie_situacao,'A') = nvl(:old.ie_situacao,'A')) then
	begin
	select 	nvl(max('S'),'N')
	into	ie_medico_w
	from	Medico
	where 	cd_pessoa_fisica	= :new.cd_medico;

	if	(:new.dt_evolucao > sysdate) and
		(:new.nr_atendimento is not null) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267636);
	end if;

	if	(:new.nr_atendimento is not null) then
		select	dt_entrada,
			cd_estabelecimento,
			dt_alta
		into	dt_entrada_w,
			cd_estabelecimento_w,
			dt_alta_w
		from	atendimento_paciente
		where	nr_atendimento	= :new.nr_atendimento;

		ie_evolucao_clinica_alta_w	:= Obter_Valor_Param_Usuario(281,477,obter_Perfil_Ativo,:new.nm_usuario,0);
		qt_hora_retroativa_w		:= Obter_Valor_Param_Usuario(281,592,obter_Perfil_Ativo,:new.nm_usuario,0);

		select	nvl(max(nvl(to_number(qt_hora_retroativa_w),qt_horas_passado_evo)),-1),
			nvl(max(qt_horas_passado_evo_med),-1)
		into	qt_hora_w,
			qt_hora_med_w
		from	parametro_medico
		where	cd_estabelecimento	= cd_estabelecimento_w;


		if	((:old.nr_atendimento) = (:new.nr_atendimento)) and
			(qt_hora_w >= 0) and
			(ie_medico_w = 'N') and
			(:new.dt_evolucao < (sysdate - qt_hora_w/24)) and
			(nvl(:old.nr_seq_assinatura,0) = nvl(:new.nr_seq_assinatura,0)) then
			Wheb_mensagem_pck.exibir_mensagem_abort(267637,	'QT_HORA_W='||QT_HORA_W);

		end if;


		if	((:old.nr_atendimento) = (:new.nr_atendimento)) and
			(qt_hora_med_w >= 0) and
			(ie_medico_w = 'S') and
			(:new.dt_evolucao < (sysdate - qt_hora_med_w/24)) and
			(nvl(:old.nr_seq_assinatura,0) = nvl(:new.nr_seq_assinatura,0)) then
			Wheb_mensagem_pck.exibir_mensagem_abort(267638,	'QT_HORA_MED_W='||QT_HORA_MED_W);
		end if;
		if	(:old.nr_atendimento	is null) and
			(:new.nr_atendimento is not null) and
			(dt_alta_w	is not null) and
			(ie_evolucao_clinica_alta_w is not null) then
			:new.ie_evolucao_clinica		:= ie_evolucao_clinica_alta_w;
		end if;

		if	((dt_entrada_w > :new.dt_evolucao) and (:old.nr_atendimento = :new.nr_atendimento)) then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267640);
		end if;
	end if;
	end;
end if;
<<Final>>
qt_reg_w	:= 0;


if	(:old.dt_liberacao is not null) and
	(:new.dt_liberacao is null) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267641);
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is not null) and
	(:old.dt_liberacao	<> :new.dt_liberacao)and
	(:old.nr_atendimento = :new.nr_atendimento) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267643);
end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.Evolucao_Paciente_befInsert
before INSERT ON TASY.EVOLUCAO_PACIENTE FOR EACH ROW
DECLARE

dt_entrada_w		Date;
ie_medico_w		varchar2(1);
cd_estabelecimento_w	Number(4);
qt_hora_w		Number(15,4);
qt_hora_med_w		Number(15,4);
qt_hora_retroativa_w	Number(15,4);

begin

    IF ( wheb_usuario_pck.get_ie_executar_trigger = 'S' ) THEN
        SELECT
            nvl(MAX(ie_tipo_evolucao), 1)
        INTO :new.ie_tipo_evolucao
        FROM
            usuario
        WHERE
            nm_usuario = :new.nm_usuario;
    END IF;

:new.CD_UNIDADE	:= Obter_Ult_Unid_Atendimento(:new.nr_atendimento);

exception
	when others then
	null;
end;
/


CREATE OR REPLACE TRIGGER TASY.evolucao_paciente_delete
before delete ON TASY.EVOLUCAO_PACIENTE for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

ie_perm_exc_evol_lib_w	varchar2(1);
ie_possui_w varchar2(1);
qt_reg_w	number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Obter_param_Usuario(916, 1077,  obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_perm_exc_evol_lib_w);

if	( :old.dt_liberacao is not null) and
	(ie_perm_exc_evol_lib_w = 'N') then
	Wheb_mensagem_pck.exibir_mensagem_abort(248453);
end if;

select 	nvl(max('S'),'N')
into	ie_possui_w
from 	evolucao_paciente_compl
where 	nr_seq_evo_paciente = :old.cd_evolucao;

if(ie_possui_w = 'S') then

	delete from evolucao_paciente_compl
	where nr_seq_evo_paciente = :old.cd_evolucao;

	commit;

end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.evolucao_paciente_atual_lib
before update ON TASY.EVOLUCAO_PACIENTE for each row
declare

ie_soap_w  varchar2(1) := 'N';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Select 	nvl(max('S'),'N')
into	ie_soap_w
from 	evolucao_paciente_vinculo
where	cd_evolucao = :new.cd_evolucao
and		ie_tipo_item = 'SOAP';

if 	(ie_soap_w = 'N') and
	(:old.dt_liberacao = :new.dt_liberacao) and
	(:new.dt_liberacao is not null ) and
	(:new.dt_inativacao is null) and
	(nvl(:new.nr_atendimento,0) = nvl(:old.nr_atendimento,0)) and
	(nvl(:new.nr_seq_formulario,0) = nvl(:old.nr_seq_formulario,0)) and
	(nvl(:new.nr_seq_assinatura,0)  = nvl(:old.nr_seq_assinatura,0)) and
	(nvl(:old.ie_relev_resumo_alta,'X') = nvl(:new.ie_relev_resumo_alta,'X')) and
	(nvl(:old.cd_medico,0) = nvl(:new.cd_medico,0))	then

	gravar_log_tasy(13580, 	substr(' :new.cd_evolucao '||:new.cd_evolucao||
							' :new.nr_atendimento '||:new.nr_atendimento||
							' Usuario: '||OBTER_USUARIO_ATIVO||
							' Perfil: '||OBTER_PERFIL_ATIVO||
							' Funcao: '||OBTER_FUNCAO_ATIVA||
							' :new.qt_caracteres '||:new.qt_caracteres||
							' :old.qt_caracteres '||:old.qt_caracteres||
							' Stack: '||dbms_utility.format_call_stack,1,2000), 'TASY');

	if(OBTER_FUNCAO_ATIVA in(281,10030)) then
		wheb_mensagem_pck.exibir_mensagem_abort(1098265);--Esta nota clinica nao pode ser alterada! Ja existe data de liberacao.
	end if;

end if;

<<Final>>
null;

end;
/


CREATE OR REPLACE TRIGGER TASY.EVOLUCAO_PACIENTE_AFTUPD
after update ON TASY.EVOLUCAO_PACIENTE for each row
declare

ie_integrar_gerint_w			tipo_evolucao.ie_integrar_gerint%type;
ie_regulacao_gerint_w			parametro_atendimento.ie_regulacao_gerint%type;
nr_protocolo_solicitacao_w		gerint_solic_internacao.nr_protocolo_solicitacao%type;
ds_sep_bv_w						varchar2(50);

begin

	--Valida se a evolu��o informada gera evento para integra��o do Gerint.
	select	nvl(max(ie_integrar_gerint),'N')
	into	ie_integrar_gerint_w
	from	tipo_evolucao
	where	cd_tipo_evolucao = :new.IE_EVOLUCAO_CLINICA;

	if (ie_integrar_gerint_w = 'S') then
		--Verifica se utiliza regula��o de leitos para o estado do Rio Grande do Sul.
		select	nvl(max(ie_regulacao_gerint),'N')
		into	ie_regulacao_gerint_w
		from	parametro_atendimento
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

		select	max(nr_protocolo_solicitacao)
		into	nr_protocolo_solicitacao_w
		from	gerint_solic_internacao
		where	nr_atendimento = :new.nr_atendimento
		and		ie_situacao <> 'L'; --Alta Gerada

		if (((:old.dt_liberacao is null) and (:new.dt_liberacao is not null)) and (ie_regulacao_gerint_w = 'S') and (nr_protocolo_solicitacao_w is not null)) then

			ds_sep_bv_w	:=	obter_separador_bv;

			exec_sql_dinamico_bv('TASY', Gerint_desc_de_para('QUERY'), 	'nm_usuario_p='					|| :new.nm_usuario  						|| ds_sep_bv_w ||
																		'cd_estabelecimento_p=' 		|| wheb_usuario_pck.get_cd_estabelecimento 	|| ds_sep_bv_w ||
																		'id_evento_p=' 					|| '10'				 						|| ds_sep_bv_w ||
																		'nr_atendimento_p=' 			|| :new.nr_atendimento 						|| ds_sep_bv_w ||
																		'ie_leito_extra_p=' 			|| null										|| ds_sep_bv_w ||
																		'dt_alta_p=' 					|| null 									|| ds_sep_bv_w ||
																		'ds_motivo_alta_p=' 			|| null 									|| ds_sep_bv_w ||
																		'ds_justif_transferencia_p='	|| null 									|| ds_sep_bv_w ||
																		'nr_seq_solic_internacao_p=' 	|| null 									|| ds_sep_bv_w ||
																		'nr_seq_leito_p=' 				|| null										|| ds_sep_bv_w ||
																		'ds_ident_leito_p='				|| null										|| ds_sep_bv_w ||
																		'nr_seq_classif_p=' 			|| null										|| ds_sep_bv_w ||
																		'ie_status_unidade_p=' 			|| null										|| ds_sep_bv_w ||
																		'nr_cpf_paciente_p=' 			|| null										|| ds_sep_bv_w ||
																		'nr_cartao_sus_p=' 				|| null										|| ds_sep_bv_w ||
																		'cd_cid_p='						|| null										|| ds_sep_bv_w ||
																		'cd_evolucao_p='				||:new.cd_evolucao);
			--Gerar_evento_gerint(:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,10,:new.nr_atendimento,null,null,null,null,null,null,null,null,null,null);

		end if;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.EVOLUCAO_PACIENTE_SBIS_DEL
before delete ON TASY.EVOLUCAO_PACIENTE for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nm_usuario,
									 nr_atendimento) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 nvl(obter_pessoa_atendimento(:old.nr_atendimento,'C'), :old.cd_pessoa_fisica),
									 obter_desc_expressao(792013),
									 nvl(wheb_usuario_pck.get_nm_usuario,:old.nm_usuario),
									 :new.nr_atendimento);

end;
/


CREATE OR REPLACE TRIGGER TASY.EVOLUCAO_PACIENTE_SBIS_INS
before insert or update ON TASY.EVOLUCAO_PACIENTE for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nm_usuario,
										 nr_atendimento) values
										 (nr_log_seq_w,
										 sysdate,
										obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
									     nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica),
									     obter_desc_expressao(792013),
										 nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario),
										 :new.nr_atendimento);
else
	ie_inativacao_w := 'N';


	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nm_usuario,
										 nr_atendimento) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
									     nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica),
									    obter_desc_expressao(792013),
										 nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario),
										 :new.nr_atendimento);
end if;


end;
/


ALTER TABLE TASY.EVOLUCAO_PACIENTE ADD (
  CONSTRAINT EVOPACI_PK
 PRIMARY KEY
 (CD_EVOLUCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EVOLUCAO_PACIENTE ADD (
  CONSTRAINT EVOPACI_NAISINS_FK 
 FOREIGN KEY (CD_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT EVOPACI_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EVOPACI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EVOPACI_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT EVOPACI_MEDPARE_FK 
 FOREIGN KEY (CD_MEDICO_PARECER) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EVOPACI_CIDOTOP_FK 
 FOREIGN KEY (CD_TOPOGRAFIA) 
 REFERENCES TASY.CIDO_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT EVOPACI_CANESDO_FK 
 FOREIGN KEY (CD_ESTADO_DOENCA) 
 REFERENCES TASY.CAN_ESTADO_DOENCA (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_CANESPA_FK 
 FOREIGN KEY (IE_ESTADO_PAC_FIM_TRAT) 
 REFERENCES TASY.CAN_ESTADO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_TIPEVOL_FK 
 FOREIGN KEY (IE_EVOLUCAO_CLINICA) 
 REFERENCES TASY.TIPO_EVOLUCAO (CD_TIPO_EVOLUCAO),
  CONSTRAINT EVOPACI_ESPMEDI_FK2 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDICO) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT EVOPACI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EVOPACI_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT EVOPACI_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_EMEREGU_FK 
 FOREIGN KEY (NR_SEQ_REMOCAO) 
 REFERENCES TASY.EME_REGULACAO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EVOPACI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT EVOPACI_ATEPACI_FK2 
 FOREIGN KEY (NR_ATEND_ALTA) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT EVOPACI_RPTRATA_FK 
 FOREIGN KEY (NR_SEQ_RP_TRATAMENTO) 
 REFERENCES TASY.RP_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_RPAPRTR_FK 
 FOREIGN KEY (NR_SEQ_APROV) 
 REFERENCES TASY.RP_APROV_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_GQAPPAC_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC_ACAO) 
 REFERENCES TASY.GQA_PEND_PAC_ACAO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_EMEREEV_FK 
 FOREIGN KEY (NR_SEQ_REGULACAO) 
 REFERENCES TASY.EME_REG_EVOLUCAO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_MEDAVPA_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.MED_AVALIACAO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EVOPACI_PRESPRO_FK 
 FOREIGN KEY (NR_SEQ_INT_PRESCR) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_SEQ_INTERNO),
  CONSTRAINT EVOPACI_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_SAE) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_EHR_REG) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EVOPACI_EVOPAGR_FK 
 FOREIGN KEY (NR_SEQ_EVOL_GRAV) 
 REFERENCES TASY.EVOL_PACIENTE_GRAVIDADE (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT EVOPACI_PESFISI_FK1 
 FOREIGN KEY (CD_HANDOVER_NURSE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.EVOLUCAO_PACIENTE TO NIVEL_1;

