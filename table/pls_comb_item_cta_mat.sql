ALTER TABLE TASY.PLS_COMB_ITEM_CTA_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMB_ITEM_CTA_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMB_ITEM_CTA_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_COMBINACAO    NUMBER(10)               NOT NULL,
  IE_AND_OR            VARCHAR2(3 BYTE)         NOT NULL,
  NR_SEQ_GRUPO_REC     NUMBER(10),
  IE_GERA_OCORRENCIA   VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_ESTRUTURA     NUMBER(10),
  IE_TIPO_DESP_MAT     VARCHAR2(1 BYTE),
  NR_SEQ_MATERIAL      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCICM_GRURECE_FK_I ON TASY.PLS_COMB_ITEM_CTA_MAT
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCICM_GRURECE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCICM_PK ON TASY.PLS_COMB_ITEM_CTA_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCICM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCICM_PLSCICT_FK_I ON TASY.PLS_COMB_ITEM_CTA_MAT
(NR_SEQ_COMBINACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCICM_PLSCICT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCICM_PLSMAT_FK_I ON TASY.PLS_COMB_ITEM_CTA_MAT
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCICM_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCICM_PLSOCES_FK_I ON TASY.PLS_COMB_ITEM_CTA_MAT
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCICM_PLSOCES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_comb_item_cta_mat_atual
before insert or update or delete ON TASY.PLS_COMB_ITEM_CTA_MAT for each row
declare
nr_seq_combinacao_w	pls_combinacao_item_cta.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_combinacao_w := :old.nr_seq_combinacao;
else
	nr_seq_combinacao_w := :new.nr_seq_combinacao;
end if;

pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_COMBINACAO_ITEM_TM', wheb_usuario_pck.get_nm_usuario,
							'PLS_COMB_ITEM_CTA_MAT_ATUAL',
							'nr_seq_combinacao_p=' || nr_seq_combinacao_w);

end pls_comb_item_cta_mat_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_COMB_ITEM_CTA_MAT_tp  after update ON TASY.PLS_COMB_ITEM_CTA_MAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_GERA_OCORRENCIA,1,4000),substr(:new.IE_GERA_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_OCORRENCIA',ie_log_w,ds_w,'PLS_COMB_ITEM_CTA_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AND_OR,1,4000),substr(:new.IE_AND_OR,1,4000),:new.nm_usuario,nr_seq_w,'IE_AND_OR',ie_log_w,ds_w,'PLS_COMB_ITEM_CTA_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA,1,4000),substr(:new.NR_SEQ_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA',ie_log_w,ds_w,'PLS_COMB_ITEM_CTA_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESP_MAT,1,4000),substr(:new.IE_TIPO_DESP_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESP_MAT',ie_log_w,ds_w,'PLS_COMB_ITEM_CTA_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_COMB_ITEM_CTA_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'PLS_COMB_ITEM_CTA_MAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_COMB_ITEM_CTA_MAT ADD (
  CONSTRAINT PLSCICM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMB_ITEM_CTA_MAT ADD (
  CONSTRAINT PLSCICM_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSCICM_PLSOCES_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_OCORRENCIA_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSCICM_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT PLSCICM_PLSCICT_FK 
 FOREIGN KEY (NR_SEQ_COMBINACAO) 
 REFERENCES TASY.PLS_COMBINACAO_ITEM_CTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COMB_ITEM_CTA_MAT TO NIVEL_1;

