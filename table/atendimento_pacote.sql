ALTER TABLE TASY.ATENDIMENTO_PACOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_PACOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_PACOTE
(
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_PACOTE           NUMBER(10)            NOT NULL,
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  PR_AFATURAR             NUMBER(15,4)          NOT NULL,
  IE_TIPO_ACOMOD          VARCHAR2(2 BYTE),
  QT_DIAS_PACOTE          NUMBER(5),
  QT_DIAS_HOSPITAL        NUMBER(5),
  QT_DIAS_UTI             NUMBER(5),
  IE_EXCEDENTE            NUMBER(2),
  DT_INICIO_PACOTE        DATE,
  NR_SEQ_PROCEDIMENTO     NUMBER(10),
  NR_SEQ_PROC_ORIGEM      NUMBER(10),
  DT_FINAL_PACOTE         DATE,
  CD_SETOR_EXCLUSIVO      NUMBER(5),
  IE_CLASSIFICACAO        VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_ALOCACAO         NUMBER(3)             NOT NULL,
  NR_SEQ_TIPO_ACOMOD      NUMBER(10)            NOT NULL,
  IE_EXIGE_GABARITO       VARCHAR2(1 BYTE)      NOT NULL,
  IE_RATEAR_REPASSE       VARCHAR2(1 BYTE)      NOT NULL,
  VL_ORIGINAL             NUMBER(15,4),
  IE_RATEADO              VARCHAR2(1 BYTE),
  VL_AJUSTE               NUMBER(15,2),
  IE_CONSISTE_CIRURGIA    VARCHAR2(1 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  IE_PRIORIDADE           NUMBER(10),
  NR_SEQ_REGRA_TX         NUMBER(10),
  CD_PROCEDIMENTO_TUSS    NUMBER(15),
  PR_ADICIONAL_HORA       NUMBER(15,4),
  NR_SEQ_REGRA_ADIC_HORA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPACO_ATEPACI_FK_I ON TASY.ATENDIMENTO_PACOTE
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACO_CONVENI_FK_I ON TASY.ATENDIMENTO_PACOTE
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACO_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPACO_PACOTE_FK_I ON TASY.ATENDIMENTO_PACOTE
(NR_SEQ_PACOTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPACO_PK ON TASY.ATENDIMENTO_PACOTE
(NR_ATENDIMENTO, NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACO_PROPACI_FK_I ON TASY.ATENDIMENTO_PACOTE
(NR_SEQ_PROCEDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACO_PROPACI_FK_I2 ON TASY.ATENDIMENTO_PACOTE
(NR_SEQ_PROC_ORIGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPACO_SETATEN_FK_I ON TASY.ATENDIMENTO_PACOTE
(CD_SETOR_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPACO_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Atendimento_Pacote_Update
before UPDATE ON TASY.ATENDIMENTO_PACOTE FOR EACH ROW
declare

ie_status_acerto_w	number(10);
ds_module_log_w		varchar2(255);
ds_call_stack_w		varchar2(2000);

BEGIN

if	(:new.dt_inicio_pacote <> :old.dt_inicio_pacote) then

	select	max(b.ie_status_acerto)
	into	ie_status_acerto_w
	from	procedimento_paciente a,
		conta_paciente b
	where	a.nr_interno_conta	= b.nr_interno_conta
	and	a.nr_sequencia 		= :new.nr_seq_procedimento;

	update 	procedimento_paciente
	set 	dt_conta = :new.dt_inicio_pacote,
		dt_procedimento = :new.dt_inicio_pacote
	where 	nr_sequencia = :new.nr_seq_procedimento
	and	ie_status_acerto_w	= 1;

	/*insert into log_tasy	(	dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
		values		(	sysdate,
					:new.nm_usuario,
					66123,
					'Alterando a data do pacote de: '||
					to_char(:old.dt_inicio_pacote,'dd/mm/yyyy hh24:mi:ss') ||
					' Para : '||to_char(:new.dt_inicio_pacote,'dd/mm/yyyy hh24:mi:ss'));*/
end if;

if	(nvl(:new.nr_seq_procedimento,0) <> nvl(:old.nr_seq_procedimento,0)) then

	select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
	into	ds_module_log_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);

	ds_call_stack_w	:= substr(dbms_utility.format_call_stack,1,1800);

	insert into proc_pac_log
		(dt_atualizacao,
		dt_atualizacao_nrec,
		ie_acao,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_seq_propaci,
		nr_sequencia,
		qt_procedimento,
		ds_observacao,
		ds_call_stack,
		ds_module)
	values	(sysdate,
		sysdate,
		'AP',
		:new.nm_usuario,
		:new.nm_usuario,
		:new.nr_atendimento,
		:new.nr_seq_procedimento,
		proc_pac_log_seq.nextval,
		0,
		--'Alterado o n�mero de sequencia do procedimento pacote, dentro do pr�prio pacote.',
		WHEB_MENSAGEM_PCK.get_texto(298660),
		'OLD= ' || :old.nr_seq_procedimento || chr(10) || chr(13) || ds_call_stack_w,
		ds_module_log_w);

end if;

END;
/


ALTER TABLE TASY.ATENDIMENTO_PACOTE ADD (
  CONSTRAINT ATEPACO_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_PACOTE ADD (
  CONSTRAINT ATEPACO_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT ATEPACO_PACOTE_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.PACOTE (NR_SEQ_PACOTE),
  CONSTRAINT ATEPACO_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPACO_PROPACI_FK2 
 FOREIGN KEY (NR_SEQ_PROC_ORIGEM) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPACO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEPACO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXCLUSIVO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.ATENDIMENTO_PACOTE TO NIVEL_1;

