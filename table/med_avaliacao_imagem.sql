ALTER TABLE TASY.MED_AVALIACAO_IMAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_AVALIACAO_IMAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_AVALIACAO_IMAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  DS_TITULO            VARCHAR2(60 BYTE)        NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_AUTOMATICO        VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDAVAI_MEDTIAV_FK_I ON TASY.MED_AVALIACAO_IMAGEM
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVAI_MEDTIAV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDAVAI_PK ON TASY.MED_AVALIACAO_IMAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MED_AVALIACAO_IMAGEM ADD (
  CONSTRAINT MEDAVAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_AVALIACAO_IMAGEM ADD (
  CONSTRAINT MEDAVAI_MEDTIAV_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MED_AVALIACAO_IMAGEM TO NIVEL_1;

