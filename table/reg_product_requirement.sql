ALTER TABLE TASY.REG_PRODUCT_REQUIREMENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_PRODUCT_REQUIREMENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_PRODUCT_REQUIREMENT
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_CODE                  VARCHAR2(255 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_TITLE                 VARCHAR2(500 BYTE)   NOT NULL,
  DS_DESCRIPTION           CLOB,
  DT_APROVACAO             DATE,
  NR_CUSTOMER_REQUIREMENT  NUMBER(10),
  NR_SEQ_APRESENTACAO      NUMBER(10),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  DS_DESCRIPTION_PR        VARCHAR2(4000 BYTE),
  DT_LIBERACAO_DEV         DATE,
  DT_LIBERACAO_VV          DATE,
  NR_SEQ_IDIOMA            NUMBER(10),
  NM_ANALISTA_LIBEROU      VARCHAR2(15 BYTE),
  NR_SEQ_PRODUCT_SUP       NUMBER(10),
  NM_USUARIO_APROVACAO     VARCHAR2(15 BYTE),
  CD_VERSAO                VARCHAR2(255 BYTE),
  NM_LIBEROU_VV            VARCHAR2(15 BYTE),
  DT_LIBEROU_GER_VV        DATE,
  NM_LIBEROU_GER_VV        VARCHAR2(15 BYTE),
  NR_SEQ_TYPE              NUMBER(10),
  CD_PRS_ID                VARCHAR2(35 BYTE),
  IE_TIPO_REVISAO          VARCHAR2(2 BYTE)     DEFAULT null,
  DT_REVISAO               DATE                 DEFAULT null,
  NR_SEQ_REVISAO           NUMBER(10)           DEFAULT null,
  IE_CLINICO               VARCHAR2(1 BYTE),
  NR_SEQ_ORDEM_SERV        NUMBER(10),
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_MOTIVO_INATIVACAO     VARCHAR2(255 BYTE),
  DT_LIBERACAO             DATE,
  NM_USUARIO_LIBERACAO     VARCHAR2(15 BYTE),
  DS_RACIONAL_CLINICO      VARCHAR2(4000 BYTE),
  IE_APROVACAO_ESPEC       VARCHAR2(1 BYTE),
  NR_SEQ_INTENCAO_USO      NUMBER(10),
  NR_SEQ_ANALISE_IMPACTO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_DESCRIPTION) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGPR_MANORSE_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPR_MANOSIM_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQ_ANALISE_IMPACTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGPR_PK ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPR_REGCR_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_CUSTOMER_REQUIREMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPR_REGINU_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPR_REGPR_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQ_PRODUCT_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPR_REGPRTY_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQ_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPR_TASYIDI_FK_I ON TASY.REG_PRODUCT_REQUIREMENT
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_product_requirement_after
after update ON TASY.REG_PRODUCT_REQUIREMENT for each row
declare

ie_prs_lancado_w		varchar2(1);
ie_tipo_alteracao_w		reg_product_req_hist.ie_tipo_alteracao%type;
nr_customer_requirement_w	reg_product_req_hist.nr_customer_requirement%type;
ds_title_w			reg_product_req_hist.ds_title%type;
ds_description_pr_w		reg_product_req_hist.ds_description_pr%type;

begin

select	decode(count(1), 0, 'N', 'S')
into	ie_prs_lancado_w -- Determinar se o PRS j?oi liberado em alguma revis?do documento de PRS
from	reg_product_req_hist
where	nr_seq_product_req = :new.nr_sequencia
and	ie_tipo_alteracao = 'I';

if	(:new.dt_aprovacao is not null) and
	(:new.nm_usuario_aprovacao is not null) then

	if (ie_prs_lancado_w = 'S') then

		select	nr_customer_requirement,
			ds_title,
			ds_description_pr
		into	nr_customer_requirement_w,
			ds_title_w,
			ds_description_pr_w
		from	reg_product_req_hist
		where	nr_sequencia = (	select	max(nr_sequencia)
						from	reg_product_req_hist
						where	nr_seq_product_req = :new.nr_sequencia
						and	ie_tipo_alteracao in ('I', 'A')
					);

		if	(:old.ie_situacao = 'A') and
			(:new.ie_situacao = 'I') then
			ie_tipo_alteracao_w := 'E';
		elsif	(:new.nr_customer_requirement <> nr_customer_requirement_w) or
			(:new.ds_title <> ds_title_w) or
			(:new.ds_description_pr <> ds_description_pr_w) then
			ie_tipo_alteracao_w := 'A';
		end if;
	else
		if (:new.ie_situacao = 'A') then
			ie_tipo_alteracao_w := 'I';
		end if;
	end if;
end if;

if (ie_tipo_alteracao_w is not null) then

	insert into reg_product_req_hist (
		nr_sequencia,
		cd_code,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_title,
		ds_description,
		dt_aprovacao,
		nr_seq_apresentacao,
		nr_customer_requirement,
		ie_situacao,
		nm_usuario_aprovacao,
		cd_versao,
		nr_seq_reg_version_rev,
		dt_liberacao_dev,
		dt_liberacao_vv,
		nr_seq_idioma,
		nm_analista_liberou,
		nm_liberou_vv,
		dt_liberou_ger_vv,
		nm_liberou_ger_vv,
		ds_description_pr,
		nr_seq_product_req,
		cd_prs_id,
		nr_seq_type,
		nr_seq_product_sup,
		ie_tipo_alteracao,
		dt_inativacao,
		nm_usuario_inativacao,
		ds_motivo_inativacao,
		dt_liberacao,
		nm_usuario_liberacao,
		dt_inclusao_hist,
		dt_revisao,
		ie_tipo_revisao,
		nr_seq_revisao,
		ie_clinico,
		nr_seq_ordem_serv,
		ds_racional_clinico,
		ie_aprovacao_espec,
		nr_seq_intencao_uso)
	values (
		reg_product_req_hist_seq.nextval,
		:new.cd_code,
		:new.dt_atualizacao,
		:new.nm_usuario,
		:new.dt_atualizacao_nrec,
		:new.nm_usuario_nrec,
		:new.ds_title,
		:new.ds_description,
		:new.dt_aprovacao,
		:new.nr_seq_apresentacao,
		:new.nr_customer_requirement,
		:new.ie_situacao,
		:new.nm_usuario_aprovacao,
		:new.cd_versao,
		null,
		:new.dt_liberacao_dev,
		:new.dt_liberacao_vv,
		:new.nr_seq_idioma,
		:new.nm_analista_liberou,
		:new.nm_liberou_vv,
		:new.dt_liberou_ger_vv,
		:new.nm_liberou_ger_vv,
		:new.ds_description_pr,
		:new.nr_sequencia,
		:new.cd_prs_id,
		:new.nr_seq_type,
		:new.nr_seq_product_sup,
		ie_tipo_alteracao_w,
		:new.dt_inativacao,
		:new.nm_usuario_inativacao,
		:new.ds_motivo_inativacao,
		:new.dt_liberacao,
		:new.nm_usuario_liberacao,
		sysdate,
		:new.dt_revisao,
		:new.ie_tipo_revisao,
		:new.nr_seq_revisao,
		:new.ie_clinico,
		:new.nr_seq_ordem_serv,
		:new.ds_racional_clinico,
		:new.ie_aprovacao_espec,
		:new.nr_seq_intencao_uso);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.REG_PRODUCT_REQUIREMENT_INSERT
BEFORE INSERT ON TASY.REG_PRODUCT_REQUIREMENT FOR EACH ROW
DECLARE
	dt_aprovacao_w reg_customer_requirement.dt_aprovacao%TYPE;
BEGIN
	SELECT	dt_aprovacao
	INTO	dt_aprovacao_w
	FROM	reg_customer_requirement
	WHERE	nr_sequencia = :NEW.nr_customer_requirement;

	IF	dt_aprovacao_w IS NULL THEN
		wheb_mensagem_pck.exibir_mensagem_abort(901103);--N�o � possivel inserir um requisito de produto em um requisito de cliente n�o aprovado
	END IF;
END;
/


CREATE OR REPLACE TRIGGER TASY.reg_product_req_update

before insert ON TASY.REG_PRODUCT_REQUIREMENT 
for each row
declare



nr_seq_ap_area_w		    reg_area_customer.nr_seq_apresentacao%type;

nr_seq_ap_feat_w		    reg_features_customer.nr_seq_apresentacao%type;

nr_seq_ap_cust_w		    reg_customer_requirement.nr_seq_apresentacao%type;

nr_seq_intencao_uso_w   number(10);

ds_prefixo_w		        varchar2(15);

nr_sequencia_w          varchar2(15);

nr_seq_prs_w            number(10);


begin



if (:old.cd_prs_id is null) then

  if :new.nr_seq_intencao_uso is null then

    select ac.nr_seq_apresentacao,

           fc.nr_seq_apresentacao,

           cr.nr_seq_apresentacao,

           iu.nr_sequencia,

           iu.ds_prefixo

      into nr_seq_ap_area_w,

           nr_seq_ap_feat_w,

           nr_seq_ap_cust_w,

           nr_seq_intencao_uso_w,

           ds_prefixo_w

      from reg_area_customer ac,

           reg_features_customer fc,

           reg_intencao_uso iu,

           reg_customer_requirement cr

     where ac.nr_sequencia = fc.nr_seq_area_customer

       and ac.nr_seq_intencao_uso = iu.nr_sequencia

       and fc.nr_sequencia = cr.nr_seq_features

       and cr.nr_sequencia = :new.nr_customer_requirement;

  elsif :new.nr_seq_intencao_uso is not null then

      select iu.nr_sequencia,

             iu.ds_prefixo

        into nr_seq_intencao_uso_w,

             ds_prefixo_w

        from reg_intencao_uso iu

       where iu.nr_sequencia = :new.nr_seq_intencao_uso;

  end if;

  if nr_seq_intencao_uso_w = 2 then

    :new.cd_prs_id := 'TASY_PRS_ID_' || nr_seq_ap_area_w || '.' || nr_seq_ap_feat_w || '.' || nr_seq_ap_cust_w || '.' || :new.nr_sequencia;

  else

    select max(a.nr_sequencia)
      into nr_seq_prs_w
      from reg_product_requirement a
     where a.nr_seq_intencao_uso = nr_seq_intencao_uso_w;

    if nr_seq_prs_w is not null then

      select nvl(obter_somente_numero(cd_prs_id), 0) + 1
        into nr_sequencia_w
        from reg_product_requirement
       where nr_sequencia = nr_seq_prs_w;

    end if;

    :new.cd_prs_id := nvl(ds_prefixo_w, nr_seq_intencao_uso_w) || '_PRS_' || nvl(nr_sequencia_w, '1');

  end if;

end if;
:new.nr_seq_intencao_uso := nr_seq_intencao_uso_w;
end;
/


ALTER TABLE TASY.REG_PRODUCT_REQUIREMENT ADD (
  CONSTRAINT REGPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_PRODUCT_REQUIREMENT ADD (
  CONSTRAINT REGPR_REGPR_FK 
 FOREIGN KEY (NR_SEQ_PRODUCT_SUP) 
 REFERENCES TASY.REG_PRODUCT_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT REGPR_REGCR_FK 
 FOREIGN KEY (NR_CUSTOMER_REQUIREMENT) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA),
  CONSTRAINT REGPR_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT REGPR_REGPRTY_FK 
 FOREIGN KEY (NR_SEQ_TYPE) 
 REFERENCES TASY.REG_PRODUCT_TYPE (NR_SEQUENCIA),
  CONSTRAINT REGPR_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT REGPR_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA),
  CONSTRAINT REGPR_MANOSIM_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_IMPACTO) 
 REFERENCES TASY.MAN_ORDEM_SERV_IMPACTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_PRODUCT_REQUIREMENT TO NIVEL_1;

