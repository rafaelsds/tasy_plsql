ALTER TABLE TASY.TRANS_FINANC_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRANS_FINANC_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRANS_FINANC_CAMPO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_TRANS_FINANC   NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NM_ATRIBUTO           VARCHAR2(30 BYTE)       NOT NULL,
  IE_OBRIGATORIO        VARCHAR2(1 BYTE)        NOT NULL,
  NM_ATRIBUTO_MUT_EXCL  VARCHAR2(30 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_PRIMEIRO_FOCUS     VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_CAIXA          NUMBER(10),
  NR_SEQ_CONTA_BANCO    NUMBER(10),
  DS_PADRAO             VARCHAR2(255 BYTE),
  NR_SEQUENCIA_REF      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TRAFICP_BANESTA_FK_I ON TASY.TRANS_FINANC_CAMPO
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFICP_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRAFICP_CAIXA_FK_I ON TASY.TRANS_FINANC_CAMPO
(NR_SEQ_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRAFICP_CAIXA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TRAFICP_PK ON TASY.TRANS_FINANC_CAMPO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFICP_TRAFICP_FK_I ON TASY.TRANS_FINANC_CAMPO
(NR_SEQUENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRAFICP_TRAFINA_FK_I ON TASY.TRANS_FINANC_CAMPO
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TRANS_FINANC_CAMPO_tp  after update ON TASY.TRANS_FINANC_CAMPO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_ATRIBUTO,1,4000),substr(:new.NM_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_FINANC,1,4000),substr(:new.NR_SEQ_TRANS_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_FINANC',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGATORIO,1,4000),substr(:new.IE_OBRIGATORIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGATORIO',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ATRIBUTO_MUT_EXCL,1,4000),substr(:new.NM_ATRIBUTO_MUT_EXCL,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO_MUT_EXCL',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PADRAO,1,4000),substr(:new.DS_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PADRAO',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRIMEIRO_FOCUS,1,4000),substr(:new.IE_PRIMEIRO_FOCUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIMEIRO_FOCUS',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CAIXA,1,4000),substr(:new.NR_SEQ_CAIXA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CAIXA',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTA_BANCO,1,4000),substr(:new.NR_SEQ_CONTA_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTA_BANCO',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TRANS_FINANC_CAMPO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TRANS_FINANC_CAMPO ADD (
  CONSTRAINT TRAFICP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRANS_FINANC_CAMPO ADD (
  CONSTRAINT TRAFICP_TRAFICP_FK 
 FOREIGN KEY (NR_SEQUENCIA_REF) 
 REFERENCES TASY.TRANS_FINANC_CAMPO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TRAFICP_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TRAFICP_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT TRAFICP_CAIXA_FK 
 FOREIGN KEY (NR_SEQ_CAIXA) 
 REFERENCES TASY.CAIXA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TRANS_FINANC_CAMPO TO NIVEL_1;

