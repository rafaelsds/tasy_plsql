ALTER TABLE TASY.AGENDA_REGRA_ENVIO_CI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_REGRA_ENVIO_CI CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_REGRA_ENVIO_CI
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_PERFIL             NUMBER(5),
  DS_TITULO             VARCHAR2(255 BYTE)      NOT NULL,
  IE_TRANSFERENCIA      VARCHAR2(1 BYTE)        NOT NULL,
  IE_AGENDAMENTO        VARCHAR2(1 BYTE)        NOT NULL,
  DS_COMUNICADO         VARCHAR2(4000 BYTE)     NOT NULL,
  CD_AGENDA             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGRGECI_AGENDA_FK_I ON TASY.AGENDA_REGRA_ENVIO_CI
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGRGECI_AGENDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGRGECI_PERFIL_FK_I ON TASY.AGENDA_REGRA_ENVIO_CI
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGRGECI_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGRGECI_PK ON TASY.AGENDA_REGRA_ENVIO_CI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGRGECI_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGRGECI_SETATEN_FK_I ON TASY.AGENDA_REGRA_ENVIO_CI
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGRGECI_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.AGENDA_REGRA_ENVIO_CI ADD (
  CONSTRAINT AGRGECI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_REGRA_ENVIO_CI ADD (
  CONSTRAINT AGRGECI_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT AGRGECI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT AGRGECI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.AGENDA_REGRA_ENVIO_CI TO NIVEL_1;

