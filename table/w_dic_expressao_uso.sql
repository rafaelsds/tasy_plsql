ALTER TABLE TASY.W_DIC_EXPRESSAO_USO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIC_EXPRESSAO_USO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIC_EXPRESSAO_USO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_TABELA            VARCHAR2(100 BYTE),
  NM_ATRIBUTO          VARCHAR2(100 BYTE),
  NM_INDICE            VARCHAR2(255 BYTE),
  CD_EXPRESSAO         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WDICEUS_DICEXPR_FK_I ON TASY.W_DIC_EXPRESSAO_USO
(CD_EXPRESSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WDICEUS_PK ON TASY.W_DIC_EXPRESSAO_USO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_DIC_EXPRESSAO_USO ADD (
  CONSTRAINT WDICEUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_DIC_EXPRESSAO_USO ADD (
  CONSTRAINT WDICEUS_DICEXPR_FK 
 FOREIGN KEY (CD_EXPRESSAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.W_DIC_EXPRESSAO_USO TO NIVEL_1;

