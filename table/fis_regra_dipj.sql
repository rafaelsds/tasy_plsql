ALTER TABLE TASY.FIS_REGRA_DIPJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_REGRA_DIPJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_REGRA_DIPJ
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_EMPRESA            NUMBER(4)               NOT NULL,
  DT_ANO                DATE                    NOT NULL,
  IE_CONSOLIDA_EMPRESA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FIREDIPJ_EMPRESA_FK_I ON TASY.FIS_REGRA_DIPJ
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FIREDIPJ_EMPRESA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FIREDIPJ_PK ON TASY.FIS_REGRA_DIPJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FIREDIPJ_PK
  MONITORING USAGE;


ALTER TABLE TASY.FIS_REGRA_DIPJ ADD (
  CONSTRAINT FIREDIPJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_REGRA_DIPJ ADD (
  CONSTRAINT FIREDIPJ_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.FIS_REGRA_DIPJ TO NIVEL_1;

