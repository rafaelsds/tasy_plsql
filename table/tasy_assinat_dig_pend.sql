ALTER TABLE TASY.TASY_ASSINAT_DIG_PEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_ASSINAT_DIG_PEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_ASSINAT_DIG_PEND
(
  NR_SEQ_ASSINATURA    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DT_ASSINATURA        DATE,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_VALIDACAO         DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROJ_ASS      NUMBER(10),
  CD_PERFIL            NUMBER(5),
  DS_UTC               VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO     VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO   VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TASASDIPE_PK ON TASY.TASY_ASSINAT_DIG_PEND
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASASDIPE_TASPRAS_FK_I ON TASY.TASY_ASSINAT_DIG_PEND
(NR_SEQ_PROJ_ASS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TASY_ASSINAT_DIG_PEND ADD (
  CONSTRAINT TASASDIPE_PK
 PRIMARY KEY
 (NR_SEQ_ASSINATURA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_ASSINAT_DIG_PEND ADD (
  CONSTRAINT TASASDIPE_TASPRAS_FK 
 FOREIGN KEY (NR_SEQ_PROJ_ASS) 
 REFERENCES TASY.TASY_PROJETO_ASSINATURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_ASSINAT_DIG_PEND TO NIVEL_1;

