ALTER TABLE TASY.CALCULOS_ADVISORY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CALCULOS_ADVISORY CASCADE CONSTRAINTS;

CREATE TABLE TASY.CALCULOS_ADVISORY
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CALCULO         NUMBER(10),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DS_TITULO              VARCHAR2(55 BYTE),
  DS_ALERTA              VARCHAR2(255 BYTE),
  IE_ADVISORY_SEVERITY   VARCHAR2(3 BYTE),
  IE_EXIBE_ALERTA        VARCHAR2(1 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CALCADV_ATEPACI_FK_I ON TASY.CALCULOS_ADVISORY
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CALCADV_CALCENG_FK_I ON TASY.CALCULOS_ADVISORY
(NR_SEQ_CALCULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CALCADV_PESFISI_FK_I ON TASY.CALCULOS_ADVISORY
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CALCADV_PK ON TASY.CALCULOS_ADVISORY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CALCULOS_ADVISORY ADD (
  CONSTRAINT CALCADV_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CALCULOS_ADVISORY ADD (
  CONSTRAINT CALCADV_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CALCADV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CALCADV_CALCENG_FK 
 FOREIGN KEY (NR_SEQ_CALCULO) 
 REFERENCES TASY.CALCULOS_ENGINE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CALCULOS_ADVISORY TO NIVEL_1;

