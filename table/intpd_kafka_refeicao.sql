ALTER TABLE TASY.INTPD_KAFKA_REFEICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTPD_KAFKA_REFEICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTPD_KAFKA_REFEICAO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  IE_EVENTO           VARCHAR2(15 BYTE),
  DS_JSON             CLOB,
  NR_SEQ_FILA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTKARE_ESTABEL_FK_I ON TASY.INTPD_KAFKA_REFEICAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTKARE_PK ON TASY.INTPD_KAFKA_REFEICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTPD_KAFKA_REFEICAO ADD (
  CONSTRAINT INTKARE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.INTPD_KAFKA_REFEICAO ADD (
  CONSTRAINT INTKARE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.INTPD_KAFKA_REFEICAO TO NIVEL_1;

