ALTER TABLE TASY.CORPO_HUMANO_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CORPO_HUMANO_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.CORPO_HUMANO_ATEND
(
  NR_SEQ_CONFIG          NUMBER(10)             NOT NULL,
  NR_SEQ_CORPO_HUMANO    NUMBER(10)             NOT NULL,
  NR_SEQ_CLASSIFICACAO   NUMBER(10),
  NR_SEQ_ATEND           NUMBER(10)             NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_CLINICA             NUMBER(5),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_IDADE               NUMBER(10),
  QT_PESO                NUMBER(10),
  QT_ALTURA              NUMBER(10),
  QT_ICM                 NUMBER(4,1),
  QT_PORCENTAGEM         NUMBER(10),
  CD_SETOR_ATENDIMENTO   NUMBER(10),
  CD_DIAGNOSTICO         VARCHAR2(500 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  DT_REGISTRO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COHUATE_ATEPACI_FK_I ON TASY.CORPO_HUMANO_ATEND
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUATE_COHUCO_FK_I ON TASY.CORPO_HUMANO_ATEND
(NR_SEQ_CONFIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUATE_COHURE_FK_I ON TASY.CORPO_HUMANO_ATEND
(NR_SEQ_CORPO_HUMANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUATE_PESFISI_FK_I ON TASY.CORPO_HUMANO_ATEND
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COHUATE_PK ON TASY.CORPO_HUMANO_ATEND
(NR_SEQ_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COHUATE_70_FK_I ON TASY.CORPO_HUMANO_ATEND
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CORPO_HUMANO_ATEND_tp  after update ON TASY.CORPO_HUMANO_ATEND FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_ATEND);  ds_c_w:=null; ds_w:=substr(:new.IE_CLINICA,1,500);gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'CORPO_HUMANO_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'CORPO_HUMANO_ATEND',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.CORPO_HUMAN_ATEND_INSERT_HIST
after insert or update ON TASY.CORPO_HUMANO_ATEND for each row
declare

ie_sexo_w pessoa_fisica.ie_sexo%type;
nr_seq_repres_idade_w corpo_humano_repres_idade.nr_seq_repres_idade%type;
nr_seq_repres_corporal_w corpo_humano_rep_corporal.nr_seq_rep_corporal%type;
ie_tipo_w corpo_humano_config.ie_tipo%type;

begin

  if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

    select nr_seq_repres_idade
    into nr_seq_repres_idade_w
    from CORPO_HUMANO_REPRES_IDADE
    where ie_situacao = 'A'
    and :new.qt_idade between qt_idade_ano_min and qt_idade_ano_max;

    select nr_seq_rep_corporal
    into nr_seq_repres_corporal_w
    from CORPO_HUMANO_REP_CORPORAL
    where ie_situacao = 'A'
    and :new.qt_icm between qt_icm_min and qt_icm_max;

    select ie_tipo
    into ie_tipo_w
    from corpo_humano_config
    where nr_seq_config = :new.nr_seq_config;

    if (inserting) then

      select IE_SEXO
      into ie_sexo_w
      from pessoa_fisica
      where cd_pessoa_fisica = :new.cd_pessoa_fisica;


      insert into CORPO_HUMANO_ATEND_REPRES (
        NR_SEQ_REP_CORPORAL,
        NR_SEQ_ATEND_REPRES,
        NR_SEQ_REPRES_IDADE,
        DT_ATUALIZACAO,
        NM_USUARIO,
        DT_ATUALIZACAO_NREC,
        NM_USUARIO_NREC,
        IE_SITUACAO,
        IE_SEXO,
        NR_SEQ_ATEND,
        IE_TIPO
      ) values (
        nr_seq_repres_corporal_w, -- NR_SEQ_REP_CORPORAL,
        CORPO_HUMANO_ATEND_REPRES_SEQ.nextval, -- NR_SEQ_ATEND_REPRES,
        nr_seq_repres_idade_w, -- NR_SEQ_REPRES_IDADE,
        sysdate,  -- DT_ATUALIZACAO,
        :new.nm_usuario, -- NM_USUARIO,
        sysdate, -- DT_ATUALIZACAO_NREC,
        :new.nm_usuario, -- NM_USUARIO_NREC,
        'A', -- IE_SITUACAO,
        ie_sexo_w, -- IE_SEXO,
        :new.NR_SEQ_ATEND,
        ie_tipo_w --IE_TIPO
      );


    elsif (updating) then

      if (:new.dt_liberacao is not null) then
        update CORPO_HUMANO_ATEND_REPRES
        set dt_liberacao = sysdate
        where NR_SEQ_ATEND = :new.NR_SEQ_ATEND;
      end if;

      update CORPO_HUMANO_ATEND_REPRES
      set NR_SEQ_REP_CORPORAL = nr_seq_repres_corporal_w,
      NR_SEQ_REPRES_IDADE = nr_seq_repres_idade_w
      where NR_SEQ_ATEND = :new.NR_SEQ_ATEND;


    end if;

  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.CORPO_HUMANO_ATEND_BEF_DELETE
before delete ON TASY.CORPO_HUMANO_ATEND for each row
declare

begin
  delete from CORPO_HUMANO_ATEND_REPRES
  where NR_SEQ_ATEND = :old.NR_SEQ_ATEND;
end;
/


ALTER TABLE TASY.CORPO_HUMANO_ATEND ADD (
  CONSTRAINT COHUATE_PK
 PRIMARY KEY
 (NR_SEQ_ATEND));

ALTER TABLE TASY.CORPO_HUMANO_ATEND ADD (
  CONSTRAINT COHUATE_COHUCO_FK 
 FOREIGN KEY (NR_SEQ_CONFIG) 
 REFERENCES TASY.CORPO_HUMANO_CONFIG (NR_SEQ_CONFIG),
  CONSTRAINT COHUATE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT COHUATE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COHUATE_COHURE_FK 
 FOREIGN KEY (NR_SEQ_CORPO_HUMANO) 
 REFERENCES TASY.CORPO_HUMANO_REFERENCIA (NR_SEQ_CORPO_HUMANO));

GRANT SELECT ON TASY.CORPO_HUMANO_ATEND TO NIVEL_1;

