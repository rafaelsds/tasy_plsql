ALTER TABLE TASY.PLS_REGRA_DEVOLUCAO_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_DEVOLUCAO_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_DEVOLUCAO_MENS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  IE_TIPO_ESTIPULANTE         VARCHAR2(2 BYTE)  NOT NULL,
  IE_ACAO_DEVOLUCAO           VARCHAR2(3 BYTE)  NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO               NUMBER(10),
  IE_TIPO_RESCISAO            VARCHAR2(2 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  NR_CONTRATO                 NUMBER(10),
  NR_SEQ_TRANS_FIN_BAIXA      NUMBER(10),
  NR_SEQ_TRANS_FIN_CONTAB     NUMBER(10),
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10),
  IE_TITULO_LIQUIDADO         VARCHAR2(1 BYTE),
  IE_TIPO_PAGADOR             VARCHAR2(2 BYTE),
  IE_LANCAMENTO_TITULAR       VARCHAR2(1 BYTE),
  IE_ORIGEM                   VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGDM_PK ON TASY.PLS_REGRA_DEVOLUCAO_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGDM_PLSMOCA_FK_I ON TASY.PLS_REGRA_DEVOLUCAO_MENS
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGDM_PLSTLAD_FK_I ON TASY.PLS_REGRA_DEVOLUCAO_MENS
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGDM_TRAFINA_FK_I ON TASY.PLS_REGRA_DEVOLUCAO_MENS
(NR_SEQ_TRANS_FIN_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGDM_TRAFINA_FK2_I ON TASY.PLS_REGRA_DEVOLUCAO_MENS
(NR_SEQ_TRANS_FIN_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_DEVOLUCAO_MENS ADD (
  CONSTRAINT PLSRGDM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_DEVOLUCAO_MENS ADD (
  CONSTRAINT PLSRGDM_PLSTLAD_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.PLS_TIPO_LANC_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSRGDM_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSRGDM_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_CONTAB) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSRGDM_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_DEVOLUCAO_MENS TO NIVEL_1;

