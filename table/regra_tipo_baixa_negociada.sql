ALTER TABLE TASY.REGRA_TIPO_BAIXA_NEGOCIADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_TIPO_BAIXA_NEGOCIADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_TIPO_BAIXA_NEGOCIADA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  IE_TIPO_TITULO       VARCHAR2(2 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORIGEM_TITULO     VARCHAR2(10 BYTE),
  CD_TRIBUTO           NUMBER(3),
  CD_TIPO_BAIXA_NEG    NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGTBNEG_ESTABEL_FK_I ON TASY.REGRA_TIPO_BAIXA_NEGOCIADA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGTBNEG_PK ON TASY.REGRA_TIPO_BAIXA_NEGOCIADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTBNEG_TIPBACP_FK_I ON TASY.REGRA_TIPO_BAIXA_NEGOCIADA
(CD_TIPO_BAIXA_NEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGTBNEG_TRIBUTO_FK_I ON TASY.REGRA_TIPO_BAIXA_NEGOCIADA
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_TIPO_BAIXA_NEGOCIADA ADD (
  CONSTRAINT REGTBNEG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_TIPO_BAIXA_NEGOCIADA ADD (
  CONSTRAINT REGTBNEG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGTBNEG_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA_NEG) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA),
  CONSTRAINT REGTBNEG_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.REGRA_TIPO_BAIXA_NEGOCIADA TO NIVEL_1;

