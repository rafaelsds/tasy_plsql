ALTER TABLE TASY.FIN_MOTIVO_REPROV_GV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIN_MOTIVO_REPROV_GV CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIN_MOTIVO_REPROV_GV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MOTIVO_REPROV     VARCHAR2(255 BYTE),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  IE_ADIANTAMENTO      VARCHAR2(1 BYTE),
  IE_DESPESA           VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_TRANSPORTE        VARCHAR2(1 BYTE),
  IE_HOSPEDAGEM        VARCHAR2(1 BYTE),
  IE_VIAGEM            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.FINMRGV_PK ON TASY.FIN_MOTIVO_REPROV_GV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FINMRGV_PK
  MONITORING USAGE;


ALTER TABLE TASY.FIN_MOTIVO_REPROV_GV ADD (
  CONSTRAINT FINMRGV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.FIN_MOTIVO_REPROV_GV TO NIVEL_1;

