ALTER TABLE TASY.ATEND_PAC_CHECKLIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PAC_CHECKLIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PAC_CHECKLIST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  NR_SEQ_CONV_CHECKLIST  NUMBER(10)             NOT NULL,
  DT_LIBERACAO           DATE,
  NM_USUARIO_LIB         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPACHE_ATEPACI_FK_I ON TASY.ATEND_PAC_CHECKLIST
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPACHE_CONCHEC_FK_I ON TASY.ATEND_PAC_CHECKLIST
(NR_SEQ_CONV_CHECKLIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATPACHE_PK ON TASY.ATEND_PAC_CHECKLIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.intpd_enviar_checklist_pac
before update ON TASY.ATEND_PAC_CHECKLIST for each row
declare

reg_integracao_w	gerar_int_padrao.reg_integracao;
ie_operacao			varchar2(1);

begin

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then

	reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;
	reg_integracao_w.ie_operacao		:=	'I';
	gerar_int_padrao.gravar_integracao('178', :new.nr_sequencia, :new.nm_usuario, reg_integracao_w);
end if;

end intpd_enviar_checklist_pac;
/


ALTER TABLE TASY.ATEND_PAC_CHECKLIST ADD (
  CONSTRAINT ATPACHE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PAC_CHECKLIST ADD (
  CONSTRAINT ATPACHE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATPACHE_CONCHEC_FK 
 FOREIGN KEY (NR_SEQ_CONV_CHECKLIST) 
 REFERENCES TASY.CONVENIO_CHECKLIST (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_PAC_CHECKLIST TO NIVEL_1;

