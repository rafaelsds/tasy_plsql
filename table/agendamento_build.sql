ALTER TABLE TASY.AGENDAMENTO_BUILD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDAMENTO_BUILD CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDAMENTO_BUILD
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AGENDAMENTO         DATE,
  CD_VERSAO              VARCHAR2(15 BYTE),
  IE_APLICACAO           VARCHAR2(2 BYTE),
  IE_PERIODO             VARCHAR2(2 BYTE),
  NR_SEQ_ORDEM_SERV      NUMBER(10),
  IE_STATUS              VARCHAR2(2 BYTE),
  DT_LIBERACAO           DATE,
  DS_MOTIVO_AGENDAMENTO  VARCHAR2(2000 BYTE),
  DT_INICIO_BUILD        DATE,
  DT_FIM_BUILD           DATE,
  DS_HOTFIX              VARCHAR2(25 BYTE),
  NM_USUARIO_LIBERACAO   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AGENDBU_PK ON TASY.AGENDAMENTO_BUILD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TRG_AGENDAMENTO_BUILD_LOG after
  INSERT OR UPDATE OR DELETE  ON TASY.AGENDAMENTO_BUILD   FOR EACH ROW
DECLARE

  action_w varchar2(2);

  change_request_action CONSTANT varchar2(2 char) := 'CR';
  pending_approval_action CONSTANT varchar2(2 char) := 'PA';
  scheduled_build_action CONSTANT varchar2(2 char) := 'SB';
  canceled_action CONSTANT varchar2(2 char) := 'C';
  executed_build_action CONSTANT varchar2(2 char) := 'EB';
  change_request_refused_action CONSTANT varchar2(2 char) := 'RR';
  change_request_approved_action CONSTANT varchar2(2 char) := 'RA';
  priority_build_action CONSTANT varchar2(2 char) := 'P';
  rescheduled_build_action CONSTANT varchar2(2 char) := 'RB';
  undefined_action CONSTANT varchar2(2 char) := 'U';
  delete_action CONSTANT varchar2(2 char) := 'D';
  lost_priority_action CONSTANT varchar2(2 char) := 'LP';

  troca_pendente_aprovacao CONSTANT varchar2(2 char) := 'TA';
  cancelado CONSTANT varchar2(2 char) := 'C';
  build_agendado CONSTANT varchar2(2 char) := 'BA';
  build_executado CONSTANT varchar2(2 char) := 'BE';
  pendente_liberacao CONSTANT varchar2(2 char) := 'PL';
  periodo_prioritario CONSTANT varchar2(2 char) := 'P';
  periodo_vespertino CONSTANT varchar2(2 char) := 'V';
  periodo_matutino CONSTANT varchar2(2 char) := 'M';


BEGIN
	if (inserting) then
		action_w := pending_approval_action;
	elsif (deleting) then
		action_w := delete_action;
		insert into agendamento_build_log
		(nr_sequencia,
		 nm_usuario,
		 nr_seq_agendamento_build,
		 dt_atualizacao,
		 dt_agendamento,
		 ie_acao,
		 cd_versao,
		 ie_aplicacao,
		 ie_periodo
		 )
		values
		(AGENDAMENTO_BUILD_LOG_SEQ.nextval,
		 :old.nm_usuario,
		 :old.nr_sequencia,
		 sysdate,
		 :old.dt_agendamento,
	     delete_action,
		 :old.cd_versao,
		 :old.ie_aplicacao,
		 :old.ie_periodo
		);
	elsif (updating) then
	    if (:old.ie_status = pendente_liberacao and :new.ie_status = build_agendado) then
			action_w :=  scheduled_build_action;

		elsif (:new.ie_status = build_executado) then
			action_w :=  executed_build_action;

		elsif (:new.ie_status = troca_pendente_aprovacao) then
			action_w := change_request_action;
		elsif (:old.ie_status = troca_pendente_aprovacao) then
			if (:new.ie_status = cancelado) then
				action_w := change_request_refused_action;
			elsif (nvl(:old.dt_agendamento, to_date('1999-01-01', 'yyyy-MM-dd')) = nvl(:new.dt_agendamento, to_date('1999-01-01', 'yyyy-MM-dd'))) then
				action_w := change_request_approved_action;
			end if;
		elsif (:new.ie_status = cancelado) then
			action_w := canceled_action;
		elsif (:new.ie_status = build_agendado and :new.ie_periodo = periodo_prioritario and :old.ie_periodo in (periodo_vespertino, periodo_matutino)) then
			action_w := priority_build_action;
		elsif(:old.ie_periodo <> :new.ie_periodo and :new.ie_periodo <> periodo_prioritario)then
			action_w := rescheduled_build_action;
		elsif(:new.ie_periodo <> :old.ie_periodo and :old.ie_periodo = periodo_prioritario)then
			action_w := lost_priority_action;
		else
			action_w :=  undefined_action;
		end if;
	end if;

	if (action_w <> delete_action) then
		insert into agendamento_build_log
		(nr_sequencia,
		 nm_usuario,
		 nr_seq_agendamento_build,
		 dt_atualizacao,
		 dt_agendamento,
		 ie_acao,
		 cd_versao,
		 ie_aplicacao,
		 ie_periodo
		 )
		values
		(AGENDAMENTO_BUILD_LOG_SEQ.nextval,
		 :new.nm_usuario,
		 :new.nr_sequencia,
		 sysdate,
		 :new.dt_agendamento,
		 action_w,
		 :new.cd_versao,
		 :new.ie_aplicacao,
		 :new.ie_periodo
		);
	end if;
end;
/


ALTER TABLE TASY.AGENDAMENTO_BUILD ADD (
  CONSTRAINT AGENDBU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AGENDAMENTO_BUILD TO NIVEL_1;

