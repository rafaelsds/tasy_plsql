ALTER TABLE TASY.EMISSAO_PRESC_SCHEDULER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMISSAO_PRESC_SCHEDULER CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMISSAO_PRESC_SCHEDULER
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_LOCAL_EMISSAO          VARCHAR2(1 BYTE)    NOT NULL,
  CD_LOCAL_ESTOQUE          NUMBER(4),
  IE_LOCAL_DISPENSACAO      VARCHAR2(1 BYTE)    NOT NULL,
  QT_INTERVALO              NUMBER(5),
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  IE_DATA_ATUAL             VARCHAR2(1 BYTE),
  IE_EMITIR_URGENTES        VARCHAR2(1 BYTE),
  IE_EMITIR_PRESC_HOR       VARCHAR2(1 BYTE),
  QT_HORAS_ADIC_INICIO      NUMBER(5),
  QT_HORAS_ADIC_FINAL       NUMBER(5),
  QT_HORAS_ANT_TURNO        NUMBER(5),
  QT_HORAS_LIB_URGENTE      NUMBER(5),
  IE_FILTRAR_ESTAB          VARCHAR2(1 BYTE),
  QT_HORAS_LIB_AGORA        NUMBER(5),
  IE_NAO_EMITIR_APOS_ALTA   VARCHAR2(1 BYTE),
  IE_EMITE_NAO_PADRONIZADO  VARCHAR2(1 BYTE),
  IE_NAO_EMITE_SEM_ATEND    VARCHAR2(1 BYTE),
  IE_CONSITI_VIGENCIA       VARCHAR2(1 BYTE),
  IE_CONSISTE_LIB_FARMACIA  VARCHAR2(1 BYTE),
  NR_SEQ_SCHEDULER          NUMBER(10)          NOT NULL,
  NR_SEQ_REGRA              NUMBER(10),
  DS_IMPRESSORA             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.EMPRESCH_PK ON TASY.EMISSAO_PRESC_SCHEDULER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPRESCH_USUSCHE_FK_I ON TASY.EMISSAO_PRESC_SCHEDULER
(NR_SEQ_SCHEDULER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EMISSAO_PRESC_SCHEDULER ADD (
  CONSTRAINT EMPRESCH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMISSAO_PRESC_SCHEDULER ADD (
  CONSTRAINT EMPRESCH_USUSCHE_FK 
 FOREIGN KEY (NR_SEQ_SCHEDULER) 
 REFERENCES TASY.USUARIO_SCHEDULER (NR_SEQUENCIA));

GRANT SELECT ON TASY.EMISSAO_PRESC_SCHEDULER TO NIVEL_1;

