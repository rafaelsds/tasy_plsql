ALTER TABLE TASY.CONTA_PACIENTE_DESC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_PACIENTE_DESC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_PACIENTE_DESC_ITEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_DESCONTO       NUMBER(10)              NOT NULL,
  CD_ESTRUTURA          NUMBER(5),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  VL_CONTA              NUMBER(15,2)            NOT NULL,
  IE_CALCULA            VARCHAR2(1 BYTE)        NOT NULL,
  PR_DESCONTO           NUMBER(5,2)             NOT NULL,
  VL_DESCONTO           NUMBER(15,2)            NOT NULL,
  VL_LIQUIDO            NUMBER(15,2)            NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_MEDICO             VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_DESC_MATERIAL      VARCHAR2(1 BYTE)        NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  CD_MATERIAL           NUMBER(6),
  NR_SEQ_PROC_PACOTE    NUMBER(10),
  DT_ITEM               DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COPADEI_COPADES_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(NR_SEQ_DESCONTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPADEI_COPAEST_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(CD_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPADEI_COPAEST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPADEI_MATERIA_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPADEI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPADEI_MEDICO_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COPADEI_PK ON TASY.CONTA_PACIENTE_DESC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPADEI_PROCEDI_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPADEI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPADEI_PROPACI_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(NR_SEQ_PROC_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPADEI_PROPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPADEI_SETATEN_FK_I ON TASY.CONTA_PACIENTE_DESC_ITEM
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONTA_PACIENTE_DESC_ITEM ADD (
  CONSTRAINT COPADEI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_PACIENTE_DESC_ITEM ADD (
  CONSTRAINT COPADEI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT COPADEI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT COPADEI_COPADES_FK 
 FOREIGN KEY (NR_SEQ_DESCONTO) 
 REFERENCES TASY.CONTA_PACIENTE_DESCONTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COPADEI_COPAEST_FK 
 FOREIGN KEY (CD_ESTRUTURA) 
 REFERENCES TASY.CONTA_PACIENTE_ESTRUTURA (CD_ESTRUTURA),
  CONSTRAINT COPADEI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT COPADEI_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT COPADEI_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROC_PACOTE) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONTA_PACIENTE_DESC_ITEM TO NIVEL_1;

