ALTER TABLE TASY.ESCALA_DIARIA_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DIARIA_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DIARIA_ADIC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_ESCALA_DIARIA   NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF_ESCALA  NUMBER(10),
  QT_MIN_EXECUTADO       NUMBER(15),
  NR_ORDEM_ESCALA        NUMBER(10),
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_AFAST    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCDIAD_ESCDIAR_FK_I ON TASY.ESCALA_DIARIA_ADIC
(NR_SEQ_ESCALA_DIARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCDIAD_ESDIADC_FK_I ON TASY.ESCALA_DIARIA_ADIC
(NR_SEQ_CLASSIF_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCDIAD_ESDIADC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCDIAD_MOAFESC_FK_I ON TASY.ESCALA_DIARIA_ADIC
(NR_SEQ_MOTIVO_AFAST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCDIAD_PESFISI_FK_I ON TASY.ESCALA_DIARIA_ADIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCDIAD_PK ON TASY.ESCALA_DIARIA_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESCALA_DIARIA_ADIC ADD (
  CONSTRAINT ESCDIAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_DIARIA_ADIC ADD (
  CONSTRAINT ESCDIAD_MOAFESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_AFAST) 
 REFERENCES TASY.MOTIVO_AFAST_ESCALA (NR_SEQUENCIA),
  CONSTRAINT ESCDIAD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCDIAD_ESCDIAR_FK 
 FOREIGN KEY (NR_SEQ_ESCALA_DIARIA) 
 REFERENCES TASY.ESCALA_DIARIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCDIAD_ESDIADC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ESCALA) 
 REFERENCES TASY.ESCALA_DIARIA_ADIC_CLASSIF (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_DIARIA_ADIC TO NIVEL_1;

