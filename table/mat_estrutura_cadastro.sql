ALTER TABLE TASY.MAT_ESTRUTURA_CADASTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_ESTRUTURA_CADASTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_ESTRUTURA_CADASTRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ESTRUTURA     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL          NUMBER(15)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATESCA_I1 ON TASY.MAT_ESTRUTURA_CADASTRO
(CD_MATERIAL, NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESCA_MATERIA_FK_I ON TASY.MAT_ESTRUTURA_CADASTRO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATESCA_MATESTR_FK_I ON TASY.MAT_ESTRUTURA_CADASTRO
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATESCA_PK ON TASY.MAT_ESTRUTURA_CADASTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MAT_ESTRUTURA_CADASTRO_tp  after update ON TASY.MAT_ESTRUTURA_CADASTRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_MATERIAL,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'MAT_ESTRUTURA_CADASTRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_ESTRUTURA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUTURA,1,4000),substr(:new.NR_SEQ_ESTRUTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUTURA',ie_log_w,ds_w,'MAT_ESTRUTURA_CADASTRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.mat_estrutura_cadastro_insert
after insert ON TASY.MAT_ESTRUTURA_CADASTRO for each row
declare

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_estrut_int_w		parametros_farmacia.nr_seq_estrut_int%type;
ds_param_integ_hl7_w	varchar2(4000) := '';
ie_supply_point_w		varchar2(1);
ie_athena_w				varchar2(1);
qt_estrut_regra_w		number(10);
ie_enviar_mat_estoque_w	VARCHAR2(1) := 'N';

begin

begin
select	b.cd_estabelecimento
into	cd_estabelecimento_w
from	mat_estrutura a,
	mat_tipo_estrutura b
where	a.nr_sequencia = :new.nr_seq_estrutura
and	b.nr_sequencia = a.nr_seq_tipo;
exception
when others then
	cd_estabelecimento_w := 1;
end;

select	nvl(max(nr_seq_estrut_int),0),
		coalesce(max(ie_enviar_mat_estoque), 'N')
into	nr_seq_estrut_int_w,
		ie_enviar_mat_estoque_w
from	parametros_farmacia
where	cd_estabelecimento = cd_estabelecimento_w;

begin
select	decode(count(*),0,'N','S')
into	ie_supply_point_w
from	far_setores_integracao
where	nr_seq_empresa_int = 82
and	rownum = 1;
exception
when others then
	ie_supply_point_w := 'N';
end;

begin
select	decode(count(*),0,'N','S')
into	ie_athena_w
from	far_setores_integracao
where	nr_seq_empresa_int = 221
and	rownum = 1;
exception
when others then
	ie_athena_w := 'N';
end;

select	count(*)
into	qt_estrut_regra_w
from	estrutura_interna_disp a
where	(cd_estabelecimento_w is null or a.cd_estabelecimento = cd_estabelecimento_w)
and 	a.nr_seq_estrutura = :new.nr_seq_estrutura;

if	((ie_supply_point_w = 'S') or (ie_athena_w = 'S')) and
	((:new.nr_seq_estrutura = nr_seq_estrut_int_w) or
	(qt_estrut_regra_w > 0)) then

	if(ie_supply_point_w = 'S') then
		ds_param_integ_hl7_w := 'cd_material=' || :new.cd_material || obter_separador_bv
							 || 'ie_enviar_mat_estoque=' || ie_enviar_mat_estoque_w || obter_separador_bv;
	else
		ds_param_integ_hl7_w := 'cd_material=' || :new.cd_material || obter_separador_bv;
	end if;
	--swisslog_gerar_integracao(438,ds_param_integ_hl7_w);
	gravar_agend_integracao(438,ds_param_integ_hl7_w);
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.mat_estrutura_cad_afinsert
after insert ON TASY.MAT_ESTRUTURA_CADASTRO for each row
declare

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_estrut_int_w		parametros_farmacia.nr_seq_estrut_int%type;
ds_param_integ_hl7_w		varchar2(4000) := '';

begin
begin
select	b.cd_estabelecimento
into	cd_estabelecimento_w
from	mat_estrutura a,
	mat_tipo_estrutura b
where	a.nr_sequencia = :new.nr_seq_estrutura
and	b.nr_sequencia = a.nr_seq_tipo;
exception
when others then
	cd_estabelecimento_w := 1;
end;

select	nvl(max(nr_seq_estrut_int),0)
into	nr_seq_estrut_int_w
from	parametros_farmacia
where	cd_estabelecimento = cd_estabelecimento_w;

if	(:new.nr_seq_estrutura = nr_seq_estrut_int_w) then

	ds_param_integ_hl7_w := 'cd_material=' || :new.cd_material || obter_separador_bv;
	swisslog_gerar_integracao(438,ds_param_integ_hl7_w);
	gravar_agend_integracao(438,ds_param_integ_hl7_w);
end if;
end;
/


ALTER TABLE TASY.MAT_ESTRUTURA_CADASTRO ADD (
  CONSTRAINT MATESCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAT_ESTRUTURA_CADASTRO ADD (
  CONSTRAINT MATESCA_MATESTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.MAT_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT MATESCA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.MAT_ESTRUTURA_CADASTRO TO NIVEL_1;

