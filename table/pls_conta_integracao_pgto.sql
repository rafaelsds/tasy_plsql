ALTER TABLE TASY.PLS_CONTA_INTEGRACAO_PGTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_INTEGRACAO_PGTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_INTEGRACAO_PGTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_PERIODO_PGTO   NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  NR_DIA_LIMITE         NUMBER(3),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  DT_REFERENCIA         DATE,
  IE_TIPO_GUIA          VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOIP_ESTABEL_FK_I ON TASY.PLS_CONTA_INTEGRACAO_PGTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOIP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCOIP_PK ON TASY.PLS_CONTA_INTEGRACAO_PGTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOIP_PLSCLPR_FK_I ON TASY.PLS_CONTA_INTEGRACAO_PGTO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOIP_PLSCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOIP_PLSPEPA_FK_I ON TASY.PLS_CONTA_INTEGRACAO_PGTO
(NR_SEQ_PERIODO_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOIP_PLSPEPA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_CONTA_INTEGRACAO_PGTO_tp  after update ON TASY.PLS_CONTA_INTEGRACAO_PGTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,2000),substr(:new.NR_SEQ_CLASSIFICACAO,1,2000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_CONTA_INTEGRACAO_PGTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CONTA_INTEGRACAO_PGTO ADD (
  CONSTRAINT PLSCOIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_INTEGRACAO_PGTO ADD (
  CONSTRAINT PLSCOIP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCOIP_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOIP_PLSPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERIODO_PGTO) 
 REFERENCES TASY.PLS_PERIODO_PAGAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_INTEGRACAO_PGTO TO NIVEL_1;

