ALTER TABLE TASY.HEM_ESTRUTURA_CONGENITA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_ESTRUTURA_CONGENITA CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_ESTRUTURA_CONGENITA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PROC             NUMBER(10)            NOT NULL,
  IE_ESTRUTURA_ANATOMICA  VARCHAR2(2 BYTE)      NOT NULL,
  NR_SEQ_TIPO_LESAO       NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMESCO_HEMPROC_FK_I ON TASY.HEM_ESTRUTURA_CONGENITA
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMESCO_HEMTLES_FK_I ON TASY.HEM_ESTRUTURA_CONGENITA
(NR_SEQ_TIPO_LESAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HEMESCO_PK ON TASY.HEM_ESTRUTURA_CONGENITA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HEM_ESTRUTURA_CONGENITA ADD (
  CONSTRAINT HEMESCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_ESTRUTURA_CONGENITA ADD (
  CONSTRAINT HEMESCO_HEMPROC_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.HEM_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HEMESCO_HEMTLES_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LESAO) 
 REFERENCES TASY.HEM_TIPO_LESAO_ESTRUTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_ESTRUTURA_CONGENITA TO NIVEL_1;

