ALTER TABLE TASY.AGHOS_REGISTRA_INF_TRIGGER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGHOS_REGISTRA_INF_TRIGGER CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGHOS_REGISTRA_INF_TRIGGER
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  IE_TIPO_INTEGRA     VARCHAR2(1 BYTE)          NOT NULL,
  IE_PROCESSADO       VARCHAR2(1 BYTE),
  NR_INTERNACAO       NUMBER(10),
  NR_SEQ_ATEPACU      NUMBER(10),
  NR_ATENDIMENTO      NUMBER(10)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGREGITRIG_I1 ON TASY.AGHOS_REGISTRA_INF_TRIGGER
(IE_PROCESSADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGREGITRIG_PK ON TASY.AGHOS_REGISTRA_INF_TRIGGER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGHOS_REGISTRA_INF_TRIGGER ADD (
  CONSTRAINT AGREGITRIG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AGHOS_REGISTRA_INF_TRIGGER TO NIVEL_1;

