ALTER TABLE TASY.CPOE_PROC_GLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_PROC_GLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_PROC_GLIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROCEDIMENTO  NUMBER(10),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  QT_GLIC_INIC         NUMBER(10,1)             NOT NULL,
  QT_GLIC_FIM          NUMBER(10,1)             NOT NULL,
  QT_UI_INSULINA       NUMBER(10,2),
  QT_GLICOSE           NUMBER(5,1),
  DS_SUGESTAO          VARCHAR2(2000 BYTE),
  QT_MINUTOS_MEDICAO   NUMBER(10),
  QT_UI_INSULINA_INT   NUMBER(10,2),
  NR_SEQ_PROC_EDIT     NUMBER(10),
  NR_ATENDIMENTO       NUMBER(10),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  IE_MODIFICADO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOPRGL_ATEPACI_FK_I ON TASY.CPOE_PROC_GLIC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPRGL_CPOEPRO_FK_I ON TASY.CPOE_PROC_GLIC
(NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPRGL_PEPPRGL_FK_I ON TASY.CPOE_PROC_GLIC
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPRGL_PESFISI_FK_I ON TASY.CPOE_PROC_GLIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOPRGL_PK ON TASY.CPOE_PROC_GLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cpoe_glic_insert_update_after
after insert or update ON TASY.CPOE_PROC_GLIC for each row
declare

ds_log_cpoe_w			varchar(2000);
dt_min_date_w			date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

begin
if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
end if;

if	(nvl(:new.nr_seq_procedimento,0) <> nvl(:old.nr_seq_procedimento,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_procedimento(' || nvl(:old.nr_seq_procedimento,0) || '/' || nvl(:new.nr_seq_procedimento,0)||'); ',1,2000);
end if;

if	(nvl(:new.nr_seq_proc_edit,0) <> nvl(:old.nr_seq_proc_edit,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_proc_edit(' || nvl(:old.nr_seq_proc_edit,0) || '/' || nvl(:new.nr_seq_proc_edit,0)||'); ',1,2000);
end if;

if	(nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_atendimento(' || nvl(:old.nr_atendimento,0) || '/' || nvl(:new.nr_atendimento,0)||'); ',1,2000);
end if;

if	(nvl(:new.nr_seq_protocolo,0) <> nvl(:old.nr_seq_protocolo,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_protocolo(' || nvl(:old.nr_seq_protocolo,0) || '/' || nvl(:new.nr_seq_protocolo,0)||'); ',1,2000);
end if;

if	(nvl(:new.qt_glic_inic,0) <> nvl(:old.qt_glic_inic,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_glic_inic(' || nvl(:old.qt_glic_inic,0) || '/' || nvl(:new.qt_glic_inic,0)||'); ',1,2000);
end if;

if	(nvl(:new.qt_glic_fim,0) <> nvl(:old.qt_glic_fim,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_glic_fim(' || nvl(:old.qt_glic_fim,0) || '/' || nvl(:new.qt_glic_fim,0)||'); ',1,2000);
end if;

if	(nvl(:new.qt_ui_insulina,0) <> nvl(:old.qt_ui_insulina,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_ui_insulina(' || nvl(:old.qt_ui_insulina,0) || '/' || nvl(:new.qt_ui_insulina,0)||'); ',1,2000);
end if;

if	(nvl(:new.qt_glicose,0) <> nvl(:old.qt_glicose,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_glicose(' || nvl(:old.qt_glicose,0) || '/' || nvl(:new.qt_glicose,0)||'); ',1,2000);
end if;

if	(nvl(:new.qt_minutos_medicao,0) <> nvl(:old.qt_minutos_medicao,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_minutos_medicao(' || nvl(:old.qt_minutos_medicao,0) || '/' || nvl(:new.qt_minutos_medicao,0)||'); ',1,2000);
end if;

if	(nvl(:new.qt_ui_insulina_int,0) <> nvl(:old.qt_ui_insulina_int,0)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' qt_ui_insulina_int(' || nvl(:old.qt_ui_insulina_int,0) || '/' || nvl(:new.qt_ui_insulina_int,0)||'); ',1,2000);
end if;

if	(nvl(:new.cd_pessoa_fisica,'XPTO') <> nvl(:old.cd_pessoa_fisica,'XPTO')) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_pessoa_fisica(' || nvl(:old.cd_pessoa_fisica,'<NULL>') || '/' || nvl(:new.cd_pessoa_fisica,'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.ie_modificado,'XPTO') <> nvl(:old.ie_modificado,'XPTO')) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_modificado(' || nvl(:old.ie_modificado,'<NULL>') || '/' || nvl(:new.ie_modificado,'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_atualizacao, dt_min_date_w) <> nvl(:old.dt_atualizacao, dt_min_date_w)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_atualizacao(' || nvl(to_char(:old.dt_atualizacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_atualizacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_atualizacao_nrec, dt_min_date_w) <> nvl(:old.dt_atualizacao_nrec, dt_min_date_w)) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_atualizacao_nrec(' || nvl(to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.nm_usuario, 'XPTO') <> nvl(:old.nm_usuario, 'XPTO')) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nm_usuario(' || nvl(:old.nm_usuario,'<NULL>') || '/' || nvl(:new.nm_usuario,'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.nm_usuario_nrec, 'XPTO') <> nvl(:old.nm_usuario_nrec, 'XPTO')) then
	ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nm_usuario_nrec(' || nvl(:old.nm_usuario_nrec,'<NULL>') || '/' || nvl(:new.nm_usuario_nrec,'<NULL>')||'); ',1,2000);
end if;

if	(ds_log_cpoe_w is not null) then

	if (nvl(:old.nr_sequencia, 0) > 0) then
		ds_log_cpoe_w := substr('Altera��es(old/new)= ' || ds_log_cpoe_w,1,2000);
	else
		ds_log_cpoe_w := substr('Cria��o(old/new)= ' || ds_log_cpoe_w,1,2000);
	end if;
	ds_log_cpoe_w := substr(ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);

	gravar_log_cpoe(ds_log_cpoe_w, :new.nr_atendimento, 'P', nvl(:new.nr_seq_procedimento, :new.nr_seq_proc_edit));

end if;
exception
when others then
	null;
end;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_glic_delete
before delete ON TASY.CPOE_PROC_GLIC for each row
declare
	ds_log_cpoe_w	varchar(2000);

	pragma autonomous_transaction;
begin
	ds_log_cpoe_w := substr('CPOE_GLIC_DELETE nr_sequencia: ' || :old.nr_sequencia || ' nr_seq_procedimento: ' || :old.nr_seq_procedimento ||
		' nr_seq_proc_edit: ' || :old.nr_seq_proc_edit || ' nr_atendimento: ' || :old.nr_atendimento || ' nr_seq_protocolo: ' || :old.nr_seq_protocolo  ,1,2000);

	gravar_log_cpoe(ds_log_cpoe_w, :old.nr_atendimento, null, null);

	commit;
exception
when others then
	null;
end;
/


ALTER TABLE TASY.CPOE_PROC_GLIC ADD (
  CONSTRAINT CPOPRGL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_PROC_GLIC ADD (
  CONSTRAINT CPOPRGL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CPOPRGL_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CPOPRGL_PEPPRGL_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PEP_PROTOCOLO_GLICEMIA (NR_SEQUENCIA),
  CONSTRAINT CPOPRGL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CPOE_PROC_GLIC TO NIVEL_1;

