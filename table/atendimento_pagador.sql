ALTER TABLE TASY.ATENDIMENTO_PAGADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_PAGADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_PAGADOR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  CD_CGC                  VARCHAR2(14 BYTE),
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  NR_SEQ_GRAU_PARENTESCO  NUMBER(10),
  DS_ACORDO               VARCHAR2(4000 BYTE),
  DS_CONDICAO             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          28M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPAGA_ATEPACI_FK_I ON TASY.ATENDIMENTO_PAGADOR
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPAGA_GRAUPA_FK_I ON TASY.ATENDIMENTO_PAGADOR
(NR_SEQ_GRAU_PARENTESCO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPAGA_GRAUPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPAGA_PESFISI_FK_I ON TASY.ATENDIMENTO_PAGADOR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          13M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPAGA_PESJURI_FK_I ON TASY.ATENDIMENTO_PAGADOR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPAGA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATEPAGA_PK ON TASY.ATENDIMENTO_PAGADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_PAGADOR_DELETE
BEFORE DELETE ON TASY.ATENDIMENTO_PAGADOR FOR EACH ROW
DECLARE

begin

gravar_log_exclusao('ATENDIMENTO_PAGADOR',:old.nm_usuario, 'NR_ATENDIMENTO= ' || :old.nr_atendimento || ', PESSOA_FISICA= ' || :old.CD_PESSOA_FISICA || ',CPF= ' || :old.CD_CGC,'N');

end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_PAGADOR_tp  after update ON TASY.ATENDIMENTO_PAGADOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'ATENDIMENTO_PAGADOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_PAGADOR_ATUAL
BEFORE UPDATE OR INSERT ON TASY.ATENDIMENTO_PAGADOR FOR EACH ROW
DECLARE

cd_pessoa_fisica_w			varchar2(10);
qt_idade_perm_w				varchar2(10);
cd_estabelecimento_w		number(4);
nr_idade_w					number(10);
qt_reg_w					number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final_Exec;
end if;
if	(:new.cd_pessoa_fisica is not null)	then

	select	obter_idade_pf(:new.cd_pessoa_fisica,sysdate,'A'),
		obter_estab_atend(:new.nr_atendimento)
	into	nr_idade_w,
		cd_estabelecimento_w
	from	dual;

	Obter_Param_Usuario(916,149,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,qt_idade_perm_w);

	if 	(nr_idade_w < to_number(qt_idade_perm_w)) then
		Wheb_mensagem_pck.exibir_mensagem_abort(278154, 'QT_IDADE_PERM_P=' || qt_idade_perm_w);

	end if;
end if;

<<Final_Exec>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.ATENDIMENTO_PAGADOR ADD (
  CONSTRAINT ATEPAGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          12M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_PAGADOR ADD (
  CONSTRAINT ATEPAGA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEPAGA_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT ATEPAGA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPAGA_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.ATENDIMENTO_PAGADOR TO NIVEL_1;

