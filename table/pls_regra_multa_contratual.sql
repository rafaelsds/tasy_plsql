ALTER TABLE TASY.PLS_REGRA_MULTA_CONTRATUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_MULTA_CONTRATUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_MULTA_CONTRATUAL
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_CONTRATO          NUMBER(10)           NOT NULL,
  IE_APLICACAO_REGRA       VARCHAR2(2 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE,
  VL_MULTA                 NUMBER(15,2),
  TX_MULTA                 NUMBER(7,4),
  IE_REFERENCIA_LANC       VARCHAR2(2 BYTE),
  IE_PROPORCIONAL          VARCHAR2(1 BYTE),
  IE_TIPO_MULTA            VARCHAR2(1 BYTE),
  IE_DATA_BASE             VARCHAR2(2 BYTE),
  IE_DESCONSIDERAR_REMIDO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRMTC_PK ON TASY.PLS_REGRA_MULTA_CONTRATUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRMTC_PLSCONT_FK_I ON TASY.PLS_REGRA_MULTA_CONTRATUAL
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_MULTA_CONTRATUAL_tp  after update ON TASY.PLS_REGRA_MULTA_CONTRATUAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CONTRATO,1,4000),substr(:new.NR_SEQ_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APLICACAO_REGRA,1,4000),substr(:new.IE_APLICACAO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_APLICACAO_REGRA',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MULTA,1,4000),substr(:new.VL_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_MULTA',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DESCONSIDERAR_REMIDO,1,4000),substr(:new.IE_DESCONSIDERAR_REMIDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESCONSIDERAR_REMIDO',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROPORCIONAL,1,4000),substr(:new.IE_PROPORCIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROPORCIONAL',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REFERENCIA_LANC,1,4000),substr(:new.IE_REFERENCIA_LANC,1,4000),:new.nm_usuario,nr_seq_w,'IE_REFERENCIA_LANC',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DATA_BASE,1,4000),substr(:new.IE_DATA_BASE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DATA_BASE',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_MULTA,1,4000),substr(:new.IE_TIPO_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MULTA',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MULTA,1,4000),substr(:new.TX_MULTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_MULTA',ie_log_w,ds_w,'PLS_REGRA_MULTA_CONTRATUAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_MULTA_CONTRATUAL ADD (
  CONSTRAINT PLSRMTC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_MULTA_CONTRATUAL ADD (
  CONSTRAINT PLSRMTC_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_MULTA_CONTRATUAL TO NIVEL_1;

