ALTER TABLE TASY.REHU_CICLO_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REHU_CICLO_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.REHU_CICLO_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CICLO         NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL,
  IE_PEDIDO_PAC        VARCHAR2(1 BYTE)         NOT NULL,
  IE_PEDIDO_CON        VARCHAR2(1 BYTE)         NOT NULL,
  IE_DEVOLVIDO_PAC     VARCHAR2(1 BYTE)         NOT NULL,
  IE_DEVOLVIDO_CON     VARCHAR2(1 BYTE)         NOT NULL,
  IE_RESULTADO_PAC     VARCHAR2(1 BYTE)         NOT NULL,
  IE_RESULTADO_CON     VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RHCIEXM_PK ON TASY.REHU_CICLO_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RHCIEXM_PK
  MONITORING USAGE;


CREATE INDEX TASY.RHCIEXM_RHCICLO_FK_I ON TASY.REHU_CICLO_EXAME
(NR_SEQ_CICLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RHCIEXM_RHCICLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RHCIEXM_RHLIEXM_FK_I ON TASY.REHU_CICLO_EXAME
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RHCIEXM_RHLIEXM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REHU_CICLO_EXAME ADD (
  CONSTRAINT RHCIEXM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REHU_CICLO_EXAME ADD (
  CONSTRAINT RHCIEXM_RHCICLO_FK 
 FOREIGN KEY (NR_SEQ_CICLO) 
 REFERENCES TASY.REHU_CICLO (NR_SEQUENCIA),
  CONSTRAINT RHCIEXM_RHLIEXM_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.REHU_LISTA_EXAME (NR_SEQUENCIA));

GRANT SELECT ON TASY.REHU_CICLO_EXAME TO NIVEL_1;

