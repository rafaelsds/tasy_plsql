ALTER TABLE TASY.ORDEM_COMPRA_NOTA_FISCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_NOTA_FISCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_NOTA_FISCAL
(
  NR_ORDEM_COMPRA     NUMBER(10)                NOT NULL,
  NR_ITEM_OCI         NUMBER(5)                 NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  CD_CGC_EMITENTE     VARCHAR2(14 BYTE)         NOT NULL,
  CD_SERIE_NF         VARCHAR2(255 BYTE)        NOT NULL,
  NR_SEQUENCIA_NF     NUMBER(10)                NOT NULL,
  NR_ITEM_NF          NUMBER(5)                 NOT NULL,
  CD_MATERIAL         NUMBER(6)                 NOT NULL,
  DT_ENTREGA          DATE                      NOT NULL,
  QT_ENTREGA          NUMBER(15,4)              NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_SEQUENCIA        NUMBER(10),
  IE_STATUS_EXPORTAR  VARCHAR2(1 BYTE),
  NR_NOTA_FISCAL      VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA    VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCONFI_MATERIA_FK_I ON TASY.ORDEM_COMPRA_NOTA_FISCAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCONFI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCONFI_NOTFIIT_FK_I ON TASY.ORDEM_COMPRA_NOTA_FISCAL
(NR_SEQUENCIA, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCONFI_ORCOITE_FK_I ON TASY.ORDEM_COMPRA_NOTA_FISCAL
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCONFI_PK ON TASY.ORDEM_COMPRA_NOTA_FISCAL
(NR_ORDEM_COMPRA, NR_ITEM_OCI, CD_ESTABELECIMENTO, CD_CGC_EMITENTE, CD_SERIE_NF, 
NR_NOTA_FISCAL, NR_SEQUENCIA_NF, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ORDEM_COMPRA_NOTA_FISCAL ADD (
  CONSTRAINT ORCONFI_PK
 PRIMARY KEY
 (NR_ORDEM_COMPRA, NR_ITEM_OCI, CD_ESTABELECIMENTO, CD_CGC_EMITENTE, CD_SERIE_NF, NR_NOTA_FISCAL, NR_SEQUENCIA_NF, NR_ITEM_NF)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_NOTA_FISCAL ADD (
  CONSTRAINT ORCONFI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ORCONFI_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQUENCIA, NR_ITEM_NF) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF),
  CONSTRAINT ORCONFI_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI));

GRANT SELECT ON TASY.ORDEM_COMPRA_NOTA_FISCAL TO NIVEL_1;

