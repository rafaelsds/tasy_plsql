ALTER TABLE TASY.INCONSISTENCIA_TIT_ESCRIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INCONSISTENCIA_TIT_ESCRIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.INCONSISTENCIA_TIT_ESCRIT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TITULO_ESCRIT   NUMBER(10),
  NR_SEQ_INCONSISTENCIA  NUMBER(10),
  DT_LIBERACAO           DATE,
  DS_MOTIVO_LIBERACAO    VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INCTITESC_INCESCR_FK_I ON TASY.INCONSISTENCIA_TIT_ESCRIT
(NR_SEQ_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INCTITESC_PK ON TASY.INCONSISTENCIA_TIT_ESCRIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INCTITESC_PK
  MONITORING USAGE;


CREATE INDEX TASY.INCTITESC_TIRECOB_FK_I ON TASY.INCONSISTENCIA_TIT_ESCRIT
(NR_SEQ_TITULO_ESCRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INCONSISTENCIA_TIT_ESCRIT ADD (
  CONSTRAINT INCTITESC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INCONSISTENCIA_TIT_ESCRIT ADD (
  CONSTRAINT INCTITESC_INCESCR_FK 
 FOREIGN KEY (NR_SEQ_INCONSISTENCIA) 
 REFERENCES TASY.INCONSISTENCIA_ESCRIT (NR_SEQUENCIA),
  CONSTRAINT INCTITESC_TIRECOB_FK 
 FOREIGN KEY (NR_SEQ_TITULO_ESCRIT) 
 REFERENCES TASY.TITULO_RECEBER_COBR (NR_SEQUENCIA));

GRANT SELECT ON TASY.INCONSISTENCIA_TIT_ESCRIT TO NIVEL_1;

