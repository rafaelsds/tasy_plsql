ALTER TABLE TASY.ADIANTAMENTO_PAGO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADIANTAMENTO_PAGO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADIANTAMENTO_PAGO
(
  NR_ADIANTAMENTO       NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ADIANTAMENTO       DATE                    NOT NULL,
  VL_ADIANTAMENTO       NUMBER(15,2)            NOT NULL,
  VL_SALDO              NUMBER(15,2)            NOT NULL,
  CD_MOEDA              NUMBER(5),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_CGC                VARCHAR2(14 BYTE),
  CD_BANCO              NUMBER(5),
  NR_LOTE_CONTABIL      NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DT_BAIXA              DATE,
  NR_SEQ_TRANS_FIN      NUMBER(10),
  NR_SEQ_CONTA_BANCO    NUMBER(10),
  NR_TITULO_ORIGINAL    NUMBER(10),
  NR_SEQ_DISTRIBUICAO   NUMBER(10),
  NR_SEQ_NF             NUMBER(10),
  NR_SEQ_TIPO           NUMBER(10),
  NR_ADIANT_ORIGINAL    NUMBER(10),
  DT_PREV_DEVOLUCAO     DATE,
  NR_SEQ_ORDEM_SERVICO  NUMBER(10),
  NM_USUARIO_LIB        VARCHAR2(15 BYTE),
  DT_LIBERACAO          DATE,
  NR_SEQ_PROPOSTA       NUMBER(10),
  NR_SEQ_CANAL_VENDA    NUMBER(10),
  VL_ADTO_ESTRANG       NUMBER(15,2),
  VL_SALDO_ESTRANG      NUMBER(15,2),
  VL_COTACAO            NUMBER(21,10),
  VL_COMPLEMENTO        NUMBER(15,2),
  NR_SEQ_GV             NUMBER(10),
  NR_SEQ_CAIXA_REC      NUMBER(10),
  NR_CODIGO_CONTROLE    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADIPAGO_ADIPAGO_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_ADIANT_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_ADIPAGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_BANCO_FK_I ON TASY.ADIANTAMENTO_PAGO
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_BANESTA_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_CONTA_BANCO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_CAIREC_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_DLDISTR_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_DISTRIBUICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_DLDISTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_ESTABEL_FK_I ON TASY.ADIANTAMENTO_PAGO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_I1 ON TASY.ADIANTAMENTO_PAGO
(CD_PESSOA_FISICA, DT_ADIANTAMENTO, VL_SALDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_I2 ON TASY.ADIANTAMENTO_PAGO
(CD_CGC, DT_ADIANTAMENTO, VL_SALDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_LOTCONT_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_MANORSE_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_MOEDA_FK_I ON TASY.ADIANTAMENTO_PAGO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_NOTFISC_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_PESFISI_FK_I ON TASY.ADIANTAMENTO_PAGO
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_PESJURI_FK_I ON TASY.ADIANTAMENTO_PAGO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ADIPAGO_PK ON TASY.ADIANTAMENTO_PAGO
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_PLSPRAD_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_PLSPRAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_PLSVEND_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_CANAL_VENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_TITPAGA_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_TITULO_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIPAGO_TPADPAG_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_TPADPAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIPAGO_TRAFINA_FK_I ON TASY.ADIANTAMENTO_PAGO
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIPAGO_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ADIANTAMENTO_PAGO_tp  after update ON TASY.ADIANTAMENTO_PAGO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_ADIANTAMENTO);  ds_c_w:=null;  exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.adiantamento_pago_after
after insert ON TASY.ADIANTAMENTO_PAGO for each row
declare

ie_cont_prov_adiant_pago_w      parametros_contas_pagar.ie_contab_prov_adiant_pago%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if      (nvl(:new.vl_adiantamento, 0) <> 0) then
        begin

        begin
        select  nvl(ie_contab_prov_adiant_pago, 'N')
        into    ie_cont_prov_adiant_pago_w
        from    parametros_contas_pagar
        where   cd_estabelecimento = :new.cd_estabelecimento;
        exception when others then
                ie_cont_prov_adiant_pago_w := 'N';
        end;

        begin
        select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_prov_adiant_pago, 'N'), ie_cont_prov_adiant_pago_w)
        into    ie_cont_prov_adiant_pago_w
        from    ctb_param_lote_contas_pag a
        where   a.cd_empresa = obter_empresa_estab(:new.cd_estabelecimento)
        and     nvl(a.cd_estab_exclusivo, :new.cd_estabelecimento) = :new.cd_estabelecimento;
        exception when others then
                null;
        end;

        if      (ie_cont_prov_adiant_pago_w = 'S') then
                begin
                ctb_concil_financeira_pck.ctb_gravar_documento  (       :new.cd_estabelecimento,
                                                                        :new.dt_adiantamento,
                                                                        7,
                                                                        :new.nr_seq_trans_fin,
                                                                        18,
                                                                        :new.nr_adiantamento,
                                                                        null,
                                                                        null,
                                                                        :new.vl_adiantamento,
                                                                        'ADIANTAMENTO_PAGO',
                                                                        'VL_ADIANT_PAGO',
                                                                        :new.nm_usuario);
                end;
        end if;

        end;
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.adiantamento_pago_atual
before insert or update ON TASY.ADIANTAMENTO_PAGO for each row
declare

ie_deduzir_ordem_adiant_w	varchar2(1);
dt_baixa_w		date;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:new.cd_pessoa_fisica is null) and
		(:new.cd_cgc is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(226813);
	end if;
end if;

if	(:new.vl_saldo = 0) and (:new.dt_baixa is null) then
	begin
	select	nvl(max(ie_deduzir_ordem_adiant),'S')
	into	ie_deduzir_ordem_adiant_w
	from	parametros_contas_pagar
	where	cd_estabelecimento	= :new.cd_estabelecimento;

	if	(ie_deduzir_ordem_adiant_w = 'N') then
		select	nvl(max(a.dt_contabil),sysdate)
		into	dt_baixa_w
		from	titulo_pagar b,
			titulo_pagar_adiant a
		where	a.nr_adiantamento	= :new.nr_adiantamento
		and	b.nr_titulo		= a.nr_titulo;
	else
		select	nvl(max(a.dt_contabil),sysdate)
		into	dt_baixa_w
		from	titulo_pagar b,
			titulo_pagar_adiant a
		where	a.nr_adiantamento	= :new.nr_adiantamento
		and	b.nr_titulo		= a.nr_titulo
		and	not exists(	select	1
				from	nota_fiscal_item y,
					ordem_compra_adiant_pago x
				where	x.nr_adiantamento	= a.nr_adiantamento
				and	x.nr_ordem_compra	= y.nr_ordem_compra
				and	y.nr_sequencia	= b.nr_seq_nota_fiscal);
	end if;

	:new.dt_baixa	:= dt_baixa_w;
	end;
end if;

if (updating) then
	if (:new.dt_baixa <> :old.dt_baixa)  then
		/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_baixa);
	end if;
else 	if (inserting) then
		/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_baixa);
	end if;
end if;

end;
/


ALTER TABLE TASY.ADIANTAMENTO_PAGO ADD (
  CONSTRAINT ADIPAGO_PK
 PRIMARY KEY
 (NR_ADIANTAMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADIANTAMENTO_PAGO ADD (
  CONSTRAINT ADIPAGO_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT ADIPAGO_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_CANAL_VENDA) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ADIPAGO_TPADPAG_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_ADIANT_PAGO (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANT_ORIGINAL) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO),
  CONSTRAINT ADIPAGO_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NF) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_DLDISTR_FK 
 FOREIGN KEY (NR_SEQ_DISTRIBUICAO) 
 REFERENCES TASY.DL_DISTRIBUICAO (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_ORIGINAL) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT ADIPAGO_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT ADIPAGO_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT ADIPAGO_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT ADIPAGO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ADIPAGO_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.ADIANTAMENTO_PAGO TO NIVEL_1;

