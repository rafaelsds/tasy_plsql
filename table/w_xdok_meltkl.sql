ALTER TABLE TASY.W_XDOK_MELTKL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_XDOK_MELTKL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_XDOK_MELTKL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_IMPORT        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  KRANKENANSTALT       VARCHAR2(4 BYTE),
  LEISTUNG             VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WXMETK_I1 ON TASY.W_XDOK_MELTKL
(KRANKENANSTALT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WXMETK_PK ON TASY.W_XDOK_MELTKL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WXMETK_WXIMPT_FK_I ON TASY.W_XDOK_MELTKL
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_XDOK_MELTKL ADD (
  CONSTRAINT WXMETK_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

