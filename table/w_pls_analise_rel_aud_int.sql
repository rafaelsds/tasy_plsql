ALTER TABLE TASY.W_PLS_ANALISE_REL_AUD_INT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_ANALISE_REL_AUD_INT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_ANALISE_REL_AUD_INT
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_ANALISE               NUMBER(10)       NOT NULL,
  NR_SEQ_GRUPO_MEMBRO_MED      NUMBER(10),
  IE_PROCED_PRINC_AUT          VARCHAR2(5 BYTE),
  NR_SEQ_GRUPO_MEMBRO_ENF      NUMBER(10),
  DS_PROCED_PRINC_AUT          VARCHAR2(260 BYTE),
  DT_PROCED_PRINC_AUT          DATE,
  IE_ALT_PROCED_ATEND          VARCHAR2(5 BYTE),
  DS_ALT_PROCED_ATEND          VARCHAR2(260 BYTE),
  DT_ALT_PROCED_ATEND          DATE,
  IE_QUIMIO_DIFERENTE_UTIL     VARCHAR2(5 BYTE),
  DS_QUIMIO_DIFERENTE_UTIL     VARCHAR2(260 BYTE),
  DT_QUIMIO_DIFERENTE_UTIL     DATE,
  IE_ALT_CIRURGICO_ADIC_PROC   VARCHAR2(5 BYTE),
  DS_ALT_CIRURGICO_ADIC_PROC   VARCHAR2(260 BYTE),
  DT_ALT_CIRURGICO_ADIC_PROC   DATE,
  IE_ALT_CIRURGICO_EXC_PROC    VARCHAR2(5 BYTE),
  DS_ALT_CIRURGICO_EXC_PROC    VARCHAR2(260 BYTE),
  DT_ALT_CIRURGICO_EXC_PROC    DATE,
  IE_ALT_CIRURGICO_MOD_TEC     VARCHAR2(5 BYTE),
  DS_ALT_CIRURGICO_MOD_TEC     VARCHAR2(260 BYTE),
  DT_ALT_CIRURGICO_MOD_TEC     DATE,
  IE_ALT_CIRURGICO_ADIC_OPME   VARCHAR2(5 BYTE),
  DS_ALT_CIRURGICO_ADIC_OPME   VARCHAR2(260 BYTE),
  DT_ALT_CIRURGICO_ADIC_OPME   DATE,
  IE_ALT_CIRURGICO_EXC_OPME    VARCHAR2(5 BYTE),
  DS_ALT_CIRURGICO_EXC_OPME    VARCHAR2(260 BYTE),
  DT_ALT_CIRURGICO_EXC_OPME    DATE,
  DS_INDICACAO_CLINICA         VARCHAR2(700 BYTE),
  DS_EVOLUCAO_CLINICA          VARCHAR2(1000 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(900 BYTE),
  IE_ANTIBIOTICO               VARCHAR2(5 BYTE),
  DS_ANTIBIOTICO               VARCHAR2(152 BYTE),
  DS_ANTIBIOTICO_POSOLOGIA     VARCHAR2(152 BYTE),
  IE_ANTIFUNGICO               VARCHAR2(5 BYTE),
  DS_ANTIFUNGICO               VARCHAR2(152 BYTE),
  DS_ANTIFUNGICO_POSOLOGIA     VARCHAR2(152 BYTE),
  IE_IMUNOBIOLOGICO            VARCHAR2(5 BYTE),
  DS_IMUNOBIOLOGICO            VARCHAR2(152 BYTE),
  DS_IMUNOBIO_POSOLOGIA        VARCHAR2(152 BYTE),
  IE_SANGUE                    VARCHAR2(5 BYTE),
  DS_SANGUE                    VARCHAR2(152 BYTE),
  DS_SANGUE_POSOLOGIA          VARCHAR2(152 BYTE),
  IE_OPME_PROC                 VARCHAR2(5 BYTE),
  DS_OPME_PROC                 VARCHAR2(152 BYTE),
  DS_OPME_PROC_POSOLOGIA       VARCHAR2(152 BYTE),
  IE_NUTRICAO_PROC             VARCHAR2(5 BYTE),
  DS_NUTRICAO_PROC             VARCHAR2(152 BYTE),
  DS_NUTRICAO_PROC_POSOLOGIA   VARCHAR2(152 BYTE),
  IE_TERAPIA_RENAL             VARCHAR2(5 BYTE),
  DS_TERAPIA_RENAL             VARCHAR2(152 BYTE),
  DS_TERAPIA_RENAL_POSOLOGIA   VARCHAR2(152 BYTE),
  IE_CURATIVO_ESPEC            VARCHAR2(5 BYTE),
  DS_CURATIVO_ESPEC            VARCHAR2(152 BYTE),
  DS_CURATIVO_ESPEC_POSOLOGIA  VARCHAR2(152 BYTE),
  IE_GASOTERAPIA               VARCHAR2(5 BYTE),
  DS_GASOTERAPIA               VARCHAR2(152 BYTE),
  DS_GASOTERAPIA_POSOLOGIA     VARCHAR2(152 BYTE),
  IE_ANATOMOPATOLOGICO         VARCHAR2(5 BYTE),
  DS_ANATOMOPATOLOGICO         VARCHAR2(152 BYTE),
  DS_ANATOMOP_POSOLOGIA        VARCHAR2(152 BYTE),
  IE_OXIGENIO_HIPER            VARCHAR2(5 BYTE),
  DS_OXIGENIO_HIPER            VARCHAR2(152 BYTE),
  DS_OXIGENIO_HIPER_POSOLOGIA  VARCHAR2(152 BYTE),
  DS_ACAO_AUDITORIA            VARCHAR2(117 BYTE),
  CD_PROCEDIMENTO              VARCHAR2(255 BYTE),
  DT_PROCEDIMENTO              DATE,
  HR_PROCED_PRINC_AUT          VARCHAR2(50 BYTE),
  HR_ALT_PROCED_ATEND          VARCHAR2(50 BYTE),
  HR_QUIMIO_DIFERENTE_UTIL     VARCHAR2(50 BYTE),
  HR_ALT_CIRURGICO_ADIC_PROC   VARCHAR2(50 BYTE),
  HR_ALT_CIRURGICO_EXC_PROC    VARCHAR2(50 BYTE),
  HR_ALT_CIRURGICO_MOD_TEC     VARCHAR2(50 BYTE),
  HR_ALT_CIRURGICO_ADIC_OPME   VARCHAR2(50 BYTE),
  HR_ALT_CIRURGICO_EXC_OPME    VARCHAR2(50 BYTE),
  DS_OBSERVACAO                VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPLSRAI_PK ON TASY.W_PLS_ANALISE_REL_AUD_INT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSRAI_PLSMGRA_FK_I ON TASY.W_PLS_ANALISE_REL_AUD_INT
(NR_SEQ_GRUPO_MEMBRO_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPLSRAI_PLSMGRA_FK2_I ON TASY.W_PLS_ANALISE_REL_AUD_INT
(NR_SEQ_GRUPO_MEMBRO_ENF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.w_pls_analise_rel_aud_int_alt
before update ON TASY.W_PLS_ANALISE_REL_AUD_INT for each row
declare
	ds_observacao_w	Varchar2(4000);
begin
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S')  then

		if	(nvl(:old.cd_procedimento,'x') <> nvl(:new.cd_procedimento,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'c�digo procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.cd_procedimento||' - modificada: '||:new.cd_procedimento;
		end if;

		if	(nvl(:old.ds_acao_auditoria,'x') <> nvl(:new.ds_acao_auditoria,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o a��o auditoria: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_acao_auditoria||' - modificada: '||:new.ds_acao_auditoria;
		end if;

		if	(nvl(:old.ds_alt_cirurgico_adic_opme,'x') <> nvl(:new.ds_alt_cirurgico_adic_opme,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o altera��o cirurgico adicionar OPME: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_alt_cirurgico_adic_opme||' - modificada: '||:new.ds_alt_cirurgico_adic_opme;
		end if;

		if	(nvl(:old.ds_alt_cirurgico_adic_proc,'x') <> nvl(:new.ds_alt_cirurgico_adic_proc,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o altera��o cirurgico adicionar procedimentos:'||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_alt_cirurgico_adic_proc||' - modificada: '||:new.ds_alt_cirurgico_adic_proc;
		end if;

		if	(nvl(:old.ds_alt_cirurgico_exc_opme,'x') <> nvl(:new.ds_alt_cirurgico_exc_opme,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o altera��o cirurgico executar OPME: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_alt_cirurgico_exc_opme||' - modificada: '||:new.ds_alt_cirurgico_exc_opme;
		end if;

		if	(nvl(:old.ds_alt_cirurgico_exc_proc,'x') <> nvl(:new.ds_alt_cirurgico_exc_proc,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o altera��o cirurgico executar procedimentos: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_alt_cirurgico_exc_proc||' - modificada: '||:new.ds_alt_cirurgico_exc_proc;
		end if;

		if	(nvl(:old.ds_alt_cirurgico_mod_tec,'x') <> nvl(:new.ds_alt_cirurgico_mod_tec,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o altera��o cirurgico modificar tec: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_alt_cirurgico_mod_tec||' - modificada: '||:new.ds_alt_cirurgico_mod_tec;
		end if;

		if	(nvl(:old.ds_alt_proced_atend,'x') <> nvl(:new.ds_alt_proced_atend,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o altera��o procedimento atendimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_alt_proced_atend||' - modificada: '||:new.ds_alt_proced_atend;
		end if;

		if	(nvl(:old.ds_anatomopatologico,'x') <> nvl(:new.ds_anatomopatologico,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o anatomopatologico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_anatomopatologico||' - modificada: '||:new.ds_anatomopatologico;
		end if;

		if	(nvl(:old.ds_antibiotico,'x') <> nvl(:new.ds_antibiotico,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o antibiotico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_antibiotico||' - modificada: '||:new.ds_antibiotico;
		end if;

		if	(nvl(:old.ds_antibiotico_posologia,'x') <> nvl(:new.ds_antibiotico_posologia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o antibiotico posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_antibiotico_posologia||' - modificada: '||:new.ds_antibiotico_posologia;
		end if;

		if	(nvl(:old.ds_anatomop_posologia,'x') <> nvl(:new.ds_anatomop_posologia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o anatomopatologico posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_anatomop_posologia||' - modificada: '||:new.ds_anatomop_posologia;
		end if;

		if	(nvl(:old.ds_antifungico,'x') <> nvl(:new.ds_antifungico,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o antif�ngico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_antifungico||' - modificada: '||:new.ds_antifungico;
		end if;

		if	(nvl(:old.ds_antifungico_posologia,'x') <> nvl(:new.ds_antifungico_posologia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o antif�ngico posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_antifungico_posologia||' - modificada: '||:new.ds_antifungico_posologia;
		end if;

		if	(nvl(:old.ds_curativo_espec,'x') <> nvl(:new.ds_curativo_espec,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o curativo espec: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_curativo_espec||' - modificada: '||:new.ds_curativo_espec;
		end if;

		if	(nvl(:old.ds_curativo_espec_posologia,'x') <> nvl(:new.ds_curativo_espec_posologia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o curativo espec posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_curativo_espec_posologia||' - modificada: '||:new.ds_curativo_espec_posologia;
		end if;

		if	(nvl(:old.ds_evolucao_clinica,'x') <> nvl(:new.ds_evolucao_clinica,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o escri��o evolu��o cl�nica: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_evolucao_clinica||' - modificada: '||:new.ds_evolucao_clinica;
		end if;

		if	(nvl(:old.ds_gasoterapia,'x') <> nvl(:new.ds_gasoterapia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o gasoterapia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_gasoterapia||' - modificada: '||:new.ds_gasoterapia;
		end if;

		if	(nvl(:old.ds_gasoterapia_posologia,'x') <> nvl(:new.ds_gasoterapia_posologia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o gasoterapia posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_gasoterapia_posologia||' - modificada: '||:new.ds_gasoterapia_posologia;
		end if;

		if	(nvl(:old.ds_imunobiologico,'x') <> nvl(:new.ds_imunobiologico,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o imunobiol�gico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_imunobiologico||' - modificada: '||:new.ds_imunobiologico;
		end if;

		if	(nvl(:old.ds_imunobio_posologia,'x') <> nvl(:new.ds_imunobio_posologia,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o imunobiol�gico posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_imunobio_posologia||' - modificada: '||:new.ds_imunobio_posologia;
		end if;

		if	(nvl(:old.ds_indicacao_clinica ,'x') <> nvl(:new.ds_indicacao_clinica ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o indica��o cl�nica: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_indicacao_clinica ||' - modificada: '||:new.ds_indicacao_clinica ;
		end if;

		if	(nvl(:old.ds_justificativa ,'x') <> nvl(:new.ds_justificativa ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o justificativa: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_justificativa ||' - modificada: '||:new.ds_justificativa ;
		end if;

		if	(nvl(:old.ds_nutricao_proc ,'x') <> nvl(:new.ds_nutricao_proc ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o nutri��o procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_nutricao_proc ||' - modificada: '||:new.ds_nutricao_proc ;
		end if;

		if	(nvl(:old.ds_nutricao_proc_posologia  ,'x') <> nvl(:new.ds_nutricao_proc_posologia  ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o nutri��o procedimento posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_nutricao_proc_posologia  ||' - modificada: '||:new.ds_nutricao_proc_posologia  ;
		end if;

		if	(nvl(:old.ds_opme_proc   ,'x') <> nvl(:new.ds_opme_proc   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o OPME procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_opme_proc   ||' - modificada: '||:new.ds_opme_proc   ;
		end if;

		if	(nvl(:old.ds_opme_proc_posologia   ,'x') <> nvl(:new.ds_opme_proc_posologia   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o OPME procedimento posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_opme_proc_posologia   ||' - modificada: '||:new.ds_opme_proc_posologia   ;
		end if;

		if	(nvl(:old.ds_oxigenio_hiper   ,'x') <> nvl(:new.ds_oxigenio_hiper   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o oxigenio hiper: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_oxigenio_hiper   ||' - modificada: '||:new.ds_oxigenio_hiper   ;
		end if;

		if	(nvl(:old.ds_oxigenio_hiper_posologia   ,'x') <> nvl(:new.ds_oxigenio_hiper_posologia   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o oxigenio hiper posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_oxigenio_hiper_posologia   ||' - modificada: '||:new.ds_oxigenio_hiper_posologia   ;
		end if;

		if	(nvl(:old.ds_proced_princ_aut   ,'x') <> nvl(:new.ds_proced_princ_aut   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o procedimento principal aut: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_proced_princ_aut   ||' - modificada: '||:new.ds_proced_princ_aut   ;
		end if;

		if	(nvl(:old.ds_quimio_diferente_util   ,'x') <> nvl(:new.ds_quimio_diferente_util   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o quimio diferente util: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_quimio_diferente_util   ||' - modificada: '||:new.ds_quimio_diferente_util   ;
		end if;

		if	(nvl(:old.ds_sangue   ,'x') <> nvl(:new.ds_sangue   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o sangue: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_sangue   ||' - modificada: '||:new.ds_sangue   ;
		end if;

		if	(nvl(:old.ds_sangue_posologia   ,'x') <> nvl(:new.ds_sangue_posologia   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o sangue posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_sangue_posologia   ||' - modificada: '||:new.ds_sangue_posologia   ;
		end if;

		if	(nvl(:old.ds_terapia_renal   ,'x') <> nvl(:new.ds_terapia_renal   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o terapia renal: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_terapia_renal   ||' - modificada: '||:new.ds_terapia_renal   ;
		end if;

		if	(nvl(:old.ds_terapia_renal_posologia   ,'x') <> nvl(:new.ds_terapia_renal_posologia   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Descri��o terapia renal posologia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ds_terapia_renal_posologia   ||' - modificada: '||:new.ds_terapia_renal_posologia   ;
		end if;

		if	(nvl(to_char(:old.dt_atualizacao_nrec, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_atualizacao_nrec, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de atualiza��o: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_atualizacao_nrec, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_atualizacao_nrec, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

				if	(nvl(to_char(:old.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de atualiza��o: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_alt_cirurgico_adic_opme, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_alt_cirurgico_adic_opme, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de altera��o cir�rgico adicionar OPME: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_alt_cirurgico_adic_opme, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_alt_cirurgico_adic_opme, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_alt_cirurgico_adic_proc, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_alt_cirurgico_adic_proc, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de altera��o cir�rgico adicionar procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_alt_cirurgico_adic_proc, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_alt_cirurgico_adic_proc, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_alt_cirurgico_exc_opme, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_alt_cirurgico_exc_opme, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de altera��o cir�rgico executar OPME: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_alt_cirurgico_exc_opme, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_alt_cirurgico_exc_opme, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_alt_cirurgico_exc_proc, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_alt_cirurgico_exc_proc, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de altera��o cir�rgico executar procedimento-: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_alt_cirurgico_exc_proc, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_alt_cirurgico_exc_proc, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_alt_cirurgico_mod_tec, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_alt_cirurgico_mod_tec, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de altera��o cir�rgico modificar tec: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_alt_cirurgico_mod_tec, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_alt_cirurgico_mod_tec, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_alt_proced_atend, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_alt_proced_atend, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data de altera��o procedimento atendimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_alt_proced_atend, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_alt_proced_atend, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_procedimento, 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_procedimento, 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_procedimento, 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_procedimento, 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(to_char(:old.dt_proced_princ_aut , 'dd/mm/yyyy hh24:mi:ss'),'x') <>  nvl(to_char(:new.dt_proced_princ_aut , 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data procedimento principal aut: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_proced_princ_aut , 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_proced_princ_aut , 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(:old.hr_alt_cirurgico_adic_opme   ,'x') <> nvl(:new.hr_alt_cirurgico_adic_opme   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora altera��o cirurgico adicionar OPME:  '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_alt_cirurgico_adic_opme   ||' - modificada: '||:new.hr_alt_cirurgico_adic_opme   ;
		end if;

		if	(nvl(:old.hr_alt_cirurgico_adic_proc   ,'x') <> nvl(:new.hr_alt_cirurgico_adic_proc   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora altera��o cirurgico adicionar procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_alt_cirurgico_adic_proc   ||' - modificada: '||:new.hr_alt_cirurgico_adic_proc   ;
		end if;

		if	(nvl(:old.hr_alt_cirurgico_exc_opme   ,'x') <> nvl(:new.hr_alt_cirurgico_exc_opme   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora altera��o cirurgico executar OPME:  '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_alt_cirurgico_exc_opme   ||' - modificada: '||:new.hr_alt_cirurgico_exc_opme   ;
		end if;

		if	(nvl(:old.hr_alt_cirurgico_exc_proc   ,'x') <> nvl(:new.hr_alt_cirurgico_exc_proc   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora altera��o cirurgico executar procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_alt_cirurgico_exc_proc   ||' - modificada: '||:new.hr_alt_cirurgico_exc_proc   ;
		end if;

		if	(nvl(:old.hr_alt_cirurgico_mod_tec   ,'x') <> nvl(:new.hr_alt_cirurgico_mod_tec   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora altera��o cirurgico modificar tec: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_alt_cirurgico_mod_tec   ||' - modificada: '||:new.hr_alt_cirurgico_mod_tec   ;
		end if;

		if	(nvl(:old.hr_alt_proced_atend   ,'x') <> nvl(:new.hr_alt_proced_atend   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora altera��o procedimento atendimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_alt_proced_atend   ||' - modificada: '||:new.hr_alt_proced_atend   ;
		end if;

		if	(nvl(:old.hr_proced_princ_aut   ,'x') <> nvl(:new.hr_proced_princ_aut   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora procedimento principal aut: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_proced_princ_aut   ||' - modificada: '||:new.hr_proced_princ_aut   ;
		end if;

		if	(nvl(:old.hr_quimio_diferente_util   ,'x') <> nvl(:new.hr_quimio_diferente_util   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Hora quimio diferente util: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.hr_quimio_diferente_util   ||' - modificada: '||:new.hr_quimio_diferente_util   ;
		end if;

		if	(nvl(to_char(:old.dt_quimio_diferente_util , 'dd/mm/yyyy hh24:mi:ss'),'x') <> nvl(to_char(:new.dt_quimio_diferente_util , 'dd/mm/yyyy hh24:mi:ss'),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Data quimio diferente util: '||chr(13)||chr(10)||
						chr(9)||'anterior: '||to_char(:old.dt_quimio_diferente_util , 'dd/mm/yyyy hh24:mi:ss')||' - modificado: '||to_char(:new.dt_quimio_diferente_util , 'dd/mm/yyyy hh24:mi:ss')||chr(13)||chr(10);
		end if;

		if	(nvl(:old.ie_alt_cirurgico_adic_opme   ,'x') <> nvl(:new.ie_alt_cirurgico_adic_opme   ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Altera��o cirurgico adicionar OPME: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_alt_cirurgico_adic_opme   ||' - modificada: '||:new.ie_alt_cirurgico_adic_opme   ;
		end if;

		if	(nvl(:old.ie_alt_cirurgico_adic_proc    ,'x') <> nvl(:new.ie_alt_cirurgico_adic_proc    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Altera��o cirurgico adicionar procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_alt_cirurgico_adic_proc    ||' - modificada: '||:new.ie_alt_cirurgico_adic_proc    ;
		end if;

		if	(nvl(:old.ie_alt_cirurgico_exc_opme    ,'x') <> nvl(:new.ie_alt_cirurgico_exc_opme    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Altera��o cirurgico executar OPME: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_alt_cirurgico_exc_opme    ||' - modificada: '||:new.ie_alt_cirurgico_exc_opme    ;
		end if;

		if	(nvl(:old.ie_alt_cirurgico_exc_proc    ,'x') <> nvl(:new.ie_alt_cirurgico_exc_proc    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Altera��o cirurgico executar procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_alt_cirurgico_exc_proc    ||' - modificada: '||:new.ie_alt_cirurgico_exc_proc    ;
		end if;

		if	(nvl(:old.ie_alt_cirurgico_mod_tec    ,'x') <> nvl(:new.ie_alt_cirurgico_mod_tec    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Altera��o cirurgico modificar tec: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_alt_cirurgico_mod_tec    ||' - modificada: '||:new.ie_alt_cirurgico_mod_tec    ;
		end if;

		if	(nvl(:old.ie_alt_proced_atend    ,'x') <> nvl(:new.ie_alt_proced_atend    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Altera��o procedimento atendimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_alt_proced_atend    ||' - modificada: '||:new.ie_alt_proced_atend    ;
		end if;

		if	(nvl(:old.ie_anatomopatologico    ,'x') <> nvl(:new.ie_anatomopatologico    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Anatomopatologico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_anatomopatologico    ||' - modificada: '||:new.ie_anatomopatologico    ;
		end if;

		if	(nvl(:old.ie_antibiotico    ,'x') <> nvl(:new.ie_antibiotico    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Antibi�tico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_antibiotico    ||' - modificada: '||:new.ie_antibiotico    ;
		end if;

		if	(nvl(:old.ie_antifungico    ,'x') <> nvl(:new.ie_antifungico    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Antif�ngico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_antifungico    ||' - modificada: '||:new.ie_antifungico    ;
		end if;

		if	(nvl(:old.ie_curativo_espec    ,'x') <> nvl(:new.ie_curativo_espec    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Curativo espec: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_curativo_espec    ||' - modificada: '||:new.ie_curativo_espec    ;
		end if;

		if	(nvl(:old.ie_gasoterapia    ,'x') <> nvl(:new.ie_gasoterapia    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Gasoterapia: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_gasoterapia    ||' - modificada: '||:new.ie_gasoterapia    ;
		end if;

		if	(nvl(:old.ie_imunobiologico    ,'x') <> nvl(:new.ie_imunobiologico    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Imunobiol�gico: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_imunobiologico    ||' - modificada: '||:new.ie_imunobiologico    ;
		end if;

		if	(nvl(:old.ie_nutricao_proc    ,'x') <> nvl(:new.ie_nutricao_proc    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Nutri��o procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_nutricao_proc    ||' - modificada: '||:new.ie_nutricao_proc    ;
		end if;

		if	(nvl(:old.ie_opme_proc    ,'x') <> nvl(:new.ie_opme_proc    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Opme procedimento: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_opme_proc    ||' - modificada: '||:new.ie_opme_proc    ;
		end if;

		if	(nvl(:old.ie_oxigenio_hiper    ,'x') <> nvl(:new.ie_oxigenio_hiper    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Oxig�nio hiper: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_oxigenio_hiper    ||' - modificada: '||:new.ie_oxigenio_hiper    ;
		end if;

		if	(nvl(:old.ie_proced_princ_aut    ,'x') <> nvl(:new.ie_proced_princ_aut    ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Procedimento principal aut: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_proced_princ_aut    ||' - modificada: '||:new.ie_proced_princ_aut    ;
		end if;

		if	(nvl(:old.ie_quimio_diferente_util     ,'x') <> nvl(:new.ie_quimio_diferente_util     ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Quimio diferente util: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_quimio_diferente_util     ||' - modificada: '||:new.ie_quimio_diferente_util     ;
		end if;

		if	(nvl(:old.ie_sangue      ,'x') <> nvl(:new.ie_sangue      ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Sangue: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_sangue      ||' - modificada: '||:new.ie_sangue      ;
		end if;

		if	(nvl(:old.ie_terapia_renal      ,'x') <> nvl(:new.ie_terapia_renal      ,'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Terapia renal: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| :old.ie_terapia_renal      ||' - modificada: '||:new.ie_terapia_renal      ;
		end if;


		if	(nvl(to_char(:old.nr_seq_analise),'x') <> nvl(to_char(:new.nr_seq_analise),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'N�mero seq an�lise '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| to_char(:old.nr_seq_analise)      ||' - modificada: '||to_char(:new.nr_seq_analise)      ;
		end if;

		if	(nvl(to_char(:old.nr_seq_grupo_membro_enf),'x') <> nvl(to_char(:new.nr_seq_grupo_membro_enf),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'N�mero seq grupo membro enf: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| to_char(:old.nr_seq_grupo_membro_enf)      ||' - modificada: '||to_char(:new.nr_seq_grupo_membro_enf)      ;
		end if;

		if	(nvl(to_char(:old.nr_seq_grupo_membro_med),'x') <> nvl(to_char(:new.nr_seq_grupo_membro_med),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'N�mero seq grupo membro med: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| to_char(:old.nr_seq_grupo_membro_med)      ||' - modificada: '||to_char(:new.nr_seq_grupo_membro_med)     ;
		end if;

		if	(nvl(to_char(:old.nr_sequencia),'x') <> nvl(to_char(:new.nr_sequencia),'x')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'N�mero seq grupo membro med: '||chr(13)||chr(10)||
						chr(9)||'anterior: '|| to_char(:old.nr_sequencia)      ||' - modificada: '||to_char(:new.nr_sequencia)      ;
		end if;


		insert into pls_log_relat_audit	( 	nr_sequencia, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec,
							ds_log, nr_seq_relatorio)
				values		( 	pls_log_relat_audit_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
							sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
							ds_observacao_w, :new.nr_sequencia);
		--end if;

end if;

end w_pls_analise_rel_aud_int_alt;
/


ALTER TABLE TASY.W_PLS_ANALISE_REL_AUD_INT ADD (
  CONSTRAINT WPLSRAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PLS_ANALISE_REL_AUD_INT ADD (
  CONSTRAINT WPLSRAI_PLSMGRA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MEMBRO_MED) 
 REFERENCES TASY.PLS_MEMBRO_GRUPO_AUD (NR_SEQUENCIA),
  CONSTRAINT WPLSRAI_PLSMGRA_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_MEMBRO_ENF) 
 REFERENCES TASY.PLS_MEMBRO_GRUPO_AUD (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PLS_ANALISE_REL_AUD_INT TO NIVEL_1;

