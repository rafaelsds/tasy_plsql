ALTER TABLE TASY.REG_PROGRAM_PRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_PROGRAM_PRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_PROGRAM_PRS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE),
  NR_SEQ_REG_PROGRAM          NUMBER(10),
  NR_SEQ_PRODUCT_REQUIREMENT  NUMBER(10),
  NM_USUARIO_EXCLUSAO         VARCHAR2(15 BYTE),
  DS_MOTIVO_EXCLUSAO          VARCHAR2(1000 BYTE),
  DS_MOTIVO_INCLUSAO          VARCHAR2(1000 BYTE),
  NM_USUARIO_INCLUSAO         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.REGPROGPR_PK ON TASY.REG_PROGRAM_PRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGPROGPR_REGPR_FK_I ON TASY.REG_PROGRAM_PRS
(NR_SEQ_PRODUCT_REQUIREMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.REGPROGPR_REGPROG_FK_I ON TASY.REG_PROGRAM_PRS
(NR_SEQ_REG_PROGRAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


ALTER TABLE TASY.REG_PROGRAM_PRS ADD (
  CONSTRAINT REGPROGPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REG_PROGRAM_PRS ADD (
  CONSTRAINT REGPROGPR_REGPROG_FK 
 FOREIGN KEY (NR_SEQ_REG_PROGRAM) 
 REFERENCES TASY.REG_PROGRAM (NR_SEQUENCIA),
  CONSTRAINT REGPROGPR_REGPR_FK 
 FOREIGN KEY (NR_SEQ_PRODUCT_REQUIREMENT) 
 REFERENCES TASY.REG_PRODUCT_REQUIREMENT (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_PROGRAM_PRS TO NIVEL_1;

