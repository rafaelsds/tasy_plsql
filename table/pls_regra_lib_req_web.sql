ALTER TABLE TASY.PLS_REGRA_LIB_REQ_WEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_LIB_REQ_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_LIB_REQ_WEB
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  IE_TIPO_LIBERACAO        VARCHAR2(2 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_USUARIO_WEB       NUMBER(10),
  IE_TIPO_GUIA             VARCHAR2(2 BYTE),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  DT_FIM_VIGENCIA          DATE,
  CD_PROCEDIMENTO          NUMBER(15),
  IE_ORIGEM_PROCED         NUMBER(10),
  CD_AREA_PROCEDIMENTO     NUMBER(15),
  CD_ESPECIALIDADE         NUMBER(15),
  CD_GRUPO_PROC            NUMBER(15),
  NR_SEQ_CONTRATO          NUMBER(10),
  NR_SEQ_SEGURADO          NUMBER(10),
  NR_SEQ_MOTIVO            NUMBER(10),
  NR_SEQ_MOTIVO_CARTAO     NUMBER(10),
  IE_CONSULTA_URGENCIA     VARCHAR2(1 BYTE),
  IE_FUNCAO_LIBERADA       NUMBER(3),
  DT_ANT_EMISSAO_CARTEIRA  DATE,
  NR_SEQ_CONGENERE         NUMBER(10),
  NR_SEQ_CONTRATO_INT      NUMBER(10),
  QT_DIAS_EMISSAO          NUMBER(5),
  IE_PRECO                 VARCHAR2(2 BYTE),
  NR_SEQ_ATEND_PLS         NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4),
  IE_TIPO_SEGURADO         VARCHAR2(3 BYTE),
  IE_TIPO_ATENDIMENTO      VARCHAR2(2 BYTE),
  NR_SEQ_PRESTADOR         NUMBER(10)           DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRLRW_AREPROC_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_ESPPROC_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_ESTABEL_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRLRW_GRUPROC_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_GRUPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRLRW_PK ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRLRW_PLSATEN_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_ATEND_PLS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRLRW_PLSCONG_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PLSCONT_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PLSINCA_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_CONTRATO_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PLSMLCA_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_MOTIVO_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSMLCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PLSMLDI_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSMLDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PLSPRES_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRLRW_PLSSEGU_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PLSUSWE_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(NR_SEQ_USUARIO_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PLSUSWE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRLRW_PROCEDI_FK_I ON TASY.PLS_REGRA_LIB_REQ_WEB
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRLRW_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_LIB_REQ_WEB_tp  after update ON TASY.PLS_REGRA_LIB_REQ_WEB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_USUARIO_WEB,1,4000),substr(:new.NR_SEQ_USUARIO_WEB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_USUARIO_WEB',ie_log_w,ds_w,'PLS_REGRA_LIB_REQ_WEB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SEGURADO,1,4000),substr(:new.NR_SEQ_SEGURADO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SEGURADO',ie_log_w,ds_w,'PLS_REGRA_LIB_REQ_WEB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_LIBERACAO,1,4000),substr(:new.IE_TIPO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_LIBERACAO',ie_log_w,ds_w,'PLS_REGRA_LIB_REQ_WEB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_LIB_REQ_WEB ADD (
  CONSTRAINT PLSRLRW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_LIB_REQ_WEB ADD (
  CONSTRAINT PLSRLRW_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSRLRW_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSRLRW_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSRLRW_PLSMLDI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.PLS_MOTIVO_LIB_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSMLCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CARTAO) 
 REFERENCES TASY.PLS_MOTIVO_LIB_CARTAO (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_INT) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PLS) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRLRW_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_USUARIO_WEB) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSRLRW_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.PLS_REGRA_LIB_REQ_WEB TO NIVEL_1;

