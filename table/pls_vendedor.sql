ALTER TABLE TASY.PLS_VENDEDOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_VENDEDOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_VENDEDOR
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA                VARCHAR2(10 BYTE),
  CD_CGC                          VARCHAR2(14 BYTE),
  IE_SITUACAO                     VARCHAR2(1 BYTE) NOT NULL,
  CD_ESTABELECIMENTO              NUMBER(4)     NOT NULL,
  CD_CONDICAO_PAGAMENTO           NUMBER(10),
  IE_TIPO_VENDEDOR                VARCHAR2(2 BYTE),
  NR_SEQ_VENDEDOR_ANT             NUMBER(10),
  CD_SISTEMA_ANTERIOR             VARCHAR2(30 BYTE),
  IE_RECEBER_EMAIL_COMERCIAL      VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_CANAL            NUMBER(10),
  IE_CANAL_PRINCIPAL              VARCHAR2(1 BYTE),
  IE_AUXILIAR_PF_VINCULADO        VARCHAR2(1 BYTE),
  NR_SEQ_RESTRICAO                NUMBER(10),
  DT_INICIO_VIGENCIA              DATE,
  DT_FIM_VIGENCIA                 DATE,
  IE_GERAR_NOTA_FISCAL            VARCHAR2(1 BYTE),
  IE_EXIBIR_SOMENTE_CANAL_PORTAL  VARCHAR2(1 BYTE),
  IE_FORMA_PAGAMENTO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSVEND_CONPAGA_FK_I ON TASY.PLS_VENDEDOR
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVEND_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVEND_ESTABEL_FK_I ON TASY.PLS_VENDEDOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVEND_PESFISI_FK_I ON TASY.PLS_VENDEDOR
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVEND_PESJURI_FK_I ON TASY.PLS_VENDEDOR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSVEND_PK ON TASY.PLS_VENDEDOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVEND_PLSCCVE_FK_I ON TASY.PLS_VENDEDOR
(NR_SEQ_CLASSIF_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVEND_PLSTPRC_FK_I ON TASY.PLS_VENDEDOR
(NR_SEQ_RESTRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVEND_PLSTPRC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_VENDEDOR_tp  after update ON TASY.PLS_VENDEDOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_VENDEDOR,1,4000),substr(:new.IE_TIPO_VENDEDOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VENDEDOR',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANTERIOR,1,4000),substr(:new.CD_SISTEMA_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANTERIOR',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_CANAL,1,4000),substr(:new.NR_SEQ_CLASSIF_CANAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_CANAL',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUXILIAR_PF_VINCULADO,1,4000),substr(:new.IE_AUXILIAR_PF_VINCULADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUXILIAR_PF_VINCULADO',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CANAL_PRINCIPAL,1,4000),substr(:new.IE_CANAL_PRINCIPAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_CANAL_PRINCIPAL',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RESTRICAO,1,4000),substr(:new.NR_SEQ_RESTRICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RESTRICAO',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEBER_EMAIL_COMERCIAL,1,4000),substr(:new.IE_RECEBER_EMAIL_COMERCIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEBER_EMAIL_COMERCIAL',ie_log_w,ds_w,'PLS_VENDEDOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_VENDEDOR ADD (
  CONSTRAINT PLSVEND_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_VENDEDOR ADD (
  CONSTRAINT PLSVEND_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSVEND_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSVEND_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSVEND_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PLSVEND_PLSCCVE_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_CANAL) 
 REFERENCES TASY.PLS_CLASSIF_CANAL_VENDA (NR_SEQUENCIA),
  CONSTRAINT PLSVEND_PLSTPRC_FK 
 FOREIGN KEY (NR_SEQ_RESTRICAO) 
 REFERENCES TASY.PLS_TIPO_RESTRICAO_CANAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_VENDEDOR TO NIVEL_1;

