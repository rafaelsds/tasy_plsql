ALTER TABLE TASY.ATENDIMENTO_PRECAUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_PRECAUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_PRECAUCAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_PRECAUCAO           NUMBER(10)         NOT NULL,
  DT_INICIO                  DATE,
  DT_TERMINO                 DATE,
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_MOTIVO_ISOL         NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_REGISTRO                DATE               NOT NULL,
  CD_MICROORGANISMO          NUMBER(10),
  CD_TOPOGRAFIA              NUMBER(10),
  DT_FINAL_PRECAUCAO         DATE,
  CD_MEDICO_SOLIC            VARCHAR2(10 BYTE),
  NM_USUARIO_FIM             VARCHAR2(15 BYTE),
  NR_SEQ_MOVTO_TERMINO       NUMBER(10),
  NM_USUARIO_LIB_ISO         VARCHAR2(15 BYTE),
  DS_JUST_FIM_PREC           VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  DS_OBS_LIB_ISOLAMENTO      VARCHAR2(255 BYTE),
  DT_FIM_ACOMPANHAMENTO      DATE,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_RESPONSAVEL             VARCHAR2(10 BYTE),
  DT_ANALISE                 DATE,
  NM_USUARIO_ANALISE         VARCHAR2(15 BYTE),
  IE_ANALISE                 VARCHAR2(1 BYTE),
  NR_SEQ_ORIGEM              NUMBER(10),
  DS_JUSTIFICATIVA_ANALISE   VARCHAR2(255 BYTE),
  NR_SEQ_TRIAGEM             NUMBER(10),
  NR_ATEND_COPIA             NUMBER(10),
  NR_SEQ_FORMULARIO          NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPREC_ATEPACI_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPREC_CIHMICR_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(CD_MICROORGANISMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPREC_CIHMICR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPREC_CIHORIIN_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPREC_CIHPREC_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_PRECAUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPREC_CIHPREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPREC_CIHTOPO_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(CD_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPREC_CIHTOPO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPREC_EHRREGI_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPREC_MEDICO_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(CD_MEDICO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPREC_MOTEPRE_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_MOVTO_TERMINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPREC_MOTEPRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPREC_MOTISOL_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_MOTIVO_ISOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPREC_PESFISI_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPREC_PK ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPREC_TASASDI_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPREC_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPREC_TASASDI_FK2_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPREC_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPREC_TRPRAT_FK_I ON TASY.ATENDIMENTO_PRECAUCAO
(NR_SEQ_TRIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atendimento_precaucao_atual
before insert or update ON TASY.ATENDIMENTO_PRECAUCAO for each row
declare
nr_seq_tipo_ocorrencia_w		number(10);
ds_comentario_w				varchar2(2000);
quebra_w				varchar2(10)	:= chr(13)||chr(10);
vl_parametro_w				varchar2(10);
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

vl_parametro_w	:= substr(Obter_Valor_Param_Usuario(281, 1124, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),1,255);

if	(vl_parametro_w = 'N') and
	(:new.DT_INICIO is not null) and
	(:new.DT_TERMINO is not null) and
	(:new.DT_TERMINO < :new.DT_INICIO) then
	-- A data de fim nao pode ser maior que a data de inicio. Parametro [1124] #@#@
	Wheb_mensagem_pck.exibir_mensagem_abort(263494);
end if;


if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	select	max(nr_seq_tipo_ocorrencia)
	into	nr_seq_tipo_ocorrencia_w
	from	cih_precaucao
	where	nr_sequencia	= :new.NR_SEQ_PRECAUCAO;

	if	(nr_seq_tipo_ocorrencia_w	is not null) then
		ds_comentario_w	:= '';
		if	(:new.NR_SEQ_MOTIVO_ISOL is not null) then
			ds_comentario_w		:= ds_comentario_w||obter_desc_expressao(327458)/*'Motivo: '*/||substr(obter_descricao_padrao('MOTIVO_ISOLAMENTO','DS_MOTIVO',:new.NR_SEQ_MOTIVO_ISOL),1,255)||quebra_w;
		end if;

		if	(:new.DT_INICIO is not null) then
			ds_comentario_w		:= ds_comentario_w||obter_desc_expressao(648277)/*'Inicio: '*/||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.DT_INICIO, 'shortDate', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||quebra_w;
		end if;

		if	(:new.DT_TERMINO is not null) then
			ds_comentario_w		:= ds_comentario_w||obter_desc_expressao(729249)/*'Fim: '*/||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.DT_TERMINO, 'shortDate', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||quebra_w;
		end if;

		if	(:new.CD_TOPOGRAFIA is not null) then
			ds_comentario_w		:= ds_comentario_w||obter_desc_expressao(343115)/*'Topografia: '*/||substr(obter_cih_topografia(:new.cd_topografia),1,255)||quebra_w;
		end if;

		if	(:new.cd_microorganismo is not null) then
			ds_comentario_w		:= ds_comentario_w||obter_desc_expressao(728004, null)||' '||substr(obter_cih_microorganismo(:new.cd_microorganismo),1,255)||quebra_w;
		end if;

		insert into PACIENTE_OCORRENCIA (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_pessoa_fisica,
							nr_seq_tipo_ocorrencia,
							ie_intensidade,
							dt_registro,
							dt_liberacao,
							nm_usuario_liberacao,
							nr_atendimento,
							ds_comentario)
					values	(	paciente_ocorrencia_seq.nextval,
							sysdate,
							:new.nm_usuario,
							sysdate,
							:new.nm_usuario,
							obter_pessoa_Atendimento(:new.nr_Atendimento,'C'),
							nr_seq_tipo_ocorrencia_w,
							'N',
							nvl(:new.dt_registro,sysdate),
							:new.dt_liberacao,
							:new.nm_usuario,
							:new.nr_atendimento,
							ds_comentario_w);




	end if;

end if;

<<final>>
quebra_w := quebra_w;

end;
/


CREATE OR REPLACE TRIGGER TASY.atend_precaucao_pend_atual
after insert or update ON TASY.ATENDIMENTO_PRECAUCAO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_anamnese_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'CIH';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XCIH';
end if;
if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_PRECAUCAO_UPDATE
before update ON TASY.ATENDIMENTO_PRECAUCAO for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

ie_inativar_alerta_w		varchar2(1);
ie_finalizar_alerta_w		varchar2(1);
ie_precaucao_isolamento_w	varchar2(1);
ie_evolucao_lib_precaucao_w	varchar2(3);
ds_texto_w			varchar2(4000);
ds_retorno_new_w	varchar2(255)	:= null ;
ds_retorno_old_w	varchar2(255)	:= null ;

begin

if	(:old.nr_seq_motivo_isol <> :new.nr_seq_motivo_isol) then

	ds_retorno_new_w := obter_dados_motivo_isolamento(:new.nr_seq_motivo_isol, 'IM');
	ds_retorno_old_w := obter_dados_motivo_isolamento(:old.nr_seq_motivo_isol, 'IM');

	if (ds_retorno_new_w <> ds_retorno_old_w) then
		if (ds_retorno_new_w <> 'M') then
			delete from atend_precaucao_micro
			where nr_seq_atend_precaucao = :old.nr_sequencia;
		else
			:new.cd_microorganismo := null;
		end if;
	end if;
end if;

obter_param_usuario(44,168,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_inativar_alerta_w);
obter_param_usuario(281,1399,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_finalizar_alerta_w);
obter_param_usuario(281,1443,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_evolucao_lib_precaucao_w);

begin
	if      (ie_inativar_alerta_w = 'S') and
		(:new.dt_final_precaucao        is not null) and
		(:old.dt_final_precaucao is null)then

		update    atendimento_alerta
		set       dt_inativacao = :new.dt_final_precaucao,
			  nm_usuario_inativacao = :new.nm_usuario,
			  ie_situacao = 'I',
			  ds_justificativa = wheb_mensagem_pck.get_texto(307032, null) -- Liberado paciente do isolamento
		where     nr_seq_precaucao = :old.nr_sequencia
		and	  ie_situacao = 'A';

		update	alerta_paciente
		set	dt_inativacao = :new.dt_final_precaucao,
			nm_usuario_inativacao = :new.nm_usuario,
			ie_situacao = 'I',
			ds_justificativa = wheb_mensagem_pck.get_texto(307032, null) -- Liberado paciente do isolamento
		where	nr_seq_precaucao = :old.nr_sequencia
		and	ie_situacao = 'A';


	end if;

	if	(:new.dt_termino is not null) and
		(:old.dt_termino  is null) then

		if	(ie_finalizar_alerta_w = 'S') then
			update	atendimento_alerta
			set	dt_fim_alerta = :new.dt_termino
			where	nr_seq_precaucao = :old.nr_sequencia
			and	ie_situacao = 'A';

			update	alerta_paciente
			set	dt_fim_alerta = :new.dt_termino
			where	nr_seq_precaucao = :old.nr_sequencia
			and	ie_situacao = 'A';
		elsif	(ie_finalizar_alerta_w = 'I') then
			select	decode(count(*),0,'N','S')
			into	ie_precaucao_isolamento_w
			from	cih_precaucao
			where	nr_sequencia = :old.nr_seq_precaucao
			and	nvl(ie_isolamento,'N') = 'S';

			if	(ie_precaucao_isolamento_w = 'S') then
				update	atendimento_alerta
				set	dt_fim_alerta = :new.dt_termino
				where	nr_seq_precaucao = :old.nr_sequencia
				and	ie_situacao = 'A';

				update	alerta_paciente
				set	dt_fim_alerta = :new.dt_termino
				where	nr_seq_precaucao = :old.nr_sequencia
				and	ie_situacao = 'A';
			end if;
		elsif	(ie_finalizar_alerta_w = 'P') then
			select	decode(count(*),0,'N','S')
			into	ie_precaucao_isolamento_w
			from	cih_precaucao
			where	nr_sequencia = :old.nr_seq_precaucao
			and	nvl(ie_isolamento,'N') = 'S';

			if	(ie_precaucao_isolamento_w = 'N') then
				update	atendimento_alerta
				set	dt_fim_alerta = :new.dt_termino
				where	nr_seq_precaucao = :old.nr_sequencia
				and	ie_situacao = 'A';

				update	alerta_paciente
				set	dt_fim_alerta = :new.dt_termino
				where	nr_seq_precaucao = :old.nr_sequencia
				and	ie_situacao = 'A';
			end if;
		end if;
	end if;

	if 	((:new.dt_inativacao is not null) and
		(:old.dt_inativacao  is null)) then
		begin

		update    atendimento_alerta
		set       dt_inativacao = :new.dt_final_precaucao,
			  nm_usuario_inativacao = :new.nm_usuario,
			  ie_situacao = 'I',
			  ds_justificativa = wheb_mensagem_pck.get_texto(307032, null)
		where     nr_seq_precaucao = :old.nr_sequencia
		and	  ie_situacao = 'A';

		update    alerta_paciente
		set       dt_inativacao = :new.dt_final_precaucao,
			  nm_usuario_inativacao = :new.nm_usuario,
			  ie_situacao = 'I',
			  ds_justificativa = wheb_mensagem_pck.get_texto(307032, null)
		where     nr_seq_precaucao = :old.nr_sequencia
		and	  ie_situacao = 'A';

		if	(:new.dt_inicio is not null)	then
			:new.dt_termino := sysdate;
		end if;

		end;
	end if;

exception
	when others then
	null;
end;
-- � NECESS�RIO DEIXAR ESTA ROTINA NO FINAL POIS H� COMMIT

if	(ie_evolucao_lib_precaucao_w is not null) and
	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao  is null) then
	begin

	ds_texto_w := wheb_mensagem_pck.get_texto(307027, null) ||': ' ||  substr(obter_descricao_padrao('CIH_PRECAUCAO','DS_PRECAUCAO',:new.nr_seq_precaucao),1,255); -- Precau��o
	ds_texto_w := ds_texto_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(307029, null) || ': ' || substr(obter_descricao_padrao('MOTIVO_ISOLAMENTO','DS_MOTIVO',:new.nr_seq_motivo_isol),1,255); -- Motivo
	ds_texto_w := ds_texto_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(111691, null) || ': ' || :new.ds_observacao; --  Observa��o

	gerar_evolucao_precaucao(ie_evolucao_lib_precaucao_w, :new.nr_atendimento, ds_texto_w, :new.cd_medico_solic, :new.nm_usuario, :new.nr_sequencia);
	end;
end if;

commit;

end;
/


ALTER TABLE TASY.ATENDIMENTO_PRECAUCAO ADD (
  CONSTRAINT ATEPREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_PRECAUCAO ADD (
  CONSTRAINT ATEPREC_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPREC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPREC_CIHORIIN_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.CIH_ORIGEM_INFECCAO (NR_SEQUENCIA),
  CONSTRAINT ATEPREC_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEPREC_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEPREC_MOTISOL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ISOL) 
 REFERENCES TASY.MOTIVO_ISOLAMENTO (NR_SEQUENCIA),
  CONSTRAINT ATEPREC_CIHMICR_FK 
 FOREIGN KEY (CD_MICROORGANISMO) 
 REFERENCES TASY.CIH_MICROORGANISMO (CD_MICROORGANISMO),
  CONSTRAINT ATEPREC_CIHTOPO_FK 
 FOREIGN KEY (CD_TOPOGRAFIA) 
 REFERENCES TASY.CIH_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT ATEPREC_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_SOLIC) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT ATEPREC_MOTEPRE_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_TERMINO) 
 REFERENCES TASY.MOTIVO_TERMINO_PRECAUCAO (NR_SEQUENCIA),
  CONSTRAINT ATEPREC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEPREC_CIHPREC_FK 
 FOREIGN KEY (NR_SEQ_PRECAUCAO) 
 REFERENCES TASY.CIH_PRECAUCAO (NR_SEQUENCIA),
  CONSTRAINT ATEPREC_TRPRAT_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM) 
 REFERENCES TASY.TRIAGEM_PRONTO_ATEND (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_PRECAUCAO TO NIVEL_1;

