ALTER TABLE TASY.MOTIVO_ALT_VALOR_TIT_REC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_ALT_VALOR_TIT_REC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_ALT_VALOR_TIT_REC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_MOTIVO            VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MOTAVTR_PK ON TASY.MOTIVO_ALT_VALOR_TIT_REC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOTAVTR_TRAFINA_FK_I ON TASY.MOTIVO_ALT_VALOR_TIT_REC
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.movto_alt_delete_tit_rec
before delete ON TASY.MOTIVO_ALT_VALOR_TIT_REC for each row
declare
count_w	number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	count(*)
into	count_w
from	alteracao_valor a,
	titulo_receber b
where	cd_motivo	= :old.nr_sequencia
and	a.nr_titulo	= b.nr_titulo;


if	(count_w > 0 ) then

	wheb_mensagem_pck.exibir_mensagem_abort(373008);

end if;
end if;

end;
/


ALTER TABLE TASY.MOTIVO_ALT_VALOR_TIT_REC ADD (
  CONSTRAINT MOTAVTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOTIVO_ALT_VALOR_TIT_REC ADD (
  CONSTRAINT MOTAVTR_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MOTIVO_ALT_VALOR_TIT_REC TO NIVEL_1;

