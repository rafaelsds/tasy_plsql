ALTER TABLE TASY.CENTRO_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CENTRO_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CENTRO_CONTROLE
(
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_CENTRO_CONTROLE       NUMBER(8)            NOT NULL,
  DS_CENTRO_CONTROLE       VARCHAR2(80 BYTE)    NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_UNIDADE_MEDIDA        VARCHAR2(30 BYTE)    NOT NULL,
  IE_TIPO_CENTRO_CONTROLE  VARCHAR2(2 BYTE)     NOT NULL,
  CD_CENTRO_CONTROLE_PAI   NUMBER(8),
  IE_CENTRO_RESULTADO      VARCHAR2(1 BYTE),
  NM_PESSOA_RESP           VARCHAR2(40 BYTE),
  CD_CLASSIF_RESULT        NUMBER(3)            NOT NULL,
  NR_SEQ_UNID_NEG          NUMBER(10),
  IE_RECEB_DISTRIB         VARCHAR2(1 BYTE),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CENCONT_CENCONT_FK_I ON TASY.CENTRO_CONTROLE
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_PAI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CENCONT_CLARESU_FK_I ON TASY.CENTRO_CONTROLE
(CD_CLASSIF_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CENCONT_ESTABEL_FK_I ON TASY.CENTRO_CONTROLE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CENCONT_PK ON TASY.CENTRO_CONTROLE
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CENCONT_UNIMEDI_FK_I ON TASY.CENTRO_CONTROLE
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CENCONT_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CENCONT_UNINEGO_FK_I ON TASY.CENTRO_CONTROLE
(NR_SEQ_UNID_NEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CENCONT_UNINEGO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CENTRO_CONTROLE ADD (
  CONSTRAINT CENCONT_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CENTRO_CONTROLE ADD (
  CONSTRAINT CENCONT_UNINEGO_FK 
 FOREIGN KEY (NR_SEQ_UNID_NEG) 
 REFERENCES TASY.UNIDADE_NEGOCIO (NR_SEQUENCIA),
  CONSTRAINT CENCONT_CENCONT_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_PAI) 
 REFERENCES TASY.CENTRO_CONTROLE (CD_ESTABELECIMENTO,CD_CENTRO_CONTROLE),
  CONSTRAINT CENCONT_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT CENCONT_CLARESU_FK 
 FOREIGN KEY (CD_CLASSIF_RESULT) 
 REFERENCES TASY.CLASSIF_RESULT (CD_CLASSIFICACAO),
  CONSTRAINT CENCONT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CENTRO_CONTROLE TO NIVEL_1;

