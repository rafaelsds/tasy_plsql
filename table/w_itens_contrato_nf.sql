ALTER TABLE TASY.W_ITENS_CONTRATO_NF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ITENS_CONTRATO_NF CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ITENS_CONTRATO_NF
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO        NUMBER(10)             NOT NULL,
  NR_SEQ_REGRA_CONTRATO  NUMBER(10)             NOT NULL,
  CD_MATERIAL            NUMBER(6)              NOT NULL,
  NR_SEQ_NOTA            NUMBER(10)             NOT NULL,
  QT_CONTRATO            NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WITCONF_CONRENF_FK_I ON TASY.W_ITENS_CONTRATO_NF
(NR_SEQ_REGRA_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITCONF_CONRENF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WITCONF_CONTRAT_FK_I ON TASY.W_ITENS_CONTRATO_NF
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITCONF_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WITCONF_NOTFISC_FK_I ON TASY.W_ITENS_CONTRATO_NF
(NR_SEQ_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITCONF_NOTFISC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WITCONF_PK ON TASY.W_ITENS_CONTRATO_NF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WITCONF_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_ITENS_CONTRATO_NF ADD (
  CONSTRAINT WITCONF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_ITENS_CONTRATO_NF ADD (
  CONSTRAINT WITCONF_CONRENF_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTRATO) 
 REFERENCES TASY.CONTRATO_REGRA_NF (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WITCONF_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WITCONF_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_ITENS_CONTRATO_NF TO NIVEL_1;

