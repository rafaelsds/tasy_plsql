ALTER TABLE TASY.USUARIO_COMPUTADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_COMPUTADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_COMPUTADOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_USUARIO_PARAM     VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_COMPUTADOR    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_PERMITE_ACESSO    VARCHAR2(1 BYTE),
  DT_INICIO_ACESSO     DATE,
  DT_FIM_ACESSO        DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.USUCOMP_COMPUTA_FK_I ON TASY.USUARIO_COMPUTADOR
(NR_SEQ_COMPUTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUCOMP_PK ON TASY.USUARIO_COMPUTADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.USUARIO_COMPUTADOR_tp  after update ON TASY.USUARIO_COMPUTADOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_USUARIO_PARAM,1,4000),substr(:new.NM_USUARIO_PARAM,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_PARAM',ie_log_w,ds_w,'USUARIO_COMPUTADOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.USUARIO_COMPUTADOR ADD (
  CONSTRAINT USUCOMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_COMPUTADOR ADD (
  CONSTRAINT USUCOMP_COMPUTA_FK 
 FOREIGN KEY (NR_SEQ_COMPUTADOR) 
 REFERENCES TASY.COMPUTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.USUARIO_COMPUTADOR TO NIVEL_1;

