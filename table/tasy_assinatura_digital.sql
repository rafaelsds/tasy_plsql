ALTER TABLE TASY.TASY_ASSINATURA_DIGITAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_ASSINATURA_DIGITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_ASSINATURA_DIGITAL
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DS_ASSINATURA               VARCHAR2(100 BYTE) NOT NULL,
  CD_PESSOA_FISICA            VARCHAR2(20 BYTE) NOT NULL,
  DS_HASH_ASSINATURA          VARCHAR2(255 BYTE),
  DT_REGISTRO                 DATE              NOT NULL,
  DT_ASSINATURA               DATE,
  NR_SEQ_PROJ_ASS             NUMBER(10)        NOT NULL,
  NR_SEQ_PROJ_XML             NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_CLASSIF_RELAT            VARCHAR2(4 BYTE),
  CD_RELATORIO                NUMBER(5),
  NR_SEQ_ASSINATURA_ANTERIOR  NUMBER(10),
  DS_UTC                      VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO            VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO          VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TASASDI_I1 ON TASY.TASY_ASSINATURA_DIGITAL
(DT_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TASASDI_I1
  MONITORING USAGE;


CREATE INDEX TASY.TASASDI_PESFISI_FK_I ON TASY.TASY_ASSINATURA_DIGITAL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASASDI_PK ON TASY.TASY_ASSINATURA_DIGITAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASASDI_TASPRAS_FK_I ON TASY.TASY_ASSINATURA_DIGITAL
(NR_SEQ_PROJ_ASS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASASDI_XMLPROJ_FK_I ON TASY.TASY_ASSINATURA_DIGITAL
(NR_SEQ_PROJ_XML)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TASY_ASSINATURA_DIGITAL_tp  after update ON TASY.TASY_ASSINATURA_DIGITAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TASY_ASSINATURA_DIGITAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROJ_XML,1,4000),substr(:new.NR_SEQ_PROJ_XML,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJ_XML',ie_log_w,ds_w,'TASY_ASSINATURA_DIGITAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROJ_ASS,1,4000),substr(:new.NR_SEQ_PROJ_ASS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJ_ASS',ie_log_w,ds_w,'TASY_ASSINATURA_DIGITAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ASSINATURA,1,4000),substr(:new.DT_ASSINATURA,1,4000),:new.nm_usuario,nr_seq_w,'DT_ASSINATURA',ie_log_w,ds_w,'TASY_ASSINATURA_DIGITAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.TASY_ASSINATURA_DIGITAL_ATUAL

before insert or update ON TASY.TASY_ASSINATURA_DIGITAL 
for each row
declare

cd_certificado_w	varchar2(255);

nr_sequencia_w		number(10);

nr_atendimento_w	number(10);

cd_pessoa_fisica_w	varchar2(10);

nr_seq_item_pront_w	number(10);

ie_tipo_w		number(10);

cd_tipo_registro_w	varchar2(255);

ie_tipo_avaliacao_w varchar2(2);
ie_tipo_paquimetria_w	oft_paquimetria.ie_tipo_paquimetria%type;
ie_tipo_registro_w      oft_angio_retino.ie_tipo_registro%type;
nr_parecer_w	parecer_medico.nr_parecer%type;

begin

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
  goto Final;
end if;

if	(:new.dt_assinatura is not null) and

	(:old.dt_assinatura is null)then


	update tasy_assinat_dig_pend
	set dt_assinatura = :new.dt_assinatura
	where nr_seq_assinatura = :new.nr_sequencia;

	if	(:new.NR_SEQ_PROJ_ASS in  (36673,36671,52448,52449)) then --Perdas e ganhos



		if	(:new.NR_SEQ_PROJ_ASS	IN( 36671,52449)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_perda_ganho

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_perda_ganho

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	ATENDIMENTO_PERDA_GANHO

			where	nr_sequencia	= nr_sequencia_W;





			Gerar_registro_pendente_PEP('XGP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

		elsif	(:new.NR_SEQ_PROJ_ASS in  (50152, 50153)) then --Feridas



		if	(:new.NR_SEQ_PROJ_ASS	= 50152) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cur_ferida

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cur_ferida

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then





			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	cur_ferida

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XCR', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;


		elsif	(:new.NR_SEQ_PROJ_ASS in  (50492, 50493)) then --Evolucao Curativos



		if	(:new.NR_SEQ_PROJ_ASS	= 50492) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cur_evolucao

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cur_evolucao

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then




			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	cur_evolucao

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XCE', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;


	elsif	(:new.NR_SEQ_PROJ_ASS in  (50658,50654,51850,51849)) then --Aldrete



		if	(:new.NR_SEQ_PROJ_ASS	in ( 50658,51849)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_aldrete

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_aldrete

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	escala_aldrete

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XAD', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (50613,60615,52071,52069)) then --Interna



		if	(:new.NR_SEQ_PROJ_ASS	in (50613,52071)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_aval_srpa

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_aval_srpa

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	atend_aval_srpa

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XLAVAL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;
	elsif (:new.NR_SEQ_PROJ_ASS in (51950,51951)) then --Equipamento
					if (:new.NR_SEQ_PROJ_ASS    = 51951 ) then
						select max(nr_sequencia)
						into nr_sequencia_w
						from equipamento_cirurgia
						where nr_seq_assinatura = :new.nr_sequencia;
					else
						select max(nr_sequencia)
						into nr_sequencia_w
						from equipamento_cirurgia
						where NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;
					end if;
					if (nr_sequencia_w is not null) then
						select max(nr_atendimento),
							max(obter_pessoa_atendimento(nr_atendimento,'C'))
						into nr_atendimento_w,
							cd_pessoa_fisica_w
						from equipamento_cirurgia
						where nr_sequencia = nr_sequencia_W;
						Gerar_registro_pendente_PEP('XEFAN', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
					end if;

   elsif (:new.NR_SEQ_PROJ_ASS in (51928,51929,53169,53170,50752,50753)) then --Eventos
					if (:new.NR_SEQ_PROJ_ASS    in (51928,53169,50752) ) then
						select max(nr_sequencia)
						into nr_sequencia_w
						from qua_evento_paciente
						where nr_seq_assinatura = :new.nr_sequencia;
					else
						select max(nr_sequencia)
						into nr_sequencia_w
						from qua_evento_paciente
						where NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;
					end if;
					if (nr_sequencia_w is not null) then
						select max(nr_atendimento),
							max(obter_pessoa_atendimento(nr_atendimento,'C'))
						into nr_atendimento_w,
							cd_pessoa_fisica_w
						from qua_evento_paciente
						where nr_sequencia = nr_sequencia_W;
						Gerar_registro_pendente_PEP('XEVT', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
					end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (52168,52169)) then --Escala Chung



		if	(:new.NR_SEQ_PROJ_ASS	= 52169) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_chung

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_chung

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



	if	(nr_sequencia_w	is not null) then


			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	escala_chung  p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XESCCC', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

	end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (52130,52129)) then --Escala Possum



		if	(:new.NR_SEQ_PROJ_ASS	= 52129) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_possum

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_possum

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



	if	(nr_sequencia_w	is not null) then


			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	escala_possum  p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XESP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

	end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (52513, 52512)) then --Escala Lee



		if	(:new.NR_SEQ_PROJ_ASS	= 52512) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	escala_lee
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	escala_lee
			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			Gerar_registro_pendente_PEP('XAEL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;


	elsif	(:new.NR_SEQ_PROJ_ASS in  (50333,50332)) then --Posicao



		if	(:new.NR_SEQ_PROJ_ASS	= 50332) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	posicao_cirurgia

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	posicao_cirurgia

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(c.nr_atendimento),

				max(c.cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	posicao_cirurgia p,

				cirurgia c

			where	p.nr_sequencia	= nr_sequencia_W

			and	p.nr_cirurgia	= c.nr_cirurgia;



			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	posicao_cirurgia p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XPO', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w,:NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (50334,50335)) then --Pele



		if	(:new.NR_SEQ_PROJ_ASS	= 50334) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pepo_pele

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pepo_pele

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(c.nr_atendimento),

				max(c.cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	pepo_pele p,

				cirurgia c

			where	p.nr_sequencia	= nr_sequencia_W

			and	p.nr_cirurgia	= c.nr_cirurgia;



			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	pepo_pele p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XPL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w,:NEW.NM_USUARIO,'A');

		end if;


	elsif	(:new.NR_SEQ_PROJ_ASS in  (52008,52010)) then --Fanep Participante



		if	(:new.NR_SEQ_PROJ_ASS	= 52010) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pepo_participante

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pepo_participante

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then


			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	pepo_participante p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;

			Gerar_registro_pendente_PEP('XPP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w,:NEW.NM_USUARIO,'A');

		end if;




	elsif	(:new.NR_SEQ_PROJ_ASS in  (50336,50337)) then --Incisao



		if	(:new.NR_SEQ_PROJ_ASS	= 50336) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	incisao_cirurgia

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	incisao_cirurgia

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(c.nr_atendimento),

				max(c.cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	incisao_cirurgia p,

				cirurgia c

			where	p.nr_sequencia	= nr_sequencia_W

			and	p.nr_cirurgia	= c.nr_cirurgia;



			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	incisao_cirurgia p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XIC', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w,:NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (50338,50339,52048,52049)) then --Curativo



		if	(:new.NR_SEQ_PROJ_ASS	in (50338,52049)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pepo_curativo

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pepo_curativo

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(c.nr_atendimento),

				max(c.cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	pepo_curativo p,

				cirurgia c

			where	p.nr_sequencia	= nr_sequencia_W

			and	p.nr_cirurgia	= c.nr_cirurgia;



			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	pepo_curativo p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XCR', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w,:NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (50952,50953)) then --Conjunto



		if	(:new.NR_SEQ_PROJ_ASS	= 50952) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cm_conjunto_cont

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cm_conjunto_cont

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	cm_conjunto_cont

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XCJ', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (44466,44467,52388,52389)) then --Tecnica Anestesica



		if	(:new.NR_SEQ_PROJ_ASS	in( 44466,52389)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cirurgia_tec_anestesica

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	cirurgia_tec_anestesica

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(c.nr_atendimento),

				max(c.cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	cirurgia_tec_anestesica t,

				cirurgia c

			where	t.nr_sequencia	= nr_sequencia_W

			and	t.nr_cirurgia	= c.nr_cirurgia;



			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	cirurgia_tec_anestesica t,

					pepo_cirurgia c

				where	t.nr_sequencia	= nr_sequencia_W

				and	t.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XCTA', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in (44469,44470, 52368, 52369)) then --Descricao Anestesica



		if	(:new.NR_SEQ_PROJ_ASS	in (44469, 52368)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	anestesia_descricao

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	anestesia_descricao

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(c.nr_atendimento),

				max(c.cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	anestesia_descricao d,

				cirurgia c

			where	d.nr_sequencia	= nr_sequencia_W

			and	d.nr_cirurgia	= c.nr_cirurgia;



			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	anestesia_descricao d,

					pepo_cirurgia c

				where	d.nr_sequencia	= nr_sequencia_W

				and	d.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XCDA', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (50672,50673,50718,50719,52150,52149,53230,53231)) then --Avaliacoes



		if	(:new.NR_SEQ_PROJ_ASS in (50672,50718,52149,53230)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	med_avaliacao_paciente

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	med_avaliacao_paciente

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	med_avaliacao_paciente

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XAV', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in (	51095,51096, 	-- Medicamentos

						51097,51098,	-- Terapia hidroeletrolitica

						51092,51094,	-- Agentes anestesicos

						51099,51100,	-- Materiais

						52229,52228,    -- Agentes anestesicos Fanep

						52289,52288,    -- Medicamentos Fanep

						52349,52348,    -- Materiais Fanep

						52308,52309,    -- Terapia hidroeletrolitica

						52208,52209, 	-- Hemocomponentes Fanep
						51101,51102 	-- Hemocomponentes PEPO

						)) then -- Hemocomponentes



		if	(:new.NR_SEQ_PROJ_ASS	in (51095,51097,51092,51099,51101,52228,52288,52348,52309,52209)) then

			select	max(nr_sequencia),

				max(ie_tipo)

			into	nr_sequencia_w,

				ie_tipo_w

			from	cirurgia_agente_anestesico

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia),

				max(ie_tipo)

			into	nr_sequencia_w,

				ie_tipo_w

			from	cirurgia_agente_anestesico

			where	nr_seq_assinat_inativacao = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	cirurgia a,

				cirurgia_agente_anestesico b

			where	a.nr_cirurgia 	= b.nr_cirurgia

			and	b.nr_sequencia 	= nr_sequencia_w;



			if	(ie_tipo_w = 1) then

				cd_tipo_registro_w :=	'XCAAA';

			elsif	(ie_tipo_w = 2) then

				cd_tipo_registro_w :=	'XCAAT';

			elsif	(ie_tipo_w = 3) then

				cd_tipo_registro_w := 	'XCAAM';

			elsif	(ie_tipo_w = 4) then

				cd_tipo_registro_w :=	'XCAAMAT';

			elsif	(ie_tipo_w = 5) then

				cd_tipo_registro_w := 	'XCAAH';

			end if;



			Gerar_registro_pendente_PEP(cd_tipo_registro_w, nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (183, 25230,53244,53245)) then --Evolucao



		if	(:new.NR_SEQ_PROJ_ASS	in (183,53244)) then

			select	max(cd_evolucao)

			into	nr_sequencia_w

			from	evolucao_paciente

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(cd_evolucao)

			into	nr_sequencia_w

			from	evolucao_paciente

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then





			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	evolucao_paciente

			where	cd_evolucao	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XE', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif (:new.NR_SEQ_PROJ_ASS in (51590,51608)) then -- Anotacao Quimioterapia



		if (:new.nr_seq_proj_ass = 51590) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_atend_anot

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_atend_anot

			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;



		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(b.nr_atendimento),

				max(obter_codigo_paciente_setor(b.nr_seq_paciente))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_atend_anot a,

				paciente_atendimento b

			where	a.nr_seq_atendimento	= b.nr_seq_atendimento

			and	a.nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XQLAN', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif (:new.NR_SEQ_PROJ_ASS in (51648)) then -- Medicacao Quimioterapia



		if (:new.nr_seq_proj_ass = 51648) then

			select	max(nr_seq_interno)

			into	nr_sequencia_w

			from	paciente_atend_medic

			where	nr_seq_assinatura	= :new.nr_sequencia;



		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(b.nr_atendimento),

				max(obter_codigo_paciente_setor(b.nr_seq_paciente))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_atend_medic a,

				paciente_atendimento b

			where	a.nr_seq_atendimento	= b.nr_seq_atendimento

			and	a.nr_seq_interno	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XQLM', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif (:new.NR_SEQ_PROJ_ASS in (51631, 51630)) then -- Intercorrencia Quimioterapia



		if (:new.nr_seq_proj_ass = 51630) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_atend_interc

			where	nr_seq_assinatura	= :new.nr_sequencia;



		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(b.nr_atendimento),

				max(obter_codigo_paciente_setor(b.nr_seq_paciente))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_atend_interc a,

				paciente_atendimento b

			where	a.nr_seq_atendimento	= b.nr_seq_atendimento

			and	a.nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XQLI', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (184, 25231,52428,52429,52548,52549)) then



		if	(:new.NR_SEQ_PROJ_ASS	in( 184,52429,52548)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_sinal_vital

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_sinal_vital

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	atendimento_sinal_vital

			where	nr_sequencia	= nr_sequencia_W;





			Gerar_registro_pendente_PEP('XSV', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (25170, 25242)) then



		if	(:new.NR_SEQ_PROJ_ASS	in(25170)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_anal_bioq_port

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_anal_bioq_port

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	atend_anal_bioq_port

			where	nr_sequencia	= nr_sequencia_W;





			Gerar_registro_pendente_PEP('XABP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (25184, 25240)) then



		if	(:new.NR_SEQ_PROJ_ASS	= 25184) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_monit_hemod

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_monit_hemod

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

					max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

					cd_pessoa_fisica_w

			from	atend_monit_hemod

			where	nr_sequencia	= nr_sequencia_W;





			Gerar_registro_pendente_PEP('XSVMH', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (25185, 25239,52408,52409, 52650, 52651)) then



		if	(:new.NR_SEQ_PROJ_ASS	in( 25185,52408, 52650)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_monit_resp

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_monit_resp

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

					max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

					cd_pessoa_fisica_w

			from	atendimento_monit_resp

			where	nr_sequencia	= nr_sequencia_W;





			Gerar_registro_pendente_PEP('XSVMR', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (25290,25291,50726,50727,50716,50715, 52703, 52704)) then



		if	(:new.NR_SEQ_PROJ_ASS	in (25290,50726,50715, 52703)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pe_prescricao

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pe_prescricao

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	pe_prescricao

			where	nr_sequencia	= nr_sequencia_W;





			Gerar_registro_pendente_PEP('XSAE', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in (50215, 51730)) then

		select	max(nr_prescricao)

		into	nr_sequencia_w

		from	prescr_medica

		where	nr_seq_assinatura_enf	= :new.nr_sequencia;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	prescr_medica

			where	nr_prescricao	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XREP', nr_sequencia_w,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in (1, 25272, 51729)) then



		select	max(nr_prescricao)

		into	nr_sequencia_w

		from	prescr_medica

		where	nr_seq_assinatura	= :new.nr_sequencia;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	prescr_medica

			where	nr_prescricao	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XREP', nr_sequencia_w,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.nr_seq_proj_ass in (25173,25232, 52688, 52689)) then

		begin



		if	(:new.nr_seq_proj_ass	IN(25173, 52688)) then

			select	max(nr_seq_interno)

			into	nr_sequencia_w

			from	diagnostico_doenca

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_seq_interno)

			into	nr_sequencia_w

			from	diagnostico_doenca

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	diagnostico_doenca

			where	nr_seq_interno	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XD', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;







		end;

	elsif	(:new.nr_seq_proj_ass in (25172,25233,53224,53225)) then

		begin



		if	(:new.nr_seq_proj_ass	in (25172,53224)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	anamnese_paciente

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	anamnese_paciente

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	anamnese_paciente

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XAP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;







		end;
	elsif	(:new.nr_seq_proj_ass in (53658, 53659)) then -- Alertas do paciente

		begin



		if	(:new.nr_seq_proj_ass	IN (53658)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ALERTA_PACIENTE

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ALERTA_PACIENTE

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;

		Gerar_registro_pendente_PEP('XALP', nr_sequencia_w, cd_pessoa_fisica_w, null, :new.nm_usuario, 'A');

		end;

	elsif	(:new.nr_seq_proj_ass in (53662, 53663)) then -- Alertas do atencimento

		begin



		if	(:new.nr_seq_proj_ass	IN (53662)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ATENDIMENTO_ALERTA

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ATENDIMENTO_ALERTA

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				substr(obter_pessoa_atendimento(max(nr_atendimento),'C'),1,255)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	ATENDIMENTO_ALERTA

			where	nr_sequencia = nr_sequencia_w;



			Gerar_registro_pendente_PEP('XALA', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO, 'A');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (51454,51455,50814,50815)) then -- Atestado

		begin



		if	(:new.nr_seq_proj_ass	IN (51454,50814)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atestado_paciente

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atestado_paciente

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	atestado_paciente

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XAT', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;







		end;

	elsif	(:new.nr_seq_proj_ass in (25175,25235, 52550, 52551)) then -- Historico de saude - Alergia / Reacoes adversas

		begin



		if	(:new.nr_seq_proj_ass	IN (25175, 52550)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_alergia

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_alergia

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_alergia

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSAL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25177,25234, 52570, 52571, 52690, 52691)) then -- Historico de saude - Doencas previas e atuais

		begin



		if	(:new.nr_seq_proj_ass	IN (25177, 52570, 52690)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_antec_clinico

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_antec_clinico

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_antec_clinico

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSAC', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25180,25237, 52572, 52573)) then -- Historico de saude - Medicamentos em uso

		begin



		if	(:new.nr_seq_proj_ass	IN (25180, 52572)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_medic_uso

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_medic_uso

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_medic_uso

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSMU', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25178,25236, 52574, 52575)) then -- Historico de saude - Habitos

		begin



		if	(:new.nr_seq_proj_ass	IN (25178, 52574)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_habito_vicio

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_habito_vicio

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_habito_vicio

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSHV', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25176,25238,52552,52553)) then -- Historico de saude - Cirugias

		begin



		if	(:new.nr_seq_proj_ass in (25176,52552)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_cirurgia

			where	nr_seq_assinatura = :new.nr_sequencia;

		elsif (:new.nr_seq_proj_ass in (25238,52553)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_cirurgia

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;


		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	historico_saude_cirurgia

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSCI', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25181,25244, 52576, 52577)) then -- Historico de saude - Ocorrencias

		begin



		if	(:new.nr_seq_proj_ass	IN(25181, 52576)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_ocorrencia

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_ocorrencia

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_ocorrencia

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSO', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25174,25246, 52554, 52555)) then -- Historico de saude - Acessorio/ortese/Protese

		begin



		if	(:new.nr_seq_proj_ass	IN (25174, 52554)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_acessorio

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_acessorio

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	paciente_acessorio

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSAS', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25179,25247)) then -- Historico de saude - Historia Familiar

		begin



		if	(:new.nr_seq_proj_ass	= 25179) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_antec_clinico

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_antec_clinico

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_antec_clinico

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSAM', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51488,51489, 52696, 52697)) then -- Historico de saude - Internacoes

		begin



		if	(:new.nr_seq_proj_ass	IN(51488, 52696)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_internacao

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_internacao

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	historico_saude_internacao

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSI', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51490,51491, 52692, 52693)) then -- Historico de saude - Tratamento anteriores

		begin



		if	(:new.nr_seq_proj_ass	IN(51490, 52692)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_tratamento

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_tratamento

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	historico_saude_tratamento

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHST', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51492,51493, 52709, 52710)) then -- Historico de saude - Soc/Econ/Cult/Out hab

		begin



		if	(:new.nr_seq_proj_ass	IN(51492, 52709)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_hist_social

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_hist_social

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	paciente_hist_social

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSS', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51494,51495, 52700, 52701)) then -- Historico de saude - Vacina

		begin



		if	(:new.nr_seq_proj_ass	IN(51494, 52700)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_vacina

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_vacina

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_pessoa_fisica)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	paciente_vacina

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSV', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51496,51497, 52694, 52695)) then -- Historico de saude - Deficiencia paciente

		begin



		if	(:new.nr_seq_proj_ass	IN(51496, 52694)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pf_tipo_deficiencia

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pf_tipo_deficiencia

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	pf_tipo_deficiencia

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSD', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51498)) then -- Historico de saude - Declaracao saude

		begin



		if	(:new.nr_seq_proj_ass	= 51498) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	pls_declaracao_segurado

			where	nr_seq_assinatura = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	pls_declaracao_segurado

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSDS', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51508,49779, 52705, 52706)) then -- Historico de saude - Exames do paciente

		begin



		if	(:new.nr_seq_proj_ass	IN(51508, 52705)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_exame

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_exame

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	paciente_exame

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSP', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (49782, 51688, 52707, 52708)) then -- Historico de saude - Transfusao

		begin


		if	(:new.nr_seq_proj_ass	IN(49782, 52708)) then


			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_transfusao

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		else




			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	paciente_transfusao

			where	NR_SEQ_ASSINATURA = :new.nr_sequencia;


		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	paciente_transfusao

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSTR', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51501,51502, 52698, 52699)) then -- Historico de saude - Saude de mulher

		begin



		if	(:new.nr_seq_proj_ass	IN(51501, 52698)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_mulher

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	historico_saude_mulher

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	historico_saude_mulher

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XHSM', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (25187,25248,52188,52189)) then

		begin



		if	(:new.nr_seq_proj_ass	in( 25187,52189)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ehr_registro

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ehr_registro

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(cd_paciente),

				max(nr_seq_item_pront)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w,

				nr_seq_item_pront_w

			from	ehr_registro

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XEHR', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A',
				null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_seq_item_pront_w);

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (50721,50722)) then

		begin

		if	(:new.nr_seq_proj_ass	= 50721) then

			select	max(nr_parecer)

			into	nr_sequencia_w

			from	PARECER_MEDICO_REQ

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_parecer)

			into	nr_sequencia_w

			from	PARECER_MEDICO_REQ

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(CD_PESSOA_FISICA )

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	PARECER_MEDICO_REQ

			where	nr_parecer	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XPM', nr_sequencia_w, cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (50723,50724)) then

		begin

		if	(:new.nr_seq_proj_ass	= 50723) then

			select	max(nr_parecer),

					max(nr_sequencia)

			into	nr_parecer_w,

					nr_sequencia_w

			from	PARECER_MEDICO

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_parecer),

					max(nr_sequencia)

			into	nr_parecer_w,

					nr_sequencia_w

			from	PARECER_MEDICO

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_parecer_w	is not null) then



			select	max(b.nr_atendimento),

				max(a.cd_medico)

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	PARECER_MEDICO a, PARECER_MEDICO_REQ b

			where	a.nr_parecer = b.nr_parecer

			and		a.nr_parecer = nr_parecer_w;

			Gerar_registro_pendente_PEP('XRP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A', null, null, null, null, null, null, null, null, null,
			null, null, null, null, null, null, nr_parecer_w);

		end if;

		end;


	elsif (:new.nr_seq_proj_ass in (25183,25241,51908,51909)) then

		begin

			if	(:new.nr_seq_proj_ass	in( 25183,51909)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ATEND_AVAL_ANALGESIA

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ATEND_AVAL_ANALGESIA

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento)

			into	nr_atendimento_w

			from	ATEND_AVAL_ANALGESIA

			where	nr_sequencia	= nr_sequencia_w;



			select 	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	atendimento_paciente

			where	nr_atendimento 	=	nr_atendimento_w;



			Gerar_registro_pendente_PEP('XSVMA', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;





		end;

	elsif	(:new.nr_seq_proj_ass in (50172,50173,50833,50834)) then -- Receita

		begin



		if	(:new.nr_seq_proj_ass	IN (50172,50833)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	med_receita

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	med_receita

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	med_receita

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XREC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51550,51552,51551)) then -- Receita ambulatorial

		begin



		if	(:new.nr_seq_proj_ass	= 51550) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	fa_receita_farmacia

			where	nr_seq_assinatura = :new.nr_sequencia;

		elsif	(:new.nr_seq_proj_ass	= 51552) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	fa_receita_farmacia

			where	nr_seq_assinatura_parcial = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	fa_receita_farmacia

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	fa_receita_farmacia

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XRECAMB', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51418,51419)) then -- Escala Ansiedade e depressao

		begin



		if	(:new.nr_seq_proj_ass	= 51418) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_ansiedade_depressao

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_ansiedade_depressao

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_ansiedade_depressao

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'41');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51172,51173)) then -- Escala Aval Nutric Subj Global (Detsky)// Aval Nutric Subj Global (Ottery)

		begin





		if	(:new.nr_seq_proj_ass	= 51172) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	aval_nutric_subjetiva

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	aval_nutric_subjetiva

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C')),

					max(ie_tipo_avaliacao)

			into	cd_pessoa_fisica_w,

					ie_tipo_avaliacao_w

			from	aval_nutric_subjetiva

			where	nr_sequencia	= nr_sequencia_w;



			if	(upper(ie_tipo_avaliacao_w) = 'O') then

				Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'39');

			else

				Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'23');

			end if;

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51348,51349)) then -- Escala Braden

		begin



		if	(:new.nr_seq_proj_ass	= 51348) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_escala_braden

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_escala_braden

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	atend_escala_braden

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'2');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (53597, 53595)) then -- Escala IMPROVE Bleeding

		begin

		if	(:new.nr_seq_proj_ass	= 53597) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ESCALA_IMPROVE_BLEEDING

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ESCALA_IMPROVE_BLEEDING

			where	NR_SEQ_ASSINATURA_INATIVACAO = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	ESCALA_IMPROVE_BLEEDING

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '240');

		end if;

		end;

		elsif	(:new.nr_seq_proj_ass in (53599, 53598)) then -- Escala Padua

		begin

		if	(:new.nr_seq_proj_ass	= 53599) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_padua

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_padua

			where	NR_SEQ_ASSINATURA_INATIVACAO = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_padua

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '241');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53604, 53602)) then -- Escala WAKE

		begin

		if	(:new.nr_seq_proj_ass	= 53604) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ESCALA_WAKE

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ESCALA_WAKE

			where	NR_SEQ_ASSINATURA_INATIVACAO = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	ESCALA_WAKE

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '242');

		end if;

		end;

		elsif	(:new.nr_seq_proj_ass in (53605, 53603)) then -- Escala MRC

		begin

		if	(:new.nr_seq_proj_ass	= 53605) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_mrc

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_mrc

			where	NR_SEQ_ASSINATURA_INATIVACAO = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_mrc

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '243');

		end if;

		end;

  elsif	(:new.nr_seq_proj_ass in (53606, 53607)) then -- Escala Pittsburg Brain Stem Score

		begin

		if	(:new.nr_seq_proj_ass	= 53606) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_pbss

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_pbss

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_pbss

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '244');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53609, 53608)) then -- Escala Revised Geneva

		begin

		if	(:new.nr_seq_proj_ass	= 53609) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_rev_geneva

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_rev_geneva

			where	nr_seq_assinat_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_rev_geneva

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '246');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53622, 53621)) then -- Thompson Score

		begin

		if	(:new.nr_seq_proj_ass	= 53622) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_thompson

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_thompson

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_thompson

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '247');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53613, 53612)) then -- Escala NPass

		begin

		if	(:new.nr_seq_proj_ass	= 53613) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_npass

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_npass

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_npass

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '248');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53616, 53617)) then -- Escala Kuss

		begin

		if	(:new.nr_seq_proj_ass	= 53616) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_kuss

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_kuss

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_kuss

			where	nr_sequencia = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '249');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53610, 53611)) then -- Escala Atice

		begin

		if	(:new.nr_seq_proj_ass	= 53610) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_atice

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_atice

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_atice

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '245');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53615, 53614)) then -- Escala Minds

		begin

		if	(:new.nr_seq_proj_ass	= 53615) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_minds

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_minds

			where	nr_seq_assinat_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_minds

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '250');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53619, 53618)) then -- Escala Balthazar

		begin

		if	(:new.nr_seq_proj_ass	= 53619) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_balthazar

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_balthazar

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_balthazar

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '251');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53625, 53623)) then -- Escala SAD PERSONS

		begin

		if	(:new.nr_seq_proj_ass	= 53625) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sad

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sad

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_sad

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '253');

		end if;

		end;

	elsif	(:new.nr_seq_proj_ass in (53624, 53624)) then -- Escala Neonatal Abstinence

		begin

		if	(:new.nr_seq_proj_ass	= 53624) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sad

			where	nr_seq_assinatura = :new.nr_sequencia;


		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sad

			where	nr_seq_assinatura_inativacao = :new.nr_sequencia;

		end if;

		if	(nr_sequencia_w	is not null) then

			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	escala_sad

			where	nr_sequencia	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w, cd_pessoa_fisica_w, null, :NEW.NM_USUARIO, 'A', null, null, null, null, null, '252');

		end if;

		end;


	elsif	(:new.nr_seq_proj_ass in (53476,53477)) then -- Escala News

		begin



		if	(:new.nr_seq_proj_ass	= 53476) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ESCALA_NEWS

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ESCALA_NEWS

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_profissional)

			into	cd_pessoa_fisica_w

			from	ESCALA_NEWS

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'183');

		end if;



		end;



	elsif	(:new.nr_seq_proj_ass in (51350,51351)) then -- Escala Braden Q

		begin



		if	(:new.nr_seq_proj_ass	= 51350) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_escala_braden_q

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_escala_braden_q

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	atend_escala_braden_q

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'67');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51352,51353)) then -- Escala Complexidade assistencial

		begin



		if	(:new.nr_seq_proj_ass	= 51352) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	gca_atendimento

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	gca_atendimento

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	gca_atendimento

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'4');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51474,51475)) then -- Escala ECOG

		begin



		if	(:new.nr_seq_proj_ass	= 51474) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_ecog

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_ecog

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_ecog

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'43');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51344,51345, 52590, 52608)) then -- Escala Cincinnati

		begin



		if	(:new.nr_seq_proj_ass	IN(51344, 52590)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_cincinnati

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_cincinnati

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_cincinnati

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'73');

		end if;



		end;



	elsif	(:new.nr_seq_proj_ass in (51478,51479)) then -- Escala de AKIN

		begin



		if	(:new.nr_seq_proj_ass	= 51478) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_akin

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_akin

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_akin

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'80');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51438,51439)) then -- Escala de Aldrete modificada

		begin



		if	(:new.nr_seq_proj_ass	= 51438) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_aldrete

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_aldrete

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			Select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_aldrete

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XAD', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'97');

		end if;



		end;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (52168,52169)) then --Escala Chung


		if	(:new.NR_SEQ_PROJ_ASS	= 52169) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_chung

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_chung

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



	if	(nr_sequencia_w	is not null) then


			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	escala_chung  p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XESCCC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'97');

	end if;

		elsif	(:new.NR_SEQ_PROJ_ASS in  (52130,52129)) then --Escala Possum


		if	(:new.NR_SEQ_PROJ_ASS	= 52129) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_possum

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_possum

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



	if	(nr_sequencia_w	is not null) then


			if 	(cd_pessoa_fisica_w is null) then

				select	max(c.nr_atendimento),

					max(c.cd_pessoa_fisica)

				into	nr_atendimento_w,

					cd_pessoa_fisica_w

				from	escala_possum  p,

					pepo_cirurgia c

				where	p.nr_sequencia	= nr_sequencia_W

				and	p.nr_seq_pepo	= c.nr_sequencia;

			end if;



			Gerar_registro_pendente_PEP('XESP', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'97');

	end if;

	elsif (:new.NR_SEQ_PROJ_ASS in (51950,51951)) then --Equipamentos
					if (:new.NR_SEQ_PROJ_ASS    = 51951) then
						select max(nr_sequencia)
						into nr_sequencia_w
						from equipamento_cirurgia
						where nr_seq_assinatura = :new.nr_sequencia;
					else
						select max(nr_sequencia)
						into nr_sequencia_w
						from equipamento_cirurgia
						where NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;
					end if;
					if (nr_sequencia_w      is not null) then
						if (cd_pessoa_fisica_w is null) then
							select max(c.nr_atendimento),
								max(c.cd_pessoa_fisica)
							into nr_atendimento_w,
								cd_pessoa_fisica_w
							from equipamento_cirurgia p,
								pepo_cirurgia c
							where p.nr_sequencia = nr_sequencia_W
							and p.nr_seq_pepo    = c.nr_sequencia;
						end if;
						Gerar_registro_pendente_PEP('XEFAN', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'97');
					end if;

	elsif (:new.NR_SEQ_PROJ_ASS in (51928,51929)) then -- Eventos
					if (:new.NR_SEQ_PROJ_ASS    = 51928) then
						select max(nr_sequencia)
						into nr_sequencia_w
						from qua_evento_paciente
						where nr_seq_assinatura = :new.nr_sequencia;
					else
						select max(nr_sequencia)
						into nr_sequencia_w
						from qua_evento_paciente
						where NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;
					end if;
					if (nr_sequencia_w      is not null) then
						if (cd_pessoa_fisica_w is null) then
							select max(c.nr_atendimento),
								max(c.cd_pessoa_fisica)
							into nr_atendimento_w,
								cd_pessoa_fisica_w
							from equipamento_cirurgia p,
								pepo_cirurgia c
							where p.nr_sequencia = nr_sequencia_W
							and p.nr_seq_pepo    = c.nr_sequencia;
						end if;
						Gerar_registro_pendente_PEP('XEVT', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'97');
					end if;

	elsif	(:new.nr_seq_proj_ass in (51420,51421, 52686, 52687)) then -- Escala de Avaliacao de Risco de Queda da NHS (EARQ)

		begin



		if	(:new.nr_seq_proj_ass	IN(51420, 52686)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_earq

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_earq

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			Select 	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_earq

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'94');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51416,51417, 52670, 52671)) then -- Escala de Depressao Geriatrica (YESAVAGE)

		begin



		if	(:new.nr_seq_proj_ass	IN(51416, 52670)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_yesavage

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_yesavage

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_yesavage

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'84');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51422,51423)) then --Escala de equilibrio e mobilidade de Tinetti

		begin



		if	(:new.nr_seq_proj_ass	= 51422) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_tenetti

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_tenetti

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_tenetti

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'91');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51337,51339,50653,50652)) then -- Escala de Stewart

		begin



		if	(:new.nr_seq_proj_ass	in (51337,50653)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_stewart

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_stewart

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_stewart

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'71');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51434,51435)) then -- Escala Morse de Quedas

		begin



		if	(:new.nr_seq_proj_ass	= 51434) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_morse

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_morse

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_morse

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'89');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51480,51481)) then --Escala Fargestrom

		begin



		if	(:new.nr_seq_proj_ass	= 51480) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_fargestrom

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_fargestrom

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	escala_fargestrom

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'14');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51440,51441)) then -- Escala Framingham

		begin



		if	(:new.nr_seq_proj_ass	= 51440) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_framingham

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_framingham

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	escala_framingham

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'13');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51346,51347, 52648, 52649)) then -- Escala Glasgow

		begin



		if	(:new.nr_seq_proj_ass	in(51346, 52648)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_escala_indice

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atend_escala_indice

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	atend_escala_indice

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'3');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51412,51413)) then -- Escala Karnofsky

		begin



		if	(:new.nr_seq_proj_ass	= 51412) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_karnofsky

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_karnofsky

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_karnofsky

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'40');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51424,51425, 52676, 52677)) then -- Escala Katz

		begin



		if	(:new.nr_seq_proj_ass	IN(51424, 52676)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_katz

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_katz

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_katz

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'79');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51414,51415, 52652, 52653)) then -- Escala Medida de independencia funcional (MIF)

		begin



		if	(:new.nr_seq_proj_ass	IN(51414, 52652)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_mif

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_mif

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_mif

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'70');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51340,51341, 52682, 52683)) then -- Escala MELD

		begin



		if	(:new.nr_seq_proj_ass	IN(51340, 52682)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	tx_meld

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	tx_meld

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(cd_pessoa_fisica)

			into	cd_pessoa_fisica_w

			from	tx_meld

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'61');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51430,51431, 52668, 52669)) then -- Escala Mini Nutritional Assessment MNA Long form

		begin



		if	(:new.nr_seq_proj_ass	IN(51430, 52668)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_man

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_man

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_man

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'60');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51428,51429, 52674, 52675)) then -- Escala Mini Nutritional Assessment MNA Short form

		begin



		if	(:new.nr_seq_proj_ass	IN(51428, 52674)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_man_lf

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_man_lf

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_man_lf

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'139');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51432,51433)) then -- Escala Mini exame do estado mental

		begin



		if	(:new.nr_seq_proj_ass	= 51432) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_mini_mental

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_mini_mental

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_mini_mental

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'101');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51482,51483)) then -- Escala MUST

		begin



		if	(:new.nr_seq_proj_ass	= 51482) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_must

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_must

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_must

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'28');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51342,51343, 52684, 52685)) then -- Escala Nursing Activities Score (NAS)

		begin



		if	(:new.nr_seq_proj_ass	IN(51342, 52684)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_nas

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_nas

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_nas

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'24');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51436,51437, 52680, 52681)) then -- Escala Qualidade de vida

		begin



		if	(:new.nr_seq_proj_ass	IN(51436, 52680)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_qualidade_vida

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_qualidade_vida

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_qualidade_vida

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'37');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51193,51195, 51193, 51195)) then -- Escala Risco de tromboembolismo venoso (TEV)

		begin



		if	(:new.nr_seq_proj_ass	IN(51193, 51193)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_tev

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_tev

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_tev

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'57');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51484,51485)) then -- Escala SAPS III

		begin



		if	(:new.nr_seq_proj_ass	= 51484) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_saps3

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_saps3

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_saps3

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'83');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (53462,53463)) then -- Escala FLEBITE

		begin



		if	(:new.nr_seq_proj_ass	= 53462) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_flebite_ins

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_flebite_ins

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_flebite_ins

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'88');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51486,51487, 52672, 52673)) then -- Escala SOFA

		begin



		if	(:new.nr_seq_proj_ass	IN(51486, 52672)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sofa

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sofa

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_sofa

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'18');

		end if;



		end;

	elsif	(:new.nr_seq_proj_ass in (51486,51487)) then -- Escala SOFA

		begin



		if	(:new.nr_seq_proj_ass	= 51486) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sofa

			where	nr_seq_assinatura = :new.nr_sequencia;

		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	escala_sofa

			where	NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	cd_pessoa_fisica_w

			from	escala_sofa

			where	nr_sequencia	= nr_sequencia_w;



			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'18');

		end if;



		end;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (25186,25243,50733,50734)) then --Orientacao de Alta



		if	(:new.NR_SEQ_PROJ_ASS	IN (25186,50733)) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_alta

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	atendimento_alta

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	atendimento_alta

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XOA', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');



		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (51335,51336)) then -- CIH



		if	(:new.NR_SEQ_PROJ_ASS	= 51335) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ATENDIMENTO_PRECAUCAO

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ATENDIMENTO_PRECAUCAO

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	ATENDIMENTO_PRECAUCAO

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XCIH', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');



		end if;



	elsif	(:new.NR_SEQ_PROJ_ASS in  (51373,51374)) then -- Orientacao de alta enfermagem



		if	(:new.NR_SEQ_PROJ_ASS	= 51373) then

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ORIENTACAO_ALTA_ENF

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_sequencia)

			into	nr_sequencia_w

			from	ORIENTACAO_ALTA_ENF

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	ORIENTACAO_ALTA_ENF

			where	nr_sequencia	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XOAE', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');



		end if;





	elsif	(:new.NR_SEQ_PROJ_ASS in  (51293,51708)) then -- Tratamento oncologico - Atendimento do ciclo



		if	(:new.NR_SEQ_PROJ_ASS	= 51293) then

			select	max(nr_seq_atendimento)

			into	nr_sequencia_w

			from	PACIENTE_ATENDIMENTO

			where	nr_seq_assinatura	= :new.nr_sequencia;



		else

			select	max(nr_seq_atendimento)

			into	nr_sequencia_w

			from	PACIENTE_ATENDIMENTO

			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;

		end if;



		if	(nr_sequencia_w	is not null) then



			select	max(nr_atendimento),

				max(obter_pessoa_atendimento(nr_atendimento,'C'))

			into	nr_atendimento_w,

				cd_pessoa_fisica_w

			from	PACIENTE_ATENDIMENTO

			where	nr_seq_atendimento	= nr_sequencia_W;



			Gerar_registro_pendente_PEP('XONC', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');



		end if;

     elsif (:new.NR_SEQ_PROJ_ASS in  (52848, 52868)) then  -- Justificativas / Solicitacoes

          if	(:new.NR_SEQ_PROJ_ASS	= 52848) then
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   PACIENTE_JUSTIFICATIVA
			where   nr_seq_assinatura	= :new.nr_sequencia;
		else
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   PACIENTE_JUSTIFICATIVA
			where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			select  max(nr_atendimento),
				   max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	   nr_atendimento_w,
				   cd_pessoa_fisica_w
			from	   PACIENTE_JUSTIFICATIVA
			where   nr_sequencia	= nr_sequencia_W;

			Gerar_registro_pendente_PEP('XJS', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;

     elsif (:new.NR_SEQ_PROJ_ASS in  (52889, 52890)) then  -- Declaracao de obito

          if	(:new.NR_SEQ_PROJ_ASS	= 52889) then
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   DECLARACAO_OBITO
			where   nr_seq_assinatura	= :new.nr_sequencia;
		else
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   DECLARACAO_OBITO
			where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			select  max(nr_atendimento),
				   max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	   nr_atendimento_w,
				   cd_pessoa_fisica_w
			from	   DECLARACAO_OBITO
			where   nr_sequencia	= nr_sequencia_W;

			Gerar_registro_pendente_PEP('XDOB', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;

		elsif (:new.NR_SEQ_PROJ_ASS in  (52510, 52511)) then  -- APAE

		  if	(:new.NR_SEQ_PROJ_ASS	= 52510) then
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   AVAL_PRE_ANESTESICA
			where   nr_seq_assinatura	= :new.nr_sequencia;
		else
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   AVAL_PRE_ANESTESICA
			where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			select  max(nr_atendimento),
				   max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	   nr_atendimento_w,
				   cd_pessoa_fisica_w
			from	   AVAL_PRE_ANESTESICA
			where   nr_sequencia	= nr_sequencia_W;

			Gerar_registro_pendente_PEP('XAPAE', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;

     elsif (:new.NR_SEQ_PROJ_ASS in  (52908, 52910)) then  -- Sumario de alta

	if	(:new.NR_SEQ_PROJ_ASS	= 52908) then
		select  max(nr_sequencia)
		into	   nr_sequencia_w
		from	   ATEND_SUMARIO_ALTA
		where   nr_seq_assinatura	= :new.nr_sequencia;
	else
		select  max(nr_sequencia)
		into	   nr_sequencia_w
		from	   ATEND_SUMARIO_ALTA
		where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
	end if;

	if	(nr_sequencia_w	is not null) then
		select  max(nr_atendimento),
			   max(obter_pessoa_atendimento(nr_atendimento,'C'))
		into	   nr_atendimento_w,
			   cd_pessoa_fisica_w
		from	   ATEND_SUMARIO_ALTA
		where   nr_sequencia	= nr_sequencia_W;

		Gerar_registro_pendente_PEP('XSUM', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
	end if;

	     elsif (:new.NR_SEQ_PROJ_ASS in  (53452, 53453)) then  -- Avaliacao Nutricional

	if	(:new.NR_SEQ_PROJ_ASS	= 53452) then
		select  max(nr_sequencia)
		into	   nr_sequencia_w
		from	   AVAL_NUTRICAO
		where   nr_seq_assinatura	= :new.nr_sequencia;
	else
		select  max(nr_sequencia)
		into	   nr_sequencia_w
		from	   AVAL_NUTRICAO
		where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
	end if;

	if	(nr_sequencia_w	is not null) then
		select  max(nr_atendimento)
		into	   nr_atendimento_w
		from	   AVAL_NUTRICAO
		where   nr_sequencia	= nr_sequencia_W;

		Gerar_registro_pendente_PEP('XAVN', nr_sequencia_w ,substr(obter_pessoa_atendimento(nr_atendimento_w,'C'),1,255), nr_atendimento_w, :NEW.NM_USUARIO,'A');
	end if;

    elsif (:new.NR_SEQ_PROJ_ASS in  (53208, 53209)) then  -- Anamnese oncologia

          if	(:new.NR_SEQ_PROJ_ASS	= 53208) then
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   ANAMNESE_ONCOLOGIA
			where   nr_seq_assinatura	= :new.nr_sequencia;
	else
		select  max(nr_sequencia)
		into	   nr_sequencia_w
		from	   ANAMNESE_ONCOLOGIA
		where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
	end if;

		if	(nr_sequencia_w	is not null) then
			select  max(nr_atendimento),
				   max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	   nr_atendimento_w,
				   cd_pessoa_fisica_w
			from	   ANAMNESE_ONCOLOGIA
			where   nr_sequencia	= nr_sequencia_W;

			Gerar_registro_pendente_PEP('XAON', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;

          elsif (:new.NR_SEQ_PROJ_ASS in  (53210, 53211)) then  -- Solicitacao externa de Exames

          if	(:new.NR_SEQ_PROJ_ASS	= 53210) then
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   PEDIDO_EXAME_EXTERNO
			where   nr_seq_assinatura	= :new.nr_sequencia;
		else
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   PEDIDO_EXAME_EXTERNO
			where   NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			select  max(nr_atendimento),
				   max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	   nr_atendimento_w,
				   cd_pessoa_fisica_w
			from	   PEDIDO_EXAME_EXTERNO
			where   nr_sequencia	= nr_sequencia_W;

			Gerar_registro_pendente_PEP('XPEE', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;

	elsif (:new.NR_SEQ_PROJ_ASS in  (50852, 50854)) then  -- Equipamento
		if (:new.NR_SEQ_PROJ_ASS	= 50852) then
			select max(nr_sequencia)
			into nr_sequencia_w
			from equipamento_cirurgia
			where nr_seq_assinatura = :new.nr_sequencia;
		else
			select max(nr_sequencia)
			into nr_sequencia_w
			from equipamento_cirurgia
			where NR_SEQ_ASSINAT_INATIVACAO = :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from equipamento_cirurgia
			where nr_sequencia = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XEFAN', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;

	elsif (:new.NR_SEQ_PROJ_ASS in  (50972, 50973, 52109, 52108)) then  -- Tempo e movimentos PEPO / FANEP
		if (:new.NR_SEQ_PROJ_ASS	in (50972, 52109)) then
			select max(nr_sequencia)
			into nr_sequencia_w
			from evento_cirurgia_paciente
			where nr_seq_assinatura = :new.nr_sequencia;
		else
			select max(nr_sequencia)
			into nr_sequencia_w
			from evento_cirurgia_paciente
			where nr_seq_assinat_inativacao = :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			/* usado  o cd_pessoa_fisica como :NEW.cd_pessoa_fisica e nr_atendimento_w = null pois se obter atraves da cirurgia gera mutante*/

			Gerar_registro_pendente_PEP('XTMOV', nr_sequencia_w , :NEW.cd_pessoa_fisica, null, :NEW.NM_USUARIO,'A');
		end if;
	elsif (:new.NR_SEQ_PROJ_ASS in  (52329, 52250, 52269, 52328, 52248, 52270)) then  /*Administracoes Fanep*/
		if (:new.NR_SEQ_PROJ_ASS	in (52329, 52250, 52269)) then
			select max(a.nr_sequencia),
					max(b.ie_tipo)
			into	nr_sequencia_w,
					ie_tipo_w
			from	cirurgia_agente_anest_ocor a,
					cirurgia_agente_anestesico b
			where	a.nr_seq_assinatura = :new.nr_sequencia
			and		b.nr_sequencia= a.nr_seq_cirur_agente;
		else
			select max(a.nr_sequencia),
					max(b.ie_tipo)
			into	nr_sequencia_w,
					ie_tipo_w
			from	cirurgia_agente_anest_ocor a,
					cirurgia_agente_anestesico b
			where	a.nr_seq_assinat_inativacao = :new.nr_sequencia
			and		b.nr_sequencia= a.nr_seq_cirur_agente;
		end if;

		if	(nr_sequencia_w	is not null) then
			if	(ie_tipo_w = 1) then

				cd_tipo_registro_w :=	'XADAA';

			elsif	(ie_tipo_w = 2) then

				cd_tipo_registro_w :=	'XADTH';

			elsif	(ie_tipo_w = 3) then

				cd_tipo_registro_w := 	'XCAAM';

			elsif	(ie_tipo_w = 5) then

				cd_tipo_registro_w := 	'XADMD';

			end if;

			/* usado  o cd_pessoa_fisica como :NEW.cd_pessoa_fisica e nr_atendimento_w = null pois se obter atraves da cirurgia gera mutante*/
			Gerar_registro_pendente_PEP(cd_tipo_registro_w, nr_sequencia_w , :NEW.cd_pessoa_fisica, null, :NEW.NM_USUARIO,'A');
		end if;
	elsif (:new.NR_SEQ_PROJ_ASS in  (51116, 51117)) then  -- Terapia Hidroeletrolitica
		if (:new.NR_SEQ_PROJ_ASS	= 51116) then
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinatura = :new.nr_sequencia;
		else
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinat_inativacao = :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			/* usado  o cd_pessoa_fisica como :NEW.cd_pessoa_fisica e nr_atendimento_w = null pois se obter atraves da cirurgia gera mutante*/
			Gerar_registro_pendente_PEP('XADTH', nr_sequencia_w , :NEW.cd_pessoa_fisica, null, :NEW.NM_USUARIO,'A');
		end if;

		elsif (:new.NR_SEQ_PROJ_ASS in  (51118, 51119)) then  -- Adm Hemocomponete/Hemoderivados ADHE
		if (:new.NR_SEQ_PROJ_ASS	= 51118) then
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinatura = :new.nr_sequencia;
		else
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinat_inativacao = :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			/* usado  o cd_pessoa_fisica como :NEW.cd_pessoa_fisica e nr_atendimento_w = null pois se obter atraves da cirurgia gera mutante*/
			Gerar_registro_pendente_PEP('XADHE', nr_sequencia_w , :NEW.cd_pessoa_fisica, null, :NEW.NM_USUARIO,'A');
		end if;

	elsif (:new.NR_SEQ_PROJ_ASS in  (51114, 51115)) then  -- Adm  Medicamentos ADMD
		if (:new.NR_SEQ_PROJ_ASS	= 51114) then
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinatura = :new.nr_sequencia;
		else
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinat_inativacao = :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			/* usado  o cd_pessoa_fisica como :NEW.cd_pessoa_fisica e nr_atendimento_w = null pois se obter atraves da cirurgia gera mutante*/
			Gerar_registro_pendente_PEP('XADMD', nr_sequencia_w , :NEW.cd_pessoa_fisica, null, :NEW.NM_USUARIO,'A');
		end if;

 	elsif (:new.NR_SEQ_PROJ_ASS in  (51112, 51113)) then  --Adm Agentes Anestesicos ADAA
		if (:new.NR_SEQ_PROJ_ASS	= 51112) then
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinatura = :new.nr_sequencia;
		else
			select max(nr_sequencia)
			into nr_sequencia_w
			from cirurgia_agente_anest_ocor
			where nr_seq_assinat_inativacao = :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			/* usado  o cd_pessoa_fisica como :NEW.cd_pessoa_fisica e nr_atendimento_w = null pois se obter atraves da cirurgia gera mutante*/
			Gerar_registro_pendente_PEP('XADAA', nr_sequencia_w , :NEW.cd_pessoa_fisica, null, :NEW.NM_USUARIO,'A');
		end if;


     elsif (:new.NR_SEQ_PROJ_ASS in  (53213, 53214)) then  -- Toxicidade

        if	(:new.NR_SEQ_PROJ_ASS	= 53214) then
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   escala_toxidade
			where   nr_seq_assinatura	= :new.nr_sequencia;
		else
			select  max(nr_sequencia)
			into	   nr_sequencia_w
			from	   escala_toxidade
			where   nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;

		if	(nr_sequencia_w	is not null) then
			select  max(nr_atendimento),
				   max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	   nr_atendimento_w,
				   cd_pessoa_fisica_w
			from	   escala_toxidade
			where   nr_sequencia	= nr_sequencia_W;

			Gerar_registro_pendente_PEP('XESC', nr_sequencia_w ,cd_pessoa_fisica_w, null, :NEW.NM_USUARIO,'A',null,null,null,null,null,'105');
		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53245, 53244)) then --Evolucoes Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53244) then
                select	max(cd_evolucao)
                           into	nr_sequencia_w
                from	   evolucao_paciente
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(cd_evolucao)
                           into	nr_sequencia_w
                from	   evolucao_paciente
                where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   evolucao_paciente
                where	cd_evolucao	= nr_sequencia_w;

                Gerar_registro_pendente_PEP('XEVL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53252, 53253)) then --Angioretinofluoresceinografia Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53252) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_angio_retino
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_angio_retino
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica),
                        nvl(max(ie_tipo_registro),'A')
                into	   nr_atendimento_w,
                        cd_pessoa_fisica_w,
                        ie_tipo_registro_w
                from	   oft_angio_retino a,  oft_consulta b
                where   a.nr_seq_consulta = b.nr_sequencia
                and     nvl(ie_tipo_registro,'A') = 'A'
				or 		nvl(ie_tipo_registro,'A') = 'R'
                and     a.nr_sequencia	  = nr_sequencia_w;

                IF   (ie_tipo_registro_w = 'A') then
                  Gerar_registro_pendente_PEP('XAGRL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
                else
                  Gerar_registro_pendente_PEP('XARTL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
                end if;
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53258, 53259)) then --Biomicroscopia Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53258) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_biomicroscopia
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into		nr_sequencia_w
                from	   oft_biomicroscopia
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                        cd_pessoa_fisica_w
                from	   oft_biomicroscopia a,
								oft_consulta b
                where  	a.nr_seq_consulta = b.nr_sequencia
                and     a.nr_sequencia	  	= nr_sequencia_w;

                Gerar_registro_pendente_PEP('XBICL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53256, 53257)) then --Biometria Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53256) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_biometria
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
					 into		nr_sequencia_w
                from	   oft_biometria
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                        cd_pessoa_fisica_w
                from	   oft_biometria a,  oft_consulta b
                where  	a.nr_seq_consulta = b.nr_sequencia
                and     a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XBIOL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53222, 53223)) then --Anamnese inicial
            if	 (:new.NR_SEQ_PROJ_ASS	= 53222) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_anamnese
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
					 into		nr_sequencia_w
                from	   oft_anamnese
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                        cd_pessoa_fisica_w
                from	   oft_anamnese a,  oft_consulta b
                where  	a.nr_seq_consulta = b.nr_sequencia
                and     a.nr_sequencia	  = nr_sequencia_w;
                Gerar_registro_pendente_PEP('XONL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53226, 53227)) then --Anexos
            if	 (:new.NR_SEQ_PROJ_ASS	= 53226) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_anexo
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
					 into		nr_sequencia_w
                from	   oft_anexo
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                        cd_pessoa_fisica_w
                from	   oft_anexo a,  oft_consulta b
                where  	a.nr_seq_consulta = b.nr_sequencia
                and     a.nr_sequencia	  = nr_sequencia_w;
                Gerar_registro_pendente_PEP('XOAL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;
   elsif	(:new.NR_SEQ_PROJ_ASS in  (53263, 53264)) then --Ceratometria
            if	 (:new.NR_SEQ_PROJ_ASS	= 53263) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_cerastocopia
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                          into	nr_sequencia_w
                from	   oft_cerastocopia
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_cerastocopia a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XCERL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53260, 53261)) then --Campimetria Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53260) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_campimetria
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_campimetria
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_campimetria a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XCAPL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53263, 53264)) then --Campimetria Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53263) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_campimetria
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_campimetria
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_campimetria a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XCERL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53349, 53350)) then --Acuidade visual nova Oftalmologia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53349) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_correcao_atual
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_correcao_atual
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_correcao_atual a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XAVNL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53271, 53272)) then --Curva tensional
            if	 (:new.NR_SEQ_PROJ_ASS	= 53271) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_curva_tencional
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_curva_tencional
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_curva_tencional a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XCTL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

   elsif	(:new.NR_SEQ_PROJ_ASS in  (53273, 53274)) then --Distancia naso pupilar
            if	 (:new.NR_SEQ_PROJ_ASS	= 53273) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_dnp
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_dnp
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_dnp a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XDNPL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;
   elsif	(:new.NR_SEQ_PROJ_ASS in  (53440, 53441)) then --Ceratoscopia computadorizada
            if	 (:new.NR_SEQ_PROJ_ASS	= 53440) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_imagem_exame
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into		nr_sequencia_w
                from	   oft_imagem_exame
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                        cd_pessoa_fisica_w
                from	   oft_imagem_exame a,  oft_consulta b
                where  	a.nr_seq_consulta 	= b.nr_sequencia
					 and		ie_ceratoscopia		= 'S'
                and    	a.nr_sequencia	  		= nr_sequencia_w;

                Gerar_registro_pendente_PEP('XOIL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53275, 53276)) then --Exame ocular externo
            if	 (:new.NR_SEQ_PROJ_ASS	= 53275) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_exame_externo
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_exame_externo
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_exame_externo a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XEOEL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53277, 53278)) then --Fundoscopia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53277) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_fundoscopia
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_fundoscopia
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_fundoscopia a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XFNCL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53280, 53281)) then --Gonioscopia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53280) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_gonioscopia
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_gonioscopia
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_gonioscopia a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XGNOL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53282, 53283)) then --Microscopia Especular
            if	 (:new.NR_SEQ_PROJ_ASS	= 53282) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_microscopia_especular
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_microscopia_especular
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_microscopia_especular a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XMIEL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53284, 53285,53286,53287)) then --Motilidade ocular
            if	 (:new.NR_SEQ_PROJ_ASS	in (53284,53286)) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_motilidade_ocular
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                           into	nr_sequencia_w
                from	   oft_motilidade_ocular
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_motilidade_ocular a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XMOOL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53288, 53289)) then --Paquimetria
            if	 (:new.NR_SEQ_PROJ_ASS	= 53288) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_paquimetria
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_paquimetria
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica),
									max(ie_tipo_paquimetria)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w,
									ie_tipo_paquimetria_w
                from	   oft_paquimetria a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;
					if	(ie_tipo_paquimetria_w = 'U') then
						Gerar_registro_pendente_PEP('XPQUL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
					else
						Gerar_registro_pendente_PEP('XPAQL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
					end if;
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53291, 53292)) then --Potencial de acuidade visual
            if	 (:new.NR_SEQ_PROJ_ASS	= 53291) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_potencial_acuidade
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_potencial_acuidade
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_potencial_acuidade a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XPAVL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53293, 53294)) then --Pupilometria
            if	 (:new.NR_SEQ_PROJ_ASS	= 53293) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_pupilometria
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_pupilometria
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_pupilometria a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XPUPL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53295, 53296)) then --Tomografia de coerencia optica
            if	 (:new.NR_SEQ_PROJ_ASS	= 53295) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_oct
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_oct
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_oct a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XTOCL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
            end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53297, 53298)) then --Tomografia do olho
            if	 (:new.NR_SEQ_PROJ_ASS	= 53297) then
                select	max(nr_sequencia)
                into	   nr_sequencia_w
                from	   oft_tomografia_olho
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_tomografia_olho
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                           max(cd_pessoa_fisica)
                into	   nr_atendimento_w,
                           cd_pessoa_fisica_w
                from	   oft_tomografia_olho a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and      a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XTOGL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

			end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53299, 53300, 53433, 53436)) then --Tonometria aplanacao e pneumatica
            if	 (:new.NR_SEQ_PROJ_ASS	in (53299,53433)) then
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_tonometria
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_tonometria
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
					select	max(nr_atendimento),
								max(cd_pessoa_fisica)
					into		nr_atendimento_w,
								cd_pessoa_fisica_w
					from		oft_tonometria a,
								oft_consulta b
					where  	a.nr_seq_consulta = b.nr_sequencia
					and    	a.nr_sequencia	  	= nr_sequencia_w;
					if	(:new.NR_SEQ_PROJ_ASS = 53433) then
						Gerar_registro_pendente_PEP('XTPNL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
					else
						Gerar_registro_pendente_PEP('XTAPL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
					end if;
				end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53301, 53302)) then --Ultrassonografia
            if	 (:new.NR_SEQ_PROJ_ASS	= 53301) then
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_ultrassonografia
                where	nr_seq_assinatura	= :new.nr_sequencia;
            else
                select	max(nr_sequencia)
                into	nr_sequencia_w
                from	oft_ultrassonografia
                where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
            end if;
            if	 (nr_sequencia_w	is not null) then
                select	max(nr_atendimento),
                        max(cd_pessoa_fisica)
                into	nr_atendimento_w,
                        cd_pessoa_fisica_w
                from	oft_ultrassonografia a,  oft_consulta b
                where  a.nr_seq_consulta = b.nr_sequencia
                and    a.nr_sequencia	  = nr_sequencia_w;

                Gerar_registro_pendente_PEP('XULTL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

			end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53303, 53304)) then --Iridectomia
		if	 (:new.NR_SEQ_PROJ_ASS	= 53303) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_iridectomia
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_iridectomia
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_iridectomia a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XIRIL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53305, 53306)) then --Avaliacao Lacrimal
		if	 (:new.NR_SEQ_PROJ_ASS	= 53305) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_olho_seco
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_olho_seco
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_olho_seco a,  oft_consulta b

			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;


			Gerar_registro_pendente_PEP('XAVAL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;


	elsif	(:new.NR_SEQ_PROJ_ASS in  (53307, 53308)) then --Daltonismo
		if	 (:new.NR_SEQ_PROJ_ASS	= 53307) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_daltonismo

			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_daltonismo

			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_daltonismo a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XDATL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53309, 53310)) then --Capsulotomia
		if	 (:new.NR_SEQ_PROJ_ASS	= 53309) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_capsulotomia
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_capsulotomia
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_capsulotomia a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XCASL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53311, 53312)) then --Refracao
		if	 (:new.NR_SEQ_PROJ_ASS	= 53311) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_refracao
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_refracao
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_refracao a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XREFL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53313, 53314)) then --Auto Refracao
		if	 (:new.NR_SEQ_PROJ_ASS	= 53313) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_auto_refracao

			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_auto_refracao

			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_auto_refracao a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XAURL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53315, 53316)) then --Mapeamento da retina
		if	 (:new.NR_SEQ_PROJ_ASS	= 53315) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_mapeamento_retina
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_mapeamento_retina
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_mapeamento_retina a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XMARL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

  elsif	(:new.NR_SEQ_PROJ_ASS in  (53580, 53584)) then --Aberrometria ocular
		if	 (:new.NR_SEQ_PROJ_ASS	= 53580) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_aberrometria
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_aberrometria
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_aberrometria a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XABRO', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53317, 53318)) then -- Sobrecarga Hidrica
		if	 (:new.NR_SEQ_PROJ_ASS	= 53317) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_sobrecarga_hidrica

			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_sobrecarga_hidrica

			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;

		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_sobrecarga_hidrica a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XSOHL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53319, 53320)) then --Fotocoagulacao
		if	 (:new.NR_SEQ_PROJ_ASS	= 53319) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_fotocoagulacao_laser

			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_fotocoagulacao_laser

			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_fotocoagulacao_laser a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XFOTL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53269, 53270)) then --Acuidade Visual
		if	 (:new.NR_SEQ_PROJ_ASS	= 53269) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_acuidade_visual
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_acuidade_visual

			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;

		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_acuidade_visual a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XACUL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');
		end if;


	elsif	(:new.NR_SEQ_PROJ_ASS in  (53321, 53322)) then --Receita
		if	 (:new.NR_SEQ_PROJ_ASS	= 53321) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	med_receita
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	med_receita
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;

		if	 (nr_sequencia_w	is not null) then
			select	max(a.nr_atendimento_hosp),
					max(a.cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	med_receita a
			where   a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XRECL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53323, 53324)) then --Receita de oculos
		if	 (:new.NR_SEQ_PROJ_ASS	= 53323) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_oculos
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_oculos
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;

		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_oculos a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XREOL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (44280, 44284, 43057, 43058,53232,53233)) then --Descricao cirurgia
		if	 (:new.NR_SEQ_PROJ_ASS in (44284, 43057, 53232)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	cirurgia_descricao
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	cirurgia_descricao
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;

		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	cirurgia_descricao a,  cirurgia b
			where  a.nr_cirurgia = b.nr_cirurgia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XCDC', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :NEW.NM_USUARIO,'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (50772, 50773)) then /*Gravidez*/
		if	 (:new.NR_SEQ_PROJ_ASS	= 50772) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	atendimento_gravidez
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	atendimento_gravidez
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;

		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	atendimento_gravidez a
			where  a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XGRAV', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53236, 53237, 51333, 51334)) then /*Consentimentos*/
		if	 (:new.NR_SEQ_PROJ_ASS in (53236,51333)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	pep_pac_ci
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	pep_pac_ci
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	pep_pac_ci a
			where  a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XCNL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53234, 53235)) then /*Conduta oftalmologica*/
		if	 (:new.NR_SEQ_PROJ_ASS	= 53234) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_conduta
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_conduta
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_conduta a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XOCL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (50992, 50993)) then /*Controle de cavidade*/
		if	 (:new.NR_SEQ_PROJ_ASS	= 50992) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	cirurgia_item_controle
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	cirurgia_item_controle
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(c.nr_atendimento),
						max(c.cd_pessoa_fisica)
			into		nr_atendimento_w,
						cd_pessoa_fisica_w
			from		cirurgia_item_controle p,
						cirurgia c
			where		p.nr_sequencia	= nr_sequencia_W
			and		p.nr_cirurgia	= c.nr_cirurgia;

			if 	(cd_pessoa_fisica_w is null) then
				select	max(c.nr_atendimento),
							max(c.cd_pessoa_fisica)
				into		nr_atendimento_w,
							cd_pessoa_fisica_w
				from		cirurgia_item_controle p,
							pepo_cirurgia c
				where		p.nr_sequencia	= nr_sequencia_W
				and		p.nr_seq_pepo	= c.nr_sequencia;
			end if;
			Gerar_registro_pendente_PEP('XLCC', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');
		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (50692, 50693,53450,53451)) then /*Faturamento medico*/
		if	 (:new.NR_SEQ_PROJ_ASS	in (50692,53450)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	pep_med_fatur
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	pep_med_fatur
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(c.nr_atendimento),
						max(c.cd_pessoa_fisica)
			into		nr_atendimento_w,
						cd_pessoa_fisica_w
			from		pep_med_fatur p,
						atendimento_paciente c
			where		p.nr_sequencia		= nr_sequencia_W
			and		p.nr_atendimento	= c.nr_atendimento;
			Gerar_registro_pendente_PEP('XLFM', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');
		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53246,51032,51033,53247)) then /*Orientacoes*/
		if	 (:new.NR_SEQ_PROJ_ASS	in (53246,51032)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	pep_orientacao_geral
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	pep_orientacao_geral
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	pep_orientacao_geral a
			where  a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XOGL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (50632,50633)) then /*Boletim informativo*/
		if	 (:new.NR_SEQ_PROJ_ASS	 = 50632) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	atendimento_boletim
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	atendimento_boletim
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(obter_pessoa_atendimento(nr_atendimento,'C'))
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	atendimento_boletim a
			where  a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XLBI', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (50792,50793,53399,53400)) then /*Participantes*/
		if	 (:new.NR_SEQ_PROJ_ASS	in (50792,53399)) then
			select	max(nr_seq_interno)
			into	nr_sequencia_w
			from	cirurgia_participante
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_seq_interno)
			into	nr_sequencia_w
			from	cirurgia_participante
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(b.nr_atendimento),
					max(b.cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	cirurgia_participante a,
					cirurgia b
			where a.nr_cirurgia 		= b.nr_cirurgia
			and	a.nr_seq_interno	= nr_sequencia_w;

			Gerar_registro_pendente_PEP('XCP', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53238, 53239, 53242, 53243)) then /*Diagnostico oftalmo*/
		if	 (:new.NR_SEQ_PROJ_ASS	IN (53238,53242)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_diagnostico
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_diagnostico
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;

		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(b.nr_atendimento),
					max(b.cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_diagnostico a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			if	(:new.NR_SEQ_PROJ_ASS in  (53238, 53239)) then
				Gerar_registro_pendente_PEP('XODL', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');
			else
				Gerar_registro_pendente_PEP('XODF', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');
			end if;

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53627, 53628, 53629, 53630)) then --Complicacoes Anestesicas PEPO e FANEP

		if	(:new.NR_SEQ_PROJ_ASS	in (53627,53629)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	anestesia_complicacoes
			where	nr_seq_assinatura	= :new.nr_sequencia;

		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	anestesia_complicacoes
			where	NR_SEQ_ASSINAT_INATIVACAO	= :new.nr_sequencia;
		end if;

		if (nr_sequencia_w	is not null) then
			select        max(c.nr_atendimento),
                        max(c.cd_pessoa_fisica)
			into        nr_atendimento_w,
                        cd_pessoa_fisica_w
			from        anestesia_complicacoes p,
                        cirurgia c
			where        p.nr_sequencia    = nr_sequencia_W
			and            p.nr_cirurgia    = c.nr_cirurgia;

			if (cd_pessoa_fisica_w is null) then
				select  max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
				into     nr_atendimento_w,
				cd_pessoa_fisica_w
				from     anestesia_complicacoes p,
				pepo_cirurgia c
				where     p.nr_sequencia = nr_sequencia_W
				and     p.nr_seq_pepo    = c.nr_sequencia;
			end if;

			Gerar_registro_pendente_PEP('XCMA', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w,:NEW.NM_USUARIO,'A');

		end if;
	elsif(:new.NR_SEQ_PROJ_ASS in  (53631,53632)) then -- Projeto de assinatura APAE alertas

		if (:new.NR_SEQ_PROJ_ASS  in (53631)) then
		select	max(nr_sequencia)
			into	nr_sequencia_w
			from	alerta_anestesia_apae
			where	nr_seq_assinatura	= :new.nr_sequencia;

		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	alerta_anestesia_apae
			where	NR_SEQ_ASSINATURA_INATIVACAO	= :new.nr_sequencia;
		end if;

		if (nr_sequencia_w is not null) then
			select max(cd_pessoa_fisica)
			into  cd_pessoa_fisica_w
			from  alerta_anestesia_apae
			where nr_sequencia  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XAPN', nr_sequencia_w ,cd_pessoa_fisica_w, null,:NEW.NM_USUARIO,'A');

		end if;
	elsif	(:new.NR_SEQ_PROJ_ASS in  (53437, 53438)) then /*Selecao de lentes Oftalmologia*/
		if	 (:new.NR_SEQ_PROJ_ASS	IN (53437)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_consulta_lente
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	oft_consulta_lente
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;

		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(b.nr_atendimento),
					max(b.cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	oft_consulta_lente a,  oft_consulta b
			where  a.nr_seq_consulta = b.nr_sequencia
			and    a.nr_sequencia	  = nr_sequencia_w;

			Gerar_registro_pendente_PEP('XCLEN', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');

		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53675, 53676)) then /*Historico de Saude - Amputacoes*/
		if	 (:new.NR_SEQ_PROJ_ASS	IN (53675)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	paciente_amputacao
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	paciente_amputacao
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	paciente_amputacao
			where   nr_sequencia = nr_sequencia_w;
			Gerar_registro_pendente_PEP('XHSAP', nr_sequencia_w ,cd_pessoa_fisica_w, null, :new.nm_usuario, 'A');
		end if;

	elsif	(:new.NR_SEQ_PROJ_ASS in  (53673, 53674)) then /*Historico de Saude - Antec. Sexuais*/
		if	 (:new.NR_SEQ_PROJ_ASS	IN (53673)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	paciente_antec_sexuais
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	paciente_antec_sexuais
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	paciente_antec_sexuais
			where   nr_sequencia = nr_sequencia_w;
			Gerar_registro_pendente_PEP('XHSAT', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');
		end if;

  elsif	(:new.NR_SEQ_PROJ_ASS in  (53681, 53682)) then /*PEP - Procedimentos */

		if	 (:new.NR_SEQ_PROJ_ASS	IN (53681)) then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	PROC_PAC_DESCRICAO
			where	nr_seq_assinatura	= :new.nr_sequencia;
		else
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	PROC_PAC_DESCRICAO
			where	nr_seq_assinat_inativacao	= :new.nr_sequencia;
		end if;
		if	 (nr_sequencia_w	is not null) then
			select	max(nr_atendimento),
					max(cd_pessoa_fisica)
			into	nr_atendimento_w,
					cd_pessoa_fisica_w
			from	PROC_PAC_DESCRICAO
			where   nr_sequencia = nr_sequencia_w;
			Gerar_registro_pendente_PEP('XPPD', nr_sequencia_w ,cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario, 'A');
		end if;

	end if;

end if;

:NEW.DS_UTC_ATUALIZACAO := OBTER_DATA_UTC(SYSDATE,'HV');

if	(:new.dt_assinatura is not null) then
	:new.ds_utc := OBTER_DATA_UTC(:new.dt_assinatura,'HV');
end	if;

:new.ie_horario_verao := obter_se_horario_verao(:new.dt_atualizacao);

exception

	when others then

	null;

end;
<<Final>>
null;

end;
/


CREATE OR REPLACE TRIGGER TASY.TASY_ASSINATURA_DIGITAL_INSERT
BEFORE INSERT ON TASY.TASY_ASSINATURA_DIGITAL FOR EACH ROW
BEGIN

INSERT INTO TASY_ASSINAT_DIG_PEND
(DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, DT_ATUALIZACAO, NM_USUARIO, NR_SEQ_ASSINATURA, CD_PERFIL, NR_SEQ_PROJ_ASS)
VALUES
(SYSDATE, :NEW.NM_USUARIO, :NEW.DT_REGISTRO, :NEW.NM_USUARIO, :NEW.NR_SEQUENCIA, wheb_usuario_pck.get_cd_perfil, :NEW.NR_SEQ_PROJ_ASS);

END;
/


ALTER TABLE TASY.TASY_ASSINATURA_DIGITAL ADD (
  CONSTRAINT TASASDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_ASSINATURA_DIGITAL ADD (
  CONSTRAINT TASASDI_TASPRAS_FK 
 FOREIGN KEY (NR_SEQ_PROJ_ASS) 
 REFERENCES TASY.TASY_PROJETO_ASSINATURA (NR_SEQUENCIA),
  CONSTRAINT TASASDI_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_XML) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA),
  CONSTRAINT TASASDI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.TASY_ASSINATURA_DIGITAL TO NIVEL_1;

