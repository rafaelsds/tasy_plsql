ALTER TABLE TASY.PLS_REGRA_CONTA_AUT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_CONTA_AUT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_CONTA_AUT
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  IE_COMPLEMENTO_GUIA     VARCHAR2(3 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_GUIA            VARCHAR2(2 BYTE),
  IE_PRECO                VARCHAR2(2 BYTE),
  NR_CONTRATO             NUMBER(10),
  IE_TIPO_SEGURADO        VARCHAR2(3 BYTE),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  NR_SEQ_MATERIAL         NUMBER(10),
  CD_DOENCA_CID           VARCHAR2(10 BYTE),
  NR_SEQ_PRESTADOR        NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR  NUMBER(10),
  DT_FIM_VIGENCIA         DATE,
  NR_SEQ_GRUPO_CONTRATO   NUMBER(10),
  NR_SEQ_GRUPO_SERVICO    NUMBER(10),
  NR_SEQ_TIPO_PRESTADOR   NUMBER(10),
  IE_EXIGE_PROCEDIMENTO   VARCHAR2(10 BYTE),
  IE_TIPO_PROCESSO_AUTOR  VARCHAR2(1 BYTE),
  NR_SEQ_INTERCAMBIO      NUMBER(10),
  CD_PRESTADOR            VARCHAR2(30 BYTE),
  IE_TIPO_RELACAO         VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO    NUMBER(10),
  IE_ORIGEM_SOLIC         VARCHAR2(2 BYTE),
  IE_TIPO_ATEND_TISS      VARCHAR2(2 BYTE),
  NM_REGRA                VARCHAR2(255 BYTE),
  CD_MEDICO_SOLICITANTE   VARCHAR2(10 BYTE),
  NM_USUARIO_ATENDIMENTO  VARCHAR2(15 BYTE),
  NM_USUARIO_WEB          VARCHAR2(100 BYTE),
  NR_SEQ_GRAU_PARTIC      NUMBER(10),
  IE_VIA_ACESSO           VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_REC        NUMBER(10),
  IE_ORIGEM_EXECUCAO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRCOA_CIDDOEN_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_ESTABEL_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCOA_GRURECE_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCOA_MEDICO_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(CD_MEDICO_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRCOA_PK ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSCLPR_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSGRPA_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCOA_PLSINCA_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSMAT_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSPRES_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSPRGC_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSPRGC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSPRGP_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSPRGS_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSPRGS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PLSTIPR_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCOA_PROCEDI_FK_I ON TASY.PLS_REGRA_CONTA_AUT
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCOA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_CONTA_AUT_tp  after update ON TASY.PLS_REGRA_CONTA_AUT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_REGRA_CONTA_AUT',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_RELACAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_RELACAO,1,4000),substr(:new.IE_TIPO_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RELACAO',ie_log_w,ds_w,'PLS_REGRA_CONTA_AUT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_CONTA_AUT ADD (
  CONSTRAINT PLSRCOA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_CONTA_AUT ADD (
  CONSTRAINT PLSRCOA_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_SOLICITANTE) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSRCOA_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PLSRCOA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRCOA_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSRCOA_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSRCOA_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_CONTA_AUT TO NIVEL_1;

