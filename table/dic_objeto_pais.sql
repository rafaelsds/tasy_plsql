ALTER TABLE TASY.DIC_OBJETO_PAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIC_OBJETO_PAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIC_OBJETO_PAIS
(
  NR_SEQ_OBJETO      NUMBER(10)                 NOT NULL,
  CD_PAIS            NUMBER(5)                  NOT NULL,
  NR_SEQ_OBJETO_REF  NUMBER(10)                 NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DICOBJPA_DICOBJE_FK2_I ON TASY.DIC_OBJETO_PAIS
(NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJPA_DICOBJE_FK3_I ON TASY.DIC_OBJETO_PAIS
(NR_SEQ_OBJETO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DICOBJPAIS_PK ON TASY.DIC_OBJETO_PAIS
(NR_SEQ_OBJETO, CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJPA_TASYPAI_FK_I ON TASY.DIC_OBJETO_PAIS
(CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DIC_OBJETO_PAIS ADD (
  CONSTRAINT DICOBJPAIS_PK
 PRIMARY KEY
 (NR_SEQ_OBJETO, CD_PAIS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIC_OBJETO_PAIS ADD (
  CONSTRAINT DICOBJPA_DICOBJE_FK2 
 FOREIGN KEY (NR_SEQ_OBJETO) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA),
  CONSTRAINT DICOBJPA_DICOBJE_FK3 
 FOREIGN KEY (NR_SEQ_OBJETO_REF) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA),
  CONSTRAINT DICOBJPA_TASYPAI_FK 
 FOREIGN KEY (CD_PAIS) 
 REFERENCES TASY.TASY_PAIS (CD_PAIS));

GRANT SELECT ON TASY.DIC_OBJETO_PAIS TO NIVEL_1;

