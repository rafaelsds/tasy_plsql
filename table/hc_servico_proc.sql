ALTER TABLE TASY.HC_SERVICO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HC_SERVICO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.HC_SERVICO_PROC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SERVICO       NUMBER(10)               NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  NR_SEQ_PROC_INTERNO  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HCSEPRO_HCSERVI_FK_I ON TASY.HC_SERVICO_PROC
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HCSEPRO_PK ON TASY.HC_SERVICO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HCSEPRO_PROCEDI_FK_I ON TASY.HC_SERVICO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HCSEPRO_PROINTE_FK_I ON TASY.HC_SERVICO_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hc_servico_proc_beforepost
before insert or update ON TASY.HC_SERVICO_PROC for each row
declare
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);

begin

if      (:new.nr_seq_proc_interno is not null) and
	((:old.nr_seq_proc_interno is null) or
	((:old.nr_seq_proc_interno is not null) and
	(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno))) then

	obter_proc_tab_interno_conv(
					:new.nr_seq_proc_interno,
					wheb_usuario_pck.get_cd_estabelecimento,
					null,
					null,
					null,
					null,
					cd_procedimento_w,
					ie_origem_proced_w,
					null,
					sysdate,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);

	:new.cd_procedimento	:= cd_procedimento_w;
	:new.ie_origem_proced	:= ie_origem_proced_w;

end if;

end;
/


ALTER TABLE TASY.HC_SERVICO_PROC ADD (
  CONSTRAINT HCSEPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HC_SERVICO_PROC ADD (
  CONSTRAINT HCSEPRO_HCSERVI_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.HC_SERVICO (NR_SEQUENCIA),
  CONSTRAINT HCSEPRO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT HCSEPRO_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HC_SERVICO_PROC TO NIVEL_1;

