ALTER TABLE TASY.NF_IMP_NOTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NF_IMP_NOTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.NF_IMP_NOTA
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  DS_VERSAO                     VARCHAR2(10 BYTE),
  DS_SERIE                      VARCHAR2(25 BYTE),
  DS_NUMERO                     VARCHAR2(40 BYTE),
  DT_EMISSAO                    DATE,
  DS_SELO                       VARCHAR2(4000 BYTE),
  NR_SEQ_CHAVE_FORMA_PAG        NUMBER(10),
  NR_CERTIFICADO                VARCHAR2(20 BYTE),
  DS_CERTIFICADO                VARCHAR2(4000 BYTE),
  DS_CONDICAO_PAG               VARCHAR2(1000 BYTE),
  VL_SUBTOTAL                   NUMBER(15,6),
  VL_DESCONTO                   NUMBER(15,6),
  NR_SEQ_CHAVE_MOEDA            NUMBER(10),
  TX_CAMBIO                     NUMBER(15,6),
  VL_TOTAL                      NUMBER(15,6),
  NR_SEQ_CHAVE_TIPO_COMP        NUMBER(10),
  NR_SEQ_CHAVE_PARCEL           NUMBER(10),
  CD_POSTAL_EMITENTE            VARCHAR2(15 BYTE),
  NR_CONFIRMACAO                VARCHAR2(5 BYTE),
  NR_IDENT_EMISSOR              VARCHAR2(20 BYTE),
  DS_NOME_EMISSOR               VARCHAR2(255 BYTE),
  NR_SEQ_CHAVE_REG_FIS_EMISSOR  NUMBER(10),
  NR_IDENT_RECEPTOR             VARCHAR2(20 BYTE),
  DS_NOME_RECEPTOR              VARCHAR2(255 BYTE),
  NR_SEQ_CHAVE_PAIS_RECEP       NUMBER(10),
  NR_REG_ID_TRIB_RECEPTOR       VARCHAR2(40 BYTE),
  NR_SEQ_CHAVE_TP_USO_RECEP     NUMBER(10),
  VL_TOT_IMPOSTO_RETIDO         NUMBER(15,6),
  VL_TOT_IMPOSTO_TRANSF         NUMBER(15,6),
  NR_SEQ_NF_IMP_ARQUIVO         NUMBER(10)      NOT NULL,
  NR_SEQ_NF_INTERNO             NUMBER(10),
  CD_LOCAL_ESTOQUE              NUMBER(4),
  CD_CENTRO_CUSTO               NUMBER(8),
  CD_NATUREZA_OPERACAO          NUMBER(4),
  CD_OPERACAO_NF                NUMBER(4),
  CD_CONTA_CONTABIL             VARCHAR2(20 BYTE),
  NR_UUID                       VARCHAR2(255 BYTE) DEFAULT null,
  NR_NUMERO                     NUMBER(30),
  DS_SELLO_SAT                  VARCHAR2(4000 BYTE),
  DS_COMPL_CERT_SAT             VARCHAR2(4000 BYTE),
  NR_CERTIFICADO_SAT            VARCHAR2(40 BYTE),
  DS_QR_CODE                    LONG RAW
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFINOTA_I1 ON TASY.NF_IMP_NOTA
(DS_SERIE, NR_NUMERO, NR_IDENT_EMISSOR, DT_EMISSAO, VL_TOTAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.NFINOTA_NFICHAV_FK_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_FORMA_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFINOTA_NFICHAV_FK2_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFINOTA_NFICHAV_FK3_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_TIPO_COMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFINOTA_NFICHAV_FK4_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_PARCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFINOTA_NFICHAV_FK5_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_REG_FIS_EMISSOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFINOTA_NFICHAV_FK6_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_PAIS_RECEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFINOTA_NFICHAV_FK7_I ON TASY.NF_IMP_NOTA
(NR_SEQ_CHAVE_TP_USO_RECEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFINOTA_PK ON TASY.NF_IMP_NOTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NF_IMP_NOTA ADD (
  CONSTRAINT NFINOTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NF_IMP_NOTA ADD (
  CONSTRAINT NFINOTA_NFICHAV_FK 
 FOREIGN KEY (NR_SEQ_CHAVE_FORMA_PAG) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA),
  CONSTRAINT NFINOTA_NFICHAV_FK2 
 FOREIGN KEY (NR_SEQ_CHAVE_MOEDA) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA),
  CONSTRAINT NFINOTA_NFICHAV_FK3 
 FOREIGN KEY (NR_SEQ_CHAVE_TIPO_COMP) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA),
  CONSTRAINT NFINOTA_NFICHAV_FK4 
 FOREIGN KEY (NR_SEQ_CHAVE_PARCEL) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA),
  CONSTRAINT NFINOTA_NFICHAV_FK5 
 FOREIGN KEY (NR_SEQ_CHAVE_REG_FIS_EMISSOR) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA),
  CONSTRAINT NFINOTA_NFICHAV_FK6 
 FOREIGN KEY (NR_SEQ_CHAVE_PAIS_RECEP) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA),
  CONSTRAINT NFINOTA_NFICHAV_FK7 
 FOREIGN KEY (NR_SEQ_CHAVE_TP_USO_RECEP) 
 REFERENCES TASY.NF_IMP_CHAVE (NR_SEQUENCIA));

GRANT SELECT ON TASY.NF_IMP_NOTA TO NIVEL_1;

