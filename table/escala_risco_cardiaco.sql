ALTER TABLE TASY.ESCALA_RISCO_CARDIACO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_RISCO_CARDIACO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_RISCO_CARDIACO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_PONTUACAO           NUMBER(3),
  IE_IAM_MENOR_6         VARCHAR2(1 BYTE),
  IE_IAM_MAIOR_6         VARCHAR2(1 BYTE),
  IE_ANGINA_III          VARCHAR2(1 BYTE),
  IE_ANGINA_IV           VARCHAR2(1 BYTE),
  IE_EDEMA_PUL_MENOR     VARCHAR2(1 BYTE),
  IE_EDEMA_PUL_MAIOR     VARCHAR2(1 BYTE),
  IE_ESTENOSE            VARCHAR2(1 BYTE),
  IE_RITMO               VARCHAR2(1 BYTE),
  IE_5_EXTRA_SISTOLES    VARCHAR2(1 BYTE),
  IE_RESULT_EXAME        VARCHAR2(1 BYTE),
  IE_IDADE_MAIOR_70      VARCHAR2(1 BYTE),
  IE_CIR_EMERGENCIA      VARCHAR2(1 BYTE),
  IE_FAT_DIABETE         VARCHAR2(1 BYTE),
  IE_FAT_EXTRA_SISTOLES  VARCHAR2(1 BYTE),
  IE_FAT_ONDA_Q          VARCHAR2(1 BYTE),
  IE_FAT_ANGINA          VARCHAR2(1 BYTE),
  IE_FAT_HIST_IAM        VARCHAR2(1 BYTE),
  IE_FAT_HIST_ICC        VARCHAR2(1 BYTE),
  IE_FAT_HIPERTENSAO     VARCHAR2(1 BYTE),
  IE_COMPLEX_CIRURGIA    VARCHAR2(1 BYTE),
  QT_RISCO               NUMBER(3),
  IE_IDADE_MAIOR_70_FAT  VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO    NUMBER(10),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCRICA_ATEPACI_FK_I ON TASY.ESCALA_RISCO_CARDIACO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCRICA_EHRREEL_FK_I ON TASY.ESCALA_RISCO_CARDIACO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCRICA_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCRICA_PESFISI_FK_I ON TASY.ESCALA_RISCO_CARDIACO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCRICA_PK ON TASY.ESCALA_RISCO_CARDIACO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCRICA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_RISCO_CARDIACO_ATUAL
before insert or update ON TASY.ESCALA_RISCO_CARDIACO for each row
declare
qt_pontos_w		number :=0 ;
qt_fator_risco_w	number :=0;

begin

if	(:new.IE_IAM_MENOR_6 = 'A') then
	qt_pontos_w	:=	qt_pontos_w + 10;
elsif	(:new.IE_IAM_MENOR_6 = 'B') then
	qt_pontos_w	:=	qt_pontos_w + 5;
end if;

if	(:new.IE_ANGINA_III = 'A') then
 	qt_pontos_w	:=	qt_pontos_w + 10;
elsif	(:new.IE_ANGINA_III = 'B') then
	qt_pontos_w	:=	qt_pontos_w + 20;
end if;

if	(:new.IE_EDEMA_PUL_MENOR = 'A') then
	qt_pontos_w	:=	qt_pontos_w + 10;
elsif	(:new.IE_EDEMA_PUL_MENOR = 'B') then
	qt_pontos_w	:=	qt_pontos_w + 5;
end if;

if	(:new.IE_ESTENOSE = 'S') then
	qt_pontos_w	:=	qt_pontos_w + 20;
end if;
if	(:new.IE_RITMO = 'S') then
	qt_pontos_w	:=	qt_pontos_w + 5;
end if;
if	(:new.IE_5_EXTRA_SISTOLES = 'S') then
	qt_pontos_w	:=	qt_pontos_w + 5;
end if;
if	(:new.IE_RESULT_EXAME = 'S') then
	qt_pontos_w	:=	qt_pontos_w + 5;
end if;
if	(:new.IE_IDADE_MAIOR_70 = 'S') then
	qt_pontos_w	:=	qt_pontos_w + 5;
end if;
if	(:new.IE_CIR_EMERGENCIA = 'S') then
	qt_pontos_w	:=	qt_pontos_w + 10;
end if;


if	(:new.IE_FAT_DIABETE = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_FAT_EXTRA_SISTOLES = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_FAT_ONDA_Q = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_FAT_ANGINA = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_FAT_HIST_IAM = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_FAT_HIST_ICC = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_FAT_HIPERTENSAO = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;
if	(:new.IE_IDADE_MAIOR_70 = 'S') then
	qt_fator_risco_w := qt_fator_risco_w + 1;
end if;

:new.QT_RISCO	  := qt_fator_risco_w;
:new.QT_PONTUACAO := qt_pontos_w;

end;
/


ALTER TABLE TASY.ESCALA_RISCO_CARDIACO ADD (
  CONSTRAINT ESCRICA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_RISCO_CARDIACO ADD (
  CONSTRAINT ESCRICA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCRICA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCRICA_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_RISCO_CARDIACO TO NIVEL_1;

