ALTER TABLE TASY.SUS_AIH_UTI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_AIH_UTI CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_AIH_UTI
(
  IE_VERSAO_AIH        VARCHAR2(20 BYTE)        NOT NULL,
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_VERSAO            VARCHAR2(2 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_SH                NUMBER(15,2)             NOT NULL,
  VL_SP                NUMBER(15,2)             NOT NULL,
  VL_SADT              NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSAIHU_PK ON TASY.SUS_AIH_UTI
(IE_VERSAO_AIH, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIHU_PK
  MONITORING USAGE;


ALTER TABLE TASY.SUS_AIH_UTI ADD (
  CONSTRAINT SUSAIHU_PK
 PRIMARY KEY
 (IE_VERSAO_AIH, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUS_AIH_UTI TO NIVEL_1;

