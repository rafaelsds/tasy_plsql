ALTER TABLE TASY.PROJ_REUNIAO_PARTICIPANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_REUNIAO_PARTICIPANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_REUNIAO_PARTICIPANTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REUNIAO       NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PARTICIPANTE      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROREUPA_PESFISI_FK_I ON TASY.PROJ_REUNIAO_PARTICIPANTE
(CD_PARTICIPANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROREUPA_PK ON TASY.PROJ_REUNIAO_PARTICIPANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREUPA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROREUPA_PROREU_FK_I ON TASY.PROJ_REUNIAO_PARTICIPANTE
(NR_SEQ_REUNIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROREUPA_PROREU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_REUNIAO_PARTICIPANTE ADD (
  CONSTRAINT PROREUPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_REUNIAO_PARTICIPANTE ADD (
  CONSTRAINT PROREUPA_PESFISI_FK 
 FOREIGN KEY (CD_PARTICIPANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROREUPA_PROREU_FK 
 FOREIGN KEY (NR_SEQ_REUNIAO) 
 REFERENCES TASY.PROJ_REUNIAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_REUNIAO_PARTICIPANTE TO NIVEL_1;

