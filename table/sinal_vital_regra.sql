ALTER TABLE TASY.SINAL_VITAL_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SINAL_VITAL_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SINAL_VITAL_REGRA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_SINAL          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  QT_MINIMO             NUMBER(15,4),
  VL_MAXIMO             NUMBER(15,4)            NOT NULL,
  QT_MIN_AVISO          NUMBER(15,4),
  QT_MAX_AVISO          NUMBER(15,4),
  QT_IDADE_MIN          NUMBER(6,2),
  QT_IDADE_MAX          NUMBER(6,2),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  QT_IDADE_MIN_DIAS     NUMBER(6,2),
  QT_IDADE_MAX_DIAS     NUMBER(6,2),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_ESCALA_DOR         VARCHAR2(5 BYTE),
  DS_MENSAGEM_ALERTA    VARCHAR2(255 BYTE),
  DS_MENSAGEM_BLOQUEIO  VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  CD_PERFIL             NUMBER(5),
  IE_SEXO               VARCHAR2(1 BYTE),
  IE_SITIO              VARCHAR2(1 BYTE),
  NR_BAIXA_SEVERA_SA    NUMBER(5,2),
  NR_BAIXA_MODERADA_SA  NUMBER(5,2),
  NR_ALTA_SEVERA_SA     NUMBER(5,2),
  NR_ALTA_MODERADA_SA   NUMBER(5,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SINVIRE_ESTABEL_FK_I ON TASY.SINAL_VITAL_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SINVIRE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SINVIRE_PERFIL_FK_I ON TASY.SINAL_VITAL_REGRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SINVIRE_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SINVIRE_PK ON TASY.SINAL_VITAL_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SINVIRE_SETATEN_FK_I ON TASY.SINAL_VITAL_REGRA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SINVIRE_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SINVIRE_SINVITA_FK_I ON TASY.SINAL_VITAL_REGRA
(NR_SEQ_SINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SINAL_VITAL_REGRA ADD (
  CONSTRAINT SINVIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SINAL_VITAL_REGRA ADD (
  CONSTRAINT SINVIRE_SINVITA_FK 
 FOREIGN KEY (NR_SEQ_SINAL) 
 REFERENCES TASY.SINAL_VITAL (NR_SEQUENCIA),
  CONSTRAINT SINVIRE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SINVIRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SINVIRE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.SINAL_VITAL_REGRA TO NIVEL_1;

