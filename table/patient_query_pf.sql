ALTER TABLE TASY.PATIENT_QUERY_PF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PATIENT_QUERY_PF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PATIENT_QUERY_PF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_PATIENT_IDS_ID    VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_ISSUER        NUMBER(10),
  DS_PATIENT_OID_NAME  VARCHAR2(255 BYTE),
  DS_PATIENT_OID_CODE  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAQUEPF_PESFISI_FK_I ON TASY.PATIENT_QUERY_PF
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAQUEPF_PK ON TASY.PATIENT_QUERY_PF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PATIENT_QUERY_PF ADD (
  CONSTRAINT PAQUEPF_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PATIENT_QUERY_PF ADD (
  CONSTRAINT PAQUEPF_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PATIENT_QUERY_PF TO NIVEL_1;

