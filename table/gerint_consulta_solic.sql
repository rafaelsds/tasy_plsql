ALTER TABLE TASY.GERINT_CONSULTA_SOLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GERINT_CONSULTA_SOLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.GERINT_CONSULTA_SOLIC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_PROTOCOLO_SOLICITACAO  VARCHAR2(12 BYTE),
  DT_SOLICITACAO            DATE,
  NM_UNIDADE_ENCAMINHADA    VARCHAR2(255 BYTE),
  NR_CNES_UNID_ENCAMIN      VARCHAR2(255 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_EVENTO             NUMBER(10),
  DS_SITUACAO               VARCHAR2(40 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GECOSO_GEEVIN_FK_I ON TASY.GERINT_CONSULTA_SOLIC
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GECOSO_GESOIN_FK_I ON TASY.GERINT_CONSULTA_SOLIC
(NR_PROTOCOLO_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GECOSO_PK ON TASY.GERINT_CONSULTA_SOLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GERINT_CONSULTA_SOLIC ADD (
  CONSTRAINT GECOSO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GERINT_CONSULTA_SOLIC ADD (
  CONSTRAINT GECOSO_GEEVIN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.GERINT_EVENTO_INTEGRACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.GERINT_CONSULTA_SOLIC TO NIVEL_1;

