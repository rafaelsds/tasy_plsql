ALTER TABLE TASY.SUP_INT_NF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_NF CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_NF
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_FORMA_INTEGRACAO     VARCHAR2(15 BYTE)     NOT NULL,
  DT_LIBERACAO            DATE,
  DT_LEITURA              DATE,
  DT_CONFIRMA_INTEGRACAO  DATE,
  NR_SEQ_NOTA             NUMBER(10)            NOT NULL,
  NR_DOCUMENTO_EXTERNO    VARCHAR2(80 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_NOTA            VARCHAR2(3 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  NR_NOTA_FISCAL          VARCHAR2(255 BYTE)    NOT NULL,
  DT_EMISSAO              DATE                  NOT NULL,
  CD_CGC_EMITENTE         VARCHAR2(14 BYTE),
  CD_CGC                  VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  CD_SERIE_NF             VARCHAR2(255 BYTE)    NOT NULL,
  CD_NATUREZA_OPERACAO    NUMBER(4),
  CD_CONDICAO_PAGAMENTO   NUMBER(10),
  CD_OPERACAO_NF          NUMBER(4)             NOT NULL,
  VL_SEGURO               NUMBER(13,2),
  VL_DESPESA_ACESSORIA    NUMBER(13,2),
  VL_FRETE                NUMBER(13,2),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINNOF_PK ON TASY.SUP_INT_NF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINNOF_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_nf_atual
before update ON TASY.SUP_INT_NF for each row
declare
qt_existe_erros_w		number(10);
nr_seq_nota_w		number(10);
qt_existe_w		number(10);
begin

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao
where	cd_estabelecimento = :new.cd_estabelecimento
and	ie_evento = 'NF'
and	ie_forma = 'R'
and	ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.ie_tipo_nota in ('EF','EN')) and
	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) and
	(:old.ie_forma_integracao = 'R') then
	begin

	:new.dt_leitura	:= sysdate;

	consiste_sup_int_nf(:new.nr_sequencia,
			:new.cd_cgc,
			:new.cd_cgc_emitente,
			:new.cd_condicao_pagamento,
			:new.cd_estabelecimento,
			:new.cd_natureza_operacao,
			:new.cd_operacao_nf,
			:new.cd_pessoa_fisica,
			:new.cd_serie_nf,
			:new.ie_tipo_nota);

	select	count(*)
	into	qt_existe_erros_w
	from	sup_int_nf_consist
	where	nr_sequencia = :new.nr_sequencia;

	if	(qt_existe_erros_w = 0) then
		begin

		gerar_nota_fiscal_integ(
			:new.cd_cgc,
			:new.cd_cgc_emitente,
			:new.cd_condicao_pagamento,
			:new.cd_estabelecimento,
			:new.cd_natureza_operacao,
			:new.cd_operacao_nf,
			:new.cd_pessoa_fisica,
			:new.cd_serie_nf,
			:new.ds_observacao,
			:new.ie_tipo_nota,
			:new.nr_documento_externo,
			:new.nr_nota_fiscal,
			:new.vl_despesa_acessoria,
			:new.vl_frete,
			:new.vl_seguro,
			:new.nr_sequencia,
			:new.dt_emissao,
			nr_seq_nota_w);

		if	(nr_seq_nota_w is not null) then
			begin

			:new.dt_confirma_integracao	:= sysdate;
			:new.nr_seq_nota			:= nr_seq_nota_w;

			end;
		end if;

		end;
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.SUP_INT_NF ADD (
  CONSTRAINT SUINNOF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_NF TO NIVEL_1;

