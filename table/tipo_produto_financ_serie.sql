ALTER TABLE TASY.TIPO_PRODUTO_FINANC_SERIE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_PRODUTO_FINANC_SERIE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_PRODUTO_FINANC_SERIE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  CD_SERIE_NF          VARCHAR2(5 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPRFSE_ESTABEL_FK_I ON TASY.TIPO_PRODUTO_FINANC_SERIE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRFSE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIPRFSE_PK ON TASY.TIPO_PRODUTO_FINANC_SERIE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRFSE_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIPRFSE_SERNOFI_FK_I ON TASY.TIPO_PRODUTO_FINANC_SERIE
(CD_SERIE_NF, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRFSE_SERNOFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIPRFSE_TIPPDFI_FK_I ON TASY.TIPO_PRODUTO_FINANC_SERIE
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRFSE_TIPPDFI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TIPO_PRODUTO_FINANC_SERIE ADD (
  CONSTRAINT TIPRFSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_PRODUTO_FINANC_SERIE ADD (
  CONSTRAINT TIPRFSE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TIPRFSE_SERNOFI_FK 
 FOREIGN KEY (CD_SERIE_NF, CD_ESTABELECIMENTO) 
 REFERENCES TASY.SERIE_NOTA_FISCAL (CD_SERIE_NF,CD_ESTABELECIMENTO),
  CONSTRAINT TIPRFSE_TIPPDFI_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_PRODUTO_FINANC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TIPO_PRODUTO_FINANC_SERIE TO NIVEL_1;

