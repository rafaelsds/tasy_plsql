ALTER TABLE TASY.LIB_ITENS_REGRA_SV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LIB_ITENS_REGRA_SV CASCADE CONSTRAINTS;

CREATE TABLE TASY.LIB_ITENS_REGRA_SV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TEMPO   NUMBER(10),
  CD_INTEGRACAO        VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LIIRSV_LIISPH_FK_I ON TASY.LIB_ITENS_REGRA_SV
(NR_SEQ_REGRA_TEMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LIIRSV_PK ON TASY.LIB_ITENS_REGRA_SV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LIB_ITENS_REGRA_SV ADD (
  CONSTRAINT LIIRSV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LIB_ITENS_REGRA_SV ADD (
  CONSTRAINT LIIRSV_LIISPH_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TEMPO) 
 REFERENCES TASY.LIB_ITENS_SV_PHILIPS (NR_SEQUENCIA));

GRANT SELECT ON TASY.LIB_ITENS_REGRA_SV TO NIVEL_1;

