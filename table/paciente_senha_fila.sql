ALTER TABLE TASY.PACIENTE_SENHA_FILA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_SENHA_FILA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_SENHA_FILA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_SENHA_GERADA              NUMBER(10),
  DT_GERACAO_SENHA             DATE             NOT NULL,
  NR_SEQ_FILA_SENHA            NUMBER(10)       NOT NULL,
  NR_SEQ_FILA_SENHA_ORIGEM     NUMBER(10)       NOT NULL,
  DT_VINCULACAO_SENHA          DATE,
  IE_RECHAMADA                 VARCHAR2(1 BYTE),
  DT_PRIMEIRA_CHAMADA          DATE,
  DT_CHAMADA                   DATE,
  DS_MAQUINA_CHAMADA           VARCHAR2(100 BYTE),
  DT_NOVA_CHAMADA              DATE,
  DT_INUTILIZACAO              DATE,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  NR_SEQ_LOCAL_SENHA           NUMBER(10),
  QT_CHAMADAS                  NUMBER(10),
  DT_CHAMADA_PA                DATE,
  NM_PACIENTE                  VARCHAR2(255 BYTE),
  DT_UTILIZACAO                DATE,
  DT_VISUALIZACAO_MONITOR      DATE,
  DT_FIM_ATENDIMENTO           DATE,
  DT_INICIO_ATENDIMENTO        DATE,
  NM_USUARIO_CHAMADA           VARCHAR2(15 BYTE),
  DT_CHAMADA_GE                DATE,
  DT_ENTRADA_FILA              DATE,
  NR_PRIORIDADE                NUMBER(5),
  NR_SEQ_MOTIVO_INUTILIZACAO   NUMBER(10),
  DT_RETORNO_SENHA             DATE,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  NM_USUARIO_INUTILIZACAO      VARCHAR2(15 BYTE),
  DS_MAQUINA_INUTILIZACAO      VARCHAR2(100 BYTE),
  DS_MAQUINA_CHAMADA_PESQUISA  VARCHAR2(100 BYTE),
  IE_FORMA_CHAMADA             VARCHAR2(1 BYTE),
  IE_SISTEMA_EXTERNO           VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACSEFI_ESTABEL_FK_I ON TASY.PACIENTE_SENHA_FILA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_FILESPS_FK_I ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_FILA_SENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_FILESPS_FK2_I ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_FILA_SENHA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSEFI_FILESPS_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSEFI_I1 ON TASY.PACIENTE_SENHA_FILA
(DT_GERACAO_SENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I10 ON TASY.PACIENTE_SENHA_FILA
(CD_ESTABELECIMENTO, DT_GERACAO_SENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I11 ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_FILA_SENHA, DT_UTILIZACAO, DT_INUTILIZACAO, DT_INICIO_ATENDIMENTO, DT_FIM_ATENDIMENTO, 
1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I2 ON TASY.PACIENTE_SENHA_FILA
(DT_ENTRADA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I3 ON TASY.PACIENTE_SENHA_FILA
(DS_MAQUINA_CHAMADA_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I4 ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_FILA_SENHA, DT_UTILIZACAO, DT_INUTILIZACAO, DT_INICIO_ATENDIMENTO, DT_FIM_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I5 ON TASY.PACIENTE_SENHA_FILA
(DT_GERACAO_SENHA, NVL("DT_CHAMADA","DT_PRIMEIRA_CHAMADA"), CD_ESTABELECIMENTO, DT_INUTILIZACAO, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I6 ON TASY.PACIENTE_SENHA_FILA
(DS_MAQUINA_CHAMADA_PESQUISA, NVL("DT_CHAMADA","DT_PRIMEIRA_CHAMADA"), DT_UTILIZACAO, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I7 ON TASY.PACIENTE_SENHA_FILA
(DS_MAQUINA_CHAMADA_PESQUISA, DT_UTILIZACAO, DT_FIM_ATENDIMENTO, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I8 ON TASY.PACIENTE_SENHA_FILA
(DT_GERACAO_SENHA, DT_CHAMADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_I9 ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_FILA_SENHA, DT_UTILIZACAO, DT_INUTILIZACAO, DT_FIM_ATENDIMENTO, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_MAQLOSE_FK_I ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_LOCAL_SENHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACSEFI_MOINUSE_FK_I ON TASY.PACIENTE_SENHA_FILA
(NR_SEQ_MOTIVO_INUTILIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACSEFI_MOINUSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACSEFI_PESFISI_FK_I ON TASY.PACIENTE_SENHA_FILA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACSEFI_PK ON TASY.PACIENTE_SENHA_FILA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.paciente_senha_fila_atual_pr
before update or insert ON TASY.PACIENTE_SENHA_FILA for each row
declare
qt_simultaneas_w	fila_espera_senha.qt_simultaneas%type;
qt_senha_simultaneas_w	number(10);
ds_fila_w		fila_espera_senha.ds_fila%type;

pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger <> 'N') then

	select	max(qt_simultaneas),
		max(ds_fila)
	into	qt_simultaneas_w,
		ds_fila_w
	from	fila_espera_senha
	where	nr_sequencia = :new.nr_seq_fila_senha;

	if	(qt_simultaneas_w is not null and
		:new.nr_seq_fila_senha <> :old.nr_seq_fila_senha) then

		begin
			select	count(a.nr_sequencia)
			into	qt_senha_simultaneas_w
			from	paciente_senha_fila a
			where	a.nr_seq_fila_senha = :new.nr_seq_fila_senha
			and	dt_inutilizacao is null
			and	dt_fim_atendimento is null
			and	dt_utilizacao is null;
		exception
			when	others then
				qt_senha_simultaneas_w := 0;
		end;

		if	(qt_senha_simultaneas_w >= qt_simultaneas_w) then
			commit;
			wheb_mensagem_pck.exibir_mensagem_abort(1034121, 'FILA='|| ds_fila_w);
		end if;
	end if;
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_senha_fila_status_upd
before update ON TASY.PACIENTE_SENHA_FILA for each row
declare
ie_canceled_reason_changed_w varchar2(1);

begin

if (:new.ie_sistema_externo = 'WHY') then
	ie_canceled_reason_changed_w := 'N';

	if (nvl(:new.dt_inutilizacao, to_date('01/01/3300', 'dd/mm/yyyy')) <> (nvl(:old.dt_inutilizacao, to_date('01/01/3300', 'dd/mm/yyyy')))) then
		ie_canceled_reason_changed_w := 'S';
	end if;

    if (:old.dt_primeira_chamada is null and (nvl(:new.dt_primeira_chamada, to_date('01/01/3300', 'dd/mm/yyyy')) <> (nvl(:old.dt_primeira_chamada, to_date('01/01/3300', 'dd/mm/yyyy'))))) then
        bft_request_senha_status(:new.cd_estabelecimento, :new.nr_seq_fila_senha, :new.cd_senha_gerada, 'processing', :new.nm_usuario, ie_canceled_reason_changed_w);
	end if;

	if (:old.dt_fim_atendimento is null and (nvl(:new.dt_fim_atendimento, to_date('01/01/3300', 'dd/mm/yyyy')) <> (nvl(:old.dt_fim_atendimento, to_date('01/01/3300', 'dd/mm/yyyy'))))) then
		bft_request_senha_status(:new.cd_estabelecimento, :new.nr_seq_fila_senha, :new.cd_senha_gerada, 'completed', :new.nm_usuario, ie_canceled_reason_changed_w);
	end if;

	if (:old.dt_inutilizacao is null and (nvl(:new.dt_inutilizacao, to_date('01/01/3300', 'dd/mm/yyyy')) <> (nvl(:old.dt_inutilizacao, to_date('01/01/3300', 'dd/mm/yyyy'))))) then
        bft_request_senha_status(:new.cd_estabelecimento, :new.nr_seq_fila_senha, :new.cd_senha_gerada, 'canceled', :new.nm_usuario, ie_canceled_reason_changed_w);
	end if;

	if (:old.dt_retorno_senha is null and (nvl(:new.dt_retorno_senha, to_date('01/01/3300', 'dd/mm/yyyy')) <> (nvl(:old.dt_retorno_senha, to_date('01/01/3300', 'dd/mm/yyyy'))))) then
		bft_request_senha_status(:new.cd_estabelecimento, :new.nr_seq_fila_senha, :new.cd_senha_gerada, 'processing', :new.nm_usuario, ie_canceled_reason_changed_w);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_senha_fila_bfinsert
before insert ON TASY.PACIENTE_SENHA_FILA for each row
declare

	nr_prioridade_w	number(5);
begin

-- caso a senha seja gerada com um atendimento vinculado, adiciona 1 segundo � data de entrada na fila, simulando a a��o de transfer�ncia de fila.
if	(:new.dt_vinculacao_senha is not null) then
	:new.dt_entrada_fila := :new.dt_geracao_senha + (1/24/60/60);
else
	:new.dt_entrada_fila := :new.dt_geracao_senha;
end if;

-- Caso a senha seja gerada com uma pessoa fisica vinculada, altera-a para priorit�ria de acordo com a Regra de prioridade de senhas (Shift+F11)
if	(:new.cd_pessoa_fisica is not null) then
	if	(OBTER_SE_PAC_SENHA_PRIOR_REGRA(:new.cd_pessoa_fisica) = 'S') then
		select	max(nvl(nr_prioridade_padrao, nr_prioridade))
		into	nr_prioridade_w
		from	fila_espera_senha
		where	nr_sequencia = :new.nr_seq_fila_senha;

		if	(nr_prioridade_w is not null) then
			:new.nr_prioridade := nr_prioridade_w - 1;
		end if;
	end if;
end if;

if	(:new.nr_prioridade is null) then

	select	max(nr_prioridade)
	into	nr_prioridade_w
	from	fila_espera_senha
	where	nr_sequencia = :new.nr_seq_fila_senha;

	if	(nr_prioridade_w is not null) then
		:new.nr_prioridade := nr_prioridade_w;
	end if;

end if;

if	(:new.DS_MAQUINA_CHAMADA is not null) then
	:new.DS_MAQUINA_CHAMADA_PESQUISA := padronizar_nome(:new.DS_MAQUINA_CHAMADA);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_senha_fila_aftinsert
after insert ON TASY.PACIENTE_SENHA_FILA for each row
declare
nr_seq_mov_senha_w	number(10);

begin

select	movimentacao_senha_fila_seq.nextval
into	nr_seq_mov_senha_w
from	dual;

insert	into movimentacao_senha_fila (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_pac_senha_fila,
	nr_seq_fila_espera,
	dt_entrada)
values (nr_seq_mov_senha_w,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.nr_sequencia,
	:new.nr_seq_fila_senha,
	sysdate);

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_senha_fila_update
before update ON TASY.PACIENTE_SENHA_FILA for each row
declare
nr_seq_mov_senha_w		number(10);
nr_seq_movimentacao_senha_w		movimentacao_senha_fila.nr_sequencia%type;
nr_seq_fila_espera_destino_w	number(10);
ie_transfere_w			varchar2(1);
ie_limpa_inicio_fim_atend_w	varchar2(1);
ie_ajusta_fila_origem_w		varchar2(1);
ie_inicia_atend_chamar_w	varchar2(1);
ie_finaliza_atend_vinc_w	varchar2(1);
ie_reabilita_senha_w	varchar2(1);
ie_utiliza_lista_senha_w	varchar2(1);
nr_sequencia_mov_ant_w		number(10);
nr_prioridade_w			number(10);
nr_fila_destino_w			number(10);

begin

if	(((((nvl(:new.dt_chamada_pa, to_date('01/01/3300', 'dd/mm/yyyy'))) = (nvl(:old.dt_chamada_pa, to_date('01/01/3300', 'dd/mm/yyyy'))))) and
	((nvl(:new.dt_chamada, to_date('01/01/3300', 'dd/mm/yyyy')) = (nvl(:old.dt_chamada, to_date('01/01/3300', 'dd/mm/yyyy'))))) and
	((nvl(:new.dt_primeira_chamada, to_date('01/01/3300', 'dd/mm/yyyy')) = (nvl(:old.dt_primeira_chamada, to_date('01/01/3300', 'dd/mm/yyyy')))))) and
	((nvl(:new.ds_maquina_chamada, 'X') <> ((nvl(:old.ds_maquina_chamada, 'X')))))) then
	Wheb_mensagem_pck.exibir_mensagem_abort(322428);
end if;



obter_param_usuario(10021, 59, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_transfere_w);
obter_param_usuario(10021, 82, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_limpa_inicio_fim_atend_w);
obter_param_usuario(10021, 83, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_ajusta_fila_origem_w);
obter_param_usuario(10021, 84, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_inicia_atend_chamar_w);
obter_param_usuario(10021, 87, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_finaliza_atend_vinc_w);
obter_param_usuario(10021, 90, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_reabilita_senha_w);
obter_param_usuario(10021, 99, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_utiliza_lista_senha_w);


if	((nvl(:new.dt_chamada, 			sysdate+1) <> nvl(:old.dt_chamada, 			sysdate+1)) or
	(nvl(:new.dt_chamada_pa, 		sysdate+1) <> nvl(:old.dt_chamada_pa, 		sysdate+1)) or
	(nvl(:new.dt_primeira_chamada, 	sysdate+1) <> nvl(:old.dt_primeira_chamada, 	sysdate+1))) then
	:new.ie_rechamada := null;
end if;

if	(ie_inicia_atend_chamar_w = 'S') and
	(:old.dt_inicio_atendimento is null) and
	((:new.dt_chamada is not null 		and (nvl(:new.dt_chamada, 		sysdate+1) <> nvl(:old.dt_chamada, 		sysdate+1))) or
	 (:new.dt_chamada_pa is not null	and (nvl(:new.dt_chamada_pa, 		sysdate+1) <> nvl(:old.dt_chamada_pa, 		sysdate+1))) or
	 (:new.dt_primeira_chamada is not null	and (nvl(:new.dt_primeira_chamada,	sysdate+1) <> nvl(:old.dt_primeira_chamada, 	sysdate+1)))) then
	:new.dt_inicio_atendimento := sysdate;
end if;

if	(ie_finaliza_atend_vinc_w = 'S') and
	(:old.dt_vinculacao_senha is null) and
	(:new.dt_vinculacao_senha is not null) then
	:new.dt_fim_atendimento := sysdate;
end if;

if	((:old.dt_inicio_atendimento is null) or
	 (:old.dt_inicio_atendimento <> :new.dt_inicio_atendimento)) and
	(:new.dt_inicio_atendimento is not null) then

	select	atendimentos_senha_seq.nextval
	into	nr_seq_mov_senha_w
	from	dual;

	select	max(nr_sequencia)
	into	nr_seq_movimentacao_senha_w
	from	movimentacao_senha_fila
	where	nr_seq_pac_senha_fila = :new.nr_sequencia
	and		dt_saida is null;

	insert	into atendimentos_senha (
		nr_sequencia,
		dt_inicio_atendimento,
		nm_usuario_inicio,
		dt_fim_atendimento,
		nm_usuario_fim,
		nr_seq_pac_senha_fila,
		nr_seq_local_chamada,
		nr_seq_fila_espera,
		nr_seq_mov_senha_fila)
	values (nr_seq_mov_senha_w,
		:new.dt_inicio_atendimento,
		:new.nm_usuario,
		null,
		null,
		:new.nr_sequencia,
		:new.nr_seq_local_senha,
		:new.nr_seq_fila_senha,
		nr_seq_movimentacao_senha_w);

elsif	(:old.dt_fim_atendimento is null) and
	(:new.dt_fim_atendimento is not null) then

	-- Verificar se existe registro dessa senha na tabela de log
	begin
		select	nr_sequencia
		into	nr_seq_mov_senha_w
		from	atendimentos_senha
		where	nr_seq_pac_senha_fila = :new.nr_sequencia
		and	dt_fim_atendimento is null
		and	rownum = 1;
	exception
		when	no_data_found then
			nr_seq_mov_senha_w := 0;
	end;

	if	(nr_seq_mov_senha_w > 0) then
		/* Normalmente ira entrar neste if, para finalizar o atendimento da senha, o registro de inicio ja deveria ter sido criado (em uma transacao anterior) */
		update	atendimentos_senha
		set	dt_fim_atendimento = :new.dt_fim_atendimento,
			nm_usuario_fim = :new.nm_usuario
		where	nr_seq_pac_senha_fila = :new.nr_sequencia
		and	dt_fim_atendimento is null;
	end if;

	-- Tratar o campo "fila de destino" do cadastro de filas
	select	max(nr_seq_fila_espera_destino)
	into	nr_seq_fila_espera_destino_w
	from	fila_espera_senha
	where	nr_sequencia = :new.nr_seq_fila_senha;

	if	(nr_seq_fila_espera_destino_w is not null) then
		:new.nr_seq_fila_senha := nr_seq_fila_espera_destino_w;
	else
	-- caso o usuario possua o parametro 161 cadastrado, e a fila nao tenha uma fila destino  informada, a fila destino sera igual ao valor informado no parametro
		obter_param_usuario(10021, 161, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_fila_destino_w);
		if  ((nr_fila_destino_w > 0) and  (:old.nr_seq_fila_senha <> nr_fila_destino_w)) then
			:new.nr_seq_fila_senha := nr_fila_destino_w;
		end if;
	end if;

end if;

	-- se mudou de fila OU se alocou a senha na mesma fila.
If	(	nvl(:old.nr_seq_fila_senha, 0) <> nvl(:new.nr_seq_fila_senha, 0)) or
	(	:old.dt_entrada_fila is not null and
		:new.dt_entrada_fila is not null and
		:old.dt_entrada_fila <> :new.dt_entrada_fila) then

	if	((nvl(:old.dt_retorno_senha, sysdate) = nvl(:new.dt_retorno_senha, sysdate)) and
                 ((:new.dt_entrada_fila = :old.dt_entrada_fila)))	then
		:new.dt_entrada_fila := sysdate;
	end if;

	-- Atualizar fila da triagem
	update 	triagem_pronto_atend
	set 	nr_seq_pac_fila = :new.nr_seq_fila_senha
	where 	nr_seq_fila_senha = :new.nr_sequencia;

	-- Limpar informacao da chamada, dependendo do parametro 59
	if	(ie_transfere_w <> 'H') and
		((ie_transfere_w = 'S') or
		 (:new.dt_vinculacao_senha is null)) then
		:new.dt_primeira_chamada	:= null;
		:new.ds_maquina_chamada 	:= null;
		:new.nr_seq_local_senha 	:= null;
		:new.dt_chamada	   		:= null;
		:new.nm_usuario_chamada 	:= null;
		:new.dt_nova_chamada		:= null;
		:new.dt_chamada_pa		:= null;
	end if;

	-- Limpar informacao de inicio e fim de atendimento dependendo do parametro 82
	If	(ie_limpa_inicio_fim_atend_w = 'S') then
		:new.dt_inicio_atendimento	:= null;
		:new.dt_fim_atendimento		:= null;
	end if;

	-- Limpar informacao de fila de origem dependendo do parametro 83
	if	(ie_ajusta_fila_origem_w = 'N') then
		:new.nr_seq_fila_senha_origem := :new.nr_seq_fila_senha;
	end if;

	-- Limpar informacao de nao respondeu dependendo do parametro 90
	if	(ie_reabilita_senha_w = 'S') then
		:new.qt_chamadas			:= null;
		:new.ie_rechamada			:= null;
	end if;

	--informar a data de saida
	select	max(nr_sequencia)
	into	nr_sequencia_mov_ant_w
	from 	MOVIMENTACAO_SENHA_FILA
	where	NR_SEQ_PAC_SENHA_FILA = :old.nr_sequencia;

	if ( nr_sequencia_mov_ant_w is not null)	then
		update	MOVIMENTACAO_SENHA_FILA
		set	dt_saida = sysdate
		where	NR_SEQUENCIA = nr_sequencia_mov_ant_w;
	end if;


	-- Gravar log da movimentacao
	select	movimentacao_senha_fila_seq.nextval
	into	nr_seq_mov_senha_w
	from	dual;

	insert	into movimentacao_senha_fila (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_pac_senha_fila,
		nr_seq_fila_espera,
		dt_entrada)
	values (
		nr_seq_mov_senha_w,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		:new.nr_seq_fila_senha,
		sysdate);

	-- se tiver atendimentos_senha com inicio de atendimento mas sem fim atendimento, registra o fim.

	-- Verificar se existe registro dessa senha na tabela de log
	begin
		select	nr_sequencia
		into	nr_seq_mov_senha_w
		from	atendimentos_senha
		where	nr_seq_pac_senha_fila = :new.nr_sequencia
		and	dt_fim_atendimento is null
		and	rownum = 1;
	exception
		when	no_data_found then
			nr_seq_mov_senha_w := 0;
	end;

	if	(nr_seq_mov_senha_w > 0) then
		/* finalizar o atendimento da senha, o registro de inicio ja deveria ter sido criado (em uma transacao anterior) */
		update	atendimentos_senha
		set	dt_fim_atendimento = nvl(:new.dt_fim_atendimento,sysdate),
			nm_usuario_fim = :new.nm_usuario
		where	nr_seq_pac_senha_fila = :new.nr_sequencia
		and	dt_fim_atendimento is null;
	end if;


end if;

if	(:old.dt_inutilizacao is null) and
	(:new.dt_inutilizacao is not null) then
	insert into log_mov(dt_atualizacao,
				nm_usuario,
				ds_log,
				cd_log)
			values(sysdate,
				:new.nm_usuario,
				substr(dbms_utility.format_call_stack,1,1800),
				7832548);

end if;


if (nvl(:old.nm_usuario_chamada, 'X') <> nvl(:new.nm_usuario_chamada, 'X')) then
  update movimentacao_senha_fila
  set NM_USUARIO_CHAMADA = :new.nm_usuario_chamada
  where NR_SEQ_PAC_SENHA_FILA = :new.nr_sequencia
  and NR_SEQ_FILA_ESPERA = :new.nr_seq_fila_senha
  and DT_SAIDA is null;
end if;


-- Gravar log de chamadas
if	(((nvl(:new.dt_chamada_pa, to_date('01/01/3300', 'dd/mm/yyyy')) <> nvl(:old.dt_chamada_pa, to_date('01/01/3300', 'dd/mm/yyyy'))) and :new.dt_chamada_pa is not null) or
	((nvl(:new.dt_chamada, to_date('01/01/3300', 'dd/mm/yyyy')) <> nvl(:old.dt_chamada, to_date('01/01/3300', 'dd/mm/yyyy'))) and :new.dt_chamada is not null) or
	((nvl(:new.dt_primeira_chamada, to_date('01/01/3300', 'dd/mm/yyyy')) <> nvl(:old.dt_primeira_chamada, to_date('01/01/3300', 'dd/mm/yyyy'))) and :new.dt_primeira_chamada is not null)) then
	gerar_hist_geren_senhas(:new.nr_sequencia,:new.ds_maquina_chamada,:new.nm_usuario,:new.ie_forma_chamada);
	gerar_senha_painel_led(:new.nr_sequencia,:new.nr_seq_local_senha,:new.nm_usuario);

	if (ie_utiliza_lista_senha_w = 'S'
		or ie_utiliza_lista_senha_w = 'T') then
		gerar_senha_fila_visualizacao(:new.nr_sequencia,:new.nr_seq_local_senha,:new.nm_usuario);
	end if;
end if;

if  (nvl(:new.ds_maquina_chamada, 'X') <> nvl(:old.ds_maquina_chamada, 'X')) or (:new.ds_maquina_chamada_pesquisa is null) then
  :new.ds_maquina_chamada_pesquisa := padronizar_nome(:new.ds_maquina_chamada);
end if;

-- Ao vincular a senha com uma pessoa fisica, altera a senha para prioritaria de acordo com a Regra de prioridade de senhas (Shift+F11)
if	(:old.cd_pessoa_fisica is null and
	 :new.cd_pessoa_fisica is not null) then
	if	(OBTER_SE_PAC_SENHA_PRIOR_REGRA(:new.cd_pessoa_fisica) = 'S') then
		select	max(nvl(nr_prioridade_padrao, nr_prioridade))
		into	nr_prioridade_w
		from	fila_espera_senha
		where	nr_sequencia = :new.nr_seq_fila_senha;

		if	(nr_prioridade_w is not null) then
			:new.nr_prioridade := nr_prioridade_w - 1;
		end if;
	end if;
end if;

--Gravar log de senhas da fila
if(nvl(:old.cd_pessoa_fisica, 'X') <> nvl(:new.cd_pessoa_fisica, 'X')) then
	insert	into paciente_senha_fila_log (
		nr_sequencia,
		nr_seq_senha,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_funcao_ativa,
		ds_log,
		ds_stack)
	values (
		paciente_senha_fila_log_seq.nextval,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		wheb_usuario_pck.get_cd_funcao,
		wheb_mensagem_pck.get_texto(1017760,'DE='||obter_nome_pessoa_fisica(nvl(:old.cd_pessoa_fisica, 'X'), '')||';PARA='||obter_nome_pessoa_fisica(nvl(:new.cd_pessoa_fisica, 'X'), '')),
		substr(dbms_utility.format_call_stack,1,4000));
end if;

end;
/


ALTER TABLE TASY.PACIENTE_SENHA_FILA ADD (
  CONSTRAINT PACSEFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_SENHA_FILA ADD (
  CONSTRAINT PACSEFI_MOINUSE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INUTILIZACAO) 
 REFERENCES TASY.MOTIVO_INUTILIZACAO_SENHA (NR_SEQUENCIA),
  CONSTRAINT PACSEFI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACSEFI_FILESPS_FK 
 FOREIGN KEY (NR_SEQ_FILA_SENHA) 
 REFERENCES TASY.FILA_ESPERA_SENHA (NR_SEQUENCIA),
  CONSTRAINT PACSEFI_FILESPS_FK2 
 FOREIGN KEY (NR_SEQ_FILA_SENHA_ORIGEM) 
 REFERENCES TASY.FILA_ESPERA_SENHA (NR_SEQUENCIA),
  CONSTRAINT PACSEFI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PACSEFI_MAQLOSE_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_SENHA) 
 REFERENCES TASY.MAQUINA_LOCAL_SENHA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_SENHA_FILA TO NIVEL_1;

