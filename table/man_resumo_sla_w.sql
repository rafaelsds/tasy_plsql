DROP TABLE TASY.MAN_RESUMO_SLA_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_RESUMO_SLA_W
(
  IE_INFORMACAO        VARCHAR2(10 BYTE)        NOT NULL,
  DT_REFERENCIA        DATE                     NOT NULL,
  IE_TIPO_SLA          VARCHAR2(10 BYTE)        NOT NULL,
  QT_SLA               NUMBER(10)               NOT NULL,
  QT_SLA_CALCULO       NUMBER(10)               NOT NULL,
  QT_ATRASO_RESPOSTA   NUMBER(10)               NOT NULL,
  QT_ATRASO_SOLUCAO    NUMBER(10)               NOT NULL,
  PR_ATINGIDO          NUMBER(10,2)             NOT NULL,
  QT_TARGET            NUMBER(10,2)             NOT NULL,
  NR_SEQ_GERENCIA_DES  NUMBER(10),
  NR_SEQ_GERENCIA_SUP  NUMBER(10),
  NR_SEQ_LOCALIZACAO   NUMBER(10),
  IE_PRIORIDADE        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.MAN_RESUMO_SLA_W TO NIVEL_1;

