ALTER TABLE TASY.PLS_REGRA_INTERCAMBIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_INTERCAMBIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_INTERCAMBIO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_CONGENERE         NUMBER(10),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  DT_FIM_VIGENCIA          DATE,
  PR_TAXA                  NUMBER(7,4)          NOT NULL,
  VL_TAXA                  NUMBER(15,2)         NOT NULL,
  QT_DIAS_ENVIO_CONTA      NUMBER(3),
  QT_DIAS_CONTESTACAO      NUMBER(3),
  NR_SEQ_INTERCAMBIO       NUMBER(10),
  IE_TIPO_INTERCAMBIO      VARCHAR2(10 BYTE)    NOT NULL,
  NR_SEQ_CONGENERE_SUP     NUMBER(10),
  QT_DIAS_ENVIO_TAXA       NUMBER(3),
  IE_COBRANCA_PAGAMENTO    VARCHAR2(1 BYTE),
  NR_SEQ_PLANO             NUMBER(10),
  NR_SEQ_GRUPO_CONGENERE   NUMBER(10),
  IE_BENEFICIO_OBITO       VARCHAR2(1 BYTE),
  IE_PCMSO                 VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_COOP_SEG    NUMBER(10),
  NR_SEQ_GRUPO_SERVICO     NUMBER(10),
  IE_TIPO_REGRA            VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_REC         NUMBER(10),
  IE_TIPO_DATA             VARCHAR2(1 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_ATEND_CART  NUMBER(10),
  IE_ATEND_PCMSO           VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA_REF   DATE,
  DT_FIM_VIGENCIA_REF      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREIN_GRURECE_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_GRURECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_I1 ON TASY.PLS_REGRA_INTERCAMBIO
(PR_TAXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREIN_I2 ON TASY.PLS_REGRA_INTERCAMBIO
(DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSREIN_PK ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREIN_PLSCONG_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_PLSCONG_FK2_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_CONGENERE_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSCONG_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_PLSGCOP_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_GRUPO_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSGCOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_PLSGCOP_FK2_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_GRUPO_COOP_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSGCOP_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_PLSINCA_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_PLSOACA_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_REGRA_ATEND_CART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREIN_PLSPLAN_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREIN_PLSPRGS_FK_I ON TASY.PLS_REGRA_INTERCAMBIO
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREIN_PLSPRGS_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_INTERCAMBIO_tp  after update ON TASY.PLS_REGRA_INTERCAMBIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_DATA,1,4000),substr(:new.IE_TIPO_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DATA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_SERVICO,1,4000),substr(:new.NR_SEQ_GRUPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_SERVICO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_ATEND_CART,1,4000),substr(:new.NR_SEQ_REGRA_ATEND_CART,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_ATEND_CART',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_REGRA,1,4000),substr(:new.IE_TIPO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REGRA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATEND_PCMSO,1,4000),substr(:new.IE_ATEND_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATEND_PCMSO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA_REF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE,1,4000),substr(:new.NR_SEQ_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_TAXA,1,4000),substr(:new.PR_TAXA,1,4000),:new.nm_usuario,nr_seq_w,'PR_TAXA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_TAXA,1,4000),substr(:new.VL_TAXA,1,4000),:new.nm_usuario,nr_seq_w,'VL_TAXA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_ENVIO_CONTA,1,4000),substr(:new.QT_DIAS_ENVIO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_ENVIO_CONTA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CONTESTACAO,1,4000),substr(:new.QT_DIAS_CONTESTACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CONTESTACAO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE_SUP,1,4000),substr(:new.NR_SEQ_CONGENERE_SUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE_SUP',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_INTERCAMBIO,1,4000),substr(:new.IE_TIPO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_ENVIO_TAXA,1,4000),substr(:new.QT_DIAS_ENVIO_TAXA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_ENVIO_TAXA',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INTERCAMBIO,1,4000),substr(:new.NR_SEQ_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COBRANCA_PAGAMENTO,1,4000),substr(:new.IE_COBRANCA_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBRANCA_PAGAMENTO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PLANO,1,4000),substr(:new.NR_SEQ_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PLANO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_CONGENERE,1,4000),substr(:new.NR_SEQ_GRUPO_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_CONGENERE',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BENEFICIO_OBITO,1,4000),substr(:new.IE_BENEFICIO_OBITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BENEFICIO_OBITO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PCMSO,1,4000),substr(:new.IE_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PCMSO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_COOP_SEG,1,4000),substr(:new.NR_SEQ_GRUPO_COOP_SEG,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_COOP_SEG',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_INTERCAMBIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_regra_intercambio_atual
before insert or update ON TASY.PLS_REGRA_INTERCAMBIO for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899 desta forma podemos utilizar um between ou fazer uma compara��o com este campo
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia, 'F');

end pls_regra_intercambio_atual;
/


ALTER TABLE TASY.PLS_REGRA_INTERCAMBIO ADD (
  CONSTRAINT PLSREIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_INTERCAMBIO ADD (
  CONSTRAINT PLSREIN_PLSOACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ATEND_CART) 
 REFERENCES TASY.PLS_OC_ATEND_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSGCOP_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_COOP_SEG) 
 REFERENCES TASY.PLS_GRUPO_COOPERATIVA (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSGCOP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONGENERE) 
 REFERENCES TASY.PLS_GRUPO_COOPERATIVA (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSCONG_FK2 
 FOREIGN KEY (NR_SEQ_CONGENERE_SUP) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSREIN_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_INTERCAMBIO TO NIVEL_1;

