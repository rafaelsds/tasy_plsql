DROP TABLE TASY.UNIDADE_FEDERACAO_BAK CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_FEDERACAO_BAK
(
  CD_UNIDADE_FEDERACAO  VARCHAR2(2 BYTE)        NOT NULL,
  DS_UNIDADE_FEDERACAO  VARCHAR2(80 BYTE)       NOT NULL,
  CD_UNIDADE_IBGE       NUMBER(2)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.UNIDADE_FEDERACAO_BAK TO NIVEL_1;

