ALTER TABLE TASY.REG_OBJECT_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_OBJECT_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_OBJECT_LOG
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  OPERATION_ID          VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_DIC_OBJ        NUMBER(10),
  NR_SEQ_OBJ_SCHEMATIC  NUMBER(10),
  NR_SERVICE_ORDER      NUMBER(10),
  NM_TABELA             VARCHAR2(50 BYTE),
  NR_SEQ_DOCUMENTO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGOBLOG_PK ON TASY.REG_OBJECT_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_OBJECT_LOG ADD (
  CONSTRAINT REGOBLOG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REG_OBJECT_LOG TO NIVEL_1;

