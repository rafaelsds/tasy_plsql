ALTER TABLE TASY.CTB_REGRA_ESTAB_DIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGRA_ESTAB_DIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGRA_ESTAB_DIF
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_EMPRESA             NUMBER(4)              NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_PERMITE             VARCHAR2(15 BYTE)      NOT NULL,
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE),
  CD_HISTORICO           NUMBER(15),
  CD_TIPO_LOTE_CONTABIL  NUMBER(10)             NOT NULL,
  IE_REGRA_LOTE          VARCHAR2(1 BYTE)       NOT NULL,
  CD_ESTAB_ORIGEM        NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBREED_CONCONT_FK_I ON TASY.CTB_REGRA_ESTAB_DIF
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBREED_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBREED_EMPRESA_FK_I ON TASY.CTB_REGRA_ESTAB_DIF
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBREED_ESTABEL_FK_I ON TASY.CTB_REGRA_ESTAB_DIF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBREED_ESTABEL_FK2_I ON TASY.CTB_REGRA_ESTAB_DIF
(CD_ESTAB_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBREED_ESTABEL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBREED_HISPADR_FK_I ON TASY.CTB_REGRA_ESTAB_DIF
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBREED_HISPADR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBREED_PK ON TASY.CTB_REGRA_ESTAB_DIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBREED_PK
  MONITORING USAGE;


CREATE INDEX TASY.CTBREED_TIPLOCO_FK_I ON TASY.CTB_REGRA_ESTAB_DIF
(CD_TIPO_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_regra_estab_dif_atual
before insert or update ON TASY.CTB_REGRA_ESTAB_DIF for each row
declare
begin

if	(:new.ie_permite = 'PCCT') and
	(:new.cd_conta_contabil is null) then
	/*Para esta regra a conta transit�ria � obrigat�ria');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266562);
end if;

if	(:new.ie_permite <> 'PCCT') and
	(:new.cd_conta_contabil is not null) then
	/*Para esta regra a conta transit�ria n�o pode ser informada');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266563);
end if;

if	(:new.ie_permite = 'PCCT') and
	(:new.cd_historico is null) then
	/*'Para esta regra o hist�rico padr�o � obrigat�rio');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266564);

end if;

if	(:new.ie_permite <> 'PCCT') and
	(:new.cd_conta_contabil is not null) then
	/*Para esta regra o hist�rico padr�o n�o pode ser informado');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266565);
end if;
/*N�o deve ser informado regra com conta transit�ria para o tipo de lote de Consumo!*/
if	(:new.cd_tipo_lote_contabil = 3) and
	(:new.ie_permite <> 'PSCT') then
	wheb_mensagem_pck.exibir_mensagem_abort(315885);
end if;
END;
/


ALTER TABLE TASY.CTB_REGRA_ESTAB_DIF ADD (
  CONSTRAINT CTBREED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_REGRA_ESTAB_DIF ADD (
  CONSTRAINT CTBREED_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBREED_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CTBREED_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBREED_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT CTBREED_TIPLOCO_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTABIL) 
 REFERENCES TASY.TIPO_LOTE_CONTABIL (CD_TIPO_LOTE_CONTABIL),
  CONSTRAINT CTBREED_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_ORIGEM) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CTB_REGRA_ESTAB_DIF TO NIVEL_1;

