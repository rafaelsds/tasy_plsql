ALTER TABLE TASY.ESCALA_NAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NAS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  DT_LIBERACAO               DATE,
  DS_ITEM_1A                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_1B                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_1C                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_2                  VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_3                  VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_4A                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_4B                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_4C                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_5                  VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_6A                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_6B                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_6C                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_7A                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_7B                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_8A                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_8B                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_8C                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_9                  VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_10                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_11                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_12                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_13                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_14                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_15                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_16                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_17                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_18                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_19                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_20                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_21                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_22                 VARCHAR2(1 BYTE)   NOT NULL,
  DS_ITEM_23                 VARCHAR2(1 BYTE)   NOT NULL,
  QT_PONTUACAO               NUMBER(15,2),
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_PRESCR              NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCANAS_ATEPACI_FK_I ON TASY.ESCALA_NAS
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANAS_EHRREEL_FK_I ON TASY.ESCALA_NAS
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANAS_I1 ON TASY.ESCALA_NAS
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANAS_PEPRESC_FK_I ON TASY.ESCALA_NAS
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANAS_PERFIL_FK_I ON TASY.ESCALA_NAS
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANAS_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCANAS_PESFISI_FK2_I ON TASY.ESCALA_NAS
(CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCANAS_PK ON TASY.ESCALA_NAS
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANAS_SETATEN_FK_I ON TASY.ESCALA_NAS
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANAS_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCANAS_TASASDI_FK_I ON TASY.ESCALA_NAS
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANAS_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCANAS_TASASDI_FK2_I ON TASY.ESCALA_NAS
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANAS_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_nas_atual
before insert or update ON TASY.ESCALA_NAS for each row
declare

qt_pontuacao_w	number(15,2) := 0;
qt_item1_w	number(15,2);
qt_item4_w	number(15,2);
qt_item6_w	number(15,2);
qt_item7_w	number(15,2);
qt_item8_w	number(15,2);
qt_max_w	number(15,2) := 0;
qt_valor_w	number(15,2);
ds_lista_w	varchar2(255);
qt_reg_w	number(1);
cd_setor_atendimento_w	number(10);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_pontuacao := 0;

if	(:new.ds_item_1a = 'S') then
	qt_item1_w := 4.5;
elsif	(:new.ds_item_1b = 'S') then
	qt_item1_w := 12.1;
elsif	(:new.ds_item_1c = 'S') then
	qt_item1_w := 19.6;
end if;

if	(:new.ds_item_2 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 4.3;
end if;

if	(:new.ds_item_3 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 5.6;
end if;

if	(:new.ds_item_4a = 'S') then
	qt_item4_w := 4.1;
elsif	(:new.ds_item_4b = 'S') then
	qt_item4_w := 16.5;
elsif	(:new.ds_item_4c = 'S') then
	qt_item4_w := 20;
end if;

if	(:new.ds_item_5 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.8;
end if;

if	(:new.ds_item_6a = 'S') then
	qt_item6_w := 5.5;
elsif	(:new.ds_item_6b = 'S') then
	qt_item6_w := 12.4;
elsif	(:new.ds_item_6c = 'S') then
	qt_item6_w := 17;
end if;

if	(:new.ds_item_7a = 'S') then
	qt_item7_w := 4;
elsif	(:new.ds_item_7b = 'S') then
	qt_item7_w := 32;
end if;

if	(:new.ds_item_8a = 'S') then
	qt_item8_w := 4.2;
elsif	(:new.ds_item_8b = 'S') then
	qt_item8_w := 23.2;
elsif	(:new.ds_item_8c = 'S') then
	qt_item8_w := 30;
end if;

if	(:new.ds_item_9       = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.4;
end if;

if	(:new.ds_item_10 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.8;
end if;

if	(:new.ds_item_11 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 4.4;
end if;

if	(:new.ds_item_12 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.2;
end if;

if	(:new.ds_item_13 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 2.5;
end if;

if	(:new.ds_item_14 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.7;
end if;

if	(:new.ds_item_15 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 7.1;
end if;

if	(:new.ds_item_16 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 7.7;
end if;

if	(:new.ds_item_17 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 7;
end if;

if	(:new.ds_item_18 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.6;
end if;

if	(:new.ds_item_19 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.3;
end if;

if	(:new.ds_item_20 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 2.8;
end if;

if	(:new.ds_item_21 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.3;
end if;

if	(:new.ds_item_22 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 2.8;
end if;

if	(:new.ds_item_23 = 'S') then
	:new.qt_pontuacao := :new.qt_pontuacao + 1.9;
end if;

:new.qt_pontuacao := nvl(:new.qt_pontuacao,0)	+	nvl(qt_item1_w,0)	+	nvl(qt_item4_w,0)	+
					 nvl(qt_item6_w,0)			+	nvl(qt_item7_w,0)	+	nvl(qt_item8_w,0);

if	(:new.nr_atendimento	is not null) then
	begin
	cd_setor_atendimento_w	:= obter_setor_atendimento(:new.nr_atendimento);

	exception
	when others then
		cd_setor_atendimento_w	:= 0;
	end;

	if	(cd_setor_atendimento_w	is not null) and
		(cd_setor_atendimento_w	> 0) then
		:new.cd_setor_atendimento := cd_setor_atendimento_w;
	end if;

end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	send_nas_integration(:new.nr_atendimento, :new.nr_sequencia, :new.dt_atualizacao, :new.dt_avaliacao,
					:new.dt_liberacao, :new.ie_situacao, :new.ds_item_1a, :new.ds_item_1b,
					:new.ds_item_1c, :new.ds_item_2, :new.ds_item_3, :new.ds_item_4a, :new.ds_item_4b,
					:new.ds_item_4c, :new.ds_item_5, :new.ds_item_6a, :new.ds_item_6b, :new.ds_item_6c,
					:new.ds_item_7a, :new.ds_item_7b, :new.ds_item_8a, :new.ds_item_8b, :new.ds_item_8c,
					:new.ds_item_9, :new.ds_item_10, :new.ds_item_11, :new.ds_item_12, :new.ds_item_13,
					:new.ds_item_14, :new.ds_item_15, :new.ds_item_16, :new.ds_item_17, :new.ds_item_18,
					:new.ds_item_19, :new.ds_item_20, :new.ds_item_21, :new.ds_item_22, :new.ds_item_23);
end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_nas_pend_atual
after insert or update ON TASY.ESCALA_NAS for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'24');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_NAS_delete
after delete ON TASY.ESCALA_NAS for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '24'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_NAS ADD (
  CONSTRAINT ESCANAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NAS ADD (
  CONSTRAINT ESCANAS_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCANAS_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT ESCANAS_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCANAS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCANAS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ESCANAS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ESCANAS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCANAS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_NAS TO NIVEL_1;

