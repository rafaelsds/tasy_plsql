ALTER TABLE TASY.CADASTRO_SPRINT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CADASTRO_SPRINT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CADASTRO_SPRINT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_SPRINT            VARCHAR2(255 BYTE)       NOT NULL,
  IE_TIPO_SPRINT       NUMBER(3)                NOT NULL,
  NM_GERENCIA          VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_PAIS          NUMBER(10),
  DT_INICIO            DATE,
  DT_FIM               DATE,
  IE_STATUS_SPRINT     NUMBER(3),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CADSPRINT_PAIS_FK_I ON TASY.CADASTRO_SPRINT
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CADSPRINT_PK ON TASY.CADASTRO_SPRINT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CADASTRO_SPRINT ADD (
  CONSTRAINT CADSPRINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CADASTRO_SPRINT ADD (
  CONSTRAINT CADSPRINT_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CADASTRO_SPRINT TO NIVEL_1;

