ALTER TABLE TASY.PLS_PROC_PARTICIPANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROC_PARTICIPANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROC_PARTICIPANTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CONTA_PROC          NUMBER(10)         NOT NULL,
  CD_MEDICO                  VARCHAR2(10 BYTE),
  NR_CPF_IMP                 VARCHAR2(11 BYTE),
  NM_MEDICO_EXECUTOR_IMP     VARCHAR2(255 BYTE),
  SG_CONSELHO_IMP            VARCHAR2(20 BYTE),
  NR_CRM_IMP                 VARCHAR2(20 BYTE),
  UF_CRM_IMP                 VARCHAR2(15 BYTE),
  IE_FUNCAO_MEDICO_IMP       VARCHAR2(2 BYTE),
  NR_SEQ_GRAU_PARTIC         NUMBER(10),
  CD_CGC_IMP                 VARCHAR2(14 BYTE),
  CD_MEDICO_IMP              VARCHAR2(10 BYTE),
  CD_CBO_SAUDE_IMP           VARCHAR2(10 BYTE),
  VL_PARTICIPANTE            NUMBER(15,2),
  NR_SEQ_CBO_SAUDE           NUMBER(10),
  VL_HONORARIO_MEDICO        NUMBER(15,2),
  NR_SEQ_HONORARIO_CRIT      NUMBER(10),
  IE_CONSELHO_PROFISSIONAL   VARCHAR2(10 BYTE),
  NR_SEQ_PRESTADOR_PGTO      NUMBER(10),
  CD_GUIA                    VARCHAR2(20 BYTE),
  NR_SEQ_CONSELHO            NUMBER(10),
  CD_PRESTADOR_IMP           VARCHAR2(30 BYTE),
  NR_SEQ_PRESTADOR           NUMBER(10),
  UF_CONSELHO                VARCHAR2(15 BYTE),
  IE_ATUALIZADO              VARCHAR2(1 BYTE),
  VL_APRESENTADO             NUMBER(15,2),
  IE_INSERCAO_MANUAL         VARCHAR2(1 BYTE),
  VL_CALCULADO               NUMBER(15,2),
  IE_GLOSA                   VARCHAR2(1 BYTE),
  VL_GLOSA                   NUMBER(15,2),
  NR_SEQ_REGRA               NUMBER(10),
  IE_STATUS                  VARCHAR2(2 BYTE),
  VL_DIGITADO_COMPLEMENTO    NUMBER(15,2),
  QT_LIBERADA                NUMBER(12,4),
  IE_GERADA_CTA_HONORARIO    VARCHAR2(1 BYTE),
  CD_SISTEMA_ANT             VARCHAR2(255 BYTE),
  VL_CALCULADO_ANT           NUMBER(15,2),
  IE_STATUS_PAGAMENTO        VARCHAR2(1 BYTE),
  NR_ID_ANALISE              NUMBER(10),
  CD_GRAU_PARTIC_IMP         VARCHAR2(2 BYTE),
  VL_POS_ESTAB               NUMBER(15,2),
  NR_SEQ_PREST_INTER         NUMBER(10),
  NR_SEQ_IMP                 NUMBER(10),
  NR_SEQ_CP_COMB_FILTRO      NUMBER(10),
  NR_SEQ_CP_COMB_ENTAO       NUMBER(10),
  NR_SEQ_CP_COMB_FILTRO_COP  NUMBER(10),
  NR_SEQ_CP_COMB_COP_ENTAO   NUMBER(10),
  VL_NEGOCIADO               NUMBER(15,2),
  VL_MEDICO                  NUMBER(15,2),
  VL_ANESTESISTA             NUMBER(15,2),
  VL_AUXILIARES              NUMBER(15,2),
  VL_MEDICO_BASE             NUMBER(15,2),
  VL_ANESTESISTA_BASE        NUMBER(15,2),
  VL_AUXILIARES_BASE         NUMBER(15,2),
  IE_GLOSA_POS_ESTAB         VARCHAR2(1 BYTE),
  IE_INCLUIDO_POS_ESTAB      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          15M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRPAR_CBOSAUD_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPAR_CBOSAUD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPAR_CONPROF_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CONSELHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPAR_CONPROF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPAR_I1 ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CONTA_PROC, NVL("NR_SEQ_CP_COMB_FILTRO",(-1)), IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_MEDICO_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PCIEQIP_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRPAR_PK ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PLCPCPA_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CP_COMB_ENTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PLCPCPA1_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CP_COMB_COP_ENTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PLSCOPRO_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PLSCPCC_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CP_COMB_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PLSCPCC1_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CP_COMB_FILTRO_COP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPAR_PLSGRPA_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPAR_PLSGRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPAR_PLSPRES_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_PRESTADOR_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPAR_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPAR_PLSPRES_FK2_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPAR_PLSPRES_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPAR_PLSPRIN_FK_I ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_PREST_INTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRPAR_UK ON TASY.PLS_PROC_PARTICIPANTE
(NR_SEQ_CONTA_PROC, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_proc_participante_delete
before delete ON TASY.PLS_PROC_PARTICIPANTE for each row
declare
	ds_log_call_w 	Varchar2(1500);
	ds_observacao_w	Varchar2(4000);
	ie_opcao_w	Varchar2(3);
begin
if	(pls_se_aplicacao_tasy = 'S')then
	ie_opcao_w := '2';
else
	ie_opcao_w := '1';
end if;

ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

ds_observacao_w := 	'Conta partic: ' || :old.nr_sequencia || pls_util_pck.enter_w ||
			'Conta proc: ' || :old.nr_seq_conta_proc || pls_util_pck.enter_w ||
			'M�dico: ' || :old.cd_medico || pls_util_pck.enter_w ||
			'M�dico imp: ' || :old.cd_medico_imp || pls_util_pck.enter_w ||
			'Prestador: ' || :old.nr_seq_prestador;

insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
					ds_log, ds_log_call, ds_funcao_ativa,
					ie_aplicacao_tasy, nm_maquina,ie_opcao,
					nr_seq_conta_proc_partic, pls_conta_proc)
		values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROC_PARTICIPANTE',
					ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, ie_opcao_w,
					:old.nr_sequencia, :old.nr_seq_conta_proc);

end pls_proc_participante_delete;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PROC_PARTICIPANTE_tp  after update ON TASY.PLS_PROC_PARTICIPANTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.UF_CONSELHO,1,4000),substr(:new.UF_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'UF_CONSELHO',ie_log_w,ds_w,'PLS_PROC_PARTICIPANTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_proc_participante_alt
before update ON TASY.PLS_PROC_PARTICIPANTE for each row
declare
	ds_log_call_w 	Varchar2(1500);
	ds_observacao_w	Varchar2(4000);
	ie_opcao_w	Varchar2(3);
begin
if	(:new.nr_seq_grau_partic 	is null and :old.nr_seq_grau_partic is not null ) or
	(:new.nr_seq_grau_partic 	<> :old.nr_seq_grau_partic) or
	(:new.nr_seq_prestador 		is null and :old.nr_seq_prestador is not null) or
	(:new.nr_seq_prestador	 	<> :old.nr_seq_prestador ) or
	(:new.cd_medico			is null and :old.cd_medico is not null) or
	(:new.cd_medico		 	<> :old.cd_medico )then
	ie_opcao_w := '2';
else
	ie_opcao_w := '1';
end if;

if	(( pls_se_aplicacao_tasy = 'N') and
	(nvl(wheb_usuario_pck.get_nm_usuario,'X') = 'X')) or
	(:new.nr_seq_grau_partic 	is null and :old.nr_seq_grau_partic is not null ) or
	(:new.nr_seq_grau_partic 	<> :old.nr_seq_grau_partic) or
	(:new.nr_seq_prestador 		is null and :old.nr_seq_prestador is not null) or
	(:new.nr_seq_prestador	 	<> :old.nr_seq_prestador ) or
	(:new.cd_medico			is null and :old.cd_medico is not null) or
	(:new.cd_medico		 	<> :old.cd_medico )then

	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);/*substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
			' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);*/
			--' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
	ds_observacao_w := 'Conta partic: '||:old.nr_sequencia||' ; Conta proc: '||:old.nr_seq_conta_proc;


	if	(nvl(:old.nr_seq_prestador,0) <> nvl(:new.nr_seq_prestador,0)) then
		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
					'Prestador executor: '||chr(13)||chr(10)||
					chr(9)||'Anterior: '||pls_obter_dados_prestador(:old.nr_seq_prestador,'N')||' - Modificado: '||pls_obter_dados_prestador(:new.nr_seq_prestador,'N')||chr(13)||chr(10);
	end if;

	if	(nvl(:old.nr_seq_grau_partic,0) <> nvl(:new.nr_seq_grau_partic,0)) then
		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
					'Grau de participa��o: '||chr(13)||chr(10)||
					chr(9)||'Anterior: '||:old.nr_seq_grau_partic||' - Modificado: '||:new.nr_seq_grau_partic||chr(13)||chr(10);
	end if;

	if	(nvl(:old.cd_medico,'X') <> nvl(:new.cd_medico,'X')) then
		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
					'M�dico: '||chr(13)||chr(10)||
					chr(9)||'Anterior: '||:old.cd_medico||' - Modificado: '||:new.cd_medico||chr(13)||chr(10);
	end if;

	insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
						ds_log, ds_log_call, ds_funcao_ativa,
						ie_aplicacao_tasy, nm_maquina,ie_opcao,
						nr_seq_conta_proc_partic)
			values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
						sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_PROC_PARTICIPANTE',
						ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
						pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, ie_opcao_w,
						:old.nr_sequencia);
	--end if;
end if;

if	(updating) and
	((:old.nr_id_analise is not null) and ((:new.nr_id_analise is null) or (:old.nr_id_analise != :new.nr_id_analise)))then

	wheb_mensagem_pck.exibir_mensagem_abort(247472);
end if;


end pls_proc_participante_alt;
/


ALTER TABLE TASY.PLS_PROC_PARTICIPANTE ADD (
  CONSTRAINT PLSPRPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          7M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSPRPAR_UK
 UNIQUE (NR_SEQ_CONTA_PROC, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROC_PARTICIPANTE ADD (
  CONSTRAINT PLSPRPAR_PLCPCPA_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_ENTAO) 
 REFERENCES TASY.PLS_CP_CTA_EPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PLSCPCC1_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_FILTRO_COP) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_FILTRO) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PCIEQIP_FK 
 FOREIGN KEY (NR_SEQ_IMP) 
 REFERENCES TASY.PLS_CONTA_ITEM_EQUIPE_IMP (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRPAR_PLCPCPA1_FK 
 FOREIGN KEY (NR_SEQ_CP_COMB_COP_ENTAO) 
 REFERENCES TASY.PLS_CP_CTA_EPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PLSPRIN_FK 
 FOREIGN KEY (NR_SEQ_PREST_INTER) 
 REFERENCES TASY.PLS_PRESTADOR_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_PGTO) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_CONPROF_FK 
 FOREIGN KEY (NR_SEQ_CONSELHO) 
 REFERENCES TASY.CONSELHO_PROFISSIONAL (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSPRPAR_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRPAR_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSPRPAR_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PROC_PARTICIPANTE TO NIVEL_1;

