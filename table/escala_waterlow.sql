ALTER TABLE TASY.ESCALA_WATERLOW
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_WATERLOW CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_WATERLOW
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  QT_WATERLOW            NUMBER(2),
  IE_BMI                 NUMBER(2),
  QT_MST_WATERLOW        NUMBER(2),
  IE_SKIN_TYPE           NUMBER(2),
  IE_SEX                 NUMBER(2),
  IE_WEIGHT_LOSS         NUMBER(2),
  IE_AGE                 NUMBER(2),
  QT_WEIGHT_LOSS         NUMBER(2),
  IE_APETITE_LOSS        VARCHAR2(1 BYTE),
  IE_CONTINENECE         NUMBER(2),
  IE_MOBILITY            NUMBER(2),
  IE_TISSUE_MALNUT       NUMBER(2),
  IE_NEUROLOGICAL_DEF    NUMBER(2),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SURG_TRAUMA         NUMBER(2),
  IE_MEDICATION          NUMBER(2),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCWATLO_ATEPACI_FK_I ON TASY.ESCALA_WATERLOW
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCWATLO_PESFISI_FK_I ON TASY.ESCALA_WATERLOW
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCWATLO_PK ON TASY.ESCALA_WATERLOW
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_waterlow_atual before
INSERT OR UPDATE ON TASY.ESCALA_WATERLOW FOR EACH row
DECLARE ie_apetite_loss_w NUMBER (1);

   ie_skin_type_w NUMBER (1) := 0;

   qt_score_mst NUMBER (10) :=0;

   ie_tissue_malnut_w number(1) := 0;

   ie_surg_trauma_w number(1) := 0;

  BEGIN

    IF (wheb_usuario_pck.get_ie_executar_trigger = 'S') THEN

      IF (:new.ie_weight_loss                    = 2) THEN
        qt_score_mst                            := qt_score_mst + 2;
      elsif (:new.ie_weight_loss                 = 1 AND :new.qt_weight_loss <> 5 ) THEN
        qt_score_mst                            := qt_score_mst + NVL(:new.qt_weight_loss,0);
      elsif (:new.ie_weight_loss                 = 1 AND :new.qt_weight_loss = 5 ) THEN
        qt_score_mst                            := 2;
      END IF;

      IF (:new.ie_apetite_loss = 'S') THEN
        qt_score_mst          := qt_score_mst + 1;
      END IF;

      if (:new.ie_skin_type = 5 or :new.ie_skin_type = 6 or :new.ie_skin_type = 7 or :new.ie_skin_type = 8) then
        ie_skin_type_w := 1;
        else
        ie_skin_type_w := :new.ie_skin_type;

      end if;

       if (:new.ie_tissue_malnut = 6 ) then
        ie_tissue_malnut_w := 5;
       elsif (:new.ie_tissue_malnut = 9 ) then
        ie_tissue_malnut_w := 8;
       else
        ie_tissue_malnut_w := :new.ie_tissue_malnut;

      end if;

      if (:new.ie_surg_trauma = 6 ) then
        ie_surg_trauma_w := 5;
     else
       ie_surg_trauma_w := :new.ie_surg_trauma;
      end if;

      :new.qt_mst_waterlow := qt_score_mst;
      :new.qt_waterlow     := NVL(:new.ie_bmi,0) + NVL(ie_skin_type_w,0) + qt_score_mst + NVL(:new.ie_continenece,0)
                            + NVL(:new.ie_mobility,0) + NVL(ie_tissue_malnut_w,0) + NVL(:new.ie_neurological_def,0) + NVL(ie_surg_trauma_w,0)
                            + NVL(:new.ie_sex,0) + NVL( :new.ie_age,0) + NVL(:new.ie_medication,0);
    END IF;


  END;
/


ALTER TABLE TASY.ESCALA_WATERLOW ADD (
  CONSTRAINT ESCWATLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_WATERLOW ADD (
  CONSTRAINT ESCWATLO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCWATLO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_WATERLOW TO NIVEL_1;

