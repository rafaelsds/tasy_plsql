ALTER TABLE TASY.AVAL_NUTRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AVAL_NUTRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AVAL_NUTRICAO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  QT_PERDA                      NUMBER(7,3),
  QT_PESO_HABIT                 NUMBER(7,3)     DEFAULT null,
  QT_PESO_ATUAL                 NUMBER(7,3)     NOT NULL,
  PR_PERDA                      NUMBER(7,3),
  NR_ATENDIMENTO                NUMBER(10)      NOT NULL,
  CD_PROFISSIONAL               VARCHAR2(10 BYTE) NOT NULL,
  DT_AVALIACAO                  DATE            NOT NULL,
  QT_ALTURA                     NUMBER(5,2),
  QT_IDADE                      NUMBER(3),
  QT_IMC                        NUMBER(6,1),
  QT_TEMPO_PERDA                NUMBER(3),
  QT_ALTURA_JOELHO              NUMBER(5,2),
  QT_ALTURA_ESTIMADA            NUMBER(3),
  QT_CIRC_PANTURRILHA           NUMBER(5,2),
  QT_CIRC_BRACO                 NUMBER(5,2),
  QT_PERC_CIRC_BRACO            NUMBER(5,2),
  PR_CIRC_BRACO                 NUMBER(7,3),
  QT_PREGA_CUT_TRICIP           NUMBER(5,2),
  QT_PERC_PREGA_CUT_TRICIP      NUMBER(5,2),
  PR_PREGA_CUT_TRICIP           NUMBER(7,3),
  QT_CIRC_MUSC_BRACO            NUMBER(5,2),
  PR_CIRC_MUSC_BRACO            NUMBER(7,3),
  QT_GASTO_ENER_REPOUSO         NUMBER(5,1),
  QT_FATOR_ATIVIDADE            NUMBER(4,1),
  QT_FATOR_ESTRESSE             NUMBER(4,1),
  QT_GASTO_ENER_TOTAL           NUMBER(5,1),
  NR_SEQ_FATOR_ATIV             NUMBER(15),
  NR_SEQ_FATOR_STRESS           NUMBER(15),
  QT_PERC_CIRC_MUSC_BRACO       NUMBER(5,2),
  IE_ORIGEM_PESO                VARCHAR2(2 BYTE),
  IE_ORIGEM_ALTURA              VARCHAR2(2 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                  DATE,
  DT_INATIVACAO                 DATE,
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  IE_AVAL_NUT                   VARCHAR2(15 BYTE) DEFAULT null,
  DS_OBSERVACAO                 VARCHAR2(4000 BYTE),
  IE_TEMPO_PERDA                VARCHAR2(3 BYTE),
  QT_CIRC_QUADRIL               NUMBER(5,2),
  QT_CIRC_ABDOMEN               NUMBER(5,2),
  QT_PREGA_CUT_SUBESC           NUMBER(5,2),
  QT_PREGA_CUT_SUPRAILIACA      NUMBER(5,2),
  QT_PREGA_CUT_BICIPAL          NUMBER(5,2),
  QT_CIRC_CINTURA               NUMBER(5,2),
  QT_IDADE_MES                  NUMBER(5),
  QT_IDADE_DIAS                 NUMBER(5),
  QT_CIRC_PULSO                 NUMBER(5,2),
  IE_FORMULA_BOLSO              VARCHAR2(1 BYTE),
  QT_KCAL_RECOMENDADO           NUMBER(15,4),
  QT_PREGA_ADUT_POLEGAR         NUMBER(5,2),
  IE_QUEIMADURA                 VARCHAR2(1 BYTE),
  IE_RESPIRACAO                 VARCHAR2(5 BYTE),
  IE_TRAUMA                     VARCHAR2(1 BYTE),
  QT_GASTO_GEB_MIFFLIN          NUMBER(5,1),
  QT_GAST_ENER_TOTAL_IRETON     NUMBER(5,1),
  QT_RAZAO_CINTURA_QUADRIL      NUMBER(15,4),
  QT_IG_SEMANA                  NUMBER(2),
  QT_PESO_AJUSTADO              NUMBER(7,3),
  PR_GORDURA                    NUMBER(7,3),
  QT_PROTEINA                   NUMBER(10,2),
  QT_NEC_PROTEICA               NUMBER(10,2),
  QT_CALORIA                    NUMBER(6,1),
  QT_NEC_CALORICA               NUMBER(10,2),
  PR_PERDA_PESO                 NUMBER(7,3),
  QT_PREGA_CUT_ABD              NUMBER(5,2),
  IE_TIPO                       VARCHAR2(1 BYTE),
  QT_PREGA_CUT_PANTUR           NUMBER(5,2),
  QT_DENSIDADE_CORPORAL         NUMBER(5,2),
  QT_PESO_GORDO                 NUMBER(7,3),
  QT_MCM                        NUMBER(7,3),
  QT_IND_FLEXIBILIDADE          NUMBER(5,2),
  QT_EXEC_TESTE_FORCA           NUMBER(5),
  QT_FORCA_MAO_ESQ_1            NUMBER(7,3),
  QT_FORCA_MAO_ESQ_2            NUMBER(7,3),
  QT_FORCA_MAO_ESQ_3            NUMBER(7,3),
  QT_FORCA_MAO_DIR_1            NUMBER(7,3),
  QT_FORCA_MAO_DIR_2            NUMBER(7,3),
  QT_TEMPO_POS_ERETA            NUMBER(7,2),
  PR_GORDURA_IDEAL              NUMBER(7,3),
  QT_FORCA_MAO_DIR_3            NUMBER(7,3),
  NR_SEQ_REG_ELEMENTO           NUMBER(10),
  DS_UTC                        VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO              VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO            VARCHAR2(50 BYTE),
  QT_PESO_HABIT_H               NUMBER(7,3),
  QT_PESO_HABIT_I               NUMBER(7,3),
  NR_SEQ_ASSINATURA             NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO     NUMBER(10),
  IE_FORMULA_BOLSO_CONSELHO     VARCHAR2(1 BYTE),
  IE_LADO_CIRC_BRACO            VARCHAR2(1 BYTE),
  IE_LADO_CIRC_PULSO            VARCHAR2(1 BYTE),
  IE_LADO_CIRC_PANT             VARCHAR2(1 BYTE),
  IE_LADO_PREGA_CUT_BIC         VARCHAR2(1 BYTE),
  IE_LADO_PREGA_CUT_TRI         VARCHAR2(1 BYTE),
  IE_LADO_PREGA_ADUT_POL        VARCHAR2(1 BYTE),
  IE_LADO_PREGA_CUT_SUB         VARCHAR2(1 BYTE),
  IE_LADO_PREGA_CUT_SUP         VARCHAR2(1 BYTE),
  QT_PESO_AJUSTADO_AMPUTADO     NUMBER(7,3),
  IE_MEMBRO_AMPUTADO            VARCHAR2(1 BYTE),
  QT_PESO_CORRIGIDO_AMP         NUMBER(7,3),
  IE_ASCITE                     VARCHAR2(3 BYTE),
  IE_EDEMA                      VARCHAR2(3 BYTE),
  QT_PESO_AJUSTADO_AMP          NUMBER(7,3),
  IE_PARAPLEGICO                VARCHAR2(1 BYTE),
  QT_PESO_HIDRICO               NUMBER(7,3),
  QT_PESO_HIDRICO_ASCITE        NUMBER(7,3),
  QT_PESO_PARAPLEGICO           NUMBER(4,2),
  IE_TETRAPLEGICO               VARCHAR2(1 BYTE),
  QT_PESO_TETRAPLEGICO          NUMBER(4,2),
  QT_PESO_SECO                  NUMBER(7,3),
  QT_ENVERGADURA_CM             NUMBER(5,2),
  QT_PCT_AMB                    NUMBER(10,3),
  QT_AMB                        NUMBER(5,2),
  QT_AMB_C                      NUMBER(5,2),
  QT_GASTO_ENER_REPOUSO_KJ      NUMBER(10,1),
  QT_GASTO_GEB_MIFFLIN_KJ       NUMBER(10,1),
  QT_GASTO_ENER_TOTAL_KJ        NUMBER(10,1),
  QT_GAST_ENER_TOTAL_IRETON_KJ  NUMBER(10,1),
  QT_NEC_CALORICA_KJ            NUMBER(10,2),
  QT_CALORIA_KJ                 NUMBER(5,1),
  IE_NIVEL_ATENCAO              VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA        NUMBER(10),
  IE_BITE_PROBLEM               VARCHAR2(1 BYTE),
  IE_DIFFICULT_SWALLOW          VARCHAR2(1 BYTE),
  IE_USE_MOUTH                  VARCHAR2(1 BYTE),
  IE_FAT_IN_BLOOD               VARCHAR2(1 BYTE),
  IE_BEDSORE                    VARCHAR2(1 BYTE),
  DT_REAVALICAO                 DATE,
  IE_ALERTA                     VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO          NUMBER(5),
  IE_STATUS                     VARCHAR2(2 BYTE),
  NR_SEQ_AVAL_NUTRICAO          NUMBER(10),
  IE_REVISADO                   VARCHAR2(1 BYTE),
  CD_DIETA                      NUMBER(10),
  CD_DOENCA_CID                 VARCHAR2(10 BYTE),
  DS_DOENCA_CID                 VARCHAR2(240 BYTE),
  CD_DEPARTAMENTO               NUMBER(6),
  QT_ALB                        NUMBER(7,3),
  QT_WBC                        NUMBER(7,3),
  CD_ENFERMEIRA                 VARCHAR2(10 BYTE),
  CD_NUTRICIONISTA              VARCHAR2(10 BYTE),
  QT_AGUA                       NUMBER(10,2),
  DT_CRIACAO                    DATE,
  DT_ALB                        DATE            DEFAULT null,
  DT_HOSPITALIZACAO             DATE,
  DT_WBC                        DATE            DEFAULT null,
  DS_OTHER                      VARCHAR2(255 BYTE),
  IE_PERDA                      VARCHAR2(1 BYTE),
  DS_OTHER_2                    VARCHAR2(255 BYTE),
  DS_NOTAS                      VARCHAR2(255 BYTE),
  IE_DENTADURA                  VARCHAR2(1 BYTE),
  IE_FERIMENTOS                 VARCHAR2(1 BYTE),
  DS_OTHER_5                    VARCHAR2(255 BYTE),
  DS_OTHER_3                    VARCHAR2(255 BYTE),
  IE_DIABETE                    VARCHAR2(1 BYTE),
  IE_PROB_CAVIDADE_ORAL         VARCHAR2(1 BYTE),
  IE_APETITE                    VARCHAR2(1 BYTE),
  DS_PROIBICAO                  VARCHAR2(255 BYTE),
  DS_REQ_NUTRI                  VARCHAR2(255 BYTE),
  IE_HIPERLIPIDEMIA             VARCHAR2(1 BYTE),
  IE_RECEB_QUIMIO               VARCHAR2(1 BYTE),
  DS_ADICAO                     VARCHAR2(255 BYTE),
  IE_HIPERTENSAO                VARCHAR2(1 BYTE),
  IE_RESTRICAO_RELIGIAO         VARCHAR2(1 BYTE),
  DS_OTHER_4                    VARCHAR2(255 BYTE),
  IE_INTENCAO_FAM_PACIENT       VARCHAR2(1 BYTE),
  IE_ORIENTACAO_NUTRI           VARCHAR2(1 BYTE),
  IE_OBESIDADE                  VARCHAR2(1 BYTE),
  IE_DIETA_DISFAGIA             VARCHAR2(1 BYTE),
  IE_INGESTAO                   VARCHAR2(1 BYTE),
  IE_ADL                        VARCHAR2(1 BYTE),
  IE_AVALIACAO_NUTRI            VARCHAR2(15 BYTE),
  QT_SITTING_HEIGHT             NUMBER(5,2),
  IE_GESTAO_NUTRI               VARCHAR2(1 BYTE),
  IE_AVAL_ALTA                  VARCHAR2(1 BYTE),
  IE_HORA_REAVALIACAO           VARCHAR2(1 BYTE),
  IE_CODIGO_CLASSIF             VARCHAR2(1 BYTE),
  QT_HEAD_CIRCUMFERENCE         NUMBER(5,2),
  IE_LIST_TRAT_MEDICO           VARCHAR2(1 BYTE),
  IE_SODIO_REDUZIDO             VARCHAR2(1 BYTE),
  IE_METODO                     VARCHAR2(1 BYTE),
  QT_BODY_SURFACE               NUMBER(5,2),
  IE_SOFT_MEAL                  VARCHAR2(1 BYTE),
  IE_CHOPPED                    VARCHAR2(1 BYTE),
  IE_BITE_SIZED                 VARCHAR2(1 BYTE),
  QT_OBESITY_INDEX              NUMBER(10,2),
  QT_THORACIC_PER               NUMBER(10),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AVNUTRI_ATCONSPEPA_FK_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_ATEPACI_FK_I ON TASY.AVAL_NUTRICAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_AVNUTRI_FK_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_AVAL_NUTRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_EHRREEL_FK_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_NUTFAAT_FK_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_FATOR_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVNUTRI_NUTFAAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AVNUTRI_NUTFAST_FK_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_FATOR_STRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AVNUTRI_NUTFAST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AVNUTRI_PESFISIENF_FK_I ON TASY.AVAL_NUTRICAO
(CD_ENFERMEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_PESFISI_FK_I ON TASY.AVAL_NUTRICAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_PESFISI_FK2_I ON TASY.AVAL_NUTRICAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_PESFISINUTRI_FK_I ON TASY.AVAL_NUTRICAO
(CD_NUTRICIONISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AVNUTRI_PK ON TASY.AVAL_NUTRICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_SETATEN_FK_I ON TASY.AVAL_NUTRICAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_TASASDI_FK_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AVNUTRI_TASASDI_FK2_I ON TASY.AVAL_NUTRICAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AVAL_NUTRICAO_SINAIS_VITAIS
AFTER INSERT OR UPDATE ON TASY.AVAL_NUTRICAO for each row
declare

begin

begin

if  (:new.dt_liberacao is not null)             and
    ((:new.QT_PESO_ATUAL is not null )          or
     (:new.QT_ALTURA is not null )              or
     (:new.QT_CIRC_BRACO is not null )          or
     (:new.QT_CIRC_PANTURRILHA is not null )    or
     (:new.QT_CIRC_CINTURA is not null )        or
     (:new.QT_CIRC_ABDOMEN is not null ))       then

    insert into atendimento_sinal_vital (    nr_sequencia,
                        nm_usuario,
                        dt_atualizacao,
                        nr_Atendimento,
                        cd_paciente,
                        dt_sinal_vital,
                        dt_liberacao,
                        QT_PESO,
                        QT_ALTURA_CM,
                        QT_CIRCUNF_BRACO,
                        QT_CIRCUNF_PANTURRILHA,
                        QT_PERIMITRO_ABDOMINAL)
                values    (    atendimento_sinal_vital_seq.nextval,
                        :new.nm_usuario,
                        :new.dt_atualizacao,
                        :new.nr_atendimento,
                        obter_pessoa_Atendimento(:new.nr_Atendimento,'C'),
                        :new.DT_AVALIACAO,
                        :new.dt_liberacao,
                        :new.QT_PESO_ATUAL,
                        :new.QT_ALTURA,
                        :new.QT_CIRC_BRACO,
                        :new.QT_CIRC_PANTURRILHA,
                        :new.QT_CIRC_ABDOMEN);

end if;

exception
when others then
    null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.aval_nutricao_pend_atual
after insert or update or delete ON TASY.AVAL_NUTRICAO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_atestado_w	varchar2(10);
nm_usuario_w        varchar2(15);

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;
	begin
	if 	((INSERTING) OR	(UPDATING)) then
		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'AVN';
		elsif	((:old.dt_liberacao is null) and (:new.dt_liberacao is not null))
			OR ((:old.NR_SEQ_ASSINATURA is null) and (:new.NR_SEQ_ASSINATURA is not null)) then
			ie_tipo_w := 'XAVN';
		end if;
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario);
		end if;
	elsif (deleting) THEN
		delete from pep_item_pendente
		where 	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L'
		and	IE_TIPO_REGISTRO = 'AVN';

		commit;
		--Gerar_registro_pendente_PEP('XAVN', :old.nr_sequencia, substr(obter_pessoa_atendimento(:old.nr_atendimento,'C'),1,255), :old.nr_atendimento, :old.nm_usuario, 'D');
	end if;
exception
when others then
	null;
end;
<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.aval_nutricao_atual
before insert or update ON TASY.AVAL_NUTRICAO for each row
declare
cd_pessoa_fisica_w	varchar2(10);
ie_sexo_w		varchar2(1);
qt_fator_ativ_w		number(15,4);
qt_fator_stress_w	number(15,4);
qt_obesidade		number(1);
qt_pont_sexo		number(1);
qt_pont_queimadura	number(1);
qt_pont_trauma		number(1);
ie_raca_negra_w			varchar2(1);
qt_nec_proteica_w	number(10,2);
begin
:new.pr_perda		:= 	dividir((:new.qt_peso_atual * 100) , :new.qt_peso_habit);
:new.QT_PERDA		:= (:new.qt_peso_habit - :new.qt_peso_atual);
:new.PR_PERDA_PESO  	:=   dividir(((:new.qt_peso_habit - :new.qt_peso_atual) * 100), :new.qt_peso_habit);
if	(nvl(:old.DT_AVALIACAO,sysdate+10) <> :new.DT_AVALIACAO) and
	(:new.DT_AVALIACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_AVALIACAO, 'HV');
end if;
if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;
if	(:new.qt_altura is not null) then
	:new.qt_imc	:= 	(dividir(:new.qt_peso_atual , ((dividir(:new.qt_altura , 100)) * (dividir(:new.qt_altura , 100)))));
end if;
if	(:new.QT_CIRC_QUADRIL	is not null) and
	(:new.QT_CIRC_CINTURA is not null)then
	:new.QT_RAZAO_CINTURA_QUADRIL	:= dividir(:new.QT_CIRC_CINTURA,:new.QT_CIRC_QUADRIL);
end if;
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;
select	obter_sexo_pf(cd_pessoa_fisica_w,'C')
into	ie_sexo_w
from	dual;
/* Obter se raca negra */
select	nvl(max(ie_negro),'N')
into	ie_raca_negra_w
from	cor_pele a,
	pessoa_fisica b
	where	b.nr_seq_cor_pele 	= a.nr_sequencia
	and	b.cd_pessoa_fisica 	= cd_pessoa_fisica_w;
if	(ie_sexo_w = 'M') then
	:new.qt_altura_estimada	:= 	(64.19 - (0.04 * :new.qt_idade) + (2.02 * :new.qt_altura_joelho));
else
	:new.qt_altura_estimada	:= 	(84.88 - (0.24 * :new.qt_idade) + (1.83 * :new.qt_altura_joelho));
end if;
/*Chumlea - 1985		- 		Arthur - OS 823502*/
if	(:new.ie_origem_altura = 'F') and
	(:new.qt_altura_joelho is not null) then
	if	(ie_sexo_w	= 	'M') then
		:new.qt_altura	:= 	(60.65 + (:new.qt_altura_joelho * 2.04));
	else
		:new.qt_altura	:= 	(84.88 + (:new.qt_altura_joelho * 1.83) - (:new.qt_idade * 0.24));
	end if;
end if;
/*Chumlea - 1988 		- 		Arthur - OS 823502*/
if	(:new.ie_origem_peso = 'C') and
	(:new.qt_prega_cut_subesc is not null) and
	(:new.qt_circ_panturrilha is not null) and
	(:new.qt_circ_braco is not null) and
	(:new.qt_altura_joelho is not null) then
	begin
	if	(ie_sexo_w	= 	'M') then
		:new.qt_peso_atual	:= ((0.98 * :new.qt_circ_panturrilha) + (1.16 * :new.qt_altura_joelho) + (1.73 * :new.qt_circ_braco) + (0.37 * :new.qt_prega_cut_subesc) - 81.69);
	else
		:new.qt_peso_atual	:= ((1.27 * :new.qt_circ_panturrilha) + (0.87 * :new.qt_altura_joelho) + (0.98 * :new.qt_circ_braco) + (0.4 * :new.qt_prega_cut_subesc) - 62.35);
	end if;
	end;
end if;
if	(:new.ie_origem_peso = 'R') and
	(:new.qt_circ_abdomen is not null) and
	(:new.qt_circ_panturrilha is not null) and
	(:new.qt_circ_braco is not null) then
	begin
	if	(ie_sexo_w	= 	'M') then
		:new.qt_peso_atual	:= ((0.5759 * :new.qt_circ_braco) + (0.5263 * :new.qt_circ_abdomen) + (1.2452 * :new.qt_circ_panturrilha) - (4.8689 * 1) - 32.9241);
	else
		:new.qt_peso_atual	:= ((0.5759 * :new.qt_circ_braco) + (0.5263 * :new.qt_circ_abdomen) + (1.2452 * :new.qt_circ_panturrilha) - (4.8689 * 2) - 32.9241);
	end if;
	end;
end if;
/*A formula abaixo estava documentada como Chumlea, mas nao condiz com as referencias
Alterada para um tipo nao existente*/
if	(:new.ie_origem_peso = 'J')and
	(:new.qt_circ_braco is not null) and
	(:new.qt_altura_joelho is not null) then
	begin
	if	(ie_sexo_w	= 	'M') then
			if (ie_raca_negra_w	=	'N') then
				if	(:new.qt_idade between 19 and 59) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.19) + (:new.qt_circ_braco * 3.14) - (83.72));
				elsif 	(:new.qt_idade between 60 and 80) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.10) + (:new.qt_circ_braco * 3.07) - (75.81));
				end if;
			else
				if (:new.qt_idade between 19 and 59) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.09) + (:new.qt_circ_braco * 3.14) - (83.72));
				elsif	(:new.qt_idade between 60 and 80) then
					:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 0.44) + (:new.qt_circ_braco * 2.86) - (39.21));
				end if;
			end if;
	elsif (ie_sexo_w	= 	'F') then
		if(ie_raca_negra_w	=	'N') then
			if	(:new.qt_idade between 19 and 59) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.01) + (:new.qt_circ_braco * 2.81) - (66.04));
			elsif	(:new.qt_idade between 60 and 80) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.09) + (:new.qt_circ_braco * 2.68) - (65.51));
			end if;
		else
			if	(:new.qt_idade between 19 and 59) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.24) + (:new.qt_circ_braco * 2.97) - (82.48));
			elsif	(:new.qt_idade between 60 and 80) then
				:new.qt_peso_atual :=  ((:new.qt_altura_joelho * 1.50) + (:new.qt_circ_braco * 2.58) - (84.22));
			end if;
		end if;
	end if;
	end;
end if ;
/*Chumlea*/
:new.QT_PERC_CIRC_BRACO 	:= obter_classif_percentil(ie_raca_negra_w,ie_sexo_w,:new.qt_idade,'CB');
:new.QT_PERC_PREGA_CUT_TRICIP 	:= obter_classif_percentil(ie_raca_negra_w,ie_sexo_w,:new.qt_idade,'PCT');
:new.QT_PERC_CIRC_MUSC_BRACO 	:= obter_classif_percentil(ie_raca_negra_w,ie_sexo_w,:new.qt_idade,'CMB');
if	(:new.QT_CIRC_BRACO is not null) and
	(:new.QT_PREGA_CUT_TRICIP is not null) then
	:new.QT_CIRC_MUSC_BRACO	:= :new.QT_CIRC_BRACO - 3.14 * (dividir(:new.QT_PREGA_CUT_TRICIP , 10));
end if;
if	(:new.QT_PERC_CIRC_BRACO is not null) then
	:new.PR_CIRC_BRACO	:= dividir((:new.QT_CIRC_BRACO * 100) , :new.QT_PERC_CIRC_BRACO);
end if;
if	(:new.QT_PERC_PREGA_CUT_TRICIP is not null) then
	:new.PR_PREGA_CUT_TRICIP	:= dividir((:new.QT_PREGA_CUT_TRICIP * 100) , :new.QT_PERC_PREGA_CUT_TRICIP);
end if;
if	(:new.QT_PERC_CIRC_MUSC_BRACO is not null) then
	:new.PR_CIRC_MUSC_BRACO	:= dividir((:new.QT_CIRC_MUSC_BRACO * 100) , :new.QT_PERC_CIRC_MUSC_BRACO);
end if;
if (ie_sexo_w = 'M') then
    :new.QT_GASTO_ENER_REPOUSO	:= 66.473 + (13.751* :new.QT_PESO_ATUAL) + (5.003 * nvl(:new.qt_altura,:new.qt_altura_estimada)) - (6.755 * :new.qt_idade);
    :new.QT_GASTO_ENER_REPOUSO_KJ	:= converter_kcal_kjoules (:new.QT_GASTO_ENER_REPOUSO,'KJ');

elsif	(ie_sexo_w = 'F') then
    :new.QT_GASTO_ENER_REPOUSO	:= 655.095 + (9.563 * :new.QT_PESO_ATUAL) + (1.849* nvl(:new.qt_altura,:new.qt_altura_estimada)) - (4.675 * :new.qt_idade);
    :new.QT_GASTO_ENER_REPOUSO_KJ	:= converter_kcal_kjoules (:new.QT_GASTO_ENER_REPOUSO,'KJ');
end if;
if	(:new.QT_PROTEINA is not null) and
	(:new.QT_PESO_ATUAL is not null) then
	:new.QT_NEC_PROTEICA := :new.QT_PROTEINA * :new.QT_PESO_ATUAL;
end if;
if	(:new.QT_CALORIA is not null or :new.QT_CALORIA_KJ is not null) and (:new.QT_PESO_ATUAL is not null) then

if	(:new.QT_CALORIA is not null  and (:old.QT_CALORIA <> :new.QT_CALORIA)) then
            	:new.QT_CALORIA_KJ := converter_kcal_kjoules (:new.QT_CALORIA,'KJ');
end if;

if 	(:new.QT_CALORIA is null and :new.QT_CALORIA_KJ is not null and (:old.QT_CALORIA_KJ <> :new.QT_CALORIA_KJ)) then
       	:new.QT_CALORIA := converter_kcal_kjoules (:new.QT_CALORIA_KJ,'KCAL');
end if;

  :new.QT_NEC_CALORICA := :new.QT_CALORIA * :new.QT_PESO_ATUAL;
  :new.QT_NEC_CALORICA_KJ := :new.QT_CALORIA_KJ * :new.QT_PESO_ATUAL;

end if;
begin
if	(:new.QT_GASTO_ENER_REPOUSO is not null or :new.QT_GASTO_ENER_REPOUSO_KJ is not null ) and
	(:new.NR_SEQ_FATOR_ATIV is not null) and
	(:new.NR_SEQ_FATOR_STRESS is not null) then

	select	nvl(max(qt_fator),1)
	into	qt_fator_stress_w
	from	nut_fator_stress
	where	nr_sequencia	= :new.NR_SEQ_FATOR_STRESS;
	select	nvl(max(qt_fator),1)
	into	qt_fator_ativ_w
	from	nut_fator_ativ
	where	nr_sequencia	= :new.NR_SEQ_FATOR_ATIV ;

                 :new.QT_GASTO_ENER_TOTAL := (:new.QT_GASTO_ENER_REPOUSO * qt_fator_ativ_w * qt_fator_stress_w);
                 :new.QT_GASTO_ENER_TOTAL_KJ := (:new.QT_GASTO_ENER_REPOUSO_KJ * qt_fator_ativ_w * qt_fator_stress_w);

end if;
exception
when others then
	null;
end;
begin
if	(:new.qt_altura is not null) then
	if (ie_sexo_w = 'M') then
		:new.QT_GASTO_GEB_MIFFLIN := (10 * :new.qt_peso_atual) + (6.25 * :new.qt_altura) - (5 * :new.qt_idade) + 5;
                                 :new.QT_GASTO_GEB_MIFFLIN_KJ := converter_kcal_kjoules (:new.QT_GASTO_GEB_MIFFLIN ,'KJ');
	else
		:new.QT_GASTO_GEB_MIFFLIN := (10 * :new.qt_peso_atual) + (6.25 * :new.qt_altura) - (5 * :new.qt_idade) - 161;
                                :new.QT_GASTO_GEB_MIFFLIN_KJ := converter_kcal_kjoules (:new.QT_GASTO_GEB_MIFFLIN ,'KJ');
	end if;
end if;
if	(:new.QT_PESO_ATUAL is not null) and
	(:new.QT_PESO_HABIT is not null) and
	(:new.QT_IMC > 25 )then
	if (nvl(:new.QT_PESO_HABIT_I,0) > 0) then
		:new.QT_PESO_AJUSTADO := ((:new.QT_PESO_ATUAL - :new.QT_PESO_HABIT_I) * 0.25) + :new.QT_PESO_HABIT_I;
	else
		:new.QT_PESO_AJUSTADO := ((:new.QT_PESO_ATUAL - :new.QT_PESO_HABIT) * 0.25) + :new.QT_PESO_HABIT;
	end if;
else
	:new.QT_PESO_AJUSTADO := 0;
end if;
if	(:new.QT_PREGA_CUT_TRICIP is not null) and
	(:new.QT_PREGA_CUT_SUPRAILIACA is not null) and
	(:new.QT_PREGA_CUT_SUBESC is not null) and
	(:new.QT_PREGA_CUT_ABD is not null) then
	:new.PR_GORDURA := ((:new.QT_PREGA_CUT_TRICIP + :new.QT_PREGA_CUT_SUPRAILIACA + :new.QT_PREGA_CUT_SUBESC + :new.QT_PREGA_CUT_ABD ) * 0.153) + 5.783;
     --                                                 ((:new.QT_PREGA_CUT_TRICIP + :new.QT_PREGA_CUT_SUPRAILIACA + :new.QT_PREGA_CUT_SUBESC + (:new.QT_CIRC_ABDOMEN * 10) ) * 0.153) + 5.783;
end if;
if	(:new.IE_RESPIRACAO is not null) then
	if	(:new.IE_RESPIRACAO = 'ESP') then

		if	(:new.QT_IMC is not null) then
			if	(:new.QT_IMC > 27) then
				qt_obesidade	:= 1;
			else
				qt_obesidade	:= 0;
			end if;
		end if;

		:new.QT_GAST_ENER_TOTAL_IRETON := 629 - (11 * :new.qt_idade) + (25 * :new.qt_peso_atual) - (609 * qt_obesidade);
                                :new.QT_GAST_ENER_TOTAL_IRETON_KJ := converter_kcal_kjoules (:new.QT_GAST_ENER_TOTAL_IRETON,'KJ');
	end if;

	if	(:new.IE_RESPIRACAO = 'MEC') then

		if	(:new.IE_QUEIMADURA = 'S') then
			qt_pont_queimadura := 1;
		else
			qt_pont_queimadura := 0;
		end if;

		if	(:new.IE_TRAUMA = 'S') then
			qt_pont_trauma := 1;
		else
			qt_pont_trauma := 0;
		end if;

		if	(ie_sexo_w = 'M') then
			qt_pont_sexo := 1;
		else
			qt_pont_sexo := 0;
		end if;

		:new.QT_GAST_ENER_TOTAL_IRETON := 1784 - (11 * :new.qt_idade) + (5 * :new.qt_peso_atual) + (244 * qt_pont_sexo) + (239 * qt_pont_trauma) + (840 * qt_pont_queimadura);
                                :new.QT_GAST_ENER_TOTAL_IRETON_KJ := converter_kcal_kjoules (:new.QT_GAST_ENER_TOTAL_IRETON,'KJ');
	end if;
end if;
exception
when others then
	null;
end;
end;
/


ALTER TABLE TASY.AVAL_NUTRICAO ADD (
  CONSTRAINT AVNUTRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AVAL_NUTRICAO ADD (
  CONSTRAINT AVNUTRI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT AVNUTRI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AVNUTRI_NUTFAAT_FK 
 FOREIGN KEY (NR_SEQ_FATOR_ATIV) 
 REFERENCES TASY.NUT_FATOR_ATIV (NR_SEQUENCIA),
  CONSTRAINT AVNUTRI_NUTFAST_FK 
 FOREIGN KEY (NR_SEQ_FATOR_STRESS) 
 REFERENCES TASY.NUT_FATOR_STRESS (NR_SEQUENCIA),
  CONSTRAINT AVNUTRI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT AVNUTRI_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AVNUTRI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT AVNUTRI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT AVNUTRI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AVNUTRI_AVNUTRI_FK 
 FOREIGN KEY (NR_SEQ_AVAL_NUTRICAO) 
 REFERENCES TASY.AVAL_NUTRICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AVNUTRI_PESFISIENF_FK 
 FOREIGN KEY (CD_ENFERMEIRA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AVNUTRI_PESFISINUTRI_FK 
 FOREIGN KEY (CD_NUTRICIONISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AVNUTRI_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AVAL_NUTRICAO TO NIVEL_1;

