ALTER TABLE TASY.MEDICO_AREA_ATUACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDICO_AREA_ATUACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDICO_AREA_ATUACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AREA_ATUACAO  NUMBER(10)               NOT NULL,
  NR_RCE               VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDARAT_AREATUM_FK_I ON TASY.MEDICO_AREA_ATUACAO
(NR_SEQ_AREA_ATUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDARAT_AREATUM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDARAT_PESFISI_FK_I ON TASY.MEDICO_AREA_ATUACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDARAT_PK ON TASY.MEDICO_AREA_ATUACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MEDICO_AREA_ATUACAO_tp  after update ON TASY.MEDICO_AREA_ATUACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'MEDICO_AREA_ATUACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RCE,1,4000),substr(:new.NR_RCE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RCE',ie_log_w,ds_w,'MEDICO_AREA_ATUACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AREA_ATUACAO,1,4000),substr(:new.NR_SEQ_AREA_ATUACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AREA_ATUACAO',ie_log_w,ds_w,'MEDICO_AREA_ATUACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.medico_area_befupdate_log
before update ON TASY.MEDICO_AREA_ATUACAO for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into medico_area_atuacao_log(
		nr_sequencia,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_area_atuacao,
		nr_rce)
	values( medico_area_atuacao_log_seq.NextVal,
		:new.cd_pessoa_fisica,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.nr_seq_area_atuacao,
		:old.nr_rce);
end if;

end;
/


ALTER TABLE TASY.MEDICO_AREA_ATUACAO ADD (
  CONSTRAINT MEDARAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDICO_AREA_ATUACAO ADD (
  CONSTRAINT MEDARAT_AREATUM_FK 
 FOREIGN KEY (NR_SEQ_AREA_ATUACAO) 
 REFERENCES TASY.AREA_ATUACAO_MEDICA (NR_SEQUENCIA),
  CONSTRAINT MEDARAT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MEDICO_AREA_ATUACAO TO NIVEL_1;

