ALTER TABLE TASY.PLS_REGRA_TX_OPME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_TX_OPME CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_TX_OPME
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  TX_COMERCIALIZACAO      NUMBER(7,4)           NOT NULL,
  DS_REGRA                VARCHAR2(255 BYTE)    NOT NULL,
  CD_PROCEDIMENTO         NUMBER(15)            NOT NULL,
  IE_ORIGEM_PROCED        NUMBER(10)            NOT NULL,
  IE_TIPO_CONGENERE       VARCHAR2(2 BYTE),
  NR_ORDEM_EXECUCAO       NUMBER(15),
  DT_INICIO_VIGENCIA      DATE,
  IE_ORDENACAO_SUGERIDA   VARCHAR2(1 BYTE),
  DT_FIM_VIGENCIA         DATE,
  NR_SEQ_COOPERATIVA      NUMBER(10),
  NR_SEQ_OPS_CONGENERE    NUMBER(10),
  NR_SEQ_INTERCAMBIO      NUMBER(10),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  NR_SEQ_CONTRATO         NUMBER(10),
  NR_CONTRATO             NUMBER(10),
  NR_SEQ_GRUPO_CONTRATO   NUMBER(10),
  IE_APLICA_TX_ADM        VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTTXO_PK ON TASY.PLS_REGRA_TX_OPME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTTXO_PLSCONG_FK_I ON TASY.PLS_REGRA_TX_OPME
(NR_SEQ_COOPERATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTTXO_PLSCONG_FK2_I ON TASY.PLS_REGRA_TX_OPME
(NR_SEQ_OPS_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTTXO_PLSCONT_FK_I ON TASY.PLS_REGRA_TX_OPME
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTTXO_PLSINCA_FK_I ON TASY.PLS_REGRA_TX_OPME
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTTXO_PLSPRGC_FK_I ON TASY.PLS_REGRA_TX_OPME
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTTXO_PROCEDI_FK_I ON TASY.PLS_REGRA_TX_OPME
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_tx_opme_atual
before insert or update ON TASY.PLS_REGRA_TX_OPME for each row

/* esta trigger foi criada para alimentar o campo de data  fim de vigencia de referencia, isto por quest�es de performance
 para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
o campo fim vigencia ref � alimentado com o campo fim ou se o mesmo for nulo
 � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
sem precisar se preocupar se o campo vai estar nulo
*/
declare

begin

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_TX_OPME_tp  after update ON TASY.PLS_REGRA_TX_OPME FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_OPS_CONGENERE,1,4000),substr(:new.NR_SEQ_OPS_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPS_CONGENERE',ie_log_w,ds_w,'PLS_REGRA_TX_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_EXECUCAO,1,4000),substr(:new.NR_ORDEM_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_EXECUCAO',ie_log_w,ds_w,'PLS_REGRA_TX_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDENACAO_SUGERIDA,1,4000),substr(:new.IE_ORDENACAO_SUGERIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_SUGERIDA',ie_log_w,ds_w,'PLS_REGRA_TX_OPME',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COOPERATIVA,1,4000),substr(:new.NR_SEQ_COOPERATIVA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COOPERATIVA',ie_log_w,ds_w,'PLS_REGRA_TX_OPME',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_TX_OPME ADD (
  CONSTRAINT PLSTTXO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_TX_OPME ADD (
  CONSTRAINT PLSTTXO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSTTXO_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_COOPERATIVA) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSTTXO_PLSCONG_FK2 
 FOREIGN KEY (NR_SEQ_OPS_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSTTXO_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSTTXO_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSTTXO_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_TX_OPME TO NIVEL_1;

