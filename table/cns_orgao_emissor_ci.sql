ALTER TABLE TASY.CNS_ORGAO_EMISSOR_CI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CNS_ORGAO_EMISSOR_CI CASCADE CONSTRAINTS;

CREATE TABLE TASY.CNS_ORGAO_EMISSOR_CI
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ORGAO_EMISSOR     NUMBER(2)                NOT NULL,
  DS_ORGAO_EMISSOR     VARCHAR2(80 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_SIGLA_ORGAO       VARCHAR2(8 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CNSOREM_PK ON TASY.CNS_ORGAO_EMISSOR_CI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CNS_ORGAO_EMISSOR_CI ADD (
  CONSTRAINT CNSOREM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CNS_ORGAO_EMISSOR_CI TO NIVEL_1;

