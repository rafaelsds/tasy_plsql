DROP TABLE TASY.EIS_AGENDA_CONSULTA_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_AGENDA_CONSULTA_PROF
(
  CD_ESTABELECIMENTO    NUMBER(5),
  QT_AGENDA             NUMBER(10),
  DT_REFERENCIA         DATE,
  IE_TIPO_PROFISSIONAL  VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.EIS_AGENDA_CONSULTA_PROF TO NIVEL_1;

