ALTER TABLE TASY.ESCALA_EORTC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_EORTC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_EORTC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  IE_DIFICULDADE_ESFORCO     NUMBER(2),
  IE_DIF_GRANDE_CAMINHADA    NUMBER(2),
  IE_DIF_CURTA_CAMINHADA     NUMBER(2),
  IE_CAMA_CADEIRA_DIA        NUMBER(2),
  IE_AJUDA_ALIMENTAR_VESTIR  NUMBER(2),
  IE_LIMITADO_TRABALHO       NUMBER(2),
  IE_LIMITADO_ATIV_LAZER     NUMBER(2),
  IE_FALTA_AR                NUMBER(2),
  IE_DOR                     NUMBER(2),
  IE_REPOUSAR                NUMBER(2),
  IE_PROBL_DORMIR            NUMBER(2),
  IE_FRACO                   NUMBER(2),
  IE_FALTA_APEDITE           NUMBER(2),
  IE_NAUSEADO                NUMBER(2),
  IE_VOMITADO                NUMBER(2),
  IE_CONSTIPADO              NUMBER(2),
  IE_DIARREIA                NUMBER(2),
  IE_CANSADO                 NUMBER(2),
  IE_DOR_INTERFERIU          NUMBER(2),
  IE_DIF_CONCENTRAR          NUMBER(2),
  IE_TENSO                   NUMBER(2),
  IE_PREOCUPADO              NUMBER(2),
  IE_IRRITADO                NUMBER(2),
  IE_DEPRIMIDO               NUMBER(2),
  IE_DIF_LEMBRAR             NUMBER(2),
  IE_INTERF_VIDA_FAM         NUMBER(2),
  IE_INTERF_VIDA_SOCIAL      NUMBER(2),
  IE_DIF_FINANCEIRA          NUMBER(2),
  IE_CLASSIF_SAUDE           NUMBER(2),
  IE_CLASSIF_QUALIDADE_VIDA  NUMBER(2),
  QT_ESCORE_QL2              NUMBER(15,4),
  QT_ESCORE_PF2              NUMBER(15,4),
  QT_ESCORE_RF2              NUMBER(15,4),
  QT_ESCORE_EF               NUMBER(15,4),
  QT_ESCORE_CF               NUMBER(15,4),
  QT_ESCORE_SF               NUMBER(15,4),
  QT_ESCORE_FA               NUMBER(15,4),
  QT_ESCORE_NV               NUMBER(15,4),
  QT_ESCORE_PA               NUMBER(15,4),
  QT_ESCORE_DY               NUMBER(15,4),
  QT_ESCORE_SL               NUMBER(15,4),
  QT_ESCORE_AP               NUMBER(15,4),
  QT_ESCORE_CO               NUMBER(15,4),
  QT_ESCORE_DI               NUMBER(15,4),
  QT_ESCORE_FI               NUMBER(15,4),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCEORT_ATEPACI_FK_I ON TASY.ESCALA_EORTC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEORT_PESFISI_FK_I ON TASY.ESCALA_EORTC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCEORT_PK ON TASY.ESCALA_EORTC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCEORT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_eortc_atual
before insert or update ON TASY.ESCALA_EORTC for each row
declare
qt_bruto_pf2_w		number(15,3) := 0;
qt_bruto_rf2_w		number(15,2) := 0;
qt_bruto_ef_w		number(15,3) := 0;
qt_bruto_ql2_w		number(15,3) := 0;
qt_bruto_cf_w		number(15,3) := 0;
qt_bruto_sf_w		number(15,3) := 0;
qt_bruto_fa_w		number(15,3) := 0;
qt_bruto_nv_w		number(15,3) := 0;
qt_bruto_pa_w		number(15,3) := 0;
qt_bruto_dy_w		number(15,3) := 0;
qt_bruto_sl_w		number(15,3) := 0;
qt_bruto_ap_w		number(15,3) := 0;
qt_bruto_co_w		number(15,3) := 0;
qt_bruto_di_w		number(15,3) := 0;
qt_bruto_fi_w		number(15,3) := 0;

begin

Consiste_Liberacao_Escala(113);

qt_bruto_ql2_w		:= 	((:new.IE_CLASSIF_SAUDE + :new.IE_CLASSIF_QUALIDADE_VIDA) / 2);

:new.QT_ESCORE_QL2	:= 	(((qt_bruto_ql2_w - 1) / 6) * 100);

qt_bruto_pf2_w		:= 	(:new.ie_dificuldade_esforco + :new.ie_dif_grande_caminhada + :new.ie_dif_curta_caminhada + :new.ie_cama_cadeira_dia + :new.ie_ajuda_alimentar_vestir)/5;

:new.qt_escore_pf2	:= 	(1 - (qt_bruto_pf2_w - 1) / 3) * 100;

qt_bruto_rf2_w		:= 	(:new.IE_LIMITADO_TRABALHO + :new.IE_LIMITADO_ATIV_LAZER)/2;

:new.QT_ESCORE_RF2	:= 	(1 - (qt_bruto_rf2_w - 1) / 3) * 100;

qt_bruto_ef_w		:= 	(:new.IE_TENSO + :new.IE_PREOCUPADO + :new.IE_IRRITADO + :new.IE_DEPRIMIDO) / 4;

:new.QT_ESCORE_EF	:= 	(1 - (qt_bruto_ef_w - 1) / 3) * 100;

qt_bruto_cf_w		:= 	(:new.IE_DIF_CONCENTRAR + :new.IE_DIF_LEMBRAR) / 2;

:new.QT_ESCORE_CF	:= 	(1 - (qt_bruto_cf_w - 1) / 3) * 100;

qt_bruto_sf_w		:= 	(:new.IE_INTERF_VIDA_FAM + :new.IE_INTERF_VIDA_SOCIAL) /2;

:new.QT_ESCORE_SF	:= 	(1 - (qt_bruto_sf_w - 1) / 3) * 100;

qt_bruto_fa_w		:= 	(:new.IE_REPOUSAR + :new.IE_FRACO + :new.IE_CANSADO) / 3;

:new.QT_ESCORE_FA	:= 	((qt_bruto_fa_w - 1) / 3) * 100;

qt_bruto_nv_w		:= 	(:new.IE_NAUSEADO + :new.IE_VOMITADO) /2;

:new.QT_ESCORE_NV	:= 	((qt_bruto_nv_w - 1) / 3) * 100;

qt_bruto_pa_w		:=	(:new.IE_DOR + :new.IE_DOR_INTERFERIU) /2;

:new.QT_ESCORE_PA	:= 	((qt_bruto_pa_w - 1) / 3) * 100;

qt_bruto_dy_w		:= 	(:NEW.IE_FALTA_AR);

:new.QT_ESCORE_DY	:= 	((qt_bruto_dy_w - 1) / 3) * 100;

qt_bruto_sl_w		:= 	(:new.IE_PROBL_DORMIR);

:new.QT_ESCORE_SL	:= 	((qt_bruto_dy_w - 1) / 3) * 100;

qt_bruto_ap_w		:= 	(:new.IE_FALTA_APEDITE);

:new.QT_ESCORE_AP	:= 	((qt_bruto_ap_w - 1) / 3) * 100;

qt_bruto_co_w		:=	(:new.IE_CONSTIPADO);

:new.QT_ESCORE_CO	:= 	((qt_bruto_co_w - 1) / 3) * 100;

qt_bruto_di_w		:= 	(:new.IE_DIARREIA);

:new.QT_ESCORE_DI	:= 	((qt_bruto_di_w - 1) / 3) * 100;

qt_bruto_fi_w		:= 	(:new.IE_DIF_FINANCEIRA);

:new.QT_ESCORE_FI	:= 	((qt_bruto_fi_w - 1) / 3) * 100;

end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_EORTC_delete
after delete ON TASY.ESCALA_EORTC for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '113'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_EORTC ADD (
  CONSTRAINT ESCEORT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_EORTC ADD (
  CONSTRAINT ESCEORT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCEORT_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_EORTC TO NIVEL_1;

