ALTER TABLE TASY.APLICACAO_TASY_BUILD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.APLICACAO_TASY_BUILD CASCADE CONSTRAINTS;

CREATE TABLE TASY.APLICACAO_TASY_BUILD
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  CD_APLICACAO_TASY  VARCHAR2(15 BYTE)          NOT NULL,
  CD_BUILD_TASY      VARCHAR2(15 BYTE),
  CD_BUILD_JENKINS   VARCHAR2(15 BYTE),
  CD_VERSAO          VARCHAR2(10 BYTE)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TASYBUILD_APLTASY_FK_I ON TASY.APLICACAO_TASY_BUILD
(CD_APLICACAO_TASY, CD_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASYBUILD_PK ON TASY.APLICACAO_TASY_BUILD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.APLICACAO_TASY_BUILD ADD (
  CONSTRAINT TASYBUILD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.APLICACAO_TASY_BUILD ADD (
  CONSTRAINT TASYBUILD_APLTASY_FK 
 FOREIGN KEY (CD_APLICACAO_TASY, CD_VERSAO) 
 REFERENCES TASY.APLICACAO_TASY_VERSAO (CD_APLICACAO_TASY,CD_VERSAO));

GRANT SELECT ON TASY.APLICACAO_TASY_BUILD TO NIVEL_1;

