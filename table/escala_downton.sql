ALTER TABLE TASY.ESCALA_DOWNTON
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DOWNTON CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DOWNTON
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_ATENDIMENTO           NUMBER(10)           NOT NULL,
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  DT_AVALIACAO             DATE                 NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  DT_LIBERACAO             DATE,
  DT_INATIVACAO            DATE,
  NM_USUARIO_INATIVACAO    VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  IE_CAIDAS_PREVIAS        VARCHAR2(1 BYTE)     NOT NULL,
  IE_USO_MEDIC             VARCHAR2(3 BYTE)     NOT NULL,
  IE_ESTADO_MENTAL         VARCHAR2(3 BYTE)     NOT NULL,
  IE_DEFCTIS_VISUAL        VARCHAR2(1 BYTE),
  IE_DEFCTIS_AUDITIVO      VARCHAR2(1 BYTE),
  IE_DEFCTIS_EXTREMIDADES  VARCHAR2(1 BYTE),
  IE_DEAMBULACAO           VARCHAR2(3 BYTE)     NOT NULL,
  QT_PONTUACAO             NUMBER(3),
  NR_HORA                  NUMBER(2),
  IE_NIVEL_ATENCAO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCDOW_ATEPACI_FK_I ON TASY.ESCALA_DOWNTON
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCDOW_I1 ON TASY.ESCALA_DOWNTON
(DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.ESCDOW_PESFISI_FK_I ON TASY.ESCALA_DOWNTON
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCDOW_PK ON TASY.ESCALA_DOWNTON
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_downton_tr
before insert or update ON TASY.ESCALA_DOWNTON for each row
declare
qt_ponto_w	number(3) := 0;
qt_reg_w	number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if  (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;

if (nvl(:new.ie_caidas_previas,'N') = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_uso_medic,'0') <> '0') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_defctis_visual,'N') = 'S')	then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_defctis_auditivo,'N') = 'S')	then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_defctis_extremidades,'N') = 'S')	then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_estado_mental,'0') <> '0') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

if (nvl(:new.ie_deambulacao,'0') <> '0') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
:new.qt_pontuacao := qt_ponto_w;

<<Final>>
qt_reg_w := 0;
end escala_downton_tr;
/


ALTER TABLE TASY.ESCALA_DOWNTON ADD (
  CONSTRAINT ESCDOW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_DOWNTON ADD (
  CONSTRAINT ESCDOW_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCDOW_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_DOWNTON TO NIVEL_1;

