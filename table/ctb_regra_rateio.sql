ALTER TABLE TASY.CTB_REGRA_RATEIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGRA_RATEIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGRA_RATEIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NR_SEQ_MES_REF       NUMBER(10)               NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TITULO            VARCHAR2(255 BYTE)       NOT NULL,
  CD_CENTRO_ORIGEM     NUMBER(8),
  CD_CONTA_ORIGEM      VARCHAR2(20 BYTE),
  CD_HISTORICO         NUMBER(10)               NOT NULL,
  PR_TOTAL_RATEIO      NUMBER(15,2)             NOT NULL,
  NR_SEQ_CALCULO       NUMBER(10)               NOT NULL,
  DT_MOVIMENTO         DATE                     NOT NULL,
  NR_LOTE_CONTABIL     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBRERAT_CENCUST_FK_I ON TASY.CTB_REGRA_RATEIO
(CD_CENTRO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRERAT_CONCONT_FK_I ON TASY.CTB_REGRA_RATEIO
(CD_CONTA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRERAT_CTBMERE_FK_I ON TASY.CTB_REGRA_RATEIO
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRERAT_ESTABEL_FK_I ON TASY.CTB_REGRA_RATEIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRERAT_HISPADR_FK_I ON TASY.CTB_REGRA_RATEIO
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBRERAT_LOTCONT_FK_I ON TASY.CTB_REGRA_RATEIO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBRERAT_PK ON TASY.CTB_REGRA_RATEIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBRERAT_PK
  MONITORING USAGE;


ALTER TABLE TASY.CTB_REGRA_RATEIO ADD (
  CONSTRAINT CTBRERAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_REGRA_RATEIO ADD (
  CONSTRAINT CTBRERAT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_ORIGEM) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CTBRERAT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_ORIGEM) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CTBRERAT_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBRERAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBRERAT_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT CTBRERAT_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL));

GRANT SELECT ON TASY.CTB_REGRA_RATEIO TO NIVEL_1;

