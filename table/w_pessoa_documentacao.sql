ALTER TABLE TASY.W_PESSOA_DOCUMENTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PESSOA_DOCUMENTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PESSOA_DOCUMENTACAO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_LINE             NUMBER(10),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_SEQ_DOCUMENTO    NUMBER(10)                NOT NULL,
  NR_CRM              VARCHAR2(20 BYTE),
  CD_MEDICO           VARCHAR2(10 BYTE),
  SI_SPECIAL_LICENSE  VARCHAR2(3 BYTE),
  NR_DOCUMENT         VARCHAR2(255 BYTE),
  DT_ENTREGA          DATE,
  DT_START            DATE,
  DT_VALIDADE         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPESDOC_PK ON TASY.W_PESSOA_DOCUMENTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PESSOA_DOCUMENTACAO ADD (
  CONSTRAINT WPESDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PESSOA_DOCUMENTACAO TO NIVEL_1;

