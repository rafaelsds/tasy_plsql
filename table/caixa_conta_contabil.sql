ALTER TABLE TASY.CAIXA_CONTA_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAIXA_CONTA_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAIXA_CONTA_CONTABIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CAIXA         NUMBER(10)               NOT NULL,
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE)        NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CACOCON_CAIXA_FK_I ON TASY.CAIXA_CONTA_CONTABIL
(NR_SEQ_CAIXA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CACOCON_CONCONT_FK_I ON TASY.CAIXA_CONTA_CONTABIL
(CD_CONTA_CONTABIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CACOCON_CONCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CACOCON_PK ON TASY.CAIXA_CONTA_CONTABIL
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.caixa_conta_contabil_atual
before insert or update ON TASY.CAIXA_CONTA_CONTABIL for each row
declare

ds_erro_w				varchar2(2000);

ds_conta_contabil_w		varchar2(40);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
ds_conta_contabil_w		:= substr(wheb_mensagem_pck.get_texto(354087,''),1,40);

ctb_consistir_conta_titulo(:new.cd_conta_contabil, ds_conta_contabil_w);

con_consiste_vigencia_conta(:new.cd_conta_contabil, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);
end if;
end if;

end;
/


ALTER TABLE TASY.CAIXA_CONTA_CONTABIL ADD (
  CONSTRAINT CACOCON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAIXA_CONTA_CONTABIL ADD (
  CONSTRAINT CACOCON_CAIXA_FK 
 FOREIGN KEY (NR_SEQ_CAIXA) 
 REFERENCES TASY.CAIXA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CACOCON_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.CAIXA_CONTA_CONTABIL TO NIVEL_1;

