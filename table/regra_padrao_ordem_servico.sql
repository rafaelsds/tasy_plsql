ALTER TABLE TASY.REGRA_PADRAO_ORDEM_SERVICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PADRAO_ORDEM_SERVICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PADRAO_ORDEM_SERVICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOCALIZACAO   NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_EQUIPAMENTO   NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_ORDEM        NUMBER(5)                NOT NULL,
  IE_STATUS_ORDEM      VARCHAR2(1 BYTE),
  NR_GRUPO_PLANEJ      NUMBER(10),
  NR_GRUPO_TRABALHO    NUMBER(10),
  NR_SEQ_TIPO_SOLUCAO  NUMBER(10),
  NR_SEQ_ESTAGIO       NUMBER(10),
  IE_CLASSIFICACAO     VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_TRAB    NUMBER(10),
  IE_TIPO              VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPAROS_ESTABEL_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPAROS_MAESTPR_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPAROS_MAESTPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPAROS_MANEQUI_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPAROS_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPAROS_MANGRPL_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_GRUPO_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPAROS_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPAROS_MANGRTR_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPAROS_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPAROS_MANLOCA_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPAROS_MANLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPAROS_MANTISO_FK_I ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_SEQ_TIPO_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPAROS_MANTISO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REPAROS_PK ON TASY.REGRA_PADRAO_ORDEM_SERVICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_PADRAO_ORDEM_SERVICO ADD (
  CONSTRAINT REPAROS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PADRAO_ORDEM_SERVICO ADD (
  CONSTRAINT REPAROS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REPAROS_MAESTPR_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.MAN_ESTAGIO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT REPAROS_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT REPAROS_MANGRPL_FK 
 FOREIGN KEY (NR_GRUPO_PLANEJ) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT REPAROS_MANGRTR_FK 
 FOREIGN KEY (NR_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT REPAROS_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA),
  CONSTRAINT REPAROS_MANTISO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_SOLUCAO) 
 REFERENCES TASY.MAN_TIPO_SOLUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_PADRAO_ORDEM_SERVICO TO NIVEL_1;

