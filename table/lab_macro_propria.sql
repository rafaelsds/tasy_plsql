ALTER TABLE TASY.LAB_MACRO_PROPRIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_MACRO_PROPRIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_MACRO_PROPRIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_MACRO             VARCHAR2(20 BYTE)        NOT NULL,
  DS_DESCRICAO         VARCHAR2(250 BYTE),
  DS_SQL               VARCHAR2(1000 BYTE)      NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LAMAPR_NM_MACRO ON TASY.LAB_MACRO_PROPRIA
(NM_MACRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAMAPR_PK ON TASY.LAB_MACRO_PROPRIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.lab_macro_propria_update
before insert or update or delete ON TASY.LAB_MACRO_PROPRIA for each row
begin

	if (regexp_like(:new.nm_macro, '[^[:alpha:]]')) then
		wheb_mensagem_pck.exibir_mensagem_abort(945475);
	end if;

end;
/


ALTER TABLE TASY.LAB_MACRO_PROPRIA ADD (
  CONSTRAINT LAMAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT LAMAPR_NM_MACRO
 UNIQUE (NM_MACRO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LAB_MACRO_PROPRIA TO NIVEL_1;

