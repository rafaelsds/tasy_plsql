ALTER TABLE TASY.COM_CLIENTE_PROPOSTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLIENTE_PROPOSTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLIENTE_PROPOSTA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_CLIENTE           NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_PROPOSTA              DATE                 NOT NULL,
  DT_APROVACAO             DATE,
  NM_USUARIO_APROV         VARCHAR2(15 BYTE),
  DS_ARQUIVO               VARCHAR2(255 BYTE),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_LIBERACAO             DATE,
  NM_USUARIO_LIB           VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  DT_VENCIMENTO            DATE,
  DS_MOT_ALT_VENC          VARCHAR2(255 BYTE),
  IE_ESCRITORIO_ATUAL      VARCHAR2(1 BYTE),
  IE_POS_VENDAS            VARCHAR2(1 BYTE),
  DT_PREV_FECHAMENTO       DATE,
  IE_STATUS                VARCHAR2(15 BYTE),
  PR_FECHAMENTO            NUMBER(15,2),
  DT_INI_IMPLANTACAO       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLPR_COMCLIE_FK_I ON TASY.COM_CLIENTE_PROPOSTA
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLPR_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLPR_PESFISI_FK_I ON TASY.COM_CLIENTE_PROPOSTA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMCLPR_PK ON TASY.COM_CLIENTE_PROPOSTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLPR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.com_cliente_proposta_update
before update ON TASY.COM_CLIENTE_PROPOSTA for each row
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	if	(:old.dt_cancelamento is null) and
		(:new.dt_cancelamento is not null) and
		(:new.nm_usuario_cancelamento is null) then
		begin
		:new.nm_usuario_cancelamento := wheb_usuario_pck.get_nm_usuario;
		end;
	end if;

	if	(:old.dt_vencimento is not null) and
		(:new.dt_vencimento is not null) and
		(:old.dt_vencimento <> :new.dt_vencimento) then
		begin
		insert into
		com_cliente_prop_log(
			nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			nr_seq_proposta,
			dt_log,
			nm_usuario_log,
			ds_motivo_log)
		values (
			com_cliente_prop_log_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			sysdate,
			:new.nm_usuario,
			:new.ds_mot_alt_venc);
		end;
	end if;
end if;
end;
/


ALTER TABLE TASY.COM_CLIENTE_PROPOSTA ADD (
  CONSTRAINT COMCLPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLIENTE_PROPOSTA ADD (
  CONSTRAINT COMCLPR_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT COMCLPR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.COM_CLIENTE_PROPOSTA TO NIVEL_1;

