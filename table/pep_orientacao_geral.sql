ALTER TABLE TASY.PEP_ORIENTACAO_GERAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEP_ORIENTACAO_GERAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEP_ORIENTACAO_GERAL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_REGISTRO                DATE               NOT NULL,
  DS_ORIENTACAO_GERAL        LONG,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_CONSULTA            NUMBER(10),
  CD_ESPECIALIDADE_MEDICO    NUMBER(5),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  NR_SEQ_CLASSIFICACAO       NUMBER(10),
  IE_RN                      VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_TIPO_TRATAMENTO_MX      NUMBER(1),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  NR_SEQ_SOAP                NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  CD_EVOLUCAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPORIG_ATCONSPEPA_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_ATEPACI_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_ATESOAP_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_CATAFAR_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_TIPO_TRATAMENTO_MX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_CLORGER_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_CLORGER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPORIG_ESPMEDI_FK2_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_ESPECIALIDADE_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_ESPMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPORIG_EVOPACI_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_OFTCONS_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPORIG_PERFIL_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPORIG_PESFISI_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_PESFISI_FK2_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_PESFISI_FK3_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPORIG_PK ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPORIG_SETATEN_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPORIG_TASASDI_FK_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPORIG_TASASDI_FK2_I ON TASY.PEP_ORIENTACAO_GERAL
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPORIG_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PEP_ORIENTACAO_GERAL_ATUAL
before insert or update ON TASY.PEP_ORIENTACAO_GERAL for each row
declare


begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

		update evolucao_paciente
		set dt_inativacao = sysdate,
		ie_situacao ='I',
		dt_atualizacao = sysdate ,
		nm_usuario = wheb_usuario_pck.get_nm_usuario,
		nm_usuario_inativacao =  wheb_usuario_pck.get_nm_usuario
		where cd_evolucao = :new.cd_evolucao;

		delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'GNRL_INSTRCT' and ie_stage = 1 and ie_soap_type = 'P';
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PEP_ORIENTACAO_GERAL_Insert
BEFORE INSERT ON TASY.PEP_ORIENTACAO_GERAL FOR EACH ROW
declare
cd_pessoa_fisica_w	varchar2(10);
BEGIN


:new.cd_setor_atendimento	:= Obter_Unidade_Atendimento(:new.nr_atendimento,'IAA','CS');



select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario	= :new.nm_usuario;

if	(cd_pessoa_fisica_w	is not null) then
	:new.cd_especialidade_medico	:= obter_especialidade_medico(cd_pessoa_fisica_w,'C');
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.pep_orientacao_pend_atual
after insert or update or delete ON TASY.PEP_ORIENTACAO_GERAL for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(inserting) or
	(updating) then

	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'OGL';
	elsif	(:old.dt_liberacao is null) and
				(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XOGL';
	end if;
	nr_atendimento_w 		:= :new.nr_atendimento;
	cd_pessoa_fisica_w	:=	:new.cd_pessoa_fisica;

	if	((nr_atendimento_w is null) or (cd_pessoa_fisica_w is null)) and (:new.nr_seq_consulta is not null) then
		select	max(a.nr_atendimento),
					max(a.cd_pessoa_fisica)
		into	   nr_atendimento_w,
					cd_pessoa_fisica_w
		from     oft_consulta a where a.nr_sequencia = :new.nr_seq_consulta;
	end if;

	begin
	if	((cd_pessoa_fisica_w is not null) or
		 (nr_atendimento_w is not null)) and
		(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
	exception
		when others then
		null;
	end;

elsif	(deleting) then
			delete 	from pep_item_pendente
			where 	IE_TIPO_REGISTRO = 'OGL'
			and	      nr_seq_registro = :old.nr_sequencia
			and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

			commit;
end if;

commit;


<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PEP_ORIENTACAO_GERAL ADD (
  CONSTRAINT PEPORIG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEP_ORIENTACAO_GERAL ADD (
  CONSTRAINT PEPORIG_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT PEPORIG_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PEPORIG_CLORGER_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIF_ORIENTACAO_GERAL (NR_SEQUENCIA),
  CONSTRAINT PEPORIG_CATAFAR_FK 
 FOREIGN KEY (CD_TIPO_TRATAMENTO_MX) 
 REFERENCES TASY.CAT_TRAT_NAO_FARMACOLOGICO (NR_SEQUENCIA),
  CONSTRAINT PEPORIG_PESFISI_FK3 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPORIG_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT PEPORIG_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPORIG_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PEPORIG_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PEPORIG_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPORIG_ESPMEDI_FK2 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDICO) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PEPORIG_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PEPORIG_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPORIG_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPORIG_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PEP_ORIENTACAO_GERAL TO NIVEL_1;

