ALTER TABLE TASY.PLS_TIPO_DESP_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_DESP_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_DESP_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MATERIAL      NUMBER(10)               NOT NULL,
  IE_TIPO_DESPESA      VARCHAR2(1 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ANTERIOR      NUMBER(10),
  CD_MATERIAL          NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTIDM_ESTABEL_FK_I ON TASY.PLS_TIPO_DESP_MAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIDM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIDM_MATERIA_FK_I ON TASY.PLS_TIPO_DESP_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIDM_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTIDM_PK ON TASY.PLS_TIPO_DESP_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTIDM_PLSMAT_FK_I ON TASY.PLS_TIPO_DESP_MAT
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIDM_PLSMAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_TIPO_DESP_MAT_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_TIPO_DESP_MAT FOR EACH ROW
declare

cd_material_w	number(6);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		select	cd_material
		into	cd_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;

		:new.cd_material	:= cd_material_w;
	end if;
end if;

END;
/


ALTER TABLE TASY.PLS_TIPO_DESP_MAT ADD (
  CONSTRAINT PLSTIDM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_DESP_MAT ADD (
  CONSTRAINT PLSTIDM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTIDM_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSTIDM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PLS_TIPO_DESP_MAT TO NIVEL_1;

