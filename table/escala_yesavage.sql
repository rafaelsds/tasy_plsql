ALTER TABLE TASY.ESCALA_YESAVAGE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_YESAVAGE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_YESAVAGE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_SATISFEITO_VIDA         NUMBER(1)          NOT NULL,
  IE_INTERROMPEU_ATIVIDADES  NUMBER(1)          NOT NULL,
  IE_VIDA_VAZIA              NUMBER(1)          NOT NULL,
  IE_ABORRECE_FREQ           NUMBER(1)          NOT NULL,
  IE_BEM_VIDA                NUMBER(1)          NOT NULL,
  IE_TEME_ALGO_RUIM          NUMBER(1)          NOT NULL,
  IE_ALEGRE                  NUMBER(1)          NOT NULL,
  IE_DESAMPARADO             NUMBER(1)          NOT NULL,
  IE_CASA                    NUMBER(1)          NOT NULL,
  IE_PROBL_MEMORIA           NUMBER(1)          NOT NULL,
  IE_MARAV_VIVO              NUMBER(1)          NOT NULL,
  IE_APENA_VIVER             NUMBER(1)          NOT NULL,
  IE_ENERGIA                 NUMBER(1)          NOT NULL,
  IE_SOLUCAO                 NUMBER(1)          NOT NULL,
  IE_SITUACAO_MELHOR         NUMBER(1)          NOT NULL,
  QT_PONTUACAO               NUMBER(3),
  NR_HORA                    NUMBER(2),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCYESA_ATEPACI_FK_I ON TASY.ESCALA_YESAVAGE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCYESA_EHRREEL_FK_I ON TASY.ESCALA_YESAVAGE
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCYESA_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCYESA_PESFISI_FK_I ON TASY.ESCALA_YESAVAGE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCYESA_PK ON TASY.ESCALA_YESAVAGE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCYESA_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCYESA_TASASDI_FK_I ON TASY.ESCALA_YESAVAGE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCYESA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCYESA_TASASDI_FK2_I ON TASY.ESCALA_YESAVAGE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCYESA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_geriatrica_yesavage_tr
before insert or update ON TASY.ESCALA_YESAVAGE for each row
declare

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

:new.qt_pontuacao := 	:new.ie_satisfeito_vida +
			:new.ie_interrompeu_atividades +
			:new.ie_vida_vazia +
			:new.ie_aborrece_freq +
			:new.ie_bem_vida +
			:new.ie_teme_algo_ruim +
			:new.ie_alegre +
			:new.ie_desamparado +
			:new.ie_casa +
			:new.ie_probl_memoria +
			:new.ie_marav_vivo +
			:new.ie_apena_viver +
			:new.ie_energia +
			:new.ie_solucao +
			:new.ie_situacao_melhor;

end escala_geriatrica_yesavage;
/


CREATE OR REPLACE TRIGGER TASY.escala_yesavage_pend_atual
after insert or update ON TASY.ESCALA_YESAVAGE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'84');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_YESAVAGE_delete
after delete ON TASY.ESCALA_YESAVAGE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '84'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_YESAVAGE ADD (
  CONSTRAINT ESCYESA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_YESAVAGE ADD (
  CONSTRAINT ESCYESA_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCYESA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCYESA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCYESA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCYESA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_YESAVAGE TO NIVEL_1;

