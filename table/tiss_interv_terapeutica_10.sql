ALTER TABLE TASY.TISS_INTERV_TERAPEUTICA_10
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_INTERV_TERAPEUTICA_10 CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_INTERV_TERAPEUTICA_10
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_VENTILACAO          VARCHAR2(1 BYTE),
  IE_CATECOLAMINAS       VARCHAR2(1 BYTE),
  IE_FLUIDOS             VARCHAR2(1 BYTE),
  IE_CATETER             VARCHAR2(1 BYTE),
  IE_PULMONAR_CATETER    VARCHAR2(1 BYTE),
  IE_DIALISE             VARCHAR2(1 BYTE),
  IE_PRESSAO_INTR        VARCHAR2(1 BYTE),
  IE_TRATAMENTO          VARCHAR2(1 BYTE),
  IE_ESPECIAL_INT        VARCHAR2(1 BYTE),
  IE_ACAO                VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  QT_PONTUACAO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIINT10_ATEPACI_FK_I ON TASY.TISS_INTERV_TERAPEUTICA_10
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIINT10_PESFISI_FK_I ON TASY.TISS_INTERV_TERAPEUTICA_10
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIINT10_PK ON TASY.TISS_INTERV_TERAPEUTICA_10
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.tiss_interv_terap_10_atual
before update or insert ON TASY.TISS_INTERV_TERAPEUTICA_10 for each row
declare

begin
select	decode(:new.ie_ventilacao,'S',5,0) +
	decode(:new.ie_catecolaminas,'S',4,0) +
	decode(:new.ie_fluidos,'S',4,0) +
	decode(:new.ie_cateter,'S',5,0) +
	decode(:new.ie_pulmonar_cateter,'S',8,0) +
	decode(:new.ie_dialise,'S',3,0) +
	decode(:new.ie_pressao_intr,'S',4,0) +
	decode(:new.ie_tratamento,'S',4,0) +
	decode(:new.ie_especial_int,'S',5,0) +
	decode(:new.ie_acao,'S',5,0)
into	:new.qt_pontuacao
from	dual;

end;
/


ALTER TABLE TASY.TISS_INTERV_TERAPEUTICA_10 ADD (
  CONSTRAINT TIINT10_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_INTERV_TERAPEUTICA_10 ADD (
  CONSTRAINT TIINT10_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT TIINT10_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.TISS_INTERV_TERAPEUTICA_10 TO NIVEL_1;

