ALTER TABLE TASY.ADIANT_PAGO_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADIANT_PAGO_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADIANT_PAGO_ARQ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ADIANTAMENTO      NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APAGARQ_ADIPAGO_FK_I ON TASY.ADIANT_PAGO_ARQ
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APAGARQ_ADIPAGO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.APAGARQ_PK ON TASY.ADIANT_PAGO_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ADIANT_PAGO_ARQ ADD (
  CONSTRAINT APAGARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADIANT_PAGO_ARQ ADD (
  CONSTRAINT APAGARQ_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ADIANT_PAGO_ARQ TO NIVEL_1;

