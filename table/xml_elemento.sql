ALTER TABLE TASY.XML_ELEMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.XML_ELEMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.XML_ELEMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_APRESENTACAO  NUMBER(10)               NOT NULL,
  NR_SEQ_PROJETO       NUMBER(10)               NOT NULL,
  NM_ELEMENTO          VARCHAR2(50 BYTE)        NOT NULL,
  DS_ELEMENTO          VARCHAR2(255 BYTE)       NOT NULL,
  DS_SQL               VARCHAR2(4000 BYTE),
  DS_CABECALHO         VARCHAR2(2000 BYTE),
  DS_GRUPO             VARCHAR2(50 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  IE_CRIAR_NULO        VARCHAR2(1 BYTE)         NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  IE_TIPO_ELEMENTO     VARCHAR2(1 BYTE)         NOT NULL,
  IE_CRIAR_ELEMENTO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO_COMPLEXO     VARCHAR2(1 BYTE)         NOT NULL,
  DS_SQL_2             VARCHAR2(4000 BYTE),
  DS_NAMESPACE         VARCHAR2(20 BYTE),
  QT_MIN_OCOR          NUMBER(5),
  QT_MAX_OCOR          NUMBER(5),
  NR_SEQ_ITEM_XSD      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.XMLELEM_PK ON TASY.XML_ELEMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.XMLELEM_XMLPROJ_FK_I ON TASY.XML_ELEMENTO
(NR_SEQ_PROJETO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.XML_ELEMENTO_tp  after update ON TASY.XML_ELEMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_APRESENTACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SQL_2,1,500);gravar_log_alteracao(substr(:old.DS_SQL_2,1,4000),substr(:new.DS_SQL_2,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL_2',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ELEMENTO,1,500);gravar_log_alteracao(substr(:old.DS_ELEMENTO,1,4000),substr(:new.DS_ELEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ELEMENTO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CABECALHO,1,500);gravar_log_alteracao(substr(:old.DS_CABECALHO,1,4000),substr(:new.DS_CABECALHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CABECALHO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ELEMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ELEMENTO,1,4000),substr(:new.IE_TIPO_ELEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ELEMENTO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_GRUPO,1,500);gravar_log_alteracao(substr(:old.DS_GRUPO,1,4000),substr(:new.DS_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GRUPO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PROJETO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PROJETO,1,4000),substr(:new.NR_SEQ_PROJETO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJETO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_ELEMENTO,1,500);gravar_log_alteracao(substr(:old.NM_ELEMENTO,1,4000),substr(:new.NM_ELEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ELEMENTO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SQL,1,500);gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CRIAR_NULO,1,500);gravar_log_alteracao(substr(:old.IE_CRIAR_NULO,1,4000),substr(:new.IE_CRIAR_NULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_NULO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_COMPLEXO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEXO,1,4000),substr(:new.IE_TIPO_COMPLEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEXO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CRIAR_ELEMENTO,1,500);gravar_log_alteracao(substr(:old.IE_CRIAR_ELEMENTO,1,4000),substr(:new.IE_CRIAR_ELEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_ELEMENTO',ie_log_w,ds_w,'XML_ELEMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.XML_ELEMENTO ADD (
  CONSTRAINT XMLELEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.XML_ELEMENTO ADD (
  CONSTRAINT XMLELEM_XMLPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.XML_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.XML_ELEMENTO TO NIVEL_1;

