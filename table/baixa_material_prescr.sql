ALTER TABLE TASY.BAIXA_MATERIAL_PRESCR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BAIXA_MATERIAL_PRESCR CASCADE CONSTRAINTS;

CREATE TABLE TASY.BAIXA_MATERIAL_PRESCR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_LOCAL_ESTOQUE     NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_GRUPO_MAT         NUMBER(3),
  CD_SUBGRUPO_MAT      NUMBER(3),
  CD_CLASSE_MAT        NUMBER(5),
  CD_MATERIAL          NUMBER(6),
  IE_RECEITA           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BAMAPRE_CLAMATE_FK_I ON TASY.BAIXA_MATERIAL_PRESCR
(CD_CLASSE_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAMAPRE_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BAMAPRE_ESTABEL_FK_I ON TASY.BAIXA_MATERIAL_PRESCR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BAMAPRE_GRUMATE_FK_I ON TASY.BAIXA_MATERIAL_PRESCR
(CD_GRUPO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAMAPRE_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BAMAPRE_LOCESTO_FK_I ON TASY.BAIXA_MATERIAL_PRESCR
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAMAPRE_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BAMAPRE_MATERIA_FK_I ON TASY.BAIXA_MATERIAL_PRESCR
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAMAPRE_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.BAMAPRE_PK ON TASY.BAIXA_MATERIAL_PRESCR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BAMAPRE_SUBMATE_FK_I ON TASY.BAIXA_MATERIAL_PRESCR
(CD_SUBGRUPO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BAMAPRE_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.BAIXA_MATERIAL_PRESCR_tp  after update ON TASY.BAIXA_MATERIAL_PRESCR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MAT,1,4000),substr(:new.CD_SUBGRUPO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MAT',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MAT,1,4000),substr(:new.CD_CLASSE_MAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MAT',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEITA,1,4000),substr(:new.IE_RECEITA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEITA',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MAT,1,4000),substr(:new.CD_GRUPO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MAT',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'BAIXA_MATERIAL_PRESCR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.BAIXA_MATERIAL_PRESCR ADD (
  CONSTRAINT BAMAPRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BAIXA_MATERIAL_PRESCR ADD (
  CONSTRAINT BAMAPRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT BAMAPRE_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT BAMAPRE_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MAT) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT BAMAPRE_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MAT) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT BAMAPRE_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MAT) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT BAMAPRE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.BAIXA_MATERIAL_PRESCR TO NIVEL_1;

