DROP TABLE TASY.W_HIGIENIZACAO CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_HIGIENIZACAO
(
  CD_SETOR_ATENDIMENTO  NUMBER(10),
  DS_LEITO1             VARCHAR2(50 BYTE),
  DS_LEITO2             VARCHAR2(50 BYTE),
  DS_LEITO3             VARCHAR2(50 BYTE),
  DS_LEITO4             VARCHAR2(50 BYTE),
  DS_LEITO5             VARCHAR2(50 BYTE),
  DS_LEITO6             VARCHAR2(50 BYTE),
  DS_LEITO7             VARCHAR2(50 BYTE),
  DS_LEITO8             VARCHAR2(50 BYTE),
  DS_LEITO9             VARCHAR2(50 BYTE),
  DS_LEITO10            VARCHAR2(50 BYTE),
  DS_LEITO11            VARCHAR2(50 BYTE),
  DS_LEITO12            VARCHAR2(50 BYTE),
  DS_LEITO13            VARCHAR2(50 BYTE),
  DS_LEITO14            VARCHAR2(50 BYTE),
  DS_LEITO15            VARCHAR2(50 BYTE),
  DS_LEITO16            VARCHAR2(50 BYTE),
  DS_LEITO17            VARCHAR2(50 BYTE),
  DS_LEITO18            VARCHAR2(50 BYTE),
  DS_LEITO19            VARCHAR2(50 BYTE),
  DS_LEITO20            VARCHAR2(50 BYTE),
  DS_LEITO21            VARCHAR2(50 BYTE),
  DS_LEITO22            VARCHAR2(50 BYTE),
  DS_LEITO23            VARCHAR2(50 BYTE),
  DS_LEITO24            VARCHAR2(50 BYTE),
  DS_LEITO25            VARCHAR2(50 BYTE),
  DS_LEITO26            VARCHAR2(50 BYTE),
  DS_LEITO27            VARCHAR2(50 BYTE),
  DS_LEITO28            VARCHAR2(50 BYTE),
  DS_LEITO29            VARCHAR2(50 BYTE),
  DS_LEITO30            VARCHAR2(50 BYTE),
  DS_LEITO31            VARCHAR2(50 BYTE),
  DS_LEITO32            VARCHAR2(50 BYTE),
  DS_LEITO33            VARCHAR2(50 BYTE),
  DS_LEITO34            VARCHAR2(50 BYTE),
  DS_LEITO35            VARCHAR2(50 BYTE),
  DS_LEITO36            VARCHAR2(50 BYTE),
  DS_LEITO37            VARCHAR2(50 BYTE),
  DS_LEITO38            VARCHAR2(50 BYTE),
  DS_LEITO39            VARCHAR2(50 BYTE),
  DS_LEITO40            VARCHAR2(50 BYTE),
  DS_LEITO41            VARCHAR2(50 BYTE),
  DS_LEITO42            VARCHAR2(50 BYTE),
  DS_LEITO43            VARCHAR2(50 BYTE),
  DS_LEITO44            VARCHAR2(50 BYTE),
  DS_LEITO45            VARCHAR2(50 BYTE),
  DS_LEITO46            VARCHAR2(50 BYTE),
  DS_LEITO47            VARCHAR2(50 BYTE),
  DS_LEITO48            VARCHAR2(50 BYTE),
  DS_LEITO49            VARCHAR2(50 BYTE),
  DS_LEITO50            VARCHAR2(50 BYTE),
  DS_LEITO51            VARCHAR2(50 BYTE),
  DS_LEITO52            VARCHAR2(50 BYTE),
  DS_LEITO53            VARCHAR2(50 BYTE),
  DS_LEITO54            VARCHAR2(50 BYTE),
  DS_LEITO55            VARCHAR2(50 BYTE),
  DS_LEITO56            VARCHAR2(50 BYTE),
  DS_LEITO57            VARCHAR2(50 BYTE),
  DS_LEITO58            VARCHAR2(50 BYTE),
  DS_LEITO59            VARCHAR2(50 BYTE),
  DS_LEITO60            VARCHAR2(50 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.W_HIGIENIZACAO TO NIVEL_1;

