ALTER TABLE TASY.MPREV_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_ATENDIMENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_INDIVIDUAL_COLETIVO   VARCHAR2(1 BYTE)     NOT NULL,
  IE_FORMA_ATENDIMENTO     VARCHAR2(1 BYTE),
  NR_SEQ_TURMA             NUMBER(10),
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  DT_INICIO                DATE                 NOT NULL,
  DT_TERMINO               DATE,
  IE_URGENCIA              VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_PLS_ATEND         NUMBER(10),
  NR_SEQ_PLS_ATEND_EVENTO  NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRATEN_ESTABEL_FK_I ON TASY.MPREV_ATENDIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEN_MPRGCTU_FK_I ON TASY.MPREV_ATENDIMENTO
(NR_SEQ_TURMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEN_PESFISI_FK_I ON TASY.MPREV_ATENDIMENTO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRATEN_PK ON TASY.MPREV_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEN_PLSASEV_FK_I ON TASY.MPREV_ATENDIMENTO
(NR_SEQ_PLS_ATEND_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRATEN_PLSATEN_FK_I ON TASY.MPREV_ATENDIMENTO
(NR_SEQ_PLS_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.mprev_atendimento_update
before update ON TASY.MPREV_ATENDIMENTO for each row
declare

nr_atendimento_w	mprev_atendimento.nr_sequencia%type;

begin
select	count(1)
into	nr_atendimento_w
from	mprev_atend_domicilio a
where 	nr_seq_atendimento = :new.nr_sequencia;

if	((:old.ie_forma_atendimento is not null) and
	(:old.ie_forma_atendimento <> nvl(:new.ie_forma_atendimento,:old.ie_forma_atendimento)) and
	(nr_atendimento_w > 0)) then
	/*antes de alterar a forma de atendimento � necess�rio excluir os dados do atendimento domiciliar!*/
	wheb_mensagem_pck.exibir_mensagem_abort(881004, '');
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.mprev_atendimento_insert
before insert ON TASY.MPREV_ATENDIMENTO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Apenas verificar se para o atendimento do tipo Coletivo foi informado uma turma.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

if	(:new.ie_individual_coletivo = 'C') and
	(:new.nr_seq_turma is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(322956);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hdm_atend_insert_update
before insert or update ON TASY.MPREV_ATENDIMENTO for each row
declare

nr_seq_grupo_coletivo_w number(10);
nr_seq_col_turma_w	number(10);

begin

if	(:new.ie_individual_coletivo = 'C') and
	(:new.nr_seq_turma is not null) then

	select	max(nr_seq_grupo_coletivo),
		max(nr_sequencia)
	into	nr_seq_grupo_coletivo_w,
		nr_seq_col_turma_w
	from	mprev_grupo_col_turma
	where	nr_sequencia = :new.nr_seq_turma;

	if	(mprev_obter_dados_grupo_atend(nr_seq_grupo_coletivo_w,'ST') = 'I') and
		(nr_seq_grupo_coletivo_w is not null) then
		--Nao e permitido gerar atendimento para grupo inativo.
		wheb_mensagem_pck.exibir_mensagem_abort(901094);
	elsif	(mprev_obter_dados_grupo_turma(nr_seq_col_turma_w,'ST') = 'N') and
		(nr_seq_col_turma_w is not null) then
		--Nao e permitido gerar atendimento para turma fora de vigencia.
		wheb_mensagem_pck.exibir_mensagem_abort(901095);
	end if;

end if;

if((:new.dt_inicio > :new.dt_termino) and (:new.dt_termino is not null)) then
    wheb_mensagem_pck.exibir_mensagem_abort(407178);
end if;

end;
/


ALTER TABLE TASY.MPREV_ATENDIMENTO ADD (
  CONSTRAINT MPRATEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_ATENDIMENTO ADD (
  CONSTRAINT MPRATEN_MPRGCTU_FK 
 FOREIGN KEY (NR_SEQ_TURMA) 
 REFERENCES TASY.MPREV_GRUPO_COL_TURMA (NR_SEQUENCIA),
  CONSTRAINT MPRATEN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MPRATEN_PLSASEV_FK 
 FOREIGN KEY (NR_SEQ_PLS_ATEND_EVENTO) 
 REFERENCES TASY.PLS_ATENDIMENTO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT MPRATEN_PLSATEN_FK 
 FOREIGN KEY (NR_SEQ_PLS_ATEND) 
 REFERENCES TASY.PLS_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT MPRATEN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MPREV_ATENDIMENTO TO NIVEL_1;

