DROP TABLE TASY.HSJ_W_MEDICAMENTO_IV CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_W_MEDICAMENTO_IV
(
  NM_PACIENTE     VARCHAR2(200 BYTE),
  NR_ATENDIMENTO  NUMBER,
  NR_PRESCRICAO   NUMBER,
  DS_MEDICAMENTO  VARCHAR2(400 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_W_MEDICAMENTO_IV TO NIVEL_1;

