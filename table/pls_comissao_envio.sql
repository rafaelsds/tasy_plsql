ALTER TABLE TASY.PLS_COMISSAO_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMISSAO_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMISSAO_ENVIO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_COMISSAO       NUMBER(10),
  DT_ENVIO              DATE                    NOT NULL,
  DS_DESTINO            VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_LOTE_COMISSAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCMSE_PK ON TASY.PLS_COMISSAO_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCMSE_PLSCOMI_FK_I ON TASY.PLS_COMISSAO_ENVIO
(NR_SEQ_COMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCMSE_PLSLTCO_FK_I ON TASY.PLS_COMISSAO_ENVIO
(NR_SEQ_LOTE_COMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_COMISSAO_ENVIO ADD (
  CONSTRAINT PLSCMSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMISSAO_ENVIO ADD (
  CONSTRAINT PLSCMSE_PLSCOMI_FK 
 FOREIGN KEY (NR_SEQ_COMISSAO) 
 REFERENCES TASY.PLS_COMISSAO (NR_SEQUENCIA),
  CONSTRAINT PLSCMSE_PLSLTCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_COMISSAO) 
 REFERENCES TASY.PLS_LOTE_COMISSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COMISSAO_ENVIO TO NIVEL_1;

