ALTER TABLE TASY.PE_PRESCRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_PRESCRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_PRESCRICAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_PRESCRICAO              DATE               NOT NULL,
  CD_PRESCRITOR              VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_LIBERACAO               DATE,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_INICIO_PRESCR           DATE,
  DT_VALIDADE_PRESCR         DATE,
  NR_PRESCRICAO              NUMBER(14),
  NR_SEQ_MODELO              NUMBER(10),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_SAEP                NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  DT_SUSPENSAO               DATE,
  NM_USUARIO_SUSP            VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NR_SAE_ORIGEM              VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_RN                      VARCHAR2(1 BYTE),
  QT_HORAS_VALIDADE          NUMBER(3),
  DT_PRIMEIRO_HORARIO        DATE,
  IE_AGORA                   VARCHAR2(1 BYTE),
  IE_ESTENDER_VALIDADE       VARCHAR2(1 BYTE),
  NR_SEQ_TRIAGEM             NUMBER(10),
  NR_RECEM_NATO              NUMBER(3),
  IE_TIPO                    VARCHAR2(5 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_PEND_PAC_ACAO       NUMBER(10),
  DT_REP_PT                  DATE,
  DT_REP_PT2                 DATE,
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_MOTIVO_SUSP         NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_PROTOCOLO               VARCHAR2(255 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  DS_NIVEL_COMPLEXIDADE      VARCHAR2(50 BYTE),
  IE_NIVEL_COMPL             NUMBER(3),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  NM_USUARIO_AUX             VARCHAR2(15 BYTE),
  DT_LIBERACAO_PLANO         DATE,
  NR_SEQ_DISP_PAC            NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_NAIS_INSURANCE      NUMBER(10),
  NR_SEQ_PREV_PRESCR         NUMBER(10),
  CD_EVOLUCAO                NUMBER(10),
  CD_ESPECIALIDADE_MED       NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.PEPRESC_ATEPACI_FK_I ON TASY.PE_PRESCRICAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_ATEPADI_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_DISP_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_CIRURGI_FK_I ON TASY.PE_PRESCRICAO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_EHRREEL_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_EVOPACI_FK_I ON TASY.PE_PRESCRICAO
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_GQAPPAC_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_PEND_PAC_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_GQAPPAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_I1 ON TASY.PE_PRESCRICAO
(DT_PRESCRICAO, NR_ATENDIMENTO, CD_PRESCRITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_I2 ON TASY.PE_PRESCRICAO
(DT_INICIO_PRESCR, DT_VALIDADE_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_I3 ON TASY.PE_PRESCRICAO
(CD_PESSOA_FISICA, DT_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_I4 ON TASY.PE_PRESCRICAO
(IE_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_I4
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_I5 ON TASY.PE_PRESCRICAO
(DT_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_I5
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_I6 ON TASY.PE_PRESCRICAO
(NR_ATENDIMENTO, DT_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_I7 ON TASY.PE_PRESCRICAO
(NM_USUARIO, DT_LIBERACAO, DT_PRESCRICAO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.PEPRESC_I8 ON TASY.PE_PRESCRICAO
(NR_ATENDIMENTO, DT_VALIDADE_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_MOTSUPR_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_NAISINS_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_PERFIL_FK_I ON TASY.PE_PRESCRICAO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_PESAEMO_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_PESAEMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_PESFISI_FK_I ON TASY.PE_PRESCRICAO
(CD_PRESCRITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_PESFISI_FK2_I ON TASY.PE_PRESCRICAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_PESFISI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_PESFISI_FK3_I ON TASY.PE_PRESCRICAO
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPRESC_PK ON TASY.PE_PRESCRICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_PRESMED_FK_I ON TASY.PE_PRESCRICAO
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRESC_SAEPERO_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_SETATEN_FK_I ON TASY.PE_PRESCRICAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_TASASDI_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_TASASDI_FK2_I ON TASY.PE_PRESCRICAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPRESC_TRPRAT_FK_I ON TASY.PE_PRESCRICAO
(NR_SEQ_TRIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRESC_TRPRAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pe_prescricao_pend_atual
after insert or update ON TASY.PE_PRESCRICAO for each row
declare

qt_reg_w	number(1);
ie_tipo_w	varchar2(10);
nm_usuario_w	varchar2(15);
ie_tipo_reg_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.dt_liberacao is null) then

	if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
		   (:new.DT_LIBERACAO_AUX is not null)then

			select 	obter_usuario_pf(:new.cd_prescritor)
			into	nm_usuario_w
			from	dual;

			Update    pep_item_pendente
			set	nm_usuario = nm_usuario_w
			where	nr_seq_sae = :new.nr_sequencia;

		end if;
end if;

	if	(nvl(:new.ie_tipo,'SAE') = 'SAPS') then
		ie_tipo_reg_w := 'SAPS';
	elsif (nvl(:new.ie_tipo,'SAE') = 'SAE') then
		ie_tipo_reg_w := 'SAE';
	end if;

if	(:new.dt_liberacao is null) then
		ie_tipo_w := ie_tipo_reg_w;
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'X' || ie_tipo_reg_w;
	end if;

if	(ie_tipo_w	is not null)then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario);
end if;
<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.PE_PRESCRICAO_aftupdate
after update ON TASY.PE_PRESCRICAO for each row
declare
qt_reg_w		number(10);
qt_acomp_w		number(10);
ie_lib_dieta_w 		varchar2(15);
ie_idade_w		number(10);
qt_atend_categ		number(10);
qt_dieta_w		number(10);
cd_risco_queda_w	number(10);
ie_considera_sae_w	varchar2(1);
cd_tipo_acomodacao_w	number(10);
ie_gerar_evolucao_care_plan_w     VARCHAR2(1);

begin
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	begin

	select	nvl(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento,'C'),sysdate,'A'),0)
	into	ie_idade_w
	from 	dual;

	if 	((ie_idade_w <= 8) or (obter_se_pac_isolamento(:new.nr_atendimento) = 'S')) then
		ie_lib_dieta_w := 'T';
	else	ie_lib_dieta_w := 'L';
	end if;

	select 	nvl(max(cd_risco_queda),0)
	into	cd_risco_queda_w
	from	parametro_atendimento;

	select	count(*)
	into	qt_reg_w
	from	PE_PRESCR_DIAG a
	where	a.nr_seq_diag	=	cd_risco_queda_w
	and	a.nr_seq_prescr = 	:new.nr_sequencia;

	select 	nvl(obter_se_considera_sae(:new.cd_setor_atendimento,:new.cd_pessoa_fisica,:new.nr_atendimento),0)
	into	ie_considera_sae_w
	from 	dual;

	select 	nvl(obter_dados_categ_conv(:new.nr_atendimento,'A'),0)
	into	cd_tipo_acomodacao_w
	from 	dual;

	if	((qt_reg_w > 0) and (ie_considera_sae_w = 'S') ) then


		select 	nvl(nr_acompanhante,0),
			nvl(qt_dieta_acomp,0)
		into	qt_acomp_w,
			qt_dieta_w
		from	atend_categoria_convenio
		where 	nr_atendimento = :new.nr_atendimento
		and	nr_seq_interno = obter_atecaco_atendimento(:new.nr_atendimento);


		if      ((qt_acomp_w = 0 ) and (qt_dieta_w = 0))then
			if ((obter_se_pac_isolamento(:new.nr_atendimento) = 'N') or
			    (ie_idade_w > 18) or
			    (ie_idade_w < 59)) then
			update  atend_categoria_convenio
			set 	nr_acompanhante = 1,
				qt_dieta_acomp = 1,
				ie_lib_dieta = ie_lib_dieta_w
			where   nr_atendimento =  :new.nr_atendimento;
			end if;

		end if;
	elsif 	(cd_risco_queda_w > 0) then
		atualizar_dados_convenio(:new.nr_atendimento,obter_setor_atendimento(:new.nr_atendimento),cd_tipo_acomodacao_w,'N',:new.nr_sequencia);
	end if;

	end;
end if;

obter_param_usuario(281, 1635, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_evolucao_care_plan_w);

if(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if ( ie_gerar_evolucao_care_plan_w = 'S' ) then
		if (:new.dt_liberacao is not null and :old.dt_inativacao is null and :new.dt_inativacao is not null  and :new.ie_situacao = 'I' and :new.cd_evolucao is not null) then
			update evolucao_paciente
			set dt_inativacao = sysdate,
			ie_situacao ='I',
			dt_atualizacao = sysdate,
			nm_usuario = wheb_usuario_pck.get_nm_usuario,
			nm_usuario_inativacao =  wheb_usuario_pck.get_nm_usuario,
			ds_justificativa = :new.ds_justificativa
			where cd_evolucao = :new.cd_evolucao;

			delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'CARE_PLAN' and ie_stage = 1  and ie_soap_type = 'P' and nr_seq_med_item = :new.nr_sequencia;
		end if;
    end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pe_prescricao_insert
before insert ON TASY.PE_PRESCRICAO for each row
declare

dt_rep_pt_w		date;
dt_rep_pt2_w		date;
dt_prim_setor_w		date;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto final;
end if;




if	(:new.nr_seq_modelo is not null) and
	(Obter_se_modelo_SAE_lib(:new.nr_seq_modelo,:new.cd_pessoa_fisica, :new.nr_atendimento) = 'N') then
	-- Nao e possivel continuar esta acao pois o Modelo DS_MODELO nao esta liberado para este Perfil e Paciente.
	Wheb_mensagem_pck.exibir_mensagem_abort( 261365 , 'DS_MODELO=' || substr(obter_desc_mod_sae(:new.nr_seq_modelo),1,255));
end if;

select	max(hr_inicio_prescricao)
into	dt_prim_setor_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

if	(dt_prim_setor_w is not null) then

	dt_rep_pt_w		:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));

	dt_prim_setor_w		:= to_date(to_char(nvl(:new.dt_inicio_prescr,:new.dt_prescricao),'dd/mm/yyyy') || ' ' || to_char(dt_prim_setor_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

	if	(nvl(:new.dt_inicio_prescr,:new.dt_prescricao)	< dt_prim_setor_w) then
		dt_rep_pt_w	:= establishment_timezone_utils.startOfDay(nvl(:new.dt_inicio_prescr,:new.dt_prescricao) - 1);
	end if;

	if	(:new.qt_horas_validade	> 24) then

		dt_rep_pt2_w	:= (dt_rep_pt_w + 1);

	else
		dt_rep_pt2_w	:= null;
	end if;

	:new.dt_rep_pt	:= dt_rep_pt_w;
	:new.dt_rep_pt2	:= dt_rep_pt2_w;

else

	:new.dt_rep_pt	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));
	:new.dt_rep_pt2	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_validade_prescr,nvl(:new.dt_inicio_prescr,:new.dt_prescricao)));

end if;

<<final>>
null;

end;
/


CREATE OR REPLACE TRIGGER TASY.pe_prescricao_delete
before delete ON TASY.PE_PRESCRICAO for each row
declare
qt_reg_w	number(10);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:old.dt_liberacao is not null) then

	wheb_mensagem_pck.exibir_mensagem_abort(196705);
end if;


<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.Smart_SAE_SAPS_aftinsert
after insert or update ON TASY.PE_PRESCRICAO for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
gera_sae_w	varchar2(1);

begin
	reg_integracao_w.cd_estab_documento := OBTER_ESTABELECIMENTO_ATIVO;

	if (:new.ie_tipo = 'SAE') then

		select nvl(max('S'),'N')
		into  gera_sae_w
		from  PE_PRESCR_PROC
		where nr_seq_prescr = :new.nr_sequencia
		and   obter_se_gera_sae_sis_ter(NR_SEQ_PROC,'I') = 'S'
		and	  Smart_obter_se_sae(NR_SEQ_PROC, reg_integracao_w.cd_estab_documento, '303','PE',wheb_usuario_pck.get_nm_usuario) = 'S';

	end if;

	if (:new.ie_tipo = 'SAPS') then
		select nvl(max('S'),'N')
		into gera_sae_w
		from pe_prescr_item_result
		where nr_seq_prescr = :new.nr_sequencia
		and obter_se_gera_sae_sis_ter(NR_SEQ_ITEM,'A') = 'S';
	end if;

	if  (:old.DT_LIBERACAO is null) and
		(:new.DT_LIBERACAO is not null) and
		(gera_sae_w = 'S') then
		BEGIN
			if (:new.ie_tipo = 'SAE') then
				gerar_int_padrao.gravar_integracao('303', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w, 'DS_OPERACAO_P=CREATE');
			elsif (:new.ie_tipo = 'SAPS') then
				gerar_int_padrao.gravar_integracao('307', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w, 'DS_OPERACAO_P=CREATE');
			end if;
		END;
	elsif  (:old.DT_INATIVACAO is null) and
		(:new.DT_INATIVACAO is not null) then
		BEGIN
			if (:new.ie_tipo = 'SAE') then
				gerar_int_padrao.gravar_integracao('303', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w, 'DS_OPERACAO_P=DELETE');
			elsif (:new.ie_tipo = 'SAPS') then
				gerar_int_padrao.gravar_integracao('307', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w, 'DS_OPERACAO_P=DELETE');
			end if;
		END;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.UPDATE_DT_EVT_PE_PRESCRICAO
after insert or update ON TASY.PE_PRESCRICAO for each row
declare

begin
if	wheb_usuario_pck.get_ie_executar_trigger = 'S' then

  if (:old.nr_seq_pend_pac_acao is null and :new.nr_seq_pend_pac_acao is not null) then

    if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') then
      update_dt_exec_prt_int_pac_evt(:new.nr_seq_pend_pac_acao, :new.dt_atualizacao);
    end if;

  end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pe_prescricao_atual
before insert or update ON TASY.PE_PRESCRICAO for each row
declare
qt_reg_w		number(1);
dt_inicio_prescricao_w	date;
ie_hora_w		varchar2(10);
ie_estender_w		varchar2(10);
ie_prox_hor_w		varchar2(10);
dt_rep_pt_w		date;
dt_rep_pt2_w		date;
dt_prim_setor_w		date;
ie_sepse_lib_sae_w	varchar2(1);
ie_liberado_modelo_w 	varchar2(1);
qt_evento_w		number(10);
reg_integracao_p	gerar_int_padrao.reg_integracao;
cd_pessoa_fisica_w	atendimento_paciente.cd_pessoa_fisica%type;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.cd_setor_atendimento is null) and
	(:new.nr_atendimento is not null)then

	:new.cd_setor_atendimento	:= obter_setor_Atendimento(:new.nr_atendimento);
end if;

if	(nvl(:old.DT_PRESCRICAO,sysdate+10) <> :new.DT_PRESCRICAO) and
	(:new.DT_PRESCRICAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_PRESCRICAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.nr_seq_modelo is not null) and (:new.dt_liberacao is null) then

	select max(Obter_se_modelo_SAE_lib(:new.nr_seq_modelo,:new.cd_pessoa_fisica, :new.nr_atendimento))
	into   ie_liberado_modelo_w
	from   dual;

	if ( ie_liberado_modelo_w = 'N') then
		-- Nao e possivel continuar esta acao pois o Modelo "#@DS_MODELO#@" nao esta liberado para este Perfil/Paciente.
		Wheb_mensagem_pck.exibir_mensagem_abort( 261365 , 'DS_MODELO='|| substr(obter_desc_mod_sae(:new.nr_seq_modelo),1,255));
	end if;

end if;

select	max(HR_INICIO_PRESCRICAO_SAE)
into	dt_inicio_prescricao_w
from	setor_atendimento
where	cd_setor_atendimento	= :new.cd_setor_atendimento;

if	(:new.DT_PRIMEIRO_HORARIO	is null) then
	ie_hora_w	:= obter_valor_param_usuario(281,388, obter_perfil_ativo, :new.nm_usuario, 0);
	ie_prox_hor_w	:= obter_valor_param_usuario(281,782, obter_perfil_ativo, :new.nm_usuario, 0);
	if	(ie_hora_w	= '1') and
		(dt_inicio_prescricao_w	is not null) then
		:new.DT_PRIMEIRO_HORARIO	:= ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(:new.dt_prescricao, dt_inicio_prescricao_w);
		if	(:new.DT_PRIMEIRO_HORARIO	< sysdate) and
			(ie_prox_hor_w	= 'S') then
			:new.DT_PRIMEIRO_HORARIO	:= trunc(:new.dt_prescricao,'hh24') + 1/24;
		end if;
	elsif	(ie_hora_w	= '2') then
		:new.DT_PRIMEIRO_HORARIO	:= trunc(:new.dt_prescricao,'hh24') + 1/24;
	end if;
end if;

:new.QT_HORAS_VALIDADE	:= nvl(:new.QT_HORAS_VALIDADE,24);

if	(:new.dt_primeiro_horario is not null) then
	if	(to_char(:new.dt_primeiro_horario,'hh24:mi') < to_char(:new.dt_prescricao,'hh24:mi')) then
		:new.dt_inicio_prescr := ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(:new.dt_prescricao+1, :new.dt_primeiro_horario);
	else
		:new.dt_inicio_prescr := ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(:new.dt_prescricao, :new.dt_primeiro_horario);
	end if;
else
	:new.dt_inicio_prescr := :new.dt_prescricao;
end if;

if	(:new.ie_agora	= 'S') then

	ie_estender_w	:= obter_valor_param_usuario(281,775, obter_perfil_ativo, :new.nm_usuario, 0);
	if	(ie_estender_w	<> 'N') then
		:new.QT_HORAS_VALIDADE	:= obter_horas_validade_SAE(:new.dt_inicio_prescr,:new.nr_atendimento,ie_estender_w,'A',:new.dt_prescricao,:new.nr_sequencia);
		:new.dt_validade_prescr := trunc(:new.dt_inicio_prescr,'hh24') + nvl(:new.qt_horas_validade,24) / 24 - 1/86400 ;
	else
		if	(dt_inicio_prescricao_w	 is null) then
			:new.dt_validade_prescr	:= :new.dt_inicio_prescr + 1 - 1/86400;
		else
			:new.dt_validade_prescr	:= ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(:new.dt_inicio_prescr, dt_inicio_prescricao_w) - 1/86400;
			if	(:new.dt_validade_prescr	< :new.dt_inicio_prescr) then
				:new.dt_validade_prescr	:= :new.dt_validade_prescr + 1;
			end if;
		end if;
		:new.QT_HORAS_VALIDADE	:= TRUNC((:new.dt_validade_prescr - :new.dt_inicio_prescr) * 24) + 1;

	end if;
else
	:new.dt_validade_prescr := trunc(:new.dt_inicio_prescr,'hh24') + nvl(:new.qt_horas_validade,24) / 24 - 1/86400;

end if;

select	max(hr_inicio_prescricao)
into	dt_prim_setor_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

if	(dt_prim_setor_w is not null) then

	dt_rep_pt_w		:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));

	dt_prim_setor_w		:= ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(nvl(:new.dt_inicio_prescr,:new.dt_prescricao), dt_prim_setor_w);

	if	(nvl(:new.dt_inicio_prescr,:new.dt_prescricao)	< dt_prim_setor_w) then
		dt_rep_pt_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_inicio_prescr,:new.dt_prescricao) - 1);
	end if;

	if	(:new.qt_horas_validade	> 24) then

		dt_rep_pt2_w	:= (dt_rep_pt_w + 1);

	else
		dt_rep_pt2_w	:= null;
	end if;

	:new.dt_rep_pt	:= dt_rep_pt_w;
	:new.dt_rep_pt2	:= dt_rep_pt2_w;
else

	:new.dt_rep_pt	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));
	:new.dt_rep_pt2	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(:new.dt_validade_prescr,nvl(:new.dt_inicio_prescr,:new.dt_prescricao)));

end if;


if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.nr_atendimento is not null) then

	begin
		gravar_agend_rothman(:new.nr_atendimento,:new.nr_sequencia,'SAE',:new.nm_usuario);
	exception
	when others then
		null;
	end;

	begin
	select	1
	into	qt_evento_w
	from	intpd_eventos
	where	ie_situacao	= 'A'
	and	ie_evento	= '406'
	and	rownum		= 1;
	exception
	when others then
		qt_evento_w 	:= 0;
	end;

	if	(qt_evento_w > 0) then

		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	atendimento_paciente
		where	nr_atendimento	= :new.nr_atendimento;

		reg_integracao_p.ie_operacao		:= 'I';
		reg_integracao_p.cd_pessoa_fisica	:= cd_pessoa_fisica_w;
		reg_integracao_p.nr_atendimento		:= :new.nr_atendimento;

		gerar_int_padrao.gravar_integracao('406', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p );

	end if;
end if;

if	(:new.dt_inativacao is not null) and
	(:old.dt_inativacao is null) and
	(:new.nr_atendimento is not null) then

	begin
	select	1
	into	qt_evento_w
	from	intpd_eventos
	where	ie_situacao	= 'A'
	and	ie_evento	= '406'
	and	rownum		= 1;
	exception
	when others then
		qt_evento_w 	:= 0;
	end;

	if	(qt_evento_w > 0) then

		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	atendimento_paciente
		where	nr_atendimento	= :new.nr_atendimento;

		reg_integracao_p.ie_operacao		:= 'E';
		reg_integracao_p.cd_pessoa_fisica	:= cd_pessoa_fisica_w;
		reg_integracao_p.nr_atendimento		:= :new.nr_atendimento;

		gerar_int_padrao.gravar_integracao('406', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p );

	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PE_PRESCRICAO ADD (
  CONSTRAINT PEPRESC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_PRESCRICAO ADD (
  CONSTRAINT PEPRESC_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT PEPRESC_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_PESFISI_FK 
 FOREIGN KEY (CD_PRESCRITOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPRESC_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPRESC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PEPRESC_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PEPRESC_PESAEMO_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.PE_SAE_MODELO (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT PEPRESC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PEPRESC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PEPRESC_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_TRPRAT_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM) 
 REFERENCES TASY.TRIAGEM_PRONTO_ATEND (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPRESC_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_GQAPPAC_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC_ACAO) 
 REFERENCES TASY.GQA_PEND_PAC_ACAO (NR_SEQUENCIA),
  CONSTRAINT PEPRESC_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPRESC_PESFISI_FK3 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPRESC_ATEPADI_FK 
 FOREIGN KEY (NR_SEQ_DISP_PAC) 
 REFERENCES TASY.ATEND_PAC_DISPOSITIVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PE_PRESCRICAO TO NIVEL_1;

