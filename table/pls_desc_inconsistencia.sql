ALTER TABLE TASY.PLS_DESC_INCONSISTENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_DESC_INCONSISTENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_DESC_INCONSISTENCIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_COBRANCA      NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_MATRICULA         VARCHAR2(30 BYTE),
  VL_TITULO            NUMBER(15,2),
  DS_INCONSISTENCIA    VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSDEIN_COBESCR_FK_I ON TASY.PLS_DESC_INCONSISTENCIA
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSDEIN_ESTABEL_FK_I ON TASY.PLS_DESC_INCONSISTENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSDEIN_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSDEIN_PK ON TASY.PLS_DESC_INCONSISTENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSDEIN_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_DESC_INCONSISTENCIA ADD (
  CONSTRAINT PLSDEIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_DESC_INCONSISTENCIA ADD (
  CONSTRAINT PLSDEIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSDEIN_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_DESC_INCONSISTENCIA TO NIVEL_1;

