ALTER TABLE TASY.TIPO_LOCALIZAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_LOCALIZAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_LOCALIZAR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_LOCALIZADOR          VARCHAR2(40 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_SQL                  VARCHAR2(4000 BYTE)   NOT NULL,
  DS_ARVORE               VARCHAR2(4000 BYTE),
  IE_RESTRINGE_ESTAB      VARCHAR2(1 BYTE)      NOT NULL,
  IE_LOCALIZADOR_INTERNO  VARCHAR2(15 BYTE),
  IE_APRESENTACAO         VARCHAR2(3 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_EXP_LOCALIZADOR      NUMBER(10),
  IE_POSICAO_FILTRO_HTML  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPLOCA_DICEXPR_FK_I ON TASY.TIPO_LOCALIZAR
(CD_EXP_LOCALIZADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPLOCA_PK ON TASY.TIPO_LOCALIZAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TIPO_LOCALIZAR_tp  after update ON TASY.TIPO_LOCALIZAR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOCALIZADOR,1,4000),substr(:new.DS_LOCALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOCALIZADOR',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXP_LOCALIZADOR,1,4000),substr(:new.CD_EXP_LOCALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_LOCALIZADOR',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTRINGE_ESTAB,1,4000),substr(:new.IE_RESTRINGE_ESTAB,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTRINGE_ESTAB',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOCALIZADOR_INTERNO,1,4000),substr(:new.IE_LOCALIZADOR_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOCALIZADOR_INTERNO',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APRESENTACAO,1,4000),substr(:new.IE_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APRESENTACAO',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ARVORE,1,4000),substr(:new.DS_ARVORE,1,4000),:new.nm_usuario,nr_seq_w,'DS_ARVORE',ie_log_w,ds_w,'TIPO_LOCALIZAR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TIPO_LOCALIZAR ADD (
  CONSTRAINT TIPLOCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_LOCALIZAR ADD (
  CONSTRAINT TIPLOCA_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_LOCALIZADOR) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.TIPO_LOCALIZAR TO NIVEL_1;

