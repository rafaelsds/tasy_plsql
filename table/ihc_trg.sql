ALTER TABLE TASY.IHC_TRG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IHC_TRG CASCADE CONSTRAINTS;

CREATE TABLE TASY.IHC_TRG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CLAIM         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_AMBULANCE         VARCHAR2(15 BYTE),
  VL_CHARGE            NUMBER(12,4),
  IE_CHARGE_RAISED     VARCHAR2(1 BYTE),
  QT_DISTANCE          NUMBER(5),
  DS_FROM_LOCALITY     VARCHAR2(40 BYTE),
  DT_SERVICE           DATE,
  DT_START             NUMBER(4),
  DS_TO_LOCALITY       VARCHAR2(40 BYTE),
  QT_TRANSPORT         VARCHAR2(4 BYTE),
  IE_TRANSPORT         VARCHAR2(1 BYTE),
  DT_START_TIME        DATE,
  QT_TRANSPORT_TIME    VARCHAR2(4 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IHCTRGE_IHCCLAI_FK_I ON TASY.IHC_TRG
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IHCTRGE_PK ON TASY.IHC_TRG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IHC_TRG ADD (
  CONSTRAINT IHCTRGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IHC_TRG ADD (
  CONSTRAINT IHCTRGE_IHCCLAI_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.IHC_CLAIM (NR_SEQUENCIA));

GRANT SELECT ON TASY.IHC_TRG TO NIVEL_1;

