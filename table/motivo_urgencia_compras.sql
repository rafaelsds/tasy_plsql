ALTER TABLE TASY.MOTIVO_URGENCIA_COMPRAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_URGENCIA_COMPRAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_URGENCIA_COMPRAS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_MOTIVO            VARCHAR2(80 BYTE)        NOT NULL,
  IE_SOLICITACAO       VARCHAR2(1 BYTE)         NOT NULL,
  IE_ORDEM             VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOURGCO_ESTABEL_FK_I ON TASY.MOTIVO_URGENCIA_COMPRAS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MOURGCO_PK ON TASY.MOTIVO_URGENCIA_COMPRAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MOTIVO_URGENCIA_COMPRAS_tp  after update ON TASY.MOTIVO_URGENCIA_COMPRAS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MOTIVO_URGENCIA_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'MOTIVO_URGENCIA_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MOTIVO,1,4000),substr(:new.DS_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO',ie_log_w,ds_w,'MOTIVO_URGENCIA_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDEM,1,4000),substr(:new.IE_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDEM',ie_log_w,ds_w,'MOTIVO_URGENCIA_COMPRAS',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLICITACAO,1,4000),substr(:new.IE_SOLICITACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLICITACAO',ie_log_w,ds_w,'MOTIVO_URGENCIA_COMPRAS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MOTIVO_URGENCIA_COMPRAS ADD (
  CONSTRAINT MOURGCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOTIVO_URGENCIA_COMPRAS ADD (
  CONSTRAINT MOURGCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MOTIVO_URGENCIA_COMPRAS TO NIVEL_1;

