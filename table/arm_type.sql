ALTER TABLE TASY.ARM_TYPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ARM_TYPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ARM_TYPE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DS_TYPE              VARCHAR2(255 BYTE)       NOT NULL,
  DS_DESCRIPTION       VARCHAR2(4000 BYTE),
  NR_SEQ_AREA          NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ARMTYPE_ARMAREA_FK_I ON TASY.ARM_TYPE
(NR_SEQ_AREA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ARMTYPE_PK ON TASY.ARM_TYPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ARM_TYPE_tp  after update ON TASY.ARM_TYPE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ARM_TYPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DESCRIPTION,1,500);gravar_log_alteracao(substr(:old.DS_DESCRIPTION,1,4000),substr(:new.DS_DESCRIPTION,1,4000),:new.nm_usuario,nr_seq_w,'DS_DESCRIPTION',ie_log_w,ds_w,'ARM_TYPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_TYPE,1,500);gravar_log_alteracao(substr(:old.DS_TYPE,1,4000),substr(:new.DS_TYPE,1,4000),:new.nm_usuario,nr_seq_w,'DS_TYPE',ie_log_w,ds_w,'ARM_TYPE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'ARM_TYPE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ARM_TYPE ADD (
  CONSTRAINT ARMTYPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ARM_TYPE ADD (
  CONSTRAINT ARMTYPE_ARMAREA_FK 
 FOREIGN KEY (NR_SEQ_AREA) 
 REFERENCES TASY.ARM_AREA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ARM_TYPE TO NIVEL_1;

