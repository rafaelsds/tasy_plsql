ALTER TABLE TASY.COBRANCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COBRANCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COBRANCA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_TITULO              NUMBER(10),
  NR_SEQ_CHEQUE          NUMBER(10),
  IE_STATUS              VARCHAR2(1 BYTE)       NOT NULL,
  VL_ORIGINAL            NUMBER(15,2)           NOT NULL,
  VL_ACOBRAR             NUMBER(15,2)           NOT NULL,
  DT_PREVISAO_COBRANCA   DATE                   NOT NULL,
  DT_INCLUSAO            DATE                   NOT NULL,
  CD_TIPO_PORTADOR       NUMBER(5),
  CD_PORTADOR            NUMBER(10),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_COBRADOR        NUMBER(10),
  NR_SEQ_PROCESSO        NUMBER(10),
  NR_SEQ_ETAPA           NUMBER(10),
  NR_SEQ_TIPO_COBRANCA   NUMBER(10),
  NR_SEQ_NOTIFIC_ITEM    NUMBER(10),
  NR_SEQ_REGRA_RAT_COBR  NUMBER(10),
  DT_FECHAMENTO          DATE,
  NR_SEQ_CLASSIF_PESSOA  NUMBER(10),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  CD_CGC                 VARCHAR2(14 BYTE),
  DT_CANCELAMENTO        DATE,
  DT_LIB_BAIXA           DATE,
  TX_JUROS               NUMBER(7,4),
  VL_JUROS_COBR          NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COBRANC_CHEQUES_FK_I ON TASY.COBRANCA
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_CLASPES_FK_I ON TASY.COBRANCA
(NR_SEQ_CLASSIF_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_CLASPES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_COBRADO_FK_I ON TASY.COBRANCA
(NR_SEQ_COBRADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_COBRADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_ESTABEL_FK_I ON TASY.COBRANCA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_ETACOBR_FK_I ON TASY.COBRANCA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_ETACOBR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_I1 ON TASY.COBRANCA
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_I2 ON TASY.COBRANCA
(DT_PREVISAO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_PESFISI_FK_I ON TASY.COBRANCA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_PESJURI_FK_I ON TASY.COBRANCA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COBRANC_PK ON TASY.COBRANCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_PLSNOIT_FK_I ON TASY.COBRANCA
(NR_SEQ_NOTIFIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_PLSNOIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_PORTADO_FK_I ON TASY.COBRANCA
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_PROCOBR_FK_I ON TASY.COBRANCA
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COBRANC_REGRACO_FK_I ON TASY.COBRANCA
(NR_SEQ_REGRA_RAT_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_REGRACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_TIPCOBR_FK_I ON TASY.COBRANCA
(NR_SEQ_TIPO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COBRANC_TIPCOBR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COBRANC_TITRECE_FK_I ON TASY.COBRANCA
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.COBRANCA_ATUAL
BEFORE INSERT OR UPDATE ON TASY.COBRANCA FOR EACH ROW
DECLARE

cd_pessoa_fisica_w	varchar2(255) := null;
cd_cgc_w		varchar2(255) := null;
ie_inclui_tit_canc_w	parametro_contas_receber.ie_inclui_tit_canc%type;
ie_situacao_w			titulo_receber.ie_situacao%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:new.nr_seq_cheque is not null) then
	select	max(cd_pessoa_fisica),
		max(cd_cgc)
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	cheque_cr
	where	nr_seq_cheque	= :new.nr_seq_cheque;
end if;

if	(:new.nr_titulo is not null) then
	select	max(cd_pessoa_fisica),
		max(cd_cgc),
			max(ie_situacao)
	into	cd_pessoa_fisica_w,
		cd_cgc_w,
			ie_situacao_w
	from	titulo_receber
	where	nr_titulo	= :new.nr_titulo;

	if (inserting) then

		select	nvl(max(a.ie_inclui_tit_canc),'S')
		into	ie_inclui_tit_canc_w
		from	parametro_contas_receber a
		where	a.cd_estabelecimento = :new.cd_estabelecimento;

		if (ie_situacao_w = '3') and (nvl(ie_inclui_tit_canc_w,'S') = 'N') then
			--A cobranca nao pode ser gerada pois o titulo #@nr_titulo_w#@ esta cancelado. Verifique na funcao Parametros do Contas a Receber, aba cobranca.
			wheb_mensagem_pck.exibir_mensagem_abort(717906, 'NR_TITULO_W=' || :new.nr_titulo);
		end if;

	end if;

end if;

:new.cd_pessoa_fisica	:= cd_pessoa_fisica_w;
:new.cd_cgc		:= cd_cgc_w;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.HISTORICO_PACIENTE_COBRANCA
AFTER INSERT ON TASY.COBRANCA FOR EACH ROW
DECLARE

nr_interno_conta_w          titulo_receber.nr_interno_conta%type;
nr_sequencia_hist_cob_w     tipo_hist_cob.nr_sequencia%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    select max(nr_interno_conta)
    into nr_interno_conta_w
    from titulo_receber
    where nr_titulo = :NEW.nr_titulo;

    if (nr_interno_conta_w is not null) then
        select max(nr_sequencia)
        into nr_sequencia_hist_cob_w
        from tipo_hist_cob
        where nvl(ie_obs_conta_paciente,'N') = 'S'
        and nvl(ie_situacao, 'I') = 'A';
    end if;

    if (nr_sequencia_hist_cob_w is not null) then
        for i in (select cpo.ds_observacao
        from conta_paciente_observacao cpo
        where nr_interno_conta = nr_interno_conta_w
        and dt_liberacao is not null) loop

            insert into cobranca_historico (nr_sequencia,
                                            nr_seq_cobranca,
                                            dt_atualizacao,
                                            nm_usuario,
                                            nr_seq_historico,
                                            vl_historico,
                                            ds_historico,
                                            dt_historico,
                                            dt_atualizacao_nrec,
                                            nm_usuario_nrec,
                                            nr_seq_retorno,
                                            id_importacao)
            values (cobranca_historico_seq.nextval,
                    :NEW.nr_sequencia,
                    sysdate,
                    obter_usuario_ativo,
                    nr_sequencia_hist_cob_w,
                    null,
                    i.ds_observacao,
                    sysdate,
                    null,
                    null,
                    null,
                    null);
        end loop;
    end if;
end if;
end;
/


ALTER TABLE TASY.COBRANCA ADD (
  CONSTRAINT COBRANC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COBRANCA ADD (
  CONSTRAINT COBRANC_COBRADO_FK 
 FOREIGN KEY (NR_SEQ_COBRADOR) 
 REFERENCES TASY.COBRADOR (NR_SEQUENCIA),
  CONSTRAINT COBRANC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COBRANC_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE),
  CONSTRAINT COBRANC_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT COBRANC_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT COBRANC_PROCOBR_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PROCESSO_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT COBRANC_ETACOBR_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.ETAPA_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT COBRANC_TIPCOBR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COBRANCA) 
 REFERENCES TASY.TIPO_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT COBRANC_PLSNOIT_FK 
 FOREIGN KEY (NR_SEQ_NOTIFIC_ITEM) 
 REFERENCES TASY.PLS_NOTIFICACAO_ITEM (NR_SEQUENCIA),
  CONSTRAINT COBRANC_REGRACO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_RAT_COBR) 
 REFERENCES TASY.REGRA_RATEIO_COBRADOR (NR_SEQUENCIA),
  CONSTRAINT COBRANC_CLASPES_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PESSOA) 
 REFERENCES TASY.CLASSIF_PESSOA (NR_SEQUENCIA),
  CONSTRAINT COBRANC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COBRANC_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.COBRANCA TO NIVEL_1;

