ALTER TABLE TASY.ESCALA_NRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NRS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  QT_ALTURA_CM               NUMBER(5,2)        NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_PESO                    NUMBER(6,3)        NOT NULL,
  QT_IMC                     NUMBER(4,1)        NOT NULL,
  QT_GRAVIDADE_DOENCA        NUMBER(2)          NOT NULL,
  QT_PESO_HABITUAL           NUMBER(6,3)        NOT NULL,
  QT_PONTUACAO               NUMBER(3),
  QT_ESTADO_NUTRICIONAL      NUMBER(2)          NOT NULL,
  QT_IDADE                   NUMBER(3)          NOT NULL,
  IE_ALIMENTACAO_REDUZIDA    VARCHAR2(1 BYTE),
  IE_DOENCA_UTI              VARCHAR2(1 BYTE),
  IE_PERDA_PESO              VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(1000 BYTE),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCANRS_ATEPACI_FK_I ON TASY.ESCALA_NRS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANRS_EHRREEL_FK_I ON TASY.ESCALA_NRS
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANRS_PESFISI_FK_I ON TASY.ESCALA_NRS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCANRS_PK ON TASY.ESCALA_NRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCANRS_TASASDI_FK_I ON TASY.ESCALA_NRS
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANRS_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCANRS_TASASDI_FK2_I ON TASY.ESCALA_NRS
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCANRS_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_nrs_atual
before insert or update ON TASY.ESCALA_NRS for each row
declare
qt_total_w		number(10)	:= 0;
nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w		number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and	ie_evento_disp		= 'LNRS'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';

begin

if (:new.QT_IMC <= 0 or :new.QT_IMC is null) and (Obter_IMC(:new.QT_PESO,:new.QT_ALTURA_CM,obter_pessoa_atendimento(:new.NR_ATENDIMENTO,'C')) is not null) then
	:new.QT_IMC := Obter_IMC(:new.QT_PESO,:new.QT_ALTURA_CM,obter_pessoa_atendimento(:new.NR_ATENDIMENTO,'C'));
end if;


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

qt_total_w	:= qt_total_w + :new.QT_ESTADO_NUTRICIONAL;
qt_total_w	:= qt_total_w + :new.QT_GRAVIDADE_DOENCA;
if	(:new.qt_idade	>= 70) then
	qt_total_w	:= qt_total_w + 1;
end if;

:new.qt_pontuacao	:= qt_total_w;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) and
	(:NEW.qt_pontuacao >= 3)then
	begin
	select	max(cd_estabelecimento),
		max(cd_pessoa_fisica)
	into	cd_estabelecimento_w,
		cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario);
		end;
	end loop;
	close C01;
	end;
end if;

EXCEPTION
WHEN OTHERS THEN
	NULL;

end;
/


ALTER TABLE TASY.ESCALA_NRS ADD (
  CONSTRAINT ESCANRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_NRS ADD (
  CONSTRAINT ESCANRS_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCANRS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCANRS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCANRS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCANRS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_NRS TO NIVEL_1;

