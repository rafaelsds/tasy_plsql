ALTER TABLE TASY.D301_DATASET_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.D301_DATASET_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.D301_DATASET_ENVIO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_ARQUIVO          NUMBER(10)            NOT NULL,
  NR_ORDEM                NUMBER(5)             NOT NULL,
  IE_DATASET              VARCHAR2(4 BYTE)      NOT NULL,
  NR_ATENDIMENTO          NUMBER(10),
  NR_INTERNO_CONTA        NUMBER(10),
  NR_SEQ_NOTA_FISCAL      NUMBER(10),
  NR_SEQ_ATEND_PREV_ALTA  NUMBER(10),
  NR_SEQ_ATEND_ALTA       NUMBER(10),
  NR_SEQ_IMPORT_301_INKA  NUMBER(10),
  IE_STATUS_VALIDACAO     VARCHAR2(1 BYTE),
  IE_STATUS_ENVIO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.D301DTE_D301ARQ_FK_I ON TASY.D301_DATASET_ENVIO
(NR_SEQ_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.D301DTE_PK ON TASY.D301_DATASET_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.D301_DATASET_ENVIO ADD (
  CONSTRAINT D301DTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.D301_DATASET_ENVIO ADD (
  CONSTRAINT D301DTE_D301ARQ_FK 
 FOREIGN KEY (NR_SEQ_ARQUIVO) 
 REFERENCES TASY.D301_ARQUIVO_ENVIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.D301_DATASET_ENVIO TO NIVEL_1;

