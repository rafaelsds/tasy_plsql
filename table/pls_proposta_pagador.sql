ALTER TABLE TASY.PLS_PROPOSTA_PAGADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROPOSTA_PAGADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROPOSTA_PAGADOR
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_PROPOSTA           NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_CGC_PAGADOR            VARCHAR2(14 BYTE),
  CD_PAGADOR                VARCHAR2(10 BYTE),
  CD_CONDICAO_PAGAMENTO     NUMBER(10)          NOT NULL,
  IE_ENDERECO_BOLETO        VARCHAR2(4 BYTE)    NOT NULL,
  DT_DIA_VENCIMENTO         NUMBER(2)           NOT NULL,
  NR_SEQ_FORMA_COBRANCA     VARCHAR2(2 BYTE)    NOT NULL,
  CD_BANCO                  NUMBER(5),
  CD_AGENCIA_BANCARIA       VARCHAR2(8 BYTE),
  IE_DIGITO_AGENCIA         VARCHAR2(1 BYTE),
  CD_CONTA                  VARCHAR2(20 BYTE),
  NR_SEQ_CONTRATO_PAGADOR   NUMBER(10),
  IE_ENVIA_COBRANCA         VARCHAR2(1 BYTE),
  NR_SEQ_PAGADOR_ANT        NUMBER(10),
  CD_TIPO_PORTADOR          NUMBER(5),
  CD_PORTADOR               NUMBER(10),
  NR_SEQ_CONTA_BANCO        NUMBER(10),
  NR_SEQ_EMPRESA            NUMBER(10),
  CD_PROFISSAO              NUMBER(10),
  NR_SEQ_VINCULO_EMPRESA    NUMBER(10),
  CD_MATRICULA              VARCHAR2(20 BYTE),
  DT_INICIO_VIGENCIA        DATE,
  DT_FIM_VIGENCIA           DATE,
  IE_MES_VENCIMENTO         VARCHAR2(1 BYTE),
  NR_SEQ_COMPL_PF_TEL_ADIC  NUMBER(10),
  NR_SEQ_DIA_VENCIMENTO     NUMBER(10),
  IE_TIPO_PAGADOR           VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_ITENS      NUMBER(10),
  NR_SEQ_TIPO_COMPL_ADIC    NUMBER(10),
  DS_EMAIL                  VARCHAR2(255 BYTE),
  IE_DIGITO_CONTA           VARCHAR2(1 BYTE),
  NR_CARTAO_CREDITO         VARCHAR2(16 BYTE),
  NR_SEQ_BANDEIRA           NUMBER(10),
  NM_TITULAR_CARTAO         VARCHAR2(255 BYTE),
  DS_TOKEN                  VARCHAR2(65 BYTE),
  QT_MESES_VENCIMENTO       NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRPG_BANCO_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_BANESTA_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_CONPAGA_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_COPFTEA_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_COMPL_PF_TEL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_COPFTEA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_PESFISI_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(CD_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPG_PESJURI_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(CD_CGC_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPRPG_PK ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_PLCONPAG_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_CONTRATO_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRPG_PLSCIPA_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_CLASSIF_ITENS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PLSCIPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_PLSDEEM_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PLSDEEM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_PLSPRAD_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PLSPRAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_PORTADO_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_PROFISS_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(CD_PROFISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_PROFISS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRPG_TIPCOAD_FK_I ON TASY.PLS_PROPOSTA_PAGADOR
(NR_SEQ_TIPO_COMPL_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRPG_TIPCOAD_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PROPOSTA_PAGADOR ADD (
  CONSTRAINT PLSPRPG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROPOSTA_PAGADOR ADD (
  CONSTRAINT PLSPRPG_PLSCIPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ITENS) 
 REFERENCES TASY.PLS_CLASSIF_ITENS_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_TIPCOAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMPL_ADIC) 
 REFERENCES TASY.TIPO_COMPLEMENTO_ADICIONAL (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT PLSPRPG_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_PLSDEEM_FK 
 FOREIGN KEY (NR_SEQ_EMPRESA) 
 REFERENCES TASY.PLS_DESC_EMPRESA (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_PROFISS_FK 
 FOREIGN KEY (CD_PROFISSAO) 
 REFERENCES TASY.PROFISSAO (CD_PROFISSAO),
  CONSTRAINT PLSPRPG_COPFTEA_FK 
 FOREIGN KEY (NR_SEQ_COMPL_PF_TEL_ADIC) 
 REFERENCES TASY.COMPL_PF_TEL_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSPRPG_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PAGADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSPRPG_PESFISI_FK 
 FOREIGN KEY (CD_PAGADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRPG_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PLSPRPG_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO));

GRANT SELECT ON TASY.PLS_PROPOSTA_PAGADOR TO NIVEL_1;

