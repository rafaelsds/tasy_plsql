ALTER TABLE TASY.ATEND_ESCALA_FOUR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_ESCALA_FOUR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_ESCALA_FOUR
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_HORA                    NUMBER(2),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  QT_PONTO                   NUMBER(2),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  CD_PERFIL_ATIVO            NUMBER(5),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_RESP_OCULAR             NUMBER(1),
  IE_RESP_MOTORA             NUMBER(1),
  IE_REFL_CEREBRAL           NUMBER(1),
  IE_RESPIRACAO              NUMBER(1)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATESCFOUR_ATEPACI_FK_I ON TASY.ATEND_ESCALA_FOUR
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESCFOUR_PERFIL_FK_I ON TASY.ATEND_ESCALA_FOUR
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESCFOUR_PESFISI_FK_I ON TASY.ATEND_ESCALA_FOUR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATESCFOUR_PK ON TASY.ATEND_ESCALA_FOUR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESCFOUR_SETATEN_FK_I ON TASY.ATEND_ESCALA_FOUR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESCFOUR_TASASDI_FK_I ON TASY.ATEND_ESCALA_FOUR
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATESCFOUR_TASASDI_FK2_I ON TASY.ATEND_ESCALA_FOUR
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atend_escala_four_atual
before insert or update ON TASY.ATEND_ESCALA_FOUR for each row
declare

qt_reg_w				number(1);
cd_setor_atendimento_w	number(10);

begin

if	(:new.nr_hora is null) or
	(:new.dt_avaliacao <> :old.dt_avaliacao) then
	begin
		:new.nr_hora	:= to_number(to_char(round(:new.dt_avaliacao,'hh24'),'hh24'));
	end;
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto final;
end if;

if	(:new.nr_atendimento is not null) and
	(:new.dt_liberacao is null) then

	begin
		cd_setor_atendimento_w := obter_setor_atendimento(:new.nr_atendimento);
	exception
	when others then
		cd_setor_atendimento_w	:= 0;
	end;

	if	(cd_setor_atendimento_w	> 0) then
		:new.cd_setor_atendimento	:= cd_setor_atendimento_w;
	end if;

end if;

:new.qt_ponto	:= 	:new.ie_resp_motora +
			:new.ie_resp_ocular +
			:new.ie_refl_cerebral +
			:new.ie_respiracao;
<<final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.atend_escala_four_delete
after delete ON TASY.ATEND_ESCALA_FOUR for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '223'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ATEND_ESCALA_FOUR ADD (
  CONSTRAINT ATESCFOUR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ATEND_ESCALA_FOUR ADD (
  CONSTRAINT ATESCFOUR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATESCFOUR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATESCFOUR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATESCFOUR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATESCFOUR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATESCFOUR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_ESCALA_FOUR TO NIVEL_1;

