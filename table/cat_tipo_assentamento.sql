ALTER TABLE TASY.CAT_TIPO_ASSENTAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAT_TIPO_ASSENTAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAT_TIPO_ASSENTAMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_TIPO_ASEN         NUMBER(2),
  NM_ASSENTAMENTO      VARCHAR2(21 BYTE),
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CATIASS_PK ON TASY.CAT_TIPO_ASSENTAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CAT_TIPO_ASSENTAMENTO ADD (
  CONSTRAINT CATIASS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CAT_TIPO_ASSENTAMENTO TO NIVEL_1;

