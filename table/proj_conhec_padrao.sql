ALTER TABLE TASY.PROJ_CONHEC_PADRAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_CONHEC_PADRAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_CONHEC_PADRAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_CONHECIMENTO      VARCHAR2(255 BYTE)       NOT NULL,
  PR_CONHECIMENTO      NUMBER(15,2)             NOT NULL,
  IE_IMPLANTAR         VARCHAR2(1 BYTE),
  IE_PARAMETRIZAR      VARCHAR2(1 BYTE),
  IE_TREINAR           VARCHAR2(1 BYTE),
  CD_EVENTO            VARCHAR2(10 BYTE),
  CD_EXP_CONHEC        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROJCONH_PK ON TASY.PROJ_CONHEC_PADRAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJCONH_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proj_conhec_padrao_update
before update ON TASY.PROJ_CONHEC_PADRAO for each row
declare

begin
if	(:old.pr_conhecimento <> :new.pr_conhecimento) then
	begin
	update	com_cons_gest_con_fun
	set		pr_conhecimento = :new.pr_conhecimento
	where	nr_seq_conhec_padrao is not null
	and		nr_seq_conhec_padrao = :new.nr_sequencia
	and		pr_conhecimento < :new.pr_conhecimento;
	end;
end if;

end;
/


ALTER TABLE TASY.PROJ_CONHEC_PADRAO ADD (
  CONSTRAINT PROJCONH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PROJ_CONHEC_PADRAO TO NIVEL_1;

