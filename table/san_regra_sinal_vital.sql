ALTER TABLE TASY.SAN_REGRA_SINAL_VITAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_REGRA_SINAL_VITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_REGRA_SINAL_VITAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SINAL_VITAL       VARCHAR2(3 BYTE)         NOT NULL,
  QT_MINIMO            NUMBER(15,4)             NOT NULL,
  QT_MAXIMO            NUMBER(15,4)             NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  QT_DIAS_INAPTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANRESV_PK ON TASY.SAN_REGRA_SINAL_VITAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANRESV_PK
  MONITORING USAGE;


ALTER TABLE TASY.SAN_REGRA_SINAL_VITAL ADD (
  CONSTRAINT SANRESV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SAN_REGRA_SINAL_VITAL TO NIVEL_1;

