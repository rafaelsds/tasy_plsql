ALTER TABLE TASY.W_INTERF_WINSAUDE_MED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTERF_WINSAUDE_MED CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTERF_WINSAUDE_MED
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIAL           DATE,
  DT_FINAL             DATE,
  CD_CBO               VARCHAR2(6 BYTE),
  CD_MEDICO            VARCHAR2(4 BYTE),
  CD_MUN_IBGE_MEDICO   VARCHAR2(6 BYTE),
  DS_CBO               VARCHAR2(60 BYTE),
  NM_MEDICO            VARCHAR2(70 BYTE),
  NR_CNS_MEDICO        VARCHAR2(15 BYTE),
  NR_CPF_MEDICO        VARCHAR2(11 BYTE),
  CD_MEDICO_EXECUTOR   VARCHAR2(15 BYTE),
  DT_NASC_MEDICO       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WINWSME_PK ON TASY.W_INTERF_WINSAUDE_MED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINWSME_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INTERF_WINSAUDE_MED ADD (
  CONSTRAINT WINWSME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INTERF_WINSAUDE_MED TO NIVEL_1;

