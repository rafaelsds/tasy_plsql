ALTER TABLE TASY.MPREV_POP_PES_ATEND_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_POP_PES_ATEND_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_POP_PES_ATEND_PROC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQ_POP_ALVO_PESSOA  NUMBER(10)            NOT NULL,
  NR_SEQ_POP_ALVO_BENEF   NUMBER(10),
  CD_PROCEDIMENTO         NUMBER(15)            NOT NULL,
  NR_SEQ_POP_ALVO_DATA    NUMBER(10),
  NR_SEQ_POP_ALVO_ATEND   NUMBER(10)            NOT NULL,
  IE_ORIGEM_PROCEDIMENTO  NUMBER(10),
  QT_PROCEDIMENTO         NUMBER(12,4),
  DS_PROCEDIMENTO         VARCHAR2(255 BYTE),
  NR_SEQ_SEGURADO         NUMBER(10),
  NR_SEQ_POPULACAO_ALVO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRPPAP_MPRPABE_FK_I ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQ_POP_ALVO_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPAP_MPRPADA_FK_I ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQ_POP_ALVO_DATA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPAP_MPRPAPE_FK_I ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQ_POP_ALVO_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPAP_MPRPOAL_FK_I ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQ_POPULACAO_ALVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPAP_MPRPPAT_FK_I ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQ_POP_ALVO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRPPAP_PK ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRPPAP_PK
  MONITORING USAGE;


CREATE INDEX TASY.MPRPPAP_PLSSEGU_FK_I ON TASY.MPREV_POP_PES_ATEND_PROC
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_POP_PES_ATEND_PROC ADD (
  CONSTRAINT MPRPPAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_POP_PES_ATEND_PROC ADD (
  CONSTRAINT MPRPPAP_MPRPABE_FK 
 FOREIGN KEY (NR_SEQ_POP_ALVO_BENEF) 
 REFERENCES TASY.MPREV_POP_ALVO_BENEF (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRPPAP_MPRPADA_FK 
 FOREIGN KEY (NR_SEQ_POP_ALVO_DATA) 
 REFERENCES TASY.MPREV_POP_ALVO_DATA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRPPAP_MPRPAPE_FK 
 FOREIGN KEY (NR_SEQ_POP_ALVO_PESSOA) 
 REFERENCES TASY.MPREV_POP_ALVO_PESSOA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRPPAP_MPRPPAT_FK 
 FOREIGN KEY (NR_SEQ_POP_ALVO_ATEND) 
 REFERENCES TASY.MPREV_POP_PES_ATEND (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRPPAP_MPRPOAL_FK 
 FOREIGN KEY (NR_SEQ_POPULACAO_ALVO) 
 REFERENCES TASY.MPREV_POPULACAO_ALVO (NR_SEQUENCIA),
  CONSTRAINT MPRPPAP_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_POP_PES_ATEND_PROC TO NIVEL_1;

