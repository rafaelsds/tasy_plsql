ALTER TABLE TASY.ESCALA_PHQ9
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PHQ9 CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PHQ9
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_INTERESSE           NUMBER(10),
  IE_DEPRIMIDO           NUMBER(10),
  IE_SONO                NUMBER(10),
  IE_DIFICULDADE         NUMBER(10),
  IE_CANSADO             NUMBER(10),
  IE_APETITE             NUMBER(10),
  IE_FRACASSO            NUMBER(10),
  IE_CONCENTRAR          NUMBER(10),
  IE_MOVIMENTAR          NUMBER(10),
  IE_FERIR               NUMBER(10),
  QT_PONTUACAO           NUMBER(3)              NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPHQ9_ATEPACI_FK_I ON TASY.ESCALA_PHQ9
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPHQ9_PESFISI_FK2_I ON TASY.ESCALA_PHQ9
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPHQ9_PK ON TASY.ESCALA_PHQ9
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_phq9_atual
before insert or update ON TASY.ESCALA_PHQ9 for each row
declare
qt_reg_w	number(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;

:new.qt_pontuacao := (nvl(:new.ie_interesse,0) +
	nvl(:new.ie_deprimido,0) +
	nvl(:new.ie_sono,0) +
	nvl(:new.ie_cansado,0) +
	nvl(:new.ie_apetite,0) +
	nvl(:new.ie_fracasso,0) +
	nvl(:new.ie_concentrar,0) +
	nvl(:new.ie_movimentar,0) +
	nvl(:new.ie_ferir,0));

<<Final>>
qt_reg_w := 0;

end;
/


ALTER TABLE TASY.ESCALA_PHQ9 ADD (
  CONSTRAINT ESCPHQ9_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PHQ9 ADD (
  CONSTRAINT ESCPHQ9_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPHQ9_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_PHQ9 TO NIVEL_1;

