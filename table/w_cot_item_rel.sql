ALTER TABLE TASY.W_COT_ITEM_REL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_COT_ITEM_REL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_COT_ITEM_REL
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4),
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_MATERIAL         NUMBER(6),
  NR_COT_COMPRA       NUMBER(10),
  CD_COMPRADOR        VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCOTIRE_COTCOMP_FK_I ON TASY.W_COT_ITEM_REL
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCOTIRE_COTCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WCOTIRE_ESTABEL_FK_I ON TASY.W_COT_ITEM_REL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCOTIRE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WCOTIRE_MATERIA_FK_I ON TASY.W_COT_ITEM_REL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCOTIRE_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WCOTIRE_PK ON TASY.W_COT_ITEM_REL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WCOTIRE_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_COT_ITEM_REL ADD (
  CONSTRAINT WCOTIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_COT_ITEM_REL ADD (
  CONSTRAINT WCOTIRE_COTCOMP_FK 
 FOREIGN KEY (NR_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA (NR_COT_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT WCOTIRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WCOTIRE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_COT_ITEM_REL TO NIVEL_1;

