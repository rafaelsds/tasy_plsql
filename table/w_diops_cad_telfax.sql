ALTER TABLE TASY.W_DIOPS_CAD_TELFAX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIOPS_CAD_TELFAX CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIOPS_CAD_TELFAX
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_SEQ_OPERADORA    NUMBER(10),
  IE_TIPO_TELEFONE    VARCHAR2(5 BYTE),
  NR_DDI_TELEFONE     VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE     VARCHAR2(3 BYTE),
  NR_TELEFONE         VARCHAR2(15 BYTE),
  NR_RAMAL_CONTATO    NUMBER(5),
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_PESSOA_FISICA    VARCHAR2(10 BYTE),
  CD_CGC              VARCHAR2(14 BYTE),
  NR_SEQ_PERIODO      NUMBER(10)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WDICATF_DIOPERI_FK_I ON TASY.W_DIOPS_CAD_TELFAX
(NR_SEQ_PERIODO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WDICATF_ESTABEL_FK_I ON TASY.W_DIOPS_CAD_TELFAX
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDICATF_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WDICATF_PESFISI_FK_I ON TASY.W_DIOPS_CAD_TELFAX
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WDICATF_PESJURI_FK_I ON TASY.W_DIOPS_CAD_TELFAX
(CD_CGC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDICATF_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WDICATF_PK ON TASY.W_DIOPS_CAD_TELFAX
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_DIOPS_CAD_TELFAX ADD (
  CONSTRAINT WDICATF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_DIOPS_CAD_TELFAX ADD (
  CONSTRAINT WDICATF_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT WDICATF_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT WDICATF_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WDICATF_DIOPERI_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.DIOPS_PERIODO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_DIOPS_CAD_TELFAX TO NIVEL_1;

