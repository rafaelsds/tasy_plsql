ALTER TABLE TASY.PLS_REQ_COPARTICIPACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REQ_COPARTICIPACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REQ_COPARTICIPACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_REQUISICAO      NUMBER(10),
  VL_COPARTICIPACAO      NUMBER(15,2),
  NR_SEQ_REQ_PROC        NUMBER(10),
  NR_SEQ_REQ_MAT         NUMBER(10),
  NR_SEQ_REGRA_PRECO     NUMBER(10),
  NR_SEQ_REGRA_COPARTIC  NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSREQC_PK ON TASY.PLS_REQ_COPARTICIPACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREQC_PLSREQU_FK_I ON TASY.PLS_REQ_COPARTICIPACAO
(NR_SEQ_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREQC_PLSRMAT_FK_I ON TASY.PLS_REQ_COPARTICIPACAO
(NR_SEQ_REQ_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREQC_PLSRPRO_FK_I ON TASY.PLS_REQ_COPARTICIPACAO
(NR_SEQ_REQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REQ_COPARTICIPACAO ADD (
  CONSTRAINT PLSREQC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REQ_COPARTICIPACAO ADD (
  CONSTRAINT PLSREQC_PLSREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.PLS_REQUISICAO (NR_SEQUENCIA),
  CONSTRAINT PLSREQC_PLSRMAT_FK 
 FOREIGN KEY (NR_SEQ_REQ_MAT) 
 REFERENCES TASY.PLS_REQUISICAO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSREQC_PLSRPRO_FK 
 FOREIGN KEY (NR_SEQ_REQ_PROC) 
 REFERENCES TASY.PLS_REQUISICAO_PROC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REQ_COPARTICIPACAO TO NIVEL_1;

