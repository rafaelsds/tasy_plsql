ALTER TABLE TASY.SAN_EXAME_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_EXAME_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_EXAME_VALOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_VALOR             VARCHAR2(40 BYTE)        NOT NULL,
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_BLOQ_SOROLOGIA    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANEXVA_PK ON TASY.SAN_EXAME_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANEXVA_SANEXAM_FK_I ON TASY.SAN_EXAME_VALOR
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_EXAME_VALOR ADD (
  CONSTRAINT SANEXVA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_EXAME_VALOR ADD (
  CONSTRAINT SANEXVA_SANEXAM_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_EXAME_VALOR TO NIVEL_1;

