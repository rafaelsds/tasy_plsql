ALTER TABLE TASY.W_IPASGO_DADOS_GERAIS_FAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_IPASGO_DADOS_GERAIS_FAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_IPASGO_DADOS_GERAIS_FAT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_LINHA              NUMBER(7),
  TP_REGISTRO           NUMBER(2),
  TP_FATURA             NUMBER(2),
  QT_GUIA               NUMBER(5),
  VL_APRESENTADO        NUMBER(15,4),
  DT_MESANO_REFERENCIA  DATE,
  DS_LINHA              VARCHAR2(2000 BYTE),
  NR_SEQ_TIPO_FATURA    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WIPDGFA_PK ON TASY.W_IPASGO_DADOS_GERAIS_FAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WIPDGFA_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_IPASGO_DADOS_GERAIS_FAT ADD (
  CONSTRAINT WIPDGFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_IPASGO_DADOS_GERAIS_FAT TO NIVEL_1;

