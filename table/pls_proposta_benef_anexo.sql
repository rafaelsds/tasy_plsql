ALTER TABLE TASY.PLS_PROPOSTA_BENEF_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROPOSTA_BENEF_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROPOSTA_BENEF_ANEXO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_BENEFICIARIO    NUMBER(10)             NOT NULL,
  DT_ANEXO               DATE                   NOT NULL,
  DS_ANEXO               VARCHAR2(255 BYTE)     NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_DOCUMENTO  NUMBER(10),
  IE_APRESENTA_PORTAL    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRBA_PK ON TASY.PLS_PROPOSTA_BENEF_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRBA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRBA_PLSPROB_FK_I ON TASY.PLS_PROPOSTA_BENEF_ANEXO
(NR_SEQ_BENEFICIARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRBA_PLSPROB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRBA_TIPODOC_FK_I ON TASY.PLS_PROPOSTA_BENEF_ANEXO
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PROPOSTA_BENEF_ANEXO ADD (
  CONSTRAINT PLSPRBA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROPOSTA_BENEF_ANEXO ADD (
  CONSTRAINT PLSPRBA_PLSPROB_FK 
 FOREIGN KEY (NR_SEQ_BENEFICIARIO) 
 REFERENCES TASY.PLS_PROPOSTA_BENEFICIARIO (NR_SEQUENCIA),
  CONSTRAINT PLSPRBA_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PROPOSTA_BENEF_ANEXO TO NIVEL_1;

