ALTER TABLE TASY.MAN_ORDEM_SERV_SLA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_SLA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_SLA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_ORDEM             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_SLA               NUMBER(10)           NOT NULL,
  IE_PRIORIDADE            VARCHAR2(10 BYTE)    NOT NULL,
  IE_CLASSIFICACAO         VARCHAR2(10 BYTE)    NOT NULL,
  QT_MIN_INICIO            NUMBER(15)           NOT NULL,
  QT_MIN_TERMINO           NUMBER(15)           NOT NULL,
  DT_INICIO_PREV           DATE                 NOT NULL,
  DT_TERMINO_PREV          DATE                 NOT NULL,
  DT_INICIO                DATE,
  DT_TERMINO               DATE,
  QT_DESVIO                NUMBER(15),
  NR_SEQ_STATUS            NUMBER(15)           NOT NULL,
  QT_MIN_REAL_INIC         NUMBER(15),
  QT_MIN_REAL_OS           NUMBER(15),
  IE_TEMPO                 VARCHAR2(15 BYTE)    NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(15)           NOT NULL,
  DT_ORDEM                 DATE                 NOT NULL,
  QT_DESVIO_INIC           NUMBER(15)           NOT NULL,
  NR_SEQ_ESTAGIO           NUMBER(10),
  NR_SEQ_EQUIPAMENTO       NUMBER(10),
  NR_SEQ_TIPO_EQUIPAMENTO  NUMBER(10),
  NR_SEQ_SLA_REGRA         NUMBER(10),
  IE_TIPO_SLA              NUMBER(3),
  IE_TIPO_SLA_TERMINO      NUMBER(3),
  IE_TEMPO_TERMINO         VARCHAR2(15 BYTE),
  DT_INICIO_SLA            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANORSS_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_SLA
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANORSS_PK ON TASY.MAN_ORDEM_SERV_SLA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_ordem_serv_sla_update
before update ON TASY.MAN_ORDEM_SERV_SLA for each row
declare

qt_min_w			Number(15,0);
dt_inicio_w			Date;
dt_estagio_w			date;
nr_seq_estagio_os_w		number(10,0);
nr_grupo_planej_w		number(10,0);
nr_seq_grupo_sup_w		number(10,0);
qt_existe_w			number(10,0);
nm_usuario_exec_w		varchar2(15);
ie_classificacao_w		varchar2(15);
--
owner_name_w			varchar2(100);
caller_name_w			varchar2(100);
line_number_w			number(10);
caller_type_w			varchar2(100);

/*
415 - Nao iniciada
413 - Acao imediata
412 - Concluida
411 - Iniciada
*/

BEGIN

if	(substr(dbms_utility.format_call_stack,1,2000) like '%MAN_ANALISAR_SLA%') then
	goto Final;
end if;

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
	goto Final;
end if;

if	(nvl(phi_is_base_philips(),'N') = 'S')  then
	goto Final;
end if;

:new.nr_seq_status	:= nvl(:new.nr_seq_status, 415);

if	(substr(dbms_utility.format_call_stack,1,2000) like '%MAN_ORDEM_SERVICO_AFTER%') then
	/*Verifica se foi encerrada e ainda nao esta com status concluida*/
	if	(:new.dt_termino is not null) and
		(:old.dt_termino is not null) and
		(:new.nr_seq_status <> 412) then
		:new.nr_seq_status	:= 412;

		/*select	Man_Obter_Min_SLA_OS(:new.nr_seq_ordem, :new.ie_tempo)
		into	:new.qt_min_real_os
		from	dual;

		if	(:new.dt_inicio > :new.dt_inicio_prev) or
			(:new.qt_min_real_OS > :new.qt_min_termino) then
			:new.qt_desvio	:= 1;
		else
			:new.qt_desvio	:= 0;
		end if;*/
	/*Se esta "nao iniciada" e iniciou*/
	elsif	(:new.nr_seq_status = 415) and
		(:old.dt_inicio is null) and
		(:new.dt_inicio is not null) then
		:new.nr_seq_status		:= 411;
	end if;
else
	/*Verifica se foi encerrada e ainda nao esta com status concluida*/
	if	(:new.dt_termino is not null) and
		(:old.dt_termino is not null) and
		(:new.nr_seq_status <> 412) then
		:new.nr_seq_status	:= 412;

		select	Man_Obter_Min_SLA_OS(:new.nr_seq_ordem, :new.ie_tempo)
		into	:new.qt_min_real_os
		from	dual;

		if	(:new.dt_inicio > :new.dt_inicio_prev) or
			(:new.qt_min_real_OS > :new.qt_min_termino) then
			:new.qt_desvio	:= 1;
		else
			:new.qt_desvio	:= 0;
		end if;
	/*Se esta "nao iniciada" e iniciou*/
	elsif	(:new.nr_seq_status = 415) and
		(:old.dt_inicio is null) and
		(:new.dt_inicio is not null) then
		:new.nr_seq_status		:= 411;
	end if;
end if;

/*Se esta iniciada, contar minutos reais da SLA*/
if	(:new.dt_inicio is not null) then
	if	(:new.ie_tempo = 'COR') then
		:new.qt_min_real_inic	:= (:new.dt_inicio - :new.dt_ordem) * 1440;

		if	(:new.nr_seq_estagio is not null) then
			:new.qt_min_real_inic	:= (:new.dt_inicio - dt_estagio_w) * 1440;
		end if;
	else
		select	Man_Obter_Min_com(:new.cd_estabelecimento, :new.dt_ordem, :new.dt_inicio)
		into	:new.qt_min_real_inic
		from	dual;

		if	(:new.nr_seq_estagio is not null) then
			select	Man_Obter_Min_com(:new.cd_estabelecimento, :new.dt_inicio, dt_estagio_w)
			into	:new.qt_min_real_inic
			from	dual;
		end if;
	end if;
end if;

/*Se esta como "nao iniciada", ve se esta perto do limite para inicio, se estiver, muda para "acao imediata"*/
if	(:new.nr_seq_status = 415) then
	qt_min_w	:= :new.qt_min_inicio / 3;
	dt_inicio_w	:= :new.dt_inicio_prev - (qt_min_w / 1440);
	if	(sysdate > dt_inicio_w) then
		:new.nr_seq_status	:= 413;
	end if;
end if;

/*Se esta como acao imediata, e foi iniciada, verifica se mantem "acao imediata" ou altera para "iniciada" - OS*/
if	(:new.nr_seq_status = 413) and
	(:new.dt_inicio is not null) and
	(:old.dt_inicio is null) then
	qt_min_w	:= :new.qt_min_termino / 3;
	if	(:new.qt_min_real_os > qt_min_w) then
		:new.nr_seq_status	:= 413;
	else
		:new.nr_seq_status	:= 411;
	end if;
end if;


if	(substr(dbms_utility.format_call_stack,1,2000) like '%MAN_ORDEM_SERVICO_AFTER%') then
	null;
else
	select	Man_Obter_Min_SLA_OS(:new.nr_seq_ordem, :new.ie_tempo)
	into	:new.qt_min_real_os
	from	dual;

	/*if	(:new.nr_seq_estagio is not null) then
		select	Man_Obter_Min_SLA_OS_estagio(:new.nr_seq_ordem, :new.ie_tempo, :new.nr_seq_estagio)
		into	:new.qt_min_real_os
		from	dual;
	end if;*/
end if;

/*Se esta como iniciada, e ja tem data de inicio, calcula os minutos, se estiver perto do limite de termino, altera para "acao imediata"*/
if	(:new.nr_seq_status = 411) and
	(:old.dt_inicio is not null) then
	qt_min_w	:= :new.qt_min_termino / 3;
	if	(:new.qt_min_real_os > qt_min_w) then
		:new.nr_seq_status	:= 413;
	end if;
end if;

if	(nvl(:new.qt_min_real_inic,0) > :new.qt_min_inicio) then
	:new.qt_desvio_inic		:= 1;
else
	:new.qt_desvio_inic		:= 0;
end if;

if	(nvl(:new.qt_min_real_inic,0) > :new.qt_min_inicio) or
	(nvl(:new.qt_min_real_os,0) > :new.qt_min_termino) then
	:new.qt_desvio		:= 1;
else
	:new.qt_desvio		:= 0;
end if;

<<Final>>
null;
END;
/


ALTER TABLE TASY.MAN_ORDEM_SERV_SLA ADD (
  CONSTRAINT MANORSS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_SLA ADD (
  CONSTRAINT MANORSS_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERV_SLA TO NIVEL_1;

