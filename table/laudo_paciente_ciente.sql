ALTER TABLE TASY.LAUDO_PACIENTE_CIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PACIENTE_CIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PACIENTE_CIENTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PRESCRICAO    NUMBER(7),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_PRESCRICAO        NUMBER(14),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LAUDO         NUMBER(10),
  IE_TIPO_ACAO         VARCHAR2(1 BYTE),
  NR_SEQ_EXT           NUMBER(10),
  NR_SEQ_RESULT_EXT    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAUCIENTE_LAUPACI_FK_I ON TASY.LAUDO_PACIENTE_CIENTE
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAUCIENTE_PK ON TASY.LAUDO_PACIENTE_CIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAUDO_PACIENTE_CIENTE ADD (
  CONSTRAINT LAUCIENTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PACIENTE_CIENTE ADD (
  CONSTRAINT LAUCIENTE_LAUPACI_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LAUDO_PACIENTE_CIENTE TO NIVEL_1;

