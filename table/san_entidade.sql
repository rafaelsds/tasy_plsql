ALTER TABLE TASY.SAN_ENTIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_ENTIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_ENTIDADE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_ENTIDADE           VARCHAR2(50 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_CGC                VARCHAR2(14 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_TIPO_ENTIDADE      VARCHAR2(1 BYTE)        NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  CD_EXTERNO            VARCHAR2(40 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_GRUPO          NUMBER(10),
  CD_LOCAL_ISBT         VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANENTI_PESJURI_FK_I ON TASY.SAN_ENTIDADE
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANENTI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SANENTI_PK ON TASY.SAN_ENTIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANENTI_SANGREN_FK_I ON TASY.SAN_ENTIDADE
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANENTI_SANGREN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANENTI_SETATEN_FK_I ON TASY.SAN_ENTIDADE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_ENTIDADE ADD (
  CONSTRAINT SANENTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_ENTIDADE ADD (
  CONSTRAINT SANENTI_SANGREN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SAN_GRUPO_ENTIDADE (NR_SEQUENCIA),
  CONSTRAINT SANENTI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SANENTI_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.SAN_ENTIDADE TO NIVEL_1;

