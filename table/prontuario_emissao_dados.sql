ALTER TABLE TASY.PRONTUARIO_EMISSAO_DADOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRONTUARIO_EMISSAO_DADOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRONTUARIO_EMISSAO_DADOS
(
  NR_SEQUENCIA     NUMBER(10)                   NOT NULL,
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  NR_ATENDIMENTO   NUMBER(10)                   NOT NULL,
  IE_ESCALA        VARCHAR2(10 BYTE),
  NR_SEQ_REGISTRO  NUMBER(10),
  CD_PROFISSIONAL  VARCHAR2(10 BYTE),
  DT_REGISTRO      DATE,
  QT_PONTUACAO     NUMBER(10),
  DS_PONTUACAO     VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREMD_ATEPACI_FK_I ON TASY.PRONTUARIO_EMISSAO_DADOS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMD_PESFISI_FK_I ON TASY.PRONTUARIO_EMISSAO_DADOS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PREMD_PK ON TASY.PRONTUARIO_EMISSAO_DADOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRONTUARIO_EMISSAO_DADOS ADD (
  CONSTRAINT PREMD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PRONTUARIO_EMISSAO_DADOS ADD (
  CONSTRAINT PREMD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PREMD_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRONTUARIO_EMISSAO_DADOS TO NIVEL_1;

