ALTER TABLE TASY.TX_DOADOR_POTENCIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_DOADOR_POTENCIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_DOADOR_POTENCIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DOADOR_ORGAO  NUMBER(10)               NOT NULL,
  NR_SEQ_RECEPTOR      NUMBER(10)               NOT NULL,
  DT_INSCRICAO         DATE                     NOT NULL,
  IE_DOADOR_EFETIVO    VARCHAR2(1 BYTE),
  NR_SEQ_REL_REC_DOA   NUMBER(10),
  DT_TRANSPLANTE       DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DOADOR        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXDOPO_PK ON TASY.TX_DOADOR_POTENCIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOPO_PK
  MONITORING USAGE;


CREATE INDEX TASY.TXDOPO_TXDOAD_FK_I ON TASY.TX_DOADOR_POTENCIAL
(NR_SEQ_DOADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOPO_TXDOAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXDOPO_TXDOORG_FK_I ON TASY.TX_DOADOR_POTENCIAL
(NR_SEQ_DOADOR_ORGAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOPO_TXDOORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXDOPO_TXRECE_FK_I ON TASY.TX_DOADOR_POTENCIAL
(NR_SEQ_RECEPTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TXDOPO_TXRREDO_FK_I ON TASY.TX_DOADOR_POTENCIAL
(NR_SEQ_REL_REC_DOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOPO_TXRREDO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TXDOPO_UK ON TASY.TX_DOADOR_POTENCIAL
(NR_SEQ_RECEPTOR, NR_SEQ_DOADOR_ORGAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXDOPO_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tx_doador_potencial_insert
before insert ON TASY.TX_DOADOR_POTENCIAL for each row
declare
ie_tipo_orgao_receptor_w	varchar2(15);
ie_tipo_orgao_doador_w	varchar2(15);

begin

/* Verifica compatibilidade entre �rg�os */
select	substr(tx_obter_dados_receptor(:new.nr_seq_receptor, 'TO'),1,15)
into	ie_tipo_orgao_receptor_w
from	dual;

select	tx_obter_dados_orgao(nr_seq_orgao, 'T')
into	ie_tipo_orgao_doador_w
from	tx_doador_orgao
where	nr_sequencia	= :new.nr_seq_doador_orgao;

if	(ie_tipo_orgao_receptor_w <> ie_tipo_orgao_doador_w) then
	--Os �rg�os do doador e receptor s�o incompat�veis.
	Wheb_mensagem_pck.exibir_mensagem_abort(264223);

end if;

end;
/


ALTER TABLE TASY.TX_DOADOR_POTENCIAL ADD (
  CONSTRAINT TXDOPO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TXDOPO_UK
 UNIQUE (NR_SEQ_RECEPTOR, NR_SEQ_DOADOR_ORGAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_DOADOR_POTENCIAL ADD (
  CONSTRAINT TXDOPO_TXDOORG_FK 
 FOREIGN KEY (NR_SEQ_DOADOR_ORGAO) 
 REFERENCES TASY.TX_DOADOR_ORGAO (NR_SEQUENCIA),
  CONSTRAINT TXDOPO_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEPTOR) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA),
  CONSTRAINT TXDOPO_TXRREDO_FK 
 FOREIGN KEY (NR_SEQ_REL_REC_DOA) 
 REFERENCES TASY.TX_RELACAO_RECEP_DOADOR (NR_SEQUENCIA),
  CONSTRAINT TXDOPO_TXDOAD_FK 
 FOREIGN KEY (NR_SEQ_DOADOR) 
 REFERENCES TASY.TX_DOADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_DOADOR_POTENCIAL TO NIVEL_1;

