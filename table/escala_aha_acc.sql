ALTER TABLE TASY.ESCALA_AHA_ACC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_AHA_ACC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_AHA_ACC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_CIRURGIA        VARCHAR2(15 BYTE),
  IE_RISCO                VARCHAR2(15 BYTE),
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  DT_AVALIACAO            DATE                  NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_LIBERACAO            DATE,
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  IE_FAT_RISCO            VARCHAR2(1 BYTE),
  DS_RESULTADO            VARCHAR2(255 BYTE),
  IE_CIRURGIA_EMERGENCIA  VARCHAR2(1 BYTE),
  IE_CONDICAO_CARDIACA    VARCHAR2(1 BYTE),
  IE_CIR_BAIXO_RISCO      VARCHAR2(1 BYTE),
  IE_CLASSE_FUNCIONAL     VARCHAR2(1 BYTE),
  IE_CARDIOMIOPATIA       VARCHAR2(1 BYTE),
  IE_ISQUEMICA            VARCHAR2(1 BYTE),
  IE_INSUF_CARDIACA       VARCHAR2(1 BYTE),
  IE_DIABETE              VARCHAR2(1 BYTE),
  IE_INSUF_RENAL          VARCHAR2(1 BYTE),
  IE_CEREBROVASCULAR      VARCHAR2(1 BYTE),
  IE_CIRUR_VASCULAR       VARCHAR2(1 BYTE),
  IE_RISCO_INTERMEDIARIO  VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO     NUMBER(10),
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESAHAAC_ATEPACI_FK_I ON TASY.ESCALA_AHA_ACC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESAHAAC_EHRREEL_FK_I ON TASY.ESCALA_AHA_ACC
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESAHAAC_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESAHAAC_PESFISI_FK_I ON TASY.ESCALA_AHA_ACC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESAHAAC_PK ON TASY.ESCALA_AHA_ACC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESAHAAC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_aha_acc_atual
before insert or update on  escala_aha_acc
for each row
declare
qt_reg_w		number(1);
qt_fator_risco_w	number(10) := 0;
ds_tipo_cirurgia_w	Varchar2(2);
ds_retorno_w		Varchar2(255);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.ie_risco	:=	null;

if	(:new.IE_CIRURGIA_EMERGENCIA is null) then
	:new.IE_CIRURGIA_EMERGENCIA := 'S';
elsif	(:new.IE_CONDICAO_CARDIACA  is null) and
	(:new.IE_CIRURGIA_EMERGENCIA = 'N')then
	:new.IE_CONDICAO_CARDIACA := 'S';
elsif	(:new.IE_CIR_BAIXO_RISCO  is null) and
	(:new.IE_CONDICAO_CARDIACA = 'N') then
	:new.IE_CIR_BAIXO_RISCO := 'S';
elsif	(:new.IE_CLASSE_FUNCIONAL  is null) and
	(:new.IE_CIR_BAIXO_RISCO = 'N') then
	:new.IE_CLASSE_FUNCIONAL := 'S';
elsif	(:new.IE_FAT_RISCO  is null) and
	(:new.IE_CLASSE_FUNCIONAL = 'N') then
	:new.IE_FAT_RISCO := 'N';
end if;


if	(:new.IE_CIRURGIA_EMERGENCIA = 'S') then
	ds_retorno_w := 'Encaminhar ao centro cir�rgico. Vigil�ncia perioperat�ria e estratifica��o de risco p�s-operat�rio e atua��o sobre fatores de risco.';
elsif	(:new.IE_CONDICAO_CARDIACA = 'S') then
	ds_retorno_w := 'Avaliar e tratar conforme diretrizes. Considerar encaminhamento ao centro cir�rgico.';
elsif	(:new.IE_CIR_BAIXO_RISCO = 'S') then
	ds_retorno_w := 'Prosseguir com a cirurgia planejada.';
elsif	(:new.IE_CLASSE_FUNCIONAL = 'S') then
	ds_retorno_w := 'Prosseguir com a cirurgia planejada.';
elsif	(:new.IE_FAT_RISCO = 'N') then
	ds_retorno_w := 'Prosseguir com a cirurgia planejada.';
else

	If	(:new.IE_CARDIOMIOPATIA = 'S') then
		qt_fator_risco_w := qt_fator_risco_w + 1;
	end if;
	If	(:new.IE_ISQUEMICA = 'S') then
		qt_fator_risco_w := qt_fator_risco_w + 1;
	end if;
	If	(:new.IE_INSUF_CARDIACA = 'S') then
		qt_fator_risco_w := qt_fator_risco_w + 1;
	end if;
	If	(:new.IE_DIABETE = 'S') then
		qt_fator_risco_w := qt_fator_risco_w + 1;
	end if;
	If	(:new.IE_INSUF_RENAL = 'S') then
		qt_fator_risco_w := qt_fator_risco_w + 1;
	end if;
	If	(:new.IE_CEREBROVASCULAR = 'S') then
		qt_fator_risco_w := qt_fator_risco_w + 1;
	end if;

	if	( qt_fator_risco_w = 0) then
		ds_retorno_w := 'Prosseguir com a cirurgia planejada.';

	else
		if	(:new.ie_tipo_cirurgia is not null) then
			if	(:new.ie_tipo_cirurgia < 15) then
				ds_tipo_cirurgia_w	:=	 'A';
			elsif	(:new.ie_tipo_cirurgia < 40) then
				ds_tipo_cirurgia_w	:=	 'I';
			else
				ds_tipo_cirurgia_w	:=	 'B';
			end if;
		end if;

		:new.ie_risco := ds_tipo_cirurgia_w;

		if	(ds_tipo_cirurgia_w = 'B') then
			ds_retorno_w := 'Prosseguir com a cirurgia planejada.';
		elsif	(ds_tipo_cirurgia_w = 'A') and
			( qt_fator_risco_w >= 3 ) then
			ds_retorno_w := 'Considerar avalia��o, se esta alterada modificar� a conduta.';
		else
			ds_retorno_w := 'Prosseguir com a cirurgia planejada com controle de frequ�ncia card�aca ou considerar realiza��o de estratifica��o n�o invasiva, se este alterado modificar� a conduta.';
		end if;


	end if;

end if;

:new.DS_RESULTADO := substr(ds_retorno_w,1,255);

<<Final>>
qt_reg_w	:= 0;

end escala_aha_acc_atual;
/


ALTER TABLE TASY.ESCALA_AHA_ACC ADD (
  CONSTRAINT ESAHAAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_AHA_ACC ADD (
  CONSTRAINT ESAHAAC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESAHAAC_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESAHAAC_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_AHA_ACC TO NIVEL_1;

