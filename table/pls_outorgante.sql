ALTER TABLE TASY.PLS_OUTORGANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OUTORGANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OUTORGANTE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_CGC_OUTORGANTE         VARCHAR2(14 BYTE)   NOT NULL,
  IE_TIPO_OUTORGANTE        VARCHAR2(2 BYTE),
  CD_ANS                    VARCHAR2(20 BYTE),
  NM_FANTASIA               VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  IE_NATUREZA_JURIDICA      VARCHAR2(6 BYTE),
  IE_SEGMENTACAO_OPERADORA  VARCHAR2(6 BYTE),
  CD_RESP_TECNICO           VARCHAR2(10 BYTE),
  IE_TIPO_RESP_TECNICO      VARCHAR2(1 BYTE),
  CD_REPRESENTANTE          VARCHAR2(10 BYTE),
  CD_REPRESENTANTE_RN117    VARCHAR2(10 BYTE),
  CD_CARGO_REP_RN117        NUMBER(6),
  NR_SEQ_RESP_CONTAB        NUMBER(10),
  NR_SEQ_ATUARIA            NUMBER(10),
  NR_SEQ_AUDITOR            NUMBER(10),
  DS_SENHA_ANS              VARCHAR2(20 BYTE),
  DT_FUNDACAO               DATE,
  DS_MASCARA_CODIGO         VARCHAR2(30 BYTE),
  CD_RESPONSAVEL_NOTIF      VARCHAR2(10 BYTE),
  IE_DESABILITA_IMP_XML     VARCHAR2(3 BYTE),
  IE_PRODUTO_ODONT          VARCHAR2(1 BYTE)    NOT NULL,
  NR_TELEFONE_ANS           VARCHAR2(15 BYTE),
  DS_EMAIL_ANS              VARCHAR2(255 BYTE),
  DS_SITE_ANS               VARCHAR2(255 BYTE),
  DS_LINK_NUCLEO_FISC       VARCHAR2(255 BYTE),
  DS_USUARIO_AUTENTICACAO   VARCHAR2(255 BYTE),
  DS_SENHA_AUTENTICACAO     VARCHAR2(255 BYTE),
  IE_AMBIENTE               VARCHAR2(255 BYTE),
  DS_NUMERO_CNES            VARCHAR2(255 BYTE),
  DS_USUARIO_CNES           VARCHAR2(255 BYTE),
  DS_IP_PROXY_CNS           VARCHAR2(255 BYTE),
  DS_PORTA_PROXY_CNS        VARCHAR2(255 BYTE),
  DS_USUARIO_PROXY_CNS      VARCHAR2(255 BYTE),
  DS_SENHA_PROXY_CNS        VARCHAR2(255 BYTE),
  NR_DDI_TELEFONE_ANS       VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOUTO_ESTABEL_FK_I ON TASY.PLS_OUTORGANTE
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_I1 ON TASY.PLS_OUTORGANTE
(CD_ANS, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_PESFISI_FK4_I ON TASY.PLS_OUTORGANTE
(CD_RESP_TECNICO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_PESFISI_FK5_I ON TASY.PLS_OUTORGANTE
(CD_REPRESENTANTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_PESFISI_FK6_I ON TASY.PLS_OUTORGANTE
(CD_REPRESENTANTE_RN117)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_PESFISI_FK7_I ON TASY.PLS_OUTORGANTE
(CD_RESPONSAVEL_NOTIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_PESJURI_FK_I ON TASY.PLS_OUTORGANTE
(CD_CGC_OUTORGANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOUTO_PK ON TASY.PLS_OUTORGANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOUTO_PLSACPF_FK_I ON TASY.PLS_OUTORGANTE
(NR_SEQ_ATUARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOUTO_PLSACPF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOUTO_PLSCARD_FK_I ON TASY.PLS_OUTORGANTE
(CD_CARGO_REP_RN117)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOUTO_PLSCARD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOUTO_PLSRESA_FK_I ON TASY.PLS_OUTORGANTE
(NR_SEQ_AUDITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOUTO_PLSRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOUTO_PLSRESC_FK_I ON TASY.PLS_OUTORGANTE
(NR_SEQ_RESP_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOUTO_PLSRESC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_outorgante_before
before insert or update ON TASY.PLS_OUTORGANTE for each row
begin

if	(:new.ds_senha_autenticacao <> :old.ds_senha_autenticacao) or
	((:old.ds_senha_autenticacao is null) and (:new.ds_senha_autenticacao is not null)) then
	:new.ds_senha_autenticacao := wheb_seguranca.encrypt(:new.ds_senha_autenticacao);
end if;

if	(:new.ds_senha_proxy_cns <> :old.ds_senha_proxy_cns) or
	((:old.ds_senha_proxy_cns is null) and (:new.ds_senha_proxy_cns is not null)) then
	:new.ds_senha_proxy_cns := wheb_seguranca.encrypt(:new.ds_senha_proxy_cns);
end if;

end pls_outorgante_before;
/


ALTER TABLE TASY.PLS_OUTORGANTE ADD (
  CONSTRAINT PLSOUTO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OUTORGANTE ADD (
  CONSTRAINT PLSOUTO_PESFISI_FK7 
 FOREIGN KEY (CD_RESPONSAVEL_NOTIF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSOUTO_PESJURI_FK 
 FOREIGN KEY (CD_CGC_OUTORGANTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSOUTO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSOUTO_PESFISI_FK4 
 FOREIGN KEY (CD_RESP_TECNICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSOUTO_PESFISI_FK5 
 FOREIGN KEY (CD_REPRESENTANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSOUTO_PLSCARD_FK 
 FOREIGN KEY (CD_CARGO_REP_RN117) 
 REFERENCES TASY.PLS_CARGO_DIOPS (CD_CARGO),
  CONSTRAINT PLSOUTO_PESFISI_FK6 
 FOREIGN KEY (CD_REPRESENTANTE_RN117) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSOUTO_PLSACPF_FK 
 FOREIGN KEY (NR_SEQ_ATUARIA) 
 REFERENCES TASY.PLS_RESPONSAVEL_ATUARIA (NR_SEQUENCIA),
  CONSTRAINT PLSOUTO_PLSRESA_FK 
 FOREIGN KEY (NR_SEQ_AUDITOR) 
 REFERENCES TASY.PLS_RESPONSAVEL_AUDITOR (NR_SEQUENCIA),
  CONSTRAINT PLSOUTO_PLSRESC_FK 
 FOREIGN KEY (NR_SEQ_RESP_CONTAB) 
 REFERENCES TASY.PLS_RESPONSAVEL_CONTAB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OUTORGANTE TO NIVEL_1;

