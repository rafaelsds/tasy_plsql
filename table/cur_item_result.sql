ALTER TABLE TASY.CUR_ITEM_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CUR_ITEM_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CUR_ITEM_RESULT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_RESULTADO         VARCHAR2(80 BYTE)        NOT NULL,
  NR_SEQ_APRES         NUMBER(3)                NOT NULL,
  QT_VALOR             NUMBER(15,4)             NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CURITRE_CURITEM_FK_I ON TASY.CUR_ITEM_RESULT
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CURITRE_CURITEM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CURITRE_PK ON TASY.CUR_ITEM_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CUR_ITEM_RESULT ADD (
  CONSTRAINT CURITRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CUR_ITEM_RESULT ADD (
  CONSTRAINT CURITRE_CURITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.CUR_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.CUR_ITEM_RESULT TO NIVEL_1;

