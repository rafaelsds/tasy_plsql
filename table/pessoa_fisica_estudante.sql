ALTER TABLE TASY.PESSOA_FISICA_ESTUDANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_ESTUDANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_ESTUDANTE
(
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO            DATE,
  DT_FIM               DATE,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEFIES_PESFISI_FK_I ON TASY.PESSOA_FISICA_ESTUDANTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PESSOA_FISICA_ESTUDANTE ADD (
  CONSTRAINT PEFIES_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA));

ALTER TABLE TASY.PESSOA_FISICA_ESTUDANTE ADD (
  CONSTRAINT PEFIES_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PESSOA_FISICA_ESTUDANTE TO NIVEL_1;

