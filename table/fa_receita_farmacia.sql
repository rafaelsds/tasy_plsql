ALTER TABLE TASY.FA_RECEITA_FARMACIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FA_RECEITA_FARMACIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FA_RECEITA_FARMACIA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_ATENDIMENTO               NUMBER(10),
  CD_MEDICO                    VARCHAR2(10 BYTE),
  NR_RECEITA                   VARCHAR2(15 BYTE),
  DT_RECEITA                   DATE             NOT NULL,
  NR_SERIE                     VARCHAR2(15 BYTE),
  DT_VALIDADE_RECEITA          DATE,
  NR_DIAS_RECEITA              NUMBER(5),
  DT_LIBERACAO                 DATE,
  DT_INICIO_RECEITA            DATE,
  NM_USUARIO_LIB               VARCHAR2(15 BYTE),
  DT_CANCELAMENTO              DATE,
  NM_USUARIO_CANC              VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  DT_PREVISTA_RETORNO          DATE,
  IE_RECEITA_EXTERNA           VARCHAR2(1 BYTE),
  IE_TIPO_RECEITA              VARCHAR2(15 BYTE),
  IE_RECEITA_MEDICO            VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  CD_PROFISSIONAL_RESP         VARCHAR2(10 BYTE),
  CD_EMPRESA                   NUMBER(10),
  DT_LIBERACAO_PARCIAL         DATE,
  NM_USUARIO_LIB_PARCIAL       VARCHAR2(15 BYTE),
  NR_SEQ_PACIENTE              NUMBER(10),
  NR_CICLO                     NUMBER(10),
  NR_SEQ_ASSINATURA            NUMBER(10),
  NR_SEQ_ASSINATURA_PARCIAL    NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO    NUMBER(10),
  NR_SEQ_ASSINAT_INAT_PARCIAL  NUMBER(10),
  NR_SEQ_MOTIVO_CANC           NUMBER(10),
  NR_SEQ_CLASSIF               NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  NR_SEQ_ATENDIMENTO           NUMBER(10),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE),
  DS_UTC                       VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO             VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO           VARCHAR2(50 BYTE),
  NR_SEQ_SOAP                  NUMBER(10),
  IE_TIPO_AVALIACAO            VARCHAR2(1 BYTE),
  IE_SOSTIGE                   VARCHAR2(1 BYTE),
  IE_TAXA                      VARCHAR2(1 BYTE),
  IE_ACIDENTE                  VARCHAR2(1 BYTE),
  IE_BVG                       VARCHAR2(1 BYTE),
  IE_VACCINE                   VARCHAR2(1 BYTE),
  IE_MATERIAL_PRESCR           VARCHAR2(1 BYTE),
  DS_ACIDENT                   VARCHAR2(255 BYTE),
  DS_ACIDENTE_NUMBER           VARCHAR2(255 BYTE),
  IE_NAO_SUBT_MARCA            VARCHAR2(1 BYTE),
  IE_PBS_RPBS                  VARCHAR2(1 BYTE),
  CD_CNPJ_ORIGEM               VARCHAR2(255 BYTE),
  DS_ESTABELECIMENTO_ORIGEM    VARCHAR2(255 BYTE),
  NR_SEQ_ORIGEM                NUMBER(10),
  NR_SEQ_REGULACAO             NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA       NUMBER(10),
  CD_CTG_PRESCR                VARCHAR2(6 BYTE) DEFAULT null,
  NR_PRESCR_SEQ_CTG            NUMBER(2)        DEFAULT null,
  IE_OUTPATIENT_TYPE           VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FAREFAR_ATCONSPEPA_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_ATEPACI_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_ATESOAP_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_EMPREFE_FK_I ON TASY.FA_RECEITA_FARMACIA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_ESTABEL_FK_I ON TASY.FA_RECEITA_FARMACIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_FACLARE_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_FACLARE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_FAMOTCE_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_FAMOTCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_PACATEN_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_PACSETO_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_PACSETO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_PESFISI_FK_I ON TASY.FA_RECEITA_FARMACIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_PESFISI_FK2_I ON TASY.FA_RECEITA_FARMACIA
(CD_PROFISSIONAL_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_PESFISI_FK3_I ON TASY.FA_RECEITA_FARMACIA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FAREFAR_PK ON TASY.FA_RECEITA_FARMACIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FAREFAR_TASASDI_FK_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_TASASDI_FK2_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FAREFAR_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.FAREFAR_TASASDI_FK3_I ON TASY.FA_RECEITA_FARMACIA
(NR_SEQ_ASSINATURA_PARCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.fa_receita_farmacia_pend_atual
after insert or update ON TASY.FA_RECEITA_FARMACIA for each row
declare

qt_reg_w				number(1);
ie_tipo_w				varchar2(10);
ie_liberacao_parcial_w	Varchar2(10);
ie_lib_receita_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

obter_param_usuario(10015,88,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_liberacao_parcial_w);

if	(nvl(ie_liberacao_parcial_w,'N') <> 'P') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'RECAMB';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XRECAMB';
	end if;
else
	if	(:new.dt_liberacao_parcial is null) then
		ie_tipo_w := 'RECAMB';
	elsif	(:old.dt_liberacao_parcial is null) and
			(:new.dt_liberacao_parcial is not null) then
		ie_tipo_w := 'XRECAMB';
	end if;
end if;

if	(:new.dt_cancelamento is not null) then
	ie_tipo_w := 'XRECAMB';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.fa_receita_farmacia_update
before update ON TASY.FA_RECEITA_FARMACIA for each row
declare

cd_pessoa_fisica_w	varchar2(10);

begin

	if (:new.nr_atendimento is not null) then

		select 	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		if (cd_pessoa_fisica_w <> :new.cd_pessoa_fisica) then
			-- O atendimento '||:new.nr_atendimento||' n�o pertence a seguinte pessoa: '||obter_nome_pf(:new.cd_pessoa_fisica)||'. Favor verificar#@#@'
			Wheb_mensagem_pck.exibir_mensagem_abort(263626,	'NR_ATENDIMENTO_W='||TO_CHAR(:NEW.NR_ATENDIMENTO)|| ';' ||
															'CD_PESSOA_FISICA_W= '|| obter_nome_pf(:NEW.CD_PESSOA_FISICA));
		end if;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.FA_RECEITA_FARMACIA_DELETE
after delete ON TASY.FA_RECEITA_FARMACIA for each row
declare

qt_reg_w	number(1);
ie_tipo_w	varchar2(10);

pragma autonomous_transaction;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	nr_seq_receita_amb = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w := 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.FA_RECEITA_FARMACIA_ATUAL
before insert or update ON TASY.FA_RECEITA_FARMACIA 
for each row
declare
qt_reg_w	number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_RECEITA,sysdate+10) <> :new.DT_RECEITA) and
	(:new.DT_RECEITA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_RECEITA, 'HV');	
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.DT_VALIDADE_RECEITA is not null) and
	(:new.DT_INICIO_RECEITA	is not null) and
    (ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.DT_VALIDADE_RECEITA)	< ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.DT_INICIO_RECEITA)) then
	wheb_mensagem_pck.exibir_mensagem_abort(32948);
end if;


<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.FA_RECEITA_FARMACIA ADD (
  CONSTRAINT FAREFAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FA_RECEITA_FARMACIA ADD (
  CONSTRAINT FAREFAR_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT FAREFAR_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT FAREFAR_TASASDI_FK3 
 FOREIGN KEY (NR_SEQ_ASSINATURA_PARCIAL) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT FAREFAR_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT FAREFAR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FAREFAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FAREFAR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FAREFAR_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FAREFAR_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FAREFAR_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT FAREFAR_PACSETO_FK 
 FOREIGN KEY (NR_SEQ_PACIENTE) 
 REFERENCES TASY.PACIENTE_SETOR (NR_SEQ_PACIENTE),
  CONSTRAINT FAREFAR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT FAREFAR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT FAREFAR_FAMOTCE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.FA_MOTIVO_CANC_ENTREGA (NR_SEQUENCIA),
  CONSTRAINT FAREFAR_FACLARE_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.FA_CLASSIF_RECEITA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FA_RECEITA_FARMACIA TO NIVEL_1;

