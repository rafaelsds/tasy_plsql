ALTER TABLE TASY.TITULO_RECEBER_NC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_NC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_NC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO_REC        NUMBER(10)               NOT NULL,
  NR_TITULO_PAGAR      NUMBER(10),
  VL_NOTA_CREDITO      NUMBER(15,2),
  NR_SEQ_NOTA_CREDITO  NUMBER(10)               NOT NULL,
  NR_SEQ_LIQ           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITRENC_NOTCRED_FK_I ON TASY.TITULO_RECEBER_NC
(NR_SEQ_NOTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRENC_NOTCRED_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITRENC_PK ON TASY.TITULO_RECEBER_NC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRENC_TIRELIQ_FK_I ON TASY.TITULO_RECEBER_NC
(NR_TITULO_REC, NR_SEQ_LIQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRENC_TITPAGA_FK_I ON TASY.TITULO_RECEBER_NC
(NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRENC_TITPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRENC_TITRECE_FK_I ON TASY.TITULO_RECEBER_NC
(NR_TITULO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRENC_TITRECE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITRENC_UK ON TASY.TITULO_RECEBER_NC
(NR_TITULO_REC, NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRENC_UK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITRENC_UK2 ON TASY.TITULO_RECEBER_NC
(NR_TITULO_REC, NR_SEQ_NOTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRENC_UK2
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_nc_insert
before insert or update ON TASY.TITULO_RECEBER_NC for each row
declare

cd_estabelecimento_w		ctb_documento.cd_estabelecimento%type;
dt_movimento_w          	ctb_documento.dt_competencia%type;
nr_seq_trans_financ_w   	ctb_documento.nr_seq_trans_financ%type;
nr_documento_w          	ctb_documento.nr_documento%type;
nr_seq_doc_compl_w      	ctb_documento.nr_seq_doc_compl%type;
ie_lib_caixa_w          	titulo_receber_liq.ie_lib_caixa%type;
ie_contab_tit_cancelado_w	parametro_contas_receber.ie_contab_tit_cancelado%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;
nr_multiplicador_w		number(1) := 1;

Cursor c01 is
	select 	a.cd_tipo_lote_contab,
		34 nr_seq_info_ctb,
		'TITULO_RECEBER_LIQ' nm_tabela,
		a.nm_atributo nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 5
	and	a.nm_atributo = 'VL_NOTA_CREDITO'
	union all
	select 	a.cd_tipo_lote_contab,
		14 nr_seq_info_ctb,
		'TITULO_RECEBER_NC' nm_tabela,
		a.nm_atributo nm_atributo
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 39
	and	a.nm_atributo = 'VL_NOTA_CREDITO'
	and	ie_concil_contab_w = 'S'
	and	exists ( select	1
			 from 	titulo_receber_liq y
			 where  y.nr_seq_lote_enc_contas is null
			 and    y.nr_seq_pls_lote_camara is null
			 and	nvl(y.ie_lib_caixa, 'S') = 'S'
			 and	y.nr_titulo 	= :new.nr_titulo_rec
			 and	y.nr_sequencia	= :new.nr_seq_liq
			 and	exists	(select	1
					from	titulo_receber x
					where	x.nr_titulo	= y.nr_titulo
					and	x.ie_pls	= 'S'
					and	x.ie_origem_titulo = '3'
					and	((x.ie_situacao	<> '3' or ie_contab_tit_cancelado_w = 'S')
					or 	(x.ie_situacao = '3' and y.vl_rec_maior <> 0))));

	vet_c01 c01%rowtype;
begin

begin
select  b.cd_estabelecimento,
        a.dt_recebimento,
        a.nr_seq_trans_fin,
        a.nr_titulo,
        a.nr_sequencia,
        a.ie_lib_caixa,
        decode(a.ie_acao, 'I', 1, -1)
into    cd_estabelecimento_w,
        dt_movimento_w,
        nr_seq_trans_financ_w,
        nr_documento_w,
        nr_seq_doc_compl_w,
        ie_lib_caixa_w,
	nr_multiplicador_w
from	titulo_receber_liq a,
        titulo_receber b
where	a.nr_titulo = b.nr_titulo
AND	a.nr_sequencia = :new.nr_seq_liq
and     a.nr_titulo = :new.nr_titulo_rec;
exception when others then
	nr_seq_trans_financ_w	:= null;
end;

begin
select 	nvl(max(ie_contab_tit_cancelado), 'S')
into    ie_contab_tit_cancelado_w
from    parametro_contas_receber
where   cd_estabelecimento      = cd_estabelecimento_w;
exception
when others then
	ie_contab_tit_cancelado_w := 'S';
end;

begin
select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_w;
exception when others then
	ie_concil_contab_w := 'N';
end;

if      (nvl(:new.vl_nota_credito, 0) <> 0 and nvl(nr_seq_trans_financ_w,0) <> 0 and nvl(ie_lib_caixa_w, 'S') = 'S') then
        begin

	open c01;
	loop
	fetch c01 into
		vet_c01;
	exit when c01%notfound;
	begin

        ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                dt_movimento_w,
                                                                vet_c01.cd_tipo_lote_contab,
                                                                nr_seq_trans_financ_w,
                                                                vet_c01.nr_seq_info_ctb,
                                                                nr_documento_w,
                                                                nr_seq_doc_compl_w,
                                                                :new.nr_sequencia,
                                                                :new.vl_nota_credito * nr_multiplicador_w,
                                                                vet_c01.nm_tabela,
                                                                vet_c01.nm_atributo,
                                                                :new.nm_usuario);

	end;
	end loop;
	close c01;
        end;
end if;

if (updating) then
	if (:new.vl_nota_credito <> :old.vl_nota_credito)   then
		/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),dt_movimento_w);
	end if;
else 	if (inserting) then
		/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),dt_movimento_w);
	end if;
end if;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_NC ADD (
  CONSTRAINT TITRENC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TITRENC_UK2
 UNIQUE (NR_TITULO_REC, NR_SEQ_NOTA_CREDITO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT TITRENC_UK
 UNIQUE (NR_TITULO_REC, NR_TITULO_PAGAR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_NC ADD (
  CONSTRAINT TITRENC_NOTCRED_FK 
 FOREIGN KEY (NR_SEQ_NOTA_CREDITO) 
 REFERENCES TASY.NOTA_CREDITO (NR_SEQUENCIA),
  CONSTRAINT TITRENC_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO_REC, NR_SEQ_LIQ) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA),
  CONSTRAINT TITRENC_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_REC) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TITRENC_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.TITULO_RECEBER_NC TO NIVEL_1;

