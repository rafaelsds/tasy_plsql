ALTER TABLE TASY.ROP_PROCESSOS_LAVAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_PROCESSOS_LAVAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_PROCESSOS_LAVAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_PROCESSO          VARCHAR2(255 BYTE)       NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  QT_M3                NUMBER(15,4),
  NR_SEQ_MAQUINA       NUMBER(10),
  DS_TEMPERATURA       VARCHAR2(255 BYTE),
  DS_NIVEL_AGUA        VARCHAR2(255 BYTE),
  QT_TEMPO             NUMBER(15,4),
  NR_SEQ_OPERACAO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPPRLA_ESTABEL_FK_I ON TASY.ROP_PROCESSOS_LAVAGEM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ROPPRLA_PK ON TASY.ROP_PROCESSOS_LAVAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPPRLA_PK
  MONITORING USAGE;


CREATE INDEX TASY.ROPPRLA_ROPMALA_FK_I ON TASY.ROP_PROCESSOS_LAVAGEM
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPPRLA_ROPMALA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPPRLA_ROPOPPR_FK_I ON TASY.ROP_PROCESSOS_LAVAGEM
(NR_SEQ_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPPRLA_ROPOPPR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_PROCESSOS_LAVAGEM_tp  after update ON TASY.ROP_PROCESSOS_LAVAGEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_M3,1,4000),substr(:new.QT_M3,1,4000),:new.nm_usuario,nr_seq_w,'QT_M3',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROCESSO,1,4000),substr(:new.DS_PROCESSO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROCESSO',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_NIVEL_AGUA,1,4000),substr(:new.DS_NIVEL_AGUA,1,4000),:new.nm_usuario,nr_seq_w,'DS_NIVEL_AGUA',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPERACAO,1,4000),substr(:new.NR_SEQ_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPERACAO',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TEMPO,1,4000),substr(:new.QT_TEMPO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TEMPO',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TEMPERATURA,1,4000),substr(:new.DS_TEMPERATURA,1,4000),:new.nm_usuario,nr_seq_w,'DS_TEMPERATURA',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MAQUINA,1,4000),substr(:new.NR_SEQ_MAQUINA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MAQUINA',ie_log_w,ds_w,'ROP_PROCESSOS_LAVAGEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_PROCESSOS_LAVAGEM ADD (
  CONSTRAINT ROPPRLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_PROCESSOS_LAVAGEM ADD (
  CONSTRAINT ROPPRLA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPPRLA_ROPMALA_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.ROP_MAQUINA_LAVAGEM (NR_SEQUENCIA),
  CONSTRAINT ROPPRLA_ROPOPPR_FK 
 FOREIGN KEY (NR_SEQ_OPERACAO) 
 REFERENCES TASY.ROP_OPERACAO_PROCESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_PROCESSOS_LAVAGEM TO NIVEL_1;

