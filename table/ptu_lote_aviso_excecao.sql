ALTER TABLE TASY.PTU_LOTE_AVISO_EXCECAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_LOTE_AVISO_EXCECAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_LOTE_AVISO_EXCECAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  NR_SEQ_PROTOCOLO     NUMBER(10),
  NR_SEQ_CONTA         NUMBER(10),
  CD_GUIA_REFERENCIA   VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTULAEX_PK ON TASY.PTU_LOTE_AVISO_EXCECAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTULAEX_PLSCOME_FK_I ON TASY.PTU_LOTE_AVISO_EXCECAO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTULAEX_PLSPRCO_FK_I ON TASY.PTU_LOTE_AVISO_EXCECAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTULAEX_PTULTAV_FK_I ON TASY.PTU_LOTE_AVISO_EXCECAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_LOTE_AVISO_EXCECAO ADD (
  CONSTRAINT PTULAEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_LOTE_AVISO_EXCECAO ADD (
  CONSTRAINT PTULAEX_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PTULAEX_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PTULAEX_PTULTAV_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PTU_LOTE_AVISO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_LOTE_AVISO_EXCECAO TO NIVEL_1;

