ALTER TABLE TASY.PLANO_VERSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLANO_VERSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLANO_VERSAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NM_MEDICO              VARCHAR2(60 BYTE)      NOT NULL,
  DT_EMISSAO             DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_VERSAO              VARCHAR2(15 BYTE),
  DT_LIBERACAO           DATE,
  NM_USUARIO_LIBERACAO   VARCHAR2(15 BYTE),
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  NM_USUARIO_LIB         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DS_BAR_CODE            CLOB,
  CD_MEDICO              VARCHAR2(10 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_AMAMENTACAO         VARCHAR2(1 BYTE),
  DS_OUTRAS_CONDICOES    VARCHAR2(255 BYTE),
  QT_CREATININA          NUMBER(10,4),
  IE_IMPORTADO           VARCHAR2(1 BYTE),
  DS_ALERGIAS            VARCHAR2(255 BYTE),
  QT_ALTURA              NUMBER(10,4),
  QT_PESO                NUMBER(10,4),
  IE_GRAVIDA             VARCHAR2(1 BYTE),
  DS_VERSAO_XML          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_BAR_CODE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLANVER_ATEPACI_FK_I ON TASY.PLANO_VERSAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLANVER_PESFISI_FK_I ON TASY.PLANO_VERSAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLANVER_PK ON TASY.PLANO_VERSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLANVER_PROFISS_FK_I ON TASY.PLANO_VERSAO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLANO_VERSAO_tp  after update ON TASY.PLANO_VERSAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_MEDICO,1,500);gravar_log_alteracao(substr(:old.NM_MEDICO,1,4000),substr(:new.NM_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'NM_MEDICO',ie_log_w,ds_w,'PLANO_VERSAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PLANO_VERSAO_UPDATE
before update ON TASY.PLANO_VERSAO for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into is_rule_exists_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'MP'
							and		x.ie_situacao = 'A');


    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
		nvl(b.nr_sequencia, 0)
    into qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'MP'
							and		x.ie_situacao = 'A');


		wl_gerar_finalizar_tarefa('MP','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
	end if;
  end if;
end;
/


ALTER TABLE TASY.PLANO_VERSAO ADD (
  CONSTRAINT PLANVER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLANO_VERSAO ADD (
  CONSTRAINT PLANVER_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PLANVER_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLANVER_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PLANO_VERSAO TO NIVEL_1;

