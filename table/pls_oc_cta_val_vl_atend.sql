ALTER TABLE TASY.PLS_OC_CTA_VAL_VL_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_VL_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_VL_ATEND
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_INCIDENCIA                VARCHAR2(2 BYTE),
  IE_VALOR_BASE                VARCHAR2(2 BYTE),
  IE_CONSIDERA_TX_INTERCAMBIO  VARCHAR2(1 BYTE),
  VL_BASE                      NUMBER(15,2),
  NR_SEQ_OC_CTA_COMB           NUMBER(10)       NOT NULL,
  CD_PROCEDIMENTO              NUMBER(15),
  IE_ORIGEM_PROCED             NUMBER(10),
  NR_SEQ_MATERIAL              NUMBER(10),
  IE_CONSIDERA_REFERENCIA      VARCHAR2(2 BYTE),
  IE_REL_NAO_PREENCHIDO        VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSVVAT_PK ON TASY.PLS_OC_CTA_VAL_VL_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVVAT_PLSMAT_FK_I ON TASY.PLS_OC_CTA_VAL_VL_ATEND
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVVAT_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_VL_ATEND
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSVVAT_PROCEDI_FK_I ON TASY.PLS_OC_CTA_VAL_VL_ATEND
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_VL_ATEND_tp  after update ON TASY.PLS_OC_CTA_VAL_VL_ATEND FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INCIDENCIA,1,4000),substr(:new.IE_INCIDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_INCIDENCIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALOR_BASE,1,4000),substr(:new.IE_VALOR_BASE,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_BASE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERA_TX_INTERCAMBIO,1,4000),substr(:new.IE_CONSIDERA_TX_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERA_TX_INTERCAMBIO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERA_REFERENCIA,1,4000),substr(:new.IE_CONSIDERA_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERA_REFERENCIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_BASE,1,4000),substr(:new.VL_BASE,1,4000),:new.nm_usuario,nr_seq_w,'VL_BASE',ie_log_w,ds_w,'PLS_OC_CTA_VAL_VL_ATEND',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_VL_ATEND ADD (
  CONSTRAINT PLSVVAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_VL_ATEND ADD (
  CONSTRAINT PLSVVAT_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSVVAT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSVVAT_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_VL_ATEND TO NIVEL_1;

