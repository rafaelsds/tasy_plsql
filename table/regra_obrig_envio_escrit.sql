ALTER TABLE TASY.REGRA_OBRIG_ENVIO_ESCRIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_OBRIG_ENVIO_ESCRIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_OBRIG_ENVIO_ESCRIT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_TIPO_REMESSA       VARCHAR2(1 BYTE)        NOT NULL,
  IE_OBRIGATORIO        VARCHAR2(1 BYTE)        NOT NULL,
  DT_INICIO_VIGENCIA    DATE                    NOT NULL,
  DT_FIM_VIGENCIA       DATE,
  NM_TABELA             VARCHAR2(50 BYTE)       NOT NULL,
  NM_ATRIBUTO           VARCHAR2(50 BYTE)       NOT NULL,
  CD_BANCO              NUMBER(3),
  NR_SEQ_CONTA_BANCO    NUMBER(10),
  NR_SEQ_CARTEIRA_COBR  NUMBER(10),
  DS_INCONSISTENCIA     VARCHAR2(4000 BYTE)     NOT NULL,
  IE_MENOR_IDADE        VARCHAR2(1 BYTE)        NOT NULL,
  QT_MAXIMA_CARACTER    NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REOBRENVES_BANCART_FK_I ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(NR_SEQ_CARTEIRA_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REOBRENVES_BANCART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REOBRENVES_BANCO_FK_I ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REOBRENVES_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REOBRENVES_BANESTA_FK_I ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REOBRENVES_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REOBRENVES_ESTABEL_FK_I ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REOBRENVES_PK ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REOBRENVES_PK
  MONITORING USAGE;


CREATE INDEX TASY.REOBRENVES_TABATRI_FK_I ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(NM_TABELA, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REOBRENVES_TABSIST_FK_I ON TASY.REGRA_OBRIG_ENVIO_ESCRIT
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REOBRENVES_TABSIST_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_OBRIG_ENVIO_ESCRIT ADD (
  CONSTRAINT REOBRENVES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_OBRIG_ENVIO_ESCRIT ADD (
  CONSTRAINT REOBRENVES_TABATRI_FK 
 FOREIGN KEY (NM_TABELA, NM_ATRIBUTO) 
 REFERENCES TASY.TABELA_ATRIBUTO (NM_TABELA,NM_ATRIBUTO),
  CONSTRAINT REOBRENVES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REOBRENVES_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA),
  CONSTRAINT REOBRENVES_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT REOBRENVES_BANCART_FK 
 FOREIGN KEY (NR_SEQ_CARTEIRA_COBR) 
 REFERENCES TASY.BANCO_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT REOBRENVES_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO));

GRANT SELECT ON TASY.REGRA_OBRIG_ENVIO_ESCRIT TO NIVEL_1;

