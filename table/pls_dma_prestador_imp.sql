ALTER TABLE TASY.PLS_DMA_PRESTADOR_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_DMA_PRESTADOR_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_DMA_PRESTADOR_IMP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_DMA_CABECALHO  NUMBER(10)              NOT NULL,
  CD_PRESTADOR          VARCHAR2(14 BYTE),
  CD_CPF_PREST          VARCHAR2(11 BYTE),
  CD_CNPJ_PREST         VARCHAR2(14 BYTE),
  NM_PRESTADOR          VARCHAR2(70 BYTE),
  CD_CNES_PREST         VARCHAR2(7 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSDAPT_PK ON TASY.PLS_DMA_PRESTADOR_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSDAPT_PLSDMAC_FK_I ON TASY.PLS_DMA_PRESTADOR_IMP
(NR_SEQ_DMA_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_DMA_PRESTADOR_IMP ADD (
  CONSTRAINT PLSDAPT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_DMA_PRESTADOR_IMP ADD (
  CONSTRAINT PLSDAPT_PLSDMAC_FK 
 FOREIGN KEY (NR_SEQ_DMA_CABECALHO) 
 REFERENCES TASY.PLS_DMA_CABECALHO_IMP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_DMA_PRESTADOR_IMP TO NIVEL_1;

