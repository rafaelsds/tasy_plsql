ALTER TABLE TASY.PE_PRESCR_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_PRESCR_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_PRESCR_PROC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  NR_SEQ_PROC            NUMBER(10)             NOT NULL,
  NR_SEQ_PRESCR          NUMBER(10)             NOT NULL,
  QT_PONTUACAO           NUMBER(10)             NOT NULL,
  DS_ORIGEM              VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_APRES           NUMBER(10)             NOT NULL,
  CD_INTERVALO           VARCHAR2(7 BYTE),
  DS_HORARIOS            VARCHAR2(2000 BYTE),
  QT_INTERVENCAO         NUMBER(5),
  HR_PRIM_HORARIO        VARCHAR2(5 BYTE),
  IE_SE_NECESSARIO       VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_SUSPENSO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_ADEP                VARCHAR2(1 BYTE),
  NR_SEQ_TOPOGRAFIA      NUMBER(10),
  DT_SUSPENSAO           DATE,
  NM_USUARIO_SUSP        VARCHAR2(15 BYTE),
  IE_LADO                VARCHAR2(1 BYTE),
  NR_OCORRENCIA          NUMBER(15,4),
  IE_PERMITE_EXCLUSAO    VARCHAR2(1 BYTE),
  IE_AGORA               VARCHAR2(1 BYTE),
  NR_SEQ_PROT_PROC       NUMBER(10),
  HR_HORARIO_ESPEC       VARCHAR2(5 BYTE),
  IE_INTERV_ESPEC_AGORA  VARCHAR2(1 BYTE),
  NR_SEQ_DIAG            NUMBER(10),
  NR_SEQ_ITEM            NUMBER(10),
  NR_SEQ_RESULT          NUMBER(10),
  IE_ACM                 VARCHAR2(1 BYTE),
  CD_RESPONSAVEL         VARCHAR2(10 BYTE),
  IE_FAOSE               VARCHAR2(15 BYTE),
  IE_PROFISSIONAL        VARCHAR2(15 BYTE),
  IE_AUXILIAR            VARCHAR2(15 BYTE),
  IE_ENCAMINHAR          VARCHAR2(15 BYTE),
  IE_FAZER               VARCHAR2(15 BYTE),
  IE_ORIENTAR            VARCHAR2(15 BYTE),
  IE_SUPERVISIONAR       VARCHAR2(15 BYTE),
  IE_CONFIRMADA          VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_SEQ_CPOE_INTERV     NUMBER(10),
  DT_FIM                 DATE,
  DT_INICIO              DATE,
  DT_PRIMEIRO_HORARIO    DATE,
  DT_HORARIO_ESPEC       DATE,
  NR_SEQ_PAT_CP_INTERV   NUMBER(10),
  NR_SEQ_PAT_CP_PROBLEM  NUMBER(10),
  NR_SEQ_REL_DIAG        NUMBER(10),
  IE_RELATED_PROBLEM     NUMBER(10),
  IE_FREE_ITEM           VARCHAR2(1 BYTE),
  DS_DISPLAY_NAME        VARCHAR2(255 BYTE),
  IE_CLASSIF_OTE         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPREPR_INTPRES_FK_I ON TASY.PE_PRESCR_PROC
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPREPR_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPREPR_PATCPIN_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_PAT_CP_INTERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_PATCPPR_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_PAT_CP_PROBLEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_PEDIAGN_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPREPR_PEDIAGN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPREPR_PEITEEX_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPREPR_PEITEEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPREPR_PEITERE_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPREPR_PEITERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPREPR_PEPPROC_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_PROT_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_PEPREDI_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_REL_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_PEPRESC_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_PEPROCE_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_PESFISI_FK_I ON TASY.PE_PRESCR_PROC
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPREPR_PK ON TASY.PE_PRESCR_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPREPR_TOPDOR_FK_I ON TASY.PE_PRESCR_PROC
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPREPR_TOPDOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPREPR_1 ON TASY.PE_PRESCR_PROC
(NR_SEQUENCIA, NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pe_prescr_proc_update
before update ON TASY.PE_PRESCR_PROC for each row
declare
qt_adm_w		number(10,0);

begin
/* --> Rafael em 03/09/2007 x ADEP x cfe definido com Marcus <-- */
if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') then
	update	pe_prescr_proc_hor
	set	dt_suspensao = sysdate,
		nm_usuario_susp = :new.nm_usuario_susp
	where	nr_seq_pe_prescr = :new.nr_seq_prescr
	and	nr_seq_pe_proc = :new.nr_sequencia
	and	dt_fim_horario is null
	and	dt_suspensao is null;
end if;

if	(nvl(:new.nr_ocorrencia,0) = 0) and
	(:new.cd_intervalo is not null) then
	:new.nr_ocorrencia	:= Obter_ocorrencia_intervalo(:new.cd_intervalo, 24,'O');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pe_prescr_proc_horario
before insert or update ON TASY.PE_PRESCR_PROC for each row
declare
qt_reg_w	number(10);

begin

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (:new.HR_PRIM_HORARIO is null) then

	:new.dt_primeiro_horario := null;


elsif ((:new.HR_PRIM_HORARIO <> :old.HR_PRIM_HORARIO) or
	((:new.HR_PRIM_HORARIO is not null) and (:old.HR_PRIM_HORARIO is null))) then

	:new.dt_primeiro_horario :=  to_date(to_char(sysdate,'dd/mm/yyyy') ||':'|| :new.HR_PRIM_HORARIO,'dd/mm/yyyy hh24:mi');

end if;


if (:new.HR_HORARIO_ESPEC is null) then

		:new.dt_horario_espec := null;

elsif ((:new.HR_HORARIO_ESPEC <> :old.HR_HORARIO_ESPEC) or
	((:new.HR_HORARIO_ESPEC is not null) and (:old.HR_HORARIO_ESPEC is null))) then

		:new.dt_horario_espec :=  to_date(to_char(sysdate,'dd/mm/yyyy') ||':'|| :new.HR_HORARIO_ESPEC,'dd/mm/yyyy hh24:mi');

end if;


exception
	when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.PE_PRESCR_PROC_tp  after update ON TASY.PE_PRESCR_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PROFISSIONAL,1,4000),substr(:new.IE_PROFISSIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROFISSIONAL',ie_log_w,ds_w,'PE_PRESCR_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Smart_ITEM_SAE_aftinsert
after insert or update ON TASY.PE_PRESCR_PROC for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;

begin
reg_integracao_w.cd_estab_documento := OBTER_ESTABELECIMENTO_ATIVO;

	if  (:old.DT_SUSPENSAO is null) and
		(:new.DT_SUSPENSAO is not null) then
		BEGIN
				gerar_int_padrao.gravar_integracao('389', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w, 'DS_OPERACAO_P=SUSPENCAO');
		END;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pe_prescr_proc_atual
before insert or update ON TASY.PE_PRESCR_PROC 
for each row
declare
ie_situacao_w	 varchar2(2);
qt_reg_w		number(10);
begin


if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.ie_faose is null) then
	begin
	select	max(ie_faose)
	into	:new.ie_faose
	from	pe_procedimento
	where	nr_sequencia = :new.NR_SEQ_PROC;
	exception
		when others then
		null;
	end;
end if;	



if	(:new.ie_profissional is null) then
	begin
	select	max(ie_profissional)
	into	:new.ie_profissional
	from	pe_procedimento
	where	nr_sequencia = :new.NR_SEQ_PROC;
	exception
		when others then
		null;
	end;
end if;	


select max(ie_situacao)
into ie_situacao_w
from pe_procedimento
where nr_sequencia = :new.nr_seq_proc;
if (ie_situacao_w = 'I') then
	Wheb_mensagem_pck.exibir_mensagem_abort(349391);
end if;

begin
if (:new.hr_horario_espec is not null) and ((:new.hr_horario_espec <> :old.hr_horario_espec) or (:old.dt_fim is null)) then
		:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_horario_espec,'dd/mm/yyyy hh24:mi');
end if;	
if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
end if;

if	(INSERTING and nvl(:new.ds_horarios,'XPTO') = 'XPTO' and nvl(:new.ie_se_necessario,'N') = 'S') then
	:new.ds_horarios	:= 'SN';
end if;

exception
	when others then
	null;
end;


<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.PE_PRESCR_PROC ADD (
  CONSTRAINT PEPREPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_PRESCR_PROC ADD (
  CONSTRAINT PEPREPR_PATCPIN_FK 
 FOREIGN KEY (NR_SEQ_PAT_CP_INTERV) 
 REFERENCES TASY.PATIENT_CP_INTERVENTION (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPREPR_PATCPPR_FK 
 FOREIGN KEY (NR_SEQ_PAT_CP_PROBLEM) 
 REFERENCES TASY.PATIENT_CP_PROBLEM (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_PEPREDI_FK 
 FOREIGN KEY (NR_SEQ_REL_DIAG) 
 REFERENCES TASY.PE_PRESCR_DIAG (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PEPREPR_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PRESCR) 
 REFERENCES TASY.PE_PRESCRICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPREPR_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_PEPPROC_FK 
 FOREIGN KEY (NR_SEQ_PROT_PROC) 
 REFERENCES TASY.PE_PROTOCOLO_PROC (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_PEDIAGN_FK 
 FOREIGN KEY (NR_SEQ_DIAG) 
 REFERENCES TASY.PE_DIAGNOSTICO (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_PEITEEX_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.PE_ITEM_EXAMINAR (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_PEITERE_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.PE_ITEM_RESULTADO (NR_SEQUENCIA),
  CONSTRAINT PEPREPR_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PE_PRESCR_PROC TO NIVEL_1;

