ALTER TABLE TASY.W_PLS_RELAT_CONTAB_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_RELAT_CONTAB_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_RELAT_CONTAB_MENS
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  VL_ATO_COOPERADO_PRINCIPAL  NUMBER(15,2),
  VL_ATO_COOPERADO_AUX        NUMBER(15,2),
  VL_ATO_NAO_COOPERADO        NUMBER(15,2),
  VL_OUTROS                   NUMBER(15,2),
  IE_TIPO_ITEM                VARCHAR2(2 BYTE),
  IE_PRECO                    VARCHAR2(2 BYTE),
  IE_TIPO_OPERACAO            VARCHAR2(3 BYTE),
  IE_TIPO_CONTRATO            VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPLSRCM_PK ON TASY.W_PLS_RELAT_CONTAB_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSRCM_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PLS_RELAT_CONTAB_MENS ADD (
  CONSTRAINT WPLSRCM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_RELAT_CONTAB_MENS TO NIVEL_1;

