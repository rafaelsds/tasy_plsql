ALTER TABLE TASY.CONVENIO_RECEB_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RECEB_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RECEB_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RECEB         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10)               NOT NULL,
  VL_ADICIONAL         NUMBER(15,2)             NOT NULL,
  NR_LOTE_CONTABIL     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10),
  VL_CAMBIAL_ATIVO     NUMBER(15,2),
  VL_CAMBIAL_PASSIVO   NUMBER(15,2),
  CD_TRIBUTO           NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONREAC_CONRECE_FK_I ON TASY.CONVENIO_RECEB_ADIC
(NR_SEQ_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREAC_LOTCONT_FK_I ON TASY.CONVENIO_RECEB_ADIC
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREAC_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONREAC_PK ON TASY.CONVENIO_RECEB_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREAC_TITRECE_FK_I ON TASY.CONVENIO_RECEB_ADIC
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREAC_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREAC_TRAFINA_FK_I ON TASY.CONVENIO_RECEB_ADIC
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREAC_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREAC_TRIBUTO_FK_I ON TASY.CONVENIO_RECEB_ADIC
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREAC_TRIBUTO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CONVENIO_RECEB_ADIC_INSERT
after insert or update ON TASY.CONVENIO_RECEB_ADIC 
for each row
declare

dt_documento_w		date;
vl_movimento_w		number(15,2);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

cursor c01 is
	select		a.nm_atributo,
			a.cd_tipo_lote_contab
	from		atributo_contab a
	where 		a.cd_tipo_lote_contab = 11
	and 		a.nm_atributo in ('VL_ADICIONAL', 'VL_CAMBIAL_ATIVO', 'VL_CAMBIAL_PASSIVO');

c01_w		c01%rowtype;

begin

if	(inserting) then

	select  dt_recebimento,
		cd_estabelecimento
	into    dt_documento_w,
		cd_estabelecimento_w
	from    Convenio_Receb a
	where   a.nr_sequencia		= :new.nr_seq_receb;
	
	open c01;
	loop
	fetch c01 into	
	c01_w;
	exit when c01%notfound;
		begin
		
		vl_movimento_w		:= :new.vl_adicional;
		
		if 	(c01_w.nm_atributo = 'VL_CAMBIAL_ATIVO') and
			(nvl(:new.vl_cambial_ativo,0) != 0) then
			begin
			vl_movimento_w	:= :new.vl_cambial_ativo;
			end;
		elsif 	(c01_w.nm_atributo = 'VL_CAMBIAL_PASSIVO') and
			(nvl(:new.vl_cambial_passivo,0) != 0) then
			begin
			vl_movimento_w	:= :new.vl_cambial_passivo;
			end;
		end if;
		
		if	(nvl(vl_movimento_w, 0) <> 0) and
			(nvl(:new.nr_seq_trans_financ, 0) <> 0) then
			begin
			ctb_concil_financeira_pck.ctb_gravar_documento	(	
										cd_estabelecimento_w,
										trunc(dt_documento_w),
										c01_w.cd_tipo_lote_contab,
										:new.nr_seq_trans_financ,
										8,
										:new.nr_seq_receb,
										:new.nr_sequencia,
										null,
										vl_movimento_w,
										'CONVENIO_RECEB_ADIC',
										c01_w.nm_atributo,
										:new.nm_usuario);

			end;
		end if;
		end;
	end loop;
	close c01;

elsif (updating) then
	begin
	update  ctb_documento
	set     vl_movimento 		= nvl(:new.vl_adicional,0),
		nr_seq_trans_financ	= :new.nr_seq_trans_financ
	where     nm_tabela   		= 'CONVENIO_RECEB_ADIC'
	and     nr_documento		= :new.nr_seq_receb
	and	nr_seq_doc_compl 	= :new.nr_sequencia
	and	nm_atributo		= 'VL_ADICIONAL'
	and	nvl(nr_lote_contabil,0)	= 0;
	
	update  ctb_documento
	set     vl_movimento 		= nvl(:new.vl_cambial_ativo,0),
		nr_seq_trans_financ	= :new.nr_seq_trans_financ
	where     nm_tabela   		= 'CONVENIO_RECEB_ADIC'
	and     nr_documento		= :new.nr_seq_receb
	and	nr_seq_doc_compl 	= :new.nr_sequencia
	and	nm_atributo		= 'VL_CAMBIAL_ATIVO'
	and	nvl(nr_lote_contabil,0)	= 0;
	
	update  ctb_documento
	set     vl_movimento 		= nvl(:new.vl_cambial_passivo,0),
		nr_seq_trans_financ	= :new.nr_seq_trans_financ
	where     nm_tabela   		= 'CONVENIO_RECEB_ADIC'
	and     nr_documento		= :new.nr_seq_receb
	and	nr_seq_doc_compl 	= :new.nr_sequencia
	and	nm_atributo		= 'VL_CAMBIAL_PASSIVO'
	and	nvl(nr_lote_contabil,0)	= 0;
	end;
end if;

end CONVENIO_RECEB_ADIC_INSERT;
/


ALTER TABLE TASY.CONVENIO_RECEB_ADIC ADD (
  CONSTRAINT CONREAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RECEB_ADIC ADD (
  CONSTRAINT CONREAC_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CONREAC_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT CONREAC_CONRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEB) 
 REFERENCES TASY.CONVENIO_RECEB (NR_SEQUENCIA),
  CONSTRAINT CONREAC_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT CONREAC_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.CONVENIO_RECEB_ADIC TO NIVEL_1;

