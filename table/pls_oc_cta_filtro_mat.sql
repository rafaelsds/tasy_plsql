ALTER TABLE TASY.PLS_OC_CTA_FILTRO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_FILTRO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_FILTRO_MAT
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_FILTRO     NUMBER(10)           NOT NULL,
  NR_SEQ_MATERIAL          NUMBER(10),
  IE_TIPO_DESPESA_MAT      VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_MATERIAL    NUMBER(10),
  IE_FERIADO               VARCHAR2(1 BYTE),
  DT_DIA_SEMANA            NUMBER(2),
  HR_INICIAL               DATE,
  HR_FINAL                 DATE,
  IE_TIPO_FERIADO          VARCHAR2(15 BYTE),
  IE_TIPO_DATA             VARCHAR2(3 BYTE),
  IE_CONSISTENCIA_VALOR    VARCHAR2(10 BYTE),
  VL_MAX_ITEM              NUMBER(15,4),
  VL_MINIMO_ITEM           NUMBER(15,4),
  NR_SEQ_ESTRUT_MAT        NUMBER(10),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  IE_PRESTADOR_ALTO_CUSTO  VARCHAR2(1 BYTE),
  VL_MINIMO_ITEM_IMP       NUMBER(15,4),
  VL_MAX_ITEM_IMP          NUMBER(15,4),
  NR_SEQ_TIPO_PRESTADOR    NUMBER(10),
  IE_TIPO_RELACAO          VARCHAR2(2 BYTE),
  IE_MATERIAL_TUSS         VARCHAR2(5 BYTE),
  ID_PACOTE_PTU            VARCHAR2(1 BYTE),
  IE_ACRES_URG_EMER        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCFM_PK ON TASY.PLS_OC_CTA_FILTRO_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCFM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCFM_PLSESMAT_FK_I ON TASY.PLS_OC_CTA_FILTRO_MAT
(NR_SEQ_ESTRUT_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCFM_PLSESMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCFM_PLSMAT_FK_I ON TASY.PLS_OC_CTA_FILTRO_MAT
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCFM_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCFM_PLSOCCF_FK_I ON TASY.PLS_OC_CTA_FILTRO_MAT
(NR_SEQ_OC_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCFM_PLSOCCF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCFM_PLSPRGM_FK_I ON TASY.PLS_OC_CTA_FILTRO_MAT
(NR_SEQ_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCFM_PLSPRGM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCFM_PLSTIPR_FK_I ON TASY.PLS_OC_CTA_FILTRO_MAT
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_FILTRO_MAT_tp  after update ON TASY.PLS_OC_CTA_FILTRO_MAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_ESTRUT_MAT,1,4000),substr(:new.NR_SEQ_ESTRUT_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ESTRUT_MAT',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO_ITEM,1,4000),substr(:new.VL_MINIMO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO_ITEM',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_RELACAO,1,4000),substr(:new.IE_TIPO_RELACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_RELACAO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.ID_PACOTE_PTU,1,4000),substr(:new.ID_PACOTE_PTU,1,4000),:new.nm_usuario,nr_seq_w,'ID_PACOTE_PTU',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MINIMO_ITEM_IMP,1,4000),substr(:new.VL_MINIMO_ITEM_IMP,1,4000),:new.nm_usuario,nr_seq_w,'VL_MINIMO_ITEM_IMP',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAX_ITEM_IMP,1,4000),substr(:new.VL_MAX_ITEM_IMP,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAX_ITEM_IMP',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MATERIAL_TUSS,1,4000),substr(:new.IE_MATERIAL_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL_TUSS',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACRES_URG_EMER,1,4000),substr(:new.IE_ACRES_URG_EMER,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACRES_URG_EMER',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_FINAL,'hh24:mi:ss'),to_char(:new.HR_FINAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_FINAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_INICIAL,'hh24:mi:ss'),to_char(:new.HR_INICIAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_INICIAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTENCIA_VALOR,1,4000),substr(:new.IE_CONSISTENCIA_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTENCIA_VALOR',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FERIADO,1,4000),substr(:new.IE_FERIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FERIADO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DATA,1,4000),substr(:new.IE_TIPO_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DATA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DESPESA_MAT,1,4000),substr(:new.IE_TIPO_DESPESA_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DESPESA_MAT',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_FERIADO,1,4000),substr(:new.IE_TIPO_FERIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FERIADO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DIA_SEMANA,1,4000),substr(:new.DT_DIA_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'DT_DIA_SEMANA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_MATERIAL,1,4000),substr(:new.NR_SEQ_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_MATERIAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_OC_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_FILTRO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAX_ITEM,1,4000),substr(:new.VL_MAX_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAX_ITEM',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESTADOR_ALTO_CUSTO,1,4000),substr(:new.IE_PRESTADOR_ALTO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_ALTO_CUSTO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_MAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_FILTRO_MAT ADD (
  CONSTRAINT PLSOCFM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_FILTRO_MAT ADD (
  CONSTRAINT PLSOCFM_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSOCFM_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSOCFM_PLSOCCF_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_FILTRO) 
 REFERENCES TASY.PLS_OC_CTA_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSOCFM_PLSPRGM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MATERIAL) 
 REFERENCES TASY.PLS_PRECO_GRUPO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSOCFM_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT_MAT) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_FILTRO_MAT TO NIVEL_1;

