ALTER TABLE TASY.ENFERM_PERFIL_PERCEPCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ENFERM_PERFIL_PERCEPCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENFERM_PERFIL_PERCEPCAO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE) NOT NULL,
  NR_ATENDIMENTO               NUMBER(10)       NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DS_CARATER_PESSOAL           VARCHAR2(255 BYTE),
  DS_PROVEITO_PESSOAL          VARCHAR2(255 BYTE),
  IE_SEM_ESPERANCA             VARCHAR2(1 BYTE),
  IE_FICA_FECHADO              VARCHAR2(1 BYTE),
  IE_SATISFACAO                VARCHAR2(1 BYTE),
  IE_SEM_CONTROLE_SITUACAO     VARCHAR2(1 BYTE),
  IE_SENTE_MEDO                VARCHAR2(1 BYTE),
  IE_SOFRIMENTO_CAUSA_DOENCA   VARCHAR2(1 BYTE),
  IE_ANSIEDADE                 VARCHAR2(1 BYTE),
  IE_NERVOSISMO                VARCHAR2(1 BYTE),
  IE_DEPRESSAO                 VARCHAR2(1 BYTE),
  IE_NEGATIVO                  VARCHAR2(1 BYTE),
  IE_COM_ESPERANCA             VARCHAR2(1 BYTE),
  IE_INCOMODADO                VARCHAR2(1 BYTE),
  IE_SENTIMENTO_SEGURANCA      VARCHAR2(1 BYTE),
  IE_OUTROS_SENTIMENTO         VARCHAR2(1 BYTE),
  DS_OUTROS_SENTIMENTO         VARCHAR2(255 BYTE),
  IE_MUDANCA_CORPORAL          VARCHAR2(1 BYTE),
  DS_MUDANCA_CORPORAL          VARCHAR2(255 BYTE),
  IE_POSSIVEIS_MUDANCAS        VARCHAR2(1 BYTE),
  DS_POSSIVEIS_MUDANCAS        VARCHAR2(255 BYTE),
  IE_AREAS_MUDANCAS            VARCHAR2(1 BYTE),
  DS_AREAS_MUDANCAS            VARCHAR2(255 BYTE),
  IE_DESEJO_MUDAR_APARECENCIA  VARCHAR2(1 BYTE),
  DS_DESEJO_MUDAR_APARECENCIA  VARCHAR2(255 BYTE),
  IE_AUTODEPRECIACAO           VARCHAR2(1 BYTE),
  IE_DESESPERO                 VARCHAR2(1 BYTE),
  IE_SOLIDAO                   VARCHAR2(1 BYTE),
  DS_EXPRESSAO_HOSPITALIZACAO  VARCHAR2(255 BYTE),
  IE_SEM_ESPERANCA_DOENCA      VARCHAR2(1 BYTE),
  IE_NERVOSO_DOENCA            VARCHAR2(1 BYTE),
  IE_SEM_CONTR_SIT_DOENCA      VARCHAR2(1 BYTE),
  IE_RELUTANTE_DOENCA          VARCHAR2(1 BYTE),
  IE_ANGUSTIA_DOENCA           VARCHAR2(1 BYTE),
  IE_IRRITADO_DOENCA           VARCHAR2(1 BYTE),
  IE_FECHADO_CASA_DOENCA       VARCHAR2(1 BYTE),
  IE_SENSACAO_MEDO_DOENCA      VARCHAR2(1 BYTE),
  IE_DEPRESSAO_DOENCA          VARCHAR2(1 BYTE),
  IE_ANSIEDADE_DOENCA          VARCHAR2(1 BYTE),
  IE_OUTROS_DOENCA             VARCHAR2(1 BYTE),
  DS_OUTROS_DOENCA             VARCHAR2(255 BYTE),
  IE_CONTATO_VISUAL            VARCHAR2(1 BYTE),
  IE_SEM_INTERESSE_COMPANHIA   VARCHAR2(1 BYTE),
  IE_NEGATIVO_SI_PROPRIO       VARCHAR2(1 BYTE),
  IE_QUESTIONA_TRATAMENTO      VARCHAR2(1 BYTE),
  IE_AGRESSIVO                 VARCHAR2(1 BYTE),
  IE_OUTROS_ESTADO_PAC         VARCHAR2(1 BYTE),
  DS_OUTROS_ESTADO_PAC         VARCHAR2(255 BYTE),
  IE_NAO_OLHA                  VARCHAR2(1 BYTE),
  IE_NAO_TOCA                  VARCHAR2(1 BYTE),
  IE_ESCONDE                   VARCHAR2(1 BYTE),
  IE_OUTROS_REACAO_CORPO       VARCHAR2(1 BYTE),
  DS_OUTROS_REACAO_CORPO       VARCHAR2(255 BYTE),
  DS_OUTROS                    VARCHAR2(255 BYTE),
  DT_RESUMO                    DATE,
  IE_PROBLEMA                  VARCHAR2(1 BYTE),
  DS_RESUMO                    VARCHAR2(255 BYTE),
  NR_SEQ_CABECALHO             NUMBER(10),
  DT_LIBERACAO                 DATE,
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ENFPPERCEP_ATEPACI_FK_I ON TASY.ENFERM_PERFIL_PERCEPCAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFPPERCEP_CABECAL_FK_I ON TASY.ENFERM_PERFIL_PERCEPCAO
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFPPERCEP_PESFISI_FK_I ON TASY.ENFERM_PERFIL_PERCEPCAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ENFPPERCEP_PK ON TASY.ENFERM_PERFIL_PERCEPCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ENFERM_PERFIL_PERCEPCAO ADD (
  CONSTRAINT ENFPPERCEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ENFERM_PERFIL_PERCEPCAO ADD (
  CONSTRAINT ENFPPERCEP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ENFPPERCEP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ENFPPERCEP_CABECAL_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.ENFERM_PERFIL_CABECALHO (NR_SEQUENCIA));

