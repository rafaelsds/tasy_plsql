ALTER TABLE TASY.REPASSE_TERCEIRO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REPASSE_TERCEIRO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.REPASSE_TERCEIRO_ITEM
(
  NR_REPASSE_TERCEIRO     NUMBER(10),
  NR_SEQUENCIA_ITEM       NUMBER(5),
  VL_REPASSE              NUMBER(15,2)          NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  NR_LOTE_CONTABIL        NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE),
  NR_SEQ_TRANS_FIN        NUMBER(10),
  NR_SEQ_REGRA            NUMBER(10),
  CD_MEDICO               VARCHAR2(10 BYTE),
  NR_SEQ_REGRA_ESP        NUMBER(10),
  DT_LANCAMENTO           DATE,
  CD_CENTRO_CUSTO         NUMBER(8),
  NR_SEQ_TERCEIRO         NUMBER(10),
  DT_LIBERACAO            DATE,
  NR_SEQ_TRANS_FIN_PROV   NUMBER(10),
  CD_CONTA_CONTABIL_PROV  VARCHAR2(20 BYTE),
  CD_CENTRO_CUSTO_PROV    NUMBER(8),
  NR_LOTE_CONTABIL_PROV   NUMBER(10),
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_CONTA_FINANC         NUMBER(10),
  CD_CONVENIO             NUMBER(5),
  NR_SEQ_RET_GLOSA        NUMBER(10),
  NR_SEQ_TERC_REP         NUMBER(10),
  DS_COMPLEMENTO          LONG,
  DT_CONTABIL             DATE,
  QT_MINUTO               NUMBER(15,2),
  NR_SEQ_TIPO             NUMBER(10),
  NR_ATENDIMENTO          NUMBER(10),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  IE_PARTIC_TRIBUTO       VARCHAR2(1 BYTE),
  NR_ADIANT_PAGO          NUMBER(10),
  CD_MATERIAL             NUMBER(6),
  NR_SEQ_TIPO_VALOR       NUMBER(10),
  NR_SEQ_TERC_REGRA_ESP   NUMBER(10),
  NR_SEQ_TERC_REGRA_ITEM  NUMBER(10),
  DT_PLANTAO              DATE,
  NR_SEQ_MED_PLANTAO      NUMBER(10),
  NR_INTERNO_CONTA        NUMBER(10),
  NR_SEQ_REPASSE_PROD     NUMBER(10),
  NR_SEQ_PROC_INTERNO     NUMBER(10),
  VL_DIA_ANTECIPACAO      NUMBER(15,2),
  NR_SEQ_REGRA_INTERV     NUMBER(10),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  VL_OUTROS_ACRESCIMOS    NUMBER(15,2),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  QT_PONTOS_REGRA         NUMBER(15,2),
  NR_SEQ_REGRA_REP_PONTO  NUMBER(10),
  VL_INFORMATIVO          NUMBER(15,2),
  NR_CODIGO_CONTROLE      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPTEIT_ADIPAGO_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_ADIANT_PAGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_ATEPACI_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_CENCUST_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_CENCUST_FK2_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_CENTRO_CUSTO_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_CENCUST_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_CONCONT_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_CONCONT_FK2_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_CONTA_CONTABIL_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_CONFINA_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_CONPACI_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_CONVENI_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_CORETGL_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_RET_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_LOTCONT_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_LOTCONT_FK2_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_LOTE_CONTABIL_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_MATERIA_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_MEDICO_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_MEPLANT_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_MED_PLANTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_MEPLANT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REPTEIT_PK ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_PROCEDI_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_PROINTE_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_REGESRE_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_REGRA_ESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_REGESRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_REPTERC_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_REPASSE_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_REREPPO_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_REGRA_REP_PONTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_RGINTPR_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_REGRA_INTERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_RGINTPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_RPRODPL_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_REPASSE_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_RPRODPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_SETATEN_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_TERCEIR_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_TERCREP_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TERC_REP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_TERCREP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_TERREES_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TERC_REGRA_ESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTEIT_TIREPIT_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_TIREPIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_TPVLRES_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TIPO_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_TPVLRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_TRAFINA_FK_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTEIT_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTEIT_TRAFINA_FK2_I ON TASY.REPASSE_TERCEIRO_ITEM
(NR_SEQ_TRANS_FIN_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REPTEIT_UK ON TASY.REPASSE_TERCEIRO_ITEM
(NR_REPASSE_TERCEIRO, NR_SEQUENCIA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.repasse_terceiro_item_Atual
BEFORE UPDATE ON TASY.REPASSE_TERCEIRO_ITEM FOR EACH ROW
declare

cont_w				number(1, 0);
nr_seq_repasse_w		Number(10,0);
qt_reg_w			number(1);
ie_vincular_rep_proc_pos_w	varchar2(1);
ie_vinc_repasse_ret_w		varchar2(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

CONSISTIR_VENC_REPASSE(
	:old.nr_repasse_terceiro,
	:new.nr_repasse_terceiro,
	:old.vl_repasse,
	:new.vl_repasse,
	0,
	0);


-- se est� desvinculando
if	(:new.nr_repasse_terceiro is null) and
	(:old.nr_repasse_terceiro is not null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :old.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		/* O repasse deste item j� est� fechado!
		Repasse: #@NR_REPASSE_TERCEIRO#@*/
		wheb_mensagem_pck.exibir_mensagem_abort(266952, 'NR_REPASSE_TERCEIRO=' || :old.nr_repasse_terceiro);
	end if;
end if;

-- se est� vinculando
if	(:new.nr_repasse_terceiro is not null) and
	(:old.nr_repasse_terceiro is null) then
	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_repasse_terceiro	= :new.nr_repasse_terceiro
	and	ie_status		= 'F';

	if	(cont_w > 0) then
		/* O repasse deste item j� est� fechado!
		Repasse: #@NR_REPASSE_TERCEIRO#@*/
		wheb_mensagem_pck.exibir_mensagem_abort(266952, 'NR_REPASSE_TERCEIRO=' || :old.nr_repasse_terceiro);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.repasse_terceiro_item_delete
before delete ON TASY.REPASSE_TERCEIRO_ITEM for each row
declare
ie_ctb_online_w         varchar2(01 char);
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;
cd_tipo_lote_contabil_w CONSTANT number(2) := 14;
nm_tabela_w             CONSTANT varchar2(21 char) := upper('REPASSE_TERCEIRO_ITEM');
ie_situacao_ctb_w         varchar2(01 char);

begin
if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
    begin
    select  t.cd_estabelecimento
    into    cd_estabelecimento_w
    from    terceiro t
    where   t.nr_sequencia = :old.nr_seq_terceiro;

    select  nvl(max(ie_situacao_ctb),'P')
    into    ie_situacao_ctb_w
    from    ctb_documento a
    where   a.cd_tipo_lote_contabil = cd_tipo_lote_contabil_w
    and     a.nm_tabela = nm_tabela_w
    and     a.nr_documento = :old.nr_sequencia;

    -- P (Pendente)
    -- C (Contabilizado)
    -- N (Normal)

    if (ie_situacao_ctb_w = 'C') then
        --if (nvl(:old.nr_lote_contabil,0) <> 0 or nvl(:old.nr_lote_contabil_prov,0) <> 0) then
        --ie_ctb_online_w := ctb_online_pck.get_modo_lote(cd_tipo_lote_contabil_p => cd_tipo_lote_contabil_w,
                                                        --cd_estabelecimento_p    => cd_estabelecimento_w);
       -- if (ie_ctb_online_w = upper('S')) then
            ctb_contab_onl_repasse_pck.deleta_contab_item_repasse(nr_sequencia_p => :old.nr_sequencia,
                                                                  nm_usuario_p   => nvl(wheb_usuario_pck.get_nm_usuario,:old.nm_usuario),
                                                                  ie_commit_p    => upper('N'));
    end if;

    delete  ctb_documento a
    where   a.cd_tipo_lote_contabil = cd_tipo_lote_contabil_w
    and     a.nm_tabela = nm_tabela_w
    and     a.nr_documento = :old.nr_sequencia;
    end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.REPASSE_TERCEIRO_ITEM_INSERT
BEFORE INSERT ON TASY.REPASSE_TERCEIRO_ITEM FOR EACH ROW
declare

nr_seq_terceiro_w	repasse_terceiro.nr_seq_terceiro%type;

begin

if	(:new.nr_seq_terceiro		is null) and
	(:new.nr_repasse_terceiro	is not null) then

	select	max(a.nr_seq_terceiro)
	into	nr_seq_terceiro_w
	from	repasse_terceiro a
	where	a.nr_repasse_terceiro	= :new.nr_repasse_terceiro;

	:new.nr_seq_terceiro	:= nr_seq_terceiro_w;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.repasse_terceiro_item_ctb_onl
  after insert or update ON TASY.REPASSE_TERCEIRO_ITEM   for each row
declare
   -- local variables here
   cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
   dt_competencia_w        ctb_documento.dt_competencia%type;
   cd_tipo_lote_contabil_w ctb_documento.cd_tipo_lote_contabil%type;
   nr_seq_trans_financ_w   ctb_documento.nr_seq_trans_financ%type;
   nr_seq_info_w           ctb_documento.nr_seq_info%type;
   nr_documento_w          ctb_documento.nr_documento%type;
   nr_seq_doc_compl_w      ctb_documento.nr_seq_doc_compl%type;
   nr_doc_analitico_w      ctb_documento.nr_doc_analitico%type;
   vl_movimento_w          ctb_documento.vl_movimento%type;
   nm_tabela_w             ctb_documento.nm_tabela%type;
   nm_atributo_w           ctb_documento.nm_atributo%type;
   ie_situacao_ctb_w       ctb_documento.ie_situacao_ctb%type;
   ds_origem_w             ctb_documento.ds_origem%type;
   nr_sequencia_doc_w      ctb_documento.nr_sequencia%type;

begin

   --- Define estabelecimnto
   select max(t.cd_estabelecimento)
     into cd_estabelecimento_w
     from terceiro t
    where t.nr_sequencia = :new.nr_seq_terceiro;

   cd_estabelecimento_w    := cd_estabelecimento_w;
   dt_competencia_w        := nvl(:new.dt_contabil, :new.dt_lancamento);
   cd_tipo_lote_contabil_w := 14;
   nr_seq_trans_financ_w   := :new.nr_seq_trans_fin_prov;
   nr_seq_info_w           := 69;
   nr_documento_w          := :new.nr_sequencia;
   nr_seq_doc_compl_w      := :new.nr_seq_terceiro;
   nr_doc_analitico_w      := null;
   nm_tabela_w             := 'REPASSE_TERCEIRO_ITEM';

   if (inserting) then
      if (:new.nr_seq_terceiro is not null) and
         (:new.dt_lancamento is not null) and
         (:new.nr_seq_trans_fin_prov is not null) then

         if (nvl(:new.vl_repasse,0) <> 0) then
            vl_movimento_w := :new.vl_repasse;
            nm_atributo_w  := 'VL_REPASSE';

            ctb_concil_financeira_pck.ctb_gravar_documento(cd_estabelecimento_p    => cd_estabelecimento_w,
                                                           dt_competencia_p        => dt_competencia_w,
                                                           cd_tipo_lote_contabil_p => cd_tipo_lote_contabil_w,
                                                           nr_seq_trans_financ_p   => nr_seq_trans_financ_w,
                                                           nr_seq_info_p           => nr_seq_info_w,
                                                           nr_documento_p          => nr_documento_w,
                                                           nr_seq_doc_compl_p      => nr_seq_doc_compl_w,
                                                           nr_doc_analitico_p      => nr_doc_analitico_w,
                                                           vl_movimento_p          => vl_movimento_w,
                                                           nm_tabela_p             => nm_tabela_w,
                                                           nm_atributo_p           => nm_atributo_w,
                                                           nm_usuario_p            => :new.nm_usuario);
         end if;

         if (nvl(:new.vl_dia_antecipacao,0) <> 0) then
            vl_movimento_w := :new.vl_dia_antecipacao;
            nm_atributo_w  := 'VL_DIA_ANTECIPACAO';

            ctb_concil_financeira_pck.ctb_gravar_documento(cd_estabelecimento_p    => cd_estabelecimento_w,
                                                           dt_competencia_p        => dt_competencia_w,
                                                           cd_tipo_lote_contabil_p => cd_tipo_lote_contabil_w,
                                                           nr_seq_trans_financ_p   => nr_seq_trans_financ_w,
                                                           nr_seq_info_p           => nr_seq_info_w,
                                                           nr_documento_p          => nr_documento_w,
                                                           nr_seq_doc_compl_p      => nr_seq_doc_compl_w,
                                                           nr_doc_analitico_p      => nr_doc_analitico_w,
                                                           vl_movimento_p          => vl_movimento_w,
                                                           nm_tabela_p             => nm_tabela_w,
                                                           nm_atributo_p           => nm_atributo_w,
                                                           nm_usuario_p            => :new.nm_usuario);
         end if;
      end if;
   elsif (updating) then
      if (nvl(:new.vl_repasse,0) <> 0) then
         vl_movimento_w := nvl(:new.vl_repasse, :old.vl_repasse);
         nm_atributo_w  := 'VL_REPASSE';

         update ctb_documento a
            set a.dt_competencia      = dt_competencia_w,
                a.vl_movimento        = vl_movimento_w,
                a.nr_seq_trans_financ = nr_seq_trans_financ_w
          where a.nr_documento = nr_documento_w
            and a.nr_seq_doc_compl = nr_seq_doc_compl_w
            and a.cd_tipo_lote_contabil = 14
            and a.nr_seq_info = 69
            and a.nm_atributo = nm_atributo_w
         returning a.nr_sequencia into nr_sequencia_doc_w;

         if (nr_sequencia_doc_w is null) then
            ctb_concil_financeira_pck.ctb_gravar_documento(cd_estabelecimento_p    => cd_estabelecimento_w,
                                                           dt_competencia_p        => dt_competencia_w,
                                                           cd_tipo_lote_contabil_p => cd_tipo_lote_contabil_w,
                                                           nr_seq_trans_financ_p   => nr_seq_trans_financ_w,
                                                           nr_seq_info_p           => nr_seq_info_w,
                                                           nr_documento_p          => nr_documento_w,
                                                           nr_seq_doc_compl_p      => nr_seq_doc_compl_w,
                                                           nr_doc_analitico_p      => nr_doc_analitico_w,
                                                           vl_movimento_p          => vl_movimento_w,
                                                           nm_tabela_p             => nm_tabela_w,
                                                           nm_atributo_p           => nm_atributo_w,
                                                           nm_usuario_p            => :new.nm_usuario);
         end if;
      end if;

      if (nvl(:new.vl_dia_antecipacao,0) <> 0) then
         vl_movimento_w := nvl(:new.vl_dia_antecipacao,
                               :old.vl_dia_antecipacao);
         nm_atributo_w  := 'VL_DIA_ANTECIPACAO';

         update ctb_documento a
            set a.dt_competencia      = dt_competencia_w,
                a.vl_movimento        = vl_movimento_w,
                a.nr_seq_trans_financ = nr_seq_trans_financ_w
          where a.nr_documento = nr_documento_w
            and a.nr_seq_doc_compl = nr_seq_doc_compl_w
            and a.cd_tipo_lote_contabil = 14
            and a.nr_seq_info = 69
            and a.nm_atributo = nm_atributo_w
         returning a.nr_sequencia into nr_sequencia_doc_w;

         if (nr_sequencia_doc_w is null) then
            ctb_concil_financeira_pck.ctb_gravar_documento(cd_estabelecimento_p    => cd_estabelecimento_w,
                                                           dt_competencia_p        => dt_competencia_w,
                                                           cd_tipo_lote_contabil_p => cd_tipo_lote_contabil_w,
                                                           nr_seq_trans_financ_p   => nr_seq_trans_financ_w,
                                                           nr_seq_info_p           => nr_seq_info_w,
                                                           nr_documento_p          => nr_documento_w,
                                                           nr_seq_doc_compl_p      => nr_seq_doc_compl_w,
                                                           nr_doc_analitico_p      => nr_doc_analitico_w,
                                                           vl_movimento_p          => vl_movimento_w,
                                                           nm_tabela_p             => nm_tabela_w,
                                                           nm_atributo_p           => nm_atributo_w,
                                                           nm_usuario_p            => :new.nm_usuario);
         end if;
      end if;
   end if;

end repasse_terceiro_item_ctb_onl;
/


ALTER TABLE TASY.REPASSE_TERCEIRO_ITEM ADD (
  CONSTRAINT REPTEIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT REPTEIT_UK
 UNIQUE (NR_REPASSE_TERCEIRO, NR_SEQUENCIA_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REPASSE_TERCEIRO_ITEM ADD (
  CONSTRAINT REPTEIT_RPRODPL_FK 
 FOREIGN KEY (NR_SEQ_REPASSE_PROD) 
 REFERENCES TASY.REPASSE_PRODUCAO_PLANTAO (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_REREPPO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_REP_PONTO) 
 REFERENCES TASY.REGRA_REPASSE_PONTUACAO (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_RGINTPR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_INTERV) 
 REFERENCES TASY.REGRA_INTERVALO_PRODUCAO (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REPTEIT_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_CENCUST_FK2 
 FOREIGN KEY (CD_CENTRO_CUSTO_PROV) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT REPTEIT_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_CONTABIL_PROV) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT REPTEIT_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_PROV) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT REPTEIT_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERCEIRO) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_PROV) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT REPTEIT_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REPTEIT_CORETGL_FK 
 FOREIGN KEY (NR_SEQ_RET_GLOSA) 
 REFERENCES TASY.CONVENIO_RETORNO_GLOSA (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_TERCREP_FK 
 FOREIGN KEY (NR_SEQ_TERC_REP) 
 REFERENCES TASY.TERCEIRO_REPASSE (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_TIREPIT_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_REPASSE_ITEM (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT REPTEIT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT REPTEIT_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANT_PAGO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO),
  CONSTRAINT REPTEIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REPTEIT_TERREES_FK 
 FOREIGN KEY (NR_SEQ_TERC_REGRA_ESP) 
 REFERENCES TASY.TERCEIRO_REGRA_ESP (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_TPVLRES_FK 
 FOREIGN KEY (NR_SEQ_TIPO_VALOR) 
 REFERENCES TASY.TIPO_VALOR_REPASSE_ESP (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_MEPLANT_FK 
 FOREIGN KEY (NR_SEQ_MED_PLANTAO) 
 REFERENCES TASY.MEDICO_PLANTAO (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT REPTEIT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT REPTEIT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT REPTEIT_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT REPTEIT_REPTERC_FK 
 FOREIGN KEY (NR_REPASSE_TERCEIRO) 
 REFERENCES TASY.REPASSE_TERCEIRO (NR_REPASSE_TERCEIRO)
    ON DELETE CASCADE,
  CONSTRAINT REPTEIT_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT REPTEIT_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT REPTEIT_REGESRE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ESP) 
 REFERENCES TASY.REGRA_ESP_REPASSE (NR_SEQUENCIA));

GRANT SELECT ON TASY.REPASSE_TERCEIRO_ITEM TO NIVEL_1;

