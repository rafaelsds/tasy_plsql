ALTER TABLE TASY.PLS_REGRA_ITEM_DUPLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ITEM_DUPLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ITEM_DUPLIC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_REGRA           NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_DIAS                NUMBER(3),
  IE_REGRA_TIPO_GUIA     VARCHAR2(1 BYTE),
  IE_REGRA_PREST_EXEC    VARCHAR2(1 BYTE),
  IE_REGRA_PROFISSIONAL  VARCHAR2(1 BYTE),
  IE_REGRA_BENEF         VARCHAR2(1 BYTE),
  IE_REGRA_DT_PROC       VARCHAR2(1 BYTE),
  IE_REGRA_GUIA_REF      VARCHAR2(1 BYTE),
  IE_REGRA_GRAU_PARTIC   VARCHAR2(1 BYTE),
  IE_REGRA_PROTOCOLO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSREID_PK ON TASY.PLS_REGRA_ITEM_DUPLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREID_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_ITEM_DUPLIC ADD (
  CONSTRAINT PLSREID_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_REGRA_ITEM_DUPLIC TO NIVEL_1;

