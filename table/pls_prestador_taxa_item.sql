ALTER TABLE TASY.PLS_PRESTADOR_TAXA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_TAXA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_TAXA_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_PRESTADOR        NUMBER(10)            NOT NULL,
  TX_ITEM                 NUMBER(7,4),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_GRUPO_PROC           NUMBER(15),
  CD_ESPECIALIDADE        NUMBER(15),
  CD_AREA_PROCEDIMENTO    NUMBER(15),
  NR_SEQ_MATERIAL         NUMBER(10),
  NR_SEQ_GRUPO_MATERIAL   NUMBER(10),
  NR_SEQ_ESTRUT_MAT       NUMBER(10),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  IE_PRECO                VARCHAR2(2 BYTE),
  IE_TIPO_SEGURADO        VARCHAR2(3 BYTE),
  NR_SEQ_GRUPO_CONTRATO   NUMBER(10),
  NR_SEQ_GRUPO_SERVICO    NUMBER(10),
  VL_FIXO                 NUMBER(15,2),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPTIT_AREPROC_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPTIT_ESPPROC_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPTIT_GRUPROC_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPTIT_I1 ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_PRESTADOR, CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPTIT_I2 ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_PRESTADOR, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPTIT_PK ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPTIT_PLSESMAT_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_ESTRUT_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_PLSESMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPTIT_PLSMAT_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPTIT_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPTIT_PLSPRGC_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPTIT_PLSPRGM_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_PLSPRGM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPTIT_PLSPRGS_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPTIT_PROCEDI_FK_I ON TASY.PLS_PRESTADOR_TAXA_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPTIT_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_PRESTADOR_TAXA_ITEM_tp  after update ON TASY.PLS_PRESTADOR_TAXA_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_SEGURADO,1,4000),substr(:new.IE_TIPO_SEGURADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SEGURADO',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO,1,4000),substr(:new.IE_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_CONTRATO,1,4000),substr(:new.NR_SEQ_GRUPO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_CONTRATO',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FIXO,1,4000),substr(:new.VL_FIXO,1,4000),:new.nm_usuario,nr_seq_w,'VL_FIXO',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_SERVICO,1,4000),substr(:new.NR_SEQ_GRUPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_SERVICO',ie_log_w,ds_w,'PLS_PRESTADOR_TAXA_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_taxa_item_atual
before update ON TASY.PLS_PRESTADOR_TAXA_ITEM for each row
declare

begin

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end;
/


ALTER TABLE TASY.PLS_PRESTADOR_TAXA_ITEM ADD (
  CONSTRAINT PLSPTIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_TAXA_ITEM ADD (
  CONSTRAINT PLSPTIT_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSPTIT_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSPTIT_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSPTIT_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPTIT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSPTIT_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSPTIT_PLSPRGM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MATERIAL) 
 REFERENCES TASY.PLS_PRECO_GRUPO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSPTIT_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT_MAT) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSPTIT_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSPTIT_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_TAXA_ITEM TO NIVEL_1;

