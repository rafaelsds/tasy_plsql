ALTER TABLE TASY.ESCALA_FRAMINGHAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_FRAMINGHAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_FRAMINGHAM
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_CLIENTE             NUMBER(10),
  NR_ATEND_TASYMED           NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  QT_IDADE                   NUMBER(5)          NOT NULL,
  QT_HDL                     NUMBER(15,2)       NOT NULL,
  QT_PA_SISTOLICA            NUMBER(3)          NOT NULL,
  IE_TRAT_PAS                VARCHAR2(1 BYTE)   NOT NULL,
  QT_COLESTEROL              NUMBER(15,2)       NOT NULL,
  IE_FUMANTE                 VARCHAR2(1 BYTE)   NOT NULL,
  QT_PONTUACAO               NUMBER(15),
  QT_RISCO                   VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_ESC_ORIGINAL            VARCHAR2(1 BYTE),
  IE_DIABETES                VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCFRAM_ATEPACI_FK_I ON TASY.ESCALA_FRAMINGHAM
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFRAM_I1 ON TASY.ESCALA_FRAMINGHAM
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFRAM_MEDATEN_FK_I ON TASY.ESCALA_FRAMINGHAM
(NR_ATEND_TASYMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFRAM_MEDATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCFRAM_MEDCLIE_FK_I ON TASY.ESCALA_FRAMINGHAM
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFRAM_MEDCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCFRAM_PERFIL_FK_I ON TASY.ESCALA_FRAMINGHAM
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFRAM_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCFRAM_PESFISI_FK_I ON TASY.ESCALA_FRAMINGHAM
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFRAM_PESFISI_FK2_I ON TASY.ESCALA_FRAMINGHAM
(CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCFRAM_PK ON TASY.ESCALA_FRAMINGHAM
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFRAM_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCFRAM_TASASDI_FK_I ON TASY.ESCALA_FRAMINGHAM
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFRAM_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCFRAM_TASASDI_FK2_I ON TASY.ESCALA_FRAMINGHAM
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFRAM_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_framingham_pend_atual
after insert or update ON TASY.ESCALA_FRAMINGHAM for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'13');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_FRAMINGHAM_UPDATE
BEFORE UPDATE OR INSERT ON TASY.ESCALA_FRAMINGHAM FOR EACH ROW
declare
nr_sequencia_w			Number(10,0);
qt_ponto_w			Number(3)	:= 0;
ie_sexo_w			varchar2(1);
qt_risco_w 			varchar2(10);
qt_reg_w			number(1,0);
BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if   (:new.qt_idade < 20) or
     (:new.qt_idade > 79) then
     wheb_mensagem_pck.exibir_mensagem_abort(228324);
end if;

if	(:new.cd_pessoa_fisica is null) and
	(:new.nr_atendimento is not null) then
	:new.cd_pessoa_fisica := obter_pessoa_atendimento(:new.nr_atendimento,'C');
end if;

select	obter_sexo_pf(:new.cd_pessoa_fisica,'C')
into	ie_sexo_w
from	dual ;

if	(:new.IE_ESC_ORIGINAL = 'N') then
	if	( ie_sexo_w = 'M' ) then
		Begin
		if	(:new.qt_idade <= 34) then
			begin
			qt_ponto_w := -9;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 7;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 9;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 11;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 8;
			end if;
			end;
		elsif	( :new.qt_idade >= 35 ) and (:new.qt_idade <= 39) then
			begin
			qt_ponto_w := -4;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 7;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 9;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 11;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 8;
			end if;
			end;
		elsif	( :new.qt_idade >= 40 ) and (:new.qt_idade <= 44) then
			begin
			qt_ponto_w := 0;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 5;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 6;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 8;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 5;
			end if;
			end;
		elsif	( :new.qt_idade >= 45 ) and (:new.qt_idade <= 49) then
			begin
			qt_ponto_w := 3;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 5;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 6;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 8;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 5;
			end if;
			end;
		elsif	( :new.qt_idade >= 50 ) and (:new.qt_idade <= 54) then
			begin
			qt_ponto_w := 6;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 5;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 3;
			end if;
			end;
		elsif	( :new.qt_idade >= 55 ) and (:new.qt_idade <= 59) then
			begin
			qt_ponto_w := 8;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 5;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 3;
			end if;
			end;
		elsif	( :new.qt_idade >= 60 ) and (:new.qt_idade <= 64) then
			begin
			qt_ponto_w := 10;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 3;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			end;
		elsif	( :new.qt_idade >= 65 ) and (:new.qt_idade <= 69) then
			begin
			qt_ponto_w := 11;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 3;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			end;
		elsif	( :new.qt_idade >= 70 ) and (:new.qt_idade <= 74) then
			begin
			qt_ponto_w := 12;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			end;
		elsif	( :new.qt_idade >= 75 ) then
			begin
			qt_ponto_w := 13;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			end;
		end if;
		if	( :new.qt_HDL >= 60 )  then
			qt_ponto_w := qt_ponto_w + (-1);
		elsif	( :new.qt_HDL> 50 ) and (:new.qt_HDL<= 59 ) then
			qt_ponto_w := qt_ponto_w + 0;
		elsif	( :new.qt_HDL > 40 ) and ( :new.qt_HDL <= 49 ) then
			qt_ponto_w := qt_ponto_w + 1;
		elsif	(:new.qt_HDL<= 40 ) then
			qt_ponto_w := qt_ponto_w + 2;
		end if;
		if	(:new.ie_trat_pas = 'S') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 3;
			end if;
			end;
		elsif	(:new.ie_trat_pas = 'N') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 2;
			end if;
			end;
		end if;

		:new.qt_pontuacao := qt_ponto_w;

		if	(qt_ponto_w < 0 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 0 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w =1 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 2 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 3) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 4 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 5 ) then
			qt_risco_w := '2';
		elsif	(qt_ponto_w = 6 ) then
			qt_risco_w := '2';
		elsif	(qt_ponto_w = 7 ) then
			qt_risco_w := '3';
		elsif	(qt_ponto_w = 8 ) then
			qt_risco_w := '4';
		elsif	(qt_ponto_w = 9 ) then
			qt_risco_w := '5';
		elsif	(qt_ponto_w = 10 ) then
			qt_risco_w := '6';
		elsif	(qt_ponto_w = 11 ) then
			qt_risco_w := '8';
		elsif	(qt_ponto_w = 12 ) then
			qt_risco_w := '10';
		elsif	(qt_ponto_w = 13 ) then
			qt_risco_w := '12';
		elsif	(qt_ponto_w = 14 ) then
			qt_risco_w := '16';
		elsif	(qt_ponto_w = 15 ) then
			qt_risco_w := '20';
		elsif	(qt_ponto_w = 16 ) then
			qt_risco_w := '25';
		elsif	(qt_ponto_w = 17 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 18 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 19 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 20 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 21 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 22 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 23 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w = 24 ) then
			qt_risco_w :=  '>=30';
		elsif	(qt_ponto_w >= 25 ) then
			qt_risco_w := '>=30';
		end if;
		end;
	end if;

	if	( ie_sexo_w = 'F' ) then
		begin
		if	(:new.qt_idade <= 34) then
			begin
			qt_ponto_w := -7;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 8;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 11;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 13;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 9;
			end if;
			end;
		elsif	( :new.qt_idade >= 35 ) and (:new.qt_idade <= 39) then
			begin
			qt_ponto_w := -3;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 8;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 11;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 13;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 9;
			end if;
			end;
		elsif	( :new.qt_idade >= 40 ) and (:new.qt_idade <= 44) then
			begin
			qt_ponto_w := 0;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 6;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 8;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 10;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 7;
			end if;
			end;
		elsif	( :new.qt_idade >= 45 ) and (:new.qt_idade <= 49) then
			begin
			qt_ponto_w := 3;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 6;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 8;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 10;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 7;
			end if;
			end;
		elsif	( :new.qt_idade >= 50 ) and (:new.qt_idade <= 54) then
			begin
			qt_ponto_w := 6;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 5;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 7;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 4;
			end if;
			end;
		elsif	( :new.qt_idade >= 55 ) and (:new.qt_idade <= 59) then
			begin
			qt_ponto_w := 8;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 5;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 7;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 4;
			end if;
			end;
		elsif	( :new.qt_idade >= 60 ) and (:new.qt_idade <= 64) then
			begin
			qt_ponto_w := 10;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 4;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 2;
			end if;
			end;
		elsif	( :new.qt_idade >= 65 ) and (:new.qt_idade <= 69) then
			begin
			qt_ponto_w := 12;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 4;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 2;
			end if;
			end;
		elsif	( :new.qt_idade >= 70 ) and (:new.qt_idade <= 74) then
			begin
			qt_ponto_w := 14;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 2;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			end;
		elsif	( :new.qt_idade >= 75 ) then
			begin
			qt_ponto_w := 16;
			If	( :new.qt_colesterol < 160 )  then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	( :new.qt_colesterol >= 280 ) then
				qt_ponto_w := qt_ponto_w + 2;
			end if;
			if	(:new.ie_fumante = 'S' ) then
				qt_ponto_w := qt_ponto_w + 1;
			end if;
			end;
		end if;

		if	( :new.qt_HDL >= 60 )  then
			qt_ponto_w := qt_ponto_w + (-1);
		elsif	( :new.qt_HDL> 50 ) and (:new.qt_HDL<= 59 ) then
			qt_ponto_w := qt_ponto_w + 0;
		elsif	( :new.qt_HDL > 40 ) and ( :new.qt_HDL <= 49 ) then
			qt_ponto_w := qt_ponto_w + 1;
		elsif	(:new.qt_HDL<= 40 ) then
			qt_ponto_w := qt_ponto_w + 2;
		end if;

		if	(:new.ie_trat_pas = 'S') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 5;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 6;
			end if;
			end;
		elsif	(:new.ie_trat_pas = 'N') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 4;
			end if;
			end;
		end if;

		:new.qt_pontuacao := qt_ponto_w;

		if	(qt_ponto_w <= 0 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w =1 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 2 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 3) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 4 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 5 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 6 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 7 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 8 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = 9 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 10 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 11 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 12 ) then
			qt_risco_w := '1';
		elsif	(qt_ponto_w = 13 ) then
			qt_risco_w := '2';
		elsif	(qt_ponto_w = 14 ) then
			qt_risco_w := '2';
		elsif	(qt_ponto_w = 15 ) then
			qt_risco_w := '3';
		elsif	(qt_ponto_w = 16 ) then
			qt_risco_w := '4';
		elsif	(qt_ponto_w = 17 ) then
			qt_risco_w :=  '5';
		elsif	(qt_ponto_w = 18 ) then
			qt_risco_w :=  '6';
		elsif	(qt_ponto_w = 19 ) then
			qt_risco_w :=  '8';
		elsif	(qt_ponto_w = 20 ) then
			qt_risco_w :=  '11';
		elsif	(qt_ponto_w = 21 ) then
			qt_risco_w :=  '14';
		elsif	(qt_ponto_w = 22 ) then
			qt_risco_w :=  '17';
		elsif	(qt_ponto_w = 23 ) then
			qt_risco_w :=  '22';
		elsif	(qt_ponto_w = 24 ) then
			qt_risco_w :=  '27';
		elsif	(qt_ponto_w >= 25 ) then
			qt_risco_w := '>=30';
		end if;
		end;
	end if;
else if	(:new.IE_ESC_ORIGINAL = 'S') then
	if	( ie_sexo_w = 'M' ) then
		Begin
		qt_ponto_w	:= 0;
		if	(:new.qt_idade >= 35) and (:new.qt_idade <= 39) then
			qt_ponto_w := 2;
		elsif	(:new.qt_idade >= 40) and (:new.qt_idade <= 44) then
			qt_ponto_w := 5;
		elsif	(:new.qt_idade >= 45) and (:new.qt_idade <= 49) then
			qt_ponto_w := 6;
		elsif	(:new.qt_idade >= 50) and (:new.qt_idade <= 54) then
			qt_ponto_w := 8;
		elsif	(:new.qt_idade >= 55) and (:new.qt_idade <= 59) then
			qt_ponto_w := 10;
		elsif	(:new.qt_idade >= 60) and (:new.qt_idade <= 64) then
			qt_ponto_w := 11;
		elsif	(:new.qt_idade >= 65) and (:new.qt_idade <= 69) then
			qt_ponto_w := 12;
		elsif	(:new.qt_idade >= 70) and (:new.qt_idade <= 74) then
			qt_ponto_w := 14;
		elsif	(:new.qt_idade > 75) then
			qt_ponto_w := 15;
		end if;

		If	( :new.qt_colesterol < 160 )  then
			qt_ponto_w := qt_ponto_w + 0;
		elsif	(  :new.qt_colesterol >= 160 ) and ( :new.qt_colesterol<= 199 ) then
			qt_ponto_w := qt_ponto_w + 1;
		elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
			qt_ponto_w := qt_ponto_w + 2;
		elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
			qt_ponto_w := qt_ponto_w + 3;
		elsif	( :new.qt_colesterol >= 280 ) then
			qt_ponto_w := qt_ponto_w + 4;
		end if;

		if	( :new.qt_HDL >= 60 )  then
			qt_ponto_w := qt_ponto_w + (-2);
		elsif	( :new.qt_HDL>= 50 ) and (:new.qt_HDL<= 59 ) then
			qt_ponto_w := qt_ponto_w + (-1);
		elsif	( :new.qt_HDL >= 45 ) and ( :new.qt_HDL <= 49 ) then
			qt_ponto_w := qt_ponto_w + 0;
		elsif	( :new.qt_HDL >= 35 ) and ( :new.qt_HDL <= 44 ) then
			qt_ponto_w := qt_ponto_w + 1;
		elsif	(:new.qt_HDL < 35 ) then
			qt_ponto_w := qt_ponto_w + 2;
		end if;
		if	(:new.ie_trat_pas = 'S') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 5;
			end if;
			end;
		elsif	(:new.ie_trat_pas = 'N') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w - 2;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 3;
			end if;
			end;
		end if;

		if	(:new.ie_fumante = 'S' ) then
			qt_ponto_w := qt_ponto_w + 4;
		end if;
		if	(:new.IE_DIABETES = 'S' ) then
			qt_ponto_w := qt_ponto_w + 3;
		end if;

		:new.qt_pontuacao := qt_ponto_w;

		if	(qt_ponto_w <= -3 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = -2 ) then
			qt_risco_w := '1.1';
		elsif	(qt_ponto_w = -1 ) then
			qt_risco_w := '1.4';
		elsif	(qt_ponto_w = 0 ) then
			qt_risco_w := '1.6';
		elsif	(qt_ponto_w = 1 ) then
			qt_risco_w := '1.9';
		elsif	(qt_ponto_w = 2 ) then
			qt_risco_w := '2.3';
		elsif	(qt_ponto_w = 3) then
			qt_risco_w := '2.8';
		elsif	(qt_ponto_w = 4 ) then
			qt_risco_w := '3.3';
		elsif	(qt_ponto_w = 5 ) then
			qt_risco_w := '3.9';
		elsif	(qt_ponto_w = 6 ) then
			qt_risco_w := '4.7';
		elsif	(qt_ponto_w = 7 ) then
			qt_risco_w := '5.6';
		elsif	(qt_ponto_w = 8 ) then
			qt_risco_w := '6.7';
		elsif	(qt_ponto_w = 9 ) then
			qt_risco_w := '7.9';
		elsif	(qt_ponto_w = 10 ) then
			qt_risco_w := '9.4';
		elsif	(qt_ponto_w = 11 ) then
			qt_risco_w := '11.2';
		elsif	(qt_ponto_w = 12 ) then
			qt_risco_w := '13.2';
		elsif	(qt_ponto_w = 13 ) then
			qt_risco_w := '15.6';
		elsif	(qt_ponto_w = 14 ) then
			qt_risco_w := '18.4';
		elsif	(qt_ponto_w = 15 ) then
			qt_risco_w := '21.6';
		elsif	(qt_ponto_w = 16 ) then
			qt_risco_w := '25.3';
		elsif	(qt_ponto_w = 17 ) then
			qt_risco_w :=  '29.4';
		elsif	(qt_ponto_w >= 18 ) then
			qt_risco_w :=  '>30';
		end if;
		end;
	end if;

	if	( ie_sexo_w = 'F' ) then
		begin

		if	(:new.qt_idade >= 35) and (:new.qt_idade <= 39) then
			qt_ponto_w := 2;
		elsif	(:new.qt_idade >= 40) and (:new.qt_idade <= 44) then
			qt_ponto_w := 4;
		elsif	(:new.qt_idade >= 45) and (:new.qt_idade <= 49) then
			qt_ponto_w := 5;
		elsif	(:new.qt_idade >= 50) and (:new.qt_idade <= 54) then
			qt_ponto_w := 7;
		elsif	(:new.qt_idade >= 55) and (:new.qt_idade <= 59) then
			qt_ponto_w := 8;
		elsif	(:new.qt_idade >= 60) and (:new.qt_idade <= 64) then
			qt_ponto_w := 9;
		elsif	(:new.qt_idade >= 65) and (:new.qt_idade <= 69) then
			qt_ponto_w := 10;
		elsif	(:new.qt_idade >= 70) and (:new.qt_idade <= 74) then
			qt_ponto_w := 11;
		elsif	(:new.qt_idade > 75) then
			qt_ponto_w := 12;
		end if;

		If	( :new.qt_colesterol < 160 )  then
			qt_ponto_w := qt_ponto_w + 0;
		elsif	(  :new.qt_colesterol >=160 ) and ( :new.qt_colesterol<= 199 ) then
			qt_ponto_w := qt_ponto_w + 1;
		elsif	( :new.qt_colesterol >= 200  ) and ( :new.qt_colesterol <= 239 ) then
			qt_ponto_w := qt_ponto_w + 3;
		elsif	( :new.qt_colesterol >= 240  ) and ( :new.qt_colesterol <= 279 ) then
			qt_ponto_w := qt_ponto_w + 4;
		elsif	( :new.qt_colesterol >= 280 ) then
			qt_ponto_w := qt_ponto_w + 5;
		end if;

		if	(:new.ie_fumante = 'S' ) then
			qt_ponto_w := qt_ponto_w + 3;
		end if;
		if	(:new.IE_DIABETES = 'S' ) then
			qt_ponto_w := qt_ponto_w + 4;
		end if;

		if	( :new.qt_HDL >= 60 )  then
			qt_ponto_w := qt_ponto_w + (-2);
		elsif	( :new.qt_HDL>= 50 ) and (:new.qt_HDL<= 59 ) then
			qt_ponto_w := qt_ponto_w + (-1);
		elsif	( :new.qt_HDL >= 45 ) and ( :new.qt_HDL <= 49 ) then
			qt_ponto_w := qt_ponto_w + 0;
		elsif	( :new.qt_HDL >= 35 ) and ( :new.qt_HDL <= 44 ) then
			qt_ponto_w := qt_ponto_w + 1;
		elsif	(:new.qt_HDL < 35 ) then
			qt_ponto_w := qt_ponto_w + 2;
		end if;

		if	(:new.ie_trat_pas = 'S') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w - 1;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 3;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 149) then
				qt_ponto_w := qt_ponto_w + 5;
			elsif	(:new.qt_pa_sistolica >= 150) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 6;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 7;
			end if;
			end;
		elsif	(:new.ie_trat_pas = 'N') then
			begin
			if	(:new.qt_pa_sistolica < 120) then
				qt_ponto_w := qt_ponto_w - 3;
			elsif	(:new.qt_pa_sistolica >= 120) and (:new.qt_pa_sistolica <= 129) then
				qt_ponto_w := qt_ponto_w + 0;
			elsif	(:new.qt_pa_sistolica >= 130) and (:new.qt_pa_sistolica <= 139) then
				qt_ponto_w := qt_ponto_w + 1;
			elsif	(:new.qt_pa_sistolica >= 140) and (:new.qt_pa_sistolica <= 149) then
				qt_ponto_w := qt_ponto_w + 2;
			elsif	(:new.qt_pa_sistolica >= 150) and (:new.qt_pa_sistolica <= 159) then
				qt_ponto_w := qt_ponto_w + 4;
			elsif	(:new.qt_pa_sistolica >= 160) then
				qt_ponto_w := qt_ponto_w + 5;
			end if;
			end;
		end if;

		:new.qt_pontuacao := qt_ponto_w;

		if	(qt_ponto_w <= -2 ) then
			qt_risco_w := '<1';
		elsif	(qt_ponto_w = -1 ) then
			qt_risco_w := '1.0';
		elsif	(qt_ponto_w = 0 ) then
			qt_risco_w := '1.2';
		elsif	(qt_ponto_w = 1 ) then
			qt_risco_w := '1.5';
		elsif	(qt_ponto_w = 2 ) then
			qt_risco_w := '1.7';
		elsif	(qt_ponto_w = 3) then
			qt_risco_w := '2.0';
		elsif	(qt_ponto_w = 4 ) then
			qt_risco_w := '2.4';
		elsif	(qt_ponto_w = 5 ) then
			qt_risco_w := '2.8';
		elsif	(qt_ponto_w = 6 ) then
			qt_risco_w := '3.3';
		elsif	(qt_ponto_w = 7 ) then
			qt_risco_w := '3.9';
		elsif	(qt_ponto_w = 8 ) then
			qt_risco_w := '4.5';
		elsif	(qt_ponto_w = 9 ) then
			qt_risco_w := '5.3';
		elsif	(qt_ponto_w = 10 ) then
			qt_risco_w := '6.3';
		elsif	(qt_ponto_w = 11 ) then
			qt_risco_w := '7.3';
		elsif	(qt_ponto_w = 12 ) then
			qt_risco_w := '8.6';
		elsif	(qt_ponto_w = 13 ) then
			qt_risco_w := '10.0';
		elsif	(qt_ponto_w = 14 ) then
			qt_risco_w := '11.7';
		elsif	(qt_ponto_w = 15 ) then
			qt_risco_w := '13.7';
		elsif	(qt_ponto_w = 16 ) then
			qt_risco_w := '15.9';
		elsif	(qt_ponto_w = 17 ) then
			qt_risco_w :=  '18.5';
		elsif	(qt_ponto_w = 18 ) then
			qt_risco_w :=  '21.5';
		elsif	(qt_ponto_w = 19 ) then
			qt_risco_w :=  '24.8';
		elsif	(qt_ponto_w = 20 ) then
			qt_risco_w :=  '28.5';
		elsif	(qt_ponto_w >= 21 ) then
			qt_risco_w :=  '>30';
		end if;
		end;
	end if;
end if;
end if;

:new.qt_risco := qt_risco_w;

<<Final>>
qt_reg_w	:= 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_FRAMINGHAM_delete
after delete ON TASY.ESCALA_FRAMINGHAM for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;


begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '13'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_FRAMINGHAM ADD (
  CONSTRAINT ESCFRAM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_FRAMINGHAM ADD (
  CONSTRAINT ESCFRAM_MEDATEN_FK 
 FOREIGN KEY (NR_ATEND_TASYMED) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO),
  CONSTRAINT ESCFRAM_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ESCFRAM_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCFRAM_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCFRAM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCFRAM_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCFRAM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCFRAM_MEDCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MED_CLIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_FRAMINGHAM TO NIVEL_1;

