ALTER TABLE TASY.EHR_TIPO_REGISTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_TIPO_REGISTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_TIPO_REGISTRO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_TIPO_REGISTRO       VARCHAR2(80 BYTE)      NOT NULL,
  DS_ABREVIACAO          VARCHAR2(20 BYTE)      NOT NULL,
  DS_COR                 VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_APRES           NUMBER(3)              NOT NULL,
  IE_STATUS_AGENDA_CONS  VARCHAR2(2 BYTE),
  IE_CLASSIF_AGENDA      VARCHAR2(5 BYTE)       DEFAULT null,
  CD_ESPECIALIDADE       NUMBER(10),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE),
  NR_SEQ_PROTOCOL_EXT    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHRTIRE_ESPMEDI_FK_I ON TASY.EHR_TIPO_REGISTRO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRTIRE_PK ON TASY.EHR_TIPO_REGISTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EHRTIRE_UK ON TASY.EHR_TIPO_REGISTRO
(IE_STATUS_AGENDA_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.EHR_TIPO_REGISTRO_tp  after update ON TASY.EHR_TIPO_REGISTRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PROTOCOL_EXT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PROTOCOL_EXT,1,4000),substr(:new.NR_SEQ_PROTOCOL_EXT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROTOCOL_EXT',ie_log_w,ds_w,'EHR_TIPO_REGISTRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.EHR_TIPO_REGISTRO ADD (
  CONSTRAINT EHRTIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT EHRTIRE_UK
 UNIQUE (IE_STATUS_AGENDA_CONS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_TIPO_REGISTRO ADD (
  CONSTRAINT EHRTIRE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.EHR_TIPO_REGISTRO TO NIVEL_1;

