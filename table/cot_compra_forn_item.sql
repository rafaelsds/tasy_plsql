ALTER TABLE TASY.COT_COMPRA_FORN_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_COMPRA_FORN_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_COMPRA_FORN_ITEM
(
  NR_COT_COMPRA             NUMBER(10)          NOT NULL,
  NR_ITEM_COT_COMPRA        NUMBER(5)           NOT NULL,
  CD_CGC_FORNECEDOR         VARCHAR2(14 BYTE),
  QT_MATERIAL               NUMBER(13,4)        NOT NULL,
  VL_UNITARIO_MATERIAL      NUMBER(13,4),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  PR_DESCONTO               NUMBER(13,4),
  VL_FRETE_RATEIO           NUMBER(15,2),
  VL_PRECO_LIQUIDO          NUMBER(15,2),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  NR_ORDEM_COMPRA           NUMBER(10),
  VL_PRESENTE               NUMBER(15,2),
  DS_MARCA                  VARCHAR2(80 BYTE)   DEFAULT null,
  QT_PRIORIDADE             NUMBER(5),
  VL_UNID_FORNEC            NUMBER(15,2),
  QT_CONV_UNID_FORNEC       NUMBER(9,4),
  DS_MATERIAL_DIRETO        VARCHAR2(255 BYTE),
  IE_SITUACAO               VARCHAR2(1 BYTE),
  DT_VALIDADE               DATE,
  NR_SEQUENCIA              NUMBER(10),
  NR_SEQ_COT_FORN           NUMBER(10)          NOT NULL,
  CD_MOTIVO_FALTA_PRECO     VARCHAR2(3 BYTE),
  VL_PRECO                  NUMBER(17,4),
  CD_MATERIAL               NUMBER(10),
  VL_UNITARIO_LIQUIDO       NUMBER(15,2),
  VL_DESCONTO               NUMBER(13,2),
  VL_UNITARIO_INICIAL       NUMBER(13,4),
  IE_PRECO_FORNEC           VARCHAR2(1 BYTE),
  NR_CONTRATO               NUMBER(10),
  DS_MARCA_FORNEC           VARCHAR2(30 BYTE),
  NR_ID_INTEGRACAO          VARCHAR2(255 BYTE),
  DS_MOTIVO_DESCONSIDERAR   VARCHAR2(255 BYTE),
  IE_CONSIDERA_MEDIA_LIC    VARCHAR2(1 BYTE),
  VL_TOTAL_LIQUIDO_ITEM     NUMBER(13,4),
  DT_LIBERACAO              DATE,
  NM_USUARIO_LIB            VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_LIB      VARCHAR2(255 BYTE),
  VL_UNITARIO_REF_MOEDA     NUMBER(15,4),
  NR_SEQ_REGRA_CONTRATO     NUMBER(10),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_PRECO_PJ           NUMBER(10),
  NR_SEQ_STATUS_MARCA       NUMBER(10),
  DT_STATUS_MARCA           DATE,
  DS_JUSTIF_ESCOLHA_FORNEC  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          96M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COCOFOI_COCOFOR_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_SEQ_COT_FORN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOFOI_ORDCOMP_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOFOI_PESJURI_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          35M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOFOI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COCOFOI_PK ON TASY.COT_COMPRA_FORN_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFI_CONRENF_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_SEQ_REGRA_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFI_CONRENF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFI_CONTRAT_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFI_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFI_CONTRAT_FK2_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFI_CONTRAT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFI_COTCOIT_FK1_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_COT_COMPRA, NR_ITEM_COT_COMPRA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFI_COTCOM_FK ON TASY.COT_COMPRA_FORN_ITEM
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFI_MATERIA_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFI_SEQCOT_FK ON TASY.COT_COMPRA_FORN_ITEM
(NR_SEQUENCIA, NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOFI_STAVAMA_FK_I ON TASY.COT_COMPRA_FORN_ITEM
(NR_SEQ_STATUS_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFI_STAVAMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOFI_VALOR_I ON TASY.COT_COMPRA_FORN_ITEM
(VL_UNITARIO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          15M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOFI_VALOR_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cot_compra_forn_item_update
BEFORE UPDATE ON TASY.COT_COMPRA_FORN_ITEM FOR EACH ROW
DECLARE

ie_situacao_w				varchar2(1);
ds_historico_w				varchar2(255);

BEGIN

if	(:new.vl_unitario_material <> :old.vl_unitario_material) then
	select	nvl(ie_situacao,'A')
	into	ie_situacao_w
	from	cot_compra_item
	where	nr_cot_compra = :new.nr_cot_compra
	and	nr_item_cot_compra = :new.nr_item_cot_compra;

	ds_historico_w	:= 	WHEB_MENSAGEM_PCK.get_texto(298865,'NR_ITEM_COT_COMPRA_W='||:new.nr_item_cot_compra||
							';NR_SEQUENCIA_W='||:new.nr_sequencia||
							';NR_SEQ_COT_FORN_W='||:new.nr_seq_cot_forn||
							';VL_OLD_UNIT_MAT_W='||campo_mascara(:old.vl_unitario_material,4)||
							';VL_NEW_UNIT_MAT_W='||campo_mascara(:new.vl_unitario_material,4));

	gerar_hist_cotacao_sem_commit(:new.nr_cot_compra, WHEB_MENSAGEM_PCK.get_texto(298885), ds_historico_w, 'S', :new.nm_usuario);


	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(265963);
		--'Este item est� inativo na pasta Itens cota��o, portanto n�o pode ser alterado.'
	end if;
end if;

if	(:new.qt_material <> :old.qt_material) then

	ds_historico_w	:= 	WHEB_MENSAGEM_PCK.get_texto(298887,'NR_ITEM_COT_COMPRA_W='||:new.nr_item_cot_compra||
							';NR_SEQUENCIA_W='||:new.nr_sequencia||
							';NR_SEQ_COT_FORN_W='||:new.nr_seq_cot_forn||
							';QT_MAT_OLD_W='||campo_mascara(:old.qt_material,4)||
							';QT_MAT_NEW_W='||campo_mascara(:new.qt_material,4));

	gerar_hist_cotacao_sem_commit(:new.nr_cot_compra, WHEB_MENSAGEM_PCK.get_texto(298888), ds_historico_w, 'S', :new.nm_usuario);
end if;

if	(:new.cd_material <> :old.cd_material) then

	ds_historico_w	:= 	WHEB_MENSAGEM_PCK.get_texto(298895,'NR_ITEM_COT_COMPRA_W='||:new.nr_item_cot_compra||
						';NR_SEQUENCIA_W='||:new.nr_sequencia||
						';NR_SEQ_COT_FORN_W='||:new.nr_seq_cot_forn||
						';NM_USUARIO_W='||:new.nm_usuario||
						';CD_MATERIAL_OLD='||:old.cd_material||
						';CD_MATERIAL_NEW='||:new.cd_material);

	gerar_hist_cotacao_sem_commit(:new.nr_cot_compra, WHEB_MENSAGEM_PCK.get_texto(298902), ds_historico_w, 'S', :new.nm_usuario);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.cot_compra_forn_item_after
AFTER INSERT OR UPDATE ON TASY.COT_COMPRA_FORN_ITEM FOR EACH ROW
DECLARE

nr_seq_autor_cir_w		cot_compra.nr_seq_autor_cir%type;
cd_tributo_tributo_w	tributo.cd_tributo%type;
pr_aliquota_tributo_w	tributo_conta_pagar.pr_aliquota%type;
ie_param_w				varchar2(1);
qt_registro_w			number(5);

/* Ha tributos ATIVOS e com incidencia no ITEM que NAO possuem
   tributo_conta_pagar, para estes NAO serao gerados tributos
   por NAO haver al�quota cadastrada */
Cursor C02 is
	SELECT	a.cd_tributo,
		b.pr_aliquota
	FROM	tributo a,
		tributo_conta_pagar b
	WHERE	a.IE_SITUACAO = 'A'
	AND	a.IE_CORPO_ITEM = 'I'
	AND	a.cd_tributo = b.cd_tributo
	and	nvl(b.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento
	and	sysdate between nvl(b.dt_inicio_vigencia, sysdate) and nvl(fim_dia(b.dt_fim_vigencia), sysdate)
	and     obter_se_estrutura_mat(b.cd_grupo_material, b.cd_subgrupo_material, b.cd_classe_material, b.cd_material, :new.cd_material, 'S') = 'S'
	order by b.nr_sequencia;
BEGIN

obter_param_usuario(915, 189, wheb_usuario_pck.get_cd_perfil, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_param_w);
if	(ie_param_w = 'S') then
open C02;
loop
fetch C02 into
	cd_tributo_tributo_w,
	pr_aliquota_tributo_w;
exit when C02%notfound;
	begin

	select	count(1)
	into	qt_registro_w
	from	cot_compra_forn_item_tr
	where	nr_seq_cot_item_forn = :new.nr_sequencia
	and	nr_cot_compra = :new.nr_cot_compra
	and	cd_tributo = cd_tributo_tributo_w;

	if	(:new.vl_unitario_material > 0) then
		if	(qt_registro_w = 0) then
			insert into cot_compra_forn_item_tr(
				nr_cot_compra,
				nr_item_cot_compra,
				cd_cgc_fornecedor,
				cd_tributo,
				pr_tributo,
				vl_tributo,
				dt_atualizacao,
				nm_usuario,
				ds_observacao,
				nr_sequencia,
				nr_seq_cot_item_forn,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(
				:new.nr_cot_compra,
				:new.nr_item_cot_compra,
				:new.cd_cgc_fornecedor,
				cd_tributo_tributo_w,
				pr_aliquota_tributo_w,
				dividir(pr_aliquota_tributo_w, 100) * (:new.qt_material * :new.vl_unitario_material),
				sysdate,
				:new.nm_usuario,
				null,
				cot_compra_forn_item_tr_seq.nextval,
				:new.nr_sequencia,
				sysdate,
				:new.nm_usuario);
		else

			UPDATE cot_compra_forn_item_tr
			SET	   pr_tributo = pr_aliquota_tributo_w,
				   vl_tributo = dividir( pr_aliquota_tributo_w , 100) * (:new.qt_material * :new.vl_unitario_material)
			WHERE  nr_cot_compra      = :new.nr_cot_compra
			AND	   nr_item_cot_compra = :new.nr_item_cot_compra
			AND	   cd_tributo		  = cd_tributo_tributo_w;
		end if;

	else
		delete	cot_compra_forn_item_tr
		where	nr_seq_cot_item_forn = :new.nr_sequencia
		and	nr_cot_compra = :new.nr_cot_compra
		and	cd_tributo = cd_tributo_tributo_w;

	end if;

	end;
end loop;
close C02;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.COT_COMPRA_FORN_ITEM_tp  after update ON TASY.COT_COMPRA_FORN_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_MARCA,1,4000),substr(:new.DS_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MARCA',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA_LIB,1,4000),substr(:new.DS_JUSTIFICATIVA_LIB,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA_LIB',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_FORNECEDOR,1,4000),substr(:new.CD_CGC_FORNECEDOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_FORNECEDOR',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MATERIAL,1,4000),substr(:new.QT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_MATERIAL',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_UNITARIO_MATERIAL,1,4000),substr(:new.VL_UNITARIO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_UNITARIO_MATERIAL',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESCONTO,1,4000),substr(:new.PR_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESCONTO',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_COT_COMPRA,1,4000),substr(:new.NR_ITEM_COT_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_COT_COMPRA',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MATERIAL_DIRETO,1,4000),substr(:new.DS_MATERIAL_DIRETO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL_DIRETO',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COT_FORN,1,4000),substr(:new.NR_SEQ_COT_FORN,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COT_FORN',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MARCA_FORNEC,1,4000),substr(:new.DS_MARCA_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'DS_MARCA_FORNEC',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIB,1,4000),substr(:new.NM_USUARIO_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIB',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_COT_COMPRA,1,4000),substr(:new.NR_COT_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_COT_COMPRA',ie_log_w,ds_w,'COT_COMPRA_FORN_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.cot_compra_forn_item_atual
BEFORE INSERT OR UPDATE ON TASY.COT_COMPRA_FORN_ITEM FOR EACH ROW
DECLARE

cd_tributo_w			number(5);
pr_tributo_w			number(7,4);
qt_registro_w			number(5);
vl_tributo_w			number(15,2);
nr_seq_autor_cir_w		cot_compra.nr_seq_autor_cir%type;
vl_total_w			cot_compra_forn_item.vl_unitario_material%type;
ie_param_190_w			varchar2(15);

cursor C01 IS
	select	cd_tributo,
		pr_tributo
	from	cot_compra_forn_item_tr
	where	nr_seq_cot_item_forn = :new.nr_sequencia
	and	nvl(pr_tributo,0) > 0;

BEGIN

obter_param_usuario(915, 190, wheb_usuario_pck.get_cd_perfil, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_param_190_w);

/*Para atualizar o valor unitario inicial da cota��o*/
if	(:new.vl_unitario_material is not null) and
	(:new.vl_unitario_inicial is null) and
	(nvl(:new.vl_unitario_material,0) <> nvl(:new.vl_unitario_inicial,0)) then
	:new.vl_unitario_inicial	:= :new.vl_unitario_material;
end if;

if	(:new.vl_unitario_material > 0) and
	(:new.qt_material > 0 ) then
	begin
	vl_total_w := :new.vl_unitario_material * :new.qt_material;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(512549);
		/*O sistema n�o conseguiu salvar o registro porque a multiplica��o da quantidade do item com o valor unit�rio informado est� com valor muito alto. Verifique se voc� est� informando uma quantidade ou um valor correto para esse item.*/
	end;
end if;


begin
select	nr_seq_autor_cir
into	nr_seq_autor_cir_w
from	cot_compra
where	nr_cot_compra = :new.nr_cot_compra;
exception
when others then
	nr_seq_autor_cir_w := null;
end;

select	count(*)
into	qt_registro_w
from	cot_compra_forn_item_tr
where	nr_seq_cot_item_forn = :new.nr_sequencia;
if	(qt_registro_w > 0) then
	begin
	OPEN C01;
	LOOP
	FETCH C01 INTO
		cd_tributo_w,
		pr_tributo_w;
	EXIT WHEN C01%NOTFOUND;
		begin

		if	(ie_param_190_w = 'S') then
			vl_tributo_w	:= dividir(pr_tributo_w, 100) * ((:new.qt_material * :new.vl_unitario_material) + nvl(:new.VL_FRETE_RATEIO,0));
		else
			vl_tributo_w	:= dividir(pr_tributo_w, 100) * (:new.qt_material * :new.vl_unitario_material);
		end if;

		update	cot_compra_forn_item_tr
		set	vl_tributo	= vl_tributo_w
		where	nr_seq_cot_item_forn = :new.nr_sequencia
		and	cd_tributo	= cd_tributo_w;
		end;
	END LOOP;
	CLOSE C01;
	end;
end if;

/*Essa mensagem � apenas para o portal de compras web, por isso tem o IF do CNPJ = Usuario
Serve para os casos em que o fornecedor est� conseguindo alterar o material, e n�s nao conseguimos reproduzir.
Essa trigger vai bloquear esses casos*/

if	(updating) and
	(nvl(nr_seq_autor_cir_w,0) = 0) and
	(:new.nm_usuario = :new.cd_cgc_fornecedor) and
	(:new.cd_material <> :old.cd_material) then
	wheb_mensagem_pck.exibir_mensagem_abort(438811); /*N�o � permitido alterar o material.*/
end if;


:new.nr_seq_status_marca	:= obter_dados_marca_material(:new.ds_marca, :new.cd_material,'C');
:new.dt_status_marca		:= obter_data_marca_material(:new.ds_marca, :new.cd_material);




END;
/


ALTER TABLE TASY.COT_COMPRA_FORN_ITEM ADD (
  CONSTRAINT COCOFOI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          18M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_COMPRA_FORN_ITEM ADD (
  CONSTRAINT COTCOFI_STAVAMA_FK 
 FOREIGN KEY (NR_SEQ_STATUS_MARCA) 
 REFERENCES TASY.STATUS_AVALIACAO_MARCA (NR_SEQUENCIA),
  CONSTRAINT COTCOFI_COTCOIT_FK1 
 FOREIGN KEY (NR_COT_COMPRA, NR_ITEM_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA_ITEM (NR_COT_COMPRA,NR_ITEM_COT_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT COTCOFI_CONRENF_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTRATO) 
 REFERENCES TASY.CONTRATO_REGRA_NF (NR_SEQUENCIA),
  CONSTRAINT COTCOFI_CONTRAT_FK2 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT COCOFOI_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT COCOFOI_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT COCOFOI_COCOFOR_FK 
 FOREIGN KEY (NR_SEQ_COT_FORN) 
 REFERENCES TASY.COT_COMPRA_FORN (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COTCOFI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT COTCOFI_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COT_COMPRA_FORN_ITEM TO NIVEL_1;

