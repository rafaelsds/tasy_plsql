ALTER TABLE TASY.ESCALA_MIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MIF
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  QT_ALIMENTACAO             NUMBER(3),
  QT_HIGIENE_PESSOAL         NUMBER(3),
  QT_VEST_TRONC_SUP          NUMBER(3),
  QT_BANHO                   NUMBER(3),
  QT_VEST_TRONC_INF          NUMBER(3),
  QT_CONTROLE_INTESTINAL     NUMBER(3),
  QT_SANITARIO               NUMBER(3),
  QT_TRANS_SANITARIO         NUMBER(3),
  QT_TRANS_LEITO             NUMBER(3),
  QT_CONTROLE_VESICAL        NUMBER(3),
  QT_LOC_CADEIRA             NUMBER(3),
  QT_TRANS_BANHEIRO          NUMBER(3),
  QT_ESCADA                  NUMBER(3),
  QT_COMP_AUDICAO            NUMBER(3),
  QT_EXPRESSAO               NUMBER(3),
  QT_PROBLEMA                NUMBER(3),
  QT_INTERACAO_SOCIAL        NUMBER(3),
  QT_MEMORIA                 NUMBER(3),
  QT_PONTUACAO               NUMBER(5),
  NR_HORA                    NUMBER(2),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAMIF_ATEPACI_FK_I ON TASY.ESCALA_MIF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAMIF_EHRREEL_FK_I ON TASY.ESCALA_MIF
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMIF_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAMIF_PESFISI_FK_I ON TASY.ESCALA_MIF
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAMIF_PK ON TASY.ESCALA_MIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMIF_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCAMIF_TASASDI_FK_I ON TASY.ESCALA_MIF
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMIF_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAMIF_TASASDI_FK2_I ON TASY.ESCALA_MIF
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMIF_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Escala_Mif_atual
BEFORE INSERT or UPDATE ON TASY.ESCALA_MIF FOR EACH ROW
declare
qt_reg_w	number(1);

BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:NEW.QT_PONTUACAO	:= 	NVL(:NEW.QT_ALIMENTACAO,0) + NVL(:NEW.QT_HIGIENE_PESSOAL,0) + NVL(:NEW.QT_BANHO,0) +
						NVL(:NEW.QT_VEST_TRONC_SUP,0) + NVL(:NEW.QT_VEST_TRONC_INF,0) + NVL(:NEW.QT_SANITARIO,0) +
						NVL(:NEW.QT_CONTROLE_VESICAL,0) + NVL(:NEW.QT_CONTROLE_INTESTINAL,0) + NVL(:NEW.QT_TRANS_LEITO,0) +
						NVL(:NEW.QT_TRANS_SANITARIO,0) + NVL(:NEW.QT_TRANS_BANHEIRO,0) + NVL(:NEW.QT_LOC_CADEIRA,0) +
						NVL(:NEW.QT_ESCADA,0) + NVL(:NEW.QT_COMP_AUDICAO,0) + NVL(:NEW.QT_EXPRESSAO,0) +
						NVL(:NEW.QT_PROBLEMA,0) + NVL(:NEW.QT_INTERACAO_SOCIAL,0) + NVL(:NEW.QT_MEMORIA,0);

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.escala_mif_pend_atual
after insert or update ON TASY.ESCALA_MIF for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'70');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_MIF_delete
after delete ON TASY.ESCALA_MIF for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '70'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_MIF ADD (
  CONSTRAINT ESCAMIF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MIF ADD (
  CONSTRAINT ESCAMIF_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCAMIF_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAMIF_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAMIF_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA),
  CONSTRAINT ESCAMIF_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_MIF TO NIVEL_1;

