ALTER TABLE TASY.PLS_CP_CTA_COMBINADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_COMBINADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_COMBINADA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NM_REGRA                VARCHAR2(200 BYTE)    NOT NULL,
  NR_SEQ_REGRA_AGRUP      NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FIM_VIGENCIA         DATE,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_PRESTADOR       VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_REEMBOLSO       VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_FATURA_POS      VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_COPARTICIPACAO  VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_INTERCAMBIO     VARCHAR2(1 BYTE)      NOT NULL,
  IE_TIPO_REGRA           VARCHAR2(5 BYTE)      NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  NR_ORDEM_EXECUCAO       NUMBER(20)            NOT NULL,
  IE_ORDENACAO_SUGERIDA   VARCHAR2(1 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCPCC_I1 ON TASY.PLS_CP_CTA_COMBINADA
(IE_DEST_PRESTADOR, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPCC_I2 ON TASY.PLS_CP_CTA_COMBINADA
(IE_DEST_REEMBOLSO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPCC_I3 ON TASY.PLS_CP_CTA_COMBINADA
(IE_DEST_COPARTICIPACAO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPCC_I4 ON TASY.PLS_CP_CTA_COMBINADA
(IE_DEST_INTERCAMBIO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCPCC_PK ON TASY.PLS_CP_CTA_COMBINADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPCC_PLCPCAG_FK_I ON TASY.PLS_CP_CTA_COMBINADA
(NR_SEQ_REGRA_AGRUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_combinada_atual
before insert or update ON TASY.PLS_CP_CTA_COMBINADA for each row
declare

qt_registro_w	pls_integer;

cursor c01(	nr_seq_cp_combinada_pc	pls_cp_cta_combinada.nr_sequencia%type) is
	select	nr_sequencia
	from	pls_cp_cta_filtro
	where	nr_seq_cp_combinada = nr_seq_cp_combinada_pc;

begin

-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');


-- percorre todos os filtros filhos
for r_c01_w in c01(:new.nr_sequencia) loop

	case (:new.ie_tipo_regra)
		when 'P' then -- procedimento

			update 	pls_cp_cta_filtro set
				ie_filtro_participante = 'N',
				ie_filtro_servico = 'N',
				ie_filtro_material = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;

		when 'PP' then -- participante do procedimento

			update 	pls_cp_cta_filtro set
				ie_filtro_servico = 'N',
				ie_filtro_material = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;

		when 'S' then -- servi�o

			update 	pls_cp_cta_filtro set
				ie_filtro_procedimento = 'N',
				ie_filtro_participante = 'N',
				ie_filtro_material = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;

		when 'M' then -- material

			update 	pls_cp_cta_filtro set
				ie_filtro_procedimento = 'N',
				ie_filtro_participante = 'N',
				ie_filtro_servico = 'N'
			where	nr_sequencia = r_c01_w.nr_sequencia;
		else
			null;
	end case;
end loop;

end pls_cp_cta_combinada_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_COMBINADA_tp  after update ON TASY.PLS_CP_CTA_COMBINADA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDENACAO_SUGERIDA,1,4000),substr(:new.IE_ORDENACAO_SUGERIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_SUGERIDA',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_REGRA,1,4000),substr(:new.NM_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_REGRA',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEST_FATURA_POS,1,4000),substr(:new.IE_DEST_FATURA_POS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEST_FATURA_POS',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEST_COPARTICIPACAO,1,4000),substr(:new.IE_DEST_COPARTICIPACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEST_COPARTICIPACAO',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEST_INTERCAMBIO,1,4000),substr(:new.IE_DEST_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEST_INTERCAMBIO',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEST_PRESTADOR,1,4000),substr(:new.IE_DEST_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEST_PRESTADOR',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEST_REEMBOLSO,1,4000),substr(:new.IE_DEST_REEMBOLSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEST_REEMBOLSO',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_REGRA,1,4000),substr(:new.IE_TIPO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REGRA',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_EXECUCAO,1,4000),substr(:new.NR_ORDEM_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_EXECUCAO',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA_REF,1,4000),substr(:new.DT_INICIO_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA_REF,1,4000),substr(:new.DT_FIM_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_AGRUP,1,4000),substr(:new.NR_SEQ_REGRA_AGRUP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_AGRUP',ie_log_w,ds_w,'PLS_CP_CTA_COMBINADA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_COMBINADA ADD (
  CONSTRAINT PLSCPCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_COMBINADA ADD (
  CONSTRAINT PLSCPCC_PLCPCAG_FK 
 FOREIGN KEY (NR_SEQ_REGRA_AGRUP) 
 REFERENCES TASY.PLS_CP_CTA_AGRUP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_COMBINADA TO NIVEL_1;

