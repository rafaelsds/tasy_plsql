ALTER TABLE TASY.PLS_GLOSA_REG_NEG_AUT_ORIG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GLOSA_REG_NEG_AUT_ORIG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GLOSA_REG_NEG_AUT_ORIG
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_REGRA_NEG_AUT_ORIG  NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_GLOSA        NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSGRNA_PK ON TASY.PLS_GLOSA_REG_NEG_AUT_ORIG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGRNA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSGRNA_PLSRNAO_FK_I ON TASY.PLS_GLOSA_REG_NEG_AUT_ORIG
(NR_SEQ_REGRA_NEG_AUT_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGRNA_PLSRNAO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSGRNA_TISSMGL_FK_I ON TASY.PLS_GLOSA_REG_NEG_AUT_ORIG
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSGRNA_TISSMGL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_GLOSA_REG_NEG_AUT_ORIG_tp  after update ON TASY.PLS_GLOSA_REG_NEG_AUT_ORIG FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_GLOSA,1,4000),substr(:new.NR_SEQ_MOTIVO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_GLOSA',ie_log_w,ds_w,'PLS_GLOSA_REG_NEG_AUT_ORIG',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_GLOSA_REG_NEG_AUT_ORIG ADD (
  CONSTRAINT PLSGRNA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GLOSA_REG_NEG_AUT_ORIG ADD (
  CONSTRAINT PLSGRNA_PLSRNAO_FK 
 FOREIGN KEY (NR_SEQ_REGRA_NEG_AUT_ORIG) 
 REFERENCES TASY.PLS_REGRA_NEG_AUT_ORIGEM (NR_SEQUENCIA),
  CONSTRAINT PLSGRNA_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_GLOSA_REG_NEG_AUT_ORIG TO NIVEL_1;

