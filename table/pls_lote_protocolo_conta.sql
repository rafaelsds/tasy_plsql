ALTER TABLE TASY.PLS_LOTE_PROTOCOLO_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_PROTOCOLO_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_PROTOCOLO_CONTA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_LOTE                     DATE              NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR            NUMBER(10),
  IE_TIPO_LOTE                VARCHAR2(10 BYTE),
  NR_SEQ_CONGENERE            NUMBER(10),
  DT_GERACAO_ANALISE          DATE,
  IE_ORIGEM_ANALISE           NUMBER(10),
  NR_SEQ_PRESTADOR_WEB        NUMBER(10),
  DT_CONFIRMACAO              DATE,
  IE_STATUS                   VARCHAR2(2 BYTE),
  NR_SEQ_MOTIVO_CANCEL        NUMBER(10),
  DS_MOTIVO_CANCELAMENTO      VARCHAR2(255 BYTE),
  DS_MENSAGEM_ERRO            VARCHAR2(2000 BYTE),
  DT_GA_GERAR_HONOR_INICIO    DATE,
  DT_GA_GERAR_HONOR_FIM       DATE,
  DT_GA_CONSISTE_CTA_INICIO   DATE,
  DT_GA_CONSISTE_CTA_FIM      DATE,
  DT_GA_OCORRENCIA_INICIO     DATE,
  DT_GA_OCORRENCIA_FIM        DATE,
  DT_GA_FLUXO_ANALISE_INICIO  DATE,
  DT_GA_FLUXO_ANALISE_FIM     DATE,
  DT_GA_ATUAL_PROT_INICIO     DATE,
  DT_GA_ATUAL_PROT_FIM        DATE,
  NR_PROTOCOLO_PRESTADOR      VARCHAR2(20 BYTE),
  IE_REGRA_AGRUP_GUIA         VARCHAR2(1 BYTE),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  IE_ARQUIVO_FARMACIA         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOPC_ESTABEL_FK_I ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOPC_I1 ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(IE_STATUS, DT_GERACAO_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSLOPC_PK ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOPC_PLSCONG_FK_I ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPC_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOPC_PLSMOCL_FK_I ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPC_PLSMOCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOPC_PLSPRES_FK_I ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOPC_PLSUSWE_FK_I ON TASY.PLS_LOTE_PROTOCOLO_CONTA
(NR_SEQ_PRESTADOR_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOPC_PLSUSWE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_lote_prot_conta_alt
before update ON TASY.PLS_LOTE_PROTOCOLO_CONTA for each row
declare
qt_rejeitado_w	pls_integer;

begin

select	count(1)
into	qt_rejeitado_w
from	pls_protocolo_conta
where	nr_seq_lote_conta = :new.nr_sequencia
and	ie_situacao	= 'RE';

if	(qt_rejeitado_w > 0) and
	(:new.dt_confirmacao is not null) and
	(:old.dt_confirmacao is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(273280);
end if;

end;
/


ALTER TABLE TASY.PLS_LOTE_PROTOCOLO_CONTA ADD (
  CONSTRAINT PLSLOPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_PROTOCOLO_CONTA ADD (
  CONSTRAINT PLSLOPC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOPC_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSLOPC_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSLOPC_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_WEB) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSLOPC_PLSMOCL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.PLS_MOTIVO_CANCEL_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_PROTOCOLO_CONTA TO NIVEL_1;

