ALTER TABLE TASY.GPI_LOG_DATA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_LOG_DATA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_LOG_DATA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROJ_GPI      NUMBER(10),
  NR_SEQ_CRONOGRAMA    NUMBER(10),
  DS_MOTIVO            VARCHAR2(255 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_INICIO_PREV_ANT   DATE,
  DT_FIM_PREV_ANT      DATE,
  DT_INICIO_PREV_ATU   DATE,
  DT_FIM_PREV_ATU      DATE,
  NR_SEQ_CRON_ETAPA    NUMBER(10),
  IE_OPERACAO          VARCHAR2(1 BYTE),
  IE_IMPACTO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.GPILOGDAT_PK ON TASY.GPI_LOG_DATA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPILOGDAT_PK
  MONITORING USAGE;


ALTER TABLE TASY.GPI_LOG_DATA ADD (
  CONSTRAINT GPILOGDAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.GPI_LOG_DATA TO NIVEL_1;

