ALTER TABLE TASY.ES_NGTB_ERROINTEGR_ERIN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ES_NGTB_ERROINTEGR_ERIN CASCADE CONSTRAINTS;

CREATE TABLE TASY.ES_NGTB_ERROINTEGR_ERIN
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  ID_ERIN_CD_SEQUENCIAL  NUMBER(10)             NOT NULL,
  ERIN_DS_CODIGOCLIENTE  VARCHAR2(17 BYTE)      NOT NULL,
  ERIN_DS_SISTEMAORIGEM  VARCHAR2(14 BYTE)      NOT NULL,
  ERIN_DS_CPFCNPJ        VARCHAR2(14 BYTE)      NOT NULL,
  ERIN_DS_NUMEROFATURA   VARCHAR2(20 BYTE)      NOT NULL,
  ERIN_DH_STATUS         DATE                   NOT NULL,
  ERIN_DS_MSGERRO        VARCHAR2(500 BYTE),
  ERIN_DS_CODOPERACAO    VARCHAR2(2 BYTE),
  ERIN_IN_STATUS         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ESERRIN_PK ON TASY.ES_NGTB_ERROINTEGR_ERIN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ES_NGTB_ERROINTEGR_UPDATE
before update ON TASY.ES_NGTB_ERROINTEGR_ERIN for each row
declare

ds_log_w	varchar2(4000)	:= '';

begin

if	(:old.erin_dh_status <> :new.erin_dh_status) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_dh_status' || ';' || 'VL_ANTIGO=' || to_char(:old.erin_dh_status,'dd/mm/yyyy hh24:mi:ss') || ';' || 'VL_NOVO=' || to_char(:new.erin_dh_status,'dd/mm/yyyy hh24:mi:ss')) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_codigocliente <> :new.erin_ds_codigocliente) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_codigocliente' || ';' || 'VL_ANTIGO=' || :old.erin_ds_codigocliente || ';' || 'VL_NOVO=' || :new.erin_ds_codigocliente) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_codoperacao <> :new.erin_ds_codoperacao) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_codoperacao' || ';' || 'VL_ANTIGO=' || :old.erin_ds_codoperacao || ';' || 'VL_NOVO=' || :new.erin_ds_codoperacao) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_cpfcnpj <> :new.erin_ds_cpfcnpj) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_cpfcnpj' || ';' || 'VL_ANTIGO=' || :old.erin_ds_cpfcnpj || ';' || 'VL_NOVO=' || :new.erin_ds_cpfcnpj) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_msgerro <> :new.erin_ds_msgerro) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_msgerro' || ';' || 'VL_ANTIGO=' || :old.erin_ds_msgerro || ';' || 'VL_NOVO=' || :new.erin_ds_msgerro) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_numerofatura <> :new.erin_ds_numerofatura) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_numerofatura' || ';' || 'VL_ANTIGO=' || :old.erin_ds_numerofatura || ';' || 'VL_NOVO=' || :new.erin_ds_numerofatura) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_ds_sistemaorigem <> :new.erin_ds_sistemaorigem) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_ds_sistemaorigem' || ';' || 'VL_ANTIGO=' || :old.erin_ds_sistemaorigem || ';' || 'VL_NOVO=' || :new.erin_ds_sistemaorigem) || chr(13) || chr(10),1,4000);
end if;

if	(:old.erin_in_status <> :new.erin_in_status) then
	ds_log_w	:= substr(ds_log_w || wheb_mensagem_pck.get_texto(293779,'DS_CAMPO=erin_in_status' || ';' || 'VL_ANTIGO=' || :old.erin_in_status || ';' || 'VL_NOVO=' || :new.erin_in_status) || chr(13) || chr(10),1,4000);
end if;

GERAR_CRM_COBRANCA_LOG(ds_log_w,'ES_NGTB_ERROINTEGR_ERIN',:new.nm_usuario);

end;
/


ALTER TABLE TASY.ES_NGTB_ERROINTEGR_ERIN ADD (
  CONSTRAINT ESERRIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ES_NGTB_ERROINTEGR_ERIN TO NIVEL_1;

