ALTER TABLE TASY.COM_FAQ_SUBCAT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_FAQ_SUBCAT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_FAQ_SUBCAT_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SUBCATEGORIA  NUMBER(10)               NOT NULL,
  DS_ITEM              VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMFSCI_COMFSCA_FK_I ON TASY.COM_FAQ_SUBCAT_ITEM
(NR_SEQ_SUBCATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMFSCI_COMFSCA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COMFSCI_PK ON TASY.COM_FAQ_SUBCAT_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.COM_FAQ_SUBCAT_ITEM ADD (
  CONSTRAINT COMFSCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_FAQ_SUBCAT_ITEM ADD (
  CONSTRAINT COMFSCI_COMFSCA_FK 
 FOREIGN KEY (NR_SEQ_SUBCATEGORIA) 
 REFERENCES TASY.COM_FAQ_SUBCATEGORIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.COM_FAQ_SUBCAT_ITEM TO NIVEL_1;

