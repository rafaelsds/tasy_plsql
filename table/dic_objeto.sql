ALTER TABLE TASY.DIC_OBJETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIC_OBJETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIC_OBJETO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE             NOT NULL,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE) NOT NULL,
  CD_FUNCAO                    NUMBER(5)        DEFAULT null,
  IE_TIPO_OBJETO               VARCHAR2(15 BYTE) NOT NULL,
  NM_OBJETO                    VARCHAR2(80 BYTE),
  DS_TEXTO                     VARCHAR2(255 BYTE),
  NR_SEQ_OBJ_SUP               NUMBER(10),
  DS_SQL                       VARCHAR2(4000 BYTE),
  NR_SEQ_APRES                 NUMBER(10),
  NM_CAMPO_BASE                VARCHAR2(30 BYTE),
  NM_CAMPO_TELA                VARCHAR2(50 BYTE),
  QT_LARGURA                   NUMBER(5),
  DS_INFORMACAO                VARCHAR2(4000 BYTE),
  DS_MASCARA                   VARCHAR2(42 BYTE),
  IE_ATALHO                    VARCHAR2(15 BYTE),
  IE_CTRL                      VARCHAR2(1 BYTE),
  IE_SHIFT                     VARCHAR2(1 BYTE),
  IE_ALT                       VARCHAR2(1 BYTE),
  DS_COMANDO                   VARCHAR2(2000 BYTE),
  NR_SEQ_MI_SUP                NUMBER(10),
  IE_MOMENTO_SQL               VARCHAR2(15 BYTE),
  NR_SEQ_MENU_SUP              NUMBER(10),
  IE_TIPO_PARAM_SCRIPT         VARCHAR2(15 BYTE),
  IE_OBJETO_NULO               VARCHAR2(1 BYTE),
  IE_OBJETO_GENERICO           VARCHAR2(1 BYTE),
  IE_TIPO_OBJ_TEXTO            VARCHAR2(15 BYTE),
  CD_DOMINIO                   NUMBER(5),
  IE_TIPO_OBJ_FILTRO           VARCHAR2(15 BYTE),
  IE_TIPO_DADO_OBJETO          VARCHAR2(15 BYTE),
  IE_LAYOUT_OBJETO             VARCHAR2(15 BYTE),
  QT_ALTURA                    NUMBER(5),
  IE_TIPO_GRUPO_OBJ_WSCB       VARCHAR2(15 BYTE),
  IE_POSICAO_OBJ_ANT           VARCHAR2(15 BYTE),
  IE_TIPO_OBJ_MENU             VARCHAR2(15 BYTE),
  IE_CONFIGURAVEL              VARCHAR2(1 BYTE),
  QT_DESLOCAMENTO              NUMBER(3),
  QT_INTERVALO                 NUMBER(3),
  VL_DEFAULT                   VARCHAR2(15 BYTE),
  QT_TOPO                      NUMBER(3),
  DS_GRUPO                     VARCHAR2(60 BYTE),
  IE_TIPO_ATRIB_WDP            VARCHAR2(15 BYTE),
  IE_ALIGN_ATRIB_WDP           VARCHAR2(15 BYTE),
  IE_LAYOUT_XY                 VARCHAR2(1 BYTE),
  IE_GERACAO_INF_WDP           VARCHAR2(15 BYTE),
  NR_SEQ_OBJETO                NUMBER(10),
  IE_TIPO_COMP_WDP             VARCHAR2(15 BYTE),
  IE_APRESENT_SERIE_WCHART     VARCHAR2(15 BYTE),
  DS_MASCARA2                  VARCHAR2(30 BYTE),
  IE_INCREMENTO_WCHART         VARCHAR2(15 BYTE),
  IE_READ_ONLY                 VARCHAR2(1 BYTE),
  IE_COMP_OBJ_TEXTO            VARCHAR2(15 BYTE),
  IE_SITUACAO                  VARCHAR2(1 BYTE),
  NM_OBJETO_ORIG               VARCHAR2(50 BYTE),
  IE_MOMENTO_OBJ               VARCHAR2(15 BYTE),
  QT_POS_X                     NUMBER(5),
  QT_POS_Y                     NUMBER(5),
  IE_TIPO_OBJ_COL_WCP          VARCHAR2(15 BYTE),
  DS_OPCOES                    VARCHAR2(2000 BYTE),
  IE_TOTALIZAR                 VARCHAR2(15 BYTE),
  DS_COR                       VARCHAR2(15 BYTE),
  IE_ESTILO_FONTE              VARCHAR2(15 BYTE),
  IE_ALINHAMENTO               VARCHAR2(15 BYTE),
  IE_MOSTRAR_SEPARADOR_COLUNA  VARCHAR2(1 BYTE),
  IE_MOSTRAR_SEPARADOR_LINHA   VARCHAR2(1 BYTE),
  DS_COR_FONTE                 VARCHAR2(15 BYTE),
  QT_TAM_FONTE                 NUMBER(5),
  IE_AUTO_EXPAND_WIDTH         VARCHAR2(1 BYTE),
  IE_RECURSIVO                 VARCHAR2(1 BYTE),
  QT_LARGURA_JAVA              NUMBER(5),
  IE_TIPO_OBJ_PAINEL           VARCHAR2(15 BYTE),
  QT_CARACTER                  NUMBER(5),
  QT_COMPENSACAO_X             NUMBER(5),
  QT_COMPENSACAO_Y             NUMBER(5),
  DS_NAME                      VARCHAR2(50 BYTE),
  DS_ACTION                    VARCHAR2(255 BYTE),
  DS_ID                        VARCHAR2(50 BYTE),
  QT_TAM_FONTE_LABEL           NUMBER(3),
  DS_COR_FONTE_LABEL           VARCHAR2(15 BYTE),
  IE_ESTILO_FONTE_ATR          VARCHAR2(15 BYTE),
  IE_VERSAO_OBJETO             VARCHAR2(15 BYTE),
  IE_COMP_WSCOMP               VARCHAR2(3 BYTE),
  NR_SEQ_DIC_OBJETO            NUMBER(10),
  IE_OBRIGATORIO               VARCHAR2(1 BYTE),
  DS_GRUPO_ATRIB               VARCHAR2(50 BYTE),
  IE_TIPO_EDICAO               VARCHAR2(5 BYTE),
  QT_LARGURA_IOS               NUMBER(5),
  DS_SQL_IMAGEM                VARCHAR2(2000 BYTE),
  NM_TABELA                    VARCHAR2(50 BYTE),
  CD_EXP_CAMPO_TELA            NUMBER(10),
  CD_EXP_TEXTO                 NUMBER(10),
  CD_EXP_INFORMACAO            NUMBER(10),
  IE_VISIBLE                   VARCHAR2(1 BYTE),
  QT_TEMPO_POOLING             NUMBER(10),
  CD_MASCARA                   NUMBER(10),
  IE_UNIT_COL_WIDTH            VARCHAR2(4 BYTE),
  NM_CAMPO_PANEL               VARCHAR2(30 BYTE),
  IE_CATEGORIA_ARVORE          VARCHAR2(1 BYTE),
  IE_TIPO_OBJ_BUTTON           VARCHAR2(15 BYTE),
  NR_SEQ_OBJ_ORIGEM            NUMBER(10),
  NR_SEQ_TAB_ORDER             NUMBER(10),
  NM_CAMPO_MAX_TARGET          VARCHAR2(30 BYTE),
  NM_CAMPO_MIX_TARGET          VARCHAR2(30 BYTE),
  NM_CAMPO_TAM_RAIO            VARCHAR2(30 BYTE),
  IE_TIPO_TARGET               VARCHAR2(1 BYTE),
  IE_CHAMADA_EXTERNA           VARCHAR2(3 BYTE),
  DS_ENDPOINT_API              VARCHAR2(255 BYTE),
  NR_SEQ_BO                    NUMBER(10),
  DS_SENSIVEL                  VARCHAR2(4000 BYTE) DEFAULT null,
  DS_INFO_PACIENTE             VARCHAR2(4000 BYTE) DEFAULT null,
  NM_TIME_ZONE_ATTRIBUTE       VARCHAR2(50 BYTE),
  IE_DATE_TYPE                 VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DICOBJE_BUSOBJ_FK_I ON TASY.DIC_OBJETO
(NR_SEQ_BO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICEXPR_FK_I ON TASY.DIC_OBJETO
(CD_EXP_CAMPO_TELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICEXPR_FK2_I ON TASY.DIC_OBJETO
(CD_EXP_TEXTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICEXPR_FK3_I ON TASY.DIC_OBJETO
(CD_EXP_INFORMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICMASC_FK_I ON TASY.DIC_OBJETO
(CD_MASCARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICOBJE_FK_I ON TASY.DIC_OBJETO
(NR_SEQ_OBJ_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICOBJE_FK2_I ON TASY.DIC_OBJETO
(NR_SEQ_MI_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICOBJE_FK3_I ON TASY.DIC_OBJETO
(NR_SEQ_MENU_SUP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DICOBJE_FK4_I ON TASY.DIC_OBJETO
(NR_SEQ_DIC_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_DOMINIO_FK_I ON TASY.DIC_OBJETO
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_FUNCAO_FK_I ON TASY.DIC_OBJETO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_I1 ON TASY.DIC_OBJETO
(IE_CONFIGURAVEL, IE_TIPO_OBJETO, NR_SEQ_OBJ_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_I2 ON TASY.DIC_OBJETO
(NR_SEQ_MI_SUP, NR_SEQ_OBJ_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICOBJE_I3 ON TASY.DIC_OBJETO
(NR_SEQUENCIA, NM_CAMPO_BASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DICOBJE_I3
  MONITORING USAGE;


CREATE INDEX TASY.DICOBJE_OBJSIST_FK_I ON TASY.DIC_OBJETO
(NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DICOBJE_PK ON TASY.DIC_OBJETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DIC_OBJETO_tp  after update ON TASY.DIC_OBJETO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ATALHO,1,4000),substr(:new.IE_ATALHO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATALHO',ie_log_w,ds_w,'DIC_OBJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALT,1,4000),substr(:new.IE_ALT,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALT',ie_log_w,ds_w,'DIC_OBJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SHIFT,1,4000),substr(:new.IE_SHIFT,1,4000),:new.nm_usuario,nr_seq_w,'IE_SHIFT',ie_log_w,ds_w,'DIC_OBJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CTRL,1,4000),substr(:new.IE_CTRL,1,4000),:new.nm_usuario,nr_seq_w,'IE_CTRL',ie_log_w,ds_w,'DIC_OBJETO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.dic_objeto_b_update
before update ON TASY.DIC_OBJETO for each row
declare
ds_user_w varchar2(100);

begin
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then
if	((:new.ie_tipo_objeto = 'AC') and (:new.qt_largura > 0) and (:new.qt_largura_ios is null)) then
	begin

	:new.qt_largura_ios := :new.qt_largura;

	end;
end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.dic_objeto_update
after update ON TASY.DIC_OBJETO for each row
declare
ds_user_w varchar2(100);

begin
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then

if	(:new.ie_tipo_objeto = 'P') and
	(:new.ds_texto <> :old.ds_texto) then
	begin
	update	dic_objeto_idioma
	set	ds_descricao = :new.ds_texto,
		ie_necessita_revisao = 'S',
		dt_atualizacao = sysdate,
		nm_usuario = :new.nm_usuario
	where	nr_seq_objeto = :new.nr_sequencia
	and	nm_atributo = 'DS_TEXTO';
	end;
elsif	(:new.ie_tipo_objeto = 'AC') and
	(:new.nm_campo_tela <> :old.nm_campo_tela) then
	begin
	update	dic_objeto_idioma
	set	ds_descricao = :new.ds_texto,
		ie_necessita_revisao = 'S',
		dt_atualizacao = sysdate,
		nm_usuario = :new.nm_usuario
	where	nr_seq_objeto = :new.nr_sequencia
	and	nm_atributo = 'NM_CAMPO_TELA';
	end;
end if;
end if;
end;
/


ALTER TABLE TASY.DIC_OBJETO ADD (
  CONSTRAINT DICOBJE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIC_OBJETO ADD (
  CONSTRAINT DICOBJE_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CAMPO_TELA) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DICOBJE_DICEXPR_FK2 
 FOREIGN KEY (CD_EXP_TEXTO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DICOBJE_DICEXPR_FK3 
 FOREIGN KEY (CD_EXP_INFORMACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DICOBJE_DICMASC_FK 
 FOREIGN KEY (CD_MASCARA) 
 REFERENCES TASY.DIC_MASCARA (CD_MASCARA),
  CONSTRAINT DICOBJE_BUSOBJ_FK 
 FOREIGN KEY (NR_SEQ_BO) 
 REFERENCES TASY.BUSINESS_OBJECT (NR_SEQUENCIA),
  CONSTRAINT DICOBJE_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT DICOBJE_DICOBJE_FK 
 FOREIGN KEY (NR_SEQ_OBJ_SUP) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA),
  CONSTRAINT DICOBJE_DICOBJE_FK2 
 FOREIGN KEY (NR_SEQ_MI_SUP) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA),
  CONSTRAINT DICOBJE_DICOBJE_FK3 
 FOREIGN KEY (NR_SEQ_MENU_SUP) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA),
  CONSTRAINT DICOBJE_DOMINIO_FK 
 FOREIGN KEY (CD_DOMINIO) 
 REFERENCES TASY.DOMINIO (CD_DOMINIO),
  CONSTRAINT DICOBJE_OBJSIST_FK 
 FOREIGN KEY (NR_SEQ_OBJETO) 
 REFERENCES TASY.OBJETO_SISTEMA (NR_SEQUENCIA),
  CONSTRAINT DICOBJE_DICOBJE_FK4 
 FOREIGN KEY (NR_SEQ_DIC_OBJETO) 
 REFERENCES TASY.DIC_OBJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIC_OBJETO TO NIVEL_1;

