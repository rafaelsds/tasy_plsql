DROP TABLE TASY.HSJ_LOG_KILL_SESSION CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_LOG_KILL_SESSION
(
  DT_LOGOFF      DATE,
  NM_USUARIO     VARCHAR2(50 BYTE),
  DS_MAQUINA     VARCHAR2(255 BYTE),
  DT_LOGIN       DATE,
  SID            NUMBER,
  SERIAL         NUMBER,
  NM_USUARIO_PC  VARCHAR2(100 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_LOG_KILL_SESSION TO NIVEL_1;

