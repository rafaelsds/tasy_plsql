ALTER TABLE TASY.W_DIOPS_FIN_LUCRO_PREJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIOPS_FIN_LUCRO_PREJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIOPS_FIN_LUCRO_PREJ
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_SEQ_OPERADORA    NUMBER(10),
  NR_SEQ_TRANSACAO    NUMBER(10),
  DS_CONTA            VARCHAR2(255 BYTE),
  VL_SALDO_FINAL      NUMBER(15,2),
  CD_CONTA_CONTABIL   VARCHAR2(20 BYTE),
  NR_SEQ_PERIODO      NUMBER(10),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  DS_DESCRICAO_CONTA  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WDIFILP_PK ON TASY.W_DIOPS_FIN_LUCRO_PREJ
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIFILP_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_DIOPS_FIN_LUCRO_PREJ ADD (
  CONSTRAINT WDIFILP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_DIOPS_FIN_LUCRO_PREJ TO NIVEL_1;

