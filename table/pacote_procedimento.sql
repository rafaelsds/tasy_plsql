ALTER TABLE TASY.PACOTE_PROCEDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACOTE_PROCEDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACOTE_PROCEDIMENTO
(
  NR_SEQ_PACOTE           NUMBER(10)            NOT NULL,
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_INCLUI_EXCLUI        VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  IE_EXCEDENTE            NUMBER(2),
  CD_AREA_PROCED          NUMBER(15),
  CD_ESPECIAL_PROCED      NUMBER(15),
  CD_GRUPO_PROCED         NUMBER(15),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  QT_LIMITE               NUMBER(15,4),
  IE_CONSIDERA_HONORARIO  VARCHAR2(1 BYTE)      NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  IE_TIPO_ATENDIMENTO     NUMBER(3),
  IE_CALCULA_HONORARIO    VARCHAR2(1 BYTE)      NOT NULL,
  IE_CALCULA_CUSTO_OPER   VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_PAC_ACOMOD       NUMBER(10),
  PR_DESCONTO             NUMBER(15,4)          NOT NULL,
  QT_IDADE_MIN            NUMBER(3),
  QT_IDADE_MAX            NUMBER(3),
  VL_MINIMO               NUMBER(15,4),
  VL_MAXIMO               NUMBER(15,4),
  IE_TIPO_VALOR           VARCHAR2(3 BYTE),
  NR_SEQ_PROC_INTERNO     NUMBER(10),
  IE_RATEAR_ITEM          VARCHAR2(1 BYTE),
  PR_DESC                 NUMBER(15,4),
  VL_NEGOCIADO            NUMBER(15,2),
  IE_AGENDAVEL            VARCHAR2(1 BYTE),
  IE_SEXO                 VARCHAR2(1 BYTE),
  CD_MEDICO               VARCHAR2(10 BYTE),
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  IE_SETOR_EXCLUSIVO      VARCHAR2(1 BYTE),
  CD_CENTRO_CUSTO         NUMBER(8),
  CD_MOEDA                NUMBER(3),
  NR_SEQ_CLASSIF          NUMBER(10),
  IE_VALIDA_LIMITE_PROC   VARCHAR2(1 BYTE),
  NR_SEQ_ESTRUTURA        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACPROC_AREPROC_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_AREA_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_CENCUST_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_ESPPROC_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_ESPECIAL_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_GRUPROC_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_GRUPO_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_MOEDA_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPROC_PACOTE_FK_I ON TASY.PACOTE_PROCEDIMENTO
(NR_SEQ_PACOTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPROC_PACTIAC_FK_I ON TASY.PACOTE_PROCEDIMENTO
(NR_SEQ_PAC_ACOMOD)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPROC_PESFISI_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPROC_PESJURI_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_PIESTTR_FK_I ON TASY.PACOTE_PROCEDIMENTO
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACPROC_PK ON TASY.PACOTE_PROCEDIMENTO
(NR_SEQ_PACOTE, NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPROC_PROCEDI_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_PROINTCLA_FK_I ON TASY.PACOTE_PROCEDIMENTO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACPROC_PROINTE_FK_I ON TASY.PACOTE_PROCEDIMENTO
(NR_SEQ_PROC_INTERNO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACPROC_SETATEN_FK_I ON TASY.PACOTE_PROCEDIMENTO
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACPROC_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PACOTE_PROCEDIMENTO_tp  after update ON TASY.PACOTE_PROCEDIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_SEQ_PACOTE='||to_char(:old.NR_SEQ_PACOTE)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'PACOTE_PROCEDIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PACOTE_PROCEDIMENTO ADD (
  CONSTRAINT PACPROC_PK
 PRIMARY KEY
 (NR_SEQ_PACOTE, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACOTE_PROCEDIMENTO ADD (
  CONSTRAINT PACPROC_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PACPROC_PROINTCLA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROC_INTERNO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT PACPROC_PIESTTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PI_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PACPROC_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PACPROC_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACPROC_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PACPROC_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCED) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PACPROC_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIAL_PROCED) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PACPROC_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROCED) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PACPROC_PACOTE_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.PACOTE (NR_SEQ_PACOTE)
    ON DELETE CASCADE,
  CONSTRAINT PACPROC_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PACPROC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACPROC_PACTIAC_FK 
 FOREIGN KEY (NR_SEQ_PAC_ACOMOD) 
 REFERENCES TASY.PACOTE_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PACPROC_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACOTE_PROCEDIMENTO TO NIVEL_1;

