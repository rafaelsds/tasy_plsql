ALTER TABLE TASY.PLS_PAGADOR_ITEM_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PAGADOR_ITEM_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PAGADOR_ITEM_MENS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_PAGADOR             NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_PAGADOR_ITEM        NUMBER(10),
  IE_TIPO_ITEM               VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_LANC           NUMBER(10),
  NR_SEQ_CENTRO_APROPRIACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPAIM_ESTABEL_FK_I ON TASY.PLS_PAGADOR_ITEM_MENS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPAIM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPAIM_I ON TASY.PLS_PAGADOR_ITEM_MENS
(NR_SEQ_PAGADOR, IE_TIPO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPAIM_PK ON TASY.PLS_PAGADOR_ITEM_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAIM_PLCONPAG_FK_I ON TASY.PLS_PAGADOR_ITEM_MENS
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAIM_PLCONPAG_FK2_I ON TASY.PLS_PAGADOR_ITEM_MENS
(NR_SEQ_PAGADOR_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAIM_PLSCEAP_FK_I ON TASY.PLS_PAGADOR_ITEM_MENS
(NR_SEQ_CENTRO_APROPRIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPAIM_PLSTLAD_FK_I ON TASY.PLS_PAGADOR_ITEM_MENS
(NR_SEQ_TIPO_LANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_pagador_item_mens_atual
before insert or update ON TASY.PLS_PAGADOR_ITEM_MENS for each row
declare

qt_registro_w			number(10);
qt_item_centro_apropriacao_w	number(10);

pragma autonomous_transaction;

begin

if	(:new.nr_seq_pagador_item is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1061247);
end if;

if	(:new.ie_tipo_item is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1061246);
end if;

if	(:new.nr_seq_centro_apropriacao is not null) then
	if	(:new.ie_tipo_item not in ('1','3','20')) then
		wheb_mensagem_pck.exibir_mensagem_abort(1060045);
	end if;

	select	count(1)
	into	qt_item_centro_apropriacao_w
	from	pls_pagador_item_mens
	where	nr_seq_pagador	= :new.nr_seq_pagador
	and	nr_sequencia <> :new.nr_sequencia
	and	ie_tipo_item = :new.ie_tipo_item
	and	nr_seq_centro_apropriacao is null;

	if	(qt_item_centro_apropriacao_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1060046, 'IE_TIPO_ITEM='||obter_valor_dominio(1930,:new.ie_tipo_item));
	end if;
else
	select	count(1)
	into	qt_item_centro_apropriacao_w
	from	pls_pagador_item_mens
	where	nr_seq_pagador	= :new.nr_seq_pagador
	and	nr_sequencia <> :new.nr_sequencia
	and	ie_tipo_item = :new.ie_tipo_item
	and	nr_seq_centro_apropriacao is not null;

	if	(qt_item_centro_apropriacao_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1060047, 'IE_TIPO_ITEM='||obter_valor_dominio(1930,:new.ie_tipo_item));
	end if;
end if;

select	count(1)
into	qt_registro_w
from	pls_pagador_item_mens
where	nr_sequencia	<> :new.nr_sequencia
and	nr_seq_pagador	= :new.nr_seq_pagador
and	ie_tipo_item	= :new.ie_tipo_item
and	((nr_seq_centro_apropriacao = :new.nr_seq_centro_apropriacao) or (:new.nr_seq_centro_apropriacao is null))
and	nvl(nr_seq_tipo_lanc,0) = nvl(:new.nr_seq_tipo_lanc,0);

if	(qt_registro_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(358190);
end if;

end;
/


ALTER TABLE TASY.PLS_PAGADOR_ITEM_MENS ADD (
  CONSTRAINT PLSPAIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PAGADOR_ITEM_MENS ADD (
  CONSTRAINT PLSPAIM_PLSCEAP_FK 
 FOREIGN KEY (NR_SEQ_CENTRO_APROPRIACAO) 
 REFERENCES TASY.PLS_CENTRO_APROPRIACAO (NR_SEQUENCIA),
  CONSTRAINT PLSPAIM_PLSTLAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LANC) 
 REFERENCES TASY.PLS_TIPO_LANC_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSPAIM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPAIM_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPAIM_PLCONPAG_FK2 
 FOREIGN KEY (NR_SEQ_PAGADOR_ITEM) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PAGADOR_ITEM_MENS TO NIVEL_1;

