DROP TABLE TASY.CORPO_HUMANO_CLASSIFICACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CORPO_HUMANO_CLASSIFICACAO
(
  NR_SEQ_CLASSIFICACAO  NUMBER(10)              NOT NULL,
  NR_SEQ_CORPO_HUMANO   NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  QT_PORCENTAGEM_MIN    NUMBER(10),
  QT_PORCENTAGEM_MAX    NUMBER(10),
  DS_CLASSIFICACAO      VARCHAR2(4000 BYTE),
  DS_COR                VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE),
  QT_IDADE_ANO_MIN      NUMBER(10),
  QT_IDADE_ANO_MAX      NUMBER(10),
  NR_SEQ_GRAVIDADE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.CORPO_HUMANO_CLASSIFICACAO TO NIVEL_1;

