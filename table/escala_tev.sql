ALTER TABLE TASY.ESCALA_TEV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_TEV CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_TEV
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  IE_RISCO                        VARCHAR2(1 BYTE),
  NR_ATENDIMENTO                  NUMBER(10)    NOT NULL,
  DT_AVALIACAO                    DATE          NOT NULL,
  IE_SITUACAO                     VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                    DATE,
  DT_INATIVACAO                   DATE,
  NM_USUARIO_INATIVACAO           VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA                VARCHAR2(255 BYTE),
  CD_PROFISSIONAL                 VARCHAR2(10 BYTE) NOT NULL,
  IE_CLINICO_CIRURGICO            VARCHAR2(1 BYTE),
  QT_IDADE                        NUMBER(3),
  IE_INSUF_ARTERIAL               VARCHAR2(1 BYTE),
  IE_MOBILIDADE_REDUZIDA          VARCHAR2(1 BYTE),
  IE_FATOR_RISCO                  VARCHAR2(1 BYTE),
  IE_CONDICAO                     VARCHAR2(1 BYTE),
  IE_ALTISSIMO_RISCO              VARCHAR2(1 BYTE),
  IE_ALTO_RISCO                   VARCHAR2(1 BYTE),
  IE_AVCI                         VARCHAR2(1 BYTE),
  IE_TROMBOFILIA                  VARCHAR2(1 BYTE),
  IE_TVP_EP_PREVIA                VARCHAR2(1 BYTE),
  IE_HIST_FAMILIAR_TEV            VARCHAR2(1 BYTE),
  IE_ANTICONCEPCIONAIS            VARCHAR2(1 BYTE),
  IE_PUERPERIO                    VARCHAR2(1 BYTE),
  IE_OBESIDADE                    VARCHAR2(1 BYTE),
  IE_INTERNACAO_UTI               VARCHAR2(1 BYTE),
  IE_INSUF_RESP                   VARCHAR2(1 BYTE),
  IE_CAT_VENOSO_CENTRAL           VARCHAR2(1 BYTE),
  IE_PARALISIA_INF                VARCHAR2(1 BYTE),
  IE_ICC                          VARCHAR2(1 BYTE),
  IE_DPOC                         VARCHAR2(1 BYTE),
  IE_PNEUMONIA                    VARCHAR2(1 BYTE),
  IE_INFLA_INTESTINAL             VARCHAR2(1 BYTE),
  IE_VARIZES                      VARCHAR2(1 BYTE),
  IE_QUIMIOTERAPIA                VARCHAR2(1 BYTE),
  IE_SINDROME_NEFROTICA           VARCHAR2(1 BYTE),
  IE_INFECSAO_GRAVE               VARCHAR2(1 BYTE),
  IE_DOENCA_AUTOIMUNE             VARCHAR2(1 BYTE),
  IE_TRAUMA                       VARCHAR2(1 BYTE),
  IE_ABORTAMENTO                  VARCHAR2(1 BYTE),
  IE_IAM                          VARCHAR2(1 BYTE),
  IE_ECLAMPSIA                    VARCHAR2(1 BYTE),
  IE_QUEIMADO                     VARCHAR2(1 BYTE),
  IE_OUTROS                       VARCHAR2(1 BYTE),
  IE_SANGRAMENTO_ULCERA           VARCHAR2(1 BYTE),
  IE_COAGULOPATIA                 VARCHAR2(1 BYTE),
  IE_HIPERTENSAO                  VARCHAR2(1 BYTE),
  IE_INSUF_RENAL                  VARCHAR2(1 BYTE),
  IE_CIRURGIA_INTRACRANIANA       VARCHAR2(1 BYTE),
  IE_PUNCAO_LIQUORICA             VARCHAR2(1 BYTE),
  IE_HIPERSENSIBILIDADE_HEPARINA  VARCHAR2(1 BYTE),
  IE_CIRUR_ORTOPEDICA             VARCHAR2(1 BYTE),
  IE_CIRUR_CANCER                 VARCHAR2(1 BYTE),
  IE_TRAUMA_GRAVE                 VARCHAR2(1 BYTE),
  IE_TRAUM_RAQUIMEDULARES         VARCHAR2(1 BYTE),
  IE_CANCER                       VARCHAR2(1 BYTE),
  IE_CIRUR_GRANDE                 VARCHAR2(1 BYTE),
  IE_MEMBROS_IMOB                 VARCHAR2(1 BYTE),
  IE_TRH                          VARCHAR2(1 BYTE),
  DS_RESULTADO                    VARCHAR2(4000 BYTE),
  IE_NENHUM_ALT_RISCO             VARCHAR2(1 BYTE),
  IE_RESTRICAO_PROLONG            VARCHAR2(1 BYTE),
  IE_NENHUM_ALTISSIMO_RISCO       VARCHAR2(1 BYTE),
  IE_NENHUM_RISCO                 VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO             NUMBER(10),
  QT_PESO                         NUMBER(6,3),
  QT_ALTURA_CM                    NUMBER(5,2),
  IE_GRAVIDEZ                     VARCHAR2(1 BYTE),
  IE_IDADE_MAIOR                  VARCHAR2(1 BYTE),
  IE_TABAGISMO                    VARCHAR2(1 BYTE),
  IE_DOENCA_REUMATOL              VARCHAR2(1 BYTE),
  IE_ULCERA_ATIVA                 VARCHAR2(1 BYTE),
  IE_RISCO_CIRURGIA               VARCHAR2(1 BYTE),
  IE_PORTE_CIRURGIA               VARCHAR2(1 BYTE),
  IE_IDADE_AVANCADA               VARCHAR2(1 BYTE),
  IE_IDADE_MENOR                  VARCHAR2(1 BYTE),
  IE_PLAQUETOPENIA_HEPARINA       VARCHAR2(1 BYTE),
  IE_ESCARA                       VARCHAR2(1 BYTE),
  IE_FRATURA_EXPOSTA              VARCHAR2(1 BYTE),
  IE_INFECCAO_MMII                VARCHAR2(1 BYTE),
  IE_INSUF_ART_MMII               VARCHAR2(1 BYTE),
  IE_INSUF_CARD_GRAVE             VARCHAR2(1 BYTE),
  IE_PADRAO                       VARCHAR2(3 BYTE),
  IE_CIR_ALTO_RISCO               VARCHAR2(1 BYTE),
  IE_CIR_GRANDE_MEDIO_PORTE       VARCHAR2(1 BYTE),
  IE_CIR_PEQUENO_PORTE            VARCHAR2(1 BYTE),
  IE_NENHUMA_CONTRA_INDIC         VARCHAR2(1 BYTE),
  IE_ARTROPLASTIA_JOELHO          VARCHAR2(1 BYTE),
  IE_ARTROPLASTIA_QUADRIL         VARCHAR2(1 BYTE),
  IE_FRATURA_QUADRIL              VARCHAR2(1 BYTE),
  IE_ONCOL_CURATIVA               VARCHAR2(1 BYTE),
  IE_TRAUMA_MEDULAR               VARCHAR2(1 BYTE),
  IE_POLITRAUMA                   VARCHAR2(1 BYTE),
  IE_NENHUM_RISCO_PROF            VARCHAR2(1 BYTE),
  IE_ANTICOAGULACAO               VARCHAR2(1 BYTE),
  IE_QUIMIOPROFILAXIA             VARCHAR2(1 BYTE),
  IE_PROFILAXIA_MEC               VARCHAR2(1 BYTE),
  IE_CONTRA_INDIC                 VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_TEV               NUMBER(10),
  IE_NAO_SE_APLICA                VARCHAR2(1 BYTE),
  NR_HORA                         NUMBER(2),
  QT_PONTUACAO                    NUMBER(10),
  NR_SEQ_ASSINATURA               NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO       NUMBER(10),
  DS_OBSERVACAO                   VARCHAR2(4000 BYTE),
  CD_SETOR_ATENDIMENTO            NUMBER(5),
  IE_RISCO_RECLASSIF              VARCHAR2(1 BYTE),
  QT_CLEARENCE                    NUMBER(15,2),
  QT_IMC                          NUMBER(4,1),
  IE_BARIATRICA                   VARCHAR2(1 BYTE),
  IE_TRAUMA_CRANIANO              VARCHAR2(1 BYTE),
  IE_DEMAIS_CIRURGIAS             VARCHAR2(1 BYTE),
  IE_CONT_POLITRAUMA              VARCHAR2(1 BYTE),
  NR_SEQ_JUST_TEV                 NUMBER(10),
  IE_MED_ACORDO                   VARCHAR2(1 BYTE),
  IE_MOB_RED_FINALIZADA           VARCHAR2(1 BYTE),
  NR_SEQ_RESPOSTA                 NUMBER(10),
  IE_CI_INESP_1                   VARCHAR2(1 BYTE),
  DS_CUIDADOS                     VARCHAR2(2000 BYTE),
  IE_CI_INESP_2                   VARCHAR2(1 BYTE),
  IE_CI_INESP_3                   VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO                VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCATEV_ATEPACI_FK_I ON TASY.ESCALA_TEV
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCATEV_I1 ON TASY.ESCALA_TEV
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCATEV_PESFISI_FK_I ON TASY.ESCALA_TEV
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCATEV_PK ON TASY.ESCALA_TEV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCATEV_SETATEN_FK_I ON TASY.ESCALA_TEV
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCATEV_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCATEV_TASASDI_FK_I ON TASY.ESCALA_TEV
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCATEV_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCATEV_TASASDI_FK2_I ON TASY.ESCALA_TEV
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCATEV_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCATEV_TEVJUST_FK_I ON TASY.ESCALA_TEV
(NR_SEQ_JUST_TEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCATEV_TEVJUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCATEV_TEVMOTI_FK_I ON TASY.ESCALA_TEV
(NR_SEQ_MOTIVO_TEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCATEV_TEVMOTI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_TEV_ALT_ATUAL
before insert or update ON TASY.ESCALA_TEV for each row
declare
qt_reg_w		number(10)  := 0;
quebra_w		varchar2(20)	:= Chr(13)||chr(10);
qt_imc_w		number(10,2);
ie_padrao_tev_w		varchar2(10)	:= 'D';
qt_contra_prof_mec_w	number(10)	:= 0;
qt_Nadroparina_w	varchar2(50);
qt_contra_ind_relat_w	number(10)	:= 0;
ie_condicao_w		varchar2(1);

ie_cir_alto_risco_w		varchar2(1);
ie_cir_pequeno_porte_w		varchar2(1);
ie_cir_grande_medio_porte_w	varchar2(1);
qt_absoluta_w			number(10,0);
qt_relativa_w			number(10,0);
qt_questao_5_w			number(10,0);

ie_artroplastia_joelho_w	varchar2(1);
ie_artroplastia_quadril_w	varchar2(1);
ie_fratura_quadril_w		varchar2(1);
ie_oncol_curativa_w		varchar2(1);
ie_trauma_medular_w		varchar2(1);
ie_politrauma_w	 		varchar2(1);

ds_texto_w			varchar2(4000);
ds_motivo_w			varchar2(255);

pragma autonomous_transaction;



	function Obter_Texto_Tev(ie_regra_p	varchar2)
					return varchar2 is
	ds_texto_w	varchar2(4000);
	begin
	select	max(ds_texto)
	into	ds_texto_w
	from	REGRA_ESCALA_TEV
	where	ie_regra	= ie_regra_p;

	ds_texto_w	:= replace(ds_texto_w,'@NADROPARINA',qt_Nadroparina_w);
	return ds_texto_w;
	end;



	procedure	Inserir_Descricao_Idade is
	begin

	if	(:new.qt_idade	< 40) then
		:new.ds_resultado:= :new.ds_resultado||nvl(Obter_Texto_Tev(1),obter_desc_expressao(347111) || quebra_w || obter_desc_expressao(347112));
	end if;
	end;

	procedure Inserir_Medicacao is

	begin

	if	(:new.ie_risco	= 'A') then
		if	(:new.qt_peso	< 70) then
			qt_Nadroparina_w	:= '3.800';
		else
			qt_Nadroparina_w	:= '5.700';
		end if;
		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||nvl(Obter_Texto_Tev(2),obter_desc_expressao(347128) || quebra_w ||
						   obter_desc_expressao(347130) || quebra_w ||
						   obter_desc_expressao(347131) || qt_Nadroparina_w ||
						   obter_desc_expressao(347132) || quebra_w ||
						   obter_desc_expressao(347134));

	elsif	(:new.ie_risco	= 'M') then

		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||nvl(Obter_Texto_Tev(3),obter_desc_expressao(347135) || quebra_w ||
						obter_desc_expressao(347136) || quebra_w ||
						obter_desc_expressao(347137) || quebra_w ||
						obter_desc_expressao(347138));
	end if;

	if	(:new.ie_risco in ('A','M')) then
		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||nvl(Obter_Texto_Tev(4),obter_desc_expressao(347139));
	end if;
	end;


	procedure Inserir_Contra_Indicacao is

	begin

	if	(ie_condicao_w	= 'S') and
		(qt_contra_prof_mec_w	= 0)then
		:new.IE_QUIMIOPROFILAXIA := 'N';
		:new.IE_PROFILAXIA_MEC    := 'S';
		:new.ds_resultado	:= 	nvl(Obter_Texto_Tev(5),obter_desc_expressao(347150));

	elsif	(qt_contra_ind_relat_w	> 0) and
		(ie_condicao_w	= 'N') and
		(qt_contra_prof_mec_w	= 0) then
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.IE_PROFILAXIA_MEC    := 'S';
		:new.ds_resultado	:= 	nvl(Obter_Texto_Tev(6),obter_desc_expressao(347185));
		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||nvl(Obter_Texto_Tev(7),obter_desc_expressao(347161));
		Inserir_Medicacao; -- edilson

	elsif	(qt_contra_ind_relat_w	> 0) and --Edilson
		(ie_condicao_w	= 'N'	) and
		(qt_contra_prof_mec_w	> 0) then
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.IE_PROFILAXIA_MEC   := 'A';
		:new.ds_resultado	:= nvl(Obter_Texto_Tev(8),obter_desc_expressao(347156));

		:new.ds_resultado	:= :new.ds_resultado ||quebra_w||quebra_w|| nvl(Obter_Texto_Tev(9),obter_desc_expressao(347158)) ||quebra_w;
		:new.ds_resultado	:= :new.ds_resultado	||nvl(Obter_Texto_Tev(10),obter_desc_expressao(347159) || obter_desc_expressao(347160));

		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||nvl(Obter_Texto_Tev(11),obter_desc_expressao(347161));
		Inserir_Medicacao; -- edilson

	elsif	(ie_condicao_w	= 'S') and
		(qt_contra_prof_mec_w > 0) then
		:new.IE_QUIMIOPROFILAXIA := 'N';
		:new.IE_PROFILAXIA_MEC   := 'A';
		:new.ds_resultado	:= nvl(Obter_Texto_Tev(12),obter_desc_expressao(347162));
--		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||'Optando-se pela quimioprofilaxia a recomenda��o �:';
--		Inserir_Medicacao;

		:new.ds_resultado	:= :new.ds_resultado ||quebra_w||quebra_w|| nvl(Obter_Texto_Tev(13),obter_desc_expressao(347158)) ||quebra_w;
		:new.ds_resultado	:= :new.ds_resultado	||nvl(Obter_Texto_Tev(14),obter_desc_expressao(347159) || obter_desc_expressao(347167));
	end if;
	end;


	procedure inserir_texto_tipo_cirur is

	begin
	ds_texto_w := null;

	if	((ie_artroplastia_quadril_w = 'S') or (ie_artroplastia_joelho_w = 'S')) then
		ds_texto_w := ds_texto_w ||  nvl(Obter_Texto_Tev(15),obter_desc_expressao(347113) || quebra_w || quebra_w || obter_desc_expressao(347115) || obter_desc_expressao(347116) || quebra_w || quebra_w);
	end if;

	if	(ie_cir_grande_medio_porte_w = 'S') then
		ds_texto_w	:= ds_texto_w || nvl(Obter_Texto_Tev(16),obter_desc_expressao(347117) || quebra_w);
	end if;

	if	(ie_fratura_quadril_w = 'S') then
		ds_texto_w	:= ds_texto_w || nvl(Obter_Texto_Tev(17),obter_desc_expressao(347118) || quebra_w);
	end if;

	if	(ie_oncol_curativa_w = 'S') then
		ds_texto_w	:= ds_texto_w || nvl(Obter_Texto_Tev(18),obter_desc_expressao(347119) || quebra_w);
	end if;

	if	((ie_trauma_medular_w = 'S') or (ie_politrauma_w = 'S')) then
		ds_texto_w	:= ds_texto_w || nvl(Obter_Texto_Tev(19),obter_desc_expressao(347121) || quebra_w);
	end if;

	if	(ie_artroplastia_quadril_w = 'S') then
		ds_texto_w	:= ds_texto_w || nvl(Obter_Texto_Tev(20),obter_desc_expressao(347118) || quebra_w);
	end if;

	if	(ie_artroplastia_joelho_w = 'S') then
		ds_texto_w	:= ds_texto_w || nvl(Obter_Texto_Tev(21),obter_desc_expressao(347122) || quebra_w);
	end if;

	:new.ds_resultado	:= :new.ds_resultado || ds_texto_w;

	end;

	procedure inserir_texto_adic_cir is

	begin
	ds_texto_w := 	quebra_w || quebra_w ||
			nvl(Obter_Texto_Tev(22),obter_desc_expressao(347125));
	:new.ds_resultado	:= :new.ds_resultado || ds_texto_w;
	end;

	procedure inserir_resposta_cirurgia is

	begin

	:new.IE_QUIMIOPROFILAXIA := 'A';
	:new.IE_PROFILAXIA_MEC    := 'A';

	if	(ie_cir_pequeno_porte_w = 'S') then
		:new.ie_risco := 'B';
		:new.IE_PROFILAXIA_MEC := 'A';
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.ds_resultado := 	nvl(Obter_Texto_Tev(23),obter_desc_expressao(347168));

	elsif	(ie_cir_grande_medio_porte_w = 'S') and
		(:new.qt_idade	< 40) and
		(qt_reg_w > 0) then
		if 	((qt_relativa_w + qt_absoluta_w) = 0) then
			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.IE_PROFILAXIA_MEC    := 'N';
			:new.ie_risco := 'M';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(24),obter_desc_expressao(347169) || quebra_w || quebra_w ||
						obter_desc_expressao(347135) || quebra_w ||
						obter_desc_expressao(347171) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347132) || quebra_w ||
						obter_desc_expressao(347172) || quebra_w || quebra_w ||
						obter_desc_expressao(347117));
						inserir_texto_adic_cir;
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w = 1) and
			(qt_questao_5_w = 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC    := 'S';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(25),obter_desc_expressao(347150));
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w > 0) and
			(qt_questao_5_w > 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(26),obter_desc_expressao(347173) || quebra_w || quebra_w ||
						obter_desc_expressao(347158) || quebra_w || quebra_w ||
						obter_desc_expressao(347159) || obter_desc_expressao(347167));

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w > 0) and
			(qt_questao_5_w > 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado :=	nvl(Obter_Texto_Tev(27),obter_desc_expressao(347156) || quebra_w || quebra_w ||
						obter_desc_expressao(347158) || quebra_w || quebra_w ||
						obter_desc_expressao(347159) || obter_desc_expressao(347167) || quebra_w || quebra_w ||
						obter_desc_expressao(347161) || quebra_w || quebra_w ||
						obter_desc_expressao(347135) || quebra_w ||
						obter_desc_expressao(347136) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347176) ||quebra_w||
						obter_desc_expressao(347172) || quebra_w || quebra_w ||
						obter_desc_expressao(347117));
						inserir_texto_adic_cir;

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w = 1) and
			(qt_absoluta_w = 0) and
			(qt_questao_5_w = 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC := 'S';
			:new.ds_resultado :=	nvl(Obter_Texto_Tev(28),obter_desc_expressao(347185) || quebra_w || quebra_w ||
						obter_desc_expressao(347161) || quebra_w || quebra_w ||
						obter_desc_expressao(347135) ||  quebra_w ||
						obter_desc_expressao(347136) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347176) || quebra_w ||
						obter_desc_expressao(347172) || quebra_w || quebra_w ||
						obter_desc_expressao(347117));
						inserir_texto_adic_cir;

		end if;
	elsif	(ie_cir_grande_medio_porte_w = 'S') and
		(:new.qt_idade	< 40) and
		(qt_reg_w = 0) then
		:new.ie_risco := 'B';
		:new.IE_PROFILAXIA_MEC := 'A';
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.ds_resultado := 	nvl(Obter_Texto_Tev(29),'Pacientes submetidos a procedimentos cir�rgicos considerados de baixo risco para TEV podem ainda assim apresentar trombose, ' ||
					'particularmente aqueles mais idosos ou com fatores de risco associados, como c�ncer, hist�ria pr�via ou familiar de TEV, ou ' ||
					'mesmo associa��es de v�rios fatores comuns, como tabagismo, varizes, obesidade, reposi��o hormonal, etc. Nestes pacientes, a avalia��o ' ||
					'de risco deve ser individualizada, podendo-se optar pela prescri��o da profilaxia.');
	elsif	(ie_cir_grande_medio_porte_w = 'S') and
		((:new.qt_idade	>= 40) and (:new.qt_idade <= 60)) and
		(qt_reg_w = 0) then
		if 	((qt_relativa_w + qt_absoluta_w) = 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.IE_PROFILAXIA_MEC := 'N';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(30),obter_desc_expressao(347169) || quebra_w || quebra_w ||
						obter_desc_expressao(347135) || quebra_w ||
						obter_desc_expressao(347136) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347178) || quebra_w ||
						obter_desc_expressao(347172) || quebra_w || quebra_w ||
						obter_desc_expressao(347117));
						inserir_texto_adic_cir;
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w = 1) and
			(qt_questao_5_w = 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'S';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(31),obter_desc_expressao(347150));
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w > 0) and
			(qt_questao_5_w > 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(32),obter_desc_expressao(347156) || quebra_w || quebra_w ||
						obter_desc_expressao(347158) || quebra_w || quebra_w ||
						obter_desc_expressao(347159) || obter_desc_expressao(347167));
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w > 0) and
			(qt_questao_5_w > 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado :=	nvl(Obter_Texto_Tev(33),obter_desc_expressao(347156) || quebra_w || quebra_w ||
						obter_desc_expressao(347158) || quebra_w || quebra_w ||
						obter_desc_expressao(347159) || 347167 || quebra_w || quebra_w ||
						obter_desc_expressao(347161) || quebra_w || quebra_w ||
						obter_desc_expressao(347135) || quebra_w ||
						obter_desc_expressao(347136) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347178) || quebra_w ||
						obter_desc_expressao(347172) || quebra_w || quebra_w ||
						obter_desc_expressao(347117));
						inserir_texto_adic_cir;
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w = 1) and
			(qt_absoluta_w = 0) and
			(qt_questao_5_w = 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC  := 'S';
			:new.ds_resultado :=	nvl(Obter_Texto_Tev(34), obter_desc_expressao(347185)|| quebra_w || quebra_w ||
						obter_desc_expressao(347161) || quebra_w || quebra_w ||
						obter_desc_expressao(347135) || quebra_w ||
						obter_desc_expressao(347136) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347178) || quebra_w ||
						obter_desc_expressao(347172) || quebra_w || quebra_w ||
						obter_desc_expressao(347117));
						inserir_texto_adic_cir;
		end if;
	elsif	(((ie_cir_grande_medio_porte_w = 'S') and
		((:new.qt_idade	>= 40) and (:new.qt_idade <= 60)) and
		(qt_reg_w > 0)) or
		((ie_cir_alto_risco_w = 'S') or
		((ie_cir_grande_medio_porte_w = 'S') and (:new.qt_idade > 60)))) then
		if	((qt_relativa_w + qt_absoluta_w) > 0) then
			if	(qt_absoluta_w = 1)  and
				(qt_questao_5_w = 0) then
				:new.ie_risco := 'A';
				:new.IE_PROFILAXIA_MEC 	 := 'S';
				:new.IE_QUIMIOPROFILAXIA := 'N';
				:new.ds_resultado :=	nvl(Obter_Texto_Tev(35),obter_desc_expressao(347150));
			elsif	(qt_relativa_w > 0) and
				(qt_absoluta_w = 0) and
				(qt_questao_5_w = 0) then

				if	(:new.qt_peso	< 70) then
					qt_Nadroparina_w	:= '3.800';
				else
					qt_Nadroparina_w	:= '5.700';
				end if;

				:new.ie_risco := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'A';
				:new.IE_PROFILAXIA_MEC   := 'S';
				:new.ds_resultado := 	nvl(Obter_Texto_Tev(36),obter_desc_expressao(347146) || quebra_w || quebra_w ||
							obter_desc_expressao(347161) || quebra_w ||
							obter_desc_expressao(347128) || quebra_w ||
							obter_desc_expressao(347130) || quebra_w ||
							obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347178) || quebra_w ||
							obter_desc_expressao(347134) || quebra_w || quebra_w);
							inserir_texto_tipo_cirur;
							inserir_texto_adic_cir;

			elsif	(qt_absoluta_w > 0) and
				(qt_questao_5_w > 0) then
				:new.ie_risco := 'A';
				:new.IE_PROFILAXIA_MEC := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'N';
				:new.ds_resultado := 	nvl(Obter_Texto_Tev(37),obter_desc_expressao(347173) || quebra_w || quebra_w ||
							obter_desc_expressao(347158) || quebra_w || quebra_w ||
							obter_desc_expressao(347159) || obter_desc_expressao(347167));

			elsif	(qt_relativa_w > 0) and
				(qt_absoluta_w = 0) and
				(qt_questao_5_w > 0) then

				if	(:new.qt_peso	< 70) then
					qt_Nadroparina_w	:= '3.800';
				else
					qt_Nadroparina_w	:= '5.700';
				end if;

				:new.ie_risco := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'A';
				:new.IE_PROFILAXIA_MEC := 'A';
				:new.ds_resultado := 	nvl(Obter_Texto_Tev(38),obter_desc_expressao(347156) || quebra_w || quebra_w ||
							obter_desc_expressao(347158) || quebra_w || quebra_w ||
							obter_desc_expressao(347159) || obter_desc_expressao(347167) || quebra_w || quebra_w ||
							obter_desc_expressao(347161) || quebra_w ||
							obter_desc_expressao(347128) || quebra_w ||
							obter_desc_expressao(347130) || quebra_w ||
							obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347178) || quebra_w ||
							obter_desc_expressao(347134) || quebra_w || quebra_w);
							inserir_texto_tipo_cirur;
							inserir_texto_adic_cir;
			end if;
		else
			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '3.800';
			else
				qt_Nadroparina_w	:= '5.700';
			end if;

			:new.ie_risco := 'A';
			:new.IE_PROFILAXIA_MEC := 'N';
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.ds_resultado := 	nvl(Obter_Texto_Tev(39),obter_desc_expressao(347169) || quebra_w || quebra_w ||
						obter_desc_expressao(347128) || quebra_w ||
						obter_desc_expressao(347130) || quebra_w ||
						obter_desc_expressao(347131) || qt_Nadroparina_w || obter_desc_expressao(347178) || quebra_w||
						obter_desc_expressao(347134) || quebra_w || quebra_w);
						inserir_texto_tipo_cirur;
						inserir_texto_adic_cir;
		end if;
	end if;

	end;

begin

ie_padrao_tev_w	:= nvl(:new.ie_padrao,'D');
if	(nvl(:new.ie_padrao,'D')	= 'ALT') then
	:new.IE_IDADE_MAIOR	:='N';
	if	(:new.qt_idade	>= 55) then
		:new.IE_IDADE_MAIOR	:= 'S';
	end if;

	qt_imc_w	:= dividir(:new.qt_peso,(:new.qt_altura_cm /100) * (:new.qt_altura_cm /100));
	if	(qt_imc_w	>=30 ) then
		:new.IE_OBESIDADE	:= 'S';
	end if;
	:new.IE_CONDICAO	:= 'N';
	ie_condicao_w		:= 'N';
	qt_reg_w		:= 0;

	if	(:new.IE_HIPERSENSIBILIDADE_HEPARINA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_SANGRAMENTO_ULCERA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_ANTICOAGULACAO	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(qt_reg_w	> 0) then
		ie_condicao_w	:= 'S';
	end if;

	qt_contra_ind_relat_w := 0;

	if	(:new.IE_COAGULOPATIA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_ULCERA_ATIVA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_HIPERTENSAO	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_INSUF_RENAL	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_CIRURGIA_INTRACRANIANA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_PUNCAO_LIQUORICA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(qt_reg_w > 0) or
		(qt_contra_ind_relat_w > 0) then
		:new.IE_CONDICAO	:= 'S';
	end if;

	:new.ds_resultado	:= null;
	qt_reg_w		:= 0;


	if	(:new.IE_ESCARA	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_FRATURA_EXPOSTA	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INFECCAO_MMII	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INSUF_ART_MMII	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INSUF_CARD_GRAVE	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;


	if	(:new.IE_CLINICO_CIRURGICO	= 'C') then
		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_IDADE_MAIOR	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_ECLAMPSIA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DOENCA_REUMATOL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_HIST_FAMILIAR_TEV	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ANTICONCEPCIONAIS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PUERPERIO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_OBESIDADE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INTERNACAO_UTI	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PARALISIA_INF	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_GRAVIDEZ	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_INSUF_RESP	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INSUF_ARTERIAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TRH	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ICC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PNEUMONIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFLA_INTESTINAL= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_VARIZES	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFECSAO_GRAVE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_QUIMIOTERAPIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_SINDROME_NEFROTICA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DOENCA_AUTOIMUNE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ABORTAMENTO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRAUMA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_IAM	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_QUEIMADO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_OUTROS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		:new.IE_FATOR_RISCO:='N';
		if	(qt_reg_w	> 0) and
			(:new.IE_MOBILIDADE_REDUZIDA	= 'S')then
			:new.IE_FATOR_RISCO := 'S';
		end if;

		:new.ie_risco	:= 'B';
		if	(:new.IE_FATOR_RISCO	= 'N') then
			:new.ie_risco	:= 'B';
		elsif	(:new.IE_FATOR_RISCO	= 'S') then
			:new.ie_risco	:= 'A';

			if	(:new.qt_idade	<= 40) and
				(ie_condicao_w	= 'N') then
				:new.ie_risco	:= 'B';
			end if;
			if	(:new.qt_idade < 40) then
				:new.ie_risco	:= 'B';
			end if;
		end if;
		:new.ds_resultado	:= Null;



		if	(:new.ie_risco	= 'B') then
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC  := 'N';

			:new.ds_resultado	:= 	nvl(Obter_Texto_Tev(40),obter_desc_expressao(347190) ||
			obter_desc_expressao(347191));

			if	(:new.IE_FATOR_RISCO	= 'S') and
				(:new.qt_idade	< 40) then

				:new.ds_resultado	:=obter_desc_expressao(347195) ||
								quebra_w ||quebra_w || '*Pacientes com menos de 40 anos, mas com fatores de risco adicionais, podem se beneficiar de profilaxia';
				--Inserir_Descricao_Idade;
				Inserir_Contra_Indicacao;
			end if;


		elsif	(:new.ie_risco	= 'A') then

			if	(ie_condicao_w	= 'N') and
				(qt_contra_ind_relat_w = 0) and -- Edilson
				(qt_contra_prof_mec_w = 0) then --Edilson
				:new.IE_QUIMIOPROFILAXIA := 'S';
				:new.IE_PROFILAXIA_MEC  := 'N';
				:new.ds_resultado	:= nvl(Obter_Texto_Tev(41),obter_desc_expressao(347169));
				Inserir_Medicacao;


				--:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||'Obs.: No estudo de Harenberg e cols, houve aumento na mortalidade no grupo que recebeu nadroparina, comparado com HNF (Haemostasis, 1993; 26(3):127-139';
			else
				Inserir_Contra_Indicacao;

			end if;

			Inserir_Descricao_Idade;
		end if;



	elsif	(:new.IE_CLINICO_CIRURGICO	= 'I')	and
		(ie_padrao_tev_w	= 'D'	)then


		qt_reg_w	:= 0;
		/*Alt�ssimo risco*/
		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_CIRUR_ORTOPEDICA	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_CIRUR_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRAUM_RAQUIMEDULARES	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		:new.IE_ALTISSIMO_RISCO	:= 'N';

		if	(qt_reg_w	> 0) then
			:new.IE_ALTISSIMO_RISCO	:= 'S';
		end if;

		qt_reg_w	:= 0;

		/*Alto risco*/
		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRAUMA_GRAVE	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_ICC	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;

		if	(:new.IE_CIRUR_GRANDE	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		:new.IE_ALTO_RISCO	:= 'N';
		if	(qt_reg_w	= 1) then
			:new.IE_ALTO_RISCO	:= '1';
		elsif	(qt_reg_w	= 2) then
			:new.ie_alto_risco	:= '2';
		elsif	(qt_reg_w	>= 3) then
			:new.ie_alto_risco	:= '3';
		end if;


		qt_reg_w	:= 0;

		/*Risco*/


		if	(:new.IE_INFECSAO_GRAVE	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_PARALISIA_INF	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_RESTRICAO_PROLONG	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_VARIZES	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_QUEIMADO	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_OBESIDADE	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_ANTICONCEPCIONAIS	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ECLAMPSIA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_PUERPERIO	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_INFLA_INTESTINAL	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;


		if	(:new.IE_INTERNACAO_UTI	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_IAM	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_DOENCA_AUTOIMUNE	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_SINDROME_NEFROTICA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_MEMBROS_IMOB	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRAUMA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_QUIMIOTERAPIA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRH	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_OUTROS	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;


		:new.ie_fator_risco	:= 'N';

		if	(qt_reg_w	= 1) then
			:new.ie_fator_risco	:= '1';
		elsif	(qt_reg_w between 2 and 4) then
			:new.ie_fator_risco	:= '2';
		elsif	(qt_reg_w	> 5) then
			:new.ie_fator_risco	:= '5';
		end if;


		if	(:new.IE_NENHUM_ALT_RISCO	= 'S') and
			(:new.qt_idade	> 60) then
			-- Paciente com mais de 60 anos. Considera-se alto risco. #@#@'
			Wheb_mensagem_pck.exibir_mensagem_abort(264632);
		end if;
		if	(ie_condicao_w	<> 'N') then
			:new.ds_resultado	:= nvl(Obter_Texto_Tev(42),'Paciente apresenta contra-indica��o ao uso de quimioprofilaxia para TEV. Os m�todos mec�nicos indicados neste caso s�o: meia el�stica de compress�o gradual (MECG) e compress�o pneum�tica intermitente (CPI). Os pacientes devem ser reavaliados em 2 dias.'||quebra_w);
		end if;
		Inserir_Descricao_Idade;

		if	(:new.IE_ALTISSIMO_RISCO	= 'N') and
			(:new.ie_alto_risco		= 'N') and
			(:new.ie_fator_risco		= 'N') then
			:new.ie_risco	:= 'B';
		elsif	(:new.IE_ALTISSIMO_RISCO	= 'S') or
			(:new.ie_alto_risco		in ('2','3'))then
			:new.IE_RISCO	:= 'A';
		elsif	(:new.ie_fator_risco	<> 'N')then
			:new.ie_risco	:= 'M';
		end if;

		Inserir_Medicacao;
	elsif	(ie_padrao_tev_w	= 'ALT') and
		(:new.ie_clinico_cirurgico	= 'I') then
		:new.ie_risco	:= 'B';

		/* INICIO RESPOSTA 2 */
		ie_cir_alto_risco_w		:= :new.ie_cir_alto_risco;
		ie_cir_pequeno_porte_w		:= :new.ie_cir_pequeno_porte;
		ie_cir_grande_medio_porte_w	:= :new.ie_cir_grande_medio_porte;


		ie_artroplastia_joelho_w	:= :new.ie_artroplastia_joelho;
		ie_artroplastia_quadril_w	:= :new.ie_artroplastia_quadril;
		ie_fratura_quadril_w		:= :new.ie_fratura_quadril;
		ie_oncol_curativa_w		:= :new.ie_oncol_curativa;
		ie_trauma_medular_w		:= :new.ie_trauma_medular;
		ie_politrauma_w			:= :new.ie_politrauma;
		/* FIM RESPOSTA 2 */

		/* INICIO RESPOSTA 3 */
		qt_reg_w	:= 0;
		if	(:new.IE_ABORTAMENTO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		/*
		if	(:new.IE_HIST_FAMILIAR_TEV	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		*/
		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_TABAGISMO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_ANTICONCEPCIONAIS	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_INFLA_INTESTINAL	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		/*
		if	(:new.IE_DOENCA_AUTOIMUNE	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		*/
		if	(:new.IE_IAM	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_INFECSAO_GRAVE	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_INSUF_ARTERIAL	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_ICC	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_INSUF_RESP	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_INTERNACAO_UTI	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_OBESIDADE	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_PARALISIA_INF	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		/*
		if	(:new.IE_PNEUMONIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		*/
		if	(:new.IE_PUERPERIO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_QUIMIOTERAPIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_SINDROME_NEFROTICA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_DOENCA_REUMATOL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TRH	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		:new.IE_FATOR_RISCO := 'N';
		if	(qt_reg_w	> 0) then
			:new.IE_FATOR_RISCO := 'S';
		end if;
		/* FIM RESPOSTA 3 */

		/* INICIO RESPOSTA 4 */
		qt_absoluta_w	:= 0;
		qt_relativa_w	:= 0;
		if	(:new.IE_HIPERSENSIBILIDADE_HEPARINA	= 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_SANGRAMENTO_ULCERA	= 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_ANTICOAGULACAO = 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_CIRURGIA_INTRACRANIANA	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_PUNCAO_LIQUORICA	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_COAGULOPATIA	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_HIPERTENSAO	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_INSUF_RENAL	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		/* FIM RESPOSTA 4 */

		/* INICIO RESPOSTA 5 */
		qt_questao_5_w := 0;
		if	(:new.IE_FRATURA_EXPOSTA	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_INFECCAO_MMII	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_INSUF_ART_MMII	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_INSUF_CARD_GRAVE	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_ESCARA	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		/* FIM RESPOTA 5 */

		:new.IE_RISCO	:= 'B';
		if	(:new.IE_CIR_PEQUENO_PORTE = 'S') then
			:new.IE_RISCO	:= 'B';
		elsif	((:new.IE_CIR_ALTO_RISCO	= 'S') 	or
			((:new.IE_CIR_GRANDE_MEDIO_PORTE = 'S')	and
			((:new.qt_idade	> 60)	or
			((:new.qt_idade	between 40 and 60 )and (qt_reg_w	> 0)))))	then
			:new.IE_RISCO	:= 'A';
		elsif	(:new.IE_CIR_GRANDE_MEDIO_PORTE = 'S') and
			((:new.qt_idade between 40 and 60) and
			(qt_reg_w	= 0)) or
			((:new.qt_idade	< 40) and
			(qt_reg_w	> 0)) then
			:new.IE_RISCO	:= 'M';
		elsif	((:new.qt_idade	< 40) and
			(qt_reg_w	= 0)) or
			(:new.IE_CIR_PEQUENO_PORTE in ('S')) then
			:new.IE_RISCO	:= 'B';
		end if;
		Inserir_Descricao_Idade;

		if	(ie_condicao_w	= 'N') and
			(:new.ie_risco	<> 'B') then
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.IE_PROFILAXIA_MEC  := 'N';
			:new.ds_resultado	:= nvl(Obter_Texto_Tev(43),obter_desc_expressao(347169));
			Inserir_Medicacao;

			--:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||'Obs.: No estudo de Harenberg e cols, houve aumento na mortalidade no grupo que recebeu nadroparina, comparado com HNF (Haemostasis, 1993; 26(3):127-139';
		else
			Inserir_Contra_Indicacao;
		end if;


		if	(:new.ie_risco	= 'B') then
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'N';
			:new.ds_resultado	:= nvl(Obter_Texto_Tev(44),obter_desc_expressao(347195));
		end if;

		/* Inicio Cirugia */

		inserir_resposta_cirurgia;

		/* Fim cirurgia */

	end if;

end if;

if	(:new.qt_peso is null) and
	(ie_padrao_tev_w	= 'ALT') then
	:new.ds_resultado := null;
end if;

if	(:new.IE_ANTICOAGULACAO	= 'S') and
	(ie_padrao_tev_w	= 'ALT') then
	:new.ds_resultado	:= nvl(Obter_Texto_Tev(45),obter_desc_expressao(599583));
end if;

if	(:new.IE_ANTICOAGULACAO	= 'S') and
	(ie_padrao_tev_w	= 'ALT') then
	:new.IE_NAO_SE_APLICA	:= 'S';
end if;

if	(:new.IE_NAO_SE_APLICA	= 'S') and
	(ie_padrao_tev_w	= 'ALT') then
	:new.ie_risco	:= 'N';

		:new.ds_resultado	:= nvl(Obter_Texto_Tev(46),obter_desc_expressao(347196));
	if	(:new.NR_SEQ_MOTIVO_TEV	is not null) then
		select	max(ds_motivo)
		into	ds_motivo_w
		from	tev_motivo
		where	nr_sequencia	= :new.NR_SEQ_MOTIVO_TEV;

		if	(ds_motivo_w	is not null) then
			:new.ds_resultado	:= :new.ds_resultado||chr(13) ||'    '|| obter_desc_expressao(293478) || ' ' || ds_motivo_w;
		end if;
	end if;


end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_tev_pend_atual
after insert or update ON TASY.ESCALA_TEV for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'57');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_TEV_delete
after delete ON TASY.ESCALA_TEV for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '57'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.HSJ_ESCALA_TEV_ATUAL
before insert or update ON TASY.ESCALA_TEV for each row
declare

qt_reg_w            number(10):=0;
dt_avaliacao_w      date;
nr_atendimento_w    number(10);

begin

    dt_avaliacao_w:= :new.dt_avaliacao;
    nr_atendimento_w:= :new.nr_atendimento;

    if(nvl(:new.ie_ci_inesp_1,'N') = 'N')then
    
        select count(*)
        into qt_reg_w
        from prescr_material t
        join prescr_medica y on (t.nr_prescricao = y.nr_prescricao)
        where y.nr_atendimento = nr_atendimento_w
        and y.dt_suspensao is null
        and to_date (y.dt_liberacao) between to_date(dt_avaliacao_w) - 1 and  to_date(dt_avaliacao_w)
        and cd_material in (select cd_material
                            from material
                            where ie_protocolo_tev = 'S');
        
        if(qt_reg_w > 0)then
            :new.ie_ci_inesp_1:='S';
            --:new.ie_quimioprofilaxia :='S';
            --:new.ie_profilaxia_mec :='S';
        end if;
    
    end if;
    
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_TEV_ATUAL
before insert or update ON TASY.ESCALA_TEV for each row
declare
qt_reg_w		number(10)  := 0;
quebra_w		varchar2(20)	:= Chr(13)||chr(10);
qt_imc_w		number(10,2);
ie_padrao_tev_w		varchar2(10)	:= 'D';
qt_contra_prof_mec_w	number(10)	:= 0;
qt_Nadroparina_w	number(10);
qt_dose_enoxa_w		number(18,6);
ds_intervalo_enoxa_w	varchar2(255);
ds_fonda_w				varchar2(255);
ds_dose_heparina_w		varchar2(255);
ds_intervalo_heparina_w	varchar2(255);
qt_absoluta_w			number(10)	:= 0;
qt_relativa_w			number(10)	:= 0;
nr_seq_resposta_w		number(10)	:= 0;
cd_estabelecimento_w	number(10)	:= 0;
ds_resposta_w			varchar2(4000);
qt_contra_indicacao_w	number(10);
ie_contra_ind_w			varchar2(1);
qt_resp_cad_w		number(10);
ds_resp_cad_w		varchar2(2000);
qt_cuidado_espec_w	number(10);





pragma autonomous_transaction;


	procedure	contagem_Contraindicacao is

	qt_reg_cad_w	number(10);
	nr_seq_cad_w	number(10);

	Cursor C01 is
		select	nr_sequencia
		from	tev_contraindicacao
		where	nvl(ie_exibir_estab,ie_exibir) = 'S'
		and	nvl(ie_tipo_contraindicacao_estab,ie_tipo_contraindicacao)  = 'A';

	Cursor C02 is
		select	nr_sequencia
		from	tev_contraindicacao
		where	nvl(ie_exibir_estab,ie_exibir) = 'S'
		and	nvl(ie_tipo_contraindicacao_estab,ie_tipo_contraindicacao)  = 'R';

	begin
	select	count(*)
	into	qt_reg_cad_w
	from	tev_contraindicacao
	where	nvl(ie_exibir_estab,ie_exibir) = 'S';
	--and	ie_tipo_contraindicacao_estab is not null;

	if	(qt_reg_cad_w = 0) then -- Cadastro SHIFT+F11  Contraindica��es TEV
		/*Contra indica��es absolutas: */
		if	(:new.IE_HIPERSENSIBILIDADE_HEPARINA = 'S') then
			qt_absoluta_w	 := qt_absoluta_w + 1;
		end if;

		if	(:new.IE_PLAQUETOPENIA_HEPARINA = 'S') then
			qt_absoluta_w	 := qt_absoluta_w + 1;
		end if;

		if	(:new.IE_SANGRAMENTO_ULCERA = 'S') then
			qt_absoluta_w	 := qt_absoluta_w + 1;
		end if;

		if	(:new.IE_ULCERA_ATIVA = 'S') then
			qt_absoluta_w	 := qt_absoluta_w + 1;
		end if;

		/*Contra indica��es relativas */
		if	(ie_contra_ind_w = 'S') then
			if	(:new.IE_CIRURGIA_INTRACRANIANA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(:new.IE_COAGULOPATIA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(:new.IE_PUNCAO_LIQUORICA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(:new.IE_HIPERTENSAO = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(:new.IE_INSUF_RENAL = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(:new.IE_CONT_POLITRAUMA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;
			if	(:new.IE_TRAUMA_CRANIANO = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;
		else
			if	(:new.IE_CIRURGIA_INTRACRANIANA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(:new.IE_COAGULOPATIA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(:new.IE_PUNCAO_LIQUORICA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(:new.IE_HIPERTENSAO = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(:new.IE_INSUF_RENAL = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(:new.IE_CONT_POLITRAUMA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(:new.IE_TRAUMA_CRANIANO = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;
		end if;
	else
		nr_seq_cad_w := 0;

		open C01;	/*  Absolutas */
		loop
		fetch C01 into
			nr_seq_cad_w;
		exit when C01%notfound;
			begin
			if	(nr_seq_cad_w = 13) and
				(:new.IE_PLAQUETOPENIA_HEPARINA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 14) and
				(:new.IE_SANGRAMENTO_ULCERA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 15) and
				(:new.IE_ULCERA_ATIVA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 12) and
				(:new.IE_HIPERSENSIBILIDADE_HEPARINA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 16) and
				(:new.IE_CIRURGIA_INTRACRANIANA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 18) and
				(:new.IE_PUNCAO_LIQUORICA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 17) and
				(:new.IE_COAGULOPATIA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 19) and
				(:new.IE_HIPERTENSAO = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 20) and
				--(:new.IE_INSUF_RENAL = 'S') then
				(:new.QT_CLEARENCE < 30) then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 24) and
				(:new.IE_CONT_POLITRAUMA = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 25) and
				(:new.IE_TRAUMA_CRANIANO = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 27) and
				(:new.IE_CI_INESP_1 = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 28) and
				(:new.IE_CI_INESP_2 = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;

			if	(nr_seq_cad_w = 29) and
				(:new.IE_CI_INESP_3 = 'S') then
				qt_absoluta_w	 := qt_absoluta_w + 1;
			end if;
			end;
		end loop;
		close C01;

		nr_seq_cad_w := 0;

		open C02;	/*  Relativas */
		loop
		fetch C02 into
			nr_seq_cad_w;
		exit when C02%notfound;
			begin
			if	(nr_seq_cad_w = 13) and
				(:new.IE_PLAQUETOPENIA_HEPARINA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 14) and
				(:new.IE_SANGRAMENTO_ULCERA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 15) and
				(:new.IE_ULCERA_ATIVA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 12) and
				(:new.IE_HIPERSENSIBILIDADE_HEPARINA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 16) and
				(:new.IE_CIRURGIA_INTRACRANIANA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 18) and
				(:new.IE_PUNCAO_LIQUORICA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 17) and
				(:new.IE_COAGULOPATIA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 19) and
				(:new.IE_HIPERTENSAO = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 20) and
				--(:new.IE_INSUF_RENAL = 'S') then
				(:new.QT_CLEARENCE < 30) then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 24) and
				(:new.IE_CONT_POLITRAUMA = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 25) and
				(:new.IE_TRAUMA_CRANIANO = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 27) and
				(:new.IE_CI_INESP_1 = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 28) and
				(:new.IE_CI_INESP_2 = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;

			if	(nr_seq_cad_w = 29) and
				(:new.IE_CI_INESP_3 = 'S') then
				qt_relativa_w	 := qt_relativa_w + 1;
			end if;
			end;
		end loop;
		close C02;

	end if;
	end;


	procedure contagem_prof_Mecanica is
	begin
		if	(:new.IE_ESCARA	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_FRATURA_EXPOSTA	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INFECCAO_MMII	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INSUF_ART_MMII	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INSUF_CARD_GRAVE	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;
	end;

	function obter_resp_tev_cad return varchar2 is

	ds_resp_w	varchar2(255);
	ds_desc_w	varchar2(255);
	ds_retorno_w	varchar2(2000);
	nr_seq_w	number(10);
	ie_concatena_w	varchar2(1);

	Cursor C01 is
		select	nvl(ds_contraindicacao_estab,ds_contraindicacao),
			ds_resposta,
			nr_sequencia
		from	tev_contraindicacao
		where	nvl(ie_exibir_estab,ie_exibir) = 'S'
		--and	ie_tipo_contraindicacao_estab is not null
		order by nr_sequencia;

	begin
	select	count(*)
	into	qt_reg_w
	from	tev_contraindicacao
	where	nvl(ie_exibir_estab,ie_exibir) = 'S'
	and	ie_tipo_contraindicacao_estab is not null
	and	trim(ds_resposta) is not null;

	if	(qt_reg_w > 0) then

		open C01;
		loop
		fetch C01 into
			ds_desc_w,
			ds_resp_w,
			nr_seq_w;
		exit when C01%notfound;
			begin
			-- VERIFICA SE O CAMPO(CHECKBOX) FOI MARCADO
			ie_concatena_w := 'N';
			if	(nr_seq_w = 12) and
				(:new.IE_HIPERSENSIBILIDADE_HEPARINA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 13) and
				(:new.IE_PLAQUETOPENIA_HEPARINA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 14) and
				(:new.IE_SANGRAMENTO_ULCERA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 15) and
				(:new.IE_ULCERA_ATIVA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 16) and
				(:new.IE_CIRURGIA_INTRACRANIANA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 17) and
				(:new.IE_COAGULOPATIA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 18) and
				(:new.IE_PUNCAO_LIQUORICA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 19) and
				(:new.IE_HIPERTENSAO = 'S') then
				ie_concatena_w := 'S';
			/*elsif	(nr_seq_w = 20) and
				(:new.IE_INSUF_RENAL = 'S') then
				ie_concatena_w := 'S';*/
			elsif	(nr_seq_w = 24) and
				(:new.IE_CONT_POLITRAUMA = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 25) and
				(:new.IE_TRAUMA_CRANIANO = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 27) and
				(:new.IE_CI_INESP_1 = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 28) and
				(:new.IE_CI_INESP_2 = 'S') then
				ie_concatena_w := 'S';
			elsif	(nr_seq_w = 29) and
				(:new.IE_CI_INESP_3 = 'S') then
				ie_concatena_w := 'S';
			end if;

			if	(ie_concatena_w = 'S') then
				ds_retorno_w := ds_retorno_w || chr(13) ||
						ds_desc_w || chr(13) ||
						ds_resp_w;
			end if;
			end;
		end loop;
		close C01;
	end if;

	return	ds_retorno_w;

	end;

	function Concatena_cuidados_especiais return varchar2 is

	ds_cuidados_espec_w	varchar2(2000);
	ds_retorno_w		varchar2(2000);

	Cursor C01 is
		select	ds_cuidados
		from	tev_cuidados_especiais
		order by nr_sequencia;

	begin
	open C01;
	loop
	fetch C01 into
		ds_cuidados_espec_w;
	exit when C01%notfound;
		begin
		if	(Trim(ds_retorno_w) is null) then
			ds_retorno_w := substr(ds_cuidados_espec_w,1,2000);
		else
			ds_retorno_w := substr(ds_retorno_w || chr(13) || ds_cuidados_espec_w,1,2000);
		end if;
		end;
	end loop;
	close C01;

	return ds_retorno_w;

	end;

begin
select	count(*)
into	qt_cuidado_espec_w
from	tev_cuidados_especiais;

if	(qt_cuidado_espec_w > 0) then
	:new.DS_CUIDADOS := Concatena_cuidados_especiais;
end if;

ie_padrao_tev_w	:= nvl(:new.ie_padrao,'D');

obter_param_usuario(697,2,obter_perfil_ativo,:new.nm_usuario,obter_estabelecimento_ativo,ie_contra_ind_w);

if	(:new.QT_CLEARENCE <=30) then
	:new.IE_INSUF_RENAL := 'S';
end if;

if	(ie_padrao_tev_w	= 'D') then
	:new.IE_IDADE_MAIOR	:='N';
	if	(:new.qt_idade	>= 55) then
		:new.IE_IDADE_MAIOR	:= 'S';
	end if;

	qt_imc_w	:= dividir(:new.qt_peso,(:new.qt_altura_cm /100) * (:new.qt_altura_cm /100));
	if	(qt_imc_w	>=30 ) then
		:new.IE_OBESIDADE	:= 'S';
	end if;
	:new.IE_CONDICAO	:= 'N';
	qt_reg_w		:= 0;
	if	(:new.IE_SANGRAMENTO_ULCERA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_SANGRAMENTO_ULCERA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_PLAQUETOPENIA_HEPARINA	=
	  'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_COAGULOPATIA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_ULCERA_ATIVA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_HIPERTENSAO	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_INSUF_RENAL	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_CIRURGIA_INTRACRANIANA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_HIPERSENSIBILIDADE_HEPARINA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_ANTICOAGULACAO	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(qt_reg_w	> 0) then
		:new.IE_CONDICAO	:= 'S';
	end if;
	:new.ds_resultado	:= null;
	qt_reg_w	:= 0;


	contagem_Contraindicacao;
	contagem_prof_Mecanica;
	qt_contra_indicacao_w	:= qt_absoluta_w + qt_relativa_w;
	if	(:new.IE_CLINICO_CIRURGICO	= 'C') then
		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_IDADE_MAIOR	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_ECLAMPSIA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DOENCA_REUMATOL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_HIST_FAMILIAR_TEV	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ANTICONCEPCIONAIS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PUERPERIO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_OBESIDADE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INTERNACAO_UTI	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PARALISIA_INF	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_GRAVIDEZ	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_INSUF_RESP	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INSUF_ARTERIAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TRH	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ICC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PNEUMONIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFLA_INTESTINAL= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_VARIZES	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFECSAO_GRAVE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_QUIMIOTERAPIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_SINDROME_NEFROTICA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DOENCA_AUTOIMUNE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ABORTAMENTO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRAUMA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_IAM	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_QUEIMADO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_OUTROS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		:new.IE_FATOR_RISCO:='N';
		if	(qt_reg_w	> 0) and
			(:new.IE_MOBILIDADE_REDUZIDA	= 'S')then
			:new.IE_FATOR_RISCO := 'S';
		end if;


		if	(:new.qt_idade	< 40) and
			(:new.IE_MOBILIDADE_REDUZIDA = 'S') and
			(:new.IE_FATOR_RISCO = 'S')then
			:new.ie_risco	:= 'N';

		elsif(:new.IE_FATOR_RISCO	= 'N') then
			:new.ie_risco	:= 'B';
		elsif	(:new.IE_FATOR_RISCO	= 'S') then
			:new.ie_risco	:= 'A';

			if	(:new.qt_idade	< 40) and
				(:new.ie_condicao	= 'N') then
				:new.ie_risco	:= 'B';
			end if;
		end if;
		:new.ds_resultado	:= Null;

		nr_seq_resposta_w	:= 0;


		if	(:new.ie_risco	= 'N') then
			nr_seq_resposta_w	:= 71;
		end if;


		if	(nvl(:new.IE_RISCO_RECLASSIF,:new.IE_RISCO)	= 'B') then

			if	(:new.qt_idade >= 40) and
				(:new.IE_MOBILIDADE_REDUZIDA = 'S') and
				(qt_reg_w	= 0) then
				nr_seq_resposta_w	:= 69;
			elsif	(:new.qt_idade <= 40) and
					(:new.IE_MOBILIDADE_REDUZIDA = 'S') then
				nr_seq_resposta_w	:= 73;
			else
				nr_seq_resposta_w	:= 74;
			end if;


		elsif	(nvl(:new.IE_RISCO_RECLASSIF,:new.IE_RISCO)	= 'A') then



			if	((:new.qt_idade > 75) or
				 (:new.qt_peso <50) or
				 (:new.QT_CLEARENCE	<30)) then

				 if	(qt_contra_indicacao_w	> 0) and
					(qt_contra_prof_mec_w	> 0) then
					nr_seq_resposta_w	:= 63;
				elsif	(qt_contra_indicacao_w	> 0) then
					nr_seq_resposta_w	:= 64;
				else
					nr_seq_resposta_w	:= 59;
				end if;
			elsif	(:new.qt_imc	> 40) then
				nr_seq_resposta_w	:= 58;
			else
				if	(qt_contra_indicacao_w	> 0) and
					(qt_contra_prof_mec_w	> 0) then
					nr_seq_resposta_w	:= 62;
				elsif	(qt_contra_indicacao_w	> 0) then
					nr_seq_resposta_w	:= 75;
				else
					nr_seq_resposta_w	:= 58;
				end if;
			end if;



		end if;




	elsif	(:new.IE_CLINICO_CIRURGICO	= 'I') then
		:new.IE_RISCO	:= 'B';

		qt_reg_w	:= 0;

		if	(:new.IE_ABORTAMENTO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_HIST_FAMILIAR_TEV	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_TABAGISMO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_ANTICONCEPCIONAIS	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_INFLA_INTESTINAL	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.ie_tabagismo	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_DOENCA_AUTOIMUNE	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_IAM	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_INFECSAO_GRAVE	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_INSUF_ARTERIAL	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end
	  if;

		if	(:new.IE_ICC	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_INSUF_RESP	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_INTERNACAO_UTI	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_OBESIDADE	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_PARALISIA_INF	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_PNEUMONIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_PUERPERIO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_QUIMIOTERAPIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_SINDROME_NEFROTICA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;

		if	(:new.IE_VARIZES	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;


		if	(:new.IE_ARTROPLASTIA_JOELHO = 'S' or
			:new.IE_ARTROPLASTIA_QUADRIL	= 'S' or
			:new.IE_FRATURA_QUADRIL	= 'S' or
			:new.IE_ONCOL_CURATIVA = 'S' or
			:new.IE_TRAUMA_MEDULAR = 'S' or
			:new.IE_POLITRAUMA = 'S') then
			:new.IE_CIR_ALTO_RISCO := 'S';
		end if;

		if	(:new.IE_BARIATRICA = 'S') or
			(:new.IE_DEMAIS_CIRURGIAS = 'S') then
			:new.IE_CIR_GRANDE_MEDIO_PORTE := 'S';
		end if;




		:new.IE_RISCO	:= 'B';
		:new.IE_RISCO	:= 'B';

		if	(:new.IE_CIR_PEQUENO_PORTE = 'S') then
			:new.IE_RISCO	:= 'B';
		elsif	((:new.IE_CIR_ALTO_RISCO	= 'S') 	or
			((:new.IE_CIR_GRANDE_MEDIO_PORTE = 'S')	and
			((:new.qt_idade	> 60)	or
			((:new.qt_idade	between 40 and 60 )and (qt_reg_w	> 0)))))	then
			:new.IE_RISCO	:= 'A';
		elsif	(:new.IE_CIR_GRANDE_MEDIO_PORTE = 'S') and
			((:new.qt_idade between 40 and 60) and
			(qt_reg_w	= 0)) or
			((:new.qt_idade	< 40) and
			(qt_reg_w	> 0) or
			(:new.IE_BARIATRICA = 'S')) then
			:new.IE_RISCO	:= 'M';
		elsif	((:new.qt_idade	< 40) and
			(qt_reg_w	= 0)) or
			(:new.IE_CIR_PEQUENO_PORTE in ('S')) then
			:new.IE_RISCO	:= 'B';
		end if;

		if	(:new.ie_risco	= 'B') then
			nr_seq_resposta_w	:= 7;
		elsif	(:new.ie_risco	= 'M') then

			if	(:new.IE_BARIATRICA = 'S') then

				if	(qt_contra_prof_mec_w	> 0) and
					(qt_absoluta_w	> 0) then
					nr_seq_resposta_w	:= 15;
				elsif	(qt_contra_prof_mec_w	> 0) and
						(qt_relativa_w	> 0) then
					nr_seq_resposta_w	:= 14;
				elsif	(qt_absoluta_w	> 0) then
					nr_seq_resposta_w	:= 13;
				elsif	(qt_relativa_w	> 0) then
					nr_seq_resposta_w	:= 38;
				else
					nr_seq_resposta_w	:= 9;
				end if;
			else
				if	(qt_contra_prof_mec_w	> 0) and
					(qt_absoluta_w	> 0) then
					nr_seq_resposta_w	:= 5; --ok
				elsif	(qt_contra_prof_mec_w	> 0) and
						(qt_relativa_w	> 0) then
					nr_seq_resposta_w	:= 6; --ok
				elsif	(qt_absoluta_w	> 0) then
					nr_seq_resposta_w	:= 3; --ok
				elsif	(qt_relativa_w	> 0) then
					nr_seq_resposta_w	:= 4; --ok
				else
					nr_seq_resposta_w	:= 1; --ok
				end if;
			end if;
		elsif	(:new.ie_risco	= 'A') then



			if	(:new.IE_CIR_GRANDE_MEDIO_PORTE	= 'S') then


				if	(:new.IE_BARIATRICA = 'S') then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 38;--ok
					else
						nr_seq_resposta_w	:= 39;--ok
					end if;
				else

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 12;
					else
						nr_seq_resposta_w	:= 9; --ok
					end if;

				end if;

			elsif	(:new.IE_ARTROPLASTIA_QUADRIL = 'S') then



				if (:new.QT_CLEARENCE	< 30) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;  -- Ok
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 18; --ok
					else
						nr_seq_resposta_w	:= 42; --ok
					end if;

				elsif 	(:new.qt_idade> 75) or
					(:new.QT_CLEARENCE between 30 and 50) or
					(:new.qt_peso < 50) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 20; --ok
					else
						nr_seq_resposta_w	:= 41; --ok
					end if;

				else


					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 16;
					else
						nr_seq_resposta_w	:= 40;
					end if;
				end if;

			elsif	(:new.IE_ARTROPLASTIA_JOELHO = 'S') then

				if (:new.QT_CLEARENCE	< 30) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 23; --ok
					else
						nr_seq_resposta_w	:= 46;--ok
					end if;

				elsif 	(:new.qt_idade> 75) or
					(:new.QT_CLEARENCE between 30 and 50) or
					(:new.qt_peso < 50) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 24; --ok
					else
						nr_seq_resposta_w	:= 45; --ok
					end if;

				else


					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 22; --ok
					else
						nr_seq_resposta_w	:= 44; --ok
					end if;

			end if;

			elsif	(:new.IE_FRATURA_QUADRIL = 'S') then

				if 	(:new.qt_idade> 75) or
					(:new.QT_CLEARENCE <30) or
					(:new.qt_peso < 50) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 27;--ok
					else
						nr_seq_resposta_w	:= 49; --ok
					end if;

				else


					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 26; --ok
					else
						nr_seq_resposta_w	:= 48; --ok
					end if;

				end if;
			elsif	(:new.IE_TRAUMA_MEDULAR = 'S') then


				if 	(:new.qt_idade> 75) or
					(:new.QT_CLEARENCE <30) or
					(:new.qt_peso < 50) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 29; --ok
					else
						nr_seq_resposta_w	:= 51; --ok
					end if;


				else


					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 28; --ok
					else
						nr_seq_resposta_w	:= 50; --ok
					end if;

				end if;

			elsif	(:new.IE_POLITRAUMA = 'S') then

				if 	(:new.qt_idade> 75) or
					(:new.QT_CLEARENCE <30) or
					(:new.qt_peso < 50) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:=35;--ok
					else
						nr_seq_resposta_w	:= 53; --ok
					end if;

				else


					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 30;--ok
					else
						nr_seq_resposta_w	:= 52;--ok
					end if;

				end if;
			elsif	(:new.IE_ONCOL_CURATIVA = 'S') then

				if 	(:new.qt_idade> 75) or
					(:new.QT_CLEARENCE <30) or
					(:new.qt_peso < 50) then

					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:=37; --ok
					else
						nr_seq_resposta_w	:= 55;--ok
					end if;

				else


					if	(qt_contra_prof_mec_w	> 0) and
						(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 15;
					elsif	(qt_contra_prof_mec_w	> 0) and
							(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 14;
					elsif	(qt_absoluta_w	> 0) then
						nr_seq_resposta_w	:= 13;
					elsif	(qt_relativa_w	> 0) then
						nr_seq_resposta_w	:= 36; --ok
					else
						nr_seq_resposta_w	:= 54; --ok
					end if;

				end if;


			end if;
		end if;

	end if;

end if;

if	(nr_seq_resposta_w	> 0) then

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;


	select	max(ds_resposta)
	into	ds_resposta_w
	from	TEV_RESPOSTA_ESTAB
	where	nr_seq_resposta	= nr_seq_resposta_w
	and		cd_estabelecimento = cd_estabelecimento_w;

	if	(trim(ds_resposta_w) is null) then

		select	max(ds_resposta)
		into	ds_resposta_w
		from	tev_resposta
		where	nr_sequencia	= nr_seq_resposta_w;
	end if;

	select	count(*)
	into	qt_resp_cad_w
	from	tev_contraindicacao
	where	nvl(ie_exibir_estab,ie_exibir) = 'S';

	if	(qt_resp_cad_w > 0) then
		ds_resposta_w := substr(ds_resposta_w || obter_resp_tev_cad,1,2000);
	end if;


	:new.ds_resultado	:= ds_resposta_w;
	:new.nr_seq_resposta  := nr_seq_resposta_w;

end if;


if	(:new.IE_CLINICO_CIRURGICO	= 'C') then
	if	(:new.IE_RISCO	= 'B') then
		:new.qt_pontuacao	:= 1;
	elsif	(:new.ie_risco	= 'M') then
		:new.qt_pontuacao	:= 2;
	elsif	(:new.ie_risco	= 'A') then
		:new.qt_pontuacao	:= 3;
	end if;
else
	if	(:new.IE_RISCO	= 'B') then
		:new.qt_pontuacao	:= 4;
	elsif	(:new.ie_risco	= 'M') then
		:new.qt_pontuacao	:= 5;
	elsif	(:new.ie_risco	= 'A') then
		:new.qt_pontuacao	:= 6;
	end if;
end if;

begin
:new.cd_setor_atendimento := obter_Setor_atendimento(:new.nr_atendimento);
exception
when others then
null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_TEV_HSL_ATUAL
before insert or update ON TASY.ESCALA_TEV for each row
declare
qt_reg_w		number(10)  := 0;
quebra_w		varchar2(20)	:= Chr(13)||chr(10);
qt_imc_w		number(10,2);
ie_padrao_tev_w		varchar2(10)	:= 'D';
qt_contra_prof_mec_w	number(10)	:= 0;
qt_Nadroparina_w	varchar2(50);
qt_contra_ind_relat_w	number(10)	:= 0;
ie_condicao_w		varchar2(1);

ie_cir_alto_risco_w		varchar2(1);
ie_cir_pequeno_porte_w		varchar2(1);
ie_cir_grande_medio_porte_w	varchar2(1);
qt_absoluta_w			number(10,0) := 0;
qt_relativa_w			number(10,0) := 0;
qt_questao_5_w			number(10,0);

ie_artroplastia_joelho_w	varchar2(1);
ie_artroplastia_quadril_w	varchar2(1);
ie_fratura_quadril_w		varchar2(1);
ie_oncol_curativa_w		varchar2(1);
ie_trauma_medular_w		varchar2(1);
ie_politrauma_w	 		varchar2(1);

ds_texto_w			varchar2(4000);
ds_motivo_w			varchar2(255);

pragma autonomous_transaction;

	procedure	Inserir_Descricao_Idade is
	begin

	if	(:new.qt_idade	< 40) then
		:new.ds_resultado:= :new.ds_resultado||Wheb_mensagem_pck.get_texto(308168)||quebra_w||Wheb_mensagem_pck.get_texto(308169);

							/*:new.ds_resultado||'Pacientes com idade menor de 40 anos e sem mobilidade reduzida n�o necessitam rotineiramente de quimioprofilaxia ou m�todos f�sicos para preven��o de TEV.'||quebra_w||
							'Sua avalia��o de risco � individualizada, recomendando-se incentivo � deambula��o e reavalia��o posterior, caso permane�a internado.';*/

	end if;
	end;

	procedure inserir_texto_tipo_cirur is

	begin
	ds_texto_w := null;

	if	((ie_artroplastia_quadril_w = 'S') or (ie_artroplastia_joelho_w = 'S')) then
		if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S') then
			ds_texto_w := ds_texto_w ||  Wheb_mensagem_pck.get_texto(308170) /*'Op��o em artroplastia quadril/joelho:'*/ || quebra_w || quebra_w ||Wheb_mensagem_pck.get_texto(308171) /*'Rivaroxabana VO 10 mg 1 X ao dia .'*/ || quebra_w || quebra_w;
		else
			ds_texto_w := ds_texto_w ||  Wheb_mensagem_pck.get_texto(308170) /*'Op��o em artroplastia quadril/joelho:'*/ || quebra_w || quebra_w || Wheb_mensagem_pck.get_texto(308172) || ' ' /*'Dabigatrana VO 220 mg 1 X ao dia (110 mg na 1� dose, iniciando 1 a 4 h ap�s a cirurgia) '*/ ||
							Wheb_mensagem_pck.get_texto(308175)/*'ou Rivaroxabana VO 10 mg 1 X ao dia (iniciando 6 a 8 h ap�s a cirurgia).'*/ || quebra_w || quebra_w;
		end if;
	end if;

	if	(ie_cir_grande_medio_porte_w = 'S') then
		ds_texto_w	:= ds_texto_w || Wheb_mensagem_pck.get_texto(308177) /*'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.'*/ || quebra_w;
	end if;

	if	(ie_fratura_quadril_w = 'S') then
		ds_texto_w	:= ds_texto_w || Wheb_mensagem_pck.get_texto(308178) /*'O uso desta quimioprofilaxia deve ser mantido por 4 a 5 semanas.'*/ || quebra_w;
	end if;

	if	(ie_oncol_curativa_w = 'S') then
		ds_texto_w	:= ds_texto_w || Wheb_mensagem_pck.get_texto(308180) /*'O uso desta quimioprofilaxia deve ser mantido por 3 a 4 semanas.'*/ || quebra_w;
	end if;

	if	((ie_trauma_medular_w = 'S') or (ie_politrauma_w = 'S')) then
		ds_texto_w	:= ds_texto_w || Wheb_mensagem_pck.get_texto(308181) /*'O uso desta quimioprofilaxia deve ser mantido at� a recupera��o do paciente.'*/ || quebra_w;
	end if;

	if	(ie_artroplastia_quadril_w = 'S') then
		ds_texto_w	:= ds_texto_w || Wheb_mensagem_pck.get_texto(308178) /*'O uso desta quimioprofilaxia deve ser mantido por 4 a 5 semanas.'*/ || quebra_w;
	end if;

	if	(ie_artroplastia_joelho_w = 'S') then
		ds_texto_w	:= ds_texto_w || Wheb_mensagem_pck.get_texto(308184) /*'O uso desta quimioprofilaxia deve ser mantido por pelo menos 10 dias.'*/ || quebra_w;
	end if;

	:new.ds_resultado	:= :new.ds_resultado || ds_texto_w;

	end;

	procedure inserir_texto_adic_cir is

	begin
	ds_texto_w := 	quebra_w || quebra_w ||	Wheb_mensagem_pck.get_texto(308188);

			/*'Pacientes provenientes do centro cir�rgico com cateter peridural podem receber profilaxia medicamentosa,'||
			' desde que o cateter tenha sido posicionado h� pelo menos 2 horas. Para a retirada do cateter,'||
			' deve-se aguardar pelo menos 12 horas desde a �ltima dose da medica��o e pelo menos 2 horas ap�s sua retirada,'||
			' para se administrar a pr�xima dose, respeitando o hor�rio de administra��o de cada medica��o.';*/

	:new.ds_resultado	:= :new.ds_resultado || ds_texto_w;
	end;

	procedure Inserir_Medicacao is

	begin

	if	(:new.ie_risco	= 'A') then
		if	(:new.qt_peso	< 70) then
			qt_Nadroparina_w	:= '3.800';
		else
			qt_Nadroparina_w	:= '5.700';
		end if;
		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||Wheb_mensagem_pck.get_texto(308190)||' '||quebra_w||
								Wheb_mensagem_pck.get_texto(308192)||' '||quebra_w||
								Wheb_mensagem_pck.get_texto(308193)||' '||qt_Nadroparina_w||' '||Wheb_mensagem_pck.get_texto(308194)||quebra_w||
								Wheb_mensagem_pck.get_texto(308196)|| quebra_w || quebra_w||
								obter_desc_expressao(891564);

						/*:new.ds_resultado||quebra_w||quebra_w||'Enoxaparina 40 mg SC 1 X ao dia ou '||quebra_w||
						'Dalteparina 5.000 UI SC 1 X ao dia ou '||quebra_w||
						'Nadroparina '||qt_Nadroparina_w||' UI SC 1 X ao dia ou'||quebra_w||
						'Heparina n�o fracionada(HNF) 5.000 UI SC 3 X ao dia.';*/

	elsif	(:new.ie_risco	= 'M') then

		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||Wheb_mensagem_pck.get_texto(308197)||quebra_w||
							Wheb_mensagem_pck.get_texto(308198)||quebra_w||
							Wheb_mensagem_pck.get_texto(308199)||quebra_w||
							Wheb_mensagem_pck.get_texto(308200);

						/*:new.ds_resultado||quebra_w||quebra_w||'Enoxaparina 20 mg SC 1 vez ao dia ou'||quebra_w||
						'Dalteparina 2500 UI SC 1 vez ao dia ou'||quebra_w||
						'Nadroparina 1900-3800 UI SC 1 vez ao dia ou'||quebra_w||
						'Heparina n�o fracionada (HNF) 5000 UI SC de 12/12 horas.';*/

	end if;

	if	(:new.ie_risco in ('A','M')) then
		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||Wheb_mensagem_pck.get_texto(308201); --'O uso desta quimioprofilaxia deve ser mantido por 6 a 14 dias ou enquanto persistir o risco.';
	end if;
	end;


	procedure Inserir_Contra_Indicacao is

	begin

	if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S') then
		:new.ie_risco := 'A';
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.IE_PROFILAXIA_MEC   := 'S';


		:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308209)|| quebra_w || quebra_w ||
					Wheb_mensagem_pck.get_texto(308210) || quebra_w ||quebra_w||
					Wheb_mensagem_pck.get_texto(308211) || quebra_w || quebra_w||
					Wheb_mensagem_pck.get_texto(308213) ||' '||quebra_w || quebra_w||
					Wheb_mensagem_pck.get_texto(308214) || ' ';

					/*'Como o paciente apresenta contra-indica��o absoluta ao uso de heparinas, '||
		'recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), '||
		'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).'|| quebra_w || quebra_w ||
					'Optando-se pela quimioprofilaxia recomenda-se:' || quebra_w ||quebra_w||
					'Fondaparina (Arixtra) 2,5 mg SC 1 x ao dia.' || quebra_w || quebra_w||
					'Op��o para casos de artroplastia de quadril e joelho: '||quebra_w || quebra_w||
					'Rivaroxabana (Xarelto) 10 mg VO 1 x ao dia. ';*/

		--inserir_texto_adic_cir;
	elsif	(:new.IE_INSUF_RENAL	= 'S') then
		:new.ie_risco := 'A';
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.IE_PROFILAXIA_MEC   := 'S';
		:new.ds_resultado := Wheb_mensagem_pck.get_texto(308216) || quebra_w || quebra_w ||
					obter_desc_expressao(891552) || quebra_w || quebra_w ||
					Wheb_mensagem_pck.get_texto(308218) || quebra_w ||quebra_w||
					Wheb_mensagem_pck.get_texto(308219) || quebra_w || quebra_w||
					Wheb_mensagem_pck.get_texto(308201) || quebra_w || quebra_w;

					/*'Como o paciente apresenta contra-indica��o relativa ao uso de ' ||
					'quimioprofilaxia para TEV, considerar o risco/benef�cio de sua ' ||
					'introdu��o ou utilizar m�todos f�sicos como meia el�stica de ' ||
					'compress�o gradual (MECG), dispositivos mec�nicos de compress�o ' ||
					'pneum�tica intermitente (CPI) ou bomba plantar (BP).' || quebra_w || quebra_w ||
					'Os m�todos de profilaxia mec�nica sugeridos devem ser mantidos por 18 horas, pelo menos, ao longo do dia.' ||quebra_w ||quebra_w
					'Optando-se pelo uso da heparina recomenda-se:' || quebra_w ||quebra_w||
					'Heparina n�o fracionada HNF 5000 UI 2x ao dia e controle do TTPA.' || quebra_w || quebra_w||
					'O uso desta quimioprofilaxia deve ser mantido por 6 a 14 dias ou enquanto persistir o risco.' || quebra_w || quebra_w;*/

		--inserir_texto_tipo_cirur;
		--inserir_texto_adic_cir;
	elsif	(ie_condicao_w	= 'S') and
		(qt_contra_prof_mec_w	= 0)then
		:new.IE_QUIMIOPROFILAXIA := 'N';
		:new.IE_PROFILAXIA_MEC    := 'S';
		:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308223);

		                /*'Como o paciente apresenta contra-indica��o absoluta ao uso de quimioprofilaxia para TEV, '||
						'recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), '||
						'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).';*/

	elsif	(qt_contra_ind_relat_w	> 0) and
		(ie_condicao_w	= 'N') and
		(qt_contra_prof_mec_w	= 0) then
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.IE_PROFILAXIA_MEC    := 'S';
		:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308216) || quebra_w || quebra_w || obter_desc_expressao(891552);

						/*'Como o paciente apresenta contra-indica��o relativa ao uso de quimioprofilaxia para TEV, '||
						'considerar o risco/benef�cio de sua introdu��o ou utilizar m�todos f�sicos como meia el�stica de compress�o gradual (MECG), '||
						'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).';
						'Os m�todos de profilaxia mec�nica sugeridos devem ser mantidos por 18 horas, pelo menos, ao longo do dia.' ||quebra_w ||quebra_w*/


		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||Wheb_mensagem_pck.get_texto(308238); --'Optando-se pela quimioprofilaxia:'; -- edilson
		Inserir_Medicacao; -- edilson
	elsif	(qt_contra_ind_relat_w	> 0) and --Edilson
		(ie_condicao_w	= 'N'	) and
		(qt_contra_prof_mec_w	> 0) then
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.IE_PROFILAXIA_MEC   := 'A';
		:new.ds_resultado	:= Wheb_mensagem_pck.get_texto(308232) || ' ';

						/*'Como este paciente apresenta contra-indica��o relativa para o uso de quimioprofilaxia e para m�todos f�sicos, '||
						'deve-se avaliar individualmente o risco/benef�cio de sua aplica��o ou n�o. ';*/

		:new.ds_resultado	:= :new.ds_resultado ||quebra_w||quebra_w|| Wheb_mensagem_pck.get_texto(308234) || ' ' /*'Optando-se pela profilaxia mec�nica: '*/||quebra_w;
		:new.ds_resultado	:= :new.ds_resultado	||Wheb_mensagem_pck.get_texto(308236) || ' ' /*'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), '*/||
						Wheb_mensagem_pck.get_texto(308237) || ' ' || /*'dispositivos mec�nicos de compress�o ';*/
						quebra_w || quebra_w || obter_desc_expressao(891552); /*'Os m�todos de profilaxia mec�nica sugeridos devem ser mantidos por 18 horas, pelo menos, ao longo do dia.' */
		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||Wheb_mensagem_pck.get_texto(308238); /*'Optando-se pela quimioprofilaxia:';*/ -- edilson
		Inserir_Medicacao; -- edilson

	elsif	(ie_condicao_w	= 'S') and
		(qt_contra_prof_mec_w > 0) then
		:new.IE_QUIMIOPROFILAXIA := 'N';
		:new.IE_PROFILAXIA_MEC   := 'A';
		:new.ds_resultado	:= Wheb_mensagem_pck.get_texto(308239);

						/*'Como este paciente apresenta contra-indica��o absoluta para o uso de quimioprofilaxia e relativa para m�todos f�sicos, '||
						'a quimioprofilaxia n�o deve ser utilizada e deve-se avaliar individualmente o risco/benef�cio da profilaxia mec�nica. ';*/
--		:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||'Optando-se pela quimioprofilaxia a recomenda��o �:';
--		Inserir_Medicacao;

		:new.ds_resultado	:= :new.ds_resultado ||quebra_w||quebra_w|| Wheb_mensagem_pck.get_texto(308234) || ' ' /*'Optando-se pela profilaxia mec�nica: '*/||quebra_w;
		:new.ds_resultado	:= :new.ds_resultado	||Wheb_mensagem_pck.get_texto(308236) || ' ' /*'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), '*/||
						Wheb_mensagem_pck.get_texto(308246); /*'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).';*/
	end if;
	end;



	procedure inserir_resposta_cirurgia is

	begin

	:new.IE_QUIMIOPROFILAXIA := 'A';
	:new.IE_PROFILAXIA_MEC    := 'A';

	if	(ie_cir_pequeno_porte_w = 'S') then
		:new.ie_risco := 'B';
		:new.IE_PROFILAXIA_MEC := 'A';
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308247);

					/*'Pacientes submetidos a procedimentos cir�rgicos considerados de baixo risco para TEV podem ainda assim apresentar trombose, ' ||
					'particularmente aqueles mais idosos ou com fatores de risco associados, como c�ncer, hist�ria pr�via ou familiar de TEV, ou ' ||
					'mesmo associa��es de v�rios fatores comuns, como tabagismo, varizes, obesidade, reposi��o hormonal, etc. Nestes pacientes, a avalia��o ' ||
					'de risco deve ser individualizada, podendo-se optar pela prescri��o da profilaxia.Estimular Deambula��o; Heparina n�o indicada.';*/

	elsif	(ie_cir_grande_medio_porte_w = 'S') and
		(:new.qt_idade	< 40) and
		(qt_reg_w > 0) then
		if 	((qt_relativa_w + qt_absoluta_w) = 0) then
			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.IE_PROFILAXIA_MEC    := 'A';
			:new.ie_risco := 'M';
			:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308248) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308249) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308250) || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' ||qt_Nadroparina_w|| ' ' || Wheb_mensagem_pck.get_texto(308194) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308252) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308177);

						/*'A quimioprofilaxia recomendada nesta situa��o cl�nica �: ' || quebra_w || quebra_w ||
						'Enoxaparina 20 mg SC 1 X ao dia ou ' || quebra_w ||
						'Dalteparina 2.500 UI 1 X ao dia ou' || quebra_w ||
						'Nadroparina '||qt_Nadroparina_w||' UI SC 1 X ao dia ou ' || quebra_w ||
						'Heparina n�o fracionada(HNF) 5.000 UI 2 X ao dia.' || quebra_w || quebra_w ||
						'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.';*/

						inserir_texto_adic_cir;
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w = 1) and
			(qt_questao_5_w = 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC    := 'S';
			:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308223);

						/*'Como o paciente apresenta contra-indica��o absoluta ao uso de quimioprofilaxia para TEV, recomendam-se m�todos ' ||
						'f�sicos como meia el�stica de compress�o gradual (MECG), ' ||
						'dispositivos mec�nicos de compress�o pneum�tica ' ||
						'intermitente (CPI) ou bomba plantar (BP).';*/

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w > 0) and
			(qt_questao_5_w > 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308257) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308234) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308236) || ' ' || Wheb_mensagem_pck.get_texto(308246);

						/*'Como este paciente apresenta contra-indica��o absoluta para o ' ||
						'uso de quimioprofilaxia e relativa para m�todos f�sicos, deve-se ' ||
						'avaliar individualmente o risco/benef�cio de sua aplica��o ou n�o. ' || quebra_w || quebra_w ||
						'Optando-se pela profilaxia mec�nica: '|| quebra_w || quebra_w ||
						'Recomendam-se m�todos f�sicos como meia el�stica de compress�o ' ||
					    'gradual (MECG), dispositivos mec�nicos de compress�o pneum�tica ' ||
						'intermitente (CPI) ou bomba plantar (BP).';*/

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w > 0) and
			(qt_questao_5_w > 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado :=	Wheb_mensagem_pck.get_texto(308257) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308234) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308236) || ' ' ||
						Wheb_mensagem_pck.get_texto(308246) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308238) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308249) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308250) || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308261) || ' ' ||quebra_w||
						Wheb_mensagem_pck.get_texto(308252) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308177);

						/*'Como este paciente apresenta contra-indica��o relativa para o uso ' ||
						'de quimioprofilaxia e para m�todos f�sicos, deve-se avaliar ' ||
						'individualmente o risco/benef�cio de sua aplica��o ou n�o. ' || quebra_w || quebra_w ||
						'Optando-se pela profilaxia mec�nica: ' || quebra_w || quebra_w ||
						'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual ' ||
						'(MECG), dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ' ||
						'ou bomba plantar (BP).' || quebra_w || quebra_w ||
						'Optando-se pela quimioprofilaxia:' || quebra_w || quebra_w ||
						'Enoxaparina 20 mg SC 1 X ao dia ou ' || quebra_w ||
						'Dalteparina 2.500 UI SC 1 X ao dia ou' || quebra_w ||
						'Nadroparina ' || qt_Nadroparina_w || ' SC 1 X ao dia ou '||quebra_w||
						'Heparina n�o fracionada(HNF) 5.000 UI SC 2 X ao dia.' || quebra_w || quebra_w ||
						'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.';*/

						inserir_texto_adic_cir;

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w = 1) and
			(qt_absoluta_w = 0) and
			(qt_questao_5_w = 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC := 'S';
			:new.ds_resultado :=	Wheb_mensagem_pck.get_texto(308216) || quebra_w || quebra_w ||
						obter_desc_expressao(891552) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308238) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308249) || ' ' ||  quebra_w ||
						Wheb_mensagem_pck.get_texto(308250) || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308261) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308252) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308177);

						/*'Como o paciente apresenta contra-indica��o relativa ao uso ' ||
						'de quimioprofilaxia para TEV, considerar o risco/benef�cio de ' ||
						'sua introdu��o ou utilizar m�todos f�sicos como meia el�stica de ' ||
						'compress�o gradual (MECG), dispositivos mec�nicos de compress�o ' ||
						'pneum�tica intermitente (CPI) ou bomba plantar (BP).' || quebra_w || quebra_w ||
						891552'Os m�todos de profilaxia mec�nica sugeridos devem ser mantidos por 18 horas, pelo menos, ao longo do dia.' ||quebra_w ||quebra_w
						308238'Optando-se pela quimioprofilaxia:' || quebra_w || quebra_w ||
						308249'Enoxaparina 20 mg SC 1 X ao dia ou '||  quebra_w ||
						308250'Dalteparina 2.500 UI SC 1 X ao dia ou' || quebra_w ||
						308193'Nadroparina ' || qt_Nadroparina_w || 308261' SC 1 X ao dia ou '|| quebra_w ||
						308252'Heparina n�o fracionada(HNF) 5.000 UI SC 2 X ao dia.' || quebra_w || quebra_w ||
						308177'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.';*/

						inserir_texto_adic_cir;

		end if;
	elsif	(ie_cir_grande_medio_porte_w = 'S') and
		(:new.qt_idade	< 40) and
		(qt_reg_w = 0) then
		:new.ie_risco := 'B';
		:new.IE_PROFILAXIA_MEC := 'A';
		:new.IE_QUIMIOPROFILAXIA := 'A';
		:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308247);

					/*'Pacientes submetidos a procedimentos cir�rgicos considerados de baixo risco para TEV podem ainda assim apresentar trombose, ' ||
					'particularmente aqueles mais idosos ou com fatores de risco associados, como c�ncer, hist�ria pr�via ou familiar de TEV, ou ' ||
					'mesmo associa��es de v�rios fatores comuns, como tabagismo, varizes, obesidade, reposi��o hormonal, etc. Nestes pacientes, a avalia��o ' ||
					'de risco deve ser individualizada, podendo-se optar pela prescri��o da profilaxia.';*/

	elsif	(ie_cir_grande_medio_porte_w = 'S') and
		((:new.qt_idade	>= 40) and (:new.qt_idade <= 60)) and
		(qt_reg_w = 0) then
		if 	((qt_relativa_w + qt_absoluta_w) = 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado := Wheb_mensagem_pck.get_texto(308248) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308249) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308250) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308267) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308252) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308177);

						/*'A quimioprofilaxia recomendada nesta situa��o cl�nica �: ' || quebra_w || quebra_w ||
						'Enoxaparina 20 mg SC 1 X ao dia ou ' || quebra_w ||
						'Dalteparina 2.500 UI SC 1 X ao dia ou ' || quebra_w ||
						'Nadroparina ' || qt_Nadroparina_w ||' 1 X ao dia ou ' || quebra_w ||
						'Heparina n�o fracionada(HNF) 5.000 UI 2 X ao dia' || quebra_w || quebra_w ||
						'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.';*/

						inserir_texto_adic_cir;
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w = 1) and
			(qt_questao_5_w = 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'S';
			:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308270);

						/*'Como o paciente apresenta contra-indica��o absoluta ao uso ' ||
						'de quimioprofilaxia gradual (MECG), para TEV, recomendam-se ' ||
						'm�todos f�sicos como meia el�stica de compress�o, dispositivos ' ||
						'mec�nicos de compress�o pneum�tica intermitente (CPI) ' ||
						'ou bomba plantar (BP).';*/

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_absoluta_w > 0) and
			(qt_questao_5_w > 0) then
			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308257) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308234) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308236) || ' ' ||
						Wheb_mensagem_pck.get_texto(308275) || quebra_w ||
						Wheb_mensagem_pck.get_texto(308276);

						/*'Como este paciente apresenta contra-indica��o absoluta para o uso ' ||
						'de quimioprofilaxia e relativa para m�todos f�sicos,  deve-se ' ||
						'avaliar individualmente o risco/benef�cio de sua aplica��o ou n�o. ' || quebra_w || quebra_w ||
						'Optando-se pela profilaxia mec�nica: '|| quebra_w || quebra_w ||
						'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), ' ||
						'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI)' || quebra_w ||
						'ou bomba plantar (BP).';*/

		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w > 0) and
			(qt_questao_5_w > 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado :=	Wheb_mensagem_pck.get_texto(308232) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308234) || ' ' || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308236) || ' ' ||
						Wheb_mensagem_pck.get_texto(308246) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308238) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308249) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308250) || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308267) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308252) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308177);

						/*'Como este paciente apresenta contra-indica��o relativa para o uso de ' ||
						'quimioprofilaxia e para m�todos f�sicos, deve-se avaliar individualmente' ||
						'o risco/benef�cio de sua  aplica��o ou n�o. ' || quebra_w || quebra_w ||
						'Optando-se pela profilaxia mec�nica: ' || quebra_w || quebra_w ||
						'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), ' ||
						'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ' ||
						'ou bomba plantar (BP).' || quebra_w || quebra_w ||
						'Optando-se pela quimioprofilaxia:' || quebra_w || quebra_w ||
						'Enoxaparina 20 mg SC 1 X ao dia ou ' || quebra_w ||
						'Dalteparina 2.500 UI SC 1 X ao dia ou' || quebra_w ||
						'Nadroparina ' || qt_Nadroparina_w || 308267' 1 X ao dia ou ' || quebra_w ||
						'Heparina n�o fracionada(HNF) 5.000 UI 2 X ao dia.' || quebra_w || quebra_w ||
						'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.';*/

						inserir_texto_adic_cir;
		elsif	((qt_relativa_w + qt_absoluta_w) > 0) and
			(qt_relativa_w = 1) and
			(qt_absoluta_w = 0) and
			(qt_questao_5_w = 0) then

			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '1.900';
			else
				qt_Nadroparina_w	:= '2.850';
			end if;

			:new.ie_risco := 'M';
			:new.IE_QUIMIOPROFILAXIA := 'A';
			:new.IE_PROFILAXIA_MEC  := 'S';
			:new.ds_resultado :=	Wheb_mensagem_pck.get_texto(308287) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308238) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308249) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308250) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308267) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308252) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308177);

						/*'Como o paciente apresenta contra-indica��o relativa ao uso de ' ||
						'quimioprofilaxia para TEV, considerar o risco/benef�cio dessa introdu��o ' ||
						'ou utilizar m�todos f�sicos como meia el�stica de compress�o gradual (MECG), ' ||
						'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ' ||
						'ou bomba plantar (BP).'|| quebra_w || quebra_w ||
						'Optando-se pela quimioprofilaxia:' || quebra_w || quebra_w ||
						'Enoxaparina 20 mg SC 1 X ao dia ou ' || quebra_w ||
						'Dalteparina 2.500 UI SC 1 X ao dia ou ' || quebra_w ||
						'Nadroparina ' || qt_Nadroparina_w || ' 1 X ao dia ou ' || quebra_w ||
						'Heparina n�o fracionada(HNF) 5.000 UI 2 X ao dia.' || quebra_w || quebra_w ||
						'O uso desta quimioprofilaxia deve ser mantido por 7 a 10 dias.';*/

						inserir_texto_adic_cir;
		end if;
	elsif	(((ie_cir_grande_medio_porte_w = 'S') and
		((:new.qt_idade	>= 40) and (:new.qt_idade <= 60)) and
		(qt_reg_w > 0)) or
		((ie_cir_alto_risco_w = 'S') or
		((ie_cir_grande_medio_porte_w = 'S') and (:new.qt_idade > 60)))) then

		if	((qt_relativa_w + qt_absoluta_w) > 0) then

			if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S') then
				:new.ie_risco := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'A';
				:new.IE_PROFILAXIA_MEC   := 'S';
				:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308209)	|| quebra_w || quebra_w ||
										Wheb_mensagem_pck.get_texto(308210) || quebra_w ||quebra_w||
										Wheb_mensagem_pck.get_texto(308211) || quebra_w || quebra_w||
										Wheb_mensagem_pck.get_texto(308213) || ' ' ||quebra_w || quebra_w||
										Wheb_mensagem_pck.get_texto(308214) || ' ';

										/*'Como o paciente apresenta contra-indica��o absoluta ao uso de heparinas, '||
										'recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), '||
										'dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).'|| quebra_w || quebra_w ||
										'Optando-se pela quimioprofilaxia recomenda-se:' || quebra_w ||quebra_w||
										'Fondaparina (Arixtra) 2,5 mg SC 1 x ao dia.' || quebra_w || quebra_w||
										'Op��o para casos de artroplastia de quadril e joelho: '||quebra_w || quebra_w||
										'Rivaroxabana (Xarelto) 10 mg VO 1 x ao dia. ';*/

			elsif	(:new.IE_INSUF_RENAL	= 'S') then
				:new.ie_risco := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'A';
				:new.IE_PROFILAXIA_MEC   := 'S';
				:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308216) || quebra_w || quebra_w ||
										obter_desc_expressao(891552) || quebra_w || quebra_w ||
										Wheb_mensagem_pck.get_texto(308218) || ' ' || quebra_w ||quebra_w||
										Wheb_mensagem_pck.get_texto(308219) || quebra_w || quebra_w||
										Wheb_mensagem_pck.get_texto(308201) || quebra_w || quebra_w;

										/*'Como o paciente apresenta contra-indica��o relativa ao uso de ' ||
										'quimioprofilaxia para TEV, considerar o risco/benef�cio de sua ' ||
										'introdu��o ou utilizar m�todos f�sicos como meia el�stica de ' ||
										'compress�o gradual (MECG), dispositivos mec�nicos de compress�o ' ||
										'pneum�tica intermitente (CPI) ou bomba plantar (BP).' || quebra_w || quebra_w ||
										'Os m�todos de profilaxia mec�nica sugeridos devem ser mantidos por 18 horas, pelo menos, ao longo do dia.' ||quebra_w ||quebra_w
										'Optando-se pelo uso da heparina recomenda-se:' || quebra_w ||quebra_w||
										'Heparina n�o fracionada HNF 5000 UI 2x ao dia e controle do TTPA.' || quebra_w || quebra_w||
										'O uso desta quimioprofilaxia deve ser mantido por 6 a 14 dias ou enquanto persistir o risco.' || quebra_w || quebra_w;*/


			elsif	(qt_absoluta_w = 1)  and
				(qt_questao_5_w = 0) then
				:new.ie_risco := 'A';
				:new.IE_PROFILAXIA_MEC 	 := 'S';
				:new.IE_QUIMIOPROFILAXIA := 'N';
				:new.ds_resultado :=	Wheb_mensagem_pck.get_texto(308223);

							/*'Como o paciente apresenta contra-indica��o absoluta ao uso de ' ||
							'quimioprofilaxia para TEV, recomendam-se m�todos f�sicos como ' ||
							'meia el�stica de compress�o gradual (MECG), dispositivos mec�nicos ' ||
							'de compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).';*/

			elsif	(qt_relativa_w > 0) and
				(qt_absoluta_w = 0) and
				(qt_questao_5_w = 0) then

				if	(:new.qt_peso	< 70) then
					qt_Nadroparina_w	:= '3.800';
				else
					qt_Nadroparina_w	:= '5.700';
				end if;

				:new.ie_risco := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'A';
				:new.IE_PROFILAXIA_MEC   := 'S';
				:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308216)	|| quebra_w || quebra_w ||
							obter_desc_expressao(891552) || quebra_w || quebra_w ||
							Wheb_mensagem_pck.get_texto(308238) || quebra_w ||
							Wheb_mensagem_pck.get_texto(308190) || ' ' || quebra_w ||
							Wheb_mensagem_pck.get_texto(308192) || ' ' || quebra_w ||
							Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308267) || ' ' || quebra_w ||
							Wheb_mensagem_pck.get_texto(308196) || quebra_w || quebra_w||
								obter_desc_expressao(891564)|| quebra_w || quebra_w;

							/*'Como o paciente apresenta contra-indica��o relativa ao uso de ' ||
							'quimioprofilaxia para TEV, considerar o risco/benef�cio de sua ' ||
							'introdu��o ou utilizar m�todos f�sicos como meia el�stica de ' ||
							'compress�o gradual (MECG), dispositivos mec�nicos de compress�o ' ||
							'pneum�tica intermitente (CPI) ou bomba plantar (BP).' || quebra_w || quebra_w ||
							'Os m�todos de profilaxia mec�nica sugeridos devem ser mantidos por 18 horas, pelo menos, ao longo do dia.' ||quebra_w ||quebra_w
							'Optando-se pela quimioprofilaxia:' || quebra_w ||
							'Enoxaparina 40 mg SC 1 X ao dia ou ' || quebra_w ||
							'Dalteparina 5.000 UI SC 1 X ao dia ou ' || quebra_w ||
							'Nadroparina '|| qt_Nadroparina_w ||' 1 X ao dia ou ' || quebra_w ||
							'Heparina n�o fracionada(HNF) 5.000 UI SC 3 X ao dia.' || quebra_w || quebra_w;*/

							inserir_texto_tipo_cirur;
							inserir_texto_adic_cir;

			elsif	(qt_absoluta_w > 0) and
				(qt_questao_5_w > 0) then
				:new.ie_risco := 'A';
				:new.IE_PROFILAXIA_MEC := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'N';
				:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308257) || ' ' || quebra_w || quebra_w ||
							Wheb_mensagem_pck.get_texto(308238) || ' ' || quebra_w || quebra_w ||
							Wheb_mensagem_pck.get_texto(308236) || ' ' || Wheb_mensagem_pck.get_texto(308246);

							/*'Como este paciente apresenta contra-indica��o absoluta para o uso de quimioprofilaxia e relativa para m�todos f�sicos, ' ||
							'deve-se avaliar individualmente o risco/benef�cio de sua aplica��o ou n�o. ' || quebra_w || quebra_w ||
							'Optando-se pela profilaxia mec�nica: ' || quebra_w || quebra_w ||
							'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual (MECG), dispositivos mec�nicos de ' ||
							'compress�o pneum�tica intermitente (CPI) ou bomba plantar (BP).';*/

			elsif	(qt_relativa_w > 0) and
				(qt_absoluta_w = 0) and
				(qt_questao_5_w > 0) then

				if	(:new.qt_peso	< 70) then
					qt_Nadroparina_w	:= '3.800';
				else
					qt_Nadroparina_w	:= '5.700';
				end if;

				:new.ie_risco := 'A';
				:new.IE_QUIMIOPROFILAXIA := 'A';
				:new.IE_PROFILAXIA_MEC := 'A';
				:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308232) || quebra_w || quebra_w ||
							Wheb_mensagem_pck.get_texto(308234) || ' ' || quebra_w || quebra_w ||
							Wheb_mensagem_pck.get_texto(308236) || ' ' || Wheb_mensagem_pck.get_texto(308246) || quebra_w || quebra_w ||
							Wheb_mensagem_pck.get_texto(308238) || quebra_w ||
							Wheb_mensagem_pck.get_texto(308190) || ' ' || quebra_w ||
							Wheb_mensagem_pck.get_texto(308192) || ' ' || quebra_w ||
							Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308267) || ' ' || quebra_w ||
							Wheb_mensagem_pck.get_texto(308196) || quebra_w || quebra_w||
							obter_desc_expressao(891564)|| quebra_w || quebra_w;

							/*'Como este paciente apresenta contra-indica��o relativa para o uso de ' ||
							'quimioprofilaxia e para m�todos f�sicos, deve-se avaliar individualmente ' ||
							'o risco/benef�cio de sua aplica��o ou n�o.' || quebra_w || quebra_w ||
							'Optando-se pela profilaxia mec�nica: ' || quebra_w || quebra_w ||
							'Recomendam-se m�todos f�sicos como meia el�stica de compress�o gradual ' ||
							'(MECG), dispositivos mec�nicos de compress�o pneum�tica intermitente (CPI) ' ||
							'ou bomba plantar (BP).' || quebra_w || quebra_w ||
							'Optando-se pela quimioprofilaxia:' || quebra_w ||
							'Enoxaparina 40 mg SC 1 X ao dia ou ' || quebra_w ||
							'Dalteparina 5.000 UI SC 1 X ao dia ou ' || quebra_w ||
							'Nadroparina ' || qt_Nadroparina_w ||' 1 X ao dia ou '|| quebra_w ||
							'Heparina n�o fracionada(HNF) 5.000 UI SC 3 X ao dia.' || quebra_w || quebra_w;*/

							inserir_texto_tipo_cirur;
							inserir_texto_adic_cir;
			end if;
		else
			if	(:new.qt_peso	< 70) then
				qt_Nadroparina_w	:= '3.800';
			else
				qt_Nadroparina_w	:= '5.700';
			end if;

			:new.ie_risco := 'A';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.ds_resultado := 	Wheb_mensagem_pck.get_texto(308248) || quebra_w || quebra_w ||
						Wheb_mensagem_pck.get_texto(308190) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308192) || ' ' || quebra_w ||
						Wheb_mensagem_pck.get_texto(308193) || ' ' || qt_Nadroparina_w || ' ' || Wheb_mensagem_pck.get_texto(308267) || ' '  || quebra_w||
						Wheb_mensagem_pck.get_texto(308196) || quebra_w || quebra_w||
								obter_desc_expressao(891564)|| quebra_w || quebra_w;

						/*'A quimioprofilaxia recomendada nesta situa��o cl�nica �:' || quebra_w || quebra_w ||
						'Enoxaparina 40 mg SC 1 X ao dia ou ' || quebra_w ||
						'Dalteparina 5.000 UI 1 X ao dia ou ' || quebra_w ||
						'Nadroparina ' || qt_Nadroparina_w || ' 1 X ao dia ou ' || quebra_w||
						'Heparina n�o fracionada(HNF) 5.000 UI SC 3 X ao dia.' || quebra_w || quebra_w;*/

						inserir_texto_tipo_cirur;
						inserir_texto_adic_cir;
		end if;
	end if;

	end;

begin

ie_padrao_tev_w	:= nvl(:new.ie_padrao,'D');
if	(nvl(:new.ie_padrao,'D')	= 'HSL') then

	if	(:old.dt_liberacao is null ) and
		(:new.dt_liberacao is not null) then

		if	(nvl(:new.IE_NAO_SE_APLICA,'N') = 'S') and
			(:new.NR_SEQ_MOTIVO_TEV is null) then

			--� necess�rio a sele��o de um motivo pelo qual n�o se aplica o protocolo de TEV.
			Wheb_mensagem_pck.exibir_mensagem_abort(264633);

		end if;

	end if;

	:new.IE_IDADE_MAIOR	:='N';
	if	(:new.qt_idade	>= 55) then
		:new.IE_IDADE_MAIOR	:= 'S';
	end if;

	qt_imc_w	:= dividir(:new.qt_peso,(:new.qt_altura_cm /100) * (:new.qt_altura_cm /100));
	if	(qt_imc_w	>=30 ) then
		:new.IE_OBESIDADE	:= 'S';
	end if;
	:new.IE_CONDICAO	:= 'N';
	ie_condicao_w		:= 'N';
	qt_reg_w		:= 0;

	if	(:new.IE_HIPERSENSIBILIDADE_HEPARINA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_SANGRAMENTO_ULCERA	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(:new.IE_ANTICOAGULACAO	= 'S') then
		qt_reg_w:= qt_reg_w+1;
	end if;

	if	(qt_reg_w	> 0) then
		ie_condicao_w	:= 'S';
	end if;

	qt_contra_ind_relat_w := 0;

	if	(:new.IE_COAGULOPATIA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_ULCERA_ATIVA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_HIPERTENSAO	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_INSUF_RENAL	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_CIRURGIA_INTRACRANIANA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(:new.IE_PUNCAO_LIQUORICA	= 'S') then
		qt_contra_ind_relat_w:= qt_contra_ind_relat_w+1;
	end if;

	if	(qt_reg_w > 0) or
		(qt_contra_ind_relat_w > 0) then
		:new.IE_CONDICAO	:= 'S';
	end if;

	:new.ds_resultado	:= null;
	qt_reg_w		:= 0;


	if	(:new.IE_ESCARA	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_FRATURA_EXPOSTA	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INFECCAO_MMII	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INSUF_ART_MMII	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_INSUF_CARD_GRAVE	= 'S') then
		qt_contra_prof_mec_w	:= qt_contra_prof_mec_w + 1;
	end if;

	if	(:new.IE_CLINICO_CIRURGICO	= 'C') then
		if	(:new.IE_IDADE_MAIOR	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ECLAMPSIA	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_HIST_FAMILIAR_TEV	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_GRAVIDEZ	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PNEUMONIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DOENCA_REUMATOL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ANTICONCEPCIONAIS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PUERPERIO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_OBESIDADE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INTERNACAO_UTI	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PARALISIA_INF	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_INSUF_RESP	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INSUF_ARTERIAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TRH	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ICC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFLA_INTESTINAL= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_VARIZES	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFECSAO_GRAVE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_QUIMIOTERAPIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_SINDROME_NEFROTICA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ABORTAMENTO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_IAM	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TABAGISMO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;


		if	(:new.IE_DOENCA_AUTOIMUNE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_TRAUMA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_QUEIMADO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		if	(:new.IE_OUTROS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		:new.IE_FATOR_RISCO:='N';
		if	(qt_reg_w	> 0) and
			(:new.IE_MOBILIDADE_REDUZIDA	= 'S')then
			:new.IE_FATOR_RISCO := 'S';
		end if;
		:new.ie_risco	:= 'B';
		if	(:new.IE_FATOR_RISCO	= 'N') then
			:new.ie_risco	:= 'B';
		elsif	(:new.IE_FATOR_RISCO	= 'S') then
			:new.ie_risco	:= 'A';

			if	(:new.qt_idade	<= 40) and
				(ie_condicao_w	= 'N') then
				:new.ie_risco	:= 'B';
			end if;
			if	(:new.qt_idade < 40) then
				:new.ie_risco	:= 'B';
			end if;
		end if;
		:new.ds_resultado	:= Null;



		if	(:new.ie_risco	= 'B') then
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC  := 'A';

			:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308327) || ' ' || Wheb_mensagem_pck.get_texto(308328);

							/*'N�o h� indica��o de profilaxia medicamentosa ou m�todos mec�nicos.'||
							' Recomenda-se que o paciente tenha sua deambula��o incentivada e que seja reavaliado caso haja mudan�a em seu estado,'||
							' como por exemplo, perda de mobilidade, desenvolvimento de fatores de risco para TEV,'||
							' resolu��o de contra-indica��es para profilaxia, entre outros.';*/
			/*
			if	(:new.IE_FATOR_RISCO	= 'S') and
				(:new.qt_idade	< 40) then

				:new.ds_resultado	:= Null;
				Inserir_Descricao_Idade;
			end if;
			*/
		elsif	(:new.ie_risco	= 'A') then

			if	(ie_condicao_w	= 'N') and
				(qt_contra_ind_relat_w = 0) and -- Edilson
				(qt_contra_prof_mec_w = 0) then --Edilson
				:new.IE_QUIMIOPROFILAXIA := 'S';
				:new.IE_PROFILAXIA_MEC  := 'A';
				:new.ds_resultado	:= Wheb_mensagem_pck.get_texto(308248); --'A quimioprofilaxia recomendada nesta situa��o cl�nica �:';
				Inserir_Medicacao;


				--:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||'Obs.: No estudo de Harenberg e cols, houve aumento na mortalidade no grupo que recebeu nadroparina, comparado com HNF (Haemostasis, 1993; 26(3):127-139';
			else
				Inserir_Contra_Indicacao;

			end if;

			Inserir_Descricao_Idade;
		end if;

	elsif	(ie_padrao_tev_w	= 'HSL') and
		(:new.ie_clinico_cirurgico	= 'I') then
		:new.ie_risco	:= 'B';

		/* INICIO RESPOSTA 2 */
		ie_cir_alto_risco_w		:= :new.ie_cir_alto_risco;
		ie_cir_pequeno_porte_w		:= :new.ie_cir_pequeno_porte;
		ie_cir_grande_medio_porte_w	:= :new.ie_cir_grande_medio_porte;


		ie_artroplastia_joelho_w	:= :new.ie_artroplastia_joelho;
		ie_artroplastia_quadril_w	:= :new.ie_artroplastia_quadril;
		ie_fratura_quadril_w		:= :new.ie_fratura_quadril;
		ie_oncol_curativa_w		:= :new.ie_oncol_curativa;
		ie_trauma_medular_w		:= :new.ie_trauma_medular;
		ie_politrauma_w			:= :new.ie_politrauma;
		/* FIM RESPOSTA 2 */

		/* INICIO RESPOSTA 3 */
		qt_reg_w	:= 0;

		if	(:new.IE_TABAGISMO	= 'S') then
			qt_reg_w	:= qt_reg_w	+ 1;
		end if;
		if	(:new.IE_TROMBOFILIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_AVCI	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_TVP_EP_PREVIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DOENCA_REUMATOL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ANTICONCEPCIONAIS	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PUERPERIO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_OBESIDADE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INTERNACAO_UTI	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CAT_VENOSO_CENTRAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_PARALISIA_INF	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_CANCER	= 'S') then
			qt_reg_w	:= qt_reg_w+1;
		end if;
		if	(:new.IE_INSUF_RESP	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INSUF_ARTERIAL	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TRH	= 'S')then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ICC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_DPOC	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFLA_INTESTINAL= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_VARIZES	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_INFECSAO_GRAVE	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_QUIMIOTERAPIA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_SINDROME_NEFROTICA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_ABORTAMENTO	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_IAM	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;
		if	(:new.IE_TRAUMA	= 'S') then
			qt_reg_w:= qt_reg_w+1;
		end if;

		:new.IE_FATOR_RISCO := 'N';
		if	(qt_reg_w	> 0) then
			:new.IE_FATOR_RISCO := 'S';
		end if;
		/* FIM RESPOSTA 3 */

		/* INICIO RESPOSTA 4 */
		qt_absoluta_w	:= 0;
		qt_relativa_w	:= 0;
		if	(:new.IE_HIPERSENSIBILIDADE_HEPARINA	= 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_PLAQUETOPENIA_HEPARINA	= 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_SANGRAMENTO_ULCERA	= 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_ANTICOAGULACAO = 'S')then
			qt_absoluta_w:= qt_absoluta_w + 1;
		end if;
		if	(:new.IE_CIRURGIA_INTRACRANIANA	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_PUNCAO_LIQUORICA	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_COAGULOPATIA	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_HIPERTENSAO	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		if	(:new.IE_INSUF_RENAL	= 'S')then
			qt_relativa_w:= qt_relativa_w + 1;
		end if;
		/* FIM RESPOSTA 4 */

		/* INICIO RESPOSTA 5 */
		qt_questao_5_w := 0;
		if	(:new.IE_FRATURA_EXPOSTA	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_INFECCAO_MMII	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_INSUF_ART_MMII	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_INSUF_CARD_GRAVE	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		if	(:new.IE_ESCARA	= 'S')then
			qt_questao_5_w	:= qt_questao_5_w + 1;
		end if;
		/* FIM RESPOTA 5 */

		:new.IE_RISCO	:= 'B';
		if	(:new.IE_CIR_PEQUENO_PORTE = 'S') then
			:new.IE_RISCO	:= 'B';
		elsif	((:new.IE_CIR_ALTO_RISCO	= 'S') 	or
			((:new.IE_CIR_GRANDE_MEDIO_PORTE = 'S')	and
			((:new.qt_idade	> 60)	or
			((:new.qt_idade	between 40 and 60 )and (qt_reg_w	> 0)))))	then
			:new.IE_RISCO	:= 'A';
		elsif	(:new.IE_CIR_GRANDE_MEDIO_PORTE = 'S') and
			((:new.qt_idade between 40 and 60) and
			(qt_reg_w	= 0)) or
			((:new.qt_idade	< 40) and
			(qt_reg_w	> 0)) then
			:new.IE_RISCO	:= 'M';
		elsif	((:new.qt_idade	< 40) and
			(qt_reg_w	= 0)) or
			(:new.IE_CIR_PEQUENO_PORTE in ('S')) then
			:new.IE_RISCO	:= 'B';
		end if;
		Inserir_Descricao_Idade;

		if	(ie_condicao_w	= 'N') and
			(:new.ie_risco	<> 'B') then
			:new.IE_QUIMIOPROFILAXIA := 'S';
			:new.IE_PROFILAXIA_MEC  := 'A';
			:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308331); --'A quimioprofilaxia recomendada nesta situa��o �:';
			Inserir_Medicacao;

			--:new.ds_resultado	:= :new.ds_resultado||quebra_w||quebra_w||'Obs.: No estudo de Harenberg e cols, houve aumento na mortalidade no grupo que recebeu nadroparina, comparado com HNF (Haemostasis, 1993; 26(3):127-139';
		else
			Inserir_Contra_Indicacao;
		end if;


		if	(:new.ie_risco	= 'B') then
			:new.IE_QUIMIOPROFILAXIA := 'N';
			:new.IE_PROFILAXIA_MEC := 'A';
			:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308332); --'N�o h� indica��o de profilaxia medicamentosa ou m�todos f�sicos. O paciente deve ter sua deambula��o incentivada e deve ser reavaliado em 2 dias, caso permane�a internado.';
		end if;

		/* Inicio Cirugia */

		inserir_resposta_cirurgia;

		/* Fim cirurgia */

	end if;

end if;

if	(:new.qt_peso is null) and
	(ie_padrao_tev_w	= 'HSL')then
	:new.ds_resultado := null;
end if;

if	(:new.IE_ANTICOAGULACAO	= 'S') and
	(ie_padrao_tev_w	= 'HSL') then
	:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308334); --'Paciente em anticoagula��o. O protocolo de profilaxia de TEV n�o se aplica.';
	:new.ie_risco	:= 'N';
end if;

if	(:new.IE_ANTICOAGULACAO	= 'S') and
	(ie_padrao_tev_w	= 'HSL') then
	:new.IE_NAO_SE_APLICA	:= 'S';
end if;

if	(:new.IE_NAO_SE_APLICA	= 'S') and
	(nvl(:new.IE_ANTICOAGULACAO,'N')	= 'N') and
	(ie_padrao_tev_w	= 'HSL') then
	:new.ie_risco	:= 'N';

		:new.ds_resultado	:= 	Wheb_mensagem_pck.get_texto(308335); --'O protocolo de profilaxia de TEV n�o se aplica';
	if	(:new.NR_SEQ_MOTIVO_TEV	is not null) then
		select	max(ds_motivo)
		into	ds_motivo_w
		from	tev_motivo
		where	nr_sequencia	= :new.NR_SEQ_MOTIVO_TEV;

		if	(ds_motivo_w	is not null) then
			:new.ds_resultado	:= :new.ds_resultado||chr(13) ||'    '|| Wheb_mensagem_pck.get_texto(308336, 'DS_MOTIVO_W='||ds_motivo_w); --'Motivo: '||ds_motivo_w;
		end if;
	else
		--� necess�rio a sele��o de um motivo pelo qual n�o se aplica o protocolo de TEV.
		Wheb_mensagem_pck.exibir_mensagem_abort(264633);
	end if;


end if;

end;
/


ALTER TABLE TASY.ESCALA_TEV ADD (
  CONSTRAINT ESCATEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_TEV ADD (
  CONSTRAINT ESCATEV_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCATEV_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCATEV_TEVMOTI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TEV) 
 REFERENCES TASY.TEV_MOTIVO (NR_SEQUENCIA),
  CONSTRAINT ESCATEV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCATEV_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCATEV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ESCATEV_TEVJUST_FK 
 FOREIGN KEY (NR_SEQ_JUST_TEV) 
 REFERENCES TASY.TEV_JUSTIFICATIVA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_TEV TO NIVEL_1;

