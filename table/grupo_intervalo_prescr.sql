ALTER TABLE TASY.GRUPO_INTERVALO_PRESCR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GRUPO_INTERVALO_PRESCR CASCADE CONSTRAINTS;

CREATE TABLE TASY.GRUPO_INTERVALO_PRESCR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_APRES            NUMBER(5)             NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DS_GRUPO_INTERV_PRESCR  VARCHAR2(255 BYTE)    NOT NULL,
  IE_VIA_APLICACAO        VARCHAR2(5 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.GRINTPR_PK ON TASY.GRUPO_INTERVALO_PRESCR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GRINTPR_VIAAPLI_FK_I ON TASY.GRUPO_INTERVALO_PRESCR
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GRUPO_INTERVALO_PRESCR ADD (
  CONSTRAINT GRINTPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GRUPO_INTERVALO_PRESCR ADD (
  CONSTRAINT GRINTPR_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO));

