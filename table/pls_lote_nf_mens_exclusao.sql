ALTER TABLE TASY.PLS_LOTE_NF_MENS_EXCLUSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_NF_MENS_EXCLUSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_NF_MENS_EXCLUSAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  NR_SEQ_MENSALIDADE   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLNME_PK ON TASY.PLS_LOTE_NF_MENS_EXCLUSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLNME_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLNME_PLSLNFM_FK_I ON TASY.PLS_LOTE_NF_MENS_EXCLUSAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLNME_PLSLNFM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLNME_PLSMENS_FK_I ON TASY.PLS_LOTE_NF_MENS_EXCLUSAO
(NR_SEQ_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLNME_PLSMENS_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOTE_NF_MENS_EXCLUSAO ADD (
  CONSTRAINT PLSLNME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_NF_MENS_EXCLUSAO ADD (
  CONSTRAINT PLSLNME_PLSLNFM_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_NF_MENSALIDADE (NR_SEQUENCIA),
  CONSTRAINT PLSLNME_PLSMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_NF_MENS_EXCLUSAO TO NIVEL_1;

