ALTER TABLE TASY.COM_CLASSIF_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLASSIF_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLASSIF_HIST
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DS_CLASSIFICACAO      VARCHAR2(255 BYTE)      NOT NULL,
  CD_EXP_CLASSIFICACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLAH_DICEXPR_FK_I ON TASY.COM_CLASSIF_HIST
(CD_EXP_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMCLAH_PK ON TASY.COM_CLASSIF_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLAH_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_CLASSIF_HIST ADD (
  CONSTRAINT COMCLAH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLASSIF_HIST ADD (
  CONSTRAINT COMCLAH_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_CLASSIFICACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.COM_CLASSIF_HIST TO NIVEL_1;

