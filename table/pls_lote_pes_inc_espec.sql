ALTER TABLE TASY.PLS_LOTE_PES_INC_ESPEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_PES_INC_ESPEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_PES_INC_ESPEC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_LOTE           NUMBER(10)              NOT NULL,
  CD_INCONSISTENCIA     NUMBER(4)               NOT NULL,
  NR_SEQ_TIPO_DOC       NUMBER(10),
  NR_SEQ_PLS_TIPO_DOC   NUMBER(10),
  IE_CONSISTE_DATA_DOC  VARCHAR2(1 BYTE),
  QT_IDADE_INICIAL      NUMBER(3),
  QT_IDADE_FINAL        NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLPIE_PK ON TASY.PLS_LOTE_PES_INC_ESPEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPIE_PLSCAIP_FK_I ON TASY.PLS_LOTE_PES_INC_ESPEC
(CD_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPIE_PLSLOPEIN_FK_I ON TASY.PLS_LOTE_PES_INC_ESPEC
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLPIE_PLSTIDO_FK_I ON TASY.PLS_LOTE_PES_INC_ESPEC
(NR_SEQ_PLS_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPIE_PLSTIDO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLPIE_TIPODOC_FK_I ON TASY.PLS_LOTE_PES_INC_ESPEC
(NR_SEQ_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLPIE_TIPODOC_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOTE_PES_INC_ESPEC ADD (
  CONSTRAINT PLSLPIE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_PES_INC_ESPEC ADD (
  CONSTRAINT PLSLPIE_PLSCAIP_FK 
 FOREIGN KEY (CD_INCONSISTENCIA) 
 REFERENCES TASY.PLS_CAD_INCONSIST_PESSOA (NR_SEQUENCIA),
  CONSTRAINT PLSLPIE_PLSLOPEIN_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_PESSOA_INCONSIST (NR_SEQUENCIA),
  CONSTRAINT PLSLPIE_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOC) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA),
  CONSTRAINT PLSLPIE_PLSTIDO_FK 
 FOREIGN KEY (NR_SEQ_PLS_TIPO_DOC) 
 REFERENCES TASY.PLS_TIPO_DOCUMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_PES_INC_ESPEC TO NIVEL_1;

