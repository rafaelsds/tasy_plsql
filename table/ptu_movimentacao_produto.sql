ALTER TABLE TASY.PTU_MOVIMENTACAO_PRODUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_MOVIMENTACAO_PRODUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_MOVIMENTACAO_PRODUTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_UNIMED_DESTINO     VARCHAR2(10 BYTE),
  CD_UNIMED_ORIGEM      VARCHAR2(10 BYTE),
  DT_GERACAO            DATE                    NOT NULL,
  IE_TIPO_MOV           VARCHAR2(1 BYTE)        NOT NULL,
  DT_MOV_INICIO         DATE                    NOT NULL,
  DT_MOV_FIM            DATE                    NOT NULL,
  IE_OPERACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_VERSAO_TRANSACAO   NUMBER(2),
  IE_TIPO_PRODUTO       VARCHAR2(2 BYTE),
  NR_SEQ_LOTE           NUMBER(10),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  QT_REG_EMPRESAS       NUMBER(10),
  QT_REG_BENEF          NUMBER(10),
  QT_REG_COMPL_BENF     NUMBER(10),
  DS_HASH_A300          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUMOPR_PK ON TASY.PTU_MOVIMENTACAO_PRODUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUMOPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTUMOPR_PTUMOPL_FK_I ON TASY.PTU_MOVIMENTACAO_PRODUTO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUMOPR_PTUMOPL_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_MOVIMENTACAO_PRODUTO ADD (
  CONSTRAINT PTUMOPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_MOVIMENTACAO_PRODUTO ADD (
  CONSTRAINT PTUMOPR_PTUMOPL_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PTU_MOV_PRODUTO_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_MOVIMENTACAO_PRODUTO TO NIVEL_1;

