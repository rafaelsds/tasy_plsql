ALTER TABLE TASY.PLS_REGRA_NOTIF_ATEND_NEG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_NOTIF_ATEND_NEG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_NOTIF_ATEND_NEG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MOTIVO_GLOSA      VARCHAR2(5 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_ATEND   NUMBER(10),
  DS_MOTIVO_ATEND      VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRENAN_ESTABEL_FK_I ON TASY.PLS_REGRA_NOTIF_ATEND_NEG
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRENAN_PK ON TASY.PLS_REGRA_NOTIF_ATEND_NEG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRENAN_PLSGRAT_FK_I ON TASY.PLS_REGRA_NOTIF_ATEND_NEG
(NR_SEQ_GRUPO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_NOTIF_ATEND_NEG ADD (
  CONSTRAINT PLSRENAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_REGRA_NOTIF_ATEND_NEG ADD (
  CONSTRAINT PLSRENAN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRENAN_PLSGRAT_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_ATEND) 
 REFERENCES TASY.PLS_GRUPO_ATENDIMENTO (NR_SEQUENCIA));

