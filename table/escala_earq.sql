ALTER TABLE TASY.ESCALA_EARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_EARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_EARQ
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_PROFISSIONAL              VARCHAR2(10 BYTE) NOT NULL,
  NR_ATENDIMENTO               NUMBER(10),
  DT_AVALIACAO                 DATE             NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                 DATE,
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  QT_IDADE                     NUMBER(3)        NOT NULL,
  IE_SEXO                      VARCHAR2(1 BYTE) NOT NULL,
  IE_MARCHA_FIRME              VARCHAR2(1 BYTE) NOT NULL,
  IE_MARCHA_HESITANTE          VARCHAR2(1 BYTE) NOT NULL,
  IE_MARCHA_INSTAVEL           VARCHAR2(1 BYTE) NOT NULL,
  IE_MARCHA_DIF_TRANSFERENCIA  VARCHAR2(1 BYTE) NOT NULL,
  IE_MOB_INDEPENDENTE          VARCHAR2(1 BYTE) NOT NULL,
  IE_MOB_ORTESE_APOIO          VARCHAR2(1 BYTE) NOT NULL,
  IE_MOB_REQ_ORTESE            VARCHAR2(1 BYTE) NOT NULL,
  IE_MOB_IMOVEL                VARCHAR2(1 BYTE) NOT NULL,
  IE_HIST_QUEDA                NUMBER(2)        NOT NULL,
  IE_DEF_VISAO                 VARCHAR2(1 BYTE) NOT NULL,
  IE_DEF_AUDICAO               VARCHAR2(1 BYTE) NOT NULL,
  IE_DEF_EQUILIBRIO            VARCHAR2(1 BYTE) NOT NULL,
  IE_HIPNOTICOS                VARCHAR2(1 BYTE) NOT NULL,
  IE_TRANQUILIZANTES           VARCHAR2(1 BYTE) NOT NULL,
  IE_HIPOTENSORES              VARCHAR2(1 BYTE) NOT NULL,
  IE_ANALGESICOS               VARCHAR2(1 BYTE) NOT NULL,
  IE_DIABETES                  VARCHAR2(1 BYTE) NOT NULL,
  IE_CONFUSAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_ATAQUE_EPILEPTICO         VARCHAR2(1 BYTE) NOT NULL,
  IE_ARTRITE                   VARCHAR2(1 BYTE) NOT NULL,
  IE_NEUROLOGICO               VARCHAR2(1 BYTE) NOT NULL,
  IE_NAO_SABE_ANO              VARCHAR2(1 BYTE) NOT NULL,
  IE_NAO_SABE_DATA_NASC        VARCHAR2(1 BYTE) NOT NULL,
  IE_NAO_SABE_ENDERECO         VARCHAR2(1 BYTE) NOT NULL,
  IE_NAO_SABE_ONDE             VARCHAR2(1 BYTE) NOT NULL,
  IE_NAO_SABE_CIDADE           VARCHAR2(1 BYTE) NOT NULL,
  QT_PONTUACAO                 NUMBER(5),
  NR_HORA                      NUMBER(2),
  NR_SEQ_ASSINATURA            NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO    NUMBER(10),
  NR_SEQ_REG_ELEMENTO          NUMBER(10),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE),
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  NR_SEQ_TRIAGEM               NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCEARQ_ATEPACI_FK_I ON TASY.ESCALA_EARQ
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEARQ_EHRREEL_FK_I ON TASY.ESCALA_EARQ
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEARQ_I1 ON TASY.ESCALA_EARQ
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEARQ_PESFISI_FK_I ON TASY.ESCALA_EARQ
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEARQ_PESFISI_FK2_I ON TASY.ESCALA_EARQ
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCEARQ_PK ON TASY.ESCALA_EARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEARQ_TASASDI_FK_I ON TASY.ESCALA_EARQ
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCEARQ_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCEARQ_TASASDI_FK2_I ON TASY.ESCALA_EARQ
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCEARQ_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCEARQ_TRPRAT_FK_I ON TASY.ESCALA_EARQ
(NR_SEQ_TRIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_EARQ_tp  after update ON TASY.ESCALA_EARQ FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'ESCALA_EARQ',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.escala_earq_tr
before insert or update ON TASY.ESCALA_EARQ for each row
declare
qt_ponto_w	number(10) := 0;
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if  (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	goto Final;
end if;
qt_ponto_w := 0;
if 	(:new.qt_idade >= 60) and
	(:new.qt_idade <= 70) then
	qt_ponto_w := 1;
elsif	(:new.qt_idade >= 71) and
	(:new.qt_idade <= 80) then
	qt_ponto_w := 2;
elsif	(:new.qt_idade > 80) then
	qt_ponto_w := 1;
end if;

if 	(:new.ie_sexo = 'F') then
	qt_ponto_w := qt_ponto_w + 2;
else
	qt_ponto_w := qt_ponto_w + 1;
end if;

/*ESTILO DE MARCHA*/
if 	(:new.ie_marcha_firme = 'S') then
	qt_ponto_w := qt_ponto_w + 0;
end if;
if 	(:new.ie_marcha_hesitante = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_marcha_instavel = 'S') then
	qt_ponto_w := qt_ponto_w + 3;
end if;
if 	(:new.ie_marcha_dif_transferencia = 'S') then
	qt_ponto_w := qt_ponto_w + 3;
end if;

/*MOBILIDADE*/
if 	(:new.ie_mob_independente = 'S') then
	qt_ponto_w := qt_ponto_w + 0;
end if;
if 	(:new.ie_mob_ortese_apoio = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;
if 	(:new.ie_mob_req_ortese = 'S') then
	qt_ponto_w := qt_ponto_w + 3;
end if;
if 	(:new.ie_mob_imovel = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

/*HIST�RIA DE QUEDA*/
qt_ponto_w := qt_ponto_w + :new.ie_hist_queda;

/*D�FICIT SENSORIAL*/
if 	(:new.ie_def_visao = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;
if 	(:new.ie_def_audicao = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_def_equilibrio = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

/*MEDICA��ES*/
if 	(:new.ie_hipnoticos = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_tranquilizantes = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_hipotensores = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_analgesicos = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

/*HIST�RIA M�DICA*/
if 	(:new.ie_diabetes = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_confusao = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_ataque_epileptico = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_artrite = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_neurologico = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;

/*MEM�RIA*/
if 	(:new.ie_nao_sabe_ano = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_nao_sabe_data_nasc = 'S') then
	qt_ponto_w := qt_ponto_w + 1;
end if;
if 	(:new.ie_nao_sabe_endereco = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;
if 	(:new.ie_nao_sabe_onde = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;
if 	(:new.ie_nao_sabe_cidade = 'S') then
	qt_ponto_w := qt_ponto_w + 2;
end if;

:new.qt_pontuacao := qt_ponto_w;

<<Final>>
qt_reg_w := 0;
end escala_earq_tr;
/


CREATE OR REPLACE TRIGGER TASY.escala_earq_pend_atual
after insert or update ON TASY.ESCALA_EARQ for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'94');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_EARQ_delete
after delete ON TASY.ESCALA_EARQ for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '94'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_EARQ ADD (
  CONSTRAINT ESCEARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_EARQ ADD (
  CONSTRAINT ESCEARQ_TRPRAT_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM) 
 REFERENCES TASY.TRIAGEM_PRONTO_ATEND (NR_SEQUENCIA),
  CONSTRAINT ESCEARQ_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCEARQ_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCEARQ_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCEARQ_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCEARQ_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCEARQ_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_EARQ TO NIVEL_1;

