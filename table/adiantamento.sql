ALTER TABLE TASY.ADIANTAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADIANTAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADIANTAMENTO
(
  NR_ADIANTAMENTO          NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ADIANTAMENTO          DATE                 NOT NULL,
  VL_ADIANTAMENTO          NUMBER(15,2)         NOT NULL,
  VL_SALDO                 NUMBER(15,2)         NOT NULL,
  CD_MOEDA                 NUMBER(5)            NOT NULL,
  CD_TIPO_RECEBIMENTO      NUMBER(5)            NOT NULL,
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  CD_CGC                   VARCHAR2(14 BYTE),
  NR_ATENDIMENTO           NUMBER(10),
  CD_BANCO                 NUMBER(5),
  CD_AGENCIA_BANCARIA      VARCHAR2(8 BYTE),
  CD_CGC_EMP_CRED          VARCHAR2(14 BYTE),
  NR_CARTAO_CRED           VARCHAR2(20 BYTE),
  NR_DOCUMENTO_CRED        VARCHAR2(20 BYTE),
  NR_LOTE_CONTABIL         NUMBER(10),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  DT_BAIXA                 DATE,
  VL_ESPECIE               NUMBER(15,2),
  NR_SEQ_TRANS_FIN         NUMBER(10),
  DT_CONTABIL              DATE                 NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  NR_RECIBO                NUMBER(10),
  NR_SEQ_TRANS_CAIXA       NUMBER(10),
  NR_SEQ_CAIXA_REC         NUMBER(10),
  IE_LIB_CAIXA             VARCHAR2(1 BYTE),
  DT_AUTENTICACAO          DATE,
  NR_SEQ_CONTA_BANCO       NUMBER(10),
  NR_DOCUMENTO             VARCHAR2(100 BYTE),
  NR_SEQ_PROPOSTA_PAGADOR  NUMBER(10),
  IE_FRANQUIA              VARCHAR2(1 BYTE),
  NR_SEQ_CONTRATO          NUMBER(10),
  NR_SEQ_TIT_REC_COBR      NUMBER(10),
  NR_SEQ_TIPO              NUMBER(10),
  NR_SEQ_VIAGEM            NUMBER(10),
  NR_TITULO_DUPLICIDADE    NUMBER(10),
  NR_SEQ_GRUPO_PROD        NUMBER(10),
  CD_PESSOA_ADIANT         VARCHAR2(10 BYTE),
  IE_STATUS                VARCHAR2(15 BYTE),
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  IE_STATUS_ANT            VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE,
  VL_ADTO_ESTRANG          NUMBER(15,2),
  VL_COTACAO               NUMBER(21,10),
  VL_COMPLEMENTO           NUMBER(15,2),
  VL_SALDO_ESTRANG         NUMBER(15,2),
  NR_SEQ_ORCAMENTO_PAC     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADIANTA_AGEBANC_FK_I ON TASY.ADIANTAMENTO
(CD_BANCO, CD_AGENCIA_BANCARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_AGEBANC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_ATEPACI_FK_I ON TASY.ADIANTAMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_BANESTA_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_CAIREC_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_CONTRAT_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_ESTABEL_FK_I ON TASY.ADIANTAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_GRPRFIN_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_GRUPO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_GRPRFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_I1 ON TASY.ADIANTAMENTO
(CD_ESTABELECIMENTO, DT_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_LOTCONT_FK_I ON TASY.ADIANTAMENTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ADIANTAMENTO_PK ON TASY.ADIANTAMENTO
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_MOEDA_FK_I ON TASY.ADIANTAMENTO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_ORCPACI_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_ORCAMENTO_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_PESFISI_FK_I ON TASY.ADIANTAMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_PESFISI_FK2_I ON TASY.ADIANTAMENTO
(CD_PESSOA_ADIANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADIANTA_PESJURI_FK_I ON TASY.ADIANTAMENTO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_PLSPRPG_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_PROPOSTA_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_PLSPRPG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_TIPRECE_FK_I ON TASY.ADIANTAMENTO
(CD_TIPO_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_TIPRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_TIRECOB_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_TIT_REC_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_TIRECOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_TITRECE_FK_I ON TASY.ADIANTAMENTO
(NR_TITULO_DUPLICIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_TPADIANT_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_TPADIANT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_TRAFINA_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_TRAFINA_FK2_I ON TASY.ADIANTAMENTO
(NR_SEQ_TRANS_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ADIANTA_VIAGEM_FK_I ON TASY.ADIANTAMENTO
(NR_SEQ_VIAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADIANTA_VIAGEM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.adiantamento_insert
before insert ON TASY.ADIANTAMENTO for each row
declare

ie_permite_incluir_adto_w	varchar2(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	obter_param_usuario(802, 42, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo, ie_permite_incluir_adto_w);

	if (nvl(ie_permite_incluir_adto_w,'S') = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(455054);
	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Adiantamento_Atual
BEFORE INSERT OR UPDATE ON TASY.ADIANTAMENTO FOR EACH ROW
DECLARE

vl_devolucao_w		number(15,2)	:= 0;
vl_baixa_titulo_w		number(15,2)	:= 0;
vl_receb_conv_w		number(15,2)	:= 0;
dt_fechamento_w		date		:= null;
nr_recibo_w		number(10);
vl_vinc_lote_audit_w	number(15,2);
ie_atualizar_saldo_adiant_w	varchar2(1)	:= 'S';
vl_receb_adiant_w		number(15,2)	:= 0;
vl_troco_adiant_w		number(15,2) 	:= 0;
ie_devo_adiant_w		varchar2(1);
ie_adto_mesma_pessoa_w	varchar2(1);
cd_pessoa_fisica_w 		caixa_receb.cd_pessoa_fisica%type;
cd_cgc_w				caixa_receb.cd_cgc%type;

begin

Obter_Param_Usuario(813, 195, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo, ie_devo_adiant_w);
Obter_Param_Usuario(813, 207, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo, ie_adto_mesma_pessoa_w);

if	(inserting) then
	begin
	:new.nm_usuario_nrec		:= wheb_usuario_pck.get_nm_usuario;
	:new.dt_atualizacao_nrec	:= sysdate;
	end;
end if;

if	((updating) and
	((:new.cd_tipo_recebimento <> :old.cd_tipo_recebimento) or (:new.ie_situacao <> :old.ie_situacao) or (:new.ie_status <> :old.ie_status))) then
	gerar_adiantamento_alteracao(:new.nr_adiantamento,wheb_usuario_pck.get_nm_usuario,:old.cd_tipo_recebimento,:new.cd_tipo_recebimento,:old.ie_situacao,:new.ie_situacao,:old.ie_status,:new.ie_status);
end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:new.vl_adiantamento > 0) then
		begin

		select	nvl(sum(vl_devolucao),0)
		into	vl_devolucao_w
		from	adiantamento_dev
		where	nr_adiantamento	= :new.nr_adiantamento;

		select	nvl(max(ie_atualizar_saldo_adiant_mens),'S')
		into	ie_atualizar_saldo_adiant_w
		from	pls_parametros_cr a
		where	a.cd_estabelecimento	= :new.cd_estabelecimento;

		select	nvl(sum(vl_recebido),0) + nvl(sum(vl_rec_maior),0) + nvl(sum(vl_juros),0) + nvl(sum(vl_multa),0)
		into	vl_baixa_titulo_w
		from	Titulo_Receber_liq
		where	nr_adiantamento	= :new.nr_adiantamento
		and	nvl(ie_lib_caixa, 'S') = 'S';

		if	(ie_atualizar_saldo_adiant_w = 'N') then
			select	nvl(sum(vl_recebido),0) + nvl(sum(vl_rec_maior),0) + nvl(sum(vl_juros),0) + nvl(sum(vl_multa),0)
			into	vl_baixa_titulo_w
			from	Titulo_Receber_liq
			where	nr_adiantamento	= :new.nr_adiantamento
			and	nvl(ie_lib_caixa, 'S') = 'S'
			and	exists	(select	1
					from	movto_trans_financ x
					where	x.nr_adiantamento = :new.nr_adiantamento);
		end if;

		select 	nvl(sum(b.vl_vinculacao),0)
		into	vl_receb_conv_w
		from	convenio_retorno c,
			convenio_ret_receb b,
			convenio_receb a
		where	b.nr_seq_receb	= a.nr_sequencia
		and	b.nr_seq_retorno	= c.nr_sequencia
		and	c.ie_status_retorno	<> 'F'
		and	a.nr_adiantamento	= :new.nr_adiantamento;

		select	nvl(sum(a.vl_vinculado),0)
		into	vl_receb_adiant_w
		from	convenio_receb_adiant a
		where	1 = 1
		/*not exists
			(select	1
			from	convenio_retorno y,
				convenio_ret_receb x
			where	y.ie_status_retorno	= 'F'
			and	x.nr_seq_retorno	= y.nr_sequencia
			and	x.nr_seq_receb	= a.nr_seq_receb) */
		and	a.nr_adiantamento	= :new.nr_adiantamento;

		select 	nvl(sum(b.vl_vinculado),0)
		into	vl_vinc_lote_audit_w
		from	lote_audit_hist c,
			lote_audit_hist_receb b,
			convenio_receb a
		where	b.nr_seq_receb	= a.nr_sequencia
		and	b.nr_seq_lote	= c.nr_sequencia
		and	c.dt_fechamento is null
		and	a.nr_adiantamento	= :new.nr_adiantamento;

		end;
	end if;

	if	(:old.nr_seq_caixa_rec is not null) and
		(:new.ie_lib_caixa = 'S') and
		(nvl(ie_devo_adiant_w,'N') = 'N') then

		select	nvl(sum(a.vl_troco_informado),0)
		into	vl_troco_adiant_w
		from	caixa_receb a,
			tipo_recebimento b
		where	a.cd_tipo_receb_caixa	= b.cd_tipo_recebimento
		and	a.dt_cancelamento 	is null
		and	a.nr_sequencia		= :old.nr_seq_caixa_rec;
	end if;

	if	(:old.nr_seq_caixa_rec is not null) and
		(:old.vl_adiantamento <> :new.vl_adiantamento) then
		select	max(dt_fechamento),
			max(nr_recibo)
		into	dt_fechamento_w,
			nr_recibo_w
		from	caixa_receb
		where	nr_sequencia	= :old.nr_seq_caixa_rec;

		if	(dt_fechamento_w is not null) then

			wheb_mensagem_pck.exibir_mensagem_abort(185392,'NR_RECIBO_W='||nr_recibo_w);
		end if;
	end if;

	:new.vl_saldo	:= nvl(:new.vl_adiantamento,0) - nvl(vl_devolucao_w,0) - nvl(vl_baixa_titulo_w,0) - nvl(vl_receb_conv_w,0) - nvl(vl_vinc_lote_audit_w,0) - nvl(vl_receb_adiant_w,0) - nvl(vl_troco_adiant_w,0);

	if	(:new.vl_adiantamento < 0) or
		(:new.vl_saldo < 0) then

		wheb_mensagem_pck.exibir_mensagem_abort(185393,'VL_SALDO_W='||:new.vl_saldo);
	end if;


	if	((inserting) and
		 (:new.cd_pessoa_fisica is null) and
		 (:new.cd_cgc is null)) or
		((updating) and
		 (:new.cd_pessoa_fisica is null) and
		 (:new.cd_cgc is null) and
		 ((:old.cd_pessoa_fisica is not null) or
		  (:old.cd_cgc is not null))) then

		wheb_mensagem_pck.exibir_mensagem_abort(185394);
	end if;

	if	(:new.ie_lib_caixa = 'N') and (:new.nr_seq_caixa_rec is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(200046);
	end if;

	if (nvl(ie_adto_mesma_pessoa_w,'N') = 'S') and (:new.nr_seq_caixa_rec is not null) then
		select	nvl(max(cd_pessoa_fisica),0),
				nvl(max(cd_cgc),0)
		into	cd_pessoa_fisica_w,
				cd_cgc_w
		from	caixa_receb
		where	nr_sequencia	= :new.nr_seq_caixa_rec;

		if ((cd_pessoa_fisica_w > 0) and (nvl(:new.cd_pessoa_fisica,0) <> cd_pessoa_fisica_w)) or
			((cd_cgc_w > 0) and (nvl(:new.cd_cgc,0) <> cd_cgc_w)) then
			wheb_mensagem_pck.exibir_mensagem_abort(460322);
		end if;
	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.adiantamento_after_insert after
  insert ON TASY.ADIANTAMENTO for each row
declare
  qt_reg_w number(10);
  reg_integracao_p                gerar_int_padrao.reg_integracao;
  begin
  if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
  select count(*)
  into qt_reg_w
  from intpd_fila_transmissao
  where nr_seq_documento                               = to_char(:new.nr_adiantamento)
  and to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
  and ie_evento                                       in ('323');
  if (qt_reg_w                                         = 0) then
    /*envio de cadastros financeiros - baixa de titulo*/
    gerar_int_padrao.gravar_integracao('323', :new.nr_adiantamento, :new.nm_usuario, reg_integracao_p);
  end if;
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ADIANTAMENTO_DELETE
BEFORE DELETE ON TASY.ADIANTAMENTO FOR EACH ROW
DECLARE

dt_fechamento_w			date	:= null;
nr_recibo_w			number(10);
nr_seq_cheque_cr_w		number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:old.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/*r.aise_application_error(-20011,'Nao e possivel excluir o adiantamento! ' || chr(13) ||
					'O recibo ' || nr_recibo_w || ' ja foi fechado!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(267355,'nr_recibo_w='||nr_recibo_w);
	end if;
end if;

select	max(nr_seq_cheque)
into	nr_seq_cheque_cr_w
from	cheque_cr
where	nr_adiantamento	= :old.nr_adiantamento;

if	(nr_seq_cheque_cr_w is not null) then
	/*r.aise_application_error(-20011,'Nao e possivel excluir o adiantamento! ' || chr(10) ||
					'E necessario desvincular o cheque.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(267356);

end if;
end if;

end;
/


ALTER TABLE TASY.ADIANTAMENTO ADD (
  CONSTRAINT ADIANTAMENTO_PK
 PRIMARY KEY
 (NR_ADIANTAMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADIANTAMENTO ADD (
  CONSTRAINT ADIANTA_ORCPACI_FK 
 FOREIGN KEY (NR_SEQ_ORCAMENTO_PAC) 
 REFERENCES TASY.ORCAMENTO_PACIENTE (NR_SEQUENCIA_ORCAMENTO),
  CONSTRAINT ADIANTA_VIAGEM_FK 
 FOREIGN KEY (NR_SEQ_VIAGEM) 
 REFERENCES TASY.VIA_VIAGEM (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_DUPLICIDADE) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT ADIANTA_GRPRFIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PROD) 
 REFERENCES TASY.GRUPO_PROD_FINANC (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_ADIANT) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ADIANTA_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_PLSPRPG_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA_PAGADOR) 
 REFERENCES TASY.PLS_PROPOSTA_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_TIRECOB_FK 
 FOREIGN KEY (NR_SEQ_TIT_REC_COBR) 
 REFERENCES TASY.TITULO_RECEBER_COBR (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_TPADIANT_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_ADIANTAMENTO (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_CAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT ADIANTA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ADIANTA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ADIANTA_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT ADIANTA_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT ADIANTA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ADIANTA_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ADIANTA_TIPRECE_FK 
 FOREIGN KEY (CD_TIPO_RECEBIMENTO) 
 REFERENCES TASY.TIPO_RECEBIMENTO (CD_TIPO_RECEBIMENTO),
  CONSTRAINT ADIANTA_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ADIANTAMENTO TO NIVEL_1;

