ALTER TABLE TASY.SCHEM_TEST_PRESET
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEM_TEST_PRESET CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEM_TEST_PRESET
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_PRESET            VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_URL           NUMBER(10),
  NR_SEQ_BROWSER       NUMBER(10),
  NR_SEQ_SERVICE       NUMBER(10),
  NR_SEQ_ROBOT         NUMBER(10),
  NR_SEQ_GRID          NUMBER(10),
  IE_RUNTYPE           VARCHAR2(255 BYTE),
  DT_LAST              DATE,
  IE_SITUACAO          VARCHAR2(255 BYTE),
  DS_INFO              VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCTEPRE_PK ON TASY.SCHEM_TEST_PRESET
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTEPRE_SCTEBRO_FK_I ON TASY.SCHEM_TEST_PRESET
(NR_SEQ_BROWSER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTEPRE_SCTEGRI_FK_I ON TASY.SCHEM_TEST_PRESET
(NR_SEQ_GRID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTEPRE_SCTEROB_FK_I ON TASY.SCHEM_TEST_PRESET
(NR_SEQ_ROBOT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTEPRE_SCTESER_FK_I ON TASY.SCHEM_TEST_PRESET
(NR_SEQ_SERVICE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTEPRE_SCTEURL_FK_I ON TASY.SCHEM_TEST_PRESET
(NR_SEQ_URL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEM_TEST_PRESET ADD (
  CONSTRAINT SCTEPRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SCHEM_TEST_PRESET ADD (
  CONSTRAINT SCTEPRE_SCTEBRO_FK 
 FOREIGN KEY (NR_SEQ_BROWSER) 
 REFERENCES TASY.SCHEM_TEST_BROWSER (NR_SEQUENCIA),
  CONSTRAINT SCTEPRE_SCTEGRI_FK 
 FOREIGN KEY (NR_SEQ_GRID) 
 REFERENCES TASY.SCHEM_TEST_GRID (NR_SEQUENCIA),
  CONSTRAINT SCTEPRE_SCTEROB_FK 
 FOREIGN KEY (NR_SEQ_ROBOT) 
 REFERENCES TASY.SCHEM_TEST_ROBOT (NR_SEQUENCIA),
  CONSTRAINT SCTEPRE_SCTESER_FK 
 FOREIGN KEY (NR_SEQ_SERVICE) 
 REFERENCES TASY.SCHEM_TEST_SERVICE (NR_SEQUENCIA),
  CONSTRAINT SCTEPRE_SCTEURL_FK 
 FOREIGN KEY (NR_SEQ_URL) 
 REFERENCES TASY.SCHEM_TEST_URL (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCHEM_TEST_PRESET TO NIVEL_1;

