ALTER TABLE TASY.MPREV_CAPTACAO_SEL_DES_INC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_CAPTACAO_SEL_DES_INC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_CAPTACAO_SEL_DES_INC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_CAPTACAO_SEL_DEST  NUMBER(10),
  IE_INCONSISTENCIA         VARCHAR2(3 BYTE)    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRCSDI_MPRCSDE_FK_I ON TASY.MPREV_CAPTACAO_SEL_DES_INC
(NR_SEQ_CAPTACAO_SEL_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRCSDI_PK ON TASY.MPREV_CAPTACAO_SEL_DES_INC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRCSDI_UK ON TASY.MPREV_CAPTACAO_SEL_DES_INC
(NR_SEQ_CAPTACAO_SEL_DEST, IE_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_CAPTACAO_SEL_DES_INC ADD (
  CONSTRAINT MPRCSDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT MPRCSDI_UK
 UNIQUE (NR_SEQ_CAPTACAO_SEL_DEST, IE_INCONSISTENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_CAPTACAO_SEL_DES_INC ADD (
  CONSTRAINT MPRCSDI_MPRCSDE_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO_SEL_DEST) 
 REFERENCES TASY.MPREV_CAPTACAO_SEL_DEST (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MPREV_CAPTACAO_SEL_DES_INC TO NIVEL_1;

