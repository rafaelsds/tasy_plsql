ALTER TABLE TASY.CARESTREM_ENCOUNTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARESTREM_ENCOUNTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARESTREM_ENCOUNTER
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  IE_TIPO_MOV           VARCHAR2(20 BYTE)       NOT NULL,
  IE_PACIENTE_ISOLADO   VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_INTEGRADO          VARCHAR2(1 BYTE)        NOT NULL,
  NR_ATENDIMENTO        NUMBER(10)              NOT NULL,
  CD_UNIDADE_BASICA     VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL      VARCHAR2(10 BYTE),
  NR_ACESSO_DICOM       VARCHAR2(20 BYTE),
  DT_ATUALIZACAO        DATE,
  NM_USUARIO            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_INTEGRACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CARESENCOU_ATEPACI_FK_I ON TASY.CARESTREM_ENCOUNTER
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CARESENCOU_PESFISI_FK_I ON TASY.CARESTREM_ENCOUNTER
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CARESENCOU_PK ON TASY.CARESTREM_ENCOUNTER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CARESTREM_ENCOUNTER ADD (
  CONSTRAINT CARESENCOU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CARESTREM_ENCOUNTER TO NIVEL_1;

