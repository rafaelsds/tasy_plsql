ALTER TABLE TASY.COM_FORMA_AQUISICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_FORMA_AQUISICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_FORMA_AQUISICAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_FORMA_AQUISICAO   VARCHAR2(15 BYTE),
  NR_SEQ_CLIENTE       NUMBER(10),
  NR_SEQ_CANAL_ACESSO  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CFAQUIS_COMCLIE_FK_I ON TASY.COM_FORMA_AQUISICAO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CFAQUIS_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CFAQUIS_COMFOCO_FK_I ON TASY.COM_FORMA_AQUISICAO
(NR_SEQ_CANAL_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CFAQUIS_COMFOCO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CFAQUIS_PK ON TASY.COM_FORMA_AQUISICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CFAQUIS_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_FORMA_AQUISICAO ADD (
  CONSTRAINT CFAQUIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_FORMA_AQUISICAO ADD (
  CONSTRAINT CFAQUIS_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT CFAQUIS_COMFOCO_FK 
 FOREIGN KEY (NR_SEQ_CANAL_ACESSO) 
 REFERENCES TASY.COM_FORMA_CONHEC (NR_SEQUENCIA));

GRANT SELECT ON TASY.COM_FORMA_AQUISICAO TO NIVEL_1;

