ALTER TABLE TASY.REGRA_DIAS_ANTIMICROBIANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_DIAS_ANTIMICROBIANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_DIAS_ANTIMICROBIANO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_DOENCA_CID        VARCHAR2(10 BYTE),
  CD_TOPOGRAFIA_CIH    NUMBER(10),
  IE_OBJETIVO          VARCHAR2(15 BYTE),
  QT_DIAS_PREV         NUMBER(3)                NOT NULL,
  CD_MATERIAL          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGDIAA_CIDDOEN_FK_I ON TASY.REGRA_DIAS_ANTIMICROBIANO
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIAA_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDIAA_CIHTOPO_FK_I ON TASY.REGRA_DIAS_ANTIMICROBIANO
(CD_TOPOGRAFIA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIAA_CIHTOPO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGDIAA_MATERIA_FK_I ON TASY.REGRA_DIAS_ANTIMICROBIANO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGDIAA_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGDIAA_PK ON TASY.REGRA_DIAS_ANTIMICROBIANO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_DIAS_ANTIMICROBIANO ADD (
  CONSTRAINT REGDIAA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_DIAS_ANTIMICROBIANO ADD (
  CONSTRAINT REGDIAA_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT REGDIAA_CIHTOPO_FK 
 FOREIGN KEY (CD_TOPOGRAFIA_CIH) 
 REFERENCES TASY.CIH_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT REGDIAA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.REGRA_DIAS_ANTIMICROBIANO TO NIVEL_1;

