ALTER TABLE TASY.PCS_ATRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_ATRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_ATRIBUTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ATRIBUTO          VARCHAR2(255 BYTE)       NOT NULL,
  DS_MACRO             VARCHAR2(80 BYTE)        NOT NULL,
  DS_SQL               VARCHAR2(4000 BYTE),
  NM_TABELA            VARCHAR2(50 BYTE),
  NM_CAMPO             VARCHAR2(50 BYTE),
  DS_APLICACAO         VARCHAR2(15 BYTE)        NOT NULL,
  DS_RESTRICAO_DATA    VARCHAR2(255 BYTE),
  IE_PERIODO           VARCHAR2(15 BYTE),
  QT_PERIODO           NUMBER(10),
  IE_PERIODO_ATUAL     VARCHAR2(1 BYTE),
  IE_OPERACAO          VARCHAR2(15 BYTE),
  DS_SQL_INFORMADO     VARCHAR2(4000 BYTE),
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PCSATRI_PK ON TASY.PCS_ATRIBUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PCS_ATRIBUTO_tp  after update ON TASY.PCS_ATRIBUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_MACRO,1,4000),substr(:new.DS_MACRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MACRO',ie_log_w,ds_w,'PCS_ATRIBUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ATRIBUTO,1,4000),substr(:new.DS_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ATRIBUTO',ie_log_w,ds_w,'PCS_ATRIBUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pcs_atributo_beforeinsert
before insert ON TASY.PCS_ATRIBUTO for each row
declare
qt_existe_macro_w number(10,0) := 0;

pragma autonomous_transaction;

begin

select	count(*) qt_registro
into	qt_existe_macro_w
from	pcs_atributo
where	upper(ds_macro) = upper(:new.ds_macro)
and		nr_sequencia <> :new.nr_sequencia;

if (qt_existe_macro_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(277466);
end if;

end;
/


ALTER TABLE TASY.PCS_ATRIBUTO ADD (
  CONSTRAINT PCSATRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PCS_ATRIBUTO TO NIVEL_1;

