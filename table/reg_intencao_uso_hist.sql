DROP TABLE TASY.REG_INTENCAO_USO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_INTENCAO_USO_HIST
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_BRIEFING             VARCHAR2(4000 BYTE),
  DS_OBS_BRIEFING         VARCHAR2(255 BYTE),
  CD_VERSAO               VARCHAR2(255 BYTE),
  NR_SEQ_REG_VERSION_REV  NUMBER(10),
  DS_DESCRICAO            VARCHAR2(4000 BYTE),
  IE_TIPO_ALTERACAO       VARCHAR2(1 BYTE),
  DT_APROVACAO            DATE,
  IE_SITUACAO             VARCHAR2(1 BYTE),
  DS_INTENCAO_USO         VARCHAR2(255 BYTE),
  NR_SEQ_INTENCAO_USO     NUMBER(10),
  NR_SEQ_ESTAGIO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGIUH_REGEST_FK_I ON TASY.REG_INTENCAO_USO_HIST
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGIUH_REGINU_FK_I ON TASY.REG_INTENCAO_USO_HIST
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGIUH_REGVERREV_FK_I ON TASY.REG_INTENCAO_USO_HIST
(NR_SEQ_REG_VERSION_REV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_INTENCAO_USO_HIST ADD (
  CONSTRAINT REGIUH_REGEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.REG_ESTAGIO_CR (NR_SEQUENCIA),
  CONSTRAINT REGIUH_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA),
  CONSTRAINT REGIUH_REGVERREV_FK 
 FOREIGN KEY (NR_SEQ_REG_VERSION_REV) 
 REFERENCES TASY.REG_VERSION_REVISION (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REG_INTENCAO_USO_HIST TO NIVEL_1;

