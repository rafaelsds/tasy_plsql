ALTER TABLE TASY.NACIONALIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NACIONALIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.NACIONALIDADE
(
  CD_NACIONALIDADE     VARCHAR2(8 BYTE)         NOT NULL,
  DS_NACIONALIDADE     VARCHAR2(100 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_BRASILEIRO        VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_IDIOMA        NUMBER(10),
  CD_EXTERNO           VARCHAR2(255 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_BACEN             VARCHAR2(10 BYTE),
  NR_SEQ_APRESENTACAO  NUMBER(3),
  IE_TIPO_PAIS_NOM     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NACIONA_IDIOMA_FK_I ON TASY.NACIONALIDADE
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NACIONA_IDIOMA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NACIONA_PK ON TASY.NACIONALIDADE
(CD_NACIONALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NACIONALIDADE ADD (
  CONSTRAINT NACIONA_PK
 PRIMARY KEY
 (CD_NACIONALIDADE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NACIONALIDADE ADD (
  CONSTRAINT NACIONA_IDIOMA_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.IDIOMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.NACIONALIDADE TO NIVEL_1;

