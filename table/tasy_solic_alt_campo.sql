ALTER TABLE TASY.TASY_SOLIC_ALT_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_SOLIC_ALT_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_SOLIC_ALT_CAMPO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NM_TABELA               VARCHAR2(30 BYTE)     NOT NULL,
  NM_ATRIBUTO             VARCHAR2(50 BYTE)     NOT NULL,
  DS_CHAVE_SIMPLES        VARCHAR2(255 BYTE),
  DS_CHAVE_COMPOSTA       VARCHAR2(500 BYTE),
  NR_SEQ_SOLICITACAO      NUMBER(10)            NOT NULL,
  IE_STATUS               VARCHAR2(1 BYTE)      NOT NULL,
  DS_VALOR_OLD            VARCHAR2(2000 BYTE),
  DS_VALOR_NEW            VARCHAR2(2000 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  NR_SEQ_MOTIVO_REJEICAO  NUMBER(10),
  IE_TIPO_LOGIN_SOLIC     VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TASSOAC_I1 ON TASY.TASY_SOLIC_ALT_CAMPO
(NM_TABELA, DS_CHAVE_SIMPLES, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASSOAC_I2 ON TASY.TASY_SOLIC_ALT_CAMPO
(NM_TABELA, DS_CHAVE_COMPOSTA, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASSOAC_PK ON TASY.TASY_SOLIC_ALT_CAMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TASSOAC_PLSMRPF_FK_I ON TASY.TASY_SOLIC_ALT_CAMPO
(NR_SEQ_MOTIVO_REJEICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TASSOAC_PLSMRPF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TASSOAC_TASSOAL_FK_I ON TASY.TASY_SOLIC_ALT_CAMPO
(NR_SEQ_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.tasy_solic_alt_campo_atual
before insert or update ON TASY.TASY_SOLIC_ALT_CAMPO for each row
declare
-- Trigger criada por quest�o de performance

ie_tipo_solicitacao_w		tasy_solic_alteracao.ie_tipo_solicitacao%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_cgc_w			pessoa_juridica.cd_cgc%type;

begin
select	max(ie_tipo_solicitacao)
into	ie_tipo_solicitacao_w
from	tasy_solic_alteracao
where	nr_sequencia	= :new.nr_seq_solicitacao
and	cd_pessoa_fisica is null
and	cd_cgc is null;

-- Tipo Solicita��o Prestador
if	(ie_tipo_solicitacao_w = 'P') and
	(inserting) then
	-- Pega Pessoa F�sica
	begin
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pls_prestador	c
	where	(:new.ds_chave_simples	= c.cd_pessoa_fisica
	or	substr(:new.ds_chave_composta,1,length(:new.ds_chave_composta)-1) = ('CD_PESSOA_FISICA=' || c.cd_pessoa_fisica || '#@#@IE_TIPO_COMPLEMENTO=')
	or	:new.ds_chave_composta	= 'CD_PESSOA_FISICA=' || c.cd_pessoa_fisica
	or	:new.ds_chave_composta	= 'CD_PESSOA_FISICA=' || q'{'}' || c.cd_pessoa_fisica ||  q'{'}' ) --Formato portal
	and	rownum <= 1;
	exception
	when no_data_found then
		cd_pessoa_fisica_w	:= null;
	end;

	--Pega Pessoa Jur�dica
	if 	(cd_pessoa_fisica_w is null) then
		begin
		select	cd_cgc
		into	cd_cgc_w
		from	pls_prestador	c
		where	(:new.ds_chave_simples        = c.cd_cgc
		or	substr(:new.ds_chave_composta,1,instr(:new.ds_chave_composta, '#@#@IE_TIPO_COMPLEMENTO=') + length('#@#@IE_TIPO_COMPLEMENTO=') -1) = ('CD_CGC=' || c.cd_cgc || '#@#@IE_TIPO_COMPLEMENTO=')
		or	:new.ds_chave_composta     = 'CD_CGC=' || c.cd_cgc
		or	:new.ds_chave_composta     = 'CD_CGC=' || q'{'}' || c.cd_cgc || q'{'}') --Formato portal
		and	rownum <= 1;
		exception
		when no_data_found then
			cd_cgc_w	:= null;
		end;
	end if;

	-- Atualizar os dados do prestador solicitante
	update	tasy_solic_alteracao
	set	cd_pessoa_fisica	= cd_pessoa_fisica_w,
		cd_cgc			= cd_cgc_w
	where	nr_sequencia		= :new.nr_seq_solicitacao;
end if;

end;
/


ALTER TABLE TASY.TASY_SOLIC_ALT_CAMPO ADD (
  CONSTRAINT TASSOAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_SOLIC_ALT_CAMPO ADD (
  CONSTRAINT TASSOAC_TASSOAL_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO) 
 REFERENCES TASY.TASY_SOLIC_ALTERACAO (NR_SEQUENCIA),
  CONSTRAINT TASSOAC_PLSMRPF_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_REJEICAO) 
 REFERENCES TASY.PLS_MOTIVO_REJEICAO_ALT_PF (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_SOLIC_ALT_CAMPO TO NIVEL_1;

