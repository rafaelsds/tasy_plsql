ALTER TABLE TASY.UNID_ATEND_HIGIENIZACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNID_ATEND_HIGIENIZACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNID_ATEND_HIGIENIZACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_UNIDADE       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_INICIO            DATE,
  DT_FIM               DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_LIMPEZA      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.UNIATHG_PK ON TASY.UNID_ATEND_HIGIENIZACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATHG_PK
  MONITORING USAGE;


CREATE INDEX TASY.UNIATHG_UNIATEND_FK_I ON TASY.UNID_ATEND_HIGIENIZACAO
(NR_SEQ_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.UNID_ATEND_HIGIENIZACAO ADD (
  CONSTRAINT UNIATHG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.UNID_ATEND_HIGIENIZACAO TO NIVEL_1;

