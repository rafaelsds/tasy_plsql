ALTER TABLE TASY.CHECKUP_IMP_PESSOA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHECKUP_IMP_PESSOA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHECKUP_IMP_PESSOA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO            DATE,
  NM_PESSOA_FISICA     VARCHAR2(120 BYTE)       NOT NULL,
  DT_FIM               DATE,
  CD_EMPRESA_REF       NUMBER(10)               NOT NULL,
  NR_CPF               VARCHAR2(11 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  IE_VINCULO           VARCHAR2(2 BYTE),
  DT_ULTIMO_CHECKUP    DATE,
  QT_PERIODICIDADE     NUMBER(10),
  DT_INICIO_VIGENCIA   DATE,
  DT_NASCIMENTO        DATE,
  IE_SEXO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHIMPPE_EMPREFE_FK_I ON TASY.CHECKUP_IMP_PESSOA
(CD_EMPRESA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHIMPPE_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHIMPPE_PESFISI_FK_I ON TASY.CHECKUP_IMP_PESSOA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHIMPPE_PK ON TASY.CHECKUP_IMP_PESSOA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHIMPPE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CHECKUP_IMP_PESSOA_tp  after update ON TASY.CHECKUP_IMP_PESSOA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'CHECKUP_IMP_PESSOA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'CHECKUP_IMP_PESSOA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'CHECKUP_IMP_PESSOA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CHECKUP_IMP_PESSOA ADD (
  CONSTRAINT CHIMPPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHECKUP_IMP_PESSOA ADD (
  CONSTRAINT CHIMPPE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHIMPPE_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REF) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA));

GRANT SELECT ON TASY.CHECKUP_IMP_PESSOA TO NIVEL_1;

