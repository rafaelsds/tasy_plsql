ALTER TABLE TASY.PRESCR_PROC_PERDA_GANHO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_PERDA_GANHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_PERDA_GANHO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TIPO           NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRPRPEGA_PK ON TASY.PRESCR_PROC_PERDA_GANHO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPRPEGA_SETATEN_FK_I ON TASY.PRESCR_PROC_PERDA_GANHO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPRPEGA_TIPPEGA_FK_I ON TASY.PRESCR_PROC_PERDA_GANHO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRESCR_PROC_PERDA_GANHO ADD (
  CONSTRAINT PRPRPEGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PRESCR_PROC_PERDA_GANHO ADD (
  CONSTRAINT PRPRPEGA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRPRPEGA_TIPPEGA_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_PERDA_GANHO (NR_SEQUENCIA));

