ALTER TABLE TASY.SAN_TRANS_REACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_TRANS_REACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_TRANS_REACAO
(
  NR_SEQ_PRODUCAO      NUMBER(10)               NOT NULL,
  NR_SEQ_REACAO        NUMBER(10),
  DS_CONDUTA           VARCHAR2(4000 BYTE),
  CD_PF_REALIZOU       VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQUENCIA         NUMBER(10),
  DT_REACAO            DATE,
  DS_SINTOMA           VARCHAR2(255 BYTE),
  DT_LIB_MEDICO        DATE,
  NM_USUARIO_MEDICO    VARCHAR2(15 BYTE),
  DS_OBS_MEDICO        VARCHAR2(255 BYTE),
  IE_CLASSIFICACAO     NUMBER(3),
  IE_GRAVIDADE         NUMBER(3),
  IE_CORRELACAO        NUMBER(3),
  IE_DIAGNOSTICO       NUMBER(3),
  DT_CLASSIFICACAO     DATE,
  CD_PF_CLASSIFICACAO  VARCHAR2(10 BYTE),
  NR_SEQ_GRUPO_REACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANTRRE_PESFISI_FK_I ON TASY.SAN_TRANS_REACAO
(CD_PF_REALIZOU)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANTRRE_PESFISI_FK2_I ON TASY.SAN_TRANS_REACAO
(CD_PF_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SANTRRE_PK ON TASY.SAN_TRANS_REACAO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANTRRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANTRRE_SAGRREA_FK_I ON TASY.SAN_TRANS_REACAO
(NR_SEQ_GRUPO_REACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANTRRE_SANPROD_FK_I ON TASY.SAN_TRANS_REACAO
(NR_SEQ_PRODUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANTRRE_SANTIRE_FK_I ON TASY.SAN_TRANS_REACAO
(NR_SEQ_REACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANTRRE_SANTIRE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.san_trans_reacao_after_insert
after insert ON TASY.SAN_TRANS_REACAO for each row
declare
nr_seq_evento_w		Number(10);
nr_atendimento_w	Number(10);
cd_pessoa_fisica_w	Varchar2(10);
ie_classificacao_w	Varchar2(10);
ds_evento_w		varchar2(4000);
begin

obter_param_usuario(450, 339, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, nr_seq_evento_w);



if (nr_seq_evento_w is not null) then

	ie_classificacao_w := obter_classif_evento(nr_seq_evento_w);

	select	max(a.nr_atendimento),
		max(a.cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	san_transfusao a,
		san_producao b
	where	b.nr_seq_transfusao = a.nr_sequencia
	and	b.nr_sequencia = :new.nr_seq_producao;

	ds_evento_w := nvl(:new.ds_sintoma,' ') || chr(13)||nvl(substr(:new.ds_conduta,1,2000),' ');

	if (Obter_Setor_Atendimento(nr_atendimento_w) is not null) then

		insert into qua_evento_paciente(nr_sequencia,
						cd_estabelecimento,
						dt_atualizacao,
						nm_usuario,
						nr_atendimento,
						nr_seq_evento,
						dt_evento,
						ds_evento,
						cd_setor_atendimento,
						dt_cadastro,
						nm_usuario_origem,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nm_usuario_reg,
						ie_situacao,
						dt_liberacao,
						cd_pessoa_fisica,
						cd_perfil_ativo,
						ie_status,
						ie_origem,
						ie_tipo_evento,
						ie_classificacao,
						cd_funcao_ativa)
					Values (qua_evento_paciente_seq.nextval,
						wheb_usuario_pck.get_cd_estabelecimento,
						sysdate,
						:new.nm_usuario,
						nr_atendimento_w,
						nr_seq_evento_w,
						nvl(:new.dt_reacao,sysdate),
						ds_evento_w,
						Obter_Setor_Atendimento(nr_atendimento_w),
						nvl(:new.dt_atualizacao_nrec,sysdate),
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario,
						'A',
						sysdate,
						cd_pessoa_fisica_w,
						obter_perfil_ativo,
						'1',
						'S',
						'E',
						ie_classificacao_w,
						obter_funcao_ativa);
	else
		wheb_mensagem_pck.exibir_mensagem_abort(244307);
	end if;

end if;

end;
/


ALTER TABLE TASY.SAN_TRANS_REACAO ADD (
  CONSTRAINT SANTRRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_TRANS_REACAO ADD (
  CONSTRAINT SANTRRE_SAGRREA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REACAO) 
 REFERENCES TASY.SAN_GRUPO_REACAO (NR_SEQUENCIA),
  CONSTRAINT SANTRRE_PESFISI_FK2 
 FOREIGN KEY (CD_PF_CLASSIFICACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANTRRE_SANPROD_FK 
 FOREIGN KEY (NR_SEQ_PRODUCAO) 
 REFERENCES TASY.SAN_PRODUCAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SANTRRE_SANTIRE_FK 
 FOREIGN KEY (NR_SEQ_REACAO) 
 REFERENCES TASY.SAN_TIPO_REACAO (NR_SEQUENCIA),
  CONSTRAINT SANTRRE_PESFISI_FK 
 FOREIGN KEY (CD_PF_REALIZOU) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.SAN_TRANS_REACAO TO NIVEL_1;

