ALTER TABLE TASY.SCORE_META
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCORE_META CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCORE_META
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_META              NUMBER(15,4)             NOT NULL,
  NR_SEQ_GERENCIA      NUMBER(10),
  CD_CARGO             NUMBER(10)               NOT NULL,
  IE_FUNCAO_USUARIO    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ACOMETA_CARGO_FK_I ON TASY.SCORE_META
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ACOMETA_GERWHEB_FK_I ON TASY.SCORE_META
(NR_SEQ_GERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ACOMETA_PK ON TASY.SCORE_META
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCORE_META ADD (
  CONSTRAINT ACOMETA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SCORE_META ADD (
  CONSTRAINT ACOMETA_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT ACOMETA_GERWHEB_FK 
 FOREIGN KEY (NR_SEQ_GERENCIA) 
 REFERENCES TASY.GERENCIA_WHEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCORE_META TO NIVEL_1;

