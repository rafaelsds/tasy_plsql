ALTER TABLE TASY.PRECO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_MATERIAL
(
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_TAB_PRECO_MAT     NUMBER(4)                NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  VL_PRECO_VENDA       NUMBER(13,4)             NOT NULL,
  CD_MOEDA             NUMBER(3)                NOT NULL,
  IE_BRASINDICE        VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  QT_CONVERSAO         NUMBER(13,4),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  NR_SEQUENCIA_NF      NUMBER(10),
  NR_ITEM_NF           NUMBER(5),
  CD_CGC_FORNECEDOR    VARCHAR2(14 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_PRECO_VENDA       VARCHAR2(1 BYTE),
  IE_INPART            VARCHAR2(1 BYTE),
  IE_MANUAL            VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO        NUMBER(10),
  DT_FINAL_VIGENCIA    DATE,
  IE_INTEGRACAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRCMATE_MATERIA_FK_I ON TASY.PRECO_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRCMATE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRCMATE_MOEDA_FK_I ON TASY.PRECO_MATERIAL
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRCMATE_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRCMATE_PK ON TASY.PRECO_MATERIAL
(CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT, CD_MATERIAL, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRCMATE_TABPRMA_FK_I ON TASY.PRECO_MATERIAL
(CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRCMATE_UNIMEDI_FK_I ON TASY.PRECO_MATERIAL
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRCMATE_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMATE_NOTFIIT_FK_I ON TASY.PRECO_MATERIAL
(NR_SEQUENCIA_NF, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMATE_PESJURI_FK_I ON TASY.PRECO_MATERIAL
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMATE_PESJURI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.preco_material_delete
after delete ON TASY.PRECO_MATERIAL for each row
declare

qt_mat_justif_w		number(10,0)	:= 0;

begin

select	count(*)
into	qt_mat_justif_w
from	preco_mat_justificativa
where	cd_estabelecimento = :old.cd_estabelecimento
and	cd_tab_preco_mat = :old.cd_tab_preco_mat
and	cd_material = :old.cd_material
and	dt_vigencia = :old.dt_inicio_vigencia;

if	(qt_mat_justif_w > 0) then

	delete from preco_mat_justificativa
	where	cd_estabelecimento = :old.cd_estabelecimento
	and	cd_tab_preco_mat = :old.cd_tab_preco_mat
	and	cd_material = :old.cd_material
	and	dt_vigencia = :old.dt_inicio_vigencia;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.preco_material_update
after update ON TASY.PRECO_MATERIAL for each row
declare

qt_mat_justif_w		number(10,0)	:= 0;

begin

if	(:old.cd_material <> :new.cd_material) or
	(:old.dt_inicio_vigencia <> :new.dt_inicio_vigencia) then

	select	count(*)
	into	qt_mat_justif_w
	from	preco_mat_justificativa
	where	cd_estabelecimento = :old.cd_estabelecimento
	and	cd_tab_preco_mat = :old.cd_tab_preco_mat
	and	cd_material = :old.cd_material
	and	dt_vigencia = :old.dt_inicio_vigencia;

	if	(qt_mat_justif_w > 0) then

		update	preco_mat_justificativa
		set	cd_material = :new.cd_material,
			dt_vigencia = :new.dt_inicio_vigencia
		where	cd_estabelecimento = :old.cd_estabelecimento
		and	cd_tab_preco_mat = :old.cd_tab_preco_mat
		and	cd_material = :old.cd_material
		and	dt_vigencia = :old.dt_inicio_vigencia;

	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PRECO_MATERIAL_tp  after update ON TASY.PRECO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_ESTABELECIMENTO='||to_char(:old.CD_ESTABELECIMENTO)||'#@#@CD_TAB_PRECO_MAT='||to_char(:old.CD_TAB_PRECO_MAT)||'#@#@CD_MATERIAL='||to_char(:old.CD_MATERIAL)||'#@#@DT_INICIO_VIGENCIA='||to_char(:old.DT_INICIO_VIGENCIA); ds_w:=substr(:new.CD_MATERIAL,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA,1,4000),substr(:new.CD_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO,1,4000),substr(:new.NR_SEQ_MOTIVO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PRECO_VENDA,1,4000),substr(:new.VL_PRECO_VENDA,1,4000),:new.nm_usuario,nr_seq_w,'VL_PRECO_VENDA',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONVERSAO,1,4000),substr(:new.QT_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INPART,1,4000),substr(:new.IE_INPART,1,4000),:new.nm_usuario,nr_seq_w,'IE_INPART',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MANUAL,1,4000),substr(:new.IE_MANUAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_MANUAL',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TAB_PRECO_MAT,1,4000),substr(:new.CD_TAB_PRECO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_TAB_PRECO_MAT',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PRECO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PRECO_MATERIAL ADD (
  CONSTRAINT PRCMATE_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT, CD_MATERIAL, DT_INICIO_VIGENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_MATERIAL ADD (
  CONSTRAINT PRCMATE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PRCMATE_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PRCMATE_TABPRMA_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT) 
 REFERENCES TASY.TABELA_PRECO_MATERIAL (CD_ESTABELECIMENTO,CD_TAB_PRECO_MAT),
  CONSTRAINT PRCMATE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PREMATE_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PREMATE_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQUENCIA_NF, NR_ITEM_NF) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRECO_MATERIAL TO NIVEL_1;

