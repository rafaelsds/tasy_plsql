ALTER TABLE TASY.LAB_RESULTADO_LOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_RESULTADO_LOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_RESULTADO_LOC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_RESULTADO      NUMBER(10)              NOT NULL,
  IE_TIPO_LOCAL         VARCHAR2(3 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LABRELO_EXALARE_FK_I ON TASY.LAB_RESULTADO_LOC
(NR_SEQ_RESULTADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABRELO_EXALARE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LABRELO_PESFISI_FK_I ON TASY.LAB_RESULTADO_LOC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LABRELO_PK ON TASY.LAB_RESULTADO_LOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABRELO_PK
  MONITORING USAGE;


CREATE INDEX TASY.LABRELO_SETATEND_FK_I ON TASY.LAB_RESULTADO_LOC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LABRELO_SETATEND_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.LAB_RESULTADO_LOC ADD (
  CONSTRAINT LABRELO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_RESULTADO_LOC ADD (
  CONSTRAINT LABRELO_SETATEND_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT LABRELO_EXALARE_FK 
 FOREIGN KEY (NR_SEQ_RESULTADO) 
 REFERENCES TASY.EXAME_LAB_RESULTADO (NR_SEQ_RESULTADO),
  CONSTRAINT LABRELO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.LAB_RESULTADO_LOC TO NIVEL_1;

