ALTER TABLE TASY.CPOE_LOCAL_PADRAO_COLETA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_LOCAL_PADRAO_COLETA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_LOCAL_PADRAO_COLETA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_TIPO_ATENDIMENTO  NUMBER(10)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CPOELOCCOL_PK ON TASY.CPOE_LOCAL_PADRAO_COLETA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_LOCAL_PADRAO_COLETA ADD (
  CONSTRAINT CPOELOCCOL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

