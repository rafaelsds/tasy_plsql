ALTER TABLE TASY.MP_PROCESSO_OBJETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MP_PROCESSO_OBJETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MP_PROCESSO_OBJETO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PROCESSO      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_OBJETO        NUMBER(10)               NOT NULL,
  QT_TOPO              NUMBER(10)               NOT NULL,
  QT_ESQUERDA          NUMBER(10)               NOT NULL,
  QT_LARGURA           NUMBER(10)               NOT NULL,
  QT_ALTURA            NUMBER(10)               NOT NULL,
  DS_COR               VARCHAR2(15 BYTE),
  NM_OBJETO            VARCHAR2(255 BYTE),
  DS_OBJETO            LONG,
  NR_SEQ_APRES         NUMBER(10),
  NR_SEQ_OBJ_ORIGEM    NUMBER(10),
  NR_SEQ_OBJ_DESTINO   NUMBER(10),
  DS_LISTA_PONTOS      VARCHAR2(4000 BYTE),
  NR_SEQ_PROCESSO_REF  NUMBER(10),
  DS_COR_FONTE         VARCHAR2(15 BYTE),
  NM_FONTE             VARCHAR2(255 BYTE),
  QT_TAM_FONTE         NUMBER(10),
  IE_NEGRITO_FONTE     VARCHAR2(1 BYTE),
  IE_ITALICO_FONTE     VARCHAR2(1 BYTE),
  IE_SUB_FONTE         VARCHAR2(1 BYTE),
  IE_ALINHAMENTO_HOR   VARCHAR2(1 BYTE),
  QT_ESPACO_CAR        NUMBER(10),
  IE_IMAGEM            VARCHAR2(2 BYTE),
  IE_IMAGEM_1          VARCHAR2(2 BYTE),
  IE_IMAGEM_2          VARCHAR2(2 BYTE),
  IE_IMAGEM_3          VARCHAR2(2 BYTE),
  IE_IMAGEM_4          VARCHAR2(2 BYTE),
  PR_ADERENCIA         NUMBER(7,2)              NOT NULL,
  PR_ADERENCIA_ES      NUMBER(7,2)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPPROOB_MPOBJET_FK_I ON TASY.MP_PROCESSO_OBJETO
(NR_SEQ_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPROOB_MPOBJET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPROOB_MPPROCE_FK_I ON TASY.MP_PROCESSO_OBJETO
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPROOB_MPPROCE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPROOB_MPPROCE_FK1_I ON TASY.MP_PROCESSO_OBJETO
(NR_SEQ_PROCESSO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPROOB_MPPROCE_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPROOB_MPPROOB_FK_I ON TASY.MP_PROCESSO_OBJETO
(NR_SEQ_OBJ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPROOB_MPPROOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPPROOB_MPPROOB_FK1_I ON TASY.MP_PROCESSO_OBJETO
(NR_SEQ_OBJ_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPROOB_MPPROOB_FK1_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MPPROOB_PK ON TASY.MP_PROCESSO_OBJETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPPROOB_PK
  MONITORING USAGE;


ALTER TABLE TASY.MP_PROCESSO_OBJETO ADD (
  CONSTRAINT MPPROOB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MP_PROCESSO_OBJETO ADD (
  CONSTRAINT MPPROOB_MPPROCE_FK1 
 FOREIGN KEY (NR_SEQ_PROCESSO_REF) 
 REFERENCES TASY.MP_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT MPPROOB_MPOBJET_FK 
 FOREIGN KEY (NR_SEQ_OBJETO) 
 REFERENCES TASY.MP_OBJETO (NR_SEQUENCIA),
  CONSTRAINT MPPROOB_MPPROOB_FK 
 FOREIGN KEY (NR_SEQ_OBJ_ORIGEM) 
 REFERENCES TASY.MP_PROCESSO_OBJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPPROOB_MPPROOB_FK1 
 FOREIGN KEY (NR_SEQ_OBJ_DESTINO) 
 REFERENCES TASY.MP_PROCESSO_OBJETO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPPROOB_MPPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.MP_PROCESSO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MP_PROCESSO_OBJETO TO NIVEL_1;

