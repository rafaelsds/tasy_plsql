DROP TABLE TASY.TESTE_RELATORIO_WEB_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.TESTE_RELATORIO_WEB_HIST
(
  CD_RELATORIO         NUMBER(10)               NOT NULL,
  CD_CLASSIF_RELAT     VARCHAR2(4 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.TESTE_RELATORIO_WEB_HIST TO NIVEL_1;

