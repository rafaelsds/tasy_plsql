ALTER TABLE TASY.NOTA_FISCAL_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_TRIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_TRIBUTO            NUMBER(3)               NOT NULL,
  VL_TRIBUTO            NUMBER(13,2)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  VL_BASE_CALCULO       NUMBER(13,2),
  TX_TRIBUTO            NUMBER(15,4),
  VL_REDUCAO_BASE       NUMBER(13,2),
  VL_TRIB_NAO_RETIDO    NUMBER(15,2)            NOT NULL,
  VL_BASE_NAO_RETIDO    NUMBER(15,2)            NOT NULL,
  VL_TRIB_ADIC          NUMBER(15,2)            NOT NULL,
  VL_BASE_ADIC          NUMBER(15,2)            NOT NULL,
  VL_REDUCAO            NUMBER(15,2),
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_DARF               VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  CD_CONTA_FINANC       NUMBER(10),
  CD_VARIACAO           VARCHAR2(2 BYTE),
  IE_PERIODICIDADE      VARCHAR2(1 BYTE),
  IE_ORIGEM_TRIB        VARCHAR2(2 BYTE),
  IE_PAGO_PREV          VARCHAR2(1 BYTE),
  NR_SEQ_INTERNO        NUMBER(10)              NOT NULL,
  VL_BC_MATRIZ_FILIAL   NUMBER(15,2),
  DT_CONFERENCIA        DATE,
  NM_USUARIO_CONF       VARCHAR2(15 BYTE),
  IE_RETENCAO           VARCHAR2(1 BYTE),
  IE_EXPORTA_XML        VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_TRIB     NUMBER(10),
  VL_RET_SUBCONTRATADO  NUMBER(13,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTFITR_CONFINA_FK_I ON TASY.NOTA_FISCAL_TRIB
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFITR_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFITR_I1 ON TASY.NOTA_FISCAL_TRIB
(NR_SEQUENCIA, CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFITR_NOTFISC_FK_I ON TASY.NOTA_FISCAL_TRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NOTFITR_PK ON TASY.NOTA_FISCAL_TRIB
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFITR_TRIBUTO_FK_I ON TASY.NOTA_FISCAL_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_trib_update
after update ON nota_fiscal_trib
FOR EACH ROW
declare

begin

if	(:new.VL_TRIBUTO <> :old.VL_TRIBUTO) or
	(:new.VL_BASE_CALCULO <> :old.VL_BASE_CALCULO) or
	(:new.TX_TRIBUTO <> :old.TX_TRIBUTO) or
	(:new.VL_REDUCAO_BASE <> :old.VL_REDUCAO_BASE) or
	(:new.VL_TRIB_NAO_RETIDO <> :old.VL_TRIB_NAO_RETIDO) or
	(:new.VL_BASE_NAO_RETIDO <> :old.VL_BASE_NAO_RETIDO) or
	(:new.VL_TRIB_ADIC <> :old.VL_TRIB_ADIC) or
	(:new.VL_BASE_ADIC <> :old.VL_BASE_ADIC) or
	(:new.VL_REDUCAO <> :old.VL_REDUCAO) then

	insert into log_tasy
			(DT_ATUALIZACAO,
			NM_USUARIO,
			CD_LOG,
			DS_LOG)
	values		(sysdate,
			 'SQLPLUS',
			 55712,
			'NF: ' || :new.nr_sequencia || chr(13) ||
			'Tributo: ' || :new.CD_TRIBUTO || chr(13) ||
			':new.VL_TRIBUTO = ' || 	:new.VL_TRIBUTO 	|| ' :old.VL_TRIBUTO = ' 	|| :old.VL_TRIBUTO || chr(13) ||
			':new.VL_BASE_CALCULO = ' || 	:new.VL_BASE_CALCULO 	|| ' :old.VL_BASE_CALCULO = ' 	|| :old.VL_BASE_CALCULO || chr(13) ||
			':new.TX_TRIBUTO = ' || 	:new.TX_TRIBUTO		|| ' :old.TX_TRIBUTO = ' 	|| :old.TX_TRIBUTO || chr(13) ||
			':new.VL_REDUCAO_BASE = ' || 	:new.VL_REDUCAO_BASE 	|| ' :old.VL_REDUCAO_BASE = ' 	|| :old.VL_REDUCAO_BASE || chr(13) ||
			':new.VL_TRIB_NAO_RETIDO = ' || :new.VL_TRIB_NAO_RETIDO	|| ' :old.VL_TRIB_NAO_RETIDO = '|| :old.VL_TRIB_NAO_RETIDO || chr(13) ||
			':new.VL_BASE_NAO_RETIDO = ' || :new.VL_BASE_NAO_RETIDO	|| ' :old.VL_BASE_NAO_RETIDO = '|| :old.VL_BASE_NAO_RETIDO || chr(13) ||
			':new.VL_TRIB_ADIC = ' || 	:new.VL_TRIB_ADIC	|| ' :old.VL_TRIB_ADIC = ' 	|| :old.VL_TRIB_ADIC || chr(13) ||
			':new.VL_BASE_ADIC = ' ||	:new.VL_BASE_ADIC	|| ' :old.VL_BASE_ADIC = ' 	|| :old.VL_BASE_ADIC || chr(13) ||
			':new.VL_REDUCAO = ' ||		:new.VL_REDUCAO		|| ' :old.VL_REDUCAO = ' 	|| :old.VL_REDUCAO);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_trib_insert
before insert or update ON TASY.NOTA_FISCAL_TRIB for each row
begin

if	(:new.vl_tributo < 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(282343,'VL_TRIBUTO= ' || :new.vl_tributo);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_trib_update2
before insert or update ON TASY.NOTA_FISCAL_TRIB FOR EACH ROW
declare

vl_base_calculo_w		number(15,4);
ie_forma_arred_nf_w	varchar2(15);
ie_tipo_nota_w		varchar2(15);

begin

select	nvl(max(ie_forma_arred_nf), 'N')
into	ie_forma_arred_nf_w
from	tributo
where	cd_tributo	= :new.cd_tributo;

select	nvl(max(obter_se_nota_entrada_saida(:new.nr_sequencia)),'E')
into	ie_tipo_nota_w
from	dual;

if	(ie_forma_arred_nf_w = 'S') then
	begin

	vl_base_calculo_w	:= nvl(:new.vl_base_calculo, 0) - nvl(:new.vl_reducao_base, 0);

	if	(vl_base_calculo_w is not null) and (:new.tx_tributo is not null) then

		if	(:new.vl_tributo <> 0) then /* RFOLIVEIRA - Inclui este IF pois existem vezes em que n�o � retido o valor (acumulado) e esta trigger acaba gerando mesmo assim*/

			begin
			:new.vl_tributo	:= trunc(dividir(vl_base_calculo_w * :new.tx_tributo, 100),2);
			exception when others then
				vl_base_calculo_w := vl_base_calculo_w;
				/*insert into log__tasy(
					DT_ATUALIZACAO,
					NM_USUARIO,
					CD_LOG,
					DS_LOG)
				values(	sysdate,
					'Trigger',
					1544,
					'Erro ao tentar ajustar o valor do tributo na nota' || chr(13) ||
					'NF: ' || :new.nr_sequencia || chr(13) ||
					'Tributo: ' || :new.CD_TRIBUTO);*/
			end;

		end if;
	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_trib_delete
before delete ON TASY.NOTA_FISCAL_TRIB FOR EACH ROW
declare
/*
PRAGMA AUTONOMOUS_TRANSACTION;
ds_benefic_w	varchar2(80);
dt_emissao_w	date;
vl_total_nota_w	number(15,2);
ds_tributo_w	varchar2(40);
*/

begin


null;
/*
select	substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc),1,80),
	dt_emissao,
	vl_total_nota
into	ds_benefic_w,
	dt_emissao_w,
	vl_total_nota_w
from	nota_fiscal
where	nr_sequencia	= :old.nr_sequencia;

select	ds_tributo
into	ds_tributo_w
from	tributo
where	cd_tributo = :old.cd_tributo;
*/

/*insert	into log__tasy
	(dt_atualizacao,
	nm_usuario,
	cd_log,
	ds_log)
values	(sysdate,
	'Tasy',
	5650,
	'Pessoa nota: ' || ds_benefic_w || chr(13) ||
	'Emiss�o: ' || dt_emissao_w || chr(13) ||
	'Dt cria��o tributo: ' || to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
	'Valor total nota: ' || vl_total_nota_w || chr(13) ||
	'Tributo: ' || ds_tributo_w || chr(13) ||
	'Valor tributo ' || :old.vl_tributo);
*/
-- commit;
END;
/


ALTER TABLE TASY.NOTA_FISCAL_TRIB ADD (
  CONSTRAINT NOTFITR_PK
 PRIMARY KEY
 (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_TRIB ADD (
  CONSTRAINT NOTFITR_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT NOTFITR_NOTFISC_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NOTFITR_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.NOTA_FISCAL_TRIB TO NIVEL_1;

