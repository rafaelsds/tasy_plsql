ALTER TABLE TASY.DB_DOCUMENT_REVIEWER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DB_DOCUMENT_REVIEWER CASCADE CONSTRAINTS;

CREATE TABLE TASY.DB_DOCUMENT_REVIEWER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_REVIEWER_TYPE     VARCHAR2(3 BYTE)         NOT NULL,
  NM_REVIEWER_USER     VARCHAR2(20 BYTE)        NOT NULL,
  NR_SEQ_DOCUMENT      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DBDOCREV_DBDOCMT_FK_I ON TASY.DB_DOCUMENT_REVIEWER
(NR_SEQ_DOCUMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DBDOCREV_PK ON TASY.DB_DOCUMENT_REVIEWER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DB_DOCUMENT_REVIEWER ADD (
  CONSTRAINT DBDOCREV_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DB_DOCUMENT_REVIEWER ADD (
  CONSTRAINT DBDOCREV_DBDOCMT_FK 
 FOREIGN KEY (NR_SEQ_DOCUMENT) 
 REFERENCES TASY.DB_DOCUMENT (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DB_DOCUMENT_REVIEWER TO NIVEL_1;

