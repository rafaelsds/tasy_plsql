ALTER TABLE TASY.ESTRATEGIA_CHAMADA_SENHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESTRATEGIA_CHAMADA_SENHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESTRATEGIA_CHAMADA_SENHA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_ESTRATEGIA        VARCHAR2(30 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_ESTRATEGIA        VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ESCHASE_PK ON TASY.ESTRATEGIA_CHAMADA_SENHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.estrat_chamada_senha_bfrdelete
before delete ON TASY.ESTRATEGIA_CHAMADA_SENHA FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;

BEGIN
begin

	delete
	from	chamada_inter_senha_item
	where 	nr_seq_chamada_inter in (	select	nr_sequencia
						from	chamada_intercalada_senha
						where 	nr_seq_estrategia_chamada = :old.nr_sequencia);

	delete
	from	chamada_intercalada_senha
	where 	nr_seq_estrategia_chamada = :old.nr_sequencia;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


ALTER TABLE TASY.ESTRATEGIA_CHAMADA_SENHA ADD (
  CONSTRAINT ESCHASE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.ESTRATEGIA_CHAMADA_SENHA TO NIVEL_1;

