ALTER TABLE TASY.PLS_MAT_UNI_FED_SC_PRECO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MAT_UNI_FED_SC_PRECO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MAT_UNI_FED_SC_PRECO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_MAT_UNIMED     NUMBER(10)              NOT NULL,
  DT_PRECO              DATE,
  VL_PRECO              NUMBER(15,2)            NOT NULL,
  CD_CGC_FORNCEDOR      VARCHAR2(14 BYTE),
  CD_MOEDA              NUMBER(3),
  CD_FORNECEDOR_FED_SC  NUMBER(10),
  IE_SITUACAO           VARCHAR2(1 BYTE),
  IE_INCONSISTENTE      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMUFP_I1 ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(CD_FORNECEDOR_FED_SC, NR_SEQ_MAT_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFP_I1
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUFP_I2 ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(NR_SEQ_MAT_UNIMED, DT_PRECO, NVL("CD_FORNECEDOR_FED_SC",1))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMUFP_I3 ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(NR_SEQ_MAT_UNIMED, DT_PRECO, CD_FORNECEDOR_FED_SC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMUFP_MOEDA_FK_I ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFP_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUFP_PESJURI_FK_I ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(CD_CGC_FORNCEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFP_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSMUFP_PK ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSMUFP_PLSMUFS_FK_I ON TASY.PLS_MAT_UNI_FED_SC_PRECO
(NR_SEQ_MAT_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMUFP_PLSMUFS_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_MAT_UNI_FED_SC_PRECO ADD (
  CONSTRAINT PLSMUFP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MAT_UNI_FED_SC_PRECO ADD (
  CONSTRAINT PLSMUFP_PLSMUFS_FK 
 FOREIGN KEY (NR_SEQ_MAT_UNIMED) 
 REFERENCES TASY.PLS_MAT_UNIMED_FED_SC (NR_SEQUENCIA),
  CONSTRAINT PLSMUFP_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNCEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSMUFP_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.PLS_MAT_UNI_FED_SC_PRECO TO NIVEL_1;

