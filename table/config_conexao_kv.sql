ALTER TABLE TASY.CONFIG_CONEXAO_KV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONFIG_CONEXAO_KV CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONFIG_CONEXAO_KV
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  ENDERECO_HOST       VARCHAR2(50 BYTE),
  PORTA_HOST          NUMBER(4),
  EMAIL_REMETENTE     VARCHAR2(50 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  DS_HOST_POP3        VARCHAR2(150 BYTE),
  DS_HOST_SMTP        VARCHAR2(150 BYTE),
  NR_PORTA_POP3       NUMBER(10),
  NR_PORTA_SMTP       NUMBER(10),
  DS_SENHA            VARCHAR2(255 BYTE),
  DS_LOGIN            VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONXKV_ESTABEL_FK_I ON TASY.CONFIG_CONEXAO_KV
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONXKV_PK ON TASY.CONFIG_CONEXAO_KV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONFIG_CONEXAO_KV ADD (
  CONSTRAINT CONXKV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONFIG_CONEXAO_KV ADD (
  CONSTRAINT CONXKV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CONFIG_CONEXAO_KV TO NIVEL_1;

