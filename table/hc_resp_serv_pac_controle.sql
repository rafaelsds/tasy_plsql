ALTER TABLE TASY.HC_RESP_SERV_PAC_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HC_RESP_SERV_PAC_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.HC_RESP_SERV_PAC_CONTROLE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_RESP_SERV_PAC  NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HCRESPC_HCRSPAC_FK_I ON TASY.HC_RESP_SERV_PAC_CONTROLE
(NR_SEQ_RESP_SERV_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HCRESPC_PK ON TASY.HC_RESP_SERV_PAC_CONTROLE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HC_RESP_SERV_PAC_CONTROLE ADD (
  CONSTRAINT HCRESPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HC_RESP_SERV_PAC_CONTROLE ADD (
  CONSTRAINT HCRESPC_HCRSPAC_FK 
 FOREIGN KEY (NR_SEQ_RESP_SERV_PAC) 
 REFERENCES TASY.HC_RESP_SERVICO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HC_RESP_SERV_PAC_CONTROLE TO NIVEL_1;

