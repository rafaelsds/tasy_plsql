ALTER TABLE TASY.PLS_COMUNIC_EXT_HIST_WEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMUNIC_EXT_HIST_WEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMUNIC_EXT_HIST_WEB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_COMUNICADO      NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_USUARIO_LEITURA     VARCHAR2(30 BYTE),
  DT_LEITURA             DATE,
  DS_EMAIL               VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA_RESP  VARCHAR2(10 BYTE),
  IE_TIPO_ACESSO         VARCHAR2(5 BYTE),
  DS_LOGIN               VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCEHW_PESFISI_FK_I ON TASY.PLS_COMUNIC_EXT_HIST_WEB
(CD_PESSOA_FISICA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCEHW_PK ON TASY.PLS_COMUNIC_EXT_HIST_WEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCEHW_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCEHW_PLSCEXW_FK_I ON TASY.PLS_COMUNIC_EXT_HIST_WEB
(NR_SEQ_COMUNICADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCEHW_PLSCEXW_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_COMUNIC_EXT_HIST_WEB_tp  after update ON TASY.PLS_COMUNIC_EXT_HIST_WEB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_EMAIL,1,500);gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PLS_COMUNIC_EXT_HIST_WEB',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA_RESP,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA_RESP,1,4000),substr(:new.CD_PESSOA_FISICA_RESP,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA_RESP',ie_log_w,ds_w,'PLS_COMUNIC_EXT_HIST_WEB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.SSJ_LEITURA_COMUNIC_PREST_WEB
AFTER INSERT
ON TASY.PLS_COMUNIC_EXT_HIST_WEB 

FOR EACH ROW
DECLARE

nm_usuario_w varchar2(255);
nr_seq_usuario_web_w number(10);
qt_registros_w number(10);

BEGIN
   
    if(:new.ie_tipo_acesso = 'P')then
        
        nm_usuario_w:= upper(:new.nm_usuario_leitura);
    
        select nr_seq_usuario
        into nr_seq_usuario_web_w
        from pls_prestador_usuario_web a
        join pls_usuario_web b on(a.nr_seq_usuario = b.nr_sequencia)
        where upper(NM_USUARIO_WEB) = upper(nm_usuario_w);
        
        select count(*)
        into qt_registros_w
        from w_pls_alerta_menu_web
        where nr_seq_usu_prest_web = nr_seq_usuario_web_w
        and ie_comunicado_externo = 'S'; 
    
        if(qt_registros_w > 0)then
            
            update w_pls_alerta_menu_web set ie_comunicado_externo = 'N'
            where nr_seq_usu_prest_web = nr_seq_usuario_web_w
            and ie_comunicado_externo = 'S'; 
            
        end if;
        
    end if;


       
END SSJ_LEITURA_COMUNIC_PREST_WEB;
/


ALTER TABLE TASY.PLS_COMUNIC_EXT_HIST_WEB ADD (
  CONSTRAINT PLSCEHW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMUNIC_EXT_HIST_WEB ADD (
  CONSTRAINT PLSCEHW_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSCEHW_PLSCEXW_FK 
 FOREIGN KEY (NR_SEQ_COMUNICADO) 
 REFERENCES TASY.PLS_COMUNIC_EXTERNA_WEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COMUNIC_EXT_HIST_WEB TO NIVEL_1;

