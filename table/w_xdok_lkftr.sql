ALTER TABLE TASY.W_XDOK_LKFTR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_XDOK_LKFTR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_XDOK_LKFTR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_IMPORT        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  TRAGERNUMMER         VARCHAR2(4 BYTE),
  KURZBEZEICHNUNG      VARCHAR2(30 BYTE),
  LANGBEZEICHNUG       VARCHAR2(150 BYTE),
  STRASSE              VARCHAR2(50 BYTE),
  PLZ                  VARCHAR2(6 BYTE),
  ORT                  VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WXLFKT_I1 ON TASY.W_XDOK_LKFTR
(TRAGERNUMMER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WXLFKT_PK ON TASY.W_XDOK_LKFTR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WXLFKT_WXIMPT_FK_I ON TASY.W_XDOK_LKFTR
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_XDOK_LKFTR ADD (
  CONSTRAINT WXLFKT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

