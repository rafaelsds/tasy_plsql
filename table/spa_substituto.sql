ALTER TABLE TASY.SPA_SUBSTITUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_SUBSTITUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_SUBSTITUTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_CARGO              NUMBER(10),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PESSOA_SUBSTITUTA  VARCHAR2(10 BYTE)       NOT NULL,
  DT_LIMITE             DATE                    NOT NULL,
  NR_SEQ_TIPO           NUMBER(10),
  NR_SEQ_MOTIVO         NUMBER(10),
  CD_ESTABELECIMENTO    NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SPASUBS_CARGO_FK_I ON TASY.SPA_SUBSTITUTO
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPASUBS_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SPASUBS_ESTABEL_FK_I ON TASY.SPA_SUBSTITUTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPASUBS_PESFISI_FK_I ON TASY.SPA_SUBSTITUTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPASUBS_PESFISI_FK1_I ON TASY.SPA_SUBSTITUTO
(CD_PESSOA_SUBSTITUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SPASUBS_PK ON TASY.SPA_SUBSTITUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPASUBS_PK
  MONITORING USAGE;


CREATE INDEX TASY.SPASUBS_SPASUBSMOT_FK_I ON TASY.SPA_SUBSTITUTO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SPASUBS_SPASUBSTIPO_FK_I ON TASY.SPA_SUBSTITUTO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SPA_SUBSTITUTO_tp  after update ON TASY.SPA_SUBSTITUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'SPA_SUBSTITUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIMITE,1,500);gravar_log_alteracao(substr(:old.DT_LIMITE,1,4000),substr(:new.DT_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIMITE',ie_log_w,ds_w,'SPA_SUBSTITUTO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_SUBSTITUTA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_SUBSTITUTA,1,4000),substr(:new.CD_PESSOA_SUBSTITUTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_SUBSTITUTA',ie_log_w,ds_w,'SPA_SUBSTITUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'SPA_SUBSTITUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.spa_substituto_afterdelete
after delete ON TASY.SPA_SUBSTITUTO for each row
declare

ds_log_w	varchar2(4000);

begin
/*ds_log_w 	:= substr('Exclu�do substituto abaixo.' || chr(13) || chr(10) || chr(13) || chr(10) ||
			'PESSOA F�SICA: ' || :old.cd_pessoa_fisica || ' - ' || obter_nome_pf(:old.cd_pessoa_fisica) || chr(13) || chr(10) ||
			'CARGO: ' || :old.cd_cargo || ' - ' || obter_desc_cargo(:old.cd_cargo) || chr(13) || chr(10) ||
			'SUBSTITUTO: ' || :old.cd_pessoa_substituta || ' - ' || obter_nome_pf(:old.cd_pessoa_substituta) || chr(13) || chr(10) ||
			'DT. LIMITE: ' || :old.dt_limite,1,4000);*/
ds_log_w 	:= substr(wheb_mensagem_pck.get_texto(313837) || chr(13) || chr(10) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313838, 'CD_PESSOA_FISICA=' || :old.cd_pessoa_fisica || ';' ||
			    				      'NM_PESSOA_FISICA=' || obter_nome_pf(:old.cd_pessoa_fisica)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313839, 'CD_CARGO=' || :old.cd_cargo || ';' ||
							      'DS_CARGO=' || obter_desc_cargo(:old.cd_cargo)) || chr(13) || chr(10) ||
			 wheb_mensagem_pck.get_texto(802631, 'NR_SEQ_TIPO=' || :old.nr_seq_tipo || ';' ||
							      'DS_TIPO_SPA=' || spa_obter_desc_tipo(:old.nr_seq_tipo)) || chr(13) || chr(10) ||
			 wheb_mensagem_pck.get_texto(802632, 'NR_SEQ_MOTIVO=' || :old.nr_seq_motivo || ';' ||
							      'DS_MOTIVO_SPA=' || spa_obter_desc_motivo(:old.nr_seq_motivo)) || chr(13) || chr(10) ||
			 wheb_mensagem_pck.get_texto(802630, 'CD_ESTABELECIMENTO=' || :old.cd_estabelecimento || ';' ||
							      'DS_ESTABELECIMENTO=' || obter_nome_estabelecimento(:old.cd_estabelecimento)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313840, 'CD_PESSOA_SUBSTITUTA=' || :old.cd_pessoa_substituta || ';' ||
							      'NM_PESSOA_SUBSTITUTA=' || obter_nome_pf(:old.cd_pessoa_substituta)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313841, 'DT_LIMITE=' || :old.dt_limite),1,4000);

insert into spa_substituto_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log)
values(	spa_substituto_log_seq.nextval,
	sysdate,
	:old.nm_usuario,
	sysdate,
	:old.nm_usuario,
	--'Exclus�o substituto',
	wheb_mensagem_pck.get_texto(313836),
	ds_log_w);

end;
/


CREATE OR REPLACE TRIGGER TASY.spa_substituto_afterinsert
after insert ON TASY.SPA_SUBSTITUTO for each row
declare

ds_log_w	varchar2(4000);

begin
/*ds_log_w 	:= substr('Inclu�do substituto abaixo.' || chr(13) || chr(10) || chr(13) || chr(10) ||
			'PESSOA F�SICA: ' || :new.cd_pessoa_fisica || ' - ' || obter_nome_pf(:new.cd_pessoa_fisica) || chr(13) || chr(10) ||
			'CARGO: ' || :new.cd_cargo || ' - ' || obter_desc_cargo(:new.cd_cargo) || chr(13) || chr(10) ||
			'SUBSTITUTO: ' || :new.cd_pessoa_substituta || ' - ' || obter_nome_pf(:new.cd_pessoa_substituta) || chr(13) || chr(10) ||
			'DT. LIMITE: ' || :new.dt_limite,1,4000);*/
ds_log_w 	:= substr(wheb_mensagem_pck.get_texto(313844) || chr(13) || chr(10) || chr(13) || chr(10) ||
			wheb_mensagem_pck.get_texto(313838, 'CD_PESSOA_FISICA=' || :new.cd_pessoa_fisica || ';' ||
			    				      'NM_PESSOA_FISICA=' || obter_nome_pf(:new.cd_pessoa_fisica)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313839, 'CD_CARGO=' || :new.cd_cargo || ';' ||
							      'DS_CARGO=' || obter_desc_cargo(:new.cd_cargo)) || chr(13) || chr(10) ||
			 wheb_mensagem_pck.get_texto(802631, 'NR_SEQ_TIPO=' || :new.nr_seq_tipo || ';' ||
							      'DS_TIPO_SPA=' || spa_obter_desc_tipo(:new.nr_seq_tipo)) || chr(13) || chr(10) ||
			 wheb_mensagem_pck.get_texto(802632, 'NR_SEQ_MOTIVO=' || :new.nr_seq_motivo || ';' ||
							      'DS_MOTIVO_SPA=' || spa_obter_desc_motivo(:new.nr_seq_motivo)) || chr(13) || chr(10) ||
			 wheb_mensagem_pck.get_texto(802630, 'CD_ESTABELECIMENTO=' || :new.cd_estabelecimento || ';' ||
							      'DS_ESTABELECIMENTO=' || obter_nome_estabelecimento(:new.cd_estabelecimento)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313840, 'CD_PESSOA_SUBSTITUTA=' || :new.cd_pessoa_substituta || ';' ||
							      'NM_PESSOA_SUBSTITUTA=' || obter_nome_pf(:new.cd_pessoa_substituta)) || chr(13) || chr(10) ||
			  wheb_mensagem_pck.get_texto(313841, 'DT_LIMITE=' || :new.dt_limite),1,4000);

insert into spa_substituto_log(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_titulo,
	ds_log)
values(	spa_substituto_log_seq.nextval,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	--'Inclus�o substituto',
	wheb_mensagem_pck.get_texto(313843),
	ds_log_w);

end;
/


ALTER TABLE TASY.SPA_SUBSTITUTO ADD (
  CONSTRAINT SPASUBS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SPA_SUBSTITUTO ADD (
  CONSTRAINT SPASUBS_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT SPASUBS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT SPASUBS_PESFISI_FK1 
 FOREIGN KEY (CD_PESSOA_SUBSTITUTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SPASUBS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SPASUBS_SPASUBSMOT_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.SPA_MOTIVO (NR_SEQUENCIA),
  CONSTRAINT SPASUBS_SPASUBSTIPO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.SPA_TIPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SPA_SUBSTITUTO TO NIVEL_1;

