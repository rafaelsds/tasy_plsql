DROP TABLE TASY.REL_ESTAT_CIRURGIA_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.REL_ESTAT_CIRURGIA_W
(
  DS_QUEBRA                   VARCHAR2(255 BYTE),
  CD_QUEBRA                   VARCHAR2(50 BYTE),
  NM_USUARIO                  VARCHAR2(15 BYTE),
  QT_JAN                      NUMBER(3),
  QT_FEV                      NUMBER(3),
  QT_MAR                      NUMBER(3),
  QT_ABR                      NUMBER(3),
  QT_MAI                      NUMBER(3),
  QT_JUN                      NUMBER(3),
  QT_JUL                      NUMBER(3),
  QT_AGO                      NUMBER(3),
  QT_SET                      NUMBER(3),
  QT_OUT                      NUMBER(3),
  QT_NOV                      NUMBER(3),
  QT_DEZ                      NUMBER(3),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_CONVENIO                 NUMBER(10),
  CD_ESPECIALIDADE_CIRURGIAO  VARCHAR2(40 BYTE),
  CD_TIPO_ANESTESIA           VARCHAR2(10 BYTE),
  IE_TIPO_ATENDIMENTO         NUMBER(3),
  CD_MEDICO_CIRURGIAO         VARCHAR2(10 BYTE),
  CD_MEDICO_ANESTESISTA       VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO_PRINC       NUMBER(15),
  NR_SEQ_TIPO_ACIDENTE        NUMBER(10),
  IE_ASA_ESTADO_PACIENTE      VARCHAR2(3 BYTE),
  CD_ESPECIALIDADE_PROC       NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.REL_ESTAT_CIRURGIA_W TO NIVEL_1;

