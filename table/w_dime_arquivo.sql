ALTER TABLE TASY.W_DIME_ARQUIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIME_ARQUIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIME_ARQUIVO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONTROLE_DIME  NUMBER(10),
  CD_REGISTRO           VARCHAR2(2 BYTE),
  NR_LINHA              NUMBER(10),
  DS_ARQUIVO            VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WDIMEARQ_FISDIMECTR_FK_I ON TASY.W_DIME_ARQUIVO
(NR_SEQ_CONTROLE_DIME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIMEARQ_FISDIMECTR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WDIMEARQ_PK ON TASY.W_DIME_ARQUIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIMEARQ_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_DIME_ARQUIVO ADD (
  CONSTRAINT WDIMEARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_DIME_ARQUIVO ADD (
  CONSTRAINT WDIMEARQ_FISDIMECTR_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE_DIME) 
 REFERENCES TASY.FIS_DIME_CONTROLE (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_DIME_ARQUIVO TO NIVEL_1;

