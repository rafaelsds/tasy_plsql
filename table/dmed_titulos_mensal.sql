ALTER TABLE TASY.DMED_TITULOS_MENSAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DMED_TITULOS_MENSAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.DMED_TITULOS_MENSAL
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_DMED_MENSAL      NUMBER(10),
  NR_DOCUMENTO            VARCHAR2(50 BYTE),
  IE_TIPO_DOCUMENTO       VARCHAR2(10 BYTE),
  CD_PESSOA_TITULAR       VARCHAR2(15 BYTE),
  CD_PESSOA_BENEFICIARIO  VARCHAR2(15 BYTE),
  VL_PAGO                 NUMBER(15,2),
  DT_LIQUIDACAO           DATE,
  IE_PRESTADORA_OPS       VARCHAR2(10 BYTE),
  NR_ATENDIMENTO          NUMBER(10),
  CD_PF_PRESTADOR         VARCHAR2(10 BYTE),
  CD_CGC                  VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DMEDTITM_ATEPACI_FK_I ON TASY.DMED_TITULOS_MENSAL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DMEDTITM_DMEDMENS_FK_I ON TASY.DMED_TITULOS_MENSAL
(NR_SEQ_DMED_MENSAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DMEDTITM_PESFISI_FK_I ON TASY.DMED_TITULOS_MENSAL
(CD_PESSOA_BENEFICIARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DMEDTITM_PESFISI_FK1_I ON TASY.DMED_TITULOS_MENSAL
(CD_PESSOA_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DMEDTITM_PESFISI2_FK_I ON TASY.DMED_TITULOS_MENSAL
(CD_PF_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DMEDTITM_PESJURI_FK_I ON TASY.DMED_TITULOS_MENSAL
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DMEDTITM_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DMEDTITM_PK ON TASY.DMED_TITULOS_MENSAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DMEDTITM_PK
  MONITORING USAGE;


ALTER TABLE TASY.DMED_TITULOS_MENSAL ADD (
  CONSTRAINT DMEDTITM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DMED_TITULOS_MENSAL ADD (
  CONSTRAINT DMEDTITM_DMEDMENS_FK 
 FOREIGN KEY (NR_SEQ_DMED_MENSAL) 
 REFERENCES TASY.DMED_MENSAL (NR_SEQUENCIA),
  CONSTRAINT DMEDTITM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_BENEFICIARIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DMEDTITM_PESFISI_FK1 
 FOREIGN KEY (CD_PESSOA_TITULAR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DMEDTITM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT DMEDTITM_PESFISI2_FK 
 FOREIGN KEY (CD_PF_PRESTADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DMEDTITM_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.DMED_TITULOS_MENSAL TO NIVEL_1;

