ALTER TABLE TASY.PROJ_AVALIACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_AVALIACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_AVALIACAO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_EMPRESA                NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_TIPO_AVALIACAO         VARCHAR2(3 BYTE),
  NR_SEQ_APRES              NUMBER(15)          NOT NULL,
  DS_QUESITO                VARCHAR2(255 BYTE)  NOT NULL,
  IE_TIPO_QUESITO           VARCHAR2(1 BYTE)    NOT NULL,
  QT_PESO                   NUMBER(15)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_MODELO             NUMBER(10)          NOT NULL,
  IE_NAO_CONSIDERAR_SATISF  VARCHAR2(1 BYTE),
  CD_EXP_QUESITO            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROAVAL_DICEXPR_FK_I ON TASY.PROJ_AVALIACAO
(CD_EXP_QUESITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAVAL_EMPRESA_FK_I ON TASY.PROJ_AVALIACAO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAVAL_EMPRESA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROAVAL_PK ON TASY.PROJ_AVALIACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAVAL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROAVAL_PROMOAV_FK_I ON TASY.PROJ_AVALIACAO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAVAL_PROMOAV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_AVALIACAO ADD (
  CONSTRAINT PROAVAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_AVALIACAO ADD (
  CONSTRAINT PROAVAL_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_QUESITO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT PROAVAL_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PROAVAL_PROMOAV_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.PROJ_MODELO_AVALIACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_AVALIACAO TO NIVEL_1;

