ALTER TABLE TASY.PLS_FRANQUIA_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FRANQUIA_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FRANQUIA_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  NR_SEQ_FRANQUIA_EST  NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PLANO         NUMBER(10),
  DT_FIM_VIGENCIA      DATE,
  VL_FRANQUIA          NUMBER(15,2),
  TX_FRANQUIA          NUMBER(7,4),
  VL_MAXIMO            NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSFRRE_PK ON TASY.PLS_FRANQUIA_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFRRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSFRRE_PLSFRES_FK_I ON TASY.PLS_FRANQUIA_REGRA
(NR_SEQ_FRANQUIA_EST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFRRE_PLSFRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFRRE_PLSPLAN_FK_I ON TASY.PLS_FRANQUIA_REGRA
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFRRE_PLSPLAN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_FRANQUIA_REGRA ADD (
  CONSTRAINT PLSFRRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FRANQUIA_REGRA ADD (
  CONSTRAINT PLSFRRE_PLSFRES_FK 
 FOREIGN KEY (NR_SEQ_FRANQUIA_EST) 
 REFERENCES TASY.PLS_FRANQUIA_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSFRRE_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FRANQUIA_REGRA TO NIVEL_1;

