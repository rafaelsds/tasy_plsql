ALTER TABLE TASY.ATEND_PACIENTE_UNIDADE_INF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PACIENTE_UNIDADE_INF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PACIENTE_UNIDADE_INF
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_ATEND_PAC_UNIDADE  NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_PROF_MEDICO            VARCHAR2(10 BYTE),
  NR_RQE                    VARCHAR2(20 BYTE),
  CD_PROF_ENFERMEIRA        VARCHAR2(10 BYTE),
  NR_ASV_TEAM               NUMBER(9)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPACUF_ATEPACU_FK_I ON TASY.ATEND_PACIENTE_UNIDADE_INF
(NR_SEQ_ATEND_PAC_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPACUF_PESFISI_FK_I ON TASY.ATEND_PACIENTE_UNIDADE_INF
(CD_PROF_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATPACUF_PESFISI_FK1_I ON TASY.ATEND_PACIENTE_UNIDADE_INF
(CD_PROF_ENFERMEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATPACUF_PK ON TASY.ATEND_PACIENTE_UNIDADE_INF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_PACIENTE_UNIDADE_INF_tp  after update ON TASY.ATEND_PACIENTE_UNIDADE_INF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_RQE,1,4000),substr(:new.NR_RQE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RQE',ie_log_w,ds_w,'ATEND_PACIENTE_UNIDADE_INF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PACIENTE_UND_INF_ATUAL
after insert or update ON TASY.ATEND_PACIENTE_UNIDADE_INF for each row
declare

CD_MEDICO_ATENDIMENTO_W		ATEND_PACIENTE_UNIDADE_INF.CD_PROF_MEDICO%type;
NR_SEQ_ATEND_PAC_UNIDADE_W		ATEND_PACIENTE_UNIDADE_INF.NR_SEQ_ATEND_PAC_UNIDADE%type;

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
    		NR_SEQ_ATEND_PAC_UNIDADE_W	:=	nvl(:new.NR_SEQ_ATEND_PAC_UNIDADE, :old.NR_SEQ_ATEND_PAC_UNIDADE);
    		CD_MEDICO_ATENDIMENTO_W		:=	nvl(:new.CD_PROF_MEDICO, :old.CD_PROF_MEDICO);

		if 	(CD_MEDICO_ATENDIMENTO_W is not null)	then
			update 	ATENDIMENTO_PACIENTE ap
			set	ap.CD_MEDICO_RESP = CD_MEDICO_ATENDIMENTO_W
			where	ap.NR_ATENDIMENTO = (select   	apu.NR_ATENDIMENTO
							from      	ATEND_PACIENTE_UNIDADE apu
							where     	apu.NR_SEQ_INTERNO = NR_SEQ_ATEND_PAC_UNIDADE_W);
		end if;
	end if;
end ATEND_PACIENTE_UND_INF_ATUAL;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PAC_UND_INF_BEFORE_DEL
before delete ON TASY.ATEND_PACIENTE_UNIDADE_INF for each row
declare

CD_MEDICO_ATENDIMENTO_W		ATEND_PACIENTE_UNIDADE_INF.CD_PROF_MEDICO%type;
NR_SEQ_ATEND_PAC_UNIDADE_W	ATEND_PACIENTE_UNIDADE_INF.NR_SEQ_ATEND_PAC_UNIDADE%type;
vl_default_w				tabela_atrib_regra.vl_default%type;

pragma autonomous_transaction;

begin

	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then

		vl_default_w := null;

      select	max(vl_default)
    into	vl_default_w
    from	tabela_atrib_regra
    where	nm_tabela	= 'ATENDIMENTO_PACIENTE'
    and		nm_atributo		= 'CD_MEDICO_RESP'
    and		nvl(cd_estabelecimento, obter_estabelecimento_ativo)	= obter_estabelecimento_ativo
    and		nvl(cd_perfil, obter_perfil_ativo)		= obter_perfil_ativo
    and		nvl(nm_usuario_param, wheb_usuario_pck.get_nm_usuario) 		= wheb_usuario_pck.get_nm_usuario;

    if (vl_default_w is null) then
      select	max(cd_pessoa_fisica)
      into	vl_default_w
      from    usuario
      where  	nm_usuario = wheb_usuario_pck.get_nm_usuario;
    end if;

    	NR_SEQ_ATEND_PAC_UNIDADE_W	:=	nvl(:new.NR_SEQ_ATEND_PAC_UNIDADE, :old.NR_SEQ_ATEND_PAC_UNIDADE);
		CD_MEDICO_ATENDIMENTO_W		:=	nvl(:new.CD_PROF_MEDICO, :old.CD_PROF_MEDICO);

		if (vl_default_w is not null) then
			CD_MEDICO_ATENDIMENTO_W		:=	substr(vl_default_w, 1, 10);
		end if;

		if 	(CD_MEDICO_ATENDIMENTO_W is not null)	then
			update_resp_physician(CD_MEDICO_ATENDIMENTO_W, NR_SEQ_ATEND_PAC_UNIDADE_W);
		end if;

	end if;

end ATEND_PAC_UND_INF_BEFORE_DEL;
/


ALTER TABLE TASY.ATEND_PACIENTE_UNIDADE_INF ADD (
  CONSTRAINT ATPACUF_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ATEND_PACIENTE_UNIDADE_INF ADD (
  CONSTRAINT ATPACUF_PESFISI_FK 
 FOREIGN KEY (CD_PROF_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATPACUF_PESFISI_FK1 
 FOREIGN KEY (CD_PROF_ENFERMEIRA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATPACUF_ATEPACU_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PAC_UNIDADE) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ATEND_PACIENTE_UNIDADE_INF TO NIVEL_1;

