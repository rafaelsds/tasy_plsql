ALTER TABLE TASY.PLS_PROT_CONTA_TITULO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROT_CONTA_TITULO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROT_CONTA_TITULO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  CD_MEDICO_EXECUTOR   VARCHAR2(10 BYTE),
  VL_TITULO            NUMBER(15,2),
  NR_SEQ_PRESTADOR     NUMBER(10)               NOT NULL,
  DT_PAGAMENTO         DATE,
  IE_FORMA_PAGTO       VARCHAR2(5 BYTE),
  CD_BANCO             VARCHAR2(20 BYTE),
  CD_AGENCIA           VARCHAR2(20 BYTE),
  NR_CONTA             VARCHAR2(50 BYTE),
  VL_COBRADO           NUMBER(15,2),
  VL_TOTAL_LIBERADO    NUMBER(15,2),
  VL_GLOSA             NUMBER(15,2),
  NR_NOTA_FISCAL       NUMBER(20),
  NR_SEQ_LOTE          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPCTI_PESFISI_FK_I ON TASY.PLS_PROT_CONTA_TITULO
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPCTI_PK ON TASY.PLS_PROT_CONTA_TITULO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPCTI_PLSLOPO_FK_I ON TASY.PLS_PROT_CONTA_TITULO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPCTI_PLSPRCO_FK_I ON TASY.PLS_PROT_CONTA_TITULO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPCTI_PLSPRES_FK_I ON TASY.PLS_PROT_CONTA_TITULO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPCTI_PLSPRES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_prot_conta_titulo_update
after update ON TASY.PLS_PROT_CONTA_TITULO for each row
declare
ds_tabela_alterada_w	varchar2(255);

begin

if	(:old.nr_nota_fiscal is not null) and
	(:old.nr_nota_fiscal <> :new.nr_nota_fiscal) then
	ds_tabela_alterada_w	:= ds_tabela_alterada_w || ' Nota F�scal: de ' ||:old.nr_nota_fiscal || ' para ' || :new.nr_nota_fiscal;
end if;

if	(:old.vl_titulo is not null) and
	(:old.vl_titulo <> :new.vl_titulo) then
	ds_tabela_alterada_w	:= ds_tabela_alterada_w || ' Valor Titulo: de ' || :old.vl_titulo || ' para ' || :new.vl_titulo;
end if;

if	(:old.nr_seq_prestador is not null) and
	(:old.nr_seq_prestador <> :new.nr_seq_prestador) then
	ds_tabela_alterada_w	:= ds_tabela_alterada_w || ' Prestador: de ' ||:old.nr_seq_prestador || ' para ' || :new.nr_seq_prestador;
end if;

if	(:old.dt_pagamento is not null) and
	(:old.dt_pagamento <> :new.dt_pagamento) then
	ds_tabela_alterada_w	:= ds_tabela_alterada_w || ' Pagador: de ' ||:old.dt_pagamento || ' para ' || :new.dt_pagamento;
end if;

if	(:old.nr_seq_lote is not null) and
	(:old.nr_seq_lote <> :new.nr_seq_lote) then
	ds_tabela_alterada_w	:= ds_tabela_alterada_w || ' Lote: de ' || :old.nr_seq_lote || ' para ' || :new.nr_seq_lote;
end if;

/*if	(ds_tabela_alterada_w is not null) then
	insert into logxxxxx_tasy
		(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values	(sysdate, :new.nm_usuario, 1208, ds_tabela_alterada_w);
end if;*/
end;
/


ALTER TABLE TASY.PLS_PROT_CONTA_TITULO ADD (
  CONSTRAINT PLSPCTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROT_CONTA_TITULO ADD (
  CONSTRAINT PLSPCTI_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSPCTI_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSPCTI_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPCTI_PLSLOPO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_PROTOCOLO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PROT_CONTA_TITULO TO NIVEL_1;

