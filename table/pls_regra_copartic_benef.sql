ALTER TABLE TASY.PLS_REGRA_COPARTIC_BENEF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_BENEF
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_REGRA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_TITULARIDADE            VARCHAR2(2 BYTE),
  NR_SEQ_PLANO               NUMBER(10),
  QT_IDADE_MIN               NUMBER(5),
  QT_IDADE_MAX               NUMBER(5),
  IE_TIPO_PARENTESCO         VARCHAR2(2 BYTE),
  DT_CONTRATO_DE             DATE,
  DT_CONTRATO_ATE            DATE,
  NR_FAIXA_SALARIAL_INICIAL  NUMBER(10),
  NR_FAIXA_SALARIAL_FINAL    NUMBER(10),
  IE_SITUACAO_BENEF          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGCB_PK ON TASY.PLS_REGRA_COPARTIC_BENEF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCB_PLSPLAN_FK_I ON TASY.PLS_REGRA_COPARTIC_BENEF
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCB_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC_BENEF
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copart_benef_update
before update ON TASY.PLS_REGRA_COPARTIC_BENEF for each row
declare

begin
if	(:new.nr_seq_plano <> :old.nr_seq_plano) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_plano, :new.nr_seq_plano, 'PLS_REGRA_COPARTIC_BENEF', 'NR_SEQ_PLANO', :new.nm_usuario);
end if;
if	(:new.ie_titularidade <> :old.ie_titularidade) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_titularidade, :new.ie_titularidade, 'PLS_REGRA_COPARTIC_BENEF', 'IE_TITULARIDADE', :new.nm_usuario);
end if;
if	(:new.ie_situacao_benef <> :old.ie_situacao_benef) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_situacao_benef, :new.ie_situacao_benef, 'PLS_REGRA_COPARTIC_BENEF', 'IE_SITUACAO_BENEF', :new.nm_usuario);
end if;
if	(:new.nr_seq_plano <> :old.ie_tipo_parentesco) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_parentesco, :new.ie_tipo_parentesco, 'PLS_REGRA_COPARTIC_BENEF', 'IE_TIPO_PARENTESCO', :new.nm_usuario);
end if;
if	(:new.qt_idade_min <> :old.qt_idade_min) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_idade_min, :new.qt_idade_min, 'PLS_REGRA_COPARTIC_BENEF', 'QT_IDADE_MIN', :new.nm_usuario);
end if;
if	(:new.qt_idade_max <> :old.qt_idade_max)then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_idade_max, :new.qt_idade_max, 'PLS_REGRA_COPARTIC_BENEF', 'QT_IDADE_MAX', :new.nm_usuario);
end if;
if	(:new.dt_contrato_de <> :old.dt_contrato_de) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.dt_contrato_de, :new.dt_contrato_de, 'PLS_REGRA_COPARTIC_BENEF', 'DT_CONTRATO_DE', :new.nm_usuario);
end if;
if	(:new.dt_contrato_ate <> :old.dt_contrato_ate)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.dt_contrato_ate, :new.dt_contrato_ate, 'PLS_REGRA_COPARTIC_BENEF', 'DT_CONTRATO_ATE', :new.nm_usuario);
end if;
if	(:new.nr_faixa_salarial_inicial <> :old.nr_faixa_salarial_inicial) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_faixa_salarial_inicial, :new.nr_faixa_salarial_inicial, 'PLS_REGRA_COPARTIC_BENEF', 'NR_FAIXA_SALARIAL_INICIAL', :new.nm_usuario);
end if;
if	(:new.nr_faixa_salarial_final <> :old.nr_faixa_salarial_final)	then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_faixa_salarial_final, :new.nr_faixa_salarial_final, 'PLS_REGRA_COPARTIC_BENEF', 'NR_FAIXA_SALARIAL_FINAL', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_BENEF ADD (
  CONSTRAINT PLSRGCB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_BENEF ADD (
  CONSTRAINT PLSRGCB_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCB_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_BENEF TO NIVEL_1;

