DROP TABLE TASY.W_HMV_PS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_HMV_PS
(
  DT_ESTATISTICA       DATE,
  QT_CLINICA4          NUMBER(5),
  QT_PSA               NUMBER(5),
  QT_PSI               NUMBER(5),
  QT_PSO               NUMBER(5),
  QT_PSGO              NUMBER(5),
  QT_TOTAL_PS          NUMBER(5),
  QT_CC                NUMBER(5),
  QT_CTI               NUMBER(5),
  QT_UCO               NUMBER(5),
  QT_UTIN              NUMBER(5),
  QT_UTIP              NUMBER(5),
  QT_MEIO_DIA          NUMBER(5),
  QT_ADMIT             NUMBER(5),
  QT_ALTA              NUMBER(5),
  QT_DIA               NUMBER(5),
  QT_MEDIA_DIARIA      NUMBER(10),
  QT_MEDIA_CIRURGIA    NUMBER(10),
  QT_MEDIA_PS          NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_CMA               NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_HMV_PS TO NIVEL_1;

