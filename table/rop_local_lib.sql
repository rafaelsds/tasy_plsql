ALTER TABLE TASY.ROP_LOCAL_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_LOCAL_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_LOCAL_LIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL,
  NR_SEQ_LOCAL_LIB     NUMBER(10)               NOT NULL,
  IE_LIBERAR           VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ROPLOLI_PK ON TASY.ROP_LOCAL_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOLI_PK
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOLI_ROPLOCA_FK_I ON TASY.ROP_LOCAL_LIB
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOLI_ROPLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPLOLI_ROPLOCA_FK2_I ON TASY.ROP_LOCAL_LIB
(NR_SEQ_LOCAL_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPLOLI_ROPLOCA_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_LOCAL_LIB_tp  after update ON TASY.ROP_LOCAL_LIB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_LIBERAR,1,4000),substr(:new.IE_LIBERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIBERAR',ie_log_w,ds_w,'ROP_LOCAL_LIB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCAL_LIB,1,4000),substr(:new.NR_SEQ_LOCAL_LIB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCAL_LIB',ie_log_w,ds_w,'ROP_LOCAL_LIB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_LOCAL_LIB ADD (
  CONSTRAINT ROPLOLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_LOCAL_LIB ADD (
  CONSTRAINT ROPLOLI_ROPLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.ROP_LOCAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ROPLOLI_ROPLOCA_FK2 
 FOREIGN KEY (NR_SEQ_LOCAL_LIB) 
 REFERENCES TASY.ROP_LOCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_LOCAL_LIB TO NIVEL_1;

