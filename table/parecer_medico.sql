ALTER TABLE TASY.PARECER_MEDICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARECER_MEDICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARECER_MEDICO
(
  NR_PARECER                 NUMBER(10)         NOT NULL,
  NR_SEQUENCIA               NUMBER(3)          NOT NULL,
  CD_MEDICO                  VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_PARECER                 LONG,
  DT_LIBERACAO               DATE,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_STATUS                  VARCHAR2(1 BYTE),
  NR_SEQ_INTERNO             NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_REGULACAO           NUMBER(10),
  NR_SEQ_AVALIACAO           NUMBER(10),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  CD_EVOLUCAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARMEDI_ATCONSPEPA_FK_I ON TASY.PARECER_MEDICO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDI_EVOPACI_FK_I ON TASY.PARECER_MEDICO
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDI_PARMERE_FK_I ON TASY.PARECER_MEDICO
(NR_PARECER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDI_PESFISI_FK_I ON TASY.PARECER_MEDICO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARMEDI_PK ON TASY.PARECER_MEDICO
(NR_PARECER, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDI_REGATEN_FK_I ON TASY.PARECER_MEDICO
(NR_SEQ_REGULACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PARMEDI_TASASDI_FK_I ON TASY.PARECER_MEDICO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMEDI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARMEDI_TASASDI_FK2_I ON TASY.PARECER_MEDICO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARMEDI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PARMEDI_UK ON TASY.PARECER_MEDICO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.parecer_medico_update
after update ON TASY.PARECER_MEDICO for each row
declare

nr_atendimento_w	number(10);
ie_gerar_lancto_auto_w	varchar2(1);
ie_gerar_evol_in_ref_w varchar2(5);

pragma autonomous_transaction;

begin

begin
obter_param_usuario(281, 1641, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_evol_in_ref_w);

ie_gerar_lancto_auto_w := nvl(obter_valor_param_usuario(281, 1104, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'S');
exception
when others then
	ie_gerar_lancto_auto_w	:= 'S';
end;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	parecer_medico_req
	where	nr_parecer = :new.nr_parecer;

	if	(ie_gerar_lancto_auto_w = 'S') then
		gerar_lancamento_automatico(nr_atendimento_w, null, 321, :new.nm_usuario,
			:new.nr_sequencia, :new.nr_parecer, :new.cd_medico, to_char(:new.dt_liberacao,'dd/mm/yyyy hh24:mi:ss'), null, null);
		commit;
	end if;
end if;
if(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if(ie_gerar_evol_in_ref_w ='S' and :new.cd_evolucao is not null and :new.dt_inativacao is not null and :old.dt_inativacao is null ) then
    update evolucao_paciente
	set dt_inativacao = sysdate,
	ie_situacao ='I',
	dt_atualizacao = sysdate ,
	nm_usuario = wheb_usuario_pck.get_nm_usuario,
	nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
	ds_justificativa = :new.ds_justificativa
	where cd_evolucao = :new.cd_evolucao;
	delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'INTERNAL_REF' and nr_seq_med_item = :new.NR_PARECER;
	commit;
    end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.parecer_medico_pend_atual
after insert or update or delete ON TASY.PARECER_MEDICO for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w			number(1);
nm_usuario_w			varchar2(15);
cd_especialidade_dest_w		number(5,0);
cd_especialidade_dest_prof_w	number(5,0);
cd_medico_parecer_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10,0);
ie_lib_parecer_medico_w		varchar2(10);
ie_pend_parecer_w		varchar2(10);
ie_registro_w			char(1);
ie_liberado_w			char(1);

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;

	if	(inserting) or
		(updating) then

		select  nvl(max(ie_lib_parecer_medico),'N'),
			nvl(max(ie_pend_parecer),'N')
		into    ie_lib_parecer_medico_w,
			ie_pend_parecer_w
		from    parametro_medico
		where   cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

		if  (nvl(ie_lib_parecer_medico_w,'N') = 'S') and (nvl(ie_pend_parecer_w,'S') = 'S') 	then

			if	(:new.dt_liberacao is null) and (not deleting) then
				select  nvl(max(cd_especialidade_dest), 0),
					nvl(max(cd_especialidade_dest_prof), 0),
					nvl(max(cd_pessoa_fisica), 0),
					nvl(max(nr_atendimento), 0)
				into    cd_especialidade_dest_w,
					cd_especialidade_dest_prof_w,
					cd_pessoa_fisica_w,
					nr_atendimento_w
				from    parecer_medico_req
				where   nr_parecer = :new.nr_parecer;

				if (cd_pessoa_fisica_w > 0 ) then
					Gerar_registro_pendente_PEP('RP',:new.nr_sequencia,cd_pessoa_fisica_w,nr_atendimento_w,:new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,:new.nr_parecer);
				end if;

			elsif   (:old.dt_liberacao is null) and
				(:new.dt_liberacao is not null) then
					Gerar_registro_pendente_PEP('XRP',:new.nr_sequencia,cd_pessoa_fisica_w,nr_atendimento_w,:new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,:new.nr_parecer);

				select	nvl(max('S'),'N')
				into	ie_liberado_w
				from	parecer_medico_req
				where	nr_parecer = :new.nr_parecer
				and		dt_liberacao is not null;

				if (ie_liberado_w = 'S') then
					Gerar_registro_pendente_PEP('XPM', :new.nr_parecer, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
				end if;
			end if;
		end if;
	elsif	(deleting) then

		select 	nvl(max('S'),'N')
		into	ie_registro_w
		from	parecer_medico_req
		where	nr_parecer = :old.nr_parecer;

		if (ie_registro_w = 'S')  then
			delete	from pep_item_pendente
			where 	ie_tipo_registro = 'RP'
			and     nr_seq_registro = :old.nr_sequencia
			and	nr_parecer = :old.nr_parecer
			and     nvl(IE_TIPO_PENDENCIA,'L') = 'L';
			commit;
		end if;

	end if;

	commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.parecer_medico_atual
before  insert or update ON TASY.PARECER_MEDICO for each row
declare

ie_integracao_w		varchar2(1);
ds_retorno_integracao_w	varchar2(4000);
cd_estabelecimento_w    NUMBER(4) := obter_estabelecimento_ativo;
nr_atendimento_w        NUMBER;
cd_pessoa_fisica_w      NUMBER;
cd_pessoa_parecer_w     NUMBER;
cd_especialidade_w      NUMBER;
cd_especialidade_dest_w NUMBER;
nr_seq_tipo_parecer_w   NUMBER;
nr_seq_tipo_parecer_ww  regra_envio_sms.nr_seq_tipo_parecer%type;
nr_seq_equipe_dest_w    NUMBER;
nr_seq_evento_w         NUMBER;
cd_medico_w             parecer_medico_req.cd_medico%type;

qt_reg_w				number(1);

CURSOR c01 IS
    select
        nr_seq_evento
    from
        (
            select
                1 cd,
                nr_seq_evento
            from
                regra_envio_sms
            where
                cd_estabelecimento = cd_estabelecimento_w
                and ie_evento_disp = 'LRP'
                and nvl(ie_situacao, 'A') = 'A'
                and nr_seq_tipo_parecer = nr_seq_tipo_parecer_w
            union
            select
                2 cd,
                nr_seq_evento
            from
                regra_envio_sms
            where
                cd_estabelecimento = cd_estabelecimento_w
                and ie_evento_disp = 'LRP'
                and nvl(ie_situacao, 'A') = 'A'
                and nvl(nr_seq_tipo_parecer, nvl(nr_seq_tipo_parecer_w, - 1)) = nvl(nr_seq_tipo_parecer_w, - 1)
        )
    where
        rownum = 1;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.nr_seq_interno is null) then
	select	parecer_medico_seq.nextval
	into	:new.nr_seq_interno
	from	dual;
end if;

if	(nvl(:old.DT_ATUALIZACAO,sysdate+10) <> :new.DT_ATUALIZACAO) and
	(:new.DT_ATUALIZACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.ie_situacao = 'I') then
	wl_gerar_finalizar_tarefa('DL', 'R', null, null, :new.nm_usuario, sysdate,'N',null,null,null,null,null,null,null, :new.nr_parecer );
end if;

-- WorkList AtePac_WL (357)
if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	wl_gerar_finalizar_tarefa('DL', 'F', null, null, :new.nm_usuario, sysdate,'N',null,null,null,null,null,null,null, :new.nr_parecer );
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then

	Select  nvl(max(ie_integracao),'N')
	into	ie_integracao_w
	from	parecer_medico_req
	where	nr_parecer = :new.nr_parecer;

	if (ie_integracao_w = 'S') then

		SELECT BIFROST.SEND_INTEGRATION(
		'clinicalnotes.opinioncounterreferralreturn',
		'com.philips.tasy.integration.atepac.clinicalNotes.opinionCounterReferralReturn.OpinionCounterReferralReturn',
		'{"internalSequence" : '|| :new.nr_seq_interno || '}',
		:new.nm_usuario)
		INTO ds_retorno_integracao_w
		FROM dual;

	end if;

-- Event Resposta_Parecer
  SELECT nr_atendimento,
         cd_pessoa_fisica,
         cd_pessoa_parecer,
         cd_especialidade,
         cd_especialidade_dest,
         nr_seq_tipo_parecer,
         nr_seq_equipe_dest,
         cd_medico
  INTO   nr_atendimento_w, cd_pessoa_fisica_w, cd_pessoa_parecer_w,
         cd_especialidade_w,
  cd_especialidade_dest_w, nr_seq_tipo_parecer_w, nr_seq_equipe_dest_w, cd_medico_w
  FROM   parecer_medico_req
  WHERE  nr_parecer = :new.nr_parecer;

  if(cd_pessoa_parecer_w is null) then
    cd_pessoa_parecer_w := :new.cd_medico;
  end if;
  cd_especialidade_dest_w := obter_especialidade_medico(:new.cd_medico, 'C');

  OPEN c01;

  LOOP
      FETCH c01 INTO nr_seq_evento_w;
      exit WHEN c01%NOTFOUND;

      BEGIN
            Gerar_evento_paciente(nr_seq_evento_w, nr_atendimento_w, cd_pessoa_fisica_w, NULL, :new.nm_usuario, NULL, NULL, NULL, NULL, cd_medico_w, cd_medico_w, NULL, :new.nr_parecer, NULL, NULL, 'N', NULL,
            ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE), NULL, NULL, NULL, NULL,
            cd_especialidade_dest_w, cd_especialidade_w, nr_seq_tipo_parecer_w, nr_seq_equipe_dest_w, 'S');

      END;
  END LOOP;

  CLOSE c01;

end if;

<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.PARECER_MEDICO ADD (
  CONSTRAINT PARMEDI_PK
 PRIMARY KEY
 (NR_PARECER, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PARMEDI_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARECER_MEDICO ADD (
  CONSTRAINT PARMEDI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PARMEDI_REGATEN_FK 
 FOREIGN KEY (NR_SEQ_REGULACAO) 
 REFERENCES TASY.REGULACAO_ATEND (NR_SEQUENCIA),
  CONSTRAINT PARMEDI_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT PARMEDI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PARMEDI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PARMEDI_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PARMEDI_PARMERE_FK 
 FOREIGN KEY (NR_PARECER) 
 REFERENCES TASY.PARECER_MEDICO_REQ (NR_PARECER)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PARECER_MEDICO TO NIVEL_1;

