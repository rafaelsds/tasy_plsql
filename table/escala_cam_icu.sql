ALTER TABLE TASY.ESCALA_CAM_ICU
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CAM_ICU CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CAM_ICU
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  IE_ALT_ESTADO_MENTAL      VARCHAR2(1 BYTE)    NOT NULL,
  IE_COMPORT_ANORMAL        VARCHAR2(1 BYTE)    NOT NULL,
  IE_FOCAR_ATENCAO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_DISTRAIDO              VARCHAR2(1 BYTE)    NOT NULL,
  IE_DESORGANIZADO          VARCHAR2(1 BYTE)    NOT NULL,
  IE_CONSIENCIA             VARCHAR2(1 BYTE)    NOT NULL,
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_PEDRA_FLUTUAM          VARCHAR2(1 BYTE),
  IE_PEIXE_MAR              VARCHAR2(1 BYTE),
  IE_QUILOGRAMA             VARCHAR2(1 BYTE),
  IE_MARTELO_MADEIRA        VARCHAR2(1 BYTE),
  IE_AGITADO                VARCHAR2(1 BYTE),
  IE_LETAGICO               VARCHAR2(1 BYTE),
  IE_ESTUPOROSO             VARCHAR2(1 BYTE),
  IE_CAMATOSO               VARCHAR2(1 BYTE),
  IE_ATENCAO_PRESENTE       VARCHAR2(1 BYTE),
  IE_PENS_DESORG_PRESENTE   VARCHAR2(1 BYTE),
  IE_NIVEL_CONC_PRESENTE    VARCHAR2(1 BYTE),
  IE_INICIO_AGUDO_PRESENTE  VARCHAR2(1 BYTE),
  IE_CAM_ICU_MODIFICADA     VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO       NUMBER(10),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCAMI_ATEPACI_FK_I ON TASY.ESCALA_CAM_ICU
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCAMI_EHRREEL_FK_I ON TASY.ESCALA_CAM_ICU
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCAMI_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCCAMI_I1 ON TASY.ESCALA_CAM_ICU
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCAMI_PESFISI_FK_I ON TASY.ESCALA_CAM_ICU
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCAMI_PK ON TASY.ESCALA_CAM_ICU
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_escala_cam_icu_evol_upd
after update ON TASY.ESCALA_CAM_ICU for each row
declare

ds_evolucao_w long;
ds_ausente_w varchar2(255):='Ausente';
ds_presente_w varchar2(255):='Presente';
ds_enter_w varchar2(255):='

';
ds_espaco_w varchar2(255):='
';

    function hsj_obter_resposta(ie_tipo_p varchar2)return varchar2 is
        ds_resposta_w varchar2(100);
    begin
        
        if(ie_tipo_p = 'P')then
            return 'Presente';
        elsif(ie_tipo_p = 'A')then
            return 'Ausente';
        end if;
    
    end;

begin


    if(:old.dt_liberacao is null and :new.dt_liberacao is not null)then
        
        ds_evolucao_w:= 'Evolu��o Escala CAM-ICU - Enfermeiro';
        
        -- Dados do bloco 1
        ds_evolucao_w:=ds_evolucao_w||ds_enter_w||'1 - In�cio agudo e curso flutuante: '||hsj_obter_resposta(:new.IE_INICIO_AGUDO_PRESENTE);
        
        if(:new.IE_INICIO_AGUDO_PRESENTE = 'P')then
        
            if(:new.IE_ALT_ESTADO_MENTAL = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- H� alguma mudan�a aguda no status mental da base? R: Sim';
            end if;
        
            if(:new.IE_COMPORT_ANORMAL = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- O status mental do paciente flutuou nas �ltimas 24h, conforme evidenciado pela flutua��o da pontua��o em uma escala de seda��o ou avalia��o pr�via de delirium? R: Sim';
            end if;
        
        end if;
        
        
        -- Dados do bloco 2
        ds_evolucao_w:=ds_evolucao_w||ds_enter_w||'2 - Falta de aten��o: '||hsj_obter_resposta(:new.IE_ATENCAO_PRESENTE);
        
        if(:new.IE_ATENCAO_PRESENTE = 'P')then
        
            if(:new.IE_FOCAR_ATENCAO = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- O paciente teve dificuldade em focar aten��o? R: Sim';
            end if;
        
            if(:new.IE_DISTRAIDO = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- O paciente esteve facilmente distra�do ou apresentou dificuldade em seguir ou manter o que estava sendo falado? R: Sim';
            end if;
        
        end if;
        
        
        -- Dados do bloco 3
        ds_evolucao_w:=ds_evolucao_w||ds_enter_w||'3 - Pensamento desorganizado: '||hsj_obter_resposta(:new.IE_PENS_DESORG_PRESENTE);
        
        if(:new.IE_PENS_DESORG_PRESENTE = 'P')then
        
            if(:new.IE_PEDRA_FLUTUAM = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Paciente incapaz de responder: As pedras flutuam na �gua? R: Sim';
            end if;
        
            if(:new.IE_PEIXE_MAR = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Paciente incapaz de responder: Existem peixes no mar? R: Sim';
            end if;
        
            if(:new.IE_QUILOGRAMA = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Paciente incapaz de responder: Um quilograma pesa mais do que dois quilogramas? R: Sim';
            end if;
            
            if(:new.IE_MARTELO_MADEIRA = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Paciente incapaz de responder: Pode-se utilizar um martelo para cortar madeira? R: Sim';
            end if;
        end if;
        
        
        -- Dados do bloco 4
        ds_evolucao_w:=ds_evolucao_w||ds_enter_w||'4 - N�vel de consci�ncia alterado: '||hsj_obter_resposta(:new.IE_NIVEL_CONC_PRESENTE);
        
        if(:new.IE_PENS_DESORG_PRESENTE = 'P')then
        
            if(:new.IE_AGITADO = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Agitado: Paciente com RASS entre +1 e +4. R: Sim';
            end if;  

            if(:new.IE_LETAGICO = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Let�gico: Sonolento por�m facilmente despert�vel. R: Sim';
            end if;
            
            if(:new.IE_ESTUPOROSO = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Estuporoso: Apenas despert�vel com est�mulos rigorosos voltando ao estado de estupor assim que finalizado o est�mulo. R: Sim';
            end if;
            
            if(:new.IE_CAMATOSO = 'S')then
                ds_evolucao_w:=ds_evolucao_w||ds_espaco_w||'- Comatoso: Completamente alienado mesmo quando estimulado vigorosamente. R: Sim';
            end if;
            
        end if;
        
        ds_evolucao_w:= ds_evolucao_w||ds_enter_w||'Resultado: '||  substr(obter_result_CAM_ICU(:new.ie_alt_estado_mental,:new.ie_comport_anormal,
                                                                                :new.ie_focar_atencao,:new.ie_distraido,null,null,'S',:new.ie_pedra_flutuam,:new.ie_peixe_mar,:new.ie_quilograma,:new.ie_martelo_madeira,
                                                                                :new.ie_agitado,:new.ie_letagico,:new.ie_estuporoso,:new.ie_camatoso),1,255);
        
        hsj_gerar_evolucao(Obter_dados_usuario_opcao(:new.nm_usuario ,'F'), :new.nr_atendimento, ds_evolucao_w, 'CAM', :new.nm_usuario, 'T', 'N', 'N', 'N', 'S');
       
    end if;

  
end hsj_escala_cam_icu_evol_upd;
/


CREATE OR REPLACE TRIGGER TASY.escala_cam_icu_atual
before insert or update ON TASY.ESCALA_CAM_ICU for each row
declare

qt_pens_desorg_presente  	Number(10);


begin

:new.ie_inicio_agudo_presente := 'A';
if	(nvl(:new.ie_alt_estado_mental,'N') = 'S') or
	(nvl(:new.ie_comport_anormal,'N') = 'S') then
	:new.ie_inicio_agudo_presente := 'P';
end if;

:new.ie_atencao_presente := 'A';
if	(nvl(:new.ie_focar_atencao,'N') = 'S') or
	(nvl(:new.ie_distraido,'N') = 'S') then
	:new.ie_atencao_presente := 'P';
end if;

:new.ie_pens_desorg_presente := 'A';
qt_pens_desorg_presente:= 0;
if	(nvl(:new.ie_pedra_flutuam,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if	(nvl(:new.ie_peixe_mar,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if (nvl(:new.ie_quilograma,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if (nvl(:new.ie_martelo_madeira,'N') = 'S') then
	qt_pens_desorg_presente := qt_pens_desorg_presente + 1;
end if;
if	(qt_pens_desorg_presente > 1) then
	:new.ie_pens_desorg_presente := 'P';
end if;

:new.ie_nivel_conc_presente := 'A';
if	(nvl(:new.ie_agitado,'N') = 'S') or
	(nvl(:new.ie_letagico,'N') = 'S') or
	(nvl(:new.ie_estuporoso,'N') = 'S') or
	(nvl(:new.ie_camatoso,'N') = 'S') then
	:new.ie_nivel_conc_presente := 'P';
end if;

end;
/


ALTER TABLE TASY.ESCALA_CAM_ICU ADD (
  CONSTRAINT ESCCAMI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CAM_ICU ADD (
  CONSTRAINT ESCCAMI_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCCAMI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCAMI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_CAM_ICU TO NIVEL_1;

