ALTER TABLE TASY.STS_REQUEST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.STS_REQUEST CASCADE CONSTRAINTS;

CREATE TABLE TASY.STS_REQUEST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_INTERNO_CONTA     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_NAME              VARCHAR2(40 BYTE),
  IE_REQUEST           VARCHAR2(10 BYTE),
  CD_REPORT_STATUS     VARCHAR2(20 BYTE),
  CD_TRANSACTION       VARCHAR2(24 BYTE),
  DT_RECEIVED_FROM     DATE,
  DT_RECEIVED_TO       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.STSREQU_PK ON TASY.STS_REQUEST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.STS_REQUEST ADD (
  CONSTRAINT STSREQU_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.STS_REQUEST TO NIVEL_1;

