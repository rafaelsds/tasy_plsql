ALTER TABLE TASY.PLS_OC_CTA_FILTRO_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_FILTRO_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_FILTRO_CONTRATO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_FILTRO   NUMBER(10)             NOT NULL,
  NR_CONTRATO            NUMBER(10),
  NR_SEQ_GRUPO_CONTRATO  NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOFCT_PK ON TASY.PLS_OC_CTA_FILTRO_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOFCT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOFCT_PLSOCCF_FK_I ON TASY.PLS_OC_CTA_FILTRO_CONTRATO
(NR_SEQ_OC_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOFCT_PLSOCCF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOFCT_PLSPRGC_FK_I ON TASY.PLS_OC_CTA_FILTRO_CONTRATO
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOFCT_PLSPRGC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_FILTRO_CONTRATO_tp  after update ON TASY.PLS_OC_CTA_FILTRO_CONTRATO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTRATO,1,4000),substr(:new.NR_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTRATO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_CONTRATO,1,4000),substr(:new.NR_SEQ_GRUPO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_CONTRATO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_OC_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_FILTRO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO_CONTRATO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_FILTRO_CONTRATO ADD (
  CONSTRAINT PLSOFCT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_FILTRO_CONTRATO ADD (
  CONSTRAINT PLSOFCT_PLSOCCF_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_FILTRO) 
 REFERENCES TASY.PLS_OC_CTA_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSOFCT_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_FILTRO_CONTRATO TO NIVEL_1;

