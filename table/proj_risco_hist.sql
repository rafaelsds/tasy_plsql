ALTER TABLE TASY.PROJ_RISCO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_RISCO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_RISCO_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_RISCO         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_HISTORICO         DATE                     NOT NULL,
  DS_HISTORICO         VARCHAR2(4000 BYTE)      NOT NULL,
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROJRIHI_PK ON TASY.PROJ_RISCO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRIHI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJRIHI_PROJRIIM_FK_I ON TASY.PROJ_RISCO_HIST
(NR_SEQ_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJRIHI_PROJRIIM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_RISCO_HIST ADD (
  CONSTRAINT PROJRIHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_RISCO_HIST ADD (
  CONSTRAINT PROJRIHI_PROJRIIM_FK 
 FOREIGN KEY (NR_SEQ_RISCO) 
 REFERENCES TASY.PROJ_RISCO_IMPLANTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_RISCO_HIST TO NIVEL_1;

