ALTER TABLE TASY.PLS_PRECO_AUTOGERADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRECO_AUTOGERADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRECO_AUTOGERADO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA         NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  CD_MOEDA             NUMBER(3)                NOT NULL,
  PR_INICIO            NUMBER(7,4)              NOT NULL,
  PR_FIM               NUMBER(7,4)              NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRAU_I1 ON TASY.PLS_PRECO_AUTOGERADO
(DT_INICIO_VIGENCIA, DT_FIM_VIGENCIA, NVL("PR_INICIO",0), NVL("PR_FIM",100), IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRAU_MOEDA_FK_I ON TASY.PLS_PRECO_AUTOGERADO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRAU_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPRAU_PK ON TASY.PLS_PRECO_AUTOGERADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRAU_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRAU_PLSRPRA_FK_I ON TASY.PLS_PRECO_AUTOGERADO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRAU_PLSRPRA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_PRECO_AUTOGERADO_tp  after update ON TASY.PLS_PRECO_AUTOGERADO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_PRECO_AUTOGERADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_PRECO_AUTOGERADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PRECO_AUTOGERADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_INICIO,1,4000),substr(:new.PR_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'PR_INICIO',ie_log_w,ds_w,'PLS_PRECO_AUTOGERADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_FIM,1,4000),substr(:new.PR_FIM,1,4000),:new.nm_usuario,nr_seq_w,'PR_FIM',ie_log_w,ds_w,'PLS_PRECO_AUTOGERADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA,1,4000),substr(:new.CD_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA',ie_log_w,ds_w,'PLS_PRECO_AUTOGERADO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PRECO_AUTOGERADO ADD (
  CONSTRAINT PLSPRAU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRECO_AUTOGERADO ADD (
  CONSTRAINT PLSPRAU_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSPRAU_PLSRPRA_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_PRECO_AUTOGERADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRECO_AUTOGERADO TO NIVEL_1;

