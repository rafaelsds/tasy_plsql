ALTER TABLE TASY.LOG_GERA_DEVOL_MAT_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_GERA_DEVOL_MAT_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_GERA_DEVOL_MAT_PROC
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  NM_USUARIO    VARCHAR2(15 BYTE)               NOT NULL,
  DT_INICIO     DATE,
  DT_FINAL      DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LOGEDE_PK ON TASY.LOG_GERA_DEVOL_MAT_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOG_GERA_DEVOL_MAT_PROC ADD (
  CONSTRAINT LOGEDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOG_GERA_DEVOL_MAT_PROC TO NIVEL_1;

