ALTER TABLE TASY.TIT_REC_LIQ_CARTAO_CR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIT_REC_LIQ_CARTAO_CR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIT_REC_LIQ_CARTAO_CR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10)               NOT NULL,
  NR_SEQ_BAIXA         NUMBER(10)               NOT NULL,
  NR_SEQ_MOVTO         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TRLCACR_PK ON TASY.TIT_REC_LIQ_CARTAO_CR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRLCACR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TIT_REC_LIQ_CARTAO_CR_AFTER
after insert or update ON TASY.TIT_REC_LIQ_CARTAO_CR for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w				number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_seq_baixa is not null) and (:new.nr_titulo is not null) then

	/*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and		nr_seq_item_documento	= to_char(:new.nr_seq_baixa)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p	:=	:new.nr_seq_baixa;
		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;

end if;
end if;

end;
/


ALTER TABLE TASY.TIT_REC_LIQ_CARTAO_CR ADD (
  CONSTRAINT TRLCACR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TIT_REC_LIQ_CARTAO_CR TO NIVEL_1;

