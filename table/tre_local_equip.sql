ALTER TABLE TASY.TRE_LOCAL_EQUIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRE_LOCAL_EQUIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRE_LOCAL_EQUIP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_LOCAL         NUMBER(10),
  NR_SEQ_EQUIP         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TRELOEQ_PK ON TASY.TRE_LOCAL_EQUIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRELOEQ_PK
  MONITORING USAGE;


CREATE INDEX TASY.TRELOEQ_TRELOC_FK_I ON TASY.TRE_LOCAL_EQUIP
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRELOEQ_TRELOC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRELOEQ_TREQUI_FK_I ON TASY.TRE_LOCAL_EQUIP
(NR_SEQ_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRELOEQ_TREQUI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TRE_LOCAL_EQUIP ADD (
  CONSTRAINT TRELOEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRE_LOCAL_EQUIP ADD (
  CONSTRAINT TRELOEQ_TRELOC_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.TRE_LOCAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TRELOEQ_TREQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIP) 
 REFERENCES TASY.TRE_EQUIP (NR_SEQUENCIA));

GRANT SELECT ON TASY.TRE_LOCAL_EQUIP TO NIVEL_1;

