ALTER TABLE TASY.INDICE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INDICE CASCADE CONSTRAINTS;

CREATE TABLE TASY.INDICE
(
  NM_TABELA            VARCHAR2(50 BYTE)        NOT NULL,
  NM_INDICE            VARCHAR2(50 BYTE)        NOT NULL,
  IE_TIPO              VARCHAR2(2 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_INDICE            VARCHAR2(255 BYTE),
  IE_CRIAR_ALTERAR     VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  DT_CRIACAO           DATE,
  IE_IGNORADO_CLIENTE  VARCHAR2(1 BYTE),
  IE_CLASSIFICACAO     VARCHAR2(3 BYTE),
  DS_MENSAGEM          VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INDICE_I1 ON TASY.INDICE
(NM_TABELA, IE_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INDICE_PK ON TASY.INDICE
(NM_TABELA, NM_INDICE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INDICE_TABSIST_FK_I ON TASY.INDICE
(NM_TABELA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INDICE_tp  after update ON TASY.INDICE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NM_TABELA='||to_char(:old.NM_TABELA)||'#@#@NM_INDICE='||to_char(:old.NM_INDICE);gravar_log_alteracao(substr(:old.NM_INDICE,1,4000),substr(:new.NM_INDICE,1,4000),:new.nm_usuario,nr_seq_w,'NM_INDICE',ie_log_w,ds_w,'INDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_INDICE,1,4000),substr(:new.DS_INDICE,1,4000),:new.nm_usuario,nr_seq_w,'DS_INDICE',ie_log_w,ds_w,'INDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'INDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'INDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CRIAR_ALTERAR,1,4000),substr(:new.IE_CRIAR_ALTERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CRIAR_ALTERAR',ie_log_w,ds_w,'INDICE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CRIACAO,1,4000),substr(:new.DT_CRIACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CRIACAO',ie_log_w,ds_w,'INDICE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.indice_before_insert
before insert ON TASY.INDICE for each row
DECLARE
ds_user_w varchar2(100);
BEGIN
select max(USERNAME)
 into ds_user_w
 from v$session
 where  audsid = (select userenv('sessionid') from dual);
if (ds_user_w = 'TASY') then

if	(:new.ie_classificacao is null) then

	if	(:new.ie_tipo in ('PK','UK')) or
		(:new.nm_indice like '%_FK') or
		(:new.nm_indice like '%_FK%_I%') then

		:new.ie_classificacao := 'IR';

	elsif	(:new.ie_tipo = 'IU') or
		(:new.nm_indice like '%_I') or
		(:new.nm_indice like '%_I_') or
		(:new.nm_indice like '%_I__') or
		((:new.ie_tipo = 'I') and (:new.nm_indice like '%_UK')) then

		:new.ie_classificacao := 'IS';

	end	if;

end	if;
end	if;
end;
/


ALTER TABLE TASY.INDICE ADD (
  CONSTRAINT INDICE_PK
 PRIMARY KEY
 (NM_TABELA, NM_INDICE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INDICE ADD (
  CONSTRAINT INDICE_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INDICE TO NIVEL_1;

