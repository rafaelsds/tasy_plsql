ALTER TABLE TASY.NUT_RECEITA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_RECEITA CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_RECEITA
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  DS_RECEITA          VARCHAR2(255 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  QT_REFEICAO         NUMBER(15)                NOT NULL,
  IE_SITUACAO         VARCHAR2(1 BYTE)          NOT NULL,
  QT_G_PESSOA         NUMBER(5)                 NOT NULL,
  DS_PREPARO          VARCHAR2(4000 BYTE),
  NR_SEQ_GRUPO        NUMBER(10),
  NR_SEQ_COMPOSICAO   NUMBER(10),
  QT_RENDIMENTO       NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUTRECE_ESTABEL_FK_I ON TASY.NUT_RECEITA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NUTRECE_NUTCOMP_FK_I ON TASY.NUT_RECEITA
(NR_SEQ_COMPOSICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTRECE_NUTCOMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTRECE_NUTGRRE_FK_I ON TASY.NUT_RECEITA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTRECE_NUTGRRE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NUTRECE_PK ON TASY.NUT_RECEITA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NUT_RECEITA ADD (
  CONSTRAINT NUTRECE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_RECEITA ADD (
  CONSTRAINT NUTRECE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT NUTRECE_NUTCOMP_FK 
 FOREIGN KEY (NR_SEQ_COMPOSICAO) 
 REFERENCES TASY.NUT_COMPOSICAO (NR_SEQUENCIA),
  CONSTRAINT NUTRECE_NUTGRRE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.NUT_GRUPO_RECEITA (NR_SEQUENCIA));

GRANT SELECT ON TASY.NUT_RECEITA TO NIVEL_1;

