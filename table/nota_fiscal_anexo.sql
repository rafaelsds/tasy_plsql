ALTER TABLE TASY.NOTA_FISCAL_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_NOTA          NUMBER(10)               NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  DS_ARQUIVO_BANCO     LONG RAW,
  DS_ARQUIVO_HTML      VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTFIAN_NOTFISC_FK_I ON TASY.NOTA_FISCAL_ANEXO
(NR_SEQ_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIAN_NOTFISC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NOTFIAN_PK ON TASY.NOTA_FISCAL_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_anexo_insert
before insert ON TASY.NOTA_FISCAL_ANEXO for each row
begin

if	(:new.ds_arquivo_html is null) then
	begin
		:new.ds_arquivo_html := :new.DS_ARQUIVO;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_nota_fiscal_anexo_insupd
before insert or update or delete ON TASY.NOTA_FISCAL_ANEXO for each row
declare

ds_arquivo_w varchar2(4000);
nr_atendimento_w number(10);
qt_reg_w number(10);
nr_nota_fiscal_w varchar2(255);
nr_seq_anexo_nf_w number(10);
nr_seq_ged_atend_w number(10);
qt_oc_w number(10);
begin
       

    ds_arquivo_w:= nvl(:new.ds_arquivo, :old.ds_arquivo);
      

    if inserting or updating then        
       
        select count(*)
        into qt_oc_w
   
        from nota_fiscal b
        join ordem_compra c on(c.nr_ordem_compra = b.nr_ordem_compra)
        where c.nr_atendimento is not null
        and b.nr_sequencia = nvl(:new.nr_seq_nota,:old.nr_seq_nota);--1178588
               
        if qt_oc_w > 0 then
        
           select  nr_atendimento,
                   nr_nota_fiscal
            into    nr_atendimento_w,
                    nr_nota_fiscal_w
            from nota_fiscal b
            join ordem_compra c on(c.nr_ordem_compra = b.nr_ordem_compra)
            where c.nr_atendimento is not null
            and b.nr_sequencia = nvl(:new.nr_seq_nota,:old.nr_seq_nota);--1178588
            
        end if;
   
    
        if nr_atendimento_w is not null and nr_atendimento_w > 0 then
            
            select count(*)
            into qt_reg_w
            from ged_atendimento
            where nr_atendimento = nr_atendimento_w
            and ds_arquivo = ds_arquivo_w;
                
                
            if nr_atendimento_w is not null and nvl(qt_reg_w,0)=0 then
                
                nr_seq_anexo_nf_w:= ged_atendimento_seq.nextval;
                
                insert into ged_atendimento
                (
                    nr_sequencia,
                    nr_atendimento,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    cd_profissional,
                    dt_registro,
                    dt_liberacao,
                    ds_arquivo,
                    cd_pessoa_fisica,
                    ie_situacao,
                    nr_seq_tipo_arquivo,
                    ds_titulo
                )
                values
                (
                    nr_seq_anexo_nf_w,
                    nr_atendimento_w,
                    sysdate,
                    obter_usuario_ativo,
                    sysdate,
                    obter_usuario_ativo,
                    obter_pf_usuario(obter_usuario_ativo,'C'),
                    sysdate,
                    sysdate,
                    ds_arquivo_w,
                    obter_dados_atendimento(nr_atendimento_w, 'CP'),
                    'A',
                    1,
                    'Anexo da NF '||nr_nota_fiscal_w
                );
                    
                    
                insert into hsj_ged_atend_anexo_nf
                (
                    nr_seq_nota_anexo,
                    nr_seq_ged_atend,
                    ds_arquivo
                )
                values
                (
                    nvl(:new.nr_sequencia, :old.nr_sequencia),
                    nr_seq_anexo_nf_w,
                    ds_arquivo_w
                );
                                
                    
            end if;
                
        end if;
            
    elsif deleting then
        
        select max(nr_seq_ged_atend)
        into nr_seq_ged_atend_w
        from hsj_ged_atend_anexo_nf
        where nr_seq_nota_anexo = :old.nr_sequencia
        and ds_arquivo = ds_arquivo_w;
        
        update ged_atendimento set ie_situacao = 'I', dt_inativacao = sysdate, nm_usuario_inativacao = obter_usuario_ativo
        where nr_sequencia = nr_seq_ged_atend_w
        and ds_arquivo = ds_arquivo_w;
        
        
    end if;


end hsj_nota_fiscal_anexo_insupd;
/


ALTER TABLE TASY.NOTA_FISCAL_ANEXO ADD (
  CONSTRAINT NOTFIAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_ANEXO ADD (
  CONSTRAINT NOTFIAN_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.NOTA_FISCAL_ANEXO TO NIVEL_1;

