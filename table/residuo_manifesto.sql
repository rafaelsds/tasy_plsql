ALTER TABLE TASY.RESIDUO_MANIFESTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESIDUO_MANIFESTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESIDUO_MANIFESTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_CGC_DESTINO           VARCHAR2(14 BYTE),
  CD_CGC_TRANSPORTADORA    VARCHAR2(14 BYTE),
  CD_CLASSIFICACAO         NUMBER(10),
  NR_MANIFESTO             NUMBER(10),
  DT_REFERENCIA            DATE,
  DS_PRINCIPIO_ATIVO       VARCHAR2(100 BYTE),
  NR_SEQ_NOME_COMERCIAL    NUMBER(10),
  NR_SEQ_ESTADO_FISICO     NUMBER(10),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NR_SEQ_ACONDICIONAMENTO  NUMBER(10),
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RESMAN_PESJURI_FK_I ON TASY.RESIDUO_MANIFESTO
(CD_CGC_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESMAN_PESJURI_FK2_I ON TASY.RESIDUO_MANIFESTO
(CD_CGC_TRANSPORTADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RESMAN_PK ON TASY.RESIDUO_MANIFESTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESMAN_RESACOND_FK_I ON TASY.RESIDUO_MANIFESTO
(NR_SEQ_ACONDICIONAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESMAN_RESESTFIS_FK_I ON TASY.RESIDUO_MANIFESTO
(NR_SEQ_ESTADO_FISICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESMAN_RESIDUO_FK_I ON TASY.RESIDUO_MANIFESTO
(CD_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RESMAN_RESNMCOM_FK_I ON TASY.RESIDUO_MANIFESTO
(NR_SEQ_NOME_COMERCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RESIDUO_MANIFESTO ADD (
  CONSTRAINT RESMAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESIDUO_MANIFESTO ADD (
  CONSTRAINT RESMAN_PESJURI_FK 
 FOREIGN KEY (CD_CGC_DESTINO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT RESMAN_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_TRANSPORTADORA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT RESMAN_RESACOND_FK 
 FOREIGN KEY (NR_SEQ_ACONDICIONAMENTO) 
 REFERENCES TASY.RESIDUO_ACONDICIONAMENTO (NR_SEQUENCIA),
  CONSTRAINT RESMAN_RESESTFIS_FK 
 FOREIGN KEY (NR_SEQ_ESTADO_FISICO) 
 REFERENCES TASY.RESIDUO_ESTADO_FISICO (NR_SEQUENCIA),
  CONSTRAINT RESMAN_RESIDUO_FK 
 FOREIGN KEY (CD_CLASSIFICACAO) 
 REFERENCES TASY.RESIDUO (NR_SEQUENCIA),
  CONSTRAINT RESMAN_RESNMCOM_FK 
 FOREIGN KEY (NR_SEQ_NOME_COMERCIAL) 
 REFERENCES TASY.RESIDUO_NOME_COMERCIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.RESIDUO_MANIFESTO TO NIVEL_1;

