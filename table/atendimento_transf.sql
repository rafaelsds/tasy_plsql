ALTER TABLE TASY.ATENDIMENTO_TRANSF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_TRANSF CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_TRANSF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  NR_SEQ_MOTIVO        NUMBER(10)               NOT NULL,
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_CGC_TRANSPORTE    VARCHAR2(14 BYTE),
  CD_MEDICO_RESP       VARCHAR2(10 BYTE),
  DT_TRANSFERENCIA     DATE,
  NM_MEDICO_EXTERNO    VARCHAR2(60 BYTE),
  CRM_MEDICO_CONTATO   VARCHAR2(60 BYTE),
  DS_JUSTIFICATIVA     VARCHAR2(2000 BYTE),
  DS_HISTORIA_CLINICA  VARCHAR2(2000 BYTE),
  CD_ESPECIALIDADE     NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATETRAN_ATEPACI_FK_I ON TASY.ATENDIMENTO_TRANSF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATETRAN_ESPMEDI_FK_I ON TASY.ATENDIMENTO_TRANSF
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATETRAN_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATETRAN_MOTTRPA_FK_I ON TASY.ATENDIMENTO_TRANSF
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATETRAN_MOTTRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATETRAN_PESFISI_FK1_I ON TASY.ATENDIMENTO_TRANSF
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATETRAN_PESJURI_FK_I ON TASY.ATENDIMENTO_TRANSF
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATETRAN_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATETRAN_PESJURI_FK2_I ON TASY.ATENDIMENTO_TRANSF
(CD_CGC_TRANSPORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATETRAN_PESJURI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATETRAN_PK ON TASY.ATENDIMENTO_TRANSF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_TRANSF_tp  after update ON TASY.ATENDIMENTO_TRANSF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MEDICO_RESP,1,2000),substr(:new.CD_MEDICO_RESP,1,2000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_RESP',ie_log_w,ds_w,'ATENDIMENTO_TRANSF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_transf_after
after insert or update ON TASY.ATENDIMENTO_TRANSF for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_tipo_atendimento_p		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_tipo_admissao_fat_p	atendimento_paciente.nr_seq_tipo_admissao_fat%type;
dt_alta_p			atendimento_paciente.dt_alta%type;
cd_motivo_alta_p		atendimento_paciente.cd_motivo_alta%type;
cd_pessoa_fisica_p		atendimento_paciente.cd_pessoa_fisica%type;
dt_cancelamento_w		date;

begin
begin
select	dt_alta,
	ie_tipo_atendimento,
	nr_seq_tipo_admissao_fat,
	cd_pessoa_fisica,
	cd_motivo_alta,
	dt_cancelamento
into	dt_alta_p,
	ie_tipo_atendimento_p,
	nr_seq_tipo_admissao_fat_p,
	cd_pessoa_fisica_p,
	cd_motivo_alta_p,
	dt_cancelamento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;
exception
when others then
	dt_alta_p			:= null;
	ie_tipo_atendimento_p		:= null;
	nr_seq_tipo_admissao_fat_p	:= null;
	cd_pessoa_fisica_p		:= null;
end;

if (dt_cancelamento_w is null) then
	if  (inserting) and (:new.ie_situacao = 'A') then
    	reg_integracao_p.ie_operacao        :=  'I';
	elsif   (updating) and (:new.ie_situacao = 'A') then
    	reg_integracao_p.ie_operacao        :=  'A';
	end if;

	reg_integracao_p.nr_atendimento			:=	:new.nr_atendimento;
	reg_integracao_p.cd_pessoa_fisica		:=	cd_pessoa_fisica_p;
	reg_integracao_p.ie_tipo_atendimento		:= 	ie_tipo_atendimento_p;
	reg_integracao_p.nr_seq_tipo_admissao_fat	:= 	nr_seq_tipo_admissao_fat_p;
	reg_integracao_p.cd_motivo_alta			:= 	cd_motivo_alta_p;

	gerar_int_padrao.gravar_integracao('114', :new.nr_atendimento, nvl(obter_usuario_ativo, :new.nm_usuario), reg_integracao_p);
end if;

end atendimento_transf_after;
/


ALTER TABLE TASY.ATENDIMENTO_TRANSF ADD (
  CONSTRAINT ATETRAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_TRANSF ADD (
  CONSTRAINT ATETRAN_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT ATETRAN_MOTTRPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_TRANSF_PAC (NR_SEQUENCIA),
  CONSTRAINT ATETRAN_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_TRANSPORTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ATETRAN_PESFISI_FK1 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATETRAN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATETRAN_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.ATENDIMENTO_TRANSF TO NIVEL_1;

