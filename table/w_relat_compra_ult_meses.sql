DROP TABLE TASY.W_RELAT_COMPRA_ULT_MESES CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RELAT_COMPRA_ULT_MESES
(
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  VL_TOTAL_ITEM_NF  NUMBER(13,4),
  DT_REFERENCIA     DATE,
  QT_MES            NUMBER(10),
  CD_MATERIAL       NUMBER(10)                  NOT NULL,
  QT_ITEM_NF        NUMBER(13,4),
  CD_CGC_EMITENTE   VARCHAR2(14 BYTE),
  NR_SEQ_MARCA      NUMBER(10),
  VL_MEDIA_MES      NUMBER(13,4),
  QT_NOTA_FISCAL    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WRECOUM_MATERIA_FK_I ON TASY.W_RELAT_COMPRA_ULT_MESES
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRECOUM_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WRECOUM_USUARIO_FK_I ON TASY.W_RELAT_COMPRA_ULT_MESES
(NM_USUARIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRECOUM_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_RELAT_COMPRA_ULT_MESES ADD (
  CONSTRAINT WRECOUM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_RELAT_COMPRA_ULT_MESES TO NIVEL_1;

