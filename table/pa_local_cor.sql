ALTER TABLE TASY.PA_LOCAL_COR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PA_LOCAL_COR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PA_LOCAL_COR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_COR               VARCHAR2(15 BYTE),
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL,
  QT_TEMPO_INICIAL     NUMBER(3)                NOT NULL,
  QT_TEMPO_FINAL       NUMBER(3)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PALOCOR_PALOCAL_FK_I ON TASY.PA_LOCAL_COR
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PALOCOR_PALOCAL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PALOCOR_PK ON TASY.PA_LOCAL_COR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PALOCOR_PK
  MONITORING USAGE;


ALTER TABLE TASY.PA_LOCAL_COR ADD (
  CONSTRAINT PALOCOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PA_LOCAL_COR ADD (
  CONSTRAINT PALOCOR_PALOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.PA_LOCAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PA_LOCAL_COR TO NIVEL_1;

