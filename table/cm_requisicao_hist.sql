ALTER TABLE TASY.CM_REQUISICAO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_REQUISICAO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_REQUISICAO_HIST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_REQUISICAO          NUMBER(10)             NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_LIBERACAO           DATE,
  DS_HISTORICO           LONG                   NOT NULL,
  NR_SEQ_TIPO_HISTORICO  NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_USUARIO_LIBERACAO   VARCHAR2(15 BYTE),
  NM_USUARIO_LIB         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_AUX     DATE                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMREQHIST_CMETIPHIST_FK_I ON TASY.CM_REQUISICAO_HIST
(NR_SEQ_TIPO_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMREQHIST_CMREQUI_FK_I ON TASY.CM_REQUISICAO_HIST
(NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMREQHIST_PESFISI_FK_I ON TASY.CM_REQUISICAO_HIST
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CMREQHIST_PK ON TASY.CM_REQUISICAO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CM_REQUISICAO_HIST ADD (
  CONSTRAINT CMREQHIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_REQUISICAO_HIST ADD (
  CONSTRAINT CMREQHIST_CMETIPHIST_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HISTORICO) 
 REFERENCES TASY.CME_TIPO_HISTORICO (NR_SEQUENCIA),
  CONSTRAINT CMREQHIST_CMREQUI_FK 
 FOREIGN KEY (NR_REQUISICAO) 
 REFERENCES TASY.CM_REQUISICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMREQHIST_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CM_REQUISICAO_HIST TO NIVEL_1;

