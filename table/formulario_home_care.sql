ALTER TABLE TASY.FORMULARIO_HOME_CARE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FORMULARIO_HOME_CARE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FORMULARIO_HOME_CARE
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  NR_ATENDIMENTO                  NUMBER(10)    NOT NULL,
  IE_SITUACAO                     VARCHAR2(1 BYTE) NOT NULL,
  CD_CID_DOENCA                   VARCHAR2(10 BYTE),
  DT_LIBERACAO                    DATE,
  DT_INATIVACAO                   DATE,
  NM_USUARIO_INATIVACAO           VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA                VARCHAR2(255 BYTE),
  CD_CID_DOENCA2                  VARCHAR2(10 BYTE),
  CD_CID_DOENCA3                  VARCHAR2(10 BYTE),
  CD_CID_DOENCA4                  VARCHAR2(10 BYTE),
  DS_DISPENSACAO_MEDIC            VARCHAR2(2000 BYTE),
  DS_GRAU_DECUBITO                VARCHAR2(2000 BYTE),
  DS_INSTRUCOES                   VARCHAR2(2000 BYTE),
  DS_LOCALIZACAO_CURATIVOS        VARCHAR2(2000 BYTE),
  DS_LOCALIZACAO_DECUBITO         VARCHAR2(2000 BYTE),
  DS_MEDIDA_TRAT                  VARCHAR2(2000 BYTE),
  DS_MEDIDA_TRAT2                 VARCHAR2(2000 BYTE),
  DS_OBSERVACAO                   VARCHAR2(2000 BYTE),
  DS_RESTRICOES                   VARCHAR2(2000 BYTE),
  DS_TAMANHO_CURATIVOS            VARCHAR2(2000 BYTE),
  DS_TAMANHO_DECUBITO             VARCHAR2(2000 BYTE),
  DS_TIPO_BAND_SUPORTE            VARCHAR2(2000 BYTE),
  DS_TRATAMENTO_FERIDA            VARCHAR2(2000 BYTE),
  DT_ATE_BASICO                   DATE,
  DT_ATE_HC                       DATE,
  DT_ATE_APLIC                    DATE,
  DT_ATE_BANDAGEM                 DATE,
  DT_ATE_CAIXA                    DATE,
  DT_ATE_CURATIVOS                DATE,
  DT_ATE_DECUBITO                 DATE,
  DT_ATE_DISP                     DATE,
  DT_ATE_GLICOSE                  DATE,
  DT_ATE_MEIA                     DATE,
  DT_ATE_SUPORTE                  DATE,
  DT_ATE_TRAT                     DATE,
  DT_ATE_TRAT2                    DATE,
  DT_DE_BASICO                    DATE,
  DT_DE_HC                        DATE,
  DT_DE_APLIC                     DATE,
  DT_DE_BANDAGEM                  DATE,
  DT_DE_CAIXA                     DATE,
  DT_DE_CURATIVOS                 DATE,
  DT_DE_DECUBITO                  DATE,
  DT_DE_DISP                      DATE,
  DT_DE_GLICOSE                   DATE,
  DT_DE_MEIA                      DATE,
  DT_DE_SUPORTE                   DATE,
  DT_DE_TRAT                      DATE,
  DT_DE_TRAT2                     DATE,
  DT_FIM                          DATE,
  DT_INICIO                       DATE,
  IE_ACIDENTE                     VARCHAR2(1 BYTE),
  IE_APOS_INTERNACAO              VARCHAR2(1 BYTE),
  IE_APOS_TRATAMENTO              VARCHAR2(1 BYTE),
  IE_BANDAGEM_SUPORTE             VARCHAR2(1 BYTE),
  IE_COLOCAR_BANDAGEM             VARCHAR2(1 BYTE),
  IE_COLOCAR_MEIA                 VARCHAR2(1 BYTE),
  IE_CONTROLE_GLICOSE             VARCHAR2(1 BYTE),
  IE_DISPENSACAO                  VARCHAR2(1 BYTE),
  IE_FREQUENCIA_BASICO            VARCHAR2(1 BYTE),
  IE_FREQUENCIA_HC                VARCHAR2(1 BYTE),
  IE_FREQUENCIA_APLIC             VARCHAR2(1 BYTE),
  IE_FREQUENCIA_BANDAGEM          VARCHAR2(1 BYTE),
  IE_FREQUENCIA_CAIXA             VARCHAR2(1 BYTE),
  IE_FREQUENCIA_CURATIVOS         VARCHAR2(1 BYTE),
  IE_FREQUENCIA_DECUBITO          VARCHAR2(1 BYTE),
  IE_FREQUENCIA_DISP              VARCHAR2(1 BYTE),
  IE_FREQUENCIA_GLICOSE           VARCHAR2(1 BYTE),
  IE_FREQUENCIA_MEIA              VARCHAR2(1 BYTE),
  IE_FREQUENCIA_SUPORTE           VARCHAR2(1 BYTE),
  IE_FREQUENCIA_TRAT              VARCHAR2(1 BYTE),
  IE_FREQUENCIA_TRAT2             VARCHAR2(1 BYTE),
  IE_HOME_CARE                    VARCHAR2(1 BYTE),
  IE_INSULINA_INTENSIVA           VARCHAR2(1 BYTE),
  IE_LADO                         VARCHAR2(1 BYTE),
  IE_OUTROS_CURATIVOS             VARCHAR2(1 BYTE),
  IE_PREPARO_CAIXA                VARCHAR2(1 BYTE),
  IE_PRESCR_RETORNO               VARCHAR2(1 BYTE),
  IE_PRIMEIRA_PRESCR              VARCHAR2(1 BYTE),
  IE_RETIRAR_BANDAGEM             VARCHAR2(1 BYTE),
  IE_RETIRAR_MEIA                 VARCHAR2(1 BYTE),
  IE_TRATAMENTO_DECUBITO          VARCHAR2(1 BYTE),
  IE_TRATAMENTO_NORMAL            VARCHAR2(1 BYTE),
  IE_VIA_APLICACAO                VARCHAR2(1 BYTE),
  QT_INSTRUCOES                   NUMBER(10),
  QT_SEMANAL                      NUMBER(2),
  QT_DIARIO                       NUMBER(2),
  QT_CUIDADO_SEMANAL              NUMBER(2),
  QT_CUIDADO_MENSAL               NUMBER(2),
  QT_CUIDADO_DIARIO               NUMBER(2),
  QT_MENSAL                       NUMBER(2),
  IE_FREQUENCIA_CUIDADO_DIARIO    VARCHAR2(1 BYTE),
  IE_FREQUENCIA_CUIDADO           VARCHAR2(1 BYTE),
  QT_FREQUENCIA_APLIC             NUMBER(10),
  QT_FREQUENCIA_BANDAGEM          NUMBER(10),
  QT_FREQUENCIA_BASICO            NUMBER(10),
  QT_FREQUENCIA_CAIXA             NUMBER(10),
  QT_FREQUENCIA_CUIDADO           NUMBER(10),
  QT_FREQUENCIA_CUIDADO_DIARIO    NUMBER(10),
  QT_FREQUENCIA_CURATIVOS         NUMBER(10),
  QT_FREQUENCIA_DECUBITO          NUMBER(10),
  QT_FREQUENCIA_DISP              NUMBER(10),
  QT_FREQUENCIA_GLICOSE           NUMBER(10),
  QT_FREQUENCIA_HC                NUMBER(10),
  QT_FREQUENCIA_MEIA              NUMBER(10),
  QT_FREQUENCIA_SUPORTE           NUMBER(10),
  QT_FREQUENCIA_TRAT              NUMBER(10),
  QT_FREQUENCIA_TRAT2             NUMBER(10),
  CD_PESSOA_FISICA                VARCHAR2(10 BYTE),
  CD_MEDICO                       VARCHAR2(10 BYTE),
  NR_RQE                          VARCHAR2(20 BYTE),
  NR_BSNR                         VARCHAR2(25 BYTE),
  DS_FERIDA                       VARCHAR2(255 BYTE),
  IE_FERIDA_CRONICA               VARCHAR2(1 BYTE),
  QT_FREQ_MEIA_MENSAL             NUMBER(10),
  QT_FREQ_MEIA_SEMANAL            NUMBER(10),
  QT_FREQ_SUPORTE_MENSAL          NUMBER(10),
  QT_FREQ_SUPORTE_SEMANAL         NUMBER(10),
  QT_FREQ_BANDAGEM_MENSAL         NUMBER(10),
  QT_FREQ_BANDAGEM_SEMANAL        NUMBER(10),
  QT_FREQ_CUIDADO_MENSAL          NUMBER(10),
  QT_FREQ_CUIDADO_SEMANAL         NUMBER(10),
  QT_FREQ_CUIDADO_DIARIO_MENSAL   NUMBER(10),
  QT_FREQ_CUIDADO_DIARIO_SEMANAL  NUMBER(10),
  QT_FREQ_TRAT2_MENSAL            NUMBER(10),
  QT_FREQ_TRAT2_SEMANAL           NUMBER(10),
  QT_FREQ_TRAT_MENSAL             NUMBER(10),
  QT_FREQ_TRAT_SEMANAL            NUMBER(10),
  QT_FREQ_DECUBITO_MENSAL         NUMBER(10),
  QT_FREQ_CAIXA_SEMANAL           NUMBER(10),
  QT_FREQ_CAIXA_MENSAL            NUMBER(10),
  QT_FREQ_DISP_SEMANAL            NUMBER(10),
  QT_FREQ_DISP_MENSAL             NUMBER(10),
  QT_FREQ_APLIC_SEMANAL           NUMBER(10),
  QT_FREQ_APLIC_MENSAL            NUMBER(10),
  QT_FREQ_GLICOSE_SEMANAL         NUMBER(10),
  QT_FREQ_GLICOSE_MENSAL          NUMBER(10),
  QT_FREQ_CURATIVOS_SEMANAL       NUMBER(10),
  QT_FREQ_CURATIVOS_MENSAL        NUMBER(10),
  QT_FREQ_DECUBITO_SEMANAL        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FORHOCA_ATEPACI_FK_I ON TASY.FORMULARIO_HOME_CARE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORHOCA_PESFISI_FK_I ON TASY.FORMULARIO_HOME_CARE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FORHOCA_PK ON TASY.FORMULARIO_HOME_CARE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORHOCA_PROFISS_FK_I ON TASY.FORMULARIO_HOME_CARE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FORMULARIO_HOME_CARE_UPDATE
before update ON TASY.FORMULARIO_HOME_CARE for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into 	is_rule_exists_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV12'
    and   	obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV12'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
	end if;
  end if;
end;
/


ALTER TABLE TASY.FORMULARIO_HOME_CARE ADD (
  CONSTRAINT FORHOCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FORMULARIO_HOME_CARE ADD (
  CONSTRAINT FORHOCA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FORHOCA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FORHOCA_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.FORMULARIO_HOME_CARE TO NIVEL_1;

