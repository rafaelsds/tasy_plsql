ALTER TABLE TASY.MPREV_PESSOA_CUBO_OPS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_PESSOA_CUBO_OPS CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_PESSOA_CUBO_OPS
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  CD_PESSOA_FISICA  VARCHAR2(10 BYTE),
  NM_PESSOA_FISICA  VARCHAR2(255 BYTE)          NOT NULL,
  DT_NASCIMENTO     DATE                        NOT NULL,
  IE_SEXO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MPRPCUO_PK ON TASY.MPREV_PESSOA_CUBO_OPS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRPCUO_PK
  MONITORING USAGE;


ALTER TABLE TASY.MPREV_PESSOA_CUBO_OPS ADD (
  CONSTRAINT MPRPCUO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MPREV_PESSOA_CUBO_OPS TO NIVEL_1;

