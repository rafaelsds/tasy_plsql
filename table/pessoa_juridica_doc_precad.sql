ALTER TABLE TASY.PESSOA_JURIDICA_DOC_PRECAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JURIDICA_DOC_PRECAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JURIDICA_DOC_PRECAD
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_PES_JUR_PRECAD       NUMBER(10)        NOT NULL,
  DT_REVISAO                  DATE,
  DT_EMISSAO                  DATE,
  NR_SEQ_TIPO_DOCTO           NUMBER(10)        NOT NULL,
  CD_PESSOA_RESPONSAVEL       VARCHAR2(10 BYTE),
  QT_TEMPO_VALIDADE           NUMBER(5),
  IE_COMUNIC_RESP_DOCUMENTO   VARCHAR2(1 BYTE)  NOT NULL,
  IE_COMUNIC_COMPRADOR        VARCHAR2(1 BYTE)  NOT NULL,
  IE_COMUNIC_TODOS_COMPRADOR  VARCHAR2(1 BYTE)  NOT NULL,
  IE_COMUNIC_SETOR            VARCHAR2(1 BYTE)  NOT NULL,
  IE_CONSISTE_COTACAO         VARCHAR2(1 BYTE)  NOT NULL,
  IE_DATA_INDETERMINADA       VARCHAR2(1 BYTE),
  QT_DIAS_AVISO_VENCTO        NUMBER(3),
  CD_SETOR_RESPONSAVEL        NUMBER(5),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  NR_REGISTRO                 VARCHAR2(20 BYTE),
  IE_COMUNIC_REGRA_ENVIO      VARCHAR2(15 BYTE),
  CD_PESSOA_DOCUMENTO         VARCHAR2(15 BYTE),
  DS_PF_RESPONSAVEL           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PJDOCPRE_ESTABEL_FK_I ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJDOCPRE_PESFISI_FK_I ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(CD_PESSOA_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJDOCPRE_PESFISI_FK_1_I ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(CD_PESSOA_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJDOCPRE_PJPRCAD_FK_I ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(NR_SEQ_PES_JUR_PRECAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PJDOCPRE_PK ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJDOCPRE_SETATEN_FK_I ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(CD_SETOR_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PJDOCPRE_TIDOFOR_FK_I ON TASY.PESSOA_JURIDICA_DOC_PRECAD
(NR_SEQ_TIPO_DOCTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PESSOA_JURIDICA_DOC_PRECAD ADD (
  CONSTRAINT PJDOCPRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PESSOA_JURIDICA_DOC_PRECAD ADD (
  CONSTRAINT PJDOCPRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PJDOCPRE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PJDOCPRE_PESFISI_FK_1 
 FOREIGN KEY (CD_PESSOA_DOCUMENTO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PJDOCPRE_PJPRCAD_FK 
 FOREIGN KEY (NR_SEQ_PES_JUR_PRECAD) 
 REFERENCES TASY.PESSOA_JURIDICA_PRECAD (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PJDOCPRE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_RESPONSAVEL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PJDOCPRE_TIDOFOR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCTO) 
 REFERENCES TASY.TIPO_DOCUMENTO_FORNEC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_JURIDICA_DOC_PRECAD TO NIVEL_1;

