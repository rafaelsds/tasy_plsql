ALTER TABLE TASY.PLS_GUIA_PLANO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GUIA_PLANO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GUIA_PLANO_MAT
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_GUIA                 NUMBER(10)        NOT NULL,
  CD_MATERIAL_IMP             VARCHAR2(15 BYTE),
  QT_SOLICITADA               NUMBER(9,3)       DEFAULT null,
  DS_MATERIAL_IMP             VARCHAR2(255 BYTE),
  QT_SOLICITADA_IMP           NUMBER(9,3)       DEFAULT null,
  QT_AUTORIZADA               NUMBER(9,3)       DEFAULT null,
  IE_STATUS                   VARCHAR2(2 BYTE),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIBERACAO        VARCHAR2(15 BYTE),
  NR_SEQ_PRORROG              NUMBER(10),
  CD_TIPO_TABELA_IMP          VARCHAR2(20 BYTE),
  NR_SEQ_MOTIVO_EXC           NUMBER(10),
  NR_SEQ_MATERIAL             NUMBER(10),
  IE_UTILIZADO                VARCHAR2(2 BYTE),
  QT_UTILIZADO                NUMBER(5),
  NR_SEQ_MAT_PRINC            NUMBER(10),
  NR_SEQ_REGRA_LIBERACAO      NUMBER(10),
  IE_TIPO_COBERTURA           VARCHAR2(1 BYTE),
  NR_SEQ_COBERTURA            NUMBER(10),
  NR_SEQ_SCA_COBERTURA        NUMBER(10),
  NR_SEQ_TIPO_LIMITACAO       NUMBER(10),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  NR_SEQ_PREST_FORNEC         NUMBER(10),
  VL_MATERIAL                 NUMBER(15,2),
  NR_SEQ_REGRA                NUMBER(10),
  NR_SEQ_COMPL_PTU            NUMBER(10),
  CD_MATERIAL                 NUMBER(10),
  NR_SEQ_MAT_AUTOR            NUMBER(10),
  CD_MEDICO_EXECUTOR          VARCHAR2(10 BYTE),
  IE_COBRANCA_PREVIA_SERVICO  VARCHAR2(1 BYTE),
  IE_ORIGEM_INCLUSAO          VARCHAR2(2 BYTE),
  VL_MATERIAL_CONTA           NUMBER(15,2),
  IE_EXIGE_VALOR_CONTA        VARCHAR2(1 BYTE),
  NR_SEQ_MATERIAL_FORN        NUMBER(10),
  NR_SEQ_PROC_PRINC           NUMBER(10),
  IE_TIPO_ANEXO               VARCHAR2(2 BYTE),
  CD_MATERIAL_PTU             NUMBER(10),
  DS_MATERIAL_PTU             VARCHAR2(80 BYTE),
  NR_SEQ_PRECO_PACOTE         NUMBER(10),
  NR_SEQ_TIPO_CARENCIA        NUMBER(10),
  IE_COBRANCA_PREVISTA        VARCHAR2(1 BYTE),
  VL_ORIGINAL                 NUMBER(15,2),
  NR_REGISTRO_ANVISA          VARCHAR2(15 BYTE),
  CD_REF_FABRICANTE_IMP       VARCHAR2(60 BYTE),
  IE_PACOTE_PTU               VARCHAR2(2 BYTE),
  NR_SEQ_ITEM_TISS            NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLGUIMAT_I ON TASY.PLS_GUIA_PLANO_MAT
(DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLGUIMAT_I
  MONITORING USAGE;


CREATE INDEX TASY.PLGUIMAT_I2 ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQ_GUIA, NR_SEQ_MATERIAL, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUIMAT_MAT_AUTOR_I ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQ_MAT_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUIMAT_MATERIA_FK_I ON TASY.PLS_GUIA_PLANO_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLGUIMAT_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLGUIMAT_PESFISI_FK_I ON TASY.PLS_GUIA_PLANO_MAT
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLGUIMAT_PK ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUIMAT_PLSFORN_FK_I ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQ_MATERIAL_FORN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLGUIMAT_PLSFORN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLGUIMAT_PLSGUIA_FK_I ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLGUIMAT_PLSMAT_FK_I ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQ_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLGUIMAT_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLGUIMAT_PLSPRES_FK_I ON TASY.PLS_GUIA_PLANO_MAT
(NR_SEQ_PREST_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLGUIMAT_PLSPRES_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_GUIA_PLANO_MAT_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_GUIA_PLANO_MAT FOR EACH ROW
declare

cd_material_w	number(6);
ie_erro_w	varchar2(1)	:= 'N';

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		begin
		select	cd_material
		into	cd_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;
		exception
			when others then
			ie_erro_w	:= 'S';
		end;

		if	(ie_erro_w = 'N') then
			:new.cd_material	:= cd_material_w;
		end if;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.pls_alt_autor_mat_log_bd
before update or delete ON TASY.PLS_GUIA_PLANO_MAT for each row
declare

nm_machine_w		varchar2(255);
ie_tipo_alteracao_w	varchar2(1)	:= null;
ds_log_w		varchar2(2000)	:= null;
qt_rotina_w		number(10)	:= null;

begin

if	(pls_se_aplicacao_tasy = 'N') then
	begin
	SELECT	machine
	INTO	nm_machine_w
	FROM	v$session
	WHERE	audsid = USERENV('sessionid');
	exception
		when others then
		nm_machine_w	:= null;
	end;

	/* Verificação de SCS */
	select	count(1)
	into	qt_rotina_w
	from 	v$session
	where	audsid	= (select userenv('sessionid') from dual)
	and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	and	action like 'TASY_SCS%';

	if	(qt_rotina_w = 0) then
		if	(updating) then
			if	(nvl(:new.qt_solicitada,0) <>  nvl(:old.qt_solicitada,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'qt_solicitada_old= ' || :old.qt_solicitada || ' qt_solicitada_new= ' || :new.qt_solicitada;
			end if;

			if	(nvl(:new.qt_autorizada,0) <>  nvl(:old.qt_autorizada,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'qt_autorizada_old= ' || :old.qt_autorizada || ' qt_autorizada_new= ' || :new.qt_autorizada;
			end if;

			if	(nvl(:new.ie_status,0) <>  nvl(:old.ie_status,0)) then
				ds_log_w	:= ds_log_w || chr(13) || chr(10) || 'ie_status_old= ' || :old.ie_status || ' ie_estagio_new= ' || :new.ie_status;
			end if;

			if	(ds_log_w is not null) then
				ie_tipo_alteracao_w	:= 'A';
			end if;
		elsif	(deleting) then
			ie_tipo_alteracao_w	:= 'E';

			ds_log_w	:= 'Material: ' || :new.nr_seq_material;
		end if;

		if	(ie_tipo_alteracao_w is not null) then
			insert into pls_log_alt_autor_bd
				(nr_sequencia,
				dt_alteracao,
				nr_seq_guia_plano,
				nr_seq_guia_plano_proc,
				nr_seq_guia_plano_mat,
				ie_tipo_alteracao,
				ds_log,
				nm_maquina)
			values	(pls_log_alt_autor_bd_seq.nextval,
				sysdate,
				:new.nr_seq_guia,
				null,
				:new.nr_sequencia,
				ie_tipo_alteracao_w,
				ds_log_w,
				nm_machine_w);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_bloqueia_insert_mat_estag
before insert ON TASY.PLS_GUIA_PLANO_MAT for each row
declare

ie_estagio_w	pls_guia_plano.ie_estagio%type;
ds_estagio_w	Varchar2(255);
qt_rotina_w	Number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
		select	ie_estagio
		into	ie_estagio_w
		from	pls_guia_plano
		where	nr_sequencia	= :new.nr_seq_guia;
	exception
	when others then
		ie_estagio_w	:= 0;
	end;

	select	count(1)
	into	qt_rotina_w
	from 	v$session
	where	audsid	= (select userenv('sessionid') from dual)
	and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	and	action like 'INCMATMED%';

	if	(ie_estagio_w	<> 7) and (qt_rotina_w = 0) then
		ds_estagio_w	:= obter_valor_dominio(2055,ie_estagio_w);
		wheb_mensagem_pck.exibir_mensagem_abort(255327, 'DS_ESTAGIO=' || ds_estagio_w);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_GUIA_PLANO_MAT ADD (
  CONSTRAINT PLGUIMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GUIA_PLANO_MAT ADD (
  CONSTRAINT PLGUIMAT_PLSFORN_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_FORN) 
 REFERENCES TASY.PLS_FORNECEDOR_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLGUIMAT_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLGUIMAT_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PREST_FORNEC) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLGUIMAT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PLGUIMAT_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLGUIMAT_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_GUIA_PLANO_MAT TO NIVEL_1;

