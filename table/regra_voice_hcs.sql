ALTER TABLE TASY.REGRA_VOICE_HCS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_VOICE_HCS CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_VOICE_HCS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_EVENTO            VARCHAR2(3 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGVOICHCS_ESTABEL_FK_I ON TASY.REGRA_VOICE_HCS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGVOICHCS_PK ON TASY.REGRA_VOICE_HCS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_VOICE_HCS_INSERT
before insert or update ON TASY.REGRA_VOICE_HCS for each row
declare
ie_exist_w	number(3);
pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if(:new.IE_SITUACAO = nvl(:old.IE_SITUACAO,:new.IE_SITUACAO)) then
		select count(*)
		into ie_exist_w
		from regra_voice_hcs
		where cd_estabelecimento = :new.cd_estabelecimento
		and ie_evento = :new.ie_evento;

			if(ie_exist_w > 0)  then
				exibir_erro_abortar(obter_desc_expressao(1038041)||' #@#@',null);
			end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.REGRA_VOICE_HCS ADD (
  CONSTRAINT REGVOICHCS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REGRA_VOICE_HCS ADD (
  CONSTRAINT REGVOICHCS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_VOICE_HCS TO NIVEL_1;

