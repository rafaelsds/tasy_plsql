ALTER TABLE TASY.TITULO_RECEBER_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_CLASSIF
(
  NR_TITULO          NUMBER(10)                 NOT NULL,
  NR_SEQUENCIA       NUMBER(5)                  NOT NULL,
  VL_CLASSIFICACAO   NUMBER(15,2)               NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  CD_CONTA_FINANC    NUMBER(10),
  CD_CENTRO_CUSTO    NUMBER(8),
  VL_DESCONTO        NUMBER(15,2),
  VL_ORIGINAL        NUMBER(15,2),
  CD_CONTA_CONTABIL  VARCHAR2(20 BYTE),
  NR_SEQ_CONTRATO    NUMBER(10),
  NR_SEQ_PRODUTO     NUMBER(10),
  CD_HISTORICO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITRECL_CENCUST_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECL_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECL_CONCONT_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECL_CONFINA_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECL_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECL_CONTRAT_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECL_HISPADR_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECL_HISPADR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITRECL_PK ON TASY.TITULO_RECEBER_CLASSIF
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECL_PRODFIN_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRECL_PRODFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRECL_TITREC_FK_I ON TASY.TITULO_RECEBER_CLASSIF
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cerdil_tit_rec_classif_insert
before insert or update ON TASY.TITULO_RECEBER_CLASSIF FOR EACH ROW
DECLARE

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(nvl(:old.cd_conta_financ,0) <> nvl(:new.cd_conta_financ,0)) and
	(:new.cd_conta_financ = 3) then

	Gerar_Comunic_Padrao(	sysdate,
				wheb_mensagem_pck.get_texto(304160),
				wheb_mensagem_pck.get_texto(304161, 'NR_TITULO=' || :new.nr_titulo),
				'Tasy',
				'N',
				'PBURIGO',
				'N',
				null,
				null,
				null,
				null,
				sysdate,
				null,
				null);

end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_classif_delete
BEFORE DELETE ON TASY.TITULO_RECEBER_CLASSIF FOR EACH ROW
BEGIN

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TR',sysdate,'E',:old.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_classif_integr
after insert or update ON TASY.TITULO_RECEBER_CLASSIF for each row
declare

qt_reg_w	number(10);
reg_integracao_w		gerar_int_padrao.reg_integracao;

begin

if (:new.nr_titulo is not null) then

	/*Esse select � para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then

		gerar_int_padrao.gravar_integracao('26', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);

	end if;

end if;

if (inserting) then
	/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',:new.dt_atualizacao,'A',:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_classif_insert
after insert ON TASY.TITULO_RECEBER_CLASSIF for each row
declare

cd_estabelecimento_w            ctb_documento.cd_estabelecimento%type;
nr_seq_trans_financ_w           ctb_documento.nr_seq_trans_financ%type;
dt_movimento_w                  ctb_documento.dt_competencia%type;
ie_contab_classif_tit_rec_w     parametro_contas_receber.ie_contab_classif_tit_rec%type;

cursor c01 is
        select  a.nm_atributo,
                a.cd_tipo_lote_contab
        from    atributo_contab a
        where   a.cd_tipo_lote_contab = 5
        and     a.nm_atributo in ('VL_TITULO_RECEBER');

c01_w           c01%rowtype;

begin

select  cd_estabelecimento,
        nr_seq_trans_fin_contab,
        dt_contabil
into    cd_estabelecimento_w,
        nr_seq_trans_financ_w,
        dt_movimento_w
from    titulo_receber
where   nr_titulo = :new.nr_titulo;


begin
select  nvl(ie_contab_classif_tit_rec, 'N')
into    ie_contab_classif_tit_rec_w
from    parametro_contas_receber
where   cd_estabelecimento = cd_estabelecimento_w;
exception when others then
        ie_contab_classif_tit_rec_w := 'N';
end;

begin
select  decode(a.ie_ctb_online, 'S', nvl(a.ie_contab_classif_tit_rec, 'N'), ie_contab_classif_tit_rec_w)
into    ie_contab_classif_tit_rec_w
from    ctb_param_lote_contas_rec a
where   a.cd_empresa = obter_empresa_estab(cd_estabelecimento_w)
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w;
exception when others then
        null;
end;

open c01;
loop
fetch c01 into
        c01_w;
exit when c01%notfound;
        begin

        if      (nvl(:new.vl_classificacao, 0) <> 0 and ie_contab_classif_tit_rec_w = 'S') then
                begin

                ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                        trunc(dt_movimento_w),
                                                                        c01_w.cd_tipo_lote_contab,
                                                                        nr_seq_trans_financ_w,
                                                                        3,
                                                                        :new.nr_titulo,
                                                                        :new.nr_sequencia,
                                                                        0,
                                                                        :new.vl_classificacao,
                                                                        'TITULO_RECEBER_CLASSIF',
                                                                        c01_w.nm_atributo,
                                                                        :new.nm_usuario);

                end;
        end if;

        end;
end loop;
close c01;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_CLASSIF ADD (
  CONSTRAINT TITRECL_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_CLASSIF ADD (
  CONSTRAINT TITRECL_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT TITRECL_PRODFIN_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.PRODUTO_FINANCEIRO (NR_SEQUENCIA),
  CONSTRAINT TITRECL_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TITRECL_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT TITRECL_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITRECL_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TITRECL_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.TITULO_RECEBER_CLASSIF TO NIVEL_1;

