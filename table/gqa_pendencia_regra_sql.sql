ALTER TABLE TASY.GQA_PENDENCIA_REGRA_SQL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GQA_PENDENCIA_REGRA_SQL CASCADE CONSTRAINTS;

CREATE TABLE TASY.GQA_PENDENCIA_REGRA_SQL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_SQL               VARCHAR2(4000 BYTE)      NOT NULL,
  NR_SEQ_GQA_REGRA     NUMBER(10),
  DS_TITULO            VARCHAR2(80 BYTE)        NOT NULL,
  DS_DOCUMENTACAO      VARCHAR2(200 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GQPERES_GQAPERE_FK_I ON TASY.GQA_PENDENCIA_REGRA_SQL
(NR_SEQ_GQA_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GQPERES_PK ON TASY.GQA_PENDENCIA_REGRA_SQL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.gqa_pendencia_regra_sql_atual
before insert or update ON TASY.GQA_PENDENCIA_REGRA_SQL for each row
declare
customSQL_w     varchar2(4000);
ie_resultado_w  varchar2(02);
begin

  if (:new.ie_situacao = 'A') then

    customSQL_w :=  ' select nvl(max(''S''), ''N'') ' ||
                    ' from  atendimento_paciente a'     ||
                    ' where  a.nr_atendimento = -1  '   ||
                    :new.DS_SQL ;

    begin
      execute immediate customSQL_w
      into   ie_resultado_w;

    exception
    when others then
      Wheb_mensagem_pck.exibir_mensagem_abort(751853, 'SQL=' || customSQL_w || ';ERRO='||sqlerrm);
    end;


  end if;

end;
/


ALTER TABLE TASY.GQA_PENDENCIA_REGRA_SQL ADD (
  CONSTRAINT GQPERES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GQA_PENDENCIA_REGRA_SQL ADD (
  CONSTRAINT GQPERES_GQAPERE_FK 
 FOREIGN KEY (NR_SEQ_GQA_REGRA) 
 REFERENCES TASY.GQA_PENDENCIA_REGRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.GQA_PENDENCIA_REGRA_SQL TO NIVEL_1;

