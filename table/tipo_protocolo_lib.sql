ALTER TABLE TASY.TIPO_PROTOCOLO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_PROTOCOLO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_PROTOCOLO_LIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_TIPO_PROTOCOLO    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_PERFIL            NUMBER(5),
  CD_ESPECIALIDADE     NUMBER(5),
  IE_REGRA             VARCHAR2(3 BYTE)         NOT NULL,
  IE_PADRAO            VARCHAR2(1 BYTE),
  NR_SEQ_APRES         NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPRLIB_ESPMEDI_FK_I ON TASY.TIPO_PROTOCOLO_LIB
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRLIB_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIPRLIB_I1 ON TASY.TIPO_PROTOCOLO_LIB
(IE_REGRA, IE_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPRLIB_PERFIL_FK_I ON TASY.TIPO_PROTOCOLO_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRLIB_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIPRLIB_PESFISI_FK_I ON TASY.TIPO_PROTOCOLO_LIB
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPRLIB_PK ON TASY.TIPO_PROTOCOLO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPRLIB_PK
  MONITORING USAGE;


CREATE INDEX TASY.TIPRLIB_TIPPROT_FK_I ON TASY.TIPO_PROTOCOLO_LIB
(CD_TIPO_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_PROTOCOLO_LIB ADD (
  CONSTRAINT TIPRLIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_PROTOCOLO_LIB ADD (
  CONSTRAINT TIPRLIB_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT TIPRLIB_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TIPRLIB_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TIPRLIB_TIPPROT_FK 
 FOREIGN KEY (CD_TIPO_PROTOCOLO) 
 REFERENCES TASY.TIPO_PROTOCOLO (CD_TIPO_PROTOCOLO));

GRANT SELECT ON TASY.TIPO_PROTOCOLO_LIB TO NIVEL_1;

