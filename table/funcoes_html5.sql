ALTER TABLE TASY.FUNCOES_HTML5
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCOES_HTML5 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCOES_HTML5
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  CD_FUNCAO                      NUMBER(10)     NOT NULL,
  DT_PREV_ENTREGA_ANALISE        DATE,
  DT_PREV_ENTREGA_PROGR          DATE,
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE),
  DT_ENTREGA_DESEJADA            DATE,
  DT_ENTREGA_DESEJ_ANALISE       DATE,
  IE_STATUS                      VARCHAR2(15 BYTE) NOT NULL,
  IE_QUARTER_ENTREGA             VARCHAR2(15 BYTE) NOT NULL,
  IE_PRIORIDADE                  NUMBER(10),
  QT_LINHAS_PREVISTAS_ANG_FRONT  NUMBER(10),
  QT_TX_CONVERSAO                NUMBER(12,2),
  IE_TAMANHO_FUNCAO              VARCHAR2(5 BYTE),
  QT_LINHAS_JAVA_FRONT           NUMBER(10),
  PR_CONCLUSAO                   NUMBER(5,2),
  IE_TIPO_FUNCAO                 VARCHAR2(5 BYTE),
  QT_LINHAS_JAVA_BACK            NUMBER(10),
  QT_LINHAS_PREVISTAS_ANG_BACK   NUMBER(10),
  QT_LINHAS_ANGULAR_FRONT        NUMBER(10),
  QT_LINHAS_ANGULAR_BACK         NUMBER(10),
  QT_OS_TOTAL_ANALISTA           NUMBER(10),
  QT_OS_PEND_ANALISTA            NUMBER(10),
  QT_HORAS_TOTAL_ANALISTA        NUMBER(15,2),
  QT_HORAS_PEND_ANALISTA         NUMBER(15,2),
  QT_OS_TOTAL_PROG               NUMBER(10),
  QT_OS_PEND_PROG                NUMBER(10),
  QT_HORAS_TOTAL_PROG            NUMBER(15,2),
  QT_HORAS_PEND_PROG             NUMBER(15,2),
  QT_OS_TEC                      NUMBER(10),
  QT_OS_VV                       NUMBER(10),
  QT_TX_CONVERSAO_FRONT          NUMBER(12,2),
  QT_TX_CONVERSAO_BACK           NUMBER(12,2),
  IE_PPC                         VARCHAR2(100 BYTE),
  IE_MERCADO_GLOBAL              VARCHAR2(100 BYTE),
  IE_ESCOPO_ATUAL                VARCHAR2(1 BYTE),
  NR_ROUND_VV                    NUMBER(10),
  PR_ADERENCIA_VV                NUMBER(5),
  IE_SITE_DESENV                 VARCHAR2(15 BYTE),
  PR_REPROV_VV                   NUMBER(5),
  IE_STATUS_VV                   VARCHAR2(15 BYTE),
  QT_OS_VV_PEND_DEV              NUMBER(10),
  QT_OS_VV_PEND_TEC              NUMBER(10),
  QT_OS_VV_AGUAR_V               NUMBER(10),
  QT_OS_VV_PEND_TEST             NUMBER(10),
  CD_PESSOA_ANALISTA_NEG         VARCHAR2(10 BYTE),
  CD_PESSOA_ANALISTA_OPER        VARCHAR2(10 BYTE),
  PR_CONCLUSAO_ANALIS            NUMBER(5,2),
  PR_CONCLUSAO_PROG              NUMBER(5,2),
  PR_CONCLUSAO_DESENV            NUMBER(5,2),
  DS_VERSAO_JAVA                 VARCHAR2(100 BYTE),
  DS_VERSAO_TEST_CASE            VARCHAR2(100 BYTE),
  QT_GAP_OS                      NUMBER(10),
  TEMPO_GAP_OS                   NUMBER(10),
  IE_PPC_ORIGINAL                VARCHAR2(1 BYTE),
  QT_OS_TEC_DEFEITO              NUMBER(10),
  DT_APROV_CHECK_POINT           DATE,
  CD_PESSOA_DEV_LED              VARCHAR2(10 BYTE),
  NR_SEQ_PROJ_TESTE              NUMBER(10),
  QT_OS_DESIGN                   NUMBER(10),
  CD_TESTER                      VARCHAR2(10 BYTE),
  QT_HORAS_ROBO_PROG             NUMBER(15,2),
  QT_HORAS_ROBO_ANALISE          NUMBER(15,2),
  QT_HORAS_CRON_PROG             NUMBER(15,2),
  QT_HORAS_CRON_ANALISE          NUMBER(15,2),
  QT_HORAS_CRON_TESTE            NUMBER(15,2),
  QT_HORAS_PEND_TESTE            NUMBER(15,2),
  QT_HORAS_TOTAL_TESTE           NUMBER(15,2),
  PR_HORAS_ANALISE               NUMBER(15,2),
  PR_CONCLUSAO_TESTE             NUMBER(15,2),
  PR_HORAS_PROG                  NUMBER(15,2),
  PR_HORAS_TESTE                 NUMBER(15,2),
  PR_OS_ANALISE                  NUMBER(15,2),
  PR_OS_PROG                     NUMBER(15,2),
  PR_OS_TESTE                    NUMBER(15,2),
  QT_HORAS_CRON_PEND_ANALISE     NUMBER(15,2),
  QT_HORAS_CRON_PEND_PROG        NUMBER(15,2),
  QT_HORAS_CRON_PEND_TESTE       NUMBER(15,2),
  QT_OS_PEND_VV                  NUMBER(10),
  PR_VAR_HORAS_ANALISE           NUMBER(15,2),
  PR_VAR_HORAS_PROG              NUMBER(15,2),
  PR_VAR_HORAS_TESTE             NUMBER(15,2),
  PR_VAR_HORAS_FUNCAO            NUMBER(15,2),
  NR_SEQ_PROJ_PROG               NUMBER(10),
  NR_SEQ_PROJ_ANALISE            NUMBER(10),
  DT_REAL_INICIO_TESTE           DATE,
  DT_REAL_FIM_TESTE              DATE,
  DT_PREV_INICIO_FUN             DATE,
  DT_PREV_FIM_FUN                DATE,
  DT_PREV_INICIO_ANAL_FUN        DATE,
  DT_PREV_FIM_ANAL_FUN           DATE,
  DT_PREV_INICIO_PROG_FUN        DATE,
  DT_PREV_FIM_PROG_FUN           DATE,
  DT_PREV_INICIO_TESTE_FUN       DATE,
  DT_PREV_FIM_TESTE_FUN          DATE,
  DT_PREV_CHECKPOINT             DATE,
  DT_REAL_INICIO_FUN             DATE,
  DT_REAL_FIM_FUN                DATE,
  DT_REAL_CHECKPOINT             DATE,
  DT_PREV_INICIO_ANALISE         DATE,
  DT_PREV_FIM_ANALISE            DATE,
  DT_PREV_INICIO_PROG            DATE,
  DT_PREV_FIM_PROG               DATE,
  DT_PREV_INICIO_TESTE           DATE,
  DT_PREV_FIM_TESTE              DATE,
  DT_REAL_INICIO_ANALISE         DATE,
  DT_REAL_FIM_ANALISE            DATE,
  DT_REAL_INICIO_PROG            DATE,
  DT_REAL_FIM_PROG               DATE,
  IE_TAMANHO_TESTE               VARCHAR2(15 BYTE),
  PR_PERFORM_PROG                NUMBER(15,2),
  PR_PERFORM_ANALISTA            NUMBER(15,2),
  PR_PERFORM_TESTE               NUMBER(15,2),
  QT_HORAS_CRON_PROG_RET         NUMBER(15,2),
  DT_PREV_APROV_GERENTE          DATE,
  DT_REAL_APROV_GERENTE          DATE,
  PR_PREV_CONCLUS_ANALISE        NUMBER(15,2),
  PR_PREV_CONCLUS_TESTE          NUMBER(15,2),
  PR_PREV_CONCLUS_PROG           NUMBER(15,2),
  QT_HORAS_CRON_PROG_EXT         NUMBER(15,2),
  QT_HORAS_CRON_PROG_EXE         NUMBER(15,2),
  QT_HORAS_CRON_ANA_EXT          NUMBER(15,2),
  QT_HORAS_CRON_ANA_EXE          NUMBER(15,2),
  QT_HORAS_CRON_TESTE_EXT        NUMBER(15,2),
  QT_HORAS_CRON_TESTE_EXE        NUMBER(15,2),
  QT_HORAS_TOTAL_PROG_EXT        NUMBER(15,2),
  QT_HORAS_TOTAL_PROG_EXE        NUMBER(15,2),
  QT_HORAS_TOTAL_ANA_EXT         NUMBER(15,2),
  QT_HORAS_TOTAL_ANA_EXE         NUMBER(15,2),
  QT_HORAS_TOTAL_TESTE_EXT       NUMBER(15,2),
  QT_HORAS_TOTAL_TESTE_EXE       NUMBER(15,2),
  QT_HORAS_CRON_PEND_PRO_EXT     NUMBER(15,2),
  QT_HORAS_CRON_PEND_PRO_EXE     NUMBER(15,2),
  QT_HORAS_CRON_PEND_ANA_EXT     NUMBER(15,2),
  QT_HORAS_CRON_PEND_ANA_EXE     NUMBER(15,2),
  QT_HORAS_CRON_PEND_TES_EXT     NUMBER(15,2),
  QT_HORAS_CRON_PEND_TES_EXE     NUMBER(15,2),
  QT_HORAS_PEND_PROG_EXT         NUMBER(15,2),
  QT_HORAS_PEND_PROG_EXE         NUMBER(15,2),
  QT_HORAS_PEND_ANALISTA_EXT     NUMBER(15,2),
  QT_HORAS_PEND_ANALISTA_EXE     NUMBER(15,2),
  QT_HORAS_PEND_TESTE_EXT        NUMBER(15,2),
  QT_HORAS_PEND_TESTE_EXE        NUMBER(15,2),
  QT_OS_PEND_PROG_EXT            NUMBER(15,2),
  QT_OS_PEND_PROG_EXE            NUMBER(15,2),
  QT_OS_PEND_ANALISTA_EXT        NUMBER(15,2),
  QT_OS_PEND_ANALISTA_EXE        NUMBER(15,2),
  QT_OS_PEND_VV_EXT              NUMBER(15,2),
  QT_OS_PEND_VV_EXE              NUMBER(15,2),
  QT_OS_TOTAL_PROG_EXT           NUMBER(15,2),
  QT_OS_TOTAL_PROG_EXE           NUMBER(15,2),
  QT_OS_TOTAL_ANALISTA_EXT       NUMBER(15,2),
  QT_OS_TOTAL_ANALISTA_EXE       NUMBER(15,2),
  QT_OS_VV_EXT                   NUMBER(15,2),
  QT_OS_VV_EXE                   NUMBER(15,2),
  QT_TEMPO_TOTAL                 NUMBER(10,2),
  QT_TOTAL_ITENS_SCHEMATIC       NUMBER(10),
  QT_TOTAL_ITENS_SCHE_AVAL       NUMBER(10),
  PR_ITENS_SCHE_AVAL             NUMBER(6,2),
  QT_TOTAL_HORAS_AVAL            NUMBER(6,2),
  QT_TOTAL_HORAS_NEC_PROG        NUMBER(6,2),
  PR_TOTAL_HORAS_NEC_PROG        NUMBER(6,2),
  QT_HORAS_TOTAL_ROBO            NUMBER(6,2),
  QT_HORAS_TOTAL_AJUSTADO        NUMBER(6,2),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_AGRUPAMENTO             NUMBER(10),
  PR_PREVISTO_CONCLUSAO          NUMBER(15,2),
  NR_ORDEM_SERV_TRE              NUMBER(10),
  NR_ORDEM_SERV_TRE_EN           NUMBER(10),
  DT_ORDEM_SERV_TRE              DATE,
  DT_INICIO_PREV_TRE             DATE,
  DT_ORDEM_SERV_TRE_EN           DATE,
  DT_INICIO_PREV_TRE_EN          DATE,
  DT_FIM_PREV_TRE                DATE,
  DT_INICIO_REAL_TRE             DATE,
  DT_FIM_REAL_TRE                DATE,
  DT_FIM_PREV_TRE_EN             DATE,
  DT_INICIO_REAL_TRE_EN          DATE,
  DT_FIM_REAL_TRE_EN             DATE,
  QT_ORDEM_SERV_TRE              NUMBER(10),
  QT_ORDEM_SERV_TRE_EN           NUMBER(10),
  DT_HANDOFF                     DATE,
  CD_RESP_HANDOFF                VARCHAR2(10 BYTE),
  CD_FUNCAO_REFERENCIA           NUMBER(5),
  IE_MARCACAO_CE                 VARCHAR2(1 BYTE),
  DS_ARQUIVO                     VARCHAR2(255 BYTE),
  DS_DEPRACATED                  VARCHAR2(4000 BYTE),
  NR_SEQ_PROJ_ELEVEN             NUMBER(10),
  DT_SCORE_FUNCAO                DATE,
  IE_IFU                         VARCHAR2(255 BYTE),
  QT_SCORE_FUNCAO                NUMBER(10,2),
  IE_MANUAL                      VARCHAR2(10 BYTE),
  IE_MODULARIZATION              VARCHAR2(1 BYTE),
  NR_PROJECT_ID                  NUMBER(10),
  IE_STATUS_MODULARIZATION       VARCHAR2(15 BYTE),
  CD_PRODUCT_MANAGER             VARCHAR2(10 BYTE),
  DS_REASON_PRIORITIZE           VARCHAR2(255 BYTE),
  QT_ESTIMATED                   NUMBER(10,2),
  CD_RESP_DIRECTOR               VARCHAR2(10 BYTE),
  DT_DELIVERY                    DATE,
  NR_PRIORITY                    NUMBER(10),
  IE_SITE_MODULARIZATION         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNHTML_AGRFUNC_FK_I ON TASY.FUNCOES_HTML5
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_FUNCAO_FK_I ON TASY.FUNCOES_HTML5
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK_I ON TASY.FUNCOES_HTML5
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK2_I ON TASY.FUNCOES_HTML5
(CD_PESSOA_ANALISTA_NEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK3_I ON TASY.FUNCOES_HTML5
(CD_PESSOA_ANALISTA_OPER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK4_I ON TASY.FUNCOES_HTML5
(CD_PESSOA_DEV_LED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK5_I ON TASY.FUNCOES_HTML5
(CD_TESTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK6_I ON TASY.FUNCOES_HTML5
(CD_RESP_HANDOFF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK7_I ON TASY.FUNCOES_HTML5
(CD_PRODUCT_MANAGER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PESFISI_FK8_I ON TASY.FUNCOES_HTML5
(CD_RESP_DIRECTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNHTML_PK ON TASY.FUNCOES_HTML5
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PROPROJ_FK_I ON TASY.FUNCOES_HTML5
(NR_SEQ_PROJ_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PROPROJ_FK2_I ON TASY.FUNCOES_HTML5
(NR_SEQ_PROJ_PROG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PROPROJ_FK3_I ON TASY.FUNCOES_HTML5
(NR_SEQ_PROJ_TESTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PROPROJ_FK4_I ON TASY.FUNCOES_HTML5
(NR_SEQ_PROJ_ELEVEN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNHTML_PROPROJ_FK5_I ON TASY.FUNCOES_HTML5
(NR_PROJECT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.funcoes_html5_atual
before insert or update ON TASY.FUNCOES_HTML5 for each row
declare

nr_seq_ordem_gerada_w number(10);
dt_ordem_servico_w    date;

  procedure inserir_ordem_servico(  nr_seq_localizacao_p  number,
                    nr_seq_equipamento_p  number,
                    cd_pessoa_solicitante_p number,
                    dt_ordem_servico_p    date,
                    ds_dano_breve_p     varchar2,
                    ds_dano_p       varchar2,
                    dt_inicio_previsto_p  date,
                    dt_fim_previsto_p   date,
                    ie_tipo_ordem_p     varchar2,
                    NR_SEQ_TIPO_ORDEM_p   number,
                    nr_grupo_planej_p   number,
                    nr_grupo_trabalho_p   number,
                    ie_status_ordem_p   varchar2,
                    nr_seq_estagio_p    number,
                    cd_funcao_p       number,
                    ie_classificacao_p    varchar2,
                    nr_seq_grupo_des_p    number,
                    nr_seq_ordem_servico_p  out number ) is

  nr_seq_ordem_w  number(10);
  begin

  select  man_ordem_servico_seq.nextval
  into  nr_seq_ordem_w
  from  dual;
  nr_seq_ordem_servico_p  := nr_seq_ordem_w;


  insert  into man_ordem_servico
    (nr_sequencia,
    nr_seq_localizacao,
    nr_seq_equipamento,
    cd_pessoa_solicitante,
    dt_ordem_servico,
    ie_prioridade,
    ie_parado,
    ds_dano_breve,
    dt_atualizacao,
    nm_usuario,
    dt_inicio_desejado,
    dt_conclusao_desejada,
    ds_dano,
    dt_inicio_previsto,
    dt_fim_previsto,
    dt_inicio_real,
    dt_fim_real,
    ie_tipo_ordem,
    ie_status_ordem,
    nr_grupo_planej,
    nr_grupo_trabalho,
    nr_seq_tipo_solucao,
    ds_solucao,
    nm_usuario_exec,
    qt_contador,
    nr_seq_planej,
    nr_seq_tipo_contador,
    nr_seq_estagio,
    cd_projeto,
    nr_seq_etapa_proj,
    dt_reabertura,
    cd_funcao,
    nm_tabela,
    ie_classificacao,
    nr_seq_origem,
    nr_seq_projeto,
    ie_grau_satisfacao,
    nr_seq_indicador,
    nr_seq_causa_dano,
    ie_forma_receb,
    nr_seq_cliente,
    nr_seq_grupo_des,
    nr_seq_grupo_sup,
    nr_seq_superior,
    ie_eficacia,
    dt_prev_eficacia,
    cd_pf_eficacia,
    nr_seq_nao_conform,
    nr_seq_complex,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    ie_obriga_news,
    nr_seq_meta_pe,
    nr_seq_classif,
    nr_seq_nivel_valor,
    nm_usuario_lib_news,
    dt_libera_news,
    dt_envio_wheb,
    ds_contato_solicitante,
    ie_prioridade_desen,
    ie_prioridade_sup,
    nr_seq_proj_cron_etapa,
    ie_exclusiva,
    ie_origem_os,
    ie_resp_teste,
    ie_plataforma,
    NR_SEQ_TIPO_ORDEM)
  values  (nr_seq_ordem_w,            -- nr_sequencia,
    nr_seq_localizacao_p,         -- nr_seq_localizacao,
    nr_seq_equipamento_p,         -- nr_seq_equipamento,
    cd_pessoa_solicitante_p,        -- cd_pessoa_solicitante,
    sysdate,                -- dt_ordem_servico,
    'M',                      -- ie_prioridade,
    'N',                  -- ie_parado,
    substr(ds_dano_breve_p,1,80),     -- ds_dano_breve,
    sysdate,                -- dt_atualizacao,
    :new.nm_usuario,              -- nm_usuario,
    null,               -- dt_inicio_desejado,
    null,             -- dt_conclusao_desejada,
    ds_dano_p,                -- ds_dano,
    null, -- dt_inicio_previsto,
    null,           -- dt_fim_previsto,
    null,               -- dt_inicio_real,
    null,                 -- dt_fim_real,
    ie_tipo_ordem_p,        -- ie_tipo_ordem,
    nvl(ie_status_ordem_p,'1'),       -- ie_status_ordem,
    nr_grupo_planej_p,            -- nr_grupo_planej,
    nr_grupo_trabalho_p,          -- nr_grupo_trabalho,
    null,                 -- nr_seq_tipo_solucao,
    null,                 -- ds_solucao,
    null, -- nm_usuario_exec,
    null,         -- qt_contador,
    null,         -- nr_seq_planej,
    null,         -- nr_seq_tipo_contador,
    nr_seq_estagio_p,       -- nr_seq_estagio,
    null,         -- cd_projeto,
    null,         -- nr_seq_etapa_proj,
    null,         -- dt_reabertura,
    cd_funcao_p,        -- cd_funcao,
    null,         -- nm_tabela,
    ie_classificacao_p,       -- ie_classificacao,
    null,         -- nr_seq_origem,
    null,         -- nr_seq_projeto,
    null,         -- ie_grau_satisfacao,
    null,         -- nr_seq_indicador,
    null,         -- nr_seq_causa_dano,
    'W',      -- ie_forma_receb,
    null,         -- nr_seq_cliente,
    nr_seq_grupo_des_p,     -- nr_seq_grupo_des,
    null,         -- nr_seq_grupo_sup,
    null,         -- nr_seq_superior,
    null,         -- ie_eficacia,
    null,         -- dt_prev_eficacia,
    null,         -- cd_pf_eficacia,
    null,         -- nr_seq_nao_conform,
    2,          -- nr_seq_complex,
    null,         -- dt_atualizacao_nrec,
    :new.nm_usuario,        -- nm_usuario_nrec,
    null,         -- ie_obriga_news,
    null,         -- nr_seq_meta_pe,
    2,          -- nr_seq_classif,
    1,          -- nr_seq_nivel_valor,
    null,         -- nm_usuario_lib_news,
    null,         -- dt_libera_news,
    null,         -- dt_envio_wheb,
    null,         -- ds_contato_solicitante,
    null,         -- ie_prioridade_desen,
    null,         -- ie_prioridade_sup
    null,     -- nr_seq_proj_cron_etapa
    'P',          -- ie_exclusiva
    '1',          --ie_origem
    null,     --ie_resp_teste
    'H',
    NR_SEQ_TIPO_ORDEM_p);


    insert into  man_ordem_servico_exec(
      nr_sequencia,
      nr_seq_ordem,
      dt_atualizacao,
      nm_usuario,
      nm_usuario_exec,
      QT_MIN_PREV)
    values( man_ordem_servico_exec_seq.nextval,
      nr_seq_ordem_w,
      sysdate,
      :new.nm_usuario,
      'artridapalli',
      120);

  end;

begin


if (:old.IE_TIPO_FUNCAO = 'F') and (:new.IE_STATUS = 'L') and (:new.IE_STATUS  <> :old.IE_STATUS) then
  dt_ordem_servico_w  := sysdate;
  if  (:new.NR_ORDEM_SERV_TRE is null) then

    inserir_ordem_servico(  6635, --nr_seq_localizacao_p  number,
                7161, --nr_seq_equipamento_p  number,
                '2960', --cd_pessoa_solicitante_p number,
                dt_ordem_servico_w, --dt_ordem_servico_p    date,
                'Portuguese Training HTML5 - '|| obter_desc_funcao(:new.cd_funcao), --ds_dano_breve_p     varchar2,
                'Portuguese Training HTML5 - '|| obter_desc_funcao(:new.cd_funcao), --ds_dano_p       varchar2,
                null,--dt_inicio_previsto_p date,
                null,--dt_fim_previsto_p    date,
                '6', --ie_tipo_ordem_p      varchar2,
                219, --NR_SEQ_TIPO_ORDEM_p    number,
                151, --nr_grupo_planej_p    number,
                53, --nr_grupo_trabalho_p   number,
                1, --ie_status_ordem_p    varchar2,
                2102, --nr_seq_estagio_p    number,
                :new.cd_funcao, --cd_funcao_p       number,
                'S', --ie_classificacao_p   varchar2,
                null, --nr_seq_grupo_des_p    number
                nr_seq_ordem_gerada_w);  --nr_seq_ordem_servico_p out number

    :new.NR_ORDEM_SERV_TRE  := nr_seq_ordem_gerada_w;
    :new.DT_ORDEM_SERV_TRE  := dt_ordem_servico_w;

  end if;


  if  (:new.NR_ORDEM_SERV_TRE_EN is null) then
    nr_seq_ordem_gerada_w := null;
    inserir_ordem_servico(  6635, --nr_seq_localizacao_p  number,
                7161, --nr_seq_equipamento_p  number,
                '2960', --cd_pessoa_solicitante_p number,
                dt_ordem_servico_w, --dt_ordem_servico_p    date,
                'English Training HTML5 - '|| obter_desc_funcao(:new.cd_funcao), --ds_dano_breve_p      varchar2,
                'English Training HTML5 - '|| obter_desc_funcao(:new.cd_funcao), --ds_dano_p        varchar2,
                null,--dt_inicio_previsto_p date,
                null,--dt_fim_previsto_p    date,
                '6', --ie_tipo_ordem_p      varchar2,
                220, --NR_SEQ_TIPO_ORDEM_p    number,
                151, --nr_grupo_planej_p    number,
                53, --nr_grupo_trabalho_p   number,
                1, --ie_status_ordem_p    varchar2,
                2102, --nr_seq_estagio_p    number,
                :new.cd_funcao, --cd_funcao_p       number,
                'S', --ie_classificacao_p   varchar2,
                null, --nr_seq_grupo_des_p    number
                nr_seq_ordem_gerada_w);  --nr_seq_ordem_servico_p out number

    :new.NR_ORDEM_SERV_TRE_EN := nr_seq_ordem_gerada_w;
    :new.DT_ORDEM_SERV_TRE_EN := dt_ordem_servico_w;

  end if;


end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.FUNCOES_HTML5_tp  after update ON TASY.FUNCOES_HTML5 FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.PR_CONCLUSAO_ANALIS,1,500);gravar_log_alteracao(substr(:old.PR_CONCLUSAO_ANALIS,1,4000),substr(:new.PR_CONCLUSAO_ANALIS,1,4000),:new.nm_usuario,nr_seq_w,'PR_CONCLUSAO_ANALIS',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.PR_CONCLUSAO,1,500);gravar_log_alteracao(substr(:old.PR_CONCLUSAO,1,4000),substr(:new.PR_CONCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'PR_CONCLUSAO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_TX_CONVERSAO_BACK,1,500);gravar_log_alteracao(substr(:old.QT_TX_CONVERSAO_BACK,1,4000),substr(:new.QT_TX_CONVERSAO_BACK,1,4000),:new.nm_usuario,nr_seq_w,'QT_TX_CONVERSAO_BACK',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_ROUND_VV,1,500);gravar_log_alteracao(substr(:old.NR_ROUND_VV,1,4000),substr(:new.NR_ROUND_VV,1,4000),:new.nm_usuario,nr_seq_w,'NR_ROUND_VV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PPC,1,500);gravar_log_alteracao(substr(:old.IE_PPC,1,4000),substr(:new.IE_PPC,1,4000),:new.nm_usuario,nr_seq_w,'IE_PPC',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ESCOPO_ATUAL,1,500);gravar_log_alteracao(substr(:old.IE_ESCOPO_ATUAL,1,4000),substr(:new.IE_ESCOPO_ATUAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESCOPO_ATUAL',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_TOTAL_ANALISTA,1,500);gravar_log_alteracao(substr(:old.QT_OS_TOTAL_ANALISTA,1,4000),substr(:new.QT_OS_TOTAL_ANALISTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_TOTAL_ANALISTA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_PEND_ANALISTA,1,500);gravar_log_alteracao(substr(:old.QT_OS_PEND_ANALISTA,1,4000),substr(:new.QT_OS_PEND_ANALISTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_PEND_ANALISTA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_HORAS_TOTAL_ANALISTA,1,500);gravar_log_alteracao(substr(:old.QT_HORAS_TOTAL_ANALISTA,1,4000),substr(:new.QT_HORAS_TOTAL_ANALISTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_TOTAL_ANALISTA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_HORAS_PEND_ANALISTA,1,500);gravar_log_alteracao(substr(:old.QT_HORAS_PEND_ANALISTA,1,4000),substr(:new.QT_HORAS_PEND_ANALISTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_PEND_ANALISTA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_TOTAL_PROG,1,500);gravar_log_alteracao(substr(:old.QT_OS_TOTAL_PROG,1,4000),substr(:new.QT_OS_TOTAL_PROG,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_TOTAL_PROG',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_PEND_PROG,1,500);gravar_log_alteracao(substr(:old.QT_OS_PEND_PROG,1,4000),substr(:new.QT_OS_PEND_PROG,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_PEND_PROG',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_HORAS_TOTAL_PROG,1,500);gravar_log_alteracao(substr(:old.QT_HORAS_TOTAL_PROG,1,4000),substr(:new.QT_HORAS_TOTAL_PROG,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_TOTAL_PROG',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_HORAS_PEND_PROG,1,500);gravar_log_alteracao(substr(:old.QT_HORAS_PEND_PROG,1,4000),substr(:new.QT_HORAS_PEND_PROG,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_PEND_PROG',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_TEC,1,500);gravar_log_alteracao(substr(:old.QT_OS_TEC,1,4000),substr(:new.QT_OS_TEC,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_TEC',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_VV,1,500);gravar_log_alteracao(substr(:old.QT_OS_VV,1,4000),substr(:new.QT_OS_VV,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_VV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_MERCADO_GLOBAL,1,500);gravar_log_alteracao(substr(:old.IE_MERCADO_GLOBAL,1,4000),substr(:new.IE_MERCADO_GLOBAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_MERCADO_GLOBAL',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS_VV,1,500);gravar_log_alteracao(substr(:old.IE_STATUS_VV,1,4000),substr(:new.IE_STATUS_VV,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_VV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_ANALISTA_NEG,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_ANALISTA_NEG,1,4000),substr(:new.CD_PESSOA_ANALISTA_NEG,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_ANALISTA_NEG',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_ANALISTA_OPER,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_ANALISTA_OPER,1,4000),substr(:new.CD_PESSOA_ANALISTA_OPER,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_ANALISTA_OPER',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_VV_PEND_DEV,1,500);gravar_log_alteracao(substr(:old.QT_OS_VV_PEND_DEV,1,4000),substr(:new.QT_OS_VV_PEND_DEV,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_VV_PEND_DEV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_VV_PEND_TEC,1,500);gravar_log_alteracao(substr(:old.QT_OS_VV_PEND_TEC,1,4000),substr(:new.QT_OS_VV_PEND_TEC,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_VV_PEND_TEC',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_VV_AGUAR_V,1,500);gravar_log_alteracao(substr(:old.QT_OS_VV_AGUAR_V,1,4000),substr(:new.QT_OS_VV_AGUAR_V,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_VV_AGUAR_V',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_OS_VV_PEND_TEST,1,500);gravar_log_alteracao(substr(:old.QT_OS_VV_PEND_TEST,1,4000),substr(:new.QT_OS_VV_PEND_TEST,1,4000),:new.nm_usuario,nr_seq_w,'QT_OS_VV_PEND_TEST',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_TX_CONVERSAO_FRONT,1,500);gravar_log_alteracao(substr(:old.QT_TX_CONVERSAO_FRONT,1,4000),substr(:new.QT_TX_CONVERSAO_FRONT,1,4000),:new.nm_usuario,nr_seq_w,'QT_TX_CONVERSAO_FRONT',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITE_DESENV,1,500);gravar_log_alteracao(substr(:old.IE_SITE_DESENV,1,4000),substr(:new.IE_SITE_DESENV,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITE_DESENV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.PR_ADERENCIA_VV,1,500);gravar_log_alteracao(substr(:old.PR_ADERENCIA_VV,1,4000),substr(:new.PR_ADERENCIA_VV,1,4000),:new.nm_usuario,nr_seq_w,'PR_ADERENCIA_VV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.PR_REPROV_VV,1,500);gravar_log_alteracao(substr(:old.PR_REPROV_VV,1,4000),substr(:new.PR_REPROV_VV,1,4000),:new.nm_usuario,nr_seq_w,'PR_REPROV_VV',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_FUNCAO,1,500);gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_PREV_ENTREGA_ANALISE,1,500);gravar_log_alteracao(substr(:old.DT_PREV_ENTREGA_ANALISE,1,4000),substr(:new.DT_PREV_ENTREGA_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'DT_PREV_ENTREGA_ANALISE',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_PREV_ENTREGA_PROGR,1,500);gravar_log_alteracao(substr(:old.DT_PREV_ENTREGA_PROGR,1,4000),substr(:new.DT_PREV_ENTREGA_PROGR,1,4000),:new.nm_usuario,nr_seq_w,'DT_PREV_ENTREGA_PROGR',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ENTREGA_DESEJADA,1,500);gravar_log_alteracao(substr(:old.DT_ENTREGA_DESEJADA,1,4000),substr(:new.DT_ENTREGA_DESEJADA,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENTREGA_DESEJADA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ENTREGA_DESEJ_ANALISE,1,500);gravar_log_alteracao(substr(:old.DT_ENTREGA_DESEJ_ANALISE,1,4000),substr(:new.DT_ENTREGA_DESEJ_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENTREGA_DESEJ_ANALISE',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_QUARTER_ENTREGA,1,500);gravar_log_alteracao(substr(:old.IE_QUARTER_ENTREGA,1,4000),substr(:new.IE_QUARTER_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'IE_QUARTER_ENTREGA',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_LINHAS_JAVA_FRONT,1,500);gravar_log_alteracao(substr(:old.QT_LINHAS_JAVA_FRONT,1,4000),substr(:new.QT_LINHAS_JAVA_FRONT,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHAS_JAVA_FRONT',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PRIORIDADE,1,500);gravar_log_alteracao(substr(:old.IE_PRIORIDADE,1,4000),substr(:new.IE_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIORIDADE',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_FUNCAO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_FUNCAO,1,4000),substr(:new.IE_TIPO_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FUNCAO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_TX_CONVERSAO,1,500);gravar_log_alteracao(substr(:old.QT_TX_CONVERSAO,1,4000),substr(:new.QT_TX_CONVERSAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TX_CONVERSAO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_LINHAS_JAVA_BACK,1,500);gravar_log_alteracao(substr(:old.QT_LINHAS_JAVA_BACK,1,4000),substr(:new.QT_LINHAS_JAVA_BACK,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHAS_JAVA_BACK',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_LINHAS_ANGULAR_FRONT,1,500);gravar_log_alteracao(substr(:old.QT_LINHAS_ANGULAR_FRONT,1,4000),substr(:new.QT_LINHAS_ANGULAR_FRONT,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHAS_ANGULAR_FRONT',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_LINHAS_ANGULAR_BACK,1,500);gravar_log_alteracao(substr(:old.QT_LINHAS_ANGULAR_BACK,1,4000),substr(:new.QT_LINHAS_ANGULAR_BACK,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHAS_ANGULAR_BACK',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_LINHAS_PREVISTAS_ANG_FRONT,1,500);gravar_log_alteracao(substr(:old.QT_LINHAS_PREVISTAS_ANG_FRONT,1,4000),substr(:new.QT_LINHAS_PREVISTAS_ANG_FRONT,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHAS_PREVISTAS_ANG_FRONT',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_LINHAS_PREVISTAS_ANG_BACK,1,500);gravar_log_alteracao(substr(:old.QT_LINHAS_PREVISTAS_ANG_BACK,1,4000),substr(:new.QT_LINHAS_PREVISTAS_ANG_BACK,1,4000),:new.nm_usuario,nr_seq_w,'QT_LINHAS_PREVISTAS_ANG_BACK',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TAMANHO_FUNCAO,1,500);gravar_log_alteracao(substr(:old.IE_TAMANHO_FUNCAO,1,4000),substr(:new.IE_TAMANHO_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TAMANHO_FUNCAO',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.PR_CONCLUSAO_PROG,1,500);gravar_log_alteracao(substr(:old.PR_CONCLUSAO_PROG,1,4000),substr(:new.PR_CONCLUSAO_PROG,1,4000),:new.nm_usuario,nr_seq_w,'PR_CONCLUSAO_PROG',ie_log_w,ds_w,'FUNCOES_HTML5',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FUNCOES_HTML5 ADD (
  CONSTRAINT FUNHTML_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCOES_HTML5 ADD (
  CONSTRAINT FUNHTML_PESFISI_FK4 
 FOREIGN KEY (CD_PESSOA_DEV_LED) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_PESFISI_FK5 
 FOREIGN KEY (CD_TESTER) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_ANALISE) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT FUNHTML_PROPROJ_FK2 
 FOREIGN KEY (NR_SEQ_PROJ_PROG) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT FUNHTML_PROPROJ_FK3 
 FOREIGN KEY (NR_SEQ_PROJ_TESTE) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT FUNHTML_AGRFUNC_FK 
 FOREIGN KEY (NR_SEQ_AGRUPAMENTO) 
 REFERENCES TASY.AGRUPAMENTO_FUNCOES_HTML5 (NR_SEQUENCIA),
  CONSTRAINT FUNHTML_PESFISI_FK6 
 FOREIGN KEY (CD_RESP_HANDOFF) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO)
    ON DELETE CASCADE,
  CONSTRAINT FUNHTML_PROPROJ_FK4 
 FOREIGN KEY (NR_SEQ_PROJ_ELEVEN) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT FUNHTML_PESFISI_FK7 
 FOREIGN KEY (CD_PRODUCT_MANAGER) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_PESFISI_FK8 
 FOREIGN KEY (CD_RESP_DIRECTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_PROPROJ_FK5 
 FOREIGN KEY (NR_PROJECT_ID) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT FUNHTML_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_ANALISTA_NEG) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_ANALISTA_OPER) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FUNHTML_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.FUNCOES_HTML5 TO NIVEL_1;

