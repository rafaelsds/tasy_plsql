ALTER TABLE TASY.PLS_TPS_REG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TPS_REG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TPS_REG
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_TPS              NUMBER(10)            NOT NULL,
  DT_MES_REGISTRO         DATE                  NOT NULL,
  IE_ABRANGENCIA          VARCHAR2(3 BYTE)      NOT NULL,
  IE_SEGMENTACAO          VARCHAR2(3 BYTE),
  IE_FAIXA_ETARIA         NUMBER(2)             NOT NULL,
  VL_BASE                 NUMBER(15,2)          NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  VL_DESCONTO             NUMBER(15,2),
  QT_PRIMEIRO_MES         NUMBER(10),
  QT_SEGUNDO_MES          NUMBER(10),
  QT_TERCEIRO_MES         NUMBER(10),
  QT_MEDIA_BENEFICIARIOS  NUMBER(15,2),
  VL_CALCULO              NUMBER(15,2),
  NR_SEQ_GRUPO_SEG        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTPSR_PK ON TASY.PLS_TPS_REG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTPSR_PLSGSEG_FK_I ON TASY.PLS_TPS_REG
(NR_SEQ_GRUPO_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTPSR_PLSOTPS_FK_I ON TASY.PLS_TPS_REG
(NR_SEQ_TPS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_TPS_REG_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_TPS_REG FOR EACH ROW
declare

begin

if	(:new.ie_segmentacao is not null) and
	(:new.nr_seq_grupo_seg is not null) then
	-- � permitido apenas informar ou o grupo ou a segmenta��o, n�o ambos.
	wheb_mensagem_pck.exibir_mensagem_abort(267056);
elsif	(:new.ie_segmentacao is null) and
	(:new.nr_seq_grupo_seg is null) then
	-- � necess�rio informar ou o grupo de segmenta��es ou uma segmenta��o.
	wheb_mensagem_pck.exibir_mensagem_abort(267057);
end if;


END;
/


ALTER TABLE TASY.PLS_TPS_REG ADD (
  CONSTRAINT PLSTPSR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TPS_REG ADD (
  CONSTRAINT PLSTPSR_PLSOTPS_FK 
 FOREIGN KEY (NR_SEQ_TPS) 
 REFERENCES TASY.PLS_TPS (NR_SEQUENCIA),
  CONSTRAINT PLSTPSR_PLSGSEG_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SEG) 
 REFERENCES TASY.PLS_GRUPO_SEGMENTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TPS_REG TO NIVEL_1;

