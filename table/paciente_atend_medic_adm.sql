ALTER TABLE TASY.PACIENTE_ATEND_MEDIC_ADM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ATEND_MEDIC_ADM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ATEND_MEDIC_ADM
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_ATENDIMENTO       NUMBER(10)           NOT NULL,
  NR_SEQ_MATERIAL          NUMBER(10)           NOT NULL,
  DT_ADMINISTRACAO         DATE                 NOT NULL,
  CD_PROFISSIONAL          VARCHAR2(10 BYTE)    NOT NULL,
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  IE_STATUS_ADM            NUMBER(3)            NOT NULL,
  DT_FIM_ADMINISTRACAO     DATE,
  CD_PROFISSIONAL_TERMINO  VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACATMA_PACATME_FK_I ON TASY.PACIENTE_ATEND_MEDIC_ADM
(NR_SEQ_ATENDIMENTO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATMA_PESFISI_FK_I ON TASY.PACIENTE_ATEND_MEDIC_ADM
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATMA_PESFISI_FK2_I ON TASY.PACIENTE_ATEND_MEDIC_ADM
(CD_PROFISSIONAL_TERMINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACATMA_PK ON TASY.PACIENTE_ATEND_MEDIC_ADM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ATEND_MEDIC_ADM_Ins
BEFORE INSERT ON TASY.PACIENTE_ATEND_MEDIC_ADM FOR EACH ROW
begin

insert into OS_1120199 values ('nr_seq_atendimento='||:new.nr_seq_atendimento||';nr_sequencia='||:new.nr_sequencia||'NM_USUARIO='||:new.NM_USUARIO||
								';cd_profissional='||:new.cd_profissional||';Stack'||substr(dbms_utility.format_call_stack,1,1700));
								end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atend_medic_adm_up
before update ON TASY.PACIENTE_ATEND_MEDIC_ADM for each row
declare

begin

if	(:new.nm_usuario <> :old.nm_usuario) or (:new.cd_profissional <> :old.cd_profissional) then

	insert into OS_1120199 values (':new.nm_usuario='||:new.nm_usuario||';:old.nm_usuario='||:old.nm_usuario
			||';:new.cd_profissional='||:new.cd_profissional||';:old.cd_profissional='||:old.cd_profissional||
			';Stack='||substr(dbms_utility.format_call_stack,1,1700));

end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.pac_atend_medic_adm_atual
before insert or update ON TASY.PACIENTE_ATEND_MEDIC_ADM for each row
declare

ie_inserir_item_adm_w	varchar2(1);
qt_reg_w	number(1);
cd_estabelecimento_w estabelecimento.cd_estabelecimento%type;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

Obter_Param_Usuario(3130,47,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_inserir_item_adm_w);

if	(ie_inserir_item_adm_w = 'N') then

	if	(:new.ie_status_adm = 1) then
		update	paciente_atend_medic
		set	ie_administracao	=	'A'
		where	nr_seq_atendimento	=	:new.nr_seq_atendimento
		and	nr_seq_material		=	:new.nr_seq_material
		and	ie_administracao 	=	'P';
	end if;

	update	paciente_atend_medic
	set	ie_checado		=	'S'
	where	nr_seq_atendimento	=	:new.nr_seq_atendimento
	and	nr_seq_material		=	:new.nr_seq_material
	and	ie_checado		=	'N';
end if;
<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PACIENTE_ATEND_MEDIC_ADM ADD (
  CONSTRAINT PACATMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ATEND_MEDIC_ADM ADD (
  CONSTRAINT PACATMA_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL_TERMINO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACATMA_PACATME_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PACIENTE_ATEND_MEDIC (NR_SEQ_ATENDIMENTO,NR_SEQ_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACATMA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PACIENTE_ATEND_MEDIC_ADM TO NIVEL_1;

