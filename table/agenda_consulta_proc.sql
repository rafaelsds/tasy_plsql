ALTER TABLE TASY.AGENDA_CONSULTA_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_CONSULTA_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_CONSULTA_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_AGENDA         NUMBER(10)              NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15)              NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  IE_EXECUTAR_PROC      VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_EXAME          NUMBER(10),
  QT_PROCEDIMENTO       NUMBER(5),
  CD_MATERIAL_EXAME     VARCHAR2(20 BYTE),
  NR_SEQ_REGRA          NUMBER(10),
  IE_REGRA              NUMBER(5),
  IE_GLOSA              VARCHAR2(1 BYTE),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE),
  IE_LADO               VARCHAR2(1 BYTE),
  DT_AGENDA_EXTERNA     DATE,
  IE_DEPENDENTE         VARCHAR2(1 BYTE)        DEFAULT null,
  DS_MATERIAL_ESPECIAL  VARCHAR2(255 BYTE),
  IE_GERADO_REGRA       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGECOPR_AGECONS_FK_I ON TASY.AGENDA_CONSULTA_PROC
(NR_SEQ_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGECOPR_EXALABO_FK_I ON TASY.AGENDA_CONSULTA_PROC
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECOPR_EXALABO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGECOPR_PK ON TASY.AGENDA_CONSULTA_PROC
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGECOPR_PROCEDI_FK_I ON TASY.AGENDA_CONSULTA_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGECOPR_PROINTE_FK_I ON TASY.AGENDA_CONSULTA_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECOPR_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AGENDA_CONSULTA_PROC_INSUPD
before insert or update ON TASY.AGENDA_CONSULTA_PROC for each row
declare

ie_solicitacao_w			varchar2(1);
ie_situacao_lab_w			varchar2(1);

begin

if 	(nvl(:old.nr_seq_exame, 0) <> nvl(:new.nr_seq_exame, 0)) and
	(:new.nr_seq_exame is not null) then
	select	nvl(max(ie_solicitacao),'N'),
		nvl(max(ie_situacao),'A')
	into	ie_solicitacao_w,
		ie_situacao_lab_w
	from	exame_laboratorio
	where	nr_seq_exame = :new.nr_seq_exame;
	if	(ie_solicitacao_w <> 'S') then
		Wheb_mensagem_pck.exibir_mensagem_abort(182653, 'NR_SEQ_EXAME=' || :new.nr_seq_exame);

	elsif   (ie_situacao_lab_w <> 'A') then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(172737, 'EXAME='||:new.nr_seq_exame);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_CONS_PROC_AFTER_ATUAL
after insert or update ON TASY.AGENDA_CONSULTA_PROC for each row
declare

ie_gerar_consent_cons_w		varchar2(1);
IE_GERAR_CONSENT_SERV_W		varchar2(1);
cd_pessoa_fisica_w			AGENDA_CONSULTA.CD_PESSOA_FISICA%TYPE;
cd_tipo_agenda_w			agenda.cd_tipo_agenda%type;
cd_agenda_w					agenda.cd_agenda%type;
cd_estab_agenda_w			agenda.cd_estabelecimento%type;
ie_status_agenda_w			AGENDA_CONSULTA.ie_status_agenda%type;
cd_medico_w					AGENDA_CONSULTA.cd_medico%type;
nr_atendimento_w            agenda_consulta.nr_atendimento%type;

begin
	begin
	SELECT ac.CD_PESSOA_FISICA,
		   ac.CD_AGENDA,
		   a.CD_TIPO_AGENDA,
		   a.CD_ESTABELECIMENTO,
		   ac.ie_status_agenda,
		   case when a.CD_TIPO_AGENDA = 3 then a.cd_pessoa_fisica else ac.cd_medico end cd_medico,
		   ac.nr_atendimento
	  INTO cd_pessoa_fisica_w,
		   cd_agenda_w,
		   cd_tipo_agenda_w,
		   cd_estab_agenda_w,
		   ie_status_agenda_w,
		   cd_medico_w,
		   nr_atendimento_w
	  FROM AGENDA_CONSULTA ac,
		   agenda a
	 WHERE ac.NR_SEQUENCIA = :new.nr_seq_agenda
	   and ac.cd_agenda = a.cd_agenda;
	exception
		when others then
		null;
	end;

Obter_Param_Usuario(821,504,obter_perfil_ativo,:new.nm_usuario,cd_estab_agenda_w,ie_gerar_consent_cons_w);
Obter_Param_Usuario(866,317,obter_perfil_ativo,:new.nm_usuario,cd_estab_agenda_w,ie_gerar_consent_serv_w);

/*
S - Agendamento ou Proc. Adicional
N - Nao gera
A - Somente Agendamento
P - Somente Proc. Adicional
*/

	if (ie_status_agenda_w = 'N' and
		cd_pessoa_fisica_w is not null and
		((:new.cd_procedimento is not null and (:new.cd_procedimento <> :old.cd_procedimento or :new.ie_origem_proced <> :old.ie_origem_proced or :old.cd_procedimento is null)) or
		 (:new.nr_seq_proc_interno is not null and (:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno or :old.nr_Seq_proc_interno is null))) and
			((ie_gerar_consent_serv_w in ('S','P') and cd_tipo_agenda_w = 5) or
			(ie_gerar_consent_cons_w = 'S' and cd_tipo_agenda_w in (3,4) ))) then

			agendas_inserir_consentimentos (
					cd_tipo_agenda_w,
					:new.nr_seq_agenda,
					:new.cd_procedimento,
					:new.ie_origem_proced,
					:new.nr_seq_proc_interno,
					cd_pessoa_fisica_w,
					cd_medico_w,
					cd_estab_agenda_w,
					nr_atendimento_w);
	end if;
end;
/


ALTER TABLE TASY.AGENDA_CONSULTA_PROC ADD (
  CONSTRAINT AGECOPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_CONSULTA_PROC ADD (
  CONSTRAINT AGECOPR_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGECOPR_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT AGECOPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGECOPR_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_CONSULTA_PROC TO NIVEL_1;

