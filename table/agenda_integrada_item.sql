ALTER TABLE TASY.AGENDA_INTEGRADA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_INTEGRADA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_INTEGRADA_ITEM
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_AGENDA_INT           NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO         NUMBER(10),
  IE_TIPO_AGENDAMENTO         VARCHAR2(15 BYTE) NOT NULL,
  CD_MEDICO                   VARCHAR2(10 BYTE),
  CD_ESPECIALIDADE            NUMBER(5),
  VL_ITEM                     NUMBER(15,2),
  IE_REGRA                    NUMBER(5),
  NR_SEQ_AGENDA_CONS          NUMBER(10),
  NR_SEQ_AGENDA_EXAME         NUMBER(10),
  CD_PROFISSIONAL_EXAME       VARCHAR2(10 BYTE),
  IE_GLOSA                    VARCHAR2(1 BYTE),
  VL_MEDICO                   NUMBER(15,2),
  VL_ANESTESISTA              NUMBER(15,2),
  VL_AUXILIARES               NUMBER(15,2),
  VL_CUSTO_OPERACIONAL        NUMBER(15,2),
  VL_MATERIAIS                NUMBER(15,2),
  NR_MINUTO_DURACAO           NUMBER(10),
  IE_CLASSIF_AGENDA           VARCHAR2(5 BYTE),
  NR_CLASSIFICACAO_AGEND      NUMBER(10),
  CD_MEDICO_REQ               VARCHAR2(10 BYTE),
  DT_ENTREGA_PREVISTA         DATE,
  IE_LADO                     VARCHAR2(1 BYTE),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  VL_LANC_AUTO                NUMBER(15,2),
  NR_SEQ_REGRA                NUMBER(10),
  IE_TIPO_EXAME_PROC          VARCHAR2(5 BYTE),
  CD_PROCEDIMENTO             NUMBER(15),
  IE_ORIGEM_PROCED            NUMBER(10),
  NR_SEQ_GRUPO_PROC           NUMBER(10),
  NR_SEQ_GRUPO_SELEC          NUMBER(10),
  IE_ANESTESIA                VARCHAR2(1 BYTE),
  IE_AUTORIZACAO              VARCHAR2(3 BYTE),
  CD_ANESTESISTA              VARCHAR2(10 BYTE),
  NR_SEQ_SALA                 NUMBER(10),
  NR_SEQ_TRANSPORTE           NUMBER(10),
  NR_SEQ_REGRA_EX_ADIC        NUMBER(10),
  NR_SEQ_PROC_PACOTE          NUMBER(10),
  DT_TRANSFERENCIA            DATE,
  IE_PACOTE                   VARCHAR2(1 BYTE),
  NR_SEQ_ITEM_SELEC           NUMBER(10),
  CD_ESTABELECIMENTO          NUMBER(4),
  NM_MEDICO_EXTERNO           VARCHAR2(60 BYTE),
  CRM_MEDICO_EXTERNO          VARCHAR2(60 BYTE),
  IE_NECESSITA_INTERNACAO     VARCHAR2(1 BYTE),
  NR_SEQ_PEND_QUIMIO          NUMBER(10),
  IE_TIPO_PEND_AGENDA         VARCHAR2(15 BYTE),
  QT_DIARIA_PREV              NUMBER(3),
  NR_SEQ_REGIAO               NUMBER(10),
  NR_SEQ_PROC_ITEM_GRUPO      NUMBER(10),
  NR_SEQ_GRUPO_QUIMIO         NUMBER(10),
  DS_DIA_CICLO                VARCHAR2(5 BYTE),
  NR_CICLO                    NUMBER(3),
  NR_SEQ_MEDICACAO            NUMBER(6),
  CD_PROTOCOLO                NUMBER(10),
  DT_PREVISTA_QUIMIO          DATE,
  IE_FORMA_AGENDAMENTO        NUMBER(3),
  IE_TIPO_ITEM                VARCHAR2(3 BYTE),
  NR_SEQ_ITEM_PRINC           NUMBER(10),
  NR_SEQ_AGEQUI               NUMBER(10),
  NR_SEQ_PACOTE               NUMBER(10),
  DT_PREVISTA_ITEM            DATE,
  DT_PREV_TRANSF_QUIMIO       DATE,
  IE_AUTOR_AGENDA_EXAME       VARCHAR2(3 BYTE),
  CD_DOENCA_CID               VARCHAR2(10 BYTE),
  DT_PREV_TRANSF_ITEM         DATE,
  NR_SEQ_AGRUPAMENTO          NUMBER(10),
  IE_DUPLICADO_ESTAB          VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_AGENDAMENTO   NUMBER(10),
  NR_SEQ_MOTIVO_ANEST         NUMBER(10),
  DS_HIPOTESE_DIAG            VARCHAR2(255 BYTE),
  NR_SEQ_LISTA                NUMBER(10),
  VL_ITEM_PARTICULAR          NUMBER(15,2),
  NR_SEQ_AREA_ATUACAO         NUMBER(10),
  IE_GENERO_PROF              VARCHAR2(1 BYTE),
  NR_PRESCRICAO               NUMBER(10),
  NR_SEQUENCIA_PRESCRICAO     NUMBER(10),
  NR_SEQ_AGRUP_SETOR          NUMBER(10),
  CD_MEDICO_PREV_LAUDO        VARCHAR2(10 BYTE),
  DS_CIRURGIA                 VARCHAR2(500 BYTE),
  NR_SEQ_SOLIC_COMPL          NUMBER(10),
  DS_SENHA                    VARCHAR2(20 BYTE),
  DT_SOLIC_EXAME              DATE,
  NR_DOC_CONVENIO             VARCHAR2(20 BYTE),
  IE_AUTOR_RECEP              VARCHAR2(1 BYTE),
  IE_PAC_POSSUI_AUTOR         VARCHAR2(1 BYTE),
  DT_VALIDADE_SENHA           DATE,
  IE_GERADO_ORC               VARCHAR2(1 BYTE),
  DT_RESULTADO                DATE,
  DT_VALIDADE_GUIA            DATE,
  IE_CARATER_CIRURGIA         VARCHAR2(1 BYTE),
  NR_SEQ_ATENDIMENTO          NUMBER(10),
  NR_SEQ_PARTIC_CICLO_ITEM    NUMBER(10),
  NR_SEQ_CAPTACAO             NUMBER(10),
  DS_COR                      VARCHAR2(255 BYTE),
  NR_SEQ_AGENDA_ANESTESISTA   NUMBER(10),
  NR_SEQ_CIRURGIA             NUMBER(10)        DEFAULT null,
  NR_SEQ_PAC_TIPO_ACOMOD      NUMBER(10),
  IE_GESTAO_EXAME             VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_AGENDA       NUMBER(10)        DEFAULT null,
  NR_SEQ_LISTA_ESPERA         NUMBER(10),
  NR_SEQ_AGEINT_ITEM_TRANSF   NUMBER(10),
  IE_ORIGEM_AGENDAMENTO       VARCHAR2(2 BYTE),
  NR_SEQ_CONTRATO             NUMBER(10),
  IE_RODIZIO_ESPECIALIDADE    VARCHAR2(5 BYTE)  DEFAULT null,
  NR_SEQ_LISTA_ESPERA_ORIGEM  NUMBER(10),
  NR_SEQ_DADOS_URA            NUMBER(10),
  NR_SEQ_MULTIRAO             NUMBER(10),
  NR_SEQ_PEDIDO_EXTERNO       NUMBER(10),
  IE_RETORNO                  VARCHAR2(1 BYTE),
  NR_SEQ_PROC_CPOE            NUMBER(10),
  NR_SEQ_TOPOGRAFIA           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEINIT_AGDAURA_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_DADOS_URA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGECONS_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGENDA_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGEINCA_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_CLASSIFICACAO_AGEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGEINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGEINGR_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_GRUPO_SELEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGEINGR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGEINTE_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGENDA_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGEPACI_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGENDA_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGEPACI_FK2_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGENDA_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGEPACI_FK3_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGEPACL_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_CLASSIF_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGEQUIM_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGEQUI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGINTCO_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGIPPCT_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_PROC_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGIPPCT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGLIESP_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_LISTA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_AGMOTAN_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_MOTIVO_ANEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGMOTAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGREGTR_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_TRANSPORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGREGTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGRUAGE_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGRUAGE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGRUSET_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGRUP_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGRUSET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGSACON_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_SALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGSACON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AGTRGAT_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AGTRGAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_AREATUM_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AREA_ATUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_AREATUM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_CIDDOEN_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_CPOEPRO_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_PROC_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_ESPARAT_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_ESPARAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_ESTABEL_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_I1 ON TASY.AGENDA_INTEGRADA_ITEM
(IE_TIPO_AGENDAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_I1
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_I2 ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_ITEM_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_I2
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_I3 ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGEINT_ITEM_TRANSF, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_I4 ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGENDA_EXAME, IE_ORIGEM_AGENDAMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_I5 ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_AGENDA_CONS, IE_ORIGEM_AGENDAMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_MOTVAGD_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_MOTIVO_AGENDAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_MOTVAGD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_MPRCAPT_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_MPRPACI_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_PARTIC_CICLO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_MUTILES_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_MULTIRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PACATEN_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PESFISI_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PESFISI_FK2_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_PROFISSIONAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PESFISI_FK3_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_MEDICO_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PESFISI_FK4_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PESFISI_FK5_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_MEDICO_PREV_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEINIT_PK ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEINIT_PRESPRO_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_PRESCRICAO, NR_SEQUENCIA_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_PRESPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_PROCEDI_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_PROINTE_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_QTGRIAG_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_GRUPO_QUIMIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_QTGRIAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_QTPENAG_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_PEND_QUIMIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_QTPENAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_RPLIESM_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_LISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_RPLIESM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_SOEXCOM_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_SOLIC_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEINIT_SOEXCOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEINIT_TOPDOR_FK_I ON TASY.AGENDA_INTEGRADA_ITEM
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.agenda_integrada_item_atual
before insert or update ON TASY.AGENDA_INTEGRADA_ITEM FOR EACH ROW
DECLARE

qt_pontos_w			  preco_amb.qt_pontuacao%type;
cd_estabelecimento_w	Number(4);
cd_convenio_w		Number(5);
cd_categoria_w		Varchar2(10);
dt_inicio_agendamento_w	Date;
cd_plano_w		Varchar2(10);
vl_procedimento_w		Number(15,2);
vl_aux_w			Number(15,4);
ds_aux_w		Varchar2(10);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
cd_convenio_ww		Number(5);
cd_categoria_ww		Varchar2(10);
cd_plano_ww		Varchar2(10);
ie_tipo_convenio_w	Number(3);
ie_regra_w		Number(5);
ie_glosa_w		Varchar2(1);
cd_usuario_convenio_w	Varchar2(30);
qt_idade_w		Number(3);
dt_nascimento_w		Date;
ie_Sexo_w		Varchar2(1);
cd_pessoa_fisica_w	Varchar2(10);
CD_EDICAO_AMB_w         Number(6);
VL_LANC_AUTOM_W         Number(15,2);
ie_calcula_lanc_auto_w	Varchar2(1);
ie_atualiza_medico_req_w Varchar2(1);
vl_custo_operacional_w	Number(15,2);
vl_anestesista_w		Number(15,2);
vl_medico_w		Number(15,2);
vl_auxiliares_w		Number(15,2);
vl_materiais_w		Number(15,2);
cd_medico_item_w	Varchar2(10);
ie_tipo_Atendimento_w	Number(3);
nr_seq_ageint_w		number(10);
cd_conv_item_w		number(5);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
nr_seq_regra_w		number(10);
nr_minuto_duracao_w	number(10);
ie_resp_autor_w		varchar2(10);
cd_medico_solicitante_w varchar2(10);

ie_edicao_w                  varchar2(1);
cd_edicao_ajuste_w      number(10);
qt_item_edicao_w         number(10);
cd_estab_agenda_w	number(5);
ie_perm_agenda_w	varchar2(1)	:= 'S';
qt_estab_user_w		number(10);
ie_estab_usuario_w	varchar2(1);
nm_paciente_w		varchar2(60);
cd_paciente_agenda_w	varchar2(10);
nm_paciente_agenda_w	varchar2(60);
ie_pacote_w		varchar2(1);
ie_inserir_prof_w	varchar2(1);
ds_erro_w		varchar2(255);
dt_nascimento_Ww	date;
ie_utiliza_regra_tempo_w varchar2(1);
nr_minuto_duracao_ww	number(10);
ie_bloq_glosa_part_w	varchar2(1);
nr_seq_cobertura_w	number(10,0);
ie_calc_glosa_atend_w	Varchar2(1);
ie_consiste_anest_w	Varchar2(1);
cd_convenio_glosa_w	Number(10);
cd_categoria_glosa_w	Varchar2(10);
ie_conv_cat_regra_w	varchar2(1);

cd_convenio_part_w	number(5,0);
cd_categoria_part_w	varchar2(10);
vl_procedimento_part_w	Number(15,2);
ie_vl_particular_w	varchar2(1);
nr_seq_ajuste_proc_w	number(10,0);
cd_paciente_w		varchar2(10);
ie_multimed_w		varchar2(1);
cd_empresa_w		agenda_integrada.cd_empresa%type;
ie_dia_semana_w		number(001,0);

pr_glosa_w			number(15,4);
vl_glosa_w			number(15,4);
ie_regra_arredondamento_tx_w	varchar2(1):= 'N';
ie_tipo_rounded_w		varchar2(1);
ie_regra_arred_IPE_w		varchar2(1):= 'N';
IE_CALCULA_GLOSA_w			parametro_Agenda_integrada.ie_calcula_glosa%type;
ie_return_w varchar2(1);
ie_param_422_w  varchar2(1);
ie_forma_agendamento_w	agenda_integrada.ie_forma_agendamento%type;
nr_seq_agrupamento_w	agrupamento_agenda.nr_sequencia%type;
ie_ignorar_regra_lib_proc_w	parametro_agenda_integrada.ie_ignorar_regra_lib_proc%type;
QT_REG_W		number(1);
ie_param_444_w               VARCHAR2(1) := '';
ie_excluir_valor             VARCHAR2(1) := 'N';
NR_SEQUENCIA_URA_W AGEINT_DADOS_URA.NR_SEQUENCIA%TYPE;
dt_agenda_w		date;
vl_item_ageint_w number(15,4);

Cursor C01 is
	select	a.cd_medico,
		c.cd_estabelecimento,
		ageint_obter_se_regra_med_ex(a.cd_medico, c.cd_Agenda, :new.nr_seq_proc_interno, :new.nr_seq_agenda_int, cd_procedimento_w, ie_origem_proced_w)
	from    agenda c,
		pessoa_fisica b,
		agenda_medico a
	where   Obter_Se_Ageint_Item_Trigger(a.cd_agenda, :new.nr_seq_agenda_int, c.cd_estabelecimento, :new.nr_seq_proc_interno, :new.cd_Especialidade,

:new.cd_medico, a.cd_medico, cd_procedimento_w, ie_origem_proced_w,:new.nr_sequencia, qt_idade_w) = 'S'
	and     a.cd_medico    			= b.cd_pessoa_fisica
	and	c.cd_agenda			= a.cd_agenda
	and	nvl(c.ie_agenda_integrada,'N')	= 'S'
	and     a.ie_situacao  			= 'A'
	and	c.ie_situacao			= 'A'
	and	c.cd_tipo_Agenda = 2
	and	((qt_idade_w	>= nvl(qt_idade_min,0)
	and	qt_idade_w	<= nvl(qt_idade_max,999))
	or	qt_idade_w = 0);

BEGIN

if wheb_usuario_pck.get_ie_executar_trigger = 'N' then
    goto finalize;
end if;

obter_param_usuario(869, 264, obter_perfil_ativo, :new.nm_usuario, 0, ie_consiste_anest_w);
obter_param_usuario(869, 297, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_conv_cat_regra_w);
obter_param_usuario(869, 331, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_vl_particular_w);
obter_param_usuario(0, 198, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_multimed_w);
obter_param_usuario(869, 444, obter_perfil_ativo, :new.nm_usuario, 0, ie_param_444_w);

select	nvl(max(IE_CALCULA_GLOSA),'N'),
	nvl(max(IE_IGNORAR_REGRA_LIB_PROC),'N')
into	IE_CALCULA_GLOSA_w,
	ie_ignorar_regra_lib_proc_w
from	parametro_Agenda_integrada
where	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento)	= wheb_usuario_pck.get_cd_estabelecimento;

if (ie_consiste_anest_w = 'S') then
	if	(nvl(:new.ie_anestesia,'N')	= 'S') and
		(nvl(:new.nr_seq_proc_interno,0) > 0) then
		select	nvl(max(nr_minuto_duracao),0)
		into	nr_minuto_duracao_w
		from	ageint_tempo_exame_anest
		where	nr_seq_proc_interno	= :new.nr_seq_proc_interno
		and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;

		if	(nr_minuto_duracao_w	> 0) then
			:new.nr_minuto_duracao	:= nr_minuto_duracao_w;
		end if;
	end if;
end if;

select	nvl(max(Obter_Valor_Param_Usuario(869, 7, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'S'),
	nvl(max(Obter_Valor_Param_Usuario(869, 81, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'N'),
	nvl(max(Obter_Valor_Param_Usuario(869, 178, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'N')
into	ie_calcula_lanc_auto_w,
	ie_atualiza_medico_req_w,
	ie_utiliza_regra_tempo_w
from 	dual;

obter_param_usuario(869, 187, obter_perfil_ativo, :new.nm_usuario, 0, ie_bloq_glosa_part_w);
obter_param_usuario(869, 262, obter_perfil_ativo, :new.nm_usuario, 0, ie_calc_glosa_atend_w);
obter_param_usuario(869, 422, obter_perfil_ativo, :new.nm_usuario, 0, ie_param_422_w);

begin
select	cd_estabelecimento,
	cd_convenio,
	cd_categoria,
	dt_inicio_agendamento,
	cd_plano,
	cd_usuario_convenio,
	cd_pessoa_fisica,
	ie_tipo_Atendimento,
	dt_nascimento,
	nr_sequencia,
	nm_paciente,
	cd_medico_solicitante,
	cd_pessoa_fisica,
	cd_empresa,
	ie_forma_agendamento
into	cd_estabelecimento_w,
	cd_convenio_ww,
	cd_categoria_ww,
	dt_inicio_agendamento_w,
	cd_plano_ww,
	cd_usuario_convenio_w,
	cd_pessoa_fisica_w,
	ie_tipo_Atendimento_w,
	dt_nascimento_w,
	nr_seq_ageint_w,
	nm_paciente_w,
	cd_medico_solicitante_w,
	cd_paciente_w,
	cd_empresa_w,
	ie_forma_agendamento_w
from	agenda_integrada
where	nr_sequencia	= :new.nr_seq_agenda_int;
exception
when others then
	nr_seq_ageint_w	:= null;
end;

ie_dia_semana_w := obter_cod_dia_semana(nvl(Obter_Horario_item_Ageint(:new.nr_seq_agenda_cons,:new.nr_seq_agenda_exame,:new.nr_sequencia),qt_obter_horario_agendado(:new.nr_sequencia)));

if	(nvl(ie_utiliza_regra_tempo_w,'N') = 'S') then
	Obter_Tempo_Padrao_Ageint(
					:new.nr_seq_proc_interno,
					null,
					null,
					null,
					null,
					cd_pessoa_fisica_w,
					nr_minuto_duracao_ww,
					:new.ie_lado,
					cd_convenio_ww,
					cd_categoria_ww,
					cd_plano_ww,
					null,
					ie_dia_semana_w);
end if;

if 	(inserting) and
	(ie_forma_agendamento_w is not null) and
	(:old.ie_forma_agendamento is null) and
	(:new.ie_forma_agendamento is null) then
	:new.ie_forma_agendamento := ie_forma_agendamento_w;
end if;

if	(nvl(ie_utiliza_regra_tempo_w,'N') = 'S') and
	(nvl(:new.nr_seq_proc_interno,0) > 0) and
	(nvl(nr_minuto_duracao_ww,0) > 0) and
	(nvl(:new.nr_minuto_duracao,0)	= 0) then
	begin
	:new.nr_minuto_duracao	:= nr_minuto_duracao_ww;
	end;
end if;

if	((:new.nr_seq_agenda_exame is not null) and
	(:old.nr_seq_agenda_exame is null)) or
	((:old.nr_seq_agenda_exame is not null) and
	(:new.nr_seq_agenda_exame <> :old.nr_seq_agenda_exame)) then
	select	max(cd_pessoa_fisica),
		max(nm_paciente)
	into	cd_paciente_agenda_w,
		nm_paciente_agenda_w
	from	agenda_paciente
	where	nr_sequencia	= :new.nr_seq_agenda_exame;

	if	((cd_pessoa_fisica_w is not null) and
		(cd_pessoa_fisica_w	<> cd_paciente_Agenda_w)) or
		((nm_paciente_w is not null) and
		(cd_pessoa_fisica_w is null) and
		(nm_paciente_w <> nm_paciente_agenda_w)) then
		ageint_gravar_log_trigger(:new.nr_seq_agenda_int,
								:new.nr_sequencia,
								:new.nr_seq_agenda_exame,
								'N',
								wheb_mensagem_pck.get_texto(306959, 'CD_PESSOA_FISICA_W=' || cd_pessoa_fisica_w || ';' ||
																	'NM_PACIENTE_W=' || nm_paciente_w || ';' ||
																	'CD_PACIENTE_AGENDA_W=' || cd_paciente_agenda_w || ';' ||
																	'NM_PACIENTE_AGENDA_W=' || nm_paciente_agenda_w || ';' ||
																	'DT_ATUAL=' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || ';' ||
																	'NM_USUARIO=' || :new.nm_usuario),
								-- Agenda exame-codigo ag int:#@CD_PESSOA_FISICA_W#@,nome ag int:#@NM_PACIENTE_W#@/codigo ag exame:#@CD_PACIENTE_AGENDA_W#@,nome ag exame:#@NM_PACIENTE_AGENDA_W#@, data:#@DT_ATUAL#@, usuario:#@NM_USUARIO#@
								:new.nm_usuario);
	end if;
end if;

ie_estab_usuario_w	:= nvl(Obter_Valor_Param_Usuario(869, 1, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w), 'S');

select	max(cd_convenio),
	max(cd_categoria),
	max(cd_plano)
into	cd_conv_item_w,
	cd_categ_item_w,
	cd_plano_item_w
from	agenda_integrada_conv_item
where	nr_seq_agenda_item	= :new.nr_sequencia;

if	(cd_conv_item_w is not null) then
	cd_convenio_ww	:= cd_conv_item_w;
	cd_categoria_ww	:= cd_categ_item_w;
	cd_plano_ww	:= cd_plano_item_w;
end if;

if	(:new.cd_estabelecimento is not null) then
	cd_estabelecimento_w	:= :new.cd_estabelecimento;
end if;

cd_procedimento_w	:= :new.cd_procedimento;
ie_origem_proced_w	:= :new.ie_origem_proced;

if	(nr_seq_ageint_w is not null) and
	(nvl(:new.ie_pacote, 'N')	= 'N') then
	if	(cd_pessoa_fisica_w is not null) then
		select	max(ie_Sexo),
			max(dt_nascimento)
		into	ie_Sexo_w,
			dt_nascimento_ww
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
	end if;
	if	(dt_nascimento_W is null) then
		dt_nascimento_W	:= dt_nascimento_Ww;
	end if;

	qt_idade_w	:= obter_idade(dt_nascimento_w, sysdate, 'A');

	select	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio	= cd_convenio_ww;


	if      (:new.nr_seq_proc_interno is not null) and
		((:old.nr_seq_proc_interno is null) or
		(:old.cd_procedimento <> :new.cd_procedimento) or
		((:old.nr_seq_proc_interno is not null) and
		(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno))) then

		obter_proc_tab_interno_conv(
						:new.nr_seq_proc_interno,
						cd_estabelecimento_w,
						cd_convenio_ww,
						cd_categoria_ww,
						cd_plano_ww,
						null,
						cd_procedimento_w,
						ie_origem_proced_w,
						null,
						sysdate,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);

		SELECT 	nvl(max(1),0)
		into	qt_reg_w
		FROM 	procedimento
		where 	cd_procedimento = cd_procedimento_w
		and	ie_origem_proced = ie_origem_proced_w;
		if (qt_reg_w > 0) then
			:new.cd_procedimento	:= cd_procedimento_w;
			:new.ie_origem_proced	:= ie_origem_proced_w;
		end if;
	end if;

	select	max(nr_seq_cobertura)
	into	nr_seq_cobertura_w
	from	agenda_integrada
	where	nr_sequencia = :new.nr_seq_agenda_int;

	if (:new.nr_seq_agenda_cons is not null) then

		select max(dt_agenda)
		into dt_agenda_w
		from agenda_consulta
	   where nr_sequencia = :new.nr_seq_agenda_cons;

	elsif (:new.nr_seq_agenda_exame is not null) then

	  select max(hr_inicio)
		into dt_agenda_w
		from agenda_paciente
	   where nr_sequencia = :new.nr_seq_agenda_exame;

	end if;
	if  (:new.nr_seq_proc_interno is not null) and
		((((:old.nr_seq_proc_interno is null) or
		((:old.nr_seq_proc_interno is not null) and
		(:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno))) or
		(((:old.cd_estabelecimento is null) and
		(:new.cd_estabelecimento is not null)) or
		((:old.cd_estabelecimento is not null) and
		(:old.cd_estabelecimento <> :new.cd_estabelecimento))) or
		((:new.dt_transferencia is not null) and
		((:old.dt_transferencia is null) or
		((:old.dt_transferencia is not null) and
		(:new.dt_transferencia	<> :old.dt_Transferencia))))) or
		((nvl(:old.nr_seq_agenda_cons,0) <> nvl(:new.nr_seq_agenda_cons,0)) or
		(nvl(:old.nr_seq_agenda_exame,0) <> nvl(:new.nr_seq_agenda_exame,0)))) then


		ie_return_w := ageint_obter_se_proc_inter(:new.nr_seq_proc_interno, dt_nascimento_W);

		if (ie_param_422_w = 'N') or
			(ie_return_w <> 'X') then

			if (ie_return_w = 'X') then
				:new.ie_necessita_internacao	:= 'N';
			else
				:new.ie_necessita_internacao	:= ie_return_w;
			end if;

		end if;

		/* Restricao de localidade colocado pois para a alemanha nao deve considerar consistencia dos planos ao gerar a agenda SO-1730150*/
		if	((((cd_conv_item_w is not null) and (ie_multimed_w = 'S')) or (ie_multimed_w = 'N')) and (nvl((pkg_i18n.get_user_locale), 'pt_BR') not in ('de_DE', 'de_AT', 'ja_JP'))) then
			ageint_consiste_plano_conv(
						null,
						cd_convenio_ww,
						cd_procedimento_w,
						ie_origem_proced_w,
						sysdate,
						1,
						nvl(ie_tipo_Atendimento_w,0),
						cd_plano_ww,
						null,
						ds_erro_w,
						0,
						null,
						ie_regra_w,
						null,
						nr_seq_regra_w,
						:new.nr_Seq_proc_interno,
						cd_categoria_ww,
						cd_estabelecimento_w,
						null,
						ie_Sexo_w,
						ie_glosa_w,
						cd_edicao_ajuste_w,
						nr_seq_cobertura_w,
						cd_convenio_glosa_w,
						cd_categoria_glosa_w,
						cd_paciente_w,
						cd_empresa_w,
						pr_glosa_w,
						vl_glosa_w);



			ie_edicao_w	:= ageint_obter_se_proc_conv(cd_estabelecimento_w, cd_convenio_ww, cd_categoria_ww, sysdate, cd_procedimento_W, ie_origem_proced_w, :new.nr_Seq_proc_interno, ie_tipo_atendimento_w);

			ie_pacote_w	:= obter_Se_pacote_convenio(cd_procedimento_w, ie_origem_proced_w, cd_convenio_ww, cd_estabelecimento_w);


			if	(ie_edicao_w 				= 'N') and
				(nvl(cd_edicao_ajuste_w,0) 	= 0) and
				(nvl(ie_glosa_w,'L') 		= 'L') and
				(ie_pacote_w				= 'N') then
				ie_glosa_w        			:= 'T';
			end if;


			if	(ie_edicao_w 				= 'N') and
				(nvl(cd_edicao_ajuste_w,0) 	> 0) and
				(nvl(ie_glosa_w,'L') 		= 'L') and
				(ie_pacote_w				= 'N') then

				select   count(*)
				into     qt_item_edicao_w
				from     preco_amb
				where    cd_edicao_amb = cd_edicao_ajuste_w
				and      cd_procedimento = cd_procedimento_w
				and      ie_origem_proced = ie_origem_proced_w;

				if	(qt_item_edicao_w = 0) then
					    ie_glosa_w :=    'G';
				end if;

			end if;

			:new.ie_autorizacao	:= 'L';
			if	((ie_Regra_w in (1,2,5)) or
				((ie_Regra_w = 8) and (ie_bloq_glosa_part_w = 'N'))) then
				:new.ie_autorizacao	:= 'B';
			elsif	(ie_Regra_w in (3,6,7)) then
				select 	nvl(max(ie_resp_autor),'H')
				into	ie_resp_autor_w
				from 	regra_convenio_plano
				where 	nr_sequencia = nr_seq_regra_w;
				if	(ie_resp_autor_w	= 'H') then
					:new.ie_autorizacao	:= 'PAH';
				elsif	(ie_resp_autor_w	= 'P') then
					:new.ie_autorizacao	:= 'PAP';
				end if;
			end if;

			if	(ie_glosa_w in ('G','T','D','F')) then
				:new.ie_autorizacao	:= 'B';
			end if;

			if 	ie_glosa_w = 'E' and
				nvl(ie_param_444_w,'N') = 'N' and
				ie_tipo_convenio_w = '1' then
				ie_excluir_valor := 'S';
			end if;

			IF (ie_excluir_valor = 'S') OR
				(((((nvl(ie_Regra_w,0) not in (1,2,5)) or
				(nvl(ie_glosa_w,'') not in ('T','E','R','B','H','Z',''))) and
				(ie_tipo_convenio_w <> 1) and
				(ie_calc_glosa_atend_w = 'N')) or
				((ie_calc_glosa_atend_w = 'S') 	and
				 (ie_tipo_convenio_w <> 1) 	and
				 ((nvl(ie_glosa_w,'') not in ('T','G','P')) or (ie_glosa_w is null)))) and
				((IE_CALCULA_GLOSA_w = 'N') or
				((IE_CALCULA_GLOSA_w = 'S') and
				(nvl(ie_glosa_w,'') not in ('P','R') or (ie_glosa_w is null))))) then
				vl_procedimento_w	:= 0;

			else
				if	(ie_glosa_w	in ('P','R') and
					IE_CALCULA_GLOSA_w = 'S') then
					Define_Preco_Procedimento(
						CD_ESTABELECIMENTO_w,
						cd_convenio_ww,
						cd_categoria_ww,
						nvl(dt_agenda_w,nvl(:new.dt_transferencia,dt_inicio_agendamento_w)),
						CD_PROCEDIMENTO_w,
						0,
						nvl(ie_tipo_Atendimento_w,0),
						0,
						:new.cd_medico,--medico
						0,
						0,
						0,
						:new.nr_seq_proc_interno,
						null,--usuario convenio
						cd_plano_w,
						0,
						0,
						null,
						VL_PROCEDIMENTO_w,
						vl_custo_operacional_w,
						vl_anestesista_w,
						vl_medico_w,
						vl_auxiliares_w,
						vl_materiais_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						qt_pontos_w,
						CD_EDICAO_AMB_w,
						ds_aux_w,
						nr_seq_ajuste_proc_w,
						0,
						null,
						0,
						'N',
						null,
						null,
						null,
						null,
						null,
						:new.cd_especialidade,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);

					if	(ie_glosa_w = 'P') then
						vl_glosa_w:= vl_procedimento_w * pr_glosa_w / 100;

						/* ROTINA DE ARREDONDAMENTO, USADO PELO CONVeNIO IPE   --->>    INICIO  <<----- */
						begin
						select 	nvl(max(ie_regra_arredondamento_tx),'N')
						into	ie_regra_arredondamento_tx_w
						from 	parametro_faturamento
						where 	cd_estabelecimento = cd_estabelecimento_w;
						exception
							when others then
								ie_regra_arredondamento_tx_w:= 'N';
						end;

						if	(ie_regra_arredondamento_tx_w = 'S')then

							select	max(ie_arredondamento)
							into	ie_tipo_rounded_w
							from	convenio_estabelecimento
							where	cd_convenio	  	= cd_convenio_w
							and	cd_estabelecimento	= cd_estabelecimento_w;

							if	(ie_tipo_rounded_w = 'R') then

								select 	obter_regra_arredondamento(cd_convenio_ww, cd_categoria_ww, cd_procedimento_w, ie_origem_proced_w, cd_estabelecimento_w,
										nvl(sysdate,sysdate), 'P', 1)
								into	ie_tipo_rounded_w
								from 	dual;

								ie_regra_arred_IPE_w:= 'S';

							end if;

							if	(ie_tipo_rounded_w is not null) and
								(ie_regra_arred_IPE_w = 'S') then

								arredondamento(vl_glosa_w, 2, ie_tipo_rounded_w);

							else
								ie_regra_arred_IPE_w:= 'N';
							end if;

						end if;
						if	(vl_glosa_w	> 0)  then
							vl_procedimento_w	:= vl_procedimento_w - vl_glosa_w;
						end if;
					else
						vl_procedimento_w:= vl_glosa_w;
					end if;
				else
					if	(ie_tipo_convenio_w <> 1) then
						select	max(cd_convenio_partic),
							max(cd_categoria_partic)
						into	cd_convenio_w,
							cd_categoria_w
						from	parametro_faturamento
						where	cd_estabelecimento	= cd_estabelecimento_w;

						if 	(ie_conv_cat_regra_w = 'S') and
							(nvl(cd_convenio_glosa_w, 0) <> 0) then
							cd_convenio_w	:= nvl(cd_convenio_glosa_w,cd_convenio_w);
							cd_categoria_w	:= nvl(cd_categoria_glosa_w,cd_categoria_w);
						end if;

						if	(cd_convenio_ww is not null) then
							select	max(cd_plano)
							into	cd_plano_w
							from	convenio_plano
							where	cd_convenio	= cd_convenio_w
							and	ie_situacao	= 'A';
						end if;
					end if;

					if	(cd_convenio_w  is null) then
						cd_convenio_w	:= cd_convenio_ww;
						cd_Categoria_w	:= cd_categoria_ww;
						cd_plano_w	:= cd_plano_ww;
					end if;
						Define_Preco_Procedimento(
							CD_ESTABELECIMENTO_w,
							cd_convenio_w,
							cd_categoria_w,
							nvl(dt_agenda_w,nvl(:new.dt_transferencia,dt_inicio_agendamento_w)),
							CD_PROCEDIMENTO_w,
							0,
							nvl(ie_tipo_Atendimento_w,0),
							0,
							:new.cd_medico,--medico
							0,
							0,
							0,
							:new.nr_seq_proc_interno,
							null,--usuario convenio
							cd_plano_w,
							0,
							0,
							null,
							VL_PROCEDIMENTO_w,
							vl_custo_operacional_w,
							vl_anestesista_w,
							vl_medico_w,
							vl_auxiliares_w,
							vl_materiais_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							vl_aux_w,
							qt_pontos_w,
							CD_EDICAO_AMB_w,
							ds_aux_w,
							nr_seq_ajuste_proc_w,
							0,
							null,
							0,
							'N',
							null,
							null,
							null,
							null,
							null,
							:new.cd_especialidade,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null);


					if ie_excluir_valor = 'S' then
						vl_lanc_autom_w := 0;
					else
						if	(ie_calcula_lanc_auto_w	<> 'N') then
							--vl_lanc_autom_w	:= obter_valor_regra_lanc_aut(cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, cd_procedimento_w, ie_origem_proced_w,:new.nr_seq_proc_interno, 34, 'T');
							ageint_Calcular_Lanc_Aut(cd_estabelecimento_w,
											cd_convenio_w,
											cd_categoria_w,
											sysdate,
											null,
											cd_procedimento_w,
											ie_origem_proced_w,
											null,
											null,
											qt_idade_w,
											null,
											null,
											:new.nr_seq_proc_interno,
											ie_tipo_atendimento_w,
											null,
											cd_usuario_convenio_w,
											cd_plano_w,
											null,
											null,
											4,
											null,
											cd_edicao_amb_w,
											vl_lanc_autom_w);
						end if;
					end if;
				end if;
			end if;
			if ie_excluir_valor = 'S' then
				:new.vl_item := 0;
			else
				if	(nvl(:new.ie_gerado_orc,'N') = 'N') then
					if	(ie_calcula_lanc_auto_w = 'S') then
						:new.vl_item			:= vl_procedimento_w + nvl(VL_LANC_AUTOM_W,0);
					else
						:new.vl_item			:= vl_procedimento_w;
					end if;
				end if;
			end if;

			:new.ie_Regra			:= ie_Regra_w;
			:new.ie_glosa			:= ie_glosa_w;

			if	(ie_ignorar_regra_lib_proc_w = 'S') and
				(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'es_MX') then
				begin
				:new.ie_Regra			:= 4;
				:new.ie_glosa			:= 'L';
				:new.ie_autorizacao		:= 'L';
				end;
			end if;

			:new.vl_custo_operacional	:= vl_custo_operacional_w;
			:new.vl_anestesista		:= vl_anestesista_w;
			:new.vl_medico			:= vl_medico_w;
			:new.vl_auxiliares		:= vl_auxiliares_w;
			:new.vl_materiais		:= vl_materiais_w;
			:new.vl_lanc_auto		:= VL_LANC_AUTOM_W;
			:new.nr_seq_regra		:= nr_seq_regra_w;


			if	(ie_vl_particular_w = 'S') then

				obter_convenio_particular(cd_estabelecimento_w, cd_convenio_part_w, cd_categoria_part_w);

				if ie_excluir_valor = 'S' then
					vl_procedimento_part_w := 0;
				else
					Define_Preco_Procedimento(
						CD_ESTABELECIMENTO_w,
						cd_convenio_part_w,
						cd_categoria_part_w,
						nvl(dt_agenda_w,nvl(:new.dt_transferencia,dt_inicio_agendamento_w)),
						CD_PROCEDIMENTO_w,
						0,
						nvl(ie_tipo_Atendimento_w,0),
						0,
						:new.cd_medico,--medico
						0,
						0,
						0,
						:new.nr_seq_proc_interno,
						null,--usuario convenio
						null,
						0,
						0,
						null,
						VL_PROCEDIMENTO_part_w,
						vl_custo_operacional_w,
						vl_anestesista_w,
						vl_medico_w,
						vl_auxiliares_w,
						vl_materiais_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						vl_aux_w,
						qt_pontos_w,
						CD_EDICAO_AMB_w,
						ds_aux_w,
						nr_seq_ajuste_proc_w,
						0,
						null,
						0,
						'N',
						null,
						null,
						null,
						null,
						null,
						:new.cd_especialidade,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);
				end if;

				:new.vl_item_particular	:= VL_PROCEDIMENTO_part_w;
			end if;
		end if;
	end if;

	if	(:new.ie_tipo_agendamento = 'E') then
		delete 	ageint_medico_item
		where	nr_seq_item	= :new.nr_sequencia;

		open C01;
		loop
		fetch C01 into
			cd_medico_item_w,
			cd_estab_agenda_w,
			ie_inserir_prof_w;
		exit when C01%notfound;
			begin
			ie_perm_agenda_w := 'S';
			if	(ie_estab_usuario_w	= 'S') or
				(ie_estab_usuario_w	= 'C') then
				if	(cd_estab_agenda_w	<> cd_estabelecimento_w) then
					ie_perm_agenda_w	:= 'N';
				end if;
			elsif	(ie_estab_usuario_w	= 'N') or
				(ie_estab_usuario_w	= 'T') then
				select	count(*)
				into	qt_estab_user_w
				from	usuario_estabelecimento
				where	nm_usuario_param	= :new.nm_usuario
				and	cd_estabelecimento	= cd_estab_agenda_w;
				if	(qt_estab_user_w	= 0) then
					ie_perm_agenda_w	:= 'N';
				end if;
			end if;
			if	(ie_perm_agenda_w	= 'S') then
				insert into ageint_medico_item
					(nr_sequencia,
					nr_seq_item,
					cd_pessoa_fisica,
					nm_usuario,
					dt_Atualizacao,
					nm_usuario_nrec,
					dt_Atualizacao_nrec,
					ie_inserir_prof)
				values
					(ageint_medico_item_seq.nextval,
					:new.nr_sequencia,
					cd_medico_item_w,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					sysdate,
					ie_inserir_prof_w);
			end if;
			end;
		end loop;
		close C01;
	end if;

	if	(:old.nr_seq_proc_interno is not null) and
		(:new.nr_seq_proc_interno	<> :old.nr_seq_proc_interno) then
		delete	ageint_exame_Associado
		where	nr_Seq_Ageint_item	= :new.nr_seq_proc_interno;
	end if;

	if	(:old.nr_sequencia is not null) and
		(:new.nr_seq_proc_interno	<> :old.nr_seq_proc_interno) then
		delete 	ageint_exame_associado
		where	nr_seq_ageint_item	= :new.nr_Sequencia;
	end if;

	if	(ageint_obter_se_colonoscopia(:new.nr_seq_proc_interno,:new.nr_seq_agenda_int, cd_estabelecimento_w) = 'S') then
		:new.ie_tipo_exame_proc	:= 'C';
	end if;


	 if	(((:old.dt_transferencia is not null) and
		(:new.dt_transferencia <> :old.dt_transferencia)) or
		((:old.dt_transferencia is null) and
		(:new.dt_transferencia is not null))) and :old.nr_sequencia is not null	then

         insert into ageint_valor_hist(  nr_sequencia,
                                         dt_atualizacao,
                                         nm_usuario,
                                         dt_atualizacao_nrec,
                                         nm_usuario_nrec,
                                         vl_item,
                                         nr_seq_ageint_item,
                                         dt_historico)
                                 values (ageint_valor_hist_seq.nextval,
                                         sysdate,
                                         :new.nm_usuario,
                                         sysdate,
                                         :new.nm_usuario,
                                         nvl(:old.vl_item,0),
                                         :new.nr_sequencia,
                                         sysdate);
	end if;

	if	(ie_atualiza_medico_req_w = 'S') and
		(:new.ie_tipo_agendamento <> 'C') and
		(:new.cd_medico_req is null) and
		(cd_medico_solicitante_w is not null) then
		:new.cd_medico_req := cd_medico_solicitante_w;
	end if;

end if;

if 	(:new.nr_seq_agrupamento is null) and
	(:new.nr_seq_proc_interno is not null) then
	nr_seq_agrupamento_w	:= ageint_obter_agrup_agenda(:new.nr_seq_proc_interno, :new.cd_procedimento, :new.ie_origem_proced, wheb_usuario_pck.get_cd_estabelecimento);
	if	(nvl(nr_seq_agrupamento_w,0) > 0) then
		:new.nr_Seq_agrupamento	:= nr_seq_agrupamento_w;
	end if;
end if;

  IF (:NEW.NM_USUARIO IS NOT NULL) THEN
     BEGIN
       SELECT MAX(ADU.NR_SEQUENCIA)
         INTO NR_SEQUENCIA_URA_W
         FROM AGEINT_DADOS_URA ADU
        WHERE ADU.ID_OPERADOR = :NEW.NM_USUARIO
          AND ADU.IE_STATUS = 'E';

       :NEW.NR_SEQ_DADOS_URA := NR_SEQUENCIA_URA_W;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
            NULL;
     END;
  END IF;
  <<finalize>>
null;

if (:new.ie_retorno is null) then
	:new.ie_retorno	:= 'N';
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_integrada_item_update
after insert or update ON TASY.AGENDA_INTEGRADA_ITEM FOR EACH ROW
DECLARE

cd_estabelecimento_w	Number(4);
cd_convenio_w		Number(5);
cd_categoria_w		Varchar2(10);
dt_inicio_agendamento_w	Date;
cd_plano_w		Varchar2(10);
vl_procedimento_w	Number(15,2);
vl_aux_w		Number(15,4);
ds_aux_w		Varchar2(10);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
cd_convenio_ww		Number(5);
cd_categoria_ww		Varchar2(10);
cd_plano_ww		Varchar2(10);
ie_tipo_convenio_w	Number(3);
ie_regra_w		Number(5);
ie_glosa_w		Varchar2(1);
cd_usuario_convenio_w	Varchar2(30);
qt_idade_w		Number(3);
dt_nascimento_w		Date;
ie_Sexo_w		Varchar2(1);
cd_pessoa_fisica_w	Varchar2(10);
CD_EDICAO_AMB_w         Number(6);
VL_LANC_AUTOM_W         Number(15,2);
ie_calcula_lanc_auto_w	Varchar2(1);
ie_atualiza_medico_req_w Varchar2(1);
vl_custo_operacional_w	Number(15,2);
vl_anestesista_w	Number(15,2);
vl_medico_w		Number(15,2);
vl_auxiliares_w		Number(15,2);
vl_materiais_w		Number(15,2);
cd_medico_item_w	Varchar2(10);
ie_tipo_Atendimento_w	Number(3);
nr_seq_ageint_w		number(10);
cd_conv_item_w		number(5);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
nr_seq_regra_w		number(10);
nr_minuto_duracao_w	number(10);
ie_resp_autor_w		varchar2(10);
cd_medico_solicitante_w varchar2(10);

ie_edicao_w                  varchar2(1);
cd_edicao_ajuste_w      number(10);
qt_item_edicao_w         number(10);
cd_estab_agenda_w	number(5);
ie_perm_agenda_w	varchar2(1)	:= 'S';
qt_estab_user_w		number(10);
ie_estab_usuario_w	varchar2(1);
nm_paciente_w		varchar2(60);
cd_paciente_agenda_w	varchar2(10);
nm_paciente_agenda_w	varchar2(60);
ie_pacote_w		varchar2(1);
ie_inserir_prof_w	varchar2(1);
ds_erro_w		varchar2(255);
dt_nascimento_Ww	date;
nr_seq_tipo_classif_pac_w	number(10);
ie_orient_long_w	varchar2(1);

Cursor C01 is
	select	a.cd_medico,
		c.cd_estabelecimento,
		ageint_obter_se_regra_med_ex(a.cd_medico, c.cd_Agenda, :new.nr_seq_proc_interno, :new.nr_seq_agenda_int, cd_procedimento_w, ie_origem_proced_w)
	from    agenda c,
		pessoa_fisica b,
		agenda_medico a
	where   Obter_Se_Ageint_Item(a.cd_agenda, :new.nr_seq_agenda_int, cd_estabelecimento_w, :new.nr_seq_proc_interno, :new.cd_Especialidade,

:new.cd_medico, a.cd_medico, cd_procedimento_w, ie_origem_proced_w,:new.nr_sequencia, qt_idade_w) = 'S'
	and     a.cd_medico    			= b.cd_pessoa_fisica
	and	c.cd_agenda			= a.cd_agenda
	and	nvl(c.ie_agenda_integrada,'N')	= 'S'
	and     a.ie_situacao  			= 'A'
	and	c.ie_situacao			= 'A'
	and	c.cd_tipo_Agenda = 2
	and	((qt_idade_w	>= nvl(qt_idade_min,0)
	and	qt_idade_w	<= nvl(qt_idade_max,999))
	or	qt_idade_w = 0);

BEGIN

select	nvl(max(Obter_Valor_Param_Usuario(869, 7, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'S'),
	nvl(max(Obter_Valor_Param_Usuario(869, 81, Obter_Perfil_Ativo, :new.nm_usuario, 0)), 'N')
into	ie_calcula_lanc_auto_w,
	ie_atualiza_medico_req_w
from 	dual;
begin
select	cd_estabelecimento,
	cd_convenio,
	cd_categoria,
	dt_inicio_agendamento,
	cd_plano,
	cd_usuario_convenio,
	cd_pessoa_fisica,
	ie_tipo_Atendimento,
	dt_nascimento,
	nr_sequencia,
	nm_paciente,
	cd_medico_solicitante,
	nr_seq_tipo_classif_pac
into	cd_estabelecimento_w,
	cd_convenio_ww,
	cd_categoria_ww,
	dt_inicio_agendamento_w,
	cd_plano_ww,
	cd_usuario_convenio_w,
	cd_pessoa_fisica_w,
	ie_tipo_Atendimento_w,
	dt_nascimento_w,
	nr_seq_ageint_w,
	nm_paciente_w,
	cd_medico_solicitante_w,
	nr_seq_tipo_classif_pac_w
from	agenda_integrada
where	nr_sequencia	= :new.nr_seq_agenda_int;
exception
when others then
	nr_seq_ageint_w	:= null;
end;

ie_estab_usuario_w	:= nvl(Obter_Valor_Param_Usuario(869, 1, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w), 'S');
ie_orient_long_w	:= nvl(Obter_Valor_Param_Usuario(869, 176, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w), 'N');

if	(ie_orient_long_w	= 'S') then
	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano)
	into	cd_conv_item_w,
		cd_categ_item_w,
		cd_plano_item_w
	from	agenda_integrada_conv_item
	where	nr_seq_agenda_item	= :new.nr_sequencia;

	if	(cd_conv_item_w is not null) then
		cd_convenio_ww	:= cd_conv_item_w;
		cd_categoria_ww	:= cd_categ_item_w;
		cd_plano_ww	:= cd_plano_item_w;
	end if;

	if	(:new.cd_estabelecimento is not null) then
		cd_estabelecimento_w	:= :new.cd_estabelecimento;
	end if;

	cd_procedimento_w	:= :new.cd_procedimento;
	ie_origem_proced_w	:= :new.ie_origem_proced;

	if	((:old.nr_seq_proc_interno is null) and
		(:new.nr_seq_proc_interno is not null)) or
		((:old.nr_seq_proc_interno is not null) and
		(:new.nr_seq_proc_interno is not null) and
		(:new.nr_seq_proc_interno	<> :old.nr_seq_proc_interno)) or
		(((:old.cd_medico is null) and (:new.cd_medico is not null)) or
		((:old.cd_medico is not null) and (:new.cd_medico is null)) or
		(:new.cd_medico <> :old.cd_medico))then
		Ageint_Gerar_Orient_Item(cd_pessoa_fisica_w, :new.nr_sequencia, :new.nr_seq_proc_interno, :new.cd_procedimento, :new.ie_origem_proced,
								cd_convenio_ww, nr_seq_tipo_classif_pac_w, cd_estabelecimento_w, :new.nm_usuario, :new.cd_medico);
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_INTEGRADA_ITEM_tp  after update ON TASY.AGENDA_INTEGRADA_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_AGENDA_EXAME,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA_EXAME,1,4000),substr(:new.NR_SEQ_AGENDA_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA_EXAME',ie_log_w,ds_w,'AGENDA_INTEGRADA_ITEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_AGENDA_CONS,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_AGENDA_CONS,1,4000),substr(:new.NR_SEQ_AGENDA_CONS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AGENDA_CONS',ie_log_w,ds_w,'AGENDA_INTEGRADA_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_int_item_after_Atual
after insert or update ON TASY.AGENDA_INTEGRADA_ITEM for each row
declare

cd_pessoa_fisica_w		varchar2(10);
qt_medico_item_w		number(10);
qt_medico_item_inserido_w	number(10);

begin

select	count(*)
into	qt_medico_item_w
from	ageint_medico_item
where	nr_seq_item	= :new.nr_sequencia;

if	(qt_medico_item_w	= 1) then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	ageint_medico_item
	where	nr_seq_item			= :new.nr_sequencia
	and	nvl(ie_inserir_prof, 'N')	= 'S';

	if	(cd_pessoa_fisica_w is not null) then
		select	count(*)
		into	qt_medico_item_inserido_w
		from	agenda_integrada_prof_item
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_seq_agenda_item	= :new.nr_sequencia;

		if	(qt_medico_item_inserido_w	= 0) then
			insert into agenda_integrada_prof_item
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_agenda_item,
				cd_pessoa_fisica)
			values
				(agenda_integrada_prof_item_seq.nextval,
				sysdate,
				:new.nm_usuario,
				sysdate,
				:new.nm_usuario,
				:new.nr_sequencia,
				cd_pessoa_fisica_w);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.agiq_ageint_item_delete
before delete ON TASY.AGENDA_INTEGRADA_ITEM for each row
declare

ds_retorno_w varchar2(50);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	agiq_excluir_questoes (	ageint_resp_quest_sup_p	=>	null,
				nr_seq_ageint_p		=>	:old.nr_seq_agenda_int,
				nr_seq_ageint_item_p	=>	:old.nr_sequencia,
				ie_excluiu_quest_p	=>	ds_retorno_w,
				ie_commit_p		=>	'N');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_agenda_integrada_item_bfup
before insert or update ON TASY.AGENDA_INTEGRADA_ITEM FOR EACH ROW
FOLLOWS agenda_integrada_item_atual
DECLARE

nr_seq_proc_interno_w number(10);

BEGIN


    if(:new.ie_gestao_exame = 'S' and :new.nr_prescricao is not null)then
        
        :new.ie_glosa:=null;
        
        if inserting then
        
            if(:new.nr_seq_proc_interno is null)then
        
                nr_seq_proc_interno_w:= obter_seq_proc_interno_proced(:new.cd_procedimento,:new.ie_origem_proced);
                
                if(nr_seq_proc_interno_w is not null)then
                    :new.nr_seq_proc_interno := nr_seq_proc_interno_w;
                end if;
                
            end if;
        
        end if;
        
    end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.hsj_agenda_integrada_item_cdi
after insert or update ON TASY.AGENDA_INTEGRADA_ITEM FOR EACH ROW
DECLARE

nr_atendimento_w number(10);
nr_seq_alert_atend_w number(10);
ds_preparo_w varchar2(2000);
NR_SEQ_ORIENT_PREPARO_W number(10);
nr_seq_agenda_w number(10);
ds_agenda_w varchar2(255);
hr_inicio_w date;
cd_agenda_w number(10);
nr_seq_contraste_w number(10);
qt_contrastado_w number(10);
ds_constraste_w varchar2(255);
BEGIN

    if(:new.nr_seq_agenda_exame is not null and :new.nr_prescricao is not null and :new.nr_seq_proc_interno is not null and :new.ie_gestao_exame = 'S')then
        
        select max(NR_SEQ_CONTRASTE)
        into nr_seq_contraste_w
        from prescr_procedimento
        where nr_prescricao = :new.nr_prescricao
        and nr_sequencia = :new.NR_SEQUENCIA_PRESCRICAO
        and NR_SEQ_PROC_INTERNO = :new.nr_seq_proc_interno;
        
        if(nr_seq_contraste_w > 0)then
            select count(*)
            into qt_contrastado_w
            from PROC_INTERNO_CONTRASTE
            where nr_sequencia = nr_seq_contraste_w
            and ie_itens_associados = 'S';
            
            if(qt_contrastado_w = 0)then -- Caso exista contraste (NR_SEQ_CONTRASTE) informado na prescri��o, este dever� ser diferente de "Sem Contraste" (IE_ITENS_ASSOCIADOS) para que o preparo seja gerado
                goto Final; 
            else
                select max(ds_contraste)
                into ds_constraste_w
                from PROC_INTERNO_CONTRASTE
                where nr_sequencia = nr_seq_contraste_w
                and ie_itens_associados = 'S';   
            end if;
            
        end if;
        
        select nvl(
            (
                select  max(c.nr_sequencia)
                from prescr_medica a
                join prescr_procedimento b on(a.nr_prescricao = b.nr_prescricao)
                join agenda_paciente c on(c.nr_sequencia = :new.nr_seq_agenda_exame)
                where a.nr_prescricao = :new.nr_prescricao
                and b.nr_sequencia = :new.nr_sequencia_prescricao
                and NVL(b.ie_suspenso, 'N') != 'S'
                and a.dt_suspensao is null
                and c.ie_status_agenda not in('L','C','B')
                AND nvl(b.ie_status_execucao,'X') != 'BE'
                and b.dt_baixa is null
            ),0)nr_atendimento
        into nr_seq_agenda_w    
        from dual;
    
        if nr_seq_agenda_w > 0 then
        
            select  (select p.nr_atendimento from prescr_medica p where p.nr_prescricao = :new.nr_prescricao),
                    obter_descricao_agenda(w.cd_agenda, 2, 1, 1) ds_agenda,
                    hr_inicio,
                    w.cd_agenda
            into    nr_atendimento_w,
                    ds_agenda_w,
                    hr_inicio_w,
                    cd_agenda_w
            from agenda_paciente w
            where w.nr_sequencia = nr_seq_agenda_w;
        
            if nr_atendimento_w > 0 then
                                
                select max(nr_sequencia)
                into NR_SEQ_ORIENT_PREPARO_W
                from AGEINT_REGRA_CONSENT
                where nr_seq_proc_interno = :new.NR_SEQ_PROC_INTERNO
                and CD_AGENDA  = cd_agenda_w;

                
                if NR_SEQ_ORIENT_PREPARO_W > 0 then
                        
                    ds_preparo_w:= REMOVE_FORMATACAO_RTF_HTML(CONVERT_LONG_TO_VARCHAR2('DS_TEXTO','AGEINT_REGRA_CONSENT', 'nr_sequencia = '||NR_SEQ_ORIENT_PREPARO_W));
                    
                    if ds_preparo_w is not null then

ds_preparo_w:= 'Alerta

                    Preparo para Exame

Exame:  '|| substr(Obter_Item_Grid_Ageint(:new.nr_seq_proc_interno, :new.cd_medico, :new.cd_Especialidade, 1, :new.nr_seq_item_selec),1,35) ||'
Agenda: '|| ds_agenda_w ||'
Data:   '|| to_char(hr_inicio_w,'dd/mm/yyyy hh24:mi:ss')|| 
case when ds_constraste_w is not null then '
'||ds_constraste_w end||'

'||ds_preparo_w;
                    
                        select atendimento_alerta_seq.NextVal
                        into nr_seq_alert_atend_w
                        from dual;
                        
                        insert into hsj_agenda_integrada_alerta(nr_sequencia, nr_seq_alerta_atend, nr_seq_agend_int_item)
                        values(hsj_agenda_int_alerta_seq.nextval, nr_seq_alert_atend_w, :new.nr_sequencia);

                        
                        insert into atendimento_alerta(
                            nr_sequencia,
                            nr_atendimento,
                            dt_alerta,
                            dt_atualizacao,
                            nm_usuario,
                            ds_alerta,
                            ie_situacao,
                            dt_fim_alerta,
                            dt_liberacao,
                            dt_inativacao,
                            nm_usuario_inativacao,
                            ds_justificativa,
                            nr_seq_tipo_alerta)
                        values(
                            nr_seq_alert_atend_w,
                            nr_atendimento_w,
                            sysdate,
                            sysdate,
                            :new.nm_usuario,
                            ds_preparo_w,
                            'A',
                            hr_inicio_w,
                            sysdate,
                            null,
                            null,
                            null,
                            22);   

                    end if;
                    
                                        
--                else 
--                    hsj_gerar_log('Procedimento sem preparo!');    
                    
                end if;           

            end if;
        
        end if;
        
    end if;


<<Final>> 
ds_preparo_w:=null;
END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_integrada_item_med_prev
before insert or update ON TASY.AGENDA_INTEGRADA_ITEM for each row
begin

null;

end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_integrada_item_delete
before delete ON TASY.AGENDA_INTEGRADA_ITEM FOR EACH ROW
DECLARE

qt_reg_w	number(1);

begin
select nvl(max(1), 0)
into qt_reg_w
from ageint_Exame_associado
where	nr_Seq_ageint_item	= :old.nr_sequencia;

if (qt_reg_w = 1) then
	delete	ageint_Exame_associado
	where	nr_Seq_ageint_item	= :old.nr_sequencia;
end if;

select nvl(max(1), 0)
into qt_reg_w
from ageint_exame_adic_item
where	nr_seq_item	= :old.nr_sequencia;

if (qt_reg_w = 1) then
	delete	ageint_exame_adic_item
	where	nr_seq_item	= :old.nr_sequencia;
end if;

delete from agendamento_coletivo
where 	nr_seq_ag_integrada = :old.nr_seq_agenda_int
and     nr_seq_agenda_int = :old.nr_sequencia;

end;
/


ALTER TABLE TASY.AGENDA_INTEGRADA_ITEM ADD (
  CONSTRAINT AGEINIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_INTEGRADA_ITEM ADD (
  CONSTRAINT AGEINIT_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_MPRPACI_FK 
 FOREIGN KEY (NR_SEQ_PARTIC_CICLO_ITEM) 
 REFERENCES TASY.MPREV_PARTIC_CICLO_ITEM (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_RPLIESM_FK 
 FOREIGN KEY (NR_SEQ_LISTA) 
 REFERENCES TASY.RP_LISTA_ESPERA_MODELO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AREATUM_FK 
 FOREIGN KEY (NR_SEQ_AREA_ATUACAO) 
 REFERENCES TASY.AREA_ATUACAO_MEDICA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQUENCIA_PRESCRICAO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGRUSET_FK 
 FOREIGN KEY (NR_SEQ_AGRUP_SETOR) 
 REFERENCES TASY.AGRUPAMENTO_SETOR (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PESFISI_FK5 
 FOREIGN KEY (CD_MEDICO_PREV_LAUDO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINIT_SOEXCOM_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_COMPL) 
 REFERENCES TASY.SOLICITACAO_EXAME_COMPL (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO),
  CONSTRAINT AGEINIT_MPRCAPT_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO) 
 REFERENCES TASY.MPREV_CAPTACAO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROC_CPOE) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_MOTVAGD_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_AGENDAMENTO) 
 REFERENCES TASY.MOTIVO_AGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGRUAGE_FK 
 FOREIGN KEY (NR_SEQ_AGRUPAMENTO) 
 REFERENCES TASY.AGRUPAMENTO_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGMOTAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_ANEST) 
 REFERENCES TASY.AGENDA_MOTIVO_ANEST (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO_REQ) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINIT_AGTRGAT_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.AGEINT_REGIAO_ATEND (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_QTGRIAG_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_QUIMIO) 
 REFERENCES TASY.QT_GRUPO_ITEM_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGEINIT_AGEINGR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SELEC) 
 REFERENCES TASY.AGENDA_INT_GRUPO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGEINCA_FK 
 FOREIGN KEY (NR_CLASSIFICACAO_AGEND) 
 REFERENCES TASY.AGEINT_CLASSIF_AGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGREGTR_FK 
 FOREIGN KEY (NR_SEQ_TRANSPORTE) 
 REFERENCES TASY.AGENDA_REGRA_TRANSPORTE (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PESFISI_FK4 
 FOREIGN KEY (CD_ANESTESISTA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINIT_AGSACON_FK 
 FOREIGN KEY (NR_SEQ_SALA) 
 REFERENCES TASY.AGENDA_SALA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL_EXAME) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINIT_AGIPPCT_FK 
 FOREIGN KEY (NR_SEQ_PROC_PACOTE) 
 REFERENCES TASY.AGEINT_PROC_PACOTE (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGEQUIM_FK 
 FOREIGN KEY (NR_SEQ_AGEQUI) 
 REFERENCES TASY.AGENDA_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT AGEINIT_QTPENAG_FK 
 FOREIGN KEY (NR_SEQ_PEND_QUIMIO) 
 REFERENCES TASY.QT_PENDENCIA_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEINIT_ESPARAT_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT AGEINIT_AGEINTE_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_INT) 
 REFERENCES TASY.AGENDA_INTEGRADA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEINIT_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_EXAME) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGEINIT_AGEPACI_FK2 
 FOREIGN KEY (NR_SEQ_AGENDA_ANESTESISTA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGEPACI_FK3 
 FOREIGN KEY (NR_SEQ_CIRURGIA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGEPACL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGLIESP_FK 
 FOREIGN KEY (NR_SEQ_LISTA_ESPERA) 
 REFERENCES TASY.AGENDA_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_AGINTCO_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.AGENDA_INTEGRADA_CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEINIT_AGDAURA_FK 
 FOREIGN KEY (NR_SEQ_DADOS_URA) 
 REFERENCES TASY.AGEINT_DADOS_URA (NR_SEQUENCIA),
  CONSTRAINT AGEINIT_MUTILES_FK 
 FOREIGN KEY (NR_SEQ_MULTIRAO) 
 REFERENCES TASY.MUTIRAO_LISTA_ESPERA (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_INTEGRADA_ITEM TO NIVEL_1;

