ALTER TABLE TASY.PRIVACY_PROPERTY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRIVACY_PROPERTY CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRIVACY_PROPERTY
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_VALUE                VARCHAR2(100 BYTE)    NOT NULL,
  DS_KEY                  VARCHAR2(15 BYTE)     NOT NULL,
  NR_SEQ_PRIVACY_VERSION  NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRPROPE_PK ON TASY.PRIVACY_PROPERTY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPROPE_PRVERSI_FK_I ON TASY.PRIVACY_PROPERTY
(NR_SEQ_PRIVACY_VERSION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRPROPE_UK ON TASY.PRIVACY_PROPERTY
(NR_SEQ_PRIVACY_VERSION, DS_KEY)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PRIVACY_PROPERTY_ATUAL
before insert or update ON TASY.PRIVACY_PROPERTY for each row
declare

is_a_valid_expression	varchar2(1);

begin

if 	(:new.ds_key in ('CD_EXP_CONFIG', 'CD_EXP_MESSAGE')) then

	select	decode(count(*), 0, 'N', 'Y')
	into	is_a_valid_expression
	from	dic_expressao
	where	cd_expressao = :new.ds_value;

	if (is_a_valid_expression = 'N') then
		--A express�o informada n�o existe.
		wheb_mensagem_pck.exibir_mensagem_abort(854529);
	end if;

end if;

end;
/


ALTER TABLE TASY.PRIVACY_PROPERTY ADD (
  CONSTRAINT PRPROPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PRPROPE_UK
 UNIQUE (NR_SEQ_PRIVACY_VERSION, DS_KEY)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRIVACY_PROPERTY ADD (
  CONSTRAINT PRPROPE_PRVERSI_FK 
 FOREIGN KEY (NR_SEQ_PRIVACY_VERSION) 
 REFERENCES TASY.PRIVACY_VERSION (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRIVACY_PROPERTY TO NIVEL_1;

