ALTER TABLE TASY.COM_CLIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLIENTE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_EMPRESA                NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_CNPJ                   VARCHAR2(14 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_NATUREZA               VARCHAR2(1 BYTE)    NOT NULL,
  IE_FASE_VENDA             VARCHAR2(3 BYTE)    NOT NULL,
  IE_STATUS_NEG             VARCHAR2(3 BYTE)    NOT NULL,
  IE_CLASSIFICACAO          VARCHAR2(3 BYTE)    NOT NULL,
  IE_TIPO                   VARCHAR2(3 BYTE)    NOT NULL,
  QT_LEITO                  NUMBER(5),
  QT_LEITO_UTI              NUMBER(3),
  VL_FAT_ANUAL              NUMBER(15,2),
  IE_RESP_ATEND             VARCHAR2(1 BYTE)    NOT NULL,
  IE_RECEBE_VISITA          VARCHAR2(1 BYTE)    NOT NULL,
  IE_REFERENCIA             VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_FORMA_CONHEC       NUMBER(10),
  QT_VIDAS                  NUMBER(10),
  DT_REVISAO_PREVISTA       DATE,
  NR_SEQ_ATIV               NUMBER(10),
  QT_CONSULTORES            NUMBER(10),
  DT_ATUALIZACAO_NREC       DATE,
  IE_ACOMPANHA_IMPLANTACAO  VARCHAR2(1 BYTE)    NOT NULL,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_VISITA_POS_VENDA       NUMBER(10),
  DT_OFICIALIZACAO_USO      DATE,
  IE_RESP_COORDENACAO       VARCHAR2(3 BYTE),
  QT_MES_ESTIMADO           NUMBER(10),
  IE_HOSP_ESCOLA            VARCHAR2(1 BYTE)    NOT NULL,
  IE_RESP_IMPLANTACAO       VARCHAR2(1 BYTE)    NOT NULL,
  IE_REVISADO               VARCHAR2(1 BYTE),
  IE_PRODUTO                VARCHAR2(15 BYTE)   NOT NULL,
  PR_POSSIB_FECHAM          NUMBER(5,2),
  IE_INCLUSAO_MANUAL        VARCHAR2(1 BYTE),
  NR_SEQ_LEAD               NUMBER(10),
  IE_FORMA_AQUISICAO        VARCHAR2(15 BYTE),
  DT_REVISAO_PREVISTA_ORIG  DATE,
  CD_ESTABELECIMENTO        NUMBER(4),
  DT_CLIENTE                DATE,
  IE_MIGRACAO               VARCHAR2(5 BYTE),
  DT_MIGRADO                DATE,
  IE_PORTE_CLIENTE          VARCHAR2(10 BYTE),
  DT_APROV_DUPLIC           DATE,
  IE_ETAPA_DUPLIC           VARCHAR2(1 BYTE),
  NR_SEQ_SITUACAO_FIN       NUMBER(10),
  IE_RESTRICAO              VARCHAR2(1 BYTE)    NOT NULL,
  IE_SEGMENTACAO            VARCHAR2(15 BYTE),
  IE_RECEBE_AUDITORIA       VARCHAR2(1 BYTE),
  IE_RESPONDE_PSC           VARCHAR2(1 BYTE),
  NR_SEQ_CLIENTE_STATUS     NUMBER(10),
  NR_SEQ_EQUIP_AUDIT        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLIE_COMATIV_FK_I ON TASY.COM_CLIENTE
(NR_SEQ_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIE_COMATIV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLIE_COMCLISTAT_FK_I ON TASY.COM_CLIENTE
(NR_SEQ_CLIENTE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMCLIE_COMFOCO_FK_I ON TASY.COM_CLIENTE
(NR_SEQ_FORMA_CONHEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIE_COMFOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLIE_CSOLEAD_FK_I ON TASY.COM_CLIENTE
(NR_SEQ_LEAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIE_CSOLEAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLIE_EMPRESA_FK_I ON TASY.COM_CLIENTE
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIE_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLIE_ESTABEL_FK_I ON TASY.COM_CLIENTE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCLIE_FINSC_FK_I ON TASY.COM_CLIENTE
(NR_SEQ_SITUACAO_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMCLIE_MANEQUI_FK_I ON TASY.COM_CLIENTE
(NR_SEQ_EQUIP_AUDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMCLIE_PESJURI_FK_I ON TASY.COM_CLIENTE
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLIE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COMCLIE_PK ON TASY.COM_CLIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.COM_CLIENTE_ATUAL
AFTER INSERT OR UPDATE ON TASY.COM_CLIENTE FOR EACH ROW
DECLARE

nr_sequencia_w		number(10);

begin

if	(nvl(:new.ie_fase_venda,0) <> nvl(:old.ie_fase_venda,0)) then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		1,
		sysdate,
		null,
		:new.ie_fase_venda,
		null);

end;

end if;

if	(nvl(:new.ie_classificacao,0) <> nvl(:old.ie_classificacao,0)) then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		2,
		sysdate,
		:new.ie_classificacao,
		null,
		null);

	end;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.COM_CLIENTE_tp  after update ON TASY.COM_CLIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'COM_CLIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ,1,4000),substr(:new.CD_CNPJ,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ',ie_log_w,ds_w,'COM_CLIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.COM_CLIENTE ADD (
  CONSTRAINT COMCLIE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLIENTE ADD (
  CONSTRAINT COMCLIE_COMATIV_FK 
 FOREIGN KEY (NR_SEQ_ATIV) 
 REFERENCES TASY.COM_ATIVIDADE (NR_SEQUENCIA),
  CONSTRAINT COMCLIE_COMFOCO_FK 
 FOREIGN KEY (NR_SEQ_FORMA_CONHEC) 
 REFERENCES TASY.COM_FORMA_CONHEC (NR_SEQUENCIA),
  CONSTRAINT COMCLIE_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT COMCLIE_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT COMCLIE_CSOLEAD_FK 
 FOREIGN KEY (NR_SEQ_LEAD) 
 REFERENCES TASY.COM_SOLIC_LEAD (NR_SEQUENCIA),
  CONSTRAINT COMCLIE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COMCLIE_FINSC_FK 
 FOREIGN KEY (NR_SEQ_SITUACAO_FIN) 
 REFERENCES TASY.FIN_STATUS_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT COMCLIE_COMCLISTAT_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE_STATUS) 
 REFERENCES TASY.COM_CLIENTE_STATUS (NR_SEQUENCIA),
  CONSTRAINT COMCLIE_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIP_AUDIT) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COM_CLIENTE TO NIVEL_1;

