ALTER TABLE TASY.ATEND_PAC_DISPOSITIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PAC_DISPOSITIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PAC_DISPOSITIVO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_DISPOSITIVO        NUMBER(10)          NOT NULL,
  DT_INSTALACAO             DATE                NOT NULL,
  DT_RETIRADA               DATE,
  NR_SEQ_TOPOGRAFIA         NUMBER(10),
  IE_LADO                   VARCHAR2(1 BYTE),
  DS_TITULO                 VARCHAR2(100 BYTE),
  NR_CIRURGIA               NUMBER(10),
  QT_HORA_PERMANENCIA       NUMBER(15),
  DT_RETIRADA_PREV          DATE,
  DS_COMPL_TOPOGRAFIA       VARCHAR2(80 BYTE),
  QT_HORA_PERM_CURAT        NUMBER(4),
  DT_RETIR_PREV_CURAT       DATE,
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_RET         NUMBER(10),
  QT_TENTATIVAS             NUMBER(10),
  QT_DISP_UTILIZADOS        NUMBER(10),
  DT_LANCAMENTO_AUTOMATICO  DATE,
  NR_SEQ_PEPO               NUMBER(10),
  IE_ACAO                   VARCHAR2(15 BYTE),
  CD_PROFISSIONAL           VARCHAR2(10 BYTE),
  NR_SEQ_CALIBRE            NUMBER(10),
  NR_ATENDIMENTO_ANT        NUMBER(15),
  NR_SEQ_ASSINATURA         NUMBER(10),
  IE_BRONCO                 VARCHAR2(1 BYTE),
  IE_INCONSCIENTE           VARCHAR2(1 BYTE),
  IE_SNE                    VARCHAR2(1 BYTE),
  IE_SUCESSO                VARCHAR2(1 BYTE),
  IE_ACAO_SAE               VARCHAR2(15 BYTE),
  DS_UTC                    VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO          VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO        VARCHAR2(50 BYTE),
  DS_DESCRICAO_RETIRADA     VARCHAR2(4000 BYTE),
  NR_SEQ_COMPLEMENTO        NUMBER(10),
  NR_SEQ_CLASSIF            NUMBER(10),
  NR_SEQ_FORMULARIO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPADI_ATEPACI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_CIRURGI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_DISCALI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_CALIBRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPADI_DISCALI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPADI_DISPCOM_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_COMPLEMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_DISPOSI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_DISPOSITIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_EHRREGI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_MOTREDI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_MOTIVO_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPADI_MOTREDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPADI_PEPOCIR_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPADI_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPADI_PESFISI_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPADI_PK ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_PROINTCLA_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPADI_TOPDOR_FK_I ON TASY.ATEND_PAC_DISPOSITIVO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPADI_TOPDOR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATEND_PAC_DISPOSITIVO_insert
before insert ON TASY.ATEND_PAC_DISPOSITIVO for each row
declare

cd_pessoa_fisica_w		varchar2(10);
qt_itens_w			number(10);
nr_seq_tecnica_w		number(10);
nr_seq_local_w			number(10);

Cursor C01 is
	select	nr_seq_tecnica,
		nr_seq_local
	from	hd_regra_acesso_disp
	where	ie_situacao = 'A'
	and	NR_SEQ_DISPOSITIVO = :new.NR_SEQ_DISPOSITIVO;

BEGIN

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

select 	count(*)
into	qt_itens_w
from	hd_pac_renal_cronico
where	cd_pessoa_fisica = cd_pessoa_fisica_w;

if	(qt_itens_w > 0) then

	open C01;
	loop
	fetch C01 into
		nr_seq_tecnica_w,
		nr_seq_local_w;
	exit when C01%notfound;
		begin

		insert into	hd_acesso	(NR_SEQUENCIA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO_NREC,
						CD_PESSOA_FISICA,
						NR_SEQ_LOCAL,
						NR_SEQ_TECNICA,
						DT_INSTALACAO)
		values		(		hd_acesso_seq.nextval,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						cd_pessoa_fisica_w,
						nr_seq_local_w,
						nr_seq_tecnica_w,
						sysdate);

		end;
	end loop;
	close C01;

end if;

END ATEND_PAC_DISPOSITIVO_insert;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PAC_DISPOSITIVO_ATUAL
before insert or update ON TASY.ATEND_PAC_DISPOSITIVO for each row
declare


begin
if	(nvl(:old.DT_INSTALACAO,sysdate+10) <> :new.DT_INSTALACAO) and
	(:new.DT_INSTALACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_INSTALACAO, 'HV');
end if;

if	(:old.dt_retirada is null) and
	(:new.dt_retirada is not null) then
	wl_gerar_finalizar_tarefa('TL','F',:new.nr_atendimento,null,:new.nm_usuario,null,'N',null,:new.nr_sequencia);
end if;

if	(:old.dt_instalacao is null) and
	(:new.dt_instalacao is not null) then
	send_device_integration(:new.nr_sequencia, :new.nr_atendimento, :new.nr_seq_dispositivo, :new.ie_lado,
							:new.nr_seq_topografia, :new.dt_instalacao, :new.dt_retirada, :new.dt_atualizacao);
end if;

if	(:old.dt_retirada is null) and
	(:new.dt_retirada is not null) then
	send_device_integration(:new.nr_sequencia, :new.nr_atendimento, :new.nr_seq_dispositivo, :new.ie_lado,
							:new.nr_seq_topografia, :new.dt_instalacao, :new.dt_retirada, :new.dt_atualizacao);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.atend_pac_dispositivo_aft_ins
after insert ON TASY.ATEND_PAC_DISPOSITIVO for each row
declare

cd_pessoa_fisica_w			varchar2(10);

nr_seq_regra_w				wl_regra_item.nr_sequencia%type;
qt_tempo_normal_w			wl_regra_item.qt_tempo_normal%type;

nr_seq_tipo_adm_fat_atd_w	atendimento_paciente.nr_seq_tipo_admissao_fat%type;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_episodio_w			atendimento_paciente.nr_seq_episodio%type;
nr_seq_tipo_adm_fat_tsk_w	wl_regra_item.nr_seq_tipo_admissao_fat%type;
ie_gerar_pendencia_tl_w		subtipo_episodio.ie_gerar_pendencia%type;

cursor C01 is
	select	nvl(b.qt_tempo_normal, 0),
			nvl(b.nr_sequencia, 0)
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = 'TL'
								and		x.ie_situacao = 'A');

BEGIN

select	max(cd_pessoa_fisica),
		max(nr_seq_tipo_admissao_fat),
		max(ie_tipo_atendimento),
		max(nr_seq_episodio)
into	cd_pessoa_fisica_w,
		nr_seq_tipo_adm_fat_atd_w,
		ie_tipo_atendimento_w,
		nr_seq_episodio_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

if (:new.dt_retirada is null) then
	open C01;
	loop
	fetch C01 into
		qt_tempo_normal_w,
		nr_seq_regra_w;
	exit when C01%notfound;
		begin

			if	(qt_tempo_normal_w > 0 and obter_se_regra_geracao(nr_seq_regra_w,nr_seq_episodio_w,nr_seq_tipo_adm_fat_atd_w) = 'S') then
				-- Gera Tarefa no Worklist para dispositivos
				wl_gerar_finalizar_tarefa('TL','I',:new.nr_atendimento,cd_pessoa_fisica_w,:new.nm_usuario,nvl(:new.dt_retirada_prev,sysdate+(qt_tempo_normal_w/24)),'N',
											null,:new.nr_sequencia,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_retirada_prev,nr_seq_episodio_w);
			end if;
		end;
	end loop;
	close C01;
end if;

END atend_pac_dispositivo_aft_ins;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_PAC_DISPOSITIVO_tp  after update ON TASY.ATEND_PAC_DISPOSITIVO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'ATEND_PAC_DISPOSITIVO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ATEND_PAC_DISPOSITIVO ADD (
  CONSTRAINT ATEPADI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PAC_DISPOSITIVO ADD (
  CONSTRAINT ATEPADI_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPADI_DISPCOM_FK 
 FOREIGN KEY (NR_SEQ_COMPLEMENTO) 
 REFERENCES TASY.DISPOSITIVO_COMPLEMENTO (NR_SEQUENCIA),
  CONSTRAINT ATEPADI_PROINTCLA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROC_INTERNO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT ATEPADI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEPADI_DISPOSI_FK 
 FOREIGN KEY (NR_SEQ_DISPOSITIVO) 
 REFERENCES TASY.DISPOSITIVO (NR_SEQUENCIA),
  CONSTRAINT ATEPADI_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT ATEPADI_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT ATEPADI_MOTREDI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RET) 
 REFERENCES TASY.MOTIVO_RETIRADA_DISP (NR_SEQUENCIA),
  CONSTRAINT ATEPADI_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPADI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPADI_DISCALI_FK 
 FOREIGN KEY (NR_SEQ_CALIBRE) 
 REFERENCES TASY.DISPOSITIVO_CALIBRE (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_PAC_DISPOSITIVO TO NIVEL_1;

