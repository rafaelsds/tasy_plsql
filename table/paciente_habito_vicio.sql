ALTER TABLE TASY.PACIENTE_HABITO_VICIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_HABITO_VICIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_HABITO_VICIO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  IE_CLASSIFICACAO           VARCHAR2(3 BYTE),
  NR_SEQ_TIPO                NUMBER(10),
  IE_INTENSIDADE             VARCHAR2(3 BYTE),
  IE_FREQUENCIA              VARCHAR2(3 BYTE),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  DS_QUANTIDADE              VARCHAR2(80 BYTE),
  DS_CONSEQUENCIA            VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  DT_REGISTRO                DATE               NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_UNIDADE_MEDIDA          NUMBER(10),
  QT_UTILIZACAO              NUMBER(15,4),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_NIVEL_SEG           NUMBER(10),
  NR_SEQ_SAEP                NUMBER(10),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  DS_ANTECEDENTES_FAM        VARCHAR2(255 BYTE),
  QT_IDADE_EXP               NUMBER(3),
  QT_IDADE_REG               NUMBER(3),
  IE_NEGA_HABITO             VARCHAR2(1 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  NR_SEQ_MARCA               NUMBER(10),
  DT_REVISAO                 DATE,
  NM_USUARIO_TERMINO         VARCHAR2(15 BYTE),
  DS_JUST_TERMINO            VARCHAR2(255 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_LISTA_PROBLEMA          VARCHAR2(1 BYTE),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  QT_MACO_ANO                NUMBER(10,2),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_DYSURIA                 VARCHAR2(1 BYTE),
  IE_ABNOR_URINE_HEM         VARCHAR2(1 BYTE),
  IE_ABNOR_URINE_OTHER       VARCHAR2(1 BYTE),
  NR_SEQ_PROP_FECES          NUMBER(10),
  IE_SENSE_EVA_INCOMP        VARCHAR2(1 BYTE),
  DS_LAXATIVE                VARCHAR2(255 BYTE),
  IE_LAXATIVE                VARCHAR2(1 BYTE),
  DS_ABNORMAL_URINE          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACHAVI_ATCONSPEPA_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACHAVI_ATEPACI_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACHAVI_EHRREEL_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_FECEPRO_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_PROP_FECES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACHAVI_HISTSAU_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACHAVI_I1 ON TASY.PACIENTE_HABITO_VICIO
(DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACHAVI_MEDTIVI_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_MEDTIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_MEDTIVIM_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_MEDTIVIM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_NISEALE_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_PESFISI_FK_I ON TASY.PACIENTE_HABITO_VICIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACHAVI_PESFISI_FK2_I ON TASY.PACIENTE_HABITO_VICIO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACHAVI_PK ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_SAEPERO_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_TASASDI_FK_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_TASASDI_FK2_I ON TASY.PACIENTE_HABITO_VICIO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACHAVI_UNIMEHV_FK_I ON TASY.PACIENTE_HABITO_VICIO
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACHAVI_UNIMEHV_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_HABITO_VICIO_ATUAL
before update or insert ON TASY.PACIENTE_HABITO_VICIO for each row
declare
begin
if	(NVL(:new.ie_nega_habito,'N') = 'N') and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pac_habito_vicio_pend_atual 
after insert or update ON TASY.PACIENTE_HABITO_VICIO 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSHV'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSHV'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_HABITO_VICIO_SBIS_INS
before insert or update ON TASY.PACIENTE_HABITO_VICIO for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

IF (INSERTING) THEN
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nm_usuario,
										 nr_atendimento) values
										 (nr_log_seq_w,
										 sysdate,
										obter_desc_expressao(656665) ,
										 wheb_usuario_pck.get_nm_maquina,
									     :new.cd_pessoa_fisica,
									     obter_desc_expressao(291200),
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario),
										 :new.nr_atendimento);
else
	ie_inativacao_w := 'N';


	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nm_usuario,
										 nr_atendimento) values
										 (nr_log_seq_w,
										 sysdate,
										 decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
										 wheb_usuario_pck.get_nm_maquina,
										 :new.cd_pessoa_fisica,
										 obter_desc_expressao(291200),
										 nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario),
										 :new.nr_atendimento);
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_HABITO_VICIO_tp  after update ON TASY.PACIENTE_HABITO_VICIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_LAXATIVE,1,500);gravar_log_alteracao(substr(:old.DS_LAXATIVE,1,4000),substr(:new.DS_LAXATIVE,1,4000),:new.nm_usuario,nr_seq_w,'DS_LAXATIVE',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FREQUENCIA,1,500);gravar_log_alteracao(substr(:old.IE_FREQUENCIA,1,4000),substr(:new.IE_FREQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FREQUENCIA',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_QUANTIDADE,1,500);gravar_log_alteracao(substr(:old.DS_QUANTIDADE,1,4000),substr(:new.DS_QUANTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'DS_QUANTIDADE',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_CONSEQUENCIA,1,500);gravar_log_alteracao(substr(:old.DS_CONSEQUENCIA,1,4000),substr(:new.DS_CONSEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONSEQUENCIA',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_UTILIZACAO,1,500);gravar_log_alteracao(substr(:old.QT_UTILIZACAO,1,4000),substr(:new.QT_UTILIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_UTILIZACAO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_UNIDADE_MEDIDA,1,500);gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LISTA_PROBLEMA,1,500);gravar_log_alteracao(substr(:old.IE_LISTA_PROBLEMA,1,4000),substr(:new.IE_LISTA_PROBLEMA,1,4000),:new.nm_usuario,nr_seq_w,'IE_LISTA_PROBLEMA',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_MACO_ANO,1,500);gravar_log_alteracao(substr(:old.QT_MACO_ANO,1,4000),substr(:new.QT_MACO_ANO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MACO_ANO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ANTECEDENTES_FAM,1,500);gravar_log_alteracao(substr(:old.DS_ANTECEDENTES_FAM,1,4000),substr(:new.DS_ANTECEDENTES_FAM,1,4000),:new.nm_usuario,nr_seq_w,'DS_ANTECEDENTES_FAM',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_IDADE_EXP,1,500);gravar_log_alteracao(substr(:old.QT_IDADE_EXP,1,4000),substr(:new.QT_IDADE_EXP,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_EXP',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_IDADE_REG,1,500);gravar_log_alteracao(substr(:old.QT_IDADE_REG,1,4000),substr(:new.QT_IDADE_REG,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_REG',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NEGA_HABITO,1,500);gravar_log_alteracao(substr(:old.IE_NEGA_HABITO,1,4000),substr(:new.IE_NEGA_HABITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NEGA_HABITO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_MARCA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_MARCA,1,4000),substr(:new.NR_SEQ_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MARCA',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ABNORMAL_URINE,1,500);gravar_log_alteracao(substr(:old.DS_ABNORMAL_URINE,1,4000),substr(:new.DS_ABNORMAL_URINE,1,4000),:new.nm_usuario,nr_seq_w,'DS_ABNORMAL_URINE',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DYSURIA,1,500);gravar_log_alteracao(substr(:old.IE_DYSURIA,1,4000),substr(:new.IE_DYSURIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DYSURIA',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ABNOR_URINE_HEM,1,500);gravar_log_alteracao(substr(:old.IE_ABNOR_URINE_HEM,1,4000),substr(:new.IE_ABNOR_URINE_HEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_ABNOR_URINE_HEM',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ABNOR_URINE_OTHER,1,500);gravar_log_alteracao(substr(:old.IE_ABNOR_URINE_OTHER,1,4000),substr(:new.IE_ABNOR_URINE_OTHER,1,4000),:new.nm_usuario,nr_seq_w,'IE_ABNOR_URINE_OTHER',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SENSE_EVA_INCOMP,1,500);gravar_log_alteracao(substr(:old.IE_SENSE_EVA_INCOMP,1,4000),substr(:new.IE_SENSE_EVA_INCOMP,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENSE_EVA_INCOMP',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LAXATIVE,1,500);gravar_log_alteracao(substr(:old.IE_LAXATIVE,1,4000),substr(:new.IE_LAXATIVE,1,4000),:new.nm_usuario,nr_seq_w,'IE_LAXATIVE',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'PACIENTE_HABITO_VICIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_HABITO_VICIO_SBIS_DEL
before delete ON TASY.PACIENTE_HABITO_VICIO for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nm_usuario,
									 nr_atendimento) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 :old.cd_pessoa_fisica,
									 obter_desc_expressao(291200),
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario),
									 :new.nr_atendimento);

end;
/


ALTER TABLE TASY.PACIENTE_HABITO_VICIO ADD (
  CONSTRAINT PACHAVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_HABITO_VICIO ADD (
  CONSTRAINT PACHAVI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_FECEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROP_FECES) 
 REFERENCES TASY.FECES_PROPERTIES (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACHAVI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACHAVI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_MEDTIVIM_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MED_TIPO_VICIO_MARCA (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_UNIMEHV_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNID_MED_HABITO_VICIO (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACHAVI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACHAVI_MEDTIVI_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MED_TIPO_VICIO (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT PACHAVI_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_HABITO_VICIO TO NIVEL_1;

