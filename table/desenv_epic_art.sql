ALTER TABLE TASY.DESENV_EPIC_ART
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_EPIC_ART CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_EPIC_ART
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ART               NUMBER(10),
  NR_EPIC              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESEPAR_DESARTR_FK_I ON TASY.DESENV_EPIC_ART
(NR_ART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESEPAR_DESEPIC_FK_I ON TASY.DESENV_EPIC_ART
(NR_EPIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DESEPAR_PK ON TASY.DESENV_EPIC_ART
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DESENV_EPIC_ART ADD (
  CONSTRAINT DESEPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_EPIC_ART ADD (
  CONSTRAINT DESEPAR_DESARTR_FK 
 FOREIGN KEY (NR_ART) 
 REFERENCES TASY.DESENV_ART (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DESEPAR_DESEPIC_FK 
 FOREIGN KEY (NR_EPIC) 
 REFERENCES TASY.DESENV_EPIC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DESENV_EPIC_ART TO NIVEL_1;

