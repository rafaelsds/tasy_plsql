ALTER TABLE TASY.INTERFACE_INSTRUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERFACE_INSTRUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERFACE_INSTRUCAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_INTERFACE         NUMBER(5)                NOT NULL,
  DS_VIEW              VARCHAR2(255 BYTE),
  NM_OBJETO            VARCHAR2(255 BYTE),
  NM_TABELA            VARCHAR2(255 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTINST_INTERFA_FK_I ON TASY.INTERFACE_INSTRUCAO
(CD_INTERFACE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTINST_PK ON TASY.INTERFACE_INSTRUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTINST_PK
  MONITORING USAGE;


ALTER TABLE TASY.INTERFACE_INSTRUCAO ADD (
  CONSTRAINT INTINST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERFACE_INSTRUCAO ADD (
  CONSTRAINT INTINST_INTERFA_FK 
 FOREIGN KEY (CD_INTERFACE) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INTERFACE_INSTRUCAO TO NIVEL_1;

