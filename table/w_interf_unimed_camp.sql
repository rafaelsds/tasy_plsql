ALTER TABLE TASY.W_INTERF_UNIMED_CAMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTERF_UNIMED_CAMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTERF_UNIMED_CAMP
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  NR_LINHA          NUMBER(8)                   NOT NULL,
  NR_SEQ_PROTOCOLO  NUMBER(10)                  NOT NULL,
  DS_CAMPOS         VARCHAR2(300 BYTE)          NOT NULL,
  NR_ATENDIMENTO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WINUNCA_PK ON TASY.W_INTERF_UNIMED_CAMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINUNCA_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INTERF_UNIMED_CAMP ADD (
  CONSTRAINT WINUNCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INTERF_UNIMED_CAMP TO NIVEL_1;

