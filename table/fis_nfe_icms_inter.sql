ALTER TABLE TASY.FIS_NFE_ICMS_INTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_NFE_ICMS_INTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_NFE_ICMS_INTER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_ITEM_NF           NUMBER(5)                NOT NULL,
  CD_TRIBUTO           NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VL_ALIQ_INT_UF_DEST  NUMBER(15,2),
  VL_BASE_UF_DEST      NUMBER(15,2),
  CD_ALIQ_INTER        VARCHAR2(255 BYTE),
  CD_PARTILHADA_ICMS   VARCHAR2(255 BYTE),
  VL_ICMS_UF_DESTINO   NUMBER(15,2),
  VL_ICMS_UF_REMET     NUMBER(15,2),
  VL_BASE_FCP_UF_DEST  NUMBER(15,2),
  PR_FCP_UF_DEST       NUMBER(15,2),
  VL_FCP_UF_DESTINO    NUMBER(15,2),
  VL_BASE_FCP          NUMBER(15,2),
  PR_FCP               NUMBER(15,2),
  VL_FCP               NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISNFEICMS_NOTFIIT_FK_I ON TASY.FIS_NFE_ICMS_INTER
(NR_SEQUENCIA, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FISNFEICMS_PK ON TASY.FIS_NFE_ICMS_INTER
(NR_SEQUENCIA, NR_ITEM_NF, CD_TRIBUTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FISNFEICMS_TRIBUTO_FK_I ON TASY.FIS_NFE_ICMS_INTER
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_NFE_ICMS_INTER ADD (
  CONSTRAINT FISNFEICMS_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_ITEM_NF, CD_TRIBUTO));

ALTER TABLE TASY.FIS_NFE_ICMS_INTER ADD (
  CONSTRAINT FISNFEICMS_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT FISNFEICMS_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQUENCIA, NR_ITEM_NF) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF)
    ON DELETE CASCADE);

