ALTER TABLE TASY.CM_REQUISICAO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_REQUISICAO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_REQUISICAO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REQUISICAO    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONJUNTO      NUMBER(10),
  QT_CONJUNTO          NUMBER(15)               NOT NULL,
  CD_MOTIVO_BAIXA      NUMBER(3),
  NM_USUARIO_ATEND     VARCHAR2(15 BYTE),
  IE_EMPRESTIMO        VARCHAR2(1 BYTE),
  IE_DEVOLUCAO         VARCHAR2(1 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  NR_SEQ_MOTIVO        NUMBER(10),
  DT_DEVOLUCAO         DATE,
  NR_SEQ_CLASSIF       NUMBER(10),
  NR_SEQ_CLASSIF_CONJ  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMREIT_CMCLCON_FK_I ON TASY.CM_REQUISICAO_ITEM
(NR_SEQ_CLASSIF_CONJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMREIT_CMCLITE_FK_I ON TASY.CM_REQUISICAO_ITEM
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMREIT_CMCONJU_FK_I ON TASY.CM_REQUISICAO_ITEM
(NR_SEQ_CONJUNTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMREIT_CMCONJU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMREIT_CMREQUI_FK_I ON TASY.CM_REQUISICAO_ITEM
(NR_SEQ_REQUISICAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMREIT_CMREQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMREIT_MORECO_FK_I ON TASY.CM_REQUISICAO_ITEM
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMREIT_MORECO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMREIT_PK ON TASY.CM_REQUISICAO_ITEM
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cm_requisicao_item_atual
before insert or update ON TASY.CM_REQUISICAO_ITEM for each row
declare

begin

if	(:new.ie_devolucao = 'S') and
	(:new.ie_emprestimo = 'S') then
	--Somente � poss�vel informar que o item foi emprestado ou devolvido.;
	Wheb_mensagem_pck.exibir_mensagem_abort(267171);
end if;

if	(inserting) then
	if	(:new.NR_SEQ_CONJUNTO is null and :new.NR_SEQ_CLASSIF is null and :new.NR_SEQ_CLASSIF_CONJ is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(1041576);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cm_requisicao_item_tie
after insert or update or delete ON TASY.CM_REQUISICAO_ITEM for each row
declare

json_w		philips_json;
json_data_w	clob;

begin

if (wheb_usuario_pck.get_nm_usuario is not null) then

	if (inserting or updating) then

		json_w := philips_json();
		json_w.put('id', :new.nr_sequencia);
		json_w.put('requisition', :new.nr_seq_requisicao);
		json_w.put('kit', :new.nr_seq_conjunto);
		json_w.put('kitQuantity', :new.qt_conjunto);
		json_w.put('settlementReason', :new.cd_motivo_baixa);
		json_w.put('encounterUser', :new.nm_usuario_atend);
		json_w.put('devolution', :new.ie_devolucao);
		json_w.put('loan', :new.ie_emprestimo);
		json_w.put('notes', :new.ds_observacao);
		json_w.put('reason', :new.nr_seq_motivo);
		json_w.put('returnDate', :new.dt_devolucao);
		json_w.put('classification', :new.nr_seq_classif);
		json_w.put('kitClassification', :new.nr_seq_classif_conj);
		json_w.put('lastUpdate', :new.dt_atualizacao);
		json_w.put('lastUpdatedBy', :new.nm_usuario);
		json_w.put('creationDate', :new.dt_atualizacao_nrec);
		json_w.put('creationUser', :new.nm_usuario_nrec);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		if (inserting) then
			json_data_w := bifrost.send_integration_content('cssd.management.add.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);
		elsif (updating) then
			json_data_w := bifrost.send_integration_content('cssd.management.update.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);
		end if;

	elsif (deleting) then

		json_w := philips_json();
		json_w.put('id', :old.nr_sequencia);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		json_data_w := bifrost.send_integration_content('cssd.management.delete.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

	end if;

end if;

end;
/


ALTER TABLE TASY.CM_REQUISICAO_ITEM ADD (
  CONSTRAINT CMREIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_REQUISICAO_ITEM ADD (
  CONSTRAINT CMREIT_CMCLITE_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CM_CLASSIF_ITEM (NR_SEQUENCIA),
  CONSTRAINT CMREIT_CMCLCON_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_CONJ) 
 REFERENCES TASY.CM_CLASSIF_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT CMREIT_CMREQUI_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.CM_REQUISICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CMREIT_CMCONJU_FK 
 FOREIGN KEY (NR_SEQ_CONJUNTO) 
 REFERENCES TASY.CM_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT CMREIT_MORECO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_REQ_CONJUNTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_REQUISICAO_ITEM TO NIVEL_1;

