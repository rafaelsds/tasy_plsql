ALTER TABLE TASY.PLS_PRIORIDADE_ANALISE_AUT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRIORIDADE_ANALISE_AUT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRIORIDADE_ANALISE_AUT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_PRIORIDADE        VARCHAR2(255 BYTE)       NOT NULL,
  IE_PRIORIDADE        NUMBER(2)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  IE_APLICACAO_REGRA   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRAA_PK ON TASY.PLS_PRIORIDADE_ANALISE_AUT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRAA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_prior_analise_aut_update
before update ON TASY.PLS_PRIORIDADE_ANALISE_AUT for each row
declare
qt_analise_w			number(10)	:= 0;

begin
if	(:new.ie_prioridade <> :old.ie_prioridade) then
	select	count(1)
	into	qt_analise_w
	from	pls_auditoria	a
	where	a.nr_seq_prioridade_analise	= :new.nr_sequencia;

	if	(qt_analise_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(253365);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_PRIORIDADE_ANALISE_AUT ADD (
  CONSTRAINT PLSPRAA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_PRIORIDADE_ANALISE_AUT TO NIVEL_1;

