ALTER TABLE TASY.PLS_REGIAO_ATUACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGIAO_ATUACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGIAO_ATUACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_OPERADORA     NUMBER(10)               NOT NULL,
  IE_TODOS_MUNICIPIOS  VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  SG_UF_MUNICIPIO      VARCHAR2(3 BYTE)         NOT NULL,
  NR_SEQ_REGIAO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSREGA_PK ON TASY.PLS_REGIAO_ATUACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREGA_PLSOUTO_FK_I ON TASY.PLS_REGIAO_ATUACAO
(NR_SEQ_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREGA_PLSREGI_FK_I ON TASY.PLS_REGIAO_ATUACAO
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREGA_PLSREGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREGA_SUSMUNI_FK_I ON TASY.PLS_REGIAO_ATUACAO
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGIAO_ATUACAO ADD (
  CONSTRAINT PLSREGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGIAO_ATUACAO ADD (
  CONSTRAINT PLSREGA_PLSREGI_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.PLS_REGIAO (NR_SEQUENCIA),
  CONSTRAINT PLSREGA_PLSOUTO_FK 
 FOREIGN KEY (NR_SEQ_OPERADORA) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSREGA_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE));

GRANT SELECT ON TASY.PLS_REGIAO_ATUACAO TO NIVEL_1;

