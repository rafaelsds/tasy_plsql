ALTER TABLE TASY.SUS_PROTOCOLO_ENVIO_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_PROTOCOLO_ENVIO_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_PROTOCOLO_ENVIO_CONTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10),
  NR_INTERNO_CONTA     NUMBER(10),
  NR_ATENDIMENTO       NUMBER(10),
  NR_SEQ_PROT_ENVIO    NUMBER(10),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  DT_ENVIO_CONTA       DATE,
  NM_USUARIO_ENVIO     VARCHAR2(15 BYTE),
  VL_TOTAL_CONTA       NUMBER(15,2),
  IE_SITUACAO_RETORNO  VARCHAR2(15 BYTE),
  NR_SEQ_MOT_GLOSA     NUMBER(10),
  IE_STATUS_AJUSTE     VARCHAR2(10 BYTE),
  NR_AIH               NUMBER(13),
  NR_APAC              NUMBER(13),
  NR_BPA               NUMBER(13),
  VL_TOTAL_CONTA_SUS   NUMBER(15,2),
  VL_RETORNO           NUMBER(15,2),
  VL_SALDO             NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSPREC_ATEPACI_FK_I ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPREC_CONPACI_FK_I ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPREC_PESFISI_FK_I ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUSPREC_PK ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPREC_PROCONV_FK_I ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPREC_SUSMOGC_FK_I ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(NR_SEQ_MOT_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPREC_SUSPREN_FK_I ON TASY.SUS_PROTOCOLO_ENVIO_CONTA
(NR_SEQ_PROT_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUS_PROTOCOLO_ENVIO_CONTA ADD (
  CONSTRAINT SUSPREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_PROTOCOLO_ENVIO_CONTA ADD (
  CONSTRAINT SUSPREC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT SUSPREC_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO)
    ON DELETE CASCADE,
  CONSTRAINT SUSPREC_SUSMOGC_FK 
 FOREIGN KEY (NR_SEQ_MOT_GLOSA) 
 REFERENCES TASY.SUS_MOTIVO_GLOSA_CONTA (NR_SEQUENCIA),
  CONSTRAINT SUSPREC_SUSPREN_FK 
 FOREIGN KEY (NR_SEQ_PROT_ENVIO) 
 REFERENCES TASY.SUS_PROTOCOLO_ENVIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUSPREC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT SUSPREC_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUS_PROTOCOLO_ENVIO_CONTA TO NIVEL_1;

