ALTER TABLE TASY.SIMULACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIMULACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIMULACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_REFERENCIA           DATE                  NOT NULL,
  DT_INICIO               DATE                  NOT NULL,
  DT_FINAL                DATE                  NOT NULL,
  DS_TITULO               VARCHAR2(80 BYTE)     NOT NULL,
  CD_CONVENIO             NUMBER(5),
  IE_TIPO_ATENDIMENTO     NUMBER(3),
  VL_MINIMO               NUMBER(15,2),
  VL_MAXIMO               NUMBER(15,2),
  QT_IDADE_MINIMA         NUMBER(3),
  QT_IDADE_MAXIMA         NUMBER(3),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  NR_ATENDIMENTO          NUMBER(10),
  CD_MEDICO_RESP          VARCHAR2(10 BYTE),
  CD_ESPECIALIDADE        NUMBER(5),
  IE_COMPLEXIDADE         VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  IE_CANCELADA            VARCHAR2(1 BYTE)      NOT NULL,
  IE_PACOTE               VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_INTERNO_CONTA        NUMBER(10),
  DS_FILTRO_PROC_INT      VARCHAR2(4000 BYTE),
  DS_FILTRO_PROCEDIMENTO  VARCHAR2(4000 BYTE),
  CD_CATEGORIA            VARCHAR2(10 BYTE),
  DT_LIBERACAO            DATE,
  DS_LISTA_PROTOCOLO      VARCHAR2(4000 BYTE),
  IE_STATUS_PROTOCOLO     NUMBER(1),
  IE_TIPO_PROTOCOLO       NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SIMULAC_ATEPACI_FK_I ON TASY.SIMULACAO
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIMULAC_CATCONV_FK_I ON TASY.SIMULACAO
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMULAC_CONPACI_FK_I ON TASY.SIMULACAO
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_CONPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMULAC_CONVENI_FK_I ON TASY.SIMULACAO
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMULAC_ESPMEDI_FK_I ON TASY.SIMULACAO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMULAC_ESTABEL_FK_I ON TASY.SIMULACAO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMULAC_PESFISI_FK_I ON TASY.SIMULACAO
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SIMULAC_PK ON TASY.SIMULACAO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SIMULAC_PROCEDI_FK_I ON TASY.SIMULACAO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIMULAC_PROCONV_FK_I ON TASY.SIMULACAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIMULAC_PROCONV_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SIMULACAO ADD (
  CONSTRAINT SIMULAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIMULACAO ADD (
  CONSTRAINT SIMULAC_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT SIMULAC_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT SIMULAC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SIMULAC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT SIMULAC_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SIMULAC_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT SIMULAC_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO),
  CONSTRAINT SIMULAC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SIMULAC_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.SIMULACAO TO NIVEL_1;

