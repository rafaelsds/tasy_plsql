ALTER TABLE TASY.MOVTO_CARTAO_CR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVTO_CARTAO_CR CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVTO_CARTAO_CR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_TRANSACAO           DATE                   NOT NULL,
  VL_TRANSACAO           NUMBER(15,2)           NOT NULL,
  NR_AUTORIZACAO         VARCHAR2(40 BYTE)      NOT NULL,
  DT_BAIXA               DATE,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  NR_SEQ_CAIXA_REC       NUMBER(10),
  NR_CARTAO              VARCHAR2(20 BYTE),
  NR_SEQ_BANDEIRA        NUMBER(10)             NOT NULL,
  IE_TIPO_CARTAO         VARCHAR2(1 BYTE)       NOT NULL,
  IE_LIB_CAIXA           VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_TRANS_CAIXA     NUMBER(10),
  NR_SEQ_FORMA_PAGTO     NUMBER(10),
  DS_COMPROVANTE         VARCHAR2(100 BYTE),
  QT_PARCELA             NUMBER(4),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  CD_CGC                 VARCHAR2(14 BYTE),
  NR_ADIANTAMENTO        NUMBER(10),
  DT_LIBERACAO           DATE,
  DT_CANCELAMENTO        DATE,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  DT_INTEGRACAO_TEF      DATE,
  DT_CONFIRMACAO_TEF     DATE,
  DS_ARQ_RECIBO_TEF      VARCHAR2(255 BYTE),
  DT_AUTENTICACAO        DATE,
  DT_IMPORTACAO          DATE,
  NR_SEQ_GRUPO_PROD      NUMBER(10),
  VL_DESCONTO_OPERADORA  NUMBER(15,2),
  NR_AUTORIZACAO_CANCEL  VARCHAR2(40 BYTE),
  NR_SEQ_ATEND_ADIANT    NUMBER(10),
  NR_NSU_TEF             VARCHAR2(40 BYTE),
  NR_SEQ_PLS_RET_PAGTO   NUMBER(10),
  DS_STACK               VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOVCACR_ADIANTA_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_ATEPACI_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_ATEND_ADIANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_BANCACR_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_BANDEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCACR_BANCACR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCACR_CAIREC_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_ESTABEL_FK_I ON TASY.MOVTO_CARTAO_CR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCACR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCACR_FOPAGCR_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_FORMA_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCACR_FOPAGCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCACR_GRPRFIN_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_GRUPO_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCACR_GRPRFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCACR_I ON TASY.MOVTO_CARTAO_CR
(DT_TRANSACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_PESFISI_FK_I ON TASY.MOVTO_CARTAO_CR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_PESJURI_FK_I ON TASY.MOVTO_CARTAO_CR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCACR_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MOVCACR_PK ON TASY.MOVTO_CARTAO_CR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_PLSRPCR_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_PLS_RET_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCACR_TRAFINA_FK_I ON TASY.MOVTO_CARTAO_CR
(NR_SEQ_TRANS_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCACR_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MOVTO_CARTAO_CR_INSERT
BEFORE INSERT ON TASY.MOVTO_CARTAO_CR FOR EACH ROW
DECLARE

dt_fechamento_w		date	:= null;
nr_recibo_w		number(10);
nr_seq_produto_w	number(10);
nr_seq_grupo_w		number(10);

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

if	(:new.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :new.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/*r.aise_application_error(-20011,'Nao e possivel inserir um novo cartao!' || chr(13) ||
					'O recibo ' || nr_recibo_w || ' ja foi fechado!');*/
		wheb_mensagem_pck.exibir_mensagem_abort(215513,'NR_RECIBO_W='||nr_recibo_w);
	end if;
end if;

obter_produto_financeiro(:new.cd_estabelecimento,null,:new.cd_cgc,nr_seq_produto_w,null,null,nr_seq_grupo_w);

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_transacao);


if	(nr_seq_grupo_w is not null) then
	:new.nr_seq_grupo_prod	:= nr_seq_grupo_w;
end if;

:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MOVTO_CARTAO_CR_UPDATE
BEFORE UPDATE ON TASY.MOVTO_CARTAO_CR FOR EACH ROW
DECLARE

dt_fechamento_w		date	:= null;
nr_recibo_w		number(10);
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:old.nr_seq_caixa_rec is not null) and
	(:new.dt_cancelamento is null) and
	(nvl(:old.vl_transacao,0) <> nvl(:new.vl_transacao,0)) then

	if (	:new.dt_transacao	<>	:old.dt_transacao) then
		/* Projeto Davita  - N�o deixa gerar movimento contabil ap�s fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_transacao);
	end if;

	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(277731, 'NR_RECIBO_P=' || nr_recibo_w);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.MOVTO_CARTAO_CR_DELETE
BEFORE DELETE ON TASY.MOVTO_CARTAO_CR FOR EACH ROW
DECLARE

dt_fechamento_w		date	:= null;
nr_recibo_w		number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:old.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* Nao e possivel excluir esse cartao!
		O recibo #@NR_RECIBO#@ ja foi fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(267091, 'NR_RECIBO=' || nr_recibo_w);
	end if;
end if;
end if;

end;
/


ALTER TABLE TASY.MOVTO_CARTAO_CR ADD (
  CONSTRAINT MOVCACR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVTO_CARTAO_CR ADD (
  CONSTRAINT MOVCACR_PLSRPCR_FK 
 FOREIGN KEY (NR_SEQ_PLS_RET_PAGTO) 
 REFERENCES TASY.PLS_RET_PAGTO_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOVCACR_ATEPACI_FK 
 FOREIGN KEY (NR_SEQ_ATEND_ADIANT) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT MOVCACR_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT MOVCACR_GRPRFIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PROD) 
 REFERENCES TASY.GRUPO_PROD_FINANC (NR_SEQUENCIA),
  CONSTRAINT MOVCACR_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_CAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT MOVCACR_FOPAGCR_FK 
 FOREIGN KEY (NR_SEQ_FORMA_PAGTO) 
 REFERENCES TASY.FORMA_PAGTO_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOVCACR_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT MOVCACR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MOVCACR_BANCACR_FK 
 FOREIGN KEY (NR_SEQ_BANDEIRA) 
 REFERENCES TASY.BANDEIRA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOVCACR_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MOVCACR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MOVTO_CARTAO_CR TO NIVEL_1;

