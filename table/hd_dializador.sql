ALTER TABLE TASY.HD_DIALIZADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALIZADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALIZADOR
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_DIALIZADOR              NUMBER(10)         NOT NULL,
  DT_PRIMING                 DATE               NOT NULL,
  QT_PRIMING                 NUMBER(12,2)       NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  QT_REUSO                   NUMBER(2)          NOT NULL,
  IE_STATUS                  VARCHAR2(15 BYTE),
  DT_DEFINIDO_PACIENTE       DATE,
  DT_DESCARTE                DATE,
  DT_PRIM_LAVAGEM            DATE,
  DT_PRIM_USO                DATE,
  QT_PRIMING_FINAL           NUMBER(12,2),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  NR_LOTE                    VARCHAR2(20 BYTE)  NOT NULL,
  NR_SEQ_MODELO              NUMBER(10)         NOT NULL,
  NR_MAX_REUSO               NUMBER(10)         NOT NULL,
  NR_DIGITO_VERIF            NUMBER(2),
  NR_CAPILAR_PRC             NUMBER(10),
  IE_MOTIVO_DESCARTE         VARCHAR2(2 BYTE),
  QT_PRIMING_ATUAL           NUMBER(12,2),
  PR_PERDA                   NUMBER(6,4),
  NR_SEQ_UNID_DIALISE        NUMBER(10)         NOT NULL,
  IE_TIPO                    VARCHAR2(2 BYTE)   NOT NULL,
  CD_PF_ULT_PREPARO          VARCHAR2(10 BYTE),
  DT_ULTIMO_PREPARO          DATE,
  CD_PF_DESCARTE             VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_TRANSF       NUMBER(10),
  NR_SEQ_LOTE_FORNECEDOR     NUMBER(10),
  CD_MATERIAL                NUMBER(6),
  NR_SEQ_UNID_DIALISE_ATUAL  NUMBER(10),
  IE_STATUS_ANT              VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDIALI_ESTABEL_FK_I ON TASY.HD_DIALIZADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALI_HDMOTRDI_FK_I ON TASY.HD_DIALIZADOR
(NR_SEQ_MOTIVO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_HDMOTRDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALI_HDPAREC_FK_I ON TASY.HD_DIALIZADOR
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALI_HDTIPDI_FK_I ON TASY.HD_DIALIZADOR
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_HDTIPDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALI_HDUNIDD_FK1_I ON TASY.HD_DIALIZADOR
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_HDUNIDD_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALI_HDUNIDD_FK2_I ON TASY.HD_DIALIZADOR
(NR_SEQ_UNID_DIALISE_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_HDUNIDD_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALI_MATERIA_FK_I ON TASY.HD_DIALIZADOR
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIALI_MATLOFO_FK_I ON TASY.HD_DIALIZADOR
(NR_SEQ_LOTE_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIALI_MATLOFO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDDIALI_NRDIALSA_UK ON TASY.HD_DIALIZADOR
(NR_DIALIZADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALI_PESFISI_FK_I ON TASY.HD_DIALIZADOR
(CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIALI_PESFISI_FK2_I ON TASY.HD_DIALIZADOR
(CD_PF_DESCARTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDDIALI_PK ON TASY.HD_DIALIZADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDDIALI_UK2 ON TASY.HD_DIALIZADOR
(CD_PESSOA_FISICA, NR_CAPILAR_PRC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HD_DIALIZADOR_tp  after update ON TASY.HD_DIALIZADOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ULTIMO_PREPARO,1,4000),substr(:new.DT_ULTIMO_PREPARO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ULTIMO_PREPARO',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_PERDA,1,4000),substr(:new.PR_PERDA,1,4000),:new.nm_usuario,nr_seq_w,'PR_PERDA',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MAX_REUSO,1,4000),substr(:new.NR_MAX_REUSO,1,4000),:new.nm_usuario,nr_seq_w,'NR_MAX_REUSO',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CAPILAR_PRC,1,4000),substr(:new.NR_CAPILAR_PRC,1,4000),:new.nm_usuario,nr_seq_w,'NR_CAPILAR_PRC',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PRIMING,1,4000),substr(:new.DT_PRIMING,1,4000),:new.nm_usuario,nr_seq_w,'DT_PRIMING',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIMING,1,4000),substr(:new.QT_PRIMING,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIMING',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DIALIZADOR,1,4000),substr(:new.NR_DIALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_DIALIZADOR',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PRIM_LAVAGEM,1,4000),substr(:new.DT_PRIM_LAVAGEM,1,4000),:new.nm_usuario,nr_seq_w,'DT_PRIM_LAVAGEM',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_PRIM_USO,1,4000),substr(:new.DT_PRIM_USO,1,4000),:new.nm_usuario,nr_seq_w,'DT_PRIM_USO',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIMING_FINAL,1,4000),substr(:new.QT_PRIMING_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIMING_FINAL',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_REUSO,1,4000),substr(:new.QT_REUSO,1,4000),:new.nm_usuario,nr_seq_w,'QT_REUSO',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DEFINIDO_PACIENTE,1,4000),substr(:new.DT_DEFINIDO_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'DT_DEFINIDO_PACIENTE',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIMING_ATUAL,1,4000),substr(:new.QT_PRIMING_ATUAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIMING_ATUAL',ie_log_w,ds_w,'HD_DIALIZADOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.hd_dializador_before_update
before update ON TASY.HD_DIALIZADOR for each row
declare
dt_fim_w	date;
begin
if	(:new.ie_status is not null) and
	(:old.ie_status is not null) and
	(:new.ie_status <> :old.ie_status) then
	:new.ie_status_ant := :new.ie_status;
end if;

if (:new.ie_status = 'O') then
	select  max(dt_fim)
	into	dt_fim_w
	from	hd_dialisador_reproc a
	where	a.nr_sequencia = (select max(b.nr_sequencia)
				from    hd_dialisador_reproc b
				where   b.nr_seq_dialisador = :new.nr_sequencia
				and	nvl(a.ie_situacao,'S') <> 'I')
	and	nvl(a.ie_situacao,'S') <> 'I';


	if 	(dt_fim_w is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(222465);

	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.hd_dializador_before_insert
    BEFORE INSERT ON TASY.HD_DIALIZADOR     FOR EACH ROW
DECLARE
    bloq_cad_dialisador_w     VARCHAR2(1);
    nr_seq_tratamento_ativo_w paciente_tratamento.nr_sequencia%TYPE;
    dt_obito_pac_w            pessoa_fisica.dt_obito%TYPE;

BEGIN

    obter_param_usuario(7009,
                        284,
                        obter_perfil_ativo,
                        obter_usuario_ativo,
                        obter_estabelecimento_ativo,
                        bloq_cad_dialisador_w);

    IF bloq_cad_dialisador_w = 'S' THEN
        BEGIN
            SELECT max(nr_sequencia)
              INTO nr_seq_tratamento_ativo_w
              FROM paciente_tratamento
             WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica
               AND (dt_final_tratamento IS NULL or dt_final_tratamento > sysdate);
        if(nr_seq_tratamento_ativo_w is null) then
                Wheb_mensagem_pck.exibir_mensagem_abort(1130131);
			end if;
        END;

        SELECT max(dt_obito) INTO dt_obito_pac_w FROM pessoa_fisica WHERE cd_pessoa_fisica = :new.cd_pessoa_fisica;
        IF dt_obito_pac_w IS NOT NULL THEN
            Wheb_mensagem_pck.exibir_mensagem_abort(1130132);
        END IF;
    END IF;

END;
/


ALTER TABLE TASY.HD_DIALIZADOR ADD (
  CONSTRAINT HDDIALI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT HDDIALI_NRDIALSA_UK
 UNIQUE (NR_DIALIZADOR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT HDDIALI_UK2
 UNIQUE (CD_PESSOA_FISICA, NR_CAPILAR_PRC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALIZADOR ADD (
  CONSTRAINT HDDIALI_HDTIPDI_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.HD_MODELO_DIALISADOR (NR_SEQUENCIA),
  CONSTRAINT HDDIALI_HDUNIDD_FK1 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIALI_PESFISI_FK2 
 FOREIGN KEY (CD_PF_DESCARTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALI_HDMOTRDI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TRANSF) 
 REFERENCES TASY.HD_MOTIVO_DIALISADOR (NR_SEQUENCIA),
  CONSTRAINT HDDIALI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT HDDIALI_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNECEDOR) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT HDDIALI_HDUNIDD_FK2 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE_ATUAL) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIALI_HDPAREC_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.HD_PAC_RENAL_CRONICO (CD_PESSOA_FISICA),
  CONSTRAINT HDDIALI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT HDDIALI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.HD_DIALIZADOR TO NIVEL_1;

