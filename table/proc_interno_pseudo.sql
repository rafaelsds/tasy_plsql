ALTER TABLE TASY.PROC_INTERNO_PSEUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INTERNO_PSEUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INTERNO_PSEUDO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO   NUMBER(10)              NOT NULL,
  DS_PSEUDO             VARCHAR2(255 BYTE)      NOT NULL,
  NR_SEQ_REGRA_PSEUDO   NUMBER(10),
  DS_PSEUDO_SEM_ACENTO  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROINPS_PK ON TASY.PROC_INTERNO_PSEUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINPS_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROINPS_PROINTE_FK_I ON TASY.PROC_INTERNO_PSEUDO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINPS_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINPS_REGPSE_FK_I ON TASY.PROC_INTERNO_PSEUDO
(NR_SEQ_REGRA_PSEUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINPS_REGPSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPSEUDO_SEARCH ON TASY.PROC_INTERNO_PSEUDO
(SUBSTRB("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("DS_PSEUDO"),1,4000))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROC_INTERNO_PSEUDO_tp  after update ON TASY.PROC_INTERNO_PSEUDO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PROC_INTERNO_PSEUDO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PSEUDO,1,4000),substr(:new.DS_PSEUDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PSEUDO',ie_log_w,ds_w,'PROC_INTERNO_PSEUDO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'PROC_INTERNO_PSEUDO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.proc_interno_pseudo_atual
before insert or update ON TASY.PROC_INTERNO_PSEUDO for each row
declare

begin

if	(:new.ds_pseudo_sem_acento is null) or
	(:new.ds_pseudo <> :old.ds_pseudo) then
	:new.ds_pseudo_sem_acento := elimina_acentuacao(:new.ds_pseudo);
end if;

end;
/


ALTER TABLE TASY.PROC_INTERNO_PSEUDO ADD (
  CONSTRAINT PROINPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INTERNO_PSEUDO ADD (
  CONSTRAINT PROINPS_REGPSE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PSEUDO) 
 REFERENCES TASY.REGRA_PSEUDONIMO (NR_SEQUENCIA),
  CONSTRAINT PROINPS_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROC_INTERNO_PSEUDO TO NIVEL_1;

