ALTER TABLE TASY.DAR_TABLES_CONTROL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DAR_TABLES_CONTROL CASCADE CONSTRAINTS;

CREATE TABLE TASY.DAR_TABLES_CONTROL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_TABELA            VARCHAR2(255 BYTE),
  DS_SQL               CLOB,
  DS_DATAMODEL         VARCHAR2(255 BYTE),
  NR_SEQ_SQL           NUMBER(10),
  IE_MANUAL            VARCHAR2(1 BYTE),
  IE_INCONSISTENCIA    VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_SQL) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DTABCONT_PK ON TASY.DAR_TABLES_CONTROL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.dar_tables_control_before
before insert or update
   ON TASY.DAR_TABLES_CONTROL    for each row
declare
   sql_stmt_w          dar_tables_control.ds_sql%type;
   message_w           dar_consist_sql.ds_consistencia%type;
   message_aux_w       varchar2(4000);
   nr_posicao_espaco_w number(10);
   nr_posicao_from_w   number(10);
   nr_posicao_where_w  number(10);
   nr_tamanho_string_w number(10);
   nr_qtd_dahsboard_w  number(10);
   ds_tabela_w         varchar2(4000);
   lista_tabela_w      lista_varchar_pck.tabela_varchar;
   ds_sql_1_w          varchar2(32767);
   ds_sql_2_w          varchar2(32767);
   ds_sql_3_w          varchar2(32767);
   ds_sql_4_w          varchar2(32767);
   qt_tamanho_sql_w    number(10);

begin

   if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

      if updating then
         --Se possuir dependencias, nao pode alterar o ds_sql
         select count(a.nr_sequencia)
           into nr_qtd_dahsboard_w
           from dar_dashboard a, dar_app b, dar_app_datamodels c
          where a.nr_seq_app = b.nr_sequencia
            and c.nr_seq_app = b.nr_sequencia
            and c.nr_seq_table_control = :new.nr_sequencia;

        if (nr_qtd_dahsboard_w > 0 ) then
          wheb_mensagem_pck.exibir_mensagem_abort(1150676);
        end if;
      end if;

      delete from dar_consist_sql
       where nr_seq_tab_controle = :new.nr_sequencia;

      sql_stmt_w := :new.ds_sql;

      if (nvl(:new.IE_MANUAL, 'N') = 'S') then

         :new.ie_inconsistencia := 'N';

         -- Tamanho em caracteres do sql gerado
         qt_tamanho_sql_w := nvl(dbms_lob.getlength(sql_stmt_w), 0);

         -- Primeira parte da string
         ds_sql_1_w := dbms_lob.substr(sql_stmt_w, 32767, 1);

         -- Segunda parte da string
         if (qt_tamanho_sql_w > 32767) then
            --
            ds_sql_2_w := dbms_lob.substr(sql_stmt_w, 32767, 32768);
         end if;
         -- Terceira parte da string
         if (qt_tamanho_sql_w > 65534) then
            --
            ds_sql_3_w := dbms_lob.substr(sql_stmt_w, 32767, 65535);
         end if;
         -- Quarta parte da string
         if (qt_tamanho_sql_w > 98302) then
            --
            ds_sql_4_w := dbms_lob.substr(sql_stmt_w, 32767, 98303);
         end if;

         -- Bloco de exception
         begin
            -- execucao do script de criacao da tabela
            EXECUTE IMMEDIATE ds_sql_1_w || ds_sql_2_w || ds_sql_3_w ||
                              ds_sql_4_w;
         exception
            when others then
               -- Sql invalido
               :new.ie_inconsistencia := 'S';

               select obter_desc_expressao(1032844)
                 into message_w
                 from dual;

               message_aux_w := message_w || '  ';

         end;

         nr_posicao_espaco_w    := instr(sql_stmt_w, '(');

         if (nr_posicao_espaco_w = 0) then
            nr_posicao_espaco_w := instr(sql_stmt_w, ')');
         end if;

         if (nr_posicao_espaco_w > 0) then
            -- Function ou procedure

            :new.ie_inconsistencia := 'S';

            select obter_desc_expressao(1042220) into message_w from dual;

            message_aux_w := message_aux_w || message_w || '  ';

         end if;

         nr_posicao_espaco_w := instr(sql_stmt_w, '*');
         if (nr_posicao_espaco_w > 0) then
            -- Uso de Asterisco
            :new.ie_inconsistencia := 'S';

            select obter_desc_expressao(1042224) into message_w from dual;

            message_aux_w := message_aux_w || message_w || '  ';

         end if;

         select instr(upper(:new.ds_sql), 'FROM'),
                instr(upper(:new.ds_sql), 'WHERE'),
                length(:new.ds_sql)
           into nr_posicao_from_w, nr_posicao_where_w, nr_tamanho_string_w
           from dual;

         if (nr_posicao_where_w = 0) then
            select trim(substr(:new.ds_sql,
                               nr_posicao_from_w + 4,
                               nr_tamanho_string_w - (nr_posicao_from_w)))
              into ds_tabela_w
              from dual;
         else
            select trim(substr(:new.ds_sql,
                               nr_posicao_from_w + 4,
                               nr_posicao_where_w - (nr_posicao_from_w + 5)))
              into ds_tabela_w
              from dual;
         end if;

         if (instr(ds_tabela_w, ',') > 0) then

            lista_tabela_w := obter_lista_string2(ds_tabela_w, ',');

            for i in 1 .. lista_tabela_w.last loop
               ds_tabela_w := trim(replace(trim(lista_tabela_w(i)),
                                           chr(10),
                                           ' '));
               if (instr(upper(ds_tabela_w), '_V') = 0) then
                  -- Uso de tabela sem acesso por view
                  :new.ie_inconsistencia := 'S';

                  select obter_desc_expressao(1042222)
                    into message_w
                    from dual;

                  message_aux_w := message_aux_w || message_w || '  ';

               end if;
            end loop;
         else
            if (instr(upper(ds_tabela_w), '_V') = 0) then
               -- Uso de tabela sem acesso por view
               :new.ie_inconsistencia := 'S';

               select obter_desc_expressao(1042222)
                 into message_w
                 from dual;

               message_aux_w := message_aux_w || message_w || '  ';
            end if;
         end if;

         if (:new.ie_inconsistencia = 'S') then

            insert into dar_consist_sql
               (nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                nr_seq_tab_controle,
                ds_consistencia,
                nr_seq_sql)
            values
               (dar_consist_sql_seq.nextval,
                sysdate,
                wheb_usuario_pck.get_nm_usuario,
                :new.nr_sequencia,
                message_aux_w,
                :new.nr_seq_sql);
         end if;
      end if;
   end if;
end dar_tables_control_before;
/


CREATE OR REPLACE TRIGGER TASY.dar_tables_control_delete
  after delete ON TASY.DAR_TABLES_CONTROL   for each row
begin

   if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

      if (nvl(:new.IE_MANUAL, 'N') = 'S') then
         delete dar_tab_control_fields
          where nr_seq_table_control = :new.nr_sequencia;
      end if;

   end if;

end dar_tables_control_delete;
/


CREATE OR REPLACE TRIGGER TASY.dar_tables_control_insert
AFTER INSERT
   ON TASY.DAR_TABLES_CONTROL    FOR EACH ROW
DECLARE
   sql_stmt_w dar_tables_control.ds_sql%TYPE;
   message_w dar_consist_sql.ds_consistencia%TYPE;
   segment_size_w number;

BEGIN

    delete from dar_consist_sql where nr_seq_tab_controle = :new.nr_sequencia;

    sql_stmt_w := :new.ds_sql;

    execute immediate
    'EXPLAIN PLAN SET STATEMENT_ID = '''|| :new.nr_sequencia || ''' INTO PLAN_TABLE FOR ' || to_char(sql_stmt_w);

    for qp in(
        select
            a.operation,
            a.options,
            a.object_owner,
            a.object_name,
            a.object_type,
            a.id,
            a.parent_id,
            a.depth,
            a.cardinality,
            coalesce(decode(t.num_rows,0,1,t.num_rows),decode(i.num_rows,0,1,i.num_rows),1) as obj_num_rows,
            a.access_predicates,
            a.filter_predicates
        from plan_table a
    left join user_tables t on(a.object_name = t.table_name)
    left join user_indexes i on(a.object_name = i.index_name)
    where statement_id = to_char(:new.nr_sequencia))

    loop
        -- If the object is a table or an index and the access method is %FULL%
        if ((qp.object_type = 'TABLE' or qp.object_type like 'INDEX%')
            and qp.options like '%FULL%')
        then
            -- Return segment size
            select round(bytes/1024/1024)
            into segment_size_w
            from user_segments
            where segment_name = upper(qp.object_name);
            -- Check if the object is bigger than 2GB
            --  or the resulting cardinality of the operation is bigger than 50% of rows in the object
            --      (possibly meaning bad filters / lot of rows for the parent operation)
            --      and the object has at least 10 thousand rows
            --  or the full access have no filter predicates (meaning it will pass the rows to the parent for filtering)
            --      and the object has at least 5 thousand rows
            if (segment_size_w > 2048
                or (round(qp.cardinality*100/qp.obj_num_rows) > 50 and qp.obj_num_rows > 10000)
                or ((qp.obj_num_rows > 5000  and qp.filter_predicates is null) or qp.cardinality > 10000)
                )
            then
                -- Build a message that explains the finding
                message_w := message_w || 'Identified "' || qp.operation || ' ' || qp.options || '" on a ' || segment_size_w ||
                ' MB ' || lower(qp.object_type) || ' ' || qp.object_owner || '.' || qp.object_name
                || '. Estimated operation cardinality: ' || qp.cardinality || '. Filter predicates: ' || nvl(qp.filter_predicates, 'NONE')
                || '. Plan operation id: ' || qp.id || '. Object number of rows: ' || qp.obj_num_rows || '. ';
            end if;
        -- If the method is not a full scan, check for inneficient range/skip scans
        -- If the operation estimated cardinality return more than 10% of object's rows or more than X thousand rows
        -- Ex.: 5 thousand rows probably means 5 thousand accesses to the table associated with the index
        elsif (qp.operation = 'INDEX'
            and qp.options in('RANGE SCAN','SKIP SCAN')
            and qp.object_owner = sys_context('USERENV','SESSION_USER')
            and ((round(qp.cardinality*100/qp.obj_num_rows) > 10 and qp.obj_num_rows > 2000) or qp.cardinality > 2000)
            )
        then
            message_w := message_w || 'Identified a possibly bad index access method: "' || qp.operation || ' ' || qp.options || '" on ' ||
            qp.object_owner || '.' || qp.object_name || '. Plan operation id: ' || qp.id || '. ';
        end if;

    if     (nvl(message_w,'X') <> 'X')then

        insert into dar_consist_sql(nr_sequencia,
            dt_atualizacao,
            nm_usuario,
            nr_seq_tab_controle,
            ds_consistencia,
			nr_seq_sql)
        values (dar_consist_sql_seq.nextval,
            sysdate,
            wheb_usuario_pck.get_nm_usuario,
            :new.nr_sequencia,
            message_w,
			:new.nr_seq_sql);
    end if;
    end loop;

END dar_tables_control_insert;
/


ALTER TABLE TASY.DAR_TABLES_CONTROL ADD (
  CONSTRAINT DTABCONT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.DAR_TABLES_CONTROL TO NIVEL_1;

