DROP TABLE TASY.W_MXM_ARQCONTABIL_TADC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_MXM_ARQCONTABIL_TADC
(
  TADC_SQPROCESSO    NUMBER(9),
  TADC_SQREGISTRO    NUMBER(9),
  TADC_STOPERACAO    NUMBER(1),
  TADC_CDEMPRESA     VARCHAR2(4 BYTE),
  TADC_LOTE          VARCHAR2(6 BYTE),
  TADC_DTLANCAMENTO  VARCHAR2(10 BYTE),
  TADC_NODOCUMENTO   VARCHAR2(6 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_MXM_ARQCONTABIL_TADC TO NIVEL_1;

