ALTER TABLE TASY.ARM_NOTIFICATION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ARM_NOTIFICATION CASCADE CONSTRAINTS;

CREATE TABLE TASY.ARM_NOTIFICATION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_INTERVAL          NUMBER(5)                NOT NULL,
  DT_LAST_EXECUTION    DATE,
  NR_SEQ_RELATORIO     NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_EMAIL_REMETENTE   VARCHAR2(255 BYTE),
  DS_CONTEUDO          VARCHAR2(4000 BYTE),
  QT_DIAS_INICIO       NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ARMNOTIF_PK ON TASY.ARM_NOTIFICATION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ARMNOTIF_RELATOR_FK_I ON TASY.ARM_NOTIFICATION
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ARM_NOTIFICATION ADD (
  CONSTRAINT ARMNOTIF_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ARM_NOTIFICATION ADD (
  CONSTRAINT ARMNOTIF_RELATOR_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ARM_NOTIFICATION TO NIVEL_1;

