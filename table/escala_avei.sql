ALTER TABLE TASY.ESCALA_AVEI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_AVEI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_AVEI
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  IE_CONCIENCIA          NUMBER(1)              NOT NULL,
  IE_TEMPORAL            NUMBER(1)              NOT NULL,
  IE_COMANDOS            NUMBER(1)              NOT NULL,
  IE_MOVIMENTOS          NUMBER(1)              NOT NULL,
  IE_VISUAL              NUMBER(1)              NOT NULL,
  IE_FACIAL              NUMBER(1)              NOT NULL,
  IE_SENSIBILIDADE       NUMBER(1)              NOT NULL,
  IE_ATAXIA              NUMBER(1)              NOT NULL,
  IE_ATAXIA_BE           NUMBER(1),
  IE_ATAXIA_BD           NUMBER(1),
  IE_ATAXIA_PE           NUMBER(1),
  IE_ATAXIA_PD           NUMBER(1),
  IE_LINGUAGEM           NUMBER(1)              NOT NULL,
  IE_DISARTRIA           NUMBER(1)              NOT NULL,
  IE_DESATENCAO          NUMBER(1)              NOT NULL,
  QT_PONTUACAO           NUMBER(3)              NOT NULL,
  IE_MOTROCIDADE_PE      NUMBER(1)              NOT NULL,
  IE_MOTROCIDADE_PD      NUMBER(1)              NOT NULL,
  IE_MOTROCIDADE_BE      NUMBER(1)              NOT NULL,
  IE_MOTROCIDADE_BD      NUMBER(1)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAVEI_ATEPACI_FK_I ON TASY.ESCALA_AVEI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAVEI_PESFISI_FK2_I ON TASY.ESCALA_AVEI
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAVEI_PK ON TASY.ESCALA_AVEI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAVEI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_avei_atual
before insert or update ON TASY.ESCALA_AVEI for each row
declare
qt_reg_w	number(1);

begin

if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_liberacao is null)  then
	begin

	:new.qt_pontuacao := 	nvl(:new.ie_conciencia,0)+
				nvl(:new.ie_temporal,0) 	+
				nvl(:new.ie_comandos,0) 	+
				nvl(:new.ie_movimentos,0)	+
				nvl(:new.ie_visual,0) 		+
				nvl(:new.ie_facial,0) 		+
				nvl(:new.ie_sensibilidade,0)+
				nvl(:new.ie_ataxia,0)		+
				nvl(:new.ie_linguagem,0)	+
				nvl(:new.ie_desatencao,0);

        if not (nvl(:new.ie_disartria,0) = 9) then
            :new.qt_pontuacao := nvl(:new.ie_disartria,0) + :new.qt_pontuacao;
        end if;

        if not (nvl(:new.ie_motrocidade_pe,0) = 9) then
            :new.qt_pontuacao := nvl(:new.ie_motrocidade_pe,0) + :new.qt_pontuacao;
        end if;

        if not (nvl(:new.ie_motrocidade_pd,0) = 9) then
            :new.qt_pontuacao := nvl(:new.ie_motrocidade_pd,0) + :new.qt_pontuacao;
        end if;

        if not (nvl(:new.ie_motrocidade_be,0) = 9) then
            :new.qt_pontuacao := nvl(:new.ie_motrocidade_be,0) + :new.qt_pontuacao;
        end if;

        if not (nvl(:new.ie_motrocidade_bd,0) = 9) then
            :new.qt_pontuacao := nvl(:new.ie_motrocidade_bd,0) + :new.qt_pontuacao;
        end if;

	end;
end if;

<<Final>>
qt_reg_w	:= 0;


end escala_avei_atual;
/


ALTER TABLE TASY.ESCALA_AVEI ADD (
  CONSTRAINT ESCAVEI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_AVEI ADD (
  CONSTRAINT ESCAVEI_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAVEI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_AVEI TO NIVEL_1;

