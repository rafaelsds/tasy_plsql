ALTER TABLE TASY.INSP_AVAL_RESPOSTA_PDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INSP_AVAL_RESPOSTA_PDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.INSP_AVAL_RESPOSTA_PDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_ORDEM_COMPRA      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PERGUNTA      NUMBER(10)               NOT NULL,
  DS_RESPOSTA          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INARPDA_ESTABEL_FK_I ON TASY.INSP_AVAL_RESPOSTA_PDA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INARPDA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INARPDA_INAPPDA_FK_I ON TASY.INSP_AVAL_RESPOSTA_PDA
(NR_SEQ_PERGUNTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INARPDA_INAPPDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INARPDA_ORDCOMP_FK_I ON TASY.INSP_AVAL_RESPOSTA_PDA
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INARPDA_ORDCOMP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.INARPDA_PK ON TASY.INSP_AVAL_RESPOSTA_PDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INSP_AVAL_RESPOSTA_PDA ADD (
  CONSTRAINT INARPDA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INSP_AVAL_RESPOSTA_PDA ADD (
  CONSTRAINT INARPDA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT INARPDA_INAPPDA_FK 
 FOREIGN KEY (NR_SEQ_PERGUNTA) 
 REFERENCES TASY.INSP_AVAL_PERGUNTA_PDA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT INARPDA_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA));

GRANT SELECT ON TASY.INSP_AVAL_RESPOSTA_PDA TO NIVEL_1;

