ALTER TABLE TASY.RECEBIMENTO_FRAC_MED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RECEBIMENTO_FRAC_MED CASCADE CONSTRAINTS;

CREATE TABLE TASY.RECEBIMENTO_FRAC_MED
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  QT_DOSE                  NUMBER(15,3)         NOT NULL,
  NR_SEQ_ATENDIMENTO       NUMBER(10)           NOT NULL,
  NR_SEQ_MATERIAL          NUMBER(10)           NOT NULL,
  DT_RECEBIMENTO           DATE,
  NM_USUARIO_RECEBIMENTO   VARCHAR2(15 BYTE),
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  DT_CANCELAMENTO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REFRMED_PACATME_FK_I ON TASY.RECEBIMENTO_FRAC_MED
(NR_SEQ_ATENDIMENTO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REFRMED_PACATME_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REFRMED_PK ON TASY.RECEBIMENTO_FRAC_MED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REFRMED_PK
  MONITORING USAGE;


ALTER TABLE TASY.RECEBIMENTO_FRAC_MED ADD (
  CONSTRAINT REFRMED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RECEBIMENTO_FRAC_MED ADD (
  CONSTRAINT REFRMED_PACATME_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PACIENTE_ATEND_MEDIC (NR_SEQ_ATENDIMENTO,NR_SEQ_MATERIAL));

GRANT SELECT ON TASY.RECEBIMENTO_FRAC_MED TO NIVEL_1;

