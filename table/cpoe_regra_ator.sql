ALTER TABLE TASY.CPOE_REGRA_ATOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_REGRA_ATOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_REGRA_ATOR
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  IE_DIETETIC_ORDER_TYPE        VARCHAR2(5 BYTE),
  IE_ENTERAL_ORDER_TYPE         VARCHAR2(5 BYTE),
  IE_IMAGING_ORDER_TYPE         VARCHAR2(5 BYTE),
  DT_ATUALIZACAO                DATE            NOT NULL,
  IE_INFUSION_ORDER_TYPE        VARCHAR2(5 BYTE),
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  IE_MEDICATION_ORDER_TYPE      VARCHAR2(5 BYTE),
  DT_ATUALIZACAO_NREC           DATE,
  IE_RECOMMENDATION_ORDER_TYPE  VARCHAR2(5 BYTE),
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  IE_GAS_ORDER_TYPE             VARCHAR2(5 BYTE),
  IE_OTHER_ORDER_TYPE           VARCHAR2(5 BYTE),
  IE_PROC_ORDER_TYPE            VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOEAT_ESTABEL_FK_I ON TASY.CPOE_REGRA_ATOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOEAT_PK ON TASY.CPOE_REGRA_ATOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_REGRA_ATOR ADD (
  CONSTRAINT CPOEAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_REGRA_ATOR ADD (
  CONSTRAINT CPOEAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CPOE_REGRA_ATOR TO NIVEL_1;

