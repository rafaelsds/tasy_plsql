ALTER TABLE TASY.QHAPDC_FILE_DATA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QHAPDC_FILE_DATA CASCADE CONSTRAINTS;

CREATE TABLE TASY.QHAPDC_FILE_DATA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_FILE_CONTENT      CLOB,
  NR_SEQ_FILE          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QH_FILE_DA_PK ON TASY.QHAPDC_FILE_DATA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QH_FILE_DA_QHAPDC_FIL_FK_I ON TASY.QHAPDC_FILE_DATA
(NR_SEQ_FILE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QHAPDC_FILE_DATA ADD (
  CONSTRAINT QH_FILE_DA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.QHAPDC_FILE_DATA ADD (
  CONSTRAINT QH_FILE_DA_QHAPDC_FIL_FK 
 FOREIGN KEY (NR_SEQ_FILE) 
 REFERENCES TASY.QHAPDC_FILE (NR_SEQUENCIA));

GRANT SELECT ON TASY.QHAPDC_FILE_DATA TO NIVEL_1;

