DROP TABLE TASY.W_MOVIMENTO_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_MOVIMENTO_CONTABIL
(
  NR_LOTE_CONTABIL        NUMBER(10),
  NR_SEQUENCIA            NUMBER(10),
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE),
  IE_DEBITO_CREDITO       VARCHAR2(1 BYTE),
  CD_HISTORICO            NUMBER(10),
  DT_MOVIMENTO            DATE,
  VL_MOVIMENTO            NUMBER(15,2),
  CD_CENTRO_CUSTO         NUMBER(8),
  DS_COMPL_HISTORICO      VARCHAR2(255 BYTE),
  DS_DOC_AGRUPAMENTO      VARCHAR2(50 BYTE),
  NR_SEQ_AGRUPAMENTO      NUMBER(15),
  NR_SEQ_TRANS_FIN        NUMBER(10),
  CD_CGC                  VARCHAR2(14 BYTE),
  NR_DOCUMENTO            VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  CD_ESTABELECIMENTO      NUMBER(15),
  IE_TRANSITORIO          VARCHAR2(1 BYTE),
  IE_ORIGEM_DOCUMENTO     NUMBER(5),
  NR_SEQ_TAB_ORIG         NUMBER(10),
  NM_TABELA               VARCHAR2(30 BYTE),
  NR_SEQ_TAB_COMPL        NUMBER(10),
  NR_SEQ_INFO             NUMBER(10),
  NM_ATRIBUTO             VARCHAR2(255 BYTE),
  NR_LANCAMENTO           NUMBER(10),
  NR_SEQ_REGRA_TF         NUMBER(10),
  DS_CONSISTENCIA         VARCHAR2(255 BYTE),
  NR_DOC_ANALITICO        NUMBER(14),
  NR_SEQ_CLASSIF_MOVTO    NUMBER(10),
  IE_INTERCOMPANY         VARCHAR2(1 BYTE),
  CD_ESTAB_INTERCOMPANY   NUMBER(4),
  CD_SEQUENCIA_PARAMETRO  NUMBER(10),
  NR_CODIGO_CONTROLE      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WMOVCON_I1 ON TASY.W_MOVIMENTO_CONTABIL
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.w_movto_contabil_insert
before insert ON TASY.W_MOVIMENTO_CONTABIL 
for each row
declare

cd_estab_centro_w centro_custo.cd_estabelecimento%type;

begin

if	(:new.cd_centro_custo is not null) then
	begin
	select a.cd_estabelecimento
	into cd_estab_centro_w
	from centro_custo a
	where a.cd_centro_custo = :new.cd_centro_custo;

	if	(nvl(:new.cd_estabelecimento,0) !=
		 nvl(cd_estab_centro_w,0)) then
		:new.cd_estabelecimento := cd_estab_centro_w;
	end if;
	exception when others then
		cd_estab_centro_w	:= null;
	end;
end if;

end;
/


GRANT SELECT ON TASY.W_MOVIMENTO_CONTABIL TO NIVEL_1;

