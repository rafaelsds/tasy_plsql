ALTER TABLE TASY.RP_LICENCA_PROFISSIONAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RP_LICENCA_PROFISSIONAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.RP_LICENCA_PROFISSIONAL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_LICENCA  NUMBER(10)             NOT NULL,
  DT_PERIODO_INICIAL     DATE,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_PERIODO_FINAL       DATE,
  DT_LIBERACAO           DATE,
  NM_USUARIO_LIBERACAO   VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_SUBSTITUTO          VARCHAR2(10 BYTE),
  IE_VARIOS_SUBS         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RPLIPRO_ESTABEL_FK_I ON TASY.RP_LICENCA_PROFISSIONAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIPRO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPLIPRO_PESFISI_FK_I ON TASY.RP_LICENCA_PROFISSIONAL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RPLIPRO_PESFISI_FK2_I ON TASY.RP_LICENCA_PROFISSIONAL
(CD_SUBSTITUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RPLIPRO_PK ON TASY.RP_LICENCA_PROFISSIONAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIPRO_PK
  MONITORING USAGE;


CREATE INDEX TASY.RPLIPRO_RPMOTLI_FK_I ON TASY.RP_LICENCA_PROFISSIONAL
(NR_SEQ_MOTIVO_LICENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIPRO_RPMOTLI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RP_LICENCA_PROFISSIONAL ADD (
  CONSTRAINT RPLIPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RP_LICENCA_PROFISSIONAL ADD (
  CONSTRAINT RPLIPRO_RPMOTLI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_LICENCA) 
 REFERENCES TASY.RP_MOTIVO_LICENCA (NR_SEQUENCIA),
  CONSTRAINT RPLIPRO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RPLIPRO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RPLIPRO_PESFISI_FK2 
 FOREIGN KEY (CD_SUBSTITUTO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.RP_LICENCA_PROFISSIONAL TO NIVEL_1;

