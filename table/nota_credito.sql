ALTER TABLE TASY.NOTA_CREDITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_CREDITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_CREDITO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  VL_NOTA_CREDITO               NUMBER(15,2)    NOT NULL,
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  CD_CGC                        VARCHAR2(14 BYTE),
  DT_NOTA_CREDITO               DATE            NOT NULL,
  DT_VENCIMENTO                 DATE,
  CD_MOEDA                      NUMBER(3),
  TX_JUROS                      NUMBER(7,4),
  TX_MULTA                      NUMBER(7,4),
  CD_TIPO_TAXA_JURO             NUMBER(10),
  CD_TIPO_TAXA_MULTA            NUMBER(10),
  NR_SEQ_LOTE_AUDIT_HIST        NUMBER(10),
  NR_SEQ_TRANS_FIN_CONTAB       NUMBER(10),
  IE_ORIGEM                     VARCHAR2(5 BYTE) NOT NULL,
  VL_SALDO                      NUMBER(15,2),
  NR_SEQ_MOTIVO                 NUMBER(10),
  DS_OBSERVACAO                 VARCHAR2(4000 BYTE),
  NR_SEQ_LOTE_HIST_GUIA         NUMBER(10),
  NR_LOTE_CONTABIL              NUMBER(10),
  NR_SEQ_TIT_REC_COBR           NUMBER(10),
  IE_SITUACAO                   VARCHAR2(3 BYTE),
  NR_SEQ_REGRA_PAG_DUPLIC       NUMBER(10),
  NR_TITULO_RECEBER             NUMBER(10),
  NR_SEQ_PAGADOR_APROP          NUMBER(10),
  NR_SEQ_TRANS_BAIXA_TIT_PAGAR  NUMBER(10),
  NR_SEQ_CONTA_BANCO            NUMBER(10),
  NR_SEQ_MOVTO_ORIGEM           NUMBER(10),
  NR_SEQ_BAIXA_ORIGEM           NUMBER(5),
  NR_SEQ_LOTE_ADIT_TIT_REC      NUMBER(10),
  NR_SEQ_RETORNO                NUMBER(10),
  IE_SITUACAO_TIT_REC           VARCHAR2(1 BYTE),
  DT_CANCELAMENTO               DATE,
  NR_LOTE_CONTABIL_EST          NUMBER(10),
  NR_SEQ_NOTA_ORIGEM            NUMBER(10),
  NR_SEQ_CREDITO_N_IDENT        NUMBER(10),
  VL_NOTA_CRED_ESTRANG          NUMBER(15,2),
  VL_COTACAO                    NUMBER(21,10),
  VL_COMPLEMENTO                NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTCRED_BANESTA_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_ESTABEL_FK_I ON TASY.NOTA_CREDITO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_LOAUTIRE_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_LOTE_ADIT_TIT_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_LOAUTIRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_LOTAUDGUI_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_LOTE_HIST_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_LOTAUDGUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_LOTAUHI_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_LOTE_AUDIT_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_LOTCONT_FK_I ON TASY.NOTA_CREDITO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_LOTCONT_FK2_I ON TASY.NOTA_CREDITO
(NR_LOTE_CONTABIL_EST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_LOTCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_MOEDA_FK_I ON TASY.NOTA_CREDITO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_MOTIVNC_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_MOTIVNC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_MOVBANPEN_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_CREDITO_N_IDENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_MOVTRFI_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_MOVTO_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_NOTCRED_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_NOTA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_PESFISI_FK_I ON TASY.NOTA_CREDITO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_PESJURI_FK_I ON TASY.NOTA_CREDITO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NOTCRED_PK ON TASY.NOTA_CREDITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_PLCONPAG_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_PAGADOR_APROP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_REGAPAD_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_REGRA_PAG_DUPLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_REGAPAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_TIRECOB_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_TIT_REC_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_TIRELIQ_FK_I ON TASY.NOTA_CREDITO
(NR_TITULO_RECEBER, NR_SEQ_BAIXA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTCRED_TITRECE_FK_I ON TASY.NOTA_CREDITO
(NR_TITULO_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_TRAFINA_FK_I ON TASY.NOTA_CREDITO
(NR_SEQ_TRANS_FIN_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTCRED_TRAFINA_FK2_I ON TASY.NOTA_CREDITO
(NR_SEQ_TRANS_BAIXA_TIT_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTCRED_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.nota_credito_insert
before insert ON TASY.NOTA_CREDITO for each row
declare

qt_regra_w			number(3);
ie_consiste_data_retroativa	varchar2(1);
qt_mes_fechado_w		number(10);

begin

obter_param_usuario(9051, 12, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_consiste_data_retroativa);

select 	count(*)
into 	qt_mes_fechado_w
from	estabelecimento b,
	ctb_mes_ref a
where 	trunc(a.dt_referencia,'month')	= trunc(:new.dt_nota_credito,'month')
and	a.cd_empresa 		= b.cd_empresa
and	b.cd_estabelecimento	= :new.cd_estabelecimento
and	substr(ctb_obter_se_mes_fechado(a.nr_sequencia,b.cd_estabelecimento),1,1) = 'F';

if 	(ie_consiste_data_retroativa = 'N') and
	(qt_mes_fechado_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(236060);
end if;

begin
select	1
into	qt_regra_w
from	regra_nota_credito
where	rownum = 1;
exception
when others then
	qt_regra_w := 0;
end;

if	(qt_regra_w > 0) then
	obter_regra_nota_credito(
		:new.ie_origem,
		:new.dt_nota_credito,
		:new.nr_seq_trans_baixa_tit_pagar,
		:new.nr_seq_trans_fin_contab,
		:new.dt_vencimento);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.NOTA_CREDITO_tp  after update ON TASY.NOTA_CREDITO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.VL_NOTA_CREDITO,1,500);gravar_log_alteracao(substr(:old.VL_NOTA_CREDITO,1,4000),substr(:new.VL_NOTA_CREDITO,1,4000),:new.nm_usuario,nr_seq_w,'VL_NOTA_CREDITO',ie_log_w,ds_w,'NOTA_CREDITO',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_NOTA_CRED_ESTRANG,1,500);gravar_log_alteracao(substr(:old.VL_NOTA_CRED_ESTRANG,1,4000),substr(:new.VL_NOTA_CRED_ESTRANG,1,4000),:new.nm_usuario,nr_seq_w,'VL_NOTA_CRED_ESTRANG',ie_log_w,ds_w,'NOTA_CREDITO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.NOTA_CREDITO_AFTER
after insert or update ON TASY.NOTA_CREDITO for each row
declare

cursor c01 is
select		a.nm_atributo,
		a.cd_tipo_lote_contab
from		atributo_contab a
where 		a.cd_tipo_lote_contab = 29
and 		a.nm_atributo = 'VL_NOTA_CREDITO';

c01_w		c01%rowtype;

begin

if	(inserting) then

	open c01;
	loop
	fetch c01 into
	c01_w;
	exit when c01%notfound;
		begin

		if	(:new.nr_seq_trans_fin_contab is not null) then
			begin
			ctb_concil_financeira_pck.ctb_gravar_documento(	:new.cd_estabelecimento,
									trunc(:new.dt_nota_credito),
									c01_w.cd_tipo_lote_contab,
									:new.nr_seq_trans_fin_contab,
									90,
									:new.nr_sequencia,
									null,
									null,
									:new.vl_nota_credito,
									'NOTA_CREDITO',
									c01_w.nm_atributo,
									:new.nm_usuario);
			end;
		end if;
		end;
	end loop;
	close c01;

elsif (updating) then

	if	(:old.ie_situacao != 'C') and
		(:new.ie_situacao = 'C') then
		begin

		-- if	(:new.nr_seq_trans_fin_contab is not null) then
			-- begin
			-- ctb_concil_financeira_pck.ctb_gravar_documento(	:new.cd_estabelecimento,
									-- trunc(:new.dt_nota_credito),
									-- c01_w.cd_tipo_lote_contab,
									-- :new.nr_seq_trans_fin_contab,
									-- 90,
									-- :new.nr_sequencia,
									-- null,
									-- null,
									-- :new.vl_nota_credito *-1,
									-- 'NOTA_CREDITO',
									-- c01_w.nm_atributo,
									-- :new.nm_usuario);
			-- end;
		-- end if;

		open c01;
		loop
		fetch c01 into
		c01_w;
		exit when c01%notfound;
			begin

			if	(:new.nr_seq_trans_fin_contab is not null) then
				begin
				ctb_concil_financeira_pck.ctb_gravar_documento(	:new.cd_estabelecimento,
										trunc(:new.dt_nota_credito),
										c01_w.cd_tipo_lote_contab,
										:new.nr_seq_trans_fin_contab,
										90,
										:new.nr_sequencia,
										null,
										null,
										:new.vl_nota_credito *-1,
										'NOTA_CREDITO',
										c01_w.nm_atributo,
										:new.nm_usuario);
				end;
			end if;
			end;
		end loop;
		close c01;


		end;
	else

		update  ctb_documento
		set     vl_movimento = :new.vl_nota_credito,
			nr_seq_trans_financ = :new.nr_seq_trans_fin_contab
		where   nm_atributo = 'VL_NOTA_CREDITO'
		and     nm_tabela   = 'NOTA_CREDITO'
		and     nr_documento = :new.nr_sequencia;
	end if;

end if;

end NOTA_CREDITO_AFTER;
/


ALTER TABLE TASY.NOTA_CREDITO ADD (
  CONSTRAINT NOTCRED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_CREDITO ADD (
  CONSTRAINT NOTCRED_MOVBANPEN_FK 
 FOREIGN KEY (NR_SEQ_CREDITO_N_IDENT) 
 REFERENCES TASY.MOVTO_BANCO_PEND (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_EST) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT NOTCRED_NOTCRED_FK 
 FOREIGN KEY (NR_SEQ_NOTA_ORIGEM) 
 REFERENCES TASY.NOTA_CREDITO (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_LOAUTIRE_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ADIT_TIT_REC) 
 REFERENCES TASY.LOTE_AUDIT_TIT_REC (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT NOTCRED_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_CONTAB) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT NOTCRED_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NOTCRED_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT NOTCRED_MOTIVNC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_NOTA_CREDITO (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT NOTCRED_TIRECOB_FK 
 FOREIGN KEY (NR_SEQ_TIT_REC_COBR) 
 REFERENCES TASY.TITULO_RECEBER_COBR (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_REGAPAD_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PAG_DUPLIC) 
 REFERENCES TASY.REGRA_ACAO_PAG_DUPLIC (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_TITRECE_FK 
 FOREIGN KEY (NR_TITULO_RECEBER) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT NOTCRED_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR_APROP) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_BAIXA_TIT_PAGAR) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_MOVTRFI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_ORIGEM) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT NOTCRED_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO_RECEBER, NR_SEQ_BAIXA_ORIGEM) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA),
  CONSTRAINT NOTCRED_LOTAUDGUI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_HIST_GUIA) 
 REFERENCES TASY.LOTE_AUDIT_HIST_GUIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.NOTA_CREDITO TO NIVEL_1;

