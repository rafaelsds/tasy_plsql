ALTER TABLE TASY.PLS_EMAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EMAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EMAIL
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  DS_REMETENTE                  VARCHAR2(255 BYTE),
  DS_DESTINATARIO               VARCHAR2(255 BYTE),
  DS_CC                         VARCHAR2(255 BYTE),
  DS_CCO                        VARCHAR2(255 BYTE),
  DS_ASSUNTO                    VARCHAR2(255 BYTE),
  DS_MENSAGEM                   VARCHAR2(4000 BYTE),
  IE_STATUS                     VARCHAR2(2 BYTE),
  IE_TIPO_DOCUMENTO             VARCHAR2(15 BYTE),
  IE_TIPO_MENSAGEM              VARCHAR2(15 BYTE),
  IE_ORIGEM                     VARCHAR2(15 BYTE),
  NR_SEQ_PROCESSAMENTO          NUMBER(10),
  NR_SEQ_MENSALIDADE            NUMBER(10),
  CD_PRIORIDADE                 NUMBER(2),
  NR_SEQ_PARAMETROS             NUMBER(10),
  NR_SEQ_SOLIC_RESCISAO         NUMBER(10),
  DS_SERVIDOR_ENVIO             VARCHAR2(255 BYTE),
  NR_SEQ_PROG_REAJ_COL          NUMBER(10),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  CD_CGC                        VARCHAR2(14 BYTE),
  NR_SEQ_PORTABILIDADE          NUMBER(10),
  NR_SEQ_PROPOSTA_ONLINE        NUMBER(10),
  IE_CONFIRMACAO_LEITURA        VARCHAR2(1 BYTE),
  NR_SEQ_DMED_IRPF_PAYER        NUMBER(10),
  NR_SEQ_SOLICITACAO_COMERCIAL  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSEMAIL_ESTABEL_FK_I ON TASY.PLS_EMAIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_I1 ON TASY.PLS_EMAIL
(IE_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_I2 ON TASY.PLS_EMAIL
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_NR_PROC_I ON TASY.PLS_EMAIL
(NR_SEQ_PROCESSAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PARAMETROSI ON TASY.PLS_EMAIL
(NR_SEQ_PARAMETROS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PESFISI_FK_I ON TASY.PLS_EMAIL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PESJURI_FK_I ON TASY.PLS_EMAIL
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSEMAIL_PK ON TASY.PLS_EMAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PLSMENS_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PLSPOPE_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_PORTABILIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PLSPRON_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_PROPOSTA_ONLINE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PLSPRRC_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_PROG_REAJ_COL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PLSSCOM_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_SOLICITACAO_COMERCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_PLSSRES_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_SOLIC_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEMAIL_TAXDMEDIRP_FK_I ON TASY.PLS_EMAIL
(NR_SEQ_DMED_IRPF_PAYER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_email_before_insert
before insert ON TASY.PLS_EMAIL for each row
declare

begin
	:new.ie_confirmacao_leitura := nvl(obter_valor_param_usuario(0, 44, obter_perfil_ativo, :new.nm_usuario_nrec, obter_estabelecimento_ativo), 'N');
end;
/


ALTER TABLE TASY.PLS_EMAIL ADD (
  CONSTRAINT PLSEMAIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EMAIL ADD (
  CONSTRAINT PLSEMAIL_PLSPRRC_FK 
 FOREIGN KEY (NR_SEQ_PROG_REAJ_COL) 
 REFERENCES TASY.PLS_PROG_REAJ_COLETIVO (NR_SEQUENCIA),
  CONSTRAINT PLSEMAIL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSEMAIL_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSEMAIL_PLSPOPE_FK 
 FOREIGN KEY (NR_SEQ_PORTABILIDADE) 
 REFERENCES TASY.PLS_PORTAB_PESSOA (NR_SEQUENCIA),
  CONSTRAINT PLSEMAIL_PLSPRON_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA_ONLINE) 
 REFERENCES TASY.PLS_PROPOSTA_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSEMAIL_TAXDMEDIRP_FK 
 FOREIGN KEY (NR_SEQ_DMED_IRPF_PAYER) 
 REFERENCES TASY.TAX_DMED_IRPF_PAYER (NR_SEQUENCIA),
  CONSTRAINT PLSEMAIL_PLSSCOM_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO_COMERCIAL) 
 REFERENCES TASY.PLS_SOLICITACAO_COMERCIAL (NR_SEQUENCIA),
  CONSTRAINT PLSEMAIL_PLSMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSEMAIL_PLSSRES_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_RESCISAO) 
 REFERENCES TASY.PLS_SOLICITACAO_RESCISAO (NR_SEQUENCIA),
  CONSTRAINT PLSEMAIL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_EMAIL TO NIVEL_1;

