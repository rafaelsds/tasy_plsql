ALTER TABLE TASY.ESCALA_EAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_EAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_EAT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_ENGOLIR_PERDA_PESO  NUMBER(1),
  IE_ENGOLIR_FORA_CASA   NUMBER(1),
  IE_FORCA_BEBER_LIQ     NUMBER(1),
  IE_FORCA_COMER         NUMBER(1),
  IE_FORCA_REMEDIO       NUMBER(1),
  IE_DOR_ENGOLIR         NUMBER(1),
  QT_PONTUACAO           NUMBER(3),
  IE_PRAZER_COMER        NUMBER(1),
  IE_COMIDA_PRESA        NUMBER(1),
  IE_TUSSO               NUMBER(1),
  IE_ESTRESSADO          NUMBER(1),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAEAT_ATEPACI_FK_I ON TASY.ESCALA_EAT
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAEAT_PESFISI_FK_I ON TASY.ESCALA_EAT
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAEAT_PK ON TASY.ESCALA_EAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_eat_atual
before insert or update on  escala_eat
for each row
declare

qt_reg_w	Number(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

:new.qt_pontuacao := 	:new.ie_engolir_perda_peso 	+ :new.ie_engolir_fora_casa 	+ :new.ie_forca_beber_liq +
			:new.ie_forca_comer 		+ :new.ie_forca_remedio 	+ :new.ie_dor_engolir +
			:new.ie_prazer_comer 		+ :new.ie_comida_presa 		+ :new.ie_tusso +
			:new.ie_estressado;

<<Final>>
qt_reg_w	:= 0;

end escala_eat_atual;
/


ALTER TABLE TASY.ESCALA_EAT ADD (
  CONSTRAINT ESCAEAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_EAT ADD (
  CONSTRAINT ESCAEAT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAEAT_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_EAT TO NIVEL_1;

