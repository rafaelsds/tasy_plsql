ALTER TABLE TASY.SECTDAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SECTDAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.SECTDAT
(
  NR_SEQUENCIA         NUMBER(10),
  SECTION              NUMBER(15),
  TITLE                VARCHAR2(128 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE,
  VERSION              NUMBER(10)               DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MIMSSECT_PK ON TASY.SECTDAT
(SECTION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SECTDAT ADD (
  CONSTRAINT MIMSSECT_PK
 PRIMARY KEY
 (SECTION));

GRANT SELECT ON TASY.SECTDAT TO NIVEL_1;

