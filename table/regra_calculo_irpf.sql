ALTER TABLE TASY.REGRA_CALCULO_IRPF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CALCULO_IRPF CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CALCULO_IRPF
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  VL_BASE_CALCULO         NUMBER(15,2)          NOT NULL,
  TX_ALIQUOTA             NUMBER(9,4)           NOT NULL,
  VL_REDUCAO              NUMBER(15,2)          NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCAIR_I1 ON TASY.REGRA_CALCULO_IRPF
(VL_BASE_CALCULO, DT_INICIO_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGCAIR_PK ON TASY.REGRA_CALCULO_IRPF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.regra_calculo_irpf_atual
before insert or update ON TASY.REGRA_CALCULO_IRPF for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');


end regra_calculo_irpf_atual;
/


CREATE OR REPLACE TRIGGER TASY.REGRA_CALCULO_IRPF_tp  after update ON TASY.REGRA_CALCULO_IRPF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.VL_BASE_CALCULO,1,4000),substr(:new.VL_BASE_CALCULO,1,4000),:new.nm_usuario,nr_seq_w,'VL_BASE_CALCULO',ie_log_w,ds_w,'REGRA_CALCULO_IRPF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_ALIQUOTA,1,4000),substr(:new.TX_ALIQUOTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_ALIQUOTA',ie_log_w,ds_w,'REGRA_CALCULO_IRPF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'REGRA_CALCULO_IRPF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'REGRA_CALCULO_IRPF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'REGRA_CALCULO_IRPF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_REDUCAO,1,4000),substr(:new.VL_REDUCAO,1,4000),:new.nm_usuario,nr_seq_w,'VL_REDUCAO',ie_log_w,ds_w,'REGRA_CALCULO_IRPF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_CALCULO_IRPF ADD (
  CONSTRAINT REGCAIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REGRA_CALCULO_IRPF TO NIVEL_1;

