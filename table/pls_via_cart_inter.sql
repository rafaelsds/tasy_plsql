ALTER TABLE TASY.PLS_VIA_CART_INTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_VIA_CART_INTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_VIA_CART_INTER
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_LOTE            NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_CARTAO_INTERCAMBIO  VARCHAR2(30 BYTE),
  NR_SEQ_SEGURADO        NUMBER(10),
  NR_VIA_SOLICITACAO     NUMBER(5),
  DT_VALIDADE_CARTEIRA   DATE,
  DT_EMISSAO             DATE,
  NR_VIA_ANTERIOR        NUMBER(5),
  DT_VALIDADE_ANTERIOR   DATE,
  CD_USUARIO_PLANO       VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSVCIN_PK ON TASY.PLS_VIA_CART_INTER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVCIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSVCIN_PLSLVCI_FK_I ON TASY.PLS_VIA_CART_INTER
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVCIN_PLSLVCI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSVCIN_PLSSEGU_FK_I ON TASY.PLS_VIA_CART_INTER
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSVCIN_PLSSEGU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_VIA_CART_INTER ADD (
  CONSTRAINT PLSVCIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_VIA_CART_INTER ADD (
  CONSTRAINT PLSVCIN_PLSLVCI_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_VIA_CART_INTER (NR_SEQUENCIA),
  CONSTRAINT PLSVCIN_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_VIA_CART_INTER TO NIVEL_1;

