ALTER TABLE TASY.RELAT_INFORMACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELAT_INFORMACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELAT_INFORMACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_DESCRICAO           VARCHAR2(255 BYTE),
  DS_OBJETIVO            VARCHAR2(255 BYTE),
  DS_INFORMACAO          VARCHAR2(255 BYTE),
  DS_FORMA_VALIDACAO     VARCHAR2(255 BYTE),
  NR_SEQ_RELAT_DOC       NUMBER(10),
  NR_SEQ_RELAT_DIVISAO   NUMBER(10),
  NR_SEQ_RELAT_MODULO    NUMBER(10),
  CD_PF_DESENVOLVEDOR    VARCHAR2(10 BYTE),
  CD_SOLICITANTE         VARCHAR2(10 BYTE),
  IE_TIPO_CLASSIFICACAO  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELINFO_PESFISI_FK_I ON TASY.RELAT_INFORMACAO
(CD_PF_DESENVOLVEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELINFO_PESFISI_FK2_I ON TASY.RELAT_INFORMACAO
(CD_SOLICITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELINFO_PK ON TASY.RELAT_INFORMACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELINFO_RELAMOD_FK_I ON TASY.RELAT_INFORMACAO
(NR_SEQ_RELAT_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELINFO_RELAMOD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELINFO_RELDIVI_FK_I ON TASY.RELAT_INFORMACAO
(NR_SEQ_RELAT_DIVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELINFO_RELDIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELINFO_RELDOCM_FK_I ON TASY.RELAT_INFORMACAO
(NR_SEQ_RELAT_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELINFO_RELDOCM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RELAT_INFORMACAO ADD (
  CONSTRAINT RELINFO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELAT_INFORMACAO ADD (
  CONSTRAINT RELINFO_PESFISI_FK 
 FOREIGN KEY (CD_PF_DESENVOLVEDOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RELINFO_RELDOCM_FK 
 FOREIGN KEY (NR_SEQ_RELAT_DOC) 
 REFERENCES TASY.RELAT_DOCUMENTACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT RELINFO_PESFISI_FK2 
 FOREIGN KEY (CD_SOLICITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RELINFO_RELAMOD_FK 
 FOREIGN KEY (NR_SEQ_RELAT_MODULO) 
 REFERENCES TASY.RELAT_MODULO (NR_SEQUENCIA),
  CONSTRAINT RELINFO_RELDIVI_FK 
 FOREIGN KEY (NR_SEQ_RELAT_DIVISAO) 
 REFERENCES TASY.RELAT_DIVISAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RELAT_INFORMACAO TO NIVEL_1;

