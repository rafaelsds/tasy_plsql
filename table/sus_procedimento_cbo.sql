ALTER TABLE TASY.SUS_PROCEDIMENTO_CBO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_PROCEDIMENTO_CBO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_PROCEDIMENTO_CBO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CONSULTA_PUBLICA  NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  IE_AIH               VARCHAR2(1 BYTE),
  IE_BPA               VARCHAR2(1 BYTE),
  CD_CBO               VARCHAR2(6 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSPCBO_PK ON TASY.SUS_PROCEDIMENTO_CBO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSPCBO_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUSPCBO_PROCEDI_FK_I ON TASY.SUS_PROCEDIMENTO_CBO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPCBO_SUSCBOU_FK_I ON TASY.SUS_PROCEDIMENTO_CBO
(CD_CBO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUS_PROCEDIMENTO_CBO ADD (
  CONSTRAINT SUSPCBO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_PROCEDIMENTO_CBO ADD (
  CONSTRAINT SUSPCBO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSPCBO_SUSCBOU_FK 
 FOREIGN KEY (CD_CBO) 
 REFERENCES TASY.SUS_CBO (CD_CBO));

GRANT SELECT ON TASY.SUS_PROCEDIMENTO_CBO TO NIVEL_1;

