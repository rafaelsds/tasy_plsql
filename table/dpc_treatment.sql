ALTER TABLE TASY.DPC_TREATMENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DPC_TREATMENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.DPC_TREATMENT
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_EDITION          NUMBER(10)            NOT NULL,
  SI_1_OR_2               VARCHAR2(1 BYTE)      NOT NULL,
  CD_MDC                  VARCHAR2(2 BYTE)      NOT NULL,
  CD_CLASSIFICATION       VARCHAR2(4 BYTE)      NOT NULL,
  CD_CORRESP_CODE         VARCHAR2(1 BYTE)      NOT NULL,
  CD_TREATMENT_FLAG       VARCHAR2(2 BYTE)      NOT NULL,
  CD_COMBINATION_SURGERY  VARCHAR2(20 BYTE),
  DS_TREATMENT_1          VARCHAR2(255 BYTE)    NOT NULL,
  DS_TREATMENT_2          VARCHAR2(255 BYTE),
  CD_TREATMENT_1          VARCHAR2(20 BYTE)     NOT NULL,
  CD_TREATMENT_2          VARCHAR2(20 BYTE),
  SI_CHANGE_CATEGORY      VARCHAR2(1 BYTE)      NOT NULL,
  DT_START                DATE                  NOT NULL,
  DT_END                  DATE,
  DT_UPDATE               DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DPCTRTM_DPCEDTN_FK_I ON TASY.DPC_TREATMENT
(NR_SEQ_EDITION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DPCTRTM_PK ON TASY.DPC_TREATMENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DPC_TREATMENT ADD (
  CONSTRAINT DPCTRTM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DPC_TREATMENT ADD (
  CONSTRAINT DPCTRTM_DPCEDTN_FK 
 FOREIGN KEY (NR_SEQ_EDITION) 
 REFERENCES TASY.DPC_EDITION (NR_SEQUENCIA));

GRANT SELECT ON TASY.DPC_TREATMENT TO NIVEL_1;

