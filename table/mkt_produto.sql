ALTER TABLE TASY.MKT_PRODUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MKT_PRODUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MKT_PRODUTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_PRODUTO           VARCHAR2(100 BYTE)       NOT NULL,
  DS_VERSAO_PRODUTO    VARCHAR2(255 BYTE),
  DS_OBSERVACAO        LONG,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  CD_EXP_PRODUTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MKTPROD_DICEXPR_FK_I ON TASY.MKT_PRODUTO
(CD_EXP_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MKTPROD_PK ON TASY.MKT_PRODUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MKT_PRODUTO ADD (
  CONSTRAINT MKTPROD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MKT_PRODUTO ADD (
  CONSTRAINT MKTPROD_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_PRODUTO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.MKT_PRODUTO TO NIVEL_1;

