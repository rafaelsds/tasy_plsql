DROP TABLE TASY.EIS_PROCED_EXECUTADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_PROCED_EXECUTADO
(
  DT_REFERENCIA          DATE                   NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_SETOR_ATENDIMENTO   NUMBER(5)              NOT NULL,
  IE_ORIGEM_PROCED       NUMBER(10)             NOT NULL,
  CD_PROCEDIMENTO        NUMBER(15)             NOT NULL,
  IE_PERIODO             VARCHAR2(1 BYTE)       NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  DT_HORA_EXAME          NUMBER(2),
  CD_TECNICO             VARCHAR2(10 BYTE),
  CD_MEDICO_SOLICITANTE  VARCHAR2(10 BYTE),
  IE_TIPO_ATENDIMENTO    NUMBER(3),
  QT_PROCEDIMENTO        NUMBER(15,2),
  CD_MEDICO_PRESCR       VARCHAR2(10 BYTE),
  CD_CONVENIO            NUMBER(10),
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE),
  CD_ESPECIALIDADE       NUMBER(5),
  NR_SEQ_GRUPO_REC       NUMBER(10),
  IE_TIPO_CONVENIO       NUMBER(2),
  NR_SEQ_PROC_INTERNO    NUMBER(10),
  CD_MEDICO_EXECUTOR     VARCHAR2(10 BYTE),
  IE_LADO                VARCHAR2(1 BYTE),
  IE_CLINICA             NUMBER(5),
  CD_PROCEDENCIA         NUMBER(5),
  IE_COMPLEXIDADE_SUS    VARCHAR2(2 BYTE),
  NR_SEQ_TURNO           NUMBER(10),
  DS_TURNO_ATEND         VARCHAR2(30 BYTE),
  VL_PROCEDIMENTO        NUMBER(15,2),
  NR_INTERNO_CONTA       NUMBER(10),
  VL_ADICIONAL           NUMBER(15,2),
  VL_AMENOR              NUMBER(15,2),
  VL_GLOSA               NUMBER(15,2),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  CD_CATEGORIA           VARCHAR2(10 BYTE),
  NR_SEQ_AGRUP_CLASSIF   NUMBER(10),
  CD_MEDICO_LAUDO        VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA_EIS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          392M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISPREX_I1 ON TASY.EIS_PROCED_EXECUTADO
(IE_PERIODO, DT_REFERENCIA, CD_ESTABELECIMENTO, CD_SETOR_ATENDIMENTO, IE_ORIGEM_PROCED, 
CD_PROCEDIMENTO, DT_HORA_EXAME, CD_TECNICO, CD_MEDICO_SOLICITANTE, IE_TIPO_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          289M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


GRANT SELECT ON TASY.EIS_PROCED_EXECUTADO TO NIVEL_1;

