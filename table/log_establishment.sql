ALTER TABLE TASY.LOG_ESTABLISHMENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_ESTABLISHMENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_ESTABLISHMENT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_STORAGE_LOG       VARCHAR2(1 BYTE)         NOT NULL,
  QT_STORAGE_MONTH     NUMBER(3)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LOG_ESTAB_PK ON TASY.LOG_ESTABLISHMENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOG_ESTAB_UK ON TASY.LOG_ESTABLISHMENT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOG_ESTABLISHMENT_ATUAL
before insert or update ON TASY.LOG_ESTABLISHMENT for each row
declare

nr_seq_log_cleaning_w	log_cleaning.nr_sequencia%type;
qt_month_w		log_cleaning.qt_month%type;
ds_log_type_w		varchar2(255);

begin

	--Checks if the time to storage the logs from the establishment is smaller than the smaller time to storage the logs from the log type.
	--Privacy Project: 9208 - BR05

	select 	nvl(max(nr_sequencia),0)
	into 	nr_seq_log_cleaning_w
	from 	log_cleaning
	where	cd_estabelecimento = :new.cd_estabelecimento
	and	nr_seq_log_estab = :new.nr_sequencia
	and	:new.qt_storage_month <= qt_month
	and	coalesce(ie_situacao, 'I') = 'A';

	if (nr_seq_log_cleaning_w > 0) then

		select	max(substr(obter_valor_dominio(8382, cd_log_type), 1, 255)),
			max(qt_month)
		into 	ds_log_type_w,
			qt_month_w
		from	log_cleaning
		where	nr_sequencia = nr_seq_log_cleaning_w;


		if (qt_month_w > 1) then
            		wheb_mensagem_pck.exibir_mensagem_abort(806408, 'DS_LOG_TYPE=' || ds_log_type_w || ';QT_MONTH=' || qt_month_w);
        	else
            		wheb_mensagem_pck.exibir_mensagem_abort(806407, 'DS_LOG_TYPE=' || ds_log_type_w || ';QT_MONTH=' || qt_month_w);
        	end if;

       	end if;

       	if (updating) and (:old.cd_estabelecimento <> :new.cd_estabelecimento) then
       		wheb_mensagem_pck.exibir_mensagem_abort(806907);
       	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOG_ESTABLISHMENT_tp  after update ON TASY.LOG_ESTABLISHMENT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'LOG_ESTABLISHMENT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_STORAGE_MONTH,1,4000),substr(:new.QT_STORAGE_MONTH,1,4000),:new.nm_usuario,nr_seq_w,'QT_STORAGE_MONTH',ie_log_w,ds_w,'LOG_ESTABLISHMENT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STORAGE_LOG,1,4000),substr(:new.IE_STORAGE_LOG,1,4000),:new.nm_usuario,nr_seq_w,'IE_STORAGE_LOG',ie_log_w,ds_w,'LOG_ESTABLISHMENT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOG_ESTABLISHMENT ADD (
  CONSTRAINT LOG_ESTAB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT LOG_ESTAB_UK
 UNIQUE (CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOG_ESTABLISHMENT ADD (
  CONSTRAINT LOG_ESTAB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.LOG_ESTABLISHMENT TO NIVEL_1;

