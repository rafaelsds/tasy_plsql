ALTER TABLE TASY.EVENTOS_ADVERSOS_GRADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EVENTOS_ADVERSOS_GRADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EVENTOS_ADVERSOS_GRADE
(
  CD_GRADE              NUMBER(10)              NOT NULL,
  CD_EXP_GRADE          NUMBER(15),
  CD_TERMO              NUMBER(15),
  CD_EXP_PHILIPS        NUMBER(15),
  DS_DESCRICAO_CLIENTE  VARCHAR2(255 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EVADGRA_DICEXPR_FK_I ON TASY.EVENTOS_ADVERSOS_GRADE
(CD_EXP_GRADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVADGRA_EVADTER_FK_I ON TASY.EVENTOS_ADVERSOS_GRADE
(CD_TERMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EVADGRA_PK ON TASY.EVENTOS_ADVERSOS_GRADE
(CD_GRADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EVENTOS_ADVERSOS_GRADE ADD (
  CONSTRAINT EVADGRA_PK
 PRIMARY KEY
 (CD_GRADE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EVENTOS_ADVERSOS_GRADE ADD (
  CONSTRAINT EVADGRA_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_GRADE) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT EVADGRA_EVADTER_FK 
 FOREIGN KEY (CD_TERMO) 
 REFERENCES TASY.EVENTOS_ADVERSOS_TERMOS (CD_TERMO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.EVENTOS_ADVERSOS_GRADE TO NIVEL_1;

