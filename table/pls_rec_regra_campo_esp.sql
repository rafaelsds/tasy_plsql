ALTER TABLE TASY.PLS_REC_REGRA_CAMPO_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REC_REGRA_CAMPO_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REC_REGRA_CAMPO_ESP
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_CAMPO                VARCHAR2(255 BYTE)    NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_GUIA            VARCHAR2(2 BYTE),
  CD_AREA_PROCEDIMENTO    NUMBER(15),
  CD_ESPECIALIDADE        NUMBER(15),
  CD_GRUPO_PROC           NUMBER(15),
  CD_PRESTADOR_EXEC       VARCHAR2(30 BYTE),
  IE_TIPO_PROCESSO        VARCHAR2(1 BYTE)      NOT NULL,
  CD_PRESTADOR_PROT       VARCHAR2(30 BYTE),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  NR_SEQ_MATERIAL         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRECESP_AREPROC_FK_I ON TASY.PLS_REC_REGRA_CAMPO_ESP
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECESP_ESPPROC_FK_I ON TASY.PLS_REC_REGRA_CAMPO_ESP
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECESP_GRUPROC_FK_I ON TASY.PLS_REC_REGRA_CAMPO_ESP
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRECESP_PK ON TASY.PLS_REC_REGRA_CAMPO_ESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECESP_PLSMAT_FK_I ON TASY.PLS_REC_REGRA_CAMPO_ESP
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRECESP_PROCEDI_FK_I ON TASY.PLS_REC_REGRA_CAMPO_ESP
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_rec_regra_campo_esp_atual
before insert or update ON TASY.PLS_REC_REGRA_CAMPO_ESP for each row
declare

begin
/* esta trigger foi criada para alimentar os campos de data de vigencia de referencia, isto por quest�es de performance
 para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
 o campo inicial vigencia ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
 com a data zero do oracle 31/12/1899, j� o campo fim vigencia ref � alimentado com o campo fim ou se o mesmo for nulo
 � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
sem precisar se preocupar se o campo vai estar nulo
*/
:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia,'I');

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_fim_vigencia,'F');

end;
/


ALTER TABLE TASY.PLS_REC_REGRA_CAMPO_ESP ADD (
  CONSTRAINT PLSRECESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REC_REGRA_CAMPO_ESP ADD (
  CONSTRAINT PLSRECESP_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSRECESP_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSRECESP_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSRECESP_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSRECESP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_REC_REGRA_CAMPO_ESP TO NIVEL_1;

