ALTER TABLE TASY.LAUDO_PAC_IMAGEM_EDICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PAC_IMAGEM_EDICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PAC_IMAGEM_EDICAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(100 BYTE),
  DS_TITULO            VARCHAR2(100 BYTE),
  NR_SEQ_LAUDO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LPIMGED_LAUPACI_FK_I ON TASY.LAUDO_PAC_IMAGEM_EDICAO
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LPIMGED_LAUPACI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LPIMGED_PK ON TASY.LAUDO_PAC_IMAGEM_EDICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LPIMGED_PK
  MONITORING USAGE;


ALTER TABLE TASY.LAUDO_PAC_IMAGEM_EDICAO ADD (
  CONSTRAINT LPIMGED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PAC_IMAGEM_EDICAO ADD (
  CONSTRAINT LPIMGED_LAUPACI_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAUDO_PAC_IMAGEM_EDICAO TO NIVEL_1;

