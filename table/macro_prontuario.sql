ALTER TABLE TASY.MACRO_PRONTUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MACRO_PRONTUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MACRO_PRONTUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NM_MACRO             VARCHAR2(50 BYTE)        NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  DS_MACRO             VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORIGEM            VARCHAR2(1 BYTE),
  DS_SQL               VARCHAR2(1000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MACPRON_I1 ON TASY.MACRO_PRONTUARIO
(UPPER("NM_MACRO"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MACPRON_PK ON TASY.MACRO_PRONTUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.macro_prontuario_atual
after insert or update ON TASY.MACRO_PRONTUARIO for each row
declare

begin
	begin
		if (inserting) then
			begin
				insert into funcao_macro(
					nr_sequencia,
					ds_macro,
					cd_funcao,
					dt_atualizacao,
					nm_usuario,
					cd_exp_observacao,
					ie_situacao)
				values	(funcao_macro_seq.nextval,
					:new.nm_macro,
					6001,
					sysdate,
					'Tasy',
					557610,
					'A');
			end;
		elsif (updating) then
			begin
				update	funcao_macro
				set		ds_macro = :new.nm_macro,
						dt_atualizacao = sysdate
				where 	ds_macro = :old.nm_macro
				and 	nm_usuario = 'Tasy'
				and 	cd_funcao = 6001;
			end;
		end if;
	exception
		when others then
		null;
	end;
end;
/


ALTER TABLE TASY.MACRO_PRONTUARIO ADD (
  CONSTRAINT MACPRON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MACRO_PRONTUARIO TO NIVEL_1;

