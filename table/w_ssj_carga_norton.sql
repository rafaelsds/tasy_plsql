DROP TABLE TASY.W_SSJ_CARGA_NORTON CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_SSJ_CARGA_NORTON
(
  NR_SEQUENCIA            INTEGER               NOT NULL,
  DT_ATUALIZACAO          DATE                  DEFAULT sysdate               NOT NULL,
  NM_USUARIO              VARCHAR2(255 BYTE)    NOT NULL,
  NOME_PAGADOR            VARCHAR2(255 BYTE),
  RAZAO_SOCIAL            VARCHAR2(255 BYTE),
  CPF_CNPJ                NUMBER(14),
  NR_CONTRATO             INTEGER,
  NR_TITULO               INTEGER,
  DT_MES_COMPETENCIA      DATE,
  DT_VENCIMENTO_TITULO    DATE,
  VL_TITULO               NUMBER(15,2),
  DS_ENDERECO             VARCHAR2(500 BYTE),
  NUM_CASA                INTEGER,
  DS_COMPLEMENTO          VARCHAR2(255 BYTE),
  DS_BAIRRO               VARCHAR2(255 BYTE),
  DS_CIDADE               VARCHAR2(255 BYTE),
  NUM_CEP                 NUMBER(8),
  UF                      CHAR(2 BYTE),
  DDD_TELEFONE_1          NUMBER(2),
  NR_TELEFONE_1           NUMBER(8),
  DDD_TELEFONE_2          NUMBER(2)             DEFAULT null,
  NR_TELEFONE_2           NUMBER(8)             DEFAULT null,
  DDD_CELULAR_1           NUMBER(2),
  NR_CELULAR_1            NUMBER(9),
  DDD_CELULAR_2           NUMBER(2)             DEFAULT null,
  NR_CELULAR_2            NUMBER(9)             DEFAULT null,
  DS_TIPO_PLANO           VARCHAR2(255 BYTE),
  NR_PROTOCOLO_ANS        INTEGER,
  IE_ALTA_SINISTRALIDADE  CHAR(1 BYTE),
  IE_ENVIADO_CARGA        CHAR(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_SSJ_CARGA_NORTON TO NIVEL_1;

