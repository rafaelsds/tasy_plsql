ALTER TABLE TASY.PLS_ITEM_RECALCULO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ITEM_RECALCULO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ITEM_RECALCULO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONTA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROCEDIMENTO  NUMBER(10),
  NR_SEQ_MATERIAL      NUMBER(10),
  VL_ITEM_ATUAL        NUMBER(15,2),
  VL_ITEM              NUMBER(15,2),
  CD_ITEM              NUMBER(15),
  DS_ITEM              VARCHAR2(255 BYTE),
  IE_TIPO_ITEM         VARCHAR2(1 BYTE),
  VL_TOTAL_CONTA       NUMBER(15,2),
  NR_SEQ_REGRA_PRECO   NUMBER(10),
  NR_REGRA_RECALCULO   NUMBER(10),
  NR_SEQ_CONTA_RESUMO  NUMBER(10),
  NR_SEQ_CTA_CONTA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSITRE_I1 ON TASY.PLS_ITEM_RECALCULO
(NR_SEQ_CONTA_RESUMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSITRE_PK ON TASY.PLS_ITEM_RECALCULO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSITRE_PLSCORE_FK_I ON TASY.PLS_ITEM_RECALCULO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ITEM_RECALCULO ADD (
  CONSTRAINT PLSITRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ITEM_RECALCULO ADD (
  CONSTRAINT PLSITRE_PLSCORE_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA_RECALCULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ITEM_RECALCULO TO NIVEL_1;

