ALTER TABLE TASY.MED_GRUPO_OBJETIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_GRUPO_OBJETIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_GRUPO_OBJETIVO
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NR_SEQ_SISTEMA     NUMBER(10)                 NOT NULL,
  CD_GRUPO_OBJETIVO  VARCHAR2(10 BYTE)          NOT NULL,
  DS_GRUPO_OBJETIVO  VARCHAR2(80 BYTE)          NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDGROB_MEDSIST_FK_I ON TASY.MED_GRUPO_OBJETIVO
(NR_SEQ_SISTEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDGROB_MEDSIST_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDGROB_PK ON TASY.MED_GRUPO_OBJETIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDGROB_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_GRUPO_OBJETIVO ADD (
  CONSTRAINT MEDGROB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_GRUPO_OBJETIVO ADD (
  CONSTRAINT MEDGROB_MEDSIST_FK 
 FOREIGN KEY (NR_SEQ_SISTEMA) 
 REFERENCES TASY.MED_SISTEMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_GRUPO_OBJETIVO TO NIVEL_1;

