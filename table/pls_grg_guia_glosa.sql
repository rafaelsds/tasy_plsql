ALTER TABLE TASY.PLS_GRG_GUIA_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GRG_GUIA_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GRG_GUIA_GLOSA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_GRG_GUIA        NUMBER(10)             NOT NULL,
  NR_SEQ_MOTIVO_GLOSA    NUMBER(10)             NOT NULL,
  DS_JUSTIFICATIVA       VARCHAR2(150 BYTE),
  NR_SEQ_DMA_GUIA_GLOSA  NUMBER(10)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSGGGG_PK ON TASY.PLS_GRG_GUIA_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGGGG_PLSDAGG_FK_I ON TASY.PLS_GRG_GUIA_GLOSA
(NR_SEQ_DMA_GUIA_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGGGG_PLSGRGG_FK_I ON TASY.PLS_GRG_GUIA_GLOSA
(NR_SEQ_GRG_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGGGG_TISSMGL_FK_I ON TASY.PLS_GRG_GUIA_GLOSA
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_GRG_GUIA_GLOSA ADD (
  CONSTRAINT PLSGGGG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GRG_GUIA_GLOSA ADD (
  CONSTRAINT PLSGGGG_PLSDAGG_FK 
 FOREIGN KEY (NR_SEQ_DMA_GUIA_GLOSA) 
 REFERENCES TASY.PLS_DMA_GUIA_GLOSA_IMP (NR_SEQUENCIA),
  CONSTRAINT PLSGGGG_PLSGRGG_FK 
 FOREIGN KEY (NR_SEQ_GRG_GUIA) 
 REFERENCES TASY.PLS_GRG_GUIA (NR_SEQUENCIA),
  CONSTRAINT PLSGGGG_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_GRG_GUIA_GLOSA TO NIVEL_1;

