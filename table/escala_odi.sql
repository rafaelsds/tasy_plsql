ALTER TABLE TASY.ESCALA_ODI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_ODI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_ODI
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  QT_DOR                 NUMBER(3),
  QT_CUIDADO_PESSOAL     NUMBER(3),
  QT_LEVANTAMENTO        NUMBER(3),
  QT_MARCHA              NUMBER(3),
  QT_SENTAR              NUMBER(3),
  QT_FICAR_EM_PE         NUMBER(3),
  QT_DORMIR              NUMBER(3),
  QT_SEXUALIDADE         NUMBER(3),
  QT_VIDA_SOCIAL         NUMBER(3),
  QT_VIAGEM              NUMBER(3),
  PR_RESULTADO           NUMBER(10,2),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAODI_ATEPACI_FK_I ON TASY.ESCALA_ODI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAODI_PESFISI_FK_I ON TASY.ESCALA_ODI
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAODI_PK ON TASY.ESCALA_ODI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAODI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_odi_atual
before insert or update ON TASY.ESCALA_ODI for each row
declare
qt_preenchido_w	number(10)	:= 0;
qt_pontuacao_w	number(10);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

if	(:new.QT_DOR is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;

if	(:new.QT_CUIDADO_PESSOAL is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;
if	(:new.QT_LEVANTAMENTO is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;
if	(:new.QT_MARCHA is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;
if	(:new.QT_SENTAR is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;

if	(:new.QT_FICAR_EM_PE is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;
if	(:new.QT_DORMIR is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;
if	(:new.QT_VIDA_SOCIAL is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;

if	(:new.QT_SEXUALIDADE is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;

if	(:new.QT_VIAGEM is not null) then
	qt_preenchido_w:= qt_preenchido_w+1;
end if;

qt_pontuacao_w	:= 	nvl(:new.QT_DOR,0)	+
			nvl(:new.QT_CUIDADO_PESSOAL,0)	+
			nvl(:new.QT_LEVANTAMENTO,0)	+
			nvl(:new.QT_MARCHA,0)	+
			nvl(:new.QT_SENTAR,0)	+
			nvl(:new.QT_FICAR_EM_PE,0)	+
			nvl(:new.QT_DORMIR,0)	+
			nvl(:new.QT_VIDA_SOCIAL,0)	+
			nvl(:new.QT_SEXUALIDADE,0)	+
			nvl(:new.QT_VIAGEM,0);

begin

:new.pr_resultado:=	round((qt_pontuacao_w / (5 * qt_preenchido_w)) * 100);
exception
	when others then
	null;
	end;


end;
/


ALTER TABLE TASY.ESCALA_ODI ADD (
  CONSTRAINT ESCAODI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_ODI ADD (
  CONSTRAINT ESCAODI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAODI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_ODI TO NIVEL_1;

