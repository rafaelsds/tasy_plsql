ALTER TABLE TASY.PEPO_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_CIRURGIA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  NR_ATENDIMENTO              NUMBER(10),
  DT_CIRURGIA                 DATE              NOT NULL,
  CD_MEDICO_ANESTESISTA       VARCHAR2(10 BYTE),
  IE_CARATER_CIRURGIA         VARCHAR2(15 BYTE),
  IE_ASA_ESTADO_PACIENTE      VARCHAR2(15 BYTE),
  IE_TIPO_CIRURGIA            VARCHAR2(15 BYTE),
  CD_TIPO_ANESTESIA           VARCHAR2(10 BYTE),
  DT_LIBERACAO                DATE,
  NM_USUARIO_LIB              VARCHAR2(15 BYTE),
  DT_LIBERACAO_ANESTESISTA    DATE,
  NM_USUARIO_LIB_ANEST        VARCHAR2(15 BYTE),
  IE_TIPO_PROCEDIMENTO        VARCHAR2(1 BYTE),
  DT_INICIO_PROCED            DATE,
  DT_FIM_CIRURGIA             DATE,
  NR_CIRURGIA_PRINCIPAL       NUMBER(10),
  NR_SEQ_ATEPACU              NUMBER(10),
  QT_MINUTOS_PREV_DURACAO     NUMBER(10),
  NR_PRESCRICAO               NUMBER(14),
  IE_MOTIVO_CANCELAMENTO      VARCHAR2(3 BYTE),
  DT_CANCELAMENTO             DATE,
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10),
  DS_MOTIVO_CANCELAMENTO      VARCHAR2(255 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_ESTABELECIMENTO          NUMBER(4),
  CD_SETOR_ATEND_PRESCR       NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPOCIR_AGMOCAN_FK_I ON TASY.PEPO_CIRURGIA
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCIR_AGMOCAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCIR_ATEPACI_FK_I ON TASY.PEPO_CIRURGIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOCIR_ATEPACU_FK_I ON TASY.PEPO_CIRURGIA
(NR_SEQ_ATEPACU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCIR_ATEPACU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCIR_CIRURGI_FK_I ON TASY.PEPO_CIRURGIA
(NR_CIRURGIA_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCIR_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCIR_ESTABEL_FK_I ON TASY.PEPO_CIRURGIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOCIR_MEDICO_FK_I ON TASY.PEPO_CIRURGIA
(CD_MEDICO_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOCIR_PESFISI_FK_I ON TASY.PEPO_CIRURGIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPOCIR_PK ON TASY.PEPO_CIRURGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPOCIR_PRESMED_FK_I ON TASY.PEPO_CIRURGIA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCIR_PRESMED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPOCIR_SETATEN_FK_I ON TASY.PEPO_CIRURGIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPOCIR_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pepo_cirurgia_atual
before insert or update ON TASY.PEPO_CIRURGIA FOR EACH ROW
declare

ie_atual_tipo_anest_w 	varchar2(1);
ie_medico_partic_w	   varchar2(1);
ie_atualiza_medico_w	   varchar2(1);
cd_funcao_ativa_w		   number(5)                                 := obter_funcao_ativa;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type   := wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w             perfil.cd_perfil%type                     := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w            usuario.nm_usuario%type                   := wheb_usuario_pck.get_nm_usuario;
BEGIN

obter_param_usuario(872, 424, cd_perfil_w,nm_usuario_w,cd_estabelecimento_w, ie_atual_tipo_anest_w);
obter_param_usuario(10026, 41, cd_perfil_w,nm_usuario_w,cd_estabelecimento_w, ie_medico_partic_w);
obter_param_usuario(10026, 50, cd_perfil_w,nm_usuario_w,cd_estabelecimento_w, ie_atualiza_medico_w);

if (:new.cd_estabelecimento is null) then
   :new.cd_estabelecimento  := cd_estabelecimento_w;
end if;

begin
if 	(ie_atual_tipo_anest_w = 'S') then
	if 	nvl(:old.cd_tipo_anestesia,'XPTO') <> nvl(:new.cd_tipo_anestesia,'XPTO') then
		update 		cirurgia
		set		dt_atualizacao 		=	sysdate,
				nm_usuario		= 	nm_usuario_w,
				cd_tipo_anestesia 	= 	:new.cd_tipo_anestesia
		where 		nr_seq_pepo 		= 	:new.nr_sequencia;
	end if;
end if;

if 	((ie_medico_partic_w = 'S') and
	(cd_funcao_ativa_w <> 871) and
	(cd_funcao_ativa_w <> 900) and
	(cd_funcao_ativa_w <> 872) and
	(((:old.cd_medico_anestesista is null) and (:new.cd_medico_anestesista is not null)) or
	 (:old.cd_medico_anestesista <> :new.cd_medico_anestesista))) then
	gerar_partic_proc_fanep(:new.nr_sequencia,:new.cd_medico_anestesista,'N',nm_usuario_w);
end if;

if 	nvl(:old.ie_asa_estado_paciente,'XPTO') <> NVL(:new.ie_asa_estado_paciente,'XPTO') then
	update 		cirurgia
	set		dt_atualizacao 		=	sysdate,
			nm_usuario		= 	nm_usuario_w,
			ie_asa_estado_paciente 	= 	:new.ie_asa_estado_paciente
	where 		nr_seq_pepo 		= 	:new.nr_sequencia;
end if;

if 	NVL(:old.cd_medico_anestesista,'XPTO') <> NVL(:new.cd_medico_anestesista,'XPTO') then
	update 		cirurgia
	set		dt_atualizacao 		=	sysdate,
			nm_usuario		= 	nm_usuario_w,
			cd_medico_anestesista	=	:new.cd_medico_anestesista
	where 		nr_seq_pepo 		= 	:new.nr_sequencia;
end if;

if 	NVL(:old.ie_tipo_cirurgia,'XPTO') <> NVL(:new.ie_tipo_cirurgia,'XPTO') then
	update 		cirurgia
	set		dt_atualizacao 		=	sysdate,
			nm_usuario		= 	nm_usuario_w,
			ie_tipo_cirurgia	=	:new.ie_tipo_cirurgia
	where 		nr_seq_pepo 		= 	:new.nr_sequencia;
end if;


if	(ie_atualiza_medico_w = 'S') and
	(cd_funcao_ativa_w = 10026) and
	(:new.nr_prescricao > 0) and
	(NVL(:old.cd_medico_anestesista,'XPTO') <> NVL(:new.cd_medico_anestesista,'XPTO')) then

	update	prescr_medica
	set	cd_medico 	= :new.cd_medico_anestesista
	where	nr_prescricao	= :new.nr_prescricao;
end if;

if	(cd_funcao_ativa_w = 10026) and
	(:new.nr_atendimento > 0) and
	(NVL(:old.nr_atendimento,0) <> NVL(:new.nr_atendimento,0)) then

	update	prescr_medica
	set	nr_atendimento 	= :new.nr_atendimento
	where	nr_prescricao	= :new.nr_prescricao;
end if;





exception
		when others then
		null;
end;

end;
/


ALTER TABLE TASY.PEPO_CIRURGIA ADD (
  CONSTRAINT PEPOCIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEPO_CIRURGIA ADD (
  CONSTRAINT PEPOCIR_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_ANESTESISTA) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PEPOCIR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPOCIR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PEPOCIR_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA_PRINCIPAL) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT PEPOCIR_ATEPACU_FK 
 FOREIGN KEY (NR_SEQ_ATEPACU) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO),
  CONSTRAINT PEPOCIR_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PEPOCIR_AGMOCAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.AGENDA_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT PEPOCIR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PEPOCIR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PEPO_CIRURGIA TO NIVEL_1;

