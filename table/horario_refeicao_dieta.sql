ALTER TABLE TASY.HORARIO_REFEICAO_DIETA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HORARIO_REFEICAO_DIETA CASCADE CONSTRAINTS;

CREATE TABLE TASY.HORARIO_REFEICAO_DIETA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_REFEICAO          VARCHAR2(15 BYTE)        NOT NULL,
  DS_HORARIOS          VARCHAR2(5 BYTE)         NOT NULL,
  CD_DIETA             NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  DS_HORARIOS_LIB      VARCHAR2(5 BYTE),
  DS_HORARIO_AUTO      VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HORREFD_DIETA_FK_I ON TASY.HORARIO_REFEICAO_DIETA
(CD_DIETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HORREFD_PK ON TASY.HORARIO_REFEICAO_DIETA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HORREFD_UK ON TASY.HORARIO_REFEICAO_DIETA
(DS_HORARIOS, CD_REFEICAO, CD_DIETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HORARIO_REFEICAO_DIETA ADD (
  CONSTRAINT HORREFD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT HORREFD_UK
 UNIQUE (DS_HORARIOS, CD_REFEICAO, CD_DIETA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HORARIO_REFEICAO_DIETA ADD (
  CONSTRAINT HORREFD_DIETA_FK 
 FOREIGN KEY (CD_DIETA) 
 REFERENCES TASY.DIETA (CD_DIETA));

GRANT SELECT ON TASY.HORARIO_REFEICAO_DIETA TO NIVEL_1;

