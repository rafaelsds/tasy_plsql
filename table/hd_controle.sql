ALTER TABLE TASY.HD_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_CONTROLE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_DIALISE          NUMBER(10)            NOT NULL,
  DT_CONTROLE             DATE                  NOT NULL,
  QT_FLUXO_SANGUE         NUMBER(15,4),
  QT_FLUXO_DIALISADO      NUMBER(15,4),
  QT_PA_TRANS_MENBRANA    NUMBER(15,4),
  QT_PA_VENOSA            NUMBER(15,4),
  QT_PA_ARTERIAL          NUMBER(15,4),
  QT_HEPARINA             NUMBER(15,4),
  QT_TEMPERATURA_AXILIAR  NUMBER(15,4),
  QT_PULSO                NUMBER(15,4),
  QT_PA_SISTOLICA         NUMBER(15,4),
  QT_PA_DIASTOLICA        NUMBER(15,4),
  QT_SANGUE_DIAL          NUMBER(15,4),
  NR_SEQ_UNID_DIALISE     NUMBER(10)            NOT NULL,
  QT_CLEARENCE            NUMBER(15,4),
  QT_KTV                  NUMBER(15,4),
  QT_SORO_REPOSICAO       NUMBER(10),
  QT_SORO_DEVOLUCAO       NUMBER(10),
  NR_SEQ_TIPO_SORO        NUMBER(10),
  QT_CONDUTIVIDADE        NUMBER(15),
  QT_HGT                  NUMBER(15,4),
  QT_INSULINA             NUMBER(15,4),
  QT_GLICOSE_ADM          NUMBER(5,1),
  QT_DEXTRO               NUMBER(15,1),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  QT_INSULINA_INT         NUMBER(15,4),
  QT_VOLUME_SUBST         NUMBER(15,4),
  QT_VOLUME_CONVECTIVO    NUMBER(15,4),
  QT_TAXA_SUBST           NUMBER(15,4),
  QT_BOLUS_ACUMULADO      NUMBER(15,4),
  IE_ULTRAFILTRACAO       VARCHAR2(15 BYTE),
  NR_SEQ_ULTRA            NUMBER(10),
  QT_ULTRAFILTRACAO       NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDCONTO_HDDIALE_FK_I ON TASY.HD_CONTROLE
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDCONTO_HDUNIDD_FK_I ON TASY.HD_CONTROLE
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDCONTO_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDCONTO_PK ON TASY.HD_CONTROLE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_controle_befup_sign
before update ON TASY.HD_CONTROLE for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_controle = :new.nr_sequencia
and 	ie_opcao = 'NC'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and 	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao		<> :new.dt_atualizacao
		or :old.nm_usuario		<> :new.nm_usuario
		or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
		or :old.nr_seq_dialise		<> :new.nr_seq_dialise
		or :old.dt_controle		<> :new.dt_controle
		or :old.qt_fluxo_sangue		<> :new.qt_fluxo_sangue
		or :old.qt_fluxo_dialisado	<> :new.qt_fluxo_dialisado
		or :old.qt_pa_trans_menbrana	<> :new.qt_pa_trans_menbrana
		or :old.qt_pa_venosa		<> :new.qt_pa_venosa
		or :old.qt_pa_arterial		<> :new.qt_pa_arterial
		or :old.qt_heparina		<> :new.qt_heparina
		or :old.qt_temperatura_axiliar	<> :new.qt_temperatura_axiliar
		or :old.qt_pulso		<> :new.qt_pulso
		or :old.qt_pa_sistolica		<> :new.qt_pa_sistolica
		or :old.qt_pa_diastolica	<> :new.qt_pa_diastolica
		or :old.qt_sangue_dial		<> :new.qt_sangue_dial
		or :old.nr_seq_unid_dialise	<> :new.nr_seq_unid_dialise
		or :old.qt_clearence		<> :new.qt_clearence
		or :old.qt_ktv			<> :new.qt_ktv
		or :old.qt_soro_reposicao	<> :new.qt_soro_reposicao
		or :old.qt_soro_devolucao	<> :new.qt_soro_devolucao
		or :old.nr_seq_tipo_soro	<> :new.nr_seq_tipo_soro
		or :old.qt_condutividade	<> :new.qt_condutividade
		or :old.qt_hgt			<> :new.qt_hgt
		or :old.qt_dextro		<> :new.qt_dextro
		or :old.qt_insulina		<> :new.qt_insulina
		or :old.qt_glicose_adm		<> :new.qt_glicose_adm
		or :old.qt_insulina_int		<> :new.qt_insulina_int
		or (:old.nr_sequencia		is null		and	:new.nr_sequencia		is not null)
		or (:old.dt_atualizacao		is null		and	:new.dt_atualizacao		is not null)
		or (:old.nm_usuario		is null		and	:new.nm_usuario			is not null)
		or (:old.dt_atualizacao_nrec	is null		and	:new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec	is null		and	:new.nm_usuario_nrec		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.dt_controle		is null		and	:new.dt_controle		is not null)
		or (:old.qt_fluxo_sangue	is null		and	:new.qt_fluxo_sangue		is not null)
		or (:old.qt_fluxo_dialisado	is null		and	:new.qt_fluxo_dialisado		is not null)
		or (:old.qt_pa_trans_menbrana	is null		and	:new.qt_pa_trans_menbrana	is not null)
		or (:old.qt_pa_venosa		is null		and	:new.qt_pa_venosa		is not null)
		or (:old.qt_pa_arterial		is null		and	:new.qt_pa_arterial		is not null)
		or (:old.qt_heparina		is null		and	:new.qt_heparina		is not null)
		or (:old.qt_temperatura_axiliar	is null		and	:new.qt_temperatura_axiliar	is not null)
		or (:old.qt_pulso		is null		and	:new.qt_pulso			is not null)
		or (:old.qt_pa_sistolica	is null		and	:new.qt_pa_sistolica		is not null)
		or (:old.qt_pa_diastolica	is null		and	:new.qt_pa_diastolica		is not null)
		or (:old.qt_sangue_dial		is null		and	:new.qt_sangue_dial		is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_clearence 		is null		and	:new.qt_clearence 		is not null)
		or (:old.qt_ktv			is null		and	:new.qt_ktv			is not null)
		or (:old.qt_soro_reposicao	is null		and	:new.qt_soro_reposicao		is not null)
		or (:old.qt_soro_devolucao	is null		and	:new.qt_soro_devolucao		is not null)
		or (:old.nr_seq_tipo_soro	is null		and	:new.nr_seq_tipo_soro		is not null)
		or (:old.qt_condutividade	is null		and	:new.qt_condutividade		is not null)
		or (:old.qt_hgt			is null		and	:new.qt_hgt			is not null)
		or (:old.qt_dextro	 	is null		and	:new.qt_dextro	 		is not null)
		or (:old.qt_insulina   		is null		and	:new.qt_insulina   		is not null)
		or (:old.qt_glicose_adm  	is null		and	:new.qt_glicose_adm  		is not null)
		or (:old.qt_insulina_int	is null		and	:new.qt_insulina_int		is not null)
		or (:old.nr_sequencia		is not null	and	:new.nr_sequencia		is null)
		or (:old.dt_atualizacao		is not null	and	:new.dt_atualizacao		is null)
		or (:old.nm_usuario		is not null	and	:new.nm_usuario			is null)
		or (:old.dt_atualizacao_nrec	is not null	and	:new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec	is not null	and	:new.nm_usuario_nrec		is null)
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.dt_controle		is not null	and	:new.dt_controle		is null)
		or (:old.qt_fluxo_sangue	is not null	and	:new.qt_fluxo_sangue		is null)
		or (:old.qt_fluxo_dialisado	is not null	and	:new.qt_fluxo_dialisado		is null)
		or (:old.qt_pa_trans_menbrana	is not null	and	:new.qt_pa_trans_menbrana	is null)
		or (:old.qt_pa_venosa		is not null	and	:new.qt_pa_venosa		is null)
		or (:old.qt_pa_arterial		is not null	and	:new.qt_pa_arterial		is null)
		or (:old.qt_heparina		is not null	and	:new.qt_heparina		is null)
		or (:old.qt_temperatura_axiliar	is not null	and	:new.qt_temperatura_axiliar	is null)
		or (:old.qt_pulso		is not null	and	:new.qt_pulso			is null)
		or (:old.qt_pa_sistolica	is not null	and	:new.qt_pa_sistolica		is null)
		or (:old.qt_pa_diastolica	is not null	and	:new.qt_pa_diastolica		is null)
		or (:old.qt_sangue_dial		is not null	and	:new.qt_sangue_dial		is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_clearence 		is not null	and	:new.qt_clearence 		is null)
		or (:old.qt_ktv			is not null	and	:new.qt_ktv			is null)
		or (:old.qt_soro_reposicao	is not null	and	:new.qt_soro_reposicao		is null)
		or (:old.qt_soro_devolucao	is not null	and	:new.qt_soro_devolucao		is null)
		or (:old.nr_seq_tipo_soro	is not null	and	:new.nr_seq_tipo_soro		is null)
		or (:old.qt_condutividade	is not null	and	:new.qt_condutividade		is null)
		or (:old.qt_hgt			is not null	and	:new.qt_hgt			is null)
		or (:old.qt_dextro	 	is not null	and	:new.qt_dextro	 		is null)
		or (:old.qt_insulina   		is not null	and	:new.qt_insulina   		is null)
		or (:old.qt_glicose_adm  	is not null	and	:new.qt_glicose_adm  		is null)
		or (:old.qt_insulina_int	is not null	and	:new.qt_insulina_int		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_CONTROLE ADD (
  CONSTRAINT HDCONTO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_CONTROLE ADD (
  CONSTRAINT HDCONTO_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDCONTO_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_CONTROLE TO NIVEL_1;

