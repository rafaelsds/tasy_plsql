ALTER TABLE TASY.MOVTO_TRANS_FINANC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVTO_TRANS_FINANC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVTO_TRANS_FINANC
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_TRANSACAO             DATE                 NOT NULL,
  NR_SEQ_TRANS_FINANC      NUMBER(10)           NOT NULL,
  VL_TRANSACAO             NUMBER(15,2),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  VL_RECEBIDO              NUMBER(15,2),
  NR_SEQ_BANCO             NUMBER(10),
  NR_SEQ_CAIXA             NUMBER(10),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  CD_CGC                   VARCHAR2(14 BYTE),
  NR_SEQ_TITULO_PAGAR      NUMBER(10),
  NR_SEQ_TITULO_RECEBER    NUMBER(10),
  NR_BORDERO               NUMBER(10),
  NR_ADIANTAMENTO          NUMBER(10),
  NR_SEQ_BANCO_OD          NUMBER(10),
  NR_SEQ_CAIXA_OD          NUMBER(10),
  CD_CONTA_CONTABIL        VARCHAR2(20 BYTE),
  CD_CENTRO_CUSTO          NUMBER(8),
  NR_SEQ_NOTA_FISCAL       NUMBER(10),
  NR_INTERNO_CONTA         NUMBER(10),
  NR_DOCUMENTO             VARCHAR2(22 BYTE),
  DS_HISTORICO             VARCHAR2(255 BYTE),
  NR_SEQ_LOTE              NUMBER(10),
  DT_REFERENCIA_SALDO      DATE,
  DT_FECHAMENTO_LOTE       DATE,
  CD_TIPO_RECEBIMENTO      NUMBER(5),
  CD_TIPO_BAIXA_CPA        NUMBER(5),
  NR_SEQ_CHEQUE            NUMBER(10),
  NR_SEQ_CONV_RECEB        NUMBER(10),
  NR_SEQ_DEPOSITO          NUMBER(10),
  NR_SEQ_SALDO_CAIXA       NUMBER(10),
  NR_SEQ_BANCO_ESCRIT      NUMBER(10),
  NR_SEQ_SALDO_BANCO       NUMBER(10),
  DT_AUTENTICACAO          DATE,
  NR_LOTE_CONTABIL         NUMBER(10)           NOT NULL,
  NR_SEQ_CONCIL            NUMBER(10),
  NR_ADIANT_PAGO           NUMBER(10),
  IE_CONCILIACAO           VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_CHEQUE_CP         NUMBER(10),
  IE_ORIGEM_LANCAMENTO     VARCHAR2(1 BYTE),
  CD_HISTORICO             NUMBER(10),
  NR_BORDERO_REC           NUMBER(10),
  NR_SEQ_MOVTO_CARTAO      NUMBER(10),
  NR_SEQ_MOTIVO_DEV        NUMBER(10),
  NR_SEQ_CAIXA_REC         NUMBER(10),
  NR_SEQ_MOVTO_TRANSF      NUMBER(10),
  DT_CONFERENCIA           DATE,
  NR_SEQ_COBR_ESCRIT       NUMBER(10),
  NR_SEQ_RELAT_DESP        NUMBER(10),
  IE_ESTORNO               VARCHAR2(1 BYTE),
  NR_SEQ_BORDERO_CHEQUE    NUMBER(10),
  NR_SEQ_PROJ_GPI          NUMBER(10),
  NR_SEQ_ETAPA_GPI         NUMBER(10),
  NR_SEQ_LOTE_CARTAO       NUMBER(10),
  NR_SEQ_PROJ_REC          NUMBER(10),
  NR_SEQ_MOVTO_ORIG        NUMBER(10),
  CD_CONTA_FINANC          NUMBER(10),
  CD_TIPO_PORTADOR         NUMBER(5),
  CD_PORTADOR              NUMBER(10),
  NR_SEQ_LOTE_ORIGEM       NUMBER(10),
  NR_SEQ_CLASSIF           NUMBER(10),
  IE_REJEITADO             VARCHAR2(1 BYTE),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  NR_SEQ_CAIXA_OD_REJ      NUMBER(10),
  NR_SEQ_MOVTO_TRANSF_BCO  NUMBER(10),
  NR_SEQ_PAGADOR           NUMBER(10),
  DT_REJEICAO              DATE,
  NM_USUARIO_REJEICAO      VARCHAR2(15 BYTE),
  NR_SEQ_MOVTO_REJEITADO   NUMBER(10),
  NR_SEQ_PARC_CARTAO       NUMBER(10),
  NR_RECIBO_CB             NUMBER(10),
  IE_OUTROS_REC            VARCHAR2(1 BYTE),
  NR_SEQ_SEGURADO          NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4),
  NR_SEQ_PERDA             NUMBER(10),
  NR_SEQ_BANDEIRA          NUMBER(10),
  NR_SEQ_REGRA_BANCO       NUMBER(10),
  NR_SEQ_MOVTO_ORIG_ACRES  NUMBER(10),
  CD_PACIENTE              VARCHAR2(10 BYTE),
  IE_SEG_DEV_CHEQUE        VARCHAR2(1 BYTE),
  VL_TRANSACAO_ESTRANG     NUMBER(15,2),
  CD_MOEDA                 NUMBER(5),
  VL_COTACAO               NUMBER(21,10),
  VL_COMPLEMENTO           NUMBER(15,2),
  NR_SEQ_APLICACAO         NUMBER(15,3),
  IE_DEV_ADIANT_NEGATIVO   VARCHAR2(1 BYTE),
  IE_CONFERIDO             VARCHAR2(1 BYTE),
  CD_CONVENIO              NUMBER(5),
  DS_STACK                 VARCHAR2(4000 BYTE),
  DS_DESCRICAO_GLOSA       VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          30M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOVTRFI_ADIANTA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_ADIPAGO_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_ADIANT_PAGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_BANCACR_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_BANDEIRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_BANCACR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_BANESCR_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_BANCO_ESCRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_BANESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_BANESTA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_BANESTA_FK2_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_BANCO_OD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_BANESTA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_BANSALD_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_SALDO_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_BCOAPL_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_BORCHEQ_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_BORDERO_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_BORCHEQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_BORPAGA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_BORDERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_BORRECE_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_BORDERO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CAIREC_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CAISADI_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_SALDO_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CAIXA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CAIXA_FK2_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CAIXA_OD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CAIXA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CAIXA_FK3_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CAIXA_OD_REJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CAIXA_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CENCUST_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CHEQUE_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CHEQUE_CP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CHEQUES_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CLASMTF_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CLASMTF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_COBESCR_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_COBR_ESCRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CONBAMO_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CONCIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CONBAMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CONCONT_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CONFINA_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CONPACI_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_CONPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_CONRECE_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_CONV_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_CONVENI_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_DEPOSIT_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_DEPOSITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_DEPOSIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_ESTABEL_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_GPICRETA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_ETAPA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_GPICRETA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_GPIPROJ_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_GPIPROJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_HISPADR_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI__I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_MOVTO_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_I1 ON TASY.MOVTO_TRANS_FINANC
(DT_TRANSACAO, NR_SEQ_SALDO_BANCO, NR_SEQ_CONCIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_I2 ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_LOTE, NR_SEQ_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_I3 ON TASY.MOVTO_TRANS_FINANC
(DT_TRANSACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_I5 ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_SALDO_BANCO, TRUNC("DT_TRANSACAO",'fmdd'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_LBCART_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_LOTE_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_LBCART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_LOTCONT_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_MOCRPAR_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_PARC_CARTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_MOCRPAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_MODEVCH_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_MODEVCH_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_MOVTRFI_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_MOVTO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_MOVTRFI_FK2_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_MOVTO_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_MOVTRFI_FK3_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_MOVTO_REJEITADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_MOVTRFI_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_NOTFISC_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_NOTA_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_NOTFISC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_PESFISI_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_PESFISI_FK2_I ON TASY.MOVTO_TRANS_FINANC
(CD_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_PESJURI_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MOVTRFI_PK ON TASY.MOVTO_TRANS_FINANC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_PLCONPAG_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_PLCONPAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_PLSSEGU_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_PORTADO_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_PRDACRE_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_PRDACRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_PRORECU_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_REGLABAN_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_REGRA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_REGLABAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_TIPBACP_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_TIPO_BAIXA_CPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_TIPBACP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_TIPRECE_FK_I ON TASY.MOVTO_TRANS_FINANC
(CD_TIPO_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_TIPRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_TITPAGA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_TITPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVTRFI_TITRECE_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_TITULO_RECEBER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_TRAFINA_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVTRFI_VIAREDE_FK_I ON TASY.MOVTO_TRANS_FINANC
(NR_SEQ_RELAT_DESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVTRFI_VIAREDE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.movto_trans_financ_after
after insert ON TASY.MOVTO_TRANS_FINANC for each row
declare

nm_tabela_w		ctb_documento.nm_tabela%type;
vl_movimento_w		ctb_documento.vl_movimento%type;
nr_seq_tab_compl_w	ctb_documento.nr_seq_doc_compl%type;
nr_seq_info_w		ctb_documento.nr_seq_info%type;
ie_contab_cb_w		transacao_financeira.ie_contab_cb%type;
ie_banco_w		transacao_financeira.ie_banco%type;
nr_documento_w		ctb_documento.nr_documento%type;
nr_doc_analitico_w	ctb_documento.nr_doc_analitico%type;


cursor c01 is
	select	a.nm_atributo,
		a.cd_tipo_lote_contab
	from	atributo_contab a
	where 	a.cd_tipo_lote_contab = 18
	and	:new.nr_seq_banco is not null
	and 	a.nm_atributo in (	'VL_TRANSACAO', 'VL_TRANSACAO_ESTRANG', 'VL_MOEDA_COMPLEMENTAR', 'VL_CHEQUE_DEPOSITO');

c01_w		c01%rowtype;

cursor c02 is
	select	a.vl_cheque * decode(:new.ie_estorno, 'E', -1, 1) vl_movimento,
		a.nr_seq_cheque
	from	cheque_cr a,
		deposito_cheque f
	where	f.nr_seq_cheque		= a.nr_seq_cheque
	and 	f.nr_seq_deposito	= :new.nr_seq_deposito
	and 	a.vl_cheque <> 0;

c02_w		c02%rowtype;

cursor c03 is
	select 	g.vl_cheque,
		g.nr_seq_cheque
	from	cheque_cr g,
		deposito_cheque f
	where	f.nr_seq_cheque		= g.nr_seq_cheque
	and	f.nr_seq_deposito	= :new.nr_seq_deposito;

c03_w		c03%rowtype;

cursor c04 is
	select	a.vl_especie * decode(:new.ie_estorno, 'E', -1, 1) vl_movimento,
			a.nr_sequencia
	from	deposito a
	where	a.nr_sequencia = :new.nr_seq_deposito
	and	nvl(a.vl_especie,0)	> 0;

c04_w		c04%rowtype;

begin


open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	if	(c01_w.cd_tipo_lote_contab = 18) then
		begin
		nm_tabela_w		:= 'MOVTO_TRANS_FINANC';
		nr_seq_info_w		:= 5;
		nr_seq_tab_compl_w	:= null;
		nr_documento_w		:= null;
		nr_doc_analitico_w	:= null;

		if	(:new.nr_seq_titulo_pagar is not null) then
			begin
			nr_documento_w:= :new.nr_seq_titulo_pagar;
			nr_seq_tab_compl_w := :new.nr_sequencia;
			end;
		elsif	(:new.nr_seq_titulo_receber is not null) then
			begin
			nr_documento_w:= :new.nr_seq_titulo_receber;
			nr_seq_tab_compl_w := :new.nr_sequencia;
			end;
		end if;

		if	(nr_documento_w is null) then
			begin
			nr_documento_w:= :new.nr_sequencia;
			nr_seq_tab_compl_w := null;
			end;
		end if;

		begin

		select	nvl(ie_contab_cb, 'N'),
			ie_banco
		into	ie_contab_cb_w,
			ie_banco_w
		from	transacao_financeira
		where	nr_sequencia = :new.nr_seq_trans_financ;

		exception
		when others then
			ie_contab_cb_w := 'N';
		end;

		if	(ie_contab_cb_w = 'S') then
			begin

			nm_tabela_w:= 	case	c01_w.nm_atributo
					when	'VL_CHEQUE_DEPOSITO' then 'CHEQUE_CR'
					else
						'MOVTO_TRANS_FINANC'
					end;
			nr_seq_info_w:= 5;


			if	(c01_w.nm_atributo = 'VL_CHEQUE_DEPOSITO') then
				begin
				vl_movimento_w := null;
				if	(nvl(ie_banco_w, 'X') = 'D') or (nvl(ie_banco_w, 'X') = 'C') or (nvl(ie_banco_w, 'X') = 'T') then
					begin

					begin

					vl_movimento_w:= null;

					open c03;
					loop
					fetch c03 into
						c03_w;
					exit when c03%notfound;
						begin

						if	(nvl(c03_w.vl_cheque, 0) <> 0) then
							begin
							ctb_concil_financeira_pck.ctb_gravar_documento	(	:new.cd_estabelecimento,
														trunc(:new.dt_transacao),
														c01_w.cd_tipo_lote_contab,
														:new.nr_seq_trans_financ,
														nr_seq_info_w,
														nr_documento_w,
														c03_w.nr_seq_cheque,
														null,
														c03_w.vl_cheque,
														nm_tabela_w,
														c01_w.nm_atributo,
														:new.nm_usuario);
							end;
						end if;

						end;
					end loop;
					close c03;

					exception
					when others then
						vl_movimento_w := null;
					end;

					end;
				end if;

				end;
			else
				begin
					vl_movimento_w := 	case	c01_w.nm_atributo
								when	'VL_TRANSACAO' then :new.vl_transacao
								when	'VL_TRANSACAO_ESTRANG' then :new.vl_transacao_estrang
								when	'VL_MOEDA_COMPLEMENTAR' then :new.vl_complemento
								else
									null
								end;
				end;
			end if;

			end;
		end if;

		end;
	end if;

	if	(nvl(vl_movimento_w, 0) <> 0) then
		begin

		ctb_concil_financeira_pck.ctb_gravar_documento	(	nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento),
									trunc(:new.dt_transacao),
									c01_w.cd_tipo_lote_contab,
									:new.nr_seq_trans_financ,
									nr_seq_info_w,
									nr_documento_w,
									nr_seq_tab_compl_w,
									nr_doc_analitico_w,
									vl_movimento_w,
									nm_tabela_w,
									c01_w.nm_atributo,
									:new.nm_usuario);
		end;
	end if;



	end;
end loop;
close c01;

end;
/


CREATE OR REPLACE TRIGGER TASY.MOVTO_TRANS_FINANC_tp  after update ON TASY.MOVTO_TRANS_FINANC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CONCIL,1,4000),substr(:new.NR_SEQ_CONCIL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONCIL',ie_log_w,ds_w,'MOVTO_TRANS_FINANC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Movto_Trans_Financ_Delete
before delete ON TASY.MOVTO_TRANS_FINANC FOR EACH ROW
declare

nr_seq_saldo_banco_w		Number(10,0);
ie_acao_w			Number(5,0);
ds_historico_w			varchar2(4000);
/* Projeto Multimoeda - Vari�veis */
vl_transacao_estrang_w		number(15,2);

begin

begin

if	(:old.dt_fechamento_lote is not null) then
	/* O lote de caixa j� foi fechado, o lan�amento n�o pode ser exclu�do!
	Lote: :old.nr_seq_lote
	Seq: :old.nr_sequencia
	Dt fechamento: :old.dt_fechamento_lote */
	wheb_mensagem_pck.exibir_mensagem_abort(244040, 'NR_SEQ_LOTE_W=' || :old.nr_seq_lote ||
							';NR_SEQUENCIA_W=' || :old.nr_sequencia ||
							';DT_FECHAMENTO_LOTE_W=' || :old.dt_fechamento_lote);
end if;

if	(:old.nr_lote_contabil <> 0) then
	/* Este lan�amento j� est� contablizado, n�o pode ser exclu�do! */
	wheb_mensagem_pck.exibir_mensagem_abort(244042);
end if;

if	(:old.nr_seq_banco	is not null) then
	begin

	select	ie_acao
	into	ie_acao_w
	from	transacao_financeira
	where	nr_sequencia = :old.nr_seq_trans_financ;

	if	(ie_acao_w <> 0) then
		/* A a��o desta transa��o n�o permite que o lan�amento seja exclu�do! */
		wheb_mensagem_pck.exibir_mensagem_abort(244043);
	end if;

	/* Projeto Multimoeda - Verifica se a transa��o � moeda estrangeira */
	if (nvl(:old.vl_transacao_estrang,0) <> 0) then
		vl_transacao_estrang_w := :old.vl_transacao_estrang * -1;
	else
		vl_transacao_estrang_w := null;
	end if;

	Atualizar_Saldo_Movto_Bco(	:old.nr_sequencia,
					:old.nm_usuario,
					(:old.vl_transacao * -1),
					:old.nr_seq_banco,
					:old.dt_transacao,
					:old.nr_seq_trans_financ,
					nr_seq_saldo_banco_w,
					vl_transacao_estrang_w);
	end;
end if;

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_sequencia,null,'CBT',:old.dt_transacao,'E',:old.nm_usuario);
if (:old.nr_seq_banco is not null) then
	gravar_agend_fluxo_caixa(:old.nr_sequencia,:old.nr_seq_banco,'CBS',:old.dt_transacao,'E',:old.nm_usuario);
end if;

exception
when	others then
	begin

	ds_historico_w	:=	substr(wheb_mensagem_pck.get_texto(304242),1,255) || ': ' || trim(to_char(:old.vl_transacao,'999999990.00')) || chr(13) || chr(10) ||
				substr(wheb_mensagem_pck.get_texto(304243),1,255) || ': ' || :old.nr_seq_trans_financ || chr(13) || chr(10) ||
				substr(wheb_mensagem_pck.get_texto(304244),1,255) || ': ' || :old.nr_sequencia;

	ds_historico_w	:= substr(sqlerrm,1,2000) || chr(13) || chr(10) || ds_historico_w;

	gerar_banco_caixa_hist(	:old.nr_seq_banco,
				:old.nr_seq_caixa,
				ds_historico_w,
				:old.nm_usuario,
				'S',
				'S');

	/* ds_historico_w */
	wheb_mensagem_pck.exibir_mensagem_abort(244091,'DS_HISTORICO_W=' || ds_historico_w);

	end;

end;

END;
/


CREATE OR REPLACE TRIGGER TASY.movto_trans_financ_insert
BEFORE INSERT ON TASY.MOVTO_TRANS_FINANC FOR EACH ROW
DECLARE

vl_documento_w			number(15,2)	:= 0;
vl_documento_estrang_w		number(15,2)	:= 0;
nr_seq_trans_banco_w		Number(10,0);
ie_banco_w			varchar2(01);
nr_sequencia_w			Number(10,0);
vl_saldo_adiant_w			Number(15,2);
vl_saldo_w			Number(15,2);
VL_TRIBUTOS_w			Number(15,2);
ie_acao_w			number(5);
cont_w				number(5);
nr_titulo_w			number(10,0);
ds_lista_titulo_w			varchar2(254);
ie_caixa_w			varchar2(5);
ie_tipo_w				varchar2(10);
nr_seq_conta_banco_w		number(10);
vl_cheque_cp_w			number(13,2);
vl_imposto_bordero_w		number(15,2);
dt_deposito_w			date;
dt_venda_terc_w			date;
dt_saque_w			date;
qt_vigente_w			number(03,0);
dt_devolucao_w			date;
cd_estabelecimento_w		number(4);
cd_estab_tit_w			number(15);
cd_empresa_w			number(15);
vl_especie_deposito_w		number(15,2);
vl_cheque_deposito_w		number(15,2);
vl_total_deposito_w			number(15,2);
vl_total_cheques_w			number(15,2);
ie_tipo_titulo_cpa_w		varchar2(2);
qt_registro_w			number(15);
cd_convenio_w			number(5);
cd_tipo_lote_contabil_w		number(2);
ie_baixa_lote_cp_w			varchar2(5);
ie_baixa_lote_cr_w			varchar2(5);
IE_CONSISTE_NF_w		varchar2(5);
nr_lote_w				number(10);
ds_tipo_lote_w			varchar2(255);
nr_lote_contab_gerado_w		number(10);
dt_referencia_w			date;
nr_seq_classif_w			number(10);
nr_seq_classif_lote_w		number(10);
nr_interno_conta_tit_w		number(10);
cd_estab_logado_w		number(10);
ie_dia_fechado_w			varchar2(1);
ie_conferir_movto_cb_w		varchar2(1);
qt_transacao_w			number(10);
ie_movto_lote_contabil_w 		varchar2(5);
ds_conta_w			banco_estabelecimento_v.ds_conta%type;
cd_moeda_unica_w		banco_estabelecimento_v.cd_moeda%type;
ds_moeda_w			moeda.ds_moeda%type;
cd_moeda_w			cheque_cr.cd_moeda%type;

/* Historico */
ds_origem_w			varchar2(255);
ds_documento_w			varchar2(255);
ds_historico_w			varchar2(4000);
nr_seq_banco_escrit_tit_w		number(10);
nr_seq_cobr_escrit_tit_w		number(10);
nr_bordero_tit_w			number(10);
nr_bordero_rec_tit_w		number(10);
nr_seq_baixa_w			number(10);
nr_seq_movto_orig_transf_w	number(10);
ie_estorno_w			varchar2(1);
dt_emissao_w			date;

ie_estornar_concil_w		varchar2(1);
ie_conciliado_w				movto_trans_financ.ie_conciliacao%type;
nr_seq_concil_w			movto_trans_financ.nr_seq_concil%type;
vl_cotacao_w			titulo_pagar.vl_cotacao%type;
nr_seq_parametro_cont_w		number(10);

cursor c01 is
select	a.nr_titulo
from	titulo_pagar a,
	bordero_pagamento b
where	(a.vl_saldo_titulo + nvl(a.VL_JUROS_BORDERO,0) +
	nvl(a.VL_MULTA_BORDERO,0) + nvl(a.VL_DESCONTO_BORDERO,0) + nvl(a.VL_OUT_DESP_BORDERO,0)) <
	(nvl(a.vl_bordero,0) + nvl(a.VL_JUROS_BORDERO,0) + nvl(a.VL_MULTA_BORDERO, 0) -
	nvl(a.vl_desconto_bordero,0) + nvl(a.vl_out_desp_bordero, 0))
and	a.nr_bordero	= b.nr_bordero
and	b.nr_bordero	= :new.nr_bordero;

cursor c02 is
select	distinct b.cd_moeda
from	deposito_cheque a,
	cheque_cr b
where	a.nr_seq_cheque		= b.nr_seq_cheque
and	a.nr_seq_deposito	= :new.nr_seq_deposito;

BEGIN

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

begin

if	(nvl(:new.cd_conta_contabil,'0') <> '0') then
	select	count(*)
	into	qt_vigente_w
	from	conta_contabil
	where	cd_conta_contabil = :new.cd_conta_contabil
	and	substr(obter_se_conta_vigente(:new.cd_conta_contabil, :new.dt_transacao),1,1) = 'S';

	if	(qt_vigente_w = 0) then
		/*A conta contabil informada esta fora da data de vigencia.*/
		wheb_mensagem_pck.exibir_mensagem_abort(183642);
	end if;
end if;

select	count(*)
into	qt_transacao_w
from	transacao_financeira a
where	a.nr_sequencia	= :new.nr_seq_trans_financ;

if	(nvl(qt_transacao_w,0)	= 0) then

	wheb_mensagem_pck.exibir_mensagem_abort(210098, 'NR_SEQ_TRANS_FINANC=' || to_char(:new.nr_seq_trans_financ));

else

	select	nr_seq_trans_banco,
		ie_banco,
		ie_acao,
		ie_caixa,
		nvl(ie_conferir_movto_cb,'N')
	into	nr_seq_trans_banco_w,
		ie_banco_w,
		ie_acao_w,
		ie_caixa_w,
		ie_conferir_movto_cb_w
	from	transacao_financeira
	where	nr_sequencia	= :new.nr_seq_trans_financ;

end if;

/* Obter estabelecimento */
if	(:new.nr_seq_banco is not null) then
	select	cd_estabelecimento,
			nr_seq_classif
	into	cd_estabelecimento_w,
			nr_seq_classif_w
	from	banco_estabelecimento
	where	nr_sequencia	= :new.nr_seq_banco;
elsif	(:new.nr_seq_saldo_banco is not null) then
	select	b.cd_estabelecimento,
			b.nr_seq_classif
	into	cd_estabelecimento_w,
			nr_seq_classif_w
	from	banco_estabelecimento b,
		banco_saldo a
	where	a.nr_seq_conta	= b.nr_sequencia
	and	a.nr_sequencia	= :new.nr_seq_saldo_banco;
elsif	(:new.nr_seq_caixa is not null) then
	select	cd_estabelecimento,
		nr_seq_classif
	into	cd_estabelecimento_w,
		nr_seq_classif_w
	from	caixa
	where	nr_sequencia	= :new.nr_seq_caixa;
end if;

/* ahoffelder - OS 536794 - 08/01/2013 - alterei para gravar sempre o estabelecimento da conta bancaria porque,
   caso os lancamentos sejam todos feitos atraves de um estabelecimento "matriz", os codigos dos estabelecimentos ficam incorretos
   causando problemas na contabilidade */

begin
	cd_estab_logado_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception when others then
	cd_estab_logado_w	:= null;
end;

if	(:new.nr_seq_banco is not null) or
	(:new.nr_seq_saldo_banco is not null) then
	:new.cd_estabelecimento	:= nvl(cd_estabelecimento_w,cd_estab_logado_w);
elsif	(nvl(cd_estab_logado_w,0) <> 0) then
	:new.cd_estabelecimento	:= cd_estab_logado_w;
end if;

/*	Consistencias	- Marcus - 06/09/2003  */
if	(:new.nr_seq_banco_escrit is not null) and
	((:new.nr_seq_banco is null) or (:new.nr_seq_caixa is not null)) then
	/*lancamento escritural somente pode ser baixado no banco !*/
	wheb_mensagem_pck.exibir_mensagem_abort(183645);
end if;

/*	Consistencias	- Marcus - 06/01/2006  */
if	(:new.cd_centro_custo is not null) then
	select	ie_tipo
	into	ie_tipo_w
	from	centro_custo
	where	cd_centro_custo	= :new.cd_centro_custo;
	if	(ie_tipo_w = 'T') then
		/*O Centro de custo nao pode ser do tipo totalizador !*/
		wheb_mensagem_pck.exibir_mensagem_abort(183646);
	end if;
end if;

if	(:new.cd_conta_contabil is not null) then
	select	ie_tipo
	into	ie_tipo_w
	from	conta_contabil
	where	cd_conta_contabil	= :new.cd_conta_contabil;
	if	(ie_tipo_w = 'T') then
		/*A conta contabil nao pode ser do tipo totalizador !*/
		wheb_mensagem_pck.exibir_mensagem_abort(183647);
	end if;
end if;

if	(nr_seq_trans_banco_w is not null) then
	if	(ie_banco_w <> 'C') and
		(:new.nr_seq_caixa is null) then /* Francisco - OS 119268 - Quando for originaria de caixa nao fazer esse tratamento */
		/*O banco base deve ser o de origem do valor(Transferencia Saida)!*/
		wheb_mensagem_pck.exibir_mensagem_abort(183648);
	end if;
	if	(:new.nr_seq_banco is null) and
		(:new.nr_seq_caixa is null) then /* Francisco - OS 119268 - Quando for originaria de caixa nao fazer esse tratamento */
		/*O banco de origem nao foi informado!*/
		wheb_mensagem_pck.exibir_mensagem_abort(183649);
	end if;
	if	(:new.nr_seq_banco_od is null) then
		/*O banco de destino nao foi informado!*/
		wheb_mensagem_pck.exibir_mensagem_abort(183650);
	end if;
end if;

if	(:new.ie_origem_lancamento = 'C') and (ie_acao_w = 5) and (:new.nr_bordero is null) then
	/*Esta transacao nao tem bordero informado!*/
	wheb_mensagem_pck.exibir_mensagem_abort(183651);
end if;

if	(:new.ie_origem_lancamento = 'C') and (ie_acao_w = 3) and (:new.nr_adiantamento is not null)  and (:new.vl_transacao < 0) then
	select	nvl(max(vl_saldo),0)
	into	vl_saldo_adiant_w
	from	adiantamento
	where	nr_adiantamento	= :new.nr_adiantamento;

	if	((vl_saldo_adiant_w + :new.vl_transacao) < 0) then
		/*O saldo do adiantamento nao pode ser menor que zero!*/
		wheb_mensagem_pck.exibir_mensagem_abort(183652);
	end if;
end if;

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_transacao);


/* ahoffelder - 09/05/2013 - coloquei a Atualizar_Saldo_Movto_Bco aqui porque abaixo sao feitas consistencia que necessitam do campo nr_seq_saldo_banco */

if	(:new.nr_seq_banco is not null) then
	Atualizar_Saldo_Movto_Bco(	:new.nr_sequencia,
					:new.nm_usuario,
					:new.vl_transacao,
					:new.nr_seq_banco,
					:new.dt_transacao,
					:new.nr_seq_trans_financ,
					:new.nr_seq_saldo_banco,
					:new.vl_transacao_estrang,
					:new.vl_cotacao);

end if;
/*Retirado daqui pois pode ocorrer Mutante. Sera tratado no delphi, no BeforePost.
Obter_Param_Usuario(814, 100, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo, ie_estornar_concil_w);

if ( nvl(ie_estornar_concil_w,'S') = 'N') and (:new.nr_seq_movto_orig is not null) then


	select	nvl(max(a.ie_conciliacao),'N'),
			max(a.nr_seq_concil)
	into	ie_conciliado_w,
			nr_seq_concil_w
	from	movto_trans_financ a
	where	a.nr_sequencia = :new.nr_seq_movto_orig;

	if ( nvl(ie_conciliado_w,'N') = 'S' ) or (nr_seq_concil_w is not null) then

		wheb_mensagem_pck.exibir_mensagem_abort(238696);
	end if;

end if;
*/
/*smsouza - 16/01/2014 - OS 680835 - Solicitacao de bloqueio do lancamento de movimentacao em mes ja gerado na Contabilidade */

if	(:new.nr_seq_saldo_banco is not null) and
	(:new.nr_seq_titulo_pagar is null) and
	(:new.nr_seq_titulo_receber is null) then

	select	max(a.ie_movto_lote_contabil)
	into 	ie_movto_lote_contabil_w
	from 	parametro_controle_banc a
	where 	a.cd_estabelecimento = nvl(cd_estabelecimento_w,:new.cd_estabelecimento);

	if (ie_movto_lote_contabil_w = 'M') then

		select	count(*),
			max(a.nr_lote_contabil),
			substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255)
		into	qt_registro_w,
			nr_lote_w,
			ds_tipo_lote_w
		from 	lote_contabil a
		where	a.cd_estabelecimento		= nvl(cd_estabelecimento_w,:new.cd_estabelecimento)
		and	trunc(a.dt_referencia,'month')	= trunc(:new.dt_transacao,'month')
		and	a.dt_geracao_lote is not null
		and	a.cd_tipo_lote_contabil		= 18
		and	exists	(
				select	1
				from	movimento_contabil z
				where	z.nr_lote_contabil   = a.nr_lote_contabil
				);

		if	(qt_registro_w > 0) then

			select	nvl(max(vl_parametro),0)
			into	nr_seq_classif_lote_w
			from	lote_contabil_parametro
			where	nr_lote_contabil	= nr_lote_w
			and	nr_seq_parametro	= 2; /* Fixo dois pois esse e a sequencia para Classificacao da conta bancaria nos Eventos Contabeis*/


			if (nvl(nr_seq_classif_lote_w,0) = 0) then

				select 	nvl(max(a.nr_documento),0)
				into	nr_seq_classif_lote_w
				from	lote_contabil_param_item a
				where	nr_lote_contabil	= nr_lote_w
				and		nr_seq_parametro	= 2; /* Fixo dois pois esse e a sequencia para Classificacao da conta bancaria nos Eventos Contabeis*/


			end if;

			if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
				/*Ja foi gerado lote contabil para esta data de baixa.*/
				wheb_mensagem_pck.exibir_mensagem_abort(280653);
			end if;
		end if;
	elsif (ie_movto_lote_contabil_w = 'D') then
		select	count(*),
			max(a.nr_lote_contabil),
			substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255)
		into	qt_registro_w,
			nr_lote_w,
			ds_tipo_lote_w
		from 	lote_contabil a
		where	a.cd_estabelecimento		= nvl(cd_estabelecimento_w,:new.cd_estabelecimento)
		and	trunc(a.dt_referencia,'dd')	= trunc(:new.dt_transacao,'dd')
		and	a.dt_geracao_lote is not null
		and	a.cd_tipo_lote_contabil		= 18
		and	exists	(
				select	1
				from	movimento_contabil z
				where	z.nr_lote_contabil   = a.nr_lote_contabil
				);

		if	(qt_registro_w > 0) then

			select	nvl(max(vl_parametro),0)
			into	nr_seq_classif_lote_w
			from	lote_contabil_parametro
			where	nr_lote_contabil	= nr_lote_w
			and	nr_seq_parametro	= 2; /* Fixo dois pois esse e a sequencia para Classificacao da conta bancaria nos Eventos Contabeis*/


			if (nvl(nr_seq_classif_lote_w,0) = 0) then

				select 	nvl(max(a.nr_documento),0)
				into	nr_seq_classif_lote_w
				from	lote_contabil_param_item a
				where	nr_lote_contabil	= nr_lote_w
				and		nr_seq_parametro	= 2; /* Fixo dois pois esse e a sequencia para Classificacao da conta bancaria nos Eventos Contabeis*/


			end if;

			if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
				/*Ja foi gerado lote contabil para esta data de baixa.*/
				wheb_mensagem_pck.exibir_mensagem_abort(280653);
			end if;
		end if;

	end if;

end if;


if	(:new.nr_bordero is not null) and (ie_acao_w = 5) then
	begin

	select	sum(a.vl_saldo_titulo_cotacao) +
		sum(a.VL_JUROS_BORDERO) +
		sum(a.VL_MULTA_BORDERO) - sum(a.VL_DESCONTO_BORDERO) + sum(a.VL_OUT_DESP_BORDERO),
		sum(a.vl_bordero) + sum(a.VL_JUROS_BORDERO) +
		sum(a.VL_MULTA_BORDERO) - sum(a.VL_DESCONTO_BORDERO) + sum(a.VL_OUT_DESP_BORDERO),
		nvl(sum(a.VL_TRIBUTOS),0)
	into	vl_saldo_w,
		vl_documento_w,
		VL_TRIBUTOS_w
	from	titulo_pagar_bordero_v a,
		bordero_pagamento b
	where	b.nr_bordero	= a.nr_bordero
	and	b.nr_bordero	= :new.nr_bordero
	and	a.dt_liquidacao is null;

	-- Edgar 12/02/2008, OS 80633, calcular tributos que serao gerados na baixa do titulo
	select	nvl(sum(vl_imposto),0)
	into	vl_imposto_bordero_w
	from	w_titulo_pagar_imposto
	where	nr_bordero	= :new.NR_BORDERO;

	if	(vl_imposto_bordero_w = 0) and
		(vl_documento_w > 0) and
		(VL_TRIBUTOS_w = 0) then

		select	sum(a.vl_saldo_titulo_cotacao) +
			sum(a.VL_JUROS_BORDERO) +
			sum(a.VL_MULTA_BORDERO) - sum(a.VL_DESCONTO_BORDERO) + sum(a.VL_OUT_DESP_BORDERO),
			sum(a.vl_bordero) + sum(a.VL_JUROS_BORDERO) +
			sum(a.VL_MULTA_BORDERO) - sum(a.VL_DESCONTO_BORDERO) + sum(a.VL_OUT_DESP_BORDERO),
			nvl(sum(a.VL_TRIBUTOS),0)
		into	vl_saldo_w,
			vl_documento_w,
			VL_TRIBUTOS_w
		from	titulo_pagar_bordero_v a,
			bordero_pagamento b
		where	b.nr_bordero	= a.nr_bordero
		and	b.nr_bordero	= :new.nr_bordero
		and	a.dt_liquidacao is not null;

		vl_imposto_bordero_w	:= (VL_TRIBUTOS_w) * -1;

	end if;

	vl_documento_w		:= vl_documento_w - vl_imposto_bordero_w;

	if	(:new.vl_transacao >= 0) then
		if	(vl_documento_w <> :new.vl_transacao) then
			wheb_mensagem_pck.exibir_mensagem_abort(183653,'VL_DOCUMENTO_P='||vl_documento_w||';VL_TRANSACAO_P='||:new.vl_transacao);
		end if;
		if	(vl_saldo_w < :new.vl_transacao) then

			ds_lista_titulo_w	:= '';
			open c01;
			loop
			fetch c01 into
				nr_titulo_w;
			exit when c01%notfound;
				if	(ds_lista_titulo_w is null) then
					ds_lista_titulo_w	:= substr(wheb_mensagem_pck.get_texto(304552),1,254) || nr_titulo_w;
				else
					ds_lista_titulo_w	:= ds_lista_titulo_w || ', ' || nr_titulo_w;
				end if;
			end loop;
			close c01;

			wheb_mensagem_pck.exibir_mensagem_abort(183658,'VL_SALDO_P= '|| vl_saldo_w ||
			';VL_TRANSACAO_P='||:new.vl_transacao||
			';DS_LISTA_TITULO_P='||ds_lista_titulo_w);

		end if;
	elsif	((:new.vl_transacao * -1) <> vl_documento_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(183661,'VL_DOCUMENTO_P='||vl_documento_w ||
							';VL_TRANSACAO_P='||:new.vl_transacao);

	end if;
	end;
elsif	(:new.nr_seq_titulo_pagar is not null) then
	begin
	select	max(vl_saldo_titulo),
		max(vl_cotacao),
		max(ie_tipo_titulo),
		max(cd_estabelecimento)
	into	vl_documento_w,
		vl_cotacao_w,
		ie_tipo_titulo_cpa_w,
		cd_estab_tit_w
	from	titulo_pagar
	where	nr_titulo	= :new.nr_seq_titulo_pagar;

	if (nvl(:new.ie_origem_lancamento,'O') = 'C') then /*Francisco - OS 34129 - Tratar somente no Controle Bancario */

		if	(nvl(:new.vl_cotacao,0) = 0 and vl_documento_w < nvl(:new.vl_transacao,0)) then
			/*O valor da transacao e maior que o valor do saldo do titulo !*/
			wheb_mensagem_pck.exibir_mensagem_abort(183662);
		end if;

		vl_documento_estrang_w := trunc(nvl(vl_cotacao_w,0) * nvl(:new.vl_transacao_estrang,0), 2);
		if	(nvl(:new.vl_cotacao,0) <> 0 and (vl_documento_estrang_w - (vl_documento_estrang_w - vl_documento_w)) > vl_documento_w) then
			/*O valor da transacao e maior que o valor do saldo do titulo !*/
			wheb_mensagem_pck.exibir_mensagem_abort(183662);
		end if;

	end if;

	/* Francisco - 02/09/2009 - OS 163867 */
	/* Acao baixa de titulo a pagar */

	if	(ie_acao_w = 2) then

		if	(nvl(fin_obter_se_mes_aberto(cd_estabelecimento_w,:new.dt_transacao,'CP',ie_tipo_titulo_cpa_w,null,null,null),'S') = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(183663);
		end if;

		select	dt_emissao
		into	dt_emissao_w
		from	titulo_pagar
		where	nr_titulo = :new.nr_seq_titulo_pagar;

		if (trunc(:new.dt_transacao,'dd') < trunc(dt_emissao_w,'dd')) then
			wheb_mensagem_pck.exibir_mensagem_abort(471997);
		end if;

		if	(cd_estab_tit_w is not null) then

			select	max(cd_empresa)
			into	cd_empresa_w
			from	estabelecimento
			where	cd_estabelecimento	= cd_estab_tit_w;

			if	(cd_empresa_w is not null) then

				if	(:new.nr_seq_saldo_banco	is not null) then

					select	max(a.ie_baixa_lote_cp)
					into	ie_baixa_lote_cp_w
					from	parametro_controle_banc a
					where	a.cd_estabelecimento	= cd_estab_tit_w;

					cd_tipo_lote_contabil_w	:= 18;
					nr_seq_parametro_cont_w := 2;

				elsif	(:new.nr_seq_saldo_caixa	is not null) then

					select	max(a.ie_baixa_lote_cp)
					into	ie_baixa_lote_cp_w
					from	parametro_tesouraria a
					where	a.cd_estabelecimento	= cd_estab_tit_w;

					cd_tipo_lote_contabil_w	:= 10;
					nr_seq_parametro_cont_w := 1;

				else

					select	max(ie_baixa_lote_contab)
					into	ie_baixa_lote_cp_w
					from	parametros_contas_pagar
					where 	cd_estabelecimento	= cd_estab_tit_w;

					cd_tipo_lote_contabil_w	:= 7;

					nr_seq_parametro_cont_w := 2;
				end if;

				if	(ie_baixa_lote_cp_w = 'N') then
					select	count(*)
					into	qt_registro_w
					from 	ctb_mes_ref
					where	cd_empresa							= cd_empresa_w
		       	        		and	substr(ctb_obter_se_mes_fechado(nr_sequencia,cd_estabelecimento_w),1,1)	= 'F'
		       		        	and	trunc(dt_referencia,'month')						= trunc(:new.dt_transacao,'month');

					if	(qt_registro_w > 0) then
						wheb_mensagem_pck.exibir_mensagem_abort(183664,'DT_TRANSACAO_P='||:new.dt_transacao);
					end if;

				elsif	(ie_baixa_lote_cp_w = 'L') then

					select	count(*),
						max(a.nr_lote_contabil),
						substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255)
					into	qt_registro_w,
						nr_lote_w,
						ds_tipo_lote_w
					from 	lote_contabil a
					where	a.cd_estabelecimento		= cd_estab_tit_w
					and	trunc(a.dt_referencia,'dd')	= trunc(:new.dt_transacao,'dd')
					and	a.dt_geracao_lote	is not null
					and	a.cd_tipo_lote_contabil			= cd_tipo_lote_contabil_w
					and	exists	(
							select	1
							from	movimento_contabil z
							where	z.nr_lote_contabil	= a.nr_lote_contabil
							);

					if	(qt_registro_w > 0) then

						select	nvl(max(vl_parametro),0)
						into	nr_seq_classif_lote_w
						from	lote_contabil_parametro
						where	nr_lote_contabil	= nr_lote_w
						and	nr_seq_parametro	= nr_seq_parametro_cont_w;

						if (nvl(nr_seq_classif_lote_w,0) = 0) then

							select 	nvl(max(a.nr_documento),0)
							into	nr_seq_classif_lote_w
							from	lote_contabil_param_item a
							where	nr_lote_contabil	= nr_lote_w
							and		nr_seq_parametro	= nr_seq_parametro_cont_w;

						end if;

						if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
							/*Ja foi gerado lote contabil para esta data de baixa.*/
							wheb_mensagem_pck.exibir_mensagem_abort(183670,'NR_LOTE_P='||nr_lote_w||';DS_TIPO_LOTE_P='||ds_tipo_lote_w);
						end if;
					end if;

				elsif	(ie_baixa_lote_cp_w = 'M') then

					select	count(*),
						max(a.nr_lote_contabil),
						substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255)
					into	qt_registro_w,
						nr_lote_w,
						ds_tipo_lote_w
					from 	lote_contabil a
					where	a.cd_estabelecimento		= cd_estab_tit_w
					and	trunc(a.dt_referencia,'dd')	>= trunc(:new.dt_transacao,'dd')
					and	trunc(a.dt_referencia,'month')	= trunc(:new.dt_transacao,'month')
					and	a.dt_geracao_lote	is not null
					and	a.cd_tipo_lote_contabil			= cd_tipo_lote_contabil_w
					and	exists	(
							select	1
							from	movimento_contabil z
							where	z.nr_lote_contabil	= a.nr_lote_contabil
							);

					if	(qt_registro_w > 0) then

						select	nvl(max(vl_parametro),0)
						into	nr_seq_classif_lote_w
						from	lote_contabil_parametro
						where	nr_lote_contabil	= nr_lote_w
						and	nr_seq_parametro	= nr_seq_parametro_cont_w;

						if (nvl(nr_seq_classif_lote_w,0) = 0) then

							select 	nvl(max(a.nr_documento),0)
							into	nr_seq_classif_lote_w
							from	lote_contabil_param_item a
							where	nr_lote_contabil	= nr_lote_w
							and		nr_seq_parametro	= nr_seq_parametro_cont_w;

						end if;

						if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
							/*Ja foi gerado lote contabil para esta data de baixa.*/
							wheb_mensagem_pck.exibir_mensagem_abort(183670,'NR_LOTE_P='||nr_lote_w||';DS_TIPO_LOTE_P='||ds_tipo_lote_w);
						end if;
					end if;

				elsif	(ie_baixa_lote_cp_w = 'F') then

					select	count(*)
					into	qt_registro_w
					from 	ctb_mes_ref
					where	cd_empresa		= cd_empresa_w
	                			and	trunc(dt_referencia,'month')	= trunc(:new.dt_transacao,'month');

					if	(qt_registro_w > 0) then
						wheb_mensagem_pck.exibir_mensagem_abort(183674,'DT_TRANSACAO_P='||:new.dt_transacao);
					end if;

				end if;
			end if;
		end if;
	end if;

	end;
elsif	(:new.nr_seq_titulo_receber is not null) then
	begin
	select	max(vl_saldo_titulo),
		obter_convenio_tit_rec(max(nr_titulo)),
		max(cd_estabelecimento),
		max(nr_interno_conta)
	into	vl_documento_w,
		cd_convenio_w,
		cd_estab_tit_w,
		nr_interno_conta_tit_w
	from	titulo_receber
	where	nr_titulo	= :new.nr_seq_titulo_receber;

	if	(vl_documento_w < :new.vl_transacao) and
		(nvl(:new.ie_origem_lancamento,'O') = 'C') then /*Francisco - OS 34129 - Tratar somente no Controle Bancario */
		/*O valor da transacao e maior que o valor do saldo do titulo !*/
		wheb_mensagem_pck.exibir_mensagem_abort(183675);
	end if;

	if	(nvl(nr_interno_conta_tit_w,0) > 0) and
		(nvl(:new.nr_interno_conta,0) > 0) and
		(nvl(nr_interno_conta_tit_w,0) <> nvl(:new.nr_interno_conta,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(193631);
	end if;

	/* Francisco - 02/09/2009 - OS 163867 */
	/* Acao baixa de titulo a receber */
	if	(ie_acao_w = 1) then

		if (nvl(fin_obter_se_mes_aberto(cd_estabelecimento_w,:new.dt_transacao,'CR',null,cd_convenio_w,null,null),'S') = 'N') then
			/*Nao e possivel baixar o titulo com esta data pois este mes/dia de referencia financeiro ja esta fechado!*/
			wheb_mensagem_pck.exibir_mensagem_abort(183676);
		end if;

		if	(cd_estab_tit_w is not null) then

			select	max(cd_empresa)
			into	cd_empresa_w
			from	estabelecimento
			where	cd_estabelecimento	= cd_estab_tit_w;

			if	(cd_empresa_w is not null) then

				if	(:new.nr_seq_saldo_banco	is not null) then

					select	max(a.ie_baixa_lote_cr)
					into	ie_baixa_lote_cr_w
					from	parametro_controle_banc a
					where	a.cd_estabelecimento	= cd_estab_tit_w;

					cd_tipo_lote_contabil_w	:= 18;
					nr_seq_parametro_cont_w	:= 2;

				elsif	(:new.nr_seq_saldo_caixa	is not null) then

					select	max(a.ie_baixa_lote_cr)
					into	ie_baixa_lote_cr_w
					from	parametro_tesouraria a
					where	a.cd_estabelecimento	= cd_estab_tit_w;

					cd_tipo_lote_contabil_w	:= 10;
					nr_seq_parametro_cont_w := 1;

				else

					select	max(ie_baixa_lote_contab)
					into	ie_baixa_lote_cr_w
					from	parametro_contas_receber
					where 	cd_estabelecimento	= cd_estab_tit_w;

					cd_tipo_lote_contabil_w	:= 5;
					nr_seq_parametro_cont_w := 2;

				end if;

				if	(ie_baixa_lote_cr_w = 'N') then
					select	count(*)
					into	qt_registro_w
					from 	ctb_mes_ref
					where	cd_empresa							= cd_empresa_w
		       	        		and	substr(ctb_obter_se_mes_fechado(nr_sequencia,cd_estabelecimento_w),1,1)	= 'F'
		       		        	and	trunc(dt_referencia,'month')						= trunc(:new.dt_transacao,'month');

					if	(qt_registro_w > 0) then
						wheb_mensagem_pck.exibir_mensagem_abort(183677,'DT_TRANSACAO_P='||:new.dt_transacao);
					end if;

				elsif	(ie_baixa_lote_cr_w = 'L') then

					select	count(*),
						max(a.nr_lote_contabil),
						substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255),
						max(a.dt_referencia)
					into	qt_registro_w,
						nr_lote_contab_gerado_w,
						ds_tipo_lote_w,
						dt_referencia_w
					from 	lote_contabil a
					where	a.cd_estabelecimento	= cd_estab_tit_w
					and	trunc(a.dt_referencia,'dd')		= trunc(:new.dt_transacao,'dd')
					and	a.dt_geracao_lote	is not null
					and	a.cd_tipo_lote_contabil			= cd_tipo_lote_contabil_w
					and	exists	(
							select	1
							from	movimento_contabil z
							where	z.nr_lote_contabil	= a.nr_lote_contabil
							);

					if	(qt_registro_w > 0) then

						select	nvl(max(vl_parametro),0)
						into	nr_seq_classif_lote_w
						from	lote_contabil_parametro
						where	nr_lote_contabil	= nr_lote_contab_gerado_w
						and	nr_seq_parametro	= nr_seq_parametro_cont_w;

						if (nvl(nr_seq_classif_lote_w,0) = 0) then

							select 	nvl(max(a.nr_documento),0)
							into	nr_seq_classif_lote_w
							from	lote_contabil_param_item a
							where	nr_lote_contabil	= nr_lote_contab_gerado_w
							and		nr_seq_parametro	= nr_seq_parametro_cont_w;

						end if;


						if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
							/*Ja foi gerado lote contabil para esta data de baixa.*/
							wheb_mensagem_pck.exibir_mensagem_abort(183678,'NR_LOTE_P='||nr_lote_contab_gerado_w||';ds_tipo_p='||

ds_tipo_lote_w||';dt_referencia_p='||dt_referencia_w);
						end if;
					end if;

				elsif	(ie_baixa_lote_cr_w = 'M') then

					select	count(*),
						max(a.nr_lote_contabil),
						substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255),
						max(a.dt_referencia)
					into	qt_registro_w,
						nr_lote_contab_gerado_w,
						ds_tipo_lote_w,
						dt_referencia_w
					from 	lote_contabil a
					where	a.cd_estabelecimento	= cd_estab_tit_w
					and	trunc(a.dt_referencia,'dd')		>= trunc(:new.dt_transacao,'dd')
					and	trunc(a.dt_referencia,'month')		= trunc(:new.dt_transacao,'month')
					and	a.dt_geracao_lote	is not null
					and	a.cd_tipo_lote_contabil			= cd_tipo_lote_contabil_w
					and	exists	(
							select	1
							from	movimento_contabil z
							where	z.nr_lote_contabil	= a.nr_lote_contabil
							);

					if	(qt_registro_w > 0) then

						select	nvl(max(vl_parametro),0)
						into	nr_seq_classif_lote_w
						from	lote_contabil_parametro
						where	nr_lote_contabil	= nr_lote_contab_gerado_w
						and	nr_seq_parametro	= nr_seq_parametro_cont_w;

						if (nvl(nr_seq_classif_lote_w,0) = 0) then

							select 	nvl(max(a.nr_documento),0)
							into	nr_seq_classif_lote_w
							from	lote_contabil_param_item a
							where	nr_lote_contabil	= nr_lote_contab_gerado_w
							and		nr_seq_parametro	= nr_seq_parametro_cont_w;

						end if;



						if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
							/*Ja foi gerado lote contabil para esta data de baixa.*/
							wheb_mensagem_pck.exibir_mensagem_abort(183678,'NR_LOTE_P='||nr_lote_contab_gerado_w||';ds_tipo_p='||

ds_tipo_lote_w||';dt_referencia_p='||dt_referencia_w);
						end if;
					end if;

				elsif	(ie_baixa_lote_cr_w = 'F') then

					select	count(*)
					into	qt_registro_w
					from 	ctb_mes_ref
					where	cd_empresa		= cd_empresa_w
	                			and	trunc(dt_referencia,'month')	= trunc(:new.dt_transacao,'month');

					if	(qt_registro_w > 0) then
						wheb_mensagem_pck.exibir_mensagem_abort(183679,'DT_TRANSACAO_P=' || :new.dt_transacao);
					end if;

				end if;
			end if;
		end if;
	end if;
	end;
elsif	(:new.nr_adiantamento is not null) then
	begin
	select	vl_saldo
	into	vl_documento_w
	from	adiantamento
	where	nr_adiantamento		= :new.nr_adiantamento;

	if	(vl_documento_w < :new.vl_transacao) then
		wheb_mensagem_pck.exibir_mensagem_abort(183680);
	end if;
	end;
elsif	(:new.nr_seq_nota_fiscal is not null) then
	begin
	if	(:new.NR_SEQ_BANCO is not null) then
		select	vl_total_nota,
			cd_estabelecimento
		into	vl_documento_w,
			cd_estabelecimento_w
		from	nota_fiscal
		where	nr_sequencia		= :new.nr_seq_nota_fiscal;

		select	nvl(max(a.IE_CONSISTE_NF), 'S')
		into	IE_CONSISTE_NF_w
		from	parametro_controle_banc a
		where	a.cd_estabelecimento	= cd_estabelecimento_w;

		if	(IE_CONSISTE_NF_w = 'S') and (abs(vl_documento_w) <> abs(:new.vl_transacao)) then
			wheb_mensagem_pck.exibir_mensagem_abort(183682);
		end if;
	end if;
	end;
end if;

if	(ie_acao_w = 16 and :new.nr_seq_cheque_cp is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(183683);
end if;
if	(ie_acao_w = 16 and :new.nr_seq_cheque_cp is not null) then
	select	vl_cheque,
		nr_seq_conta_banco
	into	vl_cheque_cp_w,
		nr_seq_conta_banco_w
	from	cheque
	where	nr_sequencia = :new.nr_seq_cheque_cp;

	if	(abs(vl_cheque_cp_w) <> abs(:new.vl_transacao)) then	/* Desfazer compensacao de cheque. (abs) */
		/*O valor da transacao nao bate com o valor do cheque!*/
		wheb_mensagem_pck.exibir_mensagem_abort(183685);
	end if;
	if	(nr_seq_conta_banco_w <> :new.nr_seq_banco) then
		/*O banco do cheque nao e o mesmo da transacao!*/
		wheb_mensagem_pck.exibir_mensagem_abort(183686);
	end if;
end if;

/* Francisco - OS 178135 - 13/11/2009 */
if	(ie_acao_w = 6) and
	(:new.nr_seq_deposito is not null) and
	(nvl(:new.ie_origem_lancamento,'O') = 'C') then

	if	(:new.vl_transacao >= 0) then
		select	vl_especie,
			vl_cheque,
			(vl_especie + vl_cheque),
			dt_deposito
		into	vl_especie_deposito_w,
			vl_cheque_deposito_w,
			vl_total_deposito_w,
			dt_deposito_w
		from	deposito
		where	nr_sequencia		= :new.nr_seq_deposito;

		if	(dt_deposito_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(183687,'NR_SEQ_DEPOSITO_P=' || :new.nr_seq_deposito);
		end if;

		select	sum(b.vl_cheque)
		into	vl_total_cheques_w
		from	deposito_cheque a,
			cheque_cr b
		where	a.nr_seq_cheque		= b.nr_seq_cheque
		and	a.nr_seq_deposito	= :new.nr_seq_deposito;

		if	(:new.vl_transacao <> vl_total_deposito_w) then
			/*O valor da transacao nao bate com o valor total do deposito !*/
			wheb_mensagem_pck.exibir_mensagem_abort(183690);
		end if;

		if	(vl_total_cheques_w <> vl_cheque_deposito_w) then
			/*O valor dos cheques vinculados nao bate com o valor do deposito !*/
			wheb_mensagem_pck.exibir_mensagem_abort(183691);
		end if;
	else
		select	vl_especie,
			vl_cheque,
			(vl_especie + vl_cheque),
			dt_deposito
		into	vl_especie_deposito_w,
			vl_cheque_deposito_w,
			vl_total_deposito_w,
			dt_deposito_w
		from	deposito
		where	nr_sequencia		= :new.nr_seq_deposito;

		if	(dt_deposito_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(183692,'NR_SEQ_DEPOSITO_P=' || :new.nr_seq_deposito);
		end if;

		select	sum(b.vl_cheque)
		into	vl_total_cheques_w
		from	deposito_cheque a,
			cheque_cr b
		where	a.nr_seq_cheque		= b.nr_seq_cheque
		and	a.nr_seq_deposito	= :new.nr_seq_deposito;

		if	(:new.vl_transacao <> vl_total_deposito_w * -1) then
			/*O valor da transacao nao bate com o valor total do deposito !*/
			wheb_mensagem_pck.exibir_mensagem_abort(183693);
		end if;

		if	(vl_total_cheques_w <> vl_cheque_deposito_w) then
			/*O valor dos cheques vinculados nao bate com o valor do deposito !*/
			wheb_mensagem_pck.exibir_mensagem_abort(183691);
		end if;
	end if;
end if;

if	(:new.nr_seq_cheque is not null) and
	(:new.nr_seq_banco is not null) and
	(nvl(:new.ie_estorno,'X') <> 'E') then

	if	(:new.ie_origem_lancamento = 'C') /* Francisco - 21/10/2009 - OS 171325 - Consistir somente se for pelo CB */ then
		if	(ie_acao_w = 7) then

			select	dt_devolucao
			into	dt_devolucao_w
			from	cheque_cr
			where	nr_seq_cheque = :new.nr_seq_cheque;

			if 	(dt_devolucao_w is not null) then
				/*O cheque nao pode ser devolvido pois ja foi devolvido ao paciente!*/
				wheb_mensagem_pck.exibir_mensagem_abort(183694);
			end if;

			if	(obter_status_cheque(:new.nr_seq_cheque) not in (2, 4, 9)) then
				wheb_mensagem_pck.exibir_mensagem_abort(183695,'NR_SEQ_CHEQUE_P='||:new.nr_seq_cheque);
			end if;
		end if;
	end if;

	select	max(a.cd_moeda)
	into	cd_moeda_w
	from	cheque_cr a
	where	a.nr_seq_cheque		= :new.nr_seq_cheque;

	select	max(a.cd_moeda),
		max(a.ds_conta)
	into	cd_moeda_unica_w,
		ds_conta_w
	from	banco_estabelecimento_v a
	where	a.nr_sequencia		= :new.nr_seq_banco;

	if	(cd_moeda_unica_w is not null) and
		(cd_moeda_w <> cd_moeda_unica_w) then

		select	max(a.ds_moeda)
		into	ds_moeda_w
		from	moeda a
		where	a.cd_moeda	= cd_moeda_unica_w;

		/* A conta bancaria cd_conta_w permite apenas movimentacoes em ds_moeda_w!*/
		wheb_mensagem_pck.exibir_mensagem_abort(301078,'CD_CONTA_W='||ds_conta_w||';DS_MOEDA_W='||ds_moeda_w);
	end if;

end if;

if	(:new.nr_seq_deposito is not null) and
	(:new.nr_seq_banco is not null) and
	(nvl(:new.ie_estorno,'X') <> 'E') then

	open c02;
	loop
	fetch c02 into
		cd_moeda_w;
	exit when c02%notfound;

		select	max(a.cd_moeda),
			max(a.ds_conta)
		into	cd_moeda_unica_w,
			ds_conta_w
		from	banco_estabelecimento_v a
		where	a.nr_sequencia		= :new.nr_seq_banco;

		if	(cd_moeda_unica_w is not null) and
			(cd_moeda_w <> cd_moeda_unica_w) then

			select	max(a.ds_moeda)
			into	ds_moeda_w
			from	moeda a
			where	a.cd_moeda	= cd_moeda_unica_w;

			/* A conta bancaria cd_conta_w permite apenas movimentacoes em ds_moeda_w!*/
			wheb_mensagem_pck.exibir_mensagem_abort(301078,'CD_CONTA_W='||ds_conta_w||';DS_MOEDA_W='||ds_moeda_w);
		end if;
	end loop;
	close c02;

end if;

if	(:new.nr_seq_banco is not null) then
	begin

	select	substr(obter_se_banco_fechado(:new.nr_seq_banco,:new.dt_transacao),1,1)
	into	ie_dia_fechado_w
	from	dual;

	if	(nvl(ie_dia_fechado_w,'N')	= 'S') then
		/* O dia dt_transacao ja foi fechado no banco!
		Consulte o fechamento na pasta Fechamento banco do Controle Bancario.
		Parametro [72] */
		wheb_mensagem_pck.exibir_mensagem_abort(231442,'DT_TRANSACAO='||:new.dt_transacao);
	end if;

	if	(ie_acao_w = 10) then
		select	count(*)
		into	cont_w
		from	convenio
		where	cd_cgc 		= :new.cd_cgc;


	if (:new.cd_convenio is not null) then
		cont_w := 1;
	end if;


		if	(cont_w = 0) then
			/*'CNPJ do convenio incorreto!'*/
			wheb_mensagem_pck.exibir_mensagem_abort(183696);
		end if;
	end if;

	/* e necessario verificar o campo nr_seq_movto_transf_bco para nao entrar em loop */
	if	(nr_seq_trans_banco_w		is not null) and
		(ie_banco_w			= 'C') and
		(:new.nr_seq_movto_transf_bco	is null) then

		if	(:new.nr_seq_movto_orig 	is not null) then

			select	max(a.nr_sequencia)
			into	nr_seq_movto_orig_transf_w
			from	movto_trans_financ a
			where	a.nr_seq_movto_transf_bco	= :new.nr_seq_movto_orig;

			ie_estorno_w	:= 'E';

		end if;

		select	movto_trans_financ_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into movto_trans_financ
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			vl_transacao,
			dt_transacao,
			nr_seq_trans_financ,
			nr_seq_banco,
			nr_seq_banco_od,
			dt_referencia_saldo,
			nr_lote_contabil,
			ie_conciliacao,
			nr_documento,
			ds_historico,
			NR_SEQ_MOVTO_TRANSF_BCO,
			nr_seq_movto_orig,
			ie_estorno)
		values	(nr_sequencia_w,
			sysdate,
			:new.nm_usuario,
			:new.vl_transacao,
			:new.dt_transacao,
			nr_seq_trans_banco_w,
			:new.nr_seq_banco_od,
			:new.nr_seq_banco,
			:new.dt_referencia_saldo,
			0,
			'N',
			:new.nr_documento,
			:new.ds_historico,
			:new.nr_sequencia,
			nr_seq_movto_orig_transf_w,
			ie_estorno_w);

		:new.NR_SEQ_MOVTO_TRANSF_BCO	:= :new.nr_sequencia;	-- Edgar 21/06/2011, OS 314641, gravar a movimentacao de origem para fins de contabilizacao
	end if;

	if	(nvl(ie_conferir_movto_cb_w,'N')	= 'S') then

		:new.dt_conferencia	:= sysdate;

	end if;

	end;
end if;

exception
when	others then
	begin

	if	(:new.nr_adiantamento		is not null) then /* Digitacao de Adiantamentos */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304553),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304554) || :new.nr_adiantamento,1,255);

	elsif	(:new.nr_adiant_pago		is not null) then /* Adiantamento Pago */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304555),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304554) || :new.nr_adiant_pago,1,255);

	elsif	(:new.nr_bordero		is not null) then /* Bordero a Pagar */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304557),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304558) || :new.nr_bordero,1,255);

	elsif	(:new.nr_bordero_rec		is not null) then /* Bordero Recebimento */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304559),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304558) || :new.nr_bordero_rec,1,255);

	elsif	(:new.nr_seq_banco_escrit	is not null) then /* Pagamento Escritural */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304560),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304561) || :new.nr_seq_banco_escrit,1,255);

	elsif	(:new.nr_seq_bordero_cheque	is not null) then /* Administracao de Cheques */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304562),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304563) || :new.nr_seq_bordero_cheque,1,255);

	elsif	(:new.nr_seq_cheque		is not null) then /* Administracao de Cheques */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304562),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304564) || :new.nr_seq_cheque,1,255);

	elsif	(:new.nr_seq_cobr_escrit	is not null) then /* Cobranca Escritural */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304565),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304566) || :new.nr_seq_cobr_escrit,1,255);

	elsif	(:new.nr_seq_conv_receb		is not null) then /* Retorno Convenio */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304567),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304568) || :new.nr_seq_conv_receb,1,255);

	elsif	(:new.nr_seq_deposito		is not null) then /* Administracao de Cheques */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304562),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304569) || :new.nr_seq_deposito,1,255);

	elsif	(:new.nr_seq_lote_cartao	is not null) then /* Administracao de Cartoes */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304573),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304574) || :new.nr_seq_lote_cartao,1,255);

	elsif	(:new.nr_seq_movto_cartao	is not null) then /* Administracao de Cartoes */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304573),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304575) || :new.nr_seq_movto_cartao,1,255);

	elsif	(:new.nr_seq_perda		is not null) then /* Tesouraria */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304576),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304577) || :new.nr_seq_perda,1,255);

	elsif	(:new.nr_seq_caixa_rec		is not null) then /* Tesouraria */

		ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304576),1,255);
		ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304568) || :new.nr_seq_caixa_rec,1,255);

	elsif	(:new.nr_seq_titulo_pagar	is not null) then

		select	max(a.nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_pagar_baixa a
		where	a.nr_titulo	= :new.nr_seq_titulo_pagar;

		if	(nvl(nr_seq_baixa_w,0)	> 0) then

			select	a.nr_seq_escrit,
				a.nr_bordero
			into	nr_seq_banco_escrit_tit_w,
				nr_bordero_tit_w
			from	titulo_pagar_baixa a
			where	a.nr_sequencia	= nr_seq_baixa_w
			and	a.nr_titulo	= :new.nr_seq_titulo_pagar;

			if	(nr_seq_banco_escrit_tit_w	is not null) then /* Pagamento Escritural */

				ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304560),1,255);
				ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304561) || nr_seq_banco_escrit_tit_w || '. ' || wheb_mensagem_pck.get_texto

(304579) || :new.nr_seq_titulo_pagar,1,255);

			elsif	(nr_bordero_tit_w		is not null) then /* Bordero a Pagar */

				ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304557),1,255);
				ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304558) || nr_bordero_tit_w || '. ' || wheb_mensagem_pck.get_texto(304579) ||

:new.nr_seq_titulo_pagar,1,255);

			else
				/* Titulos a Pagar */
				ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304580),1,255);
				ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304579) || :new.nr_seq_titulo_pagar,1,255);

			end if;

		else
			/* Titulos a Pagar */
			ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304580),1,255);
			ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304579) || :new.nr_seq_titulo_pagar,1,255);

		end if;

	elsif	(:new.nr_seq_titulo_receber	is not null) then

		select	max(a.nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_receber_liq a
		where	a.nr_titulo	= :new.nr_seq_titulo_receber;

		if	(nvl(nr_seq_baixa_w,0)	> 0) then

			select	a.nr_seq_cobranca,
				a.nr_bordero
			into	nr_seq_cobr_escrit_tit_w,
				nr_bordero_rec_tit_w
			from	titulo_receber_liq a
			where	a.nr_sequencia	= nr_seq_baixa_w
			and	a.nr_titulo	= :new.nr_seq_titulo_receber;

			if	(nr_seq_cobr_escrit_tit_w	is not null) then /* Cobranca Escritural */

				ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304565),1,255);
				ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304566) || nr_seq_cobr_escrit_tit_w || '. ' || wheb_mensagem_pck.get_texto

(304579) || :new.nr_seq_titulo_receber,1,255);

			elsif	(nr_bordero_tit_w		is not null) then /* Bordero Recebimento */

				ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304559),1,255);
				ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304558) || nr_bordero_rec_tit_w || '. ' || wheb_mensagem_pck.get_texto(304579)

|| :new.nr_seq_titulo_receber,1,255);

			else
				/* Manutencao de Titulos a Receber */
				ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304581),1,255);
				ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304579) || :new.nr_seq_titulo_receber,1,255);

			end if;

		else
			/* Manutencao de Titulos a Receber */
			ds_origem_w	:= substr(wheb_mensagem_pck.get_texto(304581),1,255);
			ds_documento_w	:= substr(wheb_mensagem_pck.get_texto(304579) || :new.nr_seq_titulo_receber,1,255);

		end if;

	end if;

	ds_historico_w	:=	substr(wheb_mensagem_pck.get_texto(304582) || trim(to_char(:new.vl_transacao,'999999990.00')) || chr(13) || chr(10) ||
				wheb_mensagem_pck.get_texto(304583) || :new.nr_seq_trans_financ || chr(13) || chr(10) ||
				wheb_mensagem_pck.get_texto(304584) || ds_origem_w || chr(13) || chr(10) || ds_documento_w,1,4000);

	ds_historico_w	:= substr(sqlerrm,1,2000) || chr(13) || chr(10) || ds_historico_w;

	gerar_banco_caixa_hist(	:new.nr_seq_banco,
				:new.nr_seq_caixa,
				ds_historico_w,
				:new.nm_usuario,
				'S',
				'S');

	/* ds_historico_w */
	wheb_mensagem_pck.exibir_mensagem_abort(244091,'DS_HISTORICO_W=' || ds_historico_w);
	end;
end;

:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.movto_trans_financ_update
  before update ON TASY.MOVTO_TRANS_FINANC   for each row
declare
  -- local variables here
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:new.dt_transacao <> :old.dt_transacao) then
		/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
		philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(:new.cd_estabelecimento),:new.dt_transacao);
	end if;
end if;

end movto_trans_financ_update;
/


CREATE OR REPLACE TRIGGER TASY.Movto_Trans_Financ_atual
before insert or update ON TASY.MOVTO_TRANS_FINANC for each row
declare

nm_tabela_w      ctb_documento.nm_tabela%type;
vl_movimento_w    ctb_documento.vl_movimento%type;
nr_seq_tab_compl_w  ctb_documento.nr_seq_doc_compl%type;
nr_seq_info_w    ctb_documento.nr_seq_info%type;
nr_documento_w    ctb_documento.nr_documento%type;
nr_doc_analitico_w  ctb_documento.nr_doc_analitico%type;

cursor c01 is
  select  a.nm_atributo,
      a.cd_tipo_lote_contab
  from  atributo_contab a
  where   a.cd_tipo_lote_contab = 10
  and   a.nm_atributo in ( 'VL_TRANSACAO', 'VL_CHEQUE_DEPOSITO', 'VL_ESPECIE_DEPOSITO')
  and    :new.nr_seq_saldo_caixa is not null
  and    :new.dt_fechamento_lote is not null
  and not exists (select 1
                 from    ctb_documento b
                 where   b.nr_documento = :new.nr_seq_saldo_caixa
                 and     b.nr_seq_doc_compl = :new.nr_sequencia
				 and	 b.nr_doc_analitico is null
                 and     b.cd_tipo_lote_contabil = a.cd_tipo_lote_contab
                 and     b.vl_movimento = :new.vl_transacao);

c01_w    c01%rowtype;

cursor c02 is
  select  a.vl_cheque * decode(:new.ie_estorno, 'E', -1, 1) vl_movimento,
      a.nr_seq_cheque
  from  cheque_cr a,
      deposito_cheque f
  where  f.nr_seq_cheque    = a.nr_seq_cheque
  and   f.nr_seq_deposito  = :new.nr_seq_deposito
  and   a.vl_cheque <> 0
  and not exists (select 1
                 from    ctb_documento b
                 where   b.nr_documento = :new.nr_seq_saldo_caixa
                 and     b.nr_seq_doc_compl = :new.nr_sequencia
				 and	 b.nr_doc_analitico = a.nr_seq_cheque
                 and     b.cd_tipo_lote_contabil = 10
                 and     b.vl_movimento = a.vl_cheque * decode(:new.ie_estorno, 'E', -1, 1));

c02_w    c02%rowtype;

cursor c04 is
  select  a.vl_especie * decode(:new.ie_estorno, 'E', -1, 1) vl_movimento,
      a.nr_sequencia
  from  deposito a
  where  a.nr_sequencia = :new.nr_seq_deposito
  and  nvl(a.vl_especie,0)  > 0
  and not exists (select 1
                 from    ctb_documento b
                 where   b.nr_documento = :new.nr_seq_saldo_caixa
                 and     b.nr_seq_doc_compl = :new.nr_sequencia
				 and	 b.nr_doc_analitico = a.nr_sequencia
                 and     b.cd_tipo_lote_contabil = 10
                 and     b.vl_movimento = a.vl_especie * decode(:new.ie_estorno, 'E', -1, 1));

c04_w    c04%rowtype;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

  if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
  end if;

  if  (inserting) and (:new.DT_TRANSACAO < to_date('01/01/1980','dd/mm/yyyy')) then
    wheb_mensagem_pck.exibir_mensagem_abort(190183,'DT_TRANSACAO=' || to_char(:new.DT_TRANSACAO, 'dd/mm/yyyy'));
    --r aise_application_error(-20011, 'Nao e possivel gerar um saldo com data de ' || to_char(:new.DT_TRANSACAO, 'dd/mm/yyyy'));
  end if;

  if  (:new.nr_seq_pagador is not null) and
    (:new.nr_seq_pagador <> :old.nr_seq_pagador)then
    begin
    select  max(a.nr_sequencia)
    into  :new.nr_seq_segurado
    from  pls_segurado  a
    where  a.nr_seq_pagador  = :new.nr_seq_pagador;
    exception
    when others then
      null;
    end;
  end if;

  open c01;
  loop
  fetch c01 into
    c01_w;
  exit when c01%notfound;
    begin

    nm_tabela_w :=   case c01_w.nm_atributo
        when 'VL_TRANSACAO' then 'CAIXA_SALDO_DIARIO'
        when 'VL_CHEQUE_DEPOSITO' then 'MOVTO_TRANS_FINANC'
        when 'VL_ESPECIE_DEPOSITO' then 'MOVTO_TRANS_FINANC'
        end;

    vl_movimento_w    := 0;
    nr_documento_w    := :new.nr_seq_saldo_caixa;
    nr_seq_tab_compl_w  := :new.nr_sequencia;
    nr_doc_analitico_w  := null;

    if  (c01_w.nm_atributo = 'VL_TRANSACAO') then
      begin
      vl_movimento_w  := :new.vl_transacao;
      nr_seq_info_w  := 4;
      end;
    elsif  (c01_w.nm_atributo = 'VL_CHEQUE_DEPOSITO') then
      begin
      vl_movimento_w  := null;

      if  (nvl(:new.nr_seq_deposito, 0) <> 0) and (nvl(:new.nr_seq_trans_financ, 0) <> 0) then
        begin

        begin

        nr_seq_info_w  := 61;
        vl_movimento_w  := null;

        open c02;
        loop
        fetch c02 into
          c02_w;
        exit when c02%notfound;
          begin
          if  (c02_w.vl_movimento <> 0) then
            ctb_concil_financeira_pck.ctb_gravar_documento  (  :new.cd_estabelecimento,
                          trunc(nvl(:new.dt_referencia_saldo,:new.dt_transacao)),
                          c01_w.cd_tipo_lote_contab,
                          :new.nr_seq_trans_financ,
                          nr_seq_info_w,
                          nr_documento_w,
                          nr_seq_tab_compl_w,
                          c02_w.nr_seq_cheque,
                          c02_w.vl_movimento,
                          nm_tabela_w,
                          c01_w.nm_atributo,
                          :new.nm_usuario);
          end if;
          end;
        end loop;
        close c02;

        exception
        when others then
          nr_seq_info_w:=  null;
          vl_movimento_w:= null;
        end;

        end;
      end if;
      end;
    elsif  (c01_w.nm_atributo = 'VL_ESPECIE_DEPOSITO') then
      begin
      if  (nvl(:new.nr_seq_deposito, 0) <> 0) and (nvl(:new.nr_seq_trans_financ, 0) <> 0) then
        begin

        begin

        nr_seq_info_w  := 62;
        vl_movimento_w := null;

        open c04;
        loop
        fetch c04 into
          c04_w;
        exit when c04%notfound;
          begin
          if  (nvl(c02_w.vl_movimento,0) <> 0) then
            ctb_concil_financeira_pck.ctb_gravar_documento  (  :new.cd_estabelecimento,
                          trunc(nvl(:new.dt_referencia_saldo,:new.dt_transacao)),
                          c01_w.cd_tipo_lote_contab,
                          :new.nr_seq_trans_financ,
                          nr_seq_info_w,
                          nr_documento_w,
                          nr_seq_tab_compl_w,
                          c04_w.nr_sequencia,
                          c04_w.vl_movimento,
                          nm_tabela_w,
                          c01_w.nm_atributo,
                          :new.nm_usuario);
          end if;
          end;
        end loop;
        close c04;

        exception
        when others then
          nr_seq_info_w  := null;
          vl_movimento_w  := null;
        end;

        end;
      end if;
      end;
    end if;


    if  (nvl(vl_movimento_w, 0) <> 0) then
      begin

      ctb_concil_financeira_pck.ctb_gravar_documento  (  nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento),
                    trunc(nvl(:new.dt_referencia_saldo,:new.dt_transacao)),
                    c01_w.cd_tipo_lote_contab,
                    :new.nr_seq_trans_financ,
                    nr_seq_info_w,
                    nr_documento_w,
                    nr_seq_tab_compl_w,
                    nr_doc_analitico_w,
                    vl_movimento_w,
                    nm_tabela_w,
                    c01_w.nm_atributo,
                    :new.nm_usuario);
      end;
    end if;

    end;
  end loop;
  close c01;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.movto_trans_financ_integr_aft
after insert or update ON TASY.MOVTO_TRANS_FINANC for each row
declare

qt_reg_w	number(10);
reg_integracao_p		gerar_int_padrao.reg_integracao;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if (:new.dt_fechamento_lote is not null) then

		select  count(*)
		   into    qt_reg_w
		   from    intpd_fila_transmissao
		   where   nr_seq_documento                = to_char(:new.nr_sequencia)
		   and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
		   and		ie_evento in ('33','29', '326');

		   if (qt_reg_w = 0) then
				gerar_int_padrao.gravar_integracao('33', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p); --Enviar cadastros financeiros  antes, cfme OS 1343067.
				gerar_int_padrao.gravar_integracao('326', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);
				gerar_int_padrao.gravar_integracao('29', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);
		   end if;

	end if;

	if(:new.nr_seq_saldo_banco is not null) then
		select  count(*)
		into qt_reg_w
		from intpd_fila_transmissao
		where nr_seq_documento = to_char(:new.nr_sequencia)
		and to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
		and ie_evento in ('326');

		if (qt_reg_w = 0) then
			gerar_int_padrao.gravar_integracao('326', :new.nr_sequencia, :new.nm_usuario, reg_integracao_p);
		end if;
	end if;


	if (inserting) then
		/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
		gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'CBT',:new.dt_transacao,'I',:new.nm_usuario);
		if (:new.nr_seq_banco is not null) then
			gravar_agend_fluxo_caixa(:new.nr_sequencia,:new.nr_seq_banco,'CBS',:new.dt_transacao,'I',:new.nm_usuario);
		end if;
	elsif (updating) then
		/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
		gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'CBT',:new.dt_transacao,'A',:new.nm_usuario);
		if (:new.nr_seq_banco is not null) then
			gravar_agend_fluxo_caixa(:new.nr_sequencia,:new.nr_seq_banco,'CBS',:new.dt_transacao,'A',:new.nm_usuario);
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.MOVTO_TRANS_FINANC ADD (
  CONSTRAINT MOVTRFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVTO_TRANS_FINANC ADD (
  CONSTRAINT MOVTRFI_BCOAPL_FK 
 FOREIGN KEY (NR_SEQ_APLICACAO) 
 REFERENCES TASY.BANCO_APLICACAO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT MOVTRFI_BANCACR_FK 
 FOREIGN KEY (NR_SEQ_BANDEIRA) 
 REFERENCES TASY.BANDEIRA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_REGLABAN_FK 
 FOREIGN KEY (NR_SEQ_REGRA_BANCO) 
 REFERENCES TASY.REGRA_LANCAMENTO_BANCO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_PESFISI_FK2 
 FOREIGN KEY (CD_PACIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MOVTRFI_BANSALD_FK 
 FOREIGN KEY (NR_SEQ_SALDO_BANCO) 
 REFERENCES TASY.BANCO_SALDO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_MOVTRFI_FK2 
 FOREIGN KEY (NR_SEQ_MOVTO_ORIG) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT MOVTRFI_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT MOVTRFI_CLASMTF_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_MTF (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CAIXA_FK3 
 FOREIGN KEY (NR_SEQ_CAIXA_OD_REJ) 
 REFERENCES TASY.CAIXA (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_MOVTRFI_FK3 
 FOREIGN KEY (NR_SEQ_MOVTO_REJEITADO) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_MOCRPAR_FK 
 FOREIGN KEY (NR_SEQ_PARC_CARTAO) 
 REFERENCES TASY.MOVTO_CARTAO_CR_PARCELA (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT MOVTRFI_TIPBACP_FK 
 FOREIGN KEY (CD_TIPO_BAIXA_CPA) 
 REFERENCES TASY.TIPO_BAIXA_CPA (CD_TIPO_BAIXA),
  CONSTRAINT MOVTRFI_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CAIXA_FK 
 FOREIGN KEY (NR_SEQ_CAIXA) 
 REFERENCES TASY.CAIXA (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_TITPAGA_FK 
 FOREIGN KEY (NR_SEQ_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT MOVTRFI_TITRECE_FK 
 FOREIGN KEY (NR_SEQ_TITULO_RECEBER) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT MOVTRFI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MOVTRFI_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MOVTRFI_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_FISCAL) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CAIXA_FK2 
 FOREIGN KEY (NR_SEQ_CAIXA_OD) 
 REFERENCES TASY.CAIXA (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_BANESTA_FK2 
 FOREIGN KEY (NR_SEQ_BANCO_OD) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT MOVTRFI_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT MOVTRFI_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT MOVTRFI_BORPAGA_FK 
 FOREIGN KEY (NR_BORDERO) 
 REFERENCES TASY.BORDERO_PAGAMENTO (NR_BORDERO),
  CONSTRAINT MOVTRFI_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE)
    ON DELETE CASCADE,
  CONSTRAINT MOVTRFI_CONRECE_FK 
 FOREIGN KEY (NR_SEQ_CONV_RECEB) 
 REFERENCES TASY.CONVENIO_RECEB (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_DEPOSIT_FK 
 FOREIGN KEY (NR_SEQ_DEPOSITO) 
 REFERENCES TASY.DEPOSITO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CAISADI_FK 
 FOREIGN KEY (NR_SEQ_SALDO_CAIXA) 
 REFERENCES TASY.CAIXA_SALDO_DIARIO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_BANESCR_FK 
 FOREIGN KEY (NR_SEQ_BANCO_ESCRIT) 
 REFERENCES TASY.BANCO_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT MOVTRFI_CONBAMO_FK 
 FOREIGN KEY (NR_SEQ_CONCIL) 
 REFERENCES TASY.CONCIL_BANC_MOVTO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANT_PAGO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO),
  CONSTRAINT MOVTRFI_BORRECE_FK 
 FOREIGN KEY (NR_BORDERO_REC) 
 REFERENCES TASY.BORDERO_RECEBIMENTO (NR_BORDERO),
  CONSTRAINT MOVTRFI_ 
 FOREIGN KEY (NR_SEQ_MOVTO_CARTAO) 
 REFERENCES TASY.MOVTO_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_MODEVCH_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.MOTIVO_DEV_CHEQUE (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_MOVTRFI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_TRANSF) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBR_ESCRIT) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_VIAREDE_FK 
 FOREIGN KEY (NR_SEQ_RELAT_DESP) 
 REFERENCES TASY.VIA_RELAT_DESP (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_BORCHEQ_FK 
 FOREIGN KEY (NR_SEQ_BORDERO_CHEQUE) 
 REFERENCES TASY.BORDERO_CHEQUE (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GPI) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_LBCART_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CARTAO) 
 REFERENCES TASY.LOTE_BAIXA_CARTAO_CR (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT MOVTRFI_PRDACRE_FK 
 FOREIGN KEY (NR_SEQ_PERDA) 
 REFERENCES TASY.PERDA_CONTAS_RECEBER (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_CHEQUE_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE_CP) 
 REFERENCES TASY.CHEQUE (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT MOVTRFI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MOVTRFI_TIPRECE_FK 
 FOREIGN KEY (CD_TIPO_RECEBIMENTO) 
 REFERENCES TASY.TIPO_RECEBIMENTO (CD_TIPO_RECEBIMENTO));

GRANT SELECT ON TASY.MOVTO_TRANS_FINANC TO NIVEL_1;

