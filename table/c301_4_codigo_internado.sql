ALTER TABLE TASY.C301_4_CODIGO_INTERNADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.C301_4_CODIGO_INTERNADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.C301_4_CODIGO_INTERNADO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_INTERNADO             VARCHAR2(5 BYTE)     NOT NULL,
  DS_INTERNADO             VARCHAR2(2000 BYTE)  NOT NULL,
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE,
  CD_COMPLETO              VARCHAR2(8 BYTE),
  NR_STATUS                NUMBER(3),
  CD_OPS_1                 VARCHAR2(255 BYTE),
  CD_OPS_2                 VARCHAR2(255 BYTE),
  CD_OPS_3                 VARCHAR2(255 BYTE),
  DS_NOME_CURTO            VARCHAR2(255 BYTE),
  DT_ATUAL_CARGA           DATE,
  CD_REFERENCIA            VARCHAR2(20 BYTE),
  CD_NUB_ID                VARCHAR2(20 BYTE),
  NR_SEQ_TIPO_COBRANCA     NUMBER(10),
  NR_SEQ_SUBTIPO_COBRANCA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.C301COI_C301SCO_FK_I ON TASY.C301_4_CODIGO_INTERNADO
(NR_SEQ_SUBTIPO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.C301COI_C301TCO_FK_I ON TASY.C301_4_CODIGO_INTERNADO
(NR_SEQ_TIPO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.C301COI_PK ON TASY.C301_4_CODIGO_INTERNADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.C301_4_CODIGO_INTERNADO ADD (
  CONSTRAINT C301COI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.C301_4_CODIGO_INTERNADO ADD (
  CONSTRAINT C301COI_C301SCO_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO_COBRANCA) 
 REFERENCES TASY.C301_4_SUBTIPO_COBRANCA (NR_SEQUENCIA),
  CONSTRAINT C301COI_C301TCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COBRANCA) 
 REFERENCES TASY.C301_4_TIPO_COBRANCA (NR_SEQUENCIA));

GRANT SELECT ON TASY.C301_4_CODIGO_INTERNADO TO NIVEL_1;

