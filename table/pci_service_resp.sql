ALTER TABLE TASY.PCI_SERVICE_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCI_SERVICE_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCI_SERVICE_RESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_VOUCHER       NUMBER(10),
  NR_SERVICE           VARCHAR2(4 BYTE),
  CD_ERROR             NUMBER(4),
  CD_ERROR_LEVEL       VARCHAR2(1 BYTE),
  NR_ACCOUNT           VARCHAR2(30 BYTE),
  CD_ASSESSMENT        VARCHAR2(10 BYTE),
  VL_BENEFIT_AMOUNT    NUMBER(9,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PCI_SER_RE_PK ON TASY.PCI_SERVICE_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PCI_SERVICE_RESP ADD (
  CONSTRAINT PCI_SER_RE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PCI_SERVICE_RESP TO NIVEL_1;

