ALTER TABLE TASY.SUBTIPO_EPISODIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUBTIPO_EPISODIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUBTIPO_EPISODIO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_SUBTIPO            VARCHAR2(255 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_EPISODIO  NUMBER(6),
  IE_TIPO_PADRAO        VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_ADMISSAO  NUMBER(10),
  IE_GERAR_PENDENCIA    VARCHAR2(1 BYTE),
  IE_MOSTAR_ASV_TEAM    VARCHAR2(1 BYTE),
  IE_PERMITE_CASE       VARCHAR2(1 BYTE),
  IE_PERMITE_INTERNAR   VARCHAR2(1 BYTE),
  IE_PERMITE_POS_ALTA   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CASUEP_CATPEP_FK_I ON TASY.SUBTIPO_EPISODIO
(NR_SEQ_TIPO_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CASUEP_PK ON TASY.SUBTIPO_EPISODIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CASUEP_TIPADMFA_FK_I ON TASY.SUBTIPO_EPISODIO
(NR_SEQ_TIPO_ADMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUBTIPO_EPISODIO_BEFINS
before insert ON TASY.SUBTIPO_EPISODIO for each row
declare
qt_reg_w	number(10);

pragma autonomous_transaction;

begin

begin
select	1
into	qt_reg_w
from	subtipo_episodio
where	nr_seq_tipo_episodio	= :new.nr_seq_tipo_episodio
and	nr_seq_tipo_admissao	= :new.nr_seq_tipo_admissao
and	nr_seq_tipo_admissao is not null
and	nr_sequencia	<> :new.nr_sequencia
and	rownum = 1;
exception
when others then
	qt_reg_w := 0;
end;

if	(qt_reg_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1077039); --Este tipo de visita/admiss�o j� est� relacionado a reste tipo de epis�dio.
end if;

end;
/


ALTER TABLE TASY.SUBTIPO_EPISODIO ADD (
  CONSTRAINT CASUEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUBTIPO_EPISODIO ADD (
  CONSTRAINT CASUEP_TIPADMFA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ADMISSAO) 
 REFERENCES TASY.TIPO_ADMISSAO_FAT (NR_SEQUENCIA),
  CONSTRAINT CASUEP_CATPEP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EPISODIO) 
 REFERENCES TASY.TIPO_EPISODIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUBTIPO_EPISODIO TO NIVEL_1;

