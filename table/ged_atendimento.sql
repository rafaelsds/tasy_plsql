ALTER TABLE TASY.GED_ATENDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GED_ATENDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GED_ATENDIMENTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_ATENDIMENTO          NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  DT_REGISTRO             DATE                  NOT NULL,
  DT_LIBERACAO            DATE,
  DS_ARQUIVO              VARCHAR2(255 BYTE)    NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  DS_TITULO               VARCHAR2(255 BYTE),
  NR_SEQ_CONSULTA         NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  NR_SEQ_TIPO_ARQUIVO     NUMBER(10),
  DT_EXPEDICAO            DATE,
  CD_ESTABELECIMENTO      NUMBER(4),
  IE_NIVEL_ATENCAO        VARCHAR2(1 BYTE),
  DS_UTC                  VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO        VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO      VARCHAR2(50 BYTE),
  NR_SEQ_AGENDA           NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA  NUMBER(10),
  NR_SEQ_AUTORIZACAO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GEDATEN_AGEPACI_FK_I ON TASY.GED_ATENDIMENTO
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEDATEN_ATCONSPEPA_FK_I ON TASY.GED_ATENDIMENTO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEDATEN_ATEPACI_FK_I ON TASY.GED_ATENDIMENTO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEDATEN_AUTCONV_FK_I ON TASY.GED_ATENDIMENTO
(NR_SEQ_AUTORIZACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEDATEN_ESTABEL_FK_I ON TASY.GED_ATENDIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEDATEN_GEDTIAR_FK_I ON TASY.GED_ATENDIMENTO
(NR_SEQ_TIPO_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GEDATEN_GEDTIAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GEDATEN_OFTCONS_FK_I ON TASY.GED_ATENDIMENTO
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GEDATEN_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GEDATEN_PESFISI_FK_I ON TASY.GED_ATENDIMENTO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GEDATEN_PESFISI_FK2_I ON TASY.GED_ATENDIMENTO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GEDATEN_PK ON TASY.GED_ATENDIMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.GED_ATENDIMENTO_ATUAL
before insert or update ON TASY.GED_ATENDIMENTO for each row
declare

qt_autorizacao_w	number(5);
qt_anexo_w	number(5);
nr_seq_anexo_agenda_w	anexo_agenda.nr_sequencia%type;

begin

if	(:new.cd_pessoa_fisica is null) and
	(:new.nr_atendimento is not null) then
	:new.cd_pessoa_fisica := obter_pessoa_atendimento(:new.nr_atendimento,'C');
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

/* OS 2007482 - - Insercao de anexo agenda da Gestao da Agenda Cirurgica - Jeferson Job (jjrsantos) */
select	count(*)
into		qt_autorizacao_w
from	autorizacao_convenio
where	nr_atendimento = :new.nr_atendimento
and		nr_seq_agenda = :new.nr_seq_agenda;

if	(qt_autorizacao_w > 0) then
	select	count(*)
	into		qt_anexo_w
	from	anexo_agenda
	where	ds_arquivo = :new.ds_arquivo
	and 		nr_seq_agenda = :new.nr_seq_agenda;

	if	( qt_anexo_w = 0 ) then
		select	anexo_agenda_seq.nextval
		into	nr_seq_anexo_agenda_w
		from	dual;

		insert into anexo_agenda (
				nr_sequencia,
				ds_arquivo,
				nr_seq_agenda,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_observacao,
				ie_situacao
			)
		values (
			nr_seq_anexo_agenda_w,
			:new.ds_arquivo,
			:new.nr_seq_agenda,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario_nrec,
			:new.ds_observacao,
			'A'
			);
	end if;
end if;
/* FIM - Insercao de anexo agenda da Gestao da Agenda Cirurgica */
end;
/


CREATE OR REPLACE TRIGGER TASY.GED_ATEND_AFTER_DEL
after delete ON TASY.GED_ATENDIMENTO for each row
declare
qt_anexo_agenda_w	 number(5);

begin

select	count(*)
into		qt_anexo_agenda_w
from	anexo_agenda
where	nr_seq_agenda = :old.nr_seq_agenda
and		ds_arquivo	= :old.ds_arquivo;

if	(qt_anexo_agenda_w > 0) then
	delete from anexo_agenda
	where	nr_seq_agenda = :old.nr_seq_agenda
	and		ds_arquivo	= :old.ds_arquivo;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_ged_atendimento_bf
before insert or update ON TASY.GED_ATENDIMENTO for each row
declare

begin

    if(:new.nr_seq_tipo_arquivo = 2 and upper(:new.ds_arquivo) not like '%.PDF%')then
         
        hsj_gerar_log('Apenas arquivos com extens�o .PDF s�o permitidos!');
       
    end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_ged_atendimento_ins
after insert or update ON TASY.GED_ATENDIMENTO for each row
declare

ds_arquivo_w varchar2(4000);
nr_atendimento_w number(10);
qt_reg_w number(10);
nr_nota_fiscal_w varchar2(255);


begin

    if :new.nr_seq_tipo_arquivo = 2 and :new.dt_liberacao is not null and :old.dt_liberacao is null then
        
        insert into hsj_workflow_agent_laudo
        (
            nr_sequencia, 
            ie_situacao,
            dt_atualizacao_nrec, 
            nm_usuario_nrec, 
            dt_atualizacao, 
            nm_usuario,
            nr_seq_doc_ged, 
            nr_atendimento, 
            cd_pessoa_fisica, 
            ie_status,
            ds_arquivo,
            cd_funcao_origem
        )
        values
        (
            hsj_workflow_agent_laudo_seq.nextval,
            'A',
            sysdate,
            obter_usuario_ativo,
            sysdate,
            obter_usuario_ativo,
            :new.nr_sequencia,
            :new.nr_atendimento,
            :new.cd_pessoa_fisica,
            'P',
            :new.ds_arquivo,
            281
        );
        
    
    end if;


    if(updating and :new.ie_situacao = 'I' and :old.ie_situacao = 'A')then
    
        select count(*)
        into qt_reg_w
        from hsj_workflow_agent_laudo w
        where nr_seq_doc_ged = :new.nr_sequencia
        and ie_situacao = 'A'
        and ie_status = 'P';
    
        if(qt_reg_w > 0)then
            update hsj_workflow_agent_laudo set ie_situacao = 'I'
            where nr_seq_doc_ged = :new.nr_sequencia;
        else
            
            select count(*)
            into qt_reg_w
            from hsj_workflow_agent_laudo w
            where nr_seq_doc_ged = :new.nr_sequencia
            and ie_situacao = 'A'
            and ie_status = 'A';    
           
            if(qt_reg_w > 0)then
                hsj_gerar_log('N�o foi poss�vel inativar o registro, existem laudos vinculados!');
            end if;
        
        end if;
    
    end if;

end;
/


ALTER TABLE TASY.GED_ATENDIMENTO ADD (
  CONSTRAINT GEDATEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GED_ATENDIMENTO ADD (
  CONSTRAINT GEDATEN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT GEDATEN_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT GEDATEN_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT GEDATEN_AUTCONV_FK 
 FOREIGN KEY (NR_SEQ_AUTORIZACAO) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA),
  CONSTRAINT GEDATEN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GEDATEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT GEDATEN_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GEDATEN_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT GEDATEN_GEDTIAR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ARQUIVO) 
 REFERENCES TASY.GED_TIPO_ARQUIVO (NR_SEQUENCIA));

GRANT SELECT ON TASY.GED_ATENDIMENTO TO NIVEL_1;

