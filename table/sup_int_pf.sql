ALTER TABLE TASY.SUP_INT_PF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_PF CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_PF
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  IE_FORMA_INTEGRACAO          VARCHAR2(15 BYTE) NOT NULL,
  DT_LIBERACAO                 DATE,
  DT_LEITURA                   DATE,
  DT_CONFIRMA_INTEGRACAO       DATE,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE) NOT NULL,
  NR_IDENTIDADE                VARCHAR2(15 BYTE),
  NM_PESSOA_FISICA             VARCHAR2(60 BYTE) NOT NULL,
  NR_TELEFONE_CELULAR          VARCHAR2(40 BYTE),
  IE_GRAU_INSTRUCAO            NUMBER(2),
  NR_CEP_CIDADE_NASC           VARCHAR2(15 BYTE),
  NR_PRONTUARIO                NUMBER(10),
  DT_ATUALIZACAO               DATE             NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_RELIGIAO                  NUMBER(5),
  NR_PIS_PASEP                 VARCHAR2(11 BYTE),
  CD_NACIONALIDADE             VARCHAR2(8 BYTE),
  IE_DEPENDENCIA_SUS           VARCHAR2(1 BYTE),
  QT_ALTURA_CM                 NUMBER(4,1),
  IE_TIPO_SANGUE               VARCHAR2(2 BYTE),
  IE_FATOR_RH                  VARCHAR2(1 BYTE),
  DT_NASCIMENTO                DATE,
  DT_OBITO                     DATE,
  IE_SEXO                      VARCHAR2(1 BYTE),
  NR_ISS                       VARCHAR2(20 BYTE),
  IE_ESTADO_CIVIL              VARCHAR2(2 BYTE),
  NR_INSS                      VARCHAR2(20 BYTE),
  NR_CPF                       VARCHAR2(11 BYTE),
  NR_CERT_NASC                 VARCHAR2(255 BYTE),
  CD_CARGO                     NUMBER(10),
  NR_CERT_CASAMENTO            VARCHAR2(255 BYTE),
  DS_CODIGO_PROF               VARCHAR2(15 BYTE),
  CD_EMPRESA                   NUMBER(4),
  IE_FUNCIONARIO               VARCHAR2(1 BYTE),
  NR_SEQ_COR_PELE              NUMBER(10),
  DS_ORGAO_EMISSOR_CI          VARCHAR2(40 BYTE),
  NR_CARTAO_NAC_SUS            NUMBER(20),
  CD_CBO_SUS                   NUMBER(6),
  CD_ATIVIDADE_SUS             NUMBER(3),
  IE_VINCULO_SUS               NUMBER(1),
  CD_SISTEMA_ANT               VARCHAR2(20 BYTE),
  IE_FREQUENTA_ESCOLA          VARCHAR2(1 BYTE),
  CD_FUNCIONARIO               VARCHAR2(15 BYTE),
  NR_PAGER_BIP                 VARCHAR2(25 BYTE),
  NR_TRANSACAO_SUS             VARCHAR2(20 BYTE),
  CD_MEDICO                    VARCHAR2(10 BYTE),
  IE_TIPO_PRONTUARIO           NUMBER(3),
  DT_EMISSAO_CI                DATE,
  NR_SEQ_CONSELHO              NUMBER(10),
  IE_FLUENCIA_PORTUGUES        VARCHAR2(5 BYTE),
  DT_ADMISSAO_HOSP             DATE,
  NR_TITULO_ELEITOR            VARCHAR2(20 BYTE),
  NR_ZONA                      VARCHAR2(5 BYTE),
  NR_SECAO                     VARCHAR2(15 BYTE),
  NR_CARTAO_ESTRANGEIRO        VARCHAR2(30 BYTE),
  NR_REG_GERAL_ESTRANG         VARCHAR2(30 BYTE),
  DT_CHEGADA_BRASIL            DATE,
  SG_EMISSORA_CI               VARCHAR2(2 BYTE),
  DT_NATURALIZACAO_PF          DATE,
  NR_SERIE_CTPS                NUMBER(10)       DEFAULT null,
  UF_EMISSORA_CTPS             VARCHAR2(15 BYTE) DEFAULT null,
  DT_EMISSAO_CTPS              DATE,
  NR_PORTARIA_NAT              VARCHAR2(16 BYTE),
  NR_SEQ_CARTORIO_NASC         NUMBER(10),
  NR_SEQ_CARTORIO_CASAMENTO    NUMBER(10),
  NR_CTPS                      NUMBER(10),
  DT_EMISSAO_CERT_NASC         DATE,
  DT_EMISSAO_CERT_CASAMENTO    DATE,
  NR_LIVRO_CERT_NASC           NUMBER(10),
  NR_LIVRO_CERT_CASAMENTO      NUMBER(10),
  DT_CADASTRO_ORIGINAL         DATE,
  NR_FOLHA_CERT_NASC           NUMBER(10),
  NR_FOLHA_CERT_CASAMENTO      NUMBER(10),
  NR_SAME                      NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(2000 BYTE),
  QT_DEPENDENTE                NUMBER(2),
  NR_TRANSPLANTE               NUMBER(10),
  DT_VALIDADE_RG               DATE,
  DT_REVISAO                   DATE,
  DT_DEMISSAO_HOSP             DATE,
  CD_PUERICULTURA              NUMBER(6),
  UF_CONSELHO                  VARCHAR2(15 BYTE) DEFAULT null,
  CD_CNES                      VARCHAR2(20 BYTE),
  DS_APELIDO                   VARCHAR2(60 BYTE),
  NM_ABREVIADO                 VARCHAR2(80 BYTE),
  IE_ENDERECO_CORRESPONDENCIA  NUMBER(2),
  NR_CCM                       NUMBER(10),
  DT_FIM_EXPERIENCIA           DATE,
  QT_PESO_NASC                 NUMBER(6,3),
  DT_VALIDADE_CONSELHO         DATE,
  DS_PROFISSAO                 VARCHAR2(255 BYTE),
  DS_EMPRESA_PF                VARCHAR2(255 BYTE),
  NR_PASSAPORTE                VARCHAR2(255 BYTE),
  NR_CNH                       VARCHAR2(255 BYTE),
  NR_CERT_MILITAR              VARCHAR2(255 BYTE),
  NR_PRONT_EXT                 VARCHAR2(100 BYTE),
  IE_ESCOLARIDADE_CNS          VARCHAR2(10 BYTE),
  IE_SITUACAO_CONJ_CNS         VARCHAR2(10 BYTE),
  NR_FOLHA_CERT_DIV            NUMBER(10),
  NR_CERT_DIVORCIO             VARCHAR2(20 BYTE),
  NR_SEQ_CARTORIO_DIVORCIO     NUMBER(10),
  DT_EMISSAO_CERT_DIVORCIO     DATE,
  NR_LIVRO_CERT_DIVORCIO       NUMBER(10),
  DT_ALTA_INSTITUCIONAL        DATE,
  DT_TRANSPLANTE               DATE,
  NR_MATRICULA_NASC            VARCHAR2(32 BYTE),
  IE_DOADOR                    VARCHAR2(15 BYTE),
  DT_VENCIMENTO_CNH            DATE,
  DS_CATEGORIA_CNH             VARCHAR2(30 BYTE),
  NM_CONTATO                   VARCHAR2(60 BYTE),
  DS_ENDERECO                  VARCHAR2(100 BYTE),
  CD_CEP                       VARCHAR2(15 BYTE),
  NR_ENDERECO                  NUMBER(5),
  DS_COMPLEMENTO               VARCHAR2(40 BYTE),
  DS_MUNICIPIO                 VARCHAR2(40 BYTE),
  DS_BAIRRO                    VARCHAR2(40 BYTE),
  SG_ESTADO                    VARCHAR2(15 BYTE),
  NR_TELEFONE                  VARCHAR2(15 BYTE),
  NR_RAMAL                     NUMBER(5),
  DS_FONE_ADIC                 VARCHAR2(80 BYTE),
  DS_EMAIL                     VARCHAR2(255 BYTE),
  CD_PROFISSAO                 NUMBER(10),
  CD_EMPRESA_REFER             NUMBER(10),
  DS_SETOR_TRABALHO            VARCHAR2(30 BYTE),
  DS_HORARIO_TRABALHO          VARCHAR2(30 BYTE),
  NR_MATRICULA_TRABALHO        VARCHAR2(20 BYTE),
  CD_MUNICIPIO_IBGE            VARCHAR2(6 BYTE),
  DS_FAX                       VARCHAR2(80 BYTE),
  CD_TIPO_LOGRADOURO           VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE              VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE              VARCHAR2(3 BYTE),
  NR_DDI_FAX                   VARCHAR2(3 BYTE),
  NR_DDD_FAX                   VARCHAR2(3 BYTE),
  DS_WEBSITE                   VARCHAR2(255 BYTE),
  NM_CONTATO_PESQUISA          VARCHAR2(60 BYTE),
  IE_TIPO_COMPLEMENTO          NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINPEF_PK ON TASY.SUP_INT_PF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINPEF_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_pf_atual
before update or insert ON TASY.SUP_INT_PF for each row
declare
qt_existe_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_fisica_ww		varchar2(10);
nr_sequencia_w			number(10);
nr_sequencia_ww			number(10);
ie_tipo_complemento_w		number(2);
begin


select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a,
	sup_int_regra_pf b
where	a.nr_sequencia = b.nr_seq_integracao
and	a.ie_evento = 'PF'
and	a.ie_forma = 'R'
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

if	(qt_existe_w > 0) and
	(:new.dt_liberacao is not null) and
	(:new.dt_confirma_integracao is null) and
	(:new.ie_forma_integracao = 'R') then

	ie_tipo_complemento_w := :new.ie_tipo_complemento;

	if	(nvl(:new.ie_tipo_complemento,0) = 0) then
		ie_tipo_complemento_w := 1;
	end if;

	select	nvl(max(cd_pessoa_fisica),0)
	into	cd_pessoa_fisica_ww
	from	pessoa_fisica
	where	cd_sistema_ant = :new.cd_pessoa_fisica;

	:new.dt_leitura	:= sysdate;

	if	(cd_pessoa_fisica_ww = 0) then

		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;

		insert into pessoa_fisica(
			cd_pessoa_fisica,
			ie_tipo_pessoa,
			nr_identidade,
			nm_pessoa_fisica,
			nr_telefone_celular,
			ie_grau_instrucao,
			nr_cep_cidade_nasc,
			nr_prontuario,
			dt_atualizacao,
			nm_usuario,
			cd_religiao,
			nr_pis_pasep,
			cd_nacionalidade,
			ie_dependencia_sus,
			qt_altura_cm,
			ie_tipo_sangue,
			ie_fator_rh,
			dt_nascimento,
			dt_obito,
			ie_sexo,
			nr_iss,
			ie_estado_civil,
			nr_inss,
			nr_cpf,
			nr_cert_nasc,
			cd_cargo,
			nr_cert_casamento,
			ds_codigo_prof,
			cd_empresa,
			ie_funcionario,
			nr_seq_cor_pele,
			ds_orgao_emissor_ci,
			nr_cartao_nac_sus,
			cd_cbo_sus,
			cd_atividade_sus,
			ie_vinculo_sus,
			ie_frequenta_escola,
			cd_funcionario,
			nr_pager_bip,
			nr_transacao_sus,
			cd_medico,
			ie_tipo_prontuario,
			dt_emissao_ci,
			nr_seq_conselho,
			dt_admissao_hosp,
			ie_fluencia_portugues,
			nr_titulo_eleitor,
			nr_zona,
			nr_secao,
			nr_cartao_estrangeiro,
			nr_reg_geral_estrang,
			dt_chegada_brasil,
			sg_emissora_ci,
			dt_naturalizacao_pf,
			nr_ctps,
			nr_serie_ctps,
			uf_emissora_ctps,
			dt_emissao_ctps,
			nr_portaria_nat,
			nr_seq_cartorio_nasc,
			nr_seq_cartorio_casamento,
			dt_emissao_cert_nasc,
			dt_emissao_cert_casamento,
			nr_livro_cert_nasc,
			nr_livro_cert_casamento,
			dt_cadastro_original,
			nr_folha_cert_nasc,
			nr_folha_cert_casamento,
			nr_same,
			ds_observacao,
			qt_dependente,
			nr_transplante,
			dt_validade_rg,
			dt_revisao,
			dt_demissao_hosp,
			cd_puericultura,
			cd_cnes,
			ds_apelido,
			ie_endereco_correspondencia,
			qt_peso_nasc,
			uf_conselho,
			nm_abreviado,
			nr_ccm,
			dt_fim_experiencia,
			dt_validade_conselho,
			ds_profissao,
			ds_empresa_pf,
			nr_passaporte,
			nr_cnh,
			nr_cert_militar,
			nr_pront_ext,
			ie_escolaridade_cns,
			ie_situacao_conj_cns,
			nr_folha_cert_div,
			nr_cert_divorcio,
			nr_seq_cartorio_divorcio,
			dt_emissao_cert_divorcio,
			nr_livro_cert_divorcio,
			dt_alta_institucional,
			dt_transplante,
			nr_matricula_nasc,
			ie_doador,
			dt_vencimento_cnh,
			ds_categoria_cnh,
			cd_sistema_ant)
		values(	cd_pessoa_fisica_w,
			'2',
			:new.nr_identidade,
			:new.nm_pessoa_fisica,
			:new.nr_telefone_celular,
			:new.ie_grau_instrucao,
			:new.nr_cep_cidade_nasc,
			:new.nr_prontuario,
			sysdate,
			'INTEGR_TASY',
			:new.cd_religiao,
			:new.nr_pis_pasep,
			:new.cd_nacionalidade,
			:new.ie_dependencia_sus,
			:new.qt_altura_cm,
			:new.ie_tipo_sangue,
			:new.ie_fator_rh,
			:new.dt_nascimento,
			:new.dt_obito,
			:new.ie_sexo,
			:new.nr_iss,
			:new.ie_estado_civil,
			:new.nr_inss,
			:new.nr_cpf,
			:new.nr_cert_nasc,
			:new.cd_cargo,
			:new.nr_cert_casamento,
			:new.ds_codigo_prof,
			:new.cd_empresa,
			:new.ie_funcionario,
			:new.nr_seq_cor_pele,
			:new.ds_orgao_emissor_ci,
			:new.nr_cartao_nac_sus,
			:new.cd_cbo_sus,
			:new.cd_atividade_sus,
			:new.ie_vinculo_sus,
			:new.ie_frequenta_escola,
			:new.cd_funcionario,
			:new.nr_pager_bip,
			:new.nr_transacao_sus,
			:new.cd_medico,
			:new.ie_tipo_prontuario,
			:new.dt_emissao_ci,
			:new.nr_seq_conselho,
			:new.dt_admissao_hosp,
			:new.ie_fluencia_portugues,
			:new.nr_titulo_eleitor,
			:new.nr_zona,
			:new.nr_secao,
			:new.nr_cartao_estrangeiro,
			:new.nr_reg_geral_estrang,
			:new.dt_chegada_brasil,
			:new.sg_emissora_ci,
			:new.dt_naturalizacao_pf,
			:new.nr_ctps,
			:new.nr_serie_ctps,
			:new.uf_emissora_ctps,
			:new.dt_emissao_ctps,
			:new.nr_portaria_nat,
			:new.nr_seq_cartorio_nasc,
			:new.nr_seq_cartorio_casamento,
			:new.dt_emissao_cert_nasc,
			:new.dt_emissao_cert_casamento,
			:new.nr_livro_cert_nasc,
			:new.nr_livro_cert_casamento,
			:new.dt_cadastro_original,
			:new.nr_folha_cert_nasc,
			:new.nr_folha_cert_casamento,
			:new.nr_same,
			:new.ds_observacao,
			:new.qt_dependente,
			:new.nr_transplante,
			:new.dt_validade_rg,
			:new.dt_revisao,
			:new.dt_demissao_hosp,
			:new.cd_puericultura,
			:new.cd_cnes,
			:new.ds_apelido,
			:new.ie_endereco_correspondencia,
			:new.qt_peso_nasc,
			:new.uf_conselho,
			:new.nm_abreviado,
			:new.nr_ccm,
			:new.dt_fim_experiencia,
			:new.dt_validade_conselho,
			:new.ds_profissao,
			:new.ds_empresa_pf,
			:new.nr_passaporte,
			:new.nr_cnh,
			:new.nr_cert_militar,
			:new.nr_pront_ext,
			:new.ie_escolaridade_cns,
			:new.ie_situacao_conj_cns,
			:new.nr_folha_cert_div,
			:new.nr_cert_divorcio,
			:new.nr_seq_cartorio_divorcio,
			:new.dt_emissao_cert_divorcio,
			:new.nr_livro_cert_divorcio,
			:new.dt_alta_institucional,
			:new.dt_transplante,
			:new.nr_matricula_nasc,
			:new.ie_doador,
			:new.dt_vencimento_cnh,
			:new.ds_categoria_cnh,
			:new.cd_pessoa_fisica);

		select	nvl(max(nr_sequencia),0) +1
		into	nr_sequencia_w
		from	compl_pessoa_fisica;

		insert into compl_pessoa_fisica(
			nr_sequencia,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			nm_contato,
			ds_endereco,
			cd_cep,
			nr_endereco,
			ds_complemento,
			ds_municipio,
			ds_bairro,
			sg_estado,
			nr_telefone,
			nr_ramal,
			ds_fone_adic,
			ds_email,
			cd_profissao,
			cd_empresa_refer,
			ds_setor_trabalho,
			ds_horario_trabalho,
			nr_matricula_trabalho,
			cd_municipio_ibge,
			ds_fax,
			cd_tipo_logradouro,
			nr_ddd_telefone,
			nr_ddi_telefone,
			nr_ddi_fax,
			nr_ddd_fax,
			ds_website,
			nm_contato_pesquisa,
			ie_tipo_complemento)
		values(	nr_sequencia_w,
			cd_pessoa_fisica_w,
			sysdate,
			'INTEGR_TASY',
			:new.nm_contato,
			:new.ds_endereco,
			:new.cd_cep,
			:new.nr_endereco,
			:new.ds_complemento,
			:new.ds_municipio,
			:new.ds_bairro,
			:new.sg_estado,
			:new.nr_telefone,
			:new.nr_ramal,
			:new.ds_fone_adic,
			:new.ds_email,
			:new.cd_profissao,
			:new.cd_empresa_refer,
			:new.ds_setor_trabalho,
			:new.ds_horario_trabalho,
			:new.nr_matricula_trabalho,
			:new.cd_municipio_ibge,
			:new.ds_fax,
			:new.cd_tipo_logradouro,
			:new.nr_ddd_telefone,
			:new.nr_ddi_telefone,
			:new.nr_ddi_fax,
			:new.nr_ddd_fax,
			:new.ds_website,
			:new.nm_contato_pesquisa,
			ie_tipo_complemento_w);
	else

		update	pessoa_fisica
		set	nr_identidade			= :new.nr_identidade,
			nm_pessoa_fisica			= :new.nm_pessoa_fisica,
			nr_telefone_celular			= :new.nr_telefone_celular,
			ie_grau_instrucao			= :new.ie_grau_instrucao,
			nr_cep_cidade_nasc		= :new.nr_cep_cidade_nasc,
			nr_prontuario			= :new.nr_prontuario,
			dt_atualizacao			= sysdate,
			nm_usuario			= 'INTEGR_TASY',
			cd_religiao			= :new.cd_religiao,
			nr_pis_pasep			= :new.nr_pis_pasep,
			cd_nacionalidade			= :new.cd_nacionalidade,
			ie_dependencia_sus		= :new.ie_dependencia_sus,
			qt_altura_cm			= :new.qt_altura_cm,
			ie_tipo_sangue			= :new.ie_tipo_sangue,
			ie_fator_rh			= :new.ie_fator_rh,
			dt_nascimento			= :new.dt_nascimento,
			dt_obito				= :new.dt_obito,
			ie_sexo				= :new.ie_sexo,
			nr_iss				= :new.nr_iss,
			ie_estado_civil			= :new.ie_estado_civil,
			nr_inss				= :new.nr_inss,
			nr_cpf				= :new.nr_cpf,
			nr_cert_nasc			= :new.nr_cert_nasc,
			cd_cargo				= :new.cd_cargo,
			nr_cert_casamento			= :new.nr_cert_casamento,
			ds_codigo_prof			= :new.ds_codigo_prof,
			cd_empresa			= :new.cd_empresa,
			ie_funcionario			= :new.ie_funcionario,
			nr_seq_cor_pele			= :new.nr_seq_cor_pele,
			ds_orgao_emissor_ci		= :new.ds_orgao_emissor_ci,
			nr_cartao_nac_sus			= :new.nr_cartao_nac_sus,
			cd_cbo_sus			= :new.cd_cbo_sus,
			cd_atividade_sus			= :new.cd_atividade_sus,
			ie_vinculo_sus			= :new.ie_vinculo_sus,
			ie_frequenta_escola		= :new.ie_frequenta_escola,
			cd_funcionario			= :new.cd_funcionario,
			nr_pager_bip			= :new.nr_pager_bip,
			nr_transacao_sus			= :new.nr_transacao_sus,
			cd_medico			= :new.cd_medico,
			ie_tipo_prontuario			= :new.ie_tipo_prontuario,
			dt_emissao_ci			= :new.dt_emissao_ci,
			nr_seq_conselho			= :new.nr_seq_conselho,
			dt_admissao_hosp			= :new.dt_admissao_hosp,
			ie_fluencia_portugues		= :new.ie_fluencia_portugues,
			nr_titulo_eleitor			= :new.nr_titulo_eleitor,
			nr_zona				= :new.nr_zona,
			nr_secao				= :new.nr_secao,
			nr_cartao_estrangeiro		= :new.nr_cartao_estrangeiro,
			nr_reg_geral_estrang		= :new.nr_reg_geral_estrang,
			dt_chegada_brasil			= :new.dt_chegada_brasil,
			sg_emissora_ci			= :new.sg_emissora_ci,
			dt_naturalizacao_pf			= :new.dt_naturalizacao_pf,
			nr_ctps				= :new.nr_ctps,
			nr_serie_ctps			= :new.nr_serie_ctps,
			uf_emissora_ctps			= :new.uf_emissora_ctps,
			dt_emissao_ctps			= :new.dt_emissao_ctps,
			nr_portaria_nat			= :new.nr_portaria_nat,
			nr_seq_cartorio_nasc		= :new.nr_seq_cartorio_nasc,
			nr_seq_cartorio_casamento		= :new.nr_seq_cartorio_casamento,
			dt_emissao_cert_nasc		= :new.dt_emissao_cert_nasc,
			dt_emissao_cert_casamento		= :new.dt_emissao_cert_casamento,
			nr_livro_cert_nasc			= :new.nr_livro_cert_nasc,
			nr_livro_cert_casamento		= :new.nr_livro_cert_casamento,
			dt_cadastro_original			= :new.dt_cadastro_original,
			nr_folha_cert_nasc			= :new.nr_folha_cert_nasc,
			nr_folha_cert_casamento		= :new.nr_folha_cert_casamento,
			nr_same				= :new.nr_same,
			ds_observacao			= :new.ds_observacao,
			qt_dependente			= :new.qt_dependente,
			nr_transplante			= :new.nr_transplante,
			dt_validade_rg			= :new.dt_validade_rg,
			dt_revisao			= :new.dt_revisao,
			dt_demissao_hosp			= :new.dt_demissao_hosp,
			cd_puericultura			= :new.cd_puericultura,
			cd_cnes				= :new.cd_cnes,
			ds_apelido			= :new.ds_apelido,
			ie_endereco_correspondencia	= :new.ie_endereco_correspondencia,
			qt_peso_nasc			= :new.qt_peso_nasc,
			uf_conselho			= :new.uf_conselho,
			nm_abreviado			= :new.nm_abreviado,
			nr_ccm				= :new.nr_ccm,
			dt_fim_experiencia			= :new.dt_fim_experiencia,
			dt_validade_conselho		= :new.dt_validade_conselho,
			ds_profissao			= :new.ds_profissao,
			ds_empresa_pf			= :new.ds_empresa_pf,
			nr_passaporte			= :new.nr_passaporte,
			nr_cnh				= :new.nr_cnh,
			nr_cert_militar			= :new.nr_cert_militar,
			nr_pront_ext			= :new.nr_pront_ext,
			ie_escolaridade_cns		= :new.ie_escolaridade_cns,
			ie_situacao_conj_cns		= :new.ie_situacao_conj_cns,
			nr_folha_cert_div			= :new.nr_folha_cert_div,
			nr_cert_divorcio			= :new.nr_cert_divorcio,
			nr_seq_cartorio_divorcio		= :new.nr_seq_cartorio_divorcio,
			dt_emissao_cert_divorcio		= :new.dt_emissao_cert_divorcio,
			nr_livro_cert_divorcio		= :new.nr_livro_cert_divorcio,
			dt_alta_institucional			= :new.dt_alta_institucional,
			dt_transplante			= :new.dt_transplante,
			nr_matricula_nasc			= :new.nr_matricula_nasc,
			ie_doador				= :new.ie_doador,
			dt_vencimento_cnh			= :new.dt_vencimento_cnh,
			ds_categoria_cnh			= :new.ds_categoria_cnh
		where	cd_pessoa_fisica = cd_pessoa_fisica_ww;

		select	nvl(max(nr_sequencia),0)
		into	nr_sequencia_ww
		from	compl_pessoa_fisica
		where	ie_tipo_complemento = ie_tipo_complemento_w
		and	cd_pessoa_fisica = cd_pessoa_fisica_ww;

		if	(nr_sequencia_ww = 0) then

			select	nvl(max(nr_sequencia),0) +1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_ww;

			insert into compl_pessoa_fisica(
				nr_sequencia,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				nm_contato,
				ds_endereco,
				cd_cep,
				nr_endereco,
				ds_complemento,
				ds_municipio,
				ds_bairro,
				sg_estado,
				nr_telefone,
				nr_ramal,
				ds_fone_adic,
				ds_email,
				cd_profissao,
				cd_empresa_refer,
				ds_setor_trabalho,
				ds_horario_trabalho,
				nr_matricula_trabalho,
				cd_municipio_ibge,
				ds_fax,
				cd_tipo_logradouro,
				nr_ddd_telefone,
				nr_ddi_telefone,
				nr_ddi_fax,
				nr_ddd_fax,
				ds_website,
				nm_contato_pesquisa,
				ie_tipo_complemento)
			values(	nr_sequencia_w,
				cd_pessoa_fisica_ww,
				sysdate,
				'INTEGR_TASY',
				:new.nm_contato,
				:new.ds_endereco,
				:new.cd_cep,
				:new.nr_endereco,
				:new.ds_complemento,
				:new.ds_municipio,
				:new.ds_bairro,
				:new.sg_estado,
				:new.nr_telefone,
				:new.nr_ramal,
				:new.ds_fone_adic,
				:new.ds_email,
				:new.cd_profissao,
				:new.cd_empresa_refer,
				:new.ds_setor_trabalho,
				:new.ds_horario_trabalho,
				:new.nr_matricula_trabalho,
				:new.cd_municipio_ibge,
				:new.ds_fax,
				:new.cd_tipo_logradouro,
				:new.nr_ddd_telefone,
				:new.nr_ddi_telefone,
				:new.nr_ddi_fax,
				:new.nr_ddd_fax,
				:new.ds_website,
				:new.nm_contato_pesquisa,
				ie_tipo_complemento_w);
		else

			update	compl_pessoa_fisica
			set	dt_atualizacao		= sysdate,
				nm_usuario		= 'INTEGR_TASY',
				nm_contato		= :new.nm_contato,
				ds_endereco		= :new.ds_endereco,
				cd_cep			= :new.cd_cep,
				nr_endereco		= :new.nr_endereco,
				ds_complemento		= :new.ds_complemento,
				ds_municipio		= :new.ds_municipio,
				ds_bairro			= :new.ds_bairro,
				sg_estado		= :new.sg_estado,
				nr_telefone		= :new.nr_telefone,
				nr_ramal			= :new.nr_ramal,
				ds_fone_adic		= :new.ds_fone_adic,
				ds_email			= :new.ds_email,
				cd_profissao		= :new.cd_profissao,
				cd_empresa_refer		= :new.cd_empresa_refer,
				ds_setor_trabalho		= :new.ds_setor_trabalho,
				ds_horario_trabalho		= :new.ds_horario_trabalho,
				nr_matricula_trabalho	= :new.nr_matricula_trabalho,
				cd_municipio_ibge		= :new.cd_municipio_ibge,
				ds_fax			= :new.ds_fax,
				cd_tipo_logradouro		= :new.cd_tipo_logradouro,
				nr_ddd_telefone		= :new.nr_ddd_telefone,
				nr_ddi_telefone		= :new.nr_ddi_telefone,
				nr_ddi_fax		= :new.nr_ddi_fax,
				nr_ddd_fax		= :new.nr_ddd_fax,
				ds_website		= :new.ds_website,
				nm_contato_pesquisa	= :new.nm_contato_pesquisa
			where	nr_sequencia		= nr_sequencia_ww
			and	cd_pessoa_fisica		= cd_pessoa_fisica_ww;
		end if;
		cd_pessoa_fisica_w := cd_pessoa_fisica_ww;
	end if;
	:new.dt_confirma_integracao	:= sysdate;
	:new.cd_sistema_ant		:= cd_pessoa_fisica_w;
end if;



end;
/


CREATE OR REPLACE TRIGGER TASY.SUP_INT_PF_tp  after update ON TASY.SUP_INT_PF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_CONTATO,1,500);gravar_log_alteracao(substr(:old.NM_CONTATO,1,4000),substr(:new.NM_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NM_CONTATO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA,1,4000),substr(:new.NM_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SANGUE,1,4000),substr(:new.IE_TIPO_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SANGUE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRONT_EXT,1,4000),substr(:new.NR_PRONT_EXT,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRONT_EXT',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESCOLARIDADE_CNS,1,4000),substr(:new.IE_ESCOLARIDADE_CNS,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESCOLARIDADE_CNS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_CONJ_CNS,1,4000),substr(:new.IE_SITUACAO_CONJ_CNS,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_CONJ_CNS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_DIVORCIO,1,4000),substr(:new.NR_CERT_DIVORCIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_DIVORCIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTORIO_DIVORCIO,1,4000),substr(:new.NR_SEQ_CARTORIO_DIVORCIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTORIO_DIVORCIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LIVRO_CERT_DIVORCIO,1,4000),substr(:new.NR_LIVRO_CERT_DIVORCIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_LIVRO_CERT_DIVORCIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CERT_DIVORCIO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CERT_DIVORCIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FOLHA_CERT_DIV,1,4000),substr(:new.NR_FOLHA_CERT_DIV,1,4000),:new.nm_usuario,nr_seq_w,'NR_FOLHA_CERT_DIV',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONE_ADIC,1,4000),substr(:new.DS_FONE_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONE_ADIC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FAX,1,4000),substr(:new.DS_FAX,1,4000),:new.nm_usuario,nr_seq_w,'DS_FAX',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CEP_CIDADE_NASC,1,4000),substr(:new.NR_CEP_CIDADE_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_CEP_CIDADE_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_NASC,1,4000),substr(:new.NR_CERT_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CI,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CI,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CI',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TRANSACAO_SUS,1,4000),substr(:new.NR_TRANSACAO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_TRANSACAO_SUS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCIONARIO,1,4000),substr(:new.CD_FUNCIONARIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCIONARIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PAGER_BIP,1,4000),substr(:new.NR_PAGER_BIP,1,4000),:new.nm_usuario,nr_seq_w,'NR_PAGER_BIP',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_RG,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_RG,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_RG',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOGRADOURO,1,4000),substr(:new.CD_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOGRADOURO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTADO_CIVIL,1,4000),substr(:new.IE_ESTADO_CIVIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADO_CIVIL',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAU_INSTRUCAO,1,4000),substr(:new.IE_GRAU_INSTRUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAU_INSTRUCAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRONTUARIO,1,4000),substr(:new.NR_PRONTUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRONTUARIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_RELIGIAO,1,4000),substr(:new.CD_RELIGIAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_RELIGIAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PIS_PASEP,1,4000),substr(:new.NR_PIS_PASEP,1,4000),:new.nm_usuario,nr_seq_w,'NR_PIS_PASEP',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DEPENDENCIA_SUS,1,4000),substr(:new.IE_DEPENDENCIA_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEPENDENCIA_SUS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_OBITO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_OBITO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_OBITO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSS,1,4000),substr(:new.NR_INSS,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CODIGO_PROF,1,4000),substr(:new.DS_CODIGO_PROF,1,4000),:new.nm_usuario,nr_seq_w,'DS_CODIGO_PROF',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FUNCIONARIO,1,4000),substr(:new.IE_FUNCIONARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FUNCIONARIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ISS,1,4000),substr(:new.NR_ISS,1,4000),:new.nm_usuario,nr_seq_w,'NR_ISS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_NACIONALIDADE,1,4000),substr(:new.CD_NACIONALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_NACIONALIDADE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FATOR_RH,1,4000),substr(:new.IE_FATOR_RH,1,4000),:new.nm_usuario,nr_seq_w,'IE_FATOR_RH',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COR_PELE,1,4000),substr(:new.NR_SEQ_COR_PELE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COR_PELE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORGAO_EMISSOR_CI,1,4000),substr(:new.DS_ORGAO_EMISSOR_CI,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORGAO_EMISSOR_CI',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CARTAO_NAC_SUS,1,4000),substr(:new.NR_CARTAO_NAC_SUS,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_NAC_SUS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CBO_SUS,1,4000),substr(:new.CD_CBO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'CD_CBO_SUS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ATIVIDADE_SUS,1,4000),substr(:new.CD_ATIVIDADE_SUS,1,4000),:new.nm_usuario,nr_seq_w,'CD_ATIVIDADE_SUS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VINCULO_SUS,1,4000),substr(:new.IE_VINCULO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_VINCULO_SUS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_NASCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ALTURA_CM,1,4000),substr(:new.QT_ALTURA_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_ALTURA_CM',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FLUENCIA_PORTUGUES,1,4000),substr(:new.IE_FLUENCIA_PORTUGUES,1,4000),:new.nm_usuario,nr_seq_w,'IE_FLUENCIA_PORTUGUES',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONSELHO,1,4000),substr(:new.NR_SEQ_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONSELHO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ADMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ADMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ADMISSAO_HOSP',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CADASTRO_ORIGINAL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CADASTRO_ORIGINAL',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TITULO_ELEITOR,1,4000),substr(:new.NR_TITULO_ELEITOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TITULO_ELEITOR',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ZONA,1,4000),substr(:new.NR_ZONA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ZONA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SECAO,1,4000),substr(:new.NR_SECAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SECAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CARTAO_ESTRANGEIRO,1,4000),substr(:new.NR_CARTAO_ESTRANGEIRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CARTAO_ESTRANGEIRO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REG_GERAL_ESTRANG,1,4000),substr(:new.NR_REG_GERAL_ESTRANG,1,4000),:new.nm_usuario,nr_seq_w,'NR_REG_GERAL_ESTRANG',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CHEGADA_BRASIL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CHEGADA_BRASIL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CHEGADA_BRASIL',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRONTUARIO,1,4000),substr(:new.IE_TIPO_PRONTUARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRONTUARIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_NATURALIZACAO_PF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_NATURALIZACAO_PF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_NATURALIZACAO_PF',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_EMISSORA_CI,1,4000),substr(:new.SG_EMISSORA_CI,1,4000),:new.nm_usuario,nr_seq_w,'SG_EMISSORA_CI',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CTPS,1,4000),substr(:new.NR_CTPS,1,4000),:new.nm_usuario,nr_seq_w,'NR_CTPS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SERIE_CTPS,1,4000),substr(:new.NR_SERIE_CTPS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SERIE_CTPS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.UF_EMISSORA_CTPS,1,4000),substr(:new.UF_EMISSORA_CTPS,1,4000),:new.nm_usuario,nr_seq_w,'UF_EMISSORA_CTPS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CTPS,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CTPS,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CTPS',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DEPENDENTE,1,4000),substr(:new.QT_DEPENDENTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_DEPENDENTE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_REVISAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REVISAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REVISAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TRANSPLANTE,1,4000),substr(:new.NR_TRANSPLANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TRANSPLANTE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNES,1,4000),substr(:new.CD_CNES,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNES',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_DEMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_DEMISSAO_HOSP,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_DEMISSAO_HOSP',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PORTARIA_NAT,1,4000),substr(:new.NR_PORTARIA_NAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTARIA_NAT',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PUERICULTURA,1,4000),substr(:new.CD_PUERICULTURA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PUERICULTURA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL,1,4000),substr(:new.NR_RAMAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_RAMAL',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA_REFER,1,4000),substr(:new.CD_EMPRESA_REFER,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA_REFER',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROFISSAO,1,4000),substr(:new.CD_PROFISSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROFISSAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SAME,1,4000),substr(:new.NR_SAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SAME',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SETOR_TRABALHO,1,4000),substr(:new.DS_SETOR_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SETOR_TRABALHO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_APELIDO,1,4000),substr(:new.DS_APELIDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_APELIDO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MATRICULA_TRABALHO,1,4000),substr(:new.NR_MATRICULA_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_MATRICULA_TRABALHO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HORARIO_TRABALHO,1,4000),substr(:new.DS_HORARIO_TRABALHO,1,4000),:new.nm_usuario,nr_seq_w,'DS_HORARIO_TRABALHO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CERT_CASAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CERT_CASAMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LIVRO_CERT_NASC,1,4000),substr(:new.NR_LIVRO_CERT_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_LIVRO_CERT_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_LIVRO_CERT_CASAMENTO,1,4000),substr(:new.NR_LIVRO_CERT_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_LIVRO_CERT_CASAMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FOLHA_CERT_NASC,1,4000),substr(:new.NR_FOLHA_CERT_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_FOLHA_CERT_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FOLHA_CERT_CASAMENTO,1,4000),substr(:new.NR_FOLHA_CERT_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_FOLHA_CERT_CASAMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO_NASC,1,4000),substr(:new.QT_PESO_NASC,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.UF_CONSELHO,1,4000),substr(:new.UF_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'UF_CONSELHO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENDERECO_CORRESPONDENCIA,1,4000),substr(:new.IE_ENDERECO_CORRESPONDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENDERECO_CORRESPONDENCIA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ABREVIADO,1,4000),substr(:new.NM_ABREVIADO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ABREVIADO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FREQUENTA_ESCOLA,1,4000),substr(:new.IE_FREQUENTA_ESCOLA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FREQUENTA_ESCOLA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CCM,1,4000),substr(:new.NR_CCM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CCM',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_EXPERIENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_EXPERIENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_EXPERIENCIA',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROFISSAO,1,4000),substr(:new.DS_PROFISSAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROFISSAO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMPRESA_PF,1,4000),substr(:new.DS_EMPRESA_PF,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMPRESA_PF',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CONSELHO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CONSELHO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CONSELHO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PASSAPORTE,1,4000),substr(:new.NR_PASSAPORTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_PASSAPORTE',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_MILITAR,1,4000),substr(:new.NR_CERT_MILITAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_MILITAR',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CNH,1,4000),substr(:new.NR_CNH,1,4000),:new.nm_usuario,nr_seq_w,'NR_CNH',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERT_CASAMENTO,1,4000),substr(:new.NR_CERT_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERT_CASAMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTORIO_NASC,1,4000),substr(:new.NR_SEQ_CARTORIO_NASC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTORIO_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CARTORIO_CASAMENTO,1,4000),substr(:new.NR_SEQ_CARTORIO_CASAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CARTORIO_CASAMENTO',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO_CERT_NASC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO_CERT_NASC',ie_log_w,ds_w,'SUP_INT_PF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_INT_PF ADD (
  CONSTRAINT SUINPEF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_INT_PF TO NIVEL_1;

