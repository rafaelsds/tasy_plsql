ALTER TABLE TASY.SUS_PROCED_URG_EMERG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_PROCED_URG_EMERG CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_PROCED_URG_EMERG
(
  CD_PROCEDIMENTO   NUMBER(15)                  NOT NULL,
  IE_ORIGEM_PROCED  NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSPRUE_PK ON TASY.SUS_PROCED_URG_EMERG
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUS_PROCED_URG_EMERG ADD (
  CONSTRAINT SUSPRUE_PK
 PRIMARY KEY
 (CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_PROCED_URG_EMERG ADD (
  CONSTRAINT SUSPRUE_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUS_PROCED_URG_EMERG TO NIVEL_1;

