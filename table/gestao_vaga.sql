ALTER TABLE TASY.GESTAO_VAGA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GESTAO_VAGA CASCADE CONSTRAINTS;

CREATE TABLE TASY.GESTAO_VAGA
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE),
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_SOLICITACAO                 DATE           NOT NULL,
  IE_SOLICITACAO                 VARCHAR2(15 BYTE) NOT NULL,
  CD_CONVENIO                    NUMBER(15)     NOT NULL,
  IE_STATUS                      VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_VAGA                   VARCHAR2(15 BYTE) NOT NULL,
  DT_PREVISTA                    DATE,
  NR_ATENDIMENTO                 NUMBER(10),
  CD_TIPO_ACOMOD_ATUAL           NUMBER(15),
  CD_TIPO_ACOMOD_DESEJ           NUMBER(15),
  CD_CID_PRINCIPAL               VARCHAR2(10 BYTE),
  CD_SETOR_ATUAL                 NUMBER(15),
  CD_MEDICO                      VARCHAR2(10 BYTE),
  IE_CLINICA                     NUMBER(15),
  CD_PROCEDIMENTO                NUMBER(15),
  IE_ORIGEM_PROCED               NUMBER(10),
  NR_SEQ_MOTIVO_TRANSF           NUMBER(10),
  CD_CNPJ_SOLIC                  VARCHAR2(14 BYTE),
  NM_PF_SOLIC                    VARCHAR2(40 BYTE),
  IE_TIPO_AMBULANCIA             VARCHAR2(15 BYTE),
  CD_CNPJ_ENCAMINHADO            VARCHAR2(14 BYTE),
  IE_RESP_RECUSA                 VARCHAR2(15 BYTE),
  CD_ORIGEM_TRANSF               NUMBER(10),
  NM_RESP_TRANSF                 VARCHAR2(15 BYTE),
  CD_SETOR_DESEJADO              NUMBER(5),
  CD_UNIDADE_BASICA              VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL               VARCHAR2(10 BYTE),
  CD_CATEGORIA                   VARCHAR2(10 BYTE),
  IE_LOCAL_AGUARDA               VARCHAR2(3 BYTE),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  QT_DIA                         NUMBER(3),
  DT_ACOMODACAO                  DATE,
  DS_HD                          VARCHAR2(4000 BYTE),
  CD_RESP_RECUSA                 VARCHAR2(10 BYTE),
  DS_OBSERVACAO                  VARCHAR2(4000 BYTE),
  DS_COD_USUARIO                 VARCHAR2(30 BYTE),
  DS_COMPL                       VARCHAR2(30 BYTE),
  DT_VALIDADE                    DATE,
  NR_SEQ_AGENDA                  NUMBER(10),
  CD_SENHA                       VARCHAR2(20 BYTE),
  CD_PLANO_CONVENIO              VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_CANCEL           NUMBER(10),
  NR_SEQ_PROC_INTERNO            NUMBER(10),
  CD_TIPO_AGENDA                 NUMBER(10),
  NM_PACIENTE                    VARCHAR2(80 BYTE),
  NR_TELEFONE                    VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_CADASTRO_PF     DATE,
  NM_USUARIO_ATUAL_CADASTRO_PF   VARCHAR2(15 BYTE),
  NM_USUARIO_APROV               VARCHAR2(15 BYTE),
  DT_APROVACAO                   DATE,
  IE_ATUAL_CADASTRO              VARCHAR2(1 BYTE),
  IE_NECESSITA_ISOLAMENTO        VARCHAR2(1 BYTE),
  IE_NECESSITA_DIALISE           VARCHAR2(1 BYTE),
  DS_SENHA_ATEND                 VARCHAR2(20 BYTE),
  NM_USUARIO_REGISTRO_SENHA      VARCHAR2(15 BYTE),
  DT_REGISTRO_SENHA_ATEND        DATE,
  QT_HORAS                       NUMBER(10),
  IE_EM_ATENDIMENTO              VARCHAR2(1 BYTE),
  QT_PESO                        NUMBER(6,3),
  NR_DOC_CONVENIO                VARCHAR2(20 BYTE),
  IE_VAGA_CEDIDA                 VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_RECUSA           NUMBER(10),
  IE_REGRA                       NUMBER(5),
  IE_AUTORIZACAO                 VARCHAR2(3 BYTE),
  NR_SEQ_STATUS_PAC              NUMBER(10),
  IE_BIOBANCO                    VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE               NUMBER(5),
  IE_QUIMIOTERAPIA               VARCHAR2(1 BYTE),
  NR_SEQ_CLASSIF_MEDICO          NUMBER(10),
  CD_FUNCIONARIO                 VARCHAR2(10 BYTE),
  IE_OPME                        VARCHAR2(1 BYTE),
  QT_PROCEDIMENTO                NUMBER(5),
  NR_SEQ_TURNO                   NUMBER(10),
  NR_SEQ_MOTIVO_NEGACAO_VAGA     NUMBER(10),
  NR_DDI                         VARCHAR2(3 BYTE),
  QT_ACOMPANHANTE                NUMBER(10),
  CD_DEPARTAMENTO_ATUAL          NUMBER(10),
  CD_DEPARTAMENTO_DESEJADO       NUMBER(10),
  NR_SEQ_MOTIVO_APROV_VAGA       NUMBER(10),
  DS_JUSTIFICATIVA_NEGACAO_VAGA  VARCHAR2(255 BYTE),
  DS_JUSTIFICATIVA_APROV         VARCHAR2(255 BYTE),
  CD_ESTAB_REFERENCIA            NUMBER(10),
  IE_TIPO_ISOLAMENTO             VARCHAR2(15 BYTE),
  IE_AGENDAMENTO_CIRURGICO       VARCHAR2(15 BYTE),
  IE_SOLIC_UTI_ECI               VARCHAR2(1 BYTE),
  NR_SEQ_LINHA_CUIDADO           NUMBER(10),
  NR_SEQ_INTERNO                 NUMBER(10),
  NR_SEQ_PRIORIDADE              NUMBER(10),
  IE_PRIVATE_ROOM                VARCHAR2(5 BYTE),
  CD_SPECIALTY                   NUMBER(5),
  DS_RISKS                       VARCHAR2(2048 BYTE),
  DT_ADMISSION                   DATE,
  IE_EXTERNAL_ASSISTANCE         VARCHAR2(4 BYTE),
  IE_TIPO_ATENDIMENTO            NUMBER(3),
  NR_SEQ_CLASSIF_ATEND           NUMBER(10),
  IE_URGENTE                     VARCHAR2(1 BYTE),
  DT_ULT_DATA_RESERVA            DATE,
  DS_GIRO_SALA                   VARCHAR2(10 BYTE),
  CD_PROCEDENCIA                 NUMBER(5),
  CD_HOSPITALIZATION_ROUTE       VARCHAR2(15 BYTE),
  CD_PRE_HOSP_HOME               VARCHAR2(15 BYTE),
  SI_CONS_MED_GUID               VARCHAR2(1 BYTE),
  DS_SHIFT                       NUMBER(3),
  CD_EVOLUCAO                    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GESVAGA_ATEPACI_FK_I ON TASY.GESTAO_VAGA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_CATCONV_FK_I ON TASY.GESTAO_VAGA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_CIDDOEN_FK_I ON TASY.GESTAO_VAGA
(CD_CID_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_CONVENI_FK_I ON TASY.GESTAO_VAGA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_DEPMED_FK_I ON TASY.GESTAO_VAGA
(CD_DEPARTAMENTO_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_DEPMED_FK2_I ON TASY.GESTAO_VAGA
(CD_DEPARTAMENTO_DESEJADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_ESTABEL_FK_I ON TASY.GESTAO_VAGA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_EVOPACI_FK_I ON TASY.GESTAO_VAGA
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_GEVAMCA_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_GEVAMCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_GVMORET_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_MOTIVO_RECUSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_GVMORET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_GVSTPAC_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_STATUS_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_GVSTPAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_I1 ON TASY.GESTAO_VAGA
(IE_STATUS, CD_ESTABELECIMENTO, NR_ATENDIMENTO, CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_I2 ON TASY.GESTAO_VAGA
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_I3 ON TASY.GESTAO_VAGA
(DT_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_I4 ON TASY.GESTAO_VAGA
(CD_ESTABELECIMENTO, DT_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_I5 ON TASY.GESTAO_VAGA
(CD_ESTABELECIMENTO, DT_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_I6 ON TASY.GESTAO_VAGA
(NR_ATENDIMENTO, CD_SETOR_DESEJADO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL, DT_ACOMODACAO, 
1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_LINCUIDPAC_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_LINHA_CUIDADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_MEDCLAT_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_CLASSIF_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_MOTTRPA_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_MOTIVO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_ORISOTR_FK_I ON TASY.GESTAO_VAGA
(CD_ORIGEM_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_ORISOTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_PESCLAS_FK_I ON TASY.GESTAO_VAGA
(CD_RESP_RECUSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_PESFISI_FK_I ON TASY.GESTAO_VAGA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_PESFISI_FK2_I ON TASY.GESTAO_VAGA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_PESFISI_FK3_I ON TASY.GESTAO_VAGA
(CD_FUNCIONARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_PESJURI_FK_I ON TASY.GESTAO_VAGA
(CD_CNPJ_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_PESJURI_FK2_I ON TASY.GESTAO_VAGA
(CD_CNPJ_ENCAMINHADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_PESJURI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GESVAGA_PK ON TASY.GESTAO_VAGA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_PRIREG_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_PROCEDI_FK_I ON TASY.GESTAO_VAGA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_PROINTE_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_SETATEN_FK_I ON TASY.GESTAO_VAGA
(CD_SETOR_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_SETATEN_FK1_I ON TASY.GESTAO_VAGA
(CD_SETOR_DESEJADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_SOLVAMOTAP_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_MOTIVO_APROV_VAGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_SOLVAMOTNE_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_MOTIVO_NEGACAO_VAGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GESVAGA_TIPACOM_FK_I ON TASY.GESTAO_VAGA
(CD_TIPO_ACOMOD_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_TIPACOM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_TIPACOM_FK2_I ON TASY.GESTAO_VAGA
(CD_TIPO_ACOMOD_DESEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GESVAGA_TIPACOM_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.GESVAGA_UNIATEN_FK_I ON TASY.GESTAO_VAGA
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.gestao_vaga_befinsert
BEFORE INSERT or UPDATE ON TASY.GESTAO_VAGA FOR EACH ROW
declare

cd_area_proc_w	    number(15);
cd_especial_proc_w  number(15);
cd_grupo_proc_w		number(15);

begin

if	(:new.cd_procedimento > 0) then
	begin
	select	cd_area_procedimento,
			cd_especialidade,
			cd_grupo_proc
	into	cd_area_proc_w,
			cd_especial_proc_w,
			cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento	= :new.cd_procedimento
	and		ie_origem_proced	= :new.ie_origem_proced;
	exception
		when no_data_found then
			--' Nao foi encontrada estrutura para este procedimento '||cd_procedimento_p||'! #@#@' || ie_origem_proced_p);
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(192951, 'CD_PROCEDIMENTO_P=' || :new.cd_procedimento ||
									';IE_ORIGEM_PROCED_P=' || :new.ie_origem_proced);
	end;

end if;

if	(:new.ie_status = 'R' and :old.ie_status <> :new.ie_status) then
  	 :new.DT_ULT_DATA_RESERVA := sysdate;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.gestao_vaga_delete
AFTER DELETE ON TASY.GESTAO_VAGA FOR EACH ROW
declare

nr_seq_interno_w	number(10);
qt_reg_w		number(1);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(nr_seq_interno)
into	nr_seq_interno_w
from	unidade_atendimento
where	cd_unidade_basica	= :old.cd_unidade_basica
and	cd_unidade_compl	= :old.cd_unidade_compl
and	cd_setor_atendimento	= :old.cd_setor_desejado;

if	(nr_seq_interno_w is not null) then
	update	agenda_paciente
	set	nr_seq_interno 	= null
	where	nr_sequencia 	= :old.nr_seq_agenda
	and	nr_seq_interno 	= nr_seq_interno_w;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.gestao_vaga_befupdate
BEFORE UPDATE ON TASY.GESTAO_VAGA FOR EACH ROW
declare

nr_seq_proc_adic_w		number(10);
cd_proc_adic_w          	number(15);
ie_origem_proc_adic_w   	number(10);
ie_autorizacao_proc_adic_w	varchar2(3);
ie_autorizacao_w		varchar2(3);
ie_verifica_aut_conv_w		varchar2(1);
ie_regra_w			number(5);
ie_regra_proc_adic_w		number(5);
nr_seq_proc_interno_w	number(10);
Cursor C01 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno
	from	gestao_vaga_proc
	where	nr_seq_gestao = :new.nr_sequencia;
BEGIN

begin
if	(not wheb_usuario_pck.get_nm_usuario is null) then
	:new.nm_usuario := wheb_usuario_pck.get_nm_usuario;
end if;
exception
when others then
	null;
end;


obter_param_usuario(1002, 100, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_verifica_aut_conv_w);

if	(:new.DS_SENHA_ATEND is null) and
	(:old.DS_SENHA_ATEND is not null) then
	:new.DT_REGISTRO_SENHA_ATEND	:= null;
end if;


if 	((nvl(ie_verifica_aut_conv_w,'N') = 'S') and
	(((:new.cd_convenio <> :old.cd_convenio) or
	  (:old.cd_convenio is null)) or
	((:new.cd_plano_convenio <> :old.cd_plano_convenio) or
	 (:old.cd_plano_convenio is null)) or
	((:new.cd_categoria <> :old.cd_categoria) or
	 (:old.cd_categoria is null)))) then


	gv_verifica_autorizacao_conv(	:new.cd_convenio,
					:new.cd_procedimento,
					:new.ie_origem_proced,
					:new.cd_plano_convenio,
					:new.cd_categoria,
					:new.nr_seq_proc_interno,
					:new.cd_estabelecimento,
					Obter_Sexo_PF(:new.cd_pessoa_fisica,'C'),
					:new.nr_sequencia,
					ie_autorizacao_w,
					ie_regra_w);
	:new.ie_autorizacao := ie_autorizacao_w;
	:new.ie_regra := ie_regra_w;

	open C01;
	loop
	fetch C01 into
		nr_seq_proc_adic_w,
		cd_proc_adic_w,
		ie_origem_proc_adic_w,
		nr_seq_proc_interno_w;
	exit when C01%notfound;
		begin
		gv_verifica_autorizacao_conv(	:new.cd_convenio,
						cd_proc_adic_w,
						ie_origem_proc_adic_w,
						:new.cd_plano_convenio,
						:new.cd_categoria,
						nr_seq_proc_interno_w,
						:new.cd_estabelecimento,
						Obter_Sexo_PF(:new.cd_pessoa_fisica,'C'),
						null,
						ie_autorizacao_proc_adic_w,
						ie_regra_proc_adic_w);


		update	gestao_vaga_proc
		set	ie_autorizacao = ie_autorizacao_proc_adic_w,
			ie_regra = ie_regra_proc_adic_w,
			dt_atualizacao = sysdate,
			nm_usuario = :new.nm_usuario
		where	nr_sequencia = nr_seq_proc_adic_w;

		end;
	end loop;
	close C01;

end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.GESTAO_VAGA_BEFORE_INSERT
BEFORE INSERT
ON TASY.GESTAO_VAGA 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE

BEGIN
    
    IF(:NEW.DS_HD IS NOT NULL) THEN
        :NEW.DS_OBSERVACAO := :NEW.DS_HD;
        
    END IF;
 
END GESTAO_VAGA_BEFORE_INSERT;
/


CREATE OR REPLACE TRIGGER TASY.GESTAO_VAGA_AFTER_INSERT
after insert ON TASY.GESTAO_VAGA for each row
declare

ds_param_integ_hl7_w		varchar2(1000);
gestao_vaga_r 			gestao_vaga%rowtype;
ie_save_insurance_holder_w 	varchar2(1);

begin

ie_save_insurance_holder_w := obter_dados_param_atend(wheb_usuario_pck.get_cd_estabelecimento, 'SI');

if 	(ie_save_insurance_holder_w = 'S') and
	(:new.cd_pessoa_fisica is not null) and
	(:new.cd_convenio is not null) and
	((nvl(:new.cd_convenio, 0) <> nvl(:old.cd_convenio, 0)) or
	 (nvl(:new.cd_pessoa_fisica, '0') <> nvl(:old.cd_pessoa_fisica, '0')) or
	 (nvl(:new.cd_categoria, 0) <> nvl(:old.cd_categoria, 0)) or
	 (nvl(:new.ds_cod_usuario, '0') <> nvl(:old.ds_cod_usuario, '0'))) then
	insere_atualiza_titular_conv(
				:new.nm_usuario,
				:new.cd_convenio,
				:new.cd_categoria,
				:new.cd_pessoa_fisica,
				:new.cd_plano_convenio,
				null,
				:new.dt_validade,
				:new.dt_validade,
				null,
				:new.ds_cod_usuario,
				null,
				'N',
				'2');
end if;


if (obter_funcao_ativa <> '1002') then

	gestao_vaga_r.nr_sequencia := :new.nr_sequencia;
	gestao_vaga_r.cd_convenio := :new.cd_convenio;
	gestao_vaga_r.ie_tipo_vaga := :new.ie_tipo_vaga;
	gestao_vaga_r.ie_status := :new.ie_status;
	gestao_vaga_r.ie_solicitacao := :new.ie_solicitacao;
	gestao_vaga_r.cd_setor_atual := :new.cd_setor_atual;
	gestao_vaga_r.cd_setor_desejado := :new.cd_setor_desejado;
	gestao_vaga_r.cd_tipo_acomod_desej := :new.cd_tipo_acomod_desej;
	gestao_vaga_r.nr_seq_status_pac := :new.nr_seq_status_pac;
	gestao_vaga_r.nr_atendimento := :new.nr_atendimento;
	gestao_vaga_r.dt_prevista	:=	:new.dt_prevista;
	gestao_vaga_r.nm_paciente	:= :new.nm_paciente;

	gerar_evento_gestao_vaga_auto(:new.cd_pessoa_fisica,:new.nm_usuario,:new.cd_estabelecimento,:new.nr_sequencia,:new.nm_paciente,'VA',:new.ie_status, gestao_vaga_r);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.update_hospitalization_detail
AFTER UPDATE ON TASY.GESTAO_VAGA FOR EACH ROW
declare

nr_sequencia_w  atendimento_paciente_inf.NR_SEQUENCIA%Type;
qt_registro_w  number;

begin

if (:old.nr_atendimento is null AND :new.nr_atendimento is not null) then

  select  count(nr_sequencia)
  into  qt_registro_w
  from  atendimento_paciente_inf
  where nr_atendimento = :new.nr_atendimento;

  if (qt_registro_w > 0) then

    select  nr_sequencia
    into  nr_sequencia_w
    from  atendimento_paciente_inf
    where nr_atendimento = :new.nr_atendimento;

    update atendimento_paciente_inf
      set
      cd_hospitalization_route = :new.cd_hospitalization_route,
      cd_pre_hosp_home = :new.cd_pre_hosp_home
    where nr_sequencia = nr_sequencia_w;

  else

    insert into atendimento_paciente_inf(
      nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      nr_atendimento,
      cd_hospitalization_route,
      cd_pre_hosp_home  )
    values  (
      atendimento_paciente_inf_seq.nextval,
      SYSDATE,
      wheb_usuario_pck.get_nm_usuario,
      :new.nr_atendimento,
      :new.cd_hospitalization_route,
      :new.cd_pre_hosp_home );

  end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.gestao_vaga_bf_delete
BEFORE DELETE ON TASY.GESTAO_VAGA FOR EACH ROW
declare

nr_seq_interno_w	number(10);
qt_reg_w		number(1);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete	aval_gestao_vaga_result a
where	nr_seq_avaliacao in (	select	b.nr_sequencia
				from	avaliacao_gestao_vaga b
				where	b.nr_seq_gestao_vaga = :old.nr_sequencia
				and	dt_liberacao is null);

delete	avaliacao_gestao_vaga
where	nr_seq_gestao_vaga = :old.nr_sequencia
and	dt_liberacao is null;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.GESTAO_VAGA_ATUAL
before insert or update ON TASY.GESTAO_VAGA for each row
declare

begin

if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then
  consistir_classif_med_retag(:new.nr_seq_classif_medico, :new.cd_medico, :new.nm_usuario,:new.cd_estabelecimento);

  if (:new.nr_seq_agenda <> :old.nr_seq_agenda and :new.nr_seq_agenda is not null) or
     (:new.cd_tipo_agenda <> :old.cd_tipo_agenda and :new.cd_tipo_agenda is not null) then
	gravar_log_tasy(8415,'nr_seq_agenda = ' || :new.nr_seq_agenda || ' cd_tipo_agenda= ' || :new.cd_tipo_agenda || ' callstack= ' || dbms_utility.format_call_stack, :new.nm_usuario);
  end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.GESTAO_VAGA_tp  after update ON TASY.GESTAO_VAGA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_DOC_CONVENIO,1,4000),substr(:new.NR_DOC_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DOC_CONVENIO',ie_log_w,ds_w,'GESTAO_VAGA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.gestao_vaga_update
AFTER UPDATE ON TASY.GESTAO_VAGA FOR EACH ROW
declare

cd_paciente_reserva_w	varchar2(10);
ie_status_unidade_w	varchar2(3);
ie_atualiza_status_w 	varchar2(1);
qt_reg_w		number(1);
ie_atualiza_agecir_w	varchar2(1);
ds_log_w				varchar2(2000);
nm_pac_reserva_w	varchar2(255);
gestao_vaga_r gestao_vaga%rowtype;
BEGIN

select	nvl(max(Obter_Valor_Param_Usuario(1002, 39, Obter_Perfil_Ativo, :NEW.nm_usuario, 0)), 'S')
into	ie_atualiza_status_w
from 	dual;

select	nvl(max(Obter_Valor_Param_Usuario(1002, 84, Obter_Perfil_Ativo, :NEW.nm_usuario, 0)), 'S')
into	ie_atualiza_agecir_w
from 	dual;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.ie_status = 'D') or (:new.ie_status = 'N') or (:new.ie_status = 'F') then
	begin

	begin
	select	nvl(max(ie_status_unidade),'L'),
		max(cd_paciente_reserva),
		max(nm_pac_reserva)
	into	ie_status_unidade_w,
		cd_paciente_reserva_w,
		nm_pac_reserva_w
	from	unidade_atendimento
	where	cd_unidade_basica	= :new.cd_unidade_basica
	and	cd_unidade_compl	= :new.cd_unidade_compl
	and	cd_setor_atendimento	= :new.cd_setor_desejado;
	exception
	when others then
		ie_status_unidade_w 	:= 'L';
		cd_paciente_reserva_w 	:= null;
		nm_pac_reserva_w	:= null;
	end;

	if	((:new.cd_pessoa_fisica = cd_paciente_reserva_w) or
		 (:new.nm_paciente = nm_pac_reserva_w)) then
		begin
		if	(ie_status_unidade_w = 'R')	then
			update	unidade_atendimento
			set	ie_status_unidade	= 'L',
				cd_paciente_reserva	= null,
				ds_observacao		= null,
				nm_usuario_reserva	= null,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate,
				nm_pac_reserva		= null
			where	cd_unidade_basica	= :new.cd_unidade_basica
			and	cd_unidade_compl	= :new.cd_unidade_compl
			and	cd_setor_atendimento	= :new.cd_setor_desejado;
		else
			update	unidade_atendimento
			set	cd_paciente_reserva	= null,
				ds_observacao		= null,
				nm_usuario_reserva	= null,
				nm_usuario		= :new.nm_usuario,
				dt_atualizacao		= sysdate,
				nm_pac_reserva		= null
			where	cd_unidade_basica	= :new.cd_unidade_basica
			and	cd_unidade_compl	= :new.cd_unidade_compl
			and	cd_setor_atendimento	= :new.cd_setor_desejado;
		end if;
		end;
	end if;

	end;
end if;


if (:new.ie_status <> :old.ie_status) then

	insert into gestao_vaga_hist_status
		(nr_sequencia,
		 dt_atualizacao,
	     	 nm_usuario,
		 dt_atualizacao_nrec,
	  	 nm_usuario_nrec,
		 nr_seq_gestao_vaga,
		 ie_status,
		 nm_usuario_resp_transf)
	values
		(gestao_vaga_hist_status_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
	  	:new.nm_usuario,
		:new.nr_sequencia,
		:new.ie_status,
		:new.nm_resp_transf
		);

	begin
	ds_log_w := substr(	' '||wheb_mensagem_pck.get_texto(795194)||' : '|| :new.nr_sequencia || chr(13) ||chr(10)||
						' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

	insert into log_mov(
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values (
		sysdate,
		:new.nm_usuario,
		88911,
		ds_log_w);

	exception
		when others then
		ds_log_w	:=	'';
	end;

end if;

if	(ie_atualiza_status_w = 'S') and
	(:new.nr_seq_agenda <> 0) and
	(:new.ie_status <> :old.ie_status) and
	((:new.ie_status = 'F') or (:new.ie_status = 'P'))  then

	update	agenda_paciente
	set	ie_status_agenda = 'N'
	where	nr_sequencia = :new.nr_seq_agenda
	and	ie_status_agenda not in ('C','E');

end if;

if 	(:new.cd_pessoa_fisica is not null) and
	(:old.cd_pessoa_fisica is null) and
	(ie_atualiza_agecir_w = 'S') then
	update	agenda_paciente
	set	ie_status_agenda = 'N'
	where	nr_sequencia = :new.nr_seq_agenda;
end if;

if 	(nvl(:new.cd_pessoa_fisica, 0) <> nvl(:old.cd_pessoa_fisica, 0)) then

	update	autorizacao_convenio
	set	cd_pessoa_fisica = :new.cd_pessoa_fisica
	where	nr_seq_gestao = :new.nr_sequencia;

end if;

if (:old.ie_status <> :new.ie_status) then

	gestao_vaga_r.nr_sequencia := :new.nr_sequencia;
	gestao_vaga_r.cd_convenio := :new.cd_convenio;
	gestao_vaga_r.ie_tipo_vaga := :new.ie_tipo_vaga;
	gestao_vaga_r.ie_status := :new.ie_status;
	gestao_vaga_r.ie_solicitacao := :new.ie_solicitacao;
	gestao_vaga_r.cd_setor_atual := :new.cd_setor_atual;
	gestao_vaga_r.cd_setor_desejado := :new.cd_setor_desejado;
	gestao_vaga_r.cd_tipo_acomod_desej := :new.cd_tipo_acomod_desej;
	gestao_vaga_r.nr_seq_status_pac := :new.nr_seq_status_pac;
	gestao_vaga_r.nr_atendimento := :new.nr_atendimento;
	gestao_vaga_r.dt_prevista	:=	:new.dt_prevista;
	gestao_vaga_r.nm_paciente	:= :new.nm_paciente;

	gerar_evento_gestao_vaga_auto(:new.cd_pessoa_fisica,:new.nm_usuario,:new.cd_estabelecimento,:new.nr_sequencia,:new.nm_paciente,'AAV', :new.ie_status, gestao_vaga_r);
end if;

if((:new.ie_status ='F' and :new.ie_solicitacao='I') and
  (:old.ie_status <> :new.ie_status or :old.ie_solicitacao <> :new.ie_solicitacao)) then

    execute_bifrost_integration(251, '{"recordId" : "' || :new.nr_sequencia|| '"' || '}');

	call_interface_file(932, 'carestream_ris_japan_l10n_pck.patient_admission_info(' || :new.nr_sequencia || ',''3H'' , 1, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento || ',''N'');', :new.nm_usuario);
	call_interface_file(948, 'tosho_pck.patient_admission_info(' || :new.nr_sequencia || ',''3H'' , 1, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento ||  ', ''N'' );' , :new.nm_usuario);
end if;

if((:new.ie_status in ('C','N', 'A') and :old.ie_status in ('F') and :new.ie_solicitacao='I') and
  (:old.ie_status <> :new.ie_status or :old.ie_solicitacao <> :new.ie_solicitacao)) then

	call_interface_file(932, 'carestream_ris_japan_l10n_pck.patient_admission_info(' || :new.nr_sequencia || ',''3I'' , 3, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento || ',''N'');' , :new.nm_usuario);
	call_interface_file(948, 'tosho_pck.patient_admission_info(' || :new.nr_sequencia || ',''3I'' , 3, ''' || :new.nm_usuario || ''' , 0, null, ' || :new.cd_estabelecimento ||  ', ''N'' );' , :new.nm_usuario);

end if;

<<Final>>
qt_reg_w := 0;
END;
/


ALTER TABLE TASY.GESTAO_VAGA ADD (
  CONSTRAINT GESVAGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GESTAO_VAGA ADD (
  CONSTRAINT GESVAGA_SOLVAMOTNE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_NEGACAO_VAGA) 
 REFERENCES TASY.SOLIC_VAGA_MOTIVO_NEGACAO (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO_ATUAL) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT GESVAGA_DEPMED_FK2 
 FOREIGN KEY (CD_DEPARTAMENTO_DESEJADO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT GESVAGA_SOLVAMOTAP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_APROV_VAGA) 
 REFERENCES TASY.SOLIC_VAGA_MOTIVO_APROV (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_LINCUIDPAC_FK 
 FOREIGN KEY (NR_SEQ_LINHA_CUIDADO) 
 REFERENCES TASY.LINHA_CUIDADO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_INTERNO) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO),
  CONSTRAINT GESVAGA_PRIREG_FK 
 FOREIGN KEY (NR_SEQ_PRIORIDADE) 
 REFERENCES TASY.PRIORIDADE_REGULACAO (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT GESVAGA_MEDCLAT_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_MEDICO) 
 REFERENCES TASY.MEDICO_CLASSIF_ATEND (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_PESFISI_FK3 
 FOREIGN KEY (CD_FUNCIONARIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GESVAGA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT GESVAGA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GESVAGA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT GESVAGA_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT GESVAGA_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_PRINCIPAL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT GESVAGA_MOTTRPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TRANSF) 
 REFERENCES TASY.MOTIVO_TRANSF_PAC (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMOD_ATUAL) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT GESVAGA_TIPACOM_FK2 
 FOREIGN KEY (CD_TIPO_ACOMOD_DESEJ) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT GESVAGA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATUAL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT GESVAGA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT GESVAGA_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_SOLIC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT GESVAGA_PESJURI_FK2 
 FOREIGN KEY (CD_CNPJ_ENCAMINHADO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT GESVAGA_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GESVAGA_SETATEN_FK1 
 FOREIGN KEY (CD_SETOR_DESEJADO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT GESVAGA_ORISOTR_FK 
 FOREIGN KEY (CD_ORIGEM_TRANSF) 
 REFERENCES TASY.ORIGEM_SOLICITACAO_TRANSF (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT GESVAGA_PESCLAS_FK 
 FOREIGN KEY (CD_RESP_RECUSA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GESVAGA_GEVAMCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.GESTAO_VAGA_MOTIVO_CANC (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_GVMORET_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RECUSA) 
 REFERENCES TASY.GV_MOTIVO_RECUSA_TRANSF (NR_SEQUENCIA),
  CONSTRAINT GESVAGA_GVSTPAC_FK 
 FOREIGN KEY (NR_SEQ_STATUS_PAC) 
 REFERENCES TASY.GESTAO_VAGA_STATUS_PAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.GESTAO_VAGA TO NIVEL_1;

