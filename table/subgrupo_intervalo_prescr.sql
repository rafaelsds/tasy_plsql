ALTER TABLE TASY.SUBGRUPO_INTERVALO_PRESCR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUBGRUPO_INTERVALO_PRESCR CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUBGRUPO_INTERVALO_PRESCR
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_APRES                NUMBER(5)         NOT NULL,
  NR_SEQ_GRUPO_INTERV_PRESCR  NUMBER(10)        NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DS_SUBGRUPO_INTERV_PRESCR   VARCHAR2(255 BYTE) NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SGRINPR_GRINTPR_FK_I ON TASY.SUBGRUPO_INTERVALO_PRESCR
(NR_SEQ_GRUPO_INTERV_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SGRINPR_PK ON TASY.SUBGRUPO_INTERVALO_PRESCR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUBGRUPO_INTERVALO_PRESCR ADD (
  CONSTRAINT SGRINPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.SUBGRUPO_INTERVALO_PRESCR ADD (
  CONSTRAINT SGRINPR_GRINTPR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INTERV_PRESCR) 
 REFERENCES TASY.GRUPO_INTERVALO_PRESCR (NR_SEQUENCIA));

