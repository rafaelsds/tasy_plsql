ALTER TABLE TASY.LATAM_MODULO_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LATAM_MODULO_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LATAM_MODULO_DOCUMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_VERSAO            NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IM_DOCUMENTO         CLOB,
  DS_VERSAO            VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_MODULO        NUMBER(10)               NOT NULL,
  NR_SEQ_GAP           NUMBER(10)               NOT NULL,
  DS_PDF_DOCUMENTO     BLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (IM_DOCUMENTO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_PDF_DOCUMENTO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAMODDOC_LATAMGAP_FK_I ON TASY.LATAM_MODULO_DOCUMENTO
(NR_SEQ_GAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LAMODDOC_LATMODU_FK_I ON TASY.LATAM_MODULO_DOCUMENTO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAMODDOC_PK ON TASY.LATAM_MODULO_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LATAM_MODULO_DOCUMENTO ADD (
  CONSTRAINT LAMODDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LATAM_MODULO_DOCUMENTO ADD (
  CONSTRAINT LAMODDOC_LATAMGAP_FK 
 FOREIGN KEY (NR_SEQ_GAP) 
 REFERENCES TASY.LATAM_GAP (NR_SEQUENCIA),
  CONSTRAINT LAMODDOC_LATMODU_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.LATAM_MODULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LATAM_MODULO_DOCUMENTO TO NIVEL_1;

