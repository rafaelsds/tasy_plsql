DROP TABLE TASY.INTERFACE_REG_CRI CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERFACE_REG_CRI
(
  CD_INTERFACE      NUMBER(5)                   NOT NULL,
  CD_REG_INTERFACE  NUMBER(3)                   NOT NULL,
  DS_REG_INTERFACE  VARCHAR2(100 BYTE)          NOT NULL,
  IE_SEPARADOR_REG  VARCHAR2(1 BYTE)            NOT NULL,
  IE_FORMATO_REG    VARCHAR2(1 BYTE)            NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  IE_REGISTRO       VARCHAR2(10 BYTE)           NOT NULL,
  IE_TIPO_REGISTRO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.INTERFACE_REG_CRI TO NIVEL_1;

