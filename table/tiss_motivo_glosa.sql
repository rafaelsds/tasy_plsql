ALTER TABLE TASY.TISS_MOTIVO_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_MOTIVO_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_MOTIVO_GLOSA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_GRUPO_GLOSA            VARCHAR2(2 BYTE),
  CD_MOTIVO_TISS            VARCHAR2(10 BYTE),
  DS_MOTIVO_TISS            VARCHAR2(255 BYTE)  NOT NULL,
  IE_ACAO_PLS               VARCHAR2(3 BYTE)    NOT NULL,
  CD_CONVENIO               NUMBER(5),
  CD_MOTIVO_GLOSA           NUMBER(5),
  IE_FECHAR_CONTA           VARCHAR2(1 BYTE),
  IE_IMPORTACAO_OPS         VARCHAR2(10 BYTE),
  IE_REGRA_INTEGRACAO_OPS   VARCHAR2(1 BYTE),
  IE_PERMITE_ALTERACAO_OPS  VARCHAR2(1 BYTE),
  IE_HABILITA_EXCECAO_OPS   VARCHAR2(1 BYTE),
  IE_PARAM_OPS              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSMGL_CONVENI_FK_I ON TASY.TISS_MOTIVO_GLOSA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSMGL_I1 ON TASY.TISS_MOTIVO_GLOSA
(CD_MOTIVO_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSMGL_I2 ON TASY.TISS_MOTIVO_GLOSA
(CD_MOTIVO_TISS, CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSMGL_MOGLOSA_FK_I ON TASY.TISS_MOTIVO_GLOSA
(CD_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TISSMGL_PK ON TASY.TISS_MOTIVO_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_MOTIVO_GLOSA_tp  after update ON TASY.TISS_MOTIVO_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_MOTIVO_TISS,1,500);gravar_log_alteracao(substr(:old.CD_MOTIVO_TISS,1,4000),substr(:new.CD_MOTIVO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_TISS',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MOTIVO_TISS,1,500);gravar_log_alteracao(substr(:old.DS_MOTIVO_TISS,1,4000),substr(:new.DS_MOTIVO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_TISS',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GRUPO_GLOSA,1,500);gravar_log_alteracao(substr(:old.IE_GRUPO_GLOSA,1,4000),substr(:new.IE_GRUPO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRUPO_GLOSA',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_ACAO_PLS,1,500);gravar_log_alteracao(substr(:old.IE_ACAO_PLS,1,4000),substr(:new.IE_ACAO_PLS,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACAO_PLS',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PERMITE_ALTERACAO_OPS,1,500);gravar_log_alteracao(substr(:old.IE_PERMITE_ALTERACAO_OPS,1,4000),substr(:new.IE_PERMITE_ALTERACAO_OPS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_ALTERACAO_OPS',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FECHAR_CONTA,1,500);gravar_log_alteracao(substr(:old.IE_FECHAR_CONTA,1,4000),substr(:new.IE_FECHAR_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FECHAR_CONTA',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_IMPORTACAO_OPS,1,500);gravar_log_alteracao(substr(:old.IE_IMPORTACAO_OPS,1,4000),substr(:new.IE_IMPORTACAO_OPS,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPORTACAO_OPS',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_INTEGRACAO_OPS,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_INTEGRACAO_OPS,1,4000),substr(:new.IE_REGRA_INTEGRACAO_OPS,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_INTEGRACAO_OPS',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MOTIVO_GLOSA,1,500);gravar_log_alteracao(substr(:old.CD_MOTIVO_GLOSA,1,4000),substr(:new.CD_MOTIVO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_GLOSA',ie_log_w,ds_w,'TISS_MOTIVO_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.tiss_motivo_glosa_atual
before insert or update ON TASY.TISS_MOTIVO_GLOSA for each row
declare

ds_log_w	log_tasy.ds_log%type;
nm_usuario_w	usuario.nm_usuario%type;
cd_funcao_w	number(10);
ds_funcao_w	funcao.ds_aplicacao%type;
nm_id_sid_w	pls_controle_upd_obj.nm_id_sid%type;
nm_id_serial_w	pls_controle_upd_obj.nm_id_serial%type;
nm_osuser_w	pls_controle_upd_obj.nm_osuser%type;
nm_machine_w	pls_controle_upd_obj.nm_machine%type;
nm_terminal_w	pls_controle_upd_obj.nm_terminal%type;
nm_program_w	pls_controle_upd_obj.nm_program%type;
nm_module_w	pls_controle_upd_obj.nm_module%type;
nm_action_w	pls_controle_upd_obj.nm_action%type;
ds_log_call_w	varchar2(1500);

begin
-- pega o usu�rio do tasy, c�digo da fun��o e descri��o da fun��o
nm_usuario_w	:= nvl(wheb_usuario_pck.get_nm_usuario, 'banco de dados');
cd_funcao_w	:= wheb_usuario_pck.get_cd_funcao;

select	max(ds_aplicacao)
into	ds_funcao_w
from	funcao
where	cd_funcao = cd_funcao_w;

-- pega os dados da sess�o atual
pls_util_pck.obter_dados_sessao(	nm_id_sid_w, nm_id_serial_w, nm_osuser_w,
					nm_machine_w, nm_terminal_w,
					nm_program_w, nm_module_w,
					nm_action_w);


if	(inserting) then

	ds_log_w := ' Inserido';

elsif	(updating) then

	ds_log_w := ' Alterado';
end if;

ds_log_w := substr( ds_log_w || ' tabela tiss_motivo_glosa; sequencia ' || :new.nr_sequencia || '; cd_motivo_glosa ' || :new.cd_motivo_glosa ||
		'; usu�rio ' || nm_usuario_w || '; fun��o ' || cd_funcao_w || ' - ' || nvl(ds_funcao_w, 'via banco') ||
		'; nm_id_sid ' || nm_id_sid_w || '; nm_id_serial ' || nm_id_serial_w || '; nm_osuser '|| nm_osuser_w ||
		'; nm_machine ' || nm_machine_w || '; nm_terminal ' || nm_terminal_w || '; nm_program ' || nm_program_w ||
		'; nm_module ' || nm_module_w || '; nm_action ' || nm_action_w, 1, 2000);

ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
							' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

ds_log_w := substr( ds_log_w ||ds_log_call_w, 1, 2000);

insert into log_tasy(
	cd_log,
	ds_log,
	dt_atualizacao,
	nm_usuario
) values (
	1,
	ds_log_w,
	sysdate,
	nm_usuario_w
);

end tiss_motivo_glosa_atual;
/


ALTER TABLE TASY.TISS_MOTIVO_GLOSA ADD (
  CONSTRAINT TISSMGL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_MOTIVO_GLOSA ADD (
  CONSTRAINT TISSMGL_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT TISSMGL_MOGLOSA_FK 
 FOREIGN KEY (CD_MOTIVO_GLOSA) 
 REFERENCES TASY.MOTIVO_GLOSA (CD_MOTIVO_GLOSA));

GRANT SELECT ON TASY.TISS_MOTIVO_GLOSA TO NIVEL_1;

