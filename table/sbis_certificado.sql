ALTER TABLE TASY.SBIS_CERTIFICADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SBIS_CERTIFICADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SBIS_CERTIFICADO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_CNPJ_CERTIFICADO    VARCHAR2(14 BYTE)      NOT NULL,
  CD_REPRESENT_LEGAL     VARCHAR2(10 BYTE)      NOT NULL,
  CD_RESP_TECNICO        VARCHAR2(10 BYTE)      NOT NULL,
  CD_CONTATO_ADM         VARCHAR2(10 BYTE)      NOT NULL,
  NM_PRODUTO             VARCHAR2(60 BYTE)      NOT NULL,
  DS_VERSAO_CERTIFICADA  VARCHAR2(15 BYTE)      NOT NULL,
  DT_CERTIFICADO         DATE                   NOT NULL,
  NR_CERTIFICADO         VARCHAR2(40 BYTE)      NOT NULL,
  IE_PLATAFORMA          VARCHAR2(1 BYTE),
  DT_CERTIFICADO_UTC     VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SBISCER_PESFISI_FK2_I ON TASY.SBIS_CERTIFICADO
(CD_REPRESENT_LEGAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SBISCER_PESFISI_FK3_I ON TASY.SBIS_CERTIFICADO
(CD_RESP_TECNICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SBISCER_PESFISI_FK4_I ON TASY.SBIS_CERTIFICADO
(CD_CONTATO_ADM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SBISCER_PESJURI_FK_I ON TASY.SBIS_CERTIFICADO
(CD_CNPJ_CERTIFICADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SBISCER_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SBISCER_PK ON TASY.SBIS_CERTIFICADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SBISCER_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SBIS_CERTIFICADO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.SBIS_CERTIFICADO FOR EACH ROW
DECLARE

dt_default_w   date  := sysdate+10;

BEGIN

if	(nvl(:old.DT_CERTIFICADO,dt_default_w) <> NVL(:new.DT_CERTIFICADO, dt_default_w) and
	(:new.DT_CERTIFICADO is not null)) then
	:new.dt_certificado_utc    := obter_data_utc(:new.DT_certificado, 'HV');
end if;

END;
/


ALTER TABLE TASY.SBIS_CERTIFICADO ADD (
  CONSTRAINT SBISCER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SBIS_CERTIFICADO ADD (
  CONSTRAINT SBISCER_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_CERTIFICADO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT SBISCER_PESFISI_FK2 
 FOREIGN KEY (CD_REPRESENT_LEGAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SBISCER_PESFISI_FK3 
 FOREIGN KEY (CD_RESP_TECNICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SBISCER_PESFISI_FK4 
 FOREIGN KEY (CD_CONTATO_ADM) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.SBIS_CERTIFICADO TO NIVEL_1;

