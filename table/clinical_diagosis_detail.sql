ALTER TABLE TASY.CLINICAL_DIAGOSIS_DETAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CLINICAL_DIAGOSIS_DETAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLINICAL_DIAGOSIS_DETAIL
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_PATIENT_IDENTIFIER         VARCHAR2(8 BYTE) NOT NULL,
  NR_ADMISSION_NUMBER           VARCHAR2(12 BYTE) NOT NULL,
  NR_MULTIPLE_PRIMARY_SITE      VARCHAR2(2 BYTE) NOT NULL,
  CD_REASON_CLINICAL_DIAGNOSIS  VARCHAR2(2 BYTE) NOT NULL,
  DS_REASON_CLINICAL_DIAGNOSIS  VARCHAR2(50 BYTE),
  DT_EXPORT_FROM                DATE            NOT NULL,
  DT_EXPORT_TO                  DATE            NOT NULL,
  NR_REPORT_SEQUENCE            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CLDIADE_PK ON TASY.CLINICAL_DIAGOSIS_DETAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CLDIADE_REDOWCO_FK_I ON TASY.CLINICAL_DIAGOSIS_DETAIL
(NR_REPORT_SEQUENCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CLINICAL_DIAGOSIS_DETAIL ADD (
  CONSTRAINT CLDIADE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CLINICAL_DIAGOSIS_DETAIL ADD (
  CONSTRAINT CLDIADE_REDOWCO_FK 
 FOREIGN KEY (NR_REPORT_SEQUENCE) 
 REFERENCES TASY.REPORT_DOWNLOADED_COUNT (NR_SEQUENCIA));

GRANT SELECT ON TASY.CLINICAL_DIAGOSIS_DETAIL TO NIVEL_1;

