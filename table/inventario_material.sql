ALTER TABLE TASY.INVENTARIO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INVENTARIO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.INVENTARIO_MATERIAL
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_INVENTARIO           NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_MATERIAL                 NUMBER(6)         NOT NULL,
  QT_CONTAGEM                 NUMBER(17,4),
  QT_RECONTAGEM               NUMBER(17,4),
  QT_INVENTARIO               NUMBER(17,4),
  QT_DIFERENCA                NUMBER(17,4),
  QT_SALDO                    NUMBER(17,4),
  QT_SEG_RECONTAGEM           NUMBER(17,4),
  QT_TERC_RECONTAGEM          NUMBER(17,4),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_LOTE_FORNEC          NUMBER(10),
  NM_USUARIO_CONTAGEM         VARCHAR2(15 BYTE),
  NM_USUARIO_RECONTAGEM       VARCHAR2(15 BYTE),
  NM_USUARIO_SEG_RECONTAGEM   VARCHAR2(15 BYTE),
  NM_USUARIO_TERC_RECONTAGEM  VARCHAR2(15 BYTE),
  IE_AGRUPADOR                NUMBER(5),
  CD_FORNECEDOR               VARCHAR2(14 BYTE),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  QT_ESTOQUE                  NUMBER(17,4),
  IE_STATUS_INVENTARIO        VARCHAR2(1 BYTE),
  NR_SEQ_ORDEM                NUMBER(10),
  IE_CONTAGEM_ATUAL           VARCHAR2(15 BYTE),
  DT_CONTAGEM                 DATE,
  DT_RECONTAGEM               DATE,
  DT_SEG_RECONTAGEM           DATE,
  DT_TERC_RECONTAGEM          DATE,
  DS_JUSTIFICATIVA            VARCHAR2(4000 BYTE),
  CD_KIT_MATERIAL             NUMBER(10),
  NR_SEQ_INV_CICLICO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INVMATE_INVENT_FK_I ON TASY.INVENTARIO_MATERIAL
(NR_SEQ_INVENTARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.INVMATE_KITMATE_FK_I ON TASY.INVENTARIO_MATERIAL
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INVMATE_KITMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INVMATE_MATERIA_FK_I ON TASY.INVENTARIO_MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INVMATE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.INVMATE_PESJURI_FK_I ON TASY.INVENTARIO_MATERIAL
(CD_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INVMATE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.INVMATE_PK ON TASY.INVENTARIO_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INVMATE_PK
  MONITORING USAGE;


CREATE INDEX TASY.INVMATE_SUPINCI_FK_I ON TASY.INVENTARIO_MATERIAL
(NR_SEQ_INV_CICLICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INVENTARIO_MATERIAL_tp  after update ON TASY.INVENTARIO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_CONTAGEM,1,4000),substr(:new.QT_CONTAGEM,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONTAGEM',ie_log_w,ds_w,'INVENTARIO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TERC_RECONTAGEM,1,4000),substr(:new.QT_TERC_RECONTAGEM,1,4000),:new.nm_usuario,nr_seq_w,'QT_TERC_RECONTAGEM',ie_log_w,ds_w,'INVENTARIO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_SEG_RECONTAGEM,1,4000),substr(:new.QT_SEG_RECONTAGEM,1,4000),:new.nm_usuario,nr_seq_w,'QT_SEG_RECONTAGEM',ie_log_w,ds_w,'INVENTARIO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_RECONTAGEM,1,4000),substr(:new.QT_RECONTAGEM,1,4000),:new.nm_usuario,nr_seq_w,'QT_RECONTAGEM',ie_log_w,ds_w,'INVENTARIO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.inventario_material_delete
before delete ON TASY.INVENTARIO_MATERIAL FOR EACH ROW
declare
cd_local_estoque_w	Number(4);
cd_estabelecimento_w	Number(4);
qt_bloqueado_w		Number(3);
ie_consignado_w		varchar2(1);
cd_fornecedor_w		varchar2(14);
dt_aprovacao_w		date;
ie_param_check_w        VARCHAR2(1);
cd_estabelecimento_ww   estabelecimento.cd_estabelecimento%TYPE := wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w             perfil.cd_perfil%TYPE := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w            usuario.nm_usuario%TYPE := wheb_usuario_pck.get_nm_usuario;

begin
obter_param_usuario(143, 391, cd_perfil_w, nm_usuario_w, cd_estabelecimento_ww,
                        ie_param_check_w);
select	cd_local_estoque,
	cd_estabelecimento,
	ie_consignado,
	nvl(:old.cd_fornecedor, cd_fornecedor),
	dt_aprovacao
into	cd_local_estoque_w,
	cd_estabelecimento_w,
	ie_consignado_w,
	cd_fornecedor_w,
	dt_aprovacao_w
from	inventario
where	nr_sequencia = :old.nr_seq_inventario;

if	(ie_consignado_w = 'N') then
	select	count(*)
	into	qt_bloqueado_w
	from	saldo_estoque
	where	ie_bloqueio_inventario	= 'S'
	and	cd_material		= :old.cd_material
	and	cd_local_estoque		= cd_local_estoque_w
	and	dt_mesano_referencia	= trunc(sysdate,'mm')
	and	cd_estabelecimento		= cd_estabelecimento_w;
else
	select	count(*)
	into	qt_bloqueado_w
	from	fornecedor_mat_consignado s
	where	s.ie_bloqueio_inventario	= 'S'
	and	s.cd_fornecedor		= cd_fornecedor_w
	and	s.cd_material		= :old.cd_material
	and	s.cd_local_estoque		= cd_local_estoque_w
	and	s.dt_mesano_referencia	<= trunc(sysdate,'mm')
	and	s.cd_estabelecimento		= cd_estabelecimento_w;
end if;

if	(dt_aprovacao_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(235097);
end if;

if	(:old.nr_seq_inv_ciclico is not null  AND nvl(ie_param_check_w,'N') = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(716973);
end if;

if	(qt_bloqueado_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(266077);
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.inventario_material_update
before update ON TASY.INVENTARIO_MATERIAL FOR EACH ROW
DECLARE

ds_observacao_w		varchar2(255);
ie_atualiza_dif_cont_w	varchar2(1) := 'N';
cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;
dt_atualizacao_saldo_w	date;

BEGIN
select	dt_atualizacao_saldo
into	dt_atualizacao_saldo_w
from	inventario
where	nr_sequencia = :new.nr_seq_inventario;

if	(dt_atualizacao_saldo_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(235097);
end if;

if	(:new.cd_material) <> (:old.cd_material) then
	begin
	if	(:new.nr_seq_inv_ciclico is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(716973);
	end if;

	ds_observacao_w	:= WHEB_MENSAGEM_PCK.get_texto(279125) || :old.cd_material || WHEB_MENSAGEM_PCK.get_texto(279127) || :new.cd_material;
	if	(:new.ds_observacao is not null) then
		ds_observacao_w	:= substr(:new.ds_observacao || chr(13) || chr(10) || ds_observacao_w,1,255);
	end if;

	:new.ds_observacao	:= ds_observacao_w;
	end;
end if;

Obter_Param_Usuario(143, 233,obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_atualiza_dif_cont_w);

if	(ie_atualiza_dif_cont_w = 'S') and
	(:new.qt_inventario is null) and
	(:new.qt_saldo is not null) then
	begin
	if	(:new.qt_terc_recontagem is not null) then
		:new.qt_diferenca := :new.qt_terc_recontagem - :new.qt_saldo;
	elsif	(:new.qt_seg_recontagem is not null) then
		:new.qt_diferenca := :new.qt_seg_recontagem - :new.qt_saldo;
	elsif	(:new.qt_recontagem is not null) then
		:new.qt_diferenca := :new.qt_recontagem - :new.qt_saldo;
	elsif	(:new.qt_contagem is not null) then
		:new.qt_diferenca := :new.qt_contagem - :new.qt_saldo;
	else
		:new.qt_diferenca := null;
	end if;
	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.inventario_bfinsert
before insert ON TASY.INVENTARIO_MATERIAL for each row
declare
nr_seq_inv_ciclico_w			inventario.nr_seq_inv_ciclico%type;
ie_novo_item_inv_ciclico_w		varchar2(1) := nvl(obter_valor_param_usuario(143, 386, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N');
action_w				v$session.action%type;

begin
begin
select	upper(action)
into	action_w
from	v$session
where	audsid = (select userenv('sessionid') from dual)
and	upper(action) = 'GERAR_INVENTARIO_CICLICO'
and	rownum = 1;
exception
when others then
	action_w	:=	'X';
end;

if	(action_w = 'X') then
	begin
	begin
	select	nvl(nr_seq_inv_ciclico,0)
	into	nr_seq_inv_ciclico_w
	from	inventario
	where	nr_sequencia = :new.nr_seq_inventario;
	exception
	when others then
		nr_seq_inv_ciclico_w	:=	0;
	end;

	if	(nr_seq_inv_ciclico_w > 0) and (ie_novo_item_inv_ciclico_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(791240);
	end if;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.inventario_material_insert
after insert ON TASY.INVENTARIO_MATERIAL 
for each row
declare
cd_local_estoque_w		number(4);
cd_estabelecimento_w	number(4);
qt_bloqueado_w			number(3);
ie_estoque_lote_w		varchar2(1);
nr_seq_lote_w			number(10);
qt_estoque_w			number(13,4);
ie_gerou_lote_w			varchar2(1)	:=	'N';
ie_consignado_w			inventario.ie_consignado%type;

cursor c01 is
select	nr_seq_lote,
	nvl(qt_estoque,0)
from	saldo_estoque_lote a
where	cd_material = :new.cd_material
and	cd_local_estoque = cd_local_estoque_w
and	cd_estabelecimento = cd_estabelecimento_w
and	dt_mesano_referencia = trunc(sysdate,'mm')
and	:new.cd_fornecedor is null
and ie_consignado_w = 'N'
and	not exists (	select 	1
			from	inventario_material_lote b
			where	b.nr_seq_item = :new.nr_sequencia
			and	a.nr_seq_lote = b.nr_seq_lote_fornec)
union all
select	nr_seq_lote,
	nvl(qt_estoque,0)
from	fornecedor_mat_consig_lote a
where	cd_material = :new.cd_material
and	cd_local_estoque = cd_local_estoque_w
and	cd_estabelecimento = cd_estabelecimento_w
and	dt_mesano_referencia = trunc(sysdate,'mm')
and	:new.cd_fornecedor is not null
and	a.cd_fornecedor = :new.cd_fornecedor
and ie_consignado_w = 'S'
and	not exists (	select 	1
			from	inventario_material_lote b
			where	b.nr_seq_item = :new.nr_sequencia
			and	a.nr_seq_lote = b.nr_seq_lote_fornec)
union all
select	nr_seq_lote,
	nvl(qt_estoque,0)
from	fornecedor_mat_consig_lote a
where	cd_material = :new.cd_material
and	cd_local_estoque = cd_local_estoque_w
and	cd_estabelecimento = cd_estabelecimento_w
and	dt_mesano_referencia = trunc(sysdate,'mm')
and	:new.cd_fornecedor is null
and obter_se_mat_consignado(a.cd_material) in ('1', '2')
and ie_consignado_w = 'S'
and	not exists (	select 	1
			from	inventario_material_lote b
			where	b.nr_seq_item = :new.nr_sequencia
			and	a.nr_seq_lote = b.nr_seq_lote_fornec);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	select	cd_local_estoque,
		cd_estabelecimento,
		ie_consignado
	into	cd_local_estoque_w,
		cd_estabelecimento_w,
		ie_consignado_w
	from	inventario
	where	nr_sequencia = :new.nr_seq_inventario;

	ie_estoque_lote_w := substr(nvl(obter_se_material_estoque_lote(cd_estabelecimento_w,:new.cd_material),'N'),1,1);

	if	(ie_estoque_lote_w = 'S') then
		begin
		open c01;
		loop
		fetch c01 into
			nr_seq_lote_w,
			qt_estoque_w;
		exit when c01%notfound;
			begin
			insert into inventario_material_lote(
				nr_seq_item,
				nr_seq_inventario,
				nr_seq_lote_fornec,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nm_usuario,
				dt_atualizacao,
				nr_sequencia,
				qt_saldo)
			values (	:new.nr_sequencia,
				:new.nr_seq_inventario,
				nr_seq_lote_w,
				:new.nm_usuario,
				:new.dt_atualizacao,
				:new.nm_usuario,
				:new.dt_atualizacao,
				inventario_material_lote_seq.nextval,
				qt_estoque_w);

			ie_gerou_lote_w	:=	'S';
			end;
		end loop;
		close c01;

		if	(ie_gerou_lote_w = 'N') then
			begin
			select	max(a.nr_sequencia)
			into	nr_seq_lote_w
			from	material b,
				material_lote_fornec a
			where	a.cd_material = b.cd_material
			and	a.ie_situacao = 'A'
			and	nvl(a.ie_bloqueio,'N') = 'N'
			and	b.ie_situacao = 'A'
			and	a.cd_estabelecimento = cd_estabelecimento_w
			and	a.cd_cgc_fornec = nvl(:new.cd_fornecedor, a.cd_cgc_fornec)
			and	b.cd_material_estoque = :new.cd_material;

			if	(nr_seq_lote_w is not null) then
				insert into inventario_material_lote(
					nr_seq_item,
					nr_seq_inventario,
					nr_seq_lote_fornec,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nr_sequencia,
					qt_saldo)
				values(	:new.nr_sequencia,
					:new.nr_seq_inventario,
					nr_seq_lote_w,
					:new.nm_usuario,
					:new.dt_atualizacao,
					:new.nm_usuario,
					:new.dt_atualizacao,
					inventario_material_lote_seq.nextval,
					0);
			end if;
			end;
		end if;
		end;
	end if;
end if;
end;
/


ALTER TABLE TASY.INVENTARIO_MATERIAL ADD (
  CONSTRAINT INVMATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INVENTARIO_MATERIAL ADD (
  CONSTRAINT INVMATE_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT INVMATE_SUPINCI_FK 
 FOREIGN KEY (NR_SEQ_INV_CICLICO) 
 REFERENCES TASY.SUP_INV_CICLICO_REGRA (NR_SEQUENCIA),
  CONSTRAINT INVMATE_INVENT_FK 
 FOREIGN KEY (NR_SEQ_INVENTARIO) 
 REFERENCES TASY.INVENTARIO (NR_SEQUENCIA),
  CONSTRAINT INVMATE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT INVMATE_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.INVENTARIO_MATERIAL TO NIVEL_1;

