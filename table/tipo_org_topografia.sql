ALTER TABLE TASY.TIPO_ORG_TOPOGRAFIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_ORG_TOPOGRAFIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_ORG_TOPOGRAFIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TOPOGRAFIA    VARCHAR2(10 BYTE),
  NR_SEQ_ORGAO         NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPORGTOP_CIDOTOP_FK_I ON TASY.TIPO_ORG_TOPOGRAFIA
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPORGTOP_PK ON TASY.TIPO_ORG_TOPOGRAFIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPORGTOP_TIPORGAO_FK_I ON TASY.TIPO_ORG_TOPOGRAFIA
(NR_SEQ_ORGAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_ORG_TOPOGRAFIA ADD (
  CONSTRAINT TIPORGTOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA),
  CONSTRAINT TIPORGTOP_UK
 UNIQUE (NR_SEQ_TOPOGRAFIA));

ALTER TABLE TASY.TIPO_ORG_TOPOGRAFIA ADD (
  CONSTRAINT TIPORGTOP_CIDOTOP_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.CIDO_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT TIPORGTOP_TIPORGAO_FK 
 FOREIGN KEY (NR_SEQ_ORGAO) 
 REFERENCES TASY.TIPO_ORGAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TIPO_ORG_TOPOGRAFIA TO NIVEL_1;

