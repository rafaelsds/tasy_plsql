DROP TABLE TASY.W_CONSULTA_BENEFICIARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CONSULTA_BENEFICIARIO
(
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  NM_PESSOA_FISICA     VARCHAR2(60 BYTE),
  DT_NASCIMENTO        DATE,
  CD_USUARIO_CONVENIO  VARCHAR2(30 BYTE),
  NR_CPF               VARCHAR2(11 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_RESULTADO         VARCHAR2(1 BYTE),
  DS_MENSAGEM          VARCHAR2(130 BYTE),
  QT_BENEFICIARIOS     VARCHAR2(10 BYTE),
  CD_MODALIDADE        VARCHAR2(10 BYTE),
  DS_MODALIDADE        VARCHAR2(40 BYTE),
  CD_PLANO             VARCHAR2(10 BYTE),
  DS_PLANO             VARCHAR2(40 BYTE),
  CD_TIPO_PLANO        VARCHAR2(10 BYTE),
  DS_TIPO_PLANO        VARCHAR2(40 BYTE),
  NM_PLANO             VARCHAR2(40 BYTE),
  NR_IDENTIDADE        VARCHAR2(14 BYTE),
  DS_ORGAO_EMISSOR     VARCHAR2(40 BYTE),
  SG_UF_ORGAO_EMISSOR  VARCHAR2(2 BYTE),
  DS_PAIS_RG           VARCHAR2(20 BYTE),
  IE_SEXO              VARCHAR2(1 BYTE),
  NM_MAE               VARCHAR2(70 BYTE),
  CD_CEP               VARCHAR2(10 BYTE),
  CD_CIDADE            VARCHAR2(50 BYTE),
  DS_CIDADE            VARCHAR2(50 BYTE),
  SG_UF                VARCHAR2(2 BYTE),
  DS_BAIRRO            VARCHAR2(30 BYTE),
  DS_LOGRADOURO        VARCHAR2(40 BYTE),
  NR_ENDERECO          VARCHAR2(40 BYTE),
  DS_COMPLEMENTO       VARCHAR2(40 BYTE),
  NR_TELEFONE_1        VARCHAR2(20 BYTE),
  NR_TELEFONE_2        VARCHAR2(50 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE),
  IE_STATUS            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER TASY.W_CONSULTA_BENEFICIARIO_tp  after update ON TASY.W_CONSULTA_BENEFICIARIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_w:=substr(:new.NM_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA,1,4000),substr(:new.NM_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA',ie_log_w,ds_w,'W_CONSULTA_BENEFICIARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'W_CONSULTA_BENEFICIARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'W_CONSULTA_BENEFICIARIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_USUARIO_CONVENIO,1,4000),substr(:new.CD_USUARIO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_USUARIO_CONVENIO',ie_log_w,ds_w,'W_CONSULTA_BENEFICIARIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


GRANT SELECT ON TASY.W_CONSULTA_BENEFICIARIO TO NIVEL_1;

