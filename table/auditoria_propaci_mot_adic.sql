ALTER TABLE TASY.AUDITORIA_PROPACI_MOT_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUDITORIA_PROPACI_MOT_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUDITORIA_PROPACI_MOT_ADIC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_AUDIT_PROPACI  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUPRMOA_AUDMOTI_FK_I ON TASY.AUDITORIA_PROPACI_MOT_ADIC
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUPRMOA_AUDMOTI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUPRMOA_AUDPROP_FK_I ON TASY.AUDITORIA_PROPACI_MOT_ADIC
(NR_SEQ_AUDIT_PROPACI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUPRMOA_AUDPROP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AUPRMOA_PK ON TASY.AUDITORIA_PROPACI_MOT_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUPRMOA_PK
  MONITORING USAGE;


ALTER TABLE TASY.AUDITORIA_PROPACI_MOT_ADIC ADD (
  CONSTRAINT AUPRMOA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUDITORIA_PROPACI_MOT_ADIC ADD (
  CONSTRAINT AUPRMOA_AUDMOTI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.AUDITORIA_MOTIVO (NR_SEQUENCIA),
  CONSTRAINT AUPRMOA_AUDPROP_FK 
 FOREIGN KEY (NR_SEQ_AUDIT_PROPACI) 
 REFERENCES TASY.AUDITORIA_PROPACI (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AUDITORIA_PROPACI_MOT_ADIC TO NIVEL_1;

