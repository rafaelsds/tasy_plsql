ALTER TABLE TASY.PTU_MOV_BENEF_LOG_INCO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_MOV_BENEF_LOG_INCO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_MOV_BENEF_LOG_INCO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MOV_SEGURADO  NUMBER(10),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  NR_SEQ_SEGURADO      NUMBER(10),
  NR_SEQ_LOTE          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO      NUMBER(10),
  NR_SEQ_MOV_EMPRESA   NUMBER(10),
  DS_INCONSISTENCIA    VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTUMBLI_PESFISI_FK_I ON TASY.PTU_MOV_BENEF_LOG_INCO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PTUMBLI_PK ON TASY.PTU_MOV_BENEF_LOG_INCO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUMBLI_PLSSEGU_FK_I ON TASY.PTU_MOV_BENEF_LOG_INCO
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUMBLI_PTUMBEM_FK_I ON TASY.PTU_MOV_BENEF_LOG_INCO
(NR_SEQ_MOV_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUMBLI_PTUMBLT_FK_I ON TASY.PTU_MOV_BENEF_LOG_INCO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUMBLI_PTUMBSE_FK_I ON TASY.PTU_MOV_BENEF_LOG_INCO
(NR_SEQ_MOV_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PTU_MOV_BENEF_LOG_INCO ADD (
  CONSTRAINT PTUMBLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_MOV_BENEF_LOG_INCO ADD (
  CONSTRAINT PTUMBLI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PTUMBLI_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PTUMBLI_PTUMBEM_FK 
 FOREIGN KEY (NR_SEQ_MOV_EMPRESA) 
 REFERENCES TASY.PTU_MOV_BENEF_EMPRESA (NR_SEQUENCIA),
  CONSTRAINT PTUMBLI_PTUMBLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PTU_MOV_BENEF_LOTE (NR_SEQUENCIA),
  CONSTRAINT PTUMBLI_PTUMBSE_FK 
 FOREIGN KEY (NR_SEQ_MOV_SEGURADO) 
 REFERENCES TASY.PTU_MOV_BENEF_SEGURADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_MOV_BENEF_LOG_INCO TO NIVEL_1;

