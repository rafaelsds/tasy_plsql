ALTER TABLE TASY.PFCS_SERVICE_REQUEST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_SERVICE_REQUEST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_SERVICE_REQUEST
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PATIENT          NUMBER(10),
  NR_SEQ_ENCOUNTER        NUMBER(10),
  ID_SERV_REQUEST         VARCHAR2(255 BYTE),
  SI_STATUS               VARCHAR2(30 BYTE),
  CD_SERVICE              VARCHAR2(30 BYTE),
  SI_INTENT               VARCHAR2(30 BYTE),
  CD_REASON               VARCHAR2(30 BYTE),
  NR_SEQ_PRACT_REQUESTER  NUMBER(10),
  NR_SEQ_PRACT_PERFORMER  NUMBER(10),
  NR_SEQ_LOCATION         NUMBER(10,1),
  DT_AUTHORED_ON          DATE                  DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCSSERVR_PFCSENC_FK_I ON TASY.PFCS_SERVICE_REQUEST
(NR_SEQ_ENCOUNTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSSERVR_PFCSLOCAL_FK_I ON TASY.PFCS_SERVICE_REQUEST
(NR_SEQ_LOCATION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSSERVR_PFCSPAT_FK_I ON TASY.PFCS_SERVICE_REQUEST
(NR_SEQ_PATIENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFCSSERVR_PFCSPRACT_FK_I ON TASY.PFCS_SERVICE_REQUEST
(NR_SEQ_PRACT_PERFORMER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCSSERVR_PK ON TASY.PFCS_SERVICE_REQUEST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PFCS_CHECK_DPT_SITUATION
BEFORE INSERT OR UPDATE ON TASY.PFCS_SERVICE_REQUEST FOR EACH ROW
DECLARE
nm_usuario_w    setor_atendimento.nm_usuario%type := 'PFCS';

/*
    This trigger will check if a department is inactive when there is a request for it.
    The IBE integration does not send the situation of a department, just for the bed. The client can disable a department
    throught the EMR or PFCS interface. It's also possible to enable again on PFCS, but if, for some reason, it's not and
    a request for a bed in that department comes, it is implicit that the department is active again.
*/

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

    if (:new.nr_seq_location is not null) then
        update setor_atendimento set
            IE_SITUACAO = 'A',
            NM_USUARIO = nm_usuario_w,
			IE_OCUP_HOSPITALAR = 'S',
            DT_ATUALIZACAO = SYSDATE
        where NM_USUARIO_NREC = nm_usuario_w
        and cd_setor_atendimento = (
            select max(cd_setor_atendimento)
            from unidade_atendimento
            where nr_seq_location = :new.nr_seq_location
        )
        and ie_situacao = 'I';
    end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.PFCS_PAT_LOC_HST_UPT
BEFORE INSERT OR UPDATE ON TASY.PFCS_SERVICE_REQUEST FOR EACH ROW
DECLARE
nm_usuario_w            pfcs_service_request.nm_usuario%type     := 'PFCS';
nr_seq_last_loc_seq_w   number(10,0) := 0;
nr_seq_last_loc_uni_w   number(10,0) := 0;
nr_seq_organization_w   number(10,0);
qt_reg_w				number(1);

/*
    This trigger is responsabile for creating a history of patient locations. It will store all the beds that a patient will
    be assigned to during his episode on the hospital

    It will also register the bed requests history, so it will be possible to analyse when the request for the bed was made
    and when the patient really got into the bed
*/

BEGIN
    wheb_usuario_pck.set_nm_usuario(nm_usuario_w);
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    if ((:new.cd_service in ('E0401',  'E0402', 'E0405')) and (:new.si_status in ('COMPLETED', 'ACTIVE'))) then
        if (:new.si_intent = 'ORDER') then
            -- The code inside this condition is not necessary if it is a new admission. In this case, the patient just arrived
            -- on the hospital
            if (:new.cd_service <> 'E0401') then
                -- Check if there is a previous location for that patient that is different of the new one
                select nvl(max(nr_sequencia),0),
                    nvl(max(nr_seq_location),0)
                into nr_seq_last_loc_seq_w,
                    nr_seq_last_loc_uni_w
                from pfcs_patient_loc_hist
                where nr_seq_location <> :new.nr_seq_location
                    and nr_seq_encounter = :new.nr_seq_encounter
                    and nr_seq_patient = :new.nr_seq_patient
                    and ie_situacao = 'A'
                    and dt_period_end is null;

                -- If the previous location is different from the new one, the code will update the end_date of the
                -- previous location history
                if (nr_seq_last_loc_seq_w <> 0) then
                    update pfcs_patient_loc_hist
                    set dt_period_end = nvl(:new.dt_authored_on, SYSDATE)
                    where nr_sequencia = nr_seq_last_loc_seq_w;
                end if;

                -- Set status of the last bed location to free
                -- The trigger on pfcs_location will also update the table unidade_atendimento, so they will be in sync
                if (nr_seq_last_loc_uni_w <> 0) then
                    update pfcs_location
                    set cd_operational_status = 'U'
                    where nr_sequencia = nr_seq_last_loc_uni_w
                    and cd_operational_status <> 'U';
                end if;
            end if;
        end if;

        if (:new.cd_service <> 'E0405') then
            -- Creating a new location history ONLY if the request is Admission or Transfer type
            -- Getting the organization related to the service request location
            select max(nr_seq_organization)
            into nr_seq_organization_w
            from pfcs_location
            where nr_sequencia = :new.nr_seq_location;

            if (:new.si_intent = 'ORDER') then
                insert into pfcs_patient_loc_hist(
                    nr_sequencia,
                    dt_atualizacao,
                    dt_atualizacao_nrec,
                    nm_usuario,
                    nm_usuario_nrec,
                    nr_seq_patient,
                    nr_seq_encounter,
                    nr_seq_location,
                    nr_seq_organization,
                    dt_period_start,
                    ie_situacao,
                    si_intent
                ) values (
                    PFCS_PATIENT_LOC_HIST_SEQ.nextval,
                    SYSDATE,
                    SYSDATE,
                    nm_usuario_w,
                    nm_usuario_w,
                    :new.nr_seq_patient,
                    :new.nr_seq_encounter,
                    :new.nr_seq_location,
                    nr_seq_organization_w,
                    nvl(:new.dt_authored_on, SYSDATE),
                    'A',
                    :new.si_intent
                );

                update unidade_atendimento
                set dt_entrada_unidade = nvl(:new.dt_authored_on, SYSDATE)
                where nr_seq_location = :new.nr_seq_location
				and nm_usuario_nrec = nm_usuario_w;
            elsif (:new.si_intent = 'PLAN') then
                insert into pfcs_patient_loc_hist(
                    nr_sequencia,
                    dt_atualizacao,
                    dt_atualizacao_nrec,
                    nm_usuario,
                    nm_usuario_nrec,
                    nr_seq_patient,
                    nr_seq_encounter,
                    nr_seq_location,
                    nr_seq_organization,
                    dt_request,
                    ie_situacao,
                    si_intent
                ) values (
                    PFCS_PATIENT_LOC_HIST_SEQ.nextval,
                    SYSDATE,
                    SYSDATE,
                    nm_usuario_w,
                    nm_usuario_w,
                    :new.nr_seq_patient,
                    :new.nr_seq_encounter,
                    :new.nr_seq_location,
                    nr_seq_organization_w,
                    nvl(:new.dt_authored_on, SYSDATE),
                    'A',
                    :new.si_intent
                );
            end if;
        end if;
    end if;

<<Final>>
qt_reg_w := 0;
END;
/


ALTER TABLE TASY.PFCS_SERVICE_REQUEST ADD (
  CONSTRAINT PFCSSERVR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PFCS_SERVICE_REQUEST ADD (
  CONSTRAINT PFCSSERVR_PFCSENC_FK 
 FOREIGN KEY (NR_SEQ_ENCOUNTER) 
 REFERENCES TASY.PFCS_ENCOUNTER (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSSERVR_PFCSPAT_FK 
 FOREIGN KEY (NR_SEQ_PATIENT) 
 REFERENCES TASY.PFCS_PATIENT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSSERVR_PFCSLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCATION) 
 REFERENCES TASY.PFCS_LOCATION (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PFCSSERVR_PFCSPRACT_FK 
 FOREIGN KEY (NR_SEQ_PRACT_PERFORMER) 
 REFERENCES TASY.PFCS_PRACTITIONER (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PFCS_SERVICE_REQUEST TO NIVEL_1;

