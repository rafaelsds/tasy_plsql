ALTER TABLE TASY.GPI_TIPO_PROJETO_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_TIPO_PROJETO_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_TIPO_PROJETO_LIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_PERMITE           VARCHAR2(1 BYTE)         NOT NULL,
  CD_PERFIL            NUMBER(5),
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPITPRLI_GPITPRO_FK_I ON TASY.GPI_TIPO_PROJETO_LIB
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPITPRLI_GRUUSUA_FK_I ON TASY.GPI_TIPO_PROJETO_LIB
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPITPRLI_GRUUSUA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPITPRLI_PERFIL_FK_I ON TASY.GPI_TIPO_PROJETO_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPITPRLI_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GPITPRLI_PK ON TASY.GPI_TIPO_PROJETO_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPITPRLI_USUARIO_FK_I ON TASY.GPI_TIPO_PROJETO_LIB
(NM_USUARIO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPITPRLI_USUARIO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.GPI_TIPO_PROJETO_LIB ADD (
  CONSTRAINT GPITPRLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_TIPO_PROJETO_LIB ADD (
  CONSTRAINT GPITPRLI_GPITPRO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.GPI_TIPO_PROJETO (NR_SEQUENCIA),
  CONSTRAINT GPITPRLI_GRUUSUA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_USUARIO (NR_SEQUENCIA),
  CONSTRAINT GPITPRLI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT GPITPRLI_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_LIB) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.GPI_TIPO_PROJETO_LIB TO NIVEL_1;

