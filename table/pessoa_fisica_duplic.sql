ALTER TABLE TASY.PESSOA_FISICA_DUPLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_DUPLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_DUPLIC
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  CD_PESSOA_FISICA  VARCHAR2(10 BYTE)           NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  CD_CADASTRO       VARCHAR2(10 BYTE)           NOT NULL,
  NM_PACIENTE       VARCHAR2(60 BYTE)           NOT NULL,
  NR_PRONTUARIO     NUMBER(10),
  QT_ATENDIMENTO    NUMBER(15),
  QT_ATEND_INTERNO  NUMBER(15),
  IE_ACERTO         VARCHAR2(1 BYTE),
  DT_INTEGRACAO     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFIDU_PESFISI_FK_I ON TASY.PESSOA_FISICA_DUPLIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFIDU_PK ON TASY.PESSOA_FISICA_DUPLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFIDU_PK
  MONITORING USAGE;


ALTER TABLE TASY.PESSOA_FISICA_DUPLIC ADD (
  CONSTRAINT PESFIDU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_DUPLIC ADD (
  CONSTRAINT PESFIDU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_FISICA_DUPLIC TO NIVEL_1;

