ALTER TABLE TASY.SUS_LAUDO_BARIATRICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_LAUDO_BARIATRICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_LAUDO_BARIATRICO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NR_SEQ_LAUDO                NUMBER(10)        NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  DT_PRIM_AVALIACAO           DATE,
  QT_IMC_PRIM_AVALIACAO       NUMBER(11,2),
  QT_PESO                     NUMBER(11,2),
  DT_AVALIACAO_ATUAL          DATE,
  QT_IMC_ATUAL                NUMBER(11,2),
  IE_COMORBIDADES             VARCHAR2(1 BYTE),
  IE_HIPERTENSAO              VARCHAR2(1 BYTE),
  IE_DIABETES                 VARCHAR2(1 BYTE),
  IE_DISLIPIDEMIA             VARCHAR2(1 BYTE),
  IE_ARTROSE                  VARCHAR2(1 BYTE),
  IE_APNEIA                   VARCHAR2(1 BYTE),
  CD_CID                      VARCHAR2(4 BYTE),
  IE_USO_MEDICAMENTOS         VARCHAR2(1 BYTE),
  IE_PRAT_ATIV_FISICA         VARCHAR2(1 BYTE),
  IE_IMC_PERDA_PONDERAL       VARCHAR2(1 BYTE),
  IE_NUTRICIONISTA            VARCHAR2(1 BYTE),
  IE_PSICOLOGO                VARCHAR2(1 BYTE),
  IE_MEDICO_PSIQUIATRA        VARCHAR2(1 BYTE),
  IE_MEDICO_CLINICO           VARCHAR2(1 BYTE),
  IE_MEDICO_ENDOCRINOLOGISTA  VARCHAR2(1 BYTE),
  IE_MEDICO_CIR_AP_DIGESTIVO  VARCHAR2(1 BYTE),
  IE_MEDICO_CIR_GERAL         VARCHAR2(1 BYTE),
  IE_PARTICIPA_REUNIOES       VARCHAR2(1 BYTE),
  IE_AVALIACAO_RISCO          VARCHAR2(1 BYTE),
  IE_EXAMES_LABORATORIAIS     VARCHAR2(1 BYTE),
  IE_ESOFAGO                  VARCHAR2(1 BYTE),
  IE_ULTRASONOGRAFIA          VARCHAR2(1 BYTE),
  IE_ECOCARDIOGRAFIA          VARCHAR2(1 BYTE),
  IE_ULTRASSONOGRAFIA         VARCHAR2(1 BYTE),
  IE_PROVA_FUNCAO_PULMONAR    VARCHAR2(1 BYTE),
  IE_APTO_PROC_CIRURGICO      VARCHAR2(1 BYTE),
  QT_PERC_EXC_PESO            NUMBER(11,2),
  QT_KG_PERDIDOS              NUMBER(11,2),
  IE_GASTREC_DUODENAL         VARCHAR2(1 BYTE),
  IE_GASTREC_VERTICAL         VARCHAR2(1 BYTE),
  IE_GASTROP_INTESTINAL       VARCHAR2(1 BYTE),
  IE_GASTROP_VERTICAL         VARCHAR2(1 BYTE),
  IE_DERMO_ABDOMINAL          VARCHAR2(1 BYTE),
  IE_MAMOPLASTIA              VARCHAR2(1 BYTE),
  IE_DERMO_BRAQUIAL           VARCHAR2(1 BYTE),
  IE_DERMO_CRURAL             VARCHAR2(1 BYTE),
  IE_DERMO_CIRCUN             VARCHAR2(1 BYTE),
  QT_TEMPO_DERMO_ABDOMINAL    NUMBER(11,2),
  QT_TEMPO_MAMOPLASTIA        NUMBER(11,2),
  QT_TEMPO_DERMO_BRAQUIAL     NUMBER(11,2),
  QT_TEMPO_DERMO_CRURAL       NUMBER(11,2),
  QT_TEMPO_DERMO_CIRCUN       NUMBER(11,2),
  QT_MES_ACOMPANHAMENTO       NUMBER(11,2),
  QT_ANO_ACOMPANHAMENTO       NUMBER(11,2),
  IE_USO_POLIVITAMINICO       VARCHAR2(1 BYTE),
  IE_HOUVE_REGANHO_PESO       VARCHAR2(1 BYTE),
  IE_HOUVE_ADESAO             VARCHAR2(1 BYTE),
  DT_CIRUR_BARIATRICA         DATE,
  NR_AIH_BARIATRICA           NUMBER(13)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSLABARIA_PK ON TASY.SUS_LAUDO_BARIATRICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLABARIA_SUSLAPA_FK_I ON TASY.SUS_LAUDO_BARIATRICO
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUS_LAUDO_BARIATRICO ADD (
  CONSTRAINT SUSLABARIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_LAUDO_BARIATRICO ADD (
  CONSTRAINT SUSLABARIA_SUSLAPA_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.SUS_LAUDO_PACIENTE (NR_SEQ_INTERNO));

GRANT SELECT ON TASY.SUS_LAUDO_BARIATRICO TO NIVEL_1;

