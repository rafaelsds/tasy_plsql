ALTER TABLE TASY.CIH_CID_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIH_CID_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIH_CID_REGRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10),
  CD_DOENCA_CID        VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIHCIRE_CIDDOEN_FK_I ON TASY.CIH_CID_REGRA
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHCIRE_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHCIRE_CIHRECI_FK_I ON TASY.CIH_CID_REGRA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHCIRE_CIHRECI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CIHCIRE_PK ON TASY.CIH_CID_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHCIRE_PK
  MONITORING USAGE;


ALTER TABLE TASY.CIH_CID_REGRA ADD (
  CONSTRAINT CIHCIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIH_CID_REGRA ADD (
  CONSTRAINT CIHCIRE_CIHRECI_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.CIH_REGRA_CID (NR_SEQUENCIA),
  CONSTRAINT CIHCIRE_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.CIH_CID_REGRA TO NIVEL_1;

