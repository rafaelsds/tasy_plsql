ALTER TABLE TASY.SAN_REGRA_BRINDE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_REGRA_BRINDE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_REGRA_BRINDE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_DOACAO            NUMBER(3)                NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_TIPO_DOACAO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANREGB_ESTABEL_FK_I ON TASY.SAN_REGRA_BRINDE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREGB_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANREGB_MATERIA_FK_I ON TASY.SAN_REGRA_BRINDE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREGB_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SANREGB_PK ON TASY.SAN_REGRA_BRINDE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREGB_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANREGB_SANTIDO_FK_I ON TASY.SAN_REGRA_BRINDE
(NR_SEQ_TIPO_DOACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANREGB_SANTIDO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_REGRA_BRINDE ADD (
  CONSTRAINT SANREGB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_REGRA_BRINDE ADD (
  CONSTRAINT SANREGB_SANTIDO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOACAO) 
 REFERENCES TASY.SAN_TIPO_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANREGB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SANREGB_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.SAN_REGRA_BRINDE TO NIVEL_1;

