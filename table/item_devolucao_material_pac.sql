ALTER TABLE TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ITEM_DEVOLUCAO_MATERIAL_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(
  NR_DEVOLUCAO             NUMBER(10)           NOT NULL,
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_MATERIAL              NUMBER(6)            NOT NULL,
  DT_ATENDIMENTO           DATE                 NOT NULL,
  CD_LOCAL_ESTOQUE         NUMBER(4)            NOT NULL,
  CD_UNIDADE_MEDIDA        VARCHAR2(30 BYTE)    NOT NULL,
  CD_MOTIVO_DEVOLUCAO      VARCHAR2(3 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO_DEVOL         VARCHAR2(15 BYTE)    NOT NULL,
  QT_MATERIAL              NUMBER(13,4)         NOT NULL,
  NR_PRESCRICAO            NUMBER(10),
  NR_SEQUENCIA_PRESCRICAO  NUMBER(6),
  CD_PESSOA_FISICA_RECEB   VARCHAR2(10 BYTE),
  DT_RECEBIMENTO           DATE,
  NM_USUARIO_RECEB         VARCHAR2(15 BYTE),
  QT_MATERIAL_ESTOQUE      NUMBER(13,4),
  IE_TIPO_BAIXA_ESTOQUE    VARCHAR2(3 BYTE),
  DT_ENTRADA_UNIDADE       DATE,
  NR_SEQ_ATENDIMENTO       NUMBER(10),
  NR_SEQ_LOTE_FORNEC       NUMBER(10),
  CD_MATERIAL_DEV          NUMBER(6),
  CD_CGC_FORNEC            VARCHAR2(14 BYTE),
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_EMISSAO_LOC_ESTOQUE   DATE,
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_GERACAO               VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  IE_BAIXA                 VARCHAR2(1 BYTE),
  DT_BAIXA_CONTA           DATE,
  NM_USUARIO_BAIXA_CONTA   VARCHAR2(15 BYTE),
  NR_SEQ_LOTE              NUMBER(10),
  NM_USUARIO_JUSTIF        VARCHAR2(15 BYTE),
  NR_SEQ_JUSTIFICATIVA     NUMBER(10),
  NR_SEQ_PROCESSO          NUMBER(10),
  NR_KIT_ESTOQUE           NUMBER(10),
  NM_USUARIO_RECEB_FAR     VARCHAR2(15 BYTE),
  NM_USUARIO_RECEB_ENF     VARCHAR2(15 BYTE),
  NR_MOVIMENTO_ESTOQUE     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ITEDEMP_APLOTE_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEMP_MATERIA_FK_I2 ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_MATERIAL_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEMP_MATERIA_FK_I2
  MONITORING USAGE;


CREATE INDEX TASY.ITEDEMP_MATLOFO_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEMP_MOJUDE_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEMP_MOJUDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEDEMP_MOVESTO_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_MOVIMENTO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEMP_PESJURI_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_CGC_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEMP_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEDEMP_SETATEN_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEMP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEDEVP_DEVMATP_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEVP_I1 ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(IE_TIPO_BAIXA_ESTOQUE, CD_LOCAL_ESTOQUE, NR_DEVOLUCAO, DT_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEVP_I2 ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEVP_LOCESTO_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEVP_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEDEVP_MATERIA_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEVP_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ITEDEVP_PESFISI_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_PESSOA_FISICA_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ITEDEVP_PK ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_DEVOLUCAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEVP_PRESMAT_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(NR_PRESCRICAO, NR_SEQUENCIA_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ITEDEVP_UNIMEDI_FK_I ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ITEDEVP_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.item_dev_mat_pac_insert
before insert ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC for each row
begin

:new.ie_tipo_baixa_estoque := '0';

end;
/


CREATE OR REPLACE TRIGGER TASY.item_devolucao_mat_pac_update
before update or insert ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC for each row
declare

ie_quantidade_maior_w	varchar2(1);
ds_log_w				varchar2(2000);

begin

if	(inserting) then
	ds_log_w := substr(ds_log_w ||';NR_DEVOLUCAO=' || :new.nr_devolucao || 'CD_MATERIAL='|| :new.cd_material || ';call stack='|| substr(dbms_utility.format_call_stack,1,1800),1,2000);
	gravar_log_tasy(5136, ds_log_w, :new.nr_sequencia);
else

	obter_param_usuario(42,9,obter_perfil_ativo,:new.nm_usuario,0,ie_quantidade_maior_w);

	if	(ie_quantidade_maior_w = 'N') and
		(:old.qt_material > 0) and
		(:new.qt_material > 0) then
		if	(:new.qt_material > :old.qt_material) then
			wheb_mensagem_pck.exibir_mensagem_abort(163388);
		end if;
	end if;

	if	(nvl(:new.qt_material, 0) <> nvl(:old.qt_material, 0)) then
		ds_log_w := ds_log_w || ' qt_material old/new:' || :old.qt_material || '-' || :new.qt_material;
	end if;
	if	(nvl(:new.cd_local_estoque, 0) <> nvl(:old.cd_local_estoque, 0)) then
		ds_log_w := ds_log_w || ' cd_local_estoque old/new:' || :old.cd_local_estoque || '-' || :new.cd_local_estoque;
	end if;
	if	(nvl(:new.nr_seq_lote_fornec, 0) <> nvl(:old.nr_seq_lote_fornec, 0)) then
		ds_log_w := ds_log_w || ' nr_seq_lote_fornec old/new:' || :old.nr_seq_lote_fornec || '-' || :new.nr_seq_lote_fornec;
	end if;
	if	(nvl(:new.nr_prescricao, 0) <> nvl(:old.nr_prescricao, 0)) then
		ds_log_w := ds_log_w || ' nr_prescricao old/new:' || :old.nr_prescricao || '-' || :new.nr_prescricao;
	end if;
	if	(nvl(:new.dt_recebimento,sysdate) <> nvl(:old.dt_recebimento,sysdate)) then
		ds_log_w := ds_log_w || ' dt_recebimento old/new:' || to_char(:old.dt_recebimento,'dd/mm/yyyy hh24:mi:ss') || '-' || to_char(:new.dt_recebimento,'dd/mm/yyyy hh24:mi:ss');
	end if;
	if	(nvl(:new.cd_material, 0) <> nvl(:old.cd_material, 0)) then
		ds_log_w := ds_log_w || ' cd_material old/new:' || :old.cd_material || '-' || :new.cd_material;
	end if;
	if	(nvl(:new.cd_motivo_devolucao, 0) <> nvl(:old.cd_motivo_devolucao, 0)) then
		ds_log_w := ds_log_w || ' cd_motivo_devolucao old/new:' || :old.cd_motivo_devolucao || '-' || :new.cd_motivo_devolucao;
	end if;
	if	(nvl(:old.ie_tipo_baixa_estoque,'X') <> nvl(:new.ie_tipo_baixa_estoque,'X')) then
		ds_log_w := ds_log_w || ' ie_tipo_baixa_estoque old/new:' || :old.ie_tipo_baixa_estoque || '-' || :new.ie_tipo_baixa_estoque;
	end if;

	if	(ds_log_w is not null) then
		ds_log_w := substr(ds_log_w ||';NR_DEVOLUCAO=' || :new.nr_devolucao || 'CD_MATERIAL='|| :new.cd_material || ';call stack='|| substr(dbms_utility.format_call_stack,1,1800),1,2000);
		gravar_log_tasy(5136, ds_log_w, :new.nm_usuario);
	end if;
end if;

end;
/


ALTER TABLE TASY.ITEM_DEVOLUCAO_MATERIAL_PAC ADD (
  CONSTRAINT ITEDEVP_PK
 PRIMARY KEY
 (NR_DEVOLUCAO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ITEM_DEVOLUCAO_MATERIAL_PAC ADD (
  CONSTRAINT ITEDEMP_MOVESTO_FK 
 FOREIGN KEY (NR_MOVIMENTO_ESTOQUE) 
 REFERENCES TASY.MOVIMENTO_ESTOQUE (NR_MOVIMENTO_ESTOQUE),
  CONSTRAINT ITEDEMP_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNEC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT ITEDEMP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ITEDEMP_MOJUDE_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.MOTIVO_JUSTIFICATIVA_DEV (NR_SEQUENCIA),
  CONSTRAINT ITEDEMP_APLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.AP_LOTE (NR_SEQUENCIA),
  CONSTRAINT ITEDEMP_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_DEV) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ITEDEMP_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT ITEDEVP_DEVMATP_FK 
 FOREIGN KEY (NR_DEVOLUCAO) 
 REFERENCES TASY.DEVOLUCAO_MATERIAL_PAC (NR_DEVOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT ITEDEVP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ITEDEVP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA_RECEB) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ITEM_DEVOLUCAO_MATERIAL_PAC TO NIVEL_1;

