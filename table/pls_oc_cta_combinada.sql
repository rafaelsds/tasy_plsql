ALTER TABLE TASY.PLS_OC_CTA_COMBINADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_COMBINADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_COMBINADA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_OCORRENCIA        NUMBER(10)           NOT NULL,
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  DT_FIM_VIGENCIA          DATE,
  IE_EVENTO                VARCHAR2(3 BYTE),
  IE_PORTAL_WEB            VARCHAR2(3 BYTE),
  IE_VALIDACAO             NUMBER(5),
  IE_UTILIZA_FILTRO        VARCHAR2(1 BYTE)     NOT NULL,
  IE_PRIMEIRA_VERIFICACAO  VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  NR_SEQ_TIPO_VALIDACAO    NUMBER(10)           NOT NULL,
  IE_APLICACAO_OCORRENCIA  VARCHAR2(15 BYTE),
  NM_REGRA                 VARCHAR2(255 BYTE)   NOT NULL,
  IE_EXCECAO               VARCHAR2(1 BYTE),
  NR_SEQ_COMBINADA         NUMBER(10),
  DT_INICIO_VIGENCIA_REF   DATE,
  DT_FIM_VIGENCIA_REF      DATE,
  IE_ORIGEM_CONTA          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCCC_ESTABEL_FK_I ON TASY.PLS_OC_CTA_COMBINADA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCCC_I1 ON TASY.PLS_OC_CTA_COMBINADA
(CD_ESTABELECIMENTO, IE_EVENTO, IE_EXCECAO, IE_PORTAL_WEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCCC_I2 ON TASY.PLS_OC_CTA_COMBINADA
(NR_SEQ_COMBINADA, IE_EXCECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCCC_I3 ON TASY.PLS_OC_CTA_COMBINADA
(NR_SEQ_OCORRENCIA, NVL("NR_SEQ_COMBINADA",1), IE_EXCECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOCCC_PK ON TASY.PLS_OC_CTA_COMBINADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCCC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCCC_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_COMBINADA
(NR_SEQ_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCCC_PLSOCCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCCC_PLSOCOR_FK_I ON TASY.PLS_OC_CTA_COMBINADA
(NR_SEQ_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCCC_PLSOCOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCCC_PLSOCTV_FK_I ON TASY.PLS_OC_CTA_COMBINADA
(NR_SEQ_TIPO_VALIDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_COMBINADA_tp  after update ON TASY.PLS_OC_CTA_COMBINADA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EVENTO,1,4000),substr(:new.IE_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EVENTO',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PORTAL_WEB,1,4000),substr(:new.IE_PORTAL_WEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_PORTAL_WEB',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APLICACAO_OCORRENCIA,1,4000),substr(:new.IE_APLICACAO_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_APLICACAO_OCORRENCIA',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UTILIZA_FILTRO,1,4000),substr(:new.IE_UTILIZA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_UTILIZA_FILTRO',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRIMEIRA_VERIFICACAO,1,4000),substr(:new.IE_PRIMEIRA_VERIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIMEIRA_VERIFICACAO',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_VALIDACAO,1,4000),substr(:new.NR_SEQ_TIPO_VALIDACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_VALIDACAO',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDACAO,1,4000),substr(:new.IE_VALIDACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDACAO',ie_log_w,ds_w,'PLS_OC_CTA_COMBINADA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_oc_cta_combinada_atual
before insert or update ON TASY.PLS_OC_CTA_COMBINADA for each row
declare
ie_situacao_w	pls_oc_cta_tipo_validacao.ie_situacao%type;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S') then
	if	(obter_se_base_wheb <> 'S') then

		select	ie_situacao
		into	ie_situacao_w
		from 	pls_oc_cta_tipo_validacao
		where	nr_sequencia = :new.nr_seq_tipo_validacao;

		if	(ie_situacao_w = 'I') then

			wheb_mensagem_pck.exibir_mensagem_abort(290982);
		end if;
	end if;

	-- alimentar os campos de data referencia, isto por quest�es de performance
	-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
	-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
	-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
	-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
	-- sem precisar se preocupar se o campo vai estar nulo

	:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
	:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');
end if;
end;
/


ALTER TABLE TASY.PLS_OC_CTA_COMBINADA ADD (
  CONSTRAINT PLSOCCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_COMBINADA ADD (
  CONSTRAINT PLSOCCC_PLSOCOR_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA) 
 REFERENCES TASY.PLS_OCORRENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSOCCC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSOCCC_PLSOCTV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_VALIDACAO) 
 REFERENCES TASY.PLS_OC_CTA_TIPO_VALIDACAO (NR_SEQUENCIA),
  CONSTRAINT PLSOCCC_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_COMBINADA) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_COMBINADA TO NIVEL_1;

