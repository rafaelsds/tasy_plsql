ALTER TABLE TASY.INT_DOC_AREA_SISTEMA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INT_DOC_AREA_SISTEMA CASCADE CONSTRAINTS;

CREATE TABLE TASY.INT_DOC_AREA_SISTEMA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INTEGRACAO    NUMBER(10)               NOT NULL,
  DS_AREA              VARCHAR2(80 BYTE)        NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTDASI_INTDOCT_FK_I ON TASY.INT_DOC_AREA_SISTEMA
(NR_SEQ_INTEGRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTDASI_INTDOCT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.INTDASI_PK ON TASY.INT_DOC_AREA_SISTEMA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTDASI_PK
  MONITORING USAGE;


ALTER TABLE TASY.INT_DOC_AREA_SISTEMA ADD (
  CONSTRAINT INTDASI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INT_DOC_AREA_SISTEMA ADD (
  CONSTRAINT INTDASI_INTDOCT_FK 
 FOREIGN KEY (NR_SEQ_INTEGRACAO) 
 REFERENCES TASY.INT_DOCUMENTACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INT_DOC_AREA_SISTEMA TO NIVEL_1;

