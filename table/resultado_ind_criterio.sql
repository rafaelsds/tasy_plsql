ALTER TABLE TASY.RESULTADO_IND_CRITERIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESULTADO_IND_CRITERIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESULTADO_IND_CRITERIO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_TABELA_CUSTO        NUMBER(5),
  NR_SEQ_INDICADOR       NUMBER(10)             NOT NULL,
  CD_SEQUENCIA_CRITERIO  NUMBER(10)             NOT NULL,
  NR_SEQ_TABELA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RESINCR_CRIDIOR_FK_I ON TASY.RESULTADO_IND_CRITERIO
(CD_SEQUENCIA_CRITERIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESINCR_CRIDIOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RESINCR_ESTABEL_FK_I ON TASY.RESULTADO_IND_CRITERIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RESINCR_PK ON TASY.RESULTADO_IND_CRITERIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESINCR_PK
  MONITORING USAGE;


CREATE INDEX TASY.RESINCR_RESINDI_FK_I ON TASY.RESULTADO_IND_CRITERIO
(NR_SEQ_INDICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESINCR_RESINDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RESINCR_TABCUST_FK_I ON TASY.RESULTADO_IND_CRITERIO
(CD_ESTABELECIMENTO, CD_TABELA_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESINCR_TABCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RESINCR_TABCUST_FK_I2 ON TASY.RESULTADO_IND_CRITERIO
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESINCR_TABCUST_FK_I2
  MONITORING USAGE;


ALTER TABLE TASY.RESULTADO_IND_CRITERIO ADD (
  CONSTRAINT RESINCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESULTADO_IND_CRITERIO ADD (
  CONSTRAINT RESINCR_CRIDIOR_FK 
 FOREIGN KEY (CD_SEQUENCIA_CRITERIO) 
 REFERENCES TASY.CRITERIO_DISTR_ORC (CD_SEQUENCIA_CRITERIO)
    ON DELETE CASCADE,
  CONSTRAINT RESINCR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RESINCR_RESINDI_FK 
 FOREIGN KEY (NR_SEQ_INDICADOR) 
 REFERENCES TASY.RESULTADO_INDICADOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT RESINCR_TABCUST_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.TABELA_CUSTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RESULTADO_IND_CRITERIO TO NIVEL_1;

