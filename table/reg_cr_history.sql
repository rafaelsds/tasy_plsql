ALTER TABLE TASY.REG_CR_HISTORY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_CR_HISTORY CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_CR_HISTORY
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CODE              VARCHAR2(255 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TITLE             VARCHAR2(255 BYTE)       NOT NULL,
  DT_APROVACAO         DATE,
  NR_SEQ_APRESENTACAO  NUMBER(10,3),
  NR_SEQ_CR            NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_VERSAO            VARCHAR2(255 BYTE),
  NR_SEQ_FEATURES      NUMBER(10)               NOT NULL,
  DS_RATIONALE         VARCHAR2(4000 BYTE)      NOT NULL,
  NR_SEQ_ESTAGIO       NUMBER(10),
  NM_CURTO             VARCHAR2(255 BYTE),
  NM_LIBERACAO_VV      VARCHAR2(15 BYTE),
  DT_LIBERACAO_VV      DATE,
  NR_REVISION          NUMBER(10),
  IE_REVISION_TYPE     VARCHAR2(1 BYTE),
  DS_DESCRICAO_CR      VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGCRHIS_PK ON TASY.REG_CR_HISTORY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCRHIS_REGCR_FK_I ON TASY.REG_CR_HISTORY
(NR_SEQ_CR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_CR_HISTORY ADD (
  CONSTRAINT REGCRHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_CR_HISTORY ADD (
  CONSTRAINT REGCRHIS_REGCR_FK 
 FOREIGN KEY (NR_SEQ_CR) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_CR_HISTORY TO NIVEL_1;

