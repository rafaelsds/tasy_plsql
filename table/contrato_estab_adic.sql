ALTER TABLE TASY.CONTRATO_ESTAB_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTRATO_ESTAB_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTRATO_ESTAB_ADIC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO         NUMBER(10)            NOT NULL,
  CD_ESTAB_ADIC           NUMBER(4)             NOT NULL,
  IE_GERAR_NF             VARCHAR2(15 BYTE)     NOT NULL,
  IE_ENVIAR_EMAIL_OC      VARCHAR2(1 BYTE),
  DS_EMAIL                VARCHAR2(255 BYTE),
  DS_CONTATO              VARCHAR2(40 BYTE),
  QT_DIAS_ENTREGA         NUMBER(5),
  CD_PESSOA_RESP          VARCHAR2(10 BYTE),
  NR_SEQ_CONTRATO_GESTAO  NUMBER(10),
  VL_CONTRATO_GESTAO      NUMBER(13,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONESAD_CONTGEST_FK_I ON TASY.CONTRATO_ESTAB_ADIC
(NR_SEQ_CONTRATO_GESTAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONESAD_CONTGEST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONESAD_CONTRAT_FK_I ON TASY.CONTRATO_ESTAB_ADIC
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONESAD_ESTABEL_FK_I ON TASY.CONTRATO_ESTAB_ADIC
(CD_ESTAB_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONESAD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONESAD_PESFISI_FK_I ON TASY.CONTRATO_ESTAB_ADIC
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONESAD_PK ON TASY.CONTRATO_ESTAB_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONESAD_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONTRATO_ESTAB_ADIC ADD (
  CONSTRAINT CONESAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTRATO_ESTAB_ADIC ADD (
  CONSTRAINT CONESAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_ADIC) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONESAD_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONESAD_CONTGEST_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO_GESTAO) 
 REFERENCES TASY.CONTRATO_GESTAO (NR_SEQUENCIA),
  CONSTRAINT CONESAD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CONTRATO_ESTAB_ADIC TO NIVEL_1;

