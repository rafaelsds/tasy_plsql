ALTER TABLE TASY.PLS_TIPO_CARENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_CARENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_CARENCIA
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  DS_CARENCIA                    VARCHAR2(255 BYTE) NOT NULL,
  IE_CPT                         VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  IE_PADRAO                      VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_IMPACTO                VARCHAR2(2 BYTE) NOT NULL,
  QT_DIAS_PADRAO                 NUMBER(5),
  NR_SEQ_CLASSIF_CPT             NUMBER(10),
  CD_CARENCIA                    VARCHAR2(3 BYTE),
  IE_UTILIZACAO                  VARCHAR2(2 BYTE),
  CD_SISTEMA_ANTERIOR            VARCHAR2(100 BYTE),
  IE_SIGLA                       VARCHAR2(100 BYTE),
  NR_SEQ_CARENCIA_ANT            NUMBER(10),
  IE_PADRAO_CARTEIRA             VARCHAR2(1 BYTE),
  CD_PTU                         VARCHAR2(10 BYTE),
  IE_TIPO_OPERACAO               VARCHAR2(1 BYTE),
  NR_APRESENTACAO                NUMBER(10),
  IE_SEXO                        VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO                   NUMBER(10),
  NR_SEQ_CLASSIF_CARENCIA        NUMBER(10),
  IE_CARENCIA_REDE               VARCHAR2(1 BYTE),
  IE_COBERTURA                   VARCHAR2(1 BYTE),
  IE_CARENCIA_ESPECIFICA_ACOMOD  VARCHAR2(1 BYTE),
  DS_CARENCIA_CARTEIRA           VARCHAR2(255 BYTE),
  DS_INFORMACAO                  VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTICA_ESTABEL_FK_I ON TASY.PLS_TIPO_CARENCIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTICA_I1 ON TASY.PLS_TIPO_CARENCIA
(NR_SEQUENCIA, IE_CPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSTICA_PK ON TASY.PLS_TIPO_CARENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTICA_PLSCCPT_FK_I ON TASY.PLS_TIPO_CARENCIA
(NR_SEQ_CLASSIF_CPT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTICA_PLSCCPT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTICA_PLSCLCA_FK_I ON TASY.PLS_TIPO_CARENCIA
(NR_SEQ_CLASSIF_CARENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTICA_PLSCLCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTICA_PLSGRCA_FK_I ON TASY.PLS_TIPO_CARENCIA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TIPO_CARENCIA ADD (
  CONSTRAINT PLSTICA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_CARENCIA ADD (
  CONSTRAINT PLSTICA_PLSCLCA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_CARENCIA) 
 REFERENCES TASY.PLS_CLASSIFICACAO_CARENCIA (NR_SEQUENCIA),
  CONSTRAINT PLSTICA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTICA_PLSCCPT_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_CPT) 
 REFERENCES TASY.PLS_CLASSIFICACAO_CPT (NR_SEQUENCIA),
  CONSTRAINT PLSTICA_PLSGRCA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PLS_GRUPO_CARENCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TIPO_CARENCIA TO NIVEL_1;

