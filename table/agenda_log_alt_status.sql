ALTER TABLE TASY.AGENDA_LOG_ALT_STATUS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_LOG_ALT_STATUS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_LOG_ALT_STATUS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO            DATE,
  DT_FINAL             DATE,
  DS_STACK             VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGLGALS_I1 ON TASY.AGENDA_LOG_ALT_STATUS
(DT_INICIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLGALS_I2 ON TASY.AGENDA_LOG_ALT_STATUS
(NR_SEQUENCIA, DT_FINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLGALS_I3 ON TASY.AGENDA_LOG_ALT_STATUS
(DT_FINAL, 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGLGALS_PK ON TASY.AGENDA_LOG_ALT_STATUS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLGALS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_log_alt_status_atual
before insert or update ON TASY.AGENDA_LOG_ALT_STATUS for each row
declare

begin

if (:old.ds_stack is not null) then
	:new.ds_stack := substr(obter_desc_expressao(645952)||': '||:old.ds_stack||obter_desc_expressao(487898)||': '||dbms_utility.format_call_stack,1,4000);
else
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,4000);
end if;

end;
/


ALTER TABLE TASY.AGENDA_LOG_ALT_STATUS ADD (
  CONSTRAINT AGLGALS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AGENDA_LOG_ALT_STATUS TO NIVEL_1;

