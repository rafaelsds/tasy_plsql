ALTER TABLE TASY.REG_ESCOPO_CUSTOMER_REQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_ESCOPO_CUSTOMER_REQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_ESCOPO_CUSTOMER_REQ
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_ESCOPO           NUMBER(10)            NOT NULL,
  NR_CUSTOMER_REQ         NUMBER(10)            NOT NULL,
  IE_STATUS_CCB           VARCHAR2(5 BYTE),
  NR_SEQ_ANALISE_IMPACTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGECRE_MANOSIM_FK_I ON TASY.REG_ESCOPO_CUSTOMER_REQ
(NR_SEQ_ANALISE_IMPACTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGECRE_PK ON TASY.REG_ESCOPO_CUSTOMER_REQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGECRE_REGCR_FK_I ON TASY.REG_ESCOPO_CUSTOMER_REQ
(NR_CUSTOMER_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGECRE_REGESCO_FK_I ON TASY.REG_ESCOPO_CUSTOMER_REQ
(NR_SEQ_ESCOPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_escopo_customer_after
after insert ON TASY.REG_ESCOPO_CUSTOMER_REQ for each row
declare

cursor c_test_cases is
select	ct.nr_sequencia
from	reg_caso_teste ct
where	ct.nr_seq_customer = :new.nr_customer_req
and	not exists (	select	1
			from	reg_escopo_caso_teste
			where	nr_seq_caso_teste = ct.nr_sequencia
			and	nr_seq_escopo = :new.nr_seq_escopo
		);

begin

for r_c_test_cases in c_test_cases loop

	insert into reg_escopo_caso_teste (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_escopo,
		nr_seq_caso_teste
	) values (
		reg_escopo_caso_teste_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_seq_escopo,
		r_c_test_cases.nr_sequencia
	);

end loop;

end;
/


CREATE OR REPLACE TRIGGER TASY.reg_escopo_customer_before
before delete ON TASY.REG_ESCOPO_CUSTOMER_REQ for each row
declare

qt_test_case_scope_w			number(10);

begin

select	count(1)
into	qt_test_case_scope_w
from	reg_caso_teste ct,
	reg_escopo_caso_teste ect
where	ct.nr_sequencia = ect.nr_seq_caso_teste
and	ct.nr_seq_customer = :old.nr_customer_req
and	ect.nr_seq_escopo = :old.nr_seq_escopo;

if (qt_test_case_scope_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1016417); -- Este escopo j� est� incluso nos casos de teste deste requisito de cliente. A exclus�o deste deve ser verificada com o time de Valida��o.
end if;

end;
/


ALTER TABLE TASY.REG_ESCOPO_CUSTOMER_REQ ADD (
  CONSTRAINT REGECRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_ESCOPO_CUSTOMER_REQ ADD (
  CONSTRAINT REGECRE_MANOSIM_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_IMPACTO) 
 REFERENCES TASY.MAN_ORDEM_SERV_IMPACTO (NR_SEQUENCIA),
  CONSTRAINT REGECRE_REGCR_FK 
 FOREIGN KEY (NR_CUSTOMER_REQ) 
 REFERENCES TASY.REG_CUSTOMER_REQUIREMENT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGECRE_REGESCO_FK 
 FOREIGN KEY (NR_SEQ_ESCOPO) 
 REFERENCES TASY.REG_ESCOPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_ESCOPO_CUSTOMER_REQ TO NIVEL_1;

