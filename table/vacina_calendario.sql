ALTER TABLE TASY.VACINA_CALENDARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VACINA_CALENDARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.VACINA_CALENDARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_DOSE              VARCHAR2(3 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE),
  QT_DOSE              NUMBER(9,3),
  CD_UNID_MEDIDA       VARCHAR2(30 BYTE),
  QT_MES               NUMBER(4,2),
  QT_PROX_MES          NUMBER(5,2),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  DS_REACAO_ADVERSA    VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_VACINA        NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  QT_IDADE_MINIMA      NUMBER(10),
  QT_IDADE_MAXIMA      NUMBER(10),
  IE_DOSE_REFERENCIA   VARCHAR2(3 BYTE),
  QT_IDADE_MINIMA_MES  NUMBER(15),
  QT_IDADE_MINIMA_ANO  NUMBER(15),
  QT_IDADE_MAXIMA_MES  NUMBER(15),
  QT_IDADE_MAXIMA_ANO  NUMBER(15),
  QT_INTERVALO_ANOS    NUMBER(3),
  QT_INTERVALO_DIAS    NUMBER(3),
  QT_INTERVALO_MESES   NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.VACCALE_PK ON TASY.VACINA_CALENDARIO
(NR_SEQUENCIA, IE_DOSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.VACCALE_PROCEDI_FK_I ON TASY.VACINA_CALENDARIO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VACCALE_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.VACCALE_VACINA_FK_I ON TASY.VACINA_CALENDARIO
(NR_SEQ_VACINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.VACCALE_VIAAPLI_FK_I ON TASY.VACINA_CALENDARIO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VACCALE_VIAAPLI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.VACINA_CALENDARIO ADD (
  CONSTRAINT VACCALE_PK
 PRIMARY KEY
 (NR_SEQUENCIA, IE_DOSE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.VACINA_CALENDARIO ADD (
  CONSTRAINT VACCALE_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED)
    ON DELETE CASCADE,
  CONSTRAINT VACCALE_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT VACCALE_VACINA_FK 
 FOREIGN KEY (NR_SEQ_VACINA) 
 REFERENCES TASY.VACINA (NR_SEQUENCIA));

GRANT SELECT ON TASY.VACINA_CALENDARIO TO NIVEL_1;

