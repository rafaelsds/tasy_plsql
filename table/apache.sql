ALTER TABLE TASY.APACHE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.APACHE CASCADE CONSTRAINTS;

CREATE TABLE TASY.APACHE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_TEMP_RETAL             NUMBER(6,2),
  QT_PAL_MEDIA              NUMBER(6,2),
  QT_FREQ_CARD              NUMBER(6,2),
  QT_FREQ_RESP              NUMBER(6,2),
  QT_OXIGENACAO_FIO2        NUMBER(6,2),
  QT_OXIGENACAO_PAAO2       NUMBER(15,1),
  QT_OXIGENACAO_PAO2        NUMBER(6,2),
  QT_PH_ARTERIAL            NUMBER(5,2),
  QT_SODIO_SERICO           NUMBER(6,2),
  QT_POTASSIO_SERICO        NUMBER(6,2),
  QT_CREATININA             NUMBER(6,2),
  QT_HEMATOCRITO            NUMBER(6,2),
  QT_GLOBULOS_BRANCOS       NUMBER(15,1),
  QT_ESCALA_GLASGOW         NUMBER(6,2),
  IE_IRA                    VARCHAR2(1 BYTE),
  IE_PO_URGENCIA            VARCHAR2(1 BYTE),
  QT_PONT_VAR_FISIOLOGICAS  NUMBER(7,3),
  QT_PONT_IDADE             NUMBER(7,3),
  QT_PONT_DOENCA_CRONICA    NUMBER(7,3),
  QT_APACHE_II              NUMBER(7,3),
  QT_RISCO_CALCULADO        NUMBER(7,3),
  DT_APACHE                 DATE                NOT NULL,
  QT_TEMP_AXILAR            NUMBER(6,2),
  QT_OXIGENACAO_PCO2        NUMBER(6,2),
  PR_RISCO                  NUMBER(7,3),
  QT_BICARBONATO_SERICO     NUMBER(5,2),
  QT_PTO_TEMP               NUMBER(3),
  QT_PTO_PA_MED             NUMBER(3),
  QT_PTO_FC                 NUMBER(3),
  QT_PTO_FR                 NUMBER(3),
  QT_PTO_OXIG               NUMBER(3),
  QT_PTO_PH_ART             NUMBER(3),
  QT_PTO_SODIO_SERICO       NUMBER(3),
  QT_PTO_POT_SERICO         NUMBER(3),
  QT_PTO_CREATININA         NUMBER(3),
  QT_PTO_HEMATOCRITO        NUMBER(3),
  QT_PTO_GLOB_BRANCO        NUMBER(3),
  QT_PTO_GLASGOW            NUMBER(3),
  QT_PONTO_BICARB_SERICO    NUMBER(3),
  NR_SEQ_CAT_DIAG           NUMBER(10),
  QT_PESO_CAT_DIAG          NUMBER(7,3),
  QT_PB                     NUMBER(15,4)        NOT NULL,
  QT_PAD                    NUMBER(3),
  QT_PAS                    NUMBER(3),
  DT_LIBERACAO              DATE,
  IE_TIPO_CATEGORIA         NUMBER(1),
  CD_SETOR_PACIENTE         NUMBER(5),
  CD_PERFIL_ATIVO           NUMBER(5),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  NR_HORA                   NUMBER(2),
  CD_PROFISSIONAL           VARCHAR2(10 BYTE),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APACHE_APACADI_FK_I ON TASY.APACHE
(NR_SEQ_CAT_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APACHE_APACADI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APACHE_ATEPACI_FK_I ON TASY.APACHE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APACHE_I1 ON TASY.APACHE
(DT_APACHE, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APACHE_PERFIL_FK_I ON TASY.APACHE
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APACHE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APACHE_PESFISI_FK_I ON TASY.APACHE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.APACHE_PK ON TASY.APACHE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APACHE_SETATEN_FK_I ON TASY.APACHE
(CD_SETOR_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APACHE_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.APACHE_Update
BEFORE INSERT OR UPDATE ON TASY.APACHE FOR EACH ROW
DECLARE

qt_pto_po_urgencia_w		Number(15,5)	:= 0;
qt_ano_w			Number(15,5)	:= 0;
qt_pto_art_bic_w		Number(15,5)	:= 0;

BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_APACHE <> :old.DT_APACHE) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_APACHE,'hh24'),'hh24'));
	end;
end if;

if	inserting then
	:new.cd_setor_paciente := obter_setor_atendimento(:new.nr_atendimento);
end if;

begin
:new.qt_pal_media	:= (((:new.qt_pad * 2) + :new.qt_pas) / 3);
exception
	when others then
		:new.qt_pal_media	:= null;
end;

if	(nvl(:new.qt_temp_axilar,0) <> nvl(:old.qt_temp_axilar,0)) then
	:new.qt_temp_retal		:= :new.qt_temp_axilar + 0.8;
elsif	(nvl(:new.qt_temp_retal,0) <> nvl(:old.qt_temp_retal,0)) then
	:new.qt_temp_axilar		:= :new.qt_temp_retal - 0.8;
end if;

if	(nvl(:new.qt_escala_glasgow,99) <= 15) then
	:new.qt_pto_glasgow	:= 15 - :new.qt_escala_glasgow;
end if;

:new.qt_pto_temp		:= obter_ponto_apache(:new.qt_temp_retal,1);
:new.qt_pto_PA_med		:= obter_ponto_apache(:new.qt_pal_media,2);
:new.qt_pto_FC			:= obter_ponto_apache(:new.qt_freq_card,3);
:new.qt_pto_FR			:= obter_ponto_apache(:new.qt_freq_resp,4);
:new.qt_pto_ph_art		:= obter_ponto_apache(:new.qt_ph_arterial,5);
:new.qt_pto_sodio_serico	:= obter_ponto_apache(:new.qt_sodio_serico,6);
:new.qt_pto_pot_serico		:= obter_ponto_apache(:new.qt_potassio_serico,7);
:new.qt_pto_hematocrito		:= obter_ponto_apache(:new.qt_hematocrito,8);
:new.qt_pto_glob_branco		:= obter_ponto_apache((:new.qt_globulos_brancos / 1000),9);
:new.qt_ponto_bicarb_serico	:= obter_ponto_apache(:new.qt_bicarbonato_serico,10);

if	(:new.qt_creatinina >= 3.5) then
	:new.qt_pto_creatinina	:= 4;
elsif	(:new.qt_creatinina >= 2) then
	:new.qt_pto_creatinina	:= 3;
elsif	(:new.qt_creatinina >= 1.5) then
	:new.qt_pto_creatinina	:= 2;
elsif	(:new.qt_creatinina < 0.6) then
	:new.qt_pto_creatinina	:= 2;
else
	:new.qt_pto_creatinina	:= 0;
end if;
if	(:new.ie_ira = 'S') then
	:new.qt_pto_creatinina	:= :new.qt_pto_creatinina * 2;
end if;

begin
:new.qt_oxigenacao_paao2	:=
		((nvl(:new.qt_pb,760) - 47) * (:new.qt_oxigenacao_fio2 / 100) - (:new.qt_oxigenacao_pco2 / 0.8)) -
		:new.qt_oxigenacao_pao2;
exception
	when others then
		:new.qt_oxigenacao_paao2	:= 0;
end;

if	(nvl(:new.qt_oxigenacao_fio2,0) = 0) and
	(nvl(:new.qt_oxigenacao_paao2,0) = 0) and
	(nvl(:new.qt_oxigenacao_pao2,0) = 0) and
	(nvl(:new.qt_oxigenacao_pco2,0) = 0) then
	:new.qt_pto_oxig		:= 0;
elsif	(nvl((:new.qt_oxigenacao_fio2 / 100),0) >= 0.5) then
	begin
	if	(:new.qt_oxigenacao_paao2 >= 500) then
		:new.qt_pto_oxig	:= 4;
	elsif	(:new.qt_oxigenacao_paao2 >= 350) then
		:new.qt_pto_oxig	:= 3;
	elsif	(:new.qt_oxigenacao_paao2 >= 200) then
		:new.qt_pto_oxig	:= 2;
	else
		:new.qt_pto_oxig	:= 0;
	end if;
	end;
else
	begin
	:new.qt_oxigenacao_pao2	:= nvl(:new.qt_oxigenacao_pao2,0);
	if	(:new.qt_oxigenacao_pao2 > 70) then
		:new.qt_pto_oxig	:= 0;
	elsif	(:new.qt_oxigenacao_pao2 >= 61) then
		:new.qt_pto_oxig	:= 1;
	elsif	(:new.qt_oxigenacao_pao2 >= 55) then
		:new.qt_pto_oxig	:= 3;
	else
		:new.qt_pto_oxig	:= 4;
	end if;
	end;
end if;

if	(:new.qt_pto_ph_art is not null) then
	qt_pto_art_bic_w := :new.qt_pto_ph_art;
else
	qt_pto_art_bic_w := :new.qt_ponto_bicarb_serico;
end if;


:new.QT_PONT_VAR_FISIOLOGICAS	:=
	nvl(:new.qt_pto_temp,0) +
	nvl(:new.qt_pto_PA_med,0) +
	nvl(:new.qt_pto_FC,0) +
	nvl(:new.qt_pto_FR,0) +
	nvl(:new.qt_pto_oxig,0) +
	nvl(qt_pto_art_bic_w,0) +
	nvl(:new.qt_pto_sodio_serico,0) +
	nvl(:new.qt_pto_pot_serico,0) +
	nvl(:new.qt_pto_creatinina,0) +
	nvl(:new.qt_pto_hematocrito,0) +
	nvl(:new.qt_pto_glob_branco,0) +
	nvl(:new.qt_pto_glasgow,0);

/* Edilson em 09/06/2008 OS 93835 */
qt_pto_po_urgencia_w		:= 0;
if	(:new.ie_po_urgencia = 'S') then
	qt_pto_po_urgencia_w	:= 0.603;
end if;

if	(nvl(:new.qt_pont_idade,0) = 0) then
	select	nvl(max(campo_numerico(obter_idade(dt_nascimento,:new.dt_apache,'A'))),0)
	into	qt_ano_w
	from	pessoa_fisica b,
		atendimento_paciente a
	where	a.nr_atendimento	= :new.nr_atendimento
	and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;
	if	(qt_ano_w >= 75) then
		:new.qt_pont_idade	:= 6;
	elsif	(qt_ano_w >= 65) then
		:new.qt_pont_idade	:= 5;
	elsif	(qt_ano_w >= 55) then
		:new.qt_pont_idade	:= 3;
	elsif	(qt_ano_w >= 45) then
		:new.qt_pont_idade	:= 2;
	else
		:new.qt_pont_idade	:= 0;
	end if;
end if;

select	nvl(max(qt_pontuacao),0)
into	:new.qt_peso_cat_diag
from	apache_categ_diag
where	nr_sequencia	= :new.nr_seq_cat_diag;

:new.qt_apache_ii	:=
	:new.qt_pont_var_fisiologicas +
	:new.qt_pont_idade +
	:new.qt_pont_doenca_cronica;

:new.qt_risco_calculado	:=	-3.517 + (:new.qt_apache_ii * 0.146);	/*	Risco Calculado	*/
:new.qt_risco_calculado	:=	exp(:new.qt_risco_calculado) / (1 + exp(:new.qt_risco_calculado)) * 100;	/*	Risco Calculado	*/

:new.pr_risco	:=	-3.517 + (:new.qt_apache_ii * 0.146) + qt_pto_po_urgencia_w  +	:new.qt_peso_cat_diag;	/*	Risco ajustado	*/
:new.pr_risco	:=	exp(:new.pr_risco) / (1 + exp(:new.pr_risco)) * 100;	/*	Risco ajustado	*/
END;
/


ALTER TABLE TASY.APACHE ADD (
  CONSTRAINT APACHE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.APACHE ADD (
  CONSTRAINT APACHE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT APACHE_APACADI_FK 
 FOREIGN KEY (NR_SEQ_CAT_DIAG) 
 REFERENCES TASY.APACHE_CATEG_DIAG (NR_SEQUENCIA),
  CONSTRAINT APACHE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_PACIENTE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT APACHE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APACHE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.APACHE TO NIVEL_1;

