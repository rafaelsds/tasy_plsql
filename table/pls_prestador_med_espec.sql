ALTER TABLE TASY.PLS_PRESTADOR_MED_ESPEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_MED_ESPEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_MED_ESPEC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_PRESTADOR        NUMBER(10)            NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  CD_ESPECIALIDADE        NUMBER(5)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_AREA_ATUACAO     NUMBER(3),
  IE_PUBLICACAO           VARCHAR2(3 BYTE),
  IE_PTU                  VARCHAR2(1 BYTE)      NOT NULL,
  IE_GUIA_MEDICO          VARCHAR2(1 BYTE)      NOT NULL,
  IE_RCE_ATUACAO          VARCHAR2(1 BYTE)      NOT NULL,
  IE_PUBLICACAO_ARQ_GUIA  VARCHAR2(3 BYTE),
  IE_RQE_ESPEC            VARCHAR2(1 BYTE),
  IE_ENDERECO_PRINCIPAL   VARCHAR2(1 BYTE),
  IE_ESPECIALIDADE_PRINC  VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  IE_TITULO_AMB           VARCHAR2(1 BYTE),
  DT_TITULO_AMB           DATE,
  IE_TITULO_CNRM          VARCHAR2(1 BYTE),
  DT_TITULO_CNRM          DATE,
  CD_TITULACAO_PTU        VARCHAR2(20 BYTE),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPMEE_AREATUM_FK_I ON TASY.PLS_PRESTADOR_MED_ESPEC
(NR_SEQ_AREA_ATUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPMEE_ESPMEDI_FK_I ON TASY.PLS_PRESTADOR_MED_ESPEC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPMEE_I1 ON TASY.PLS_PRESTADOR_MED_ESPEC
(IE_GUIA_MEDICO, NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPMEE_PESFISI_FK_I ON TASY.PLS_PRESTADOR_MED_ESPEC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPMEE_PK ON TASY.PLS_PRESTADOR_MED_ESPEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPMEE_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_MED_ESPEC
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_PRESTADOR_MED_ESPEC_tp  after update ON TASY.PLS_PRESTADOR_MED_ESPEC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_ESPECIALIDADE,1,500);gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'PLS_PRESTADOR_MED_ESPEC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR_MED_ESPEC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_med_espec_atual
before insert or update ON TASY.PLS_PRESTADOR_MED_ESPEC for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_prestador_med_espec_atual;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_med_espec_delete
after delete ON TASY.PLS_PRESTADOR_MED_ESPEC for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PRESTADOR_MED_ESPEC',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_inserir_atenc_saude_espec
after insert or update ON TASY.PLS_PRESTADOR_MED_ESPEC for each row
declare
cursor C01(especialidade_p Number) is
	select	ie_tp_atencao_saude,
		dt_inicio_vigencia,
		dt_fim_vigencia
	from	pls_atencao_saude
	where	cd_especialidade = especialidade_p;


begin
delete	pls_prest_atencao_saude
where	nr_seq_prest_med_espec = :old.nr_sequencia;

for	r_c01_w	in	C01( :new.cd_especialidade ) loop
	insert into pls_prest_atencao_saude(	nr_sequencia,
						nr_seq_prest_med_espec,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_tp_atencao_saude,
						dt_inicio_vigencia,
						dt_fim_vigencia)
	values (pls_prest_atencao_saude_seq.nextval,
		:new.nr_sequencia,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		r_c01_w.ie_tp_atencao_saude,
		nvl(r_c01_w.dt_inicio_vigencia, sysdate),
		r_c01_w.dt_fim_vigencia);
end loop;

end;
/


ALTER TABLE TASY.PLS_PRESTADOR_MED_ESPEC ADD (
  CONSTRAINT PLSPMEE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_MED_ESPEC ADD (
  CONSTRAINT PLSPMEE_AREATUM_FK 
 FOREIGN KEY (NR_SEQ_AREA_ATUACAO) 
 REFERENCES TASY.AREA_ATUACAO_MEDICA (NR_SEQUENCIA),
  CONSTRAINT PLSPMEE_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPMEE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSPMEE_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.PLS_PRESTADOR_MED_ESPEC TO NIVEL_1;

