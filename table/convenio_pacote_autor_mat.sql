ALTER TABLE TASY.CONVENIO_PACOTE_AUTOR_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_PACOTE_AUTOR_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_PACOTE_AUTOR_MAT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PACOTE         NUMBER(10)              NOT NULL,
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  NR_SEQ_FAMILIA        NUMBER(10),
  CD_MATERIAL           NUMBER(10),
  QT_MATERIAL           NUMBER(15,3),
  TX_PAGAMENTO          NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CNVPCTMT_CLAMATE_FK_I ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CNVPCTMT_CNVPCTAT_FK_I ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CNVPCTMT_GRUMATE_FK_I ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CNVPCTMT_MATERIA_FK_I ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CNVPCTMT_MATFAMI_FK_I ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CNVPCTMT_PK ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CNVPCTMT_SUBMATE_FK_I ON TASY.CONVENIO_PACOTE_AUTOR_MAT
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONVENIO_PACOTE_AUTOR_MAT ADD (
  CONSTRAINT CNVPCTMT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_PACOTE_AUTOR_MAT ADD (
  CONSTRAINT CNVPCTMT_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT CNVPCTMT_CNVPCTAT_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.CONVENIO_PACOTE_AUTOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CNVPCTMT_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT CNVPCTMT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT CNVPCTMT_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT CNVPCTMT_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.CONVENIO_PACOTE_AUTOR_MAT TO NIVEL_1;

