ALTER TABLE TASY.HD_PAC_RENAL_CRONICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_PAC_RENAL_CRONICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_PAC_RENAL_CRONICO
(
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  DT_INICIO               DATE,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_FIM                  DATE,
  IE_PODE_PESAR           VARCHAR2(1 BYTE),
  DS_MOTIVO_FIM           VARCHAR2(255 BYTE),
  NR_SEQ_UNID_DIALISE     NUMBER(10)            NOT NULL,
  IE_DIABETICO            VARCHAR2(1 BYTE)      NOT NULL,
  DT_INICIO_TRATAMENTO    DATE,
  IE_PACIENTE_AGUDO       VARCHAR2(1 BYTE),
  QT_PESO_IDEAL           NUMBER(6,3),
  IE_PACIENTE_TRANSITO    VARCHAR2(1 BYTE),
  NR_SEQ_MAQUINA          NUMBER(10),
  NR_SEQ_PONTO_ACESSO     NUMBER(10),
  CD_CONVENIO             NUMBER(5),
  IE_HIPERTENSO           VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  NR_SEQ_UNIDADE_ATUAL    NUMBER(10),
  IE_PACIENTE_CHEGOU      VARCHAR2(1 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  CD_ESTAGIO_HIPERTENSAO  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDPAREC_CONVENI_FK_I ON TASY.HD_PAC_RENAL_CRONICO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDPAREC_ESTABEL_FK_I ON TASY.HD_PAC_RENAL_CRONICO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDPAREC_HDUNIDD_FK1_I ON TASY.HD_PAC_RENAL_CRONICO
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDPAREC_HDUNIDD_FK2_I ON TASY.HD_PAC_RENAL_CRONICO
(NR_SEQ_UNIDADE_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDPAREC_PK ON TASY.HD_PAC_RENAL_CRONICO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_pac_renal_cronico_insert
before insert ON TASY.HD_PAC_RENAL_CRONICO for each row
declare

cd_perfil_w			number(5);
ie_insere_tratamento_w		varchar2(1);
ie_pac_agudo_w			varchar2(1);
ie_nova_forma_w			varchar2(1);
nr_seq_protocolo_w		number(10);
ie_tipo_paciente_w		varchar2(1);
begin
ie_insere_tratamento_w := Obter_Valor_Param_Usuario(7009, 126, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento);
ie_pac_agudo_w := obter_valor_param_usuario(7009, 141, Obter_Perfil_Ativo, :new.nm_usuario, :new.cd_estabelecimento);
:new.ie_situacao := 'S';

:new.nr_seq_unidade_atual := :new.NR_SEQ_UNID_DIALISE;

if (ie_insere_tratamento_w = 'S') then

	insert into paciente_tratamento (
		nr_sequencia,
		cd_pessoa_fisica,
		ie_tratamento,
		dt_inicio_tratamento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_unid_dialise,
		ie_paciente_agudo
	) values (
		paciente_tratamento_seq.nextval,
		:new.cd_pessoa_fisica,
		'HD',
		sysdate,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.NR_SEQ_UNID_DIALISE,
		decode(ie_pac_agudo_w,'S','S',:new.ie_paciente_agudo)
	);



	select	max(ie_nova_forma_dialise)
	into	ie_nova_forma_w
	from	hd_parametro
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if (ie_nova_forma_w = 'S') then

		select	max(nr_sequencia)
		into	nr_seq_protocolo_w
		from	hd_prot_exame_padrao
		where	ie_tratamento = 'HD'
		and		(cd_estabelecimento = :new.cd_estabelecimento or cd_estabelecimento is null)
		and		(cd_convenio = :new.cd_convenio or cd_convenio is null)
		and		ie_situacao = 'A'
		and		((nvl(ie_tipo_paciente,'A') = decode(ie_pac_agudo_w,'S','S',:new.ie_paciente_agudo)) or (nvl(ie_tipo_paciente,'A') = 'A'));

		if (nr_seq_protocolo_w is not null) then
			insert into hd_protocolo_exame (nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_protocolo,
							ie_tratamento,
							cd_pessoa_fisica,
							ie_manual)
						values(	hd_protocolo_exame_seq.nextval,
							sysdate,
							:new.nm_usuario,
							sysdate,
							:new.nm_usuario,
							nr_seq_protocolo_w,
							'HD',
							:new.cd_pessoa_fisica,
							'N');
		end if;
	end if;
end if;

begin
select	cd_perfil_paciente
into	cd_perfil_w
from	hd_parametro
where	cd_estabelecimento = :new.cd_estabelecimento;
exception
	when others then
	cd_perfil_w	:= 0;
end;

if 	(cd_perfil_w > 0) then

	if 	(:new.cd_pessoa_fisica is not null) then
		insert into comunic_interna (
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			ds_perfil_adicional,
			nr_sequencia,
			ie_gerencial,
			dt_liberacao,
			cd_estab_destino
		) values (
			sysdate,
			wheb_mensagem_pck.get_texto(802538) || ' ' || substr(obter_valor_dominio(2197,'HD'),1,80),
			wheb_mensagem_pck.get_texto(791350) || ' ' || :new.cd_pessoa_fisica ||' - '|| obter_nome_pf(:new.cd_pessoa_fisica) || ' ' || wheb_mensagem_pck.get_texto(802539) || ' ' || substr(obter_valor_dominio(2197,'HD'),1,80) || '.' || chr(10) ||
				wheb_mensagem_pck.get_texto(795798) || ': ' || SUBSTR(HD_OBTER_DESC_UNID_DIALISE(:new.NR_SEQ_UNID_DIALISE),1,90),
			:new.nm_usuario,
			sysdate,
			'N',
			'',
			cd_perfil_w||', ',
			comunic_interna_seq.nextval,
			'N',
			sysdate,
			:new.cd_estabelecimento
		);
	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.hd_pac_renal_cron_aft_insert
after insert ON TASY.HD_PAC_RENAL_CRONICO for each row
declare
ie_insere_profissional_w	varchar2(1);
cd_medico_resp_w		atendimento_paciente.cd_medico_resp%type;

begin
ie_insere_profissional_w := obter_valor_param_usuario(7009, 283, Obter_Perfil_Ativo, :new.nm_usuario, :new.cd_estabelecimento);
if	(ie_insere_profissional_w = 'S') then
	select 	max(cd_medico_resp)
	into	cd_medico_resp_w
	from 	atendimento_paciente a
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
	and		nr_atendimento = (select	max(nr_atendimento)
							from		atendimento_paciente b
							where		cd_pessoa_fisica = :new.cd_pessoa_fisica
							and			cd_medico_resp is not null
							and			b.dt_cancelamento is null);

	if	(cd_medico_resp_w is not null) then
		hd_gerar_profissional_rotina(:new.nm_usuario,:new.cd_pessoa_fisica,'M',cd_medico_resp_w,'I',null,:new.cd_estabelecimento,'S');
	end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.HD_PAC_RENAL_CRONICO_tp  after update ON TASY.HD_PAC_RENAL_CRONICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_PESSOA_FISICA);  ds_c_w:=null;  exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.HD_PAC_RENAL_CRONICO ADD (
  CONSTRAINT HDPAREC_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_PAC_RENAL_CRONICO ADD (
  CONSTRAINT HDPAREC_HDUNIDD_FK2 
 FOREIGN KEY (NR_SEQ_UNIDADE_ATUAL) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDPAREC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT HDPAREC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HDPAREC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT HDPAREC_HDUNIDD_FK1 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_PAC_RENAL_CRONICO TO NIVEL_1;

