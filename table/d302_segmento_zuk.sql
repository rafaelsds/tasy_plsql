ALTER TABLE TASY.D302_SEGMENTO_ZUK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.D302_SEGMENTO_ZUK CASCADE CONSTRAINTS;

CREATE TABLE TASY.D302_SEGMENTO_ZUK
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_DATASET           NUMBER(10),
  NR_IDENTIFICADOR         NUMBER(3),
  VL_BRUTO                 NUMBER(10,2),
  NR_SEQ_302_CO_PAGAMENTO  NUMBER(10),
  VL_TAXA_ADIC             NUMBER(10,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.D302ZUK_D302DTEN_FK_I ON TASY.D302_SEGMENTO_ZUK
(NR_SEQ_DATASET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.D302ZUK_PK ON TASY.D302_SEGMENTO_ZUK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.D302_SEGMENTO_ZUK ADD (
  CONSTRAINT D302ZUK_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.D302_SEGMENTO_ZUK ADD (
  CONSTRAINT D302ZUK_D302DTEN_FK 
 FOREIGN KEY (NR_SEQ_DATASET) 
 REFERENCES TASY.D302_DATASET_ENVIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.D302_SEGMENTO_ZUK TO NIVEL_1;

