ALTER TABLE TASY.ATEND_PAC_FUTURO_CONTATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PAC_FUTURO_CONTATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PAC_FUTURO_CONTATO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_FUTURO  NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_CONTATO           DATE,
  DS_CONTATO           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPAFUC_ATEPACFU_FK_I ON TASY.ATEND_PAC_FUTURO_CONTATO
(NR_SEQ_ATEND_FUTURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPAFUC_ATEPACFU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATPAFUC_PESFISI_FK_I ON TASY.ATEND_PAC_FUTURO_CONTATO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATPAFUC_PK ON TASY.ATEND_PAC_FUTURO_CONTATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPAFUC_PK
  MONITORING USAGE;


ALTER TABLE TASY.ATEND_PAC_FUTURO_CONTATO ADD (
  CONSTRAINT ATPAFUC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PAC_FUTURO_CONTATO ADD (
  CONSTRAINT ATPAFUC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATPAFUC_ATEPACFU_FK 
 FOREIGN KEY (NR_SEQ_ATEND_FUTURO) 
 REFERENCES TASY.ATEND_PAC_FUTURO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_PAC_FUTURO_CONTATO TO NIVEL_1;

