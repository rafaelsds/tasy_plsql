ALTER TABLE TASY.W_ESUS_ZICA_MICROCEFALIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ESUS_ZICA_MICROCEFALIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ESUS_ZICA_MICROCEFALIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  DT_ATENDIMENTO          DATE                  NOT NULL,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  CD_CBO                  VARCHAR2(6 BYTE),
  CD_CNES_UNIDADE         NUMBER(7),
  NR_SEQ_SUS_EQUIPE       NUMBER(10),
  IE_TURNO_ATENDIMENTO    VARCHAR2(1 BYTE)      NOT NULL,
  NR_ATENDIMENTO          NUMBER(10),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  CD_PESSOA_RESPONSAVEL   VARCHAR2(10 BYTE),
  DT_TESTE_OLHINHO        DATE,
  IE_RESULT_TE_OLHINHO    VARCHAR2(15 BYTE),
  DT_EXAME_FUNDO_OLHO     DATE,
  IE_RESULT_EX_FU_OLHO    VARCHAR2(15 BYTE),
  DT_TESTE_ORELHINHA      DATE,
  IE_RESULT_TE_ORELHIN    VARCHAR2(15 BYTE),
  DT_ULTRAS_TRANSFON      DATE,
  IE_RESULT_ULT_TRANSF    VARCHAR2(15 BYTE),
  DT_TOMO_COMPUT          DATE,
  IE_RESULT_TOMO_COMPU    VARCHAR2(15 BYTE),
  DT_RESSO_MAGNETICA      DATE,
  IE_RESULT_RESS_MAGNE    VARCHAR2(15 BYTE),
  CD_UUID_ORIGINAL        VARCHAR2(255 BYTE),
  CD_MUNICIPIO_IBGE       VARCHAR2(7 BYTE),
  NR_SEQ_LOTE_ENVIO       NUMBER(10),
  NR_SEQ_FICHA            NUMBER(10),
  CD_CNS_PACIENTE         VARCHAR2(20 BYTE),
  CD_CNS_PESSOA_RESP      VARCHAR2(20 BYTE),
  DT_NASCIMENTO_PACIENTE  DATE,
  CD_CNS_PROFISSIONAL     VARCHAR2(20 BYTE),
  NR_DATA_ATENDIMENTO     NUMBER(20),
  NR_DATA_EXA_FUN_OLHO    NUMBER(20),
  NR_DATA_RESSO_MAGNET    NUMBER(20),
  NR_DATA_TESTE_OLHINH    NUMBER(20),
  NR_DATA_TESTE_ORELHI    NUMBER(20),
  NR_DATA_TOMO_COMPUT     NUMBER(20),
  NR_DATA_ULTRAS_TRANS    NUMBER(20),
  NR_DATA_NASC_PAC        NUMBER(20)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WESUZM_ATEPACI_FK_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_ESTABEL_FK_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_ESUSLEN_FK_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(NR_SEQ_LOTE_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_PESFISI_FK_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_PESFISI_FK1_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_PESFISI_FK3_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(CD_PESSOA_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WESUZM_PK ON TASY.W_ESUS_ZICA_MICROCEFALIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_SUSCBOU_FK_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(CD_CBO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WESUZM_SUSEQPE_FK_I ON TASY.W_ESUS_ZICA_MICROCEFALIA
(NR_SEQ_SUS_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_ESUS_ZICA_MICROCEFALIA ADD (
  CONSTRAINT WESUZM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_ESUS_ZICA_MICROCEFALIA ADD (
  CONSTRAINT WESUZM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT WESUZM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WESUZM_ESUSLEN_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENVIO) 
 REFERENCES TASY.ESUS_LOTE_ENVIO (NR_SEQUENCIA),
  CONSTRAINT WESUZM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT WESUZM_PESFISI_FK1 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT WESUZM_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT WESUZM_SUSCBOU_FK 
 FOREIGN KEY (CD_CBO) 
 REFERENCES TASY.SUS_CBO (CD_CBO),
  CONSTRAINT WESUZM_SUSEQPE_FK 
 FOREIGN KEY (NR_SEQ_SUS_EQUIPE) 
 REFERENCES TASY.SUS_EQUIPE (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_ESUS_ZICA_MICROCEFALIA TO NIVEL_1;

