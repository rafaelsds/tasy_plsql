ALTER TABLE TASY.SMP_SERVICE_DATA_
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SMP_SERVICE_DATA_ CASCADE CONSTRAINTS;

CREATE TABLE TASY.SMP_SERVICE_DATA_
(
  OWNER         VARCHAR2(32 BYTE),
  SERVICE_NAME  VARCHAR2(120 BYTE),
  SERVICE_TYPE  VARCHAR2(120 BYTE),
  NODE          VARCHAR2(120 BYTE),
  DATA          VARCHAR2(1024 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TASY.SMP_SERVICE_DATA_ ADD (
  PRIMARY KEY
 (OWNER, SERVICE_NAME, SERVICE_TYPE, NODE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SMP_SERVICE_DATA_ TO NIVEL_1;

