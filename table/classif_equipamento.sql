ALTER TABLE TASY.CLASSIF_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CLASSIF_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLASSIF_EQUIPAMENTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_CLASSIFICACAO        VARCHAR2(80 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  QT_TEMPO_ESTERELIZACAO  NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  QT_MIN_FIM_CIRUR        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CLEQUIP_PK ON TASY.CLASSIF_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CLASSIF_EQUIPAMENTO_UPDATE
BEFORE INSERT or UPDATE ON TASY.CLASSIF_EQUIPAMENTO FOR EACH ROW
declare
qt_equip_inativos_w	number(10);

BEGIN

if	(:new.ie_situacao = 'I') AND
	(:new.ie_situacao <> :old.ie_situacao ) then

	select 	count(*)
	into	qt_equip_inativos_w
	from	equipamento
	where	ie_situacao = 'A'
	and	CD_CLASSIFICACAO = :new.nr_sequencia;

	if	(qt_equip_inativos_w > 0) then
		--N�o � poss�vel inativar a classifica��o do equipamento pois existem equipamentos ativos para esta classifica��o#@#@
		Wheb_mensagem_pck.exibir_mensagem_abort(263505);
	end if;

end if;

end;
/


ALTER TABLE TASY.CLASSIF_EQUIPAMENTO ADD (
  CONSTRAINT CLEQUIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CLASSIF_EQUIPAMENTO TO NIVEL_1;

