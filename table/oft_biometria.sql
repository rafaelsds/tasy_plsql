ALTER TABLE TASY.OFT_BIOMETRIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_BIOMETRIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_BIOMETRIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CONSULTA            NUMBER(10)         NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DT_REGISTRO                DATE,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  QT_OD_ACD_INTER            NUMBER(5,2),
  QT_OD_EIXO_INTER           NUMBER(5,2),
  QT_OD_EMT_INTER            NUMBER(5,2),
  IE_OD_FORMULA_INTER        VARCHAR2(15 BYTE),
  QT_OD_ACD_ULTRA            NUMBER(5,2),
  QT_OD_EIXO_ULTRA           NUMBER(5,2),
  QT_OD_EMT_ULTRA            NUMBER(5,2),
  IE_OD_FORMULA_ULTRA        VARCHAR2(15 BYTE),
  QT_OE_ACD_INTER            NUMBER(5,2),
  QT_OE_EIXO_INTER           NUMBER(5,2),
  QT_OE_EMT_INTER            NUMBER(5,2),
  IE_OE_FORMULA_INTER        VARCHAR2(15 BYTE),
  QT_OE_ACD_ULTRA            NUMBER(5,2),
  QT_OE_EIXO_ULTRA           NUMBER(5,2),
  QT_OE_EMT_ULTRA            NUMBER(5,2),
  IE_OE_FORMULA_ULTRA        VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(1000 BYTE),
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  IE_TIPO_BIOMETRIA          VARCHAR2(1 BYTE),
  QT_OD_ACD                  NUMBER(5,2),
  QT_OD_EIXO                 NUMBER(5,2),
  QT_OD_EMT                  NUMBER(5,2),
  IE_OD_FORMULA              VARCHAR2(15 BYTE),
  QT_OE_ACD                  NUMBER(5,2),
  QT_OE_EIXO                 NUMBER(5,2),
  QT_OE_EMT                  NUMBER(5,2),
  IE_OE_FORMULA              VARCHAR2(15 BYTE),
  CD_LENTE_PLANEJADA_OD      NUMBER(6),
  CD_LENTE_PLANEJADA_OE      NUMBER(6),
  CD_LENTE_ALTERNATIVA_OD    NUMBER(6),
  CD_LENTE_ALTERNATIVA_OE    NUMBER(6),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_CONSULTA_FORM       NUMBER(10),
  NR_COMPRIMENTO_AXIAL_OD    NUMBER(4,2),
  NR_COMPRIMENTO_AXIAL_OE    NUMBER(4,2),
  NR_COMP_AXIAL_ULTRA_OD     NUMBER(4,2),
  NR_COMP_AXIAL_ULTRA_OE     NUMBER(4,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTBIOE_MATERIA_FK_I ON TASY.OFT_BIOMETRIA
(CD_LENTE_ALTERNATIVA_OD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTBIOE_MATERIA_FK2_I ON TASY.OFT_BIOMETRIA
(CD_LENTE_ALTERNATIVA_OE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTBIOE_MATERIA_FK3_I ON TASY.OFT_BIOMETRIA
(CD_LENTE_PLANEJADA_OD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTBIOE_MATERIA_FK4_I ON TASY.OFT_BIOMETRIA
(CD_LENTE_PLANEJADA_OE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTBIOE_OFCOFOR_FK_I ON TASY.OFT_BIOMETRIA
(NR_SEQ_CONSULTA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTBIOE_OFTCONS_FK_I ON TASY.OFT_BIOMETRIA
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTBIOE_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFTBIOE_PESFISI_FK_I ON TASY.OFT_BIOMETRIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFTBIOE_PK ON TASY.OFT_BIOMETRIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTBIOE_PK
  MONITORING USAGE;


CREATE INDEX TASY.OFTBIOE_TASASDI_FK_I ON TASY.OFT_BIOMETRIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTBIOE_TASASDI_FK2_I ON TASY.OFT_BIOMETRIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.oft_biometria_pend_atual
after insert or update or delete ON TASY.OFT_BIOMETRIA for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);
ie_libera_exames_oft_w	varchar2(5);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_exames_oft),'N')
into		ie_libera_exames_oft_w
from		parametro_medico
where		cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_exames_oft_w = 'S') then
	if	(inserting) or
		(updating) then

		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'BIOL';
		elsif	(:old.dt_liberacao is null) and
               (:new.dt_liberacao is not null) then
               ie_tipo_w := 'XBIOL';
		end if;

      select	max(a.nr_atendimento),
                  max(a.cd_pessoa_fisica)
      into	   nr_atendimento_w,
                  cd_pessoa_fisica_w
      from     oft_consulta a where a.nr_sequencia = :new.nr_seq_consulta;

		begin
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
		exception
			when others then
			null;
		end;

	elsif	(deleting) then
            delete 	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'BIOL'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

            commit;
    end if;

	commit;

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.OFT_BIOMETRIA ADD (
  CONSTRAINT OFTBIOE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_BIOMETRIA ADD (
  CONSTRAINT OFTBIOE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTBIOE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTBIOE_MATERIA_FK 
 FOREIGN KEY (CD_LENTE_ALTERNATIVA_OD) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT OFTBIOE_MATERIA_FK2 
 FOREIGN KEY (CD_LENTE_ALTERNATIVA_OE) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT OFTBIOE_MATERIA_FK3 
 FOREIGN KEY (CD_LENTE_PLANEJADA_OD) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT OFTBIOE_OFCOFOR_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_FORM) 
 REFERENCES TASY.OFT_CONSULTA_FORMULARIO (NR_SEQUENCIA),
  CONSTRAINT OFTBIOE_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OFTBIOE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT OFTBIOE_MATERIA_FK4 
 FOREIGN KEY (CD_LENTE_PLANEJADA_OE) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.OFT_BIOMETRIA TO NIVEL_1;

