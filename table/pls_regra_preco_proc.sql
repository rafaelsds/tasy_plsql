ALTER TABLE TASY.PLS_REGRA_PRECO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_PROC
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR             NUMBER(10),
  NR_SEQ_PLANO                 NUMBER(10),
  NR_SEQ_CATEGORIA             NUMBER(10),
  NR_SEQ_CLINICA               NUMBER(10),
  NR_SEQ_TIPO_ACOMODACAO       NUMBER(10),
  NR_SEQ_TIPO_ATENDIMENTO      NUMBER(10),
  CD_EDICAO_AMB                NUMBER(6)        NOT NULL,
  CD_PROCEDIMENTO              NUMBER(15),
  IE_ORIGEM_PROCED             NUMBER(10),
  CD_AREA_PROCEDIMENTO         NUMBER(15),
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  CD_ESPECIALIDADE             NUMBER(15),
  CD_GRUPO_PROC                NUMBER(15),
  TX_AJUSTE_GERAL              NUMBER(15,4)     NOT NULL,
  VL_CH_HONORARIOS             NUMBER(15,4)     NOT NULL,
  VL_CH_CUSTO_OPER             NUMBER(15,4)     NOT NULL,
  VL_FILME                     NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_FILME              NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_CH_HONOR           NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_CUSTO_OPER         NUMBER(15,4)     NOT NULL,
  TX_AJUSTE_PARTIC             NUMBER(15,4)     NOT NULL,
  VL_PROC_NEGOCIADO            NUMBER(15,4),
  DT_INICIO_VIGENCIA           DATE             NOT NULL,
  DT_FIM_VIGENCIA              DATE,
  IE_TIPO_TABELA               VARCHAR2(2 BYTE),
  VL_MEDICO                    NUMBER(15,4),
  VL_AUXILIARES                NUMBER(15,4),
  VL_ANESTESISTA               NUMBER(15,4),
  VL_CUSTO_OPERACIONAL         NUMBER(15,4),
  VL_MATERIAIS                 NUMBER(15,4),
  IE_PRECO_INFORMADO           VARCHAR2(1 BYTE),
  NR_SEQ_OUTORGANTE            NUMBER(10),
  NR_SEQ_CONTRATO              NUMBER(10),
  IE_PRECO                     VARCHAR2(2 BYTE),
  NR_SEQ_CONGENERE             NUMBER(10),
  CD_CONVENIO                  NUMBER(4),
  CD_CATEGORIA                 VARCHAR2(10 BYTE),
  IE_TIPO_CONTRATACAO          VARCHAR2(2 BYTE),
  QT_DIAS_INTER_INICIO         NUMBER(10),
  QT_DIAS_INTER_FINAL          NUMBER(10),
  NR_SEQ_REGRA_ANT             NUMBER(10),
  CD_MOEDA_CH_MEDICO           NUMBER(3),
  CD_MOEDA_CH_CO               NUMBER(3),
  IE_TIPO_VINCULO              VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO         NUMBER(10),
  IE_TIPO_SEGURADO             VARCHAR2(3 BYTE),
  DS_OBSERVACAO                VARCHAR2(4000 BYTE),
  NR_SEQ_GRUPO_CONTRATO        NUMBER(10),
  NR_SEQ_GRUPO_SERVICO         NUMBER(10),
  NR_SEQ_GRUPO_PRESTADOR       NUMBER(10),
  NR_SEQ_GRUPO_PRODUTO         NUMBER(10),
  CD_MOEDA_FILME               NUMBER(3),
  CD_MOEDA_ANESTESISTA         NUMBER(3),
  IE_COOPERADO                 VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_PRESTADOR        NUMBER(10),
  IE_INTERNADO                 VARCHAR2(1 BYTE),
  NR_SEQ_CBHPM_EDICAO          NUMBER(10),
  IE_TECNICA_UTILIZADA         VARCHAR2(1 BYTE),
  IE_TIPO_GUIA                 VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_ACOMOD_PROD      NUMBER(10),
  IE_ACOMODACAO                VARCHAR2(1 BYTE),
  IE_TIPO_INTERCAMBIO          VARCHAR2(10 BYTE),
  IE_FRANQUIA                  VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_REC             NUMBER(10),
  SG_UF_OPERADORA_INTERCAMBIO  VARCHAR2(2 BYTE),
  NR_SEQ_SETOR_ATEND           NUMBER(10),
  NR_SEQ_CBO_SAUDE             NUMBER(10),
  NR_SEQ_GRUPO_INTERCAMBIO     NUMBER(10),
  NR_SEQ_INTERCAMBIO           NUMBER(10),
  CD_PRESTADOR                 VARCHAR2(30 BYTE),
  IE_AUTOGERADO                VARCHAR2(1 BYTE),
  NR_SEQ_CONGENERE_PROT        NUMBER(10),
  IE_CARATER_INTERNACAO        VARCHAR2(1 BYTE),
  NR_CONTRATO                  NUMBER(10),
  IE_TAXA_COLETA               VARCHAR2(1 BYTE),
  IE_ORIGEM_PROCEDIMENTO       NUMBER(5),
  NR_SEQ_OPS_CONGENERE         NUMBER(10),
  IE_TIPO_CONTRATO             VARCHAR2(2 BYTE),
  IE_PCMSO                     VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_PROC_REF        NUMBER(10),
  IE_TIPO_ACOMODACAO_PTU       VARCHAR2(2 BYTE),
  QT_IDADE_INICIAL             NUMBER(5),
  QT_IDADE_FINAL               NUMBER(5),
  CD_ESPECIALIDADE_PREST       NUMBER(5),
  IE_PROC_TABELA               VARCHAR2(1 BYTE),
  DS_REGRA                     VARCHAR2(255 BYTE),
  NR_SEQ_RP_COMBINADA          NUMBER(10),
  IE_NAO_GERA_TX_INTER         VARCHAR2(1 BYTE),
  IE_TIPO_PRESTADOR            VARCHAR2(2 BYTE),
  NR_SEQ_CATEGORIA_PLANO       NUMBER(10),
  CD_PRESTADOR_PROT            VARCHAR2(30 BYTE),
  NR_SEQ_TIPO_PRESTADOR_PROT   NUMBER(10),
  NR_SEQ_REGRA_ATEND_CART      NUMBER(10),
  NR_SEQ_CP_COMBINADA          NUMBER(10),
  IE_TIPO_CONSULTA             NUMBER(1),
  IE_CH_PADRAO_ANESTESISTA     VARCHAR2(1 BYTE),
  NR_SEQ_EDICAO_TUSS           NUMBER(10),
  IE_ORIGEM_PROTOCOLO          VARCHAR2(1 BYTE),
  IE_ATEND_PCMSO               VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_OPERADORA       NUMBER(10),
  IE_REF_GUIA_INTERNACAO       VARCHAR2(1 BYTE),
  NR_SEQ_PREST_INTER           NUMBER(10),
  CD_VERSAO_TISS               VARCHAR2(20 BYTE),
  CD_PRESTADOR_SOLIC           VARCHAR2(30 BYTE),
  IE_ACOMODACAO_AUTORIZADA     VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA_REF       DATE,
  DT_FIM_VIGENCIA_REF          DATE,
  CD_MEDICO                    VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_ATEND_PRINC      NUMBER(10),
  IE_TIPO_ATENDIMENTO          VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_PREST_INT       NUMBER(10),
  NR_SEQ_MOT_REEMBOLSO         NUMBER(10),
  NR_SEQ_CBO_SAUDE_SOLIC       NUMBER(10),
  IE_VALOR_AUTORIZADO          VARCHAR2(3 BYTE),
  CD_MOEDA_TABELA              NUMBER(3),
  NR_SEQ_GRUPO_MED_EXEC        NUMBER(10),
  CD_PRESTADOR_PARTIC          VARCHAR2(30 BYTE),
  IE_GERAR_REMIDO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREPRP_AREPROC_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_CATCONV_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_CBOSAUD_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_CBOSAUD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_CONVENI_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_EDIAMB_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_EDIAMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_ESPMEDI_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_ESPECIALIDADE_PREST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_ESPPROC_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_ESTABEL_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_GRUPROC_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_GRURECE_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_GRURECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_I1 ON TASY.PLS_REGRA_PRECO_PROC
(DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_I2 ON TASY.PLS_REGRA_PRECO_PROC
(IE_TIPO_TABELA, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_MEDICO_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_MOEDA_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_MOEDA_CH_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_MOEDA_FK2_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_MOEDA_CH_CO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_MOEDA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_MOEDA_FK3_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_MOEDA_FILME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_MOEDA_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_MOEDA_FK4_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_MOEDA_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_MOEDA_FK4_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSREPRP_PK ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSCATE_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CATEGORIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCATE_FK1_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CATEGORIA_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCATE_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCLIN_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCLIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCLPR_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCLPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCONG_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCONG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCONG_FK2_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_OPS_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCONG_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCONT_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSCPCC_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_CP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSINCA_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSMORE_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_MOT_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSOACA_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_REGRA_ATEND_CART)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSOUTO_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_OUTORGANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSOUTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSPGOP_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSPGPI_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_PREST_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSPGRP_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSPGRP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSPLAN_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSPRES_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSPRGC_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSPRGC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSPRGP_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSPRGP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSPRGS_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSPRGS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSPRIN_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_PREST_INTER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSREGM_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_MED_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPRP_PLSREPRP_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_REGRA_PROC_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSREPRP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSRGIN_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_GRUPO_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSRGIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSSEAT_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_SETOR_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSSEAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSTIAC_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSTIAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSTIAT_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSTIAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSTIPR_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PLSTIPR_FK1_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_TIPO_PRESTADOR_PROT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PLSTIPR_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PRCCOMB_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(NR_SEQ_RP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PRCCOMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPRP_PROCEDI_FK_I ON TASY.PLS_REGRA_PRECO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPRP_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_regra_preco_proc_delete
after delete ON TASY.PLS_REGRA_PRECO_PROC for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_REGRA_PRECO_PROC',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.pls_regra_preco_proc_befins
before insert ON TASY.PLS_REGRA_PRECO_PROC for each row
declare
ie_classificacao_w	Varchar2(1);
ie_verifica_proc_w	Varchar2(1);

begin
/* Obter o valor do par�metro 7 da fun��o OPS - Regras e Crit�rios de Pre�o - 1298 */
ie_verifica_proc_w	:= nvl(obter_valor_param_usuario(1298, 7, Obter_Perfil_Ativo, :new.nm_usuario, :new.cd_estabelecimento), 'N');

if	(ie_verifica_proc_w	= 'S') then
	select	nvl(max(ie_classificacao),1)
	into	ie_classificacao_w
	from	procedimento
	where	:new.cd_procedimento = cd_procedimento
	and	ie_origem_proced = :new.ie_origem_proced;

	if	(ie_classificacao_w <> '1') then
		wheb_mensagem_pck.exibir_mensagem_abort( 267215, null ); /* N�o � permitido cadastrar di�rias ou servi�os na pasta Procedimentos! [7] */
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_regra_preco_proc_atual
before insert or update ON TASY.PLS_REGRA_PRECO_PROC for each row
declare

begin

-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_regra_preco_proc_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_PRECO_PROC_tp  after update ON TASY.PLS_REGRA_PRECO_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRESTADOR,1,4000),substr(:new.IE_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONSULTA,1,4000),substr(:new.IE_TIPO_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONSULTA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_COMBINADA,1,4000),substr(:new.NR_SEQ_CP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_COMBINADA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_ATEND_CART,1,4000),substr(:new.NR_SEQ_REGRA_ATEND_CART,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_ATEND_CART',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROC_TABELA,1,4000),substr(:new.IE_PROC_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_TABELA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_RP_COMBINADA,1,4000),substr(:new.NR_SEQ_RP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RP_COMBINADA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_PROT,1,4000),substr(:new.CD_PRESTADOR_PROT,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_PROT',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR_PROT,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR_PROT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR_PROT',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_GERA_TX_INTER,1,4000),substr(:new.IE_NAO_GERA_TX_INTER,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_GERA_TX_INTER',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_OPERADORA,1,4000),substr(:new.NR_SEQ_GRUPO_OPERADORA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_OPERADORA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROTOCOLO,1,4000),substr(:new.IE_ORIGEM_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROTOCOLO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CH_PADRAO_ANESTESISTA,1,4000),substr(:new.IE_CH_PADRAO_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CH_PADRAO_ANESTESISTA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REF_GUIA_INTERNACAO,1,4000),substr(:new.IE_REF_GUIA_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REF_GUIA_INTERNACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CATEGORIA_PLANO,1,4000),substr(:new.NR_SEQ_CATEGORIA_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CATEGORIA_PLANO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PREST_INTER,1,4000),substr(:new.NR_SEQ_PREST_INTER,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PREST_INTER',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_VERSAO_TISS,1,4000),substr(:new.CD_VERSAO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO_TISS',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EDICAO_TUSS,1,4000),substr(:new.NR_SEQ_EDICAO_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EDICAO_TUSS',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE_PREST,1,4000),substr(:new.CD_ESPECIALIDADE_PREST,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE_PREST',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PLANO,1,4000),substr(:new.NR_SEQ_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PLANO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CATEGORIA,1,4000),substr(:new.NR_SEQ_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CATEGORIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLINICA,1,4000),substr(:new.NR_SEQ_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLINICA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ACOMODACAO,1,4000),substr(:new.NR_SEQ_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ACOMODACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATENDIMENTO,1,4000),substr(:new.NR_SEQ_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CH_HONORARIOS,1,4000),substr(:new.VL_CH_HONORARIOS,1,4000),:new.nm_usuario,nr_seq_w,'VL_CH_HONORARIOS',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CH_CUSTO_OPER,1,4000),substr(:new.VL_CH_CUSTO_OPER,1,4000),:new.nm_usuario,nr_seq_w,'VL_CH_CUSTO_OPER',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_CUSTO_OPER,1,4000),substr(:new.TX_AJUSTE_CUSTO_OPER,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_CUSTO_OPER',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PARTIC,1,4000),substr(:new.TX_AJUSTE_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PARTIC',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PROC_NEGOCIADO,1,4000),substr(:new.VL_PROC_NEGOCIADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PROC_NEGOCIADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_FILME,1,4000),substr(:new.TX_AJUSTE_FILME,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_FILME',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FILME,1,4000),substr(:new.VL_FILME,1,4000),:new.nm_usuario,nr_seq_w,'VL_FILME',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_CH_HONOR,1,4000),substr(:new.TX_AJUSTE_CH_HONOR,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_CH_HONOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_GERAL,1,4000),substr(:new.TX_AJUSTE_GERAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_GERAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MEDICO,1,4000),substr(:new.VL_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MEDICO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_AUXILIARES,1,4000),substr(:new.VL_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'VL_AUXILIARES',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ANESTESISTA,1,4000),substr(:new.VL_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ANESTESISTA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CUSTO_OPERACIONAL,1,4000),substr(:new.VL_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_CUSTO_OPERACIONAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MATERIAIS,1,4000),substr(:new.VL_MATERIAIS,1,4000),:new.nm_usuario,nr_seq_w,'VL_MATERIAIS',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO_INFORMADO,1,4000),substr(:new.IE_PRECO_INFORMADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO_INFORMADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR_SOLIC,1,4000),substr(:new.CD_PRESTADOR_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_SOLIC',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATEND_PRINC,1,4000),substr(:new.NR_SEQ_TIPO_ATEND_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATEND_PRINC',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACOMODACAO_AUTORIZADA,1,4000),substr(:new.IE_ACOMODACAO_AUTORIZADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACOMODACAO_AUTORIZADA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OUTORGANTE,1,4000),substr(:new.NR_SEQ_OUTORGANTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OUTORGANTE',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONTRATO,1,4000),substr(:new.NR_SEQ_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATEND_PCMSO,1,4000),substr(:new.IE_ATEND_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATEND_PCMSO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO,1,4000),substr(:new.IE_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE,1,4000),substr(:new.NR_SEQ_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONTRATACAO,1,4000),substr(:new.IE_TIPO_CONTRATACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONTRATACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_CH_CO,1,4000),substr(:new.CD_MOEDA_CH_CO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_CH_CO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_CH_MEDICO,1,4000),substr(:new.CD_MOEDA_CH_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_CH_MEDICO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_INTER_INICIO,1,4000),substr(:new.QT_DIAS_INTER_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_INTER_INICIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_INTER_FINAL,1,4000),substr(:new.QT_DIAS_INTER_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_INTER_FINAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_SERVICO,1,4000),substr(:new.NR_SEQ_GRUPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_SERVICO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_GRUPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_PRODUTO,1,4000),substr(:new.NR_SEQ_GRUPO_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_PRODUTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_FILME,1,4000),substr(:new.CD_MOEDA_FILME,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_FILME',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_ANESTESISTA,1,4000),substr(:new.CD_MOEDA_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_ANESTESISTA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_CONTRATO,1,4000),substr(:new.NR_SEQ_GRUPO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_VINCULO,1,4000),substr(:new.IE_TIPO_VINCULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VINCULO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SEGURADO,1,4000),substr(:new.IE_TIPO_SEGURADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SEGURADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TECNICA_UTILIZADA,1,4000),substr(:new.IE_TECNICA_UTILIZADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TECNICA_UTILIZADA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ACOMOD_PROD,1,4000),substr(:new.NR_SEQ_TIPO_ACOMOD_PROD,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ACOMOD_PROD',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACOMODACAO,1,4000),substr(:new.IE_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACOMODACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBHPM_EDICAO,1,4000),substr(:new.NR_SEQ_CBHPM_EDICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBHPM_EDICAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTERNADO,1,4000),substr(:new.IE_INTERNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTERNADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COOPERADO,1,4000),substr(:new.IE_COOPERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COOPERADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_INTERCAMBIO,1,4000),substr(:new.IE_TIPO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FRANQUIA,1,4000),substr(:new.IE_FRANQUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRANQUIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_UF_OPERADORA_INTERCAMBIO,1,4000),substr(:new.SG_UF_OPERADORA_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'SG_UF_OPERADORA_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SETOR_ATEND,1,4000),substr(:new.NR_SEQ_SETOR_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SETOR_ATEND',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBO_SAUDE,1,4000),substr(:new.NR_SEQ_CBO_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBO_SAUDE',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PRESTADOR,1,4000),substr(:new.CD_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_INTERCAMBIO,1,4000),substr(:new.NR_SEQ_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_INTERCAMBIO,1,4000),substr(:new.NR_SEQ_GRUPO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_INTERCAMBIO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTOGERADO,1,4000),substr(:new.IE_AUTOGERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTOGERADO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE_PROT,1,4000),substr(:new.NR_SEQ_CONGENERE_PROT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE_PROT',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARATER_INTERNACAO,1,4000),substr(:new.IE_CARATER_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARATER_INTERNACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTRATO,1,4000),substr(:new.NR_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TAXA_COLETA,1,4000),substr(:new.IE_TAXA_COLETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TAXA_COLETA',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCEDIMENTO,1,4000),substr(:new.IE_ORIGEM_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCEDIMENTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PCMSO,1,4000),substr(:new.IE_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PCMSO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OPS_CONGENERE,1,4000),substr(:new.NR_SEQ_OPS_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OPS_CONGENERE',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONTRATO,1,4000),substr(:new.IE_TIPO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONTRATO',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_PROC_REF,1,4000),substr(:new.NR_SEQ_REGRA_PROC_REF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_PROC_REF',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ACOMODACAO_PTU,1,4000),substr(:new.IE_TIPO_ACOMODACAO_PTU,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ACOMODACAO_PTU',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_INICIAL,1,4000),substr(:new.QT_IDADE_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_INICIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_FINAL,1,4000),substr(:new.QT_IDADE_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_FINAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_PROC ADD (
  CONSTRAINT PLSREPRP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_PROC ADD (
  CONSTRAINT PLSREPRP_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSREPRP_PLSCATE_FK1 
 FOREIGN KEY (NR_SEQ_CATEGORIA_PLANO) 
 REFERENCES TASY.PLS_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSREGM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MED_EXEC) 
 REFERENCES TASY.PLS_REGRA_GRUPO_MEDICO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMBINADA) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPGOP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_OPERADORA) 
 REFERENCES TASY.PLS_PRECO_GRUPO_OPERADORA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPRIN_FK 
 FOREIGN KEY (NR_SEQ_PREST_INTER) 
 REFERENCES TASY.PLS_PRESTADOR_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPGPI_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PREST_INT) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PREST_INT (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSMORE_FK 
 FOREIGN KEY (NR_SEQ_MOT_REEMBOLSO) 
 REFERENCES TASY.PLS_MOTIVO_REEMBOLSO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSOACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ATEND_CART) 
 REFERENCES TASY.PLS_OC_ATEND_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PRCCOMB_FK 
 FOREIGN KEY (NR_SEQ_RP_COMBINADA) 
 REFERENCES TASY.PLS_RP_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_PREST) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PLSREPRP_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSRGIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INTERCAMBIO) 
 REFERENCES TASY.PLS_REGRA_GRUPO_INTER (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSCONG_FK2 
 FOREIGN KEY (NR_SEQ_OPS_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSREPRP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PROC_REF) 
 REFERENCES TASY.PLS_REGRA_PRECO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSSEAT_FK 
 FOREIGN KEY (NR_SEQ_SETOR_ATEND) 
 REFERENCES TASY.PLS_SETOR_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PLSREPRP_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT PLSREPRP_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA_CH_MEDICO) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSREPRP_MOEDA_FK2 
 FOREIGN KEY (CD_MOEDA_CH_CO) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSREPRP_PLSCLPR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIF_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPRGP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRESTADOR) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPGRP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PRODUTO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_PRODUTO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_MOEDA_FK3 
 FOREIGN KEY (CD_MOEDA_FILME) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSREPRP_MOEDA_FK4 
 FOREIGN KEY (CD_MOEDA_ANESTESISTA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLSREPRP_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSREPRP_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSREPRP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSREPRP_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSREPRP_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSREPRP_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.PLS_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT PLSREPRP_PLSOUTO_FK 
 FOREIGN KEY (NR_SEQ_OUTORGANTE) 
 REFERENCES TASY.PLS_OUTORGANTE (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREPRP_PLSTIPR_FK1 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR_PROT) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_PROC TO NIVEL_1;

