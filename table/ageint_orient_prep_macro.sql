ALTER TABLE TASY.AGEINT_ORIENT_PREP_MACRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_ORIENT_PREP_MACRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_ORIENT_PREP_MACRO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_ORIENT_PREP_REGRA  NUMBER(10)          NOT NULL,
  IE_MACRO                  NUMBER(3)           NOT NULL,
  VL_MACRO                  NUMBER(10)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGORPRMA_AGORPRRE_FK_I ON TASY.AGEINT_ORIENT_PREP_MACRO
(NR_SEQ_ORIENT_PREP_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGORPRMA_PK ON TASY.AGEINT_ORIENT_PREP_MACRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGEINT_ORIENT_PREP_MACRO ADD (
  CONSTRAINT AGORPRMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AGEINT_ORIENT_PREP_MACRO ADD (
  CONSTRAINT AGORPRMA_AGORPRRE_FK 
 FOREIGN KEY (NR_SEQ_ORIENT_PREP_REGRA) 
 REFERENCES TASY.AGEINT_ORIENT_PREP_REGRA (NR_SEQUENCIA));

