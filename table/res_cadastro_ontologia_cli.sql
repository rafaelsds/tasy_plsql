ALTER TABLE TASY.RES_CADASTRO_ONTOLOGIA_CLI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RES_CADASTRO_ONTOLOGIA_CLI CASCADE CONSTRAINTS;

CREATE TABLE TASY.RES_CADASTRO_ONTOLOGIA_CLI
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_VALOR_TASY        VARCHAR2(255 BYTE)       NOT NULL,
  NM_TABELA            VARCHAR2(50 BYTE),
  NM_TABELA_ATRIBUTO   VARCHAR2(50 BYTE),
  NM_ATRIBUTO          VARCHAR2(50 BYTE),
  IE_ONTOLOGIA         VARCHAR2(10 BYTE)        NOT NULL,
  CD_VALOR_ONTOLOGIA   VARCHAR2(255 BYTE),
  DS_INFORMACAO        VARCHAR2(255 BYTE),
  DS_INFORMACAO_CLI    VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RECAONC_PK ON TASY.RES_CADASTRO_ONTOLOGIA_CLI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RES_CADASTRO_ONTOLOGIA_CLI ADD (
  CONSTRAINT RECAONC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.RES_CADASTRO_ONTOLOGIA_CLI TO NIVEL_1;

