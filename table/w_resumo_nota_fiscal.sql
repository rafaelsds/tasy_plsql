ALTER TABLE TASY.W_RESUMO_NOTA_FISCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_RESUMO_NOTA_FISCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RESUMO_NOTA_FISCAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_CODIGO            NUMBER(6),
  DS_DESCRICAO         VARCHAR2(100 BYTE),
  VL_COMPRA_DIA        NUMBER(15,2),
  VL_COMPRA_MES        NUMBER(15,2),
  VL_COMPRA_MES_ANT    NUMBER(15,2),
  VL_PERC_VARIACAO     NUMBER(13,4),
  VL_PERC_TETO         NUMBER(13,4),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WRESNOT_ESTABEL_FK_I ON TASY.W_RESUMO_NOTA_FISCAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESNOT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WRESNOT_PK ON TASY.W_RESUMO_NOTA_FISCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESNOT_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_RESUMO_NOTA_FISCAL ADD (
  CONSTRAINT WRESNOT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_RESUMO_NOTA_FISCAL ADD (
  CONSTRAINT WRESNOT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.W_RESUMO_NOTA_FISCAL TO NIVEL_1;

