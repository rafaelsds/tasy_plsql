ALTER TABLE TASY.CONTA_ANALISE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_ANALISE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_ANALISE
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  NR_INTERNO_CONTA  NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  NR_ATEND_CONTA    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONANAL_ANACONT_FK_I ON TASY.CONTA_ANALISE
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONANAL_CONPACI_FK_I ON TASY.CONTA_ANALISE
(NR_INTERNO_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONANAL_CONPACI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONANAL_PK ON TASY.CONTA_ANALISE
(NR_SEQUENCIA, NR_INTERNO_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONANAL_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONTA_ANALISE ADD (
  CONSTRAINT CONANAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_INTERNO_CONTA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_ANALISE ADD (
  CONSTRAINT CONANAL_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT CONANAL_ANACONT_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.ANALISE_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CONTA_ANALISE TO NIVEL_1;

