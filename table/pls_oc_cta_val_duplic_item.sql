ALTER TABLE TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_COMB            NUMBER(10)      NOT NULL,
  IE_MESMO_HORARIO              VARCHAR2(1 BYTE) NOT NULL,
  IE_MESMA_GUIA                 VARCHAR2(1 BYTE) NOT NULL,
  IE_MESMA_FATURA               VARCHAR2(1 BYTE),
  IE_MESMO_BENEFICIARIO         VARCHAR2(1 BYTE),
  IE_PROFISSIONAL_INTERCAMBIO   VARCHAR2(2 BYTE),
  IE_VIA_ACESSO                 VARCHAR2(1 BYTE),
  IE_CONSIDERA_CONS_DESC        VARCHAR2(5 BYTE),
  IE_GERA_SEGUNDO_ITEM          VARCHAR2(5 BYTE),
  IE_PROFISSIONAL               VARCHAR2(5 BYTE),
  IE_SOMENTE_INTEGRADO          VARCHAR2(3 BYTE),
  IE_DESCONSIDERA_ITEM_GLOSADO  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCDI_PK ON TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCDI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCDI_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCDI_PLSOCCC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM_tp  after update ON TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_MESMO_BENEFICIARIO,1,4000),substr(:new.IE_MESMO_BENEFICIARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMO_BENEFICIARIO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MESMO_HORARIO,1,4000),substr(:new.IE_MESMO_HORARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMO_HORARIO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MESMA_GUIA,1,4000),substr(:new.IE_MESMA_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMA_GUIA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROFISSIONAL,1,4000),substr(:new.IE_PROFISSIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROFISSIONAL',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_SEGUNDO_ITEM,1,4000),substr(:new.IE_GERA_SEGUNDO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_SEGUNDO_ITEM',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERA_CONS_DESC,1,4000),substr(:new.IE_CONSIDERA_CONS_DESC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERA_CONS_DESC',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_ACESSO,1,4000),substr(:new.IE_VIA_ACESSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_ACESSO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROFISSIONAL_INTERCAMBIO,1,4000),substr(:new.IE_PROFISSIONAL_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROFISSIONAL_INTERCAMBIO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MESMA_FATURA,1,4000),substr(:new.IE_MESMA_FATURA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MESMA_FATURA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_DUPLIC_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM ADD (
  CONSTRAINT PLSOCDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM ADD (
  CONSTRAINT PLSOCDI_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_DUPLIC_ITEM TO NIVEL_1;

