ALTER TABLE TASY.HISTORICO_INFECCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HISTORICO_INFECCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HISTORICO_INFECCAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_REGISTRO            DATE,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  IE_NEGA_INFECCAO       VARCHAR2(1 BYTE),
  DT_EXAME               DATE,
  DS_INSTITUICAO         VARCHAR2(255 BYTE),
  IE_ALERTA              VARCHAR2(1 BYTE),
  CD_TIPO_INFECCAO       NUMBER(10),
  DS_ITEM_INVESTIGACAO   VARCHAR2(250 BYTE),
  DT_FIM                 DATE,
  CD_DOENCA              VARCHAR2(10 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NM_USUARIO_LIBERACAO   VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HSINFEC_CIDDOEN_FK_I ON TASY.HISTORICO_INFECCAO
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HSINFEC_PESFISI_FK2_I ON TASY.HISTORICO_INFECCAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HSINFEC_PK ON TASY.HISTORICO_INFECCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HSINFEC_TIPINFE_FK_I ON TASY.HISTORICO_INFECCAO
(CD_TIPO_INFECCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HISTORICO_INFECCAO_tp  after update ON TASY.HISTORICO_INFECCAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'HISTORICO_INFECCAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_TIPO_INFECCAO,1,500);gravar_log_alteracao(substr(:old.CD_TIPO_INFECCAO,1,4000),substr(:new.CD_TIPO_INFECCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_INFECCAO',ie_log_w,ds_w,'HISTORICO_INFECCAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'HISTORICO_INFECCAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'HISTORICO_INFECCAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'HISTORICO_INFECCAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ITEM_INVESTIGACAO,1,500);gravar_log_alteracao(substr(:old.DS_ITEM_INVESTIGACAO,1,4000),substr(:new.DS_ITEM_INVESTIGACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM_INVESTIGACAO',ie_log_w,ds_w,'HISTORICO_INFECCAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.HISTORICO_INFECCAO_BEF_UPDATE
before update ON TASY.HISTORICO_INFECCAO for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')	then
	if	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then

		:new.nm_usuario_liberacao := :new.nm_usuario;

	end if;
end if;

end;
/


ALTER TABLE TASY.HISTORICO_INFECCAO ADD (
  CONSTRAINT HSINFEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.HISTORICO_INFECCAO ADD (
  CONSTRAINT HSINFEC_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT HSINFEC_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT HSINFEC_TIPINFE_FK 
 FOREIGN KEY (CD_TIPO_INFECCAO) 
 REFERENCES TASY.TIPO_INFECCAO (NR_SEQUENCIA));

