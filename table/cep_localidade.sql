ALTER TABLE TASY.CEP_LOCALIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CEP_LOCALIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CEP_LOCALIDADE
(
  NR_LOCALIDADE         NUMBER(9)               NOT NULL,
  NM_LOCALIDADE         VARCHAR2(55 BYTE)       NOT NULL,
  CD_UNIDADE_FEDERACAO  VARCHAR2(2 BYTE)        NOT NULL,
  TP_LOCALIDADE         VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_FONETICA           VARCHAR2(60 BYTE),
  CD_CEP2               VARCHAR2(8 BYTE),
  CD_LOCALIDADE         VARCHAR2(8 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CEPLOCA_CODIGO_I ON TASY.CEP_LOCALIDADE
(CD_LOCALIDADE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CEPLOCA_NOME_I ON TASY.CEP_LOCALIDADE
(NM_LOCALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CEPLOCA_PK ON TASY.CEP_LOCALIDADE
(NR_LOCALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CEP_LOC_I2 ON TASY.CEP_LOCALIDADE
(CD_UNIDADE_FEDERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CEP_LOCALIDADE ADD (
  CONSTRAINT CEPLOCA_PK
 PRIMARY KEY
 (NR_LOCALIDADE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.CEP_LOCALIDADE TO NIVEL_1;

