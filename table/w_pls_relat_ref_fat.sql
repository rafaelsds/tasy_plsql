ALTER TABLE TASY.W_PLS_RELAT_REF_FAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_RELAT_REF_FAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_RELAT_REF_FAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_FANTASIA          VARCHAR2(255 BYTE),
  NR_SEQ_CONGENERE     NUMBER(10),
  DS_TIPO_GUIA         VARCHAR2(255 BYTE),
  DS_CONSULTA          VARCHAR2(255 BYTE),
  DS_TIPO_ATENDIMENTO  VARCHAR2(255 BYTE),
  DS_REFERENCIA        VARCHAR2(255 BYTE),
  NM_REFERENCIA        VARCHAR2(255 BYTE),
  QT_GUIA              VARCHAR2(255 BYTE),
  IE_BANDA             VARCHAR2(2 BYTE),
  QT_TOTAL_GUIA        VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPRELRF_PK ON TASY.W_PLS_RELAT_REF_FAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PLS_RELAT_REF_FAT ADD (
  CONSTRAINT WPRELRF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PLS_RELAT_REF_FAT TO NIVEL_1;

