ALTER TABLE TASY.CAPTURA_IMAGEM_EDICAO_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAPTURA_IMAGEM_EDICAO_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAPTURA_IMAGEM_EDICAO_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CAPTURA       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAPIMGEDLO_CAPIMG_FK_I ON TASY.CAPTURA_IMAGEM_EDICAO_LOG
(NR_SEQ_CAPTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAPIMGEDLO_PK ON TASY.CAPTURA_IMAGEM_EDICAO_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CAPTURA_IMAGEM_EDICAO_LOG ADD (
  CONSTRAINT CAPIMGEDLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAPTURA_IMAGEM_EDICAO_LOG ADD (
  CONSTRAINT CAPIMGEDLO_CAPIMG_FK 
 FOREIGN KEY (NR_SEQ_CAPTURA) 
 REFERENCES TASY.CAPTURA_IMAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CAPTURA_IMAGEM_EDICAO_LOG TO NIVEL_1;

