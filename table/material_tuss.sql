ALTER TABLE TASY.MATERIAL_TUSS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_TUSS CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_TUSS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CONVENIO           NUMBER(5),
  CD_CATEGORIA          VARCHAR2(10 BYTE),
  IE_TIPO_ATENDIMENTO   NUMBER(3),
  DT_VIGENCIA_INICIAL   DATE                    NOT NULL,
  DT_VIGENCIA_FINAL     DATE,
  IE_ORIGEM_PRECO       NUMBER(2),
  NR_SEQ_TUSS_MAT       NUMBER(10)              NOT NULL,
  NR_SEQ_TUSS_MAT_ITEM  NUMBER(10)              NOT NULL,
  CD_MATERIAL_TUSS      NUMBER(15)              NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  NR_SEQ_MARCA          NUMBER(10),
  CD_CGC_FORNECEDOR     VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATTUSS_CATCONV_FK_I ON TASY.MATERIAL_TUSS
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTUSS_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATTUSS_CONVENI_FK_I ON TASY.MATERIAL_TUSS
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTUSS_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATTUSS_ESTABEL_FK_I ON TASY.MATERIAL_TUSS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTUSS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATTUSS_MARCA_FK_I ON TASY.MATERIAL_TUSS
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTUSS_MARCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATTUSS_MATERIA_FK_I ON TASY.MATERIAL_TUSS
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATTUSS_PESJURI_FK_I ON TASY.MATERIAL_TUSS
(CD_CGC_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATTUSS_PK ON TASY.MATERIAL_TUSS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATTUSS_TUSSITE_FK_I ON TASY.MATERIAL_TUSS
(NR_SEQ_TUSS_MAT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTUSS_TUSSITE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATTUSS_TUSSMAT_FK_I ON TASY.MATERIAL_TUSS
(NR_SEQ_TUSS_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATTUSS_TUSSMAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.material_tuss_after
after insert or update or delete ON TASY.MATERIAL_TUSS for each row
declare

ds_retorno_integracao_w clob;
event_w					varchar2(25);
event_class_w			varchar2(90);
nr_sequencia_w 			material_tuss.nr_sequencia%type;
qt_retorno_w        varchar2(1);

begin

select	obter_valor_param_usuario(9041,
                                  10,
                                  obter_perfil_ativo,
                                  wheb_usuario_pck.get_nm_usuario(),
                                  obter_estabelecimento_ativo())
into	qt_retorno_w
from	dual;

if (qt_retorno_w = 'S')then
  /*Quando Incluir, alterar e excluir as informa��es da material_tuss chama a integra��o padr�o*/
  if	(inserting) then
    nr_sequencia_w			:= :new.nr_sequencia;
    event_w					:= 'materialtuss.added'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialtuss.outbound.MaterialTussAddedCallback'; -- event class created in tasy-interfaces(material module).
  elsif	(updating) then
    nr_sequencia_w			:= :new.nr_sequencia;
    event_w					:= 'materialtuss.updated'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialtuss.outbound.MaterialTussUpdatedCallback'; -- event class created in tasy-interfaces(material module).
  else
    nr_sequencia_w			:= :old.nr_sequencia;
    event_w					:= 'materialtuss.deleted'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialtuss.outbound.MaterialTussDeletedCallback'; -- event class created in tasy-interfaces(material module).
  end if;

  SELECT BIFROST.SEND_INTEGRATION(
      event_w,
      event_class_w,
      '{"code" : '||nr_sequencia_w||'}',
      'integration')
  INTO ds_retorno_integracao_w
  FROM dual;
end if;

end;
/


ALTER TABLE TASY.MATERIAL_TUSS ADD (
  CONSTRAINT MATTUSS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_TUSS ADD (
  CONSTRAINT MATTUSS_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT MATTUSS_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT MATTUSS_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATTUSS_TUSSITE_FK 
 FOREIGN KEY (NR_SEQ_TUSS_MAT_ITEM) 
 REFERENCES TASY.TUSS_MATERIAL_ITEM (NR_SEQUENCIA),
  CONSTRAINT MATTUSS_TUSSMAT_FK 
 FOREIGN KEY (NR_SEQ_TUSS_MAT) 
 REFERENCES TASY.TUSS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT MATTUSS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATTUSS_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA),
  CONSTRAINT MATTUSS_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.MATERIAL_TUSS TO NIVEL_1;

