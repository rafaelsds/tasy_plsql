ALTER TABLE TASY.W_DIOT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIOT CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIOT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_TERCEIRO     VARCHAR2(10 BYTE),
  IE_TIPO_OPERACAO_MX  VARCHAR2(10 BYTE),
  CD_RFC               VARCHAR2(13 BYTE),
  NM_ESTRANGEIRO       VARCHAR2(255 BYTE),
  CD_INTERNACIONAL     VARCHAR2(255 BYTE),
  DS_NACIONALIDADE     VARCHAR2(255 BYTE),
  VL_CAMPO_8           NUMBER(15,4),
  SG_PAIS              VARCHAR2(3 BYTE),
  VL_CAMPO_9           NUMBER(15,4),
  VL_CAMPO_10          NUMBER(15,4),
  VL_CAMPO_11          NUMBER(15,4),
  VL_CAMPO_12          NUMBER(15,4),
  VL_CAMPO_13          NUMBER(15,4),
  VL_CAMPO_14          NUMBER(15,4),
  VL_CAMPO_15          NUMBER(15,4),
  VL_CAMPO_16          NUMBER(15,4),
  VL_CAMPO_17          NUMBER(15,4),
  VL_CAMPO_18          NUMBER(15,4),
  VL_CAMPO_19          NUMBER(15,4),
  VL_CAMPO_20          NUMBER(15,4),
  VL_CAMPO_21          NUMBER(15,4),
  VL_CAMPO_22          NUMBER(15,4),
  NR_SEQ_CONTROLE      NUMBER(10),
  NR_TITULO            NUMBER(10),
  NR_LOTE_CONTABIL     NUMBER(10),
  NR_SEQ_CHEQUE        NUMBER(10),
  VL_TOTAL_NOTA        NUMBER(13,2),
  NR_SEQ_NOTA          NUMBER(10),
  DT_BAIXA             DATE,
  VL_MERCADORIA        NUMBER(15,2),
  VL_BAIXA             NUMBER(15,2),
  VL_CAMPO_23          NUMBER(15,4),
  VL_CAMPO_24          NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WDIOT_PK ON TASY.W_DIOT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_DIOT ADD (
  CONSTRAINT WDIOT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_DIOT TO NIVEL_1;

