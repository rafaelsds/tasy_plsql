ALTER TABLE TASY.DVY_RESPONSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DVY_RESPONSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DVY_RESPONSE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_BANK_ACCOUNT      VARCHAR2(30 BYTE),
  CD_BANK_ACCOUNT      VARCHAR2(9 BYTE),
  CD_BSB               NUMBER(6),
  VL_DEPOSIT           NUMBER(12,4),
  DT_PAYMENT           DATE,
  NR_PAYMENT           NUMBER(4),
  NR_SEQ_TRANSACTION   VARCHAR2(25 BYTE),
  NR_SEQ_REQUEST       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DVYRESP_PK ON TASY.DVY_RESPONSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DVY_RESPONSE ADD (
  CONSTRAINT DVYRESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.DVY_RESPONSE TO NIVEL_1;

