ALTER TABLE TASY.REGRA_MEDICO_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_MEDICO_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_MEDICO_CONVENIO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_CONVENIO           NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MEDICO_EXECUTOR    VARCHAR2(10 BYTE),
  CD_CGC_PRESTADOR      VARCHAR2(14 BYTE),
  CD_MEDICO_CONVENIO    VARCHAR2(15 BYTE)       NOT NULL,
  IE_PRIORIDADE         NUMBER(3)               NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  IE_FUNCAO_MEDICO      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REMECON_AREPROC_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMECON_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REMECON_CONVENI_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REMECON_ESPPROC_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMECON_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REMECON_ESTABEL_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMECON_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REMECON_GRUPROC_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMECON_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REMECON_PESFISI_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REMECON_PESJURI_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMECON_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REMECON_PK ON TASY.REGRA_MEDICO_CONVENIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REMECON_PROCEDI_FK_I ON TASY.REGRA_MEDICO_CONVENIO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REMECON_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_MEDICO_CONVENIO_tp  after update ON TASY.REGRA_MEDICO_CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO_EXECUTOR,1,4000),substr(:new.CD_MEDICO_EXECUTOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_EXECUTOR',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_PRESTADOR,1,4000),substr(:new.CD_CGC_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_PRESTADOR',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO_CONVENIO,1,4000),substr(:new.CD_MEDICO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_CONVENIO',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRIORIDADE,1,4000),substr(:new.IE_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIORIDADE',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'REGRA_MEDICO_CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_MEDICO_CONVENIO ADD (
  CONSTRAINT REMECON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_MEDICO_CONVENIO ADD (
  CONSTRAINT REMECON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REMECON_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT REMECON_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT REMECON_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT REMECON_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT REMECON_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REMECON_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT REMECON_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.REGRA_MEDICO_CONVENIO TO NIVEL_1;

