ALTER TABLE TASY.TEL_REG_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TEL_REG_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TEL_REG_EVENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_EVENTO             DATE                    NOT NULL,
  NR_RAMAL              VARCHAR2(15 BYTE)       NOT NULL,
  CD_EVENTO             VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_EVENTO         NUMBER(10),
  NR_SEQ_ACAO           NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_UNIDADE_BASICA     VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL      VARCHAR2(10 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DS_ERRO               VARCHAR2(255 BYTE),
  DT_REALIZACAO         DATE,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_PROCESSADO         NUMBER(1)               NOT NULL,
  NR_PAGER_BIP          VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TELREEV_ESTABEL_FK_I ON TASY.TEL_REG_EVENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TELREEV_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TELREEV_PK ON TASY.TEL_REG_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TELREEV_PK
  MONITORING USAGE;


CREATE INDEX TASY.TELREEV_TELACTA_FK_I ON TASY.TEL_REG_EVENTO
(NR_SEQ_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TELREEV_TELACTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TELREEV_TELEVEN_FK_I ON TASY.TEL_REG_EVENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TELREEV_TELEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TELREEV_UNIATEN_FK_I ON TASY.TEL_REG_EVENTO
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TELREEV_UNIATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Tel_Reg_Evento_INSERT
BEFORE INSERT ON TASY.TEL_REG_EVENTO FOR EACH ROW
declare
nr_seq_evento_w	Number(10,0);
nr_seq_interno_w	Number(10,0);
ds_erro_w		Varchar2(255);
cd_unidade_compl_w	Varchar2(10);

cursor c01 is
	select	nr_seq_interno,
			cd_unidade_compl
	from	unidade_atendimento
	where	nr_ramal	= :new.nr_ramal
	order by 1 desc;

BEGIN

select	max(nr_sequencia)
into	nr_seq_evento_w
from	tel_evento
where	cd_evento	= :new.cd_evento;

if	(nr_seq_evento_w is not null) then
	:new.nr_seq_evento	:= nr_seq_evento_w;
	select	nr_seq_acao
	into	:new.nr_seq_acao
	from	tel_evento
	where	nr_sequencia	= nr_seq_evento_w;
end if;

:new.ds_erro		:= wheb_mensagem_pck.get_texto(792020);

if	(:new.nm_usuario is null) then
	:new.nm_usuario := wheb_mensagem_pck.get_texto(792021);
end if;

if	(:new.nr_seq_acao is not null) and
	(:new.nr_seq_acao in (2, 4, 5, 6, 9, 13, 14, 15)) then

	open c01;
	loop
	fetch c01 into	nr_seq_interno_w,
					cd_unidade_compl_w;
	exit when c01%notfound;

	if 	(obter_se_somente_numero(cd_unidade_compl_w) = 'S') then
		if	((instr(:new.ds_observacao, '-> 1') > 0) and
			 (mod(somente_numero(cd_unidade_compl_w),2) = 1)) or
			((instr(:new.ds_observacao, '-> 2') > 0) and
			 (mod(somente_numero(cd_unidade_compl_w),2) = 0)) then
			exit;
		end if;
	end if;
	end loop;
	close c01;

	if	(nr_seq_interno_w is null) and
		(:new.cd_setor_atendimento is null) and
		(:new.cd_unidade_basica is not null) and
		(:new.cd_unidade_compl is not null) then
		select	min(cd_setor_atendimento),
				min(nr_seq_interno)
		into	:new.cd_setor_atendimento,
				nr_seq_interno_w
		from	unidade_atendimento
		where	cd_unidade_basica = :new.cd_unidade_basica
		and		cd_unidade_compl  = :new.cd_unidade_compl;
	end if;

	if	(nr_seq_interno_w is not null) then
		select 	cd_setor_atendimento,
				cd_unidade_basica,
				cd_unidade_compl
		into	:new.cd_setor_atendimento,
				:new.cd_unidade_basica,
				:new.cd_unidade_compl
		from	unidade_atendimento
		where	nr_seq_interno	= nr_seq_interno_w;

		Atualizar_Acao_tasy(nr_seq_interno_w, :new.nr_seq_acao, nvl(:new.nm_usuario, wheb_mensagem_pck.get_texto(792021)), ds_erro_w);

		:new.ds_erro		:= ds_erro_w;
	else
		:new.ds_erro		:= wheb_mensagem_pck.get_texto(792022);

	end if;
else
	:new.ds_erro		:= wheb_mensagem_pck.get_texto(792027);
end if;

END;
/


ALTER TABLE TASY.TEL_REG_EVENTO ADD (
  CONSTRAINT TELREEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TEL_REG_EVENTO ADD (
  CONSTRAINT TELREEV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TELREEV_TELACTA_FK 
 FOREIGN KEY (NR_SEQ_ACAO) 
 REFERENCES TASY.TEL_ACAO_TASY (NR_SEQUENCIA),
  CONSTRAINT TELREEV_TELEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.TEL_EVENTO (NR_SEQUENCIA),
  CONSTRAINT TELREEV_UNIATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL));

GRANT SELECT ON TASY.TEL_REG_EVENTO TO NIVEL_1;

