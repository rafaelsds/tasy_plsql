DROP TABLE TASY.PLS_MENS_BENEF_PAG_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_MENS_BENEF_PAG_TMP
(
  NR_SEQ_SEGURADO            NUMBER(10),
  NR_SEQ_PAGADOR             NUMBER(10),
  IE_TIPO_PAGADOR            VARCHAR2(2 BYTE),
  IE_TIPO_ITEM               VARCHAR2(3 BYTE),
  NR_SEQ_TIPO_LANC           NUMBER(10),
  DT_INICIO_VIG_PAG          DATE,
  DT_FIM_VIG_PAG             DATE,
  NR_SEQ_CENTRO_APROPRIACAO  NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.PLSMBPT_I2 ON TASY.PLS_MENS_BENEF_PAG_TMP
(NR_SEQ_PAGADOR);


CREATE INDEX TASY.PLSMBPT_I3 ON TASY.PLS_MENS_BENEF_PAG_TMP
(NR_SEQ_SEGURADO, IE_TIPO_PAGADOR, DT_INICIO_VIG_PAG, DT_FIM_VIG_PAG);


GRANT SELECT ON TASY.PLS_MENS_BENEF_PAG_TMP TO NIVEL_1;

