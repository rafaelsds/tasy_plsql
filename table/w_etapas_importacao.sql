ALTER TABLE TASY.W_ETAPAS_IMPORTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ETAPAS_IMPORTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ETAPAS_IMPORTACAO
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  NM_USUARIO    VARCHAR2(15 BYTE)               NOT NULL,
  NR_SEQ_ETAPA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WETIM_PK ON TASY.W_ETAPAS_IMPORTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_ETAPAS_IMPORTACAO ADD (
  CONSTRAINT WETIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_ETAPAS_IMPORTACAO TO NIVEL_1;

