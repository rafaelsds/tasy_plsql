ALTER TABLE TASY.PLS_PARAMETROS_REC_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PARAMETROS_REC_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PARAMETROS_REC_GLOSA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  QT_RECURSO_CONTA             NUMBER(10),
  IE_VERIFICAR_REGRA_RECURSO   VARCHAR2(1 BYTE),
  IE_PROCESSO_ANALISE_RECURSO  VARCHAR2(5 BYTE),
  IE_GERAR_COPARTICIPACAO      VARCHAR2(2 BYTE),
  IE_FINALIZA_SEM_ITENS_PEND   VARCHAR2(5 BYTE),
  IE_INTEGRAR_IMP              VARCHAR2(5 BYTE),
  IE_AGRUPAR_LOTE_IMP          VARCHAR2(5 BYTE),
  IE_GERAR_VAL_ADIC            VARCHAR2(15 BYTE),
  IE_QT_MAX_CONTA              NUMBER(10),
  IE_PRESTADOR_PAGAMENTO       VARCHAR2(6 BYTE),
  IE_PARTICIPANTE_COOPERADO    VARCHAR2(3 BYTE),
  IE_CONSID_NEGADO             VARCHAR2(1 BYTE),
  IE_PGTO_DEFINITIVO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.PLSPREG_ESTABEL_FK_I ON TASY.PLS_PARAMETROS_REC_GLOSA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPREG_PK ON TASY.PLS_PARAMETROS_REC_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_PARAMETROS_REC_GLOSA_tp  after update ON TASY.PLS_PARAMETROS_REC_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_QT_MAX_CONTA,1,4000),substr(:new.IE_QT_MAX_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_QT_MAX_CONTA',ie_log_w,ds_w,'PLS_PARAMETROS_REC_GLOSA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESTADOR_PAGAMENTO,1,4000),substr(:new.IE_PRESTADOR_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_PAGAMENTO',ie_log_w,ds_w,'PLS_PARAMETROS_REC_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PARAMETROS_REC_GLOSA ADD (
  CONSTRAINT PLSPREG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PARAMETROS_REC_GLOSA ADD (
  CONSTRAINT PLSPREG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_PARAMETROS_REC_GLOSA TO NIVEL_1;

