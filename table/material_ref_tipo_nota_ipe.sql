ALTER TABLE TASY.MATERIAL_REF_TIPO_NOTA_IPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_REF_TIPO_NOTA_IPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_REF_TIPO_NOTA_IPE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TIPO_NOTA  NUMBER(10)            NOT NULL,
  CD_MATERIAL             NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MRETNI_MATERIA_FK_I ON TASY.MATERIAL_REF_TIPO_NOTA_IPE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MRETNI_PK ON TASY.MATERIAL_REF_TIPO_NOTA_IPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MRETNI_RGTPNOTIP_FK_I ON TASY.MATERIAL_REF_TIPO_NOTA_IPE
(NR_SEQ_REGRA_TIPO_NOTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MATERIAL_REF_TIPO_NOTA_IPE ADD (
  CONSTRAINT MRETNI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_REF_TIPO_NOTA_IPE ADD (
  CONSTRAINT MRETNI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MRETNI_RGTPNOTIP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TIPO_NOTA) 
 REFERENCES TASY.REGRA_TIPO_NOTA_IPE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_REF_TIPO_NOTA_IPE TO NIVEL_1;

