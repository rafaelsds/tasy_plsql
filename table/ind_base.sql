ALTER TABLE TASY.IND_BASE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IND_BASE CASCADE CONSTRAINTS;

CREATE TABLE TASY.IND_BASE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_INDICADOR         VARCHAR2(255 BYTE),
  NR_SEQ_ORDEM_SERV    NUMBER(20),
  IE_PADRAO_SISTEMA    VARCHAR2(1 BYTE)         NOT NULL,
  DS_SQL_WHERE         LONG,
  DS_ORIGEM_INF        VARCHAR2(255 BYTE)       NOT NULL,
  CD_EXP_INDICADOR     NUMBER(10),
  DS_OBJETIVO          VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  NR_SEQ_IND_GESTAO    NUMBER(10),
  IE_QUALIDADE         VARCHAR2(1 BYTE),
  NR_SEQ_SUPERIOR      NUMBER(10),
  NR_SEQ_APRES         NUMBER(5),
  IE_ORIGEM            VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INDBASE_INDBASE_FK_I ON TASY.IND_BASE
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INDBASE_PK ON TASY.IND_BASE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.IND_BASE_tp  after update ON TASY.IND_BASE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_INDICADOR,1,500);gravar_log_alteracao(substr(:old.DS_INDICADOR,1,4000),substr(:new.DS_INDICADOR,1,4000),:new.nm_usuario,nr_seq_w,'DS_INDICADOR',ie_log_w,ds_w,'IND_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ORIGEM_INF,1,500);gravar_log_alteracao(substr(:old.DS_ORIGEM_INF,1,4000),substr(:new.DS_ORIGEM_INF,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIGEM_INF',ie_log_w,ds_w,'IND_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_PADRAO_SISTEMA,1,500);gravar_log_alteracao(substr(:old.IE_PADRAO_SISTEMA,1,4000),substr(:new.IE_PADRAO_SISTEMA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO_SISTEMA',ie_log_w,ds_w,'IND_BASE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM_SERV,1,4000),substr(:new.NR_SEQ_ORDEM_SERV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM_SERV',ie_log_w,ds_w,'IND_BASE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.IND_BASE ADD (
  CONSTRAINT INDBASE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.IND_BASE ADD (
  CONSTRAINT INDBASE_INDBASE_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.IND_BASE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IND_BASE TO NIVEL_1;

