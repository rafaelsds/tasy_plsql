ALTER TABLE TASY.AP_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AP_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.AP_LOTE
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_PRESCRICAO               NUMBER(14),
  NR_SEQ_TURNO                NUMBER(10),
  DT_DISP_FARMACIA            DATE,
  DT_RECEBIMENTO_SETOR        DATE,
  DT_ATEND_FARMACIA           DATE,
  NM_USUARIO_ATEND            VARCHAR2(15 BYTE),
  NM_USUARIO_DISP             VARCHAR2(15 BYTE),
  DT_ENTREGA_SETOR            DATE,
  NM_USUARIO_ENTREGA          VARCHAR2(15 BYTE),
  NM_USUARIO_RECEB            VARCHAR2(15 BYTE),
  IE_STATUS_LOTE              VARCHAR2(3 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  DT_GERACAO_LOTE             DATE              NOT NULL,
  NM_USUARIO_GERACAO          VARCHAR2(15 BYTE),
  DS_MAQUINA_GERACAO          VARCHAR2(80 BYTE),
  DT_ATEND_LOTE               DATE,
  DT_INICIO_TURNO             DATE,
  NR_SEQ_CLASSIF              NUMBER(10),
  DT_INICIO_DISPENSACAO       DATE,
  NM_USUARIO_INI_DISP         VARCHAR2(15 BYTE),
  DS_MAQUINA_INI_DISP         VARCHAR2(80 BYTE),
  DS_MAQUINA_ATEND            VARCHAR2(80 BYTE),
  DS_MAQUINA_DISP             VARCHAR2(80 BYTE),
  DS_MAQUINA_ENTREGA          VARCHAR2(80 BYTE),
  DS_MAQUINA_RECEB            VARCHAR2(80 BYTE),
  DT_CANCELAMENTO             DATE,
  NM_USUARIO_CANCELAMENTO     VARCHAR2(15 BYTE),
  DS_MAQUINA_CANCELAMENTO     VARCHAR2(80 BYTE),
  DT_LIMITE_INICIO_ATEND      DATE,
  DT_LIMITE_ATEND             DATE,
  DT_LIMITE_DISP_FARM         DATE,
  DT_LIMITE_ENTREGA_SETOR     DATE,
  DT_LIMITE_RECEB_SETOR       DATE,
  QT_MIN_ATRASO_INICIO_ATEND  NUMBER(15),
  QT_MIN_ATRASO_ATEND         NUMBER(15),
  QT_MIN_ATRASO_DISP          NUMBER(15),
  QT_MIN_ATRASO_ENTREGA       NUMBER(15),
  QT_MIN_ATRASO_RECEB         NUMBER(15),
  CD_TIPO_BAIXA               NUMBER(3),
  IE_CONTA_PACIENTE           VARCHAR2(1 BYTE),
  IE_ATUALIZA_ESTOQUE         VARCHAR2(1 BYTE),
  DT_PRIM_HORARIO             DATE,
  DT_IMPRESSAO                DATE,
  CD_LOCAL_ESTOQUE            NUMBER(4),
  NR_SEQ_LOTE_SUP             NUMBER(10),
  CD_PERFIL_GERACAO           NUMBER(5),
  CD_PERFIL_INI_DISP          NUMBER(5),
  CD_PERFIL_ATEND             NUMBER(5),
  CD_PERFIL_DISP              NUMBER(5),
  CD_PERFIL_ENTREGA           NUMBER(5),
  CD_PERFIL_RECEB             NUMBER(5),
  CD_PERFIL_CANCEL            NUMBER(5),
  QT_PRIORIDADE               NUMBER(3),
  IE_ORIGEM_LOTE              VARCHAR2(15 BYTE),
  DT_IMPRESSAO_AQUI           DATE,
  NM_USUARIO_IMP_AQUI         VARCHAR2(15 BYTE),
  DS_MAQUINA_IMP_AQUI         VARCHAR2(80 BYTE),
  CD_PERFIL_IMP_AQUI          NUMBER(5),
  NR_SEQ_MOTIVO_CANCEL        NUMBER(10),
  IE_REAPRAZADO               VARCHAR2(1 BYTE),
  DT_ENVIO_INTEGRACAO         DATE,
  IE_INTEGRACAO               VARCHAR2(1 BYTE),
  NR_SEQ_EMBALAGEM            NUMBER(10),
  CD_SETOR_ANT                NUMBER(5),
  NR_SEQ_MOT_DESDOBRAR        NUMBER(10),
  DT_EMISSAO_RESUMO           DATE,
  NM_USUARIO_CONFERENCIA      VARCHAR2(15 BYTE),
  DT_CONFERENCIA              DATE,
  DS_MAQUINA_CONFERENCIA      VARCHAR2(80 BYTE),
  IE_GERACAO_ROTINA           VARCHAR2(1 BYTE),
  DS_ORIGEM_GER_LOTE          VARCHAR2(255 BYTE),
  DS_STACK                    VARCHAR2(2000 BYTE),
  NR_ATENDIMENTO              NUMBER(10)        DEFAULT null,
  NR_SEQ_LOCAL_GERACAO        VARCHAR2(5 BYTE),
  DS_PRESCRICOES              VARCHAR2(255 BYTE),
  IE_AGRUPAMENTO              VARCHAR2(1 BYTE),
  NR_LOTE_AGRUPAMENTO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APLOTE_AGRUP_FK ON TASY.AP_LOTE
(NR_LOTE_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_APLOTE_FK_I ON TASY.AP_LOTE
(NR_SEQ_LOTE_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_ATEPACI_FK_I ON TASY.AP_LOTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_CLALODF_FK_I ON TASY.AP_LOTE
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_CLALODF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_EMBALAG_FK_I ON TASY.AP_LOTE
(NR_SEQ_EMBALAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_EMBALAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_I1 ON TASY.AP_LOTE
(IE_STATUS_LOTE, NR_SEQ_TURNO, CD_SETOR_ATENDIMENTO, DT_GERACAO_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I10 ON TASY.AP_LOTE
(DT_PRIM_HORARIO, NR_PRESCRICAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I11 ON TASY.AP_LOTE
(IE_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I12 ON TASY.AP_LOTE
(CD_SETOR_ATENDIMENTO, DT_IMPRESSAO, DT_ATEND_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I2 ON TASY.AP_LOTE
(DT_GERACAO_LOTE, CD_SETOR_ATENDIMENTO, NR_SEQ_TURNO, IE_STATUS_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I3 ON TASY.AP_LOTE
(DT_ATEND_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I4 ON TASY.AP_LOTE
(IE_STATUS_LOTE, CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I5 ON TASY.AP_LOTE
(IE_STATUS_LOTE, NR_SEQ_TURNO, CD_SETOR_ATENDIMENTO, DT_ATEND_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I6 ON TASY.AP_LOTE
(IE_STATUS_LOTE, NM_USUARIO_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I7 ON TASY.AP_LOTE
(DT_PRIM_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_I8 ON TASY.AP_LOTE
(DT_ATEND_FARMACIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_I8
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_I9 ON TASY.AP_LOTE
(IE_STATUS_LOTE, NR_SEQ_CLASSIF, CD_SETOR_ATENDIMENTO, DT_GERACAO_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_MODESLO_FK_I ON TASY.AP_LOTE
(NR_SEQ_MOT_DESDOBRAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_MODESLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK_I ON TASY.AP_LOTE
(CD_PERFIL_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK2_I ON TASY.AP_LOTE
(CD_PERFIL_INI_DISP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK3_I ON TASY.AP_LOTE
(CD_PERFIL_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK4_I ON TASY.AP_LOTE
(CD_PERFIL_DISP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK5_I ON TASY.AP_LOTE
(CD_PERFIL_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK6_I ON TASY.AP_LOTE
(CD_PERFIL_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK7_I ON TASY.AP_LOTE
(CD_PERFIL_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK7_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_PERFIL_FK8_I ON TASY.AP_LOTE
(CD_PERFIL_IMP_AQUI)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_PERFIL_FK8_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.APLOTE_PK ON TASY.AP_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_PRESMED_FK_I ON TASY.AP_LOTE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_REGTUDI_FK_I ON TASY.AP_LOTE
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTE_SETATEN_FK_I ON TASY.AP_LOTE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APLOTE_SETATEN_FK2_I ON TASY.AP_LOTE
(CD_SETOR_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTE_SETATEN_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ap_lote_after
after update ON TASY.AP_LOTE for each row
declare
pragma autonomous_transaction;

qt_lotes_susp_w			number(5);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
  qt_lotes_susp_w:= 5;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.AP_LOTE_ATUAL
BEFORE INSERT OR UPDATE ON TASY.AP_LOTE FOR EACH ROW
DECLARE

ds_alteracao_w		varchar2(1800);
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
qt_existe_regra_w	number(10);
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
ie_permite_atend_lote_w		setor_atendimento.ie_permite_atend_lote%type := 'S';

BEGIN

if wheb_usuario_pck.get_ie_executar_trigger = 'S' then
	if	(updating) then
		if	(nvl(:old.cd_local_estoque,0) <> nvl(:new.cd_local_estoque,0)) then

			ds_alteracao_w	:= substr(ds_alteracao_w || ' CD_LOCAL_ESTOQUE(' || nvl(:old.cd_local_estoque,0) || '/' || nvl(:new.cd_local_estoque,0)||'); ',1,1800);

			select	count(1)
			into	qt_existe_regra_w
			from	dis_regra_setor;

			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	prescr_medica
			where	nr_prescricao = :new.nr_prescricao;

			if	(qt_existe_regra_w > 0) then

				select	count(1)
				into	qt_existe_regra_w
				from	local_estoque c,
					dis_regra_setor b,
					dis_regra_local_setor a
				where	a.nr_seq_dis_regra_setor = b.nr_sequencia
				and	c.cd_local_estoque = a.cd_local_estoque
				and	b.cd_setor_atendimento = obter_setor_atendimento(nr_atendimento_w)
				and	a.cd_local_estoque = :new.cd_local_estoque
				and	c.ie_tipo_local = '11';

				if	(qt_existe_regra_w > 0) then
					intdisp_movto_mat_hor(:new.nr_sequencia, :new.ie_status_lote, :new.cd_local_estoque, '1');
				else

					select	count(1)
					into	qt_existe_regra_w
					from	int_disp_mat_hor a
					where	a.nr_seq_lote = nvl(:new.nr_seq_lote_sup,:new.nr_sequencia)
					and	a.ie_tipo_movimentacao = 'EOA'
					and	a.cd_local_estoque = :old.cd_local_estoque;

					if	(qt_existe_regra_w > 0) then
						intdisp_movto_mat_hor(:new.nr_sequencia, :new.ie_status_lote, :old.cd_local_estoque, '2');
					end if;

					select	count(1)
					into	qt_existe_regra_w
					from	local_estoque c,
						dis_regra_setor b,
						dis_regra_local_setor a
					where	a.nr_seq_dis_regra_setor = b.nr_sequencia
					and	c.cd_local_estoque = a.cd_local_estoque
					and	a.cd_local_estoque = :new.cd_local_estoque
					and	c.ie_tipo_local = '11';

					if	(qt_existe_regra_w > 0) then
						intdisp_movto_mat_hor(:new.nr_sequencia, :new.ie_status_lote, :new.cd_local_estoque, '1');
					end if;
				end if;
			end if;
			if	(:new.ie_status_lote = 'G') then
				gerar_int_dankia_pck.dankia_gerar_horario_mov(:new.nr_prescricao, :new.nr_sequencia, nr_atendimento_w, :new.cd_local_estoque, :old.cd_local_estoque, :new.nm_usuario);
			end if;

		end if;

		if	(nvl(:old.cd_setor_atendimento,0) <> nvl(:new.cd_setor_atendimento,0)) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' CD_SETOR_ATENDIMENTO(' || nvl(:old.cd_setor_atendimento,0) || '/' || nvl(:new.cd_setor_atendimento,0)||'); ',1,1800);

			select	nvl(max(ie_permite_atend_lote),'S')
			into	ie_permite_atend_lote_w
			from	setor_atendimento
			where	cd_setor_atendimento = :new.cd_setor_atendimento;

			if	(ie_permite_atend_lote_w = 'N') then
				:new.cd_setor_atendimento := :old.cd_setor_atendimento;
			end if;
		end if;

		if	(nvl(:old.ie_status_lote,0) <> nvl(:new.ie_status_lote,0)) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' IE_STATUS_LOTE(' || nvl(:old.ie_status_lote,0) || '/' || nvl(:new.ie_status_lote,0)||'); ',1,1800);
			if	(:new.ie_status_lote in ('C', 'CA')) then
				gerar_int_dankia_pck.dankia_cancelamento_lote(:new.nr_prescricao, :new.nr_sequencia, :new.cd_local_estoque, :new.nm_usuario);
			end if;
		end if;

		if	(:old.dt_inicio_dispensacao is not null) and (:new.dt_inicio_dispensacao is null) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' DT_INICIO_DISPENSACAO(' || :old.dt_inicio_dispensacao || '/' || :new.dt_inicio_dispensacao || '); ',1,1800);
		end if;

		if	(:old.dt_inicio_dispensacao is null and :new.dt_inicio_dispensacao is not null or nvl(:old.dt_inicio_dispensacao, sysdate) <> nvl(:new.dt_inicio_dispensacao, sysdate)) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' DT_INICIO_DISPENSACAO(' || :old.dt_inicio_dispensacao || '/' || :new.dt_inicio_dispensacao || '); ',1,1800);
		end if;

		if	(:old.dt_impressao is not null) and (:new.dt_impressao is null) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' DT_IMPRESSAO(' || :old.dt_impressao || '/' || :new.dt_impressao || '); ',1,1800);
		end if;

		if	(:old.nr_seq_turno is not null) and (:new.nr_seq_turno is null) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' NR_SEQ_TURNO(' || :old.nr_seq_turno || '/' || :new.nr_seq_turno || '); ',1,1800);
		end if;

		if	(nvl(:old.nr_seq_classif,0) <> nvl(:new.nr_seq_classif,0)) then
			ds_alteracao_w	:= substr(ds_alteracao_w || ' NR_SEQ_CLASSIF(' || nvl(:old.nr_seq_classif,0) || '/' || nvl(:new.nr_seq_classif,0)||'); ',1,1800);
		end if;

		if	(ds_alteracao_w is not null) then
			ds_alteracao_w := substr(' '||wheb_mensagem_pck.get_texto(167701)||'(old/new)= '||ds_alteracao_w||' FUNCAO('||to_char(wheb_usuario_pck.get_cd_funcao)||'); PERFIL('||to_char(wheb_usuario_pck.get_cd_perfil)||
						 '); STACK('||substr(dbms_utility.format_call_stack,1,1500)||');',1,1800);

			gravar_log_tasy(99997,'LotePrescr=' || :new.nr_sequencia || ds_alteracao_w || ';CD_SETOR_LOGADO='||wheb_usuario_pck.get_cd_setor_atendimento,:new.nm_usuario);
		end if;


		if	(:new.ie_status_lote = 'R') and (:new.nm_usuario_receb is null) then
			-- Nao foi possivel receber o lote!
			wheb_mensagem_pck.exibir_mensagem_abort(252539);
		end if;
	end if;

	:new.qt_min_atraso_inicio_atend	:= 0;
	:new.qt_min_atraso_atend	:= 0;
	:new.qt_min_atraso_disp		:= 0;
	:new.qt_min_atraso_entrega	:= 0;
	:new.qt_min_atraso_receb	:= 0;

	if	(:new.dt_limite_inicio_atend is not null) and
		(:new.dt_inicio_dispensacao is not null) and
		(:new.dt_limite_inicio_atend < :new.dt_inicio_dispensacao) then
		begin
		:new.qt_min_atraso_inicio_atend	:= round((:new.dt_inicio_dispensacao - :new.dt_limite_inicio_atend) * 1440);
		end;
	end if;
	if	(:new.dt_limite_atend is not null) and
		(:new.dt_atend_farmacia is not null) and
		(:new.dt_limite_atend < :new.dt_atend_farmacia) then
		begin
		:new.qt_min_atraso_atend	:= round((:new.dt_atend_farmacia - :new.dt_limite_atend) * 1440);
		end;
	end if;

	if	(:new.dt_limite_disp_farm is not null) and
		(:new.dt_disp_farmacia is not null) and
		(:new.dt_limite_disp_farm < :new.dt_disp_farmacia) then
		begin
		:new.qt_min_atraso_disp		:= round((:new.dt_disp_farmacia - :new.dt_limite_disp_farm) * 1440);
		end;
	end if;

	if	(:new.dt_limite_entrega_setor is not null) and
		(:new.dt_entrega_setor is not null) and
		(:new.dt_limite_entrega_setor < :new.dt_entrega_setor) then
		begin
		:new.qt_min_atraso_entrega	:= round((:new.dt_entrega_setor - :new.dt_limite_entrega_setor) * 1440);
		end;
	end if;

	if	(:new.dt_limite_receb_setor is not null) and
		(:new.dt_recebimento_setor is not null) and
		(:new.dt_limite_receb_setor < :new.dt_recebimento_setor) then
		begin
		:new.qt_min_atraso_receb	:= round((:new.dt_recebimento_setor - :new.dt_limite_receb_setor) * 1440);
		end;
	end if;

	if	(inserting) then
		:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
	end if;
end if;
END;
/


ALTER TABLE TASY.AP_LOTE ADD (
  CONSTRAINT APLOTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AP_LOTE ADD (
  CONSTRAINT APLOTE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT APLOTE_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT APLOTE_REGTUDI_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.REGRA_TURNO_DISP (NR_SEQUENCIA),
  CONSTRAINT APLOTE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT APLOTE_CLALODF_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_LOTE_DISP_FAR (NR_SEQUENCIA),
  CONSTRAINT APLOTE_APLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE_SUP) 
 REFERENCES TASY.AP_LOTE (NR_SEQUENCIA),
  CONSTRAINT APLOTE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_GERACAO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK2 
 FOREIGN KEY (CD_PERFIL_INI_DISP) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK3 
 FOREIGN KEY (CD_PERFIL_ATEND) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK4 
 FOREIGN KEY (CD_PERFIL_DISP) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK5 
 FOREIGN KEY (CD_PERFIL_ENTREGA) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK6 
 FOREIGN KEY (CD_PERFIL_RECEB) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK7 
 FOREIGN KEY (CD_PERFIL_CANCEL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_PERFIL_FK8 
 FOREIGN KEY (CD_PERFIL_IMP_AQUI) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT APLOTE_EMBALAG_FK 
 FOREIGN KEY (NR_SEQ_EMBALAGEM) 
 REFERENCES TASY.EMBALAGEM (NR_SEQUENCIA),
  CONSTRAINT APLOTE_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ANT) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT APLOTE_MODESLO_FK 
 FOREIGN KEY (NR_SEQ_MOT_DESDOBRAR) 
 REFERENCES TASY.MOTIVO_DESDOBRAR_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.AP_LOTE TO NIVEL_1;

