ALTER TABLE TASY.ATENDIMENTO_ALTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_ALTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_ALTA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_TIPO_ORIENTACAO         VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_ORIENTACAO              LONG,
  IE_DESFECHO                VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE           NUMBER(5),
  CD_MOTIVO_ALTA             NUMBER(3),
  CD_MEDICO_DEST             VARCHAR2(10 BYTE),
  IE_MEDICO_CIENTE           VARCHAR2(1 BYTE)   NOT NULL,
  DT_DESFECHO                DATE,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  DT_LIBERACAO               DATE,
  CD_PROCESSO_ALTA           NUMBER(10),
  QT_CARACTERES              NUMBER(15),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  NR_SEQ_PEPO                NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_CIRURGIA                NUMBER(10),
  DT_REGISTRO                DATE,
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_TIPO_EVOLUCAO           VARCHAR2(3 BYTE),
  QT_PERIODO                 NUMBER(10),
  IE_PERIODO                 VARCHAR2(15 BYTE),
  CD_ESPECIALIDADE_MEDICO    NUMBER(5),
  CD_CGC                     VARCHAR2(14 BYTE),
  NR_SEQ_MOTIVO              NUMBER(10),
  IE_RECEM_NATO              VARCHAR2(1 BYTE),
  NR_RECEM_NATO              NUMBER(3),
  NR_SEQ_AREA_ATUACAO        NUMBER(10),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_DOENCA_CID              VARCHAR2(10 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  IE_DESFECHO_CONSULTA       VARCHAR2(1 BYTE),
  IE_DESFECHO_TRIAGEM        VARCHAR2(1 BYTE),
  NR_SEQ_SOAP                NUMBER(10),
  NR_SEQ_ORIGEM              NUMBER(10)         DEFAULT null,
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_SEQ_FORMULARIO          NUMBER(10),
  IE_FICOU_OBSERVACAO        VARCHAR2(1 BYTE),
  NR_SEQ_NAIS_INSURANCE      NUMBER(10),
  CD_EVOLUCAO                NUMBER(10),
  CD_ESPECIALIDADE_MED       NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEALTA_AREATUM_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_AREA_ATUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_ATCONSPEPA_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_ATEPACI_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_ATESOAP_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_CIRURGI_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_EHRREGI_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_ESPMEDI_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_ESPMEDI_FK2_I ON TASY.ATENDIMENTO_ALTA
(CD_ESPECIALIDADE_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_ESPMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_EVOPACI_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_I1 ON TASY.ATENDIMENTO_ALTA
(DT_LIBERACAO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_I1
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_I2 ON TASY.ATENDIMENTO_ALTA
(IE_TIPO_ORIENTACAO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_I3 ON TASY.ATENDIMENTO_ALTA
(DT_DESFECHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_I3
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_I4 ON TASY.ATENDIMENTO_ALTA
(NR_ATENDIMENTO, IE_DESFECHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_I4_TK ON TASY.ATENDIMENTO_ALTA
(DT_DESFECHO, IE_DESFECHO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_MOTALTA_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_MOTIVO_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_MOTTRPA_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_MOTTRPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_NAISINS_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_PEPOCIR_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_PERFIL_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_PESFISI_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_MEDICO_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_PESFISI_FK2_I ON TASY.ATENDIMENTO_ALTA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_PESJURI_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATEALTA_PK ON TASY.ATENDIMENTO_ALTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_SETATEN_FK_I ON TASY.ATENDIMENTO_ALTA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEALTA_TASASDI_FK_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEALTA_TASASDI_FK2_I ON TASY.ATENDIMENTO_ALTA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEALTA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atendimento_alta_update
AFTER UPDATE ON TASY.ATENDIMENTO_ALTA FOR EACH ROW
declare
ie_status_pa_w	varchar2(15);
qt_reg_w	number(1);
ie_liberar_desfecho_w varchar2(1);

BEGIN

select nvl(max(ie_liberar_desfecho),'N')
into   ie_liberar_desfecho_w
from   parametro_medico
where  cd_estabelecimento = obter_estabelecimento_ativo;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  or ((:new.ie_tipo_orientacao = 'P') and (:new.dt_liberacao is null) and (ie_liberar_desfecho_w = 'S')) then
	goto Final;
end if;

if	(:new.ie_desfecho <> 'T') and
	(:new.ie_tipo_orientacao = 'P') then

	update	atendimento_paciente
	set	dt_lib_medico	= sysdate
	where	nr_atendimento	= :new.nr_atendimento
	and	dt_lib_medico is null;
end if;

if 	(:new.ie_desfecho = 'I') then

	select 	nvl(max(ie_status_pa),'NA')
	into	ie_status_pa_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if 	(ie_status_pa_w not in ('AL','PL')) then

		update	atendimento_paciente
		set	ie_status_pa = 'IN'
		where	nr_atendimento	= :new.nr_atendimento;

		gravar_atend_status_pa(:new.nr_atendimento,'IN',:new.nm_usuario);

	end if;

end if;

if	(:new.dt_liberacao is not null)	and
	(:old.dt_liberacao is null) and
	(:new.nr_Atendimento is not null) then

	executar_evento_agenda_atend(:new.nr_Atendimento,'LOP',obter_estab_atend(:new.nr_Atendimento),:new.nm_usuario);
end if;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

	update evolucao_paciente
	set dt_inativacao = sysdate,
	ie_situacao ='I',
	dt_atualizacao = sysdate ,
	nm_usuario = wheb_usuario_pck.get_nm_usuario,
	nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
	ds_justificativa = :new.ds_justificativa
	where cd_evolucao = :new.cd_evolucao;
	delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'DSCHRG_INSTR'  and ie_stage = 1  and ie_soap_type = 'P' and nr_seq_med_item = :new.nr_sequencia;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.Atendimento_Alta_BeforeInsert
BEFORE INSERT ON TASY.ATENDIMENTO_ALTA FOR EACH ROW
declare
cd_pessoa_fisica_w	varchar2(10);
BEGIN

if	(:new.ie_tipo_orientacao <> 'P') then
	:new.cd_setor_atendimento	:= Obter_Unidade_Atendimento(:new.nr_atendimento,'IAA','CS');
end if;


select	max(ie_tipo_evolucao),
	max(cd_pessoa_fisica)
into	:new.ie_tipo_evolucao,
	cd_pessoa_fisica_w
from	usuario
where	nm_usuario	= :new.nm_usuario;

if	(cd_pessoa_fisica_w	is not null) then
	:new.cd_especialidade_medico	:= obter_especialidade_medico(cd_pessoa_fisica_w,'C');
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_alta_atual
BEFORE INSERT OR UPDATE ON  atendimento_alta
for each row
declare
qt_min_caracteres_w		number(15);

begin
if	(nvl(:old.DT_DESFECHO,sysdate+10) <> :new.DT_DESFECHO) and
	(:new.DT_DESFECHO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_DESFECHO,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(Obter_Funcao_Ativa	= 281) then
	begin
	qt_min_caracteres_w	:= Obter_Valor_Param_Usuario(281,400,Obter_Perfil_Ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
	exception
		when others then
		qt_min_caracteres_w	:= 0;
		end;

	if	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) and
		(qt_min_caracteres_w	> 0) and
		(:new.QT_CARACTERES < qt_min_caracteres_w) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(267649);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_alta_INSERT
AFTER INSERT ON TASY.ATENDIMENTO_ALTA FOR EACH ROW
declare
ie_status_pa_w			varchar2(15);
ie_executar_agenda_w		varchar2(1);
nr_seq_agenda_w			number(10,0);
qt_idade_w			number(10);
nr_seq_evento_desfecho_alta_w	number(10);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_evento_desfecho_int_w	number(10);
dt_fim_consulta_w		date;
qt_reg_w	number(1);
ie_liberar_desfecho_w varchar2(1);

Cursor C01 is
	SELECT	a.nr_seq_evento
	FROM	regra_envio_sms a
	WHERE	a.cd_estabelecimento	= obter_estabelecimento_ativo
	AND	a.ie_evento_disp	= 'DALT'
	AND	qt_idade_w BETWEEN NVL(qt_idade_min,0)	AND NVL(qt_idade_max,9999)
	and	((:new.cd_motivo_alta = cd_motivo_alta) or (0 = nvl(cd_motivo_alta,0)))
	and	nvl(a.ie_situacao,'A') = 'A';

Cursor C02 is
	SELECT	a.nr_seq_evento
	FROM	regra_envio_sms a
	WHERE	a.cd_estabelecimento	= obter_estabelecimento_ativo
	AND	a.ie_evento_disp	= 'DINT'
	AND	qt_idade_w BETWEEN NVL(qt_idade_min,0)	AND NVL(qt_idade_max,9999)
	and	nvl(a.ie_situacao,'A') = 'A';

BEGIN
qt_idade_w	:= NVL(obter_idade_pf(obter_pessoa_atendimento(:new.nr_atendimento,'C'),SYSDATE,'A'),0);
cd_pessoa_fisica_w	:= substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,10);

select nvl(max(ie_liberar_desfecho),'N')
into   ie_liberar_desfecho_w
from   parametro_medico
where  cd_estabelecimento = obter_estabelecimento_ativo;

if ((ie_liberar_desfecho_w = 'S') and (:new.ie_tipo_orientacao = 'P') and (:new.dt_liberacao is null)) then
	goto Final;
end if;

if	(:new.ie_desfecho <> 'T') and
	(:new.ie_tipo_orientacao = 'P') then
	update	atendimento_paciente
	set	dt_lib_medico	= sysdate
	where	nr_atendimento	= :new.nr_atendimento
	and	dt_lib_medico is null;
end if;

if 	(nvl(obter_valor_param_usuario(935, 110, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N') = 'S')  then


	select 	max(dt_fim_consulta)
	into	dt_fim_consulta_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if (dt_fim_consulta_w is null) then

	update	atendimento_paciente
	set		dt_fim_consulta		= sysdate,
			nm_usuario     		= :new.nm_usuario,
			dt_atualizacao 		= sysdate
	where	nr_atendimento		= :new.nr_atendimento
	and		dt_atend_medico is not null;

	end if;

end if;

if 	(:new.ie_desfecho = 'I') then

	select 	nvl(max(ie_status_pa),'NA')
	into	ie_status_pa_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if 	(ie_status_pa_w not in ('AL','PL')) then

		update	atendimento_paciente
		set	ie_status_pa = 'IN'
		where	nr_atendimento	= :new.nr_atendimento;

		gravar_atend_status_pa(:new.nr_atendimento,'IN',:new.nm_usuario);


	end if;

	begin
	open c02;
	loop
	fetch c02 into
		nr_seq_evento_desfecho_int_w;
	exit when c02%notfound;
		begin
		gerar_evento_paciente_trigger( nr_seq_evento_desfecho_int_w ,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario,null,:new.dt_liberacao,null);
		end;
	end loop;
	close c02;
	exception
		when others then
		null;
	end;

elsif	(:new.ie_desfecho = 'T') then
	if	(nvl(obter_valor_param_usuario(935, 137, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N') = 'S') then

		Atualizar_datas_pa(:new.nr_atendimento, 'FC', :new.nm_usuario, 'N');

		insert into atendimento_pac_chamado (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_atendimento,
			dt_inicio_atendimento,
			dt_atend_medico,
			dt_chamada_paciente,
			dt_chamada_reavaliacao,
			dt_reavaliacao_medica,
			dt_chamada_enfermagem,
			ie_status_pa,
			dt_fim_consulta)
		select	atendimento_pac_chamado_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_atendimento,
			dt_inicio_atendimento,
			dt_atend_medico,
			dt_chamada_paciente,
			dt_chamada_reavaliacao,
			dt_reavaliacao_medica,
			dt_chamada_enfermagem,
			ie_status_pa,
			dt_fim_consulta
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		update	atendimento_paciente set
			--dt_inicio_atendimento = null,
			dt_atend_medico = null,
			dt_chamada_paciente = null,
			dt_chamada_reavaliacao = null,
			dt_reavaliacao_medica = null,
			dt_fim_consulta = null,
			--dt_chamada_enfermagem = null,
			ie_status_pa = 'LE' -- Libera��o da Enfermagem
		where	nr_atendimento = :new.nr_atendimento;
	end if;

end if;

/* Rafael em 07/12/06 OS40695 */
select	nvl(max(obter_valor_param_usuario(935, 15, obter_perfil_ativo, :new.nm_usuario, 0)),'N')
into	ie_executar_agenda_w
from	dual;

if	(ie_executar_agenda_w = 'S') and
	(:new.ie_desfecho = 'A') then
	select	nvl(max(b.nr_sequencia),0)
	into	nr_seq_agenda_w
	from	agenda_consulta b,
		atendimento_paciente a
	where	b.nr_atendimento = a.nr_atendimento
	and	a.nr_atendimento = :new.nr_atendimento;

	if	(nr_seq_agenda_w > 0) then
		update	agenda_consulta
		set	ie_status_agenda	= 'E'
		where	nr_sequencia		= nr_seq_agenda_w;
	end if;
end if;
begin
if	(:new.ie_desfecho = 'A') then
	open c01;
	loop
	fetch c01 into
		nr_seq_evento_desfecho_alta_w;
	exit when c01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_desfecho_alta_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario,null,:new.dt_liberacao,null);
		end;
	end loop;
	close c01;
end if;

exception
	when others then
	null;
end;


<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.hsj_atendimento_alta_bfinsert
before insert ON TASY.ATENDIMENTO_ALTA for each row
declare
cd_pessoa_fisica_w  varchar2(10);
qt_presc_w          number(10);
qt_presc_tc_w       number(10);
qt_evol_w           number(10);

begin


    if (:new.nr_atendimento is not null and :new.ie_desfecho = 'A' and :new.dt_desfecho is not null) then 
    
        if(wheb_usuario_pck.get_cd_perfil = 1669)then -- M�dico PS
            select count(*)
            into qt_presc_w
            from PRESCR_MEDICA a
            where a.nr_atendimento = :new.nr_atendimento
            and to_date(a.dt_liberacao) = to_date(:new.dt_desfecho)
            and a.dt_suspensao is null;

            if(qt_presc_w > 0)then
                
                select count(*)
                into qt_evol_w
                from evolucao_paciente b
                where b.nr_atendimento = :new.nr_atendimento
                and to_date(b.dt_liberacao) = to_date(:new.dt_desfecho)
                and obter_pf_usuario(wheb_usuario_pck.get_nm_usuario ,'C') = b.cd_medico
                and b.ie_tipo_evolucao = 1
                and b.dt_inativacao is null
                and b.ie_situacao = 'A';
         
                if(qt_evol_w <= 0)then
                   hsj_gerar_log('Atendimento '||:new.nr_atendimento||' n�o pode receber desfecho sem uma evolu��o cl�nica do profissional do desfecho, '||obter_pf_usuario(wheb_usuario_pck.get_nm_usuario ,'D'));
                end if;
                
            end if;
/*            
            select count(*)
            into qt_presc_w
            from prescr_medica a
            where nr_atendimento = :new.nr_atendimento
            and exists
            (   
                select 1
                from prescr_procedimento w
                where w.nr_prescricao = a.nr_prescricao
                and w.cd_setor_atendimento = 141
                and w.dt_baixa is not null
                and  nvl(ie_status_execucao,'X')!='BE'
            )
            and not exists
            (
                select 1
                from evolucao_paciente y
                where y.nr_atendimento = a.nr_atendimento
                and y.dt_liberacao is not null
                and y.dt_inativacao is null
                and y.ie_evolucao_clinica = 'ATC'
            );

            if(qt_presc_w > 0)then
                   hsj_gerar_log('Atendimento '||:new.nr_atendimento||' n�o pode receber desfecho sem uma evolu��o cl�nica do tipo ''Avalia��o de Tomografia''');
            end if;
*/        
        end if;
        
        HSJ_PEP_GERAR_INFORMACAO_REQ(:new.nr_atendimento, wheb_usuario_pck.get_nm_usuario, 'X');
        
    end if;   

end;
/


CREATE OR REPLACE TRIGGER TASY.atendimento_alta_pend_atual
after insert or update ON TASY.ATENDIMENTO_ALTA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_orientacao_alta_w	varchar2(10);
nm_usuario_w		varchar2(15) := :new.nm_usuario;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (:new.dt_liberacao is null) then

     if (nvl(:new.IE_AVALIADOR_AUX,'N') = 'S') and
	   (:new.DT_LIBERACAO_AUX is not null)then

		select 	obter_usuario_pf(:new.CD_PROFISSIONAL)
		into	nm_usuario_w
		from	dual;

		Update    pep_item_pendente
		set		nm_usuario = nm_usuario_w
		where	NR_SEQ_ORIENT_ALTA  = :new.nr_sequencia;

	end if;

end if;


if	((wheb_usuario_pck.get_cd_funcao = 281) or (wheb_usuario_pck.get_cd_funcao = 872) or (wheb_usuario_pck.get_cd_funcao = 10030)) and
	(:new.ie_tipo_orientacao <> 'P')then

	select	max(ie_lib_orientacao_alta)
	into	ie_lib_orientacao_alta_w
	from	parametro_medico
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if	(nvl(ie_lib_orientacao_alta_w,'N') = 'S') then
		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'OA';
		elsif	(:old.dt_liberacao is null) and
				(:new.dt_liberacao is not null) then
			ie_tipo_w := 'XOA';
		end if;
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario);
		end if;
	end if;

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ATENDIMENTO_ALTA ADD (
  CONSTRAINT ATEALTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_ALTA ADD (
  CONSTRAINT ATEALTA_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT ATEALTA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEALTA_MOTALTA_FK 
 FOREIGN KEY (CD_MOTIVO_ALTA) 
 REFERENCES TASY.MOTIVO_ALTA (CD_MOTIVO_ALTA),
  CONSTRAINT ATEALTA_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT ATEALTA_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_DEST) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEALTA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEALTA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEALTA_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEALTA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEALTA_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_AREATUM_FK 
 FOREIGN KEY (NR_SEQ_AREA_ATUACAO) 
 REFERENCES TASY.AREA_ATUACAO_MEDICA (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEALTA_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEALTA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_ESPMEDI_FK2 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDICO) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT ATEALTA_MOTTRPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_TRANSF_PAC (NR_SEQUENCIA),
  CONSTRAINT ATEALTA_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.ATENDIMENTO_ALTA TO NIVEL_1;

