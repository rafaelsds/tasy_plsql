ALTER TABLE TASY.CPOE_PRESENTATION_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_PRESENTATION_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_PRESENTATION_RULE
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_PERFIL                   NUMBER(5),
  IE_ORAL_DIET                VARCHAR2(1 BYTE),
  NR_SEQ_ORAL                 NUMBER(10),
  IE_ENTERAL_DIET             VARCHAR2(1 BYTE),
  NR_SEQ_ENTERAL              NUMBER(10),
  IE_SUPPLEMENT               VARCHAR2(1 BYTE),
  NR_SEQ_SUPPLEMENT           NUMBER(10),
  IE_FASTING                  VARCHAR2(1 BYTE),
  NR_SEQ_FASTING              NUMBER(10),
  IE_MILK                     VARCHAR2(1 BYTE),
  NR_SEQ_MILK                 NUMBER(10),
  IE_MEDICINE                 VARCHAR2(1 BYTE),
  NR_SEQ_MEDICINE             NUMBER(10),
  IE_EXAM                     VARCHAR2(1 BYTE),
  NR_SEQ_EXAM                 NUMBER(10),
  IE_GAS                      VARCHAR2(1 BYTE),
  NR_SEQ_GAS                  NUMBER(10),
  IE_RECOMENDATION            VARCHAR2(1 BYTE),
  NR_SEQ_RECOMENDATION        NUMBER(10),
  IE_HEMOTHERAPY              VARCHAR2(1 BYTE),
  NR_SEQ_HEMOTHERAPY          NUMBER(10),
  IE_DIALYSIS                 VARCHAR2(1 BYTE),
  NR_SEQ_DIALYSIS             NUMBER(10),
  IE_INTERVENTION             VARCHAR2(1 BYTE),
  NR_SEQ_INTERVENTION         NUMBER(10),
  IE_MATERIAL                 VARCHAR2(1 BYTE),
  NR_SEQ_MATERIAL             NUMBER(10),
  IE_PARENTERAL               VARCHAR2(1 BYTE),
  NR_SEQ_PARENTERAL           NUMBER(10),
  IE_INFANT_PARENTERAL        VARCHAR2(1 BYTE),
  NR_SEQ_INFANT_PARENTERAL    NUMBER(10),
  QT_HORAS_ANTERIORES         NUMBER(2),
  IE_ANATOMIA_PAT             VARCHAR2(1 BYTE),
  IE_FUNCAO_MEDICO_LIB        VARCHAR2(1 BYTE),
  IE_FUNCAO_ENF_LIB           VARCHAR2(1 BYTE),
  IE_FUNCAO_FARM_LIB          VARCHAR2(1 BYTE),
  IE_FUNCAO_NUTRIC_LIB        VARCHAR2(1 BYTE),
  IE_FUNCAO_FISIO_LIB         VARCHAR2(1 BYTE),
  IE_VISUALIZAR_CIRURGIA      VARCHAR2(1 BYTE)  DEFAULT null,
  CD_ESTABELECIMENTO          NUMBER(4),
  IE_EXIBE_DILUICAO           VARCHAR2(1 BYTE)  DEFAULT null,
  IE_EXIBE_HORARIOS           VARCHAR2(1 BYTE),
  IE_EXIBE_LATERAL            VARCHAR2(1 BYTE),
  IE_ALERT_SIMULTANEOUS_USER  VARCHAR2(15 BYTE),
  QT_TIME_VERIFY_SIMUL_PRESC  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOPRER_ESTABEL_FK_I ON TASY.CPOE_PRESENTATION_RULE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOPRER_PERFIL_FK_I ON TASY.CPOE_PRESENTATION_RULE
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOPRER_PK ON TASY.CPOE_PRESENTATION_RULE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cpoe_rule_before_ins_upd
before insert or update ON TASY.CPOE_PRESENTATION_RULE for each row
declare

nr_seq_mensagem_w number(15) := 1089760;

begin

if (:new.ie_oral_diet = 'N'
    and :new.ie_enteral_diet = 'N'
    and :new.ie_supplement = 'N'
    and :new.ie_fasting = 'N'
    and :new.ie_milk = 'N'
    and :new.ie_parenteral = 'N'
    and :new.ie_infant_parenteral = 'N'
    and :new.ie_medicine = 'N'
    and :new.ie_exam = 'N'
    and :new.ie_gas = 'N'
    and :new.ie_recomendation = 'N'
    and :new.ie_hemotherapy = 'N'
    and :new.ie_dialysis = 'N'
    and :new.ie_intervention = 'N'
    and :new.ie_material = 'N'
    and :new.ie_anatomia_pat = 'N') then

    wheb_mensagem_pck.exibir_mensagem_abort(nr_seq_mensagem_w);
end if;

end;
/


ALTER TABLE TASY.CPOE_PRESENTATION_RULE ADD (
  CONSTRAINT CPOPRER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_PRESENTATION_RULE ADD (
  CONSTRAINT CPOPRER_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CPOPRER_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CPOE_PRESENTATION_RULE TO NIVEL_1;

