ALTER TABLE TASY.PLS_PP_LOTE_PREST_EVENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PP_LOTE_PREST_EVENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PP_LOTE_PREST_EVENT
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  NR_SEQ_LOTE       NUMBER(10)                  NOT NULL,
  NR_SEQ_PRESTADOR  NUMBER(10)                  NOT NULL,
  NR_SEQ_EVENTO     NUMBER(10)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PPLPEVE_I1 ON TASY.PLS_PP_LOTE_PREST_EVENT
(NR_SEQ_LOTE, NR_SEQ_PRESTADOR, NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PPLPEVE_PK ON TASY.PLS_PP_LOTE_PREST_EVENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PPLPEVE_PLSEVEN_FK_I ON TASY.PLS_PP_LOTE_PREST_EVENT
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PPLPEVE_PLSPRES_FK_I ON TASY.PLS_PP_LOTE_PREST_EVENT
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PPLPEVE_PPPLOTE_FK_I ON TASY.PLS_PP_LOTE_PREST_EVENT
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PP_LOTE_PREST_EVENT ADD (
  CONSTRAINT PPLPEVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PP_LOTE_PREST_EVENT ADD (
  CONSTRAINT PPLPEVE_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PPLPEVE_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PPLPEVE_PPPLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_PP_LOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PP_LOTE_PREST_EVENT TO NIVEL_1;

