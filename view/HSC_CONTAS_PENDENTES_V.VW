create or replace view hsc_contas_pendentes_v as
select	/*+ CHOOSE */
	distinct 
	a.nr_atendimento nr_atendimento,
	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,40) nm_paciente,
	p.dt_entrada dt_entrada,
	substr(obter_matricula_usuario(a.cd_convenio_parametro, a.nr_atendimento),1,254) cd_matricula,
	b.cd_autorizacao cd_autorizacao,
	a.nr_seq_protocolo nr_seq_protocolo,
	nvl(b.vl_guia,0) vl_conta,
	nvl(r.dt_pagamento_previsto, r.dt_vencimento) dt_pagamento_previsto,
	obter_valor_pendente_conta(a.nr_interno_conta, b.cd_autorizacao) vl_pendente,
       	r.nr_titulo nr_titulo,
	nvl(b.vl_convenio,0) vl_recebido,
	b.nr_interno_conta nr_interno_conta,
	(select x.cd_procedimento 
	from 	procedimento_paciente x, 
		conta_paciente z
	where 	a.nr_atendimento 	= x.nr_atendimento 
	and 	z.nr_interno_conta 	= x.nr_interno_conta
	and 	z.nr_seq_protocolo 	= a.nr_seq_protocolo 
	and 	rownum 			<= 1) ds_procedimento,
	(select x.ds_observacao 
	from 	convenio_retorno_item x 
	where 	x.nr_interno_conta	=a.nr_interno_conta 
	and 	x.ds_observacao 	is not null 
	and 	rownum=1 
	having 	max(dt_atualizacao)	=dt_atualizacao 
	group by x.ds_observacao,
		dt_atualizacao) ds_observacao,
	(select x.nr_seq_retorno  from convenio_retorno_item x  where x.nr_titulo = r.nr_titulo and rownum <= 1) nr_retorno,
	a.cd_estabelecimento cd_estabelecimento,
	a.cd_convenio_parametro cd_convenio,
	p.cd_categoria cd_categoria,
	nvl(r.dt_pagamento_previsto, r.dt_vencimento) dt_referencia,
	p.cd_setor_atendimento cd_setor_atendimento
from	atendimento_paciente_v p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_interno_conta	= a.nr_interno_conta
and	r.dt_liquidacao	is null
and	r.nr_seq_protocolo is null
and	(('O' ='O' and obter_valor_pendente_conta(a.nr_interno_conta, b.cd_autorizacao) <= nvl(b.vl_guia,0)))
and	obter_valor_pendente_conta(a.nr_interno_conta, b.cd_autorizacao) > 0
union
select	/*+ CHOOSE */
	a.nr_atendimento nr_atendimento,
	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,40) nm_paciente,
	p.dt_entrada dt_entrada,
	substr(obter_matricula_usuario(a.cd_convenio_parametro, a.nr_atendimento),1,254) cd_matricula,
	b.cd_autorizacao cd_autorizacao,
	a.nr_seq_protocolo nr_seq_protocolo,
	nvl(b.vl_guia,0) vl_conta,
	nvl(r.dt_pagamento_previsto, r.dt_vencimento) dt_pagamento_previsto,
	obter_valor_pendente_conta(a.nr_interno_conta, b.cd_autorizacao) vl_pendente,
	r.nr_titulo nr_titulo,	
	nvl(b.vl_convenio,0) vl_recebido,
	b.nr_interno_conta nr_interno_conta,
	(select	x.cd_procedimento 
	from 	procedimento_paciente x, 
		conta_paciente z
	where 	a.nr_atendimento	= x.nr_atendimento 
	and 	z.nr_interno_conta 	= x.nr_interno_conta
	and 	z.nr_seq_protocolo 	= a.nr_seq_protocolo 
	and 	rownum 			<= 1) ds_procedimento,
	(select x.ds_observacao 
	from 	convenio_retorno_item x 
	where 	x.nr_interno_conta=a.nr_interno_conta 
	and 	x.ds_observacao is not null 
	and 	rownum=1 
	having max(dt_atualizacao)=dt_atualizacao 
	group by x.ds_observacao,
	dt_atualizacao) ds_observacao,
	(select x.nr_seq_retorno  from convenio_retorno_item x  where x.nr_titulo = r.nr_titulo and rownum <= 1) nr_retorno,
	a.cd_estabelecimento cd_estabelecimento,
	a.cd_convenio_parametro cd_convenio,
	p.cd_categoria cd_categoria,
	nvl(r.dt_pagamento_previsto, r.dt_vencimento) dt_referencia,
	p.cd_setor_atendimento cd_setor_atendimento
from	atendimento_paciente_v p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_seq_protocolo 	= a.nr_seq_protocolo
and	r.nr_interno_conta is null
and	r.dt_liquidacao	is null
and	(('O' ='O' and obter_valor_pendente_conta(a.nr_interno_conta, b.cd_autorizacao) <= nvl(b.vl_guia,0)))
and	obter_valor_pendente_conta(a.nr_interno_conta, b.cd_autorizacao) > 0;
/