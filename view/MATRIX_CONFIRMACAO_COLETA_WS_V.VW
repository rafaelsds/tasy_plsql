create or replace 
view matrix_confirmacao_coleta_ws_v as
SELECT	b.nr_prescricao numero_atendimento,
	b.nr_sequencia sequencia_exame,
	substr(d.cd_exame_equip,1,20) codigo_exame,
	to_char(c.dt_atualizacao,'dd/mm/yyyy hh24:mi:ss') data_confirmacao_coleta,
	c.nm_usuario usuario,
	a.cd_pessoa_fisica,
	'RecebeConfirmacaoColeta' ie_metodo
FROM  	prescr_medica a,
	prescr_procedimento b,
	prescr_proc_etapa c,
	lab_exame_equip d,
	equipamento_lab e
WHERE 	a.nr_prescricao = b.nr_prescricao
AND	b.nr_prescricao = c.nr_prescricao
AND	b.nr_sequencia = c.nr_seq_prescricao
AND	b.nr_seq_exame = d.nr_seq_exame
AND	d.cd_equipamento = e.cd_equipamento
AND	e.ds_sigla = 'MATRIX'
AND	c.ie_etapa = 20
/