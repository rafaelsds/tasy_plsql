create or replace
view resumo_medico_v as
select 	a.cd_medico_executor,
	b.nr_seq_protocolo,
       	substr(obter_nome_pf(a.cd_medico_executor),1,100) nm_medico,
       	sum(vl_procedimento) vl_medico
from   procedimento_paciente a,
       conta_paciente b
where  a.nr_interno_conta = b.nr_interno_conta
and    a.cd_medico_executor is not null
having sum(vl_procedimento) <> 0
group by b.nr_seq_protocolo,
	 a.cd_medico_executor,
         substr(obter_nome_pf(a.cd_medico_executor),1,100);
/

