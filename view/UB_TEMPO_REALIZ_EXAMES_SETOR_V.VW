create or replace 
view ub_tempo_realiz_exames_setor_v as
select 	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,25,'D') ,'dd/mm/yyyy hh24:mi:ss') - 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 25, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*)) * 24) - 
		(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,25,'D'), 'dd/mm/yyyy hh24:mi:ss') - 					to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 25, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))) * 24),0) vl_tempo_distr_hr,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,25,'D') ,'dd/mm/yyyy hh24:mi:ss') - 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 25, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))), 0) vl_tempo_distr_dia,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,30,'D') ,'dd/mm/yyyy hh24:mi:ss')- 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 30, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*)) * 24) - 
		(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,30,'D') ,'dd/mm/yyyy hh24:mi:ss')- 					to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 30, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))) * 24), 0) vl_tempo_digit_distr_hr,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,30,'D') ,'dd/mm/yyyy hh24:mi:ss')- 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 30, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))), 0) vl_tempo_digit_distr_dia,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss')- 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 35, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*)) * 24) - 
		(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss')- 					to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 35, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))) * 24), 0) vl_tempo_aprov_digit_hr,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss')- 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 35, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))), 0) vl_tempo_aprov_digit_dia,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss')- 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 25, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*)) * 24) - 
		(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss')- 					to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 25, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))) * 24), 0) vl_tempo_aprov_lib_hr,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss')- 						to_date(lab_obter_etapa_ant(a.nr_prescricao, a.nr_sequencia, 25, 1), 'dd/mm/yyyy hh24:mi:ss')))), count(*))), 0) vl_tempo_aprov_lib_dia,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao))),count(*)) * 24)-
		(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D'), 'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao))), count(*))) * 24),0) vl_tempo_lib_aprov_hr,
	nvl(trunc(dividir_sem_round((sum((to_date(obter_lab_execucao_etapa(a.nr_prescricao,a.nr_sequencia,35,'D') ,'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao))), count(*))), 0) vl_tempo_lib_aprov_dia,
	b.cd_setor_atendimento,
	a.nr_seq_exame
from	exame_laboratorio e,
	prescr_medica b,
	prescr_procedimento a
where	a.nr_prescricao 		= b.nr_prescricao
and	e.nr_seq_exame		= a.nr_seq_exame
group by b.cd_setor_atendimento,
	a.nr_seq_exame
/
