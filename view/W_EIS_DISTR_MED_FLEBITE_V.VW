create or replace view w_eis_distr_med_flebite_v  as 
select distinct
	   d.nr_atendimento,
	   b.cd_material, 
	   substr(obter_valor_dominio(3685,a.ie_extravazamento),1,80) tipo,
	   c.cd_setor_atendimento,
	   substr(obter_nome_setor(c.cd_setor_atendimento),1,255) ds_setor_atendimento,	
	   c.dt_evento,
	   obter_unidade_atendimento(c.nr_atendimento,'A','U') ds_unidade,
	   substr(obter_idade(obter_data_nascto_pf(c.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria	   
from   material a,
	   prescr_material b,
	   qua_evento_paciente c,	      
	   prescr_medica d,
	   qua_evento_flebite e,
	   qua_evento f,
	   qua_tipo_evento g	   
where  b.cd_material = a.cd_material
and	   d.dt_liberacao_medico is not null
and	   b.nr_prescricao = d.nr_prescricao
and	   b.ie_via_aplicacao = 'IV'
and	   a.ie_extravazamento is not null 
and	   d.nr_atendimento = c.nr_atendimento
and	   c.dt_evento between d.dt_inicio_prescr and nvl(d.dt_validade_prescr,sysdate)
and	   f.nr_sequencia = c.nr_seq_evento	
and	   g.ie_tipo_evento  = 'AF'
and	   f.nr_seq_tipo = g.nr_sequencia
and	   e.nr_seq_evento = c.nr_sequencia
and	   e.ie_origem = '1';		
/