CREATE OR REPLACE VIEW Interf_Elosaude_v
AS
SELECT	1						tp_registro,
	a.nr_seq_protocolo				nr_seq_protocolo,
	c.cd_cgc_hospital				cd_cgc_prestador,
	a.cd_usuario_convenio				cd_usuario_convenio,
	substr(a.nm_paciente,1,50)			nm_beneficiario,
	d.cd_autorizacao				nr_documento,
	a.dt_entrada					dt_atendimento,
	substr(obter_interf_tipo_prestador(b.cd_medico_executor, b.cd_cgc_prestador, b.cd_setor_atendimento, c.cd_cgc_hospital),1,2) ie_tipo_prestador,
	upper(substr(c.nm_hospital,1,50))		nm_prestador,
	substr(obter_desc_espec_medica(b.cd_especialidade),1,20) ds_especialidade,
	b.cd_item					cd_servico,
	b.ds_item					ds_servico,
	substr(a.nm_medico_resp,1,50)			nm_medico_solic,
	b.qt_item					qt_item,
	b.vl_unitario_ch				vl_chs,
	b.vl_total_item					vl_real,
	b.vl_filme					vl_filme,
	b.vl_custo_oper					vl_uco,
	b.dt_item					hr_execucao,
	a.dt_entrada					dt_internacao,
	a.dt_alta					dt_alta,
	decode(a.ie_tipo_atendimento, 1, 'I', 7, 'E')	ie_tipo_ato,
	a.cd_cid_principal				cd_cid,
	b.ie_via_acesso					ie_via_acesso,
	a.dt_entrada					hr_entrada,
	a.dt_alta					hr_alta,
	decode(b.cd_funcao_executor, 1, 'C', 0, 'M', 9, 'P', 10, 'S', 11, 'T', 3, 'A', 5, 'I') ie_participacao,
	' '						cd_dente,
	' '						cd_face_dente
from	conta_paciente d,
	w_interf_conta_header c,
	w_interf_conta_item b,
	w_interf_conta_cab a
where	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_seq_protocolo	= b.nr_seq_protocolo
and	a.nr_seq_protocolo	= c.nr_seq_protocolo
and	a.nr_interno_conta	= d.nr_interno_conta
order by	d.cd_autorizacao,
		b.cd_item,
		b.ds_item;
/