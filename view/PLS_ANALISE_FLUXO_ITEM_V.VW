create or replace
view pls_analise_fluxo_item_v as
select	a.nr_sequencia,
	SUBSTR(obter_nome_usuario(a.nm_usuario_nrec),1,255) nm_auditor,
	SUBSTR(to_char(a.dt_atualizacao_nrec, 'dd/mm/yyyy hh24:mi:ss'),1,255) dt_atualizacao,
	a.dt_atualizacao_nrec,
	a.nr_seq_analise,
	a.nr_seq_grupo,
	SUBSTR(pls_obter_nome_grupo_auditor(a.nr_seq_grupo),1,255) ds_grupo,
	a.nr_seq_conta,
	a.nr_seq_conta_proc,
	a.nr_seq_conta_mat,
	a.nr_seq_proc_partic,
	a.nr_seq_ordem,
	a.vl_glosa,
	a.qt_liberada,
	a.ie_acao_item,
	decode(a.ie_acao_item,'G','Glosado','L','Liberado','M','Mantido') ds_acao,
	a.ie_finalizacao,
	a.ie_glosa_conta,
	decode(a.ie_tipo_glosa,'A','Ambos','P','Pagamento','F','Faturamento','Pagamento') ds_tipo_glosa,
	decode(a.nr_seq_acao_analise,null, substr(Obter_Valor_Dominio(5527,a.ie_origem_acao),1,255),(	select	x.ds_acao_analise
													from	pls_acao_analise	x
													where	x.nr_sequencia = a.nr_seq_acao_analise)) ds_origem_acao,
	a.ds_parecer,
	a.nr_seq_motivo_glosa
from	pls_analise_fluxo_item a;
/
