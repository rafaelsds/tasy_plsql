create or replace
view adep_hist_v
as
select	1 nr_seq_apresent,
	'D' ie_tipo_item,
	'Dieta oral' ds_tipo_item,
	'Dietas orais' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_prescricao,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	null nr_seq_item,
	c.cd_refeicao cd_item,
	substr(obter_valor_dominio(99,c.cd_refeicao),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	prescr_dieta_hor c,
	prescr_medica a
where	c.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	2 nr_seq_apresent,
	'S' ie_tipo_item,
	'Suplemento oral' ds_tipo_item,
	'Suplementos orais' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_prescricao,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 12
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	3 nr_seq_apresent,
	'M' ie_tipo_item,
	'Medicamento' ds_tipo_item,
	'Medicamentos' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_prescricao,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 1
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	4 nr_seq_apresent,
	'MAT' ie_tipo_item,
	'Material' ds_tipo_item,
	'Materiais' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_prescricao,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 2
and	nvl(a.ie_adep,'S') = 'S'
and	obter_se_material_adep(a.cd_estabelecimento,b.cd_material) = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	5 nr_seq_apresent,
	'P' ie_tipo_item,
	'Procedimento' ds_tipo_item,
	'Procedimentos' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_prescricao,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_procedimento) cd_item,
	substr(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	prescr_proc_hor c,
	prescr_procedimento b,
	prescr_medica a
where	c.nr_seq_procedimento = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.nr_seq_solic_sangue is null
and	b.nr_seq_derivado is null
and	b.nr_seq_prot_glic is null
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	6 nr_seq_apresent,
	'R' ie_tipo_item,
	'Recomendação' ds_tipo_item,
	'Recomendações' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_prescricao,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	b.nr_sequencia nr_seq_item,
	nvl(to_char(b.cd_recomendacao),b.ds_recomendacao) cd_item,
	substr(obter_rec_prescricao(b.nr_sequencia, b.nr_prescricao),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	prescr_rec_hor c,
	prescr_recomendacao b,
	prescr_medica a
where	c.nr_seq_recomendacao = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	7 nr_seq_apresent,
	'E' ie_tipo_item,
	'SAE' ds_tipo_item,
	'SAE' ds_tipo_item_plural,
	a.nr_atendimento,
	a.nr_sequencia,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	b.nr_sequencia nr_seq_item,
	to_char(b.nr_seq_proc) cd_item,
	substr(obter_desc_intervencoes(b.nr_seq_proc),1,240) ds_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario
from	pe_prescr_proc_hor c,
	pe_prescr_proc b,
	pe_prescricao a
where	c.nr_seq_pe_proc = b.nr_sequencia
and	c.nr_seq_pe_prescr = b.nr_seq_prescr
and	b.nr_seq_prescr = a.nr_sequencia
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
--and	nvl(b.ie_adep,'N') = 'S'
and	nvl(b.ie_adep,'N') in ('S','M');
/