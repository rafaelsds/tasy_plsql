Create or replace view  pls_requisic_glosa_web_v
as
select	'G' ie_tipo,
	a.nr_sequencia,
	b.cd_motivo_tiss nr_codigo,
	b.ds_motivo_tiss ds_descricao_glosa_ocor,
	substr(obter_valor_dominio(1771, ie_grupo_glosa),1,255) ds_grupo,
	substr(ds_observacao,1,255) ds_observacao,
	substr(pls_obter_dados_msg_glosa(nr_seq_motivo_glosa,'ME'),1,255) ds_msg_externa,
	'' ds_documentacao,
	0 nr_regra_ocor,
	a.nr_seq_requisicao 	nr_seq_requisicao,
	a.nr_seq_req_proc	nr_seq_proc,
	a.nr_seq_req_mat	nr_seq_mat,
	a.nr_seq_ocorrencia 	nr_ocorrencia,
	decode(a.nr_seq_ocorrencia,null,'G','O') ie_vinculo,
	a.nr_seq_ocorrencia	nr_seq_vinculo
from	pls_requisicao_glosa	a,
	tiss_motivo_glosa	b
where	a.nr_seq_motivo_glosa	= b.nr_sequencia
and	a.nr_seq_execucao is null
union
select	'O' ie_tipo,
	a.nr_sequencia,
	b.cd_ocorrencia nr_codigo,
	b.ds_ocorrencia ds_descricao_glosa_ocor,
	'' ds_grupo,
	substr(a.ds_observacao,1,255) ds_observacao,
	b.ds_mensagem_externa ds_msg_externa,
	replace(replace(b.ds_documentacao,Chr(13),''),Chr(10),' ') ds_documentacao,
	a.nr_seq_regra nr_regra_ocor,
	a.nr_seq_requisicao nr_seq_requisicao,
	a.nr_seq_proc nr_seq_proc,
	a.nr_seq_mat nr_seq_mat,
	a.nr_seq_ocorrencia nr_ocorrencia,
	decode(a.nr_seq_glosa_req,null,'O','G') ie_vinculo,
	a.nr_seq_glosa_req	nr_seq_vinculo
from	pls_ocorrencia_benef	a,
	pls_ocorrencia		b
where	a.nr_seq_ocorrencia	= b.nr_sequencia
and	a.nr_seq_conta is null
and	a.nr_seq_guia_plano is null
and	a.nr_seq_execucao is null;
/