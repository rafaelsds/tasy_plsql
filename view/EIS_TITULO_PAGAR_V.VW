CREATE OR REPLACE VIEW EIS_TITULO_PAGAR_V AS
Select	a.*,
	obter_empresa_estab(a.cd_estabelecimento) cd_empresa,
	decode(a.cd_pessoa_fisica,null,'Pessoa jur�dica','Pessoa f�sica') ie_tipo_fornec,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,80) ds_fornec,
	substr(obter_valor_dominio(501,a.ie_situacao),1,100) ds_situacao,
	substr(obter_valor_dominio(502,a.ie_tipo_titulo),1,100) ds_tipo_titulo,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'TP'),1,100) ie_tipo_pessoa,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'TPD'),1,100) ds_tipo_pessoa,
	DECODE(IE_SITUACAO,'A',VL_SALDO_TITULO,'L',VL_TITULO,VL_TITULO) vl_indicador,
	to_number(obter_dias_entre_datas(a.dt_emissao,a.dt_liquidacao)) qt_prazo_pagamento
from	TITULO_PAGAR a;
/