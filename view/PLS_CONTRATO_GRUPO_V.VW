Create or replace 
View pls_contrato_grupo_v as
select	nr_seq_contrato,
	nr_seq_grupo,
	nr_seq_intercambio,
	substr(pls_obter_dados_contrato(nr_seq_contrato,'N'),1,255) nr_contrato,
	substr(pls_obter_contrato_plan_inter(nr_seq_contrato,nr_seq_intercambio,'E'),1,255) nm_estipulante,
	substr(pls_obter_desc_classificacao(nr_seq_classificacao),1,255) ds_classificacao,
	nr_seq_classificacao,
	substr(pls_obter_contrato_plan_inter(nr_seq_contrato,nr_seq_intercambio,'D'),1,255) dt_contrato,
	substr(pls_obter_contrato_plan_inter(nr_seq_contrato,nr_seq_intercambio,'DR'),1,255) dt_recisao,
	cd_estabelecimento
from	pls_contrato_grupo;
/
