create or replace view	 fsfx_sgps_tasy_empr_benef_v
as
select	substr(b.cd_operadora_empresa,1,length(b.cd_operadora_empresa)-3) CODIGO_EMPP,
	substr(b.cd_operadora_empresa,length(b.cd_operadora_empresa)-2,2) CODIGO_SUBS_EMPP,
	substr(b.cd_operadora_empresa,length(b.cd_operadora_empresa),1) DV_EMPP,
	a.cd_matricula_familia MATRICULA_TITULAR,
	to_number(a.ie_titularidade) SEQUENCIA_DEP,
	upper(fsfx_elimina_acentos(d.nm_pessoa_fisica,'N')) NOME,
	d.nr_cpf CPF,
	d.ie_sexo SEXO,
	substr(obter_compl_pf(d.cd_pessoa_fisica,1,'M'),1,255) email,
	d.dt_nascimento DATA_NASCIMENTO,
	substr(obter_compl_pf(d.cd_pessoa_fisica,5,'N'),1,255) NOME_MAE,
	substr(obter_compl_pf(d.cd_pessoa_fisica,4,'N'),1,255) NOME_PAI,
	a.DT_CONTRATACAO DATA_CADASTRAMENTO,
	a.dt_rescisao EXCLUSAO,
	d.ie_estado_civil CODIGO_ECIV,
	a.NR_SEQ_LOCALIZACAO_BENEF CODIGO_EMLO,
	a.NR_SEQ_CAUSA_RESCISAO MODE_CODIGO,
	d.dt_obito DATA_OBITO,
	nvl(d.CD_SISTEMA_ANT, d.CD_PESSOA_FISICA) REF_IDENTIFICADOR,
	decode(a.nr_seq_titular,null,'00',(	select	max(x.CD_SISTEMA_ANTERIOR)
						from	GRAU_PARENTESCO	x
						where	x.nr_sequencia	= a.nr_seq_parentesco)) TIPO_BENEFICIARIO_TPBE,
	PLS_OBTER_CARTEIRA_MASCARA(a.nr_sequencia) NUMERO_CARTEIRINHA
from	pls_contrato		b,
	pls_segurado		a,
	pls_segurado_carteira	c,
	pessoa_fisica		d
where	a.nr_seq_contrato	= b.nr_sequencia
and	c.nr_seq_segurado	= a.nr_sequencia
and	a.cd_pessoa_fisica	= d.cd_pessoa_fisica
and	exists	(	select	1
			from	PLS_REGRA_DIREITO_MED_PREV x
			where	sysdate between x.dt_inicio_vigencia and nvl(x.dt_fim_vigencia,sysdate)
			and	((x.nr_seq_contrato	= b.nr_sequencia) or (x.nr_seq_contrato is null))
			and	((x.nr_seq_plano	= a.nr_seq_plano) or (x.nr_seq_plano is null)));
/