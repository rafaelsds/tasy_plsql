create or replace view prescr_material_pend_mov_v
as
select	a.nr_prescricao,
	a.dt_prescricao,
	a.nr_atendimento,
	a.dt_primeiro_horario,
	m.nm_pessoa_fisica nm_medico,
	p.nm_pessoa_fisica nm_paciente,
	count(*) nr_itens,
	sum(decode(b.ie_urgencia,'S',1,0)) nr_itens_urgentes,
	max(decode(b.ie_urgencia,'S',1,0)) ie_item_urgente,
	a.dt_liberacao,
	a.dt_liberacao_medico,
	a.dt_liberacao_farmacia,
	a.ie_lib_farm,
	a.dt_emissao_farmacia,
	a.nm_usuario,
	a.nm_usuario_original,
	a.cd_setor_atendimento,
	s.ds_setor_atendimento,
	s.cd_local_estoque,
	b.cd_local_estoque cd_local_material,
	b.dt_emissao_setor_atend dt_emissao_setor_atend,
	a.cd_estabelecimento,
	substr(obter_se_prescr_imp_parcial(a.nr_prescricao),1,1) ie_imp_parcial,
	p.nr_prontuario,
	a.nr_cirurgia,
	substr(obter_se_prescr_cirurgia(a.nr_prescricao),1,2) ie_cirurgia,
	substr(Obter_Valor_Dominio(9,A.IE_ORIGEM_INF),1,100) DS_ORIGEM_INF,
	a.nr_seq_atend_futuro,
	b.ie_agrupador
from	prescr_mat_hor f,
	setor_atendimento s,
	pessoa_fisica m,
	Pessoa_fisica p,
	prescr_medica a,
	prescr_material b
where	b.nr_prescricao 	= f.nr_prescricao
and     b.nr_sequencia 		= f.nr_seq_material
and	a.nr_prescricao		= b.nr_prescricao
and	b.cd_motivo_baixa		= 0
and	nvl(b.ie_medicacao_paciente,'N') = 'N'
and	a.cd_setor_atendimento 	= s.cd_setor_atendimento
and	a.cd_setor_atendimento is not null
and	b.ie_suspenso		= 'N'
and	a.dt_suspensao is null
and	a.cd_medico		= m.cd_pessoa_fisica
and	a.cd_pessoa_fisica		= p.cd_pessoa_fisica
and     coalesce(f.dt_suspensao,f.dt_recusa,b.dt_suspensao, f.dt_fim_horario) is null
group by 
	a.nr_prescricao,
	a.dt_prescricao,
	a.nr_atendimento,
	a.dt_primeiro_horario,
	p.nm_pessoa_fisica,
	m.nm_pessoa_fisica,
	a.dt_liberacao,
	a.dt_liberacao_medico,
	a.dt_liberacao_farmacia,
	a.ie_lib_farm,
	a.dt_emissao_farmacia,
	a.nm_usuario,
	a.nm_usuario_original,
	a.cd_setor_atendimento,
	s.cd_local_estoque,
	s.ds_setor_atendimento,
	b.cd_local_estoque,
	b.dt_emissao_setor_atend,
	a.cd_estabelecimento,
	a.nr_cirurgia,
	p.nr_prontuario,
	A.IE_ORIGEM_INF,
	a.nr_seq_atend_futuro,
	b.ie_agrupador
union all
select	a.nr_prescricao,
	a.dt_prescricao,
	a.nr_atendimento,
	a.dt_primeiro_horario,
	m.nm_pessoa_fisica nm_medico,
	p.nm_pessoa_fisica nm_paciente,
	count(*) nr_itens,
	sum(decode(b.ie_urgencia,'S',1,0)) nr_itens_urgentes,
	max(decode(b.ie_urgencia,'S',1,0)) ie_item_urgente,
	a.dt_liberacao,
	a.dt_liberacao_medico,
	a.dt_liberacao_farmacia,
	a.ie_lib_farm,
	a.dt_emissao_farmacia,
	a.nm_usuario,
	a.nm_usuario_original,
	a.cd_setor_atendimento,
	'' ds_setor_atendimento,
	null cd_local_estoque,
	b.cd_local_estoque cd_local_material,
	b.dt_emissao_setor_atend dt_emissao_setor_atend,
	a.cd_estabelecimento,
	substr(obter_se_prescr_imp_parcial(a.nr_prescricao),1,1) ie_imp_parcial,
	p.nr_prontuario,
	a.nr_cirurgia,
	substr(obter_se_prescr_cirurgia(a.nr_prescricao),1,2) ie_cirurgia,
	substr(Obter_Valor_Dominio(9,A.IE_ORIGEM_INF),1,100) DS_ORIGEM_INF,
	a.nr_seq_atend_futuro,
	b.ie_agrupador
from	prescr_mat_hor f,
	pessoa_fisica m,
	Pessoa_fisica p,
	prescr_medica a,
	prescr_material b
where	b.nr_prescricao 	= f.nr_prescricao
and     b.nr_sequencia 		= f.nr_seq_material
and	a.nr_prescricao		= b.nr_prescricao
and	b.cd_motivo_baixa		= 0
and	nvl(b.ie_medicacao_paciente,'N') = 'N'
and	a.cd_setor_atendimento 	is null
and	b.ie_suspenso		= 'N'
and	a.dt_suspensao is null
and	a.cd_medico		= m.cd_pessoa_fisica
and	a.cd_pessoa_fisica		= p.cd_pessoa_fisica
and     coalesce(f.dt_suspensao,f.dt_recusa,b.dt_suspensao, f.dt_fim_horario) is null
group by 
	a.nr_prescricao,
	a.dt_prescricao,
	a.nr_atendimento,
	a.dt_primeiro_horario,
	p.nm_pessoa_fisica,
	m.nm_pessoa_fisica,
	a.dt_liberacao,
	a.dt_liberacao_medico,
	a.dt_liberacao_farmacia,
	a.ie_lib_farm,
	a.dt_emissao_farmacia,
	a.nm_usuario,
	a.nm_usuario_original,
	a.cd_setor_atendimento,
	b.cd_local_estoque,
	a.nr_cirurgia,
	b.dt_emissao_setor_atend,
	a.cd_estabelecimento,
	p.nr_prontuario,
	a.IE_ORIGEM_INF,
	a.nr_seq_atend_futuro,
	b.ie_agrupador;
/