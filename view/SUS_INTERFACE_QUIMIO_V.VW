CREATE OR REPLACE
VIEW Sus_Interface_Quimio_V
as
select	'CABECALHO' ds_registro,
	1 tp_registro,
	'1' cd_tipo_registro,
	a.nr_sequencia nr_lote,
	0 nr_seq_laudo,
	b.cd_estabelecimento cd_estabelecimento,
	c.cd_cnes cd_cnes,
	substr(obter_nome_pf_pj('',b.cd_cgc),1,60) ds_razao_social,
	Count_Laudos_Lote(a.nr_sequencia) qt_laudos,
	SYSDATE dt_geracao,
	null qt_laudo,
	null nr_cpf,
	null nm_paciente,
	null cd_nacionalidade,
	null cd_municipio_ibge,
	null ds_endereco,
	null nr_endereco,
	null ds_bairro,
	null cd_cep,
	null ds_compl,
	null nr_telefone_celular,
	null nr_telefone_contato,
	null dt_nascimento,
	null ie_sexo,
	null nm_mae,
	null cd_naturalidade,
	null cd_cor,
	null nm_responsavel,
	null nr_cartao_nac_sus,
	null nr_cpf_medic_solic,
	null dt_emissao,
	null cd_procedimento_solic,
	null cd_cid10,
	null ie_linfonodos,
	null ds_estadio,
	null ds_abrev_tnm,
	null ds_estadio_outro_sist,
	null ds_grau_histo,
	null ds_cid_morfologico,
	null ds_diag_cito_hist,
	null dt_diag_cito_hist,
	null ds_local_metastase,
	null ie_tratamento_ant,
	null ds_pri_tratamento,	
	null ds_seg_tratamento,
	null ds_ter_tratamento,
	null dt_pri_tratamento,
	null dt_seg_tratamento,
	null dt_ter_tratamento,
	null ie_continuidade_trat,
	null ie_via_iv,
	null ie_via_sc,
	null ie_via_im,
	null ie_via_vo,
	null ie_via_it,
	null ie_via_ives,
	null ie_via_outros,
	null ie_finalidade,
	null ds_esquema,
	null qt_meses_prev,
	null qt_meses_autorizado,
	null dt_inicio_trat,
	null dt_fim_trat
from	sus_parametros	c,
	estabelecimento	b,
	sus_lote_autor	a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.cd_estabelecimento	= c.cd_estabelecimento
union
select	'PACIENTE' ds_registro,
	2 tp_registro,
	'2' cd_tipo_registro,
	a.nr_sequencia nr_lote,
	b.nr_seq_interno nr_seq_laudo,
	null cd_estabelecimento,
	null cd_cnes,
	'' ds_razao_social,
	null qt_laudos,
	null dt_geracao,
	0 nr_seq_laudo,
	d.nr_cpf nr_cpf,
	d.nm_pessoa_fisica nm_paciente,
	decode(d.cd_nacionalidade,'10','10','20','20','50') cd_nacionalidade,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'CDM'),1,6) cd_municipio_ibge,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'EN'),1,60) ds_endereco,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'NR'),1,4) nr_endereco,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'B'),1,30) ds_bairro,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'CEP'),1,8) cd_cep,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'CO'),1,20) ds_compl,
/*	substr(lpad(decode(d.nr_telefone_celular,'','',somente_numero(d.nr_telefone_celular)),12,' '),1,12) nr_telefone_celular,
	substr(lpad(decode(obter_compl_pf(c.cd_pessoa_fisica, 1, 'T'),'','',somente_numero(obter_compl_pf(c.cd_pessoa_fisica, 1, 'T'))),12,' '),1,12) 
nr_telefone_contato,*/
	' ' nr_telefone_celular,
	' ' nr_telefone_contato,
	d.dt_nascimento,
	d.ie_sexo,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 5, 'N'),1,50) nm_mae,
	substr(decode(d.cd_nacionalidade,10,obter_compl_pf(c.cd_pessoa_fisica, 1, 'CDM')),1,6) cd_naturalidade,
	nvl(nr_seq_cor_pele,00) cd_cor,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 3, 'N'),1,20) nm_responsavel,
	d.nr_cartao_nac_sus nr_cartao_nac_sus,
	null nr_cpf_medic_solic,
	null dt_emissao,
	null cd_procedimento_solic,
	null cd_cid10,
	null ie_linfonodos,
	null ds_estadio,
	null ds_abrev_tnm,
	null ds_estadio_outro_sist,
	null ds_grau_histo,
	null ds_cid_morfologico,
	null ds_diag_cito_hist,
	null dt_diag_cito_hist,
	null ds_local_metastase,
	null ie_tratamento_ant,
	null ds_pri_tratamento,
	null ds_seg_tratamento,
	null ds_ter_tratamento,
	null dt_pri_tratamento,
	null dt_seg_tratamento,
	null dt_ter_tratamento,
	null ie_continuidade_trat,
	null ie_via_iv,
	null ie_via_sc,
	null ie_via_im,
	null ie_via_vo,
	null ie_via_it,
	null ie_via_ives,
	null ie_via_outros,
	null ie_finalidade,
	null ds_esquema,
	null qt_meses_prev,
	null qt_meses_autorizado,
	null dt_inicio_trat,
	null dt_fim_trat
from	pessoa_fisica		d,
	atendimento_paciente	c,
	sus_laudo_paciente	b,
	sus_lote_autor		a
where	a.nr_sequencia		= b.nr_seq_lote
and	b.nr_atendimento	= c.nr_atendimento
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica
union
select	'QUIMIOTERAPIA'	ds_registro,
	3 tp_registro,
	'3' cd_tipo_registro,
	a.nr_sequencia nr_lote,
	b.nr_seq_interno nr_seq_laudo,
	null cd_estabelecimento,
	null cd_cnes,
	'' ds_razao_social,
	null qt_laudos,
	null dt_geracao,
	null qt_laudo,
	substr(obter_cpf_pessoa_fisica(c.cd_pessoa_fisica),1,11) nr_cpf,
	null nm_paciente,
	null cd_nacionalidade,
    	null cd_municipio_ibge,
	null ds_endereco,
	null nr_endereco,
	null ds_bairro,
	null cd_cep,
	null ds_compl,
	null nr_telefone_celular,
	null nr_telefone_contato,
	null dt_nascimento,
	null ie_sexo,
	null nm_mae,
	null cd_naturalidade,
	null cd_cor,
	null nm_responsavel,
	null nr_cartao_nac_sus,
	substr(obter_cpf_pessoa_fisica(b.cd_medico_requisitante),1,11) nr_cpf_medic_solic,
	b.dt_emissao dt_emissao,
	lpad(b.cd_procedimento_solic,10,0) cd_procedimento_solic,
	substr(b.cd_cid_principal,1,4) cd_cid10,
	decode(b.ie_lifonodos_reg_inval,'S','S','N','N','A') ie_linfonodos,
	decode(obter_valor_dominio(2502,b.ds_estadio_uicc),'0',1,'I',2,'II',3,'III',4,'IV',5,5) ds_estadio,
	substr(Obter_Dados_Loco_Regional(c.cd_pessoa_fisica, 1),1,5) ds_abrev_tnm,
	substr(b.ds_estadio_outro_sist,1,20) ds_estadio_outro_sist,
	substr(decode(cd_grau_histopat,8,'GX',9,'G2',10,'G3',11,'G1',12,'G4',''),1,20) ds_grau_histo,
	substr(b.cd_diag_cito_hist,1,6) ds_cid_morfologico,
	substr(obter_desc_morfologia(b.cd_diag_cito_hist, b.NR_SEQ_MORF_DESC_ADIC),1,60) ds_diag_cito_hist,
	b.dt_diag_cito_hist,
	substr(nvl(b.ds_localizacao_metastase,' 0'),1,50) ds_local_metastase,
	b.ie_tratamento_ant,
	substr(replace(b.ds_tratamento_ant,chr(13)||chr(10),' '),1,60) ds_pri_tratamento,
	substr(replace(b.ds_tratamento_ant2,chr(13)||chr(10),' '),1,60) ds_seg_tratamento,
	substr(replace(b.ds_tratamento_ant3,chr(13)||chr(10),' '),1,60) ds_ter_tratamento,
	b.dt_pri_tratamento,
	b.dt_seg_tratamento,
	b.dt_ter_tratamento,
	b.ie_continuidade_trat,
	nvl(b.ie_via_iv,'N') ie_via_iv,
	nvl(b.ie_via_sc,'N') ie_via_sc,
	nvl(b.ie_via_im,'N') ie_via_im,
	nvl(b.ie_via_vo,'N') ie_via_vo,
	nvl(b.ie_via_it,'N') ie_via_it,
	nvl(b.ie_via_ives,'N') ie_via_ives,
	nvl(b.ie_via_outros,'N') ie_via_outros,
	decode(b.ie_finalidade,1,'07',2,'08',3,'09',4,'10',8,'04',5,'11') ie_finalidade,
	elimina_caracteres_especiais(replace(b.ds_sigla_esquema,'/','')) ds_esquema,
	b.qt_meses_prev qt_meses_prev,
	substr(decode(b.ie_continuidade_trat,'S',qt_meses_autorizado,0),1,4) qt_meses_autorizado,
	b.dt_inicio_trat_solic dt_inicio_trat,
	add_months(dt_emissao, 2) dt_fim_trat
from	atendimento_paciente	c,
	sus_laudo_paciente	b,
	sus_lote_autor		a
where	a.nr_sequencia		= b.nr_seq_lote
and	b.nr_atendimento	= c.nr_atendimento;
/