create or replace view pls_reembolso_v
as
select	/*+ index(b PLSPRCO_I2) */
	a.nr_sequencia nr_seq_reembolso,
	a.nr_seq_protocolo,
	a.ie_tipo_segurado,
	b.dt_mes_competencia dt_mes_comp_protocolo,
	nvl(a.vl_total,0) vl_liquido,
	nvl(a.vl_total,0)  + nvl(a.vl_coparticipacao,0) vl_liberado
from	pls_protocolo_conta	b,
	pls_conta		a
where	a.nr_seq_protocolo	= b.nr_sequencia
and	b.ie_tipo_protocolo	= 'R';
/