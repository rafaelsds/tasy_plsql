create or replace view eis_ops_consultas_proc_v
as
select	pkg_date_utils.start_of(dt_mes_competencia, 'MONTH', 0) dt_mes_competencia,
	cd_estabelecimento,
	nr_seq_prestador_exec,
	nm_prestador_exec,
	cd_procedimento,
	ds_procedimento,
	vl_procedimento_imp
from	(	select	p.dt_mes_competencia,
			c.cd_estabelecimento,
			c.nr_seq_prestador_exec,
			substr(pls_obter_dados_prestador(nr_seq_prestador_exec,'N'),1,255) nm_prestador_exec,
			cp.cd_procedimento,
			substr(obter_descricao_procedimento(cp.cd_procedimento,cp.ie_origem_proced),1,255) ds_procedimento,
			cp.vl_procedimento_imp
		from	pls_conta_proc cp,
			pls_conta c,
			pls_protocolo_conta p,
			ans_grupo_despesa h
		where	c.nr_sequencia		= cp.nr_seq_conta
		and	cp.nr_seq_grupo_ans 	= h.nr_sequencia
		and	c.nr_seq_protocolo 		= p.nr_sequencia
		and	h.ie_tipo_grupo_ans 	= '20'
		and	c.nr_seq_prestador_exec is not null);
/
