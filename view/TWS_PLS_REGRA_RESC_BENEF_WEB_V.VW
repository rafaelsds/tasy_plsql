create or replace view tws_pls_regra_resc_benef_web_v as 
select 	nr_sequencia,
	dt_atualizacao,
	ie_permite_solicitar_rescisao,
	ie_permite_selecionar_benef,
	ie_acao_rescindir_titular,
	ds_msg_transf_titular,
	ie_informacoes_financeiras,
	ie_tipo_contratacao,
	ie_regulamentacao,
	cd_estabelecimento
from 	pls_regra_resc_benef_web;
/