CREATE OR REPLACE VIEW CONSULTA_AGECIRUR_V  AS
/*
IMPORTANTE!
ESSAS VIEWS DEVEM CONTER A MESMA QUANTIDADE DE CAMPOS E OS MESMOS ALIAS:
CONSULTA_AGECHECK_V
CONSULTA_AGESERV_V
CONSULTA_AGERXT_V
CONSULTA_AGEQUI_V
CONSULTA_AGEEXAM_V
CONSULTA_AGECONS_V
CONSULTA_AGECIRUR_V
*/
select	a.cd_agenda,
	a.ds_agenda,
	a.cd_tipo_agenda,
	substr(v.ds_valor_dominio,1,60) ds_tipo_agenda,
	a.cd_estabelecimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_pessoa_fisica cd_medico_agenda,
	substr(p.nm_pessoa_fisica,1,60) nm_medico_agenda,
	a.ie_situacao,
	b.nr_sequencia,
	b.dt_agenda,
	b.hr_inicio,
	to_char(b.hr_inicio,'hh24:mi') hr_hora,
	b.hr_inicio hr_hora_html5,
	b.nr_minuto_duracao,
	substr(y.ie_status_atendimento,1,1) ie_status_atendimento,
	b.ie_status_agenda,
	substr(i.ds_valor_dominio,1,60) ds_status_agenda,
	to_char(b.nr_seq_classif_agenda) ie_classif_agenda,
	substr(f.ds_classificacao,1,80) ds_classif_agenda,
	b.cd_pessoa_fisica,
	substr(c.nm_pessoa_fisica,1,60) nm_paciente_pf,
	b.nm_paciente,
	decode(b.cd_pessoa_fisica,null,b.nm_paciente,substr(c.nm_pessoa_fisica,1,60)) nm_cliente,
	coalesce(b.cd_medico, (select obter_medico_consulta_agepac (b.nr_sequencia, 'C') from dual)) cd_medico,	
	substr(coalesce(m.nm_pessoa_fisica, (select obter_medico_consulta_agepac (b.nr_sequencia, 'N') from dual)),1,60) nm_medico,
	b.cd_medico_exec,
	substr(z.nm_pessoa_fisica,1,60) nm_medico_exec,
	b.cd_convenio,
	b.cd_categoria,
	b.cd_plano,
	b.cd_procedencia,
	substr(x.ds_convenio,1,40) ds_convenio,
	b.nr_atendimento,
	b.cd_procedimento,
	b.ie_origem_proced,
	b.nr_seq_proc_interno,
	substr(obter_exame_agenda(b.cd_procedimento,b.ie_origem_proced,b.nr_seq_proc_interno),1,240) ds_procedimento,
	b.nr_telefone,
	--substr(obter_fone_pac_agenda(b.cd_pessoa_fisica),1,255) ds_telefone_cad,
	--substr(obter_idade(nvl(b.dt_nascimento_pac, c.dt_nascimento), b.dt_agenda, 'S'),1,30) ds_idade,
	round((nvl(y.dt_alta, sysdate) - y.dt_entrada) * 1440) qt_min_espera_tasy,
	substr(b.ds_observacao,1,255) ds_observacao,
	b.cd_turno,
	b.dt_confirmacao,
	b.cd_setor_atendimento,
	a.cd_setor_agenda,
	'' ds_sala,
	'' ds_tipo_pendencia,
	--b.cd_usuario_convenio,
	substr(a.ds_complemento,1,60) ds_complemento,
	a.ds_curta,
	c.nr_pront_ext,
	substr(Obter_motivo_canc_consulta_ag(a.cd_tipo_agenda,b.cd_motivo_cancelamento),1,255) ds_motivo_cancelamento,
	substr(obter_valor_dominio(35,obter_cod_dia_semana(b.dt_agenda)),1,255) ds_dia_semana,
	substr(obter_prontuario_pf(a.cd_estabelecimento,b.cd_pessoa_fisica),1,15) nr_prontuario_pf,
	b.ie_encaixe,
	substr(obter_nome_pf(y.cd_medico_resp),1,255) nm_responsavel,
	substr(ageint_obter_observacao_final(null, b.nr_sequencia),1,255) ds_observacao_final,
	b.ie_anestesia ie_anestesia,
	a.cd_especialidade cd_especialidade,
	substr(obter_desc_espec_medica(a.cd_especialidade),1,240) ds_especialidade,
	b.nm_usuario_orig nm_usuario_origem,
	b.dt_agendamento dt_agendamento
from	valor_dominio i,
	valor_dominio v,
	pessoa_fisica z,
	pessoa_fisica m,
	pessoa_fisica p,
	pessoa_fisica c,
	atendimento_paciente y,
	convenio x,
	agenda_paciente_classif f,
	agenda_paciente b,
	agenda a
where	a.cd_agenda		= b.cd_agenda
and	b.nr_seq_classif_agenda	= f.nr_sequencia (+)
and	a.cd_tipo_agenda	= 1
and	b.cd_pessoa_fisica	= c.cd_pessoa_fisica (+)
and	a.cd_pessoa_fisica	= p.cd_pessoa_fisica (+)
and	b.cd_medico		= m.cd_pessoa_fisica (+)
and	b.cd_medico_exec	= z.cd_pessoa_fisica (+)
and	b.cd_convenio		= x.cd_convenio (+)
and	b.nr_atendimento	= y.nr_atendimento (+)
and	a.cd_tipo_agenda	= v.vl_dominio
and	v.cd_dominio		= 34
and	b.ie_status_agenda	= i.vl_dominio
and	i.cd_dominio		= 83;
/
