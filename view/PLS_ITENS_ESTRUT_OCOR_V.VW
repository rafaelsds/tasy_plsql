create or replace view pls_itens_estrut_ocor_v as
select a.nr_sequencia nr_seq_estrutura,
       b.ie_origem_proced, 
       b.cd_procedimento, 
       b.nr_seq_material 
from   pls_ocorrencia_estrutura a,
       table(pls_grupos_pck.obter_itens_estrut_ocor( a.nr_sequencia )) b
where a.ie_situacao = 'A';
/