create or replace
view uc_pessoa_fisica_v as
select	a.cd_pessoa_fisica,
	a.nm_pessoa_fisica,
	a.dt_nascimento,
	a.ie_sexo,
	a.ie_estado_civil,
	a.dt_atualizacao,
	a.nr_prontuario,
	a.nr_identidade,
	a.nr_cpf,
	a.nr_telefone_celular,
	b.cd_cep,
	b.ds_endereco,
	b.nr_endereco,
	b.ds_complemento,
	b.ds_bairro,
	b.ds_municipio,
	b.sg_estado,
	substr(obter_nome_pais(b.nr_seq_pais),1,100) nm_pais,
	b.nr_ddd_telefone,
	b.nr_telefone,
	b.ds_email
from	compl_pessoa_fisica b,
	pessoa_fisica a
where	a.cd_pessoa_fisica = b.cd_pessoa_fisica(+)
and	b.ie_tipo_complemento = 1;
/
