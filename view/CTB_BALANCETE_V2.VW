CREATE OR REPLACE VIEW CTB_BALANCETE_V2
AS
select
	'N' ie_normal_encerramento,
	b.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.nr_seq_mes_ref,
	d.dt_referencia,
	substr(ctb_obter_classif_conta(b.cd_conta_contabil, b.cd_classificacao, d.dt_referencia),1,40) cd_classificacao,
	b.ds_conta_contabil,
	substr(ctb_obter_conta_apres(b.cd_conta_contabil),1,255) ds_conta_apres,
	b.cd_grupo,
	c.ds_grupo,
	b.ie_tipo,
	a.vl_debito,
	a.vl_credito,
	a.vl_saldo,
	a.vl_movimento,
	a.vl_saldo - a.vl_movimento vl_saldo_ant,
	ctb_obter_nivel_classif_conta(ctb_obter_classif_conta(b.cd_conta_contabil, b.cd_classificacao, d.dt_referencia)) ie_nivel,
	obter_classif_ctb(ctb_obter_classif_conta(b.cd_conta_contabil, b.cd_classificacao, d.dt_referencia), 'SUPERIOR') ie_classif_superior,
	b.ie_centro_custo,
	c.ie_tipo ie_tipo_conta,
	a.cd_estabelecimento cd_estab,
	a.cd_centro_custo
from	ctb_grupo_conta c,
	ctb_mes_ref d,
	conta_contabil b,
	ctb_saldo a
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	d.nr_sequencia		= a.nr_seq_mes_ref
and	b.cd_grupo		= c.cd_grupo (+)
union all
select
	'E' ie_normal_encerramento,
	b.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.nr_seq_mes_ref,
	d.dt_referencia,
	substr(ctb_obter_classif_conta(b.cd_conta_contabil, b.cd_classificacao, d.dt_referencia),1,40) cd_classificacao,
	b.ds_conta_contabil,
	substr(ctb_obter_conta_apres(b.cd_conta_contabil),1,255) ds_conta_apres,
	b.cd_grupo,
	c.ds_grupo,
	b.ie_tipo,
	a.vl_debito - a.vl_enc_debito,
	a.vl_credito - a.vl_enc_credito,
	a.vl_saldo - a.vl_encerramento,
	a.vl_movimento - a.vl_encerramento,
	a.vl_saldo - a.vl_movimento vl_saldo_ant,
	ctb_obter_nivel_classif_conta(ctb_obter_classif_conta(b.cd_conta_contabil, b.cd_classificacao, d.dt_referencia)) ie_nivel,
	obter_classif_ctb(ctb_obter_classif_conta(b.cd_conta_contabil, b.cd_classificacao, d.dt_referencia), 'SUPERIOR') ie_classif_superior,
	b.ie_centro_custo,
	c.ie_tipo ie_tipo_conta,
	a.cd_estabelecimento cd_estab,
	a.cd_centro_custo
from	ctb_grupo_conta c,
	ctb_mes_ref d,
	conta_contabil b,
	ctb_saldo a
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	d.nr_sequencia		= a.nr_seq_mes_ref
and	b.cd_grupo		= c.cd_grupo (+);
/