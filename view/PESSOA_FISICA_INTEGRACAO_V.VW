create or replace view pessoa_fisica_integracao_v
as
select  a.cd_pessoa_fisica,
        a.nm_pessoa_fisica,
	a.ie_tipo_pessoa,
        	a.nr_cpf,
	a.ie_sexo,
	a.dt_nascimento,
	a.nr_identidade,
	a.nr_prontuario,
	a.cd_nacionalidade,
	a.dt_obito,
	b.nr_crm,
	b.uf_crm,
	a.nr_seq_conselho,
	c.sg_conselho,
	a.dt_atualizacao,
	p.cd_cep,
	p.ds_endereco,
	p.nr_endereco,
	p.ds_complemento,
	p.ds_bairro,
	p.ds_municipio,
	p.sg_estado,
	p.ds_email,
	p.nr_ddi_telefone,
	p.nr_ddd_telefone,
	p.nr_telefone,
	p.nr_ddi_fax,
	p.nr_ddd_fax,
	p.ds_fax,
	p.ds_fone_adic,
	(select	max(nm_usuario) 
	 from 	usuario 
	 where 	cd_pessoa_fisica = a.cd_pessoa_fisica) nm_usuario_pf,
	a.ds_fonetica
from    pessoa_fisica a,
	medico b,
	conselho_profissional c,
	compl_pessoa_fisica p
where 	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica(+)
and	a.nr_seq_conselho 	= c.nr_sequencia(+)
and	a.cd_pessoa_fisica	= p.cd_pessoa_fisica(+)
and	p.ie_tipo_complemento (+) = 1
/