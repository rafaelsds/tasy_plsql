create or replace view	 fsfx_sgps_tasy_empr_plano_v
as
select	substr(b.cd_operadora_empresa,1,length(b.cd_operadora_empresa)-3) CODIGO_EMPP,
	substr(b.cd_operadora_empresa,length(b.cd_operadora_empresa)-2,2) CODIGO_SUBS_EMPP,
	substr(b.cd_operadora_empresa,length(b.cd_operadora_empresa),1) DV_EMPP,
	a.cd_matricula_familia MATRICULA_TITULAR_EMBE,
	to_number(a.ie_titularidade) SEQUENCIA_DEP_EMBE,
	a.nr_seq_plano CODIGO_PLAP,
	decode(a.dt_rescisao,null,1,2) CODIGO_STBE
from	pls_contrato		b,
	pls_segurado		a
where	a.nr_seq_contrato	= b.nr_sequencia
and	exists	(	select	1
			from	PLS_REGRA_DIREITO_MED_PREV x
			where	sysdate between x.dt_inicio_vigencia and nvl(x.dt_fim_vigencia,sysdate)
			and	((x.nr_seq_contrato	= b.nr_sequencia) or (x.nr_seq_contrato is null))
			and	((x.nr_seq_plano	= a.nr_seq_plano) or (x.nr_seq_plano is null)));
/
