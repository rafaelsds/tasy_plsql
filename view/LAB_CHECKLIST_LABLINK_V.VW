create or replace view lab_checklist_lablink_v as
select	a.nr_prescricao,
	a.cd_estabelecimento,
	b.nr_sequencia nr_seq_prescr,
	b.nr_seq_exame,
	c.ie_padrao_amostra,
	b.nr_seq_lab,
	c.ie_padrao_amostra || '/' || nvl(b.nr_seq_lab, 0) padrao_amostra_nr_seq_lab,
	case
		when c.ie_padrao_amostra in ('PM','PM11','PMR11','PM13')
		then 'Sim'
		else decode(nvl(b.nr_seq_lab,0), 0, 'N�o', 'Sim')
	end integra_amostra,
	c.ie_salva_etapa,
	decode(nvl(c.ie_salva_etapa, 'N'), 'N', 'N�o', 'Sim') integra_salva_etapa,
	b.dt_integracao,
	decode(b.dt_integracao, null, 'Sim', 'N�o') integra_dt_integracao,
	b.ie_status_atend,
	b.ie_status_atend || ' - ' || obter_valor_dominio(1030, b.ie_status_atend) ds_status_atend,
	decode(b.ie_status_atend, c.ie_status_envio, 'Sim', 'N�o') integra_envio,
	Obter_Equipamento_Exame(b.nr_seq_exame, null, 'CETUS') equip_exame,
	decode(Obter_Equipamento_Exame(b.nr_seq_exame, null, 'CETUS'), null, 'N�o', 'Sim') integra_equip,
	d.nr_seq_grupo_imp,
	d.nr_seq_grupo_imp || ' - ' || obter_desc_grupo_imp_lab(d.nr_seq_grupo_imp)  ds_grupo_imp,
	decode(nvl(d.nr_seq_grupo_imp, 0), 0, 'N�o', 'Sim') integra_grupo_imp,
	b.ds_material_especial,
	decode(b.ds_material_especial, null, 'Sim', 'N�o') integra_mat_especial,
	obter_equip_exame_duplicado(b.nr_seq_exame, 'CETUS') equip_exame_duplicado,
	decode(obter_equip_exame_duplicado(b.nr_seq_exame, 'CETUS'), null, 'Sim', 'N�o') integra_equip_exame_dup,
	b.ie_suspenso,
	decode(nvl(b.ie_suspenso, 'N'), 'N', 'Sim', 'N�o') integra_suspenso,
	nvl(a.dt_liberacao_medico, a.dt_liberacao) dt_liberacao,
	decode(nvl(a.dt_liberacao_medico, a.dt_liberacao), null, 'N�o', 'Sim') integra_dt_liberacao
from 	prescr_medica a,
	prescr_procedimento b,
	lab_parametro c,
	exame_laboratorio d
where a.nr_prescricao = b.nr_prescricao
  and a.cd_estabelecimento = c.cd_estabelecimento
  and b.nr_seq_exame = d.nr_seq_exame
  and ((c.ie_padrao_amostra in ('PM','PM11','PMR11','PM13')) or (b.nr_seq_lab is not null))
  and c.ie_padrao_amostra not in ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13', 'EAM10')
  and b.cd_material_exame is not null
  and b.nr_seq_exame is not null  
union
select	a.nr_prescricao,
	a.cd_estabelecimento,
	b.nr_sequencia nr_seq_prescr,
	b.nr_seq_exame,
	c.ie_padrao_amostra,
	b.nr_seq_lab,
	c.ie_padrao_amostra || '/' || nvl(b.nr_seq_lab, 0) padrao_amostra_nr_seq_lab,
	case
		when c.ie_padrao_amostra in ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13','EAM10')
		then decode(nvl(b.nr_seq_lab,0), 0, 'N�o', 'Sim')	
		else 'N�o'
	end integra_amostra,
	c.ie_salva_etapa,
	decode(nvl(c.ie_salva_etapa, 'N'), 'N', 'N�o', 'Sim') integra_salva_etapa,
	b.dt_integracao,
	decode(b.dt_integracao, null, 'Sim', 'N�o') integra_dt_integracao,
	b.ie_status_atend,
	b.ie_status_atend || ' - ' || obter_valor_dominio(1030, b.ie_status_atend) ds_status_atend,
	decode(b.ie_status_atend, c.ie_status_envio, 'Sim', 'N�o') integra_envio,
	Obter_Equipamento_Exame(b.nr_seq_exame, null, 'CETUS') equip_exame,
	decode(Obter_Equipamento_Exame(b.nr_seq_exame, null, 'CETUS'), null, 'N�o', 'Sim') integra_equip,
	d.nr_seq_grupo_imp,
	d.nr_seq_grupo_imp || ' - ' || obter_desc_grupo_imp_lab(d.nr_seq_grupo_imp)  ds_grupo_imp,
	decode(nvl(d.nr_seq_grupo_imp, 0), 0, 'N�o', 'Sim') integra_grupo_imp,
	b.ds_material_especial,
	decode(b.ds_material_especial, null, 'Sim', 'N�o') integra_mat_especial,
	obter_equip_exame_duplicado(b.nr_seq_exame, 'CETUS') equip_exame_duplicado,
	decode(obter_equip_exame_duplicado(b.nr_seq_exame, 'CETUS'), null, 'Sim', 'N�o') integra_equip_exame_dup,
	b.ie_suspenso,
	decode(nvl(b.ie_suspenso, 'N'), 'N', 'Sim', 'N�o') integra_suspenso,
	nvl(a.dt_liberacao_medico, a.dt_liberacao) dt_liberacao,
	decode(nvl(a.dt_liberacao_medico, a.dt_liberacao), null, 'N�o', 'Sim') integra_dt_liberacao
from 	prescr_medica a,
	prescr_procedimento b,
	lab_parametro c,
	exame_laboratorio d
where a.nr_prescricao = b.nr_prescricao
  and a.cd_estabelecimento = c.cd_estabelecimento
  and b.nr_seq_exame = d.nr_seq_exame
  and c.ie_padrao_amostra in ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13','EAM10')
  and b.cd_material_exame is not null
  and b.nr_seq_exame is not null
order by 3;
/
