create or replace
view Eis_Paciente_Cadastro_v as
select	/*+ index (b PESFISI_PK) */
	b.cd_religiao,
	b.ie_sexo,
	b.ie_estado_civil,
	b.ie_grau_instrucao,
	substr(obter_idade(b.dt_nascimento,sysdate,'E'),1,10) ie_faixa_etaria,
	initcap(f.ds_municipio) ds_municipio,
	initcap(f.ds_bairro) ds_bairro,
	f.cd_municipio_ibge,
	initcap(obter_desc_municipio_ibge(f.cd_municipio_ibge)) ds_municipio_ibge,
	b.dt_atualizacao dt_atualizacao_pf
from	compl_pessoa_fisica f,
	pessoa_fisica b
where 	1			= f.ie_tipo_complemento(+)
and	b.cd_pessoa_fisica	= f.cd_pessoa_fisica(+);
/