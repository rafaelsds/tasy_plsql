create or replace view escala_sist_nutricional_v  as
select	nvl(obter_setor_atend_data_ent(a.nr_atendimento, e.dt_avaliacao),a.cd_setor_atendimento) cd_setor_atendimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
	substr(obter_nome_convenio(obter_convenio_atendimento(a.nr_atendimento)),1,150) ds_convenio,
	obter_nome_setor(nvl(obter_setor_atend_data_ent(a.nr_atendimento, e.dt_avaliacao),a.cd_setor_atendimento)) ds_setor_atendimento,
	obter_sexo_pf(a.cd_pessoa_fisica,'C') ie_sexo,
	obter_sexo_pf(a.cd_pessoa_fisica,'D') ds_sexo,
	obter_dados_esc_nutricional(e.nr_sequencia,a.dt_entrada_unidade,'24') qt_24h,
	obter_dados_esc_nutricional(e.nr_sequencia,a.dt_entrada_unidade,'48') qt_48h,
	obter_dados_esc_nutricional(e.nr_sequencia,a.dt_entrada_unidade,'72') qt_72h,
	substr(obter_idade(obter_data_nascto_pf(a.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria,
	obter_unidade_atendimento(a.nr_atendimento,'A','U') ds_unidade,
	a.ie_clinica ie_clinica,
	substr(obter_Clinica(a.ie_clinica),1,250) ds_clinica,
	e.dt_avaliacao,
	f.cd_empresa,
	a.cd_estabelecimento,
	a.nr_atendimento nr_atendimento,
	substr(obter_nome_paciente(a.nr_atendimento),1,255) nm_paciente,	
	substr(obter_result_escala_sist_nut(ie_dietoterapia,ie_fator_risco),1,255) ds_resultado,
	a.dt_entrada_unidade dt_entrada
from 	escala_sist_nutricional e,
	estabelecimento f,
	atendimento_paciente_v a
where 	a.nr_atendimento= e.nr_atendimento
and	a.cd_estabelecimento = f.cd_estabelecimento
and	e.dt_liberacao is not null
and	e.dt_inativacao is null;
/