create or replace
view Resumo_Item_Conta_V2 as
select	98 IE_EMITE_CONTA,
	a.nr_interno_conta,
	a.cd_convenio,
	a.ie_emite_conta cd_resumo,
	substr(Obter_Desc_Estrut_Conta(somente_numero(a.ie_emite_conta)),1,240) ds_resumo,
	sum(a.qt_material) qt_resumo,
	sum(a.vl_material) vl_resumo,
	sum(a.qt_material_convenio) qt_convenio,
	sum(a.vl_material_convenio) vl_convenio,
	0 vl_medico
from	conta_paciente_material_v a
group by a.nr_interno_conta,
	a.cd_convenio,
	a.ie_emite_conta,
	substr(Obter_Desc_Estrut_Conta(somente_numero(a.ie_emite_conta)),1,240)
union	
select	98,
	a.nr_interno_conta,
	a.cd_convenio,
	a.ie_emite_conta cd_resumo,
	substr(Obter_Desc_Estrut_Conta(somente_numero(a.ie_emite_conta)),1,240) ds_resumo,
	sum(a.qt_procedimento) qt_resumo,
	sum(a.vl_procedimento) vl_resumo,
	sum(a.qt_proced_convenio) qt_convenio,
	sum(a.vl_proced_convenio) vl_convenio,
	sum(a.vl_medico) vl_medico
from	conta_paciente_procedimento_v a
group by a.nr_interno_conta,
	a.cd_convenio,
	a.ie_emite_conta,
	substr(Obter_Desc_Estrut_Conta(somente_numero(a.ie_emite_conta)),1,240);	
/