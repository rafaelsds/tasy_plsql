create or replace view rmv_cultura as
select	distinct '1' codcoligada,
	substr(a.nr_prontuario,1,9) prontuario,
	substr(a.nm_pessoa_fisica,1,50) paciente,
	to_date(to_char(c.dt_coleta,'dd/mm/yyyy')) datacultura,
	to_date(to_char(d.dt_resultado,'dd/mm/yyyy')) dataresultado,
	substr(c.cd_material_exame,2,50) codmaterial,
	substr(c.cd_setor_coleta,1,50) codclinica,
	substr(obter_especialidade_pf(b.cd_medico),1,50) codespecialidade,
	e.ds_resultado gram,
	e.ds_observacao observacoes,
	substr(d.nr_seq_resultado||e.nr_sequencia,1,50) numpedidocultura
from 	pessoa_fisica a,
	prescr_medica b,
	prescr_procedimento c,
	exame_lab_resultado d,
	exame_lab_result_item e,
	exame_laboratorio f
where 	b.nr_prescricao = c.nr_prescricao
and	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	d.nr_prescricao = b.nr_prescricao
and	d.nr_seq_resultado = e.nr_seq_resultado
and	c.nr_sequencia = e.nr_seq_prescr
and	c.nr_seq_exame = f.nr_seq_exame
and	e.dt_aprovacao is not null
and	e.dt_coleta is not null
and	f.ie_formato_resultado in ('SM','SDM')
order by 	5;
/