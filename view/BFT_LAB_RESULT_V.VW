create or replace view bft_lab_result_v as
select	a.nr_seq_resultado result_id,
	a.nr_prescricao prescription_id,
	a.dt_liberacao release_date,
	a.dt_resultado result_date,
	a.cd_pessoa_fisica person_id,
	a.cd_medico doctor_id,
	a.nr_atendimento encounter_id,
	a.nr_seq_protocolo protocol_id,
	obter_desc_protocolo_lab(a.nr_seq_protocolo) protocol_desc,
	a.cd_solicitacao_externa external_code
from	exame_lab_resultado a;
/