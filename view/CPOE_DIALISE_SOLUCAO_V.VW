CREATE OR REPLACE VIEW cpoe_dialise_solucao_v
AS
  SELECT nr_sequencia
         , 1                AS nr_solucao
         , nr_seq_protocolo
         , ie_tipo_solucao  AS tipo_solucao
         , qt_solucao_total AS solucao_total
         , qt_temp_solucao  AS temp_solucao
         , qt_dosagem       AS dosagem
         , ie_unid_vel_inf  AS unid_vel_inf
         , ie_bomba_infusao AS bomba_infusao
         , qt_dose_ataque   AS dose_ataque
         , ds_orientacao    AS orientacao
  FROM   (SELECT nr_sequencia
                 , 1 AS nr_solucao
                 , nr_seq_protocolo
                 , ie_tipo_solucao
                 , qt_solucao_total
                 , qt_temp_solucao
                 , qt_dosagem
                 , ie_unid_vel_inf
                 , ie_bomba_infusao
                 , qt_dose_ataque
                 , ds_orientacao
          FROM   cpoe_dialise
          WHERE  nr_seq_protocolo IS NOT NULL
          UNION
          SELECT nr_sequencia
                 , 2                 AS nr_solucao
                 , nr_seq_protocolo2 AS nr_seq_protocolo
                 , ie_tipo_solucao2  AS ie_tipo_solucao
                 , qt_solucao_total2 AS qt_solucao_total
                 , qt_temp_solucao2  AS qt_temp_solucao
                 , qt_dosagem2       AS qt_dosagem
                 , ie_unid_vel_inf2  AS ie_unid_vel_inf
                 , ie_bomba_infusao2 AS ie_bomba_infusao
                 , qt_dose_ataque2   AS qt_dose_ataque
                 , ds_orientacao2    AS ds_orientacao
          FROM   cpoe_dialise
          WHERE  nr_seq_protocolo2 IS NOT NULL
          UNION
          SELECT nr_sequencia
                 , 3                 AS nr_solucao
                 , nr_seq_protocolo3 AS nr_seq_protocolo
                 , ie_tipo_solucao3  AS ie_tipo_solucao
                 , qt_solucao_total3 AS qt_solucao_total
                 , qt_temp_solucao3  AS qt_temp_solucao
                 , qt_dosagem3       AS qt_dosagem
                 , ie_unid_vel_inf3  AS ie_unid_vel_inf
                 , ie_bomba_infusao3 AS ie_bomba_infusao
                 , qt_dose_ataque3   AS qt_dose_ataque
                 , ds_orientacao3    AS ds_orientacao
          FROM   cpoe_dialise
          WHERE  nr_seq_protocolo3 IS NOT NULL
          UNION
          SELECT nr_sequencia
                 , 4                 AS nr_solucao
                 , nr_seq_protocolo4 AS nr_seq_protocolo
                 , ie_tipo_solucao4  AS ie_tipo_solucao
                 , qt_solucao_total4 AS qt_solucao_total
                 , qt_temp_solucao4  AS qt_temp_solucao
                 , qt_dosagem4       AS qt_dosagem
                 , ie_unid_vel_inf4  AS ie_unid_vel_inf
                 , ie_bomba_infusao4 AS ie_bomba_infusao
                 , qt_dose_ataque4   AS qt_dose_ataque
                 , ds_orientacao4    AS ds_orientacao
          FROM   cpoe_dialise
          WHERE  nr_seq_protocolo4 IS NOT NULL);

/ 