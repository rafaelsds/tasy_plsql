create or replace 
view     dashboard_pmo_prog_resumo_v as
select	v.*,
	obter_cor_status_dashboard_pmo(v.ie_status_resumo) cor_status_resumo,
	obter_cor_status_dashboard_pmo(v.ie_status_horas) cor_status_horas,
	obter_cor_status_dashboard_pmo(v.ie_status_milestone) cor_status_milestone,
	obter_cor_status_dashboard_pmo(v.ie_status_checkpoint) cor_status_checkpoint,
	obter_pr_prev_programa(0, nr_sequencia, 0, null) pr_prev,
	obter_pr_prev_programa(0, nr_sequencia, 1, null) pr_real
from   (
        select	t.*,
		obter_fim_prev_pmo(t.nr_sequencia) dt_fim_real,
		obter_status_dashboard_pmo(	'PR',
						t.ie_status,
						t.dt_inicio_prev,
						t.dt_inicio_real,
						t.dt_fim_prev,
						obter_fim_prev_pmo(t.nr_sequencia),
						t.pr_prev2,
						t.pr_real2 ) ie_status_resumo,
		obter_status_hr_dashboard_pmo(t.qt_hora_prev, t.qt_hora_real) ie_status_horas,
		obter_status_ckp_dashboard_pmo('PR', t.nr_sequencia) ie_status_checkpoint,
		obter_status_mlt_dashboard_pmo('PR', t.nr_sequencia) ie_status_milestone
	from	(
		select	a.nr_seq_programa nr_sequencia,
			a.ds_programa,
			a.nr_seq_portifolio,
			a.ie_status_programa ie_status,
			min(a.dt_inicio_prev) dt_inicio_prev,
			min(a.dt_inicio_real) dt_inicio_real,
			max(a.dt_fim_prev) dt_fim_prev,
			sum(a.qt_hora_prev) qt_hora_prev,
			sum(a.qt_hora_real) qt_hora_real,
			obter_pr_prev_crono(min(a.dt_inicio_prev),max(a.dt_fim_prev)) pr_prev2,
			obter_pr_real_programa(a.nr_seq_programa) pr_real2,
			a.ie_sit_programa
		from	proj_projeto_inf_v a
                group by	a.nr_seq_programa,
				a.nr_seq_portifolio,
				a.ds_programa,
				a.ie_status_programa,
				obter_pr_real_programa(a.nr_seq_programa),
				a.ie_sit_programa
		) t
	) v;
/
