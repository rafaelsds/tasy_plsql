create or replace
view	lock_v3 as
SELECT 	substr('ALTER SYSTEM DISCONNECT SESSION ' || chr(39) || s.sid || ',' || s.serial# || chr(39) || ' IMMEDIATE; ',1,70) ds_comando
FROM 	GV$LOCK L, 
	GV$SESSION S,
	user_objects u
WHERE	s.sid = l.sid
AND	u.object_id = l.ID1
union 
SELECT	substr('ALTER SYSTEM DISCONNECT SESSION ' || chr(39) || c.sid || ',' || c.serial# || chr(39) || ' IMMEDIATE; ',1,70) ds_comando
       FROM	user_objects a,
		gv$locked_object b,
		gv$session c
      WHERE	a.object_id = b.object_id 
      AND	b.session_id = c.SID
union
select	distinct
	substr('ALTER SYSTEM DISCONNECT SESSION ' || chr(39) || a.sid || ',' || a.serial# || chr(39) || ' IMMEDIATE; ',1,70) ds_comando
from 	sys.v_$session a,
	sys.v_$lock x
where	x.sid = a.sid
and	(x.request > 0 OR x.block > 0)
 ORDER BY 1;
/