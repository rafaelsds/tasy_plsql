create or replace view plussoft_fx_etaria_proposta_v
as
	select	a.nr_seq_proposta,
		'Faixa et�ria de '|| b.qt_idade_inicial || ' � ' || b.qt_idade_final || ' anos' ds_faixa_etaria,
		b.qt_idade_inicial,
		b.qt_idade_final,
		count(1) qt_vidas,
		max(pls_obter_faixa_etaria_prop(a.nr_sequencia,b.nr_sequencia)) vl_unitario,
		sum(pls_obter_faixa_etaria_prop(a.nr_sequencia,b.nr_sequencia)) vl_total
	from  	pls_proposta_beneficiario	a,
		pls_plano_preco			b,
		pessoa_fisica			c
	where 	a.nr_seq_tabela = b.nr_seq_tabela
	and	a.cd_beneficiario = c.cd_pessoa_fisica
	and	trunc(months_between(sysdate, dt_nascimento) / 12) between b.qt_idade_inicial and b.qt_idade_final
	group by a.nr_seq_proposta, b.qt_idade_inicial, b.qt_idade_final
	order by 2;
/