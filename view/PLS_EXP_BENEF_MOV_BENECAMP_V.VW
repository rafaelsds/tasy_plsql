create or replace view pls_exp_benef_mov_benecamp_v 
as
select	CD_MATRICULA_FAMILIA,
	NR_SEQUENCIAL_FAMILIA,
	NM_BENEFICIARIO,
	DT_NASCIMENTO,
	ie_sexo,
	(	select	max(x.CD_SISTEMA_ANTERIOR)
		from	GRAU_PARENTESCO x
		where	x.nr_sequencia	= NR_SEQ_PARENTESCO) CD_GRAU_PARENTESCO,
	cd_plano,
	DT_CONTRATACAO,
	DS_ENDERECO,
	nr_endereco,
	DS_COMPLEMENTO,
	DS_BAIRRO,
	DS_MUNICIPIO,
	CD_CEP,
	SG_ESTADO,
	NR_TELEFONE,
	NR_IDENTIDADE,
	nr_cpf,
	decode(IE_TIPO_MOVIMENTACAO,'I',1,'A',2,'E',3) IE_TIPO_MOVIMENTACAO,
	nm_mae,
	to_number(replace(replace(campo_mascara(vl_preco_segurado,2),',',''),'.',''))		vl_preco_segurado,
	' ' ds_branco,
	decode(IE_ESTADO_CIVIL,'1','03','2','04','3','07','4','06','5','05','9','07') ds_estado_civil,
	nr_seq_lote
from	pls_movimentacao_benef;
/