create or replace
view 	TISS_GUIAS_SPSADT_V as
select	'2.01.01' ds_versao,
	c.cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	a.ie_tipo_atend_tiss,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	substr(TISS_OBTER_TIPO_SAIDA('SPSADT', a.nr_interno_conta),1,3) ie_tipo_saida,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao) CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	b.cd_medico_resp cd_medico_executor,
	substr(nvl(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro), decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao)),1,255) cd_autorizacao_princ,
	nvl(nvl(e.cd_cgc_prestador_tiss, e.cd_cgc_prestador), h.cd_cgc) cd_cgc_prestador
from 	estabelecimento h,
	regra_honorario g,
	procedimento_paciente e,
	motivo_alta d,
	conta_paciente_guia c,
	atendimento_paciente b,
	conta_paciente a 
where	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= c.nr_interno_conta
and	b.cd_motivo_alta	= d.cd_motivo_alta(+)
and	e.nr_interno_conta	= c.nr_interno_conta
and	e.nr_doc_convenio	= c.cd_autorizacao
and	e.ie_responsavel_credito	= g.cd_regra(+)
and	a.cd_estabelecimento		= h.cd_estabelecimento
and	((TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				e.ie_tiss_tipo_guia) = '4') or
	 (TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				null,
				null,
				e.ie_tiss_tipo_guia) = '4'))
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null
and	(((b.ie_tipo_atendimento	<> 1) and (e.ie_tiss_tipo_guia is null)) or
	 (e.ie_tiss_tipo_guia = '4'))
union
select	'2.01.01' ds_versao,
	'N�o Informada' cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	a.ie_tipo_atend_tiss,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	substr(TISS_OBTER_TIPO_SAIDA('SPSADT', a.nr_interno_conta),1,3) ie_tipo_saida,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	null CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	b.cd_medico_resp cd_medico_executor,
	substr(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro),1,255) cd_autorizacao_princ,
	nvl(nvl(e.cd_cgc_prestador_tiss, e.cd_cgc_prestador), h.cd_cgc) cd_cgc_prestador
from 	estabelecimento h,
	regra_honorario g,
	procedimento_paciente e,
	motivo_alta d,
	atendimento_paciente b,
	conta_paciente a 
where	a.nr_atendimento	= b.nr_atendimento
and	b.cd_motivo_alta	= d.cd_motivo_alta(+)
and	a.nr_interno_conta	= e.nr_interno_conta
and	nvl(e.nr_doc_convenio, 'N�o Informada') = 'N�o Informada'
and	e.ie_responsavel_credito			= g.cd_regra(+)
and	a.cd_estabelecimento		= h.cd_estabelecimento
and	((TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				e.ie_tiss_tipo_guia) = '4') or
	 (TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				null,
				null,
				e.ie_tiss_tipo_guia) = '4'))
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null
and	(((b.ie_tipo_atendimento	<> 1) and (e.ie_tiss_tipo_guia is null)) or
	 (e.ie_tiss_tipo_guia = '4'))
union
select	'2.01.01' ds_versao,
	c.cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	a.ie_tipo_atend_tiss,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	substr(TISS_OBTER_TIPO_SAIDA('SPSADT', a.nr_interno_conta),1,3) ie_tipo_saida,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao) CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	b.cd_medico_resp cd_medico_executor,
	substr(nvl(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro), decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao)),1,255) cd_autorizacao_princ,
	nvl(f.cd_cgc, h.cd_cgc) cd_cgc_prestador
from 	estabelecimento h,
	regra_honorario g,
	procedimento_participante f,
	procedimento_paciente e,
	motivo_alta d,
	conta_paciente_guia c,
	atendimento_paciente b,
	conta_paciente a 
where	a.nr_atendimento		= b.nr_atendimento
and	a.nr_interno_conta		= c.nr_interno_conta
and	b.cd_motivo_alta		= d.cd_motivo_alta(+)
and	e.nr_interno_conta		= c.nr_interno_conta
and	e.nr_doc_convenio		= c.cd_autorizacao
and	f.ie_responsavel_credito	= g.cd_regra(+)
and	e.nr_sequencia			= f.nr_sequencia
and	a.cd_estabelecimento		= h.cd_estabelecimento
and	TISS_OBTER_GUIA_PROC(	b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				f.ie_tiss_tipo_guia) = '4'
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				null,
				null,
				e.ie_tiss_tipo_guia)	= '6'		-- Edgar 08/06/2007 - s� imprimir participante na SADT se o superioor for honor�rio
and	e.cd_motivo_exc_conta	is null
union
select	'2.01.01' ds_versao,
	'N�o Informada' cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	a.ie_tipo_atend_tiss,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	substr(TISS_OBTER_TIPO_SAIDA('SPSADT', a.nr_interno_conta),1,3) ie_tipo_saida,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	null CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	b.cd_medico_resp cd_medico_executor,
	substr(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro),1,255) cd_autorizacao_princ,
	nvl(f.cd_cgc, h.cd_cgc) cd_cgc_prestador
from 	estabelecimento h,
	regra_honorario g,
	procedimento_participante f,
	procedimento_paciente e,
	motivo_alta d,
	atendimento_paciente b,
	conta_paciente a 
where	a.nr_atendimento				= b.nr_atendimento
and	b.cd_motivo_alta				= d.cd_motivo_alta(+)
and	a.nr_interno_conta				= e.nr_interno_conta
and	nvl(e.nr_doc_convenio, 'N�o Informada') 	= 'N�o Informada'
and	f.ie_responsavel_credito			= g.cd_regra(+)
and	e.nr_sequencia			= f.nr_sequencia
and	a.cd_estabelecimento		= h.cd_estabelecimento
and	TISS_OBTER_GUIA_PROC(	b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				f.ie_tiss_tipo_guia) = '4'
and	TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				null,
				null,
				e.ie_tiss_tipo_guia)	= '6'		-- Edgar 08/06/2007 - s� imprimir participante na SADT se o superioor for honor�rio
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null;
/

