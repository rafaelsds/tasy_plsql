create or replace view exame_laboratorial_v as
select pp.nr_seq_interno codigo,
       pp.nr_prescricao prescricao,
       pp.cd_procedimento codigo_procedimento,
       coalesce(obter_desc_proc_interno(pp.nr_seq_proc_interno), obter_desc_exame(pp.nr_prescricao)) ds_exame,
       obter_nome_setor(coalesce(ppac.cd_setor_atendimento, coalesce(pp.cd_setor_atendimento, pm.cd_setor_atendimento))) centro_custo,
       obter_nome_setor(coalesce(ppac.cd_setor_atendimento, coalesce(pp.cd_setor_atendimento, pm.cd_setor_atendimento))) centro_custo_desc,
       coalesce(pp.cd_setor_atendimento, pm.cd_setor_atendimento) centro_custo_solicitante,
       obter_nome_setor(coalesce(pp.cd_setor_atendimento, pm.cd_setor_atendimento)) centro_custo_solicitante_desc,
       pm.dt_prescricao data_solicitacao,
       obter_lab_execucao_etapa(pp.nr_prescricao, pp.nr_sequencia, '35', 'D') data_efetivacao,
       pp.ds_observacao observacao,
       decode(ap.ie_tipo_atendimento,7,1,0) tipo_paciente,
       null modalidade,
       pm.cd_medico medico_solicitante,
       obter_nome_pf(pm.cd_medico) medico_solicitante_desc,
       ap.ie_tipo_atendimento tipo_atendimento,
       obter_descricao_dominio(12,ap.ie_tipo_atendimento) tipo_atendimento_desc,
       rl.ds_resultado resultado_exame
from   prescr_procedimento pp
inner  join prescr_medica pm on pp.nr_prescricao = pm.nr_prescricao
inner  join atendimento_paciente ap on pm.nr_atendimento = ap.nr_atendimento
inner  join procedimento_paciente ppac on ppac.nr_sequencia_prescricao = pp.nr_sequencia and ppac.nr_prescricao = pp.nr_prescricao
inner  join result_laboratorio rl on rl.nr_prescricao = pp.nr_prescricao and rl.nr_seq_prescricao = pp.nr_sequencia
inner  join exame_laboratorio el on el.nr_seq_exame = pp.nr_seq_exame
where  pp.nr_seq_exame is not null
and    nvl(pp.ie_suspenso,'N') = 'N'
and    pp.ie_status_atend >= 35
and    pp.cd_material_exame is not null
and    nvl(el.ie_situacao, 'A') = 'A'
and    el.ie_anatomia_patologica = 'N'
and    nvl(pm.dt_liberacao_medico, pm.dt_liberacao) is not null
and    rl.ds_resultado is not null;
/