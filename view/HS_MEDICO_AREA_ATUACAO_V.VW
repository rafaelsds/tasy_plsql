create or replace
view hs_medico_area_atuacao_v as
select	a.cd_pessoa_fisica,
	a.nr_seq_area_atuacao,
	b.ds_area_atuacao
from	medico_area_atuacao a,
	area_atuacao_medica b,
	medico c
where	a.nr_seq_area_atuacao	= b.nr_sequencia
and	a.cd_pessoa_fisica	= c.cd_pessoa_fisica
and	c.ie_situacao		= 'A';
/