create or replace view prescr_proc_carestrem_cancel_v as
select  a."NR_SEQ_INTERNO",a."NR_PRESCRICAO",a."NR_SEQUENCIA",a."DT_INTEGRACAO",a."DT_RESULTADO",a."NM_USUARIO",a."DS_INTEGRACAO",a."DT_LIB_IMAGEM",a."CD_MOTIVO_BAIXA",a."DT_BAIXA",a."DT_RESULT_INTEGRACAO",a."NA_ACCESSIONNUMBER",a."NR_ACESSO_DICOM",a."DT_INTEGRACAO_SUSPENSO",a."NR_SEQ_PROC_INTERNO",a."CD_PROCEDIMENTO",a."IE_ORIGEM_PROCED",a."DS_PROCEDIMENTO",
    d.nr_atendimento,
    c.dt_cancelamento,
    case
    when c.ie_suspenso = 'S' then 'Procedimento suspenso'
    when d.DT_SUSPENSAO = 'S' then 'Prescrição suspensa'
    when c.DT_CANCELAMENTO is not null then 'Procedimento cancelado' end ds_situacao_item
FROM   PRESCR_PROC_PACS_INTEG_CARE_V a,
    REGRA_PROC_INTERNO_INTEGRA b,
    prescr_procedimento c,
    prescr_medica d
where  a.nr_seq_proc_interno = b.nr_seq_proc_interno
and    b.IE_TIPO_INTEGRACAO = 15
and    a.nr_prescricao = c.nr_prescricao
and    a.nr_sequencia = c.nr_sequencia
and    c.nr_prescricao = d.nr_prescricao
and    a.dt_integracao is not null
and    (c.ie_suspenso = 'S' or d.DT_SUSPENSAO = 'S' or c.DT_CANCELAMENTO is not null)

and c.dt_integracao_suspenso is null;
/