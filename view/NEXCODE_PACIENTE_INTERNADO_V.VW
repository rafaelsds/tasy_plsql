create or replace
view nexcode_paciente_internado_v
as
SELECT	/*+ ordered */
	a.nr_atendimento,
	a.cd_pessoa_fisica,
	p.nm_pessoa_fisica nm_paciente,
	b.cd_setor_atendimento,
	s.ds_setor_atendimento,
	b.cd_unidade_basica,
	b.cd_unidade_compl,
	(b.cd_unidade_basica || ' '|| b.cd_unidade_compl) ds_unidade
FROM	atendimento_paciente a,
	atend_paciente_unidade b,
	setor_atendimento s,
	pessoa_fisica p
WHERE	a.nr_atendimento	= b.nr_atendimento
AND		b.dt_entrada_unidade	= 	(SELECT /*+ index(x atepacu_i2) */
						MAX(x.dt_entrada_unidade)
					FROM	setor_atendimento y,
						atend_paciente_unidade x
					WHERE	x.nr_atendimento	= a.nr_atendimento
					AND	x.cd_setor_atendimento	= y.cd_setor_atendimento
					AND	x.dt_entrada_unidade	>= a.dt_entrada)
AND	b.cd_setor_atendimento  = s.cd_setor_atendimento
AND	a.cd_pessoa_fisica	= p.cd_pessoa_fisica
AND	a.dt_alta IS NULL;
/
