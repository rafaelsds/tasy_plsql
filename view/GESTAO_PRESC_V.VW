CREATE OR REPLACE VIEW gestao_presc_v
AS
SELECT qt_total valor,
	0 valor_f,
	'' param_16,
	'01' vl_chave, 
	'TT' ie_tipo,
	nr_sequencia 
FROM w_processo_adep
UNION ALL
SELECT qt_dieta_atras valor,
	qt_dieta_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '02' vl_chave,
    'D' ie_tipo,
	nr_sequencia
FROM w_processo_adep  
UNION ALL
SELECT qt_sne_atras valor,
	qt_sne_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '03' vl_chave,
    'SNE' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
SELECT qt_solucao_atras valor,
	qt_solucao_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '04' vl_chave,
    'S' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
SELECT qt_medic_atras valor,
	qt_medic_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '05' vl_chave,
    'M' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
  SELECT qt_proced_atras valor,
  	qt_solucao_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '06' vl_chave,
    'P' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
SELECT qt_glicemia_atras valor,
	qt_glicemia_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '07' vl_chave,
    'CG' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
SELECT qt_recomendacao_atras valor,
	qt_recomendacao_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '08' vl_chave,
    'R' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
SELECT qt_coleta_atras valor,
	qt_coleta_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '09' vl_chave,
    'C' ie_tipo,
	nr_sequencia
FROM w_processo_adep
UNION ALL
SELECT qt_gaso_atras valor,
	qt_gaso_futuro valor_f,
	PARAM_PCK.OBTER_VALOR_PARAMETRO_USUARIO(1290,16,WHEB_USUARIO_PCK.GET_CD_PERFIL,WHEB_USUARIO_PCK.GET_NM_USUARIO,WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO) param_16,
    '10' vl_chave,
    'G' ie_tipo,
	nr_sequencia
FROM w_processo_adep;
/
