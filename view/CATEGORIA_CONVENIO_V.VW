Create or Replace
VIEW Categoria_Convenio_v as
select  a.*,
	b.ds_convenio,
	substr(b.ds_convenio || ' ' || a.ds_categoria,1,200) ds_cat_convenio,
	b.ie_tipo_convenio,
	somente_numero(Obter_Classif_Conv(b.cd_convenio, 'C')) nr_seq_classificacao,
	substr(Obter_Classif_Conv(b.cd_convenio, 'D'),1,100) ds_classificacao,
	decode(Obter_Se_Conv_Nacional(b.cd_convenio), 'N', 'Internacional', 'S', 'Nacional', 'N�o Informado') ie_pais_conv
from 	convenio b,
	categoria_convenio a
where a.cd_convenio = b.cd_convenio;
/
