create or replace view fsfx_sgps_tasy_emp_plano_v
as 
select	substr(b.cd_operadora_empresa,1,length(b.cd_operadora_empresa)-3) codigo,
	substr(b.cd_operadora_empresa,length(b.cd_operadora_empresa)-2,2) CODIGO_SUBS,
	substr(b.cd_operadora_empresa,length(b.cd_operadora_empresa),1) DV_EMPP,
	a.nr_seq_plano CODIGO_PLAP,
	c.dt_inicio_vigencia VALIDADE,
	c.dt_fim_vigencia VIGENCIA
from	pls_contrato			b,
	pls_contrato_plano		a,
	PLS_REGRA_DIREITO_MED_PREV	c
where	a.nr_seq_contrato	= b.nr_sequencia
and	((c.nr_seq_contrato	= a.nr_sequencia) or (c.nr_seq_contrato is null))
and	((c.nr_seq_plano		= a.nr_seq_plano) or (c.nr_seq_plano is null))
group by b.cd_operadora_empresa,a.nr_seq_plano,c.dt_inicio_vigencia,c.dt_fim_vigencia;
/
