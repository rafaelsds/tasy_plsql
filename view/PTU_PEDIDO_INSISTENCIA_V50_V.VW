create or replace view	 ptu_pedido_insistencia_v50_v
as
select	1				ie_tipo_registro,
	00302				cd_transacao,
	nr_sequencia 			nr_sequencia,
	nr_seq_guia			nr_seq_guia,
	nr_seq_requisicao		nr_seq_requisicao,
	decode(ie_tipo_cliente,'U','UNIMED','P','PORTAL', 'R','PRESTADOR') ie_tipo_cliente,
	cd_unimed_executora		cd_unimed_executora,
	cd_unimed_beneficiario		cd_unimed_beneficiario,
	nr_seq_execucao 		nr_seq_execucao,
	nr_seq_origem 			nr_seq_origem,
	pls_obter_versao_scs		nr_versao,
	null	  			ds_mensagem,
	null				ds_fim
from	ptu_pedido_insistencia
where	ie_enviado	= 'N'
union
select	2				ie_tipo_registro,
	00302				cd_transacao,
	nr_sequencia			nr_sequencia,
	nr_seq_guia			nr_seq_guia,
	nr_seq_requisicao		nr_seq_requisicao,
	null				ie_tipo_cliente,
	null				cd_unimed_executora,
	null				cd_unimed_beneficiario,
	nr_seq_execucao 		nr_seq_execucao,
	null 				nr_seq_origem,
	null				nr_versao,
	Elimina_Acentuacao(substr(replace(replace(ds_mensagem,chr(13),''),chr(10),''),1,4000))	ds_mensagem,
	null				ds_fim
from	ptu_pedido_insistencia
where	ie_enviado	= 'N'
union
select	3				ie_tipo_registro,
	00302				cd_transacao,
	nr_sequencia			nr_sequencia,
	nr_seq_guia			nr_seq_guia,
	nr_seq_requisicao		nr_seq_requisicao,
	null				ie_tipo_cliente,
	null				cd_unimed_executora,
	null				cd_unimed_beneficiario,
	nr_seq_execucao 		nr_seq_execucao,
	null 				nr_seq_origem,
	null				nr_versao,
	null	  			ds_mensagem,
	'FIM$'				ds_fim
from	ptu_pedido_insistencia
where	ie_enviado	= 'N';
/