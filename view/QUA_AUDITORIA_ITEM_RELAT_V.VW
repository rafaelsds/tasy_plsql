create or replace 
view Qua_Auditoria_Item_Relat_v as
select	1 ordem,
	b.nr_sequencia nr_seq_audit,
	0 nr_seq_item_result,
	a.nr_seq_tipo, 
	a.cd_classificacao,
	'#' || c.ds_capitulo || '#N' ds_item,
	0 nr_seq_item,
	0 nr_seq_apres,
	'' ie_resultado,
	'' ds_orientacao,
	'' ds_complemento,
	'S' ie_permite_na,
	nvl(c.nr_seq_apres,0) nr_seq_apres_cap,
	nvl(a.nr_seq_apres,0) nr_seq_apres_estrut,
	0 nr_seq_apres_item,
	a.nr_sequencia nr_seq_audit_estrut,
	0 nr_seq_apres_subitem,
	0  nr_seq_subitem
from	qua_auditoria_cap c,
	qua_auditoria b,
	qua_auditoria_estrut a
where	a.nr_seq_tipo = b.nr_seq_tipo
and	a.nr_seq_cap = c.nr_sequencia
and	c.ie_situacao = 'A'
union
select	2 ordem,
	b.nr_sequencia nr_seq_audit,
	0 nr_seq_item_result,
	a.nr_seq_tipo, 
	a.cd_classificacao,
	'#' || decode(a.nr_seq_cap,null,a.ds_estrutura,'    ' || a.ds_estrutura) || '#N#G' ds_item,
	0 nr_seq_item,
	0 nr_seq_apres,
	'' ie_resultado,
	'' ds_orientacao,
	'' ds_complemento,
	'S' ie_permite_na,
	nvl(c.nr_seq_apres,0) nr_seq_apres_cap,
	nvl(a.nr_seq_apres,0) nr_seq_apres_estrut,
	0 nr_seq_apres_item,
	a.nr_sequencia nr_seq_audit_estrut,
	0 nr_seq_apres_subitem,
	0  nr_seq_subitem
from	qua_auditoria_cap c,
	qua_auditoria b,
	qua_auditoria_estrut a
where	a.nr_seq_tipo = b.nr_seq_tipo
and	a.nr_seq_cap = c.nr_sequencia
and	c.ie_situacao = 'A'
union
select	3 ordem,
	d.nr_sequencia nr_seq_audit,
	c.nr_sequencia nr_seq_item_result,
	a.nr_seq_tipo, 
	a.cd_classificacao,
	'        ' || b.nr_seq_apres || ' ' || b.ds_item ds_item,
	b.nr_sequencia nr_seq_item,
	b.nr_seq_apres,
	c.ie_resultado,
	b.ds_orientacao,
	c.ds_complemento,
	b.ie_permite_na,
	nvl(e.nr_seq_apres,0) nr_seq_apres_cap,
	nvl(a.nr_seq_apres,0) nr_seq_apres_estrut,
	nvl(b.nr_seq_apres,0) nr_seq_apres_item,
	a.nr_sequencia nr_seq_audit_estrut,
	0 nr_seq_apres_subitem,
	0  nr_seq_subitem
from	qua_auditoria_cap e,
	qua_auditoria d,
	qua_auditoria_result c,
	qua_auditoria_item b,
	qua_auditoria_estrut a
where	b.nr_seq_estrutura = a.nr_sequencia
and	d.nr_sequencia = c.nr_seq_auditoria
and	a.nr_seq_cap = e.nr_sequencia
and	d.nr_seq_tipo = a.nr_seq_tipo
and	nvl(c.nr_seq_subitem,0) = 0
and	c.nr_seq_item = b.nr_sequencia
union 
select	4 ordem,
	d.nr_sequencia nr_seq_audit,
	c.nr_sequencia nr_seq_item_result,
	a.nr_seq_tipo, 
	a.cd_classificacao,
	'               ' || b.nr_seq_apres || ' ' || s.ds_item ds_item,
	b.nr_sequencia nr_seq_item,
	b.nr_seq_apres,
	c.ie_resultado,
	s.ds_orientacao,
	c.ds_complemento,
	b.ie_permite_na,
	nvl(e.nr_seq_apres,0) nr_seq_apres_cap,
	nvl(a.nr_seq_apres,0) nr_seq_apres_estrut,
	nvl(b.nr_seq_apres,0) nr_seq_apres_item,
	a.nr_sequencia nr_seq_audit_estrut,
	nvl(s.nr_seq_apres,0) nr_seq_apres_subitem,
	s.nr_sequencia nr_seq_subitem
from	qua_auditoria_cap e,
	qua_auditoria d,
	qua_auditoria_result c,
	qua_auditoria_item b,
	qua_auditoria_subitem s,
	qua_auditoria_estrut a
where	b.nr_seq_estrutura = a.nr_sequencia
and	d.nr_sequencia = c.nr_seq_auditoria
and	a.nr_seq_cap = e.nr_sequencia
and	s.nr_seq_item = b.nr_sequencia
and	d.nr_seq_tipo = a.nr_seq_tipo
and	s.nr_sequencia= c.nr_seq_subitem
and	c.nr_seq_item = b.nr_sequencia;
/