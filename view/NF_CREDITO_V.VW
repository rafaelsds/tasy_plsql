create or replace view NF_CREDITO_V
as
select 	decode(obter_se_anticipo(a.nr_sequencia), 'S', '84111506', decode(fis_obter_usocfdi(a.nr_sequencia), 'G02', '84111506', nvl(nvl(b.cd_clave_agrup, obter_nr_proc_interno(cd_procedimento, ie_origem_proced)), nvl(decode(c.ie_devolucao, 'S', b.cd_mat_proc_fornec, obter_classif_fiscal(b.cd_material)), '84111506')))) clave,
	decode(obter_se_anticipo(a.nr_sequencia), 'S', 'ACT', decode(fis_obter_vinculo_adiantamento(a.nr_sequencia), 'S', 'ACT', decode(fis_obter_usocfdi(a.nr_sequencia), 'G02', 'ACT', decode((SELECT
	nvl(p.ie_rateio_trib, 'N')
from 	procedimento_fiscal p
where 	p.cd_procedimento = b.cd_procedimento), 'S', '10', decode(b.cd_procedimento, NULL, obter_conversao_externa(NULL, 'UNIDADE_MEDIDA', 'CD_SISTEMA_ANT', b.cd_unidade_medida_compra), 'E48'))))) clave_unidade,
	1 identificacion,
	b.qt_item_nf cantidad,
	decode(obter_se_anticipo(a.nr_sequencia), 'S', 'Aplicacion de anticipo', decode(fis_obter_usocfdi(b.nr_sequencia), 'G02', 'Descuentos, devoluciones o bonificaciones a CFDI relacionado', decode(cd_material, NULL, decode(b.cd_procedimento, NULL, obter_desc_proc_interno(nr_seq_proc_interno), obter_desc_proc_material(NULL, b.cd_procedimento, ie_origem_proced)), obter_desc_material(b.cd_material)))) descripcion,
	b.vl_unitario_item_nf valor_unitario,
	b.vl_total_item_nf importe,
	decode(decode(obter_se_anticipo(a.nr_sequencia), 'S', 'ACT', decode(fis_obter_vinculo_adiantamento(a.nr_sequencia), 'S', 'ACT', decode(fis_obter_usocfdi(a.nr_sequencia), 'G02', 'ACT', decode((SELECT
	nvl(p.ie_rateio_trib, 'N')
from 	procedimento_fiscal p
where 	p.cd_procedimento = b.cd_procedimento), 'S', '10', 'E48')))), 'ACT', 'Actividad', '10', 'Grupo', 'E48', decode(b.cd_procedimento, NULL, obter_desc_unidade_medida(b.cd_unidade_medida_compra), 'Unidad de servicio'), 'Unidad de servicio') unidad,
	b.vl_desconto descuento,
	b.nr_item_nf nr_item,
	a.nr_sequencia nr_sequencia,
	obter_se_possui_iva_zero(a.nr_sequencia,b.nr_item_nf) iva_zero,
	b.vl_liquido vl_liquido,
	nfe_obter_valor_trib_item(a.nr_sequencia,b.nr_item_nf,null,'TRIB') vl_tributo,
	nfe_obter_valor_trib_item(a.nr_sequencia,b.nr_item_nf,null,'BC') vl_base,
	nvl(c.ie_nota_credito,'N') ie_nota_credito,
	a.nr_sequencia_ref nr_sequencia_ref
from 	nota_fiscal a,
	nota_fiscal_item b,
	operacao_nota c
where 	a.nr_sequencia = b.nr_sequencia
and 	a.cd_operacao_nf = c.cd_operacao_nf;
/
