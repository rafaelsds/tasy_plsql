create or replace view RISLIS_DESENV_INFORMATION_OS_V as
SELECT os.NR_SEQUENCIA os_number,
  os.NR_SEQ_LOCALIZACAO os_location,
  SUBSTR(obter_desc_man_localizacao(os.NR_SEQ_LOCALIZACAO),1,255) os_desc_location,
  os.DS_DANO_BREVE os_description,
  loc.IE_TERCEIRO loc_terceiro,
  DECODE(loc.IE_TERCEIRO, 'S', 'External', 'Internal') loc_desc_terceiro,
  os.IE_PLATAFORMA os_plataform,
  SUBSTR(obter_valor_dominio(2811, os.IE_PLATAFORMA),1,255) os_desc_plataform,
  os.IE_CLASSIFICACAO os_classification,
  SUBSTR(obter_valor_dominio(1149, os.IE_CLASSIFICACAO),1,255) os_desc_classification,
  os.NR_SEQ_CLASSIF os_subclassification,
  SUBSTR(obter_desc_classificacao(os.NR_SEQ_CLASSIF),1,255) os_desc_subclassification,
  os.NR_SEQ_NIVEL_VALOR os_aggregation,
  DECODE(os.NR_SEQ_NIVEL_VALOR, 2, 'No', 1, 'Yes', 3, 'Yes', 'NA') os_desc_aggregation_value,
  SUBSTR(obter_desc_nivel_valor(os.NR_SEQ_NIVEL_VALOR),1,255) os_desc_aggregation,
  os.NR_SEQ_GRUPO_DES os_group,
  SUBSTR(obter_desc_grupo_desen(os.NR_SEQ_GRUPO_DES),1,255) os_desc_group,
  os.CD_FUNCAO os_function,
  SUBSTR(obter_nome_funcao(os.CD_FUNCAO),1,255) os_desc_function,
  DECODE(obter_periodo_os(os.NR_SEQUENCIA),'G','Good','W','Warning','C','Critical','B','Breached') os_sla_status,
  os.NR_SEQ_ESTAGIO os_stage,
  SUBSTR(obter_desc_estagio_proc(os.NR_SEQ_ESTAGIO),1,255) os_desc_stage,
  os.CD_PESSOA_SOLICITANTE os_requester,
  obter_nome_pf(os.CD_PESSOA_SOLICITANTE) os_desc_requester,
  os.DT_ORDEM_SERVICO os_date,
  os.DT_FIM_REAL os_close_date,
  os.IE_GRAU_SATISFACAO os_satisfaction,
  DECODE(os.IE_GRAU_SATISFACAO, 'N', 'NA', 'R', 'No', 'P', 'No', 'Yes') os_desc_satisfaction_value,
  SUBSTR(obter_valor_dominio(1197, os.IE_GRAU_SATISFACAO),1,255) os_desc_satisfaction,
  satisf.IE_GRAU_SATISFACAO_GERAL satisf_geral,
  SUBSTR(obter_valor_dominio(1197, satisf.IE_GRAU_SATISFACAO_GERAL),1,255) satisf_desc_geral,
  satisf.IE_AGILIDADE satisf_agility,
  SUBSTR(obter_valor_dominio(1197, satisf.IE_AGILIDADE),1,255) satisf_desc_agility,
  satisf.IE_CONHECIMENTO satisf_knowledge,
  SUBSTR(obter_valor_dominio(1197, satisf.IE_CONHECIMENTO),1,255) satisf_desc_knowledge,
  satisf.IE_CORDIALIDADE satisf_cordiality,
  SUBSTR(obter_valor_dominio(1197, satisf.IE_CORDIALIDADE),1,255) satisf_desc_cordiality,
  satisf.IE_SOLUCAO satisf_solution,
  SUBSTR(obter_valor_dominio(1197, satisf.IE_SOLUCAO),1,255) satisf_desc_solution,
  os.NR_SEQ_JUSTIF_GRAU_SATISF satisf_justification,
  SUBSTR(man_obter_justif_satisf_ordem(os.NR_SEQ_JUSTIF_GRAU_SATISF),1,255) satisf_desc_justification,
  os.NR_SEQ_GERENCIA_INSATISF satisf_management,
  SUBSTR(obter_descricao_gerencia(os.NR_SEQ_GERENCIA_INSATISF),1,255) satisf_desc_management,
  CASE defect.IE_ORIGEM_ERRO  WHEN 'W' THEN 'Philips'
                              WHEN 'O' THEN 'Distributor'
                              WHEN 'N' THEN 'Philips other'
                              WHEN 'E' THEN 'External'
                              ELSE  'Customer' END  defect_origin,
  substr(obter_desc_tipo_erro_os(defect.NR_SEQ_TIPO), 1,200) defect_type,
  substr(man_obter_desc_subtipo_erro_os(defect.NR_SEQ_SUBTIPO),1,80) defect_subtype,
  substr(obter_desc_linguagem_prog(defect.NR_SEQ_LP), 1,255)  defect_language,
  obter_nome_pf(defect.CD_PESSOA_FISICA) defect_responsible,
  SUBSTR(obter_desc_grupo_desen(defect.NR_SEQ_GRUPO_DES),1,255) defect_group,
  SUBSTR(obter_valor_dominio(2682, defect.IE_IDENT_ERRO),1,255) defect_detected,
  SUBSTR(obter_valor_dominio(4319, defect.IE_BASE_CLIENTE),1,255) defect_database,
  SUBSTR(obter_valor_dominio(2852, defect.IE_FORMA_ORIGEM),1,255) defect_cause,
  defect.QT_CLIENTE defect_amount,
  defect.DT_OCORRENCIA_ERRO defect_date,
  defect.NR_SEQ_ORDEM_DEF defect_so,
  defect.DT_LIBERACAO defect_submission_date,  
  man_obter_data_prim_hist(os.NR_SEQUENCIA) os_hist_first_date,
  man_obter_data_ult_hist_tipo(os.NR_SEQUENCIA, 1, 'P') os_hist_last_date,
  obter_dif_data(man_obter_data_ult_hist_tipo(os.NR_SEQUENCIA, 1, 'P'), nvl(os.DT_FIM_REAL, sysdate), 'TM') os_hist_last_min,
  ativ.DT_ATIVIDADE ativ_date,
  ativ.QT_MINUTO ativ_time,
  obter_dif_data(os.DT_ORDEM_SERVICO, nvl(os.DT_FIM_REAL, sysdate), 'TM') os_age_min
FROM MAN_ORDEM_SERVICO os
LEFT JOIN MAN_LOCALIZACAO loc
ON os.NR_SEQ_LOCALIZACAO = loc.NR_SEQUENCIA
LEFT JOIN GRUPO_DESENVOLVIMENTO grupo
ON os.NR_SEQ_GRUPO_DES = grupo.NR_SEQUENCIA
LEFT JOIN MAN_ORDEM_SERV_ATIV ativ
ON os.NR_SEQUENCIA = ativ.NR_SEQ_ORDEM_SERV
AND OBTER_SE_USUARIO_GERENCIA(ativ.NM_USUARIO_EXEC, 21) = 'S'
LEFT JOIN MAN_DOC_ERRO defect
ON os.NR_SEQUENCIA = defect.NR_SEQ_ORDEM
LEFT JOIN MAN_ORDEM_SERV_SATISF satisf
ON os.NR_SEQUENCIA = satisf.NR_SEQ_ORDEM_SERV
WHERE grupo.NR_SEQ_GERENCIA = 21
AND EXISTS (SELECT 	1
			FROM MAN_ESTAGIO_PROCESSO estag_proc
			INNER JOIN MAN_ORDEM_SERV_ESTAGIO serv_estag
			ON estag_proc.NR_SEQUENCIA = serv_estag.NR_SEQ_ESTAGIO
			WHERE serv_estag.NR_SEQ_ORDEM = os.NR_SEQUENCIA
			AND	estag_proc.IE_DESENV = 'S');
/