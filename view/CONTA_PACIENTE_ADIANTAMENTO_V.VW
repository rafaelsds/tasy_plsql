create or replace 
view CONTA_PACIENTE_ADIANTAMENTO_V as
select	to_number(OBTER_SALDO_ADIANTAMENTO_CONTA(a.nr_adiantamento,1)) ie_tipo,
	a.nr_atendimento,		
	a.dt_adiantamento,		
	a.nr_adiantamento,		
	a.cd_tipo_recebimento,		 
	to_number(OBTER_SALDO_ADIANTAMENTO_CONTA(a.nr_adiantamento,2)) vl_disponivel,
	a.vl_saldo,
	a.vl_adiantamento,
	c.nr_interno_conta,
	a.ds_observacao,
	a.cd_estabelecimento,
	a.ie_lib_caixa,
	a.nr_seq_contrato,
	a.nr_seq_orcamento_pac
from	adiantamento a,
	conta_paciente_adiant b,
	conta_paciente c
where	a.nr_adiantamento 	= b.nr_adiantamento(+)
and	b.nr_interno_conta 	= c.nr_interno_conta(+)
and	c.ie_cancelamento is null;
/
