create or replace view w_eis_escala_prm_prev_fator_v  as 
select distinct
	e.cd_setor_atendimento,
	substr(obter_nome_setor(e.cd_setor_atendimento),1,255) ds_Setor_Atendimento,	
	b.dt_evento,
	f.cd_empresa,
	a.cd_estabelecimento,	
	d.nr_seq_fator,
	obter_unidade_atendimento(b.nr_atendimento,'A','U') ds_unidade,
	substr(obter_dados_escala_prm(d.nr_seq_fator,'F',null,null),1,255) ds_fator,
	to_number(obter_dados_escala_prm(null,'T',b.dt_evento,null)) qt_total,
	b.nr_atendimento,
	e.nr_sequencia,
	decode((obter_se_material_risco_PRM(e.nr_sequencia)),'S','Sim','N�o') ds_alto_risco
from	qua_evento_paciente b,
	PRM_FICHA_OCORRENCIA e,
	PRM_FICHA_FAT_CONTR d,
	PRM_FICHA_MATERIAL c,
	estabelecimento f,
	atendimento_paciente a
where	a.nr_atendimento 	= b.nr_atendimento
and	b.dt_inativacao is null
and	a.cd_estabelecimento 	= f.cd_estabelecimento
and	e.nr_seq_evento 	= b.nr_sequencia
and	e.nr_sequencia		= d.nr_seq_ficha(+)
and	e.nr_sequencia		= c.nr_seq_ficha(+);	
/
