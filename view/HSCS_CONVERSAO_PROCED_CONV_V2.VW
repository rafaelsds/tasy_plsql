CREATE OR REPLACE VIEW hscs_conversao_proced_conv_v2 as
SELECT	1 ie_opcao,
	d.cd_convenio,
	b.ds_tabela_servico ds_tab_serv,
	SUBSTR(NVL(obter_proced_conversao_conv(d.cd_convenio, a.cd_procedimento, 1, 'C'), 	a.cd_procedimento),1,20) 	cd_procedimento_conversao,
	SUBSTR(NVL(obter_proced_conversao_conv(d.cd_convenio, a.cd_procedimento, 1, 'D'), 	c.ds_procedimento),1,100) 	ds_procedimento_conversao,
	a.vl_servico vl_serv,
	a.cd_unidade_medida cd_um,
	a.dt_inicio_vigencia dt_inic_vig,
	a.cd_procedimento,
	c.ds_procedimento,
	e.ds_especialidade,
	e.DS_GRUPO_PROC,
	e.DS_AREA_PROCEDIMENTO
FROM	procedimento c,
	preco_servico a,
	tabela_servico b,
	convenio_servico d,
	estrutura_procedimento_v e
WHERE	a.cd_procedimento		= c.cd_procedimento
AND	c.ie_origem_proced		= 1
AND	a.cd_tabela_servico	= d.cd_tabela_servico
AND	a.cd_tabela_servico	= b.cd_tabela_servico
AND	a.cd_estabelecimento	= d.cd_estabelecimento
AND	e.cd_procedimento	= c.cd_procedimento
AND	e.ie_origem_proced	= c.ie_origem_proced
AND	a.dt_inicio_vigencia		= ( 	SELECT	MAX(e.dt_inicio_vigencia)
						FROM	preco_servico e
						WHERE	e.cd_procedimento = a.cd_procedimento
						AND	e.cd_tabela_servico = a.cd_tabela_servico)
GROUP BY	b.ds_tabela_servico,
	SUBSTR(NVL(obter_proced_conversao_conv(d.cd_convenio, a.cd_procedimento, 1, 'C'), a.cd_procedimento),1,20),
	SUBSTR(NVL(obter_proced_conversao_conv(d.cd_convenio, a.cd_procedimento, 1, 'D'), c.ds_procedimento),1,100),
	a.vl_servico,
	a.cd_unidade_medida,
	a.dt_inicio_vigencia,
	a.cd_procedimento,
	e.ds_especialidade,
	e.DS_GRUPO_PROC,
	e.DS_AREA_PROCEDIMENTO,
	d.cd_convenio,
	c.ds_procedimento
union all
SELECT	2 ie_opcao,
	c.cd_convenio,
	null ds_tab_serv,
	SUBSTR(NVL(obter_proced_conversao_conv(c.cd_convenio, c.cd_procedimento, 1, 'C'), 	
	c.cd_procedimento),1,20) 	cd_procedimento_conversao,
	SUBSTR(NVL(obter_proced_conversao_conv(c.cd_convenio, c.cd_procedimento, 1, 'D'), 	
	obter_descricao_procedimento(c.cd_procedimento, c.ie_origem_proced)),1,100) 	ds_procedimento_conversao,
	null,
	null,
	null,
	c.cd_procedimento,
	null,
	null,
	null,
	null
FROM	procedimento_paciente c
GROUP BY	null,
	SUBSTR(NVL(obter_proced_conversao_conv(c.cd_convenio, c.cd_procedimento, 1, 'C'), 
	c.cd_procedimento),1,20),
	SUBSTR(NVL(obter_proced_conversao_conv(c.cd_convenio, c.cd_procedimento, 1, 'D'), 
	obter_descricao_procedimento(c.cd_procedimento, c.ie_origem_proced)),1,100),
	null,
	null,
	null,
	c.cd_procedimento,
	null,
	null,
	null,
	c.cd_convenio,
	null
/