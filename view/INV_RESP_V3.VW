create or replace 
view inv_resp_v3 (DS_COMANDO, DS_ERRO, NM_OBJETO, NM_RESPONSAVEL)
as
select	o.ds_comando, o.DS_ERRO, o.nm_objeto, nvl(nm_usuario,'N�o existe documentacao') as nm_responsavel 
from	inv_v3 o
      left join objeto_sistema s on o.nm_objeto = s.nm_objeto;
/