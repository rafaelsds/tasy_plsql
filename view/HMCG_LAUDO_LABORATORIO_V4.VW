CREATE OR REPLACE VIEW HMCG_LAUDO_LABORATORIO_V4 ( IE_STATUS_ATEND, 
NR_PROTOCOLO, NR_ATENDIMENTO, NM_POSTO, DS_IDADE, 
NM_PACIENTE, NM_PACIENTE_RN, DS_CONVENIO, NM_MEDICO, 
DT_ENTRADA, DT_EMISSAO, NR_MATRICULA, NR_PRESCRICAO, 
CD_MEDICO_RESP, NM_MEDICO_ASS, DS_EXAME_MEDICO_P, CD_MEDICO_SOLICITANTE, 
IE_TIPO_P, CD_PROCEDENCIA, DS_PROCEDENCIA, CD_UNIDADE, 
IE_TIPO_ATENDIMENTO, NR_SEQUENCIA, NM_USUARIO_LIBERADOR, NM_LIBERADOR
 ) AS SELECT DISTINCT C.IE_STATUS_ATEND, DECODE (a.cd_estabelecimento,22, 'JAC',23, 'ADY',24, 'SAT',26, 'CAR',4, 'VIL',61, 'UBA')|| ' '|| LPAD (b.nr_prescricao, 6, '0') nr_protocolo,
                a.nr_atendimento,
                SUBSTR(obter_nome_estabelecimento (b.cd_estabelecimento),1,99)|| DECODE (a.cd_estabelecimento,4, DECODE (b.cd_setor_atendimento,18, '','/' || z.ds_setor_atendimento)) nm_posto,
                obter_idade_pf (DECODE (b.cd_recem_nato,NULL, a.cd_pessoa_fisica,b.cd_recem_nato),SYSDATE,'S')|| ' '|| DECODE (DECODE (b.cd_recem_nato,NULL, a.ie_sexo,obter_sexo_pf (b.cd_recem_nato, 'C')),'M', 'Masc.','Fem.') ds_idade,
                DECODE (b.cd_recem_nato,NULL, UPPER (a.nm_paciente),'RN ' || UPPER (a.nm_paciente)) nm_paciente,
                DECODE (b.cd_recem_nato,NULL, UPPER (nm_paciente),UPPER (nm_paciente)|| ' / RN: '|| UPPER (obter_nome_pf (b.cd_recem_nato))) nm_paciente_rn,
                UPPER (a.ds_convenio) ds_convenio,
                UPPER (SUBSTR (   obter_nome_pf (b.cd_medico) ||' - CRM '|| obter_dados_medico (b.cd_medico, 'CRM') ,1,140)) nm_medico,
                a.dt_entrada,
                SYSDATE dt_emissao,
                a.cd_usuario_convenio nr_matricula,
                c.nr_prescricao,
                NVL (k.cd_medico_resp, 0) cd_medico_resp,
                obter_nome_pf (w.cd_pessoa_fisica)|| ' - '|| SUBSTR (obter_conselho_profissional (w.nr_seq_conselho,'S'),1,20)|| ': '|| w.ds_codigo_prof nm_medico_ass,
                '' ds_exame_medico_p,
                b.cd_medico cd_medico_solicitante,
                1 ie_tipo_p,
                a.cd_procedencia,
                obter_desc_procedencia(a.cd_procedencia) ds_procedencia,
                a.cd_unidade,
                a.ie_tipo_atendimento,
	c.nr_Sequencia,
	SUBSTR(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U'),1,15)nm_usuario_liberador,
	SUBSTR(obter_nome_usuario(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U')),1,60)nm_liberador
           FROM pessoa_fisica w,
                pessoa_fisica g,
                grupo_exame_lab e,
                exame_lab_material h,
                exame_laboratorio d,
                material_exame_lab f,
                prescr_medica b,
                exame_lab_resultado j,
                exame_lab_result_item k,
                prescr_procedimento c,
                atendimento_paciente_v a,
                setor_atendimento z
          WHERE a.nr_atendimento = b.nr_atendimento
            AND b.nr_prescricao = c.nr_prescricao
            AND c.nr_seq_exame = d.nr_seq_exame
            AND c.cd_material_exame = f.cd_material_exame
            AND b.cd_medico = g.cd_pessoa_fisica
            AND c.nr_seq_exame = h.nr_seq_exame
            AND h.nr_seq_material = f.nr_sequencia
            AND d.nr_seq_grupo = e.nr_sequencia
            AND b.nr_prescricao = j.nr_prescricao
            AND j.nr_seq_resultado = k.nr_seq_resultado
            AND c.nr_sequencia = k.nr_seq_prescr
            AND w.cd_pessoa_fisica(+) = k.cd_medico_resp
            AND z.cd_setor_atendimento = b.cd_setor_atendimento
            AND c.cd_medico_solicitante IS NULL
            AND c.ie_status_atend >=40
            AND k.nr_seq_formato IS NOT NULL
UNION ALL
SELECT DISTINCT   C.IE_STATUS_ATEND, DECODE (a.cd_estabelecimento,22, 'JAC',23, 'ADY',24, 'SAT',26, 'CAR',4, 'VIL',61, 'UBA')|| ' '|| LPAD (b.nr_prescricao, 6, '0') nr_protocolo,
                a.nr_atendimento,
                SUBSTR(obter_nome_estabelecimento (b.cd_estabelecimento),1,99)|| DECODE (b.cd_estabelecimento,4, DECODE (a.cd_setor_atendimento,18, '','/' || z.ds_setor_atendimento)) nm_posto,
                obter_idade_pf (DECODE (b.cd_recem_nato,NULL, a.cd_pessoa_fisica,b.cd_recem_nato),SYSDATE,'S')|| ' '|| DECODE (DECODE (b.cd_recem_nato,NULL, a.ie_sexo,obter_sexo_pf (b.cd_recem_nato, 'C')),'M', 'Masc.','Fem.') ds_idade,
                DECODE (b.cd_recem_nato,NULL, UPPER (a.nm_paciente),'RN ' || UPPER (a.nm_paciente)) nm_paciente,
                DECODE (b.cd_recem_nato,NULL, UPPER (nm_paciente),UPPER (nm_paciente)|| ' / RN: '|| UPPER (obter_nome_pf (b.cd_recem_nato))) nm_paciente_rn,
                UPPER (a.ds_convenio) ds_convenio,
                UPPER (SUBSTR (   obter_nome_pf (i.cd_profissional) ||' - CRM '|| obter_dados_medico (i.cd_profissional, 'CRM') ,1,140)) nm_medico,
                a.dt_entrada dt_entrada,
                SYSDATE dt_emissao,
                a.cd_usuario_convenio nr_matricula,
                c.nr_prescricao,
                NVL (k.cd_medico_resp, 0) cd_medico_resp,
                obter_nome_pf (w.cd_pessoa_fisica)|| ' - '|| SUBSTR (obter_conselho_profissional (w.nr_seq_conselho,'S'),1,20)|| ': '|| w.ds_codigo_prof nm_medico_ass,
                obter_exame_medico_adic(c.nr_prescricao,i.cd_profissional) ds_exame_medico_p,
	NVL(c.cd_medico_solicitante,b.cd_medico) cd_medico_solicitante,
                2 ie_tipo_p,
                a.cd_procedencia,
                obter_desc_procedencia(a.cd_procedencia) ds_procedencia,
                a.cd_unidade,
                a.ie_tipo_atendimento,
	c.nr_Sequencia,
	SUBSTR(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U'),1,15)nm_usuario_liberador,
	SUBSTR(obter_nome_usuario(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U')),1,60)nm_liberador
           FROM prescr_proced_prof_adic i,
                pessoa_fisica w,
                pessoa_fisica g,
                grupo_exame_lab e,
                exame_lab_material h,
                exame_laboratorio d,
                material_exame_lab f,
                prescr_medica b,
                exame_lab_resultado j,
                exame_lab_result_item k,
                prescr_procedimento c,
                atendimento_paciente_v a,
                setor_atendimento z
          WHERE a.nr_atendimento = b.nr_atendimento
            AND b.nr_prescricao = c.nr_prescricao
            AND c.nr_seq_exame = d.nr_seq_exame
            AND c.cd_material_exame = f.cd_material_exame
            AND b.cd_medico = g.cd_pessoa_fisica
            AND c.nr_seq_exame = h.nr_seq_exame
            AND h.nr_seq_material = f.nr_sequencia
            AND d.nr_seq_grupo = e.nr_sequencia
            AND c.nr_prescricao = i.nr_prescricao
            AND c.nr_sequencia = i.nr_seq_procedimento
            AND b.nr_prescricao = j.nr_prescricao
            AND j.nr_seq_resultado = k.nr_seq_resultado
            AND c.nr_sequencia = k.nr_seq_prescr
            AND w.cd_pessoa_fisica(+) = k.cd_medico_resp
            AND z.cd_setor_atendimento = b.cd_setor_atendimento
            AND i.cd_profissional IS NOT NULL
            AND c.ie_status_atend >=40
            AND k.nr_seq_formato IS NOT NULL
UNION ALL
SELECT DISTINCT   C.IE_STATUS_ATEND, DECODE (a.cd_estabelecimento,22, 'JAC',23, 'ADY',24, 'SAT',26, 'CAR',4, 'VIL',61, 'UBA')|| ' '|| LPAD (b.nr_prescricao, 6, '0') nr_protocolo,
                a.nr_atendimento,
                SUBSTR(obter_nome_estabelecimento (b.cd_estabelecimento),1,99)|| DECODE (b.cd_estabelecimento,4, DECODE (a.cd_setor_atendimento,18, '','/' || z.ds_setor_atendimento)) nm_posto,
                obter_idade_pf (DECODE (b.cd_recem_nato,NULL, a.cd_pessoa_fisica,b.cd_recem_nato),SYSDATE,'S')|| ' '|| DECODE (DECODE (b.cd_recem_nato,NULL, a.ie_sexo,obter_sexo_pf (b.cd_recem_nato, 'C')),'M', 'Masc.','Fem.') ds_idade,
                DECODE (b.cd_recem_nato,NULL, UPPER (a.nm_paciente),'RN ' || UPPER (a.nm_paciente)) nm_paciente,
                DECODE (b.cd_recem_nato,NULL, UPPER (nm_paciente),UPPER (nm_paciente)|| ' / RN: '|| UPPER (obter_nome_pf (b.cd_recem_nato))) nm_paciente_rn,
                UPPER (a.ds_convenio) ds_convenio,
                UPPER (SUBSTR (   obter_nome_pf (i.cd_medico_solicitante) ||' - CRM '|| obter_dados_medico (i.cd_medico_solicitante, 'CRM') ,1,140)) nm_medico,
                a.dt_entrada dt_entrada,
                SYSDATE dt_emissao,
                a.cd_usuario_convenio nr_matricula,
                c.nr_prescricao,
                NVL (k.cd_medico_resp, 0) cd_medico_resp,
                obter_nome_pf (w.cd_pessoa_fisica)|| ' - '|| SUBSTR (obter_conselho_profissional (w.nr_seq_conselho,'S'),1,20)|| ': '|| w.ds_codigo_prof nm_medico_ass,
                '' ds_exame_medico_p,
	NVL(c.cd_medico_solicitante,b.cd_medico) cd_medico_solicitante,
                3 ie_tipo_p,
                a.cd_procedencia,
                obter_desc_procedencia(a.cd_procedencia) ds_procedencia,
                a.cd_unidade,
                a.ie_tipo_atendimento,
	c.nr_Sequencia,
	SUBSTR(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U'),1,15)nm_usuario_liberador,
	SUBSTR(obter_nome_usuario(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U')),1,60)nm_liberador
           FROM prescr_medica_prof_adic i,
                pessoa_fisica w,
                pessoa_fisica g,
                grupo_exame_lab e,
                exame_lab_material h,
                exame_laboratorio d,
                material_exame_lab f,
                prescr_medica b,
                exame_lab_resultado j,
                exame_lab_result_item k,
                prescr_procedimento c,
                atendimento_paciente_v a,
                setor_atendimento z
          WHERE a.nr_atendimento = b.nr_atendimento
            AND b.nr_prescricao = c.nr_prescricao
            AND c.nr_seq_exame = d.nr_seq_exame
            AND c.cd_material_exame = f.cd_material_exame
            AND b.cd_medico = g.cd_pessoa_fisica
            AND c.nr_seq_exame = h.nr_seq_exame
            AND h.nr_seq_material = f.nr_sequencia
            AND d.nr_seq_grupo = e.nr_sequencia
            AND c.nr_prescricao = i.nr_prescricao
            AND b.nr_prescricao = j.nr_prescricao
            AND j.nr_seq_resultado = k.nr_seq_resultado
            AND c.nr_sequencia = k.nr_seq_prescr
            AND w.cd_pessoa_fisica(+) = k.cd_medico_resp
            AND z.cd_setor_atendimento = b.cd_setor_atendimento
            AND i.cd_medico_solicitante IS NOT NULL
            AND c.ie_status_atend >=40
            AND k.nr_seq_formato IS NOT NULL
UNION ALL
SELECT DISTINCT    C.IE_STATUS_ATEND, DECODE (a.cd_estabelecimento,22, 'JAC',23, 'ADY',24, 'SAT',26, 'CAR',4, 'VIL',61, 'UBA')|| ' '|| LPAD (b.nr_prescricao, 6, '0') nr_protocolo,
                a.nr_atendimento,
                SUBSTR(obter_nome_estabelecimento (b.cd_estabelecimento),1,99)|| DECODE (a.cd_estabelecimento,4, DECODE (b.cd_setor_atendimento,18, '','/' || z.ds_setor_atendimento)) nm_posto,
                obter_idade_pf (DECODE (b.cd_recem_nato,NULL, a.cd_pessoa_fisica,b.cd_recem_nato),SYSDATE,'S')|| ' '|| DECODE (DECODE (b.cd_recem_nato,NULL, a.ie_sexo,obter_sexo_pf (b.cd_recem_nato, 'C')),'M', 'Masc.','Fem.') ds_idade,
                DECODE (b.cd_recem_nato,NULL, UPPER (a.nm_paciente),'RN ' || UPPER (a.nm_paciente)) nm_paciente,
                DECODE (b.cd_recem_nato,NULL, UPPER (nm_paciente),UPPER (nm_paciente)|| ' / RN: '|| UPPER (obter_nome_pf (b.cd_recem_nato))) nm_paciente_rn,
                UPPER (a.ds_convenio) ds_convenio,
                UPPER (SUBSTR (   obter_nome_pf (c.cd_medico_solicitante) ||' - CRM '|| obter_dados_medico (c.cd_medico_solicitante, 'CRM') ,1,140)) nm_medico,
                a.dt_entrada,
                SYSDATE dt_emissao,
                a.cd_usuario_convenio nr_matricula,
                c.nr_prescricao, 
                NVL (k.cd_medico_resp, 0) cd_medico_resp,
                 obter_nome_pf (w.cd_pessoa_fisica)|| ' - '|| SUBSTR (obter_conselho_profissional (w.nr_seq_conselho,'S'),1,20)|| ': '|| w.ds_codigo_prof nm_medico_ass,
                '' ds_exame_medico_p,
                c.cd_medico_solicitante,
                4 ie_tipo_p,
                a.cd_procedencia,
                obter_desc_procedencia(a.cd_procedencia) ds_procedencia,
                a.cd_unidade,
                a.ie_tipo_atendimento,
	c.nr_Sequencia,
	SUBSTR(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U'),1,15)nm_usuario_liberador,
	SUBSTR(obter_nome_usuario(obter_lab_execucao_etapa(c.nr_prescricao, c.nr_sequencia, '40', 'U')),1,60)nm_liberador
           FROM pessoa_fisica w,
                pessoa_fisica g,
                grupo_exame_lab e,
                exame_lab_material h,
                exame_laboratorio d,
                material_exame_lab f,
                prescr_medica b,
                exame_lab_resultado j,
                exame_lab_result_item k,
                prescr_procedimento c,
                atendimento_paciente_v a,
                setor_atendimento z
          WHERE a.nr_atendimento = b.nr_atendimento
            AND b.nr_prescricao = c.nr_prescricao
            AND c.nr_seq_exame = d.nr_seq_exame
            AND c.cd_material_exame = f.cd_material_exame
            AND c.cd_medico_solicitante = g.cd_pessoa_fisica
            AND c.nr_seq_exame = h.nr_seq_exame
            AND h.nr_seq_material = f.nr_sequencia
            AND d.nr_seq_grupo = e.nr_sequencia
            AND b.nr_prescricao = j.nr_prescricao
            AND j.nr_seq_resultado = k.nr_seq_resultado
            AND c.nr_sequencia = k.nr_seq_prescr
            AND w.cd_pessoa_fisica(+) = k.cd_medico_resp
            AND z.cd_setor_atendimento = b.cd_setor_atendimento
            AND c.cd_medico_solicitante IS NOT NULL
            AND c.ie_status_atend >=40
            AND k.nr_seq_formato IS NOT NULL
            ORDER BY cd_medico_resp
/