create or replace view bl_frasco_v
as
select	a.nr_sequencia,
	a.cd_frasco,
	a.dt_frasco,
	a.qt_volume,
	substr(Bl_obter_desc_status(a.nr_sequencia),1,250) ds_status,
	a.nr_seq_status_leite,
	c.cd_pessoa_fisica,
	substr(obter_nome_pf(c.cd_pessoa_fisica),1,250) nm_doadora,
	substr(BL_obter_dados_status(a.nr_seq_status_leite,'S'),1,10) ie_status,
	substr(Bl_obter_situacao_frasco(a.nr_sequencia),1,10) ie_situacao_frasco,
	a.nr_seq_motivo_inutil,
	Obter_Descricao_Padrao('BL_MOTIVO_INUTIL','DS_MOTIVO',a.nr_seq_motivo_inutil) ds_motivo_inutil,
	a.nm_usuario_inutil,
	a.ie_local_inutilizacao,
	a.dt_inutilizacao,
	b.ie_classif_leite,
	substr(obter_valor_dominio(5668,b.ie_classif_leite),1,255) ds_classif_leite,
	substr(BL_obter_se_em_processamento(a.nr_sequencia),1,1) ie_em_reprocessamento,
	b.dt_fim_coleta,
	a.dt_utilizacao,
	b.dt_coleta,
	a.nr_seq_saida,
	a.nr_seq_nut_prod,
	a.nr_seq_coleta,
	null nr_seq_agrupamento,
	a.nr_seq_origem,
	a.ie_situacao_processa,
	a.nr_seq_entrada,
	b.dt_inicio_coleta,
	a.ds_observacao
from	bl_frasco a,
	bl_coleta b,
	bl_doacao c
where	a.nr_seq_coleta = b.nr_sequencia
and	b.nr_seq_doacao = c.nr_sequencia
union
select	a.nr_sequencia,
	a.cd_frasco,
	a.dt_frasco,
	a.qt_volume,
	substr(Bl_obter_desc_status(a.nr_sequencia),1,250) ds_status,
	a.nr_seq_status_leite,
	b.cd_doadora cd_pessoa_fisica,
	substr(obter_nome_pf(b.cd_doadora),1,250) nm_doadora,
	substr(BL_obter_dados_status(a.nr_seq_status_leite,'S'),1,10) ie_status,
	substr(Bl_obter_situacao_frasco(a.nr_sequencia),1,10) ie_situacao_frasco,
	a.nr_seq_motivo_inutil,
	Obter_Descricao_Padrao('BL_MOTIVO_INUTIL','DS_MOTIVO',a.nr_seq_motivo_inutil) ds_motivo_inutil,
	a.nm_usuario_inutil,
	a.ie_local_inutilizacao,
	a.dt_inutilizacao,
	b.ie_classif_leite,
	substr(obter_valor_dominio(5668,b.ie_classif_leite),1,255) ds_classif_leite,
	substr(BL_obter_se_em_processamento(a.nr_sequencia),1,1) ie_em_reprocessamento,
	b.dt_liberacao dt_fim_coleta,
	a.dt_utilizacao,
	b.dt_agrupamento dt_coleta,
	a.nr_seq_saida,
	a.nr_seq_nut_prod,
	null nr_seq_coleta,
	a.nr_seq_agrupamento,
	a.nr_seq_origem,
	a.ie_situacao_processa,
	a.nr_seq_entrada,
	null dt_inicio_coleta,
	a.ds_observacao
from	bl_frasco a,
	bl_agrupamento b
where	a.nr_seq_agrupamento = b.nr_sequencia
union
select 	a.nr_sequencia,
	a.cd_frasco,
	a.dt_frasco,
	a.qt_volume,
	substr(Bl_obter_desc_status(a.nr_sequencia),1,250) ds_status,
	a.nr_seq_status_leite,
	a.cd_pessoa_fisica cd_doadora,
	case
		when a.cd_pessoa_fisica is not null THEN substr(obter_nome_pf(a.cd_pessoa_fisica),1,255)
		when a.nm_doadora is not null then a.nm_doadora
	else null
	end nm_doadora,
	substr(BL_obter_dados_status(a.nr_seq_status_leite,'S'),1,10) ie_status,
	substr(Bl_obter_situacao_frasco(a.nr_sequencia),1,10) ie_situacao_frasco,
	a.nr_seq_motivo_inutil,
	Obter_Descricao_Padrao('BL_MOTIVO_INUTIL','DS_MOTIVO',a.nr_seq_motivo_inutil) ds_motivo_inutil,
	a.nm_usuario_inutil,
	a.ie_local_inutilizacao,
	a.dt_inutilizacao,
	a.ie_classif_leite,
	substr(obter_valor_dominio(5668,a.ie_classif_leite),1,255) ds_classif_leite,
	substr(BL_obter_se_em_processamento(a.nr_sequencia),1,1) ie_em_reprocessamento,
	b.dt_fechamento dt_fim_coleta,
	a.dt_utilizacao,
	a.dt_coleta,
	a.nr_seq_saida,
	a.nr_seq_nut_prod,
	null nr_seq_coleta,
	a.nr_seq_agrupamento,
	a.nr_seq_origem,
	a.ie_situacao_processa,
	a.nr_seq_entrada,
	null dt_inicio_coleta,
	a.ds_observacao
from 	bl_frasco a,
	BL_ENTRADA b
where	a.nr_seq_entrada is not null
and 	a.NR_SEQ_ENTRADA = b.NR_SEQUENCIA
and 	b.DT_FECHAMENTO is not null;
/