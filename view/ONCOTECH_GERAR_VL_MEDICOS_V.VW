create or replace view ONCOTECH_GERAR_VL_MEDICOS_V
as
SELECT 	distinct                                                                                                                                                                                        
	'R' ie_data,                                                                                                                                                                                           
	a.cd_convenio,                                                                                                                                                                                         
	a.ie_status_retorno,                                                                                                                                                                                   
	c.dt_recebimento dt_referencia,                                                                                                                                                                        
	OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO) cd_setor_atendimento,                                                                                                                                        
	e.cd_medico_atendimento,                                                                                                                                                                               
	SUBSTR(obter_pessoa_conta(b.nr_interno_conta,'N'),1,60) nm_paciente,                                                                                                                                   
	obter_dados_protocolo(a.nr_seq_protocolo, 'DEN') dt_entrega,                                                                                                                                           
	c.dt_recebimento,                                                                                                                                                                                      
	SUBSTR(obter_dados_setor(OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO),'DS'),1,255)                                                                                                                        
 ds_setor,                                                                                                                                                                                              
	Obter_Dados_Atendimento(e.nr_atendimento,'DE') dt_atendimento,                                                                                                                                         
	SUBSTR(Obter_desc_convenio(a.cd_convenio),1,255) ds_convenio,                                                                                                                                          
	b.vl_pago,                                                                                                                                                                                             
	d.vl_conta,                                                                                                                                                                                            
	SUBSTR(OBTER_SENHA_ATENDIMENTO(e.nr_atendimento),1,20) cd_senha,                                                                                                                                       
	SUBSTR(OBTER_GUIA_CONVENIO(e.nr_atendimento),1,20) nr_doc_convenio,                                                                                                                                    
	SUBSTR(obter_nome_medico(e.cd_medico_resp,'N'),1,255) nm_medico,  
	e.cd_pessoa_fisica
FROM   	convenio_ret_receb x,                                                                                                                                                                           
	convenio_receb c,                                                                                                                                                                                      
	atendimento_paciente e,                                                                                                                                                                                
	conta_paciente d,                                                                                                                                                                                      
	convenio_retorno a,                                                                                                                                                                                    
	convenio_retorno_item b                                                                                                                                                                                
WHERE  	b.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                              
AND	b.nr_interno_conta 	= d.nr_interno_conta                                                                                                                                                            
AND	d.nr_atendimento 	= e.nr_atendimento                                                                                                                                                                
AND	x.nr_seq_receb 		= c.nr_sequencia                                                                                                                                                                   
AND	x.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                                  
UNION                                                                                                                                                                                                   
SELECT	distinct                                                                                                                                                                                         
	'A' ie_data,                                                                                                                                                                                           
	a.cd_convenio,                                                                                                                                                                                         
	a.ie_status_retorno,                                                                                                                                                                                   
	TO_DATE(Obter_Dados_Atendimento(e.nr_atendimento,'DE'),'dd/mm/yyyy hh24:mi:ss')                                                                                                                        
 dt_referencia,                                                                                                                                                                                         
	OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO) cd_setor_atendimento,                                                                                                                                        
	e.cd_medico_atendimento,                                                                                                                                                                               
	SUBSTR(obter_pessoa_conta(b.nr_interno_conta,'N'),1,60) nm_paciente,                                                                                                                                   
	obter_dados_protocolo(a.nr_seq_protocolo, 'DEN') dt_entrega,                                                                                                                                           
	c.dt_recebimento,                                                                                                                                                                                      
	SUBSTR(obter_dados_setor(OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO),'DS'),1,255)                                                                                                                        
 ds_setor,                                                                                                                                                                                              
	Obter_Dados_Atendimento(e.nr_atendimento,'DE') dt_atendimento,                                                                                                                                         
	SUBSTR(Obter_desc_convenio(a.cd_convenio),1,255) ds_convenio,                                                                                                                                          
	b.vl_pago,                                                                                                                                                                                             
	d.vl_conta,                                                                                                                                                                                            
	SUBSTR(OBTER_SENHA_ATENDIMENTO(e.nr_atendimento),1,20) cd_senha,                                                                                                                                       
	SUBSTR(OBTER_GUIA_CONVENIO(e.nr_atendimento),1,20) nr_doc_convenio,                                                                                                                                    
	SUBSTR(obter_nome_medico(e.cd_medico_resp,'n'),1,255) nm_medico,
	e.cd_pessoa_fisica
FROM 	convenio_ret_receb x,                                                                                                                                                                             
	convenio_receb c,                                                                                                                                                                                      
	convenio_retorno a,                                                                                                                                                                                    
	conta_paciente d,                                                                                                                                                                                      
	atendimento_paciente e,                                                                                                                                                                                
	convenio_retorno_item b                                                                                                                                                                                
WHERE 	b.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                               
AND	b.nr_interno_conta 	= d.nr_interno_conta                                                                                                                                                            
AND	d.nr_atendimento 	= e.nr_atendimento                                                                                                                                                                
AND	x.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                                  
AND	x.nr_seq_receb 		= c.nr_sequencia                                                                                                                                                                   
UNION                                                                                                                                                                                                   
SELECT	distinct                                                                                                                                                                                         
	'E' ie_data,
	a.cd_convenio,
	a.ie_status_retorno,                                                                                                                                                                                   
	to_date(obter_dados_protocolo(a.nr_seq_protocolo,'DEN'),'dd/mm/yyyy') dt_referencia,
	OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO) cd_setor_atendimento,                                                                                                                                        
	e.cd_medico_atendimento,                                                                                                                                                                               
	SUBSTR(obter_pessoa_conta(b.nr_interno_conta,'N'),1,60) nm_paciente,                                                                                                                                   
	obter_dados_protocolo(a.nr_seq_protocolo,'DEN') dt_entrega,                                                                                                                                            
	c.dt_recebimento,                                                                                                                                                                                      
	SUBSTR(obter_dados_setor(OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO),'DS'),1,255) ds_setor,                                                                                                                                                                                              
	Obter_Dados_Atendimento(e.nr_atendimento,'DE') dt_atendimento,                                                                                                                                         
	SUBSTR(Obter_desc_convenio(a.cd_convenio),1,255) ds_convenio,                                                                                                                                          
	b.vl_pago,                                                                                                                                                                                             
	d.vl_conta,                                                                                                                                                                                            
	SUBSTR(OBTER_SENHA_ATENDIMENTO(e.nr_atendimento),1,20) cd_senha,                                                                                                                                       
	SUBSTR(OBTER_GUIA_CONVENIO(e.nr_atendimento),1,20) nr_doc_convenio,                                                                                                                                    
	SUBSTR(obter_nome_medico(e.cd_medico_resp,'n'),1,255) nm_medico,
	e.cd_pessoa_fisica
FROM 	convenio_ret_receb x,                                                                                                                                                                             
	convenio_receb c,                                                                                                                                                                                      
	convenio_retorno a,                                                                                                                                                                                    
	conta_paciente d,                                                                                                                                                                                      
	atendimento_paciente e,                                                                                                                                                                                
	convenio_retorno_item b                                                                                                                                                                                
WHERE 	b.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                               
AND	b.nr_interno_conta 	= d.nr_interno_conta                                                                                                                                                            
AND	d.nr_atendimento 	= e.nr_atendimento                                                                                                                                                                
AND	x.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                                  
AND	x.nr_seq_receb 		= c.nr_sequencia                                                                                                                                                                   
UNION ALL                                                                                                                                                                                               
SELECT 	distinct                                                                                                                                                                                        
	'RT' ie_data,                                                                                                                                                                                          
	a.cd_convenio,                                                                                                                                                                                         
	a.ie_status_retorno,                                                                                                                                                                                   
	a.dt_retorno,                                                                                                                                                                                          
	OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO) cd_setor_atendimento,                                                                                                                                        
	e.cd_medico_atendimento,                                                                                                                                                                               
	SUBSTR(obter_pessoa_conta(b.nr_interno_conta,'N'),1,60) nm_paciente,                                                                                                                                   
	obter_dados_protocolo(a.nr_seq_protocolo,'DEN') dt_entrega,                                                                                                                                            
	c.dt_recebimento,                                                                                                                                                                                      
	SUBSTR(obter_dados_setor(OBTER_SETOR_ATENDIMENTO(E.NR_ATENDIMENTO),'DS'),1,255) ds_setor,                                                                                                                                                                                              
	Obter_Dados_Atendimento(e.nr_atendimento,'DE') dt_atendimento,                                                                                                                                         
	SUBSTR(Obter_desc_convenio(a.cd_convenio),1,255) ds_convenio,                                                                                                                                          
	b.vl_pago,                                                                                                                                                                                             
	d.vl_conta,                                                                                                                                                                                            
	SUBSTR(OBTER_SENHA_ATENDIMENTO(e.nr_atendimento),1,20) cd_senha,                                                                                                                                       
	SUBSTR(OBTER_GUIA_CONVENIO(e.nr_atendimento),1,20) nr_doc_convenio,                                                                                                                                    
	SUBSTR(obter_nome_medico(e.cd_medico_resp,'n'),1,255) nm_medico,
	e.cd_pessoa_fisica
FROM   	convenio_ret_receb x,                                                                                                                                                                           
	convenio_receb c,                                                                                                                                                                                      
	convenio_retorno a,                                                                                                                                                                                    
	conta_paciente d,                                                                                                                                                                                      
	atendimento_paciente e,                                                                                                                                                                                
	convenio_retorno_item b                                                                                                                                                                                
WHERE  	b.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                              
AND	b.nr_interno_conta 	= d.nr_interno_conta                                                                                                                                                            
AND	d.nr_atendimento 	= e.nr_atendimento                                                                                                                                                                
AND	x.nr_seq_retorno 	= a.nr_sequencia                                                                                                                                                                  
AND	x.nr_seq_receb 		= c.nr_sequencia                                                                                                                                                                   
ORDER BY 1                                 
/