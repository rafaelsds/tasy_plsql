create or replace view w_benef_validation_v as
select  NR_SEQUENCIA,
        CD_WEBID,
        CD_BENEF_ID,
        CD_DIAGNOSTICO,
        CD_SERVICE,
        CD_PROFESSIONAL_PROVIDER,
        CD_PROVIDER,
        CD_EMERGENCY,
        CD_SPECIALTY,
        CD_REFER,
        CD_CONTRACT,
        CD_IDENTIFICATION,
        CD_MEMBER,
        CD_FAMILY_ID,
        IE_SEND_TYPE  
from  W_AUT_SERV_TRANSACTION;
/
