create or replace 
view log_int_sup_consitencia_v as
select	ds_inconsistencia ds_consistencia,
	'M' ie_tipo_documento,
	0 nr_documento,
	nr_seq_movto_int nr_seq_superior
from	w_inconsistencia_int_movto
union all
select	ds_inconsistencia,
	ie_tipo_documento ,
	nr_documento,
	0 nr_seq_superior
from	w_sup_inconsistencia_int;
/