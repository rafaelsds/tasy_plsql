CREATE OR REPLACE VIEW eis_evol_paciente_v 
AS
select a.CD_EVOLUCAO,
  trunc(a.DT_EVOLUCAO) dt_evolucao,
  a.ie_tipo_evolucao,
  substr(obter_desc_tipo_evolucao(a.IE_TIPO_EVOLUCAO),1,255) ds_tipo_evolucao,
  a.ie_evolucao_clinica,
	SUBSTR(obter_desc_tipo_evolucao(a.ie_evolucao_clinica),1,255) ds_tipo_evolucao_clinica,
  substr(obter_nome_pf(a.cd_medico),1,255) nm_profissional,
  a.NR_ATENDIMENTO,
  a.DT_LIBERACAO,
 a.CD_SETOR_ATENDIMENTO cd_setor_atendimento,
  a.IE_SITUACAO,
  a.cd_medico,
  a.CD_ESPECIALIDADE_MEDICO cd_especialidade_medico,
  substr(obter_desc_espec_medica(CD_ESPECIALIDADE_MEDICO),1,50) ds_especialidade_medica,
  obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
  obter_desc_convenio(obter_convenio_atendimento(a.nr_atendimento)) ds_convenio,
  obter_nome_setor(a.cd_setor_atendimento) ds_setor_atendimento,
  a.cd_pessoa_fisica,
  substr(obter_nome_pf(a.cd_pessoa_fisica),1,80) nm_pessoa_fisica,
  to_number(obter_estab_atendimento(a.nr_atendimento)) cd_estabelecimento
from eis_evolucao_profissional a;
/