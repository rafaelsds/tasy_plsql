CREATE OR REPLACE VIEW execucao_final_v
AS
select	5 nr_consistencia,
	a.nr_sequencia,
	substr(obter_valor_dominio(4958,5),1,255)ds_consistencia
from	proj_projeto a
where	exists (    select 	1
		    from	proj_ata x
		    where	a.nr_sequencia 	    	= x.nr_seq_projeto
		    and		x.nr_seq_classif	= 4);
/		    