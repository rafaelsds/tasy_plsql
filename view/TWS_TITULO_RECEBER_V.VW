create or replace view tws_titulo_receber_v as 
select 	nr_titulo,
	dt_atualizacao,
	ie_situacao,
	dt_vencimento,
	nr_seq_mensalidade,
	dt_pagamento_previsto,
	vl_saldo_titulo,
	vl_titulo,
	dt_emissao,
	ie_pls 
from 	titulo_receber;
/