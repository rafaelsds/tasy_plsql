Create or Replace view pls_conta_proc_imp_v as
select	--  Retorna procedimentos, importados ou integrados, usa union por quest�es de performance e organiza��o
	nr_seq_conta,
	nr_sequencia,
	cd_guia_referencia,
	qt_ok,
	nr_nota_cobranca,
	pls_obter_nr_fatura(nr_seq_fatura) nr_fatura,
	cd_congenere,
	ie_tipo_conta,
	dt_procedimento,
	to_char(nr_seq_prestador_exec) nr_seq_prestador_exec,
	nr_seq_segurado,
	cd_medico_executor,
	ie_origem_proced,
	cd_procedimento,
	nr_seq_proc_ref,
	nr_seq_participante_hi
from	pls_conta_proc_v
where	ie_status <> 'D';
/