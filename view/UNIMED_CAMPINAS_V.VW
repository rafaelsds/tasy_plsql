Create or replace View Unimed_campinas_v as
Select substr(Obter_Valor_Conv_Estab(b.cd_convenio_parametro,b.cd_estabelecimento,'CD_REGIONAL'),1,15) cd_executante,		 
	b.cd_convenio_parametro cd_convenio,
	decode(a.ie_tipo_atendimento,1,'21','22') tp_local,
	a.nr_atendimento,
	b.nr_interno_conta,
	'62' ie_emite_conta
from 	atendimento_paciente a,
	conta_paciente b
where 	a.nr_atendimento = b.nr_atendimento;
/