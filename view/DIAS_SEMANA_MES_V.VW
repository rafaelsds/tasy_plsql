Create or replace View dias_semana_mes_v as
select	substr(Obter_Dia_Semana(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia1,		
		substr(Obter_Dia_Semana(to_date('02/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia2,
		substr(Obter_Dia_Semana(to_date('03/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia3,
		substr(Obter_Dia_Semana(to_date('04/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia4,
		substr(Obter_Dia_Semana(to_date('05/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia5,
		substr(Obter_Dia_Semana(to_date('06/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia6,
		substr(Obter_Dia_Semana(to_date('07/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia7,
		substr(Obter_Dia_Semana(to_date('08/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia8,
		substr(Obter_Dia_Semana(to_date('09/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia9,
		substr(Obter_Dia_Semana(to_date('10/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia10,
		substr(Obter_Dia_Semana(to_date('11/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia11,
		substr(Obter_Dia_Semana(to_date('12/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia12,
		substr(Obter_Dia_Semana(to_date('13/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia13,
		substr(Obter_Dia_Semana(to_date('14/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia14,
		substr(Obter_Dia_Semana(to_date('15/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia15,
		substr(Obter_Dia_Semana(to_date('16/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia16,
		substr(Obter_Dia_Semana(to_date('17/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia17,
		substr(Obter_Dia_Semana(to_date('18/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia18,
		substr(Obter_Dia_Semana(to_date('19/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia19,
		substr(Obter_Dia_Semana(to_date('20/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia20,
		substr(Obter_Dia_Semana(to_date('21/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia21,
		substr(Obter_Dia_Semana(to_date('22/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia22,
		substr(Obter_Dia_Semana(to_date('23/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia23,
		substr(Obter_Dia_Semana(to_date('24/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia24,
		substr(Obter_Dia_Semana(to_date('25/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia25,
		substr(Obter_Dia_Semana(to_date('26/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia26,
		substr(Obter_Dia_Semana(to_date('27/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia27,
		substr(Obter_Dia_Semana(to_date('28/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia28,
		substr(Obter_Dia_Semana(to_date('29/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia29,
		substr(Obter_Dia_Semana(to_date('30/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia30,
		substr(Obter_Dia_Semana(to_date('31/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia31,
		31 qt_dia,
		to_char(d.dt_inicio,'mm') dt_inicio,
		to_char(d.dt_fim,'yyyy') dt_fim,
		e.nr_sequencia nr_seq_escala
from	escala_diaria d,
		escala_grupo g,
		escala_classif c,
		escala	e
where	c.nr_sequencia = g.nr_seq_classif
and		g.nr_sequencia = e.nr_seq_grupo
and		e.nr_sequencia = d.nr_seq_escala
and     d.cd_pessoa_fisica is not null
and     nvl(obter_tipo_classif_escala(e.nr_sequencia),'P') = 'P'
and		to_number (obter_qt_dias_mes(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy'))) = 31
union
select	substr(Obter_Dia_Semana(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia1,		
		substr(Obter_Dia_Semana(to_date('02/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia2,
		substr(Obter_Dia_Semana(to_date('03/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia3,
		substr(Obter_Dia_Semana(to_date('04/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia4,
		substr(Obter_Dia_Semana(to_date('05/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia5,
		substr(Obter_Dia_Semana(to_date('06/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia6,
		substr(Obter_Dia_Semana(to_date('07/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia7,
		substr(Obter_Dia_Semana(to_date('08/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia8,
		substr(Obter_Dia_Semana(to_date('09/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia9,
		substr(Obter_Dia_Semana(to_date('10/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia10,
		substr(Obter_Dia_Semana(to_date('11/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia11,
		substr(Obter_Dia_Semana(to_date('12/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia12,
		substr(Obter_Dia_Semana(to_date('13/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia13,
		substr(Obter_Dia_Semana(to_date('14/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia14,
		substr(Obter_Dia_Semana(to_date('15/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia15,
		substr(Obter_Dia_Semana(to_date('16/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia16,
		substr(Obter_Dia_Semana(to_date('17/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia17,
		substr(Obter_Dia_Semana(to_date('18/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia18,
		substr(Obter_Dia_Semana(to_date('19/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia19,
		substr(Obter_Dia_Semana(to_date('20/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia20,
		substr(Obter_Dia_Semana(to_date('21/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia21,
		substr(Obter_Dia_Semana(to_date('22/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia22,
		substr(Obter_Dia_Semana(to_date('23/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia23,
		substr(Obter_Dia_Semana(to_date('24/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia24,
		substr(Obter_Dia_Semana(to_date('25/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia25,
		substr(Obter_Dia_Semana(to_date('26/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia26,
		substr(Obter_Dia_Semana(to_date('27/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia27,
		substr(Obter_Dia_Semana(to_date('28/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia28,
		substr(Obter_Dia_Semana(to_date('29/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia29,
		substr(Obter_Dia_Semana(to_date('30/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia30,
		'' dia31,
		30 qt_dia,
		to_char(d.dt_inicio,'mm') dt_inicio,
		to_char(d.dt_fim,'yyyy') dt_fim,
		e.nr_sequencia nr_seq_escala
from	escala_diaria d,
		escala_grupo g,
		escala_classif c,
		escala	e
where	c.nr_sequencia = g.nr_seq_classif
and		g.nr_sequencia = e.nr_seq_grupo
and		e.nr_sequencia = d.nr_seq_escala
and     d.cd_pessoa_fisica is not null
and     nvl(obter_tipo_classif_escala(e.nr_sequencia),'P') = 'P'
and		to_number (obter_qt_dias_mes(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy'))) = 30
union
select	substr(Obter_Dia_Semana(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia1,		
		substr(Obter_Dia_Semana(to_date('02/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia2,
		substr(Obter_Dia_Semana(to_date('03/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia3,
		substr(Obter_Dia_Semana(to_date('04/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia4,
		substr(Obter_Dia_Semana(to_date('05/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia5,
		substr(Obter_Dia_Semana(to_date('06/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia6,
		substr(Obter_Dia_Semana(to_date('07/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia7,
		substr(Obter_Dia_Semana(to_date('08/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia8,
		substr(Obter_Dia_Semana(to_date('09/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia9,
		substr(Obter_Dia_Semana(to_date('10/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia10,
		substr(Obter_Dia_Semana(to_date('11/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia11,
		substr(Obter_Dia_Semana(to_date('12/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia12,
		substr(Obter_Dia_Semana(to_date('13/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia13,
		substr(Obter_Dia_Semana(to_date('14/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia14,
		substr(Obter_Dia_Semana(to_date('15/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia15,
		substr(Obter_Dia_Semana(to_date('16/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia16,
		substr(Obter_Dia_Semana(to_date('17/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia17,
		substr(Obter_Dia_Semana(to_date('18/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia18,
		substr(Obter_Dia_Semana(to_date('19/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia19,
		substr(Obter_Dia_Semana(to_date('20/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia20,
		substr(Obter_Dia_Semana(to_date('21/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia21,
		substr(Obter_Dia_Semana(to_date('22/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia22,
		substr(Obter_Dia_Semana(to_date('23/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia23,
		substr(Obter_Dia_Semana(to_date('24/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia24,
		substr(Obter_Dia_Semana(to_date('25/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia25,
		substr(Obter_Dia_Semana(to_date('26/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia26,
		substr(Obter_Dia_Semana(to_date('27/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia27,
		substr(Obter_Dia_Semana(to_date('28/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia28,
		substr(Obter_Dia_Semana(to_date('29/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia29,
		'' dia30,
		'' dia31,
		29 qt_dia,
		to_char(d.dt_inicio,'mm') dt_inicio,
		to_char(d.dt_fim,'yyyy') dt_fim,
		e.nr_sequencia nr_seq_escala
from	escala_diaria d,
		escala_grupo g,
		escala_classif c,
		escala	e
where	c.nr_sequencia = g.nr_seq_classif
and		g.nr_sequencia = e.nr_seq_grupo
and		e.nr_sequencia = d.nr_seq_escala
and     d.cd_pessoa_fisica is not null
and     nvl(obter_tipo_classif_escala(e.nr_sequencia),'P') = 'P'
and		to_number (obter_qt_dias_mes(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy'))) = 29
union
select	substr(Obter_Dia_Semana(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia1,		
		substr(Obter_Dia_Semana(to_date('02/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia2,
		substr(Obter_Dia_Semana(to_date('03/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia3,
		substr(Obter_Dia_Semana(to_date('04/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia4,
		substr(Obter_Dia_Semana(to_date('05/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia5,
		substr(Obter_Dia_Semana(to_date('06/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia6,
		substr(Obter_Dia_Semana(to_date('07/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia7,
		substr(Obter_Dia_Semana(to_date('08/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia8,
		substr(Obter_Dia_Semana(to_date('09/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia9,
		substr(Obter_Dia_Semana(to_date('10/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia10,
		substr(Obter_Dia_Semana(to_date('11/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia11,
		substr(Obter_Dia_Semana(to_date('12/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia12,
		substr(Obter_Dia_Semana(to_date('13/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia13,
		substr(Obter_Dia_Semana(to_date('14/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia14,
		substr(Obter_Dia_Semana(to_date('15/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia15,
		substr(Obter_Dia_Semana(to_date('16/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia16,
		substr(Obter_Dia_Semana(to_date('17/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)  dia17,
		substr(Obter_Dia_Semana(to_date('18/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia18,
		substr(Obter_Dia_Semana(to_date('19/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia19,
		substr(Obter_Dia_Semana(to_date('20/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia20,
		substr(Obter_Dia_Semana(to_date('21/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia21,
		substr(Obter_Dia_Semana(to_date('22/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia22,
		substr(Obter_Dia_Semana(to_date('23/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia23,
		substr(Obter_Dia_Semana(to_date('24/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia24,
		substr(Obter_Dia_Semana(to_date('25/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1)dia25,
		substr(Obter_Dia_Semana(to_date('26/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia26,
		substr(Obter_Dia_Semana(to_date('27/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia27,
		substr(Obter_Dia_Semana(to_date('28/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy')),1,1) dia28,
		'' dia29,
		'' dia30,
		'' dia31,
		28 qt_dia,
		to_char(d.dt_inicio,'mm') dt_inicio,
		to_char(d.dt_fim,'yyyy') dt_fim,
		e.nr_sequencia nr_seq_escala
from	escala_diaria d,
		escala_grupo g,
		escala_classif c,
		escala	e
where	c.nr_sequencia = g.nr_seq_classif
and		g.nr_sequencia = e.nr_seq_grupo
and		e.nr_sequencia = d.nr_seq_escala
and     d.cd_pessoa_fisica is not null
and     nvl(obter_tipo_classif_escala(e.nr_sequencia),'P') = 'P'
and 	to_number (obter_qt_dias_mes(to_date('01/'||to_char(d.dt_inicio,'mm')||'/'||to_char(d.dt_fim,'yyyy'),'dd/mm/yyyy'))) = 28;
/
