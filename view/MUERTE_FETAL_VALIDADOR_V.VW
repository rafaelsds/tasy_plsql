CREATE OR REPLACE VIEW MUERTE_FETAL_VALIDADOR_V AS
SELECT	b.dt_obito, a.nr_atendimento NR_ATENDIMENTO_FILHO,
	a.nr_atendimento_mae NR_ATENDIMENTO_MAE,--901257
	b.cd_cid_direta FETCAUSACOM,
	NVL(b.cd_cid_basica,b.cd_cid_direta) FETCAUSA,
	RPAD(NVL(UPPER(obter_desc_cid(NVL(b.cd_cid_basica,b.cd_cid_direta))),'NE'),100) FETCAUSA_DESC,
	NVL(b.ie_tipo_mort_fetal,'0') FETTIPCAUSA,
	b.cd_cid_adic_1 FETCAUSAB,
	RPAD(NVL(UPPER(obter_desc_cid(b.cd_cid_adic_1)),'NE'),100) FETCAUSAB_DESC,
	b.cd_cid_adic_2 FETCAUSAC,
	RPAD(NVL(UPPER(obter_desc_cid(b.cd_cid_adic_2)),'NE'),100) FETCAUSAC_DESC,
	b.cd_cid_adic_3 FETCAUSAD,
	RPAD(NVL(UPPER(obter_desc_cid(b.cd_cid_adic_3)),'NE'),100) FETCAUSAD_DESC,
	b.cd_cid_basica FETCAUSAE,
	RPAD(NVL(UPPER(obter_desc_cid(b.cd_cid_basica)),'NE'),100) FETCAUSAE_DESC,
	b.cd_cid_adic_4 FETCAUSAF,
	RPAD(NVL(UPPER(obter_desc_cid(b.cd_cid_adic_4)),'NE'),100) FETCAUSAF_DESC,
	NVL(b.nm_usuario,'TASY') USUAR_CRE,
	NVL(b.dt_atualizacao,SYSDATE) FETFEC_CRE,
	NVL(b.nm_usuario,'TASY') USUAR_MOD,
	NVL(b.dt_atualizacao,SYSDATE) FETFEC_MOD,
	NVL(b.ie_tipo_certif_obt,'8') IDTIPOHOJACERTIFICADO,
	LPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'CD_ESTADO'),'99'),2,'0') EDO,--903851	
	cpf.nm_contato CERNOMBINFOR,
	RPAD(NVL(y.ds_component_name_1,'NO ESPECIFICADO'),30) PRO_APECERTINF,
	RPAD(NVL(y.ds_family_name,'NO ESPECIFICADO'),30) PRO_APEMCERTINF,
	LPAD((SELECT x.cd_grau_parentesco_mx FROM cat_grau_parentesco x WHERE x.nr_sequencia = (SELECT p.cd_parentesco_mx FROM grau_parentesco p WHERE p.nr_sequencia = cpf.nr_seq_parentesco)),2,'0') DEFPARENTINFOR,
	NVL(obter_dados_cat_cert_parto(b.ie_emissor,'CD_CERT_PARTO_MF'),'8') CERCERTIFI,
	RPAD(NVL(medico.ds_codigo_prof,'0000000'),15) CERCEDCERTIFI,
	RPAD(NVL(w.ds_given_name,'NO ESPECIFICADO'),30) CERNOMBCERTIFI,
	RPAD(NVL(w.ds_family_name,'NO ESPECIFICADO'),30) PRO_APECERTIFI,
	RPAD(NVL(w.ds_component_name_1,'NO ESPECIFICADO'),30) PRO_APEMCERTIFI,
	nvl(UPPER(Elimina_Acentuacao(nvl(get_info_end_endereco(medico_compl.nr_seq_pessoa_endereco,'RUA_VIALIDADE','D') || ' - ' || nvl(get_info_end_endereco(medico_compl.nr_seq_pessoa_endereco,'NUMERO','D'),get_info_end_endereco(medico_compl.nr_seq_pessoa_endereco,'COMPLEMENTO','D')), medico_compl.ds_endereco ||' - ' || NVL(medico_compl.nr_endereco, medico_compl.ds_compl_end)))),'NO ESPECIFICADO') CERDOMCERTIFI,
	nvl(UPPER(Elimina_Acentuacao(nvl(get_info_end_endereco(medico_compl.nr_seq_pessoa_endereco,'BAIRRO_VILA','D'), medico_compl.DS_BAIRRO))),'NO ESPECIFICADO') PRO_CERTCOL,
	nvl(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'LOCALIDADE','C'), obter_dados_cat_localidade(cpf.nr_seq_localizacao_mx,'CD_CAT_LOCALIDADE')),'9999') PRO_CERTLOC,
	nvl(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'MUNICIPIO','C'), obter_dados_cat_localidade(cpf.nr_seq_localizacao_mx,'CD_CAT_MUNICIPIO')),'999') PRO_CERTMUN,
	nvl(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'ESTADO_PROVINCI','C'), obter_dados_cat_localidade(cpf.nr_seq_localizacao_mx,'CD_EFE')),'99') PRO_CERTENT,
	medico_compl.nr_telefone PRO_CERTTEL,
	DECODE(b.dt_liberacao,NULL,'2','1') PRO_CERTFIRM,
	NVL(TO_CHAR(b.dt_liberacao,'dd'),'99') CERDIA_DEF,
	NVL(TO_CHAR(b.dt_liberacao,'mm'),'99') CERMES_DEF,
	NVL(TO_CHAR(b.dt_liberacao,'yyyy'),'9999') CERANO_DEF,--900083
	nasc.nr_dfm FETFOLIO,
	DECODE(NVL(nasc.ie_sexo,'X'),'F','2','M','1','0') PRODSEXO,
	LPAD(NVL(NVL(obter_idade_gest_parto(nasc.nr_atendimento),nasc.qt_sem_ig_total),'99'),2,'0') PRODEDAD,
	RPAD(NVL(nasc.qt_peso_sala_parto,'9999'),4) PRODPESO,
	DECODE(NVL(par.qt_feto,'1'),'1','1','2','2','3') PRODCANTIDAD,
	DECODE(NVL(par.ie_pre_natal,'N'),'S','1','2') PRODATEPREN,
	LPAD(NVL(par.qt_consultas,'99'),2,'0') PRO_CONRECIB,
	DECODE(NVL(NVL(ag.ie_risco_gravidez,par.ie_risco_gestacao),'X'),'N','1','B','1','2') PRODTIPEMB,
	nasc.ie_tipo_nascimento PRODEFFET,
	DECODE(NVL(ec.qt_textura_pele,'99'),'0','1','5','1','10','1','2') PRO_ESTPIEL,
	TO_CHAR(NVL(nasc.dt_nascimento,SYSDATE),'dd') PRODDIA,
	TO_CHAR(NVL(nasc.dt_nascimento,SYSDATE),'mm') PRODMES,
	TO_CHAR(NVL(nasc.dt_nascimento,SYSDATE),'yyyy') PRODANIO,
	TO_CHAR(NVL(nasc.dt_nascimento,SYSDATE),'HH24') PRODHORA,
	TO_CHAR(NVL(nasc.dt_nascimento,SYSDATE),'MI') PRODMIN,
	RPAD(UPPER(Elimina_Acentuacao(NVL(d.ds_endereco||' - '||d.nr_endereco,'F8'))),50) PRODDOM_OCUR,
	RPAD(UPPER(Elimina_Acentuacao(NVL(d.ds_bairro,'F8'))),50) PRO_DOMCOL,
	LPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'CD_LOCALIDADE'),'9999'),4,'0') PRODLOC_OCUR,
	LPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'CD_MUNICIPIO'),'999'),3,'0') PRODMPO_OCUR,
	LPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'CD_ESTADO'),'99'),2,'0') PRODENT_OCUR,
	NVL(d.ie_tipo_inst_saude,'99') PRODSITIO,
	RPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'DS_ESTAB_SAUDE'),'NE'),150) FETHOSPIT,
	RPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'CD_CLUES'),'9999'),11) PRO_CLUES,
	NVL(obter_dados_cat_prof_parto(par.nr_seq_prof_parto,'CD_ATEND_PARTO_MF'),'9') PRODATENMED,
	hsm.qt_abortos_esp,
	hsm.qt_abortos_prov,
	hsm.qt_abortos_terap,
	DECODE((SELECT COUNT(1) FROM PARTO_EVENTOS z WHERE z.nr_atendimento = par.nr_atendimento),'0','1','2') PRODTIPPART,  
	par.ie_parto_normal,
	par.ie_parto_cesaria,
	par.ie_parto_forceps,	
	RPAD(Elimina_Acentuacao(nasc.ds_observacao),50) PROD_OTROEXP,
	DECODE(hsm.ie_parentesco_agressor,NULL,'2','1') PRO_CAUVIOLE,
	NVL(hsm.ie_parentesco_agressor,'88') PRO_PARENAGRE,--901222
	RPAD(NVL(z.ds_given_name,'SE IGNORA'),40) MADNOMBRE,
	RPAD(NVL(z.ds_family_name,'SE IGNORA'),20) MADAPEPATER,
	RPAD(NVL(z.ds_component_name_1,'SE IGNORA'),20) MADAPEMATER,
	RPAD(NVL(mae.cd_curp,'NO ESPECIFICADO'),18) PRO_CURPMADRE,
	DECODE((SELECT NVL(x.ie_brasileiro,'N') FROM nacionalidade x WHERE x.cd_nacionalidade = mae.cd_nacionalidade),'S','1','2') PRO_NACMADRE,
	DECODE(mae.nr_seq_lingua_indigena,NULL,'2','1') PRO_LINDMADRE,
	LPAD(obter_idade(mae.dt_nascimento,SYSDATE,'A'),2,'0') MADEDAD,
	DECODE(NVL(mae.ie_estado_civil,'9'),'1','1','2','5','3','3','4','6','5','2','6','6','7','4','9') MADEDO_CIV,  
	cpfm.nr_endereco MADDOM_HAB,
	cpfm.ds_bairro PRO_MADCOL,
	RPAD(NVL(obter_dados_cat_localidade(cpfm.nr_seq_localizacao_mx,'CD_CAT_LOCALIDADE'),'9999'),4) MADLOC_HAB,
	RPAD(NVL(obter_dados_cat_localidade(cpfm.nr_seq_localizacao_mx,'CD_CAT_MUNICIPIO'),'999'),3) MADDMPO_HAB,
	RPAD(NVL(obter_dados_cat_localidade(cpfm.nr_seq_localizacao_mx,'CD_EFE'),'99'),2) MADENT_HAB,
	RPAD(NVL((SELECT NVL(w.cd_externo,'99') FROM cargo w WHERE w.cd_cargo = mae.cd_cargo),'0'),2) MADOCUPACI,
	DECODE(mae.cd_cargo,NULL,'9','1') PRO_TRABACT,
	LPAD(NVL(par.qt_filhos_vivos,'00'),2,'0') MADNACVIVOS,
	LPAD(NVL(par.qt_natimortos,'00'),2,'0') MADNACMUERTOS,
	NVL(DECODE(mae.ie_grau_instrucao,'1','01','2','02','3','05','4','06','5','10','6','10','7','02','8','04','9','11','10','12','11','01','12','10','13','10','14','00','15','07','99'),'99') MADESCOLAR,
	RPAD(NVL(obter_dados_cat_derecho(co.cd_tipo_convenio_mx,'CD_DER_FETAL'),'99'),2) MADDERECHO,
	RPAD(NVL(mae.nr_spss,'000000000'),18) MADAFILIAC,
	DECODE(dom.nr_declaracao,NULL,'1','2') MADCONDIC,
	NVL(dom.nr_declaracao,'000000000') MADCERDEF,
	d.cd_cgc CD_PESSOA_JURIDICA,
	cpf.CD_PESSOA_FISICA CD_PESSOA_FISICA,
	medico.CD_PESSOA_FISICA CD_PESSOA_FISICA_MEDICO,
	mae.CD_PESSOA_FISICA cd_pessoa_fisica_mae,
	OBTER_DESC_CONVENIO(CO.CD_CONVENIO) CD_CGC_CONVENIO
FROM	atendimento_paciente a,
	atendimento_paciente am,
	declaracao_obito b,
	declaracao_obito dom,
	estabelecimento c,
	pessoa_juridica d,
	pessoa_fisica pf,
	compl_pessoa_fisica cpf,
	pessoa_fisica medico,
	person_name Y,
	compl_pessoa_fisica medico_compl,
	person_name W,
	nascimento nasc,
	parto par,
	atendimento_gravidez ag,
	escala_capurro ec,
	pessoa_fisica mae,
	compl_pessoa_fisica cpfm,
	historico_saude_mulher hsm,
	atend_categoria_convenio acc,
	convenio co,
	person_name  Z
WHERE	a.nr_atendimento_mae 	IS NOT NULL
AND	a.nr_atendimento	= b.nr_atendimento
AND	b.ie_situacao		= 'A'
AND	a.cd_estabelecimento	= c.cd_estabelecimento
AND	c.cd_cgc 		= d.cd_cgc
AND	a.cd_pessoa_fisica		= pf.cd_pessoa_fisica
AND	pf.cd_pessoa_fisica		= cpf.cd_pessoa_fisica(+)
AND 	cpf.ie_tipo_complemento 		= 3
AND 	pf.nr_seq_person_name		= y.nr_sequencia(+)
AND	b.cd_medico			= medico.cd_pessoa_fisica
AND 	medico.cd_pessoa_fisica 	= medico_compl.cd_pessoa_fisica(+)
AND 	medico_compl.ie_tipo_complemento (+) = 2
AND 	medico.nr_seq_person_name	= w.nr_sequencia(+)
AND 	a.nr_atendimento_mae = am.nr_atendimento
AND	am.nr_atendimento 	= nasc.nr_atendimento(+)
AND	am.nr_atendimento 	= par.nr_atendimento(+)
AND	am.nr_atendimento 	= ag.nr_atendimento(+)
AND	am.nr_atendimento 	= ec.nr_atendimento(+)
AND 	am.cd_pessoa_fisica = mae.cd_pessoa_fisica
AND 	am.cd_pessoa_fisica = hsm.cd_pessoa_fisica(+)
AND   	mae.cd_pessoa_fisica 	= cpfm.cd_pessoa_fisica
AND	cpfm.ie_tipo_complemento 	= 1
AND	am.nr_atendimento 	= acc.nr_atendimento(+)
AND	acc.cd_convenio 		= co.cd_convenio(+)
AND 	am.nr_atendimento = dom.nr_atendimento(+)
AND 	mae.nr_seq_person_name	= z.nr_sequencia(+)
AND     z.ds_type (+) = 'main'
AND     w.ds_type (+) = 'main'
AND     y.ds_type (+) = 'main';
/