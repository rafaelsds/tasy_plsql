create or replace view w_eis_tx_preventivo_flebite_v  as 
SELECT	DISTINCT	
	SUBSTR(obter_nome_setor(Obter_Setor_Atendimento(b.nr_Atendimento)),1,255) ds_Setor_Atendimento,	
	obter_unidade_atendimento(b.nr_atendimento,'A','U') ds_unidade,
	SUBSTR(obter_idade(obter_data_nascto_pf(b.cd_pessoa_fisica),SYSDATE,'E'),1,10) ie_faixa_etaria,
	SUBSTR(obter_desc_resultados_flebite(b.nr_atendimento,NULL,b.dt_atualizacao_nrec,'PO'),1,2) ie_prescrito,	
	b.nr_atendimento, 
	c.ds_regra ds_conduta,
	trunc(b.dt_atualizacao_nrec) dt_atualizacao_nrec,
	b.ie_escala
FROM	gqa_pend_pac_acao a,
	gqa_pendencia_pac b,
	gqa_pendencia_regra c,
	escala_flebite_ins d
WHERE	a.nr_seq_proc IS NOT NULL
AND	a.nr_seq_pend_pac =  b.nr_sequencia
AND	b.nr_seq_pend_regra = c.nr_sequencia
AND	b.nr_seq_escala = d.nr_sequencia
AND	d.ie_flebite = 0
AND	ie_escala = 88;
/