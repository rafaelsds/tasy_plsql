create or replace
view os_projeto_migracao_v as
select	p.nr_sequencia nr_seq_projeto,
	o.nr_sequencia nr_seq_os,
	o.dt_ordem_servico dt_os,
	p.nr_seq_grupo_des nr_seq_grupo_desenv,
	p.nr_seq_estagio nr_seq_estagio_projeto,
	o.nr_seq_estagio nr_seq_estagio_os,
	o.ie_resp_teste
from	man_ordem_servico_exec e,
	proj_projeto p,
	man_ordem_servico o
where	e.nr_seq_ordem = o.nr_sequencia
and	p.nr_seq_ordem_serv = o.nr_sequencia
and	p.nr_seq_gerencia = 9
and	p.nr_seq_classif = 14;
/
