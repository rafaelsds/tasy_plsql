create or replace
view sip_discussao_mat_v as
select	a.nr_sequencia nr_seq_lote,
	b.nr_sequencia nr_seq_discussao,
	c.nr_sequencia nr_seq_discussao_mat,
	a.dt_fechamento dt_mes_competencia,
	c.nr_seq_conta_mat,
	c.vl_aceito vl_liberado,
	c.qt_aceita qt_material
from	pls_lote_discussao		a,
	pls_contestacao_discussao	b,
	pls_discussao_mat		c
where	-- busca somente os lotes fechados
	a.ie_status = 'F' 
	-- s� aceita lotes de despesa
and	(a.nr_titulo_pagar is not null or a.nr_titulo_pagar_ndr is not null)
and	b.nr_seq_lote		= a.nr_sequencia
	-- somente discuss�es encerradas
and	b.ie_status		= 'E'
and	c.nr_seq_discussao	= b.nr_sequencia
	-- somente que tenham gerado valores.
and	c.vl_aceito		> 0;
/
