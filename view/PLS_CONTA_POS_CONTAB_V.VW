create or replace view pls_conta_pos_contab_v as
select	nvl(d.ie_tipo_movto,'X') ie_tipo_movto,
	nvl(d.dt_contabil,b.dt_mes_competencia) dt_mes_competencia,
	substr(pls_obter_dados_segurado(s.nr_sequencia,'CR'),1,30) cd_beneficiario,
	substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_beneficiario,
	pls_obter_dados_segurado(s.nr_sequencia,'CPF') nr_cpf_cnpj_benef,
	n.ie_regulamentacao,
	substr(obter_valor_dominio(1666, n.ie_tipo_contratacao),1,255) ds_tipo_contratacao,
	substr(obter_valor_dominio(1665, n.ie_segmentacao),1,255) ds_segmentacao,
	r.ie_ato_cooperado,
	nvl(d.vl_contabil,b.vl_provisao) vl_provisao,
	d.cd_conta_debito cd_conta_deb_provisao,
	d.cd_conta_credito cd_conta_cred_provisao,
	c.nr_sequencia nr_seq_conta,
	t.nr_contrato,
	substr(decode(x.ie_tipo_protocolo,'I',pls_obter_nome_congenere(x.nr_seq_congenere),pls_obter_dados_prestador(nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec),'N')),1,255) nm_prestador,
	nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'), pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj_pagador,
	n.nr_protocolo_ans,
	substr(obter_descricao_procedimento(nvl(m.cd_procedimento,r.cd_procedimento),nvl(m.ie_origem_proced,r.ie_origem_proced)),1,255) ds_item_mat_proc,
	b.dt_mes_competencia dt_mes_competencia_pos,
	'PN' ie_pos_novo,
	o.nr_sequencia nr_seq_conta_pos_estab,
	c.dt_atendimento_referencia,
	substr(pls_obter_dados_grupo_ans(r.nr_seq_grupo_ans,'D'),1,255) ds_grupo_ans,
	substr(obter_valor_dominio(1669,n.ie_preco),1,255) ds_modalidade_contrat,
	x.nr_sequencia nr_seq_protocolo,
	nvl(m.cd_procedimento,r.cd_procedimento) cd_procedimento,
	null nr_seq_material,
	substr(obter_valor_dominio(2406,s.ie_tipo_segurado),1,255) ds_tipo_segurado,
	substr(obter_valor_dominio(3648,x.ie_tipo_protocolo),1,255) ds_tipo_protocolo,
	substr(pls_obter_dados_prestador(decode(x.ie_tipo_protocolo,'I',x.nr_seq_congenere,nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec)),'TR'),1,255) ds_tipo_vinculo_operadora,
	substr(pls_obter_dados_segurado(nvl(s.nr_seq_titular,s.nr_sequencia),'N'),1,255)  nm_usuario_princ,
	c.cd_estabelecimento
from	pls_protocolo_conta		x,
	pls_conta			c,
	pls_conta_proc			r,
	pls_conta_medica_resumo		m,
	pls_conta_pos_proc		o,
	pls_conta_pos_proc_contab	b,
	pls_pos_estab_dados_contab	d,
	pls_segurado			s,
	pls_contrato			t,
	pls_plano			n
where	x.nr_sequencia		= c.nr_seq_protocolo
and	c.nr_sequencia		= o.nr_seq_conta
and	r.nr_sequencia		= o.nr_seq_conta_proc
and	m.nr_sequencia		= b.nr_seq_conta_resumo
and	c.nr_sequencia		= m.nr_seq_conta
and	o.nr_sequencia		= b.nr_seq_conta_pos_proc
and	d.nr_seq_pos_proc_contab = b.nr_sequencia(+)
and	o.nr_seq_segurado	= s.nr_sequencia(+)
and	s.nr_seq_contrato	= t.nr_sequencia(+)
and	c.nr_seq_plano		= n.nr_sequencia(+)
union all
select	nvl(d.ie_tipo_movto,'X') ie_tipo_movto,
	nvl(d.dt_contabil,b.dt_mes_competencia) dt_mes_competencia,
	substr(pls_obter_dados_segurado(s.nr_sequencia,'CR'),1,30) cd_beneficiario,
	substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_beneficiario,
	pls_obter_dados_segurado(s.nr_sequencia,'CPF') nr_cpf_cnpj_benef,
	n.ie_regulamentacao,
	substr(obter_valor_dominio(1666, n.ie_tipo_contratacao),1,255) ds_tipo_contratacao,
	substr(obter_valor_dominio(1665, n.ie_segmentacao),1,255) ds_segmentacao,
	r.ie_ato_cooperado,
	nvl(d.vl_contabil,b.vl_provisao) vl_provisao,
	d.cd_conta_debito cd_conta_deb_provisao,
	d.cd_conta_credito cd_conta_cred_provisao,
	c.nr_sequencia nr_seq_conta,
	t.nr_contrato,
	substr(decode(x.ie_tipo_protocolo,'I',pls_obter_nome_congenere(x.nr_seq_congenere),pls_obter_dados_prestador(nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec),'N')),1,255) nm_prestador,
	nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'), pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj_pagador,
	n.nr_protocolo_ans,
	substr(pls_obter_dados_conta_mat(nvl(m.nr_seq_conta_mat,r.nr_sequencia),'D'),1,14) ds_item_mat_proc,
	b.dt_mes_competencia dt_mes_competencia_pos,
	'MN' ie_pos_novo,
	o.nr_sequencia nr_seq_conta_pos_estab,
	c.dt_atendimento_referencia,
	substr(pls_obter_dados_grupo_ans(r.nr_seq_grupo_ans,'D'),1,255) ds_grupo_ans,
	substr(obter_valor_dominio(1669,n.ie_preco),1,255) ds_modalidade_contrat,
	x.nr_sequencia nr_seq_protocolo,
	0 cd_procedimento,
	nvl(m.nr_seq_conta_mat,r.nr_sequencia) nr_seq_material,
	substr(obter_valor_dominio(2406,s.ie_tipo_segurado),1,255) ds_tipo_segurado,
	substr(obter_valor_dominio(3648,x.ie_tipo_protocolo),1,255) ds_tipo_protocolo,
	substr(pls_obter_dados_prestador(decode(x.ie_tipo_protocolo,'I',x.nr_seq_congenere,nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec)),'TR'),1,255) ds_tipo_vinculo_operadora,
	substr(pls_obter_dados_segurado(nvl(s.nr_seq_titular,s.nr_sequencia),'N'),1,255)  nm_usuario_princ,
	c.cd_estabelecimento
from	pls_protocolo_conta		x,
	pls_conta			c,
	pls_conta_mat			r,
	pls_conta_medica_resumo		m,
	pls_conta_pos_mat		o,
	pls_conta_pos_mat_contab	b,
	pls_pos_estab_dados_contab	d,
	pls_segurado			s,
	pls_contrato			t,
	pls_plano			n
where	x.nr_sequencia		= c.nr_seq_protocolo
and	c.nr_sequencia		= o.nr_seq_conta
and	r.nr_sequencia		= o.nr_seq_conta_mat
and	m.nr_sequencia		= b.nr_seq_conta_resumo
and	c.nr_sequencia		= m.nr_seq_conta
and	o.nr_sequencia		= b.nr_seq_conta_mat_pos
and	d.nr_seq_pos_mat_contab = b.nr_sequencia(+)
and	o.nr_seq_segurado	= s.nr_sequencia(+)
and	s.nr_seq_contrato	= t.nr_sequencia(+)
and	c.nr_seq_plano		= n.nr_sequencia(+)
union all
select	'X' ie_tipo_movto,
	p.dt_mes_competencia dt_mes_competencia,
	substr(pls_obter_dados_segurado(c.nr_seq_segurado,'CR'),1,30) cd_beneficiario,
	substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_beneficiario,
	pls_obter_dados_segurado(s.nr_sequencia,'CPF') nr_cpf_cnpj_benef,
	d.ie_regulamentacao,
	substr(obter_valor_dominio(1666, d.ie_tipo_contratacao),1,255) ds_tipo_contratacao,
	substr(obter_valor_dominio(1665, d.ie_segmentacao),1,255) ds_segmentacao,
	(	select	b.ie_ato_cooperado
		from	pls_conta_proc b
		where	e.nr_seq_conta_proc = b.nr_sequencia
		and	b.nr_seq_conta = c.nr_sequencia
		union all
		select	b.ie_ato_cooperado
		from	pls_conta_mat b
		where	e.nr_seq_conta_mat = b.nr_sequencia
		and	b.nr_seq_conta = c.nr_sequencia)ie_ato_cooperado,
	b.vl_provisao vl_provisao,
	nvl(b.cd_conta_deb_provisao, b.cd_conta_deb_ndc) cd_conta_deb_provisao,
	nvl(b.cd_conta_cred_provisao, b.cd_conta_cred_ndc) cd_conta_cred_provisao,
	c.nr_sequencia nr_seq_conta,
	t.nr_contrato,
	substr(decode(p.ie_tipo_protocolo,'I',pls_obter_nome_congenere(p.nr_seq_congenere),pls_obter_dados_prestador(nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec),'N')),1,255) nm_prestador,
	nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'), pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj_pagador,
	d.nr_protocolo_ans,
	nvl(substr(obter_descricao_procedimento(e.cd_procedimento,e.ie_origem_proced),1,255), substr(pls_obter_dados_conta_mat(e.nr_seq_conta_mat,'D'),1,255)) ds_item_mat_proc,
	b.dt_mes_competencia dt_mes_competencia_pos,
	'AA' ie_pos_novo,
	e.nr_sequencia nr_seq_conta_pos_estab,
	c.dt_atendimento_referencia,
	(	select	substr(pls_obter_dados_grupo_ans(b.nr_seq_grupo_ans,'D'),1,255)
		from	pls_conta_proc b
		where	e.nr_seq_conta_proc = b.nr_sequencia
		and	b.nr_seq_conta = c.nr_sequencia
		union all
		select	substr(pls_obter_dados_grupo_ans(b.nr_seq_grupo_ans,'D'),1,255)
		from	pls_conta_mat b
		where	e.nr_seq_conta_mat = b.nr_sequencia
		and	b.nr_seq_conta = c.nr_sequencia) ds_grupo_ans,
	substr(obter_valor_dominio(1669,d.ie_preco),1,255) ds_modalidade_contrat,
	p.nr_sequencia nr_seq_protocolo,
	e.cd_procedimento,
	e.nr_seq_material,
	substr(obter_valor_dominio(2406,s.ie_tipo_segurado),1,255) ds_tipo_segurado,
	substr(obter_valor_dominio(3648,p.ie_tipo_protocolo),1,255) ds_tipo_protocolo,
	substr(pls_obter_dados_prestador(decode(p.ie_tipo_protocolo,'I',p.nr_seq_congenere,nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec)),'TR'),1,255) ds_tipo_vinculo_operadora,
	substr(pls_obter_dados_segurado(nvl(s.nr_seq_titular,s.nr_sequencia),'N'),1,255)  nm_usuario_princ,
	c.cd_estabelecimento
from	pls_protocolo_conta 		p,
	pls_conta 			c,
	pls_conta_pos_estabelecido 	e,
	pls_conta_pos_estab_contab 	b,
	pls_segurado 			s,
	pls_plano 			d,
	pls_contrato 			t
where	p.nr_sequencia 		= c.nr_seq_protocolo
and	c.nr_sequencia 		= e.nr_seq_conta
and	e.nr_sequencia 		= b.nr_seq_conta_pos
and	c.nr_seq_segurado	= s.nr_sequencia(+)
and	s.nr_seq_plano		= d.nr_sequencia(+)
and	s.nr_seq_contrato 	= t.nr_sequencia(+)
and 	not exists (	select	1
			from 	pls_conta_pos_mat cm
			where	cm.nr_seq_conta = c.nr_sequencia
			union all
			select	1
			from 	pls_conta_pos_proc cp
			where	cp.nr_seq_conta = c.nr_sequencia);
/
