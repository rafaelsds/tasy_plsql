create or replace
view pls_oc_cta_arvore_filtro_v as
select	-- Esta view s� foi criada por que no dicion�rio de objetos n�o cabe este select.
	1 nr_ordem,
	obter_desc_expressao(284292, 'Benefici�rio') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22279 nr_seq_visao,
	22280 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_BENEF' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_benef	b
where	a.ie_filtro_benef = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	2 nr_ordem,
	obter_desc_expressao(285928, 'Conta') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22911 nr_seq_visao,
	22914 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_CONTA' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_conta	b
where	a.ie_filtro_conta = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	3 nr_ordem,
	obter_desc_expressao(286142, 'Contrato') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22912 nr_seq_visao,
	22912 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_CONTRATO' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro		a,
	pls_oc_cta_filtro_contrato	b 
where	a.ie_filtro_contrato = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	4 nr_ordem,
	obter_desc_expressao(292147, 'Interc�mbio') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22913 nr_seq_visao,
	22913 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_INTERC' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro		a,
	pls_oc_cta_filtro_interc	b
where	a.ie_filtro_interc = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	5 nr_ordem,
	obter_desc_expressao(292952, 'Material') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22917 nr_seq_visao,
	22917 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_MAT' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_mat	b
where	a.ie_filtro_mat = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	6 nr_ordem,
	obter_desc_expressao(296259, 'Prestador') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22919 nr_seq_visao,
	22919 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_PREST' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_prest	b
where	a.ie_filtro_prest = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	7  nr_ordem,
	obter_desc_expressao(296422, 'Procedimento') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22915 nr_seq_visao,
	22916 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_PROC' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_proc	b
where	a.ie_filtro_proc = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	8  nr_ordem,
	obter_desc_expressao(296491, 'Produto') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22918 nr_seq_visao,
	22918 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_PRODUTO' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro		a,
	pls_oc_cta_filtro_produto	b
where	a.ie_filtro_produto = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	9  nr_ordem,
	obter_desc_expressao(296509, 'Profissional') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22924 nr_seq_visao,
	22834 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_PROF' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_prof	b
where	a.ie_filtro_prof = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
union all
select	10 nr_ordem,
	obter_desc_expressao(296585, 'Protocolo') ||
	-- se n�o tem registros vai vai vazio, se n�o bota o count
	decode(count(b.nr_sequencia), 
		0, '',
		' (' || count(b.nr_sequencia) || ')') ds_descricao,
	22920 nr_seq_visao,
	22920 nr_seq_visao_imp,
	count(b.nr_sequencia) qt_registros,
	'PLS_OC_CTA_FILTRO_PROT' nm_tabela,
	a.nr_sequencia
from	pls_oc_cta_filtro	a,
	pls_oc_cta_filtro_prot	b
where	a.ie_filtro_protocolo = 'S'
and	b.nr_seq_oc_cta_filtro(+) = a.nr_sequencia
group by a.nr_sequencia
order by nr_ordem, ds_descricao;
/