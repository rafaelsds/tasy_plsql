create or replace view rel_cot_v as
select	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_cotacao_item,
	cd_fornecedor,
	qt_fornecedor,
	vl_fornecedor,
	nr_seq_coluna,
	ds_marca,
	vl_ipi,
	cd_condicao_pagamento,
	vl_frete,
	qt_dias_entrega,
	vl_unitario_inicial,
	ie_vencedor,
	ds_observacao,
	ds_observacao_item,
	vl_liquido,
	nr_item_cot_compra,
	nr_seq_fornec,
	ds_material_direto,
	ds_marca_fornec,
	ds_motivo_venc_alt,
	nr_cot_compra,
	ds_cond_pagto,
	vl_minimo_nf,
	vl_total,
	cd_pessoa_fisica
from	w_cotacao_item_coluna;
/
