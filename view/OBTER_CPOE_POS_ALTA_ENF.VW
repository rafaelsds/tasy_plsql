CREATE OR REPLACE VIEW obter_cpoe_pos_alta_enf("DS", "TIPO_CPOE", "IE_USUARIO_ENF", "DT_PREV_EXECUCAO", "IE_TIPO_ITEM","NR_ATENDIMENTO", "DT_SUSPENSAO", "NR_SEQ_ORDER_TYPE") AS
 SELECT DISTINCT substr((CASE WHEN cp.DT_SUSPENSAO IS NOT NULL THEN  obter_desc_expressao_idioma(782908, null, wheb_usuario_pck.get_nr_seq_idioma) || ' ' ELSE '' END) || obter_desc_material(cp.cd_material), 1, 255) DS,
                decode(cp.ie_material,'S','MATERIALS','MEDICINES') tipo_cpoe,
                obter_funcao_usuario(cp.nm_usuario) ie_usuario_enf,
                TO_DATE(cp.DT_INICIO) dt_prev_execucao,
                decode(cp.ie_material,'S','MA','M') IE_TIPO_ITEM,
                cp.nr_atendimento,
                cp.DT_SUSPENSAO,
                mot.nr_seq_order_type nr_seq_order_type
FROM CPOE_MATERIAL cp,
    material_order_type mot
WHERE mot.cd_material = cp.cd_material
AND cp.dt_liberacao IS NOT NULL
AND    ((cp.dt_fim > sysdate) or (cp.dt_fim is null))
UNION ALL
/* Procedure */
SELECT DISTINCT ((CASE WHEN cp.DT_SUSPENSAO IS NOT NULL THEN  obter_desc_expressao_idioma(782908, null, wheb_usuario_pck.get_nr_seq_idioma) || ' ' ELSE '' END) || Obter_Desc_Proc_Interno(cp.NR_SEQ_PROC_INTERNO) || ' - ' || Obter_desc_intervalo(cp.cd_intervalo)) DS,
                'PROCEDURES' tipo_cpoe,
                obter_funcao_usuario(cp.nm_usuario) ie_usuario_enf,
                TO_DATE(cp.dt_prev_execucao) dt_prev_execucao,
                'P' IE_TIPO_ITEM,
                cp.nr_atendimento,
                cp.DT_SUSPENSAO,
                pot.nr_seq_order_type nr_seq_order_type
FROM CPOE_PROCEDIMENTO cp,
    proc_order_type pot
WHERE cp.dt_liberacao IS NOT NULL
AND    ((cp.dt_fim > sysdate) or (cp.dt_fim is null))
AND pot.nr_seq_proc_interno = cp.nr_seq_proc_interno
UNION ALL
/* Interventions and Recommendations */
SELECT DISTINCT (CASE WHEN cp.DT_SUSPENSAO IS NOT NULL THEN  obter_desc_expressao_idioma(782908, null, wheb_usuario_pck.get_nr_seq_idioma) || ' ' ELSE '' END) || obter_desc_recomendacao(cd_recomendacao) DS,
                'INTERVENTIONS' tipo_cpoe,
                obter_funcao_usuario(cp.nm_usuario) ie_usuario_enf,
                TO_DATE(cp.DT_INICIO) dt_prev_execucao,
                'R' IE_TIPO_ITEM,
                cp.nr_atendimento,
                cp.DT_SUSPENSAO,
                null nr_seq_order_type
FROM CPOE_RECOMENDACAO cp
WHERE cp.dt_liberacao IS NOT NULL
AND    ((cp.dt_fim > sysdate) or (cp.dt_fim is null))
UNION ALL
/* Nursing interventions */
SELECT DISTINCT substr((CASE WHEN cp.DT_SUSPENSAO IS NOT NULL THEN  obter_desc_expressao_idioma(782908, null, wheb_usuario_pck.get_nr_seq_idioma) || ' ' ELSE '' END) || Obter_desc_intervencoes(nr_seq_proc) || ' - ' || obter_desc_intervalo_prescr(cd_intervalo), 1, 255) DS,
                'NURSING INTERVENTIONS' tipo_cpoe,
                obter_funcao_usuario(cp.nm_usuario) ie_usuario_enf,
                TO_DATE(cp.DT_INICIO) dt_prev_execucao,
                'I' IE_TIPO_ITEM,
                cp.nr_atendimento,
                cp.DT_SUSPENSAO,
                null nr_seq_order_type
FROM CPOE_INTERVENCAO cp
WHERE cp.dt_liberacao IS NOT NULL
AND    ((cp.dt_fim > sysdate) or (cp.dt_fim is null));

/
