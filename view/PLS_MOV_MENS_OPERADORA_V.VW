create or replace
view pls_mov_mens_operadora_v
as
select	1 ie_tipo_reg, --Cabe�alho
	a.nr_sequencia nr_seq_mov_operadora,
	b.nr_sequencia nr_seq_lote,
	to_char(b.dt_referencia,'ddmmyyyy') dt_referencia,
	to_char(b.dt_geracao_lote,'ddmmyyyy') dt_geracao_lote,
	b.nr_seq_regra,
	to_char(b.dt_movimento_inicial,'ddmmyyyy') dt_movimento_inicial,
	to_char(b.dt_movimento_final,'ddmmyyyy') dt_movimento_final,
	c.cd_cooperativa cd_cooperativa_destino,
	pls_obter_cd_cooperativa(b.cd_estabelecimento) cd_cooperativa_origem,
	null cd_usuario_plano,
	null ie_tipo_item,
	null dt_inicio_cobertura,
	null dt_fim_cobertura,
	null vl_item,
	null vl_antecipacao,
	null vl_ato_cooperado,
	null vl_ato_cooperado_antec,
	null vl_ato_cooperado_pro_rata,
	null vl_ato_nao_cooperado,
	null vl_ato_nao_coop_antec,
	null vl_ato_nao_coop_pro_rata,
	null vl_pro_rata_dia,
	null vl_ato_auxiliar,
	null vl_ato_auxiliar_antec,
	null vl_ato_auxiliar_pro_rata
from	pls_mov_mens_operadora	a,
	pls_mov_mens_lote	b,
	pls_congenere		c
where	b.nr_sequencia		= a.nr_seq_lote
and	c.nr_sequencia		= a.nr_seq_congenere
union 
select	2 ie_tipo_reg,
	a.nr_sequencia nr_seq_mov_operadora,
	null nr_seq_lote,
	null dt_referencia,
	null dt_geracao_lote,
	null nr_seq_regra,
	null dt_movimento_inicial,
	null dt_movimento_final,
	null cd_cooperativa_destino,
	null cd_cooperativa_origem,
	c.cd_usuario_plano,
	null ie_tipo_item,
	null dt_inicio_cobertura,
	null dt_fim_cobertura,
	null vl_item,
	null vl_antecipacao,
	null vl_ato_cooperado,
	null vl_ato_cooperado_antec,
	null vl_ato_cooperado_pro_rata,
	null vl_ato_nao_cooperado,
	null vl_ato_nao_coop_antec,
	null vl_ato_nao_coop_pro_rata,
	null vl_pro_rata_dia,
	null vl_ato_auxiliar,
	null vl_ato_auxiliar_antec,
	null vl_ato_auxiliar_pro_rata
from	pls_mov_mens_operadora	a,
	pls_mov_mens_lote	b,
	pls_mov_mens_benef	c
where	b.nr_sequencia		= a.nr_seq_lote
and	a.nr_sequencia		= c.nr_seq_mov_operadora
union all
select	3 ie_tipo_reg,
	a.nr_sequencia nr_seq_mov_operadora,
	null nr_seq_lote,
	null dt_referencia,
	null dt_geracao_lote,
	null nr_seq_regra,
	null dt_movimento_inicial,
	null dt_movimento_final,
	null cd_cooperativa_destino,
	null cd_cooperativa_origem,
	c.cd_usuario_plano,
	d.ie_tipo_item,
	to_char(d.dt_inicio_cobertura,'ddmmyyyy') dt_inicio_cobertura,
	to_char(d.dt_fim_cobertura,'ddmmyyyy') dt_fim_cobertura,
	trim(to_char(d.vl_item, '999999990D99')) vl_item,
	trim(to_char(d.vl_antecipacao, '999999990D99')) vl_antecipacao,
	trim(to_char(d.vl_ato_cooperado, '999999990D99')) vl_ato_cooperado,
	trim(to_char(d.vl_ato_cooperado_antec, '999999990D99')) vl_ato_cooperado_antec,
	trim(to_char(d.vl_ato_cooperado_pro_rata, '999999990D99')) vl_ato_cooperado_pro_rata,
	trim(to_char(d.vl_ato_nao_cooperado, '999999990D99')) vl_ato_nao_cooperado,
	trim(to_char(d.vl_ato_nao_coop_antec, '999999990D99')) vl_ato_nao_coop_antec,
	trim(to_char(d.vl_ato_nao_coop_pro_rata, '999999990D99')) vl_ato_nao_coop_pro_rata,
	trim(to_char(d.vl_pro_rata_dia, '999999990D99')) vl_pro_rata_dia,
	trim(to_char(d.vl_ato_auxiliar, '999999990D99')) vl_ato_auxiliar,
	trim(to_char(d.vl_ato_auxiliar_antec, '999999990D99')) vl_ato_auxiliar_antec,
	trim(to_char(d.vl_ato_auxiliar_pro_rata, '999999990D99')) vl_ato_auxiliar_pro_rata
from	pls_mov_mens_operadora	a,
	pls_mov_mens_lote	b,
	pls_mov_mens_benef	c,
	pls_mov_mens_benef_item	d
where	b.nr_sequencia		= a.nr_seq_lote
and	a.nr_sequencia		= c.nr_seq_mov_operadora
and	c.nr_sequencia		= d.nr_seq_mov_benef;
/

