create or replace view pls_concil_mov_contab_copart_v
as
select	c.nr_sequencia nr_documento,
	--'Provisao de coparticipacao da conta: '
	nvl(c.vl_coparticipacao,0) vl_copart,
	null vl_estorno,
	null vl_copart_disc,
	c.dt_mes_competencia,
	a.nr_sequencia nr_seq_conta,
	b.ie_estorno_custo,
	x.nr_sequencia nr_seq_protocolo,
	b.ie_status_coparticipacao,
	x.cd_estabelecimento,
	a.ie_status,
	x.ie_tipo_protocolo
from	pls_conta 			a,
	pls_conta_coparticipacao	b,
	pls_conta_copartic_contab	c,
	pls_plano			d,
	pls_protocolo_conta 		x
where	a.nr_sequencia			= b.nr_seq_conta
and	b.nr_sequencia			= c.nr_seq_conta_copartic
and	a.nr_sequencia			= b.nr_seq_conta
and	x.nr_sequencia			= a.nr_seq_protocolo
and	d.nr_sequencia			= a.nr_seq_plano
and	nvl(c.vl_coparticipacao,0)	> 0
and	a.ie_status			in ('F','S')
and	((x.ie_tipo_protocolo		in ('C','I','F')
and	b.ie_status_coparticipacao	in ('D','S'))
or	(x.ie_tipo_protocolo		= 'R'
and	b.ie_status_coparticipacao	<> 'N'))
union all
select	c.nr_sequencia nr_documento,
	--'Estorno da provisao de coparticipacao da conta: '
	null vl_copart,
	nvl(c.vl_coparticipacao,0) vl_estorno,
	null vl_copart_disc,
	b.dt_estorno dt_mes_competencia,
	a.nr_sequencia nr_seq_conta,
	b.ie_estorno_custo,
	x.nr_sequencia nr_seq_protocolo,
	b.ie_status_coparticipacao,
	x.cd_estabelecimento,
	a.ie_status,
	x.ie_tipo_protocolo
from	pls_conta 			a,
	pls_conta_coparticipacao	b,
	pls_conta_copartic_contab	c,
	pls_plano			d,
	pls_protocolo_conta 		x
where	a.nr_sequencia			= b.nr_seq_conta
and	b.nr_sequencia			= c.nr_seq_conta_copartic
and	a.nr_sequencia			= b.nr_seq_conta
and	x.nr_sequencia			= a.nr_seq_protocolo
and	d.nr_sequencia			= a.nr_seq_plano
and	a.ie_status			in ('F','S') -- Conta fechada
and	nvl(b.ie_estorno_custo,'N')	= 'S'
and	((x.ie_tipo_protocolo		in ('C','I','F')
and	b.ie_status_coparticipacao	in ('D','S'))
or	(x.ie_tipo_protocolo		= 'R'))
and	nvl(c.vl_coparticipacao,0)	< 0
union all
select	c.nr_sequencia nr_documento,
	--'Discussao da provisao de coparticipacao da conta: '
	null vl_copart,
	null vl_estorno,
	nvl(c.vl_coparticipacao,0) * -1 vl_copart_disc,
	b.dt_fechamento_discussao dt_mes_competencia,
	a.nr_sequencia nr_seq_conta,
	b.ie_estorno_custo,
	x.nr_sequencia nr_seq_protocolo,
	b.ie_status_coparticipacao,
	x.cd_estabelecimento,
	a.ie_status,
	x.ie_tipo_protocolo
from	pls_conta 			a,
	pls_conta_coparticipacao	b,
	pls_conta_copartic_contab	c,
	pls_plano			d,
	pls_protocolo_conta 		x
where	a.nr_sequencia			= b.nr_seq_conta
and	b.nr_sequencia			= c.nr_seq_conta_copartic
and	a.nr_sequencia			= b.nr_seq_conta
and	x.nr_sequencia			= a.nr_seq_protocolo
and	d.nr_sequencia			= a.nr_seq_plano
and	b.ie_status_coparticipacao 	= 'F'
and	a.ie_status			in ('F','S') -- Conta fechada
and	nvl(b.ie_estorno_custo,'N')	= 'N'
and	x.ie_tipo_protocolo		in ('C','I','R','F')
and	b.dt_fechamento_discussao	is not null;
/
