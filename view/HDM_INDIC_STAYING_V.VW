create or replace view
HDM_INDIC_STAYING_V as
/* Esta view ir� conter a jun��o de todas as tabelas de fato e dimens�o do modelo de perman�ncia */
SELECT	/* Dados m�s */
	b.nr_year,       
	b.nr_month,
	b.nr_semester,
	b.nr_quarter,  
	b.dt_complete_date dt_month,
	/* Dados grupo paciente */ 
	a.nr_seq_patient_group,
	d.si_sex,
	d.ds_sex,
	d.nm_age_range,
	d.ds_participant,
	d.si_participant,
	/* Dados programa */
	e.nr_sequencia nr_seq_dm_program,
	e.nr_seq_program nr_seq_program, 
	e.nm_program,
	e.nm_program_module nm_program_module, 
	/* Dados grupo atividade */
	i.nr_sequencia nr_seq_dm_group,
	i.nm_group, 
	i.nm_class nm_group_class, 
	i.nr_seq_turma,
	/* Patologia */
	n.nr_sequencia nr_seq_disease_risk,
	n.nm_disease_risk, 
	/* Dados fato */
	a.nr_days_permanence, 
	a.nr_months_permanence,
	(a.nr_days_permanence / COUNT(1) OVER (PARTITION BY a.nr_sequencia)) qt_days_by_fact,
	(a.nr_months_permanence / COUNT(1) OVER (PARTITION BY a.nr_sequencia)) qt_months_by_fact,
	a.nr_sequencia nr_seq_fact,
	a.ds_unique
FROM	hdm_indic_dm_risk_disease n,
	hdm_indic_dm_program e,
	hdm_indic_dm_activ_group i,
	hdm_indic_ft_permanence_rd o,
	hdm_indic_ft_permanence_pr f,
	hdm_indic_ft_permanence_ag j,
	hdm_indic_dm_patient_group d,
	hdm_indic_dm_month b,
	hdm_indic_ft_permanence a
WHERE	1 = 1
AND	f.nr_seq_dimension = e.nr_sequencia
AND	j.nr_seq_dimension = i.nr_sequencia
AND	o.nr_seq_dimension = n.nr_sequencia
AND	a.nr_sequencia = f.nr_seq_fact
AND	a.nr_sequencia = j.nr_seq_fact
AND	a.nr_sequencia = o.nr_seq_fact
AND	a.nr_seq_patient_group = d.nr_sequencia
AND	b.nr_sequencia = a.nr_seq_month;
/