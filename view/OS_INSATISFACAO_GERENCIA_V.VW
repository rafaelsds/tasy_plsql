create or replace view
	os_insatisfacao_gerencia_v as
select	a.nr_sequencia,
	a.dt_fim_real,
	nvl(a.nr_seq_gerencia_insatisf,b.nr_seq_gerencia) nr_seq_gerencia,
	b.nr_sequencia nr_seq_grupo,
	a.ds_dano_breve,
	a.ie_origem_os,
	a.nr_seq_localizacao,
	a.cd_funcao,
	a.nr_seq_gerencia_insatisf,
	(select max(ie_area_gerencia) from gerencia_wheb c where c.nr_sequencia  =  nvl(a.nr_seq_gerencia_insatisf,b.nr_seq_gerencia)) ie_area_gerencia,
	m.ie_terceiro,
	man_obter_dt_os_encerrada(a.nr_sequencia) dt_encerramento_ordem
from	man_grau_satisf_justif j,
	grupo_desenvolvimento b,
	man_localizacao m,
	man_ordem_servico a	
where	a.nr_seq_grupo_des = b.nr_sequencia
and	m.nr_sequencia	= a.nr_seq_localizacao
and	b.ie_situacao = 'A'
and	a.ie_status_ordem = 3
and	a.nr_seq_justif_grau_satisf = j.nr_sequencia
and	a.nr_seq_justif_grau_satisf is not null
and	nvl(j.ie_indicador,'S') = 'S'
and	a.ie_grau_satisfacao in ('P','R')
and	exists (select 1
		from 	man_estagio_processo d,
			man_ordem_serv_estagio e
		where	e.nr_seq_ordem = a.nr_sequencia
		and	d.nr_sequencia = e.nr_seq_estagio
		and	((d.ie_desenv = 'S' or (d.ie_tecnologia = 'S') or (d.ie_infra = 'S'))));
/