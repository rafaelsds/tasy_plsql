create or replace view pls_plusoft_solic_alteracao_v as
	select 	sa.nr_sequencia	nr_seq_solicitacao,
		substr(obter_valor_dominio((4437), sa.ie_status), 1, 255)					ds_status,
		sa.nm_usuario_nrec										cd_usuario,
		sac.nr_sequencia                                                                             	nr_seq_alt_campo,
		substr(pls_obter_dados_solic_alt(sac.nm_atributo, null, 'S', sac.ds_chave_composta), 1, 255) 	ds_atributo,
		substr(pls_obter_dados_solic_alt(sac.nm_atributo, sac.ds_valor_old, null), 1, 255)           	ds_valor_anterior,
		substr(pls_obter_dados_solic_alt(sac.nm_atributo, sac.ds_valor_new, null), 1, 255)           	ds_valor_solicitado,
		decode(sac.ie_status, 'A', 'Aprovado', 'R', 'Reprovado', 'Pendente')                         	ds_status_campo,
		sa.cd_pessoa_fisica										cd_pessoa_fisica
	from 	tasy_solic_alt_campo sac,
		tasy_solic_alteracao sa
	where 	sac.nr_seq_solicitacao = sa.nr_sequencia
	order by 	
		sac.nr_sequencia;
/