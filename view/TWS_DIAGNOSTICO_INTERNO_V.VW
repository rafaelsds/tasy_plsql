create or replace view tws_diagnostico_interno_v as 
select 	nr_sequencia,
	dt_atualizacao,
	ds_diagnostico,
	cd_doenca_cid,
	ie_utilizacao
from 	diagnostico_interno;
/