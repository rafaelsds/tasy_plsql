create or replace view tws_pls_regra_vis_mens_web_v as 
select 	nr_sequencia,
	dt_atualizacao,
	cd_estabelecimento,
	dt_fim_vigencia,
	dt_inicio_vigencia,
	ie_tipo_acesso,
	ie_tipo_pagador,
	ie_titular_pagador,
	nr_seq_forma_cobranca,
	nr_seq_pagador 
from 	pls_regra_visual_mens_web;
/