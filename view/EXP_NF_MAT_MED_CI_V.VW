create or replace 
view exp_nf_mat_med_ci_v as
select	1 	tp_registro,
	1	nr_linha,
	substr(obter_cgc_cpf_editado(n.cd_cgc),1,18) cd_cnpj_cpf,
	n.nr_nota_fiscal,
	n.dt_entrada_saida,
	substr(subst_virgula_ponto_adic_zero(campo_mascara_virgula(n.vl_total_nota)),1,100) vl_total_nota,		
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'UF'),1,2) sg_estado,
	n.nr_sequencia,
	''			   vl_tributo,
	''			   tx_tributo,
	''			   vl_base,
	''			   ie_tipo_imposto,
	''			   vl_outros_trib,
	''				cd_cfop,
	n.dt_emissao
from	nota_fiscal n
where	n.dt_atualizacao_estoque is not null
and 	substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'E'
union
select	3 	tp_registro,
	2	nr_linha,
	''			  cd_cnpj_cpf,
	n.nr_nota_fiscal,
	n.dt_entrada_saida,
	substr(subst_virgula_ponto_adic_zero(campo_mascara_virgula(n.vl_total_nota)),1,100) vl_total_nota,	
	''			sg_estado,
	n.nr_sequencia,
	substr(subst_virgula_ponto_adic_zero(campo_mascara_virgula(decode(substr(obter_tipo_tributo(t.cd_tributo),1,15),'ISS',
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS'),'ICMS',
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ICMS')))),1,100) vl_tributo,
	substr(subst_virgula_ponto_adic_zero(campo_mascara_virgula(decode(substr(obter_tipo_tributo(t.cd_tributo),1,15),'ISS',
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS'),'ICMS',
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ICMS')))),1,100) tx_tributo,
	substr(subst_virgula_ponto_adic_zero(campo_mascara_virgula(decode(substr(obter_tipo_tributo(t.cd_tributo),1,15),'ISS',
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS'),'ICMS',
	obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ICMS')))),1,100) vl_base,	
	decode(substr(obter_tipo_tributo(t.cd_tributo),1,15),'ISS','2','ICMS','1') ie_tipo_imposto,
	subst_virgula_ponto_adic_zero(campo_mascara_virgula(nvl((select sum(y.vl_tributo)
				 from 	nota_fiscal_trib y
				 where	y.nr_sequencia = n.nr_sequencia
				 and	substr(obter_tipo_tributo(y.cd_tributo),1,15) not in ('ISS','ICMS')),0))) vl_outros_trib,
	obter_cod_serv_prestado(n.cd_operacao_nf) cd_cfop,
	n.dt_emissao
from	nota_fiscal n,
	nota_fiscal_trib t
where	n.dt_atualizacao_estoque is not null
and	substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'E'
and	n.nr_sequencia = t.nr_sequencia(+)
and	substr(obter_tipo_tributo(t.cd_tributo),1,15) in ('ISS','ICMS')
order by nr_sequencia, tp_registro
/