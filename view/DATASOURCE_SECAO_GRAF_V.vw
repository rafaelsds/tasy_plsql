CREATE OR REPLACE VIEW DATASOURCE_SECAO_GRAF_V AS 
  SELECT  
              d.nr_sequencia ROW_ID ,
              b.nr_cirurgia,
              b.nr_seq_pepo,
              b.nm_usuario,
              c.nr_seq_linked_data
            FROM pepo_modelo a, 
                 w_flowsheet_cirurgia_pac b, 
                 w_flowsheet_cirurgia_grupo c, 
                 w_flowsheet_cirurgia_info d 
            WHERE 
               a.NR_SEQUENCIA           = b.nr_seq_modelo  
               AND b.nr_sequencia       = c.nr_seq_flowsheet 
               AND c.nr_sequencia       = d.nr_seq_grupo;
/
