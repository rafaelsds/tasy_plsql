create or replace view lote_fornec_utilizado_v as
select	b.cd_estabelecimento,
	x.nr_seq_lote_fornec,
	b.cd_pessoa_fisica cd_pessoa_fisica,
	c.nm_pessoa_fisica nm_pessoa_fisica,
	b.nr_atendimento nr_atendimento,
	b.nr_prescricao nr_prescricao,
	wheb_mensagem_pck.get_Texto(761628) ie_origem,
	'S' ie_entrada_saida,
	sum(nvl(x.qt_executada, x.qt_material)) qt_material,
	trunc(x.DT_ATENDIMENTO,'dd') dt_utilizado,
	a.cd_material,
	x.cd_local_estoque,
	b.cd_prescritor,
	x.cd_unidade_medida,
	null cd_acao
from	pessoa_fisica c,
	prescr_medica b,
	prescr_material a,
	material_atend_paciente x
where	b.cd_pessoa_fisica 	= c.cd_pessoa_fisica
and	a.nr_prescricao    	= b.nr_prescricao
and	a.nr_prescricao	= x.nr_prescricao
and	a.nr_sequencia	= x.nr_sequencia_prescricao
and	x.nr_seq_lote_fornec is not null
group by
	b.cd_estabelecimento,
	x.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	b.nr_prescricao,
	trunc(x.DT_ATENDIMENTO,'dd'),
	a.cd_material,
	x.cd_local_estoque,
	b.cd_prescritor,
	x.cd_unidade_medida
union all
select	c.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	c.cd_pessoa_fisica,
	d.nm_pessoa_fisica,
	c.nr_atendimento,
	0 nr_prescricao,
	wheb_mensagem_pck.get_Texto(761633),
	'E' ie_entrada_saida,
	sum(a.qt_material),
	trunc(b.dt_devolucao,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	null,
	a.cd_unidade_medida,
	null cd_acao
from	pessoa_fisica d,
	atendimento_paciente c,
	devolucao_material_pac b,
	item_devolucao_material_pac a
where	c.cd_pessoa_fisica = d.cd_pessoa_fisica
and	b.nr_atendimento   = c.nr_atendimento
and	a.nr_devolucao     = b.nr_devolucao
and	a.nr_seq_lote_fornec is not null
group by
	c.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	c.cd_pessoa_fisica,
	d.nm_pessoa_fisica,
	c.nr_atendimento,
	trunc(b.dt_devolucao,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	a.cd_unidade_medida
union all
select	b.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	a.nr_prescricao,
	wheb_mensagem_pck.get_Texto(761634),
	'S' ie_entrada_saida,	
	decode(a.cd_acao,1,sum(nvl(a.qt_executada, a.qt_material)),2,sum(a.qt_material)) qt_material,
	trunc(dt_atendimento,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	null,
	a.cd_unidade_medida,
	a.cd_acao
from	pessoa_fisica c,
	atendimento_paciente b,
	material_atend_paciente a
where	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_seq_lote_fornec is not null
and	a.nr_sequencia_prescricao is null
and	a.nr_seq_item_devol is null
group by
	b.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	a.nr_prescricao,
	trunc(dt_atendimento,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	a.cd_unidade_medida,
	a.cd_acao
union all
select	b.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	a.nr_seq_reag,
	wheb_mensagem_pck.get_Texto(761643),
	'S' ie_entrada_saida,	
	sum(1),
	trunc(dt_entrada,'dd'),
	a.cd_material,
	null,
	null,
	substr(obter_dados_material(a.cd_material,'UME'),1,30) cd_unidade_medida,
	null cd_acao
from	pessoa_fisica c,
	atendimento_paciente b,
	san_exame_realizado_reagente_v a
where	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_seq_lote_fornec is not null
group by
	b.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	a.nr_seq_reag,
	trunc(dt_entrada,'dd'),
	a.cd_material
union all
select	a.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	a.nr_prescricao,
	wheb_mensagem_pck.get_Texto(761634),
	decode(a.cd_acao,1,'S','E') ie_entrada_saida,	
	sum(a.qt_movimento),
	trunc(a.dt_movimento_estoque,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	null,
	a.cd_unidade_med_mov,
	null cd_acao
from	pessoa_fisica c,
	atendimento_paciente b,
	movimento_estoque a,
	operacao_estoque d
where	a.nr_atendimento	= b.nr_atendimento
and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
and	a.cd_operacao_estoque = d.cd_operacao_estoque
and	d.ie_tipo_requisicao not in ('2','21')
and	a.nr_atendimento is not null
and	ie_origem_documento = 3
and	nr_seq_tab_orig = 0
and	nr_seq_lote_fornec is not null
group by a.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	a.nr_prescricao,
	wheb_mensagem_pck.get_Texto(761634),
	decode(a.cd_acao,1,'S','E'),
	trunc(a.dt_movimento_estoque,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	null,
	a.cd_unidade_med_mov
union all
select	a.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	substr(obter_dados_atendimento(nr_atendimento,'CP'),1,10) cd_pessoa_fisica,
	substr(obter_dados_atendimento(a.nr_atendimento,'NP'),1,100) nm_pessoa_fisica,
	a.nr_atendimento,
	a.nr_prescricao,
	wheb_mensagem_pck.get_Texto(761635),
	decode(a.cd_acao,1,'S','E') ie_entrada_saida,	
	sum(a.qt_movimento),
	trunc(a.dt_movimento_estoque,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	null,
	a.cd_unidade_med_mov,
	null cd_acao
from	movimento_estoque a
where	a.nr_atendimento is not null
and	nr_seq_lote_fornec is not null
and	ie_origem_documento = 10
group by a.cd_estabelecimento,
	a.nr_seq_lote_fornec,
	a.nr_atendimento,
	a.nr_prescricao,
	wheb_mensagem_pck.get_Texto(761635),
	decode(a.cd_acao,1,'S','E'),
	trunc(a.dt_movimento_estoque,'dd'),
	a.cd_material,
	a.cd_local_estoque,
	null,
	a.cd_unidade_med_mov
union all
select	b.cd_estabelecimento,
	d.nr_seq_lote_fornec,
	b.cd_pessoa_fisica cd_pessoa_fisica,
	c.nm_pessoa_fisica nm_pessoa_fisica,
	b.nr_atendimento nr_atendimento,
	b.nr_prescricao nr_prescricao,
	wheb_mensagem_pck.get_Texto(761628) ie_origem,
	'S' ie_entrada_saida,
	sum(a.qt_unitaria) qt_material,
	trunc(nvl(d.dt_horario, sysdate),'dd') dt_utilizado,
	a.cd_material,
	a.cd_local_estoque,
	b.cd_prescritor,
	a.CD_UNIDADE_MEDIDA_DOSE,
	null cd_acao
from	prescr_mat_hor d,
	prescr_material a,
	prescr_medica b,
	pessoa_fisica c
where	d.nr_prescricao		= a.nr_prescricao
and 	d.nr_seq_material	= a.nr_sequencia
and	a.nr_prescricao    	= b.nr_prescricao
and	b.cd_pessoa_fisica 	= c.cd_pessoa_fisica
and	d.nr_seq_lote_fornec is not null
and	a.ie_medicacao_paciente = 'S'
group by
	b.cd_estabelecimento,
	d.nr_seq_lote_fornec,
	b.cd_pessoa_fisica,
	c.nm_pessoa_fisica,
	b.nr_atendimento,
	b.nr_prescricao,
	trunc(nvl(d.dt_horario, sysdate),'dd'),
	a.cd_material,
	a.cd_local_estoque,
	b.cd_prescritor,
	a.CD_UNIDADE_MEDIDA_DOSE;
/