create or replace view bft_service_provider_v as
select	a.cd_cgc legal_entity_code,
	a.cd_sistema_ant previous_system_code,
	a.cd_tipo_pessoa type_code,
	b.cd_estabelecimento establishment_code,
	substr(obter_desc_tipo_pessoa(a.cd_tipo_pessoa),1,255) legal_entity_type,
	a.cd_rfc company_number,
	a.ds_razao_social corporate_name,
	a.nm_fantasia commercial_name,
	a.ds_endereco address,
	a.ds_municipio city,
	a.sg_estado state_code,
	obter_valor_dominio_idioma(50, a.sg_estado, wheb_usuario_pck.get_nr_seq_idioma) state_desc,
	a.nr_seq_pais country_code,
	obter_nome_pais(a.nr_seq_pais) country_name,
	a.cd_cep postal_code,
	a.nr_telefone Phone_number
from	pessoa_juridica a,
	estabelecimento b
where 	a.cd_cgc = b.cd_cgc
and 	b.ie_situacao = 'A'
and 	a.ie_situacao = 'A';
/