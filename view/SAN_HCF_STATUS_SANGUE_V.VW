create or replace
view san_hcf_status_sangue_v
as
select	distinct
	a.*
from (
	select	'Transfundido' ie_situacao,
        	a.nr_sangue,
	        OBTER_DESC_SAN_DERIVADO(a.nr_seq_derivado) ds_derivado,
	        a.ie_tipo_sangue,
	        a.ie_fator_rh,
	        obter_nome_convenio(obter_convenio_atendimento(b.nr_atendimento)) ds_convenio,
	        null nr_guia,
		b.nr_atendimento,
		substr(obter_nome_pessoa_fisica(b.cd_pessoa_fisica,''),1,255) nm_paciente,
		decode(a.ie_irradiado,'S','I','')||decode(a.ie_lavado,'S','L','')
		||decode(a.ie_filtrado,'S','F','')||decode(a.ie_aliquotado,'S','A','') ie_tratamento,
		a.nr_seq_reserva,
		a.nr_seq_derivado,
		a.nr_sequencia,
		a.nr_seq_emp_ent,
		a.nr_seq_emp_saida,
		a.nr_seq_inutil,
		a.nr_seq_transfusao,
		b.dt_transfusao dt_movimento,
		b.dt_transfusao,
		trunc(san_obter_data_saida_estoque(a.nr_sequencia, a.nr_seq_transfusao, null, null)) dt_saida_estoque,
		c.dt_emprestimo
	from	san_emprestimo c,
		san_transfusao b,
		san_producao a
	where	nr_seq_transfusao 	= b.nr_sequencia
	and	a.nr_seq_emp_ent	= c.nr_sequencia
	union
	select	'Inutilizado',
		a.nr_sangue,
		OBTER_DESC_SAN_DERIVADO(a.nr_seq_derivado) ds_derivado,
		a.ie_tipo_sangue,
		a.ie_fator_rh,
		'',
		'',
		0,
		'',
		decode(a.ie_irradiado,'S','I','')||decode(a.ie_lavado,'S','L','')
		||decode(a.ie_filtrado,'S','F','')||decode(a.ie_aliquotado,'S','A','') ie_tratamento,
		a.nr_seq_reserva,
		a.nr_seq_derivado,
		a.nr_sequencia,
		a.nr_seq_emp_ent,
		a.nr_seq_emp_saida,
		a.nr_seq_inutil,
		a.nr_seq_transfusao,
		b.dt_inutilizacao dt_movimento,
		b.dt_inutilizacao dt_transfusao,
		trunc(san_obter_data_saida_estoque(a.nr_sequencia, a.nr_seq_transfusao, a.nr_seq_emp_saida, a.nr_seq_inutil)) dt_saida_estoque,
		x.dt_emprestimo dt_emprestimo
	from	san_emprestimo x,
		san_motivo_inutil c,
		san_inutilizacao b,
		san_producao a
	where	a.nr_seq_inutil 	= b.nr_sequencia
	and	b.nr_seq_motivo_inutil 	= c.nr_sequencia
	and	x.nr_sequencia		= a.nr_seq_emp_ent
	union
	select	'Emprestado',
		a.nr_sangue,
		OBTER_DESC_SAN_DERIVADO(a.nr_seq_derivado) ds_derivado,
		a.ie_tipo_sangue,
		a.ie_fator_rh,
		'',
		'',
		0,
		'',
		decode(a.ie_irradiado,'S','I','')||decode(a.ie_lavado,'S','L','')
		||decode(a.ie_filtrado,'S','F','')||decode(a.ie_aliquotado,'S','A','') ie_tratamento,
		a.nr_seq_reserva,
		a.nr_seq_derivado,
		a.nr_sequencia,
		a.nr_seq_emp_ent,
		a.nr_seq_emp_saida,
		a.nr_seq_inutil,
		a.nr_seq_transfusao,
		b.dt_emprestimo dt_movimento,
		null dt_transfusao,
		trunc(san_obter_data_saida_estoque(a.nr_sequencia, a.nr_seq_transfusao, a.nr_seq_emp_saida, a.nr_seq_inutil)) dt_saida_estoque,
		null dt_emprestimo
	from	san_entidade c,
		san_emprestimo b,
		san_producao a
	where	a.nr_seq_emp_saida 	= b.nr_sequencia
	and	b.nr_seq_entidade 	= c.nr_sequencia
			   ) a;
/
