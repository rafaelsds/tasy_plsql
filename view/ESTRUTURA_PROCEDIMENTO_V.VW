CREATE OR REPLACE VIEW Estrutura_Procedimento_V
AS
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.ds_procedimento,
	a.cd_grupo_proc,
	b.cd_especialidade,
	b.ds_grupo_proc,
	c.cd_area_procedimento,
	c.ds_especialidade,
	d.ds_area_procedimento,
	c.cd_especialidade_medica,
	a.ie_alta_complexidade,
	a.cd_tipo_procedimento,
	a.ie_exige_laudo,
	a.ie_classificacao,
	a.cd_setor_exclusivo,
	a.ie_tipo_despesa_tiss,
	a.ie_situacao
from	procedimento a,
	grupo_proc b,
	especialidade_proc c,
	area_procedimento d
where	a.cd_grupo_proc		= b.cd_grupo_proc
and	b.cd_especialidade		= c.cd_especialidade
and	c.cd_area_procedimento	= d.cd_area_procedimento;
/
