create or replace 
view icesp_indicador_patologia_v as
SELECT	1 ie_tipo,
	COUNT(DISTINCT b.nr_sequencia) qt,
	d.ds_amostra ds,
	a.dt_liberacao,
	c.dt_liberacao dt_liberacao_laudo,
	sum(c.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  b.NR_SEQ_MORF_DESC_ADIC
FROM 	prescr_medica a, 
	prescr_proc_peca b,
	laudo_paciente c,
	tipo_amostra_patologia d
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_laudo = c.nr_sequencia(+)
and	d.nr_sequencia = b.nr_seq_amostra_princ 
AND	b.nr_seq_peca IS NULL 
and	not exists(select 1 from prescr_proc_peca x where x.nr_seq_peca = b.nr_sequencia and b.NR_SEQ_EXAME_COMPLEMENTAR is not null)
GROUP BY a.dt_liberacao,
	c.dt_liberacao,
	d.ds_amostra,
  NR_SEQ_MORF_DESC_ADIC
union
SELECT	2 ie_tipo,
	COUNT(DISTINCT b.nr_sequencia) qt,
	d.ds_tipo_amostra ds,
	a.dt_liberacao,
	c.dt_liberacao dt_liberacao_laudo,
	sum(c.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  b.NR_SEQ_MORF_DESC_ADIC
FROM 	prescr_medica a, 
	prescr_proc_peca b,
	laudo_paciente c,
	tipo_amostra d
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_laudo = c.nr_sequencia(+)
and	d.nr_sequencia = b.nr_seq_tipo
AND	b.nr_seq_peca IS NULL 
and	not exists(select 1 from prescr_proc_peca x where x.nr_seq_peca = b.nr_sequencia and b.NR_SEQ_EXAME_COMPLEMENTAR is not null)
GROUP BY a.dt_liberacao,
	c.dt_liberacao,
	d.ds_tipo_amostra,
  b.NR_SEQ_MORF_DESC_ADIC
union
SELECT	3 ie_tipo,
	COUNT(DISTINCT a.nr_sequencia) qt,
	DECODE(NVL(a.NR_SEQ_EXAME_COMPLEMENTAR,0), 0, '', 'Exame complementar - ')||SUBSTR(obter_valor_dominio(3253,IE_TIPO_DESIGNACAO),1,255) ds,
	d.dt_liberacao,
	e.dt_liberacao dt_liberacao_laudo,
	sum(e.dT_liberacao-d.dt_liberacao) qt_dias_etapa,
  a.NR_SEQ_MORF_DESC_ADIC
FROM 	PRESCR_PROC_PECA a,
	tipo_amostra b,
	tipo_amostra_patologia c,
	prescR_medica d,
	laudo_paciente e                                
WHERE 	1 = 1            
AND	a.NR_SEQ_TIPO 	 	   	= b.nr_sequencia(+)     
AND	a.NR_SEQ_AMOSTRA_PRINC	= c.nr_sequencia(+)
AND	d.nr_prescricao = a.nr_prescricao
AND	a.nr_seq_laudo = e.nr_sequencia(+)
AND	a.ie_status <> 'C'
GROUP BY DECODE(NVL(a.NR_SEQ_EXAME_COMPLEMENTAR,0), 0, '', 'Exame complementar - ')||SUBSTR(obter_valor_dominio(3253,IE_TIPO_DESIGNACAO),1,255),
	d.dt_liberacao,
	e.dt_liberacao,
  a.NR_SEQ_MORF_DESC_ADIC
union
SELECT	4 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	decode(nvl(d.nr_Seq_superior,-1),-1,'','Laudo complementar - ')||
	CASE
	WHEN d.dt_cancelamento IS NOT NULL THEN 'Cancelado'
	WHEN d.dt_liberacao IS NOT NULL THEN 'Laudo liberado'
	WHEN d.dt_liberacao IS NULL THEN 'Laudo n�o liberado' END ds_sit_laudo,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	nvl(ie_anatomia_patologica,'S') = 'S'
GROUP BY decode(nvl(d.nr_Seq_superior,-1),-1,'','Laudo complementar - ')||
	CASE
	WHEN d.dt_cancelamento IS NOT NULL THEN 'Cancelado'
	WHEN d.dt_liberacao IS NOT NULL THEN 'Laudo liberado'
	WHEN d.dt_liberacao IS NULL THEN 'Laudo n�o liberado' END,
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	5 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	substr(obter_desc_morfologia(e.cd_morfologia, e.NR_SEQ_MORF_DESC_ADIC),1,200) ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  e.NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d,
	LAUDO_CIDO_MORFOLOGIA e
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	d.nr_sequencia = e.nr_seq_laudo
and	nvl(ie_anatomia_patologica,'S') = 'S'
GROUP BY substr(obter_desc_morfologia(e.cd_morfologia, e.NR_SEQ_MORF_DESC_ADIC),1,200),
	a.dt_liberacao,
	d.dt_liberacao,
  e.NR_SEQ_MORF_DESC_ADIC
union
select	6 ie_tipo,
	count(distinct a.nr_prescricao) qt,
	substr(obter_nome_setor(a.cd_setor_atendimento),1,200) ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
from	prescr_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d
where	a.nr_prescricao = b.nr_prescricao
and	b.nr_seq_exame = c.nr_seq_exame
and	b.nr_prescricao = d.nr_prescricao(+)
and	b.nr_sequencia = d.nr_seq_prescricao(+)
and	nvl(c.ie_anatomia_patologica,'S') = 'S'
group by a.cd_setor_atendimento,
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	7 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	DECODE(d.ie_tumor,'N','N�o tumoral','S','Tumoral benigno','M','Tumoral maligno') ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	nvl(ie_anatomia_patologica,'S') = 'S'
GROUP BY DECODE(d.ie_tumor,'N','N�o tumoral','S','Tumoral benigno','M','Tumoral maligno'),
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	8 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	substr(obter_nome_pf(e.cd_pessoa_fisica),1,200) ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d,
	usuario e
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	nvl(ie_anatomia_patologica,'S') = 'S'
and	d.nm_usuario_liberacao = e.nm_usuario
and	d.dt_liberacao is not null
GROUP BY e.cd_pessoa_fisica,
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	8 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	substr(obter_nome_pf(e.cd_medico),1,200) ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d,
	laudo_paciente_medico e
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	d.nr_sequencia = e.nr_seq_laudo
and	nvl(ie_anatomia_patologica,'S') = 'S'
and	d.dt_liberacao is not null
and	not exists (select 1 from usuario x where x.cd_pessoa_fisica = e.cd_medico and x.nm_usuario = d.nm_usuario_liberacao)
GROUP BY e.cd_medico,
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	9 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	'M�dico Patologista' ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	nvl(ie_anatomia_patologica,'S') = 'S'
and	d.dt_liberacao is not null
GROUP BY 
	a.dt_liberacao,
	d.dt_liberacao  
union
SELECT	9 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	DS_LAUDO_MEDICO_CLASSIF ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d,
	laudo_paciente_medico e,
	laudo_medico_classif f
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	d.nr_sequencia = e.nr_seq_laudo
and	e.nr_seq_laudo_medico_classif = f.nr_sequencia
and	nvl(ie_anatomia_patologica,'S') = 'S'
and	d.dt_liberacao is not null
and	not exists (select 1 from usuario x where x.cd_pessoa_fisica = e.cd_medico and x.nm_usuario = d.nm_usuario_liberacao)
GROUP BY  DS_LAUDO_MEDICO_CLASSIF,
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	10 ie_tipo,
	COUNT(DISTINCT d.nr_sequencia) qt,
	substr(obter_sexo_pf(a.cd_pessoa_fisica, 'D'),1,200) ds,
	a.dt_liberacao,
	d.dt_liberacao dt_liberacao_laudo,
	sum(d.dT_liberacao-a.dt_liberacao) qt_dias_etapa,
  0 NR_SEQ_MORF_DESC_ADIC
FROM  	prescR_medica a,
	prescr_procedimento b,
	exame_laboratorio c,
	laudo_paciente d
WHERE	a.nr_prescricao = b.nr_prescricao
AND	b.nr_seq_exame = c.nr_seq_exame
AND	b.nr_prescricao = d.nr_prescricao
AND	b.nr_sequencia = d.nr_Seq_prescricao
and	nvl(ie_anatomia_patologica,'S') = 'S'
and	DECODE(d.ie_tumor,'N','N�o tumoral','S','Tumoral benigno','M','Tumoral maligno') is not null
GROUP BY substr(obter_sexo_pf(a.cd_pessoa_fisica, 'D'),1,200),
	a.dt_liberacao,
	d.dt_liberacao
union
SELECT	11 ie_tipo,
	COUNT(DISTINCT a.nr_sequencia) qt,	
	SUBSTR(obter_descricao_padrao('PATOLOGIA_EXAME_COMPL','DS_EXAME_COMPL',a.NR_SEQ_PATO_EXAME),1,255) ds,
	d.dt_liberacao,
	e.dt_liberacao dt_liberacao_laudo,
	sum(e.dT_liberacao-d.dt_liberacao) qt_dias_etapa,
  a.NR_SEQ_MORF_DESC_ADIC
FROM 	PRESCR_PROC_PECA a,
	tipo_amostra b,
	tipo_amostra_patologia c,
	prescR_medica d,
	laudo_paciente e                                
WHERE 	1 = 1            
AND	a.NR_SEQ_TIPO 	 	   	= b.nr_sequencia(+)     
AND	a.NR_SEQ_AMOSTRA_PRINC	= c.nr_sequencia(+)
AND	d.nr_prescricao = a.nr_prescricao
AND	a.nr_seq_laudo = e.nr_sequencia(+)
AND	a.ie_status <> 'C'
and	a.NR_SEQ_EXAME_COMPLEMENTAR is not null
GROUP BY SUBSTR(obter_descricao_padrao('PATOLOGIA_EXAME_COMPL','DS_EXAME_COMPL',a.NR_SEQ_PATO_EXAME),1,255),
	d.dt_liberacao,
	e.dt_liberacao,
  a.NR_SEQ_MORF_DESC_ADIC;
/