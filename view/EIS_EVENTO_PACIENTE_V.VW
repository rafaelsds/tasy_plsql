create or replace view eis_evento_paciente_v as
select	a.*,
	b.ds_setor_atendimento,
	c.ds_evento,
	(select	nvl(max(x.ds_classificacao),obter_desc_expressao(327119))
	from	qua_classif_evento x
	where	x.ie_classificacao 	= a.ie_classificacao
	and	x.cd_empresa 	= obter_empresa_estab(a.cd_estabelecimento)) ds_classificacao,
	substr(obter_cargo_usuario(a.nm_usuario_origem), 1, 255) ds_cargo,
	substr(obter_valor_dominio(4, a.ie_sexo),1,100) ds_sexo,
	d.ds_tipo_evento,
	nvl(a.ie_classificacao,obter_desc_expressao(327119)) ie_classif,
	nvl(substr(obter_descricao_padrao('QUA_EVENTO_CLASSIF','DS_CLASSIFICACAO',a.nr_seq_classif_evento),1,255),obter_desc_expressao(327119)) ds_detalhamento,
	substr(nvl(obter_descricao_padrao('QUA_GRAVIDADE_EVENTO','DS_GRAVIDADE',a.nr_seq_gravidade),obter_desc_expressao(327119)),1,255) ds_gravidade,
	substr(obter_valor_dominio(3502,a.ie_status),1,100) ds_status,
	decode(a.ie_possui_hist,'S',obter_desc_expressao(347758),obter_desc_expressao(599787)) ds_possui_hist,
	decode(a.ie_situacao,'A',obter_desc_expressao(787005),'I',obter_desc_expressao(319239)) ds_situacao,
	c.ie_situacao ie_sit_evento,
	substr(obter_valor_dominio(2,c.ie_situacao),1,255) ds_sit_evento,
	d.ie_situacao ie_sit_tipo_evento,
	substr(obter_valor_dominio(2,d.ie_situacao),1,255) ds_sit_tipo_evento,
	substr(nvl(obter_ds_turno_evento(a.nr_seq_turno,'D'),obter_desc_expressao(327119)),1,255) ds_turno,
	decode(a.ie_liberado, 'S', obter_desc_expressao(928855), obter_desc_expressao(928857)) ds_liberado
from	qua_tipo_evento d,
	qua_evento c,
	setor_atendimento b,
	eis_evento_paciente a
where	a.cd_setor_atendimento = b.cd_setor_atendimento
and	a.nr_seq_evento = c.nr_sequencia
and	c.nr_seq_tipo = d.nr_sequencia;
/
