create or replace view HSL_KITS_ESTOQUE_V as 
select distinct c.nr_cirurgia,
		e.cd_material,
		substr(obter_desc_material(e.cd_material),1,100) ds_material,
		e.qt_material qt_prescricao,
		0 qt_conta,
		0 qt_prescr_intra,
		0 qt_conta_intra,
		null cd_medico_item,
		d.cd_kit_material,
		c.dt_inicio_real,
		c.cd_medico_cirurgiao,
		x.cd_medico,
		null nr_prescricao
from		kit_estoque d,
		kit_estoque_comp e,
		prescr_material a,
		prescr_medica b,
		cirurgia c,
		componente_kit x
where		a.nr_prescricao   = b.nr_prescricao
and		b.nr_prescricao   = c.nr_prescricao
and		a.nr_seq_kit_estoque = d.nr_sequencia
and		e.nr_seq_kit_estoque =  d.nr_sequencia
and		x.cd_kit_material = d.cd_kit_material
and		x.cd_material = a.cd_material
and		a.nr_seq_kit_estoque is not null
union
select		a.nr_cirurgia,
		a.cd_material,
		substr(obter_desc_material(a.cd_material),1,100),
		0,
		a.qt_executada,
		0,
		0,
		null,
		d.cd_kit_material,
		c.dt_inicio_real,
		c.cd_medico_cirurgiao,
		x.cd_medico,
		null
from		kit_estoque d,
		kit_estoque_comp e,
		cirurgia c,
		prescr_material b,
		material_atend_paciente a,
		componente_kit x
where		a.nr_prescricao = b.nr_prescricao
and		a.nr_sequencia_prescricao = b.nr_sequencia
and		a.nr_prescricao = c.nr_prescricao
and		b.nr_seq_kit_estoque 	= d.nr_sequencia
and		b.cd_material 	= e.cd_material
and		x.cd_kit_material = d.cd_kit_material
and		x.cd_material = b.cd_material
and		e.nr_seq_kit_estoque = d.nr_sequencia
and		b.nr_seq_kit_estoque is not null
union
select		c.nr_cirurgia,
		a.cd_material,
		substr(obter_desc_material(a.cd_material),1,100) ds_material,
		0,
		0,
		(nvl(sum(d.qt_devolucao),0) + a.qt_material),
		0,
		null,
		null cd_kit_material,
		c.dt_inicio_real,
		c.cd_medico_cirurgiao,
		null cd_medico,
		null nr_prescricao
from		prescr_material_devolucao d,
		prescr_material a,
		prescr_medica b,
		cirurgia c,
		kit_estoque k
where		a.nr_prescricao   	= b.nr_prescricao
and		b.nr_prescricao   	= c.nr_prescricao
and		a.nr_prescricao 	= d.nr_prescricao(+)
and		a.nr_sequencia 	= d.nr_seq_prescricao(+)
and		a.nr_seq_kit_estoque 	= k.nr_sequencia
and		a.nr_seq_kit_estoque is null
and		exists 	(select 1
		from prescr_material x, kit_estoque y
		where x.nr_seq_kit_estoque = y.nr_sequencia
		and y.cd_kit_material = k.cd_kit_material
		and x.nr_prescricao = a.nr_prescricao)
group by 	c.nr_cirurgia,	a.cd_material,d.qt_devolucao,a.qt_material, c.dt_inicio_real, c.cd_medico_cirurgiao,a.nr_prescricao
union
select		a.nr_cirurgia,
		a.cd_material,
		substr(obter_desc_material(a.cd_material),1,100),
		0,
		0,
		0,
		a.qt_executada,
		null,
		null cd_kit_material,
		c.dt_inicio_real,
		c.cd_medico_cirurgiao,
		null cd_medico,
		a.nr_prescricao
from		cirurgia c,
		prescr_material b,
		material_atend_paciente a,
		kit_estoque d
where		a.nr_prescricao = b.nr_prescricao and		a.nr_sequencia_prescricao = b.nr_sequencia
and		a.nr_prescricao = c.nr_prescricao and		b.nr_seq_kit_estoque is null
and		b.nr_seq_kit_estoque 	= d.nr_sequencia
and 	exists (select 1
		from prescr_material x, kit_estoque y
		where x.nr_seq_kit_estoque = y.nr_sequencia
		and y.cd_kit_material = d.cd_kit_material
		and x.nr_prescricao = a.nr_prescricao);
/
