CREATE OR REPLACE VIEW EXPORT_NURSING_HFILE_V AS
SELECT 
    c.dt_entrada DATA_REFERENCIA,
    c.NR_ATENDIMENTO,
    i.CD_FACILITY_CODE FACILITY_CODE,
    OBTER_DEPARTAMENTO_SETOR(cd_setor_usuario_atend) DEPARTMENT_CODE,
    b.cd_sistema_ant DATA_IDENTIFICATION,
    NVL(TO_CHAR(c.dt_entrada,'YYYYMMDD'),'00000000') HOSPITALIZATION_DATE,
    NVL(TO_CHAR(d.dt_avaliacao,'YYYYMMDD'),'00000000') IMPLEMENTATION_DATE,
    GET_CODE_NURSING_CARE_EXPORT(e.NR_SEQUENCIA,g.NR_SEQ_CLASSIFICACAO,d.dt_avaliacao) CODE,
    TO_CHAR(e.dt_inicio_vigencia,'YYYYMMDD') VERSION,
    ROW_NUMBER() OVER( PARTITION BY c.NR_ATENDIMENTO ORDER BY NVL(TO_CHAR(d.dt_avaliacao,'YYYYMMDD'),'00000000')) SERIAL_NUMBER,
    MAX( CASE WHEN g.CD_PAYLOAD = 1 THEN h.qt_ponto ELSE null END ) PAYLOAD_1,
    MAX( CASE WHEN g.CD_PAYLOAD = 2 THEN h.qt_ponto ELSE null END )  PAYLOAD_2,
    MAX( CASE WHEN g.CD_PAYLOAD = 3 THEN h.qt_ponto ELSE null END )  PAYLOAD_3,
    MAX( CASE WHEN g.CD_PAYLOAD = 4 THEN h.qt_ponto ELSE null END )  PAYLOAD_4,
    MAX( CASE WHEN g.CD_PAYLOAD = 5 THEN h.qt_ponto ELSE null END )  PAYLOAD_5,
    MAX( CASE WHEN g.CD_PAYLOAD = 6 THEN h.qt_ponto ELSE null END )  PAYLOAD_6,
    MAX( CASE WHEN g.CD_PAYLOAD = 7 THEN h.qt_ponto ELSE null END )  PAYLOAD_7,
    MAX( CASE WHEN g.CD_PAYLOAD = 8 THEN h.qt_ponto ELSE null END )  PAYLOAD_8,
    MAX( CASE WHEN g.CD_PAYLOAD = 9 THEN h.qt_ponto ELSE null END )  PAYLOAD_9,
    MAX( CASE WHEN g.CD_PAYLOAD = 10 THEN h.qt_ponto ELSE null END )  PAYLOAD_10,
    MAX( CASE WHEN g.CD_PAYLOAD = 11 THEN h.qt_ponto ELSE null END )  PAYLOAD_11,
    MAX( CASE WHEN g.CD_PAYLOAD = 12 THEN h.qt_ponto ELSE null END )  PAYLOAD_12,
    MAX( CASE WHEN g.CD_PAYLOAD = 13 THEN h.qt_ponto ELSE null END )  PAYLOAD_13,
    MAX( CASE WHEN g.CD_PAYLOAD = 14 THEN h.qt_ponto ELSE null END )  PAYLOAD_14,
    MAX( CASE WHEN g.CD_PAYLOAD = 15 THEN h.qt_ponto ELSE null END )  PAYLOAD_15,
    MAX( CASE WHEN g.CD_PAYLOAD = 16 THEN h.qt_ponto ELSE null END )  PAYLOAD_16,
    MAX( CASE WHEN g.CD_PAYLOAD = 17 THEN h.qt_ponto ELSE null END )  PAYLOAD_17,
    MAX( CASE WHEN g.CD_PAYLOAD = 18 THEN h.qt_ponto ELSE null END )  PAYLOAD_18,
    MAX( CASE WHEN g.CD_PAYLOAD = 19 THEN h.qt_ponto ELSE null END )  PAYLOAD_19,
    MAX( CASE WHEN g.CD_PAYLOAD = 20 THEN h.qt_ponto ELSE null END )  PAYLOAD_20
FROM PESSOA_FISICA b
INNER JOIN atendimento_paciente c ON
    b.cd_pessoa_fisica = c.cd_pessoa_fisica
INNER JOIN NURSING_CARE_ENCOUNTER d ON
    d.nr_atendimento = c.nr_atendimento
INNER JOIN nursing_care_version e ON
    d.NR_SEQ_NURSING_CARE = e.NR_SEQUENCIA
INNER JOIN nursing_care_enc_result f ON
    f.nr_seq_avaliacao = d.nr_sequencia
INNER JOIN nursing_care_item g ON
    g.NR_SEQUENCIA = f.nr_seq_item
INNER JOIN nursing_care_result h ON
    h.NR_SEQUENCIA = f.nr_seq_item_result AND
    h.nr_seq_item  = f.nr_seq_item
LEFT JOIN PESSOA_JURIDICA i ON
    i.CD_CGC = obter_cnpj_estabelecimento(wheb_usuario_pck.get_cd_estabelecimento())
GROUP BY
    c.dt_entrada,
    c.NR_ATENDIMENTO,
    OBTER_DEPARTAMENTO_SETOR(cd_setor_usuario_atend),
    b.cd_sistema_ant,
    i.CD_FACILITY_CODE,
    NVL(TO_CHAR(c.dt_entrada,'YYYYMMDD'),'00000000'),
    NVL(TO_CHAR(d.dt_avaliacao,'YYYYMMDD'),'00000000'),
    TO_CHAR(e.dt_inicio_vigencia,'YYYYMMDD'),
    GET_CODE_NURSING_CARE_EXPORT(e.NR_SEQUENCIA,g.NR_SEQ_CLASSIFICACAO,d.dt_avaliacao);
/