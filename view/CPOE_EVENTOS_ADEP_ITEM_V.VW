create or replace 
view cpoe_eventos_adep_item_v as
select	a.nr_atendimento, --- Solução
		substr(obter_desc_evento_sol(a.nr_sequencia),1,255) ds_evento,
		c.nr_sequencia nr_seq_item_cpoe,
		'SOL' ie_tipo_item,
		a.nr_prescricao, 
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_profissional,
		a.dt_alteracao dt_evento,
		a.ds_observacao,
		a.qt_volume_fase qt_dose,
		decode(a.qt_volume_fase, null, null, 'ml') cd_unidade_medida,
		a.ds_justificativa,
		NULL nr_seq_horario,
		substr(obter_desc_motivo_interrupcao(nr_seq_motivo),1,255) ds_motivo,
		to_char(a.ie_alteracao) ie_evento,
		a.nr_sequencia nr_seq_evento,
		a.dt_horario
from	prescr_solucao_evento a,
		prescr_material b,
		cpoe_material c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_solucao = b.nr_sequencia_solucao
and		b.nr_seq_mat_cpoe = c.nr_sequencia
and		c.ie_controle_tempo = 'S'
and		a.ie_tipo_solucao = 1
and		b.ie_agrupador = 4
union
select	c.nr_atendimento, --- Gasoterapia
		substr(obter_valor_dominio(2702,a.ie_evento),1,255),
		c.nr_sequencia,
		'G',
		b.nr_prescricao,
		a.nm_usuario,
		substr(obter_pf_usuario(a.nm_usuario,'N'),1,255),
		a.dt_evento,
		a.ds_observacao,
		a.qt_gasoterapia,
		a.ie_unidade_medida,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_desc_motivo_int_gas(nr_seq_motivo),1,255),
		a.ie_evento,
		a.nr_sequencia,
		null
from	prescr_gasoterapia_evento a,
		prescr_gasoterapia b,
		cpoe_gasoterapia c
where	a.nr_seq_gasoterapia = b.nr_sequencia
and		b.nr_seq_gas_cpoe = c.nr_sequencia
union
select	c.nr_atendimento, --- Dialise
		substr(obter_valor_dominio(2184,a.ie_evento),1,255),
		c.nr_sequencia,
		'DI',
		a.nr_prescricao,	
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_evento),1,255),
		a.dt_evento,
		null ds_observacao,
		a.qt_volume,
		decode(a.qt_volume, null, null, 'ml'),
		null,
		null,
		null,
		a.ie_evento,
		a.nr_sequencia,
		null
from	hd_prescricao_evento a,
		prescr_solucao b,
		cpoe_dialise c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_solucao = b.nr_seq_solucao
and		b.nr_seq_dialise_cpoe = c.nr_sequencia
union
select	a.nr_atendimento, --- Medicamentos
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'M',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		null
from	prescr_mat_alteracao a,
		prescr_material b,
		cpoe_material c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_prescricao = b.nr_sequencia
and 	b.nr_seq_mat_cpoe = c.nr_sequencia
and		nvl(a.ie_evento_valido,'S') = 'S'
and		a.ie_tipo_item = 'M'
and		b.ie_agrupador = 1
and		nvl(c.ie_controle_tempo,'N') = 'N'
and		nvl(c.ie_material,'N') = 'N'
union
select 	a.nr_atendimento, --- Procedimentos
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'P',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_mat_alteracao a,
		prescr_procedimento b,
		cpoe_procedimento c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_procedimento = b.nr_sequencia
and		b.nr_seq_proc_cpoe = c.nr_sequencia
and		a.ie_tipo_item in ('P','C','G','I','L')
union
select	a.nr_atendimento, --- Recomendações
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'R',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_mat_alteracao a,
		prescr_recomendacao b,
		cpoe_recomendacao c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_recomendacao = b.nr_sequencia
and		b.nr_seq_rec_cpoe = c.nr_sequencia
and		a.ie_tipo_item = 'R'
union
select	a.nr_atendimento, --- Dietas Orais
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'O',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_mat_alteracao a,
		prescr_dieta b,
		cpoe_dieta c
where	a.nr_prescricao = b.nr_prescricao
and		somente_numero(a.cd_item) = b.cd_dieta
and		b.nr_seq_dieta_cpoe = c.nr_sequencia
and		a.ie_tipo_item = 'D'
and		c.ie_tipo_dieta = 'O'
union
select	a.nr_atendimento, --- Suplemento Oral
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'S',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_mat_alteracao a,
		prescr_material b,
		cpoe_dieta c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_prescricao = b.nr_sequencia
and		b.nr_seq_dieta_cpoe = c.nr_sequencia
and		b.ie_agrupador = 12
and		c.ie_tipo_dieta = 'S'
and		a.ie_tipo_item = 'S'
union
SELECT	a.nr_atendimento, --- Jejum
		SUBSTR(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'J',
		a.nr_prescricao,
		a.nm_usuario,
		SUBSTR(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		SUBSTR(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
FROM	prescr_mat_alteracao a,
		rep_jejum b,
		cpoe_dieta c
WHERE	a.nr_prescricao = b.nr_prescricao
AND		a.cd_item = TO_CHAR(b.dt_inicio,'dd/mm/yyyy hh24:mi:ss') || '-' || TO_CHAR(b.dt_fim,'dd/mm/yyyy hh24:mi:ss')
AND		b.nr_seq_dieta_cpoe = c.nr_sequencia
AND		c.ie_tipo_dieta = 'J'
AND		a.ie_tipo_item = 'J'
union
select	a.nr_atendimento, --- Materiais
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'MA',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_mat_alteracao a,
		prescr_material b,
		cpoe_material c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_prescricao = b.nr_sequencia
and 	b.nr_seq_mat_cpoe = c.nr_sequencia
and		nvl(a.ie_evento_valido,'S') = 'S'
and		a.ie_tipo_item = 'MAT'
and		b.ie_agrupador = 2
and		nvl(c.ie_controle_tempo,'N') = 'N'
and		nvl(c.ie_material,'N') = 'S'
union
select	a.nr_atendimento, --- Leites e derivados
		substr(obter_valor_dominio(1620,a.ie_alteracao),1,255),
		c.nr_sequencia,
		'L',
		a.nr_prescricao,
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
		a.dt_alteracao,
		a.ds_observacao,
		a.qt_dose_adm,
		a.cd_um_dose_adm,
		a.ds_justificativa,
		a.nr_seq_horario,
		substr(obter_motivo_suspensao_prescr(a.nr_seq_motivo_susp),1,255),
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_mat_alteracao a,
		prescr_material b,
		cpoe_dieta c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_prescricao = b.nr_sequencia
and 	b.nr_seq_dieta_cpoe = c.nr_sequencia
and		nvl(a.ie_evento_valido,'S') = 'S'
and		a.ie_tipo_item = 'LD'
and		b.ie_agrupador = 16
union
select	a.nr_atendimento, --- Dieta Enteral
		substr(obter_desc_evento_sol(a.nr_sequencia),1,255) ds_evento,
		c.nr_sequencia nr_seq_item_cpoe,
		'E' ie_tipo_item,
		a.nr_prescricao, 
		a.nm_usuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_profissional,
		a.dt_alteracao dt_evento,
		a.ds_observacao,
		a.qt_volume_fase qt_dose,
		decode(a.qt_volume_fase, null, null, 'ml') cd_unidade_medida,
		a.ds_justificativa,
		NULL nr_seq_horario,
		substr(obter_desc_motivo_interrupcao(nr_seq_motivo),1,255) ds_motivo,
		to_char(a.ie_alteracao),
		a.nr_sequencia,
		a.dt_horario
from	prescr_solucao_evento a,
		prescr_material b,
		cpoe_dieta c
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_material = b.nr_sequencia
and		b.nr_seq_dieta_cpoe = c.nr_sequencia
and		a.ie_tipo_solucao = 2
and		c.ie_tipo_dieta = 'E'
and		b.ie_agrupador = 8;
/