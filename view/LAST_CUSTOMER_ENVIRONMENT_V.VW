create or replace view last_customer_environment_v as
	select	tce.nm_database,
			tce.ds_server_host,
			tce.ie_production,
			tce.ds_environment,
			tce.dt_atualizacao
	from	(select	b.nm_database,
					b.ds_server_host,
					b.ie_production,
					b.ds_environment,
					b.dt_atualizacao
			 from	tasy_customer_environment b
			 where	b.nm_database = SYS_CONTEXT('USERENV', 'DB_NAME')
			 and	b.ds_server_host = SYS_CONTEXT('USERENV', 'SERVER_HOST')
			 order by b.dt_atualizacao desc) tce
	where	rownum = 1;
/			 