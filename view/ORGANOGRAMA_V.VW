create or replace view organograma_v ( nm_diretoria_geral,
cd_diretoria_geral, nm_aprov_gerente, cd_aprov_gerente, nm_sub_gerente,
cd_sub_gerente, nr_seq_centro_custo, nm_diretor, cd_diretor,
nm_sub_diretor, cd_aprov_substituto, cd_cc_dir, nm_viajante,
cd_viajante, ie_cargo ) as
select distinct --Diretor
	nvl((select max(obter_nome_pf(x.cd_pessoa_fisica)) from departamento_philips x where x.nr_sequencia = a.nr_seq_superior),'Solange Plebani') nm_diretoria_geral,
	nvl((select max(x.cd_pessoa_fisica) from departamento_philips x where x.nr_sequencia = a.nr_seq_superior),'134') cd_diretoria_geral,
	null nm_aprov_gerente,
	null cd_aprov_gerente,
	null nm_sub_gerente,
	null cd_sub_gerente,
	null nr_seq_centro_custo,
	null nm_diretor,
	null cd_diretor,
	null nm_sub_diretor,
	null cd_aprov_substituto,
	a.nr_seq_centro_custo cd_cc_dir,
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,60) nm_viajante,
	a.cd_pessoa_fisica cd_viajante,
	'D' ie_cargo
from departamento_philips a
where a.ie_situacao = 'A'
union
select distinct --Gerente
	'Solange Plebani' nm_diretoria_geral,
	'134' cd_diretoria_geral,
	null nm_aprov_gerente,
	null cd_aprov_gerente,
	null nm_sub_gerente,
	null cd_sub_gerente,
	null nr_seq_centro_custo,
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,60) nm_diretor,
	a.cd_pessoa_fisica cd_diretor,
	substr(obter_nome_pf(obter_cd_pessoa_substituta(a.cd_pessoa_fisica)),1,60) nm_sub_diretor,
	obter_cd_pessoa_substituta(a.cd_pessoa_fisica) cd_aprov_substituto,
	a.nr_seq_centro_custo cd_cc_dir,
	substr(obter_nome_pf(c.cd_responsavel),1,60) nm_viajante,
	c.cd_responsavel cd_viajante,
	'G' ie_cargo
from departamento_philips a
    join depto_gerencia_philips b on b.nr_seq_departamento = a.nr_sequencia
    join gerencia_wheb c on c.nr_sequencia = b.nr_seq_gerencia
where a.ie_situacao = 'A'
  and c.ie_situacao = 'A'
  and a.cd_pessoa_fisica <> c.cd_responsavel
union
select distinct --Lider
	'Solange Plebani' nm_diretoria_geral,
	'134' cd_diretoria_geral,
	substr(obter_nome_pf(a.cd_responsavel),1,60) nm_aprov_gerente,
	a.cd_responsavel cd_aprov_gerente,
	substr(obter_nome_pf(obter_cd_pessoa_substituta(obter_cd_pessoa_responsavel(b.nr_seq_gerencia))),1,60) nm_sub_gerente,
	obter_cd_pessoa_substituta(obter_cd_pessoa_responsavel(b.nr_seq_gerencia)) cd_sub_gerente,
	b.nr_seq_centro_custo,
	substr(obter_nome_pf(d.cd_pessoa_fisica),1,60) nm_diretor,
	d.cd_pessoa_fisica cd_diretor,
	substr(obter_nome_pf(obter_cd_pessoa_substituta(d.cd_pessoa_fisica)),1,60) nm_sub_diretor,
	obter_cd_pessoa_substituta(d.cd_pessoa_fisica) cd_sub_diretor,
	d.nr_seq_centro_custo cd_cc_dir,
	substr(obter_nome_usuario(c.nm_usuario_lider),1,60) nm_viajante,
	trim(nvl(obter_pf_usuario(c.nm_usuario_lider,'C'),'0')) cd_viajante,
	'L' ie_cargo
from gerencia_wheb a
    join depto_gerencia_philips b on b.nr_seq_gerencia = a.nr_sequencia
    join gerencia_wheb_grupo c on c.nr_seq_gerencia = a.nr_sequencia
    join departamento_philips d on d.nr_sequencia = b.nr_seq_departamento
where a.ie_situacao = 'A'
  and c.ie_situacao = 'A'
  and d.ie_situacao = 'A'
  and trim(nvl(obter_pf_usuario(c.nm_usuario_lider,'C'),'0')) <> a.cd_responsavel
  and trim(nvl(obter_pf_usuario(c.nm_usuario_lider,'C'),'0')) <> d.cd_pessoa_fisica
union
select distinct --Funcionario
	'Solange Plebani' nm_diretoria_geral,
	'134' cd_diretoria_geral,
	substr(obter_nome_pf(a.cd_responsavel),1,60) nm_aprov_gerente,
	a.cd_responsavel cd_aprov_gerente,
	substr(obter_nome_pf(obter_cd_pessoa_substituta(obter_cd_pessoa_responsavel(b.nr_seq_gerencia))),1,60) nm_sub_gerente,
	obter_cd_pessoa_substituta(obter_cd_pessoa_responsavel(b.nr_seq_gerencia)) cd_sub_gerente,
	b.nr_seq_centro_custo,
	substr(obter_nome_pf(d.cd_pessoa_fisica),1,60) nm_diretor,
	d.cd_pessoa_fisica cd_diretor,
	substr(obter_nome_pf(obter_cd_pessoa_substituta(d.cd_pessoa_fisica)),1,60) nm_sub_diretor,
	obter_cd_pessoa_substituta(d.cd_pessoa_fisica) cd_sub_diretor,
	d.nr_seq_centro_custo cd_cc_dir,
	substr(obter_nome_usuario(e.nm_usuario_grupo),1,60) nm_viajante,
	trim(nvl(obter_pf_usuario(e.nm_usuario_grupo,'C'),'0')) cd_viajante,
	'F' ie_cargo
from gerencia_wheb a
    join depto_gerencia_philips b on b.nr_seq_gerencia = a.nr_sequencia
    join gerencia_wheb_grupo c on c.nr_seq_gerencia = a.nr_sequencia
    join departamento_philips d on d.nr_sequencia = b.nr_seq_departamento
    join gerencia_wheb_grupo_usu e on e.nr_seq_grupo = c.nr_sequencia
where a.ie_situacao = 'A'
  and c.ie_situacao = 'A'
  and d.ie_situacao = 'A'
  and nvl(c.nm_usuario_lider,'0') <> e.nm_usuario_grupo
  and nvl(e.ie_emprestimo,'N') = 'N'
  and trunc(sysdate) between trunc(e.dt_inicio) and trunc(nvl(e.dt_fim, sysdate))
order by nm_viajante;
/
