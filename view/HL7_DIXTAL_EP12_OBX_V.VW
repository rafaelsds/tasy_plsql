create or replace
view hl7_dixtal_ep12_obx_v as --Exames
select	1 ie_tipo,
	get_obx_value_type_hl7(c.ds_resultado, c.qt_resultado, c.pr_resultado) value_type,
	--c.nr_seq_exame oi_identifier,
	e.cd_exame_equip oi_identifier,
	a.nm_exame oi_text,
	subst_virgula_ponto_adic_zero(get_obx_observation_value_hl7(c.ds_resultado, c.qt_resultado, c.pr_resultado)) observation_value,
	a.nr_seq_unid_med u_identifier,
	substr(obter_lab_unid_med(a.nr_seq_unid_med,'D'),1,40) u_text,
	decode((select min(qt_minima)
	 	from exame_lab_padrao
	 	where nr_seq_exame = c.nr_seq_exame
	 	and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0)), null, null,
			(select min(qt_minima)
			 from exame_lab_padrao
	 		 where nr_seq_exame = c.nr_seq_exame
	 		 and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0))
			 ||' - '||(select max(qt_maxima)
	 		 	   from exame_lab_padrao
	 		 	   where nr_seq_exame = c.nr_seq_exame
	 			   and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0))) references_range,
	'F' observ_result_status,
	to_char(c.dt_aprovacao,'yyyymmddhh24mi') date_time_of_the_observation,
	(select	max(ds_unidade_medida) ds
	from	lab_unidade_medida x
	where	x.nr_sequencia = a.nr_seq_unid_med) Units,
	null AbnormalFlags,
	b.nr_seq_resultado,
	c.nr_seq_prescr,
	'TASY' oi_name_coding_system,
	define_proc_tuss_exame(d.nr_seq_exame, d.cd_convenio, d.cd_categoria, c.nr_seq_material, null) oi_alternate_identifier,	
	substr(obter_descricao_procedimento(define_proc_tuss_exame(d.nr_seq_exame, d.cd_convenio, d.cd_categoria, c.nr_seq_material, null), 8),1,255) oi_alternate_text,
	'TUSS' oi_alternate_coding_text,
	c.nr_sequencia nr_seq_result_item
from	exame_laboratorio a,
	exame_lab_resultado b,
	exame_lab_result_item c,
	prescr_procedimento d,
	lab_exame_equip e,
	equipamento_lab f
where	a.nr_seq_exame = c.nr_seq_exame
and	c.nr_seq_resultado = b.nr_seq_resultado
and	c.nr_seq_prescr = d.nr_sequencia
and	b.nr_prescricao = d.nr_prescricao 
and	e.nr_seq_exame = a.nr_seq_exame
and	e.cd_equipamento = f.cd_equipamento
and	f.ds_sigla = 'DIXEP12'
and	a.cd_exame is not null
union all --Antimicrobianos
select	2 ie_tipo,
	get_obx_value_type_hl7(c.ds_resultado, c.qt_resultado, c.pr_resultado) value_type,
	to_char(e.cd_medicamento) oi_identifier,
	e.ds_medicamento oi_text,
	d.ds_resultado_antib observation_value,
	a.nr_seq_unid_med u_identifier,
	substr(obter_lab_unid_med(a.nr_seq_unid_med,'D'),1,40) u_text,
	decode((select min(qt_minima)
	 	from exame_lab_padrao
	 	where nr_seq_exame = c.nr_seq_exame
	 	and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0)), null, null,
			(select min(qt_minima)
			 from exame_lab_padrao
	 		 where nr_seq_exame = c.nr_seq_exame
	 		 and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0))
			 ||' - '||(select max(qt_maxima)
	 		 	   from exame_lab_padrao
	 		 	   where nr_seq_exame = c.nr_seq_exame
	 			   and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0))) references_range,
	'F' observ_result_status,
	to_char(c.dt_aprovacao,'yyyymmddhh24mi') date_time_of_the_observation,
	(select	max(ds_unidade_medida) ds
	from	lab_unidade_medida x
	where	x.nr_sequencia = a.nr_seq_unid_med) Units,
	substr(lab_obter_result_antib_hl7(d.nr_sequencia),1,10) AbnormalFlags,
	b.nr_seq_resultado,
	c.nr_seq_prescr,
	'TASY' oi_name_coding_system,
	define_proc_tuss_exame(f.nr_seq_exame, f.cd_convenio, f.cd_categoria, c.nr_seq_material, null) oi_alternate_identifier,	
	substr(obter_descricao_procedimento(define_proc_tuss_exame(f.nr_seq_exame, f.cd_convenio, f.cd_categoria, c.nr_seq_material, null), 8),1,255) oi_alternate_text,	
	'TUSS' oi_alternate_coding_text,
	c.nr_sequencia nr_seq_result_item
from	exame_laboratorio a,
	exame_lab_resultado b,
	exame_lab_result_item c,
	exame_lab_result_antib d,
	cih_medicamento e,
	prescr_procedimento f,
	lab_exame_equip g,
	equipamento_lab h
where	a.nr_seq_exame = c.nr_seq_exame
and	c.nr_seq_resultado = b.nr_seq_resultado
and	c.nr_seq_resultado = d.nr_seq_resultado
and	c.nr_sequencia = d.nr_seq_result_item
and	d.cd_medicamento = e.cd_medicamento
and	d.ie_resultado in('S','I','R')
and	c.nr_seq_prescr = f.nr_sequencia
and	b.nr_prescricao = f.nr_prescricao 
and	g.nr_seq_exame = a.nr_seq_exame
and	g.cd_equipamento = h.cd_equipamento
and	h.ds_sigla = 'DIXEP12'
and	a.cd_exame is not null
union all --Microbianos
select	distinct
	3 ie_tipo,
	get_obx_value_type_hl7(c.ds_resultado, c.qt_resultado, c.pr_resultado) value_type,
	to_char(e.cd_microorganismo) oi_identifier,
	e.ds_microorganismo oi_text,
	null observation_value,
	a.nr_seq_unid_med u_identifier,
	substr(obter_lab_unid_med(a.nr_seq_unid_med,'D'),1,40) u_text,
	decode((select min(qt_minima)
		from exame_lab_padrao
		where nr_seq_exame = c.nr_seq_exame
		and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0)), null, null,
			(select min(qt_minima)
			 from exame_lab_padrao
			 where nr_seq_exame = c.nr_seq_exame
			 and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0))
			 ||' - '||(select max(qt_maxima)
				   from exame_lab_padrao
				   where nr_seq_exame = c.nr_seq_exame
				   and nvl(nr_seq_material,0) = nvl(c.nr_seq_material,0))) references_range,
	'F' observ_result_status,
	to_char(c.dt_aprovacao,'yyyymmddhh24mi') date_time_of_the_observation,
	(select	max(ds_unidade_medida) ds
	from	lab_unidade_medida x
	where	x.nr_sequencia = a.nr_seq_unid_med) Units,
	'' AbnormalFlags,
	b.nr_seq_resultado,
	c.nr_seq_prescr,
	'TASY' oi_name_coding_system,
	define_proc_tuss_exame(f.nr_seq_exame, f.cd_convenio, f.cd_categoria, c.nr_seq_material, null) oi_alternate_identifier,	
	substr(obter_descricao_procedimento(define_proc_tuss_exame(f.nr_seq_exame, f.cd_convenio, f.cd_categoria, c.nr_seq_material, null), 8),1,255) oi_alternate_text,
	'TUSS' oi_alternate_coding_text,
	c.nr_sequencia nr_seq_result_item
from	exame_laboratorio a,
	exame_lab_resultado b,
	exame_lab_result_item c,
	exame_lab_result_antib d,
	cih_microorganismo e,
	prescr_procedimento f,
	lab_exame_equip g,
	equipamento_lab h
where	a.nr_seq_exame = c.nr_seq_exame
and	c.nr_seq_resultado = b.nr_seq_resultado
and	c.nr_seq_resultado = d.nr_seq_resultado
and	c.nr_sequencia = d.nr_seq_result_item
and	d.cd_microorganismo = e.cd_microorganismo
and	d.ie_resultado in('S','I','R')
and	upper(c.ds_resultado) <> 'NEGATIVO'
and	c.nr_seq_prescr = f.nr_sequencia
and	b.nr_prescricao = f.nr_prescricao 
and	g.nr_seq_exame = a.nr_seq_exame
and	g.cd_equipamento = h.cd_equipamento
and	h.ds_sigla = 'DIXEP12'
and	a.cd_exame is not null
order by 1,2,3,4,5,6,7,8,9,10
/