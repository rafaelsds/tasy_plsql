create or replace view ptu_transacoes_intercambio_v
as
select 	1					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	null					nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Pedido de autorização' 		ie_tipo_transacao
from 	ptu_pedido_autorizacao
union
select 	2					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Resposta de pedido de autorização' 	ie_tipo_transacao
from 	ptu_resposta_autorizacao
union	
select 	3					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Pedido de complemento de autorização' 	ie_tipo_transacao
from 	ptu_pedido_compl_aut
union	
select 	4					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Pedido de insistência' 		ie_tipo_transacao
from 	ptu_pedido_insistencia
union	
select 	5					id_transacao,	
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Cancelamento' 				ie_tipo_transacao
from 	ptu_cancelamento
union	 
select 	6					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Resposta de auditoria' 		ie_tipo_transacao
from 	ptu_resposta_auditoria
union	
select 	7					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Erro inesperado' 			ie_tipo_transacao
from 	ptu_erro_inesperado
union	
select 	8					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Confirmação' 				ie_tipo_transacao
from 	ptu_confirmacao
union	
select 	9					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_transacao_solicitante		nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Requisição de ordem de serviço' 	ie_tipo_transacao
from 	ptu_requisicao_ordem_serv
union	
select 	10					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Resposta da ordem de serviço' 		ie_tipo_transacao
from 	ptu_resposta_req_ord_serv
union	
select 	11					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Autorização de ordem de serviço'	ie_tipo_transacao
from	ptu_autorizacao_ordem_serv
union	
select 	12					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_transacao_uni_exec			nr_seq_execucao,
	null					nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Pedido de status transação' 		ie_tipo_transacao
from 	ptu_pedido_status 
union	
select 	13					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Resposta pedido de status da transação' ie_tipo_transacao
from 	ptu_resp_pedido_status
union	
select 	14					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	nr_seq_origem				nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Decurso de prazo' 			ie_tipo_transacao
from 	ptu_decurso_prazo
union	
select 	15					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	null					nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Consulta de dados do prestador' 	ie_tipo_transacao
from 	ptu_consulta_prestador
union	
select 	16					id_transacao,
	nr_seq_guia				nr_seq_guia,
	nr_seq_requisicao			nr_seq_requisicao,
	nr_seq_execucao				nr_seq_execucao,
	null					nr_seq_origem,
	cd_unimed_executora			cd_unimed_executora,
	cd_unimed_beneficiario			cd_unimed_beneficiario,
	'Consulta de dados do beneficiário' 	ie_tipo_transacao
from 	ptu_consulta_beneficiario;
/