create or replace
view EIS_CAREPLAN_ESPECIFIC_V as
select	ds_display_name plano_de_cuidado,
		'' metas,
		'' problemas,
		'' dias,
		nr_seq_care_plan sequencia,
		ds_display_name display_name,
		trunc(p.dt_prescricao) dt_prescricao,
		sysdate inicio,
		sysdate fim,
		0  tempo_fim,
		count(*) qtd
from 	patient_care_plan a
join 	care_plan plano on a.nr_seq_care_plan = plano.nr_sequencia
join 	pe_prescricao p on p.nr_sequencia = a.nr_seq_prescr
where 	p.dt_liberacao is not null
and		p.ie_tipo= 'CP'
group by
	a.nr_seq_care_plan,
	plano.ds_display_name,
	trunc(p.dt_prescricao)
union
select	'' plano_de_cuidado,
		m.ds_display_name metas,
		'' problemas,
		'' dias,
		pcg.nr_seq_cp_goal sequencia,
		m.ds_display_name display_name,
		trunc(p.dt_prescricao) dt_prescricao,
		sysdate inicio,
		sysdate  fim,
		0  tempo_fim,
		count(*) qtd
from	pe_prescr_indicator ppi
join	patient_cp_indicator pci on pci.nr_sequencia = ppi.nr_seq_pat_cp_indicator
join	patient_cp_goal pcg on pcg.nr_sequencia = pci.nr_seq_pat_cp_goal
join	cp_goal m  on m.nr_sequencia = pcg.nr_seq_cp_goal
join	patient_care_plan c on c.nr_sequencia = pcg.nr_seq_pat_care_plan
join	pe_prescricao p on p.nr_sequencia = c.nr_seq_prescr
where	pcg.nr_seq_cp_goal is not null
and		m.ie_type_goal = 'C'
and		p.ie_tipo='CP'
and		p.dt_liberacao is not null
group by
		pcg.nr_seq_cp_goal,
		m.ds_display_name,
		trunc(p.dt_prescricao)
union
select	'' plano_de_cuidado,
		'' metas,
		p.ds_display_name problemas,
		'' dias,
		t.nr_seq_cp_problem sequencia,
		p.ds_display_name display_name,
		trunc(m.dt_prescricao) dt_prescricao,
		sysdate inicio,
		sysdate  fim,
		0  tempo_fim,
		count(*) qtd
from 	pe_prescr_problem ppr
join 	patient_cp_problem t on t.nr_sequencia = ppr.nr_seq_pat_cp_problem
join 	cp_problem p on p.nr_sequencia = t.nr_seq_cp_problem
join 	patient_care_plan c on c.nr_sequencia = t.nr_seq_pat_care_plan
join 	pe_prescricao m on m.nr_sequencia = c.nr_seq_prescr
where 	m.ie_tipo='CP'
and		m.dt_liberacao is not null
group by
		t.nr_seq_cp_problem,
		p.ds_display_name,
		trunc(m.dt_prescricao)
union
select	'' plano_de_cuidado,
		'' metas,
		'' problemas,
		plano.ds_display_name dias,
		nr_seq_care_plan sequencia,
		ds_display_name,
		sysdate  dt_prescricao,
		dt_start inicio,
		dt_end fim,
		trunc(a.dt_end- a.dt_start)tempo_fim,
		0 qtd
from	patient_care_plan a
join	care_plan plano on a.nr_seq_care_plan = plano.nr_sequencia
join	pe_prescricao p on p.nr_sequencia = a.nr_seq_prescr
where	a.dt_end is not null
and		p.dt_liberacao is not null
and		p.ie_tipo='CP'
group by
	a.nr_seq_care_plan,
	plano.ds_display_name,
	dt_start,
	dt_end;
/
