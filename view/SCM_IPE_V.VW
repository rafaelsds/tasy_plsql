Create or replace view scm_ipe_v
As
Select	1				tp_registro,
	a.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	a.cd_cgc_hospital		cd_cgc_hospital,
	b.qt_total_conta		qt_total_conta,
	count(*) - nvl(scm_buscar_qtd_taxas(a.nr_seq_protocolo, 'P', 0),0) qt_total_lancamento,
	somente_numero('03' || lpad(substr(nvl(a.cd_interno,'0'),1,6),6,'0')) cd_interno,
	substr(a.nm_hospital,1,45)	nm_hospital,
	' '				ds_espaco,
	0				ie_zeros,
	0				tp_nota,
	0				nr_folha,
	' '				cd_usuario_convenio,
	' '				cd_cid,
	''				cd_prestador,
	3				tp_prestador,
	0				cd_motivo_cobranca,
	sysdate				dt_entrada,
	sysdate				dt_alta,
	' '				nr_guia,
	0				nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0		 		nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0				vl_total_honorario,
	0				nr_linha,
	sysdate				dt_item,
	0				cd_item_convenio,
	0				qt_dias_executado,
	0				qt_ocorrencia_dia,
	0				cd_funcao_executor,
	0				ie_via_acesso,
	0				ie_urgencia,
	0				vl_honorario,
	0				vl_total_item,
	0				vl_total_matmed,
	' '				cd_brasindice,
	' '				cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_trailler b,
	w_interf_conta_header a
where	a.nr_seq_protocolo		= b.nr_seq_protocolo
and	a.nr_seq_protocolo		= c.nr_seq_protocolo
group by
	a.nr_seq_protocolo,
	a.cd_cgc_hospital,
	b.qt_total_conta,
	somente_numero('03' || lpad(substr(nvl(a.cd_interno,'0'),1,6),6,'0')),
	substr(a.nm_hospital,1,45)
union all
select
	2				tp_registro,
	a.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	''		cd_cgc_hospital,
	0				qt_total_conta,
	max(decode(c.tp_nota,75,c.nr_linha,85,c.nr_linha)) qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	decode(a.ie_tipo_atendimento,1,75,8,85,0)
					tp_nota,
	0				nr_folha,
	substr(a.cd_usuario_convenio,1,13)
					cd_usuario_convenio,
	substr(a.cd_cid_principal,1,6)	cd_cid,
	a.cd_interno			cd_prestador,
	3				tp_prestador,
	case
		when decode(a.ie_tipo_atendimento,1,75,8,85,0) = 85 then 0
		when a.dt_periodo_final < a.dt_alta then 4
		else
		decode(a.ie_tipo_atendimento,1,
			decode(a.cd_motivo_alta,1,1,2,1,4,1,6,1,7,1,8,1,
			10,4,11,4,12,4,13,4,14,4,15,4,16,4,17,4,
			18,3,27,2,28,2,29,2,30,2,31,2,32,2,33,2,34,2,
			44,1,45,1,46,1,47,1,48,1,4),4)	
	end cd_motivo_cobranca,
	a.dt_periodo_inicial			dt_entrada,
	a.dt_periodo_final			dt_alta,
	substr(a.nr_doc_convenio,1,6)	nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
		to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	(sum(decode(c.tp_nota,76,c.vl_total_item,86,c.vl_total_item,0)) +
	sum(decode(c.tp_nota,77,c.vl_total_item,87,c.vl_total_item,0)) +
	sum(decode(c.tp_nota,75,decode(c.vl_total_item,0,c.vl_honorario,c.vl_total_item),
		85,decode(c.vl_total_item,0,c.vl_honorario,c.vl_total_item),0))) vl_total_conta,
	min(decode(c.tp_nota,76,c.nr_folha,86,c.nr_folha))	nr_folha_ini_despesa,
	max(decode(c.tp_nota,76,c.nr_folha,86,c.nr_folha))	nr_folha_fim_despesa,
	sum(decode(c.tp_nota,76,c.vl_total_item,86,c.vl_total_item,0)) vl_total_despesa,
	min(decode(c.tp_nota,77,c.nr_folha,87,c.nr_folha))	nr_folha_ini_servico,
	max(decode(c.tp_nota,77,c.nr_folha,87,c.nr_folha))	nr_folha_fim_servico,
	sum(decode(c.tp_nota,77,c.vl_total_item,87,c.vl_total_item,0)) vl_total_servico,
	sum(decode(c.tp_nota,75,decode(c.vl_total_item,0,c.vl_honorario,c.vl_total_item),
		85,decode(c.vl_total_item,0,c.vl_honorario,c.vl_total_item),0)) vl_total_honorario,
	0				nr_linha,
	sysdate				dt_item,
	0				cd_item_convenio,
	0				qt_dias_executado,
	0				qt_ocorrencia_dia,
	0				cd_funcao_executor,
	0				ie_via_acesso,
	0				ie_urgencia,
	0				vl_honorario,
	0				vl_total_item,
	0				vl_total_matmed,
	' '				cd_brasindice,
	' '				cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_total b,
	w_interf_conta_cab a
where	a.nr_interno_conta		= b.nr_interno_conta
  and	a.nr_interno_conta		= c.nr_interno_conta
  and	c.cd_funcao_executor not in (41,42,43)
group by 
	a.nr_seq_protocolo,
	decode(a.ie_tipo_atendimento,1,75,8,85,0), a.cd_interno,
	substr(a.cd_usuario_convenio,1,13),
	substr(a.cd_cid_principal,1,6),
	case
		when decode(a.ie_tipo_atendimento,1,75,8,85,0) = 85 then 0
		when a.dt_periodo_final < a.dt_alta then 4
		else
		decode(a.ie_tipo_atendimento,1,
			decode(a.cd_motivo_alta,1,1,2,1,4,1,6,1,7,1,8,1,
			10,4,11,4,12,4,13,4,14,4,15,4,16,4,17,4,
			18,3,27,2,28,2,29,2,30,2,31,2,32,2,33,2,34,2,
			44,1,45,1,46,1,47,1,48,1,4),4)	
	end,
	a.dt_periodo_inicial,
	a.dt_periodo_final,
	substr(a.nr_doc_convenio,1,6),
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
		to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5)))
union all
select
	3				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	a.cd_cgc_hospital		cd_cgc_hospital,
	0				qt_total_conta,
	0 				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	c.tp_nota			tp_nota,
	0				nr_folha,
	' '				cd_usuario_convenio,
	' '				cd_cid,
	decode(c.ie_responsavel_credito,'RM',c.cd_cgc_hospital,to_char(c.cd_prestador))	cd_prestador,
	3				tp_prestador,
	0 				cd_motivo_cobranca,
	sysdate				dt_entrada,
	sysdate				dt_alta,
	' '				nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
		to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0 				vl_total_honorario,
	c.nr_linha			nr_linha,
	c.dt_item			dt_item,
	c.cd_item_convenio	cd_item_convenio,
	--decode(substr(c.cd_item,1,1),'4',1,c.qt_item)	qt_dias_executado,
	decode(c.ie_tipo_item,1,c.qt_item,3,c.qt_item,1) qt_dias_executado,
	1				qt_ocorrencia_dia,
	c.cd_funcao_executor	cd_funcao_executor,	
	c.ie_via_acesso			ie_via_acesso,
	decode(c.ie_video,'S',15,c.ie_urgencia)	ie_urgencia,
	decode(c.vl_total_item,0,c.vl_honorario,c.vl_total_item)
					vl_honorario,
	c.vl_total_item			vl_total_item,
	0				vl_total_matmed,
	' '				cd_brasindice,
	' '				cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_cab a
where	c.tp_nota	in(75,85)
  and	a.nr_interno_conta		= c.nr_interno_conta
  and	c.cd_funcao_executor not in (41,42,43)
union all
select
	4				tp_registro,
	a.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	a.cd_cgc_hospital		cd_cgc_hospital,
	0				qt_total_conta,
	(max(decode(c.tp_nota,76,c.nr_linha,86,c.nr_linha)) - nvl(scm_buscar_qtd_taxas(c.nr_interno_conta, 'C', c.nr_folha),0)) qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	decode(a.ie_tipo_atendimento,1,76,8,86,0)
					tp_nota,
	c.nr_folha			nr_folha,
	substr(a.cd_usuario_convenio,1,13)
					cd_usuario_convenio,
	substr(a.cd_cid_principal,1,6)	cd_cid,
	a.cd_interno		cd_prestador,
	3				tp_prestador,
	case
		when a.dt_periodo_final < a.dt_alta then 4
		else
		decode(a.ie_tipo_atendimento,1,
			decode(a.cd_motivo_alta,1,1,2,1,4,1,6,1,7,1,8,1,
			10,4,11,4,12,4,13,4,14,4,15,4,16,4,17,4,
			18,3,27,2,28,2,29,2,30,2,31,2,32,2,33,2,34,2,
			44,1,45,1,46,1,47,1,48,1,4),4)	
	end cd_motivo_cobranca,
	a.dt_periodo_inicial			dt_entrada,
	a.dt_periodo_final			dt_alta,
	substr(a.nr_doc_convenio,1,6)	nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
 		to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0				vl_total_honorario,
	0				nr_linha,
	sysdate				dt_item,
	0				cd_item_convenio,
	0				qt_dias_executado,
	0				qt_ocorrencia_dia,
	0				cd_funcao_executor,
	0				ie_via_acesso,
	0				ie_urgencia,
	0				vl_honorario,
	sum(c.vl_total_item)		vl_total_item,
	sum(decode(c.cd_brasindice, null, 0, decode(to_char(c.cd_item_convenio), to_char(c.cd_item), c.vl_total_item, 0)))
					vl_total_matmed,
	' '				cd_brasindice,
	' '				cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_total b,
	w_interf_conta_cab a
where	a.nr_interno_conta		= b.nr_interno_conta
and	a.nr_interno_conta		= c.nr_interno_conta
and	c.tp_nota			in(76,86)
group by
	a.nr_seq_protocolo,
	decode(a.ie_tipo_atendimento,1,76,8,86,0),
	c.nr_folha, a.cd_cgc_hospital		,
	substr(a.cd_usuario_convenio,1,13),
	substr(a.cd_cid_principal,1,6),
	a.cd_interno,
	case
		when a.dt_periodo_final < a.dt_alta then 4
		else
		decode(a.ie_tipo_atendimento,1,
			decode(a.cd_motivo_alta,1,1,2,1,4,1,6,1,7,1,8,1,
			10,4,11,4,12,4,13,4,14,4,15,4,16,4,17,4,
			18,3,27,2,28,2,29,2,30,2,31,2,32,2,33,2,34,2,
			44,1,45,1,46,1,47,1,48,1,4),4)	
	end,
	a.dt_periodo_inicial,
	a.dt_periodo_final,
	substr(a.nr_doc_convenio,1,6),
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))),
	scm_buscar_qtd_taxas(c.nr_interno_conta, 'C', c.nr_folha)
union all
select
	5				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	0 				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	c.tp_nota			tp_nota,
	c.nr_folha			nr_folha,
	' '				cd_usuario_convenio,
	' '				cd_cid,
	decode(c.ie_responsavel_credito,'RM',c.cd_cgc_hospital,c.ie_prestador) cd_prestador,
	3				tp_prestador,
	0 				cd_motivo_cobranca,
	sysdate				dt_entrada,
	sysdate				dt_alta,
	' '				nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0 				vl_total_honorario,
	c.nr_linha			nr_linha,
	c.dt_item			dt_item,
	decode(c.cd_brasindice, null, c.cd_item_convenio, decode(to_char(c.cd_item_convenio), to_char(c.cd_item), 701, c.cd_item_convenio)) cd_item_convenio,
	decode(c.ie_tipo_item,1,c.qt_item,3,c.qt_item,1)
					qt_dias_executado,
	decode(c.cd_item_convenio,8001,1,
	decode(c.ie_tipo_item,1,1,3,1,ceil(c.qt_item)))
					qt_ocorrencia_dia,
	c.cd_funcao_executor		cd_funcao_executor,	
	c.ie_via_acesso			ie_via_acesso,
	c.ie_urgencia			ie_urgencia,
	0				vl_honorario,
	c.vl_total_item			vl_total_item,
	0				vl_total_matmed,
	decode(to_char(c.cd_item_convenio), to_char(c.cd_item), lpad(trim(c.cd_brasindice),'13','0'), ' ') cd_brasindice,
	decode(to_char(c.cd_item_convenio), to_char(c.cd_item), c.cd_unidade_medida, ' ') cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_cab a
where	c.tp_nota	in(76,86)
and	c.cd_item_convenio	<> '701'
and c.cd_item_convenio not in (2054, 2062, 2070, 2089)
and	a.nr_interno_conta		= c.nr_interno_conta
union all
select
	5				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	0 				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	c.tp_nota			tp_nota,
	c.nr_folha			nr_folha,
	' '				cd_usuario_convenio,
	' '				cd_cid,
	c.cd_cgc_hospital cd_prestador,
	3				tp_prestador,
	0 				cd_motivo_cobranca,
	sysdate				dt_entrada,
	sysdate				dt_alta,
	' '				nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0 				vl_total_honorario,
	min(c.nr_linha)			nr_linha,
	min(c.dt_item)			dt_item,
	decode(c.cd_brasindice, null, c.cd_item_convenio, decode(to_char(c.cd_item_convenio), to_char(c.cd_item), 701, c.cd_item_convenio)) cd_item_convenio,
	1				qt_dias_executado,
	1				qt_ocorrencia_dia,
	0				cd_funcao_executor,	
	min(c.ie_via_acesso)			ie_via_acesso,
	min(c.ie_urgencia)			ie_urgencia,
	0				vl_honorario,
	sum(c.vl_total_item	)		vl_total_item,
	0				vl_total_matmed,
	decode(to_char(c.cd_item_convenio), to_char(c.cd_item), substr(c.cd_brasindice,1,13), ' ') cd_brasindice,
	decode(to_char(c.cd_item_convenio), to_char(c.cd_item), c.cd_unidade_medida, ' ') cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_cab a
where	c.tp_nota	in(76,86)
and	c.cd_item_convenio	<> '701'
and	a.nr_interno_conta		= c.nr_interno_conta
and c.cd_item_convenio in (2054, 2062, 2070, 2089)
group by
	c.nr_seq_protocolo,
	c.tp_nota,
	c.nr_folha,
	c.cd_cgc_hospital,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))),
	decode(c.cd_brasindice, null, c.cd_item_convenio, decode(to_char(c.cd_item_convenio), to_char(c.cd_item), 701, c.cd_item_convenio)),
	decode(to_char(c.cd_item_convenio), to_char(c.cd_item), substr(c.cd_brasindice,1,13), ' '),
	decode(to_char(c.cd_item_convenio), to_char(c.cd_item), c.cd_unidade_medida, ' ')
union all
select
	5				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	0 				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	c.tp_nota			tp_nota,
	c.nr_folha			nr_folha,
	' '				cd_usuario_convenio,
	' '				cd_cid,
	decode(c.ie_responsavel_credito,'RM',c.cd_cgc_hospital,c.ie_prestador) cd_prestador,
	3				tp_prestador,
	0 				cd_motivo_cobranca,
	sysdate				dt_entrada,
	sysdate				dt_alta,
	' '				nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0 				vl_total_honorario,
	c.nr_linha			nr_linha,
	c.dt_item			dt_item,
	c.cd_item_convenio		cd_item_convenio,
	decode(c.ie_tipo_item,1,c.qt_item,3,c.qt_item,1)
					qt_dias_executado,
	decode(c.cd_item_convenio,8001,1,
	decode(c.ie_tipo_item,1,1,3,1,ceil(c.qt_item)))
					qt_ocorrencia_dia,
	c.cd_funcao_executor		cd_funcao_executor,	
	c.ie_via_acesso			ie_via_acesso,
	c.ie_urgencia			ie_urgencia,
	0				vl_honorario,
	c.vl_total_item			vl_total_item,
	0				vl_total_matmed,
	decode(c.cd_item_convenio,'701',decode(substr(c.cd_brasindice,1,13),null,' ',lpad(trim(c.cd_brasindice),'13','0')), ' ') cd_brasindice,
	decode(c.cd_item_convenio,'701',c.cd_unidade_medida, ' ') cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_cab a
where	c.tp_nota	in(76,86)
and	c.cd_item_convenio	= '701'
  and	a.nr_interno_conta		= c.nr_interno_conta
union all
select
	6				tp_registro,
	a.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	max(decode(c.tp_nota,77,c.nr_linha,87,c.nr_linha)) qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	decode(a.ie_tipo_atendimento,1,77,8,87,0)
					tp_nota,
	c.nr_folha			nr_folha,
	substr(a.cd_usuario_convenio,1,13)
					cd_usuario_convenio,
	substr(a.cd_cid_principal,1,6)	cd_cid,
	a.cd_interno		cd_prestador,
	3				tp_prestador,
	case
		when a.dt_periodo_final < a.dt_alta then 4
		else
		decode(a.ie_tipo_atendimento,1,
			decode(a.cd_motivo_alta,1,1,2,1,4,1,6,1,7,1,8,1,
			10,4,11,4,12,4,13,4,14,4,15,4,16,4,17,4,
			18,3,27,2,28,2,29,2,30,2,31,2,32,2,33,2,34,2,
			44,1,45,1,46,1,47,1,48,1,4),4)	
	end cd_motivo_cobranca,
	a.dt_periodo_inicial			dt_entrada,
	a.dt_periodo_final			dt_alta,
	substr(a.nr_doc_convenio,1,6)	nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0				vl_total_honorario,
	0				nr_linha,
	sysdate				dt_item,
	0				cd_item_convenio,
	0				qt_dias_executado,
	0				qt_ocorrencia_dia,
	0				cd_funcao_executor,
	0				ie_via_acesso,
	0				ie_urgencia,
	0				vl_honorario,
	sum(c.vl_total_item)		vl_total_item,
	sum(decode(c.cd_item_convenio,8001,c.vl_total_item,
						32200005,c.vl_total_item,0))
					vl_total_matmed,
	' '				cd_brasindice,
	' '				cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_total b,
	w_interf_conta_cab a
where	a.nr_interno_conta		= b.nr_interno_conta
and	a.nr_interno_conta		= c.nr_interno_conta
and	c.tp_nota			in(77,87)
group by
	a.nr_seq_protocolo,
	decode(a.ie_tipo_atendimento,1,77,8,87,0),
	c.nr_folha,
	substr(a.cd_usuario_convenio,1,13),
	substr(a.cd_cid_principal,1,6),
	a.cd_interno,
	case
		when a.dt_periodo_final < a.dt_alta then 4
		else
		decode(a.ie_tipo_atendimento,1,
			decode(a.cd_motivo_alta,1,1,2,1,4,1,6,1,7,1,8,1,
			10,4,11,4,12,4,13,4,14,4,15,4,16,4,17,4,
			18,3,27,2,28,2,29,2,30,2,31,2,32,2,33,2,34,2,
			44,1,45,1,46,1,47,1,48,1,4),4)	
	end,
	a.dt_periodo_inicial,
	a.dt_periodo_final,
	substr(a.nr_doc_convenio,1,6),
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5)))
union all
select
	7				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	0 				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	c.tp_nota			tp_nota,
	c.nr_folha			nr_folha,
	' '				cd_usuario_convenio,
	' '				cd_cid,
	decode(c.ie_responsavel_credito,'RM',c.cd_cgc_hospital,c.ie_prestador) cd_prestador,
	3				tp_prestador,
	0 				cd_motivo_cobranca,
	sysdate				dt_entrada,
	sysdate				dt_alta,
	' '				nr_guia,
	nvl(to_number(substr(lpad(c.nr_seq_conta_convenio,10,'0'),6,5)),
	to_number(substr(lpad(c.nr_interno_conta,10,'0'),6,5))) nr_interno_conta,
	0				vl_total_conta,
	0				nr_folha_ini_despesa,
	0				nr_folha_fim_despesa,
	0				vl_total_despesa,
	0				nr_folha_ini_servico,
	0				nr_folha_fim_servico,
	0				vl_total_servico,
	0 				vl_total_honorario,
	c.nr_linha			nr_linha,
	c.dt_item			dt_item,
	c.cd_item_convenio		cd_item_convenio,
	1				qt_dias_executado,
	c.qt_item			qt_ocorrencia_dia,
	c.cd_funcao_executor		cd_funcao_executor,	
	c.ie_via_acesso			ie_via_acesso,
	c.ie_urgencia			ie_urgencia,
	decode(c.vl_total_item,0,c.vl_honorario,c.vl_total_item)
					vl_honorario,
	c.vl_total_item			vl_total_item,
	0				vl_total_matmed,
	' '				cd_brasindice,
	' '				cd_unidade_medida
from	w_interf_conta_item_ipe c,
	w_interf_conta_cab a
where	c.tp_nota	in(77,87)
  and	a.nr_interno_conta		= c.nr_interno_conta;
/