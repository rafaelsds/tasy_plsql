CREATE OR REPLACE VIEW CCRI_OBTER_UTI_I_V
AS
SELECT
        w.*,
        (ROUND((vl_tx_custo * qt_resumo),2)) vl_custo_calc,
	(ROUND((vl_tx_custo * (qt_resumo-vl_pac_dia)),2)) vl_custo_diff_calc
FROM    (
        SELECT  'PROC_ORIG' ds_info,
                ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
                TRUNC(a.dt_referencia,'mm') dt_mes_referencia,
                a.nr_interno_conta,
                c.nr_atendimento,
                a.vl_custo,
                a.qt_resumo,
                b.cd_centro_custo,
                b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
                ccri_obter_pac_dia(b.cd_estabelecimento, b.cd_setor_atendimento, a.dt_referencia, c.nr_atendimento) vl_pac_dia,
                TRUNC(a.dt_referencia,'mm') dt_ref,
                obter_pessoa_atendimento(c.nr_atendimento, 'C') cd_pessoa_fisica
        FROM    conta_paciente c,
                setor_atendimento b,
                conta_paciente_resumo a
        WHERE   1=1
        AND     a.nr_interno_conta = c.nr_interno_conta
        AND     a.cd_setor_atendimento = b.cd_setor_atendimento
        AND     c.ie_cancelamento IS NULL
        AND     (a.cd_procedimento,a.ie_origem_proced) IN
                (SELECT x.cd_procedimento,
                        x.ie_origem_proced
                FROM    ficha_tecnica x
                WHERE   x.cd_procedimento IS NOT NULL
                AND     x.ie_origem_proced IS NOT NULL
                AND     x.nr_seq_proc_interno IS NULL
                AND     x.cd_setor_atendimento IS NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        AND     (a.cd_procedimento,a.ie_origem_proced,a.cd_setor_atendimento) NOT IN
                (SELECT x.cd_procedimento,
                        x.ie_origem_proced,
                        x.cd_setor_atendimento
                FROM    ficha_tecnica x
                WHERE   x.cd_procedimento IS NOT NULL
                AND     x.ie_origem_proced IS NOT NULL
                AND     x.cd_setor_atendimento IS NOT NULL
                AND     x.nr_seq_proc_interno IS NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        AND     (a.nr_seq_proc_interno) NOT IN
                (SELECT x.nr_seq_proc_interno
                FROM    ficha_tecnica x
                WHERE   x.nr_seq_proc_interno IS NOT NULL
                AND     x.cd_setor_atendimento IS NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        AND     (a.nr_seq_proc_interno, a.cd_setor_atendimento) NOT IN
                (
                SELECT  x.nr_seq_proc_interno,
                        x.cd_setor_atendimento
                FROM    ficha_tecnica x
                WHERE   x.nr_seq_proc_interno IS NOT NULL
                AND     x.cd_setor_atendimento IS NOT NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        UNION   ALL
        SELECT  'PROC_ORIG_SET' ds_info,
                ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
                TRUNC(a.dt_referencia,'mm') dt_mes_referencia,
                a.nr_interno_conta,
                c.nr_atendimento,
                a.vl_custo,
                a.qt_resumo,
                b.cd_centro_custo,
                b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
                ccri_obter_pac_dia(b.cd_estabelecimento, b.cd_setor_atendimento, a.dt_referencia, c.nr_atendimento) vl_pac_dia,
                TRUNC(a.dt_referencia,'mm') dt_ref,
                obter_pessoa_atendimento(c.nr_atendimento, 'C') cd_pessoa_fisica
        FROM    conta_paciente c,
                setor_atendimento b,
                conta_paciente_resumo a
        WHERE   1=1
        AND     a.nr_interno_conta = c.nr_interno_conta
        AND     a.cd_setor_atendimento = b.cd_setor_atendimento
        AND     c.ie_cancelamento IS NULL
        AND     (a.cd_procedimento,a.ie_origem_proced,a.cd_setor_atendimento) IN
                (SELECT x.cd_procedimento,
                        x.ie_origem_proced,
                        x.cd_setor_atendimento
                FROM    ficha_tecnica x
                WHERE   x.cd_procedimento IS NOT NULL
                AND     x.ie_origem_proced IS NOT NULL
                AND     x.cd_setor_atendimento IS not NULL
                AND     x.nr_seq_proc_interno IS NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        AND     (a.nr_seq_proc_interno) NOT IN
                (SELECT x.nr_seq_proc_interno
                FROM    ficha_tecnica x
                WHERE   x.nr_seq_proc_interno IS NOT NULL
                AND     x.cd_setor_atendimento IS NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        AND     (a.nr_seq_proc_interno, a.cd_setor_atendimento) NOT IN
                (
                SELECT  x.nr_seq_proc_interno,
                        x.cd_setor_atendimento
                FROM    ficha_tecnica x
                WHERE   x.nr_seq_proc_interno IS NOT NULL
                AND     x.cd_setor_atendimento IS NOT NULL
                AND     EXISTS  (
                                SELECT  1
                                FROM    ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        UNION   ALL
        SELECT  'PROC_INT' ds_info,
                ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
                TRUNC(a.dt_referencia,'mm') dt_mes_referencia,
                a.nr_interno_conta,
                c.nr_atendimento,
                a.vl_custo,
                a.qt_resumo,
                b.cd_centro_custo,
                b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
                ccri_obter_pac_dia(b.cd_estabelecimento, b.cd_setor_atendimento, a.dt_referencia, c.nr_atendimento) vl_pac_dia,
                TRUNC(a.dt_referencia,'mm') dt_ref,
                obter_pessoa_atendimento(c.nr_atendimento, 'C') cd_pessoa_fisica
        FROM    conta_paciente c,
                setor_atendimento b,
                conta_paciente_resumo a
        WHERE   1=1
        AND     a.nr_interno_conta = c.nr_interno_conta
        AND     a.cd_setor_atendimento = b.cd_setor_atendimento
        AND     c.ie_cancelamento IS NULL
        AND     (a.nr_seq_proc_interno) IN
                (
                SELECT  x.nr_seq_proc_interno
                FROM    ficha_tecnica x
                WHERE   x.nr_seq_proc_interno IS NOT NULL
                AND     x.cd_setor_atendimento IS NULL
                AND     EXISTS  (
                                SELECT  1 FROM ficha_tecnica_componente y
                                WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                AND     y.ie_tipo_componente = '3'
                                AND     y.cd_estabelecimento = 21
                                AND     y.cd_centro_controle = 3206
                                )
                )
        AND     (a.nr_seq_proc_interno, a.cd_setor_atendimento) NOT IN
                (
                    SELECT  x.nr_seq_proc_interno,
                            x.cd_setor_atendimento
                    FROM    ficha_tecnica x
                    WHERE   x.nr_seq_proc_interno IS NOT NULL
                    AND     x.cd_setor_atendimento IS NOT NULL
                    AND     EXISTS  (
                                    SELECT  1 FROM ficha_tecnica_componente y
                                    WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                    AND     y.ie_tipo_componente = '3'
                                    AND     y.cd_estabelecimento = 21
                                    AND     y.cd_centro_controle = 3206
                                    )
                )
        UNION   ALL
        SELECT  'PROC_INT_SET' ds_info,
                ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
                TRUNC(a.dt_referencia,'mm') dt_mes_referencia,
                a.nr_interno_conta,
                c.nr_atendimento,
                a.vl_custo,
                a.qt_resumo,
                b.cd_centro_custo,
                b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
                ccri_obter_pac_dia(b.cd_estabelecimento, b.cd_setor_atendimento, a.dt_referencia, c.nr_atendimento) vl_pac_dia,
                TRUNC(a.dt_referencia,'mm') dt_ref,
                obter_pessoa_atendimento(c.nr_atendimento, 'C') cd_pessoa_fisica
        FROM    conta_paciente c,
                setor_atendimento b,
                conta_paciente_resumo a
        WHERE   1=1
        AND     a.nr_interno_conta = c.nr_interno_conta
        AND     a.cd_setor_atendimento = b.cd_setor_atendimento
        AND     c.ie_cancelamento IS NULL
        AND     (a.nr_seq_proc_interno, a.cd_setor_atendimento) IN
                (
                    SELECT  x.nr_seq_proc_interno,
                            x.cd_setor_atendimento
                    FROM    ficha_tecnica x
                    WHERE   x.nr_seq_proc_interno IS NOT NULL
                    AND     x.cd_setor_atendimento IS NOT NULL
                    AND     EXISTS  (
                                    SELECT  1
                                    FROM    ficha_tecnica_componente y
                                    WHERE   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                    AND     y.ie_tipo_componente = '3'
                                    AND     y.cd_estabelecimento = 21
                                    AND     y.cd_centro_controle = 3206
                                    )
                )
        ) w;
/