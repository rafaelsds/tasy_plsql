CREATE OR REPLACE VIEW USJRP_REL_PAD_HC_v2
AS
select	sum(b.qt_procedimento) qt_proced,
	obter_preco_procedimento( wheb_usuario_pck.get_cd_estabelecimento , obter_convenio_atendimento( e.nr_atendimento_origem ), obter_categoria_atendimento( e.nr_atendimento_origem ),
	sysdate, b.cd_procedimento, b.ie_origem_proced,
	0, 0, 0, null, null, null, null, null, null, 'P') vl_proced,
	obter_preco_procedimento( wheb_usuario_pck.get_cd_estabelecimento , obter_convenio_atendimento( e.nr_atendimento_origem ), obter_categoria_atendimento( e.nr_atendimento_origem ),
	sysdate, b.cd_procedimento, b.ie_origem_proced,
	0, 0, 0, null, null, null, null, null, null, 'P') * sum(b.qt_procedimento) vl_total_proc,
	'TOTAL' ds_total,
	d.nr_sequencia,
	c.nr_seq_profissional
from	procedimento_paciente b,
	hc_pad_controle_prof c,
	hc_pad_controle d,
	paciente_home_care e
where	c.nr_seq_controle = d.nr_sequencia
and	e.nr_sequencia = d.nr_seq_paciente
and	e.nr_atendimento_origem = b.nr_atendimento
and 	b.cd_motivo_exc_conta is null
and 	b.dt_procedimento between inicio_dia(d.DT_INICIAL) and fim_dia(d.DT_FINAL)
and	not exists (	select	1
			from	hc_pad_equipamento a,
				hc_equipamento e
			where	a.nr_seq_equipamento = e.nr_sequencia
			and	e.cd_procedimento = b.cd_procedimento
			and	e.ie_origem_proced = b.ie_origem_proced
			and	a.nr_seq_controle = c.nr_seq_controle)
group by e.nr_atendimento_origem, b.cd_procedimento, b.ie_origem_proced, d.nr_sequencia, c.nr_seq_profissional
union all
select	sum(b.qt_material),
	obter_preco_material(wheb_usuario_pck.get_cd_estabelecimento,obter_convenio_atendimento( e.nr_atendimento_origem ),obter_categoria_atendimento( e.nr_atendimento_origem ),
	sysdate,b.cd_material,0,0,0,null,null,null),
	obter_preco_material(wheb_usuario_pck.get_cd_estabelecimento,obter_convenio_atendimento( e.nr_atendimento_origem ),obter_categoria_atendimento( e.nr_atendimento_origem ),
	sysdate,b.cd_material,0,0,0,null,null,null) * sum(b.qt_material) ,
	null,
	d.nr_Sequencia,
	99999999
from	material_atend_paciente b,
	hc_pad_controle d,
	paciente_home_care e
where	e.nr_sequencia = d.nr_seq_paciente
and	e.nr_atendimento_origem = b.nr_atendimento
and  	b.cd_motivo_exc_conta is null
and 	b.dt_atendimento between inicio_dia(d.DT_INICIAL) and fim_dia(d.DT_FINAL)
group by b.cd_material, e.nr_atendimento_origem, d.nr_sequencia
union all
select	sum(c.qt_procedimento),
	obter_preco_procedimento( wheb_usuario_pck.get_cd_estabelecimento , obter_convenio_atendimento( e.nr_atendimento_origem ), obter_categoria_atendimento( e.nr_atendimento_origem ),
	sysdate, b.cd_procedimento, b.ie_origem_proced,
	0, 0, 0, null, null, null, null, null, null, 'P'),
	obter_preco_procedimento( wheb_usuario_pck.get_cd_estabelecimento , obter_convenio_atendimento( e.nr_atendimento_origem ), obter_categoria_atendimento( e.nr_atendimento_origem ),
	sysdate, b.cd_procedimento, b.ie_origem_proced,
	0, 0, 0, null, null, null, null, null, null, 'P') * sum(c.qt_procedimento),
	null,
	d.nr_sequencia,
	99999999
from	hc_pad_equipamento a,
	hc_equipamento b,
	procedimento_paciente c,
	hc_pad_controle d,
	paciente_home_care e
where	a.nr_seq_equipamento = b.nr_sequencia	
and	b.cd_procedimento = c.cd_procedimento
and	b.ie_origem_proced = c.ie_origem_proced
and	e.nr_atendimento_origem = c.nr_atendimento
and	d.nr_sequencia = a.nr_seq_controle
and	e.nr_sequencia = d.nr_seq_paciente
and  	c.cd_motivo_exc_conta is null
and 	c.dt_procedimento between inicio_dia(d.DT_INICIAL) and fim_dia(d.DT_FINAL)
group by b.cd_procedimento, b.ie_origem_proced, e.nr_atendimento_origem, d.nr_sequencia;
/
	
