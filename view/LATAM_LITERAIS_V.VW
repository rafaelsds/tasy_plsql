create or replace view latam_literais_v as
--C01
-- Ds_LABEL dos componentes
SELECT	b.nr_sequencia,
	b.nm_tabela,
	a.nm_atributo,
	trim(ds_label) ds_texto,
	trunc(a.dt_atualizacao,'dd') dt_atualizacao_nrec,
	'L' ie_tipo,
	'Label Componentes' ds_tipo
FROM	TABELA_VISAO_ATRIBUTO a,
	TABELA_VISAO b
WHERE	a.nr_sequencia = b.nr_sequencia
and	trim(ds_label) is not null
and	a.nr_seq_apresent is not null
and	b.nm_tabela not like 'PLS%'
and	length(trim(ds_label)) > 2
AND	TRUNC(a.dt_atualizacao,'dd') < TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	LENGTH(trim(somente_nao_numero(ds_label))) > 2 /*pegar somente os que possuem n�o num�rico e mais de 2 caracteres (evitar pegar 3� por exemplo)*/
union all
--C02
--DS_LABEL da GRID
SELECT	b.nr_sequencia,
	b.nm_tabela,
	a.nm_atributo,
	trim(ds_label_grid) ds_texto,
	trunc(a.dt_atualizacao,'dd') dt_atualizacao_nrec,
	'G' ie_tipo,
	'Label Grid' ds_tipo
FROM	TABELA_VISAO_ATRIBUTO a,
	TABELA_VISAO b
WHERE	a.nr_sequencia = b.nr_sequencia
AND	trim(ds_label_grid) IS NOT NULL
AND	a.nr_seq_apresent IS NOT NULL
and	length(trim(ds_label_grid)) > 2
and	b.nm_tabela not like 'PLS%'
AND	TRUNC(a.dt_atualizacao,'dd') < TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	LENGTH(trim(somente_nao_numero(ds_label_grid))) > 2 /*pegar somente os que possuem n�o num�rico e mais de 2 caracteres (evitar pegar 3� por exemplo)*/
union all
--C03
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_texto) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'WP' ie_tipo,
	'Consulta (WCPanel)' ds_tipo
FROM 	DIC_OBJETO a
WHERE	ie_tipo_objeto = 'C'
AND	a.cd_funcao not IN (4444, 9033, 99008)
and	length(trim(ds_texto)) > 2
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	 LENGTH(trim(somente_nao_numero(ds_texto))) > 2
and	trim(ds_texto)  is not null
union all
--C04
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(nm_campo_tela) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'WC' ie_tipo,
	'Atributo consulta' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(NM_CAMPO_TELA) IS NOT NULL
AND	ie_tipo_objeto = 'AC'
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	a.cd_funcao not IN (4444, 9033, 99008)
AND	nr_seq_obj_sup IS NOT NULL
union all
--C05
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(DS_TEXTO) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'TS' ie_tipo,
	'Pasta' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(DS_TEXTO) IS NOT NULL
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	a.cd_funcao not IN (4444, 9033, 99008)
AND	ie_tipo_objeto = 'P'
union all
--C06
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_texto) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'MI' ie_tipo,
	'Menu item' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(DS_TEXTO) IS NOT NULL
AND	ie_tipo_objeto = 'MI'
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	a.cd_funcao not IN (4444, 9033, 99008)
AND	nr_seq_obj_sup IS NOT NULL
union all
--C07
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_texto) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'WF' ie_tipo,
	'WFiltro' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(DS_TEXTO) IS NOT NULL
AND	a.cd_funcao not IN (4444, 9033, 99008)
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	ie_tipo_objeto = 'WF'
union all
--C08
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_label) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'OF' ie_tipo,
	'Atributos do WFiltro' ds_tipo
FROM	DIC_OBJETO_FILTRO a
WHERE	trim(DS_LABEL) IS NOT NULL
AND 	SUBSTR(ds_label,1,2) <> 'WJ'
AND 	SUBSTR(ds_label,1,3) <> '...'
AND 	SUBSTR(ds_label,1,3) <> '___'
AND 	SUBSTR(ds_label,1,5) <> '100.0'
AND 	SUBSTR(ds_label,1,6) <> '(100,0'
AND 	SUBSTR(ds_label,1,3) <> 'WDT'
AND 	SUBSTR(ds_label,1,3) <> 'WDB'
AND 	SUBSTR(ds_label,1,3) <> 'WTE'
and	length(trim(ds_label)) > 2
AND	LENGTH(trim(somente_nao_numero(ds_label))) > 2
AND	NOT EXISTS (SELECT 1 FROM dic_objeto x WHERE cd_funcao IN (4444, 9033, 99008) AND x.nr_sequencia = a.nr_seq_objeto)
union all
--C09
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_texto) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'LC' ie_tipo,
	'Consulta (LookupComboBox)' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(DS_TEXTO) IS NOT NULL
AND	ie_tipo_objeto = 'CLCB'
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	a.cd_funcao not IN (4444, 9033, 99008)
union all
--C10
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_texto) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'WS' ie_tipo,
	'Consulta (WSelecaoCB)' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(DS_TEXTO) IS NOT NULL
AND	a.cd_funcao not IN (4444, 9033, 99008)
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	ie_tipo_objeto = 'CWSCB'
union all
--C11
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_item) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'LE' ie_tipo,
	'Legenda' ds_tipo
FROM	TASY_PADRAO_COR a
WHERE	trim(ds_item) IS NOT NULL
and	length(trim(ds_item)) > 2
AND	 LENGTH(trim(somente_nao_numero(ds_item))) > 2 /*pegar somente os que possuem n�o num�rico e mais de 2 caracteres (evitar pegar 3� por exemplo)*/
union all
--C12
SELECT	cd_dominio nr_sequencia,
	vl_dominio nm_tabela,
	'' nm_atributo,
	trim(ds_valor_dominio) ds_texto,
	trunc(a.dt_atualizacao,'dd') dt_atualizacao_nrec,
	'DO' ie_tipo,
	'Dominio' ds_tipo
FROM	valor_dominio a
where	nvl(ie_situacao,'A') = 'A'
and	trim(DS_VALOR_DOMINIO) is not null
and	length(trim(DS_VALOR_DOMINIO)) > 2
AND	TRUNC(a.dt_atualizacao,'dd') < TO_DATE('03/06/2013', 'dd/mm/yyyy')
union all
--C13
SELECT	null nr_sequencia,
	nm_tabela nm_tabela,
	'' nm_atributo,
	Trim(ds_cadastro) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'CG' ie_tipo,
	'Cadastros Gerais' ds_tipo
FROM	TABELA_SISTEMA a
WHERE	IE_CADASTRO_GERAL = 'S'
AND	trim(DS_CADASTRO) IS NOT NULL
union all
--C14
SELECT	cd_funcao nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	Trim(ds_funcao) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'TA' ie_tipo,
	'Descri��o Fun��o' ds_tipo
FROM	funcao a
WHERE	nvl(ie_situacao,'A') <> 'I'
union all
--C15
SELECT	cd_funcao nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_hint) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'TH' ie_tipo,
	'Hint Fun��o' ds_tipo
FROM	funcao a
WHERE	nvl(ie_situacao,'A') <> 'I'
AND	trim(ds_hint) IS NOT NULL
union all
--C16
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(ds_opcoes) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'FR' ie_tipo,
	'WJRadioButton' ds_tipo
FROM	DIC_OBJETO_FILTRO a
WHERE	trim(ds_opcoes) IS NOT NULL
AND	LENGTH(trim(ds_opcoes)) > 2
AND	 LENGTH(trim(somente_nao_numero(ds_opcoes))) > 2
AND	 ie_componente = 'WJRB'
AND	NOT EXISTS (SELECT 1
		    FROM dic_objeto x,
			 funcao f
		    WHERE	x.cd_funcao = f.cd_funcao
		    AND 	x.nr_sequencia = a.nr_seq_objeto
		    AND		f.ie_classif_produto = 'O')
AND	NOT EXISTS (SELECT 1 FROM dic_objeto x WHERE cd_funcao IN (4444, 9033, 99008) AND x.nr_sequencia = a.nr_seq_objeto)
union all
--C17
SELECT	b.nr_sequencia,
	b.nm_tabela,
	a.nm_atributo,
	trim(DS_VALORES) ds_texto,
	trunc(a.dt_atualizacao,'dd') dt_atualizacao_nrec,
	'AR' ie_tipo,
	'Radio Group' ds_tipo
FROM	TABELA_VISAO_ATRIBUTO a,
	TABELA_VISAO b
WHERE	a.nr_sequencia = b.nr_sequencia
AND	trim(DS_VALORES) IS NOT NULL
AND	ie_componente = 'rg'
AND	a.nr_seq_apresent IS NOT NULL
AND	b.nm_tabela NOT LIKE 'PLS%'
AND	LENGTH(trim(DS_VALORES)) > 2
AND	TRUNC(a.dt_atualizacao,'dd') < TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	LENGTH(trim(somente_nao_numero(DS_VALORES))) > 2 /*pegar somente os que possuem n�o num�rico e mais de 2 caracteres (evitar pegar 3� por exemplo)*/
union all
--C18
SELECT 	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	trim(DS_INFORMACAO) ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'T' ie_tipo,
	'Texto' ds_tipo
FROM 	DIC_OBJETO a
WHERE	trim(DS_INFORMACAO) IS NOT NULL
AND	a.cd_funcao not IN (4444, 9033, 99008)
and	a.cd_funcao not in (select cd_funcao from funcao where ie_classif_produto = 'O')
AND	ie_tipo_objeto = 'T'
union all
-- Descri��o dos par�metros
-- C19
select	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	ds_parametro ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'PA' ie_tipo,
	'Descri��o do Par�metro' ds_tipo
from	funcao_parametro a
union all
--C20
select	nr_sequencia,
	'' nm_tabela,
	'' nm_atributo,
	ds_observacao ds_texto,
	trunc(a.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'PO' ie_tipo,
	'Observa��o do Par�metro' ds_tipo
from	funcao_parametro a
where	ds_observacao is not null
union all
/* Shift+F8 */
SELECT  DISTINCT
	b.nr_sequencia,
	c.nm_tabela, 
	c.nm_atributo, 
	c.ds_atributo,
	trunc(c.dt_atualizacao_nrec,'dd') dt_atualizacao_nrec,
	'S8' ie_tipo,
	'Descri��o do Shift+F8' ds_tipo
FROM    TABELA_VISAO A,
	TABELA_VISAO_ATRIBUTO B,
	TABELA_ATRIBUTO C
WHERE  	a.nr_sequencia = b.nr_sequencia
AND	b.nm_atributo = c.nm_atributo
AND	a.nm_tabela = c.nm_tabela
AND	b.nr_seq_apresent IS NOT NULL
AND	c.nm_tabela NOT LIKE 'PLS%'
AND	trim(c.ds_atributo) IS NOT NULL
union all
/* Dominios */
SELECT	a.cd_dominio nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	a.vl_dominio,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
AND	a.cd_dominio is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
--AND	b.ds_traducao IS NULL
AND	a.ie_forma_geracao = 'T'
union all
/* Fun��es */
SELECT	a.cd_funcao nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
AND	a.cd_funcao is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
--AND	b.ds_traducao IS NULL
AND	a.ie_forma_geracao = 'T'
union all
/* Legenda */
SELECT	a.nr_seq_cor_legenda nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
--AND	b.ds_traducao IS NULL
AND	a.nr_seq_cor_legenda is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	a.ie_forma_geracao = 'T'
union all
/* Sequ�ncia no dicion�rio de objetos */
SELECT	a.nr_seq_objeto nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
--AND	b.ds_traducao IS NULL
AND	a.nr_seq_objeto is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	a.ie_forma_geracao = 'T'
union all
/* Componentes do WFiltro */
SELECT	a.nr_seq_objeto_filtro nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
--AND	b.ds_traducao IS NULL
AND	a.nr_seq_objeto_filtro is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	a.ie_forma_geracao = 'T'
union all
/* Par�metros */
SELECT	a.nr_seq_parametro nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
--AND	b.ds_traducao IS NULL
AND	a.nr_seq_parametro is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	a.ie_forma_geracao = 'T'
union all
/* Tabelas de exporta��o */
SELECT	a.nr_seq_tab_exp nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
--AND	b.ds_traducao IS NULL
AND	a.nr_seq_tab_exp is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	a.ie_forma_geracao = 'T'
union all
/* Vis�es */
SELECT	a.nr_seq_visao nr_sequencia,
	a.nm_tabela,
	a.nm_atributo,
	b.ds_texto,
	TRUNC(a.dt_criacao,'dd') dt_atualizacao_nrec,
	a.ie_tipo_campo ie_tipo,
	SUBSTR(latam_obter_tipo_literal(a.ie_tipo_campo),1,255) ds_tipo
FROM	latam_tabela_visao a,
	latam_dic_palavras b
WHERE	a.nr_seq_dic_palavra = b.nr_sequencia
--AND	b.ds_traducao IS NULL
AND	a.nr_seq_visao is not null
AND	TRUNC(a.dt_criacao,'dd') >= TO_DATE('03/06/2013', 'dd/mm/yyyy')
AND	a.ie_forma_geracao = 'T';
/