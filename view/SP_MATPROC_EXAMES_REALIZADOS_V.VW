CREATE OR REPLACE VIEW SP_MATPROC_EXAMES_REALIZADOS_V ( CD_SETOR_ATENDIMENTO, 
IE_PROC_MAT, NR_ATENDIMENTO, NR_INTERNO_CONTA, NM_PACIENTE, 
VL_ITEM, VL_ITEM2, VL_ITEM3, IE_STATUS_ACERTO, 
DT_PERIODO_INICIAL, IE_CANCELAMENTO, NR_SEQ_PROTOCOLO, NR_SEQ_PROC_PACOTE, 
CD_ITEM, CD_CONVENIO_PARAMETRO, IE_TIPO_ATENDIMENTO ) AS SELECT		a.cd_setor_atendimento,
		1 ie_proc_mat,
		a.nr_atendimento,
		a.nr_interno_conta,
		SUBSTR(obter_pessoa_atendimento(a.nr_atendimento, 'N'),1,100) nm_paciente,
		SUM(DECODE(obter_se_contido(b.ie_classificacao,'1'), 'S', a.vl_procedimento, 0)) vl_item,
		SUM(DECODE(obter_se_contido(b.ie_classificacao,'2'), 'S', a.vl_procedimento, 0)) vl_item2,
		SUM(DECODE(obter_se_contido(b.ie_classificacao,'3'), 'S', a.vl_procedimento, 0)) vl_item3,
		c.ie_status_acerto,
		c.dt_periodo_inicial,
		c.ie_cancelamento,
		c.nr_seq_protocolo,
		a.nr_seq_proc_pacote,
		a.cd_procedimento cd_item,
		c.cd_convenio_parametro,
		d.ie_tipo_atendimento
FROM		procedimento_paciente a,
		procedimento b,
		atendimento_paciente d,
		conta_paciente c
WHERE		a.cd_procedimento = b.cd_procedimento
AND		a.ie_origem_proced = b.ie_origem_proced
AND		a.nr_interno_conta = c.nr_interno_conta
AND		c.nr_atendimento = d.nr_atendimento
GROUP BY		a.cd_setor_atendimento,
		a.nr_atendimento,
		a.nr_interno_conta,
		c.ie_status_acerto,
		c.dt_periodo_inicial,
		c.ie_cancelamento,
		c.nr_seq_protocolo,
		a.nr_seq_proc_pacote,
		a.cd_procedimento,
		c.cd_convenio_parametro,
		d.ie_tipo_atendimento
UNION ALL
SELECT		a.cd_setor_atendimento,
		2 ie_proc_mat,
		a.nr_atendimento,
		a.nr_interno_conta,
		SUBSTR(obter_pessoa_atendimento(a.nr_atendimento, 'N'),1,100) nm_paciente,
		SUM(DECODE(obter_se_contido(b.ie_tipo_material,'1,4'), 'S', a.vl_material, 0)) vl_item,
		SUM(DECODE(obter_se_contido(b.ie_tipo_material,'2,3'), 'S', a.vl_material, 0)) vl_item2,
		SUM(DECODE(obter_se_contido(b.ie_tipo_material,'5'), 'S', a.vl_material, 0)) vl_item3,
		c.ie_status_acerto,
		c.dt_periodo_inicial,
		c.ie_cancelamento,
		c.nr_seq_protocolo,
		a.nr_seq_proc_pacote,
		a.cd_material cd_item,
		c.cd_convenio_parametro,
		d.ie_tipo_atendimento
FROM		material_atend_paciente a,
		material b,
		atendimento_paciente d,
		conta_paciente c
WHERE		a.cd_material = b.cd_material
AND		a.nr_interno_conta = c.nr_interno_conta
AND		c.nr_atendimento = d.nr_atendimento
GROUP BY		a.cd_setor_atendimento,
		a.nr_atendimento,
		a.nr_interno_conta,
		c.ie_status_acerto,
		c.dt_periodo_inicial,
		c.ie_cancelamento,
		c.nr_seq_protocolo,
		a.nr_seq_proc_pacote,
		a.cd_material,
		c.cd_convenio_parametro,
		d.ie_tipo_atendimento
ORDER BY		1
/