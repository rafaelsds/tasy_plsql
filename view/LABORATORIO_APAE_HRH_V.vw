CREATE OR REPLACE VIEW LABORATORIO_APAE_HRH_V AS
select 10 cd_tipo_registro,
   substr(lote_obter_barras_ficha(b.nr_seq_ficha_lote, b.nr_seq_lote_entrada),1,100) ds_amostra,
   ' ' ie_reservado,
   decode (a.ie_exame_bloqueado,'R','R','N') ie_repeticao,
   ' ' qt_diluicao,
   decode(g.ie_prescr_agrupamento, 'S', to_char(a.nr_prescricao) ,' ') cd_agrupamento,
   ' ' ie_reservado2,
   max(a.dt_coleta) dt_coleta,
   decode(a.ie_urgencia, 'S', 'U', 'R') ie_prioridade,
   decode(g.ie_gerar_result_prescr,'S',lab_obter_material_integracao(a.nr_prescricao, a.nr_sequencia,'C'), nvl(d.cd_material_integracao, d.cd_material_exame)) cd_material,
   ' ' cd_instrumento,
   a.nr_prescricao,
   decode(nvl(g.ie_cons_pf_interf,'N'),'N',a.nr_prescricao,b.cd_pessoa_fisica) nr_prescr_consulta,
   a.cd_setor_coleta ie_origem,
   substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, d.nr_sequencia, e.nr_sequencia),1,1),'',c.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), ((x.linha-1)*160)+1, 160) ds_exames,
   '' nm_paciente,
   '0' dt_idade,
   sysdate dt_nascimento,
   '' ie_sexo,
   ''  ie_cor,
   '' cd_atributo,
   '' ds_valor,
   '10' ie_digito,
   ' ' ds_sigla,
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material e,
  material_exame_lab d,
  exame_laboratorio c,
  prescr_medica b,
  prescr_procedimento a,
  (SELECT ROWNUM linha FROM tabela_sistema x WHERE ROWNUM <= 12) x
where a.nr_seq_exame  = c.nr_seq_exame
and a.nr_prescricao = b.nr_prescricao
and a.cd_material_exame  = d.cd_material_exame
and a.nr_prescricao   = e.nr_prescricao
and d.nr_sequencia  = e.nr_seq_material
and g.cd_estabelecimento = b.cd_estabelecimento
and ((a.nr_seq_lab is not null) or (g.ie_padrao_amostra in ('PM','PM11','PMR11','PM13') and obter_equipamento_exame(a.nr_seq_exame,null,'MATRIX') is not null))
and g.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
--and a.dt_coleta  = e.dt_coleta
and a.dt_integracao is null
and length(substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, d.nr_sequencia, e.nr_sequencia),1,1),'',c.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), 1, 160)) > 0
and x.linha < (length(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, d.nr_sequencia, e.nr_sequencia),1,1),'',c.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo))/160)+1
and nvl(a.ie_suspenso, 'N') = 'N'
group by lote_obter_barras_ficha(b.nr_seq_ficha_lote, b.nr_seq_lote_entrada),
  decode(a.ie_urgencia, 'S', 'U', 'R'),
  decode(g.ie_gerar_result_prescr,'S',lab_obter_material_integracao(a.nr_prescricao, a.nr_sequencia,'C'), nvl(d.cd_material_integracao, d.cd_material_exame)),
  a.nr_prescricao,
  decode(nvl(g.ie_cons_pf_interf,'N'),'N',a.nr_prescricao,b.cd_pessoa_fisica),
  a.cd_setor_coleta,
  substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento,a.cd_material_exame, c.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, d.nr_sequencia, e.nr_sequencia),1,1),'',c.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), ((x.linha-1)*160)+1, 160),
  a.nr_seq_lote_externo, decode(g.ie_prescr_agrupamento, 'S', to_char(a.nr_prescricao) ,' '),decode (a.ie_exame_bloqueado,'R','R','N')
union
select 10 cd_tipo_registro,
   substr(lote_obter_barras_ficha(b.nr_seq_ficha_lote, b.nr_seq_lote_entrada),1,100) ds_amostra,
   ' ' ie_reservado,
   decode (a.ie_exame_bloqueado,'R','R','N') ie_repeticao,
   ' ' qt_diluicao,
   decode(g.ie_prescr_agrupamento, 'S', to_char(a.nr_prescricao) ,' ') cd_agrupamento,
   ' ' ie_reservado2,
   max(a.dt_coleta) dt_coleta,
   decode(a.ie_urgencia, 'S', 'U', 'R') ie_prioridade,
   decode(g.ie_gerar_result_prescr,'S',lab_obter_material_integracao(a.nr_prescricao, a.nr_sequencia,'C'), nvl(d.cd_material_integracao, d.cd_material_exame)) cd_material,
   ' ' cd_instrumento,
   a.nr_prescricao,
   decode(nvl(g.ie_cons_pf_interf,'N'),'N',a.nr_prescricao,b.cd_pessoa_fisica) nr_prescr_consulta,
   a.cd_setor_coleta ie_origem,
  substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', e.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), ((x.linha-1)*160)+1, 160) ds_exames,
   '' nm_paciente,
   '0' dt_idade,
   sysdate dt_nascimento,
   '' ie_sexo,
   ''  ie_cor,
   '' cd_atributo,
   '' ds_valor,
   '10' ie_digito,
   ' ' ds_sigla,
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material e,
  material_exame_lab d,
  exame_laboratorio c,
  prescr_medica b,
  prescr_procedimento a,
  prescr_proc_mat_item f,
  (SELECT ROWNUM linha FROM tabela_sistema x WHERE ROWNUM <= 12) x
where a.nr_seq_exame    = c.nr_seq_exame
and a.nr_prescricao = b.nr_prescricao
and a.cd_material_exame  = d.cd_material_exame
and a.dt_integracao is null
and f.nr_prescricao = a.nr_prescricao
and f.nr_seq_prescr = a.nr_sequencia
and f.dt_integracao is null
and a.nr_prescricao   = e.nr_prescricao
and d.nr_sequencia  = e.nr_seq_material
and g.cd_estabelecimento = b.cd_estabelecimento
and e.nr_sequencia = f.nr_seq_prescr_proc_mat
and g.ie_padrao_amostra IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and nvl(Obter_Equipamento_Exame(a.nr_seq_exame,null,'MATRIX'),c.cd_exame_integracao) is not null
and a.ie_suspenso <> 'S'
--and a.dt_coleta    = e.dt_coleta
and length(substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', e.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), 1, 160)) > 0
and x.linha < (length(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', e.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento))/160)+1
and nvl(a.ie_suspenso, 'N') = 'N'
group by substr(lote_obter_barras_ficha(b.nr_seq_ficha_lote, b.nr_seq_lote_entrada),1,100),
  decode(a.ie_urgencia, 'S', 'U', 'R'),
  decode(g.ie_gerar_result_prescr,'S',lab_obter_material_integracao(a.nr_prescricao, a.nr_sequencia,'C'), nvl(d.cd_material_integracao, d.cd_material_exame)),
  a.nr_prescricao,
  decode(nvl(g.ie_cons_pf_interf,'N'),'N',a.nr_prescricao,b.cd_pessoa_fisica),
  a.cd_setor_coleta,
  substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', e.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), ((x.linha-1)*160)+1, 160),
  a.nr_seq_lote_externo, decode(g.ie_prescr_agrupamento, 'S', to_char(a.nr_prescricao) ,' '),decode (a.ie_exame_bloqueado,'R','R','N')
union
select  distinct
   11 cd_tipo_registro,
   '0' ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   sysdate dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   decode(nvl(g.ie_cons_pf_interf,'N'),'N',a.nr_prescricao,c.cd_pessoa_fisica) nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   c.nm_pessoa_fisica nm_paciente,
   lpad(OBTER_IDADE(c.dt_nascimento, b.dt_prescricao,'A'),3,0)||lpad(OBTER_IDADE(c.dt_nascimento, b.dt_prescricao,'MM'),2,0)||lpad(OBTER_IDADE(c.dt_nascimento, b.dt_prescricao,'DI'),2,0) dt_idade,
   c.dt_nascimento,
   c.ie_sexo,
   Matrix_Obter_Cor_Pele(c.nr_seq_cor_pele) ie_cor,
   '' cd_atributo,
   '' ds_valor,
   '11' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and g.cd_estabelecimento = b.cd_estabelecimento
and ((a.nr_seq_lab is not null) or (g.ie_padrao_amostra in ('PM','PM11','PMR11','PM13') and obter_equipamento_exame(a.nr_seq_exame,null,'MATRIX') is not null))
and g.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.dt_integracao is null
and length(substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, d.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, e.nr_sequencia, f.nr_sequencia),1,1),'',d.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   11 cd_tipo_registro,
   '0' ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   sysdate dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   decode(nvl(g.ie_cons_pf_interf,'N'),'N',a.nr_prescricao,c.cd_pessoa_fisica) nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   c.nm_pessoa_fisica nm_paciente,
   lpad(OBTER_IDADE(c.dt_nascimento, b.dt_prescricao,'A'),3,0)||lpad(OBTER_IDADE(c.dt_nascimento, b.dt_prescricao,'MM'),2,0)||lpad(OBTER_IDADE(c.dt_nascimento, b.dt_prescricao,'DI'),2,0) dt_idade,
   c.dt_nascimento,
   c.ie_sexo,
   Matrix_Obter_Cor_Pele(c.nr_seq_cor_pele) ie_cor,
   '' cd_atributo,
   '' ds_valor,
   '11' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a,
  prescr_proc_mat_item h
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and h.nr_prescricao = a.nr_prescricao
and h.nr_seq_prescr = a.nr_sequencia
and h.dt_integracao is null
and f.nr_sequencia = h.nr_seq_prescr_proc_mat
and g.cd_estabelecimento = b.cd_estabelecimento
and nvl(Obter_Equipamento_Exame(a.nr_seq_exame,null,'MATRIX'),d.cd_exame_integracao) is not null
and g.ie_padrao_amostra IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.ie_suspenso <> 'S'
and a.dt_integracao is null
and length(substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', f.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   12 cd_tipo_registro,
   Lab_obter_amostra_prescr(g.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia) ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   null dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   0 nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   '' nm_paciente,
   '' dt_idade,
   null dt_nascimento,
   '' ie_sexo,
   '' ie_cor,
   'VOL'  cd_atributo,
   to_char(f.qt_volume) ds_valor,
   '12' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and g.cd_estabelecimento = b.cd_estabelecimento
and nvl(e.ie_volume_tempo,'N') = 'S'
and f.qt_volume > 0
and ((a.nr_seq_lab is not null) or (g.ie_padrao_amostra in ('PM','PM11','PMR11','PM13') and obter_equipamento_exame(a.nr_seq_exame,null,'MATRIX') is not null))
and g.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.dt_integracao is null
and length(substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, d.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, e.nr_sequencia, f.nr_sequencia),1,1),'',d.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   12 cd_tipo_registro,
   lab_obter_caracteres_amostra(f.cd_barras,g.ie_padrao_amostra) ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   null dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   0 nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   '' nm_paciente,
   '' dt_idade,
   null dt_nascimento,
   '' ie_sexo,
   '' ie_cor,
   'VOL'  cd_atributo,
   to_char(f.qt_volume) ds_valor,
   '12' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a,
  prescr_proc_mat_item h
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and h.nr_prescricao = a.nr_prescricao
and h.nr_seq_prescr = a.nr_sequencia
and h.dt_integracao is null
and f.nr_sequencia = h.nr_seq_prescr_proc_mat
and g.cd_estabelecimento = b.cd_estabelecimento
and nvl(e.ie_volume_tempo,'N') = 'S'
and f.qt_volume > 0
and nvl(Obter_Equipamento_Exame(a.nr_seq_exame,null,'MATRIX'),d.cd_exame_integracao) is not null
and g.ie_padrao_amostra IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.ie_suspenso <> 'S'
and a.dt_integracao is null
and length(substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', f.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   12 cd_tipo_registro,
   Lab_obter_amostra_prescr(g.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia) ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   null dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   0 nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   '' nm_paciente,
   '' dt_idade,
   null dt_nascimento,
   '' ie_sexo,
   '' ie_cor,
   'ALTURA'  cd_atributo,
  to_char(nvl(b.qt_altura_cm,c.qt_altura_cm)) ds_valor,
   '12' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and g.cd_estabelecimento = b.cd_estabelecimento
and  nvl(b.qt_altura_cm,c.qt_altura_cm) is not null
and ((a.nr_seq_lab is not null) or (g.ie_padrao_amostra in ('PM','PM11','PMR11','PM13') and obter_equipamento_exame(a.nr_seq_exame,null,'MATRIX') is not null))
and g.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.dt_integracao is null
and length(substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, d.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, e.nr_sequencia, f.nr_sequencia),1,1),'',d.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   12 cd_tipo_registro,
   lab_obter_caracteres_amostra(f.cd_barras,g.ie_padrao_amostra) ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   null dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   0 nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   '' nm_paciente,
   '' dt_idade,
   null dt_nascimento,
   '' ie_sexo,
   '' ie_cor,
   'ALTURA'  cd_atributo,
   to_char(nvl(b.qt_altura_cm,c.qt_altura_cm)) ds_valor,
   '12' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a,
  prescr_proc_mat_item h
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and h.nr_prescricao = a.nr_prescricao
and h.nr_seq_prescr = a.nr_sequencia
and h.dt_integracao is null
and f.nr_sequencia = h.nr_seq_prescr_proc_mat
and g.cd_estabelecimento = b.cd_estabelecimento
and  nvl(b.qt_altura_cm,c.qt_altura_cm) is not null
and nvl(Obter_Equipamento_Exame(a.nr_seq_exame,null,'MATRIX'),d.cd_exame_integracao) is not null
and g.ie_padrao_amostra IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.ie_suspenso <> 'S'
and a.dt_integracao is null
and length(substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', f.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   12 cd_tipo_registro,
   Lab_obter_amostra_prescr(g.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia) ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   null dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   0 nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   '' nm_paciente,
   '' dt_idade,
   null dt_nascimento,
   '' ie_sexo,
   '' ie_cor,
   'PESO'  cd_atributo,
   to_char(nvl(b.qt_peso,c.qt_peso)) ds_valor,
   '12' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and g.cd_estabelecimento = b.cd_estabelecimento
and nvl(b.qt_peso,c.qt_peso) is not null
and ((a.nr_seq_lab is not null) or (g.ie_padrao_amostra in ('PM','PM11','PMR11','PM13') and obter_equipamento_exame(a.nr_seq_exame,null,'MATRIX') is not null))
and g.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.dt_integracao is null
and length(substr(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, d.nr_seq_grupo, null, 'CI8;20;;'||
     substr(obter_compl_desc_mat_lab(a.nr_prescricao, e.nr_sequencia, f.nr_sequencia),1,1),'',d.nr_seq_grupo_imp, nr_seq_lab, g.ie_status_envio, 'MATRIX', null,nr_seq_lote_externo), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N'
union
select  distinct
   12 cd_tipo_registro,
   lab_obter_caracteres_amostra(f.cd_barras,g.ie_padrao_amostra) ds_amostra,
   '' ie_reservado,
   '' ie_repeticao,
   '' qt_diluicao,
   null cd_agrupamento,
   '' ie_reservado2,
   null dt_coleta,
   '' ie_prioridade,
   '' cd_material,
   '' cd_instrumento,
   a.nr_prescricao,
   0 nr_prescr_consulta,
   0 ie_origem,
   '' ds_exames,
   '' nm_paciente,
   '' dt_idade,
   null dt_nascimento,
   '' ie_sexo,
   '' ie_cor,
   'PESO'  cd_atributo,
   to_char(nvl(b.qt_peso,c.qt_peso)) ds_valor,
   '12' ie_digito,
   '',
   a.nr_seq_lote_externo
from   lab_parametro g,
  prescr_proc_material f,
  material_exame_lab e,
  exame_laboratorio d,
  pessoa_fisica c,
  prescr_medica b,
  prescr_procedimento a,
  prescr_proc_mat_item h
where a.nr_prescricao = b.nr_prescricao
and b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.nr_seq_exame = d.nr_seq_exame
and a.cd_material_exame = e.cd_material_exame
and a.nr_prescricao   = f.nr_prescricao
and e.nr_sequencia  = f.nr_seq_material
and h.nr_prescricao = a.nr_prescricao
and h.nr_seq_prescr = a.nr_sequencia
and h.dt_integracao is null
and f.nr_sequencia = h.nr_seq_prescr_proc_mat
and g.cd_estabelecimento = b.cd_estabelecimento
and nvl(b.qt_peso,c.qt_peso) is not null
and nvl(Obter_Equipamento_Exame(a.nr_seq_exame,null,'MATRIX'),d.cd_exame_integracao) is not null
and g.ie_padrao_amostra IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
and a.ie_suspenso <> 'S'
and a.dt_integracao is null
and length(substr(obter_lab_integr_item_matrix(a.nr_prescricao,a.cd_setor_atendimento,'CI8','',
    g.ie_status_envio,'MATRIX', f.nr_sequencia,a.nr_seq_lote_externo,b.cd_estabelecimento), 1, 160)) > 0
and nvl(a.ie_suspenso, 'N') = 'N';
/