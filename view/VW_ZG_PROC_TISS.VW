CREATE OR REPLACE FORCE VIEW vw_zg_proc_tiss (
		versao,
		id_item,
		numero_conta,
		numero_guia_prestador,
		sequencial,
		nr_seq_protocolo,
		tiss_tipo_guia,
		cd_autorizacao,
		senha_guia,
		id_guia,
		codigo_tabela_item,
		categoria_item,
		codigo_item,
		descricao_item,
		quantidade_item,
		valor_unitario_item,
		valor_total_item,
		data_atendimento_item,
		nome_executante,
		crm_executante
)
	AS
		SELECT
			'1.0.0'                                                     AS versao,
			proc_partic.nr_sequencia                                    AS id_item,
			proc_partic.nr_interno_conta                                AS numero_conta,
			conta.nr_guia_prestador                                     AS numero_guia_prestador,
			conta.nr_seq_apresentacao                                   AS sequencial,
			conta_paciente.nr_seq_protocolo                             AS nr_seq_protocolo,
			proc_partic.ie_tiss_tipo_guia                               AS tiss_tipo_guia,
			proc_partic.cd_autorizacao                                  AS cd_autorizacao,
			conta.cd_senha                                              AS senha_guia,
			proc_partic.nr_seq_guia                                     AS id_guia,
			NULL                                                        AS codigo_tabela_item,
			'Procedimentos'                                             AS categoria_item,
			proc_partic.cd_procedimento                                 AS codigo_item,
			proc_partic.ds_procedimento                                 AS descricao_item,
			proc_partic.qt_procedimento                                 AS quantidade_item,
			proc_partic.vl_unitario                                     AS valor_unitario_item,
			proc_partic.vl_procedimento                                 AS valor_total_item,
			nvl(proc_partic.dt_procedimento, proc_partic.dt_inicio_cir) AS data_atendimento_item,
			participantes.nm_medico_executor                            AS nome_executante,
			participantes.nr_crm                                        AS crm_executante
		FROM
			tiss_conta_guia conta
			INNER JOIN conta_paciente
				ON conta_paciente.nr_interno_conta = conta.nr_interno_conta
			INNER JOIN tiss_conta_proc proc_partic
				ON conta.nr_sequencia = proc_partic.nr_seq_guia
			LEFT JOIN tiss_conta_partic participantes
				ON conta.nr_sequencia = participantes.nr_seq_guia
				   AND participantes.nr_seq_proc = proc_partic.nr_sequencia
				   AND nvl(participantes.cd_cbos, 'x') = nvl(proc_partic.cd_cbos, 'x')
				   AND nvl(participantes.ie_funcao_medico, 'x') = nvl(proc_partic.ie_funcao_medico, 'x');
/