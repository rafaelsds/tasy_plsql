CREATE OR REPLACE VIEW SEPSIS_QSOFA_V AS
select	c.nr_atendimento,
		d.nr_sequencia,
		d.fld_nm_first_col_content,
		d.fld_nm_second_col_sub_info,
		d.fld_nm_second_col_content,
		null fld_nm_sub_content,
		d.fld_nm_chart_first_val || ' - ' || d.fld_nm_chart_seccond_val fld_nm_chart_tooltip,
		d.fld_nm_chart_first_val,
		d.fld_nm_chart_seccond_val,
		nvl(to_char(d.fld_nm_chart_seccond_val), '--') fld_nm_third_col_content,
		d.fld_nm_chart_seccond_val - d.fld_nm_chart_first_val nr_delta_view
from	(select b.nr_atendimento from sepsis_qsofa b group by b.nr_atendimento) c,
		(
			select	a.nr_sequencia,
					a.nr_atendimento,
					substr(obter_desc_expressao_idioma(1038996 , null, wheb_usuario_pck.get_nr_seq_idioma), 1, 255) fld_nm_second_col_content,
					null fld_nm_second_col_sub_info,
					obter_pontuacao_telehealth(a.nr_atendimento, 'sepsis_qsofa', 'nr_pontuacao', 'dt_pontuacao', 2) fld_nm_chart_first_val,
					obter_pontuacao_telehealth(a.nr_atendimento, 'sepsis_qsofa', 'nr_pontuacao', 'dt_pontuacao', 1) fld_nm_chart_seccond_val,
					a.nr_pontuacao,
					null fld_nm_first_col_content
			from sepsis_qsofa a
		) d
where	c.nr_atendimento = d.nr_atendimento
and 	d.nr_sequencia = (select max(b.nr_sequencia) from sepsis_qsofa b where b.nr_atendimento = d.nr_atendimento);
/
