create or replace view tws_pls_causa_rescisao_v as 
select	nr_sequencia,
	dt_atualizacao,
	ds_causa_rescisao,
	ie_situacao,
	ie_portal_beneficiario,
	cd_estabelecimento 
from 	pls_causa_rescisao;
/