create or replace 
view fornec_medicamentos_v as
select distinct
	a.cd_material cd_material,
	upper(trim(a.ds_material)) ds_material,
	b.vl_preco_venda vl_preco_venda,
	d.vl_total_item_nf vl_total_item_nf,
	e.DT_EMISSAO dt_emissao,
	b.cd_cgc_fornecedor cd_cgc_fornecedor,
	c.cd_cgc,
	upper(trim(nvl(c.nm_fantasia, c.ds_razao_social))) ds_razao_social,
	1 qt_total_medicamento
from
	material a,
	preco_material b,
	pessoa_juridica c,
	nota_fiscal_item d,
  	nota_fiscal e
where a.cd_material = b.cd_material
	and b.cd_cgc_fornecedor = c.cd_cgc
	and b.nr_item_nf = d.nr_item_nf
	and e.nr_sequencia = d.nr_sequencia;
/