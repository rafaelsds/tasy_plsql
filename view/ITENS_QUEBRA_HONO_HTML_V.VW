CREATE OR REPLACE VIEW ITENS_QUEBRA_HONO_HTML_V ( IE_ORDEM, 
CD_SELECIONADO, CD_N_SELECIONADO, DS, NM
 ) AS SELECT 2 ie_ordem, 'S' cd_selecionado, 'N' cd_n_selecionado, obter_desc_expressao(696048,NULL) ds, 'CD_CONVENIO' nm
FROM   dual
UNION
SELECT 3 ie_ordem, 'S' cd_selecionado, 'N' cd_n_selecionado, obter_desc_expressao(298373,NULL) ds, 'CD_SETOR_ATENDIMENTO' nm
FROM   dual
UNION
SELECT 4 ie_ordem, 'S' cd_selecionado, 'N' cd_n_selecionado, obter_desc_expressao(299425,NULL) ds, 'IE_TIPO_ATENDIMENTO' nm
FROM   dual
UNION
SELECT 5 ie_ordem, 'S' cd_selecionado, 'N' cd_n_selecionado, obter_desc_expressao(297651,NULL) ds, 'IE_RESPONSAVEL_CREDITO' nm
FROM   dual
UNION
SELECT 6 ie_ordem, 'S' cd_selecionado, 'N' cd_n_selecionado, obter_desc_expressao(284678,NULL) ds, 'CD_CATEGORIA_LOCATE' nm
FROM   dual
UNION
SELECT 7 ie_ordem, 'S' cd_selecionado, 'N' cd_n_selecionado, obter_desc_expressao(289496,NULL) ds, 'CD_ESTABELECIMENTO' nm
FROM   dual;
/