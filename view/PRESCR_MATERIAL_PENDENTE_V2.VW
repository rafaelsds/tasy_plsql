create or replace view prescr_material_pendente_v2
as
select	a.nr_prescricao,
		a.dt_prescricao,
		a.nr_atendimento,
		a.dt_primeiro_horario,
		m.nm_pessoa_fisica nm_medico,
		p.nm_pessoa_fisica nm_paciente,
		count(*) nr_itens,
		sum(decode(b.ie_urgencia,'S',1,0)) nr_itens_urgentes,
		max(decode(b.ie_urgencia,'S',1,0)) ie_item_urgente,
		a.dt_liberacao,
		a.dt_liberacao_medico,
		a.dt_liberacao_farmacia,
		a.ie_lib_farm,
		a.dt_emissao_farmacia,
		a.nm_usuario,
		a.nm_usuario_original,
		a.cd_setor_atendimento,
		s.ds_setor_atendimento,
		s.cd_local_estoque,
		b.cd_local_estoque cd_local_material,
		b.dt_emissao_setor_atend dt_emissao_setor_atend,
		a.cd_estabelecimento,
		p.nr_prontuario,
		a.nr_cirurgia
from	setor_atendimento s,
		pessoa_fisica m,
		Pessoa_fisica p,
		prescr_medica a,
		prescr_material b
where	a.nr_prescricao		= b.nr_prescricao
and		b.cd_motivo_baixa = 0
and		nvl(b.ie_medicacao_paciente,'N') = 'N'
and		a.cd_setor_atendimento 	= s.cd_setor_atendimento
and		a.cd_setor_atendimento is not null
and		nvl(b.ie_suspenso,'N') = 'N'
and		a.dt_suspensao 		is null
and		a.cd_medico		= m.cd_pessoa_fisica
and		a.cd_pessoa_fisica		= p.cd_pessoa_fisica
group by 
	a.nr_prescricao,
	a.dt_prescricao,
	a.nr_atendimento,
	a.dt_primeiro_horario,
	p.nm_pessoa_fisica,
	m.nm_pessoa_fisica,
	a.dt_liberacao,
	a.dt_liberacao_medico,
	a.dt_liberacao_farmacia,
	a.ie_lib_farm,
	a.dt_emissao_farmacia,
	a.nm_usuario,
	a.nm_usuario_original,
	a.cd_setor_atendimento,
	s.cd_local_estoque,
	s.ds_setor_atendimento,
	b.cd_local_estoque,
	b.dt_emissao_setor_atend,
	a.cd_estabelecimento,
	a.nr_cirurgia,
	p.nr_prontuario,
	A.IE_ORIGEM_INF
union all
select	a.nr_prescricao,
		a.dt_prescricao,
		a.nr_atendimento,
		a.dt_primeiro_horario,
		m.nm_pessoa_fisica nm_medico,
		p.nm_pessoa_fisica nm_paciente,
		count(*) nr_itens,
		sum(decode(b.ie_urgencia,'S',1,0)) nr_itens_urgentes,
		max(decode(b.ie_urgencia,'S',1,0)) ie_item_urgente,
		a.dt_liberacao,
		a.dt_liberacao_medico,
		a.dt_liberacao_farmacia,
		a.ie_lib_farm,
		a.dt_emissao_farmacia,
		a.nm_usuario,
		a.nm_usuario_original,
		a.cd_setor_atendimento,
		'' ds_setor_atendimento,
		null cd_local_estoque,
		b.cd_local_estoque cd_local_material,
		b.dt_emissao_setor_atend dt_emissao_setor_atend,
		a.cd_estabelecimento,
		p.nr_prontuario,
		a.nr_cirurgia
from	pessoa_fisica m,
		Pessoa_fisica p,
		prescr_medica a,
		prescr_material b
where	a.nr_prescricao		= b.nr_prescricao
and		b.cd_motivo_baixa	= 0
and		nvl(b.ie_medicacao_paciente,'N') = 'N'
and		a.cd_setor_atendimento 	is null
and		nvl(b.ie_suspenso,'N') = 'N'
and		a.dt_suspensao		is null
and		a.cd_medico		= m.cd_pessoa_fisica
and		a.cd_pessoa_fisica		= p.cd_pessoa_fisica
group by 
	a.nr_prescricao,
	a.dt_prescricao,
	a.nr_atendimento,
	a.dt_primeiro_horario,
	p.nm_pessoa_fisica,
	m.nm_pessoa_fisica,
	a.dt_liberacao,
	a.dt_liberacao_medico,
	a.dt_liberacao_farmacia,
	a.ie_lib_farm,
	a.dt_emissao_farmacia,
	a.nm_usuario,
	a.nm_usuario_original,
	a.cd_setor_atendimento,
	b.cd_local_estoque,
	a.nr_cirurgia,
	b.dt_emissao_setor_atend,
	a.cd_estabelecimento,
	p.nr_prontuario,
	a.IE_ORIGEM_INF;
/