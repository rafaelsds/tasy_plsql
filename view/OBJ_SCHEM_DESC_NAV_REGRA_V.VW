CREATE OR REPLACE 
VIEW OBJ_SCHEM_DESC_NAV_REGRA_V (NR_SEQ_OBJ_SCHEM_PARAM, CD_ESTABELECIMENTO, CD_PERFIL, NM_USUARIO_PARAM, VL_PARAMETRO, DS_CLIENTE, IE_TIPO) AS
SELECT b.nr_sequencia nr_seq_obj_schem_param,
  a.cd_estabelecimento cd_estabelecimento,
  NULL cd_perfil,
  NULL nm_usuario_param,
  NULL vl_parametro,
  a.ds_cliente,
  'E' ie_tipo
FROM obj_schem_order_estab a,
  obj_schem_order b
WHERE a.NR_SEQ_REGRA = b.NR_SEQUENCIA
UNION ALL
SELECT b.nr_sequencia nr_seq_obj_schem_param,
  NULL cd_estabelecimento,
  a.cd_perfil cd_perfil,
  NULL nm_usuario_param,
  NULL vl_parametro,
  a.ds_cliente,
  'P' ie_tipo
FROM obj_schem_order_perfil a,
  obj_schem_order b
WHERE a.NR_SEQ_REGRA = b.NR_SEQUENCIA
UNION ALL
SELECT b.nr_sequencia nr_seq_obj_schem_param,
  NULL cd_estabelecimento,
  NULL cd_perfil,
  a.nm_usuario_regra nm_usuario_param,
  NULL vl_parametro,
  a.ds_cliente,
  'U' ie_tipo
FROM obj_schem_order_usuario a,
  obj_schem_order b
WHERE a.NR_SEQ_REGRA = b.NR_SEQUENCIA;
/
