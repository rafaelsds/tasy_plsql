create or replace
view Eis_Diagnostico_v as
select	a.*,
	obter_nome_pessoa_fisica(a.cd_medico, null) nm_medico,
	obter_nome_convenio(a.cd_convenio) ds_convenio,
	obter_desc_procedencia(a.cd_procedencia) ds_procedencia,
	substr(obter_desc_cid(a.cd_doenca),1,50) ds_doenca_cid,
	obter_desc_categoria_cid(a.cd_categoria_doenca) ds_categoria_cid,
	obter_valor_dominio(12, a.ie_tipo_atendimento) ds_tipo_atendimento,
	obter_valor_dominio(17, a.ie_clinica) ds_clinica,
	obter_valor_dominio(58, a.ie_diagnostico) ds_diagnostico,
	substr(obter_nome_setor(a.cd_setor_atendimento),1,100) ds_setor_atendimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,100) ds_estabelecimento,
	obter_empresa_estab(a.cd_estabelecimento) cd_empresa,
	substr(obter_valor_dominio(13, a.ie_tipo_diagnostico),1,200) ds_tipo_diagnostico,
	to_number(obter_especialidade_medico(a.cd_medico,'C')) cd_especialidade,
	Obter_Dias_Internacao(a.nr_atendimento) qt_tempo_permanencia,
	SUBSTR(OBTER_VALOR_DOMINIO(1631,IE_PROFISSIONAL),1,255) ds_classe_prof,
	nvl(IE_PROFISSIONAL,'0') ie_classe_prof,
	substr(eis_obter_faixa_etaria(qt_idade),1,50) ie_faixa_etaria 
from	Eis_Diagnostico a;
/
