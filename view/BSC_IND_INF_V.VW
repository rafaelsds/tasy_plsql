create or replace view bsc_ind_inf_v
as
select	c.nr_sequencia nr_seq_indicador,
	c.nm_indicador,
	b.nr_sequencia nr_seq_inf,
	b.cd_estabelecimento,
	b.cd_empresa,
	b.cd_ano,
	b.cd_periodo,
	b.qt_meta,
	b.qt_real,
	b.qt_limite,
	b.qt_referencia,
	b.ie_fechado,
	b.ie_informado,
	b.nr_seq_result,
	b.dt_liberacao,
	substr(bsc_obter_periodo_inf(b.nr_seq_indicador,null,b.cd_ano,b.cd_periodo),1,20) ds_periodo,
	substr(bsc_obter_desc_periodo(nr_seq_indicador),1,20) ds_tipo_periodo,
	substr(obter_descricao_padrao('BSC_CLASSIF_RESULT','DS_COR',b.nr_seq_result),1,30) ds_cor,
	substr(obter_descricao_padrao('BSC_CLASSIF_RESULT','DS_COR_FUNDO',b.nr_seq_result),1,30) ds_cor_fundo,
	c.ie_periodo,
	c.cd_classificacao,
	c.ie_regra_intervalo,
	bsc_obter_consolidado(c.nr_sequencia, b.nr_sequencia,'M') qt_meta_consol,
	bsc_obter_consolidado(c.nr_sequencia, b.nr_sequencia,'L') qt_limite_consol,
	bsc_obter_consolidado(c.nr_sequencia, b.nr_sequencia,'R') qt_real_consol,
	b.ds_justificativa,
	nvl(c.nr_seq_tipo,0) nr_seq_tipo
from	bsc_indicador c,
	bsc_ind_inf b
where	b.nr_seq_indicador	= c.nr_sequencia;
/