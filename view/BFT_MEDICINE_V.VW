create or replace view bft_medicine_v as
select  a.nr_atendimento encounter_id,
        a.cd_material medicine_code,
        obter_desc_material(a.cd_material) medicine_description,
        a.ie_via_aplicacao medicine_route,
        a.qt_dose medicine_dose,
        a.cd_intervalo medicine_interval,
        a.nr_seq_motivo_susp medicine_suspension_code,
        obter_desc_motivo_susp(a.nr_seq_motivo_susp) medicine_susp_desc,
        a.dt_fim medicine_final_date,
        decode(a.dt_suspensao, null, 'N', 'S') change_type,
        a.dt_liberacao med_submission_date,
        nvl(a.cd_medico, a.nm_usuario) doctor_code,
        obter_dados_pf(nvl(a.cd_medico, a.nm_usuario),'PNG') doctor_given_name,
        obter_dados_pf(nvl(a.cd_medico, a.nm_usuario),'PNL') doctor_last_name,
        obter_dados_pf(nvl(a.cd_medico, a.nm_usuario),'PNM') doctor_middle_name
from    cpoe_material a
where   a.dt_liberacao is not null
union
select  b.nr_atendimento encounter_id,
        c.cd_material medicine_code,
        obter_desc_material(c.cd_material) medicine_description,
        c.ie_via_aplicacao medicine_route,
        c.qt_dose medicine_dose,
        c.cd_intervalo medicine_interval,
        b.nr_seq_motivo_canc medicine_suspension_code,
        obter_desc_motivo_canc(b.nr_seq_motivo_canc) medicine_susp_desc,
        b.dt_validade_receita medicine_final_date,
        decode(b.dt_cancelamento, null, 'N', 'S') change_type,
        b.dt_liberacao med_submission_date,
        nvl(b.cd_medico, b.nm_usuario) doctor_code,
        obter_dados_pf(nvl(b.cd_medico, b.nm_usuario),'PNG') doctor_given_name,
        obter_dados_pf(nvl(b.cd_medico, b.nm_usuario),'PNL') doctor_last_name,
        obter_dados_pf(nvl(b.cd_medico, b.nm_usuario),'PNM') doctor_middle_name
from    fa_receita_farmacia b, fa_receita_farmacia_item c
where   b.nr_sequencia = c.nr_seq_receita
and     b.dt_liberacao is not null
union
select  d.nr_atendimento encounter_id,
        d.cd_material medicine_code,
        obter_desc_material(d.cd_material) medicine_description,
        d.ie_via_aplicacao medicine_route,
        d.qt_dose medicine_dose,
        d.cd_intervalo medicine_interval,
        null medicine_suspension_code,
        d.ds_motivo||d.ds_justificativa medicine_susp_desc,
        d.dt_fim medicine_final_date,
        decode(d.dt_inativacao, null, 'N', 'S') change_type,
        d.dt_liberacao med_submission_date,
        nvl(d.cd_profissional, d.nm_usuario) doctor_code,
        obter_dados_pf(nvl(d.cd_profissional, d.nm_usuario),'PNG') doctor_given_name,
        obter_dados_pf(nvl(d.cd_profissional, d.nm_usuario),'PNL') doctor_last_name,
        obter_dados_pf(nvl(d.cd_profissional, d.nm_usuario),'PNM') doctor_middle_name
from    paciente_medic_uso d
where   d.dt_liberacao is not null;
/