create or replace view	 fsfx_sgps_tasy_estado_civil_v
as
select	vl_dominio CODIGO, 
	substr(upper(fsfx_elimina_acentos(ds_valor_dominio,'N')),1,60) DESCRICAO
from	valor_dominio 
	where cd_dominio = 5;
/
