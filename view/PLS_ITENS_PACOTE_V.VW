create or replace view pls_itens_pacote_v as
select  'P' ie_tipo,
        p.ie_classificacao ie_classificacao,
        substr(OBTER_CLASSIFICACAO_PROCED(a.cd_procedimento, a.ie_origem_proced,''),1,255) ds_classificacao,
        a.cd_procedimento cd_proc_mat,
	    a.ie_origem_proced ie_origem,
	    substr(obter_desc_estrut_proc('','','',a.cd_procedimento,a.ie_origem_proced),1,255) ds_proc_mat,
	    substr(obter_valor_dominio(30, a.ie_origem_proced),1,255) ds_origem,
	    a.qt_procedimento qt_proc_mat,
		a.ie_situacao,
		a.nr_seq_composicao,
		a.nr_seq_prestador,
		a.ie_tipo_guia
from    pls_pacote_procedimento a,
        procedimento p
where   a.ie_situacao		= 'A'
and     a.cd_procedimento	= p.cd_procedimento
and     a.ie_origem_proced	= p.ie_origem_proced
union all
select	'M' ie_tipo,
        m.ie_tipo_despesa ie_classificacao,
        substr(obter_valor_dominio(1854,m.IE_TIPO_DESPESA),1,255) ds_classificacao,
        to_number(nvl(substr(pls_obter_seq_codigo_material(a.nr_seq_material,''),1,255), a.nr_seq_material)) cd_proc_mat,
        null ie_origem,
        substr(pls_obter_desc_material(a.nr_seq_material),1,255) ds_proc_mat,
        '' ds_origem,
        a.qt_material qt_proc_mat,
		a.ie_situacao,
		a.nr_seq_composicao,
		a.nr_seq_prestador,
		a.ie_tipo_guia
from 	pls_pacote_material a,
        pls_material m
where	a.ie_situacao		= 'A'
and     a.nr_seq_material	= m.nr_sequencia
order by 1;
/