create or replace view ctb_pls_info_conta_prov_cprt_v as
select x.nr_sequencia nr_seq_protocolo,
       a.nr_sequencia nr_seq_conta,
       nvl(c.ie_ato_cooperado,b.ie_ato_cooperado) ie_ato_cooperado,
       d.ie_regulamentacao,
       d.ie_tipo_contratacao,
       d.ie_segmentacao,
       case when
               nvl(g.nr_seq_contrato, 0) <> 0 then
              (select    decode(cd_pf_estipulante, null, 'PJ', 'PF')
               from      pls_contrato
               where     nr_sequencia    = g.nr_seq_contrato
               and       rownum  = 1)
	else case when   nvl(g.nr_seq_intercambio, 0) <> 0 then
              (select    decode(cd_pessoa_fisica, null, 'PJ', 'PF')
               from      pls_intercambio
               where     nr_sequencia    = g.nr_seq_intercambio
               and 	 rownum  = 1)
        else   null
               end
       end ie_tipo_contrato,
       nvl(c.nr_seq_grupo_ans,b.nr_seq_grupo_ans) nr_seq_grupo_ans,
       substr(obter_valor_dominio(3796 ,(select r.ie_tipo_despesa
					from	pls_conta_medica_resumo r
					where	r.nr_sequencia = c.nr_seq_conta_resumo
					and	r.nr_seq_conta = a.nr_sequencia
					union
					select	p.ie_tipo_despesa
					from	pls_conta_proc p
					where	p.nr_sequencia = b.nr_seq_conta_proc
					and	nvl(c.nr_seq_conta_resumo,0) = 0
					union
					select	m.ie_tipo_despesa
					from	pls_conta_mat m
					where	m.nr_sequencia = b.nr_seq_conta_mat
					and	nvl(c.nr_seq_conta_resumo,0) = 0)), 1, 255) ds_tipo_despesa,
       nvl(a.ie_tipo_segurado,g.ie_tipo_segurado) ie_tipo_segurado,
	pls_obter_comp_repasse(m.dt_atendimento,g.nr_sequencia,x.nr_seq_congenere, 'C') ie_tipo_compartilhamento,
	pls_obter_comp_repasse(m.dt_atendimento,g.nr_sequencia,x.nr_seq_congenere, 'R') ie_tipo_repasse, 
	(select	max(nr_seq_tipo_prestador)
	from	pls_prestador
	where	nr_sequencia	= decode(h.ie_prestador_codificacao, 'E',a.nr_seq_prestador_exec, 
								     'S',a.nr_seq_prestador, 
								     'A',x.nr_seq_prestador,
								     'P',c.nr_seq_prestador_pgto)) nr_seq_tipo_prestador,
       (select	max(ie_tipo_relacao)
       from	pls_prestador
       where	nr_sequencia	= decode(h.ie_prestador_codificacao, 'E',a.nr_seq_prestador_exec, 
								     'S',a.nr_seq_prestador, 
								     'A',x.nr_seq_prestador,
								     'P',c.nr_seq_prestador_pgto)) ie_tipo_relacao,
       i.nr_contrato,
       c.nr_lote_contabil_prov,
       c.cd_conta_cred_provisao cd_conta_prov_cred,
       c.cd_conta_deb_provisao cd_conta_prov_deb,
       c.nr_sequencia nr_seq_resumo,
       null nr_seq_mat,
       b.nr_sequencia nr_seq_proc,
       c.nr_sequencia nr_documento,
       g.nr_sequencia nr_seq_segurado,
       x.nr_seq_congenere
from	pls_conta 			a,
	pls_conta_coparticipacao	b,
	pls_conta_copartic_contab	c,
	pls_plano			d,
	pls_segurado 			g,
       	pls_esquema_contabil 		h,
       	pls_contrato 			i,
	pls_protocolo_conta 		x,
	pls_conta_mat			m
where	a.nr_sequencia			= b.nr_seq_conta
and	b.nr_sequencia			= c.nr_seq_conta_copartic
and	a.nr_sequencia			= b.nr_seq_conta
and	x.nr_sequencia			= a.nr_seq_protocolo
and	d.nr_sequencia			= a.nr_seq_plano
and   	h.nr_sequencia 			= c.nr_seq_esquema_prov	
and    	i.nr_sequencia(+) 		= g.nr_seq_contrato
and    	g.nr_sequencia 			= a.nr_seq_segurado
and	b.nr_seq_conta_mat 		= m.nr_sequencia
union all
select x.nr_sequencia nr_seq_protocolo,
       a.nr_sequencia nr_seq_conta,
       nvl(c.ie_ato_cooperado,b.ie_ato_cooperado) ie_ato_cooperado,
       d.ie_regulamentacao,
       d.ie_tipo_contratacao,
       d.ie_segmentacao,
       case when
               nvl(g.nr_seq_contrato, 0) <> 0 then
              (select    decode(cd_pf_estipulante, null, 'PJ', 'PF')
               from      pls_contrato
               where     nr_sequencia    = g.nr_seq_contrato
               and       rownum  = 1)
	else case when   nvl(g.nr_seq_intercambio, 0) <> 0 then
              (select    decode(cd_pessoa_fisica, null, 'PJ', 'PF')
               from      pls_intercambio
               where     nr_sequencia    = g.nr_seq_intercambio
               and 	 rownum  = 1)
        else   null
               end
       end ie_tipo_contrato,
       nvl(c.nr_seq_grupo_ans,b.nr_seq_grupo_ans) nr_seq_grupo_ans,
       substr(obter_valor_dominio(3796 ,(select r.ie_tipo_despesa
					from	pls_conta_medica_resumo r
					where	r.nr_sequencia = c.nr_seq_conta_resumo
					and	r.nr_seq_conta = a.nr_sequencia
					union
					select	p.ie_tipo_despesa
					from	pls_conta_proc p
					where	p.nr_sequencia = b.nr_seq_conta_proc
					and	nvl(c.nr_seq_conta_resumo,0) = 0
					union
					select	m.ie_tipo_despesa
					from	pls_conta_mat m
					where	m.nr_sequencia = b.nr_seq_conta_mat
					and	nvl(c.nr_seq_conta_resumo,0) = 0)), 1, 255) ds_tipo_despesa,
       nvl(a.ie_tipo_segurado,g.ie_tipo_segurado) ie_tipo_segurado,
	pls_obter_comp_repasse(p.dt_procedimento,g.nr_sequencia,x.nr_seq_congenere, 'C') ie_tipo_compartilhamento,
	pls_obter_comp_repasse(p.dt_procedimento,g.nr_sequencia,x.nr_seq_congenere, 'R') ie_tipo_repasse, 
	(select	max(nr_seq_tipo_prestador)
	from	pls_prestador
	where	nr_sequencia	= decode(h.ie_prestador_codificacao, 'E',a.nr_seq_prestador_exec, 
								     'S',a.nr_seq_prestador, 
								     'A',x.nr_seq_prestador,
								     'P',c.nr_seq_prestador_pgto)) nr_seq_tipo_prestador,
       (select	max(ie_tipo_relacao)
       from	pls_prestador
       where	nr_sequencia	= decode(h.ie_prestador_codificacao, 'E',a.nr_seq_prestador_exec, 
								     'S',a.nr_seq_prestador, 
								     'A',x.nr_seq_prestador,
								     'P',c.nr_seq_prestador_pgto)) ie_tipo_relacao,
       i.nr_contrato,
       c.nr_lote_contabil_prov,
       c.cd_conta_cred_provisao cd_conta_prov_cred,
       c.cd_conta_deb_provisao cd_conta_prov_deb,
       c.nr_sequencia nr_seq_resumo,
       null nr_seq_mat,
       b.nr_sequencia nr_seq_proc,
       c.nr_sequencia nr_documento,
       g.nr_sequencia nr_seq_segurado,
       x.nr_seq_congenere
from	pls_conta 			a,
	pls_conta_coparticipacao	b,
	pls_conta_copartic_contab	c,
	pls_plano			d,
	pls_segurado 			g,
       	pls_esquema_contabil 		h,
       	pls_contrato 			i,
	pls_protocolo_conta 		x,
	pls_conta_proc			p
where	a.nr_sequencia			= b.nr_seq_conta
and	b.nr_sequencia			= c.nr_seq_conta_copartic
and	a.nr_sequencia			= b.nr_seq_conta
and	x.nr_sequencia			= a.nr_seq_protocolo
and	d.nr_sequencia			= a.nr_seq_plano
and   	h.nr_sequencia 			= c.nr_seq_esquema_prov	
and    	i.nr_sequencia(+) 		= g.nr_seq_contrato
and    	g.nr_sequencia 			= a.nr_seq_segurado
and	b.nr_Seq_conta_proc		= p.nr_sequencia;
/
