create or replace
view tabela_visao_utilizacao_v as
select	a.nm_tabela,
	a.nm_atributo,
	b.nm_funcao,
	b.ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	tabela_visao_utilizacao b,
	w_dic_expressao_uso a		
where	b.nr_seq_visao	= a.nm_indice
and	a.nm_tabela	= 'TABELA_VISAO_ATRIBUTO'
union
select	a.nm_tabela,
	a.nm_atributo,
	b.nm_funcao,
	b.ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	tabela_visao_utilizacao b,
	w_dic_expressao_uso a		
where	b.nr_seq_objeto	= a.nm_indice
and	a.nm_tabela	= 'DIC_OBJETO'
union
select	a.nm_tabela,
	a.nm_atributo,
	substr(obter_nome_funcao(b.cd_funcao),1,80),
	('Texto/Mensagem:' || substr(obter_desc_expressao(b.CD_EXP_INFORMACAO),1,80)) ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	dic_objeto b,
		w_dic_expressao_uso a
where	b.nr_sequencia	= a.nm_indice
and	b.IE_TIPO_OBJETO = 'T'
and	a.nm_tabela	= 'DIC_OBJETO'
union
select	a.nm_tabela,
	a.nm_atributo,
	substr(obter_nome_funcao(x.cd_funcao),1,80),
	('Grid: '|| substr(obter_desc_expressao(x.CD_EXP_TEXTO),1,80)) ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	dic_objeto x,
		dic_objeto b,
		w_dic_expressao_uso a
where	b.nr_sequencia	= a.nm_indice
and	b.nr_seq_obj_sup = x.nr_sequencia
and	x.IE_TIPO_OBJETO = 'C'
and	a.nm_tabela	= 'DIC_OBJETO'
union
select	a.nm_tabela,
	a.nm_atributo,
	substr(obter_nome_funcao(x.cd_funcao),1,80),
	('Bot�o direito:' || substr(obter_desc_expressao(b.CD_EXP_TEXTO),1,80)) ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	dic_objeto x,
		dic_objeto b,
		w_dic_expressao_uso a
where	b.nr_sequencia	= a.nm_indice
and	b.nr_seq_obj_sup = x.nr_sequencia
and	x.IE_TIPO_OBJETO = 'M'
and	a.nm_tabela	= 'DIC_OBJETO'
union
select	a.nm_tabela,
	a.nm_atributo,
	('Dicion�rio de Dados/Fun��o: '|| substr(obter_nome_funcao(x.cd_funcao),1,80)),
	'Fun��o/Par�metro' ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	funcao_parametro x,
		w_dic_expressao_uso a		
where	x.cd_funcao		= a.nm_indice
and		a.nm_tabela		= 'FUNCAO_PARAMETRO'
union
select	a.nm_tabela,
		a.nm_atributo,
		substr(obter_nome_funcao(f.cd_funcao),1,80),
		'Filtro' ds_pasta_objeto,
		a.cd_expressao,
		a.nm_usuario
from	dic_objeto f,
		dic_objeto_filtro x,
		w_dic_expressao_uso a		
where	x.nr_sequencia	= a.nm_indice
and		f.nr_sequencia	= x.nr_seq_objeto
and		a.nm_tabela		= 'DIC_OBJETO_FILTRO'
union
select	a.nm_tabela,
	a.nm_atributo,
	('Shift+F11: ' || substr(obter_desc_expressao(CD_EXP_CADASTRO),1,80)) nm_funcao,
	'' ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	tabela_sistema b,
	w_dic_expressao_uso a	
where	a.nm_indice	= b.nm_tabela
and	a.nm_tabela	= 'TABELA_ATRIBUTO'
and	b.CD_EXP_CADASTRO is not null
and	((b.NM_TABELA_SUPERIOR is not null) or (b.IE_CADASTRO_GERAL = 'S'))
union
select	a.nm_tabela,
	a.nm_atributo,
	substr(Obter_funcao_util_tabela(a.nm_tabela, 'F'),1,255) nm_funcao,
	substr(Obter_funcao_util_tabela(a.nm_tabela,'T'),1,255) ds_pasta_objeto,
	a.cd_expressao,
	a.nm_usuario
from	w_dic_expressao_uso a
where	a.nm_tabela	in('ANVISA_VIA_ADMINISTRACAO', 'CONSISTENCIA_LIB_PEPO', 'APLICACAO_TASY', 'EHR_ARQUETIPO',
'EHR_CEFALOCAUDAL', 'EHR_DOENCA', 'EHR_ELEMENTO', 'EHR_ELEMENTO_VISUAL', 'EHR_ENTIDADE_TASY', 'EHR_ESPECIALIDADE', 'EHR_GRUPO_ELEMENTO', 'EHR_SUBGRUPO_ELEMENTO', 'EHR_TIPO_DADO', 'EHR_UNIDADE_MEDIDA', 'FANEP_ITEM','FICHA_FINANC_ITEM', 'FUNCAO', 'INCONSISTENCIA_ESCRIT', 'EVENTO_TASY', 'INDICADOR_GESTAO', 'INDICADOR_GESTAO_ATRIB', 'INFORMACAO_CONTABIL', 'MAN_SEVERIDADE', 'MODULO_IMPLANTACAO', 'MODULO_TASY', 'OFTALMOLOGIA_ITEM','OFT_ACAO', 'PEPO_ACAO', 'PEPO_ITEM', 'PEP_ACAO', 'PERGUNTA_RECUP_SENHA', 'PE_ESCALA_ITEM', 'PRONTUARIO_ITEM', 'PRONTUARIO_PASTA','REGRA_CONSISTE_ONC', 'REGRA_CONSISTE_PRESCR','REGRA_CONSISTE_SAE', 'SAEP_ITEM', 'SAME_OPERACAO', 'SUBINDICADOR_GESTAO','SUBINDICADOR_GESTAO_ATRIB', 'TASY_LEGENDA', 'TASY_PADRAO_COR', 'TASY_PADRAO_IMAGEM', 'TIPO_AMPUTACAO', 'TIPO_LOCALIZAR_ATRIBUTO','TIPO_LOTE_CONTABIL', 'TIPO_LOTE_CONTABIL_PARAM')
order by	nm_funcao;
/