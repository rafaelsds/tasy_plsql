create or replace
view pls_utilizacao_benef_v as
select	sum(t.vl_total) vl_total,
	t.nr_sequencia,
	t.cd_tiss,
	t.ie_tipo_guia,
	t.ie_tipo_item,
	t.nr_seq_segurado,
	t.nr_seq_prestador,
	t.dt_atendimento_referencia,
	t.ie_tipo_conta,
	t.ie_tipo_benef,
	t.nr_seq_conta_princ,
	count(t.nr_seq_item) count_item
from (	select	decode(nvl(b.ie_tipo_protocolo,'C'),'R',nvl(d.vl_liquido,0), nvl(d.vl_lib_original,0)) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		a.ie_tipo_guia,
		decode(d.ie_tipo_despesa, '1', 'P', 'O') ie_tipo_item,
		a.nr_seq_segurado,
		a.nr_seq_prestador_exec nr_seq_prestador,
		a.dt_atendimento_referencia,
		'CM' ie_tipo_conta,
		'T' ie_tipo_benef,
		a.nr_seq_conta_princ nr_seq_conta_princ,
		d.nr_sequencia nr_seq_item
	from	pls_conta		a,
		pls_protocolo_conta	b,
		pls_tipo_atendimento	c,
		pls_conta_proc		d,
		pls_segurado		e
	where	b.nr_sequencia(+)		= a.nr_seq_protocolo
	and	a.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_seq_conta			= a.nr_sequencia
	and	e.nr_sequencia			= a.nr_seq_segurado
	and	a.ie_status			= 'F'
	and	d.ie_status			<> 'D'
	union all
	select	decode(nvl(b.ie_tipo_protocolo,'C'),'R',nvl(d.vl_liquido,0), nvl(d.vl_lib_original,0)) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		a.ie_tipo_guia,
		decode(d.ie_tipo_despesa, '1', 'P', 'O') ie_tipo_item,
		e.nr_seq_titular nr_seq_segurado,
		a.nr_seq_prestador_exec nr_seq_prestador,
		a.dt_atendimento_referencia,
		'CM' ie_tipo_conta,
		'D' ie_tipo_benef,
		a.nr_seq_conta_princ nr_seq_conta_princ,
		d.nr_sequencia nr_seq_item
	from	pls_conta		a,
		pls_protocolo_conta	b,
		pls_tipo_atendimento	c,
		pls_conta_proc		d,
		pls_segurado		e
	where	b.nr_sequencia(+)		= a.nr_seq_protocolo
	and	a.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_seq_conta			= a.nr_sequencia
	and	e.nr_sequencia			= a.nr_seq_segurado
	and	d.ie_status			<> 'D'
	and	a.ie_status			= 'F'
	union all
	select	decode(nvl(b.ie_tipo_protocolo,'C'),'R',nvl(d.vl_liberado,0), nvl(d.vl_lib_original,0)) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		a.ie_tipo_guia,
		'O' ie_tipo_item,
		a.nr_seq_segurado,
		a.nr_seq_prestador_exec nr_seq_prestador,
		a.dt_atendimento_referencia,
		'CM' ie_tipo_conta,
		'T' ie_tipo_benef,
		a.nr_seq_conta_princ nr_seq_conta_princ,
		d.nr_sequencia nr_seq_item
	from	pls_conta		a,
		pls_protocolo_conta	b,
		pls_tipo_atendimento	c,
		pls_conta_mat		d,
		pls_segurado		e
	where	b.nr_sequencia(+)		= a.nr_seq_protocolo
	and	a.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_seq_conta			= a.nr_sequencia
	and	e.nr_sequencia			= a.nr_seq_segurado
	and	a.ie_status			= 'F'
	and	d.ie_status			<> 'D'
	union all
	select	decode(nvl(b.ie_tipo_protocolo,'C'),'R',nvl(d.vl_liberado,0), nvl(d.vl_lib_original,0)) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		a.ie_tipo_guia,
		'O' ie_tipo_item,
		e.nr_seq_titular nr_seq_segurado,
		a.nr_seq_prestador_exec nr_seq_prestador,
		a.dt_atendimento_referencia,
		'CM' ie_tipo_conta,
		'D' ie_tipo_benef,
		a.nr_seq_conta_princ nr_seq_conta_princ,
		d.nr_sequencia nr_seq_item
	from	pls_conta		a,
		pls_protocolo_conta	b,
		pls_tipo_atendimento	c,
		pls_conta_mat		d,
		pls_segurado		e
	where	b.nr_sequencia(+)		= a.nr_seq_protocolo
	and	a.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_seq_conta			= a.nr_sequencia
	and	e.nr_sequencia			= a.nr_seq_segurado
	and	a.ie_status			= 'F'
	and	d.ie_status			<> 'D'
	union all
	select	nvl(e.vl_liberado,0) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		b.ie_tipo_guia,
		decode(g.ie_tipo_despesa, '1', 'P', 'O') ie_tipo_item,
		b.nr_seq_segurado,
		b.nr_seq_prestador_exec nr_seq_prestador,
		b.dt_atendimento_referencia,
		'RG' ie_tipo_conta,
		'T' ie_tipo_benef,
		b.nr_seq_conta_princ nr_seq_conta_princ,
		g.nr_sequencia nr_seq_item
	from	pls_rec_glosa_conta		a,
		pls_conta			b,
		pls_tipo_atendimento		c,
		pls_segurado			d,
		pls_conta_rec_resumo_item	e,
		pls_rec_glosa_proc		f,
		pls_conta_proc			g
	where	b.nr_sequencia			= a.nr_seq_conta
	and	b.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_sequencia			= b.nr_seq_segurado	
	and	e.nr_seq_conta_rec		= a.nr_sequencia
	and	f.nr_seq_conta_rec		= e.nr_seq_conta_rec
	and	f.nr_sequencia			= e.nr_seq_proc_rec
	and	g.nr_sequencia			= f.nr_seq_conta_proc
	and	b.ie_status			= 'F'
	and	g.ie_status			<> 'D'
	and	nvl(e.vl_liberado,0)		> 0
	and	a.ie_status			!= '3'
	union all
	select	nvl(e.vl_liberado,0) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		b.ie_tipo_guia,
		decode(g.ie_tipo_despesa, '1', 'P', 'O') ie_tipo_item,
		d.nr_seq_titular nr_seq_segurado,
		b.nr_seq_prestador_exec nr_seq_prestador,
		b.dt_atendimento_referencia,
		'RG' ie_tipo_conta,
		'D' ie_tipo_benef,
		b.nr_seq_conta_princ nr_seq_conta_princ,
		g.nr_sequencia nr_seq_item
	from	pls_rec_glosa_conta		a,
		pls_conta			b,
		pls_tipo_atendimento		c,
		pls_segurado			d,
		pls_conta_rec_resumo_item	e,
		pls_rec_glosa_proc		f,
		pls_conta_proc			g
	where	b.nr_sequencia			= a.nr_seq_conta
	and	b.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_sequencia			= b.nr_seq_segurado	
	and	e.nr_seq_conta_rec		= a.nr_sequencia
	and	f.nr_seq_conta_rec		= e.nr_seq_conta_rec
	and	f.nr_sequencia			= e.nr_seq_proc_rec
	and	g.nr_sequencia			= f.nr_seq_conta_proc
	and	b.ie_status			= 'F'
	and	g.ie_status			<> 'D'
	and	nvl(e.vl_liberado,0)		> 0
	and	a.ie_status			!= '3'
	union all
	select	nvl(e.vl_liberado,0) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		b.ie_tipo_guia,
		'O' ie_tipo_item,
		b.nr_seq_segurado,
		b.nr_seq_prestador_exec nr_seq_prestador,
		b.dt_atendimento_referencia,
		'RG' ie_tipo_conta,
		'T' ie_tipo_benef,
		b.nr_seq_conta_princ nr_seq_conta_princ,
		g.nr_sequencia nr_seq_item
	from	pls_rec_glosa_conta		a,
		pls_conta			b,
		pls_tipo_atendimento		c,
		pls_segurado			d,
		pls_conta_rec_resumo_item	e,
		pls_rec_glosa_mat		f,
		pls_conta_mat			g
	where	b.nr_sequencia			= a.nr_seq_conta
	and	b.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_sequencia			= b.nr_seq_segurado	
	and	e.nr_seq_conta_rec		= a.nr_sequencia
	and	f.nr_seq_conta_rec		= e.nr_seq_conta_rec
	and	f.nr_sequencia			= e.nr_seq_mat_rec
	and	g.nr_sequencia			= f.nr_seq_conta_mat
	and	b.ie_status			= 'F'
	and	g.ie_status			<> 'D'
	and	nvl(e.vl_liberado,0)		> 0
	and	a.ie_status			!= '3'
	union all
	select	nvl(e.vl_liberado,0) vl_total,
		a.nr_sequencia,
		c.cd_tiss,
		b.ie_tipo_guia,
		'O' ie_tipo_item,
		d.nr_seq_titular nr_seq_segurado,
		b.nr_seq_prestador_exec nr_seq_prestador,
		b.dt_atendimento_referencia,
		'RG' ie_tipo_conta,
		'D' ie_tipo_benef,
		b.nr_seq_conta_princ nr_seq_conta_princ,
		g.nr_sequencia nr_seq_item
	from	pls_rec_glosa_conta		a,
		pls_conta			b,
		pls_tipo_atendimento		c,
		pls_segurado			d,
		pls_conta_rec_resumo_item	e,
		pls_rec_glosa_mat		f,
		pls_conta_mat			g
	where	b.nr_sequencia			= a.nr_seq_conta
	and	b.nr_seq_tipo_atendimento	= c.nr_sequencia(+)
	and	d.nr_sequencia			= b.nr_seq_segurado	
	and	e.nr_seq_conta_rec		= a.nr_sequencia
	and	f.nr_seq_conta_rec		= e.nr_seq_conta_rec
	and	f.nr_sequencia			= e.nr_seq_mat_rec
	and	g.nr_sequencia			= f.nr_seq_conta_mat
	and	b.ie_status			= 'F'
	and	g.ie_status			<> 'D'
	and	a.ie_status			!= '3') t
group by	t.nr_sequencia, t.cd_tiss, t.ie_tipo_guia, t.ie_tipo_item, t.nr_seq_segurado, t.nr_seq_prestador, t.dt_atendimento_referencia, t.ie_tipo_conta, t.ie_tipo_benef, t.nr_seq_conta_princ
order by to_date(t.dt_atendimento_referencia) asc;
/