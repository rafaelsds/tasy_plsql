CREATE OR REPLACE 
VIEW EIS_DIALISE_DIABETICOS_V AS 
select	a.*,
	(select	d.cd_setor_atendimento
	from	HD_DIALISE_DIALISADOR b,
			hd_ponto_acesso c,
			setor_atendimento d
	where	a."NR_SEQ_DIALISADOR" = b.nr_sequencia
	and		b.nr_seq_ponto_acesso = c.nr_sequencia
	and		c.cd_setor_atendimento = d.cd_setor_atendimento) cd_setor_atendimento,	
	substr(hd_obter_desc_unid_dialise(a.nr_seq_unid_dialise_atual),1,80) ds_unid_dialise,
	substr(obter_desc_cid_doenca(a.cd_cid_direta),1,240) ds_cid_morte,
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,254) nm_pessoa_fisica,
	substr(obter_nome_empresa(a.cd_empresa),1,80) nm_empresa,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80)  nm_estabelecimento,
	(select	d.ds_setor_atendimento
	from	HD_DIALISE_DIALISADOR b,
			hd_ponto_acesso c,
			setor_atendimento d
	where	a."NR_SEQ_DIALISADOR" = b.nr_sequencia
	and		b.nr_seq_ponto_acesso = c.nr_sequencia
	and		c.cd_setor_atendimento = d.cd_setor_atendimento) ds_setor_atendimento,
	substr (OBTER_DESC_CONVENIO(a.cd_convenio),1,100) ds_convenio
	from	eis_dialise a
	where	a.qt_pac_diabeticos > 0;
/
