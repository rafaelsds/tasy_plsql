create or replace
view eis_solic_compra_v as
select	b.cd_estabelecimento,
	b.nr_solic_compra,
	b.ie_urgente,
	decode(b.ie_urgente,'S','Sim','N�o') ds_urgente,
	trunc(b.dt_solicitacao_compra,'dd') dt_solicitacao_compra,
	trunc(b.dt_liberacao,'dd') dt_liberacao,
	trunc(b.dt_autorizacao,'dd') dt_autorizacao,
	trunc(b.dt_baixa,'dd') dt_baixa,
	b.cd_pessoa_solicitante,
	substr(obter_nome_pf_pj(b.cd_pessoa_solicitante, null),1,80) nm_pessoa_solicitante,
	b.cd_local_estoque,
	substr(obter_desc_local_estoque(b.cd_local_estoque),1,80) ds_local_estoque,
	b.cd_centro_custo,
	substr(obter_desc_centro_custo(b.cd_centro_custo),1,80) ds_centro_custo,
	b.cd_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,80) ds_setor_atendimento,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,80) ds_material,
	a.cd_conta_contabil,
	substr(obter_desc_conta_contabil(a.cd_conta_contabil),1,80) ds_conta_contabil,
	c.cd_grupo_material,
	c.ds_grupo_material,
	c.cd_subgrupo_material,
	c.ds_subgrupo_material,
	c.cd_classe_material,
	c.ds_classe_material,
	b.cd_comprador_resp,
	substr(sup_obter_nome_comprador(b.cd_estabelecimento, b.cd_comprador_resp),1,80) nm_comprador_resp,
	b.nr_seq_motivo_urgente,
	substr(OBTER_DESC_MOTIVO_URGENCIA(b.NR_SEQ_MOTIVO_URGENTE),1,100) ds_motivo_urgente,
	nvl(a.vl_unit_previsto,0) vl_unit_previsto,
	a.qt_material
from	estrutura_material_v c,
	solic_compra_item a,
	solic_compra b
where	a.nr_solic_compra = b.nr_solic_compra
and	a.cd_material = c.cd_material
and	b.dt_autorizacao is not null
and	a.dt_autorizacao is not null
and	b.nr_seq_motivo_cancel is null
order by b.nr_solic_compra;
/
