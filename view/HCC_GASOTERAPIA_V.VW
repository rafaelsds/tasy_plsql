create or replace view HCC_Gasoterapia_v as
select	substr(obter_descricao_padrao('GAS', 'DS_GAS', nr_seq_gas),1,254) ds_tipo_gas,
	substr(obter_valor_dominio(1299, ie_respiracao),1,200) ds_respiracao,
	substr(obter_valor_dominio(1612,a.IE_DISP_RESP_ESP),1,255) ds_dispositivo,
	substr(obter_descricao_padrao('MODALIDADE_VENTILATORIA', 'DS_MODALIDADE', cd_modalidade_vent),1,155) DS_MODALIDADE_VENT,
	b.cd_prescritor,
	b.nr_prescricao,
	a.nr_sequencia,
	a.QT_GASOTERAPIA QT_GASOTERAPIA,
	substr(obter_valor_dominio(1580, a.IE_UNIDADE_MEDIDA),1,80) DS_UNIDADE_MEDIDA
from	prescr_gasoterapia a,
	prescr_medica b
where	a.nr_prescricao		= b.nr_prescricao;
/
