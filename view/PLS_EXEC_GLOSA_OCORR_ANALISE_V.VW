create or replace view pls_exec_glosa_ocorr_analise_v as
select	b.ie_tipo,
	a.nr_sequencia,
	substr(tiss_obter_motivo_glosa(nr_seq_motivo_glosa,'C'),1,10) nr_codigo,
	substr(tiss_obter_motivo_glosa(nr_seq_motivo_glosa,'D'),1,255) ds_descricao_glosa_ocor,
	substr(tiss_obter_motivo_glosa(nr_seq_motivo_glosa,'DG'),1,255) ds_grupo,
	substr(ds_observacao,1,255) ds_observacao,
	substr(pls_obter_dados_msg_glosa(nr_seq_motivo_glosa,'ME'),1,255) ds_msg_externa,
	0 nr_regra_ocor,
	a.nr_seq_ocorrencia nr_ocorrencia,
	b.nr_seq_auditoria,
	b.nr_seq_aud_item,
	b.nr_nivel_liberacao,
	b.ie_status,
       	a.nr_seq_execucao               nr_seq_execucao,
       	a.nr_seq_exec_proc              nr_seq_exec_proc,
       	a.nr_seq_exec_mat               nr_seq_exec_mat
from	pls_analise_ocor_glosa_aut b,
	pls_requisicao_glosa	a
where	a.nr_sequencia = b.nr_seq_glosa
and	a.nr_seq_execucao is not null
union
select	b.ie_tipo,
	a.nr_sequencia,
	substr(pls_obter_dados_ocorrencia(a.nr_seq_ocorrencia,'C'),1,255) nr_codigo,
	substr(pls_obter_dados_ocorrencia(a.nr_seq_ocorrencia,'D'),1,255) ds_descricao_glosa_ocor,
	'' ds_grupo,
	nvl(substr(ds_observacao,1,255),substr(pls_obter_dados_ocorrencia(a.nr_seq_ocorrencia,'DO'),1,255)) ds_observacao,
	'' ds_msg_externa,
	a.nr_seq_regra nr_regra_ocor,
	a.nr_seq_ocorrencia nr_ocorrencia,
	b.nr_seq_auditoria,
	b.nr_seq_aud_item,
	b.nr_nivel_liberacao,
	b.ie_status,
        	nr_seq_execucao       nr_seq_execucao,
        	nr_seq_proc           nr_seq_exec_proc,
        	nr_seq_mat            nr_seq_exec_mat
from	pls_analise_ocor_glosa_aut b,
	pls_ocorrencia_benef	a
where	a.nr_seq_conta is null
and	a.nr_seq_guia_plano is null
and	a.nr_seq_requisicao is null
and	a.nr_seq_conta_pos_estab is null
and	a.nr_sequencia = b.nr_seq_ocorrencia_benef;
/
