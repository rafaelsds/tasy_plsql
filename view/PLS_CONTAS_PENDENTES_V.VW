create or replace 
view pls_contas_pendentes_v 
as
select	'N' ie_pos_novo,
	b.nr_sequencia nr_seq_conta,
	null nr_seq_conta_sus,
	b.dt_atualizacao dt_fechamento,
	c.ie_pcmso ie_pcmso,
	substr(obter_nome_pf(c.cd_pessoa_fisica), 1, 60) nm_pessoa,
	substr(pls_obter_nome_congenere(c.nr_seq_congenere), 1, 80) nm_congenere,
	pls_obter_nr_contrato(c.nr_seq_contrato) nr_contrato,
	substr(pls_obter_estipulante_contrato(c.nr_seq_contrato), 1, 80) ds_estip,
	b.cd_guia,
	nvl(x.vl_beneficiario, 0) vl_beneficiario,
	nvl(x.vl_administracao, 0) vl_administracao,
	b.cd_guia_referencia,
	substr(pls_obter_dados_segurado(b.nr_seq_segurado, 'CR'), 1,255) cd_carteirinha,
	b.dt_atendimento_referencia,
	b.nr_seq_segurado,
	nvl(x.vl_medico, 0) + nvl(x.vl_custo_operacional, 0) + nvl(x.vl_materiais, 0) vl_servico,
	nvl(x.vl_lib_taxa_servico, 0) + nvl(x.vl_lib_taxa_co, 0) + nvl(x.vl_lib_taxa_material, 0) vl_taxa,
	nvl(x.vl_medico, 0) + nvl(x.vl_custo_operacional, 0) + nvl(x.vl_materiais, 0) + nvl(x.vl_lib_taxa_servico, 0) + nvl(x.vl_lib_taxa_co, 0) + nvl(x.vl_lib_taxa_material, 0) vl_total,
	nvl(decode(a.ie_tipo_protocolo,'R',
		(select	max(f.ds_camara)
		from	pls_camara_compensacao	f,
			pls_congenere_camara	e
		where	e.nr_seq_congenere	= a.nr_seq_congenere
		and	e.nr_seq_camara		= f.nr_sequencia
		and	b.dt_atendimento_referencia between e.dt_inicio_vigencia_ref and e.dt_fim_vigencia_ref),	
		(select	max(f.ds_camara)
		from	pls_camara_compensacao	f,
			pls_congenere_camara	e
		where	e.nr_seq_congenere	= c.nr_seq_congenere
		and	e.nr_seq_camara		= f.nr_sequencia
		and	b.dt_atendimento_referencia between e.dt_inicio_vigencia_ref and e.dt_fim_vigencia_ref)), 'Sem camara') ds_camara,
	(select	w.cd_cooperativa
		from    pls_congenere	w
		where	w.nr_sequencia	= c.nr_seq_congenere) cd_cooperativa,
	(select	y.ds_razao_social
		from	pls_congenere	w,
			pessoa_juridica y
		where	y.cd_cgc	= w.cd_cgc
		and	w.nr_sequencia	= c.nr_seq_congenere) ds_cooperativa,
	nvl(a.dt_lib_pagamento,	(	select	max(n.dt_historico)
					from	pls_prot_conta_hist	n
					where	n.ie_tipo_historico	= '11' -- Liberado pra pagamento
					and	n.nr_seq_protocolo	= a.nr_sequencia)) dt_lib_pagamento,
	b.ie_tipo_conta,
	c.ie_tipo_segurado,
	a.ie_status,
	a.nr_sequencia nr_seq_protocolo_conta,
	a.dt_mes_competencia,
	a.nr_seq_prestador,
	b.nr_seq_prestador_exec,
	d.ie_preco,
	c.nr_seq_congenere,
	c.cd_pessoa_fisica,
	c.nr_seq_contrato,
	b.dt_emissao,
	a.ie_tipo_protocolo,
	obter_valor_dominio(3648, a.ie_tipo_protocolo) ds_tipo_protocolo
from	pls_plano			d,
	pls_segurado   			c,
	pls_conta_pos_estabelecido	x,
	pls_conta  			b,
	pls_protocolo_conta		a
where	a.nr_sequencia			= b.nr_seq_protocolo
and	c.nr_sequencia			= b.nr_seq_segurado
and	b.nr_sequencia			= x.nr_seq_conta
and	c.nr_seq_plano			= d.nr_sequencia(+)
and	b.ie_status_fat			= 'L'
and	b.ie_status			= 'F' -- Somente contas fechadas
and	x.nr_seq_evento_fat is null
and	x.nr_seq_lote_disc is null
and	x.nr_seq_lote_fat is null
and	x.nr_seq_mensalidade_seg is null
and	x.nr_seq_sca is null
and	x.ie_status_faturamento = 'L'
and	((nvl(d.ie_preco, '3') in ('2', '3')) or ((d.ie_preco = '1') and (c.dt_rescisao is not null)) or 
	((d.ie_preco = '1') and (a.ie_tipo_protocolo = 'R')) or ((d.ie_preco = '1') and (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = 'S')))
and	nvl(x.ie_situacao, 'A') = 'A'
and	(((select y.ie_status from pls_analise_conta y where y.nr_sequencia = b.nr_seq_analise) in ('S', 'T')) or (b.nr_seq_analise is null))
and not exists (select	1
		from	pls_analise_conta x
		where	x.nr_sequencia = b.nr_seq_analise
		and	x.ie_status in ('A', 'D', 'G', 'J', 'L', 'P', 'X'))
and not exists(	select	1
		from	pls_conta x
		where	x.nr_seq_conta_referencia = b.nr_sequencia
		and	x.nr_seq_ajuste_fat is not null)
and not exists(	select	1
		from	pls_ajuste_fatura_conta x
		where	x.nr_seq_conta	= b.nr_sequencia
		and	x.ie_status	= 'N')
-- RESSARCIMENTO SUS
union all
select 'N' ie_pos_novo,
	null nr_seq_conta,
	b.nr_sequencia nr_seq_conta_sus,
	b.dt_atualizacao dt_fechamento,
	c.ie_pcmso ie_pcmso,
	substr(obter_nome_pf(c.cd_pessoa_fisica), 1, 60) nm_pessoa,
	substr(pls_obter_nome_congenere(c.nr_seq_congenere), 1, 80) nm_congenere,
	pls_obter_nr_contrato(c.nr_seq_contrato) nr_contrato,
	substr(pls_obter_estipulante_contrato(c.nr_seq_contrato), 1, 80) ds_estip,
	null cd_guia,
	a.vl_item vl_beneficiario,
	0 vl_administracao,
	null cd_guia_referencia,
	substr(pls_obter_dados_segurado(b.nr_seq_segurado, 'CR'), 1,255) cd_carteirinha,
	null dt_atendimento_referencia,
	b.nr_seq_segurado,
	a.vl_item vl_servico,
	0 vl_taxa,
	a.vl_item vl_total,
	null ds_camara,
	(select	w.cd_cooperativa
		from    pls_congenere	w
		where	w.nr_sequencia	= c.nr_seq_congenere) cd_cooperativa,
	(select	y.ds_razao_social
		from	pls_congenere	w,
			pessoa_juridica y
		where	y.cd_cgc	= w.cd_cgc
		and	w.nr_sequencia	= c.nr_seq_congenere) ds_cooperativa,
	null dt_lib_pagamento,
	null ie_tipo_conta,
	c.ie_tipo_segurado,
	null ie_status,
	null nr_seq_protocolo_conta,
	null dt_mes_competencia,
	null nr_seq_prestador,
	null nr_seq_prestador_exec,
	d.ie_preco,
	c.nr_seq_congenere,
	c.cd_pessoa_fisica,
	c.nr_seq_contrato,
	null dt_emissao,
	'S' ie_tipo_protocolo,
	null ds_tipo_protocolo
from	pls_segurado_mensalidade	a,
	pls_processo_conta		b,
	pls_segurado			c,
	pls_plano			d
where	b.nr_sequencia	= a.nr_seq_processo_conta
and	c.nr_sequencia	= b.nr_seq_segurado
and	d.nr_sequencia  = c.nr_seq_plano
and	a.nr_seq_lote_fat is null
and	a.nr_seq_evento_fat is null
and	((d.ie_preco	= '1') and (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_internacao) = 'S'))
and	a.ie_tipo_item	= '39'
union all
select	'S' ie_pos_novo,
	b.nr_sequencia nr_seq_conta,
	null nr_seq_conta_sus,
	b.dt_atualizacao dt_fechamento,
	c.ie_pcmso ie_pcmso,
	substr(obter_nome_pf(c.cd_pessoa_fisica), 1, 60) nm_pessoa,
	substr(pls_obter_nome_congenere(c.nr_seq_congenere), 1, 80) nm_congenere,
	pls_obter_nr_contrato(c.nr_seq_contrato) nr_contrato,
	substr(pls_obter_estipulante_contrato(c.nr_seq_contrato), 1, 80) ds_estip,
	b.cd_guia,
	nvl(x.vl_medico, 0) + nvl(x.vl_materiais, 0) + nvl(x.vl_custo_operacional, 0) + nvl(x.vl_lib_taxa_co, 0) + nvl(x.vl_lib_taxa_material, 0) + nvl(x.vl_lib_taxa_servico, 0) vl_beneficiario,
	0 vl_administracao,
	b.cd_guia_referencia,
	substr(pls_obter_dados_segurado(b.nr_seq_segurado, 'CR'), 1,255) cd_carteirinha,
	b.dt_atendimento_referencia,
	b.nr_seq_segurado,
	nvl(x.vl_medico, 0) + nvl(x.vl_custo_operacional, 0) + nvl(x.vl_materiais, 0) vl_servico,
	nvl(x.vl_lib_taxa_servico, 0) + nvl(x.vl_lib_taxa_co, 0) + nvl(x.vl_lib_taxa_material, 0) vl_taxa,
	nvl(x.vl_medico, 0) + nvl(x.vl_custo_operacional, 0) + nvl(x.vl_materiais, 0) + nvl(x.vl_lib_taxa_servico, 0) + nvl(x.vl_lib_taxa_co, 0) + nvl(x.vl_lib_taxa_material, 0) vl_total,
	nvl(decode(a.ie_tipo_protocolo,'R',
		(select	max(f.ds_camara)
		from	pls_camara_compensacao	f,
			pls_congenere_camara	e
		where	e.nr_seq_congenere	= a.nr_seq_congenere
		and	e.nr_seq_camara		= f.nr_sequencia
		and	b.dt_atendimento_referencia between e.dt_inicio_vigencia_ref and e.dt_fim_vigencia_ref),	
		(select	max(f.ds_camara)
		from	pls_camara_compensacao	f,
			pls_congenere_camara	e
		where	e.nr_seq_congenere	= c.nr_seq_congenere
		and	e.nr_seq_camara		= f.nr_sequencia
		and	b.dt_atendimento_referencia between e.dt_inicio_vigencia_ref and e.dt_fim_vigencia_ref)), 'Sem camara') ds_camara,
	(select	w.cd_cooperativa
	from    pls_congenere	w
	where	w.nr_sequencia	= c.nr_seq_congenere) cd_cooperativa,
	(select	y.ds_razao_social
	from	pls_congenere	w,
		pessoa_juridica y
	where	y.cd_cgc	= w.cd_cgc
	and	w.nr_sequencia	= c.nr_seq_congenere) ds_cooperativa,
	nvl(a.dt_lib_pagamento,	(	select	max(n.dt_historico)
					from	pls_prot_conta_hist	n
					where	n.ie_tipo_historico	= '11' -- Liberado pra pagamento
					and	n.nr_seq_protocolo	= a.nr_sequencia)) dt_lib_pagamento,
	b.ie_tipo_conta,
	c.ie_tipo_segurado,
	a.ie_status,
	a.nr_sequencia nr_seq_protocolo_conta,
	a.dt_mes_competencia,
	a.nr_seq_prestador,
	b.nr_seq_prestador_exec,
	d.ie_preco,
	c.nr_seq_congenere,
	c.cd_pessoa_fisica,
	c.nr_seq_contrato,
	b.dt_emissao,
	a.ie_tipo_protocolo,
	obter_valor_dominio(3648, a.ie_tipo_protocolo) ds_tipo_protocolo
from	pls_plano		d,
	pls_segurado   		c,
	pls_conta_pos_proc	x,
	pls_conta  		b,
	pls_protocolo_conta	a
where	a.nr_sequencia		= b.nr_seq_protocolo
and	c.nr_sequencia		= b.nr_seq_segurado
and	b.nr_sequencia		= x.nr_seq_conta
and	c.nr_seq_plano		= d.nr_sequencia(+)
and	b.ie_status_fat		= 'L'
and	b.ie_status		= 'F' -- Somente contas fechadas
and	x.nr_seq_evento_fat is null
and	x.nr_seq_disc_proc is null
and	x.nr_seq_lote_fat is null
and	x.nr_seq_sca is null
and	x.ie_status_faturamento in ('L', 'P')
and	((nvl(d.ie_preco, '3') in ('2', '3')) or ((d.ie_preco = '1') and (c.dt_rescisao is not null)) or ((d.ie_preco = '1') and (a.ie_tipo_protocolo = 'R')) or ((d.ie_preco = '1') and (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = 'S')))
and	(((select y.ie_status from pls_analise_conta y where y.nr_sequencia = b.nr_seq_analise) in ('S', 'T')) or (b.nr_seq_analise is null))
and not exists (select	1
		from	pls_analise_conta x
		where	x.nr_sequencia = b.nr_seq_analise
		and	x.ie_status in ('A', 'D', 'G', 'J', 'L', 'P', 'X'))
and not exists(	select	1
		from	pls_conta x
		where	x.nr_seq_conta_referencia = b.nr_sequencia
		and	x.nr_seq_ajuste_fat is not null)
and not exists(	select	1
		from	pls_ajuste_fatura_conta x
		where	x.nr_seq_conta	= b.nr_sequencia
		and	x.ie_status	= 'N')
union all
select	'S' ie_pos_novo,
	b.nr_sequencia nr_seq_conta,
	null nr_seq_conta_sus,
	b.dt_atualizacao dt_fechamento,
	c.ie_pcmso ie_pcmso,
	substr(obter_nome_pf(c.cd_pessoa_fisica), 1, 60) nm_pessoa,
	substr(pls_obter_nome_congenere(c.nr_seq_congenere), 1, 80) nm_congenere,
	pls_obter_nr_contrato(c.nr_seq_contrato) nr_contrato,
	substr(pls_obter_estipulante_contrato(c.nr_seq_contrato), 1, 80) ds_estip,
	b.cd_guia,
	nvl(x.vl_materiais, 0) +  nvl(x.vl_lib_taxa_material, 0) vl_beneficiario,
	nvl(x.vl_administracao, 0) vl_administracao,
	b.cd_guia_referencia,
	substr(pls_obter_dados_segurado(b.nr_seq_segurado, 'CR'), 1,255) cd_carteirinha,
	b.dt_atendimento_referencia,
	b.nr_seq_segurado,
	0 vl_servico,
	nvl(x.vl_materiais_calc, 0)  + nvl(x.vl_taxa_material, 0) vl_taxa,
	nvl(x.vl_materiais, 0) +  nvl(x.vl_lib_taxa_material, 0) + nvl(x.vl_materiais_calc, 0)  + nvl(x.vl_taxa_material, 0) vl_total,
	nvl(decode(a.ie_tipo_protocolo,'R',
		(select	max(f.ds_camara)
		from	pls_camara_compensacao	f,
			pls_congenere_camara	e
		where	e.nr_seq_congenere	= a.nr_seq_congenere
		and	e.nr_seq_camara		= f.nr_sequencia
		and	b.dt_atendimento_referencia between e.dt_inicio_vigencia_ref and e.dt_fim_vigencia_ref),	
		(select	max(f.ds_camara)
		from	pls_camara_compensacao	f,
			pls_congenere_camara	e
		where	e.nr_seq_congenere	= c.nr_seq_congenere
		and	e.nr_seq_camara		= f.nr_sequencia
		and	b.dt_atendimento_referencia between e.dt_inicio_vigencia_ref and e.dt_fim_vigencia_ref)), 'Sem camara') ds_camara,
	(select	w.cd_cooperativa
	from    pls_congenere	w
	where	w.nr_sequencia	= c.nr_seq_congenere) cd_cooperativa,
	(select	y.ds_razao_social
	from	pls_congenere	w,
		pessoa_juridica y
	where	y.cd_cgc	= w.cd_cgc
	and	w.nr_sequencia	= c.nr_seq_congenere) ds_cooperativa,
	nvl(a.dt_lib_pagamento,	(	select	max(n.dt_historico)
					from	pls_prot_conta_hist	n
					where	n.ie_tipo_historico	= '11' -- Liberado pra pagamento
					and	n.nr_seq_protocolo	= a.nr_sequencia)) dt_lib_pagamento,
	b.ie_tipo_conta,
	c.ie_tipo_segurado,
	a.ie_status,
	a.nr_sequencia nr_seq_protocolo_conta,
	a.dt_mes_competencia,
	a.nr_seq_prestador,
	b.nr_seq_prestador_exec,
	d.ie_preco,
	c.nr_seq_congenere,
	c.cd_pessoa_fisica,
	c.nr_seq_contrato,
	b.dt_emissao,
	a.ie_tipo_protocolo,
	obter_valor_dominio(3648, a.ie_tipo_protocolo) ds_tipo_protocolo
from	pls_plano		d,
	pls_segurado   		c,
	pls_conta_pos_mat	x,
	pls_conta  		b,
	pls_protocolo_conta	a
where	a.nr_sequencia		= b.nr_seq_protocolo
and	c.nr_sequencia		= b.nr_seq_segurado
and	b.nr_sequencia		= x.nr_seq_conta
and	c.nr_seq_plano		= d.nr_sequencia(+)
and	b.ie_status_fat		= 'L'
and	b.ie_status		= 'F' -- Somente contas fechadas
and	x.nr_seq_evento_fat is null
and	x.nr_seq_disc_mat is null
and	x.nr_seq_lote_fat is null
and	x.nr_seq_sca is null
and	x.ie_status_faturamento in ('L', 'P')
and	((nvl(d.ie_preco, '3') in ('2', '3')) or ((d.ie_preco = '1') and (c.dt_rescisao is not null)) or ((d.ie_preco = '1') and (a.ie_tipo_protocolo = 'R')) or ((d.ie_preco = '1') and (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = 'S')))
and	(((select y.ie_status from pls_analise_conta y where y.nr_sequencia = b.nr_seq_analise) in ('S', 'T')) or (b.nr_seq_analise is null))
and not exists (select	1
		from	pls_analise_conta x
		where	x.nr_sequencia = b.nr_seq_analise
		and	x.ie_status in ('A', 'D', 'G', 'J', 'L', 'P', 'X'))
and not exists(	select	1
		from	pls_conta x
		where	x.nr_seq_conta_referencia = b.nr_sequencia
		and	x.nr_seq_ajuste_fat is not null)
and not exists(	select	1
		from	pls_ajuste_fatura_conta x
		where	x.nr_seq_conta	= b.nr_sequencia
		and	x.ie_status	= 'N');
/