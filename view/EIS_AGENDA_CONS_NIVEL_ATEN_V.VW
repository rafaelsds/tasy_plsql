create or replace view eis_agenda_cons_nivel_aten_v as
SELECT	substr(obter_valor_dominio(4, obter_dados_pf(a.cd_pessoa_fisica,'SE')),1,200) ds_sexo,
		a.dt_referencia,
		a.dt_agendamento,
		a.nr_seq_status_pac,
		a.ie_status_agenda,
		PKG_DATE_FORMATERS.TO_VARCHAR(a.dt_agendamento, 'shortMonth', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO) ds_mes,
		SUBSTR(DECODE(a.cd_turno,'0',Obter_Desc_Expressao(487774),'1',Obter_Desc_Expressao(487775),Obter_Desc_Expressao(308326)),1,50) ds_turno,
		SUBSTR(obter_desc_status_pac_ag(a.nr_seq_status_pac),1,100) DS_STATUS_PACIENTE,
		substr(obter_valor_dominio(5, obter_dados_pf(a.cd_pessoa_fisica,'EC')),1,200) ds_estado_civil,
		to_char(trunc(a.dt_agendamento, 'hh24'), 'hh24') ds_hora, --N�o existe mascara especifica s� para Hora
		to_char(trunc(a.dt_agendamento, 'Year'), 'yyyy') ds_ano, --N�o existe mascara especifica s� para Ano
		SUBSTR(obter_desc_classif_agenda_pac(nr_seq_classif_agenda),1,80) ds_classificacao,
		obter_nome_convenio(a.cd_convenio) ds_convenio,
		obter_cod_dia_semana(dt_referencia) ie_dia_semana,
		SUBSTR(obter_valor_dominio(35,obter_cod_dia_semana(dt_referencia)),1,150) ds_dia_semana,
		obter_valor_dominio(83, a.ie_status_agenda) ds_status_agenda,		
		a.CD_CONVENIO,
		Obter_Tipo_Convenio(A.CD_CONVENIO) cd_tipo_convenio,	
		SUBSTR(obter_valor_dominio(11, Obter_Tipo_Convenio(A.CD_CONVENIO)),1,100) ds_tipo_convenio,
		SUBSTR(obter_categoria_convenio(a.cd_convenio,a.cd_categoria),1,255) ds_categoria_convenio,
		a.CD_ESTABELECIMENTO,
		obter_empresa_estab(a.cd_estabelecimento) cd_empresa,
		obter_dados_pf_dt(a.cd_pessoa_fisica, 'DN') dt_nascimento,
		a.CD_MEDICO
FROM	agenda b,
		eis_agenda a
WHERE	a.cd_agenda			= b.cd_agenda
AND		a.CD_PROC_ADIC		IS NULL
and		a.cd_tipo_agenda	= 3
UNION ALL
SELECT	substr(obter_valor_dominio(4, obter_dados_pf(a.cd_pessoa_fisica,'SE')),1,200) ds_sexo,
		a.dt_referencia,
		a.dt_agendamento,
		a.nr_seq_status_pac,
		a.ie_status_agenda,
		PKG_DATE_FORMATERS.TO_VARCHAR(a.dt_agendamento, 'shortMonth', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO),
		SUBSTR(DECODE(a.cd_turno,'0',Obter_Desc_Expressao(487774),'1',Obter_Desc_Expressao(487775),Obter_Desc_Expressao(308326)),1,50) ds_turno,
		SUBSTR(obter_desc_status_pac_ag(a.nr_seq_status_pac),1,100) DS_STATUS_PACIENTE,	
		substr(obter_valor_dominio(5, obter_dados_pf(a.cd_pessoa_fisica,'EC')),1,200) ds_estado_civil,
		to_char(trunc(a.dt_agendamento, 'hh24'), 'hh24') ds_hora, --N�o existe mascara especifica s� para Hora
		to_char(trunc(a.dt_agendamento, 'Year'), 'yyyy') ds_ano, --N�o existe mascara especifica s� para Ano
		SUBSTR(obter_desc_classif_agenda_pac(nr_seq_classif_agenda),1,80) ds_classificacao,
		obter_nome_convenio(a.cd_convenio) ds_convenio,
		obter_cod_dia_semana(dt_referencia) ie_dia_semana,
		SUBSTR(obter_valor_dominio(35,obter_cod_dia_semana(dt_referencia)),1,150) ds_dia_semana,
		obter_valor_dominio(83, a.ie_status_agenda) ds_status_agenda,
		a.CD_CONVENIO,
		Obter_Tipo_Convenio(A.CD_CONVENIO) cd_tipo_convenio,	
		SUBSTR(obter_valor_dominio(11, Obter_Tipo_Convenio(A.CD_CONVENIO)),1,100) ds_tipo_convenio,
		SUBSTR(obter_categoria_convenio(a.cd_convenio,a.cd_categoria),1,255) ds_categoria_convenio,
		a.CD_ESTABELECIMENTO,
		obter_empresa_estab(a.cd_estabelecimento) cd_empresa,
		obter_dados_pf_dt(a.cd_pessoa_fisica, 'DN') dt_nascimento,
		a.CD_MEDICO
FROM	agenda b,
		eis_agenda a
WHERE	a.cd_agenda			= b.cd_agenda
AND		a.CD_PROC_ADIC		IS NOT NULL
and 	a.cd_tipo_agenda	= 3;
/
