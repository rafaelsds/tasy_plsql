create or replace
view os_usuario_v as
select	o.nr_sequencia nr_seq_os,
	obter_tipo_os(o.dt_ordem_servico) ie_tipo_os,
	obter_se_ordem_orig_projeto(o.nr_sequencia) ie_os_projeto,
	e.nm_usuario_exec nm_executor,
	o.nr_seq_grupo_des nr_seq_grupo_desenv,
	o.cd_funcao,
	o.ie_resp_teste
from	man_ordem_servico_exec e,
	man_ordem_servico o
where	e.nr_seq_ordem = o.nr_sequencia
and	o.ie_status_ordem in ('1','2')
and	obter_se_usuario_acao_exec_os(o.nr_sequencia, e.nm_usuario_exec) = 'S'
and	o.nr_seq_estagio in (
		select	a.nr_seq_estagio
		from	man_estagio_usuario a
		where	a.nm_usuario_acao = e.nm_usuario_exec);
/