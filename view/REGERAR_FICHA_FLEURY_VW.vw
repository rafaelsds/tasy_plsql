CREATE OR REPLACE VIEW REGERAR_FICHA_FLEURY_VW
AS
  SELECT DISTINCT nr_prescricao,
    cd_pessoa_fisica,
    cd_estabelecimento,
    SUBSTR(obter_nome_estabelecimento(cd_estabelecimento),1,255) ds_estabelecimento,
    cd_perfil_ativo,
    nm_usuario,
    dt_liberacao,
    nr_sequencia,
    nr_seq_exame,
    nr_controle
  FROM
    (SELECT a.nr_prescricao,
      a.cd_pessoa_fisica,
      a.cd_estabelecimento,
      a.cd_perfil_ativo,
      a.nm_usuario,
      NVL(a.dt_liberacao,a.dt_liberacao_medico) dt_liberacao,
      b.nr_sequencia,
      b.nr_seq_exame,
      b.nr_controle
    FROM prescr_medica a,
      prescr_procedimento b,
      lab_parametro c
    WHERE a.nr_prescricao                          = b.nr_prescricao
    AND a.cd_estabelecimento                       = c.cd_estabelecimento
    AND NVL(a.dt_liberacao,a.dt_liberacao_medico) IS NOT NULL
    AND b.dt_integracao                           IS NULL
    AND NOT EXISTS
      (SELECT 1
      FROM prescR_procedimento x
      WHERE x.nr_prescricao                                          = a.nr_prescricao
      AND x.dt_integracao                                           IS NOT NULL
      AND obter_sigla_exame_fleury(x.nr_prescricao, x.nr_sequencia) IS NOT NULL
      )
    AND obter_sigla_exame_fleury(b.nr_prescricao, b.nr_sequencia) IS NOT NULL
    UNION
    SELECT a.nr_prescricao,
      a.cd_pessoa_fisica,
      a.cd_estabelecimento,
      a.cd_perfil_ativo,
      a.nm_usuario,
      NVL(a.dt_liberacao,a.dt_liberacao_medico) dt_liberacao,
      b.nr_sequencia,
      b.nr_seq_exame,
      b.nr_controle
    FROM prescr_medica a,
      prescr_procedimento b,
      lab_parametro c
    WHERE a.nr_prescricao                          = b.nr_prescricao
    AND a.cd_estabelecimento                       = c.cd_estabelecimento
    AND NVL(a.dt_liberacao,a.dt_liberacao_medico) IS NOT NULL
    AND b.dt_integracao                           IS NULL
    AND NOT EXISTS
      (SELECT 1
      FROM prescR_procedimento x
      WHERE x.nr_prescricao                                          = a.nr_prescricao
      AND x.dt_integracao                                           IS NOT NULL
      AND obter_sigla_exame_fleury(x.nr_prescricao, x.nr_sequencia) IS NOT NULL
      )
    AND obter_sigla_exame_fleury(b.nr_prescricao, b.nr_sequencia) IS NOT NULL
    UNION
    SELECT a.nr_prescricao,
      a.cd_pessoa_fisica,
      a.cd_estabelecimento,
      a.cd_perfil_ativo,
      a.nm_usuario,
      NVL(a.dt_liberacao,a.dt_liberacao_medico) dt_liberacao,
      b.nr_sequencia,
      b.nr_seq_exame,
      b.nr_controle
    FROM prescr_medica a,
      prescr_procedimento b,
      lab_parametro c
    WHERE a.nr_prescricao                          = b.nr_prescricao
    AND a.cd_estabelecimento                       = c.cd_estabelecimento
    AND NVL(a.dt_liberacao,a.dt_liberacao_medico) IS NOT NULL
    AND b.dt_integracao                           IS NULL
    AND NOT EXISTS
      (SELECT 1
      FROM prescR_procedimento x
      WHERE x.nr_prescricao                                               = a.nr_prescricao
      AND x.dt_integracao                                                IS NOT NULL
      AND obter_sigla_exame_fleury_diag(x.nr_prescricao, x.nr_sequencia) IS NOT NULL
      )
    AND obter_sigla_exame_fleury_diag(b.nr_prescricao, b.nr_sequencia) IS NOT NULL
    UNION
    SELECT a.nr_prescricao,
      a.cd_pessoa_fisica,
      a.cd_estabelecimento,
      a.cd_perfil_ativo,
      a.nm_usuario,
      NVL(a.dt_liberacao,a.dt_liberacao_medico) dt_liberacao,
      b.nr_sequencia,
      b.nr_seq_exame,
      b.nr_controle
    FROM prescr_medica a,
      prescr_procedimento b,
      lab_parametro c
    WHERE a.nr_prescricao                          = b.nr_prescricao
    AND a.cd_estabelecimento                       = c.cd_estabelecimento
    AND NVL(a.dt_liberacao,a.dt_liberacao_medico) IS NOT NULL
    AND b.dt_integracao                           IS NULL
    AND NOT EXISTS
      (SELECT 1
      FROM prescR_procedimento x
      WHERE x.nr_prescricao                                               = a.nr_prescricao
      AND x.dt_integracao                                                IS NOT NULL
      AND obter_sigla_exame_fleury_diag(x.nr_prescricao, x.nr_sequencia) IS NOT NULL
      )
    AND obter_sigla_exame_fleury_diag(b.nr_prescricao, b.nr_sequencia) IS NOT NULL
    )
  ORDER BY dt_liberacao;
  /
