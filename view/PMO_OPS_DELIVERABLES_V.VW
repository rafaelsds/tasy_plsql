CREATE OR REPLACE VIEW PMO_OPS_DELIVERABLES_V AS
select w.*,
case when pv_sch>0 then round(ev_sch/pv_sch,2) else 0 end spi_sch,
case when ac_sch>0 then round(ev_sch/ac_sch,2) else 0 end hpi_sch,
case when ac_sch>0 and ev_sch>0 then round(effort_sch/(ev_sch/ac_sch),2) else effort_sch end adjusted_effort_sch,
case when ac_sch>0 and ev_sch>0 then round(effort_sch/(ev_sch/ac_sch),2)-ac_sch else effort_sch end adjusted_rem_effort_sch,
(effort_sch-ev_sch) rem_effort_sch,
case when pv_prog>0 then round(ev_prog/pv_prog,2) else 0 end spi_prog,
case when ac_prog>0 then round(ev_prog/ac_prog,2) else 0 end hpi_prog,
case when ac_prog>0 and ev_prog>0 then round(effort_prog/(ev_prog/ac_prog),2) else effort_prog end adjusted_effort_prog,
case when ac_prog>0 and ev_prog>0 then round(effort_prog/(ev_prog/ac_prog),2)-ac_prog else effort_prog end adjusted_rem_effort_prog,
(effort_prog-ev_prog) rem_effort_prog,
case when pv_test>0 then round(ev_test/pv_test,2) else 0 end spi_test,
case when ac_test>0 then round(ev_test/ac_test,2) else 0 end hpi_test,
case when ac_test>0 and ev_test>0 then round(effort_test/(ev_test/ac_test),2) else effort_test end adjusted_effort_test,
case when ac_test>0 and ev_test>0 then round(effort_test/(ev_test/ac_test),2)-ac_test else effort_test end adjusted_rem_effort_test,
(effort_test-ev_test) rem_effort_test,
case when pv_tc>0 then round(ev_tc/pv_tc,2) else 0 end spi_tc,
case when ac_tc>0 then round(ev_tc/ac_tc,2) else 0 end hpi_tc,
case when ac_tc>0 and ev_tc>0 then round(effort_tc/(ev_tc/ac_tc),2) else effort_tc end adjusted_effort_tc,
case when ac_tc>0 and ev_tc>0 then round(effort_tc/(ev_tc/ac_tc),2)-ac_tc else effort_tc end adjusted_rem_effort_tc,
(effort_tc-ev_tc) rem_effort_tc,
case when pv_clearance>0 then round(ev_clearance/pv_clearance,2) else 0 end spi_clearance,
case when ac_clearance>0 then round(ev_clearance/ac_clearance,2) else 0 end hpi_clearance,
case when ac_clearance>0 and ev_clearance>0 then round(effort_clearance/(ev_clearance/ac_clearance),2) else effort_clearance end adjusted_effort_clearance,
case when ac_clearance>0 and ev_clearance>0 then round(effort_clearance/(ev_clearance/ac_clearance),2)-ac_clearance else effort_clearance end adjusted_rem_effort_clearance,
(effort_clearance-ev_clearance) rem_effort_clearance,
effort-(effort_sch+effort_prog+effort_test+effort_tc+effort_clearance) effort_others
from (
select v.*,
round(effort_sch*pr_sch/100,2) ev_sch,
CASE 
WHEN (OBTER_DIAS_UTEIS_PERIODO(dt_start_sch, TRUNC(SYSDATE), 1) >= OBTER_DIAS_UTEIS_PERIODO(dt_start_sch, dt_finish_sch, 1))
 THEN effort_sch
 ELSE (((OBTER_DIAS_UTEIS_PERIODO(dt_start_sch, TRUNC(SYSDATE), 1) / OBTER_DIAS_UTEIS_PERIODO(dt_start_sch, dt_finish_sch, 1))) * effort_sch)
END pv_sch,
round(effort_prog*pr_prog/100,2) ev_prog,
CASE 
WHEN (OBTER_DIAS_UTEIS_PERIODO(dt_start_prog, TRUNC(SYSDATE), 1) >= OBTER_DIAS_UTEIS_PERIODO(dt_start_prog, dt_finish_prog, 1))
 THEN effort_prog
 ELSE (((OBTER_DIAS_UTEIS_PERIODO(dt_start_prog, TRUNC(SYSDATE), 1) / OBTER_DIAS_UTEIS_PERIODO(dt_start_prog, dt_finish_prog, 1))) * effort_prog)
END pv_prog,
round(effort_test*pr_test/100,2) ev_test,
CASE 
WHEN (OBTER_DIAS_UTEIS_PERIODO(dt_start_test, TRUNC(SYSDATE), 1) >= OBTER_DIAS_UTEIS_PERIODO(dt_start_test, dt_finish_test, 1))
 THEN effort_test
 ELSE (((OBTER_DIAS_UTEIS_PERIODO(dt_start_test, TRUNC(SYSDATE), 1) / OBTER_DIAS_UTEIS_PERIODO(dt_start_test, dt_finish_test, 1))) * effort_test)
END pv_test,
round(effort_tc*pr_tc/100,2) ev_tc,
CASE 
WHEN (OBTER_DIAS_UTEIS_PERIODO(dt_start_tc, TRUNC(SYSDATE), 1) >= OBTER_DIAS_UTEIS_PERIODO(dt_start_tc, dt_finish_tc, 1))
 THEN effort_tc
 ELSE (((OBTER_DIAS_UTEIS_PERIODO(dt_start_tc, TRUNC(SYSDATE), 1) / OBTER_DIAS_UTEIS_PERIODO(dt_start_tc, dt_finish_tc, 1))) * effort_tc)
END pv_tc,
round(effort_clearance*pr_clearance/100,2) ev_clearance,
CASE 
WHEN (OBTER_DIAS_UTEIS_PERIODO(dt_start_clearance, TRUNC(SYSDATE), 1) >= OBTER_DIAS_UTEIS_PERIODO(dt_start_clearance, dt_finish_clearance, 1))
 THEN effort_clearance
 ELSE (((OBTER_DIAS_UTEIS_PERIODO(dt_start_clearance, TRUNC(SYSDATE), 1) / OBTER_DIAS_UTEIS_PERIODO(dt_start_clearance, dt_finish_clearance, 1))) * effort_clearance)
END pv_clearance,
case
when pr_test=100 and sos_vev_imped=0 and sos_cp=0 then '10 - validation'
when pr_test=100 and sos_cp=0 then '09 - clearance'
when pr_prog=100 and pr_test_dev=100 and pr_dev_end=100 and pr_test>0 and sos_cp=0 then '08 - verification in progress'
when pr_prog=100 and pr_test_dev=100 and pr_dev_end=100 and pr_test=0 and sos_cp=0 then '07 - verification to start'
when pr_prog=100 and pr_test_dev=100 and pr_cp_dev=100 and (pr_dev_end<100 or sos_cp>0) then '06 - CP actions'
when pr_prog=100 and pr_test_dev=100 and pr_cp_dev<100 then '05 - CP pending'
when pr_prog=100 and pr_test_dev<100 then '04 - test_analyst'
when pr_sch=100 then '03 - programming'
when pr_sch>0 then '02 - schematic'
else '01 - not_initiated'
end status_mig
from (
select nr_prog,nr_proj,
case when pv=0 then 0 else round(ev/pv,2) end spi, case when ac=0 then 0 else round(ev/ac,2) end hpi,
deliverable,dt_start,dt_finish,pr_cp_sch,dt_cp_sch,pr_cp_dev,dt_cp_dev,effort,pv,ev,ac,analyst,function_cat,sos_vev,sos_vev_s5,(sos_vev-sos_vev_s5) sos_vev_imped,sos_closed,sos_total,(sos_total-sos_closed) sos_open,
sos_vev_analista,sos_vev_doc_def,sos_vev_prog,sos_vev_test,sos_vev_triagem,sos_vev_tec,sos_vev_test_analista,sos_vev_total,sos_vev_s4,
round(ev/effort,2) pr_actual, round(pv/effort,2) pr_planned,
(select f.qt_hora_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_sch) effort_sch,
(select f.qt_hora_real from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_sch) ac_sch,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_sch) pr_sch,
(select f.dt_inicio_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_sch) dt_start_sch,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_sch) dt_finish_sch,
(select f.qt_hora_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_prog) effort_prog,
(select f.qt_hora_real from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_prog) ac_prog,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_prog) pr_prog,
(select f.dt_inicio_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_prog) dt_start_prog,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_prog) dt_finish_prog,
(select f.qt_hora_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test) effort_test,
(select f.qt_hora_real from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test) ac_test,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test) pr_test,
(select f.dt_inicio_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test) dt_start_test,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test) dt_finish_test,
(select f.qt_hora_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_tc) effort_tc,
(select f.qt_hora_real from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_tc) ac_tc,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_tc) pr_tc,
(select f.dt_inicio_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_tc) dt_start_tc,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_tc) dt_finish_tc,
(select f.qt_hora_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_clearance) effort_clearance,
(select f.qt_hora_real from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_clearance) ac_clearance,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_clearance) pr_clearance,
(select f.dt_inicio_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_clearance) dt_start_clearance,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_clearance) dt_finish_clearance,
(select f.qt_hora_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test_dev) effort_test_dev,
(select f.qt_hora_real from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test_dev) ac_test_dev,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test_dev) pr_test_dev,
(select f.dt_inicio_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test_dev) dt_start_test_dev,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_test_dev) dt_finish_test_dev,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_dev_end) pr_dev_end,
(select count(1) from MAN_ORDEM_SERVICO mo where mo.nr_seq_estagio<>9 AND MO.NR_SEQ_PROJ_CRON_ETAPA=nr_seq_dev_end) sos_cp,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQuencia=nr_seq_dev_end) pr_icon,
(select count(1) from MAN_ORDEM_SERVICO mo where mo.nr_seq_estagio<>9 AND MO.NR_SEQ_PROJ_CRON_ETAPA=nr_seq_icon) so_icon_open,
(select count(1) from MAN_ORDEM_SERVICO mo where MO.NR_SEQ_PROJ_CRON_ETAPA=nr_seq_icon) so_icon,
(select mo.nr_sequencia from MAN_ORDEM_SERVICO mo where MO.NR_SEQ_PROJ_CRON_ETAPA=nr_seq_icon) so_icon_id,
(select mo.NR_SEQ_GRUPO_DES from MAN_ORDEM_SERVICO mo where MO.NR_SEQ_PROJ_CRON_ETAPA=nr_seq_icon) so_icon_grupo
from (
SELECT p.nr_sequencia nr_proj, p.nr_seq_programa nr_prog, trim(M.DS_ATIVIDADE) deliverable, M.DT_INICIO_PREV dt_start, M.DT_FIM_PREV dt_finish,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA=m.NR_SEQ_CRONOGRAMA and F.IE_CHECKPOINT='S' and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.01%') pr_cp_sch,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA=m.NR_SEQ_CRONOGRAMA and F.IE_CHECKPOINT='S' and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.01%') dt_cp_sch,
(select f.pr_etapa from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA=m.NR_SEQ_CRONOGRAMA and F.IE_CHECKPOINT='S' and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.02%') pr_cp_dev,
(select f.dt_fim_prev from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA=m.NR_SEQ_CRONOGRAMA and F.IE_CHECKPOINT='S' and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.02%') dt_cp_dev,
round((select sum (F.QT_HORA_PREV) from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA = m.NR_SEQ_CRONOGRAMA and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.%' and NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior = f.nr_sequencia)),2) effort,
round((select sum (least(OBTER_DIAS_UTEIS_PERIODO(f.dt_inicio_prev, TRUNC(SYSDATE), 1) / greatest(OBTER_DIAS_UTEIS_PERIODO(f.dt_inicio_prev, f.dt_fim_prev, 1),1),1)*f.qt_hora_prev) from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA = m.NR_SEQ_CRONOGRAMA and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.%' and NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior = f.nr_sequencia)),2) pv,
round((select sum (F.QT_HORA_PREV*F.PR_ETAPA)/100 from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA = m.NR_SEQ_CRONOGRAMA and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.%' and NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior = f.nr_sequencia)),2) ev,
round(((select SUM(mosa.QT_MINUTO) 
from MAN_ORDEM_SERV_ATIV mosa INNER JOIN MAN_ORDEM_SERVICO mos on mosa.NR_SEQ_ORDEM_SERV=mos.NR_SEQUENCIA INNER JOIN PROJ_CRON_ETAPA f on mos.NR_SEQ_PROJ_CRON_ETAPA = f.NR_SEQUENCIA 
where F.NR_SEQ_CRONOGRAMA=m.NR_SEQ_CRONOGRAMA and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.%')/60),2) ac,
(SELECT min(r.cd_programador) from PROJ_CRON_ETAPA_EQUIPE r where r.NR_SEQ_ETAPA_CRON=(select f.NR_SEQUENCIA from PROJ_CRON_ETAPA f WHERE F.NR_SEQ_CRONOGRAMA=m.NR_SEQ_CRONOGRAMA and F.IE_CHECKPOINT='S' and F.CD_CLASSIFICACAO LIKE m.CD_CLASSIFICACAO||'.01%')) analyst,
'green' function_cat,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and mo.nr_seq_estagio<>9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_vev,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and mo.nr_seq_estagio<>9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) AND upper(mo.ds_dano_breve) LIKE 'S5%') sos_vev_s5,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and mo.nr_seq_estagio<>9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) AND upper(mo.ds_dano_breve) LIKE 'S4%') sos_vev_s4,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_vev_total,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio=999999) sos_vev_analista,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio=2011) sos_vev_doc_def,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (1191,732,1751)) sos_vev_prog,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (791,2155,1071,1234,1061)) sos_vev_test,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (1051,4,731,2112)) sos_vev_triagem,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (1902,1731,131,2213,2206,2194,2205,2212,2195,2204)) sos_vev_tec,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio=2082) sos_vev_test_analista,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and mo.nr_seq_estagio=9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_closed,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_total,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and pe.ds_atividade LIKE '%Analyst Sos%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_sch,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and pe.ds_atividade LIKE '%Sos Programming%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_prog,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and (trim(pe.ds_atividade) LIKE 'Smoke Test%' or trim(pe.ds_atividade) LIKE 'Round%') and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_test,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE '%Test Cases Creation%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_tc,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_clearance,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Finilize the Check Point Actions' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_dev_end,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Function Flow Review and Corrections%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_test_dev,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE '%Function Icon%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_icon
FROM proj_projeto p
INNER JOIN proj_cronograma c
ON p.nr_sequencia = c.nr_seq_proj
INNER JOIN proj_cron_etapa m
ON c.nr_sequencia = m.nr_seq_cronograma
WHERE p.NR_SEQ_PROGRAMA IN (146,136,140,133,147,145,144,135,134,142,137,143,139,131,129,141,113,114,148)
AND c.ie_situacao = 'A' 
AND c.DT_APROVACAO is not null 
AND m.NR_SEQ_SUB_PROJ is null 
AND (c.IE_CLASSIFICACAO <> 'N' OR c.IE_CLASSIFICACAO IS NULL)
AND m.IE_MILESTONE = 'S'
and m.cd_classificacao='1'
AND (select count(1)
from proj_projeto pp INNER JOIN proj_cronograma pc ON pp.nr_sequencia=pc.nr_seq_proj INNER JOIN proj_cron_etapa pe ON pc.nr_sequencia=pe.NR_SEQ_CRONOGRAMA
where pe.IE_MILESTONE = 'S' and pp.NR_SEQUENCIA = p.NR_SEQUENCIA AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null and pe.cd_classificacao='1') = 1
UNION
SELECT p.nr_sequencia nr_proj, p.nr_seq_programa nr_prog, trim(P.DS_TITULO) deliverable, P.DT_INICIO_PREV dt_start, P.DT_FIM_PREV dt_finish,
null pr_cp_sch,
null dt_cp_sch,
(select e.pr_etapa from proj_cronograma c INNER JOIN proj_cron_etapa e ON c.nr_sequencia=E.NR_SEQ_CRONOGRAMA where E.IE_CHECKPOINT = 'S' and c.NR_SEQ_PROJ = p.nr_sequencia AND c.ie_situacao = 'A' AND c.DT_APROVACAO is not null) pr_cp_dev,
(select e.dt_fim_prev from proj_cronograma c INNER JOIN proj_cron_etapa e ON c.nr_sequencia=E.NR_SEQ_CRONOGRAMA where E.IE_CHECKPOINT = 'S' and c.NR_SEQ_PROJ = p.nr_sequencia AND c.ie_situacao = 'A' AND c.DT_APROVACAO is not null) dt_cp_dev,
round((SELECT SUM(e.QT_HORA_PREV) FROM proj_projeto pp INNER JOIN proj_cronograma c ON pp.nr_sequencia = c.nr_seq_proj INNER JOIN proj_cron_etapa e ON c.nr_sequencia = e.nr_seq_cronograma WHERE pp.nr_sequencia = p.nr_sequencia AND c.ie_situacao  = 'A' AND c.DT_APROVACAO is not null AND e.NR_SEQ_SUB_PROJ is null AND (c.IE_CLASSIFICACAO <> 'N' OR c.IE_CLASSIFICACAO IS NULL) AND NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior =  e.nr_sequencia)),2) effort,
round((SELECT SUM(least(OBTER_DIAS_UTEIS_PERIODO(e.dt_inicio_prev, TRUNC(SYSDATE), 1) / greatest(OBTER_DIAS_UTEIS_PERIODO(e.dt_inicio_prev, e.dt_fim_prev, 1),1),1)*e.qt_hora_prev) FROM proj_projeto pp INNER JOIN proj_cronograma c ON pp.nr_sequencia = c.nr_seq_proj INNER JOIN proj_cron_etapa e ON c.nr_sequencia = e.nr_seq_cronograma WHERE pp.nr_sequencia = p.nr_sequencia AND c.ie_situacao  = 'A' AND c.DT_APROVACAO is not null AND e.NR_SEQ_SUB_PROJ is null AND (c.IE_CLASSIFICACAO <> 'N' OR c.IE_CLASSIFICACAO IS NULL) AND NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior =  e.nr_sequencia)),2) pv,
round((SELECT SUM((e.QT_HORA_PREV*e.PR_ETAPA)/100) FROM proj_projeto pp INNER JOIN proj_cronograma c ON pp.nr_sequencia = c.nr_seq_proj INNER JOIN proj_cron_etapa e ON c.nr_sequencia = e.nr_seq_cronograma WHERE pp.nr_sequencia = p.nr_sequencia AND c.ie_situacao  = 'A' AND c.DT_APROVACAO is not null AND e.NR_SEQ_SUB_PROJ is null AND (c.IE_CLASSIFICACAO <> 'N' OR c.IE_CLASSIFICACAO IS NULL) AND NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior =  e.nr_sequencia)),2) ev,
round((SELECT SUM(mo.QT_MINUTO)/60 FROM proj_projeto pp INNER JOIN proj_cronograma c ON pp.nr_sequencia = c.nr_seq_proj INNER JOIN proj_cron_etapa e ON c.nr_sequencia = e.nr_seq_cronograma INNER JOIN MAN_ORDEM_SERVICO m ON m.NR_SEQ_PROJ_CRON_ETAPA = e.nr_sequencia INNER JOIN MAN_ORDEM_SERV_ATIV mo ON MO.NR_SEQ_ORDEM_SERV = m.nr_sequencia WHERE pp.nr_sequencia = p.nr_sequencia AND c.ie_situacao  = 'A' AND c.DT_APROVACAO is not null AND e.NR_SEQ_SUB_PROJ is null AND (c.IE_CLASSIFICACAO <> 'N' OR c.IE_CLASSIFICACAO IS NULL) AND NOT EXISTS (SELECT 1 FROM proj_cron_etapa xx WHERE xx.nr_seq_superior =  e.nr_sequencia)),2) ac,
p.cd_coordenador analyst,
'blue' function_cat,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and mo.nr_seq_estagio<>9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_vev,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and mo.nr_seq_estagio<>9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) AND upper(mo.ds_dano_breve) LIKE 'S5%') sos_vev_s5,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and mo.nr_seq_estagio<>9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) AND upper(mo.ds_dano_breve) LIKE 'S4%') sos_vev_s4,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_vev_total,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio=999999) sos_vev_analista,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio=2011) sos_vev_doc_def,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (1191,732,1751)) sos_vev_prog,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (791,2155,1071,1234,1061)) sos_vev_test,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (1051,4,731,2112)) sos_vev_triagem,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio IN (1902,1731,131,2213,2206,2194,2205,2212,2195,2204)) sos_vev_tec,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL) and mo.nr_seq_estagio=2082) sos_vev_test_analista,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and mo.nr_seq_estagio=9 AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_closed,
(select count(1) from MAN_ORDEM_SERVICO mo inner join PROJ_CRON_ETAPA pe on Mo.NR_SEQ_PROJ_CRON_ETAPA=pe.nr_sequencia inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) sos_total,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and pe.ds_atividade LIKE '%Analyst Sos%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_sch,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and pe.ds_atividade LIKE '%Sos Programming%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_prog,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and (trim(pe.ds_atividade) LIKE 'Smoke Test%' or trim(pe.ds_atividade) LIKE 'Round%') and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_test,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE '%Test Cases Creation%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_tc,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Test Clearance (Bug fixing)%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_clearance,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Finilize the Check Point Actions' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_dev_end,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE 'Function Flow Review and Corrections%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_test_dev,
(select PE.nr_sequencia from PROJ_CRON_ETAPA pe inner JOIN PROJ_CRONOGRAMA pc on pe.nr_seq_cronograma=pc.nr_sequencia where pc.nr_seq_proj=p.nr_sequencia and trim(pe.ds_atividade) LIKE '%Function Icon%' and pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null AND (pc.IE_CLASSIFICACAO <> 'N' OR pc.IE_CLASSIFICACAO IS NULL)) nr_seq_icon
FROM proj_projeto p
INNER JOIN proj_cronograma c
ON p.nr_sequencia = c.nr_seq_proj
WHERE p.NR_SEQ_PROGRAMA IN (146,136,140,133,147,145,144,135,134,142,137,143,139,131,129,141,113,114,148)
AND c.ie_situacao = 'A' 
AND c.DT_APROVACAO is not null 
AND (c.IE_CLASSIFICACAO <> 'N' OR c.IE_CLASSIFICACAO IS NULL)
AND p.NR_SEQ_ESTAGIO <> 44 
AND (select count(1)
from proj_projeto pp INNER JOIN proj_cronograma pc ON pp.nr_sequencia=pc.nr_seq_proj INNER JOIN proj_cron_etapa pe ON pc.nr_sequencia=pe.NR_SEQ_CRONOGRAMA
where pe.IE_MILESTONE = 'S' and pp.NR_SEQUENCIA = p.NR_SEQUENCIA AND pc.ie_situacao = 'A' AND pc.DT_APROVACAO is not null and pe.cd_classificacao='1') <> 1
) ) v ) w
;