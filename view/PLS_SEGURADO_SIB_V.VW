create or replace view pls_segurado_sib_v
as
select	*
from	pls_segurado_sib a
where	a.nr_sequencia	= (	select	max(x.nr_sequencia)
				from	pls_segurado_sib x
				where	a.nr_seq_segurado = x.nr_seq_segurado);
/