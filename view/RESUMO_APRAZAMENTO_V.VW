create or replace 
view resumo_aprazamento_v
as
select	1 nr_seq_apresent,
		substr(obter_desc_expressao(314221, null),1,255) ie_tipo_item,
		substr(obter_desc_material(cd_material),1,80) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		nr_agrupamento
from	prescr_material
where	ie_agrupador = 12
and		nvl(ie_suspenso,'N') <> 'S'
union
select	2 nr_seq_apresent,
		substr(obter_desc_expressao(314222, null),1,255) ie_tipo_item,
		substr(obter_desc_material(cd_material),1,80) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		nr_agrupamento
from	prescr_material
where	ie_agrupador = 8
and		nvl(ie_suspenso,'N') <> 'S'
union
select	3 nr_se_apresent,
		substr(obter_desc_expressao(293085, null),1,255) ie_tipo_item,
		substr(obter_desc_material(cd_material),1,80) ds_item,
		substr(decode(nvl(ie_administrar,'S'),'N',obter_desc_expressao(331200, null), substr(ds_horarios,1,255)),1,255) ds_horarios,
		nr_prescricao,
		nr_agrupamento
from	prescr_material
where	ie_agrupador = 1
and		obter_classif_material_proced(cd_material, null,null) <> 1
and		nr_sequencia_solucao is null
and		nr_sequencia_proc is null
and		nvl(ie_suspenso,'N') <> 'S'
--and	nvl(ie_administrar,'S') = 'S'
and		nr_sequencia_diluicao is null
union
select	4 nr_seq_apresent,
		substr(obter_desc_expressao(298694, null),1,255) ie_tipo_item,
		substr(nvl(ds_solucao,obter_prim_comp_sol(nr_prescricao, nr_seq_solucao)),1,240) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	prescr_solucao
where	nvl(ie_suspenso,'N') <> 'S'
union
select	5 nr_seq_apresent,
		substr(obter_desc_expressao(314362, null),1,255) ie_tipo_item,
		substr(Obter_Desc_Prescr_Proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,240) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	prescr_procedimento
where	obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced, 'O') = 'S'
and		nvl(ie_suspenso,'N') <> 'S'
and		nr_seq_origem is null
union
select	6 nr_seq_apresent,
		substr(obter_desc_expressao(314282, null),1,255) ie_tipo_item,
		substr(obter_rec_prescricao(nr_sequencia, nr_prescricao),1,255)	ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	prescr_recomendacao
where	nvl(ie_suspenso,'N') <> 'S'
union
select	7 nr_seq_apresent,
		substr(obter_desc_expressao(310507, null),1,255) ie_tipo_item,
		substr(obter_desc_hemoterapia(nr_prescricao, nr_sequencia, ie_tipo_proced, cd_procedimento, ie_origem_proced, nr_seq_proc_interno, nr_seq_derivado),1,50) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	prescr_procedimento
where	( obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'BSHE') = 'S'
		or obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'PH') = 'S' )
and		nvl(ie_suspenso,'N') <> 'S'
and		nr_seq_origem is null
union
select	8 nr_seq_apresent,
		substr(obter_desc_expressao(290567, null),1,255) ie_tipo_item,
		substr(obter_descricao_padrao('GAS', 'DS_GAS', nr_seq_gas),1,255) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	prescr_gasoterapia
where	nvl(ie_suspenso,'N') <> 'S'
union
select	9 nr_seq_apresent,
		substr(obter_desc_expressao(287902, null),1,255) ie_tipo_item,
		substr(Obter_desc_Dieta(cd_dieta),1,255) ds_item,
		substr(ds_horarios,1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	prescr_dieta
where	nvl(ie_suspenso,'N') <> 'S'
union
select	10 nr_seq_apresent,
		substr(obter_desc_expressao(341829, null),1,255) ie_tipo_item,
		substr(obter_desc_material(b.cd_material),1,80) ds_item,
		substr(a.ds_horarios,1,255) ds_horarios,
		a.nr_prescricao,
		0 nr_agrupamento
from	prescr_leite_deriv a,
		prescr_material b
where	a.nr_prescricao = b.nr_prescricao
and		b.nr_seq_leite_deriv = a.nr_sequencia
and 	nvl(ie_suspenso,'N') <> 'S'
union   
select 	11 nr_seq_apresent,
		substr(obter_desc_expressao(304828, null),1,255) ie_tipo_item,
		substr(obter_desc_tipo_jejum(nr_seq_tipo),1,80) ds_item,
		substr(to_char(dt_inicio, 'hh24:mi'),1,255) ds_horarios,
		nr_prescricao,
		0 nr_agrupamento
from	rep_jejum
where 	nvl(ie_suspenso,'N') <> 'S'
/
