create or replace
view UJ_adep_pend_v
as
select	1 nr_seq_apresent,
	'D' ie_tipo_item,
	'Dieta oral' ds_tipo_item,
	'Dietas orais' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	-1 nr_seq_item,
	null ie_acm_sn,
	null ie_di,
	c.cd_refeicao cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,
	substr(obter_valor_dominio(99,c.cd_refeicao),1,240) ds_item,
	to_number(null) nr_agrupamento,
	'' cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(null) nr_seq_area_prep,
	to_number(null) nr_seq_lote,
	to_number(null) nr_seq_processo,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	'' ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	to_number(null) nr_seq_superior,
	null ie_item_superior,
	null ds_justificativa
from	prescr_dieta_hor c,
	prescr_medica a
where	c.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	2 nr_seq_apresent,
	'S' ie_tipo_item,
	'Suplemento oral' ds_tipo_item,
	'Suplementos orais' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	to_number(null) ie_origem_proced,	
	to_number(null) nr_seq_proc_interno,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	to_number(null) nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(c.nr_seq_area_prep) nr_seq_area_prep,
	c.nr_seq_lote nr_seq_lote,
	c.nr_seq_processo nr_seq_processo,	
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	b.ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	c.nr_seq_superior,
	b.ie_item_superior,
	b.ds_justificativa
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador IN (12)
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	3 nr_seq_apresent,
	'M' ie_tipo_item,
	'Medicamento' ds_tipo_item,
	'Medicamentos' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	nvl(a.dt_liberacao,b.dt_lib_material),
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,	
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	b.nr_agrupamento nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	substr(obter_diluicao_medic_adep(c.nr_sequencia),1,240) ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(c.nr_seq_area_prep) nr_seq_area_prep,	
	c.nr_seq_lote nr_seq_lote,
	c.nr_seq_processo nr_seq_processo,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	nvl(a.dt_liberacao_medico,b.dt_lib_material),
	b.ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	c.nr_seq_superior,
	b.ie_item_superior,
	b.ds_justificativa
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 1
and	obter_se_gerar_item_adep('M',b.nr_prescricao,b.nr_sequencia,b.nr_agrupamento,'N') = 'S'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	4 nr_seq_apresent,
	'MAT' ie_tipo_item,
	'Material' ds_tipo_item,
	'Materiais' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,	
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	to_number(null) nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(c.nr_seq_area_prep) nr_seq_area_prep,
	c.nr_seq_lote nr_seq_lote,
	c.nr_seq_processo nr_seq_processo,	
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	b.ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	c.nr_seq_superior,
	b.ie_item_superior,
	b.ds_justificativa
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 2
and	nvl(a.ie_adep,'S') = 'S'
and	obter_se_material_adep(a.cd_estabelecimento,b.cd_material) = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	--4 nr_seq_apresent,
	--'P' ie_tipo_item,
	--'Procedimento' ds_tipo_item,
	--'Procedimentos' ds_tipo_item_plural,
	decode(b.nr_seq_exame, null, 5, 8) nr_seq_apresent,
	decode(b.nr_seq_exame, null, 'P', 'L') ie_tipo_item,
	decode(b.nr_seq_exame, null, 'Procedimento', 'Coleta') ds_tipo_item,
	decode(b.nr_seq_exame, null, 'Procedimentos', 'Coletas') ds_tipo_item_plural,
	decode(b.nr_seq_exame, null, 'N', 'S') ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	'N' ie_di,
	to_char(b.cd_procedimento) cd_item,
	b.ie_origem_proced,
	b.nr_seq_proc_interno,
	decode(b.nr_seq_exame, null, substr(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno),1,240), substr(obter_desc_exame(b.nr_seq_exame),1,240)) ds_item,
	to_number(null) nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	substr(obter_desc_topografia_dor(b.nr_seq_topografia),1,60) ds_topografia,
	substr(obter_se_assoc_proc_adep(b.cd_procedimento,b.ie_origem_proced),1,1) ie_assoc_adep,
	to_number(null) nr_seq_area_prep,
	to_number(null) nr_seq_lote,
	to_number(null) nr_seq_processo,	
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	'' ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	to_number(null) nr_seq_superior,
	null ie_item_superior,
	null ds_justificativa
from	prescr_proc_hor c,
	prescr_procedimento b,
	prescr_medica a
where	c.nr_seq_procedimento = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.nr_seq_solic_sangue is null
and	b.nr_seq_derivado is null
and	b.nr_seq_prot_glic is null
and	((b.nr_seq_proc_interno is null) or (obter_ctrl_glic_proc(b.nr_seq_proc_interno) = 'NC'))
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	6 nr_seq_apresent,
	'R' ie_tipo_item,
	'Recomendação' ds_tipo_item,
	'Recomendações' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(padroniza_horario(b.ds_horarios), '', 'S', 'N') ie_acm_sn,
	'N' ie_di,
	nvl(to_char(b.cd_recomendacao), b.ds_recomendacao) cd_item,
	to_number(null) ie_origem_proced,	
	to_number(null) nr_seq_proc_interno,	
	substr(obter_rec_prescricao(b.nr_sequencia, b.nr_prescricao),1,240) ds_item,
	to_number(null) nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(null) nr_seq_area_prep,
	to_number(null) nr_seq_lote,
	to_number(null) nr_seq_processo,	
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	'' ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	to_number(null) nr_seq_superior,
	null ie_item_superior,
	null ds_justificativa
from	prescr_rec_hor c,
	prescr_recomendacao b,
	prescr_medica a
where	c.nr_seq_recomendacao = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	7 nr_seq_apresent,
	'E' ie_tipo_item,
	'SAE' ds_tipo_item,
	'SAE' ds_tipo_item_plural,
	'N' ie_laboratorio,
	to_number(null) cd_estabelecimento,
	a.nr_atendimento,
	a.nr_sequencia,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	to_number(null) cd_setor_prescr,
	'N' ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', decode(padroniza_horario(b.ds_horarios), '', 'S', 'N')) ie_acm_sn,
	'N' ie_di,
	to_char(b.nr_seq_proc) cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,		
	substr(obter_desc_intervencoes(b.nr_seq_proc),1,240) ds_item,
	to_number(null) nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	substr(obter_desc_topografia_dor(b.nr_seq_topografia),1,60) ds_topografia,
	'S' ie_assoc_adep,
	to_number(null) nr_seq_area_prep,
	to_number(null) nr_seq_lote,
	to_number(null) nr_seq_processo,	
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	to_date(null) dt_liberacao_medico,
	'' ie_urgencia,
	to_number(null) cd_local_estoque,
	to_number(null) nr_seq_superior,
	null ie_item_superior,
	null ds_justificativa
from	pe_prescr_proc_hor c,
	pe_prescr_proc b,
	pe_prescricao a
where	c.nr_seq_pe_proc = b.nr_sequencia
and	c.nr_seq_pe_prescr = b.nr_seq_prescr
and	b.nr_seq_prescr = a.nr_sequencia
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
--and	nvl(b.ie_adep,'N') = 'S'
and	nvl(b.ie_adep,'N') in ('S','M')
and	nvl(a.ie_situacao, 'A') = 'A'
union
select	8 nr_seq_apresent,
	'ME' ie_tipo_item,
	'Medicamento' ds_tipo_item,
	'Medicamentos' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,	
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	b.nr_agrupamento nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	substr(obter_diluicao_medic_adep(c.nr_sequencia),1,240) ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(c.nr_seq_area_prep) nr_seq_area_prep,	
	c.nr_seq_lote nr_seq_lote,
	c.nr_seq_processo nr_seq_processo,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	b.ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	c.nr_seq_superior,
	b.ie_item_superior,
	b.ds_justificativa
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 14
and	obter_se_gerar_item_adep('M',b.nr_prescricao,b.nr_sequencia,b.nr_agrupamento,'N') = 'S'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	9 nr_seq_apresent,
	'MAP' ie_tipo_item,
	'Medicamento' ds_tipo_item,
	'Medicamentos' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,	
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	b.nr_agrupamento nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	substr(obter_diluicao_medic_adep(c.nr_sequencia),1,240) ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(c.nr_seq_area_prep) nr_seq_area_prep,	
	c.nr_seq_lote nr_seq_lote,
	c.nr_seq_processo nr_seq_processo,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	a.dt_liberacao_medico,
	b.ie_urgencia,
	obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
	c.nr_seq_superior,
	b.ie_item_superior,
	b.ds_justificativa
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 5
and	obter_se_gerar_item_adep('M',b.nr_prescricao,b.nr_sequencia,b.nr_agrupamento,'N') = 'S'
and	nvl(a.ie_adep,'S') = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	10 nr_seq_apresent,
	'GAS' ie_tipo_item,
	'Gasoterapia' ds_tipo_item,
	'Gasoterapias' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
	b.nr_sequencia nr_seq_item,
	substr(obter_valor_dominio(2569,b.ie_inicio),1,100) ie_acm_sn,
	'N' ie_di,
	to_char(b.nr_seq_gas) cd_item,
	to_number(null) ie_origem_proced,
	to_number(null) nr_seq_proc_interno,	
	substr(x.ds_gas,1,240) ds_item,
	to_number(null) nr_agrupamento,
	b.cd_intervalo cd_intervalo,
	'' ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	to_number(null) nr_seq_area_prep,	
	to_number(null) nr_seq_lote,
	to_number(null) nr_seq_processo,
	to_number(null) nr_seq_horario,
	b.dt_prev_execucao dt_horario,
	to_number(null) qt_pend,
	b.cd_unidade_medida cd_um,
	b.qt_gasoterapia qt_dose,
	b.cd_unidade_medida cd_unidade_medida_dose,
	'' ie_via,
	-- cd_pessoa_evento,
	-- nm_pessoa_evento,
	'' cd_pessoa_evento,
	'' nm_pessoa_evento,
	null dt_liberacao_medico,
	'' ie_urgencia,
	null cd_local_estoque,
	null nr_seq_superior,
	null ie_item_superior,
	'' ds_justificativa
from	gas x,
	prescr_gasoterapia b,
	prescr_medica a
where	b.nr_prescricao = a.nr_prescricao
and	b.nr_seq_gas	= x.nr_sequencia
and	not exists   (select 1
				  from   prescr_gasoterapia_evento c 
				  where  c.nr_seq_gasoterapia = b.nr_sequencia);
/
