create or replace view ecd_demonstracao_resultado_v2
as
select	1									tp_registro,
	'|J150'									ds_identificador,
	c.dt_referencia								dt_referencia,
	a.cd_conta_contabil								cd_aglutinacao,
	ctb_obter_nivel_conta(a.cd_conta_contabil)					ie_nivel_aglutinacao,
	b.ds_conta_contabil							ds_aglutinacao,
	(nvl(sum(a.vl_saldo), 0) - nvl(sum(a.vl_encerramento), 0)) 				vl_conta,
	substr(ctb_obter_situacao_result(a.cd_conta_contabil, null, sum(a.vl_saldo)),1,255)||'|'	ie_valor,
	b.cd_classificacao								cd_classificacao
from	ctb_grupo_conta d,
	ctb_mes_ref c,
	conta_contabil b,
	ctb_saldo a
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	a.nr_seq_mes_ref		= c.nr_sequencia
and	b.cd_empresa		= c.cd_empresa
and	b.cd_grupo		= d.cd_grupo
and	d.ie_tipo			in ('R','D', 'C')
and	b.ie_ecd_reg_dre	= 'S'
group by	c.dt_referencia, 
	a.cd_conta_contabil,
	b.cd_classificacao,
	d.cd_grupo,
	b.ds_conta_contabil
order by	ie_nivel_aglutinacao;
/