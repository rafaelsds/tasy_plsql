create or replace view pls_segreg_desp_grupo_diops_v
as
select	distinct
	0 ie_tipo_doc,
	a.nr_sequencia,
	nvl(g.vl_liberado,0) vl_total,
	nvl(g.vl_liberado,0) vl_liberado,
	nvl(g.vl_glosa,0) vl_glosa,
	0 vl_coparticipacao,
	lpad(e.ie_segmentacao,2,0) ie_cobertura,
	nvl(g.nr_seq_grupo_ans, -1) nr_seq_grupo_ans,
	f.dt_mes_competencia dt_mes_comp_protocolo,
	b.ie_tipo_vinculo_operadora,
	b.ie_tipo_segurado,
	d.ie_tipo_relacao,
	g.nr_sequencia nr_seq_proc_mat,
	a.vl_total vl_conta,
	nvl(h.nr_seq_apresentacao,9999999999) nr_seq_apresentacao,
	nvl(h.ds_grupo,'Outras despesas') ds_grupo,
	decode(h.ie_tipo_grupo_ans,'10','Consultas',
				'20','Exames',
				'30','Terapias',
				'41','Interna��o','42','Interna��o','43','Interna��o','44','Interna��o','45','Interna��o','46','Interna��o','49','Interna��o',
				'50','Outros atendimentos',
				'60','Demais despesas') ds_grupo_diops,
	substr(obter_valor_dominio(1666,e.ie_tipo_contratacao),1,30) || '-' ||
	substr(obter_valor_dominio(1669,e.ie_preco),1,30) || '-' ||
	decode(e.ie_regulamentacao,'R','N�o regulamentado','Regulamentado') ds_dados_produto,
	decode(f.ie_tipo_protocolo,'I','Interc�mbio','R','Reembolso',
	(select	decode(x.ie_tipo_relacao,'P','Rede Pr�pria','Rede contratada')
	from	pls_prestador x
	where	x.nr_sequencia = a.nr_seq_prestador_exec)) ds_tipo_prestador,
	f.ie_situacao ie_sit_protocolo,
	e.ie_tipo_contratacao,
	e.ie_preco,
	e.ie_regulamentacao,
	f.ie_tipo_protocolo
from	pls_conta		a,
	pls_segurado		b,
	pls_contrato		c,
	pls_prestador		d,
	pls_plano		e,
	pls_protocolo_conta	f,
	pls_conta_proc		g,
	ans_grupo_despesa	h
	--pls_conta_coparticipacao i
where	b.nr_sequencia		= a.nr_seq_segurado
and	c.nr_sequencia(+)	= b.nr_seq_contrato
and	f.nr_sequencia		= a.nr_seq_protocolo
and	f.nr_seq_prestador	= d.nr_sequencia(+)
and	e.nr_sequencia		= b.nr_seq_plano
and	g.nr_seq_conta		= a.nr_sequencia
--and	i.nr_seq_conta_proc(+)	= g.nr_sequencia
and	g.ie_status		<> 'D'
--and	f.ie_tipo_protocolo	in ('C','I','R')
and	((f.ie_tipo_protocolo = 'R') or 
	(b.ie_tipo_segurado not in ('T','C','I','H') and
	f.ie_tipo_protocolo <> 'R') or
	(b.ie_tipo_segurado in ('T','C','I','H') and
	f.ie_tipo_protocolo <> 'R'))
and	h.nr_sequencia(+)	= g.nr_seq_grupo_ans
union
select	distinct
	0 ie_tipo_doc,
	a.nr_sequencia,
	nvl(g.vl_liberado,0) vl_total,
	nvl(g.vl_liberado,0) vl_liberado,
	nvl( g.vl_glosa,0) vl_glosa,
	0 vl_coparticipacao,
	lpad(e.ie_segmentacao,2,0) ie_cobertura,
	nvl(g.nr_seq_grupo_ans, -1) nr_seq_grupo_ans,
	f.dt_mes_competencia dt_mes_comp_protocolo,
	b.ie_tipo_vinculo_operadora ,
	b.ie_tipo_segurado,
	d.ie_tipo_relacao,
	g.nr_sequencia nr_seq_proc_mat,
	a.vl_total vl_conta,
	nvl(h.nr_seq_apresentacao,9999999999) nr_seq_apresentacao,
	nvl(h.ds_grupo,'Outras despesas') ds_grupo,
	decode(h.ie_tipo_grupo_ans,'10','Consultas',
				'20','Exames',
				'30','Terapias',
				'41','Interna��o','42','Interna��o','43','Interna��o','44','Interna��o','45','Interna��o','46','Interna��o','49','Interna��o',
				'50','Outros atendimentos',
				'60','Demais despesas') ds_grupo_diops,
	substr(obter_valor_dominio(1666,e.ie_tipo_contratacao),1,30) || '-' ||
	substr(obter_valor_dominio(1669,e.ie_preco),1,30) || '-' ||
	decode(e.ie_regulamentacao,'R','N�o regulamentado','Regulamentado') ds_dados_produto,
	decode(f.ie_tipo_protocolo,'I','Interc�mbio','R','Reembolso',
	(select	decode(x.ie_tipo_relacao,'P','Rede Pr�pria','Rede contratada')
	from	pls_prestador x
	where	x.nr_sequencia = a.nr_seq_prestador_exec)) ds_tipo_prestador,
	f.ie_situacao ie_sit_protocolo,
	e.ie_tipo_contratacao,
	e.ie_preco,
	e.ie_regulamentacao,
	f.ie_tipo_protocolo
from	pls_conta		a,
	pls_segurado		b,
	pls_contrato		c,
	pls_prestador		d,
	pls_plano		e,
	pls_protocolo_conta 	f,
	pls_conta_mat		g,
	ans_grupo_despesa	h
	--pls_conta_coparticipacao i
where	b.nr_sequencia		= a.nr_seq_segurado
and	b.nr_seq_contrato	= c.nr_sequencia(+)
and	f.nr_sequencia		= a.nr_seq_protocolo
and	f.nr_seq_prestador	= d.nr_sequencia(+)
and	e.nr_sequencia		= b.nr_seq_plano
and	g.nr_seq_conta		= a.nr_sequencia
--and	i.nr_seq_conta_mat(+)	= g.nr_sequencia
and	g.ie_status		<> 'D'
--and	f.ie_tipo_protocolo	in ('C','I','R')
and	((f.ie_tipo_protocolo = 'R') or 
	(b.ie_tipo_segurado not in ('T','C','I','H') and
	f.ie_tipo_protocolo <> 'R') or
	(b.ie_tipo_segurado in ('T','C','I','H') and
	f.ie_tipo_protocolo <> 'R'))
and	h.nr_sequencia(+)	= g.nr_seq_grupo_ans
union
select	1 ie_tipo_doc,
	null,
	nvl(i.vl_coparticipacao,0) * -1 vl_total,
	nvl(g.vl_liberado,0) vl_liberado,
	nvl(g.vl_glosa,0) vl_glosa,
	0 vl_coparticipacao,
	lpad(e.ie_segmentacao,2,0) ie_cobertura,
	nvl(g.nr_seq_grupo_ans, -1) nr_seq_grupo_ans,
	b.dt_referencia,
	f.ie_tipo_vinculo_operadora,
	f.ie_tipo_segurado,
	w.ie_tipo_relacao,
	g.nr_sequencia nr_seq_proc_mat,
	k.vl_total vl_conta,
	nvl(h.nr_seq_apresentacao,9999999999) nr_seq_apresentacao,
	nvl(h.ds_grupo,'Outras despesas') ds_grupo,
	decode(h.ie_tipo_grupo_ans,'10','Consultas',
				'20','Exames',
				'30','Terapias',
				'41','Interna��o','42','Interna��o','43','Interna��o','44','Interna��o','45','Interna��o','46','Interna��o','49','Interna��o',
				'50','Outros atendimentos',
				'60','Demais despesas') ds_grupo_diops,
	substr(obter_valor_dominio(1666,e.ie_tipo_contratacao),1,30) || '-' ||
	substr(obter_valor_dominio(1669,e.ie_preco),1,30) || '-' ||
	decode(e.ie_regulamentacao,'R','N�o regulamentado','Regulamentado') ds_dados_produto,
	decode(j.ie_tipo_protocolo,'I','Interc�mbio','R','Reembolso',
	(select	decode(x.ie_tipo_relacao,'P','Rede Pr�pria','Rede contratada')
	from	pls_prestador x
	where	x.nr_sequencia = k.nr_seq_prestador_exec)) ds_tipo_prestador,
	j.ie_situacao ie_sit_protocolo,
	e.ie_tipo_contratacao,
	e.ie_preco,
	e.ie_regulamentacao,
	j.ie_tipo_protocolo
from	pls_prestador			w,
	pls_protocolo_conta 		j,
	pls_conta			k,
	pls_conta_proc			g,
	pls_conta_coparticipacao	i,
	pls_plano			e,
	pls_segurado			f,
	pls_mensalidade_segurado	c,
	pls_mensalidade			b,
	pls_lote_mensalidade		a,
	ans_grupo_despesa		h
where	c.nr_sequencia	= i.nr_seq_mensalidade_seg
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	a.nr_sequencia	= b.nr_seq_lote
and	e.nr_sequencia	= f.nr_seq_plano
and	f.nr_sequencia	= c.nr_seq_segurado
and	k.nr_sequencia	= g.nr_seq_conta
and	i.nr_seq_conta_proc(+)	= g.nr_sequencia
and	h.nr_sequencia(+)	= g.nr_seq_grupo_ans
and	j.nr_seq_prestador	= w.nr_sequencia(+)
and	j.nr_sequencia	= k.nr_seq_protocolo
and	j.ie_tipo_protocolo	in ('C','I','R')
and	g.ie_status	<> 'D'
and	b.ie_cancelamento is null
union
select	1 ie_tipo_doc,
	null,
	nvl(i.vl_coparticipacao,0) * -1,
	nvl(g.vl_liberado,0) vl_liberado,
	nvl(g.vl_glosa,0) vl_glosa,
	0 vl_coparticipacao,
	lpad(e.ie_segmentacao,2,0) ie_cobertura,
	nvl(g.nr_seq_grupo_ans, -1) nr_seq_grupo_ans,
	b.dt_referencia,
	f.ie_tipo_vinculo_operadora,
	f.ie_tipo_segurado,
	w.ie_tipo_relacao,
	g.nr_sequencia nr_seq_proc_mat,
	k.vl_total vl_conta,
	nvl(h.nr_seq_apresentacao,9999999999) nr_seq_apresentacao,
	nvl(h.ds_grupo,'Outras despesas') ds_grupo,
	decode(h.ie_tipo_grupo_ans,'10','Consultas',
				'20','Exames',
				'30','Terapias',
				'41','Interna��o','42','Interna��o','43','Interna��o','44','Interna��o','45','Interna��o','46','Interna��o','49','Interna��o',
				'50','Outros atendimentos',
				'60','Demais despesas') ds_grupo_diops,
	substr(obter_valor_dominio(1666,e.ie_tipo_contratacao),1,30) || '-' ||
	substr(obter_valor_dominio(1669,e.ie_preco),1,30) || '-' ||
	decode(e.ie_regulamentacao,'R','N�o regulamentado','Regulamentado') ds_dados_produto,
	decode(j.ie_tipo_protocolo,'I','Interc�mbio','R','Reembolso',
	(select	decode(x.ie_tipo_relacao,'P','Rede Pr�pria','Rede contratada')
	from	pls_prestador x
	where	x.nr_sequencia = k.nr_seq_prestador_exec)) ds_tipo_prestador,
	j.ie_situacao ie_sit_protocolo,
	e.ie_tipo_contratacao,
	e.ie_preco,
	e.ie_regulamentacao,
	j.ie_tipo_protocolo
from	pls_prestador			w,
	pls_protocolo_conta 		j,
	pls_conta			k,
	pls_conta_mat			g,
	pls_conta_coparticipacao	i,
	pls_plano			e,
	pls_segurado			f,
	pls_mensalidade_segurado	c,
	pls_mensalidade			b,
	pls_lote_mensalidade		a,
	ans_grupo_despesa		h
where	c.nr_sequencia	= i.nr_seq_mensalidade_seg
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	a.nr_sequencia	= b.nr_seq_lote
and	e.nr_sequencia	= f.nr_seq_plano
and	f.nr_sequencia	= c.nr_seq_segurado
and	k.nr_sequencia	= g.nr_seq_conta
and	i.nr_seq_conta_mat(+)	= g.nr_sequencia
and	h.nr_sequencia(+)	= g.nr_seq_grupo_ans
and	j.nr_seq_prestador	= w.nr_sequencia(+)
and	j.nr_sequencia	= k.nr_seq_protocolo
and	j.ie_tipo_protocolo	in ('C','I','R')
and	g.ie_status	<> 'D'
and	b.ie_cancelamento is null;
/