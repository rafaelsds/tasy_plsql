CREATE OR replace VIEW tws_wsuite_grupo_relatorio_v AS
SELECT  nr_sequencia,
	dt_atualizacao,
	cd_grupo,
	cd_exp_relatorio,
	cd_exp_descricao
FROM wsuite_grupo_relatorio;
/