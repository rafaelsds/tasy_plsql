create or replace 
view shs_boletim_internista_v as
select cd_pessoa_fisica,
	  decode(Horario_1,null,'N�o Preenchido',Horario_1)Horario_1,	  
	  decode(Horario_2,null,'N�o Preenchido',Horario_2)Horario_2,
	  decode(Horario_3,null,'N�o Preenchido',Horario_3)Horario_3,
	  decode(Andar_1,null,'N�o Preenchido',Andar_1)Andar_1,
	  decode(Andar_2,null,'N�o Preenchido',Andar_2)Andar_2,
	  decode(Andar_3,null,'N�o Preenchido',Andar_3)Andar_3,
	  decode(Solicitante_1,null,'N�o Preenchido',Solicitante_1)Solicitante_1,
	  decode(Solicitante_2,null,'N�o Preenchido',Solicitante_2)Solicitante_2,
	  decode(Solicitante_3,null,'N�o Preenchido',Solicitante_3)Solicitante_3,
	  decode(Prescricao_1,null,'N�o Preenchido',Prescricao_1)Prescricao_1,
	  decode(Prescricao_2,null,'N�o Preenchido',Prescricao_2)Prescricao_2,
	  decode(Prescricao_3,null,'N�o Preenchido',Prescricao_3)Prescricao_3,
	  decode(Solic_Exames_1,null,'N�o Preenchido',Solic_Exames_1)Solic_Exames_1,
	  decode(Solic_Exames_2,null,'N�o Preenchido',Solic_Exames_2)Solic_Exames_2,
	  decode(Solic_Exames_3,null,'N�o Preenchido',Solic_Exames_3)Solic_Exames_3,
	  decode(Inter_Eletro_1,null,'N�o Preenchido',Inter_Eletro_1)Inter_Eletro_1,
	  decode(Inter_Eletro_2,null,'N�o Preenchido',Inter_Eletro_2)Inter_Eletro_2,
	  decode(Inter_Eletro_3,null,'N�o Preenchido',Inter_Eletro_3)Inter_Eletro_3,
	  decode(Medico_1,null,'N�o Preenchido',Medico_1)Medico_1,
	  decode(Medico_2,null,'N�o Preenchido',Medico_2)Medico_2,
	  decode(Medico_3,null,'N�o Preenchido',Medico_3)Medico_3
from
(	  
select 	a.cd_pessoa_fisica,
(
	select ds_resultado
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4373
)Horario_1,
(
    select decode (spr.qt_resultado,1,'Terreo',2,'1� Andar',3,'2� Andar',4,'3� Andar',5,'4� Andar',6,'5� Andar',7,'6� Andar','Vazio')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4284
)Andar_1,
(
    select decode (spr.qt_resultado,1,'Enfermagem',2,'M�dico')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4285
)Solicitante_1,
(
    select decode (spr.qt_resultado,1,'C�pia de Prescri��o',2,'Validade',3,'Incosist�ncias',4,'Prescri��o n�o Liberada',5,'Banco de Sangue',6,'Gerar Alta')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4286
)Prescricao_1,
(
    select decode (spr.qt_resultado,1,'Laborat�rio',2,'SADT')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4287
)Solic_Exames_1,
(
    select decode (spr.qt_resultado,1,'Day-Hospital',2,'Eletiva')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4288
)Inter_Eletro_1,
(
    select decode(spr.ds_resultado,null,'N�o Preenchido',OBTER_NOME_PF(spr.ds_resultado)) nome
    from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4195	
	and spr.nr_seq_item = 4363
)Medico_1,
(
	select ds_resultado
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290	
	and spr.nr_seq_item = 4374
)Horario_2,
(
    select decode (spr.qt_resultado,1,'Terreo',2,'1� Andar',3,'2� Andar',4,'3� Andar',5,'4� Andar',6,'5� Andar',7,'6� Andar','Vazio')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290
	and spr.nr_seq_item = 4323
)Andar_2,
(
    select decode (spr.qt_resultado,1,'Enfermagem',2,'M�dico')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290	
	and spr.nr_seq_item = 4292
)Solicitante_2,
(
    select decode (spr.qt_resultado,1,'C�pia de Prescri��o',2,'Validade',3,'Incosist�ncias',4,'Prescri��o n�o Liberada',5,'Banco de Sangue',6,'Gerar Alta')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290	
	and spr.nr_seq_item = 4293
)Prescricao_2,
(
    select decode (spr.qt_resultado,1,'Laborat�rio',2,'SADT')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290	
	and spr.nr_seq_item = 4294
)Solic_Exames_2,
(
    select decode (spr.qt_resultado,1,'Day-Hospital',2,'Eletiva')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290	
	and spr.nr_seq_item = 4295
)Inter_Eletro_2,
(
    select decode(spr.ds_resultado,null,'N�o Preenchido',OBTER_NOME_PF(spr.ds_resultado)) nome
    from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4290
	and spr.nr_seq_item = 4364
)Medico_2,
(
	select ds_resultado
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297
	and spr.nr_seq_item = 4375
)Horario_3,
(
    select decode (spr.qt_resultado,1,'Terreo',2,'1� Andar',3,'2� Andar',4,'3� Andar',5,'4� Andar',6,'5� Andar',7,'6� Andar','Vazio')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297	
	and spr.nr_seq_item = 4325
)Andar_3,
(
    select decode (spr.qt_resultado,1,'Enfermagem',2,'M�dico')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297	
	and spr.nr_seq_item = 4298
)Solicitante_3,
(
    select decode (spr.qt_resultado,1,'C�pia de Prescri��o',2,'Validade',3,'Incosist�ncias',4,'Prescri��o n�o Liberada',5,'Banco de Sangue',6,'Gerar Alta')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297	
	and spr.nr_seq_item = 4299
)Prescricao_3,
(
    select decode (spr.qt_resultado,1,'Laborat�rio',2,'SADT')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297	
	and spr.nr_seq_item = 4300
)Solic_Exames_3,
(
    select decode (spr.qt_resultado,1,'Day-Hospital',2,'Eletiva')
	from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297	
	and spr.nr_seq_item = 4301
)Inter_Eletro_3,
(
    select decode(spr.ds_resultado,null,'N�o Preenchido',OBTER_NOME_PF(spr.ds_resultado)) nome
    from sac_pesquisa sp		
	inner join med_tipo_avaliacao mta
		  on (sp.nr_seq_tipo_avaliacao = mta.nr_sequencia )
		  inner join sac_pesquisa_result spr
		  		inner join med_item_avaliar mia
				on (spr.nr_seq_item = mia.nr_sequencia )
		  on (sp.nr_sequencia = spr.nr_seq_pesquisa )
	where a.nr_sequencia = sp.nr_sequencia
	and	mia.nr_seq_superior = 4297	
	and spr.nr_seq_item = 4365
)Medico_3
from sac_pesquisa a
)
Boletim;
/