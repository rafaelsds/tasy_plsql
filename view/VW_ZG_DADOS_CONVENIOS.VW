CREATE OR REPLACE FORCE VIEW vw_zg_dados_convenios
(
        versao,
        registro_ans,
        codigo_convenio,
        nome_convenio,
        cd_estabelecimento,
        cnpj_convenio,
        ativo
)
    AS
        SELECT
            '2.0.0'                                     AS versao,
            convenio_estabelecimento.cd_ans             AS registro_ans,
            convenio.cd_convenio                        AS codigo_convenio,
            convenio.ds_convenio                        AS nome_convenio,
            convenio_estabelecimento.cd_estabelecimento AS cd_estabelecimento,
            convenio.cd_cgc                             AS cnpj_convenio,
            CASE WHEN convenio.ie_situacao = 'A'
                THEN 1
            ELSE 0
            END                                         AS ativo
        FROM
            tasy.convenio
            INNER JOIN tasy.convenio_estabelecimento
                ON convenio.cd_convenio = convenio_estabelecimento.cd_convenio;
/