create or replace view data_ano_v as
select trunc(sysdate,'year') dt_ano from dual
union all
select add_months(trunc(sysdate,'year'),12) from dual
union all
select add_months(trunc(sysdate,'year'),-12) from dual
union all
select add_months(trunc(sysdate,'year'),24) from dual
union all
select add_months(trunc(sysdate,'year'),-24) from dual
union all
select add_months(trunc(sysdate,'year'),36) from dual
union all
select add_months(trunc(sysdate,'year'),-36) from dual
union all
select add_months(trunc(sysdate,'year'),48) from dual
union all
select add_months(trunc(sysdate,'year'),-48) from dual
union all
select add_months(trunc(sysdate,'year'),60) from dual
union all
select add_months(trunc(sysdate,'year'),-60) from dual
union all
select add_months(trunc(sysdate,'year'),72) from dual
union all
select add_months(trunc(sysdate,'year'),-72) from dual
union all
select add_months(trunc(sysdate,'year'),84) from dual
union all
select add_months(trunc(sysdate,'year'),-84) from dual
union all
select add_months(trunc(sysdate,'year'),96) from dual
union all
select add_months(trunc(sysdate,'year'),-96) from dual
union all
select add_months(trunc(sysdate,'year'),108) from dual
union all
select add_months(trunc(sysdate,'year'),-108) from dual
union all
select add_months(trunc(sysdate,'year'),120) from dual
union all
select add_months(trunc(sysdate,'year'),-120) from dual;
/