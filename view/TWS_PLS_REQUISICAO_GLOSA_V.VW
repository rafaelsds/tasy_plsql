create or replace view tws_pls_requisicao_glosa_v as 
select	nr_sequencia,
	dt_atualizacao,
	nr_seq_execucao,
	nr_seq_motivo_glosa,
	nr_seq_ocorrencia,
	nr_seq_req_mat,
	nr_seq_req_proc,
	nr_seq_requisicao,
	ds_observacao 
from 	pls_requisicao_glosa;
/