CREATE OR REPLACE VIEW ORDEM_SERV_CLI_ERRO_V ( QT_ORDENS_ABERTAS, 
QT_ORDENS_PROCESSO, QT_ORDENS_CLIENTE, QT_ORDENS_ENCERRADAS, DT_MES, 
DT_MES_ORDER, NR_SEQ_PROJ ) AS SELECT	SUM(qt_ordens_abertas) + SUM(qt_ordens_cliente) + SUM(qt_ordens_encerradas) qt_ordens_abertas,
	SUM(qt_ordens_abertas) qt_ordens_processo,
	SUM(qt_ordens_cliente) qt_ordens_cliente,
	SUM(qt_ordens_encerradas) qt_ordens_encerradas,
	NVL(TO_CHAR(dt_mes,'dd/mm/yyyy'),'Demais meses') dt_mes,
	dt_mes dt_mes_order,  nr_seq_proj
FROM(	SELECT	0 qt_ordens_abertas,
		0 qt_ordens_cliente,
		SUM(qt_ordens) qt_ordens_encerradas,
		NULL dt_mes,
		nr_seq_proj
	FROM	(SELECT	COUNT(*) qt_ordens,
			TRUNC(a.dt_fim_real,'month') dt_fim_real,
			d.nr_seq_proj
		FROM	man_ordem_servico_v a,
			proj_ordem_servico d
		WHERE	a.nr_Sequencia = d.nr_seq_ordem
		AND	a.ie_status_ordem = 3
		AND	a.ie_classificacao = 'E'
		GROUP BY	d.nr_seq_proj, TRUNC(a.dt_fim_real,'month')) x
	WHERE	TRUNC(x.dt_fim_real) < ADD_MONTHS(TRUNC(SYSDATE,'month'),-5)
	GROUP BY nr_seq_proj
	UNION ALL
	SELECT	0 qt_ordens_abertas,
		SUM(qt_ordens) qt_ordens_cliente,
		0 qt_ordens_encerradas,
		NULL dt_mes, nr_seq_proj
	FROM	(SELECT	COUNT(*) qt_ordens,
			TRUNC(a.dt_inicio_real,'month') dt_inicio_real,
			d.nr_seq_proj
		FROM	man_ordem_servico_v a,
			proj_ordem_servico d
		WHERE	a.nr_Sequencia = d.nr_seq_ordem
		AND	a.ie_status_ordem = 2
		AND	a.ie_classificacao = 'E'
		AND	a.nr_seq_estagio IN
			(SELECT   nr_seq_proj
			FROM     man_estagio_processo
			WHERE    ie_acao = 2)
		GROUP BY	d.nr_seq_proj, TRUNC(a.dt_inicio_real,'month')) x
	WHERE	TRUNC(x.dt_inicio_real) < ADD_MONTHS(TRUNC(SYSDATE,'month'),-5)
	GROUP BY nr_seq_proj
	UNION ALL
	SELECT	SUM(qt_ordens) qt_ordens_abertas,
		0 qt_ordens_cliente,
		0 qt_ordens_encerradas,
		NULL dt_mes,
		nr_seq_proj
	FROM	(SELECT	COUNT(*) qt_ordens,
			TRUNC(a.dt_inicio_real,'month') dt_inicio_real,
			d.nr_seq_proj
		FROM	man_ordem_servico_v a,
			proj_ordem_servico d
		WHERE	a.nr_Sequencia = d.nr_seq_ordem
		AND	a.ie_status_ordem = 2
		AND	a.ie_classificacao = 'E'
		AND	a.nr_seq_estagio IN
			(SELECT   nr_seq_proj
			FROM     man_estagio_processo
			WHERE    ie_acao = 3)
		GROUP BY	d.nr_seq_proj, TRUNC(a.dt_inicio_real,'month')) x
	WHERE	TRUNC(x.dt_inicio_real) < ADD_MONTHS(TRUNC(SYSDATE,'month'),-5)
	GROUP BY nr_seq_proj
	UNION ALL
	SELECT	0 qt_ordens_abertas,
		0 qt_ordens_cliente,
		SUM(qt_ordens) qt_ordens_encerradas,
		TRUNC(x.dt_fim_real,'month'),
		nr_seq_proj
	FROM	(SELECT	COUNT(*) qt_ordens,
			TRUNC(a.dt_fim_real,'month') dt_fim_real,d.nr_seq_proj
		FROM	man_ordem_servico_v a,
			proj_ordem_servico d
		WHERE	a.nr_Sequencia = d.nr_seq_ordem
		AND	a.ie_status_ordem = 3
		AND	a.ie_classificacao = 'E'
		GROUP BY	TRUNC(a.dt_fim_real,'month'),d.nr_seq_proj) x
	WHERE	TRUNC(x.dt_fim_real) BETWEEN ADD_MONTHS(TRUNC(SYSDATE,'month'),-5) AND TRUNC(SYSDATE,'month')
	GROUP BY	TRUNC(x.dt_fim_real,'month'), nr_seq_proj
	UNION ALL
	SELECT	0 qt_ordens_abertas,
		SUM(qt_ordens) qt_ordens_cliente,
		0 qt_ordens_encerradas,
		TRUNC(x.dt_inicio_real,'month'),
		nr_seq_proj
	FROM	(SELECT	COUNT(*) qt_ordens,
			TRUNC(a.dt_inicio_real,'month') dt_inicio_real,d.nr_seq_proj
		FROM	man_ordem_servico_v a,
			proj_ordem_servico d
		WHERE	a.nr_Sequencia = d.nr_seq_ordem
		AND	a.ie_status_ordem = 2
		AND	a.ie_classificacao = 'E'
		AND	a.nr_seq_estagio IN (SELECT   nr_seq_proj
			FROM     man_estagio_processo
			WHERE    ie_acao = 2)
		GROUP BY	d.nr_seq_proj, TRUNC(a.dt_inicio_real,'month')) x
	WHERE	TRUNC(x.dt_inicio_real) BETWEEN ADD_MONTHS(TRUNC(SYSDATE,'month'),-5) AND TRUNC(SYSDATE,'month')
	GROUP BY	TRUNC(x.dt_inicio_real,'month'), nr_seq_proj
	UNION ALL
	SELECT	SUM(qt_ordens) qt_ordens_abertas,
		0 qt_ordens_cliente,
		0 qt_ordens_encerradas,
		TRUNC(x.dt_inicio_real,'month'),
		nr_seq_proj
	FROM	(SELECT	COUNT(*) qt_ordens,
			TRUNC(a.dt_inicio_real,'month') dt_inicio_real,d.nr_seq_proj
		FROM	man_ordem_servico_v a,
			proj_ordem_servico d
		WHERE	a.nr_sequencia = d.nr_seq_ordem
		AND	a.ie_status_ordem = 2
		AND	a.ie_classificacao = 'E'
		AND	a.nr_seq_estagio IN
			(SELECT   nr_seq_proj
			FROM     man_estagio_processo
			WHERE    ie_acao = 3)
		GROUP BY	d.nr_seq_proj, TRUNC(a.dt_inicio_real,'month')) x
	WHERE	TRUNC(x.dt_inicio_real) BETWEEN ADD_MONTHS(TRUNC(SYSDATE,'month'),-5) AND TRUNC(SYSDATE,'month')
	GROUP BY	TRUNC(x.dt_inicio_real,'month'), nr_seq_proj)
GROUP BY	dt_mes, nr_seq_proj;
/
