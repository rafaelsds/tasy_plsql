create or replace 
view processo_lock_v3 as
SELECT SUBSTR(DECODE(l.request, 0, 'Lock', 'Wait'),1,4) ds_status,
	   s.sid,
	   s.serial#,
	   SUBSTR(s.program,1,15) programa,
	   SUBSTR(s.machine,1,16) machine,
	   SUBSTR(s.MODULE,1,15) modulo,
	   SUBSTR(u.object_name,1,25) nm_objeto,
	   SUBSTR(DECODE(l.lmode, 0, 'None',
                             1, 'Null (NULL)',
                             2, 'Row-S (SS)',
                             3, 'Row-X (SX)',
                             4, 'Share (S)',
                             5, 'S/Row-X (SSX)',
                             6, 'Exclusive (X)',
                             l.lmode),1,13) locked_mode,
	s.status,
	substr(s.osuser,1,15) usuario,
	substr(obter_pf_usuario(s.osuser,'N'),1,30) nome
FROM 	GV$LOCK L, 
	GV$SESSION S,
	user_objects u
WHERE	s.sid = l.sid
AND		u.object_id = l.ID1
ORDER BY 1;
/
