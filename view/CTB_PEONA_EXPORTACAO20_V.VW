create or replace 
view ctb_peona_exportacao20_v
as
select	cd_unimed,
	ie_rede,
	dt_ocorrencia,
	dt_aviso,
	vl_avisado,
	vl_coparticipacao,
	vl_liquido,
	vl_clpp,
	dt_referencia
from	(select	substr(lpad(somente_numero(e.cd_cooperativa),3,'0'),1,3) cd_unimed,
		'P' ie_rede,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_ocorrencia,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_aviso,
		elimina_caractere_especial(sum(nvl(a.vl_total,0) + nvl(a.vl_glosa,0))) vl_avisado,
		lpad(0,16,0) vl_coparticipacao,
		elimina_caractere_especial(sum(nvl(a.vl_total,0) + nvl(a.vl_glosa,0))) vl_liquido,
		lpad(0,16,0) vl_clpp,
		trunc(b.dt_mes_competencia,'mm') dt_referencia,
		pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) ie_exclui
	from	pls_plano		c,
		pls_protocolo_conta	b,
		pls_conta		a,
		pls_congenere 		e
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_seq_plano		= c.nr_sequencia
	and	e.nr_sequencia(+) 	= b.nr_seq_congenere
	and	c.ie_preco		= '1'
	and	a.ie_tipo_segurado	= 'B'
	and	b.ie_tipo_protocolo	= 'C'
	--and	pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) = 'N'
	group by e.cd_cooperativa ,
		b.dt_mes_competencia,
		a.cd_estabelecimento,
		a.nr_seq_segurado)
where	ie_exclui	= 'N'
union
select	cd_unimed,
	ie_rede,
	dt_ocorrencia,
	dt_aviso,
	vl_avisado,
	vl_coparticipacao,
	vl_liquido,
	vl_clpp,
	dt_referencia
from	(select	substr(lpad(somente_numero(e.cd_cooperativa),3,'0'),1,3) cd_unimed,
		'P' ie_rede,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_ocorrencia,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_aviso,
		elimina_caractere_especial(sum(nvl(a.vl_total,0) + nvl(a.vl_coparticipacao,0))) vl_avisado,
		lpad(0,16,0) vl_coparticipacao,
		elimina_caractere_especial(sum(nvl(a.vl_total,0) + nvl(a.vl_coparticipacao,0))) vl_liquido,
		lpad(0,16,0) vl_clpp,
		trunc(b.dt_mes_competencia,'mm') dt_referencia,
		pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) ie_exclui
	from	pls_plano		c,
		pls_protocolo_conta	b,
		pls_conta		a,
		pls_congenere		e
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_seq_plano		= c.nr_sequencia
	and	e.nr_sequencia(+) 	= b.nr_seq_congenere
	and	c.ie_preco		= '1'
	and	a.ie_tipo_segurado	= 'B'
	and	b.ie_tipo_protocolo	= 'R'
	--and	pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) = 'N'
	group by e.cd_cooperativa,
		b.dt_mes_competencia,
		a.cd_estabelecimento,
		a.nr_seq_segurado)
where	ie_exclui	= 'N'
union
select	cd_unimed,
	ie_rede,
	dt_ocorrencia,
	dt_aviso,
	vl_avisado,
	vl_coparticipacao,
	vl_liquido,
	vl_clpp,
	dt_referencia
from	(select	substr(lpad(somente_numero(e.cd_cooperativa),3,'0'),1,3) cd_unimed,
		'P' ie_rede,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_ocorrencia,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_aviso,
		elimina_caractere_especial(sum(nvl(a.vl_total,0))) vl_avisado,
		lpad(0,16,0) vl_coparticipacao,
		elimina_caractere_especial(sum(nvl(a.vl_total,0))) vl_liquido,
		lpad(0,16,0) vl_clpp,
		trunc(b.dt_mes_competencia,'mm') dt_referencia,
		pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) ie_exclui
	from	pls_plano		c,
		pls_protocolo_conta	b,
		pls_conta		a,
		pls_congenere		e
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_seq_plano		= c.nr_sequencia
	and	e.nr_sequencia(+) 	= b.nr_seq_congenere
	and	c.ie_preco		= '1'
	and	a.ie_tipo_segurado	= 'B'
	and	b.ie_tipo_protocolo	= 'R'
	--and	pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) = 'N'
	group by e.cd_cooperativa ,
		b.dt_mes_competencia,
		a.cd_estabelecimento,
		a.nr_seq_segurado)
where	ie_exclui	= 'N'
union all
select	cd_unimed,
	ie_rede,
	dt_ocorrencia,
	dt_aviso,
	vl_avisado,
	vl_coparticipacao,
	vl_liquido,
	vl_clpp,
	dt_referencia
from	(select	substr(lpad(somente_numero(e.cd_cooperativa),3,'0'),1,3) cd_unimed,
		'R' ie_rede,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_ocorrencia,
		to_char(b.dt_mes_competencia,'mm/yyyy') dt_aviso,
		elimina_caractere_especial(sum(nvl(a.vl_glosa,0))) vl_avisado,
		lpad(0,16,0) vl_coparticipacao,
		elimina_caractere_especial(sum(nvl(a.vl_glosa,0))) vl_liquido,
		lpad(0,16,0) vl_clpp,
		trunc(b.dt_mes_competencia,'mm') dt_referencia,
		pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) ie_exclui
	from	pls_plano		c,
		pls_protocolo_conta	b,
		pls_conta		a,
		pls_congenere 		e
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_seq_plano		= c.nr_sequencia
	and	e.nr_sequencia(+) 	= b.nr_seq_congenere
	and	c.ie_preco		= '1'
	and	a.ie_tipo_segurado	= 'B'
	and	b.ie_tipo_protocolo	= 'R'
	--and	pls_obter_se_exclui_seg_peona(a.nr_seq_segurado, b.dt_mes_competencia, a.cd_estabelecimento) = 'N'
	group by e.cd_cooperativa ,
		b.ie_tipo_protocolo,
		b.dt_mes_competencia,
		a.cd_estabelecimento,
		a.nr_seq_segurado)
where	ie_exclui	= 'N'
union all
select	null cd_unimed,
	null ie_rede,
	to_char(c.dt_mesano_referencia,'mm/yyyy')dt_ocorrencia,
	to_char(c.dt_mesano_referencia,'mm/yyyy')dt_aviso,
	elimina_caractere_especial(sum(nvl(c.vl_coparticipacao,0))) vl_avisado,
	lpad(0,16,0) vl_coparticipacao,
	elimina_caractere_especial(sum(nvl(c.vl_coparticipacao,0))) vl_liquido,
	elimina_caractere_especial(sum(pls_obter_receita_peona(c.dt_mesano_referencia))) vl_clpp,
	trunc(c.dt_mesano_referencia,'mm') dt_referencia
from	pls_mensalidade_seg_item 	d,
	pls_mensalidade_segurado	c,
	pls_segurado			e,
	pls_plano 			f,
	pls_mensalidade			b,
	pls_lote_mensalidade		a
where	c.nr_sequencia		= d.nr_seq_mensalidade_seg
and	c.nr_seq_segurado	= e.nr_sequencia
and	e.nr_seq_plano		= f.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_mensalidade
and	a.nr_sequencia		= b.nr_seq_lote
and	d.ie_tipo_item in ('1','4','5','12')
and	f.ie_preco		= '1'
and	e.ie_tipo_segurado	= 'B'
and	a.dt_contabilizacao is null
group by c.dt_mesano_referencia
union all
select	null cd_unimed,
	null ie_rede,
	to_char(c.dt_mesano_referencia,'mm/yyyy') dt_ocorrencia,
	to_char(c.dt_mesano_referencia,'mm/yyyy') dt_aviso,
	elimina_caractere_especial(sum(pls_obter_receita_peona(c.dt_mesano_referencia))) vl_avisado,
	lpad(0,16,0) vl_coparticipacao,
	elimina_caractere_especial(sum(pls_obter_receita_peona(c.dt_mesano_referencia))) vl_liquido,
	elimina_caractere_especial(sum(pls_obter_receita_peona(c.dt_mesano_referencia))) vl_clpp,
	trunc(c.dt_mesano_referencia,'mm') dt_referencia
from	pls_mensalidade_seg_item 	d,
	pls_mensalidade_segurado	c,
	pls_segurado			e,
	pls_plano 			f,
	pls_mensalidade			b,
	pls_lote_mensalidade		a
where	c.nr_sequencia		= d.nr_seq_mensalidade_seg
and	c.nr_seq_segurado	= e.nr_sequencia
and	e.nr_seq_plano		= f.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_mensalidade
and	a.nr_sequencia		= b.nr_seq_lote
and	d.ie_tipo_item in ('1','4','5','12')
and	f.ie_preco		= '1'
and	e.ie_tipo_segurado	= 'B'
and	a.dt_contabilizacao is null
group  by c.dt_mesano_referencia
union all
select	null cd_unimed,
	'S' ie_rede,
	to_char(a.dt_competencia,'mm/yyyy') dt_ocorrencia,
	to_char(a.dt_competencia,'mm/yyyy') dt_aviso,
	elimina_caractere_especial(sum(nvl(b.vl_ressarcimento_sus,0))) vl_avisado,
	lpad(0,16,0) vl_coparticipacao,
	elimina_caractere_especial(sum(nvl(b.vl_ressarcimento_sus,0))) vl_liquido,
	lpad(0,16,0) vl_clpp,
	trunc(a.dt_competencia,'mm') dt_referencia
from	pls_peona		a,
	pls_valores_peona	b
where	a.nr_sequencia	= b.nr_seq_peona
and	nvl(b.vl_ressarcimento_sus,0) <> 0
group by
	a.dt_competencia;
/