CREATE OR REPLACE VIEW SBJSC_exportacao_repasse_v
AS
select	distinct
	SUBSTR(obter_dados_pf(c.cd_pessoa_fisica, 'CPF'),1,14) nr_cpf,
	SUBSTR(obter_nome_terceiro(a.nr_seq_terceiro),1,35) ds_terceiro,	
	SUBSTR(obter_compl_pf(c.cd_pessoa_fisica,1,'EN'),1,35) ds_endereco,
	SUBSTR(obter_compl_pf(c.cd_pessoa_fisica,1,'CI'),1,20) ds_cidade,
	SUBSTR(obter_compl_pf(c.cd_pessoa_fisica,1,'CEP'),1,9) ds_cep,
	SUBSTR(obter_compl_pf(c.cd_pessoa_fisica,1,'UF'),1,2) ds_uf,
	SUBSTR(obter_dados_pf(c.cd_pessoa_fisica,'QD'),1,2) qt_dependente,
	'0588' cd_darf,
	b.nr_titulo,
	b.dt_emissao,
	to_number(nvl(obter_dados_tit_pagar(b.nr_titulo,'P') + obter_dados_tit_pagar(b.nr_titulo,'D') + obter_dados_tit_pagar(b.nr_titulo,'DV') + obter_dados_tit_pagar(b.nr_titulo,'OD') - obter_dados_tit_pagar(b.nr_titulo,'J') - obter_dados_tit_pagar(b.nr_titulo,'M') - obter_dados_tit_pagar(b.nr_titulo,'OA') + obter_dados_tit_pagar(b.nr_titulo,'T'),0)) vl_titulo,
	(	select	sum(NVL(x.vl_desc_base,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 17
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_desc_base,
	(	select	sum(NVL(x.vl_reducao,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 17
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_reducao,
	(	select	sum(NVL(x.vl_base_calculo,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 17
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_base_calculo,
	(	select	sum(NVL(x.pr_tributo,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 17
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) pr_tributo,
	(	select	sum(NVL(x.vl_imposto,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 17
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_imposto,
	(	select	max(x.dt_imposto)
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 17
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) dt_imposto,
	a.nr_repasse_terceiro,
	(	select	(sum(NVL(x.vl_imposto,0)) - (sum(NVL(x.vl_imposto,0)) * 0.64516) - (sum(NVL(x.vl_imposto,0)) * 0.13978))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 12
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_csll,		
	(	select	(sum(NVL(x.vl_imposto,0)) * 0.64516)
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 12
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_cofins,
	(	select	(sum(NVL(x.vl_imposto,0)) * 0.13978)
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 12
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_pis,
	(	select	sum(NVL(x.vl_imposto,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 20
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_inss,
	(	select	sum(NVL(x.vl_imposto,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 10
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_iss,
	b.dt_liquidacao,
	a.dt_mesano_referencia		
from 	titulo_pagar b,
	terceiro c,
	repasse_terceiro a,
	repasse_terceiro_venc d
where 	a.nr_seq_terceiro	= c.nr_sequencia
and	d.nr_repasse_terceiro	= a.nr_repasse_terceiro
and	b.nr_titulo		= d.nr_titulo
and	c.cd_pessoa_fisica 	is not null
and	b.nr_titulo_original 	is null
union
select	distinct 
	c.cd_cgc nr_cpf,
	SUBSTR(obter_nome_terceiro(a.nr_seq_terceiro),1,254) ds_terceiro,	
	SUBSTR(obter_compl_pj(c.cd_cgc,1,'E'),1,35) ds_endereco,
	SUBSTR(obter_compl_pj(c.cd_cgc,1,'CI'),1,20) ds_cidade,
	SUBSTR(obter_compl_pj(c.cd_cgc,1,'CEP'),1,9) ds_cep,
	SUBSTR(obter_compl_pj(c.cd_cgc,1,'UF'),1,2) ds_uf,
	'0' qt_dependente,
	'1708' cd_darf,
	b.nr_titulo,
	b.dt_emissao,
	to_number(nvl(obter_dados_tit_pagar(b.nr_titulo,'P') + obter_dados_tit_pagar(b.nr_titulo,'D') + obter_dados_tit_pagar(b.nr_titulo,'DV') + obter_dados_tit_pagar(b.nr_titulo,'OD') - obter_dados_tit_pagar(b.nr_titulo,'J') - obter_dados_tit_pagar(b.nr_titulo,'M') - obter_dados_tit_pagar(b.nr_titulo,'OA') + obter_dados_tit_pagar(b.nr_titulo,'T'),0)) vl_titulo,
	(	select	sum(NVL(x.vl_desc_base,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 11
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_desc_base,
	(	select	sum(NVL(x.vl_reducao,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 11
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_reducao,
	(	select	sum(NVL(x.vl_base_calculo,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 11
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_base_calculo,
	(	select	sum(NVL(x.pr_tributo,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 11
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) pr_tributo,
	(	select	sum(NVL(x.vl_imposto,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 11
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_imposto,
	(	select	max(x.dt_imposto)
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 11
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) dt_imposto,
	a.nr_repasse_terceiro,
	(	select	(sum(NVL(x.vl_imposto,0)) - (sum(NVL(x.vl_imposto,0)) * 0.64516) - (sum(NVL(x.vl_imposto,0)) * 0.13978))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 12
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_csll,
	(	select	(sum(NVL(x.vl_imposto,0)) * 0.64516)
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 12
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_cofins,
	(	select	(sum(NVL(x.vl_imposto,0)) * 0.13978)
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 12
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_pis,
	(	select	sum(NVL(x.vl_imposto,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 7
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_inss,
	(	select	sum(NVL(x.vl_imposto,0))
		from	repasse_terc_venc_trib x,
			repasse_terceiro_venc z
		where	x.nr_seq_rep_venc	= z.nr_sequencia
		and	x.cd_tributo   		= 16
		and	z.nr_repasse_terceiro	= a.nr_repasse_terceiro) vl_iss,
	b.dt_liquidacao,
	a.dt_mesano_referencia
from 	titulo_pagar b,
	terceiro c,
	repasse_terceiro a,
	repasse_terceiro_venc d
where 	a.nr_seq_terceiro	= c.nr_sequencia
and	d.nr_repasse_terceiro	= a.nr_repasse_terceiro
and	b.nr_titulo		= d.nr_titulo
and	c.cd_cgc 		is not null
and	b.nr_titulo_original 	is null;
/
