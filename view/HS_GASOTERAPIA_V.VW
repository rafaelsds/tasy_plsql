create or replace view HS_Gasoterapia_v as
select	substr(obter_descricao_padrao('GAS', 'DS_GAS', nr_seq_gas),1,254) ds_tipo_gas,
	substr(obter_valor_dominio(1299, ie_respiracao),1,200) ds_respiracao,
	substr(obter_valor_dominio(1612,a.IE_DISP_RESP_ESP),1,255) ds_dispositivo,
	substr(obter_descricao_padrao('MODALIDADE_VENTILATORIA', 'DS_MODALIDADE', cd_modalidade_vent),1,155) DS_MODALIDADE_VENT,
	b.cd_prescritor,
	b.nr_prescricao,
	a.nr_sequencia,
	substr(obter_valor_dominio(2702,c.IE_EVENTO),1,255) ds_evento,
	substr(obter_nome_pf(obter_dados_usuario_opcao(c.nm_usuario,'C')),1,80) nm_usuario_evento,
	c.dt_evento,
	c.QT_GASOTERAPIA QT_GASOTERAPIA_evento,
	substr(obter_valor_dominio(1580, c.IE_UNIDADE_MEDIDA),1,80) DS_UNIDADE_MEDIDA_evento,
	a.QT_GASOTERAPIA QT_GASOTERAPIA,
	substr(obter_valor_dominio(1580, a.IE_UNIDADE_MEDIDA),1,80) DS_UNIDADE_MEDIDA
from	prescr_gasoterapia a,
	prescr_gasoterapia_evento c,
	prescr_medica b
where	a.nr_prescricao		= b.nr_prescricao
and	c.nr_seq_gasoterapia	= a.nr_sequencia
and	nvl(c.ie_evento_valido,'S')	= 'S';
/
