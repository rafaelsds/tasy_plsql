create or replace 
view scola_nascimento_v as
select nr_sequencia,
	   nr_atendimento, 
	   dt_nascimento,
	   ie_sexo,
	   cd_pessoa_rn,
	   cd_pessoa_resp,
	   cd_pai_rn	   
from   nascimento
/