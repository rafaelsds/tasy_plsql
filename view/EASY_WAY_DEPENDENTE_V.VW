CREATE OR REPLACE
VIEW Easy_Way_dependente_v AS 
SELECT 	LPAD(NVL(a.NR_CPF,' '),14,' ')  				nr_cpf,
	LPAD(NVL(cd_pessoa_dependente,' '),10,' ') 			cd_dependente ,
	RPAD(NVL(a.nm_dependente,' '),60,' ') 				nm_dependente,
	'1' 							ie_dep,
	LPAD(NVL(to_char(a.dt_nascimento, 'ddmmyyyy'),' '),8,' ') 		dt_vini,
	LPAD(NVL(to_char(a.dt_fim_vigencia, 'ddmmyyyy'),' '),8,' ')  		dt_vfi,
	LPAD(NVL(to_char(a.dt_atualizacao_nrec, 'ddmmyyyy'),' '),8,' ')  	dt_atualizacao,
	LPAD(NVL(to_char(a.dt_atualizacao, 'ddmmyyyy'),' '),8,' ')  		dt_uatualizacao,
	LPAD(NVL(to_char(a.dt_nascimento, 'ddmmyyyy'),' '),8,' ')  		dt_nasc_depen
	FROM 	pessoa_fisica_dependente a,
		pessoa_fisica b
	WHERE 	a.cd_pessoa_fisica = b.cd_pessoa_fisica;
/