create or replace view plussoft_abrangencia_geo_v
as
	select 	vl_dominio	cd_abrangencia_geografica,
	       	ds_valor_dominio	ds_abrangencia_geografica
	from   	valor_dominio
	where  	cd_dominio = 1667;
/
