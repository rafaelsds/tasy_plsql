CREATE OR REPLACE VIEW biweekly_verification_v as
select	b.seq,
	a.cd_prs_id,
	a.tc_id,
	a.ds_descricao,
	a.ds_funcao,
	a.nr_seq_ciclo,
	a.ds_tc_passo,
	a.ds_result_esperado,
	a.dt_execucao,
	a.ds_result_atual,
	a.result_passo,
	a.nm_executor,
	a.nr_seq_service_order,
	a.ie_status_ordem,
	a.medical_device,
	obter_desc_estagio_proc(man_obter_estagio_ordem(a.nr_seq_service_order)) ds_estagio,
	obter_nome_gerencia(obter_gerencia_resp_os(a.nr_seq_service_order)) ds_gerencia_os,
	obter_nome_gerencia(obter_gerencia_funcao_2(a.cd_funcao)) ds_gerencia_funcao,
	a.ie_matriz_risco,
	a.ie_tipo_execucao,
	a.ds_tipo_mudanca,
	c.ds_descricao_os,
	c.ie_classificacao,
	c.ds_severidade,
	c.ds_justificativa_vv,
	c.ds_privacy_risk,
	c.potencial_security,
	c.ie_potencial_privacy,
	a.nr_seq_intencao_uso,
	a.ds_version,
	coalesce(a.ie_clinico, b.ie_clinico, 'N') ie_clinico
from	(select	f.cd_funcao,
		p.cd_prs_id,
		'TASY_TC_' || x.nr_seq_tc tc_id,
		t.ds_descricao,
		obter_desc_expressao_idioma(f.cd_exp_funcao, f.ds_funcao, 5) ds_funcao,
		y.nr_seq_ciclo,
		y.ds_item_description ds_tc_passo,
		y.ds_result ds_result_esperado,
		trunc(y.dt_execution) dt_execucao,
		y.ds_exec_result ds_result_atual,
		decode(y.ie_result,'P','Passed','B','Blocked','F','Failed') result_passo,
		obter_nome_usuario(y.nm_user_execution) nm_executor,
		z.nr_seq_service_order,
		decode(	(select r.ie_status_ordem
			from man_ordem_servico r
			where r.nr_sequencia = z.nr_seq_service_order),1,'Open',2,'In progress',3,'Closed') ie_status_ordem,
		nvl(c.ie_clinico, 'N') medical_device,
		x.nr_seq_tc ||'-'|| x.cd_funcao || decode(t.ie_obrigatorio,'S','-'||t.ie_obrigatorio,'-N') ds_key,
        decode((select max(rt.nr_seq_type)
           from reg_product_req_type rt
          where rt.nr_seq_type = 12
            and rt.nr_seq_product_req = p.nr_sequencia),12,'S','N') ie_matriz_risco,
		nvl(reg_test_plan_pck.get_change_type_desc(x.ie_tipo_mudanca),'Automacao') ds_tipo_mudanca,
		decode(t.ie_tipo_execucao,'M','Manual','A','Automatizado','') ie_tipo_execucao,
		area.nr_seq_intencao_uso nr_seq_intencao_uso,
		x.ds_version ds_version,
		p.ie_clinico ie_clinico
	from	reg_tc_pendencies x,
		reg_tc_evidence_item y,
		reg_tc_so_pendencies z,
		funcao f,
		reg_caso_teste t,
		reg_product_requirement p,
		reg_customer_requirement c,
		reg_features_customer fea,
		reg_area_customer area
	where	y.nr_seq_ect = x.nr_sequencia
	and	f.cd_funcao (+) = x.cd_funcao
	and	y.nr_sequencia = z.nr_seq_ev_item (+)
	and	t.nr_sequencia = x.nr_seq_tc
	and	t.nr_seq_product = p.nr_sequencia
	and	c.nr_sequencia = p.nr_customer_requirement
	and	c.nr_seq_features = fea.nr_sequencia
	and 	fea.nr_seq_area_customer = area.nr_sequencia
	and	x.ie_status <> 'E'
	) a,
	(select	rownum seq,
		a.ds_key,
		a.nr_seq_intencao_uso,
		a.ds_version,
		a.ie_clinico
	from	(select distinct x.nr_seq_tc ||'-'|| x.cd_funcao || decode(t.ie_obrigatorio,'S','-'||t.ie_obrigatorio,'-N') ds_key,
			area.nr_seq_intencao_uso nr_seq_intencao_uso,
			x.ds_version ds_version,
			p.ie_clinico ie_clinico
		from	reg_tc_pendencies x,
			reg_tc_evidence_item y,
			reg_tc_so_pendencies z,
			reg_caso_teste t,
			reg_product_requirement p,
			reg_customer_requirement urs,
			reg_features_customer fea,
			reg_area_customer area
		where	y.nr_seq_ect = x.nr_sequencia
		and	y.nr_sequencia = z.nr_seq_ev_item (+)
		and	t.nr_sequencia = x.nr_seq_tc
		and	t.nr_seq_product = p.nr_sequencia
		and	p.nr_customer_requirement = urs.nr_sequencia
		and	urs.nr_seq_features = fea.nr_sequencia
		and	fea.nr_seq_area_customer = area.nr_sequencia
		and	x.ie_status <> 'E') a
	) b,
	(select	mos.nr_sequencia nr_seq_os,
                mos.ds_dano_breve ds_descricao_os,
                obter_valor_dominio(1149, mos.ie_classificacao) ie_classificacao,
                (select	substr(obter_desc_expressao(ms.cd_exp_severidade, ms.ds_severidade), 1, 255) ds_severidade
                from	man_severidade ms
                where 	ms.nr_sequencia = mos.nr_seq_severidade) ds_severidade,
                obter_nome_gerencia(nr_seq_gerencia_insatisf) ds_gerencia,
                (select	max(ds_justificativa)
                from	man_ordem_serv_analise_vv mosav
                where	mosav.nr_seq_ordem_serv = mos.nr_sequencia
                and	mosav.dt_atualizacao = (select	max(mosavd.dt_atualizacao)
                                                from	man_ordem_serv_analise_vv mosavd
                                                where	mosavd.nr_seq_ordem_serv = mosav.nr_seq_ordem_serv)) ds_justificativa_vv,
                obter_valor_dominio(9431, mos.ie_severity_harm) ds_privacy_risk,
                decode(mos.ie_potencial_security, 'N', 'Nao', 'S', 'Sim', null, 'Nao') potencial_security,
                decode(mos.ie_potencial_privacy, 'N', 'Nao', 'S', 'Sim', null, 'Nao') ie_potencial_privacy
        from	man_ordem_servico mos) c
where	a.ds_key = b.ds_key
and	a.nr_seq_service_order = c.nr_seq_os (+)
and	a.nr_seq_intencao_uso = b.nr_seq_intencao_uso
and	a.ds_version = b.ds_version;
/