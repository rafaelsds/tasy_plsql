create or replace
view adep_pend_ext_v
as
select	1 nr_seq_apresent,
	'D' ie_tipo_item,
	'Dieta oral' ds_tipo_item,
	'Dietas orais' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	-1 nr_seq_item,
	null ie_acm_sn,
	null ie_di,
	c.cd_refeicao cd_item,
	substr(obter_valor_dominio(99,c.cd_refeicao),1,240) ds_item,
	'' cd_intervalo, 
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	prescr_dieta_hor c,
	prescr_medica a
where	c.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'N'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	2 nr_seq_apresent,
	'S' ie_tipo_item,
	'Suplemento oral' ds_tipo_item,
	'Suplementos orais' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 12
and	nvl(a.ie_adep,'S') = 'N'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	3 nr_seq_apresent,
	'M' ie_tipo_item,
	'Medicamento' ds_tipo_item,
	'Medicamentos' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	b.cd_intervalo cd_intervalo,
	substr(obter_diluicao_medic_adep(c.nr_sequencia),1,240) ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
--and	nvl(c.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 1
and	nvl(a.ie_adep,'S') = 'N'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	3 nr_seq_apresent,
	'MAT' ie_tipo_item,
	'Material' ds_tipo_item,
	'Materiais' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	nvl(b.ie_bomba_infusao,'N') ie_di,
	to_char(b.cd_material) cd_item,
	substr(obter_desc_material(b.cd_material),1,240) ds_item,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	c.qt_dispensar_hor qt_pend,
	c.cd_unidade_medida cd_um,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	b.ie_via_aplicacao ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.ie_agrupador = 2
and	nvl(a.ie_adep,'S') = 'N'
and	obter_se_material_adep(a.cd_estabelecimento,b.cd_material) = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	--4 nr_seq_apresent,
	--'P' ie_tipo_item,
	--'Procedimento' ds_tipo_item,
	--'Procedimentos' ds_tipo_item_plural,
	decode(b.nr_seq_exame, null, 5, 8) nr_seq_apresent,
	decode(b.nr_seq_exame, null, 'P', 'L') ie_tipo_item,
	decode(b.nr_seq_exame, null, 'Procedimento', 'Coleta') ds_tipo_item,
	decode(b.nr_seq_exame, null, 'Procedimentos', 'Coletas') ds_tipo_item_plural,
	decode(b.nr_seq_exame, null, 'N', 'S') ie_laboratorio,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_acm,'N'), 'S', 'S', decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', 'N')) ie_acm_sn,
	'N' ie_di,
	to_char(b.cd_procedimento) cd_item,
	substr(obter_desc_prescr_proc(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno),1,240) ds_item,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	substr(obter_desc_topografia_dor(b.nr_seq_topografia),1,60) ds_topografia,
	substr(obter_se_assoc_proc_adep(b.cd_procedimento,b.ie_origem_proced),1,1) ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	prescr_proc_hor c,
	prescr_procedimento b,
	prescr_medica a
where	c.nr_seq_procedimento = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	b.nr_seq_solic_sangue is null
and	b.nr_seq_derivado is null
and	b.nr_seq_prot_glic is null
and	((b.nr_seq_proc_interno is null) or (obter_ctrl_glic_proc(b.nr_seq_proc_interno) = 'NC'))
and	nvl(a.ie_adep,'S') = 'N'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	6 nr_seq_apresent,
	'R' ie_tipo_item,
	'Recomendação' ds_tipo_item,
	'Recomendações' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.nr_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	a.cd_setor_atendimento cd_setor_prescr,
	b.nr_sequencia nr_seq_item,
	decode(padroniza_horario(b.ds_horarios), '', 'S', 'N') ie_acm_sn,
	'N' ie_di,
	to_char(b.cd_recomendacao) cd_item,
	substr(obter_rec_prescricao(b.nr_sequencia, b.nr_prescricao),1,240) ds_item,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	null ds_topografia,
	'N' ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	prescr_rec_hor c,
	prescr_recomendacao b,
	prescr_medica a
where	c.nr_seq_recomendacao = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	nvl(a.ie_adep,'S') = 'N'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	7 nr_seq_apresent,
	'E' ie_tipo_item,
	'SAE' ds_tipo_item,
	'SAE' ds_tipo_item_plural,
	'N' ie_laboratorio,
	a.nr_atendimento,
	a.nr_sequencia,
	a.cd_prescritor,
	substr(obter_nome_pf(a.cd_prescritor),1,60) nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	to_number(null) cd_setor_prescr,
	b.nr_sequencia nr_seq_item,
	decode(nvl(b.ie_se_necessario,'N'), 'S', 'S', decode(padroniza_horario(b.ds_horarios), '', 'S', 'N')) ie_acm_sn,
	'N' ie_di,
	to_char(b.nr_seq_proc) cd_item,
	substr(obter_desc_intervencoes(b.nr_seq_proc),1,240) ds_item,
	b.cd_intervalo cd_intervalo,
	null ds_diluente,
	substr(obter_desc_topografia_dor(b.nr_seq_topografia),1,60) ds_topografia,
	'S' ie_assoc_adep,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario,
	1 qt_pend,
	'' cd_um,
	1 qt_dose,
	'' cd_unidade_medida_dose,
	'' ie_via,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'C'),1,10) cd_pessoa_evento,
	--substr(obter_resp_atend_data_opcao(a.nr_atendimento,c.dt_horario,'N'),1,60) nm_pessoa_evento
	'' cd_pessoa_evento,
	'' nm_pessoa_evento
from	pe_prescr_proc_hor c,
	pe_prescr_proc b,
	pe_prescricao a
where	c.nr_seq_pe_proc = b.nr_sequencia
and	c.nr_seq_pe_prescr = b.nr_seq_prescr
and	b.nr_seq_prescr = a.nr_sequencia
and	c.dt_fim_horario is null
and	c.dt_suspensao is null
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_horario_especial,'N') <> 'S'
and	nvl(b.ie_adep,'N') = 'N';
/