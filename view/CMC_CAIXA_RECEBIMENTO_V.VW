create or replace 
view cmc_caixa_recebimento_v as
select	'Recebemos de: ' || substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,255) nm_pessoa,
	a.cd_cgc nr_cnpj,
	substr(obter_dados_pf(a.cd_pessoa_fisica,'CPF'),1,100) nr_cpf,
	obter_valores_caixa_rec(a.nr_sequencia,'VDO') vl_recibo,
	substr(obter_extenso(obter_valores_caixa_rec(a.nr_sequencia,'VDO')),1,255) ds_valor,
	substr(obter_desc_trans_financ(a.nr_seq_trans_financ),1,100) ds_trans_fin,
	a.ds_observacao,
	nvl(a.vl_especie,0) vl_especie,
	nvl(obter_valores_caixa_rec(a.nr_sequencia,'VCH'),0) vl_cheque,
	nvl(obter_valores_caixa_rec(a.nr_sequencia,'VCA'),0) vl_cartao,
	null nr_atendimento,
	substr(Obter_Pessoa_Conta(c.nr_interno_conta,'X'),1,255) nm_paciente,
	null dt_entrada,
	OBTER_DATA_NASCTO_PF(c.cd_pessoa_fisica) dt_nascimento,	
	obter_cpf_pessoa_fisica(c.cd_pessoa_fisica) nr_cpf_paciente,
	c.nr_documento nr_documento,
	a.nr_recibo
from	caixa_receb a,
	movto_cartao_cr b,
	movto_trans_financ c 
where 	a.nr_sequencia 		= b.nr_seq_caixa_rec(+)
and	c.nr_seq_caixa_rec	= a.nr_sequencia 
and 	not exists(	select	1
			from	titulo_receber_liq x
			where	x.nr_seq_caixa_rec	= a.nr_sequencia)	
group by	
	'Recebemos de: ' || substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,255),
	a.cd_cgc,
	substr(obter_dados_pf(a.cd_pessoa_fisica,'CPF'),1,100),
	obter_valores_caixa_rec(a.nr_sequencia,'VDO'),
	substr(obter_extenso(obter_valores_caixa_rec(a.nr_sequencia,'VDO')),1,255),
	substr(obter_desc_trans_financ(a.nr_seq_trans_financ),1,100),
	nvl(obter_valores_caixa_rec(a.nr_sequencia,'VCA'),0),
	a.ds_observacao,
	nvl(a.vl_especie,0),
	obter_valores_caixa_rec(a.nr_sequencia,'VCH'),
	c.nr_documento,
	substr(Obter_Pessoa_Conta(c.nr_interno_conta,'X'),1,255),
	OBTER_DATA_NASCTO_PF(c.cd_pessoa_fisica),
	obter_cpf_pessoa_fisica(c.cd_pessoa_fisica),
	a.nr_recibo
union all
select	'Recebemos de: ' || substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,255) nm_pessoa,
	a.cd_cgc nr_cnpj,
	substr(obter_dados_pf(a.cd_pessoa_fisica,'CPF'),1,100) nr_cpf,
	obter_valores_caixa_rec(a.nr_sequencia,'VDO') vl_recibo,
	substr(obter_extenso(obter_valores_caixa_rec(a.nr_sequencia,'VDO')),1,255) ds_valor,	
	substr(obter_desc_trans_financ(a.nr_seq_trans_financ),1,100) ds_trans_fin,
	a.ds_observacao,
	nvl(c.vl_recebido,0) vl_especie,
	nvl(obter_valores_caixa_rec(a.nr_sequencia,'VCH'),0) vl_cheque,
	nvl(obter_valores_caixa_rec(a.nr_sequencia,'VCA'),0) vl_cartao,
	e.nr_atendimento nr_atendimento,
	substr(obter_nome_pessoa_fisica(e.cd_pessoa_fisica,null),1,255) nm_paciente,
	e.dt_entrada dt_entrada,
	OBTER_DATA_NASCTO_PF(e.cd_pessoa_fisica) dt_nascimento,	
	obter_cpf_pessoa_fisica(e.cd_pessoa_fisica) nr_cpf_paciente,
	c.nr_documento nr_documento,
	a.nr_recibo
from	caixa_receb a,	
	titulo_receber_liq c,
	titulo_receber d,
	atendimento_paciente e	
where	a.nr_sequencia		= c.nr_seq_caixa_rec 
and 	c.nr_titulo		= d.nr_titulo
and	d.nr_atendimento	= e.nr_atendimento
group by	
	'Recebemos de: ' || substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,255),
	a.cd_cgc,
	substr(obter_dados_pf(a.cd_pessoa_fisica,'CPF'),1,100),
	obter_valores_caixa_rec(a.nr_sequencia,'VDO'),
	substr(obter_extenso(obter_valores_caixa_rec(a.nr_sequencia,'VDO')),1,255),
	substr(obter_desc_trans_financ(a.nr_seq_trans_financ),1,100),
	a.ds_observacao,
	c.vl_recebido,
	obter_valores_caixa_rec(a.nr_sequencia,'VCH'),
	nvl(obter_valores_caixa_rec(a.nr_sequencia,'VCA'),0),
	e.nr_atendimento,
	substr(obter_nome_pessoa_fisica(e.cd_pessoa_fisica,null),1,255),
	e.dt_entrada,
	OBTER_DATA_NASCTO_PF(e.cd_pessoa_fisica),
	obter_cpf_pessoa_fisica(e.cd_pessoa_fisica),c.nr_documento,
	a.nr_recibo
/