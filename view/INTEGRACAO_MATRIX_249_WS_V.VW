CREATE OR REPLACE VIEW integracao_matrix_249_WS_V AS
select 
MAX(a.IE_SITUACAO) as IE_SITUACAO
from cliente_integracao a,
     informacao_integracao c
where a.nr_seq_inf_integracao = c.nr_sequencia
and c.nr_seq_evento = 249;
/