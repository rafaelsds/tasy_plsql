create or replace view tws_tiss_motivo_glosa_v as 
select 	nr_sequencia,
	cd_motivo_tiss,
	ds_motivo_tiss,
	dt_atualizacao 
from 	tiss_motivo_glosa;
/