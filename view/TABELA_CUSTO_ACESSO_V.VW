create or replace
view tabela_custo_acesso_v as
select	a.nr_sequencia,
	a.cd_empresa,
	a.cd_estabelecimento,
	a.cd_tabela_custo,
	a.cd_tipo_tabela_custo,
	a.nr_seq_grupo_emp,
	1 ie_nivel_acesso
from	tabela_custo a
where	a.cd_estabelecimento is not null
and	a.nr_seq_grupo_emp is null
union all
select	a.nr_sequencia,
	a.cd_empresa,
	b.cd_estabelecimento,
	a.cd_tabela_custo,
	a.cd_tipo_tabela_custo,
	a.nr_seq_grupo_emp,
	2 ie_nivel_acesso
from	empresa e,
	estabelecimento b,
	tabela_custo a
where	a.cd_estabelecimento is null
and	a.nr_seq_grupo_emp is null
and	a.cd_empresa is not null
and	a.cd_empresa	= b.cd_empresa
and	e.cd_empresa	= a.cd_empresa
and	b.ie_situacao	= 'A'
and	e.ie_situacao	= 'A'
union all
select	a.nr_sequencia,
	ge.cd_empresa,
	b.cd_estabelecimento,
	a.cd_tabela_custo,
	a.cd_tipo_tabela_custo,
	a.nr_seq_grupo_emp,
	3 ie_nivel_acesso
from	grupo_empresa_v ge,
	empresa e,
	estabelecimento b,
	tabela_custo a
where	a.cd_estabelecimento is null
and	a.nr_seq_grupo_emp is not null
and	ge.nr_seq_grupo_empresa	= a.nr_seq_grupo_emp
and	ge.cd_empresa	= e.cd_empresa
and	e.cd_empresa	= b.cd_empresa
and	b.ie_situacao	= 'A'
and	e.ie_situacao	= 'A';
/