CREATE OR REPLACE 
VIEW 	UDI_VALORES_PROT_RET_CONV_V AS
 select	a.nr_Seq_protocolo,
	max(a.dt_mesano_referencia) dt_mesano_referencia,
	max((	select	sum(obter_valor_conta(b.nr_interno_conta,0))
		from	conta_paciente b
		where	b.nr_seq_protocolo = a.nr_Seq_protocolo)) vl_conta,
	max((	select	max(b.nr_atendimento)
		from	conta_paciente b
		where	b.nr_Seq_protocolo = a.nr_seq_protocolo)) nr_atendimento,
	max((	select	max(b.nr_interno_conta)
		from	conta_paciente b
		where	b.nr_seq_protocolo = a.nr_Seq_protocolo)) nr_interno_conta,
	max((	select	max(c.cd_autorizacao)
		from	conta_paciente_guia c,
			conta_paciente b
		where	c.nr_interno_conta = b.nr_interno_conta
		and	b.nr_seq_protocolo = a.nr_Seq_protocolo)) cd_autorizacao,
	max((	select	max(d.dt_entrada)
		from	atendimento_paciente d,
			conta_paciente b
		where	d.nr_atendimento = b.nr_atendimento
		and	b.nr_Seq_protocolo = a.nr_seq_protocolo)) dt_entrada,
	max((	select	max(x.dt_pagamento_previsto)
		from	titulo_receber x
		where	x.nr_seq_protocolo = a.nr_Seq_protocolo)) dt_vencimento,
	a.cd_convenio cd_convenio,
	nvl(max(obter_total_protocolo(a.nr_Seq_protocolo)),0) vl_protocolo,
	sum((	select	nvl(sum(x.vl_pago),0)
		from	convenio_retorno_item x,
			conta_paciente s
		where	x.nr_interno_conta = s.nr_interno_conta
		and	s.nr_Seq_protocolo = a.nr_seq_protocolo)) vl_recebido,
	nvl(sum((	select	nvl(sum(j.vl_saldo_titulo),0)
			from	titulo_receber j,
				protocolo_convenio x
			where	x.nr_Seq_protocolo = j.nr_Seq_protocolo
			and	x.nr_seq_protocolo = a.nr_seq_protocolo)),0) vl_amenor,
	nvl(sum((	select	nvl(sum(obter_valores_guia_grc(q.nr_seq_lote_hist,q.nr_interno_conta,q.cd_autorizacao,'VA')),0)
			from	lote_audit_hist_guia q ,
				conta_paciente t
			where	q.nr_interno_conta = t.nr_interno_conta
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) vl_recurso,
	nvl(sum((	select 	sum(x.vl_pago)
			from 	convenio_retorno_item x,
				conta_paciente t,
				convenio_retorno s
			where 	x.nr_interno_conta = t.nr_interno_conta
			and	x.nr_seq_retorno   = s.nr_Sequencia
			and	s.nr_seq_tipo      = 3
			and 	t.nr_Seq_protocolo = a.nr_Seq_protocolo
			and 	x.nr_seq_retorno > (	select 	min(j.nr_seq_retorno)
							from 	convenio_retorno_item j,
								conta_paciente f
							where 	j.nr_interno_conta = f.nr_interno_conta
							and 	f.nr_seq_protocolo = a.nr_seq_protocolo))),0) vl_rec_recurso,
	nvl(sum((	select	sum(x.vl_glosado)
			from	convenio_Retorno_item x,
				conta_paciente t
			where	x.nr_interno_conta = t.nr_interno_conta
			and	x.ie_glosa = 'S'
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
				nvl(sum((select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
										from	lote_audit_hist_guia r,
											conta_paciente c
										where	r.nr_interno_conta = c.nr_interno_conta
										and	c.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_glosa_aceita,
	/*nvl(sum((	select	nvl(max(j.vl_saldo_titulo),0)
			from	titulo_receber j
			where	exists(	select	1
					from 	convenio_retorno_item x,
						conta_paciente y
					where	x.nr_interno_conta = y.nr_interno_conta
					and	y.nr_seq_protocolo = j.nr_seq_protocolo)
			and	j.nr_seq_protocolo = a.nr_seq_protocolo)),0) -
				nvl(sum((	select	nvl(sum(obter_valores_guia_grc(q.nr_seq_lote_hist,q.nr_interno_conta,q.cd_autorizacao,'VA')),0)
						from	lote_audit_hist_guia q ,
							conta_paciente t
						where	q.nr_interno_conta = t.nr_interno_conta
						and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) -
							nvl(sum((	select	sum(x.vl_glosado)
			from	convenio_Retorno_item x,
				conta_paciente t
			where	x.nr_interno_conta = t.nr_interno_conta
			and	x.ie_glosa = 'S'
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
				nvl(sum((select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
										from	lote_audit_hist_guia r,
											conta_paciente c
										where	r.nr_interno_conta = c.nr_interno_conta
										and	c.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_recursar,*/
	nvl(sum((	select	nvl(sum(obter_valores_guia_grc(q.nr_seq_lote_hist,q.nr_interno_conta,q.cd_autorizacao,'VA')),0)
			from	lote_audit_hist_guia q ,
				conta_paciente t
			where	q.nr_interno_conta = t.nr_interno_conta
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) vl_recursar,
			/*nvl(sum((	select	nvl(max(j.vl_saldo_titulo),0)
			from	titulo_receber j
			where	exists(	select	1
					from 	convenio_retorno_item x,
						conta_paciente y
					where	x.nr_interno_conta = y.nr_interno_conta
					and	y.nr_seq_protocolo = j.nr_seq_protocolo)
			and	j.nr_seq_protocolo = a.nr_seq_protocolo)),0) +
					nvl(sum((	select	sum(x.vl_glosado)
			from	convenio_Retorno_item x,
				conta_paciente t
			where	x.nr_interno_conta = t.nr_interno_conta
			and	x.ie_glosa = 'S'
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
				nvl(sum((select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
										from	lote_audit_hist_guia r,
											conta_paciente c
										where	r.nr_interno_conta = c.nr_interno_conta
										and	c.nr_Seq_protocolo = a.nr_seq_protocolo)),0) -
											nvl(sum((	select 	sum(x.vl_pago)
													from 	convenio_retorno_item x,
														conta_paciente t
													where 	x.nr_interno_conta = t.nr_interno_conta
													and 	t.nr_Seq_protocolo = a.nr_Seq_protocolo
													and 	x.nr_seq_retorno > (	select 	min(j.nr_seq_retorno)
																	from 	convenio_retorno_item j,
																		conta_paciente f
																	where 	j.nr_interno_conta = f.nr_interno_conta
																	and 	f.nr_seq_protocolo = a.nr_seq_protocolo))),0) vl_glosado,*/
	nvl(sum((select	sum(q.vl_amenor)
	from	convenio_retorno_item q,
		conta_paciente c
	where	c.nr_interno_conta = q.nr_interno_conta
	and	c.nr_seq_protocolo = a.nr_seq_protocolo
	and	q.vl_pago > 0
	and	not exists	(	select	f.nr_seq_retorno
					from	convenio_retorno_item f,
						conta_paciente g
					where	f.nr_interno_conta 	= g.nr_interno_conta
					and	q.nr_interno_conta	= f.nr_interno_conta
					and	g.nr_seq_protocolo 	= a.nr_Seq_protocolo
					and	f.nr_seq_retorno 	> q.nr_seq_retorno))),0) +
						nvl(sum((	select	sum(x.vl_glosado)
								from	convenio_Retorno_item x,
									conta_paciente t
								where	x.nr_interno_conta = t.nr_interno_conta
								and	x.ie_glosa = 'S'
								and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
									nvl(sum((	select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
											from	lote_audit_hist_guia r,
												conta_paciente c
											where	r.nr_interno_conta = c.nr_interno_conta
											and	c.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_glosado,
	nvl(sum((	select	nvl(sum(g.vl_saldo_titulo),0)
		from	titulo_receber g,
			protocolo_convenio f
		where	f.nr_Seq_protocolo = a.nr_seq_protocolo
		and	f.nr_seq_protocolo = g.nr_seq_protocolo
		and	g.dt_pagamento_previsto < sysdate)),0) vl_vencido,
	nvl(sum((	select	nvl(sum(g.vl_saldo_titulo),0)
		from	titulo_receber g,
			protocolo_convenio f
		where	f.nr_Seq_protocolo = a.nr_seq_protocolo
		and	f.nr_seq_protocolo = g.nr_seq_protocolo
		and	g.dt_pagamento_previsto > sysdate)),0) vl_vencer,
	nvl(max(obter_nota_conta_protocolo(a.nr_Seq_protocolo,0)),0) nr_nota_fiscal,
	max((	select	max(decode(obter_se_negativo(Obter_Dias_Entre_Datas(x.dt_pagamento_previsto,sysdate)),
			'S',Obter_Dias_Entre_Datas(sysdate,x.dt_pagamento_previsto),
				Obter_Dias_Entre_Datas(x.dt_pagamento_previsto,sysdate)))
		from	titulo_receber x
		where	x.nr_seq_protocolo = a.nr_Seq_protocolo)) nr_atr,
	max((	select	count(*)
		from	titulo_receber x
		where	x.nr_Seq_protocolo = a.nr_seq_protocolo)) nr_parcela
from 	protocolo_convenio a
where 	exists(	select	1 from titulo_receber t where t.nr_Seq_protocolo = a.nr_Seq_protocolo)
group by a.nr_Seq_protocolo,
	 a.cd_convenio
order by a.nr_Seq_protocolo
/