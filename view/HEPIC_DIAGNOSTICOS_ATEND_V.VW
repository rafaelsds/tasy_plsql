create or replace 
view hepic_diagnosticos_atend_v as
select  a.nr_atendimento, b.ie_tipo_atendimento, c.nr_prontuario,
		dt_diagnostico, cd_doenca, a.dt_atualizacao, a.nm_usuario,
		decode (a.cd_medico, null, cd_medico_resp, a.cd_medico) cd_medico,
		decode (a.ie_classificacao_doenca,'P', 'Principal','Secundária') ie_classificacao_doenca,
		decode (a.ie_tipo_diagnostico,'P', 'Preliminar','Definitiva') ie_tipo_diagnostico
   from diagnostico_doenca a, 
		atendimento_paciente b, 
		pessoa_fisica c
  where a.nr_atendimento = b.nr_atendimento
	and b.cd_pessoa_fisica = c.cd_pessoa_fisica
	order by nr_atendimento desc, 
			dt_diagnostico desc;
/