create or replace view lab_info_compl_pardini_v2 as
  select  distinct
    t.volumediurese_p,
    t.tempodiurese_p,
    t.cd_estabelecimento,
    t.qt_peso,
    t.qt_altura_cm,
    t.ds_campo,
    t.nr_prescricao,
    t.nr_seq_lote_externo,
    t.nr_seq_exame,
    t.nr_seq_material,
    t.tempo_amostra,
    t.peso_amostra,
    t.generico,
    z.ie_integ_hist_saude ie_novo_pardini
from
    (
        select
            null volumediurese_p,
            null tempodiurese_p,
            nvl(e.nr_sequencia, 0) nr_seq_proc_material,
            d.cd_estabelecimento,
            d.qt_peso,
            null qt_altura_cm,
            'Peso' ds_campo,
            a.nr_prescricao,
            a.nr_seq_lote_externo,
            a.nr_seq_exame,
            b.nr_sequencia nr_seq_material,
            null tempo_amostra,
            null peso_amostra,
            null generico
        from
            prescr_procedimento a
            inner join prescr_medica d on
                (a.nr_prescricao = d.nr_prescricao)
            inner join exame_laboratorio c on
                (c.nr_seq_exame = a.nr_seq_exame)
            inner join material_exame_lab b on
                (b.nr_sequencia = obter_mat_exame_lab_prescr(a.nr_prescricao, a.nr_sequencia, 1))
            inner join exame_lab_material f on
                (f.nr_seq_exame =  c.nr_seq_exame and f.nr_seq_material = b.nr_sequencia)
            left outer join prescr_proc_material e on
                (e.nr_prescricao  =  a.nr_prescricao and
                    e.nr_seq_material = b.nr_sequencia and
                    decode(e.nr_seq_grupo,0,c.nr_seq_grupo,e.nr_seq_grupo)   = c.nr_seq_grupo and
                    nvl(f.qt_coleta,0) > 1)
        where
            d.qt_peso is not null
        union all
        select
            null volumediurese_p,
            null tempodiurese_p,
            nvl(e.nr_sequencia, 0) nr_seq_proc_material,
            d.cd_estabelecimento,
            null qt_peso,
            d.qt_altura_cm qt_altura_cm,
            'Altura' ds_campo,
            a.nr_prescricao,
            a.nr_seq_lote_externo,
            a.nr_seq_exame,
            b.nr_sequencia nr_seq_material,
            null tempo_amostra,
            null peso_amostra,
            null generico
        from
            prescr_procedimento a
            inner join prescr_medica d on
                (a.nr_prescricao = d.nr_prescricao)
            inner join exame_laboratorio c on
                (c.nr_seq_exame = a.nr_seq_exame)
            inner join material_exame_lab b on
                (b.nr_sequencia = obter_mat_exame_lab_prescr(a.nr_prescricao, a.nr_sequencia, 1))
            inner join exame_lab_material f on
                (f.nr_seq_exame =  c.nr_seq_exame and f.nr_seq_material = b.nr_sequencia)
            left outer join prescr_proc_material e on
                (e.nr_prescricao  =  a.nr_prescricao and
                    e.nr_seq_material = b.nr_sequencia and
                    decode(e.nr_seq_grupo,0,c.nr_seq_grupo,e.nr_seq_grupo)   = c.nr_seq_grupo and
                    nvl(f.qt_coleta,0) > 1)
        where
            d.qt_altura_cm is not null
       
       
        union all
        
        select
            null volumediurese_p,
            null tempodiurese_p,
            e.nr_sequencia nr_seq_proc_material,
            d.cd_estabelecimento,
            null qt_peso,
            null qt_altura_cm,
            h.ds_tag ds_campo,
            a.nr_prescricao,
            a.nr_seq_lote_externo,
            a.nr_seq_exame,
            b.nr_sequencia nr_seq_material,
            null tempo_amostra,
            null peso_amostra,
            g.ds_valor generico
        from
            prescr_procedimento a,
            exame_laboratorio c,
            material_exame_lab b,
            exame_lab_material f,
            prescr_medica d,
            prescr_proc_material e,
            lab_comple_pardini g,
            lab_tipo_comple_pardini h
        where
            f.nr_seq_material = b.nr_sequencia and
            f.nr_seq_exame =  a.nr_seq_exame and
            b.nr_sequencia = obter_mat_exame_lab_prescr(a.nr_prescricao, a.nr_sequencia, 1) and
            a.nr_prescricao = d.nr_prescricao and
            c.nr_seq_exame = a.nr_seq_exame and
            e.nr_seq_material = obter_mat_exame_lab_prescr(a.nr_prescricao, a.nr_sequencia, 1) and
            e.nr_prescricao  =  a.nr_prescricao and
            decode(e.nr_seq_grupo,0,c.nr_seq_grupo,e.nr_seq_grupo)   = c.nr_seq_grupo and
            g.nr_prescricao  =  a.nr_prescricao and
            g.nr_seq_prescr = a.nr_sequencia and
            g.ds_valor is not null and
            g.nr_tipo_comple_pardini = h.nr_sequencia
    ) t,
    lab_parametro z
where
    t.cd_estabelecimento = z.cd_estabelecimento;
/