create or replace view pls_sca_vinculo_relat_v
as
select	decode(c.ie_tipo_item,'3',0,nvl(c.vl_item,0)) vl_item,
	decode(c.ie_tipo_item,'3',nvl(c.vl_item,0),0) vl_coparticipacao,
	substr(pls_obter_dados_produto(k.nr_seq_plano,'N') ,1,255) ds_produto,
	g.ie_tipo_contratacao,
	g.ie_regulamentacao,
	a.nr_seq_lote,
	j.dt_emissao,
	j.dt_liquidacao,
	j.dt_vencimento,
	c.ie_tipo_item,
	f.cd_portador,
	k.nr_seq_plano nr_seq_plano,
	b.nr_seq_plano nr_seq_plano_mens,
	f.nr_seq_conta_banco,
	a.nr_seq_forma_cobranca,
	a.qt_beneficiarios,
	b.nr_seq_segurado nr_seq_segurado
from	pls_sca_vinculo			k,
	titulo_receber			j,
	pls_plano			g,
	pls_contrato_pagador_fin	f,
	pls_contrato_pagador		e,
	pls_mensalidade_seg_item	c,
	pls_mensalidade_segurado	b,
	pls_mensalidade			a
where	c.nr_seq_mensalidade_seg	= b.nr_sequencia
and	b.nr_seq_mensalidade		= a.nr_sequencia
and	a.nr_seq_pagador		= e.nr_sequencia
and	f.nr_seq_pagador		= e.nr_sequencia
and	b.nr_seq_plano			= g.nr_sequencia
and	j.nr_seq_mensalidade		= a.nr_sequencia
and	k.nr_seq_segurado		= b.nr_seq_segurado
and	c.nr_seq_vinculo_sca 		= k.nr_sequencia;
/