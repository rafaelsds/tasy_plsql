Create or replace View ATENDIMENTO_PRECAUCAO_V as
select	a.*,
	substr(obter_descricao_padrao('MOTIVO_ISOLAMENTO','DS_MOTIVO',NR_SEQ_MOTIVO_ISOL),1,255) ds_motivo,
	substr(obter_descricao_padrao('CIH_PRECAUCAO','DS_PRECAUCAO',NR_SEQ_PRECAUCAO),1,255) DS_PRECAUCAO,
	substr(obter_descricao_padrao('SETOR_ATENDIMENTO','DS_SETOR_ATENDIMENTO',OBTER_SETOR_ATENDIMENTO(A.NR_ATENDIMENTO)),1,255) DS_SETOR_ATENDIMENTO
from	ATENDIMENTO_PRECAUCAO a
where	dt_inativacao is null;	
/
