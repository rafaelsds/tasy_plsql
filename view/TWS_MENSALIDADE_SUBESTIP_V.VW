create or replace view tws_mensalidade_subestip_v as
select	b.dt_mesano_referencia,
	b.vl_mensalidade,
	b.nr_sequencia nr_seq_mensalidade_seg,
	b.nr_seq_subestipulante,
	a.nr_seq_contrato,
	d.nr_titulo,
	c.nr_seq_pagador,
	d.dt_vencimento,
	d.dt_pagamento_previsto,
	d.ie_situacao,
	nvl(b.vl_coparticipacao, 0) vl_coparticipacao,
	c.nr_sequencia nr_seq_mensalidade,
	e.nr_sequencia nr_seq_lote
from 	pls_segurado a,
	pls_mensalidade_segurado b,
	pls_mensalidade c,
	titulo_receber d,
	pls_lote_mensalidade e
where 	a.nr_sequencia = b.nr_seq_segurado
and 	c.nr_sequencia = b.nr_seq_mensalidade
and	c.nr_sequencia = d.nr_seq_mensalidade
and	e.nr_sequencia = c.nr_seq_lote
and 	c.dt_cancelamento is null
and	d.ie_situacao in ('1', '2', '4', '6')
and	((e.ie_visualizar_portal = 'S') or (e.ie_visualizar_portal is null))
and	b.nr_seq_subestipulante is not null;
/