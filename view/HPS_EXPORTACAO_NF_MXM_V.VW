Create or replace View hps_exportacao_nf_mxm_v as
select	cd_n,
	nr_nf,
	dt_en,
	dt_emissao,
	vl_nf,
	dt_entrada_saida,
	ds_cp,
	cd_gp,
	cd_cp,
	ds_c,
	dt_atualizacao,
	cd_estabelecimento
from(	select	n.dt_atualizacao,
		n.cd_estabelecimento,
		lpad((decode(n.ie_tipo_nota,'EF',(nvl(n.cd_pessoa_fisica,0)),decode((nvl(n.cd_cgc,n.cd_cgc_emitente)),'64718004000176',
			nvl(substr(obter_dados_pf(substr(obter_pessoa_atendimento(i.nr_atendimento,'C'),1,10),'CPF'),1,14),'    PARTICULAR'),
			(nvl(n.cd_cgc,n.cd_cgc_emitente))))),14,'0')cd_n,
		lpad(n.nr_nota_fiscal,20,' ')nr_nf,
		n.dt_emissao dt_en,
		n.dt_atualizacao dt_emissao,
		lpad(replace(replace(campo_mascara_virgula(sum(nvl(r.vl_rateio,i.vl_total_item_nf)) + 
			sum(obter_frete_rateio_item_nf(n.nr_sequencia,i.nr_item_nf)) +
			sum(obter_ipi_rateio_item_nf(n.nr_sequencia,i.nr_item_nf))),'.',''),',','.'),11,' ') vl_nf,
		n.dt_entrada_saida,
		substr(obter_desc_cond_pagto(n.cd_condicao_pagamento),1,11) ds_cp,
		nvl(substr(obter_dados_conta_contabil(i.cd_conta_contabil,null,'SC'),16,10),'5230019') cd_gp,
		lpad((decode(c.cd_sistema_ant,null,'02',c.cd_sistema_ant)),2,' ') cd_cp,
		rpad(nvl(decode(n.cd_operacao_nf,3, substr(ctb_obter_dados_centro_custo(nvl(i.cd_centro_custo,r.cd_centro_custo),'S'),1,60),
			21,substr(ctb_obter_dados_centro_custo(nvl(i.cd_centro_custo,r.cd_centro_custo),'S'),1,60),
			31,substr(ctb_obter_dados_centro_custo(nvl(i.cd_centro_custo,r.cd_centro_custo),'S'),1,60),' '),' '),15,' ') ds_c
	from	nota_fiscal_item_rateio r,
		nota_fiscal_item i,
		nota_fiscal n,
		condicao_pagamento c
	where	i.nr_sequencia=n.nr_sequencia
	and	n.dt_atualizacao_estoque is not null
	and	n.ie_situacao='1'
	and	i.nr_sequencia=r.nr_seq_nota(+)
	and	i.nr_item_nf=r.nr_item_nf(+)
	and	n.cd_operacao_nf in (31,21,23,30,29,1,3)
	and	n.cd_condicao_pagamento = c.cd_condicao_pagamento
	group by n.ie_tipo_nota,
		n.cd_estabelecimento,
		n.cd_pessoa_fisica,
		n.cd_cgc,
		n.cd_cgc_emitente,
		n.nr_nota_fiscal,
		n.dt_emissao,
		n.dt_entrada_saida,
		i.cd_conta_contabil,
		i.nr_atendimento,
		n.cd_condicao_pagamento,
		n.dt_atualizacao,
		c.cd_sistema_ant,
		n.cd_operacao_nf,
		i.cd_centro_custo,
		r.cd_centro_custo)
order by 2;
/