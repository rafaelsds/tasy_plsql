CREATE OR REPLACE VIEW REG_TEST_CASE_VALIDATION_V AS 
  select  distinct  
        R.ds_area
        ,R.Crs_Id  
        ,R.Nr_Customer_Requirement  
        ,R.Ds_Customer_Requirement  
        ,R.Ds_Crs_Title  
        ,T.Nr_Sequencia nr_seq_caso_teste  
        ,decode(Obter_Nome_Usuario(T.Nm_Usuario_Nrec),null,'WHEB:'||T.Nm_Usuario_Nrec,Obter_Nome_Usuario(T.Nm_Usuario_Nrec))  USUARIO_CRIACAO
        ,T.Ds_Descricao ds_caso_teste  
        ,T.Ds_Pre_Condicao  
        ,A.Nr_Ordenacao  
        ,A.Nr_Sequencia nr_seq_acao_teste  
        ,A.Ds_Passo  
        ,A.Ds_Funcionalidade  
        ,A.Ds_Resultado  
        ,T.Cd_Versao  
        ,T.Nr_Seq_Revisao  
        ,T.Ie_Tipo_Revisao  
        ,T.Dt_Revisao 
        ,'TASY_VTC_'||T.Nr_Sequencia CT_VALIDATION_ID
        ,'TASY_VTC_'||T.Nr_Sequencia||' - '||  t.Ds_Descricao ds_tc  
        ,'TASY_VTC_'||T.Nr_Sequencia||' - '||  t.Ds_Descricao || chr(13)  || 'Pre condition: ' || t.Ds_Pre_Condicao ds_titulo  
        ,Obter_DS_Formatada_Regulatorio( 'TASY_VTC_'||T.Nr_Sequencia||' - '||  t.Ds_Descricao || chr(13)  || 'Pre condition: ' || t.Ds_Pre_Condicao, t.ie_tipo_revisao) ds_titulo_formatado  
from    Reg_Caso_Teste t,  
        reg_acao_teste a,  
        ( select  distinct v.Crs_Id  
                  ,v.Nr_Customer_Requirement  
                  ,v.Ds_Customer_Requirement  
                  ,v.Ds_Crs_Title
                  ,v.ds_area
           from   Reg_Product_Requirement_v v ) r  

where   T.Nr_Sequencia = A.Nr_Seq_Caso_Teste  
and     R.Nr_Customer_Requirement = T.Nr_Seq_Customer  
and     Ie_Tipo_Documento = 'V';
/