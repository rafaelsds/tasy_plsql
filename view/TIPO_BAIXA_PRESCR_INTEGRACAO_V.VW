create or replace view tipo_baixa_prescr_integracao_v
as
select	cd_tipo_baixa, ds_tipo_baixa
from 	tipo_baixa_prescricao
where 	ie_prescricao_devolucao = 'P'
and 	ie_situacao = 'A'
order by	cd_tipo_baixa, ds_tipo_baixa
/