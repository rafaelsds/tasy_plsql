CREATE OR REPLACE VIEW DETAILS_FROM_SP_PROCESS_V
AS 
  SELECT a.nr_sequencia,
    a.versao,
    a.hotfix,
    a.candidate,
    a.nm_package,
    a.ie_estag_exec,
    a.ie_status_test,
    Details_From_Sp_Process_Pck.Get_Date_Build(a.versao, a.candidate, 'I') dt_inicio_candidate,
    Details_From_Sp_Process_Pck.Get_Date_Build(a.versao, a.candidate, 'F') dt_fim_candidate,
    MAX(a.dt_inicio_exec) dt_inicio_exec,
    MAX(a.dt_fim_exec) dt_fim_exec,
    MAX(a.dt_ini_analise) dt_ini_analise,
    MAX(a.dt_fim_analise) dt_fim_analise,
    MAX(a.dt_doc_inicio) dt_doc_inicio,
    MAX(a.dt_doc_fim) dt_doc_fim,
    sp.nr_service_pack,
    sp.nr_pacote,
    sp.ie_status_build,
    sp.ie_regulatorio,
    sp.ie_situacao_service_pack,
    sp.dt_atualizacao dt_service_pack
  FROM
    (SELECT DISTINCT a.nr_sequencia,
      SUBSTR(b.ds_version,1,9) versao,
      CASE
        WHEN LENGTH(b.ds_version) > 20
        THEN trim(SUBSTR(b.ds_version,23,3))
        ELSE trim(SUBSTR(b.ds_version,11,5))
      END candidate,
      --decode(length(b.ds_version), substr(b.ds_version,11,4),
      a.ds_version,
      b.ds_version ds_version_build,
      NVL(regexp_substr(a.DS_INFO, '(\-)[0-9]{2}(\-)[0-9]{7}', 11), '--') hotfix,
      a.DS_INFO,
      nvl(a.ie_estag_exec, '0') as ie_estag_exec,
      a.ie_status      AS ie_status_test,
      a.dt_schedule    AS dt_inicio_exec,
      a.dt_execution   AS dt_fim_exec,
      a.dt_ini_analise AS dt_ini_analise,
      a.dt_fim_analise AS dt_fim_analise,
      a.dt_doc_inicio  AS dt_doc_inicio,
      a.dt_doc_fim     AS dt_doc_fim,
      c.nm_package
    FROM schem_test_package_sched@schematic4test a,
      schem_test_schedule@schematic4test b,
      schem_test_package@schematic4test c
    WHERE a.nr_sequencia = b.nr_seq_package
    AND c.nr_sequencia = a.nr_seq_package
    AND a.ie_estag_exec IS NOT NULL
    AND upper(c.nm_package) like 'SUITES FIXA%'
    ORDER BY 3
    ) a left join
    corp.man_service_pack_versao@whebl01_dbcorp sp on 
     a.versao    = sp.cd_versao
  AND a.candidate = sp.nr_old_build
  WHERE a.candidate NOT LIKE '%SHOT%'
  AND a.candidate IS NOT NULL
  AND a.versao NOT LIKE '%SHOT%'
  
 
  GROUP BY a.versao,
    a.candidate,
    a.dt_inicio_exec,
    a.ie_estag_exec,
    a.dt_fim_exec,
    a.dt_ini_analise,
    a.dt_fim_analise,
    a.dt_doc_inicio,
    a.dt_doc_fim,
    a.dt_doc_fim,
    a.ie_status_test,
    a.nr_sequencia,
    a.hotfix,
    a.nm_package,
    sp.nr_service_pack,
    sp.nr_pacote,
    sp.ie_status_build,
    sp.ie_regulatorio,
    sp.ie_situacao_service_pack,
    sp.dt_atualizacao
  ORDER BY dt_inicio_candidate DESC;
/