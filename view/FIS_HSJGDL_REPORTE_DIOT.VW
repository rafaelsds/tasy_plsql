create or replace
view FIS_HSJGDL_REPORTE_DIOT as
  select  y.ie_tipo_terceiro,
	y.ie_tipo_operacao_mx,
	y.cd_rfc,
	y.cd_internacional,
	Elimina_Acentos(y.nm_estrangeiro) nm_estrangeiro,
	Elimina_Acentos(y.sg_pais) sg_pais,
	elimina_acentos(y.ds_nacionalidade) ds_nacionalidade,
	trunc(sum(y.vl_total_nota)) vl_total_nota,
	trunc(sum(y.vl_baixa)) vl_baixa,
	trunc(sum(y.vl_mercadoria)) vl_mercadoria,
	trunc(sum(y.vl_campo_8))  vl_campo_8,
	trunc(sum(y.vl_campo_9))  vl_campo_9,
	trunc(sum(y.vl_campo_10)) vl_campo_10,
	trunc(sum(y.vl_campo_11)) vl_campo_11,
	trunc(sum(y.vl_campo_12)) vl_campo_12,
	trunc(sum(y.vl_campo_13)) vl_campo_13,
	trunc(sum(y.vl_campo_14)) vl_campo_14,
	trunc(sum(y.vl_campo_15)) vl_campo_15,
	trunc(sum(y.vl_campo_16)) vl_campo_16,
	trunc(sum(y.vl_campo_17)) vl_campo_17,
	trunc(sum(y.vl_campo_18)) vl_campo_18,
	trunc(sum(y.vl_campo_19)) vl_campo_19,
	trunc(sum(y.vl_campo_20)) vl_campo_20,
	trunc(sum(y.vl_campo_21)) vl_campo_21,
	trunc(sum(y.vl_campo_22)) vl_campo_22,
	y.nr_titulo, y.nota, y.dt_baixa,
	y.nr_lote_contabil,
	replace(y.nr_seq_cheque_cp,',',null)nr_seq_cheque_cp,
	y.nr_seq_diot,
	y.cd_cgc,
	y.ie_union
from (
select 	decode(t.ie_internacional,'N','04','05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(t.ie_internacional, 'N', null, j.cd_internacional) cd_internacional,
	decode(t.ie_internacional, 'N', null, Elimina_Acentos(j.ds_razao_social)) nm_estrangeiro,
	decode(t.ie_internacional, 'N', null, e.sg_pais) sg_pais,
	decode(t.ie_internacional, 'N', null, obter_nome_pais(e.nr_sequencia))  ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_baixa) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,	
	decode(y.nr_tit_receber, null, fis_obter_dados_iva(c.nr_sequencia, 1)) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	to_number(null) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,	
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_19,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 6, z.dt_ref_inicial, fim_dia(z.dt_ref_final)) vl_campo_22,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_baixa) dt_baixa,
	y.nr_lote_contabil,
	nvl(obter_cheque_bordero(y.nr_bordero),y.nr_seq_cheque_cp) nr_seq_cheque_cp,
	z.nr_sequencia nr_seq_diot,
	j.cd_cgc,
	'1' ie_union
from	pessoa_juridica j,
	tipo_pessoa_juridica t,
	nota_fiscal c,
	operacao_nota b,
	pais e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_baixa y,
	fis_diot_controle z
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.nr_seq_pais = e.nr_sequencia(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
--and	trunc(y.dt_baixa) between dt_inicio_p and dt_fim_p
and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)
and	t.cd_tipo_pessoa = j.cd_tipo_pessoa
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_cgc = x.cd_cgc
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
--and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	(((y.nr_seq_cheque_cp is null) and (trunc(y.dt_baixa) between dt_inicio_p and dt_fim_p)) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao between dt_inicio_p and dt_fim_p) = 1))*/
and	((exists (select 1 		
		from 	cheque_bordero_titulo a, 
			titulo_pagar_bordero_v b, 
			cheque c 
		where 	a.nr_bordero = b.nr_bordero 
		and 	a.nr_seq_cheque = c.nr_sequencia
		and 	b.nr_titulo = x.nr_titulo
		and     c.dt_cancelamento is null	
		and 	c.dt_compensacao between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
		union	
		select 	1 
		from 	cheque d 
		where 	d.nr_sequencia = y.nr_seq_cheque_cp
		and     d.dt_cancelamento is null
		and 	d.dt_compensacao between z.dt_ref_inicial and fim_dia(z.dt_ref_final)))
	 or (not exists (select 1 		
			from 	cheque_bordero_titulo a, 
				titulo_pagar_bordero_v b, 
				cheque c 
			where 	a.nr_bordero = b.nr_bordero 
			and 	a.nr_seq_cheque = c.nr_sequencia
			and 	b.nr_titulo = x.nr_titulo
			and	c.dt_atualizacao > sysdate-365
			and     c.dt_cancelamento is null
			union	
			select 	1 
			from 	cheque d 
			where 	d.nr_sequencia = y.nr_seq_cheque_cp
			and     d.dt_cancelamento is null
			and	d.dt_atualizacao > sysdate-365) 
		and trunc(y.dt_baixa) between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
		and  y.nr_tit_receber is null))
group by t.ie_internacional,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.cd_internacional,
	j.ds_razao_social,
	e.sg_pais,
	y.nr_tit_receber,	
	e.nr_sequencia,
	c.nr_sequencia,	
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_baixa),
	y.nr_lote_contabil,
	nvl(obter_cheque_bordero(y.nr_bordero),y.nr_seq_cheque_cp),
	z.dt_ref_inicial, 
	z.dt_ref_final,
	z.nr_sequencia,
	j.cd_cgc
union all
select decode(e.ie_brasileiro, 'S', '04', '05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(e.ie_brasileiro, 'N', j.nr_cartao_estrangeiro, null) cd_internacional,
	decode(e.ie_brasileiro, 'N', Elimina_Acentos(substr(j.nm_pessoa_fisica, 1, 43), null)) nm_estrangeiro,
	decode(e.ie_brasileiro, 'N', substr(p.sg_pais,1,2), null) sg_pais,
	decode(e.ie_brasileiro, 'N', e.ds_nacionalidade, null) ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_baixa) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,
	decode(y.nr_tit_receber, null, fis_obter_dados_iva(c.nr_sequencia, 1)) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	to_number(null) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,	
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_19,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 6, z.dt_ref_inicial, fim_dia(z.dt_ref_final)) vl_campo_22,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_baixa) dt_baixa,
	y.nr_lote_contabil,
	nvl(obter_cheque_bordero(y.nr_bordero),y.nr_seq_cheque_cp) nr_seq_cheque_cp,
	z.nr_sequencia nr_seq_diot,
	j.cd_pessoa_fisica cd_cgc,
	'2' ie_union
from	pessoa_fisica j,
	nota_fiscal c,
	operacao_nota b,
	nacionalidade e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_baixa y,
	pais p,
	fis_diot_controle z
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.cd_nacionalidade = e.cd_nacionalidade(+)
and	obter_dados_pf_pj(j.cd_pessoa_fisica, null, 'CPAIS') = p.cd_codigo_pais(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
--and	trunc(y.dt_baixa) between dt_inicio_p and dt_fim_p
and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_pessoa_fisica = x.cd_pessoa_fisica
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
--and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	((y.nr_seq_cheque_cp is null) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao is not null) = 1))*/
and	((exists (select 1 		
		from 	cheque_bordero_titulo a, 
			titulo_pagar_bordero_v b, 
			cheque c 
		where 	a.nr_bordero = b.nr_bordero 
		and 	a.nr_seq_cheque = c.nr_sequencia
		and 	b.nr_titulo = x.nr_titulo
		and     c.dt_cancelamento is null	
		and 	c.dt_compensacao between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
		union	
		select 	1 
		from 	cheque d 
		where 	d.nr_sequencia = y.nr_seq_cheque_cp
		and     d.dt_cancelamento is null
		and 	d.dt_compensacao between z.dt_ref_inicial and fim_dia(z.dt_ref_final)))
	 or (not exists (select 1 		
			from 	cheque_bordero_titulo a, 
				titulo_pagar_bordero_v b, 
				cheque c 
			where 	a.nr_bordero = b.nr_bordero 
			and 	a.nr_seq_cheque = c.nr_sequencia
			and 	b.nr_titulo = x.nr_titulo
			and	c.dt_atualizacao > sysdate-365
			and     c.dt_cancelamento is null
			union	
			select 	1 
			from 	cheque d 
			where 	d.nr_sequencia = y.nr_seq_cheque_cp
			and     d.dt_cancelamento is null
			and	d.dt_atualizacao > sysdate-365) 
		and trunc(y.dt_baixa) between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
		and  y.nr_tit_receber is null))
group by e.ie_brasileiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.nr_cartao_estrangeiro,
	j.nm_pessoa_fisica,
	p.sg_pais,
	y.nr_tit_receber,	
	e.ds_nacionalidade,
	c.nr_sequencia,
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_baixa),
	y.nr_lote_contabil,
	nvl(obter_cheque_bordero(y.nr_bordero),y.nr_seq_cheque_cp),
	z.dt_ref_inicial, 
	z.dt_ref_final,
	z.nr_sequencia,
	j.cd_pessoa_fisica
union
select 	decode(t.ie_internacional,'N','04','05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(t.ie_internacional, 'N', null, j.cd_internacional) cd_internacional,
	decode(t.ie_internacional, 'N', null, Elimina_Acentos(j.ds_razao_social)) nm_estrangeiro,
	decode(t.ie_internacional, 'N', null, e.sg_pais) sg_pais,
	decode(t.ie_internacional, 'N', null, obter_nome_pais(e.nr_sequencia))  ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_adiantamento) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,	
	fis_obter_dados_iva(c.nr_sequencia, 1) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	to_number(null) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,	
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_19,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 6, z.dt_ref_inicial, fim_dia(z.dt_ref_final)) vl_campo_22,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_atualizacao) dt_baixa,
	null,
	null,
	z.nr_sequencia nr_seq_diot,
	j.cd_cgc,
	'3' ie_union
from	pessoa_juridica j,
	tipo_pessoa_juridica t,
	nota_fiscal c,
	operacao_nota b,
	pais e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_adiant y,
	fis_diot_controle z
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.nr_seq_pais = e.nr_sequencia(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
--and	trunc(y.dt_atualizacao) between dt_inicio_p and dt_fim_p
and	y.dt_atualizacao between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
/*and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)*/
and	t.cd_tipo_pessoa = j.cd_tipo_pessoa
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_cgc = x.cd_cgc
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
--and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	((y.nr_seq_cheque_cp is null) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao is not null) = 1))*/
group by t.ie_internacional,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.cd_internacional,
	j.ds_razao_social,
	e.sg_pais,
	e.nr_sequencia,
	c.nr_sequencia,	
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_atualizacao),
	z.dt_ref_inicial, 
	z.dt_ref_final,
	z.nr_sequencia,
	j.cd_cgc
union all
select decode(e.ie_brasileiro, 'S', '04', '05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(e.ie_brasileiro, 'N', j.nr_cartao_estrangeiro, null) cd_internacional,
	decode(e.ie_brasileiro, 'N', Elimina_Acentos(substr(j.nm_pessoa_fisica, 1, 43), null)) nm_estrangeiro,
	decode(e.ie_brasileiro, 'N', substr(p.sg_pais,1,2), null) sg_pais,
	decode(e.ie_brasileiro, 'N', e.ds_nacionalidade, null) ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_adiantamento) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,
	fis_obter_dados_iva(c.nr_sequencia, 1) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	to_number(null) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,	
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_19,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 6, z.dt_ref_inicial, fim_dia(z.dt_ref_final)) vl_campo_22,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_atualizacao) dt_baixa,
	null,
	null,
	z.nr_sequencia nr_seq_diot,
	'' cd_cgc,
	'4' ie_union
from	pessoa_fisica j,
	nota_fiscal c,
	operacao_nota b,
	nacionalidade e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_adiant y,
	pais p,
	fis_diot_controle z
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.cd_nacionalidade = e.cd_nacionalidade(+)
and	obter_dados_pf_pj(j.cd_pessoa_fisica, null, 'CPAIS') = p.cd_codigo_pais(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
--and	trunc(y.dt_atualizacao) between dt_inicio_p and dt_fim_p
and	y.dt_atualizacao between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
/*and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)*/
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_pessoa_fisica = x.cd_pessoa_fisica
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
--and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	((y.nr_seq_cheque_cp is null) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao is not null) = 1))*/
group by e.ie_brasileiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.nr_cartao_estrangeiro,
	j.nm_pessoa_fisica,
	p.sg_pais,
	e.ds_nacionalidade,
	c.nr_sequencia,
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_atualizacao),
	z.dt_ref_inicial, 
	z.dt_ref_final,
	z.nr_sequencia,
	''
union -------come�o union 5
	select  decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', '04', '05') ie_tipo_terceiro,
	'85',
	max(OBTER_DADOS_PF_PJ(mtf.cd_pessoa_fisica,mtf.cd_cgc,'RFC')) cd_rfc,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'CINT')) cd_internacional,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, elimina_acentos(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'N'))) nm_estrangeiro,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'SG')) sg_pais,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'N'))  ds_nacionalidade,
	0 vl_total_nota,
	0 vl_baixa,
	0 vl_mercadoria,
	(mtf.vl_transacao / 1.16) vl_campo_8,
	null vl_campo_9,
	null vl_campo_10,
	null vl_campo_11,
	null vl_campo_12,
	null vl_campo_13,
	null vl_campo_14,
	null vl_campo_15,
	null vl_campo_16,
	null vl_campo_17,
	null vl_campo_18,
	null vl_campo_19,
	null vl_campo_20,
	null vl_campo_21,
	null vl_campo_22,
	null nr_titulo,
	null nota,
	trunc(mtf.dt_transacao) dt_baixa,
	null,
	null,
	z.nr_sequencia nr_seq_diot,
	nvl(mtf.cd_cgc,mtf.cd_pessoa_fisica) cd_cgc,
	'5' ie_union
from 	movto_trans_financ mtf,
	fis_diot_controle z
where	trunc(mtf.dt_transacao) between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
and 	mtf.nr_seq_trans_financ in     (
					select  distinct 
						nr_seq_transacao
					from 	fis_diot_regras
					)
group by trunc(mtf.dt_transacao),
	mtf.vl_transacao,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', '04', '05'),
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'CINT')),
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, elimina_acentos(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'N'))) ,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'SG')),
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'N')),
	z.nr_sequencia,	-------final union 5,
	nvl(mtf.cd_cgc,mtf.cd_pessoa_fisica)
union    -- come�o union 6
	select  decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'S', '05', '04') ie_tipo_terceiro,
	'85' ie_tipo_operacao_mx,
	max(OBTER_DADOS_PF_PJ(null,c.cd_cgc_agencia,'RFC')) cd_rfc,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pf_pj(null,c.cd_cgc_agencia,'CINT')) cd_internacional,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, elimina_acentos(obter_dados_pf_pj(null,c.cd_cgc_agencia,'N'))) nm_estrangeiro,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'SG')) sg_pais,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'N'))  ds_nacionalidade,
	0 vl_total_nota,
	0 vl_baixa,
	0 vl_mercadoria,
	decode((select count(*) from fis_diot_controle_banc WHERE nr_seq_controle = z.nr_sequencia and nr_seq_trans_iva_acred  = mtf.nr_seq_trans_financ), 1, mtf.vl_transacao, 0)vl_campo_8,
	null vl_campo_9,
	null vl_campo_10,
	null vl_campo_11,
	null vl_campo_12,
	null vl_campo_13,
	null vl_campo_14,
	null vl_campo_15,
	null vl_campo_16,
	null vl_campo_17,
	null vl_campo_18,
	0  vl_campo_19,
	decode((select count(*) from fis_diot_controle_banc WHERE nr_seq_controle = z.nr_sequencia and nr_seq_trans_base_imp = mtf.nr_seq_trans_financ), 1, mtf.vl_transacao, 0)vl_campo_20,
	0 vl_campo_21,
	0 vl_campo_22,  
	null nr_titulo,
	null  nota,
	trunc(mtf.dt_transacao) dt_baixa,
	null,
	null,
	z.nr_sequencia nr_seq_diot,
	c.cd_cgc_agencia cd_cgc,
	'6' ie_union
from 	movto_trans_financ mtf,
	banco_estabelecimento_v b,
	agencia_bancaria c,
	fis_diot_controle z
where	trunc(mtf.dt_transacao) between z.dt_ref_inicial and fim_dia(z.dt_ref_final)
and	b.cd_banco = c.cd_banco	
and	b.nr_sequencia = mtf.nr_seq_banco		
and 	(mtf.nr_seq_trans_financ =	(select  nr_seq_trans_base_imp from fis_diot_controle_banc where nr_seq_controle = z.nr_sequencia)
      or mtf.nr_seq_trans_financ =  (select  nr_seq_trans_iva_acred from fis_diot_controle_banc where nr_seq_controle = z.nr_sequencia))
group by mtf.nr_seq_trans_financ,
	mtf.vl_transacao,
	trunc(mtf.dt_transacao),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'S', '05', '04'),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pf_pj(null,c.cd_cgc_agencia,'CINT')),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, elimina_acentos(obter_dados_pf_pj(null,c.cd_cgc_agencia,'N'))),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'SG')),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'N')),
	z.nr_sequencia,
	c.cd_cgc_agencia	-- fim union 6
	) y
group	by	y.ie_tipo_terceiro,
		y.ie_tipo_operacao_mx,
		y.cd_rfc,
		y.cd_internacional,
		y.nm_estrangeiro,
		y.sg_pais,
		y.ds_nacionalidade,
		y.nr_titulo,
		y.nota,
		y.dt_baixa,
		y.nr_lote_contabil,
		y.nr_seq_cheque_cp,
		y.nr_seq_diot,
		y.cd_cgc,
		y.ie_union
order by y.cd_rfc;
/