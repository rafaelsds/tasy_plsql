create or replace view lab_Sergio_Franco_Lote_v
as
select distinct
	'01' cd_tipo_registro, --laboratorio
	substr(lab_obter_dados_integracao(43,'CD'),1,4) cd_laboratorio,
	obter_dados_estab(b.cd_estabelecimento,'1') nm_laboratorio,
	sysdate dt_geracao,
	' ' ds_filler,
	null cd_paciente_lab_ext,
	1 cd_paciente_prescr,
	' ' ie_urgente,
	null dt_atendimento,
	null nr_crm,
	' ' nm_medico,
	' ' ds_observacao,
	' ' nm_paciente,
	' ' ie_sexo,
	null dt_nascimento,
	null qt_peso,
	null dt_menstruacao,
	' ' cd_medico_convenio,
	null nr_dependente,
	null nr_matricula,
	null dt_validade_carteira,
	' 'nm_empregado,
	' 'ds_orgao,
	null dt_validade_pedido,
	null dt_ultimo_pagto,
	' ' nr_dv,
	null cd_validacao,
	null cd_empresa,
	null cd_plano_convenio,
	null qt_documentos,
	null dt_idade,
	' ' nm_unidade_atendimento,
	' ' ds_visita,
	' ' uf_crm,
	' ' sg_conselho,
	' 'cd_exame_integracao,
	' 'nr_guia_convenio,
	' 'nr_volume_material,
	null dt_coleta,
	null hr_coleta,
	' 'ds_compl_material,
	' 'ie_refrigerado,
	' 'ie_usa_diu,
	' 'ie_terapia_hormonal,
	' 'ie_histerectomia,
	' 'qt_laminas,
	' 'nr_seq_admissao,
	null nr_recipiente,
	null qt_total_pedidos,
	null qt_total_exames,
	1 nr_prescricao,
	null nr_atendimento,
	' ' nm_medicamento,
	null qt_altura,
	null cd_setor_prescr,
	' ' nr_matricula_associado,
	' ' cd_validacao_associado,
	null dt_solicitacao,
	' ' nr_cpf,
	c.nr_seq_lote_externo,
	b.cd_estabelecimento
from    pessoa_fisica d,
	--lab_parametro j,
  	atend_categoria_convenio e,
	lab_lote_externo h,
	atend_paciente_unidade f,
	prescr_procedimento c,
   	prescr_medica b,
	atendimento_paciente a
where   a.nr_atendimento = b.nr_atendimento
and	a.nr_atendimento = e.nr_atendimento
and	a.nr_atendimento = f.nr_atendimento
and     d.cd_pessoa_fisica = b.cd_pessoa_fisica 
--and	j.cd_estabelecimento = b.cd_estabelecimento
--and	c.ie_status_atend = j.ie_status_envio
and	b.nr_prescricao  = c.nr_prescricao
and	c.nr_seq_lote_externo = h.nr_sequencia
and	f.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento,'A')
--and 	c.dt_integracao is null
and 	Obter_Equipamento_Exame(c.nr_seq_exame,null,'LSFRANCO') is not null
union  
select distinct
	'02' cd_tipo_registro, --paciente
	' ' cd_laboratorio,
	' ' nm_laboratorio,
	sysdate dt_geracao,
	' ' ds_filler,
	to_char(b.nr_controle_int_lab)  cd_paciente_lab_ext,
	b.nr_prescricao cd_paciente_prescr,
	' ' ie_urgente,
	a.dt_entrada dt_atendimento,
	substr(obter_crm_medico(b.cd_medico),1, 12) nr_crm,
	substr(obter_nome_pf(b.cd_medico),1,255) nm_medico,
	substr(b.ds_observacao,1,2000) ds_observacao,
	substr(d.nm_pessoa_fisica,1,255) nm_paciente,
	d.ie_sexo,
	d.dt_nascimento dt_nascimento,
	d.qt_peso,
	b.dt_mestruacao dt_menstruacao,
	b.cd_medico cd_medico_convenio,
	d.qt_dependente nr_dependente,
	e.cd_usuario_convenio nr_matricula,
	e.dt_validade_carteira dt_validade_carteira,
	decode(e.cd_dependente,0,' ',substr(obter_titular_convenio(e.nr_atendimento, e.cd_convenio, 'D'),1,60)) nm_empregado,
	' 'ds_orgao,
	b.dt_validade_prescr dt_validade_pedido,
	 e.dt_ultimo_pagto dt_ultimo_pagto,
	' ' nr_dv,
	e.cd_senha cd_validacao,
	e.cd_empresa,
	e.cd_plano_convenio,
	nvl(null,1) qt_documentos,
	obter_idade_pf(d.cd_pessoa_fisica,sysdate,'MAD')dt_idade,
	substr(obter_dados_unidade(f.cd_setor_atendimento,f.cd_unidade_basica,f.cd_unidade_compl,'NSUB'),1,255) nm_unidade_atendimento,
	a.ie_permite_visita ds_visita,
	substr(obter_dados_medico(b.cd_medico,'UFCRM'),1,2) uf_crm,
	substr(obter_dados_medico(b.cd_medico,'SGCRM'),1,10) sg_conselho,
	' 'cd_exame_integracao,
	' 'nr_guia_convenio,
	' 'nr_volume_material,
	null dt_coleta,
	null hr_coleta,
	' 'ds_compl_material,
	' 'ie_refrigerado,
	' 'ie_usa_diu,
	' 'ie_terapia_hormonal,
	' 'ie_histerectomia,
	' 'qt_laminas,
	' 'nr_seq_admissao,
	null nr_recipiente,
	null qt_total_pedidos,
	null qt_total_exames,
	b.nr_prescricao,
	b.nr_atendimento,
	' ' nm_medicamento,
	d.qt_altura_cm qt_altura,
	b.cd_setor_atendimento cd_setor_prescr,
	e.cd_usuario_convenio nr_matricula_associado,
	e.cd_senha cd_validacao_associado,
	b.dt_prescricao dt_solicitacao,
	d.nr_cpf,
	c.nr_seq_lote_externo,
	b.cd_estabelecimento
from   	pessoa_fisica d,
	--lab_parametro j,
  	atend_categoria_convenio e,
	lab_lote_externo h,
	atend_paciente_unidade f,
	prescr_procedimento c,
   	prescr_medica b,
	atendimento_paciente a
where   a.nr_atendimento = b.nr_atendimento
and	a.nr_atendimento = e.nr_atendimento
and	a.nr_atendimento = f.nr_atendimento
--and	j.cd_estabelecimento = b.cd_estabelecimento
--and	c.ie_status_atend = j.ie_status_envio
and     d.cd_pessoa_fisica  = b.cd_pessoa_fisica 
and	b.nr_prescricao  = c.nr_prescricao
and	f.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento,'A')
and	c.nr_seq_lote_externo = h.nr_sequencia
--and 	c.dt_integracao is null
and 	Obter_Equipamento_Exame(c.nr_seq_exame,null,'LSFRANCO') is not null
union 
select distinct
	'03' cd_tipo_registro, --exames
	' ' cd_laboratorio,
	' ' nm_laboratorio,
	sysdate dt_geracao,
	' ' ds_filler,
	' ' cd_paciente_lab_ext,
	c.nr_prescricao cd_paciente_prescr,
	c.ie_urgencia ie_urgente,
	a.dt_entrada  dt_atendimento,
	' ' nr_crm,
	substr(obter_nome_pf(b.cd_medico),1,255) nm_medico,
	substr(b.ds_observacao,1,2000) ds_observacao,
	substr(d.nm_pessoa_fisica,1,255) nm_paciente,
	d.ie_sexo,
	d.dt_nascimento dt_nascimento,
	d.qt_peso,
	b.dt_mestruacao dt_menstruacao,
	b.cd_medico cd_medico_convenio,
	d.qt_dependente nr_dependente,
	e.cd_usuario_convenio nr_matricula,
	 e.dt_validade_carteira dt_validade_carteira,
	' 'nm_empregado,
	' 'ds_orgao,
	null dt_validade_pedido,
	 e.dt_ultimo_pagto dt_ultimo_pagto,
	' ' nr_dv,
	e.cd_senha cd_validacao,
	e.cd_empresa,
	e.cd_plano_convenio,
	nvl(null,1) qt_documentos,
	obter_idade_pf(d.cd_pessoa_fisica,sysdate,'MAD')dt_idade,
	substr(obter_dados_unidade(f.cd_setor_atendimento,f.cd_unidade_basica,f.cd_unidade_compl,'NSUB'),1,255) nm_unidade_atendimento,
	a.ie_permite_visita ds_visita,
	substr(obter_dados_medico(b.cd_medico,'UFCRM'),1,2) uf_crm,
	substr(obter_dados_medico(b.cd_medico,'SGCRM'),1,10) sg_conselho,
	nvl(g.cd_exame_integracao,cd_exame) cd_exame_integracao,
	' 'nr_guia_convenio,
	' 'nr_volume_material,
	 c.dt_coleta  dt_coleta,
	 c.dt_coleta  hr_coleta,
	' 'ds_compl_material,
	' 'ie_refrigerado,
	' 'ie_usa_diu,
	' 'ie_terapia_hormonal,
	' 'ie_histerectomia,
	' 'qt_laminas,
	' 'nr_seq_admissao,
	c.nr_controle_exame nr_recipiente,
	null qt_total_pedidos,
	null qt_total_exames,
	b.nr_prescricao,
	b.nr_atendimento,
	' ' nm_medicamento,
	null qt_altura,
	b.cd_setor_atendimento cd_setor_prescr,
	' ' nr_matricula_associado,
	' ' cd_validacao_associado,
	null dt_solicitacao,
	' ' nr_cpf,
	c.nr_seq_lote_externo,
	b.cd_estabelecimento
from    pessoa_fisica d,
	--lab_parametro j,
  	atend_categoria_convenio e,
	lab_lote_externo h,
	atend_paciente_unidade f,
	exame_laboratorio g,
	prescr_procedimento c,
   	prescr_medica b,
	atendimento_paciente a
where   a.nr_atendimento = b.nr_atendimento
and	a.nr_atendimento = e.nr_atendimento
and	a.nr_atendimento = f.nr_atendimento
and	b.nr_prescricao  = c.nr_prescricao
--and	j.cd_estabelecimento = b.cd_estabelecimento
--and	c.ie_status_atend = j.ie_status_envio
and     d.cd_pessoa_fisica = b.cd_pessoa_fisica 
and	c.nr_seq_exame 	  = g.nr_seq_exame
and	f.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento,'A')
and	c.nr_seq_lote_externo = h.nr_sequencia
--and 	c.dt_integracao is null
and 	Obter_Equipamento_Exame(c.nr_seq_exame,null,'LSFRANCO') is not null
union 	
select distinct
	'04' cd_tipo_registro, --finalizador
	' ' cd_laboratorio,
	' ' nm_laboratorio,
	sysdate dt_geracao,
	' ' ds_filler,
	' ' cd_paciente_lab_ext,
	999999999 cd_paciente_prescr,
	' ' ie_urgente,
	null dt_atendimento,
	' ' nr_crm,
	null nm_medico,
	null ds_observacao,
	null nm_paciente,
	null ie_sexo,
	null dt_nascimento,
	null qt_peso,
	null dt_menstruacao,
	null cd_medico_convenio,
	null nr_dependente,
	null nr_matricula,
	null dt_validade_carteira,
	' 'nm_empregado,
	' 'ds_orgao,
	null dt_validade_pedido,
	null dt_ultimo_pagto,
	' ' nr_dv,
	null cd_validacao,
	null cd_empresa,
	null cd_plano_convenio,
	null qt_documentos,
	null dt_idade,
	' ' nm_unidade_atendimento,
	null ds_visita,
	' ' uf_crm,
	' ' sg_conselho,
	' 'cd_exame_integracao,
	' 'nr_guia_convenio,
	' 'nr_volume_material,
	null dt_coleta,
	null hr_coleta,
	' 'ds_compl_material,
	' 'ie_refrigerado,
	' 'ie_usa_diu,
	' 'ie_terapia_hormonal,
	' 'ie_histerectomia,
	' 'qt_laminas,
	' 'nr_seq_admissao,
	null nr_recipiente,
	lab_obter_tot_pedido_lsf(c.nr_seq_lote_externo, 'P') qt_total_pedidos,
	lab_obter_tot_pedido_lsf(c.nr_seq_lote_externo, 'E') qt_total_exames,
	--0 qt_total_pedidos,
	--0 qt_total_exames,
	999999999 nr_prescricao,
	null nr_atendimento,
	' ' nm_medicamento,
	null qt_altura,
	null cd_setor_prescr,
	' ' nr_matricula_associado,
	' ' cd_validacao_associado,
	null dt_solicitacao,
	' ' nr_cpf,
	c.nr_seq_lote_externo,
	b.cd_estabelecimento
from    pessoa_fisica d,
	--lab_parametro j,
  	atend_categoria_convenio e,
	lab_lote_externo h,
	atend_paciente_unidade f,
	prescr_procedimento c,
   	prescr_medica b,
	atendimento_paciente a
where   a.nr_atendimento = b.nr_atendimento
and	a.nr_atendimento = e.nr_atendimento
and	a.nr_atendimento = f.nr_atendimento
and	 b.nr_prescricao  = c.nr_prescricao
--and	j.cd_estabelecimento = b.cd_estabelecimento
--and	c.ie_status_atend = j.ie_status_envio
and     d.cd_pessoa_fisica  = b.cd_pessoa_fisica 
and	f.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento,'A')
and	c.nr_seq_lote_externo = h.nr_sequencia
--and 	c.dt_integracao is null
and 	Obter_Equipamento_Exame(c.nr_seq_exame,null,'LSFRANCO') is not null
union
select distinct 
	'05' cd_tipo_registro, --medicamento
	' ' cd_laboratorio,
	' ' nm_laboratorio,
	null dt_geracao,
	' ' ds_filler,
	' ' cd_paciente_lab_ext,
	null cd_paciente_prescr,
	' ' ie_urgente,
	null  dt_atendimento,
	' ' nr_crm,
	' ' nm_medico,
	' ' ds_observacao,
	' ' nm_paciente,
	' ' ie_sexo,
	null dt_nascimento,
	null qt_peso,
	null dt_menstruacao,
	' ' cd_medico_convenio,
	null nr_dependente,
	null nr_matricula,
	null dt_validade_carteira,
	' 'nm_empregado,
	' 'ds_orgao,
	null dt_validade_pedido,
	null dt_ultimo_pagto,
	' ' nr_dv,
	' ' cd_validacao,
	null cd_empresa,
	null cd_plano_convenio,
	null qt_documentos,
	' ' dt_idade,
	' ' nm_unidade_atendimento,
	' ' ds_visita,
	' ' uf_crm,
	' ' sg_conselho,
	' 'cd_exame_integracao,
	' 'nr_guia_convenio,
	' 'nr_volume_material,
	null dt_coleta,
	null hr_coleta,
	' 'ds_compl_material,
	' 'ie_refrigerado,
	' 'ie_usa_diu,
	' 'ie_terapia_hormonal,
	' 'ie_histerectomia,
	' 'qt_laminas,
	' 'nr_seq_admissao,
	null nr_recipiente,
	null qt_total_pedidos,
	null qt_total_exames,
	b.nr_prescricao,
	b.nr_atendimento,
	' ' nm_medicamento,
	null qt_altura,
	null cd_setor_prescr,
	' ' nr_matricula_associado,
	' ' cd_validacao_associado,
	null dt_solicitacao,
	' ' nr_cpf,
	c.nr_seq_lote_externo,
	b.cd_estabelecimento
from 	pessoa_fisica d,
	lab_parametro j,
	atend_categoria_convenio e,
	atend_paciente_unidade f,
	prescr_procedimento c,
	prescr_medica b,
	atendimento_paciente a
where  	a.nr_atendimento = b.nr_atendimento
and	1=2
and	a.nr_atendimento = e.nr_atendimento
and	a.nr_atendimento = f.nr_atendimento
and	  b.nr_prescricao  = c.nr_prescricao
and	j.cd_estabelecimento = b.cd_estabelecimento
and	c.ie_status_atend = j.ie_status_envio
and    	d.cd_pessoa_fisica  = b.cd_pessoa_fisica 
and	f.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento,'A')
--and	c.nr_seq_lote_externo = h.nr_sequencia
--and 	c.dt_integracao is null
and 	Obter_Equipamento_Exame(c.nr_seq_exame,null,'LSFRANCO') is not null;
/