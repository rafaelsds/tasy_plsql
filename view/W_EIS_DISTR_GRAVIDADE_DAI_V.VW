create or replace view w_eis_distr_gravidade_DAI_v  as 
select	distinct
	Obter_Setor_Atendimento(b.nr_atendimento) cd_setor,
	SUBSTR(obter_nome_setor(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Setor_Atendimento,	
	TRUNC(b.dt_Evento) dt_evento,
	SUBSTR(obter_sexo_pf(OBTER_PESSOA_ATENDIMENTO(b.nr_atendimento,'C'),'D'),1,20) ds_sexo,
	SUBSTR(Obter_desc_ficha_DAI(null,'E',b.dt_atualizacao_nrec,obter_data_nascto_pf(OBTER_PESSOA_ATENDIMENTO(b.nr_atendimento,'C'))),1,80) ie_faixa_etaria,
	SUBSTR(obter_valor_dominio(4918,a.ie_gravidade),1,255)  ds_gravidade,	
	b.nr_atendimento NA
from	qua_evento_paciente b,
	qua_tipo_evento d,
	qua_evento f,
	qua_evento_dermatite a
where	b.dt_inativacao is null
and	a.nr_seq_evento = b.nr_sequencia
and	d.ie_tipo_evento = 'DAI'
and	d.nr_sequencia = f.nr_Seq_tipo
and	b.nr_Seq_evento = f.nr_sequencia
and	a.ie_origem_dermatite = 'U';
/