Create or replace view pls_pendencia_v
as
select	1 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Dados inconsistentes' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_pessoa_fisica,
		count(1) qt_pendencia
	from	pls_cad_inconsist_pessoa	b,
		pls_inconsistencia_pessoa	a
	where	a.nr_seq_inconsistencia		= b.nr_sequencia
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica) a
where	a.cd_pessoa_fisica is not null
union
select	1 ie_pendencia,
	'' cd_pessoa_fisica,
	a.cd_cgc,
	'Dados inconsistentes' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_cgc,
		count(1) qt_pendencia
	from	pls_cad_inconsist_pessoa	b,
		pls_inconsistencia_pessoa	a
	where	a.nr_seq_inconsistencia		= b.nr_sequencia
	and	a.cd_cgc is not null
	group by a.cd_cgc) a
where	a.cd_cgc is not null
union
select	2 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Mensalidades' ds_pendencia,
	a.qt_pendencia,
	ie_parametro
from	(select	a.cd_pessoa_fisica,
		count(1) qt_pendencia,
		0 ie_parametro
	from	titulo_receber		c,
		pls_mensalidade		b,
		pls_contrato_pagador	a
	where	a.nr_sequencia		= b.nr_seq_pagador
	and	b.nr_sequencia		= c.nr_seq_mensalidade
	and	c.dt_liquidacao is null
	and	trunc(c.dt_pagamento_previsto,'dd')	< trunc(sysdate,'dd')
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica
	union
	select	a.cd_pessoa_fisica,
		count(1) qt_pendencia,
		1 ie_parametro
	from	titulo_receber		d,
		pls_mensalidade		c,
		pls_mensalidade_segurado	b,
		pls_segurado		a
	where	a.nr_sequencia		= b.nr_seq_segurado
	and	b.nr_seq_mensalidade	= c.nr_sequencia
	and	c.nr_sequencia		= d.nr_seq_mensalidade
	and	d.dt_liquidacao is null
	and	trunc(d.dt_pagamento_previsto,'dd')	< trunc(sysdate,'dd')
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica
	union
	select	a.cd_pf_estipulante cd_pessoa_fisica,
		count(1) qt_pendencia,
		1 ie_parametro
	from	titulo_receber		d,
		pls_mensalidade		c,
		pls_contrato_pagador	b,
		pls_contrato		a
	where	c.nr_seq_pagador	= b.nr_sequencia
	and	b.nr_seq_contrato	= a.nr_sequencia
	and	c.nr_sequencia		= d.nr_seq_mensalidade
	and	d.dt_liquidacao is null
	and	trunc(d.dt_pagamento_previsto,'dd')	< trunc(sysdate,'dd')
	and	a.cd_pf_estipulante is not null
	group by a.cd_pf_estipulante) a
where	a.cd_pessoa_fisica is not null	
union
select	2 ie_pendencia,
	'' cd_pessoa_fisica,
	a.cd_cgc,
	'Mensalidades' ds_pendencia,
	a.qt_pendencia,
	ie_parametro
from	(select	a.cd_cgc,
		count(1) qt_pendencia,
		0 ie_parametro
	from	titulo_receber		c,
		pls_mensalidade		b,
		pls_contrato_pagador	a
	where	a.nr_sequencia		= b.nr_seq_pagador
	and	b.nr_sequencia		= c.nr_seq_mensalidade
	and	c.dt_liquidacao is null
	and	trunc(c.dt_pagamento_previsto,'dd')	< trunc(sysdate,'dd')
	and	a.cd_cgc is not null
	group by a.cd_cgc
	union
	select	a.cd_cgc_estipulante cd_cgc,
		count(1) qt_pendencia,
		1 ie_parametro
	from	titulo_receber		d,
		pls_mensalidade		c,
		pls_contrato_pagador	b,
		pls_contrato		a
	where	c.nr_seq_pagador	= b.nr_sequencia
	and	b.nr_seq_contrato	= a.nr_sequencia
	and	c.nr_sequencia		= d.nr_seq_mensalidade
	and	d.dt_liquidacao is null
	and	trunc(d.dt_pagamento_previsto,'dd')	< trunc(sysdate,'dd')
	and	a.cd_cgc_estipulante is not null
	group by a.cd_cgc_estipulante) a
where	a.cd_cgc is not null	
union
select	3 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Autorizações' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	b.cd_pessoa_fisica,
		count(1) qt_pendencia
	from 	pls_segurado	b,
		pls_guia_plano	a
	where 	a.nr_seq_segurado	= b.nr_sequencia
	and	a.ie_status 		= '2'
	and	b.cd_pessoa_fisica is not null
	group by b.cd_pessoa_fisica) a
where	a.cd_pessoa_fisica is not null	
union
select	4 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Boletim de ocorrência' ds_pendencia,
	sum(1) qt_pendencia,
	2 ie_parametro
from	(select	a.cd_pessoa_fisica,
		a.nr_sequencia
	from	sac_resp_bol_ocor	b,
		sac_boletim_ocorrencia	a
	where	a.nr_sequencia 		= b.nr_seq_bo
	and	b.ie_status not in ('E','S')
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica, a.nr_sequencia) a
where	a.cd_pessoa_fisica is not null	
group by a.cd_pessoa_fisica
union
select	4 ie_pendencia,
	'' cd_pessoa_fisica,
	a.cd_cgc,
	'Boletim de ocorrência' ds_pendencia,
	sum(1) qt_pendencia,
	2 ie_parametro
from	(select	a.cd_cgc,
		a.nr_sequencia
	from	sac_resp_bol_ocor	b,
		sac_boletim_ocorrencia	a
	where	a.nr_sequencia 		= b.nr_seq_bo
	and	b.ie_status not in ('E','S')
	and	a.cd_cgc is not null
	group by a.cd_cgc, a.nr_sequencia) a
where	a.cd_cgc is not null	
group by a.cd_cgc
union
select	5 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Atendimentos' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_pessoa_fisica,
		count(1) qt_pendencia
	from	pls_atendimento	a
	where	ie_status	= 'P'
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica) a
where	a.cd_pessoa_fisica is not null	
union
select	5 ie_pendencia,
	'' cd_pessoa_fisica,
	a.cd_cgc,
	'Atendimentos' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_cgc,
		count(1) qt_pendencia
	from	pls_atendimento	a
	where	ie_status	= 'P'
	and	a.cd_cgc is not null
	group by a.cd_cgc) a
where	a.cd_cgc is not null
union
select	6 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Notificações' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_pessoa_fisica,
		count(1) qt_pendencia
	from	pls_notificacao_atend	c,
		pls_auditoria		b,
		pls_segurado		a
	where	c.nr_seq_auditoria = b.nr_sequencia
	and	b.nr_seq_segurado = a.nr_sequencia
	and	c.ie_status = 'AG'
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica) a
where	a.cd_pessoa_fisica is not null
union
select	7 ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Liminares' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_pessoa_fisica,
		count(1) qt_pendencia
	from	processo_judicial_liminar	b,
		pls_segurado			a
	where	b.nr_seq_segurado 	= a.nr_sequencia
	and	b.ie_estagio 		= 2
	and	sysdate between trunc(dt_inicio_validade) and fim_dia(nvl(dt_fim_validade,sysdate))
	and	a.cd_pessoa_fisica is not null
	group by a.cd_pessoa_fisica) a
where	a.cd_pessoa_fisica is not null
union
select	8 ie_ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Cumprimento de carência' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	a.cd_pessoa_fisica,
		sum(qt_pendencia) qt_pendencia
	from	(
		select	b.cd_pessoa_fisica,
			count(1) qt_pendencia
		from	pls_segurado	b,
			pls_carencia	a
		where	a.nr_seq_segurado	= b.nr_sequencia
		and	a.ie_cpt		= 'N'
		and	b.ie_situacao_atend	= 'A'
		and	sysdate <= nvl(a.dt_inicio_vigencia,b.dt_inclusao_operadora) + a.qt_dias
		and	b.cd_pessoa_fisica is not null
		group by b.cd_pessoa_fisica
		union all
		select	b.cd_pessoa_fisica,
			count(1) qt_pendencia
		from	pls_segurado	b,
			pls_carencia	a,
			pls_contrato	c
		where	a.nr_seq_contrato	= c.nr_sequencia
		and	b.nr_seq_contrato	= c.nr_sequencia
		and	a.ie_cpt		= 'N'
		and	b.ie_situacao_atend	= 'A'
		and	sysdate <= nvl(a.dt_inicio_vigencia,b.dt_inclusao_operadora) + a.qt_dias
		and	not exists	(	select	1
						from	pls_carencia	x
						where	x.nr_seq_segurado	= b.nr_sequencia
						and	x.ie_cpt		= 'N')
		and	b.cd_pessoa_fisica is not null
		group by b.cd_pessoa_fisica
		union all
		select	b.cd_pessoa_fisica,
			count(1) qt_pendencia
		from	pls_segurado	b,
			pls_carencia	a,
			pls_plano	c
		where	a.nr_seq_plano		= c.nr_sequencia
		and	b.nr_seq_plano		= c.nr_sequencia
		and	a.ie_cpt		= 'N'
		and	b.ie_situacao_atend	= 'A'
		and	sysdate <= nvl(a.dt_inicio_vigencia,b.dt_inclusao_operadora) + a.qt_dias
		and	not exists	(	select	1
						from	pls_carencia	x
						where	x.nr_seq_segurado	= b.nr_sequencia
						and	x.ie_cpt		= 'N')
		and	not exists	(	select	1
						from	pls_carencia	x
						where	x.nr_seq_contrato	= b.nr_seq_contrato
						and	x.ie_cpt		= 'N')
		and	b.cd_pessoa_fisica is not null
		group by b.cd_pessoa_fisica
		union all
		select	b.cd_pessoa_fisica,
			count(1) qt_pendencia
		from	pls_carencia_sca_v	a,
			pls_segurado		b
		where	a.nr_seq_segurado	= b.nr_sequencia
		and	b.ie_situacao_atend	= 'A'
		and	sysdate <= a.dt_inicio_vigencia + a.qt_dias
		and	b.cd_pessoa_fisica is not null
		group by b.cd_pessoa_fisica) a
		group by a.cd_pessoa_fisica
		) a
union
select	9 ie_ie_pendencia,
	a.cd_pessoa_fisica,
	'' cd_cgc,
	'Cumprimento de CPT' ds_pendencia,
	a.qt_pendencia,
	0 ie_parametro
from	(select	b.cd_pessoa_fisica,
		count(1) qt_pendencia
	from	pls_segurado	b,
		pls_carencia	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	a.ie_cpt		= 'S'
	and	sysdate <= nvl(a.dt_inicio_vigencia,b.dt_inclusao_operadora) + a.qt_dias
	and	b.cd_pessoa_fisica is not null
	group by b.cd_pessoa_fisica) a	;
/
