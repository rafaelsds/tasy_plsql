CREATE OR REPLACE VIEW reg_diff_documents_v
AS
SELECT	nr_sequencia,
	ds_title,
	obter_desc_expressao(cd_exp_object_type) ds_object_type,
	ie_action_type,
	obter_desc_expressao(
		DECODE(ie_action_type,
			'U', 283380,
			'I', 284403,
			'D', 289757)) ds_action_type,
	ie_doc_type,
	ie_checked,
	reg_find_artifact_responsible(ie_doc_type, nr_sequencia) ds_responsible
FROM (
	SELECT	NVL(h.nr_seq_pr, i.nr_sequencia) nr_sequencia,
		NVL(h.ds_title, i.ds_title) ds_title,
		784823 cd_exp_object_type,
		CASE
			WHEN h.dt_atualizacao_nrec IS NULL
				THEN 'I'
			WHEN i.dt_atualizacao_nrec IS NULL
				THEN 'D'
			WHEN h.dt_atualizacao <> i.dt_atualizacao
				THEN 'U'
			ELSE	'N'
		END ie_action_type,
		'PR' ie_doc_type,
		MAX(h.nr_revision),
		NULL ie_checked
	FROM	reg_pr_history h
	FULL OUTER JOIN reg_product_requirement i ON i.nr_sequencia = h.nr_seq_pr
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY NVL(h.nr_seq_pr, i.nr_sequencia), NVL(h.ds_title, i.ds_title), CASE WHEN h.dt_atualizacao_nrec IS NULL THEN 'I' WHEN i.dt_atualizacao_nrec IS NULL THEN 'D' WHEN h.dt_atualizacao <> i.dt_atualizacao THEN 'U' ELSE	'N' END, 784823, 'PR', NULL
	UNION
	SELECT	NVL(h.nr_seq_area_customer, i.nr_sequencia) nr_sequencia,
		NVL(h.ds_area, i.ds_area) ds_title,
		291189 cd_exp_object_type,
		CASE
			WHEN h.dt_atualizacao_nrec IS NULL
				THEN 'I'
			WHEN i.dt_atualizacao_nrec IS NULL
				THEN 'D'
			WHEN h.dt_atualizacao <> i.dt_atualizacao
				THEN 'U'
			ELSE	'N'
		END ie_action_type,
		'AR' ie_doc_type,
		MAX(h.nr_revision),
		NULL ie_checked
	FROM	reg_area_history h
	FULL OUTER JOIN reg_area_customer i ON i.nr_sequencia = h.nr_seq_area_customer
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY NVL(h.nr_seq_area_customer, i.nr_sequencia), NVL(h.ds_area, i.ds_area), CASE WHEN h.dt_atualizacao_nrec IS NULL THEN 'I' WHEN i.dt_atualizacao_nrec IS NULL THEN 'D' WHEN h.dt_atualizacao <> i.dt_atualizacao THEN 'U' ELSE	'N' END, 291189, 'AR', NULL
	UNION
	SELECT	NVL(h.nr_seq_cr, i.nr_sequencia) nr_sequencia,
		NVL(h.ds_title, i.ds_title) ds_title,
		642391 cd_exp_object_type,
		CASE
			WHEN h.dt_atualizacao_nrec IS NULL
				THEN 'I'
			WHEN i.dt_atualizacao_nrec IS NULL
				THEN 'D'
			WHEN h.dt_atualizacao <> i.dt_atualizacao
				THEN 'U'
			ELSE	'N'
		END ie_action_type,
		'CR' ie_doc_type,
		MAX(h.nr_revision),
		NULL ie_checked
	FROM	reg_cr_history h
	FULL OUTER JOIN reg_customer_requirement i ON i.nr_sequencia = h.nr_seq_cr
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY NVL(h.nr_seq_cr, i.nr_sequencia), NVL(h.ds_title, i.ds_title), CASE WHEN h.dt_atualizacao_nrec IS NULL THEN 'I' WHEN i.dt_atualizacao_nrec IS NULL THEN 'D' WHEN h.dt_atualizacao <> i.dt_atualizacao THEN 'U' ELSE	'N' END, 642391, 'CR', NULL
	UNION
	SELECT	NVL(h.nr_seq_feature, i.nr_sequencia) nr_sequencia,
		NVL(h.ds_feature, i.ds_feature) ds_title,
		652335 cd_exp_object_type,
		CASE
			WHEN h.dt_atualizacao_nrec IS NULL
				THEN 'I'
			WHEN i.dt_atualizacao_nrec IS NULL
				THEN 'D'
			WHEN h.dt_atualizacao <> i.dt_atualizacao
				THEN 'U'
			ELSE	'N'
		END ie_action_type,
		'FE' ie_doc_type,
		MAX(h.nr_revision),
		NULL ie_checked
	FROM	reg_features_history h
	FULL OUTER JOIN reg_features_customer i ON i.nr_sequencia = h.nr_seq_feature
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY NVL(h.nr_seq_feature, i.nr_sequencia), NVL(h.ds_feature, i.ds_feature), CASE WHEN h.dt_atualizacao_nrec IS NULL THEN 'I' WHEN i.dt_atualizacao_nrec IS NULL THEN 'D' WHEN h.dt_atualizacao <> i.dt_atualizacao THEN 'U' ELSE	'N' END, 652335, 'FE', NULL
	UNION
	SELECT	NVL(h.nr_seq_int_use, i.nr_sequencia) nr_sequencia,
		NVL(h.ds_intencao_uso, i.ds_intencao_uso) ds_title,
		773802 cd_exp_object_type,
		CASE
			WHEN h.dt_atualizacao_nrec IS NULL
				THEN 'I'
			WHEN i.dt_atualizacao_nrec IS NULL
				THEN 'D'
			WHEN h.dt_atualizacao <> i.dt_atualizacao
				THEN 'U'
			ELSE	'N'
		END ie_action_type,
		'IU' ie_doc_type,
		MAX(h.nr_revision),
		NULL ie_checked
	FROM	reg_int_use_history h
	FULL OUTER JOIN reg_intencao_uso i ON i.nr_sequencia = h.nr_seq_int_use
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY NVL(h.nr_seq_int_use, i.nr_sequencia), NVL(h.ds_intencao_uso, i.ds_intencao_uso), CASE WHEN h.dt_atualizacao_nrec IS NULL THEN 'I' WHEN i.dt_atualizacao_nrec IS NULL THEN 'D' WHEN h.dt_atualizacao <> i.dt_atualizacao THEN 'U' ELSE	'N' END, 773802, 'FE', NULL
	UNION
	SELECT	NVL(h.nr_seq_tc, i.nr_sequencia) nr_sequencia,
		NVL(H.ds_descricao, i.ds_descricao) ds_title,
		284667 cd_exp_object_type,
		CASE
			WHEN h.dt_atualizacao_nrec IS NULL
				THEN 'I'
			WHEN i.dt_atualizacao_nrec IS NULL
				THEN 'D'
			WHEN h.dt_atualizacao <> i.dt_atualizacao
				THEN 'U'
			ELSE reg_get_tc_item_diff(i.nr_sequencia)
		END ie_action_type,
		'TC' ie_doc_type,
		MAX(h.nr_revision),
		NULL ie_checked
	FROM	reg_tc_history h
	FULL OUTER JOIN reg_caso_teste i ON i.nr_sequencia = h.nr_seq_tc
	WHERE	NVL(h.cd_versao, (SELECT MAX(ds_version) FROM reg_verion_scheduler WHERE ie_major = 'S')) >= (
					SELECT	MAX(ds_version)
					FROM	reg_verion_scheduler
					WHERE	ie_major = 'S')
	GROUP BY NVL(h.nr_seq_tc, i.nr_sequencia), NVL(H.ds_descricao, i.ds_descricao),
	CASE WHEN h.dt_atualizacao_nrec IS NULL THEN 'I'
		WHEN i.dt_atualizacao_nrec IS NULL THEN 'D'
		WHEN h.dt_atualizacao <> i.dt_atualizacao THEN 'U'
		ELSE reg_get_tc_item_diff(i.nr_sequencia)
	END, 284667, 'TC', NULL
)
WHERE ie_action_type <> 'N';
/