create or replace view centro_custo_v3 as
select  a.*,
    nvl((select max('S')
    from    centro_motivo_desconto x
    where   x.cd_centro_custo   = a.cd_centro_custo),'N') ie_motivo_desconto,
    nvl((select max('S')
    from    motivo_desconto_cc x
    where   x.cd_centro_custo   = a.cd_centro_custo),'N') ie_motivo_desc_titulo,
    substr(obter_desc_centro_custo(a.cd_centro_custo_ref),1,255) ds_centro_custo_ref
from    centro_custo a;
/