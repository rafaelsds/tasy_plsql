CREATE OR REPLACE VIEW HSP_TITULO_RECEBER_MXM_V2 AS 
select	ie_tipo,
	cd_pessoa,
	nr_titulo,
	nr_nota_fiscal,
	cd_empresa_emitente,
	cd_filial,
	cd_empresa_pagadora,
	ie_tipo_titulo,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	cd_moeda,
        lpad(replace(replace(campo_mascara_virgula(sum(vl_titulo)),'.',''),'.',','),11,' ') vl_titulo,
	cd_tipo_cobranca,
	ds_observacao,
	cd_grupo_receb,
	cd_centro_custo,
        cd_pessoa_fisica
from
(	/* procedimentos*/
	select	'PR' ie_tipo,
		decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')) cd_pessoa,
		to_char(a.nr_titulo) nr_titulo,
		substr(x.nr_nota_fiscal,1,20) nr_nota_fiscal,
		'1232' cd_empresa_emitente,
		'00' cd_filial,
		'1232' cd_empresa_pagadora,
		'NF' ie_tipo_titulo,
		to_char(a.dt_emissao,'ddmmyyyy') dt_emissao,
		to_char(a.dt_vencimento,'ddmmyyyy') dt_vencimento_original,
		to_char(a.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento_atual,
		'R' cd_moeda,
		--lpad(replace(replace(campo_mascara_virgula(sum(c.vl_procedimento)),'.',''),',','.'),11,' ') vl_titulo,
		sum(c.vl_procedimento) vl_titulo,		
		'CT' cd_tipo_cobranca,
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46) ds_observacao,
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)) cd_grupo_receb,
		10140 cd_centro_custo,
                x.cd_pessoa_fisica cd_pessoa_fisica
	from	nota_fiscal x,
		convenio z,
		conta_paciente b,
		procedimento_paciente c,
		titulo_receber a
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	x.nr_sequencia		= a.nr_seq_nf_saida
	and	z.cd_convenio		= b.cd_convenio_parametro
	and	nvl(c.nr_seq_proc_pacote, c.nr_sequencia) = c.nr_sequencia
	and	c.cd_motivo_exc_conta is null
	and	a.dt_integracao_externa is null
	and	x.cd_operacao_nf	= 25
	and	c.vl_procedimento	<> 0
	group by decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')),
		to_char(a.nr_titulo),
		substr(x.nr_nota_fiscal,1,20),
		to_char(a.dt_emissao,'ddmmyyyy'),
		to_char(a.dt_vencimento,'ddmmyyyy'),
		to_char(a.dt_pagamento_previsto,'ddmmyyyy'),
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46),
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)),
                x.cd_pessoa_fisica,
		a.vl_titulo,
		x.vl_total_nota
	union all
	/* Materiais */
	select	'PR' ie_tipo,
		decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')) cd_pessoa,
		to_char(a.nr_titulo) nr_titulo,
		substr(x.nr_nota_fiscal,1,20) nr_nota_fiscal,
		'1232' cd_empresa_emitente,
		'00' cd_filial,
		'1232' cd_empresa_pagadora,
		'NF' ie_tipo_titulo,
		to_char(a.dt_emissao,'ddmmyyyy') dt_emissao,
		to_char(a.dt_vencimento,'ddmmyyyy') dt_vencimento_original,
		to_char(a.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento_atual,
		'R' cd_moeda,
		--lpad(replace(replace(campo_mascara_virgula(sum(c.vl_material)),'.',''),',','.'),11,' ') vl_titulo,
		sum(c.vl_material) vl_titulo,
		'CT' cd_tipo_cobranca,
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46) ds_observacao,
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)) cd_grupo_receb,
		10140 cd_centro_custo,
                x.cd_pessoa_fisica cd_pessoa_fisica
	from	nota_fiscal x,
		convenio z,
		conta_paciente b,
		material_atend_paciente c,
		titulo_receber a
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	x.nr_sequencia		= a.nr_seq_nf_saida
	and	z.cd_convenio		= b.cd_convenio_parametro
	and	c.nr_seq_proc_pacote is null
	and	c.cd_motivo_exc_conta is null
	and	a.dt_integracao_externa is null
	and	x.cd_operacao_nf	= 25
	and	c.vl_material	<> 0
	group by decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')),
		to_char(a.nr_titulo),
		substr(x.nr_nota_fiscal,1,20),
		to_char(a.dt_emissao,'ddmmyyyy'),
		to_char(a.dt_vencimento,'ddmmyyyy'),
		to_char(a.dt_pagamento_previsto,'ddmmyyyy'),
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46),
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)),
                x.cd_pessoa_fisica,
		a.vl_titulo,
		x.vl_total_nota
)
group by
	ie_tipo,
	cd_pessoa,
	nr_titulo,
	nr_nota_fiscal,
	cd_empresa_emitente,
	cd_filial,
	cd_empresa_pagadora,
	ie_tipo_titulo,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	cd_moeda,
	cd_tipo_cobranca,
	ds_observacao,
	cd_grupo_receb,
	cd_centro_custo,
        cd_pessoa_fisica
union
/* titulos de  protocolos */
select	ie_tipo,
	cd_pessoa,
	nr_titulo,
	nr_nota_fiscal,
	cd_empresa_emitente,
	cd_filial,
	cd_empresa_pagadora,
	ie_tipo_titulo,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	cd_moeda,
	lpad(replace(replace(campo_mascara_virgula(sum(vl_titulo)),'.',''),'.',','),11,' ') vl_titulo,
	cd_tipo_cobranca,
	ds_observacao,
	cd_grupo_receb,
	cd_centro_custo,
        cd_pessoa_fisica
from
(	/* procedimentos*/
	select	'PR' ie_tipo,
		decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')) cd_pessoa,
		to_char(a.nr_titulo) nr_titulo,
		substr(x.nr_nota_fiscal,1,20) nr_nota_fiscal,
		'1232' cd_empresa_emitente,
		'00' cd_filial,
		'1232' cd_empresa_pagadora,
		'NF' ie_tipo_titulo,
		to_char(a.dt_emissao,'ddmmyyyy') dt_emissao,
		to_char(a.dt_vencimento,'ddmmyyyy') dt_vencimento_original,
		to_char(a.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento_atual,
		'R' cd_moeda,
		--lpad(replace(replace(campo_mascara_virgula(sum(c.vl_procedimento)),'.',''),',','.'),11,' ') vl_titulo,
		sum(c.vl_procedimento) vl_titulo,
		'CT' cd_tipo_cobranca,
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46) ds_observacao,
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)) cd_grupo_receb,
		10140 cd_centro_custo,
                x.cd_pessoa_fisica cd_pessoa_fisica
	from	nota_fiscal x,
		convenio z,
		conta_paciente b,
		procedimento_paciente c,
		titulo_receber a
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	a.nr_seq_protocolo	= b.nr_seq_protocolo
	and	x.nr_sequencia		= a.nr_seq_nf_saida
	and	z.cd_convenio		= b.cd_convenio_parametro
	and	nvl(c.nr_seq_proc_pacote, c.nr_sequencia) = c.nr_sequencia
	and	c.cd_motivo_exc_conta is null
	and	a.dt_integracao_externa is null
	and	x.cd_operacao_nf	= 25
	and	c.vl_procedimento	<> 0
	group by decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')),
		to_char(a.nr_titulo),
		substr(x.nr_nota_fiscal,1,20),
		to_char(a.dt_emissao,'ddmmyyyy'),
		to_char(a.dt_vencimento,'ddmmyyyy'),
		to_char(a.dt_pagamento_previsto,'ddmmyyyy'),
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46),
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)),
                x.cd_pessoa_fisica,
		a.vl_titulo,
		x.vl_total_nota
	union all
	/* Materiais */
	select	'PR' ie_tipo,
		decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')) cd_pessoa,
		to_char(a.nr_titulo) nr_titulo,
		substr(x.nr_nota_fiscal,1,20) nr_nota_fiscal,
		'1232' cd_empresa_emitente,
		'00' cd_filial,
		'1232' cd_empresa_pagadora,
		'NF' ie_tipo_titulo,
		to_char(a.dt_emissao,'ddmmyyyy') dt_emissao,
		to_char(a.dt_vencimento,'ddmmyyyy') dt_vencimento_original,
		to_char(a.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento_atual,
		'R' cd_moeda,
		--lpad(replace(replace(campo_mascara_virgula(sum(c.vl_material)),'.',''),',','.'),11,' ') vl_titulo,
		sum(c.vl_material) vl_titulo,
		'CT' cd_tipo_cobranca,
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46) ds_observacao,
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)) cd_grupo_receb,
		10140 cd_centro_custo,
                x.cd_pessoa_fisica cd_pessoa_fisica
	from	nota_fiscal x,
		convenio z,
		conta_paciente b,
		material_atend_paciente c,
		titulo_receber a
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	a.nr_seq_protocolo	= b.nr_seq_protocolo
	and	x.nr_sequencia		= a.nr_seq_nf_saida
	and	z.cd_convenio		= b.cd_convenio_parametro
	and	c.nr_seq_proc_pacote is null
	and	c.cd_motivo_exc_conta is null
	and	a.dt_integracao_externa is null
	and	x.cd_operacao_nf	= 25
	and	c.vl_material	<> 0
	group by decode(a.cd_convenio_conta,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')),
		to_char(a.nr_titulo),
		substr(x.nr_nota_fiscal,1,20),
		to_char(a.dt_emissao,'ddmmyyyy'),
		to_char(a.dt_vencimento,'ddmmyyyy'),
		to_char(a.dt_pagamento_previsto,'ddmmyyyy'),
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46),
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)),
                x.cd_pessoa_fisica,
		a.vl_titulo,
		x.vl_total_nota
)
group by
	ie_tipo,
	cd_pessoa,
	nr_titulo,
	nr_nota_fiscal,
	cd_empresa_emitente,
	cd_filial,
	cd_empresa_pagadora,
	ie_tipo_titulo,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	cd_moeda,
	cd_tipo_cobranca,
	ds_observacao,
	cd_grupo_receb,
	cd_centro_custo,
        cd_pessoa_fisica
union all
/* Particulares - Nota fiscal */
select	ie_tipo,
	cd_pessoa,
	nr_titulo,
	nr_nota_fiscal,
	cd_empresa_emitente,
	cd_filial,
	cd_empresa_pagadora,
	ie_tipo_titulo,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	cd_moeda,
	lpad(replace(replace(campo_mascara_virgula(sum(vl_titulo)),'.',''),'.',','),11,' ') vl_titulo,
	cd_tipo_cobranca,
	ds_observacao,
	cd_grupo_receb,
	cd_centro_custo,
        cd_pessoa_fisica
from
(	/* procedimentos*/
	select	'PR' ie_tipo,
		decode(b.cd_convenio_parametro,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')) cd_pessoa,
		null nr_titulo,
		substr(x.nr_nota_fiscal,1,20) nr_nota_fiscal,
		'1232' cd_empresa_emitente,
		'00' cd_filial,
		'1232' cd_empresa_pagadora,
		'NF' ie_tipo_titulo,
		to_char(x.dt_emissao,'ddmmyyyy') dt_emissao,
		to_char(nvl(a.dt_vencimento,x.dt_emissao),'ddmmyyyy') dt_vencimento_original,
		to_char(nvl(a.dt_vencimento,x.dt_emissao),'ddmmyyyy') dt_vencimento_atual,
		'R' cd_moeda,
		--lpad(replace(replace(campo_mascara_virgula(sum(c.vl_procedimento)),'.',''),',','.'),11,' ') vl_titulo,
		sum(c.vl_procedimento) vl_titulo,
		'CT' cd_tipo_cobranca,
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46) ds_observacao,
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)) cd_grupo_receb,
		10140 cd_centro_custo,
                x.cd_pessoa_fisica cd_pessoa_fisica
	from	nota_fiscal x,
		convenio z,
		conta_paciente b,
		procedimento_paciente c,
		nota_fiscal_venc a
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	x.nr_interno_conta	= b.nr_interno_conta
	and	x.nr_sequencia		= a.nr_sequencia(+)
	and	z.cd_convenio		= b.cd_convenio_parametro
	and	nvl(c.nr_seq_proc_pacote, c.nr_sequencia) = c.nr_sequencia
	and	c.cd_motivo_exc_conta is null
	and	x.cd_operacao_nf	= 25
	and	c.vl_procedimento	<> 0
	and	not exists	(select	1
				from	titulo_receber z
				where	z.nr_seq_nf_saida	= x.nr_sequencia)
	group by decode(b.cd_convenio_parametro,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')),
		substr(x.nr_nota_fiscal,1,20),
		to_char(x.dt_emissao,'ddmmyyyy'),
		to_char(nvl(a.dt_vencimento,x.dt_emissao),'ddmmyyyy'),
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46),
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)),
                x.cd_pessoa_fisica,
		a.vl_vencimento,
		x.vl_total_nota
	union all
	/* Materiais */
	select	'PR' ie_tipo,
		decode(b.cd_convenio_parametro,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')) cd_pessoa,
		null nr_titulo,
		substr(x.nr_nota_fiscal,1,20) nr_nota_fiscal,
		'1232' cd_empresa_emitente,
		'00' cd_filial,
		'1232' cd_empresa_pagadora,
		'NF' ie_tipo_titulo,
		to_char(x.dt_emissao,'ddmmyyyy') dt_emissao,
		to_char(nvl(a.dt_vencimento,x.dt_emissao),'ddmmyyyy') dt_vencimento_original,
		to_char(nvl(a.dt_vencimento,x.dt_emissao),'ddmmyyyy') dt_vencimento_atual,
		'R' cd_moeda,
		--lpad(replace(replace(campo_mascara_virgula(sum(c.vl_material)),'.',''),',','.'),11,' ') vl_titulo,
		sum(c.vl_material) vl_titulo,
		'CT' cd_tipo_cobranca,
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46) ds_observacao,
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)) cd_grupo_receb,
		10140 cd_centro_custo,
                x.cd_pessoa_fisica cd_pessoa_fisica
	from	nota_fiscal x,
		convenio z,
		conta_paciente b,
		material_atend_paciente c,
		nota_fiscal_venc a
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	x.nr_interno_conta	= b.nr_interno_conta
	and	x.nr_sequencia		= a.nr_sequencia(+)
	and	z.cd_convenio		= b.cd_convenio_parametro
	and	c.nr_seq_proc_pacote is null
	and	c.cd_motivo_exc_conta is null
	and	x.cd_operacao_nf	= 25
	and	c.vl_material		<> 0
	and	not exists	(select	1
				from	titulo_receber z
				where	z.nr_seq_nf_saida	= x.nr_sequencia)
	group by decode(b.cd_convenio_parametro,1,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					100,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					999,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1003,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					1004,rpad(hsp_obter_cd_pessoa(x.cd_pessoa_fisica),14,' '),
					lpad(z.cd_cgc,14,'0')),
		substr(x.nr_nota_fiscal,1,20),
		to_char(x.dt_emissao,'ddmmyyyy'),
		to_char(nvl(a.dt_vencimento,x.dt_emissao),'ddmmyyyy'),
		substr(replace(replace(x.ds_observacao,chr(13),''), chr(10),''),1,46),
		TRIM(substr(obter_dados_conta_contabil(c.cd_conta_contabil, b.cd_estabelecimento, 'SC'),16,10)),
                x.cd_pessoa_fisica,
		a.vl_vencimento,
		x.vl_total_nota
)
group by
	ie_tipo,
	nr_titulo,
	cd_pessoa,
	nr_nota_fiscal,
	cd_empresa_emitente,
	cd_filial,
	cd_empresa_pagadora,
	ie_tipo_titulo,
	dt_emissao,
	dt_vencimento_original,
	dt_vencimento_atual,
	cd_moeda,
	cd_tipo_cobranca,
	ds_observacao,
	cd_grupo_receb,
	cd_centro_custo,
        cd_pessoa_fisica
/