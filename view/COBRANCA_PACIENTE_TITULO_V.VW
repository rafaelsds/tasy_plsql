create or replace view cobranca_paciente_titulo_v
as
select	b.nr_titulo,
	b.nr_seq_lote,
	b.vl_saldo_titulo,
	nvl(to_number(obter_dados_titulo_receber(b.nr_titulo, 'VT')),0) vl_total_titulo,
	b.nr_seq_cobranca,
	b.dt_pagamento_previsto,
	b.dt_prevista_cobr,
	substr(obter_dados_titulo_receber(b.nr_titulo, 'N'),1,255) nm_pessoa,
	substr(obter_dados_titulo_receber(b.nr_titulo, 'DEF'),1,255) dt_emissao,
	substr(obter_dados_titulo_receber(b.nr_titulo, 'D'),1,255) dt_vencto,
	substr(obter_dados_titulo_receber(b.nr_titulo, 'NFT'),1,255) nr_nota_fiscal,
	substr(obter_dados_titulo_receber(b.nr_titulo, 'NE'),1,255) nr_nfe,
	substr(obter_valor_dominio(1162, b.ie_status_cobranca),1,255) ds_status_cobr,
	substr(obter_dados_titulo_receber(b.nr_titulo, 'S'),1,255) ds_situacao,
	(select	substr(max(x.ds_orgao),1,255) from cobranca_orgao y, orgao_cobranca x where x.nr_sequencia = y.nr_seq_orgao and	y.nr_seq_cobranca = b.nr_seq_cobranca) ds_orgao_cobranca,
	substr((select obter_nome_estabelecimento(x.cd_estabelecimento) from titulo_receber x where nr_titulo = b.nr_titulo),1,255) ds_estabelecimento,
	substr(obter_dados_cobranca(b.nr_seq_cobranca, 2),1,255) ds_paciente_cobranca,
	substr(obter_desc_etapa_cobranca(b.nr_seq_etapa_cobr),1,255) ds_etapa_cobranca,
	substr(obter_dados_titulo_receber(b.nr_titulo,'TP'),1,255) ds_tipo_portador,
	substr(obter_dados_titulo_receber(b.nr_titulo,'NTP'),1,255) ds_portador,
	substr(obter_prod_financ_titulo(b.nr_titulo,'D'),1,255) ds_produto_financeira,
	substr(obter_desc_conta_financ(obter_dados_titulo_receber(b.nr_titulo,'CF')),1,255) ds_conta_financeira,
	substr((select to_char(x.dt_inclusao,'dd/mm/yyyy') from cobranca x where x.nr_sequencia = b.nr_seq_cobranca),1,255) dt_inclusao,
	substr((select obter_nome_pf_pj(x.cd_pessoa_fisica,null) from cobrador x where x.nr_sequencia = b.nr_seq_cobrador),1,255) ds_cobrador,
	substr((select substr(x.ds_historico,1,255) from cobranca_historico x where x.nr_seq_cobranca = b.nr_seq_cobranca and x.nr_sequencia = (select max(y.nr_sequencia) from cobranca_historico y where y.nr_seq_cobranca = x.nr_seq_cobranca)),1,255) ds_historico_cobranca
from	cobranca_paciente_lote a,
	cobranca_paciente_titulo b,
	estabelecimento d
where	b.nr_seq_lote = a.nr_sequencia
and	a.cd_estabelecimento = d.cd_estabelecimento;
/