CREATE OR REPLACE VIEW PAGTO_ESCRIT_UNICRED_V AS
select	1 tp_registro,
	sysdate dt_movimento,
	count(*) qt_registro,
	sum(c.vl_escritural) vl_tot_escrit,
	0 vl_escritural,
	'871' ie_tipo_lanc,
	'' nr_conta,
	null nr_documento,
	e.nr_sequencia nr_seq_envio
from	banco_estabelecimento_v b,      
	banco_escritural e,
	titulo_pagar_escrit c
where	b.nr_sequencia		= e.nr_seq_conta_banco
and	c.nr_seq_escrit		= e.nr_sequencia
and	b.ie_tipo_relacao       in ('EP','ECC')
group by e.nr_sequencia
union
select	2 tp_registro,
	sysdate dt_movimento,
	0 qt_registro,
	0 vl_tot_escrit,
	c.vl_escritural,
	'871' ie_tipo_lanc,
	substr(c.nr_conta,1,5) ||'-' || c.ie_digito_conta nr_conta,
	c.nr_titulo nr_documento,
	e.nr_sequencia nr_seq_envio
from	banco_estabelecimento_v b,      
	banco_escritural e,
	titulo_pagar_escrit c
where	b.nr_sequencia		= e.nr_seq_conta_banco
and	c.nr_seq_escrit		= e.nr_sequencia
and	b.ie_tipo_relacao       in ('EP','ECC');
/
