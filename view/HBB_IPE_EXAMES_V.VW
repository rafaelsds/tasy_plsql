Create or replace view HBB_IPE_EXAMES_V
As
Select
	1				tp_registro,
	a.nr_seq_protocolo	nr_seq_protocolo,
	'SMH'				nm_sistema,
	a.cd_cgc_hospital		cd_cgc_hospital,
	max(c.nr_folha) 		qt_total_conta,
	count(*)			qt_total_lancamento,
	somente_numero(nvl(a.cd_interno,0))
					cd_interno,
	substr(a.nm_hospital,1,45) 
					nm_hospital,
	' '				ds_espaco,
	0				ie_zeros,
	0				tp_nota,
	0				nr_folha,
	0				qt_lancamento,
	0				qt_matricula,
	' '				cd_usuario_convenio,
	0				cd_prestador,
	3				tp_prestador,
	sysdate			dt_referencia,
	0				nr_interno_conta,
	0				vl_total_conta,
	0				nr_linha,
	sysdate			dt_item,
	0				cd_item_convenio,
	0				qt_item,
	0				vl_matmed,
	0				vl_total_matmed,
	' '				nm_paciente,
	0 				cd_solicitante,
	' '				nm_arquivo_pdf
from	w_interf_conta_item_ipe c,
	w_interf_conta_header a
where	a.nr_seq_protocolo		= c.nr_seq_protocolo
group by
	a.nr_seq_protocolo,
	a.cd_cgc_hospital,
	somente_numero(nvl(a.cd_interno,0)),
	substr(a.nm_hospital,1,45)
union all
select
	2				tp_registro,
	c.nr_seq_protocolo	nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	0				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	35				tp_nota,
	0				nr_folha,
	count(*)		qt_lancamento,
	0				qt_matricula,
	' '				cd_usuario_convenio,
	/*decode(obter_se_medico_conveniado(Obter_estab_atend(c.nr_atendimento),c.cd_medico_executor,c.cd_convenio),'S',
		somente_numero(c.NR_CPF_EXECUTOR), somente_numero(c.cd_cgc_hospital))*/
	somente_numero(c.cd_cgc_hospital)
					cd_prestador,
	3				tp_prestador,
	min(c.dt_item)		dt_referencia,
	c.nr_seq_conta_convenio	nr_interno_conta,
	sum(c.vl_total_item)	vl_total_conta,
	0				nr_linha,
	sysdate			dt_item,
	0				cd_item_convenio,
	0				qt_item,
	0				vl_matmed,
	--sum(decode(c.cd_item_convenio,32200005,c.vl_total_item,0)) Geliard OS 279258
 	round(sum(decode(c.cd_item_convenio,32200005,(c.qt_item*0.0645),0)),2) vl_total_matmed,
	' '				nm_paciente,
	0 				cd_solicitante,
	' '				nm_arquivo_pdf
from	protocolo_convenio d,
	w_interf_conta_item_ipe c
where	c.nr_seq_protocolo	= d.nr_seq_protocolo
group by 
	c.nr_seq_protocolo,
	/*decode(obter_se_medico_conveniado(Obter_estab_atend(c.nr_atendimento),c.cd_medico_executor,c.cd_convenio),'S',
		somente_numero(c.NR_CPF_EXECUTOR), somente_numero(c.cd_cgc_hospital)),*/
	somente_numero(c.cd_cgc_hospital),
	c.nr_seq_conta_convenio
union all
select
	3				tp_registro,
	c.nr_seq_protocolo	nr_seq_protocolo,
	'SMH'				nm_sistema,
	' '				cd_cgc_hospital,
	0				qt_total_conta,
	0 				qt_total_lancamento,
	0				cd_interno,
	' '				nm_hospital,
	' '				ds_espaco,
	0 				ie_zeros,
	35				tp_nota,
	0				nr_folha,
	0				qt_lancamento,
	0				qt_matricula,
	substr(b.cd_usuario_convenio,1,13)
					cd_usuario_convenio,
	0				cd_prestador,
	3				tp_prestador,
	sysdate			dt_referencia,
	c.nr_seq_conta_convenio	nr_interno_conta,
	0				vl_total_conta,
	c.nr_linha			nr_linha,
	c.dt_item			dt_item,
	c.cd_item_convenio	cd_item_convenio,
	c.qt_item			qt_item,
	0				vl_matmed,
	0				vl_total_matmed,
	substr(b.nm_paciente,1,45)	nm_paciente,
	somente_numero(nvl(nvl(b.cd_conv_medico_resp,b.nr_crm_medico_resp),0))
					cd_solicitante,
	substr(c.nm_arquivo_pdf,1,50)	nm_arquivo_pdf
from	w_interf_conta_item_ipe c,
	w_interf_conta_cab b
where	b.nr_interno_conta		= c.nr_interno_conta;
/
