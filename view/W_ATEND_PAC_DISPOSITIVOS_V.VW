create or replace
view w_atend_pac_dispositivos_v as
select	distinct
		a.dt_instalacao,
		a.nr_atendimento,
		a.nr_seq_dispositivo,
		substr(Obter_nome_dispositivo(a.nr_seq_dispositivo),1,255) ds_dispositivo,
		nvl(a.ie_sucesso,'S') ie_sucesso,
		decode(nvl(a.ie_sucesso,'S'), 'S', 1, 0) qt_sucesso,
		obter_unidade_atendimento(a.nr_atendimento,'A','CS') cd_setor_atendimento,
		substr(obter_nome_setor(obter_unidade_atendimento(a.nr_atendimento,'A','CS')),1,255) ds_setor_atendimento,
		substr(obter_unidade_atendimento(a.nr_atendimento,'A','U'),1,255) ds_unidade
from	atend_pac_dispositivo a;
/