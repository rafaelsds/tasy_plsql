CREATE OR REPLACE VIEW SMA_REPASSE_PROCEDIMENTOS AS
select	nr_seq_classificacao,
	nr_atendimento,
	dt_inicio_real,
	SUBSTR(obter_nome_pf(CD_PESSOA_FISICA),1,60) nm_paciente,
	SUBSTR(obter_nome_pessoa_fisica(cd_medico_resp, NULL),1,40) nm_medico_responsavel,
	SUBSTR(obter_nome_pessoa_fisica(CD_MEDICO_CIRURGIAO, NULL),1,200) nm_medico_exec,
	SUBSTR(obter_nome_medico(CD_MEDICO_ANESTESISTA,'N'),1,200) NM_MEDICO_ANESTESISTA,
	SUBSTR(obter_valor_dominio(36,CD_TIPO_ANESTESIA),1,150) DS_TIPO_ANESTESIA,
	SUBSTR(obter_nome_convenio(cd_convenio_parametro),1,200) DS_CONVENIO,
	SUBSTR(obter_nome_tipo_atend(IE_TIPO_ATENDIMENTO),1,30) DS_TIPO_ATENDIMENTO,
	SUBSTR(obter_unid_setor_cirurgia(nr_cirurgia,'S'),1,80) DS_SALA_SETOR,
	DS_OBSERVACAO ds_observacao,
	cd_procedimento cd_procedimento,
	SUBSTR(Obter_Desc_Prescr_Proc_exam(cd_procedimento, ie_origem_proced, NR_SEQ_PROC_INTERNO, nr_seq_exame),1,255) ds_procedimento,
	vl_procedimento,
	SUBSTR(Obter_Desc_Prescr_Proc(cd_procedimento_princ, ie_origem_proced_cir, nr_seq_proc_interno_cir),1,255) ds_procedimento_princ,
	cd_procedimento_princ, 
	nr_cirurgia,
	cd_convenio cd_convenio_cir,
	SUBSTR(OBTER_STATUS_CONTA(NR_INTERNO_CONTA, 'A'),1,49) ds_status_conta,
	vl_glosa,
	vl_recebido,
	vl_saldo,
	dt_entrada,
	ie_tipo_atendimento,
	CD_MEDICO_CIRURGIAO,
	CD_MEDICO_EXECUTOR,
	cd_medico_resp,
	CD_MEDICO_ANESTESISTA,
	obter_sexo_pf(cd_pessoa_fisica, 'C') ie_sexo,
	ie_status_acerto ie_status,
	cd_convenio_parametro cd_convenio,
	cd_especialidade,
	cd_grupo_proc,
	cd_setor_atendimento,
	cd_tipo_anestesia,
	nr_sequencia,
	nr_interno_conta,
	dt_liquidacao,
	cd_estabelecimento,
	dt_termino,
	qt_procedimento
from	(SELECT	b.nr_seq_classificacao,
		b.nr_atendimento,
		b.dt_entrada dt_inicio_real,
		b.cd_medico_resp,
		a.CD_MEDICO_CIRURGIAO,
		a.CD_MEDICO_ANESTESISTA,
		a.CD_TIPO_ANESTESIA,
		d.cd_convenio_parametro,
		b.IE_TIPO_ATENDIMENTO,
		a.nr_cirurgia,
		b.DS_OBSERVACAO,
		c.cd_procedimento,
		c.ie_origem_proced,
		c.NR_SEQ_PROC_INTERNO,
		a.cd_convenio,
		c.nr_seq_exame,
		nvl(c.vl_procedimento,0) vl_procedimento,
		a.cd_procedimento_princ, 
		a.ie_origem_proced ie_origem_proced_cir, 
		a.nr_seq_proc_interno nr_seq_proc_interno_cir,
		d.nr_interno_conta,
		b.DT_ENTRADA,
		c.CD_MEDICO_EXECUTOR,
		b.cd_pessoa_fisica,
		d.ie_status_acerto,
		e.cd_especialidade,
		e.cd_grupo_proc,
		c.cd_setor_atendimento,
		c.nr_sequencia,
		decode(nvl(obter_valor_conta(d.nr_interno_conta,0),0),0,0,((nvl(c.vl_procedimento,0) * nvl(obter_valores_conpaci_guia(d.nr_interno_conta,null,null,'VG'),0)) / nvl(obter_valor_conta(d.nr_interno_conta,0),0))) vl_glosa,
		decode(nvl(obter_valor_conta(d.nr_interno_conta,0),0),0,0,((nvl(c.vl_procedimento,0) * nvl(obter_valores_conpaci_guia(d.nr_interno_conta,null,null,'VP'),0)) / nvl(obter_valor_conta(d.nr_interno_conta,0),0))) vl_recebido,
		decode(nvl(obter_valor_conta(d.nr_interno_conta,0),0),0,0,nvl(c.vl_procedimento,0) -
			((nvl(c.vl_procedimento,0) * nvl(obter_valores_conpaci_guia(d.nr_interno_conta,null,null,'VG'),0)) / nvl(obter_valor_conta(d.nr_interno_conta,0),0)) -
			((nvl(c.vl_procedimento,0) * nvl(obter_valores_conpaci_guia(d.nr_interno_conta,null,null,'VP'),0)) / nvl(obter_valor_conta(d.nr_interno_conta,0),0))) vl_saldo,
		(select	max(x.dt_recebimento) 
		from 	convenio_receb x,
			convenio_ret_receb y,
			convenio_retorno_item w
		where	w.nr_seq_retorno	= y.nr_seq_retorno
		and	y.nr_seq_receb		= x.nr_sequencia
		and	w.nr_interno_conta	= d.nr_interno_conta) dt_liquidacao,
		b.cd_estabelecimento,
		a.dt_termino,
		c.qt_procedimento
	FROM	convenio e,
		conta_paciente d,
		estrutura_procedimento_v e,
		cirurgia a,
		atendimento_paciente b,
		procedimento_paciente c
	WHERE	e.ie_tipo_convenio	<> 1
	and	d.cd_convenio_parametro	= e.cd_convenio
	AND	d.nr_atendimento	= b.nr_atendimento 
	AND	c.nr_cirurgia		= a.nr_cirurgia(+)
	AND	c.cd_procedimento	= e.cd_procedimento
	AND	c.ie_origem_proced	= e.ie_origem_proced
	and	c.nr_interno_conta	= d.nr_interno_conta
	and	c.cd_motivo_exc_conta	is null
	AND	d.ie_cancelamento	is null
	union	all
	SELECT	b.nr_seq_classificacao,
		b.nr_atendimento,
		b.dt_entrada dt_inicio_real,
		b.cd_medico_resp,
		a.CD_MEDICO_CIRURGIAO,
		a.CD_MEDICO_ANESTESISTA,
		a.CD_TIPO_ANESTESIA,
		d.cd_convenio_parametro,
		b.IE_TIPO_ATENDIMENTO,
		a.nr_cirurgia,
		b.DS_OBSERVACAO,
		c.cd_procedimento,
		c.ie_origem_proced,
		c.NR_SEQ_PROC_INTERNO,
		a.cd_convenio,
		c.nr_seq_exame,
		nvl(c.vl_procedimento,0) vl_procedimento,
		a.cd_procedimento_princ, 
		a.ie_origem_proced ie_origem_proced_cir, 
		a.nr_seq_proc_interno nr_seq_proc_interno_cir,
		d.nr_interno_conta,
		b.DT_ENTRADA,
		c.CD_MEDICO_EXECUTOR,
		b.cd_pessoa_fisica,
		d.ie_status_acerto,
		e.cd_especialidade,
		e.cd_grupo_proc,
		c.cd_setor_atendimento,
		c.nr_sequencia,
		0 vl_glosa,
		decode(nvl(obter_valor_conta(d.nr_interno_conta,0),0),0,0,((nvl(c.vl_procedimento,0) * (select	nvl(sum(y.vl_recebido),0)
													from	titulo_receber_liq y,
														titulo_receber x
													where	x.nr_titulo	= y.nr_titulo
													and	(x.nr_interno_conta = d.nr_interno_conta or x.nr_seq_protocolo = d.nr_seq_protocolo))) / nvl(obter_valor_conta(d.nr_interno_conta,0),0))) vl_recebido,
		decode(nvl(obter_valor_conta(d.nr_interno_conta,0),0),0,0,nvl(c.vl_procedimento,0) -
			((nvl(c.vl_procedimento,0) * (select	nvl(sum(y.vl_recebido),0)
							from	titulo_receber_liq y,
								titulo_receber x
							where	x.nr_titulo	= y.nr_titulo
							and	(x.nr_interno_conta = d.nr_interno_conta or x.nr_seq_protocolo = d.nr_seq_protocolo))) / nvl(obter_valor_conta(d.nr_interno_conta,0),0))) vl_saldo,
		(select	max(x.dt_liquidacao) 
		from 	titulo_receber x
		where	x.nr_interno_conta	= d.nr_interno_conta) dt_liquidacao,
		b.cd_estabelecimento,
		a.dt_termino,
		c.qt_procedimento
	FROM	convenio e,
		conta_paciente d,
		estrutura_procedimento_v e,
		cirurgia a,
		atendimento_paciente b,
		procedimento_paciente c
	WHERE	e.ie_tipo_convenio	= 1
	and	d.cd_convenio_parametro	= e.cd_convenio
	AND	d.nr_atendimento	= b.nr_atendimento 
	AND	c.nr_cirurgia		= a.nr_cirurgia(+)
	AND	c.cd_procedimento	= e.cd_procedimento
	AND	c.ie_origem_proced	= e.ie_origem_proced
	and	c.nr_interno_conta	= d.nr_interno_conta
	and	c.cd_motivo_exc_conta	is null
	AND	d.ie_cancelamento	is null) A;
/
