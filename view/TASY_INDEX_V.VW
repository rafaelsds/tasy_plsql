create or replace view
tasy_index_v as
select 	tablespace_name,
		segment_type,
		count(*) quantidade
from 		user_segments
group by 	tablespace_name,
	 	segment_type;
/
