create or replace
view pro_eis_receita_custo_v as
select	a.cd_estabelecimento,
	b.cd_setor_atendimento,
	trunc(a.dt_referencia,'month') dt_referencia,
	trunc(a.dt_receita,'month') dt_receita,
	nvl(b.cd_centro_custo_receita, b.cd_centro_custo) cd_centro_custo,
	a.ie_protocolo,
	(a.vl_procedimento + a.vl_material) vl_receita,
	(a.vl_procedimento + a.vl_material + a.vl_terceiro) vl_total
from	Setor_Atendimento b,
	Eis_Resultado a
where	a.cd_setor_atendimento	= b.cd_setor_atendimento;
/