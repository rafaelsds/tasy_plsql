create or replace view w_eis_dor_prevalencia_v  as 
select	distinct	
	e.cd_setor_atendimento cd_Setor,
	obter_nome_setor(e.cd_setor_atendimento) ds_setor_atendimento,
	obter_unidade_atendimento(e.nr_atendimento,'A','U') ds_unidade,
	obter_sexo_pf(e.cd_pessoa_fisica,'C') ie_sexo,
	nvl(obter_sexo_pf(e.cd_pessoa_fisica,'D'),'N�o informado') ds_sexo,	
	substr(obter_idade(obter_data_nascto_pf(e.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria,	
	e.dt_sinal_vital,			
	e.qt_avaliacao,
	e.qt_avaliacao_dor,
	e.qt_avaliacao_sem_dor,	
	e.qt_max_intensidade,	
	e.nr_atendimento,
	e.cd_pessoa_fisica,
	e.qt_paciente_dor,
	e.nr_seq_result_dor,	
	substr(obter_desc_result_dor(e.nr_seq_result_dor,'D'),1,150) ds_result_dor,
	e.nr_seq_turno_atend nr_turno,
	substr(obter_desc_result_dor(e.nr_seq_turno_atend,'T'),1,150) ds_turno
from 	w_eis_escala_dor e
where 	e.qt_avaliacao <> 0	
and	obter_se_dor_avaliado_turnos(e.nr_atendimento,e.dt_sinal_vital) = 'S';	
/