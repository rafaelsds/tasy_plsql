CREATE OR REPLACE 
VIEW Laudo_integracao_v
AS
select	a.nr_sequencia,
	a.dt_integracao,
	a.dt_laudo,
	a.ds_laudo,
	b.nr_acesso_dicom,
	a.dt_liberacao
FROM	Prescr_procedimento b,
	laudo_paciente a
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_seq_prescricao	= b.nr_sequencia
and	a.dt_liberacao is not null
and 	a.ds_laudo is not null;	

DROP PUBLIC SYNONYM Laudo_integracao_v;

CREATE PUBLIC SYNONYM Laudo_integracao_v FOR TASY.Laudo_integracao_v;
	
GRANT SELECT ON Laudo_integracao_v TO Pixeon;
