create or replace view pls_espelho_conta_relat_v as
select	'1' ie_union,
	f.cd_guia_ok cd_guia,
	h.cd_usuario_plano,
	substr(i.nm_pessoa_fisica,1,40) nm_segurado,
	k.cd_doenca,
	a.nr_sequencia nr_seq_lote_fat,
	g.nr_sequencia nr_seq_segurado,
	b.dt_mes_competencia,
	b.nr_sequencia nr_seq_protocolo,
	f.nr_sequencia nr_seq_conta,
	c.nr_sequencia  nr_seq_fatura,
	l.ie_tipo_despesa,
	f.nr_seq_prestador_exec,
	l.dt_procedimento dt_item,
	l.cd_procedimento cd_item,
	l.ie_origem_proced,
	substr(m.ds_procedimento, 1, 240) ds_item,
	nvl(l.qt_procedimento, 0) qt_liberada,
	nvl(l.vl_material_ptu, 0) vl_material_ptu,
	nvl(l.vl_unitario, 0) vl_unitario,
	nvl(l.tx_prestador_item, 0) tx_prestador_item,
	l.vl_liberado,
	f.nr_seq_clinica,
	f.nr_seq_tipo_acomodacao,
	c.nr_seq_pagador,
	n.cd_cooperativa,
	a.nr_seq_regra_fat,
	a.dt_mesano_referencia,
	f.cd_medico_solicitante,
	l.tx_item
from	pls_congenere		n,
	procedimento		m,
	pls_conta_proc		l,
	pls_diagnostico_conta	k,
	pessoa_fisica		i,
	pls_segurado_carteira	h,
	pls_segurado		g,
	pls_conta		f,
	pls_fatura_conta	e,
	pls_fatura_evento	d,
	pls_fatura		c,
	pls_protocolo_conta	b,
	pls_lote_faturamento	a
where	f.nr_sequencia 		= e.nr_seq_conta
and	d.nr_sequencia 		= e.nr_seq_fatura_evento
and	c.nr_sequencia 		= d.nr_seq_fatura
and	b.nr_sequencia 		= f.nr_seq_protocolo
and	a.nr_sequencia 		= c.nr_seq_lote
and	g.nr_sequencia 		= f.nr_seq_segurado
and	g.nr_sequencia 		= h.nr_seq_segurado
and	i.cd_pessoa_fisica 	= g.cd_pessoa_fisica
and	f.nr_sequencia 		= k.nr_seq_conta(+)
and	f.nr_sequencia		= l.nr_seq_conta
and	l.cd_procedimento	= m.cd_procedimento
and	l.ie_origem_proced	= m.ie_origem_proced
and	n.nr_sequencia(+) 	= c.nr_seq_congenere
and	f.cd_guia_ok is not null
union all
select	'2' ie_union,
	f.cd_guia_ok cd_guia,
	h.cd_usuario_plano,
	substr(i.nm_pessoa_fisica,1,40) nm_segurado,
	k.cd_doenca,
	a.nr_sequencia nr_seq_lote_fat,
	g.nr_sequencia nr_seq_segurado,
	b.dt_mes_competencia,
	b.nr_sequencia nr_seq_protocolo,
	f.nr_sequencia nr_seq_conta,
	c.nr_sequencia  nr_seq_fatura,
	l.ie_tipo_despesa,
	f.nr_seq_prestador_exec,
	l.dt_atendimento dt_item,
	m.nr_sequencia cd_item,
	null ie_origem_proced,
	substr(m.ds_material, 1, 240) ds_item,
	nvl(l.qt_material, 0) qt_liberada,
	nvl(l.vl_material_ptu, 0) vl_material_ptu,
	nvl(l.vl_unitario, 0) vl_unitario,
	0 tx_prestador_item,
	l.vl_liberado,
	f.nr_seq_clinica,
	f.nr_seq_tipo_acomodacao,
	c.nr_seq_pagador,
	n.cd_cooperativa,
	a.nr_seq_regra_fat,
	a.dt_mesano_referencia,
	f.cd_medico_solicitante,
	null tx_item
from	pls_congenere		n,
	pls_material		m,
	pls_conta_mat		l,
	pls_diagnostico_conta	k,
	pessoa_fisica		i,
	pls_segurado_carteira	h,
	pls_segurado		g,
	pls_conta		f,
	pls_fatura_conta	e,
	pls_fatura_evento	d,
	pls_fatura		c,
	pls_protocolo_conta	b,
	pls_lote_faturamento	a
where	f.nr_sequencia 		= e.nr_seq_conta
and	d.nr_sequencia 		= e.nr_seq_fatura_evento
and	c.nr_sequencia 		= d.nr_seq_fatura
and	b.nr_sequencia 		= f.nr_seq_protocolo
and	a.nr_sequencia 		= c.nr_seq_lote
and	g.nr_sequencia 		= f.nr_seq_segurado
and	g.nr_sequencia 		= h.nr_seq_segurado
and	i.cd_pessoa_fisica 	= g.cd_pessoa_fisica
and	f.nr_sequencia 		= k.nr_seq_conta(+)
and	f.nr_sequencia		= l.nr_seq_conta
and	m.nr_sequencia		= l.nr_seq_material
and	n.nr_sequencia(+) 	= c.nr_seq_congenere
and	f.cd_guia_ok is not null;
/