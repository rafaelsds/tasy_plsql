create or replace view EPIMED_DADOS_LABORATORIO_V  as 
select b.cd_pessoa_fisica PATIENTID,
       b.nr_atendimento visit_id,
       b.nr_prescricao,
       elab.nm_exame,
       to_char(elri.dt_coleta ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE,
       elab.nr_seq_exame ID,
       lee.cd_exame_equip EXAM_CODE ,
       lab_obter_resultado_exame(elri.nr_seq_resultado,elri.nr_sequencia) EXAM_VALUE,
       lab_obter_unidade_medida(elab.nr_seq_unid_med) unit_of_measure,
       b.cd_estabelecimento
from prescr_procedimento a
inner join prescr_medica b on a.nr_prescricao = b.nr_prescricao
inner join lab_exame_equip lee on lee.nr_seq_exame = a.nr_seq_exame
inner join equipamento_lab el on el.cd_equipamento = lee.cd_equipamento and el.ds_sigla = 'EPIMED'   
inner join exame_lab_resultado elr on elr.nr_prescricao = a.nr_prescricao
inner join exame_lab_result_item elri on elri.nr_seq_resultado = elr.nr_seq_resultado  and elri.nr_seq_prescr = a.nr_sequencia
inner join material_exame_lab mel on mel.nr_sequencia = elri.nr_seq_material
inner join exame_laboratorio elab on elab.nr_seq_exame = a.nr_seq_exame
where  a.ie_status_atend>=35
and  elri.nr_seq_material is not null;
/