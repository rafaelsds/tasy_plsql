create or replace 
view ACOMP_TRIBUTO_V
as
select	nvl(sum(a.VL_NAO_RETIDO),0) vl_soma_trib_nao_retido,
	nvl(sum(a.VL_BASE_NAO_RETIDO),0) vl_soma_base_nao_retido,
	nvl(sum(a.VL_TRIB_ADIC),0) vl_soma_trib_adic,
	nvl(sum(a.VL_BASE_ADIC),0) vl_soma_base_adic,
	b.cd_cgc,
	b.cd_pessoa_fisica,
	decode(c.ie_vencimento, 'V', b.dt_vencimento_atual, 'C', nvl(b.dt_contabil, b.dt_emissao), b.dt_emissao) dt_tributo,
	a.cd_tributo,
	'TP' ie_origem_valores,
	nvl(sum(a.vl_imposto),0) vl_tributo,
	'' cd_cgc_emitente,
--	sum(to_number(OBTER_DADOS_TIT_PAGAR(b.nr_titulo, 'V'))) vl_total_base,  Edgar 15/03/2006 OS 31396, devido ao INSS descontado do IRPF
	nvl(sum(a.vl_base_calculo),0) vl_total_base,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(a.nr_titulo, 'TP'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	b.nr_titulo nr_titulo,
	0 nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	nvl(c.ie_baixa_titulo, 'N') ie_baixa_titulo,
	e.cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	b.cd_estab_financeiro,
	a.CD_DARF,
	to_number(null) nr_seq_distribuicao,
	to_number(null) nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	pessoa_juridica e,
	tributo c,
	titulo_pagar b,
	TITULO_PAGAR_IMPOSTO a
where	a.nr_titulo		= b.nr_titulo
and	b.ie_situacao	in 	('A','L','D')
and	a.ie_pago_prev	= 'V'
and	a.cd_tributo		= c.cd_tributo
and	nvl(c.ie_baixa_titulo, 'N')	= 'N'
and	b.cd_cgc		= e.cd_cgc(+)
group 	by b.cd_cgc,
	b.cd_pessoa_fisica,
	b.dt_vencimento_atual,
	a.cd_tributo,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(a.nr_titulo, 'TP'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	b.nr_titulo,
	c.ie_baixa_titulo,
	e.cd_cnpj_raiz,
	b.cd_estab_financeiro,
	c.ie_vencimento,
	b.dt_contabil,
	b.dt_emissao,
	a.CD_DARF,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento)
union
select	nvl(sum(a.VL_NAO_RETIDO),0) vl_soma_trib_nao_retido,
	nvl(sum(a.VL_BASE_NAO_RETIDO),0) vl_soma_base_nao_retido,
	nvl(sum(a.VL_TRIB_ADIC),0) vl_soma_trib_adic,
	nvl(sum(a.VL_BASE_ADIC),0) vl_soma_base_adic,
	d.CD_CGC,
	d.CD_PESSOA_FISICA,
	decode(e.ie_vencimento, 'V', B.DT_VENCIMENTO, 'C', nvl(B.DT_VENCIMENTO, c.dt_mesano_referencia), c.dt_mesano_referencia) dt_tributo,
	a.cd_tributo,
	'RT' ie_origem_valores,
	nvl(sum(a.vl_imposto),0) vl_tributo,
	'' cd_cgc_emitente,
	sum(a.VL_BASE_CALCULO) vl_total_base,
	c.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(c.nr_repasse_terceiro, 'RT'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	c.nr_repasse_terceiro nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	f.cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(c.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	to_number(null) nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(c.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	pessoa_juridica f,
	tributo e,
	terceiro d,
	repasse_terceiro c,
	repasse_terceiro_venc b,
	Repasse_terc_venc_trib a
where	a.nr_seq_rep_venc	= b.nr_sequencia
and	b.nr_repasse_terceiro	= c.nr_repasse_terceiro
and	c.nr_seq_terceiro	= d.nr_sequencia
and	a.ie_pago_prev		= 'V'
and	e.cd_tributo		= a.cd_tributo
and	(e.ie_repasse_titulo	= 'N' or exists(select 1 from titulo_pagar x where x.nr_repasse_terceiro = c.nr_repasse_terceiro))
and	d.cd_cgc		= f.cd_cgc(+)
group	by d.CD_CGC,
	d.CD_PESSOA_FISICA,
	decode(e.ie_vencimento, 'V', B.DT_VENCIMENTO, 'C', nvl(B.DT_VENCIMENTO, c.dt_mesano_referencia), c.dt_mesano_referencia),
	a.cd_tributo,
	c.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(c.nr_repasse_terceiro, 'RT'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	c.nr_repasse_terceiro,
	f.cd_cnpj_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(c.cd_estabelecimento)),
	a.cd_darf,
	OBTER_EMPRESA_ESTAB(c.cd_estabelecimento)
union
select	nvl(sum(a.VL_TRIB_NAO_RETIDO), 0) vl_soma_trib_nao_retido,
	nvl(sum(a.VL_BASE_NAO_RETIDO), 0) vl_soma_base_nao_retido,
	nvl(sum(a.vl_trib_adic), 0) vl_soma_trib_adic,
	nvl(sum(a.vl_base_adic), 0) vl_soma_base_adic,
	decode(nvl(b.cd_pessoa_fisica,'X'),'X', b.cd_cgc, '') cd_cgc,
	b.cd_pessoa_fisica,
	b.dt_emissao dt_tributo,
	a.cd_tributo,
	'NFE' ie_origem_valores,
	nvl(sum(a.vl_tributo),0) vl_tributo,
	b.cd_cgc_emitente,
	sum(a.vl_base_calculo) vl_total_base,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(b.nr_sequencia, 'NFE'),1,1)  ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	0 nr_repasse_terceiro,
	b.nr_sequencia nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	e.cd_cnpj_raiz,
	f.cd_cnpj_raiz cd_cnpj_emitente_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(b.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	to_number(null) nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	pessoa_juridica f,
	pessoa_juridica e,
	tributo d,
	operacao_nota o,
	nota_fiscal b,
	nota_fiscal_trib a
where	a.nr_sequencia	= b.nr_sequencia
and	nvl(b.ie_situacao,'1')	= '1'
and	b.cd_operacao_nf	= o.cd_operacao_nf
and	o.IE_OPERACAO_FISCAL	= 'E'
and	a.cd_tributo		= d.cd_tributo
and	d.ie_corpo_item	= 'C'
and	nvl(a.ie_origem_trib, 'N')	= 'N'
and	nvl(ie_pago_prev,'V')	= 'V'
and	b.cd_cgc		= e.cd_cgc(+)
and	b.cd_cgc_emitente	= f.cd_cgc(+)
group 	by b.cd_cgc,
	b.cd_pessoa_fisica,
	b.dt_emissao,
	a.cd_tributo,
	b.cd_cgc_emitente,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(b.nr_sequencia, 'NFE'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	b.nr_sequencia,
	e.cd_cnpj_raiz,
	f.cd_cnpj_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(b.cd_estabelecimento)),
	a.cd_darf,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento)
union	
select	nvl(sum(a.VL_TRIB_NAO_RETIDO), 0) vl_soma_trib_nao_retido, -- Edgar OS 50572 tributos que tem a base de calculo conforme vencimentos da NF
	nvl(sum(a.VL_BASE_NAO_RETIDO), 0) vl_soma_base_nao_retido,
	nvl(sum(a.vl_trib_adic), 0) vl_soma_trib_adic,
	nvl(sum(a.vl_base_adic), 0) vl_soma_base_adic,
	decode(nvl(b.cd_pessoa_fisica,'X'),'X', b.cd_cgc, '') cd_cgc,
	b.cd_pessoa_fisica,
	a.dt_vencimento dt_tributo,
	a.cd_tributo,
	'NFE' ie_origem_valores,
	nvl(sum(a.vl_tributo),0) vl_tributo,
	b.cd_cgc_emitente,
	sum(a.vl_base_calculo) vl_total_base,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(b.nr_sequencia, 'NFE'),1,1)  ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	0 nr_repasse_terceiro,
	b.nr_sequencia nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	e.cd_cnpj_raiz,
	f.cd_cnpj_raiz cd_cnpj_emitente_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(b.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	to_number(null) nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	pessoa_juridica f,
	pessoa_juridica e,
	tributo d,
	operacao_nota o,
	nota_fiscal b,
	nota_fiscal_venc_trib a
where	a.nr_sequencia	= b.nr_sequencia
and	nvl(b.ie_situacao,'1')	= '1'
and	b.cd_operacao_nf	= o.cd_operacao_nf
and	o.IE_OPERACAO_FISCAL	= 'E'
and	a.cd_tributo		= d.cd_tributo
and	d.ie_corpo_item	= 'V'
and	b.cd_cgc		= e.cd_cgc(+)
and	b.cd_cgc_emitente	= f.cd_cgc(+)
group 	by b.cd_cgc,
	b.cd_pessoa_fisica,
	a.dt_vencimento,
	a.cd_tributo,
	b.cd_cgc_emitente,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(b.nr_sequencia, 'NFE'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	b.nr_sequencia,
	e.cd_cnpj_raiz,
	f.cd_cnpj_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(b.cd_estabelecimento)),
	a.cd_darf,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento)
union
select	nvl(sum(a.VL_NAO_RETIDO),0) vl_soma_trib_nao_retido,
	nvl(sum(a.VL_BASE_NAO_RETIDO),0) vl_soma_base_nao_retido,
	nvl(sum(a.VL_TRIB_ADIC),0) vl_soma_trib_adic,
	nvl(sum(a.VL_BASE_ADIC),0) vl_soma_base_adic,
	b.cd_cgc,
	b.cd_pessoa_fisica,
	d.dt_baixa dt_tributo,
	a.cd_tributo,
	'TP' ie_origem_valores,
	nvl(sum(a.vl_imposto),0) vl_tributo,
	'' cd_cgc_emitente,
--	sum(to_number(OBTER_DADOS_TIT_PAGAR(b.nr_titulo, 'V'))) vl_total_base,  Edgar 15/03/2006 OS 31396, devido ao INSS descontado do IRPF
	nvl(sum(a.vl_base_calculo),0) vl_total_base,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(a.nr_titulo, 'TP'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	b.nr_titulo nr_titulo,
	0 nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	c.ie_baixa_titulo ie_baixa_titulo,
	e.cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	b.cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	to_number(null) nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	pessoa_juridica e,
	tributo c,
	titulo_pagar_baixa d,
	titulo_pagar b,
	TITULO_PAGAR_IMPOSTO a
where	a.nr_titulo		= b.nr_titulo
and	b.ie_situacao	in 	('A','L')
and	a.ie_pago_prev	= 'V'
and	a.cd_tributo		= c.cd_tributo
and	c.ie_baixa_titulo	= 'S'
and	a.nr_seq_baixa		= d.nr_sequencia
and	a.nr_titulo		= d.nr_titulo
and	b.cd_cgc		= e.cd_cgc(+)
group 	by b.cd_cgc,
	b.cd_pessoa_fisica,
	d.dt_baixa,
	a.cd_tributo,
	b.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(a.nr_titulo, 'TP'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	b.nr_titulo,
	c.ie_baixa_titulo,
	e.cd_cnpj_raiz,
	b.cd_estab_financeiro,
	a.cd_darf,
	OBTER_EMPRESA_ESTAB(b.cd_estabelecimento)
union
select	nvl(sum(a.VL_NAO_RETIDO),0) vl_soma_trib_nao_retido,
	nvl(sum(a.VL_BASE_NAO_RETIDO),0) vl_soma_base_nao_retido,
	nvl(sum(a.VL_TRIB_ADIC),0) vl_soma_trib_adic,
	nvl(sum(a.VL_BASE_ADIC),0) vl_soma_base_adic,
	'' CD_CGC,
	d.CD_PESSOA_FISICA,
	decode(e.ie_vencimento, 'V', C.DT_VENCIMENTO, 'C', nvl(c.DT_VENCIMENTO, f.dt_mesano_ref), f.dt_mesano_ref) dt_tributo,
	a.cd_tributo,
	'DL' ie_origem_valores,
	nvl(sum(a.vl_tributo),0) vl_tributo,
	'' cd_cgc_emitente,
	sum(a.VL_BASE_CALCULO) vl_total_base,
	d.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(c.nr_sequencia, 'DL'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	to_number(null) nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	'' cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(d.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	c.nr_sequencia nr_seq_distribuicao,
	to_number(null) nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(d.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	tributo e,
	dl_socio d,
	dl_lote_distribuicao f,
	dl_distribuicao c,
	dl_distribuicao_trib a
where	a.nr_seq_distribuicao	= c.nr_sequencia
and	c.nr_seq_socio		= d.nr_sequencia
and	a.cd_tributo		= e.cd_tributo
and	c.nr_seq_lote		= f.nr_sequencia
group	by d.CD_PESSOA_FISICA,
	decode(e.ie_vencimento, 'V', C.DT_VENCIMENTO, 'C', nvl(c.DT_VENCIMENTO, f.dt_mesano_ref), f.dt_mesano_ref),
	a.cd_tributo,
	d.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(c.nr_sequencia, 'DL'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	to_number(OBTER_ESTAB_FINANCEIRO(d.cd_estabelecimento)),
	a.cd_darf,
	c.nr_sequencia,
	OBTER_EMPRESA_ESTAB(d.cd_estabelecimento)
union
select	nvl(sum(a.VL_NAO_RETIDO),0) vl_soma_trib_nao_retido,
	nvl(sum(a.VL_BASE_NAO_RETIDO),0) vl_soma_base_nao_retido,
	nvl(sum(a.VL_TRIB_ADIC),0) vl_soma_trib_adic,
	nvl(sum(a.VL_BASE_ADIC),0) vl_soma_base_adic,
	d.CD_CGC,
	d.CD_PESSOA_FISICA,
	decode(e.ie_vencimento, 'V', B.DT_VENCIMENTO, 'C', nvl(B.DT_VENCIMENTO, c.dt_mes_competencia), c.dt_mes_competencia) dt_tributo,
	a.cd_tributo,
	'LP' ie_origem_valores,
	nvl(sum(a.vl_imposto),0) vl_tributo,
	'' cd_cgc_emitente,
	sum(a.VL_BASE_CALCULO) vl_total_base,
	c.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(c.nr_sequencia, 'LP'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	to_number(null) nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	substr(obter_cnpj_raiz(d.cd_cgc),1,20) cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	to_number(OBTER_ESTAB_FINANCEIRO(c.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	c.nr_sequencia nr_seq_lote_protocolo,
	OBTER_EMPRESA_ESTAB(c.cd_estabelecimento) cd_empresa,
	to_number(null) nr_seq_pag_prest,
	null nr_seq_trib_valor
from	tributo e,
	pls_prestador d,
	pls_lote_protocolo c,
	pls_lote_protocolo_venc b,
	pls_lote_prot_venc_trib a
where	a.nr_seq_lote_venc	= b.nr_sequencia
and	b.nr_seq_lote		= c.nr_sequencia
and	c.nr_seq_prestador	= d.nr_sequencia
and	a.ie_pago_prev		= 'V'
and	e.cd_tributo		= a.cd_tributo
group	by d.CD_CGC,
	d.CD_PESSOA_FISICA,
	decode(e.ie_vencimento, 'V', B.DT_VENCIMENTO, 'C', nvl(B.DT_VENCIMENTO, c.dt_mes_competencia), c.dt_mes_competencia),
	a.cd_tributo,
	c.cd_estabelecimento,
	substr(OBTER_SE_BASE_CALCULO(c.nr_sequencia, 'LP'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	c.nr_sequencia,
	substr(obter_cnpj_raiz(d.cd_cgc),1,20),
	to_number(OBTER_ESTAB_FINANCEIRO(c.cd_estabelecimento)),
	a.cd_darf,
	OBTER_EMPRESA_ESTAB(c.cd_estabelecimento)
union
select	nvl(sum(a.vl_nao_retido),0) vl_soma_trib_nao_retido,
	nvl(sum(a.vl_base_nao_retido),0) vl_soma_base_nao_retido,
	nvl(sum(a.vl_trib_adic),0) vl_soma_trib_adic,
	nvl(sum(a.vl_base_adic),0) vl_soma_base_adic,
	d.cd_cgc,
	d.cd_pessoa_fisica,
	decode(e.ie_venc_pls_pag_prod, 'C', p.dt_mes_competencia, 'V', b.dt_vencimento, decode(e.ie_vencimento, 'V', b.dt_vencimento, 'C', nvl(b.dt_vencimento, p.dt_mes_competencia), p.dt_mes_competencia)) dt_tributo,
	a.cd_tributo,
	'PP' ie_origem_valores,
	nvl(sum(a.vl_imposto),0) vl_tributo,
	'' cd_cgc_emitente,
	sum(a.vl_base_calculo) vl_total_base,
	p.cd_estabelecimento,
	substr(obter_se_base_calculo(c.nr_sequencia, 'PP'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	to_number(null) nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	substr(obter_cnpj_raiz(d.cd_cgc),1,20) cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	to_number(obter_estab_financeiro(p.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	null nr_seq_lote_protocolo,
	obter_empresa_estab(p.cd_estabelecimento) cd_empresa,
	c.nr_sequencia nr_seq_pag_prest,
	null nr_seq_trib_valor
from	tributo				e,
	pls_prestador			d,
	pls_lote_pagamento		p,
	pls_pagamento_prestador		c,
	pls_pag_prest_vencimento	b,
	pls_pag_prest_venc_trib		a
where	a.nr_seq_vencimento	= b.nr_sequencia
and	b.nr_seq_pag_prestador	= c.nr_sequencia
and	c.nr_seq_prestador	= d.nr_sequencia
and	c.nr_seq_lote		= p.nr_sequencia
and	a.ie_pago_prev		= 'V'
and	e.cd_tributo		= a.cd_tributo
and	c.IE_CANCELAMENTO is null
group by
	d.cd_cgc,
	d.cd_pessoa_fisica,
	decode(e.ie_venc_pls_pag_prod, 'C', p.dt_mes_competencia, 'V', b.dt_vencimento, decode(e.ie_vencimento, 'V', b.dt_vencimento, 'C', nvl(b.dt_vencimento, p.dt_mes_competencia), p.dt_mes_competencia)),
	a.cd_tributo,
	p.cd_estabelecimento,
	substr(obter_se_base_calculo(c.nr_sequencia, 'PP'),1,1),
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	c.nr_sequencia,
	substr(obter_cnpj_raiz(d.cd_cgc),1,20),
	to_number(obter_estab_financeiro(p.cd_estabelecimento)),
	a.cd_darf,
	obter_empresa_estab(p.cd_estabelecimento)
union
select	nvl(sum(a.vl_nao_retido),0) vl_soma_trib_nao_retido,
	nvl(sum(a.vl_base_nao_retido),0) vl_soma_base_nao_retido,
	nvl(sum(a.vl_trib_adic),0) vl_soma_trib_adic,
	nvl(sum(a.vl_base_adic),0) vl_soma_base_adic,
	'' cd_cgc,
	b.cd_pessoa_fisica,
	decode(e.ie_vencimento, 'V', a.dt_imposto, 'C', nvl(a.dt_imposto, c.dt_mes_referencia), c.dt_mes_referencia) dt_tributo,
	a.cd_tributo,
	'PP' ie_origem_valores,
	nvl(sum(a.vl_imposto),0) vl_tributo,
	'' cd_cgc_emitente,
	sum(a.vl_base_calculo) vl_total_base,
	c.cd_estabelecimento,
	substr(obter_se_base_calculo(c.nr_sequencia, 'PP'),1,1) ie_base_calculo,
	sum(nvl(a.vl_reducao,0)) vl_reducao,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao) dt_criacao,
	0 nr_titulo,
	to_number(null) nr_repasse_terceiro,
	0 nr_seq_nota_fiscal,
	'N' ie_baixa_titulo,
	null cd_cnpj_raiz,
	'' cd_cnpj_emitente_raiz,
	to_number(obter_estab_financeiro(c.cd_estabelecimento)) cd_estab_financeiro,
	a.cd_darf,
	to_number(null) nr_seq_distribuicao,
	null nr_seq_lote_protocolo,
	obter_empresa_estab(c.cd_estabelecimento) cd_empresa,
	null nr_seq_pag_prest,
	a.nr_sequencia nr_seq_trib_valor
from	pls_lote_ret_trib_valor		a,
	pls_lote_ret_trib_prest		b,
	pls_lote_retencao_trib		c,
	tributo				e
where	a.nr_seq_trib_prest	= b.nr_sequencia
and	b.nr_seq_lote		= c.nr_sequencia
and	b.cd_pessoa_fisica is not null
and	a.ie_pago_prev		in ('R','V')
and	e.cd_tributo		= a.cd_tributo
group by
	b.cd_pessoa_fisica,
	decode(e.ie_vencimento, 'V', a.dt_imposto, 'C', nvl(a.dt_imposto, c.dt_mes_referencia), c.dt_mes_referencia),
	a.cd_tributo,
	c.cd_estabelecimento,
	c.nr_sequencia,
	nvl(a.dt_atualizacao_nrec,a.dt_atualizacao),
	a.cd_darf,
	a.nr_sequencia;
/
