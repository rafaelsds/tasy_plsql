CREATE OR REPLACE VIEW Tipo_Atendimento_v
AS 
SELECT Campo_Numerico(VL_DOMINIO) CD_tipo_atendimento,
       DS_VALOR_DOMINIO DS_tipo_atendimento
FROM VALOR_DOMINIO
WHERE CD_DOMINIO = 12;