Create or Replace view pls_conta_partic_ocor_v
As
/* Partcipantes que n�o geraram conta de honor�rio */
select	a.nr_seq_conta_proc nr_seq_conta_proc,
	a.nr_sequencia nr_seq_proc_partic,
	b.cd_procedimento,
	b.ie_origem_proced,
	b.dt_procedimento,
	b.dt_procedimento_trunc,
	b.cd_guia_referencia cd_guia_referencia,
	a.cd_medico cd_medico,
	a.nr_crm_imp nr_crm,
	a.nr_cpf_imp nr_cpf,
	a.cd_grau_partic_imp,
	a.nr_seq_grau_partic,
	a.nr_seq_prestador nr_seq_prestador,
	b.nr_seq_segurado
from	pls_conta_proc_v b,
	pls_proc_participante a
where	a.nr_seq_conta_proc = b.nr_sequencia
and	b.ie_status 		<> 'D'
and	(a.ie_status 		<> 'C' or a.ie_status is null)
and	(a.ie_gerada_cta_honorario = 'N' or a.ie_gerada_cta_honorario is null)
union all
/* Procedimentos referentes a contas de honor�rio geradas pelo sistema - Participante e grau na conta */
select	b.nr_sequencia nr_seq_conta_proc,
	null nr_seq_proc_partic,
	b.cd_procedimento,
	b.ie_origem_proced,
	b.dt_procedimento,
	b.dt_procedimento_trunc,
	b.cd_guia_referencia cd_guia_referencia,
	b.cd_medico_executor cd_medico,
	b.nr_crm_exec nr_crm,
	null nr_cpf,
	null cd_grau_partic_imp,
	b.nr_seq_grau_partic,
	b.nr_seq_prestador_exec nr_seq_prestador,
	b.nr_seq_segurado
from	pls_conta_proc_v b
where	b.ie_status 		<> 'D'
and	not exists	(select	1
			from	pls_proc_participante x
			where	x.nr_seq_conta_proc = b.nr_sequencia);
/