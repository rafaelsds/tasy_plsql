create or replace
view HAOC_Laudo_paciente_v as
select	NR_ATENDIMENTO,                 
	NM_USUARIO, 
	CD_MEDICO_RESP,                 
	DS_TITULO_LAUDO,                
	DT_LAUDO,                       
	NR_PRESCRICAO,                  
	DS_LAUDO,
	nr_laudo,
	dt_exame,
	dt_liberacao
from	laudo_paciente;
/