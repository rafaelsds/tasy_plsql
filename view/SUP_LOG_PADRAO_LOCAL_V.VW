create or replace view sup_log_padrao_local_v as
select	nm_tabela,
	nr_sequencia nr_seq_log,
	substr('Local: ' || obter_desc_local_estoque(somente_numero(substr(substr(ds_chave_composta,18,255),1,instr(substr(ds_chave_composta,18,255),'#')-1))) || ' - Material: ' || somente_numero(substr(ds_chave_composta,instr(ds_chave_composta,'CD_MATERIAL')+12,255)) || ' - ' || obter_desc_material(somente_numero(substr(ds_chave_composta,instr(ds_chave_composta,'CD_MATERIAL')+12,255))),1,255)  ds_historico, 
	somente_numero(substr(substr(ds_chave_composta,18,255),1,instr(substr(ds_chave_composta,18,255),'#')-1)) cd_local_estoque,
	somente_numero(substr(ds_chave_composta,instr(ds_chave_composta,'CD_MATERIAL')+12,255)) cd_material,
	dt_atualizacao,
	nm_usuario
from	tasy_log_alteracao a
where	nm_tabela = 'PADRAO_ESTOQUE_LOCAL'
union all
select	a.nm_tabela,
	a.nr_sequencia nr_seq_log,
	substr('Seq: ' || somente_numero(a.ds_chave_simples) || ' - Local: ' || obter_desc_local_estoque(b.cd_local_estoque) || ' - Material: ' || b.cd_material || ' - '||obter_desc_material(b.cd_material),1,255) ds_historico,
	b.cd_local_estoque,
	b.cd_material,
	a.dt_atualizacao,
	a.nm_usuario
from	tasy_log_alteracao a,
	loc_estoque_estrut_materiais b
where	a.nm_tabela = 'LOC_ESTOQUE_ESTRUT_MATERIAIS'
and	somente_numero(a.ds_chave_simples) = b.nr_sequencia(+)
union all
select	a.nm_tabela,
	a.nr_sequencia nr_seq_log,
	substr('Local: ' || obter_desc_local_estoque(b.cd_local_estoque),1,255) ds_historico,
	b.cd_local_estoque,
	null cd_material,
	a.dt_atualizacao,
	a.nm_usuario
from	tasy_log_alteracao a,
	local_estoque_crit_inv b
where	nm_tabela = 'LOCAL_ESTOQUE_CRIT_INV'
and	somente_numero(a.ds_chave_simples) = b.nr_sequencia(+)
union all
select	a.nm_tabela,
	a.nr_sequencia nr_seq_log,
	substr('Local: ' || obter_desc_local_estoque(b.cd_local_estoque) || ' - Material: ' || b.cd_material || ' - '||obter_desc_material(b.cd_material),1,255) ds_historico,
	b.cd_local_estoque,
	b.cd_material,
	a.dt_atualizacao,
	a.nm_usuario
from	tasy_log_alteracao a,
	localizacao_estoque_local b
where	nm_tabela = 'LOCALIZACAO_ESTOQUE_LOCAL'
and	somente_numero(a.ds_chave_simples) = b.nr_sequencia(+)
union all
select	a.nm_tabela,
	a.nr_sequencia nr_seq_log,
	substr('Local: ' || obter_desc_local_estoque(b.cd_local_estoque) || ' - Material: ' || b.cd_material || ' - '||obter_desc_material(b.cd_material),1,255) ds_historico,
	b.cd_local_estoque,
	b.cd_material,
	a.dt_atualizacao,
	a.nm_usuario
from	tasy_log_alteracao a,
	regra_ressup_estab b
where	nm_tabela = 'REGRA_RESSUP_ESTAB'
and	somente_numero(a.ds_chave_simples) = b.nr_sequencia(+);
/