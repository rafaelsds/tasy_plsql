create or replace
view SUS_MUNICIPIO_V as
select	cd_municipio_ibge,
	ds_municipio,
	ds_unidade_federacao
from	sus_municipio
where      1 = 1
order by 2;
/