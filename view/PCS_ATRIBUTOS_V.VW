create or replace view PCS_ATRIBUTOS_V as
select	'CD_EMPRESA' cd_dominio,
	'� o c�digo da empresa' ds_observacao
from	dual	
union all
select	'DS_EMPRESA' cd_dominio,
	'� a descri��o da empresa' ds_observacao
from	dual	
union all
select	'CD_GRUPAMENTO' cd_dominio,
	'� o c�digo do grupamento' ds_observacao
from	dual	
union all	
select	'DS_GRUPAMENTO' cd_dominio,
	'� a descri��o do grupamento' ds_observacao
from	dual	
union all	
select	'CD_SEGMENTO' cd_dominio,
	'� o c�digo do segmento' ds_observacao
from	dual	
union all	
select 	'DS_SEGMENTO' cd_dominio,
	'� a descri��o do segmento' ds_observacao
from	dual	
union all
select 	'CD_MATERIAL' cd_dominio,
	'� o c�digo do material' ds_observacao
from	dual	
union all
select 	'DS_MATERIAL' cd_dominio,
	'� a descri��o do material' ds_observacao
from	dual	
union all
select 	'CD_ESTAB' cd_dominio, --CD_ESTABELECIMENTO
	'� o c�digo do estabelecimento' ds_observacao
from	dual	
union all
select 	'CD_CONDICAO_PAG' cd_dominio, --CD_CONDICAO_PAG
	'Condi��o de pagamento padr�o ao gerar transfer�ncia' ds_observacao
from	dual	
union all
select 	'DS_ESTAB' cd_dominio,
	'� a descri��o do estabelecimento' ds_observacao
from	dual	
union all
select 	'CD_REGIONAL' cd_dominio,
	'� o c�digo da regional' ds_observacao
from	dual	
union all	
select 	'DS_REGIONAL' cd_dominio,
	'� a descri��o da regional' ds_observacao
from	dual	
union all	
select 	'DT_EMISSAO_OC' cd_dominio,
	'� a data de emiss�o da ordem de compra pendente de entrega mais antiga. Refere-se ao campo �Dt. emiss�o� da lista Tasy.' ds_observacao
from	dual	
union all	
select 	'NR_ORDEM_COMPRA' cd_dominio,
	'� o n�mero da ordem de compra pendente de entrega mais antiga. Refere-se ao campo �Ordem compra� da lista Tasy.' ds_observacao
from	dual	
union all	
select 	'IE_OC_APROVADA' cd_dominio,
	'Identifica se a ordem de compra pendente de entrega mais antiga est� aprovada.  Refere-se ao campo �OC aprovada� da lista Tasy.' ds_observacao
from	dual	
union all
select 	'IE_TIPO_COMPRA' cd_dominio,
	'Tipo da solicita��o' ds_observacao
from	dual
union all	
select 	'DS_FORNEC_OC' cd_dominio,
	'� o fornecedor (PJ) da ordem de compra pendente de entrega mais antiga.  Refere-se ao campo �PJ ordem compra� da lista Tasy' ds_observacao
from	dual	
union all	
select 	'NM_PESSOA_FISIC' cd_dominio,
	'� o fornecedor (PF) da ordem de compra pendente de entrega mais antiga. Refere-se ao campo �PF ordem compra� da lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_PEDIDO' cd_dominio,
	'� a quantidade pendente da ordem de compra mais antiga. Se n�o tiver ordem de compra, � a quantidade da solicita��o de compras pendente mais antiga. Refere-se ao campo �Qt. pedido� da lista Tasy' ds_observacao
from	dual	
union all
select 	'DT_PREV_ENTREGA' cd_dominio,
	'� a data prevista de entrega da ordem de compra pendente de entrega mais antiga. Refere-se ao campo �Dt. entrega� da lista Tasy' ds_observacao
from	dual	
union all
select 	'NM_COMPRADOR' cd_dominio,
	'� o nome do comprador da ordem de compra pendente de entrega mais antiga. Refere-se ao campo �Comprador� da lista Tasy' ds_observacao
from	dual	
union all	
select 	'CDA' cd_dominio,
	'� a quantidade consumida no dia anterior. Refere-se ao campo �CDA� da lista Tasy' ds_observacao
from	dual	
union all	
select 	'CD07' cd_dominio,
	'� a quantidade consumida nos �ltimos 7 dias. Refere-se ao campo �CD07� da lista Tasy' ds_observacao
from	dual	
union all	
select 	'CD15' cd_dominio,
	'� a quantidade consumida nos �ltimos 15 dias. Refere-se ao campo �CD15� da lista Tasy' ds_observacao
from	dual	
union all	
select 	'CD30' cd_dominio,
	'� a quantidade consumida nos �ltimos 30 dias. Refere-se ao campo �CD30� da lista Tasy' ds_observacao
from	dual	
union all	
select 	'CD60' cd_dominio,
	'� a quantidade consumida nos �ltimos 60 dias. Refere-se ao campo �CD60� da lista Tasy' ds_observacao
from	dual	
union all
select 	'CD90' cd_dominio,
	'� a quantidade consumida nos �ltimos 90 dias. Refere-se ao campo �CD90� da lista Tasy' ds_observacao
from	dual	
union all
select 	'CD120' cd_dominio,
	'� a quantidade consumida nos �ltimos 120 dias. Refere-se ao campo �CD120� da lista Tasy' ds_observacao
from	dual	
union all
select 	'CD150' cd_dominio,
	'� a quantidade consumida nos �ltimos 150 dias. Refere-se ao campo �CD150� da lista Tasy' ds_observacao
from	dual	
union all
select 	'CD180' cd_dominio,
	'� a quantidade consumida nos �ltimos 180 dias. Refere-se ao campo �CD180� da lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_MAIOR_CONS_3' cd_dominio,
	'� a maior m�dia mensal consumida do material nos �ltimos 3 meses. Refere-se ao campo �Maior consumo 3M� da lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_COBERTURA_AX' cd_dominio,
	'� a quantidade de dias que tem de cobertura nos locais de estoque do tipo �Almoxarifado�. Refere-se ao campo �Cobertura almox.� da lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_COBERTURA_FM' cd_dominio,
	'� a quantidade de dias que tem de cobertura nos locais de estoque do tipo �Farm�cia�. Refere-se ao campo �Cobertura farm�cias.� da lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_COB_REGRA' cd_dominio,
	'Cobertura de acordo com os locais cadastrados na regra do PCS' ds_observacao
from	dual	
union all
select 	'QT_SALDO_ALMOX' cd_dominio,
	'� o saldo em estoque nos locais de estoque do tipo seja �Almoxarifado.� Refere-se ao campo �Saldo almox.� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_SALDO_FARM' cd_dominio,
	'� a quantidade em estoque nos locais cujo o tipo seja �Farm�cia�. Refere-se ao campo �Saldo farm�cias.� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_SALDO_TOTAL' cd_dominio,
	'� a soma do saldo do material em todos os locais de estoque. Refere-se ao campo �Total unidade� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'VL_SALDO_TOTAL' cd_dominio,
	'� o valor total em estoque de todos os locais de estoque. Refere-se ao campo �Estoque total R$� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'VL_SALDO_REGRA' cd_dominio, 
	'Para cada item da Lista Tasy multiplicar o Saldo Estoque Regra pelo custo m�dio j� calculado no campo atual Vl. Custo M�dio.' ds_observacao	
from	dual
union all
select 	'QT_SALDO_REGRA' cd_dominio, 
	'Para cada item da Lista Tasy buscar o saldo somente dos locais de estoque da regra do grupamento ao qual o item pertence.' ds_observacao	
from	dual
union all
select 	'QT_ORDENS_TOTAL' cd_dominio,
	'� a quantidade do material que est� pendente em ordens de compras. Refere-se ao campo �Ordens total� que tem na lista Tasy.' ds_observacao
from	dual	
union all
select 	'VL_CUSTO_MEDIO' cd_dominio,
	'� o valor de custo m�dio do material. Refere-se ao campo �Custo m�dio� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_MELHOR_COMPR' cd_dominio,
	'� o lote m�ltiplo para melhor compra do material. Refere-se ao campo �Unidade compra� que tem na lista Tasy.' ds_observacao
from	dual	
union all
select 	'QT_LEAD_TIME' cd_dominio,
	'� o tempo estipulado (em dias) para a entrega deste material. Refere-se ao campo �Lead Time� que tem na lista Tasy.' ds_observacao
from	dual	
union all
select 	'QT_ESTOQUE_MIN' cd_dominio,
	'� a quantidade m�nima que este produto deve ter em estoque. Refere-se ao campo �Est. M�nimo� que tem na lista Tasy.' ds_observacao
from	dual	
union all
select 	'DT_INICIO_FALTA' cd_dominio,
	'� a data em que o material est� em falta. Refere-se ao campo �Dt. in�cio falta� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'IE_FALTA_SOLUCI' cd_dominio,
	'Identifica se a falta do material j� foi solucionada. Refere-se ao campo �Falta solucionada� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'DS_MOTIVO_FALTA' cd_dominio,
	'� a descri��o do motivo que o material est� em falta. Refere-se ao campo �Motivo falta� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_SALDO_EXT' cd_dominio,
	'� o saldo em estoque nos locais de estoque que s�o Externos. Refere-se ao campo �Saldo local externo� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'DS_MODALIDADE' cd_dominio,
	'� a descri��o de qual modalidade o material se encaixa (Mercado ou Contrato). Refere-se ao campo �Modalidade� que tem na lista Tasy.' ds_observacao
from	dual	
union all
select 	'VL_FATUR_MINIMO' cd_dominio,
	'� o valor de faturamento m�nimo do contrato onde encontra-se o material. Refere-se ao campo �Vl fat m�nimo� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'DS_FORNEC_CONTR' cd_dominio,
	'� a descri��o do fornecedor do contrato onde encontra-se o material. Refere-se ao campo �Fornecedor� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'NM_COMPRA_REGRA' cd_dominio,
	'� o nome do comprador respons�vel pela estrutura do material, conforme definido na �Regras respons�vel compras�. Refere-se ao campo �Comprador regra� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'NR_SOLIC_COMPRA' cd_dominio,
	'� o n�mero da solicita��o de compra pendente de entrega mais antiga. Refere-se ao campo �Solic. compra� da lista Tasy.' ds_observacao
from	dual	
union all
select 	'QT_PENDENTE_OC' cd_dominio,
	'� a quantidade total pendente em ordens de compra que ainda n�o foram entregues. Refere-se ao campo �Qt. Pendente OC� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'NR_DOCUMENTO' cd_dominio,
	'� o n�mero da ordem de compra pendente de entrega, ou o n�mero da solicita��o de compras pendente de entrega. Leva em considera��o o que for mais antigo. Refere-se ao campo �Nr. Documento� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'TP_DOCUMENTO' cd_dominio,
	'Identifica se o documento mais antigo � uma Ordem de compra ou um Solicita��o de compra. Refere-se ao campo �Tipo documento� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_PEND_DOC' cd_dominio,
	'� a quantidade pendente de entrega da ordem de compra ou da solicita��o. Refere-se ao campo �Qt. Pendente� que tem na lista Tasy' ds_observacao
from	dual	
union all
select 	'QT_POLITICA' cd_dominio,
	'� a pol�tica informada na estrutura do grupamento ou do pr�prio grupamento.' ds_observacao
from	dual	
union all
select 	'QT_FREQUENCIA' cd_dominio,
	'� a frequ�ncia informada na estrutura do grupamento ou do pr�prio grupamento.' ds_observacao
from	dual	
union all
select 	'QT_MARGEM_DIAS' cd_dominio,
	'� a margem de dias informada na estrutura do grupamento ou do pr�prio grupamento.' ds_observacao
from	dual	
union all
select 	'QT_MIN_ESTOQUE' cd_dominio,
	'� a quantidade m�nima de estoque informada na estrutura do segmento' ds_observacao
from	dual	
union all
select 	'QT_MAX_ESTOQUE' cd_dominio,
	'� a quantidade m�xima de estoque informada na estrutura do segmento' ds_observacao
from	dual
union all
select 	'CD_LOCAL_EST' cd_dominio, --CD_LOCAL_EST
	'Local de estoque da solicita��o' ds_observacao
from	dual
union all
select 	'NR_SEQ_MOTIVO_SOLIC' cd_dominio,
	'Motivo da solicita��o' ds_observacao
from	dual
union all
select 	'QT_MATERIAL' cd_dominio, --QT_COMPRAS
	'Quantidade de compra da solicita��o' ds_observacao
from	dual
union all
select 	'QT_TRANSF' cd_dominio, --QT_transf
	'Quantidade informada para transfer�ncia' ds_observacao
from	dual
union all
select 	'QT_EMPRESTIMO' cd_dominio, --QT_transf
	'Quantidade informada para o empr�stimo' ds_observacao
from	dual
union all
select 	'VL_INTERVALO_SUP_Z' cd_dominio, 
	'Intervalo de confian�a superior Z' ds_observacao
from 	dual
union all
select 	'VL_INTERVALO_INF_Z' cd_dominio, 
	'Intervalo de confian�a inferior Z' ds_observacao
from 	dual
union all
select 	'TESTE_Z' cd_dominio, 
	'C�lculo do TESTE Z' ds_observacao	
from	dual
union all
select 	'VL_INTERVALO_SUP_T' cd_dominio, 
	'Intervalo de confian�a superior T' ds_observacao
from 	dual
union all
select 	'VL_INTERVALO_INF_T' cd_dominio, 
	'Intervalo de confian�a inferior T' ds_observacao
from 	dual
union all
select 	'TESTE_T' cd_dominio, 
	'C�lculo do TESTE T' ds_observacao	
from	dual
union all
select distinct 'QT_SALDO_' || CD_LOCAL_ESTOQUE ||'_' || substr(replace(OBTER_DESC_LOCAL_ESTOQUE(CD_LOCAL_ESTOQUE),' ','_'),1,15) cd_dominio,
	'Saldo dispon�vel do local de estoque ' || cd_local_estoque || ' - ' || substr(obter_desc_local_estoque(cd_local_estoque),1,20)  ds_observacao
from   pcs_local_estoque_grup;
/
