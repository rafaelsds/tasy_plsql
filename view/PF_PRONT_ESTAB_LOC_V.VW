CREATE OR REPLACE VIEW PF_PRONT_ESTAB_LOC_V AS
SELECT a.cd_pessoa_fisica,
       a.nm_pessoa_fisica_sem_acento,
       decode(obter_valor_param_usuario(0, 120, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'ESTAB', b.nr_prontuario, a.nr_prontuario) nr_prontuario,
       a.dt_nascimento,
       a.ds_apelido,
       a.cd_funcionario,
       NVL(a.ie_funcionario, 'N') ie_funcionario,
       NVL(a.ie_funcionario, 'N') ie_func_grid,
       SUBSTR(Obter_se_func_assistencial(a.cd_pessoa_fisica), 1, 10) ie_func_assistencial,
       SUBSTR(Obter_PESSOA_FISICA_LOC_TRAB(a.cd_pessoa_fisica), 1, 200) ds_setor_atendimento,
       SUBSTR(Obter_se_func_assistencial(a.cd_pessoa_fisica), 1, 10) ie_func_assist_grid,
       obter_dados_ultimo_atend_dt(a.cd_pessoa_fisica, NULL, 1, 'DA') dt_ultima_alta,
       a.nr_identidade
  FROM pessoa_fisica a left join pessoa_fisica_pront_estab b on (a.cd_pessoa_fisica = b.cd_pessoa_fisica
                                                              and wheb_usuario_pck.get_cd_estabelecimento = b.cd_estabelecimento);
/