create or replace view pls_itens_pendentes_v
AS 	select	a.nr_sequencia,
	a.nr_seq_mens_seg,
	b.dt_mesano_referencia,
	b.nr_seq_segurado,
	e.nm_pessoa_fisica,
	b.qt_idade,
	b.nr_parcela nr_parcela_benef,
	b.nr_parcela_contrato,
	b.vl_mensalidade,
	b.vl_pre_estabelecido vl_pre_estab,
	a.vl_item_comissao,
	a.vl_repasse,
	a.dt_liberacao,
	a.nm_usuario_lib,
	d.nr_seq_contrato,
	f.nr_contrato,
	(	select	x.ds_razao_social
		from	pessoa_juridica x
		where	x.cd_cgc = f.cd_cgc_estipulante
		union all
		select	x.nm_pessoa_fisica
		from	pessoa_fisica x
		where	x.cd_pessoa_fisica = f.cd_pf_estipulante) nm_estipulante,
	(	select	x.ds_razao_social
		from	pessoa_juridica x
		where	x.cd_cgc = g.cd_cgc
		union all
		select	x.nm_pessoa_fisica
		from	pessoa_fisica x
		where	x.cd_pessoa_fisica = g.cd_pessoa_fisica) nm_pagador,
	(	select	x.ds_razao_social
		from	pessoa_juridica x,
			pls_sub_estipulante y
		where	x.cd_cgc = y.cd_cgc
		and	y.nr_sequencia = d.nr_seq_subestipulante
		union all
		select	x.nm_pessoa_fisica
		from	pessoa_fisica x,
			pls_sub_estipulante y
		where	x.cd_pessoa_fisica = y.cd_pessoa_fisica
		and	y.nr_sequencia = d.nr_seq_subestipulante) nm_subestipulante,
	h.ds_plano,
	(	select	x.ds_plano
		from	pls_plano x
		where	x.nr_sequencia = a.nr_seq_plano_sca) nm_sca,
	substr(obter_valor_dominio(2115,d.ie_acao_contrato),1,200) ds_acao_contrato,
	decode(f.cd_cgc_estipulante,null,'PF','PJ') ie_tipo_contrato,
	d.dt_contratacao dt_adesao_contrato,
	i.dt_liquidacao,
	a.dt_cancelamento,
	a.nm_usuario_cancelamento,
	(	select	a.nr_seq_vendedor_resp||' - '||y.nm_pessoa_fisica
		from	pls_vendedor x,
			pessoa_fisica y
		where	x.cd_pessoa_fisica = y.cd_pessoa_fisica
		and	x.nr_sequencia	= a.nr_seq_vendedor_resp
		union all
		select	a.nr_seq_vendedor_resp||' - '||y.ds_razao_social
		from	pls_vendedor x,
			pessoa_juridica y
		where	x.cd_cgc = y.cd_cgc
		and	x.nr_sequencia	= a.nr_seq_vendedor_resp) nm_vendedor_resp,
	(	select	y.nm_pessoa_fisica
		from	pls_vendedor_vinculado x,
			pessoa_fisica y
		where	x.cd_pessoa_fisica = y.cd_pessoa_fisica
		and	x.nr_sequencia	= d.nr_seq_vendedor_pf) nm_vendedor_vinculado,
	(	select	decode(count(1),0,'N�o','Sim')
		from	pls_carencia x
		where	x.nr_seq_segurado = d.nr_sequencia
		and	x.ie_cpt = 'S') ie_cpt,
	substr(pls_obter_vidas_contr_vend(a.nr_seq_vendedor,a.nr_seq_contrato,d.dt_contratacao),1,10) qt_vidas_contrato,
	b.nr_seq_mensalidade,
	c.nr_seq_lote,
	i.nr_titulo,
	a.nr_seq_regra_mensal,
	i.ie_situacao,
	decode(a.ie_cancelamento,'C','Cancelada','E','Estorno') ds_situacao,
	substr(obter_valor_dominio(710,i.ie_situacao),1,100) ds_situacao_titulo,
	substr(pls_obter_status_repasse(a.nr_sequencia),1,255) ie_status_repasse_legenda,
	substr(a.ds_observacao,1,255) ds_observacao,
	sum(a.vl_repasse) OVER (order by a.nr_sequencia ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) vl_total_pendente,
	sum(decode(a.dt_liberacao,null,a.vl_repasse,0)) OVER (order by a.nr_sequencia ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) vl_total_aberto,
	sum(decode(a.dt_liberacao,null,0,a.vl_repasse)) OVER (order by a.nr_sequencia ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) vl_total_liberado
from	pls_repasse_mens a,
	pls_mensalidade_segurado b,
	pls_mensalidade c,
	pls_segurado d,
	pessoa_fisica e,
	pls_contrato f,
	pls_contrato_pagador g,
	pls_plano h,
	titulo_receber i
where	b.nr_sequencia	= a.nr_seq_mens_seg
and	c.nr_sequencia	= b.nr_seq_mensalidade
and	d.nr_sequencia	= b.nr_seq_segurado
and	e.cd_pessoa_fisica = d.cd_pessoa_fisica
and	f.nr_sequencia	= d.nr_seq_contrato
and	g.nr_sequencia	= c.nr_seq_pagador
and	h.nr_sequencia	= d.nr_seq_plano
and	c.nr_sequencia	= i.nr_seq_mensalidade(+)
and	a.nr_seq_repasse is null
and	a.nr_seq_comissao_benef is null;
/
