create or replace
view pls_resumo_proposta_pf_v as
select	ie_reg,
	substr(ds_descricao,1,255) ds_descricao,
	vl_resumo,
	nr_seq_proposta,
	nr_seq_benef
from(	select	1 ie_reg,
		substr(obter_nome_pf(cd_beneficiario),1,255) ||' - ' ||substr(obter_idade_pf(b.cd_beneficiario,a.dt_inicio_proposta,'A'),1,3) || ' anos - ' || substr(decode(b.nr_seq_titular,null,decode(b.nr_seq_titular_contrato,null,'Titular','Dependente'),'Dependente'),1,50) ds_descricao,
		to_number(substr(pls_obter_total_proposta_benef(b.nr_sequencia),1,255)) vl_resumo,
		a.nr_sequencia nr_seq_proposta,
		b.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario	b,
		pls_proposta_adesao		a
	where	b.nr_seq_proposta	= a.nr_sequencia
	and	b.dt_cancelamento is null
	union
	select	2 ie_reg,
		'     Produto - '||substr(pls_obter_dados_produto(a.nr_seq_plano,'N'),1,255) ds_descricao,
		to_number(substr(pls_obter_valores_propostas(a.nr_sequencia,0,'P'),1,255)) vl_resumo,
		b.nr_sequencia nr_seq_proposta,
		a.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario	a,
		pls_proposta_adesao		b
	where	a.nr_seq_proposta = b.nr_sequencia
	and	a.dt_cancelamento is null
	and	b.ie_tipo_proposta <> 5
	union
	select	3 ie_reg,
		'     SCA - '||substr(pls_obter_dados_produto(a.nr_seq_plano,'N'),1,255) ds_descricao,
		to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,a.nr_sequencia,'S'),1,255)) vl_resumo,
		c.nr_sequencia nr_seq_proposta,
		b.nr_sequencia	nr_seq_benef
	from	pls_sca_vinculo			a,
		pls_proposta_beneficiario	b,
		pls_proposta_adesao		c
	where	a.nr_seq_benef_proposta	= b.nr_sequencia
	and	b.nr_seq_proposta	= c.nr_sequencia
	and	b.dt_cancelamento is null
	union
	select	4 ie_reg,
		'     '|| wheb_mensagem_pck.get_texto(1126906) ||' - '||substr(pls_obter_dados_bonificacao(a.nr_sequencia,'D'),1,255) ds_descricao, --Bonificacao
		to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,a.nr_seq_bonificacao,'B'),1,255)) vl_resumo,
		c.nr_sequencia nr_seq_proposta,
		b.nr_sequencia	nr_seq_benef
	from	pls_bonificacao_vinculo		a,
		pls_proposta_beneficiario	b,
		pls_proposta_adesao		c
	where	a.nr_seq_segurado_prop	= b.nr_sequencia
	and	b.nr_seq_proposta	= c.nr_sequencia
	and	b.dt_cancelamento is null
	union all
	select	5 ie_reg,
		'     '|| wheb_mensagem_pck.get_texto(1126907) ||' - '||substr(pls_obter_dados_bonificacao(a.nr_sequencia,'D'),1,255) ds_descricao, --Bonificacao da proposta
		to_number(substr(pls_obter_valor_bonif_tot_prop(a.nr_seq_proposta,a.nr_seq_bonificacao,b.nr_sequencia),1,15)) vl_resumo,
		c.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_bonificacao_vinculo			a,
		pls_proposta_beneficiario		b,
		pls_proposta_adesao			c
	where	a.nr_seq_proposta			= c.nr_sequencia
	and	b.nr_seq_proposta			= c.nr_sequencia
	union
	select	12 ie_reg,
		'     '|| wheb_mensagem_pck.get_texto(1126908) ||' - '||substr(pls_obter_dados_bonificacao(a.nr_sequencia,'D'),1,255) ds_descricao, --Bonificacao do contrato
		to_number(substr(pls_obter_valor_bonif_tot_prop(c.nr_sequencia,a.nr_seq_bonificacao,b.nr_sequencia),1,15)) vl_resumo,
		c.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_bonificacao_vinculo			a,
		pls_proposta_beneficiario		b,
		pls_proposta_adesao			c,
		pls_contrato				d
	where	a.nr_seq_contrato			= d.nr_sequencia
	and	b.nr_seq_proposta			= c.nr_sequencia
	and	c.nr_seq_contrato			= d.nr_contrato
	union	
	select	6 ie_reg,
		'     ' || wheb_mensagem_pck.get_texto(1126905) ds_descricao, --Taxa de inscricao (Proposta)
		to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'I'),1,255)) vl_resumo,
		a.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	b.nr_seq_proposta			= a.nr_sequencia
	and	a.ie_tipo_proposta			<> 9
	and	b.ie_taxa_inscricao			= 'S'
	and	to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'I'),1,255)) > 0
	union
	select  7 ie_reg,
		'     ' || wheb_mensagem_pck.get_texto(1126904) ds_descricao, --Taxa de inscricao (Produto)
		pls_obter_valores_propostas(b.nr_sequencia,0,'IP'),
		a.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	b.nr_seq_proposta			= a.nr_sequencia
	and	a.ie_tipo_proposta			<> 9
	and	b.ie_taxa_inscricao			= 'S'
	and	to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'IP'),1,255)) > 0
	and	not exists(	select	1
				from	pls_regra_inscricao x
				 where	x.nr_seq_proposta  = b.nr_seq_proposta)
	and	not exists(	select	1
				from	pls_regra_inscricao	x,
					pls_contrato		y
				 where	x.nr_seq_contrato	= y.nr_sequencia
				 and	a.nr_seq_contrato	= y.nr_contrato)
	union
	select  8 ie_reg,
		'     ' || wheb_mensagem_pck.get_texto(1126903) ds_descricao, --Taxa adapatacao do produto
		pls_obter_valores_propostas(b.nr_sequencia,0,'AP'),
		a.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	b.nr_seq_proposta			= a.nr_sequencia
	and	to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'AP'),1,255)) > 0	
	union
	select  9 ie_reg,
		'     SCA do Contrato - ' || substr(pls_obter_dados_produto(d.nr_seq_plano,'N'),1,255) ds_descricao,
		pls_obter_valores_propostas(b.nr_sequencia,d.nr_seq_plano,'SC') vl_resumo,
		a.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_sca_regra_contrato			d,
		pls_contrato				c,
		pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	d.nr_seq_contrato			= c.nr_sequencia
	and	c.nr_contrato				= a.nr_seq_contrato
	and	b.nr_seq_proposta			= a.nr_sequencia
	and	a.dt_inicio_proposta between nvl(dt_inicio_vigencia,a.dt_inicio_proposta) and fim_dia(nvl(dt_fim_vigencia,a.dt_inicio_proposta))
	and	((d.ie_geracao_valores = 'T' and b.nr_seq_titular is null and b.nr_seq_titular_contrato is null) or
		((d.ie_geracao_valores = 'D' and ((b.nr_seq_titular is not null) or (b.nr_seq_titular_contrato is not null)))) or
		 (d.ie_geracao_valores = 'B'))
	and	((d.nr_seq_plano_benef	= b.nr_seq_plano) or (d.nr_seq_plano_benef is null))
	and	not exists	(	select	1
					from	pls_sca_vinculo		x
					where	x.nr_seq_benef_proposta	= b.nr_sequencia
					and	d.nr_seq_plano		= x.nr_seq_plano)
	union
	select	10 ie_reg,
		'     ' || wheb_mensagem_pck.get_texto(1126902) ds_descricao, --Taxa de inscricao (Contrato)
		to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'IC'),1,255)) vl_resumo,
		a.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	b.nr_seq_proposta			= a.nr_sequencia
	and	a.ie_tipo_proposta			<> 9
	and	b.ie_taxa_inscricao			= 'S'
	and	to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'IC'),1,255)) > 0
	and	not exists(	select	1
				from	pls_regra_inscricao x
				 where	x.nr_seq_proposta	= b.nr_seq_proposta)
	union
	select	11 ie_reg,
		'     ' || wheb_mensagem_pck.get_texto(1126901) ds_descricao, --Via do cartao de identificacao
		to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'VC'),1,255)) vl_resumo,
		a.nr_sequencia,
		b.nr_sequencia	nr_seq_benef
	from	pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	b.nr_seq_proposta			= a.nr_sequencia
	and	a.ie_tipo_proposta			not in (5,9)
	and	b.dt_cancelamento is null
	and	to_number(substr(pls_obter_valores_propostas(b.nr_sequencia,0,'VC'),1,255)) > 0
);
/