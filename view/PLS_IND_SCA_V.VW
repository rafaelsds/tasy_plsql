create or replace view pls_ind_sca_v as 
select	decode(a.cd_pf_estipulante,null,'PESSOA JUR�DICA','PESSOA F�SICA') ds_estipulante,
	substr(obter_nome_pf_pj(a.cd_pf_estipulante, a.cd_cgc_estipulante),1,200) nm_estipulante,	
	d.nr_seq_plano,
	substr(pls_obter_dados_produto(d.nr_seq_plano,'N'),1,255) ds_tipo_plano,
	d.nr_seq_vendedor_canal,
	substr(pls_obter_nomes_contrato(d.nr_seq_vendedor_canal,'VC'),1,255) nm_canal_venda,
	d.nr_seq_vendedor_pf,
	substr(pls_obter_nomes_contrato(d.nr_seq_vendedor_pf,'V'),1,255) nm_vendedor,	
	c.cd_pessoa_fisica,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1,'DSM'),1,255) ds_municipio,
	c.dt_rescisao dt_rescisao_benef,
	c.dt_contratacao dt_adesao_benef,
	d.dt_inicio_vigencia dt_inicio_vigencia,
	'1' ie_mov_nova_venda -- Novas vendas
from	pls_contrato		a,
	pls_segurado		c,
	pls_sca_vinculo		d
where	a.nr_sequencia = c.nr_seq_contrato
and	c.nr_sequencia = d.nr_seq_segurado
and	c.dt_liberacao is not null
and	c.ie_acao_contrato in ('A','S')
union all
select	decode(a.cd_pf_estipulante,null,'PESSOA JUR�DICA','PESSOA F�SICA') ds_estipulante,
	substr(obter_nome_pf_pj(a.cd_pf_estipulante, a.cd_cgc_estipulante),1,200) nm_estipulante,	
	d.nr_seq_plano,
	substr(pls_obter_dados_produto(d.nr_seq_plano,'N'),1,255) ds_tipo_plano,
	d.nr_seq_vendedor_canal,
	substr(pls_obter_nomes_contrato(d.nr_seq_vendedor_canal,'VC'),1,255) nm_canal_venda,
	d.nr_seq_vendedor_pf,
	substr(pls_obter_nomes_contrato(d.nr_seq_vendedor_pf,'V'),1,255) nm_vendedor,	
	c.cd_pessoa_fisica,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1,'DSM'),1,255) ds_municipio,
	c.dt_rescisao dt_rescisao_benef,
	c.dt_contratacao dt_adesao_benef,
	d.dt_inicio_vigencia dt_inicio_vigencia,
	'2' ie_mov_nova_venda -- Movimenta��es
from	pls_contrato		a,
	pls_segurado		c,
	pls_sca_vinculo		d
where	a.nr_sequencia = c.nr_seq_contrato
and	c.nr_sequencia = d.nr_seq_segurado
and	c.dt_liberacao is not null
and	c.ie_acao_contrato in ('M','L','D');
/