CREATE OR REPLACE VIEW CID_DOENCA_JAPAN_V AS
SELECT DISTINCT cij.nr_sequencia nr_sequencia,
         cij.cd_doenca_mdfy cd_doenca_mdfy,
         cij.ds_doenca_mdfy ds_doenca_mdfy,
         cij. ds_doenca_katana ds_doenca_katana,
         cij.cd_doenca cd_doenca
   FROM cid_interno_japan cij;
/
