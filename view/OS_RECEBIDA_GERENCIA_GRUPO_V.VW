create or replace view os_recebida_gerencia_grupo_v as
select	a.nr_sequencia,
	b.nr_Seq_gerencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_localizacao,
	a.cd_funcao
from    grupo_desenvolvimento b,
	man_ordem_servico a
where   a.nr_seq_grupo_des = b.nr_sequencia (+)
and	exists ( select 1
		from	man_estagio_processo c,
			man_ordem_serv_estagio d
		where	c.nr_sequencia = d.nr_seq_estagio
		and	d.nr_seq_ordem = a.nr_sequencia
		and	((c.ie_desenv = 'S') or (c.ie_suporte = 'S')))
union
select		distinct
		a.nr_sequencia,
		2 nr_seq_gerencia,
		a.dt_ordem_servico,
		f.nr_seq_grupo,
		a.nr_seq_localizacao,
		a.cd_funcao
from		man_ordem_servico a,
		grupo_desenvolvimento b,
		man_ordem_serv_ativ d,
		grupo_desenvolvimento e,
		usuario_grupo_des f
where		b.nr_sequencia			= a.nr_seq_grupo_des
and		b.nr_seq_gerencia		!= 2
and		d.nr_seq_ordem_serv		= a.nr_sequencia
and		e.nr_seq_gerencia		= 2
and		f.nr_seq_grupo			= e.nr_sequencia
and		d.nm_usuario_exec		= f.nm_usuario_grupo;
/