create or replace view eis_escala_stratify_v  as 
select	distinct 
	eis_obter_setor_atend_data(a.nr_atendimento, e.dt_avaliacao) cd_setor_atendimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
	substr(obter_nome_convenio(obter_convenio_atendimento(a.nr_atendimento)),1,150) ds_convenio,
	obter_nome_setor(eis_obter_setor_atend_data(a.nr_atendimento, e.dt_avaliacao) ) ds_setor_atendimento,
	obter_sexo_pf(a.cd_pessoa_fisica,'C') ie_sexo,
	obter_sexo_pf(a.cd_pessoa_fisica,'D') ds_sexo,	
	a.cd_medico_resp,
	substr(obter_idade(obter_data_nascto_pf(a.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria,
	substr(obter_unidade_atend_data(a.nr_atendimento, e.dt_avaliacao),1,255) ds_unidade,
	trunc(e.dt_avaliacao) dt_avaliacao,
	e.qt_score,
	f.cd_empresa,
	a.cd_estabelecimento,
	substr(obter_desc_escala_stratify(e.qt_score),1,100) ds_gradacao,
	a.nr_atendimento,
	a.cd_pessoa_fisica cd_paciente,
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_paciente,
   	substr(obter_turno_data(dt_avaliacao),1,255) ie_turno,
	decode(obter_desc_escala_stratify(e.qt_score,'R'),'S',1,0) qt_risco,
	a.ie_tipo_atendimento,
	substr(Obter_Valor_Dominio(12,a.ie_tipo_atendimento),1,100) ds_tipo_atendimento
from 	escala_stratify e,
	estabelecimento f,
	atendimento_paciente a
where 	a.nr_atendimento= e.nr_atendimento
and	a.cd_estabelecimento = f.cd_estabelecimento
and	e.dt_liberacao is not null
and	e.dt_inativacao is null;
/