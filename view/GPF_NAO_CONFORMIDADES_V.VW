CREATE OR REPLACE VIEW gpf_nao_conformidades_v AS
	SELECT
		a.nr_prescricao,
		b.nr_sequencia,
		substr(obter_desc_material(a.cd_material), 1, 255) ds_material,
		b.ie_libera,
		substr(nvl(b.ds_erro, '') || ' ' || nvl(b.ds_compl_erro, ''), 1, 255) ds_erro_compl,
		nvl(b.ds_erro, '') || ' ' || nvl(b.ds_compl_erro, '') ds_erro_compl_desc,
		substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia), 1, 100) ds_dose_intervalo,
		substr(a.ds_justificativa, 1, 255) ds_justificativa,
		( a.ds_justificativa ) ds_justificativa_desc,
		substr(b.ds_inf_adicional, 1, 255) ds_observacao,
		( b.ds_inf_adicional ) ds_observacao_desc,
		substr(b.ds_compl_erro, 1, 255) ds_orientacao,
		( b.ds_compl_erro ) ds_orientacao_desc,
		b.dt_confirmacao,
		b.nm_usuario_ciente,
		substr(b.ds_motivo, 1, 255) ds_motivo,
		nvl(b.ie_ciente, 'N') ie_ciente,
		substr(c.ds_justificativa, 1, 255) ds_justificativa_prescr
	FROM
		prescr_material      a,
		prescr_medica_erro   b,
		prescr_medica        c
	WHERE
		a.nr_prescricao = b.nr_prescricao
		AND c.nr_prescricao = a.nr_prescricao
		AND b.nr_seq_medic = a.nr_sequencia
		AND a.ie_agrupador IN (1, 5)
		AND EXISTS (
			SELECT 1
			FROM regra_consiste_prescr d
			WHERE b.nr_regra = d.nr_sequencia
			AND nvl(ie_exibe_farmacia, 'S') = 'S'
		)
	UNION
	SELECT
		a.nr_prescricao,
		a.nr_sequencia,
		substr(obter_desc_material(a.cd_material), 1, 255) ds_material,
		'' ie_libera,
		substr(obter_descricao_padrao('RISCO_MEDICAMENTO', 'DS_RISCO', c.nr_seq_risco), 1, 255) ds_erro_compl,
		obter_descricao_padrao('RISCO_MEDICAMENTO', 'DS_RISCO', c.nr_seq_risco) ds_erro_compl_desc,
		substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia), 1, 100) ds_dose_intervalo,
		substr(a.ds_justificativa, 1, 255) ds_justificativa,
		( a.ds_justificativa ) ds_justificativa_desc,
		substr(c.ds_observacao, 1, 255) ds_observacao,
		( c.ds_observacao ) ds_observacao_desc,
		substr(c.ds_orientacao, 1, 255) ds_orientacao,
		( c.ds_orientacao ) ds_orientacao_desc,
		b.dt_confirmacao,
		b.nm_usuario_ciente,
		substr(b.ds_motivo, 1, 255) ds_motivo,
		nvl(b.ie_ciente, 'N') ie_ciente,
		'' ds_justificativa_prescr
	FROM
		prescr_material      a,
		prescr_medica_erro   b,
		medicamento_risco    c
	WHERE
		a.cd_material = c.cd_material
		AND a.ie_agrupador IN (1, 5)
		AND nvl(c.ie_nao_conformidade, 'N') = 'S'
		AND a.nr_prescricao = b.nr_prescricao
		AND b.nr_seq_medic = a.nr_sequencia
		AND nvl(c.ie_situacao, 'A') = 'A'
		AND EXISTS (
			SELECT 1
			FROM regra_consiste_prescr d
			WHERE b.nr_regra = d.nr_sequencia
			AND nvl(ie_exibe_farmacia, 'S') = 'S'
		)
	UNION
	SELECT
		a.nr_prescricao,
		a.nr_sequencia,
		substr(obter_desc_material(a.cd_material), 1, 255) ds_material,
		'' ie_libera,
		substr(obter_descricao_padrao('RISCO_MEDICAMENTO', 'DS_RISCO', c.nr_seq_risco), 1, 255) ds_erro_compl,
		obter_descricao_padrao('RISCO_MEDICAMENTO', 'DS_RISCO', c.nr_seq_risco) ds_erro_compl_desc,
		substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia), 1, 100) ds_dose_intervalo,
		substr(a.ds_justificativa, 1, 255) ds_justificativa,
		( a.ds_justificativa ) ds_justificativa_desc,
		substr(c.ds_observacao, 1, 255) ds_observacao,
		( c.ds_observacao ) ds_observacao_desc,
		substr(c.ds_orientacao, 1, 255) ds_orientacao,
		( c.ds_orientacao ) ds_orientacao_desc,
		b.dt_confirmacao,
		b.nm_usuario_ciente,
		substr(b.ds_motivo, 1, 255) ds_motivo,
		nvl(b.ie_ciente, 'N') ie_ciente,
		'' ds_justificativa_prescr
	FROM
		prescr_material       a,
		prescr_medica_risco   b,
		medicamento_risco     c
	WHERE
		a.cd_material = c.cd_material
		AND a.ie_agrupador IN (1, 5)
		AND nvl(c.ie_nao_conformidade, 'N') = 'S'
		AND a.nr_prescricao = b.nr_prescricao
		AND nvl(c.ie_situacao, 'A') = 'A'
	/* INTEGRACAO */
	UNION
	SELECT
		a.nr_prescricao,
		a.nr_sequencia,
		substr(obter_desc_material(a.cd_material), 1, 255) ds_material,
		'S' ie_libera,
		gpt_obter_nome_api('LXC', b.cd_api) ds_erro_compl,
		gpt_obter_nome_api('LXC', b.cd_api) ds_erro_compl_desc,
		substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia), 1, 100) ds_dose_intervalo,
		substr(a.ds_justificativa, 1, 255) ds_justificativa,
		( a.ds_justificativa ) ds_justificativa_desc,
		'' ds_observacao,
		'' ds_observacao_desc,
		substr(b.ds_resultado_curto, 1, 2000) ds_orientacao,
		( b.ds_resultado_curto ) ds_orientacao_desc,
		b.dt_confirmacao,
		b.nm_usuario_ciente,
		substr(b.ds_motivo, 1, 255) ds_motivo,
		nvl(b.ie_ciente, 'N') ie_ciente,
		'' ds_justificativa_prescr
	FROM
		prescr_material   a,
		alerta_api        b
	WHERE
		a.nr_prescricao = b.nr_prescricao
		AND a.nr_sequencia = b.nr_seq_material
		AND a.ie_agrupador IN (1, 4)
		AND nvl(b.ie_status_requisicao, 'XPTO') = 'XPTO'
	UNION
	SELECT
		a.nr_prescricao,
		a.nr_sequencia,
		substr(obter_desc_material(a.cd_material), 1, 255) ds_material,
		'S' ie_libera,
		gpt_obter_nome_api('LXC', b.cd_api) ds_erro_compl,
		gpt_obter_nome_api('LXC', b.cd_api) ds_erro_compl_desc,
		substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia), 1, 100) ds_dose_intervalo,
		substr(a.ds_justificativa, 1, 255) ds_justificativa,
		( a.ds_justificativa ) ds_justificativa_desc,
		'' ds_observacao,
		'' ds_observacao_desc,
		substr(b.ds_resultado_curto, 1, 2000) ds_orientacao,
		( b.ds_resultado_curto ) ds_orientacao_desc,
		b.dt_confirmacao,
		b.nm_usuario_ciente,
		substr(b.ds_motivo, 1, 255) ds_motivo,
		nvl(b.ie_ciente, 'N') ie_ciente,
		'' ds_justificativa_prescr
	FROM
		prescr_material   a,
		cpoe_material     a1,
		alerta_api        b
	WHERE
		a.nr_seq_mat_cpoe = a1.nr_sequencia
		AND a1.nr_sequencia = b.nr_seq_cpoe
		AND a.ie_agrupador IN (1, 4)
		AND nvl(b.ie_status_requisicao, 'XPTO') = 'XPTO'
	UNION
	SELECT
		a.nr_prescricao,
		a.nr_sequencia,
		substr(obter_desc_material(a.cd_material), 1, 255) ds_material,
		'S' ie_libera,
		gpt_obter_nome_api(b.ie_integracao, b.cd_api) ds_erro_compl,
		gpt_obter_nome_api(b.ie_integracao, b.cd_api) ds_erro_compl_desc,
		substr(obter_um_dosagem_prescr(a.nr_prescricao, a.nr_sequencia), 1, 100) ds_dose_intervalo,
		substr(a.ds_justificativa, 1, 255) ds_justificativa,
		( a.ds_justificativa ) ds_justificativa_desc,
		'' ds_observacao,
		'' ds_observacao_desc,
		substr(b.ds_resultado_curto, 1, 2000) ds_orientacao,
		( b.ds_resultado_curto ) ds_orientacao_desc,
		b.dt_confirmacao,
		b.nm_usuario_ciente,
		substr(b.ds_motivo, 1, 255) ds_motivo,
		nvl(b.ie_ciente, 'N') ie_ciente,
		'' ds_justificativa_prescr
	FROM
		prescr_material         a,
		cpoe_material           a1,
		cpoe_justif_interacao   b
	WHERE
		a.nr_seq_mat_cpoe = a1.nr_sequencia
		AND a1.nr_sequencia = b.nr_seq_cpoe_princ
		AND a.ie_agrupador IN (1, 4)
		AND b.ie_integracao = 'MDX'
/