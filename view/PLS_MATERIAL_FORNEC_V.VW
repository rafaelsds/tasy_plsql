create or replace view pls_material_fornec_v
as 
	select	a.cd_material_ops cd_material_ops,
		a.cd_estabelecimento cd_estabelecimento,
		a.nr_sequencia nr_sequencia,
		a.nr_sequencia cd_material,
		a.ds_material ds_material,
		substr(pls_obter_cd_anvisa_material(a.nr_sequencia),1,15) ds_anvisa,
		substr(pls_obter_fornecedor_material(b.nr_seq_fornecedor),1,255) nm_fornecedor_mat,
		b.nr_seq_fornecedor nr_seq_fornec,
		(select	max(x.cd_fabricante) from material x where x.cd_material = a.cd_material) cd_fabricante
	from	pls_material a,
		pls_material_fornec b
	where	b.nr_seq_material = a.nr_sequencia(+)
	and 	(a.dt_exclusao is null or a.dt_exclusao > sysdate)
	and	(a.dt_inclusao is null or trunc(a.dt_inclusao) <= trunc(sysdate))
	and   	b.ie_visualizar_portal = 'S'
	and     exists (select 1
			from   pls_material_fornec_preco x
			where  x.nr_seq_mat_fornec = b.nr_sequencia
			and    sysdate between x.dt_inicio_vigencia and nvl(x.dt_fim_vigencia,sysdate))
	union all
	select	a.cd_material_ops cd_material_ops,
		a.cd_estabelecimento cd_estabelecimento,
		a.nr_sequencia nr_sequencia,
		a.nr_sequencia cd_material,
		a.ds_material ds_material,
		substr(pls_obter_cd_anvisa_material(a.nr_sequencia),1,15) ds_anvisa,
		null nm_fornecedor_mat,
		null nr_seq_fornec,
		(select	max(x.cd_fabricante) from material x where x.cd_material = a.cd_material) cd_fabricante
	from	pls_material a
	where	(a.dt_exclusao is null or a.dt_exclusao > sysdate)
	and	(a.dt_inclusao is null or trunc(a.dt_inclusao) <= trunc(sysdate));
/
