create or replace view tws_ptu_inconsistencia_v as 
select 	nr_sequencia,
	cd_inconsistencia,
	ds_inconsistencia,
	dt_atualizacao 
from 	ptu_inconsistencia;
/