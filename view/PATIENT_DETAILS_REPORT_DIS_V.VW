CREATE OR REPLACE 
VIEW PATIENT_DETAILS_REPORT_DIS_V AS 
  select    
	obter_nome_pf(a.cd_pessoa_fisica) ds_patient,
        substr(to_char(a.dt_nascimento,'DD Mon YYYY')||''|| ' ('||obter_idade(a.dt_nascimento, SYSDATE, 'A')||'y)',1,255) ds_dob_age,
        obter_valor_dominio(4,a.ie_sexo) ds_gender,
        substr(replace(AUS_PERSON_GET_ADDRESS(a.cd_pessoa_fisica,1),chr(10),''),1,255) ds_address,
        replace(to_char(a.nr_telefone_celular, '09,9999,9999'),',',' ') Mob_numer,
        replace(to_char(obter_compl_pf(a.cd_pessoa_fisica,1,'T'), '09,9999,9999'),',',' ') ds_phone_number,
        c.dt_entrada dt_admission,
        a.nr_prontuario nr_mrn,
        c.nr_atendimento nr_encounter,    
        a.cd_rfc cd_medicare,
        get_med_expire_date(a.cd_rfc, a.cd_pessoa_fisica) dt_medicare_exp,    
        substr(obter_desc_convenio(g.cd_convenio),1,255) ds_health_fund,
        g.cd_usuario_convenio ds_ins_member,      
        GET_DATA_CONCESSION_CARD(a.cd_pessoa_fisica,1,'M') ds_concessio_card,
        GET_DATA_CONCESSION_CARD(a.cd_pessoa_fisica,2,'M') cd_concession_number, 
        GET_DATA_CONCESSION_CARD(a.cd_pessoa_fisica,1,'S') ds_safetynet,
        get_person_ihi(a.cd_pessoa_fisica,'I') nr_ihi,
        substr(obter_nome_medico(c.CD_MEDICO_RESP, 'N'),1,255) nm_doctor,
        p.nr_sequencia as nr_discharge
from    pessoa_fisica a,
        atendimento_paciente c,
        atend_categoria_convenio g,
        atendimento_alta p
where   a.cd_pessoa_fisica = c.cd_pessoa_fisica
and     c.nr_atendimento = p.nr_atendimento
and     c.nr_atendimento = g.nr_atendimento
and     g.dt_inicio_vigencia = (select max(x.dt_inicio_vigencia) from ATEND_CATEGORIA_CONVENIO x where x.nr_atendimento = c.nr_atendimento)
order by nr_encounter, nr_discharge;
/