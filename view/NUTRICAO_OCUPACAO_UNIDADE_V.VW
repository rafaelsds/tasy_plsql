/*
View � uma c�pia da view ocupacao_unidade_v por�m apenas com select do nr_atendimento e cd_setor_atendimento.
Utilizado na procedure gerar_servico_nutricao onde precisamos apenas do nr_atendimento e cd_setor_atendimento.
N�o colocar outras funtions de retorno, alterar apenas restri��es caso for alterado na view ocupacao_unidade_v
*/
create or replace 
view nutricao_ocupacao_unidade_v
as
select 	a.nr_atendimento,
       	a.cd_setor_atendimento
from  	valor_dominio g,
		pessoa_fisica e,
      	pessoa_fisica f,     
      	medico d,
      	atendimento_paciente b,
      	unidade_atendimento a,
	atend_paciente_unidade u
where	a.nr_atendimento      = b.nr_atendimento
and 	b.cd_pessoa_fisica    = e.cd_pessoa_fisica
and	u.nr_atendimento      = b.nr_atendimento	
and	u.nr_Seq_interno = (	select 	max(x.nr_seq_interno) 
				from 	atend_paciente_unidade x 
				where 	x.nr_Atendimento = a.nr_Atendimento 
				and	a.cd_Setor_atendimento	= x.cd_setor_Atendimento
				and 	a.cd_unidade_basica	 = x.cd_unidade_basica
				and 	a.cd_unidade_compl	= x.cd_unidade_Compl)
and 	b.cd_medico_resp      = d.cd_pessoa_fisica (+)
and 	a.ie_situacao         = 'A'
and 	a.ie_status_unidade   = 'P'
and 	a.cd_paciente_reserva = f.cd_pessoa_fisica (+)
and 	g.cd_dominio (+)	    = 17
and 	b.ie_clinica	    = vl_dominio (+)
union
select  a.nr_atendimento_acomp,
	a.cd_setor_atendimento
from  	valor_dominio g,
	pessoa_fisica e,
      	pessoa_fisica f,     
      	medico d,
      	atendimento_paciente b,
      	unidade_atendimento a,
	atend_paciente_unidade u
where 	a.nr_atendimento_acomp= b.nr_atendimento
  and b.cd_pessoa_fisica    = e.cd_pessoa_fisica
  and u.nr_atendimento      = b.nr_atendimento
  and b.cd_medico_resp      = d.cd_pessoa_fisica(+)
  and a.ie_situacao         = 'A'
  and a.ie_status_unidade   = 'M'
  and a.cd_paciente_reserva = f.cd_pessoa_fisica
  and g.cd_dominio (+)	    = 17
  and b.ie_clinica	    = vl_dominio (+)
  and u.nr_seq_interno = Obter_Atepacu_paciente(b.nr_atendimento, 'IAA')
union
select  a.nr_atendimento,
        a.cd_setor_atendimento
from  	valor_dominio b,
	pessoa_fisica f,
	unidade_atendimento a
where a.cd_paciente_reserva = f.cd_pessoa_fisica
  and a.ie_situacao         = 'A'
  and a.ie_status_unidade   in ('R','H','G','A','E','C','O')
  and b.cd_dominio 		= 82
  and b.vl_dominio		= a.ie_status_unidade
union
select  a.nr_atendimento,
        a.cd_setor_atendimento
from  valor_dominio b,
	unidade_atendimento a
where a.ie_situacao         = 'A'
  and a.ie_status_unidade in ('R','L', 'H', 'G', 'A','O','E','C')
  and a.cd_paciente_reserva is null
  and b.cd_dominio 		= 82
  and b.vl_dominio		= a.ie_status_unidade
union
select  a.nr_atendimento,
        a.cd_setor_atendimento
from 	motivo_interdicao_leito b,
	unidade_atendimento a
where 	a.ie_status_unidade in ('I','S')
  and 	a.cd_motivo_interdicao = cd_motivo (+)
  and 	a.ie_situacao = 'A';
/