Create or Replace view pls_regras_recalc_autoger_v As
  select  nr_sequencia nr_seq_regra,
    cd_procedimento,
    ie_origem_proced,
    'S' ie_principal,
    inicio_dia(dt_inicio_vigencia) dt_ini_vigencia,
    nvl(dt_fim_vigencia, sysdate) dt_fim_vigencia,
    0 pr_inicio,
    0 pr_fim,
    100 tx_item,
    null nr_seq_ocorrencia
  from  pls_recal_autoger_regra  
  union
  select  nr_seq_regra_recalc_autoger nr_seq_regra,
    b.cd_procedimento,
    b.ie_origem_proced,
    'N' ie_principal,
    inicio_dia(dt_inicio_vigencia) dt_ini_vigencia,
    nvl(dt_fim_vigencia, sysdate) dt_fim_vigencia,
    pr_inicio,
    pr_fim,
    tx_item,
    b.nr_seq_ocorrencia
  from  pls_recal_autoger_regra a,
    pls_recal_autoger_reg_item b
  where  b.nr_seq_regra_recalc_autoger = a.nr_sequencia;
/