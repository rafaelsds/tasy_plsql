create or replace 
view hepic_motivo_alta_v as
	select 	a.cd_motivo_alta cd, 
			a.ds_motivo_alta ds, 
			a.ie_situacao
     from 	motivo_alta a
	where	1 = 1
	order by ds;
/
