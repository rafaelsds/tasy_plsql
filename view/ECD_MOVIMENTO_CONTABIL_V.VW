create or replace
view ecd_movimento_contabil_v as
select	1 						tp_registro,
	'|I200' 						ds_identificador,
	b.dt_referencia					dt_referencia,
	b.cd_empresa					cd_empresa,
	a.cd_estabelecimento				cd_estabelecimento,
	min(a.nr_sequencia) 				nr_sequencia,
	trunc(a.dt_movimento) 				dt_movimento,
	decode(nvl(sum(a.vl_credito),0),0,nvl(sum(a.vl_debito),0), nvl(sum(a.vl_credito),0)) vl_movimento,
	substr(decode(c.ie_encerramento,'N','N','S','E'),1,1) ie_tipo_lancamento,
	'' 						cd_conta_contabil,
	''						cd_classificacao,
	null						cd_centro_custo,
	''						ie_debito_credito,
	null						nr_documento,
	null						cd_historico,
	''						ds_historico,
	'|'						cd_participante
from	lote_contabil c,
	ctb_mes_ref b,
	ctb_movimento_v a
where	b.nr_sequencia		= a.nr_seq_mes_ref
and	c.nr_lote_contabil	= a.nr_lote_contabil
and	a.ie_tipo		= 'A'
group by	b.dt_referencia,
	b.cd_empresa,
	a.cd_estabelecimento,
	trunc(a.dt_movimento),
	c.ie_encerramento
union all
select	2 						tp_registro,
	'|I250' 						ds_identificador,
	b.dt_referencia					dt_referencia,
	b.cd_empresa					cd_empresa,
	a.cd_estabelecimento				cd_estabelecimento,
	a.nr_sequencia					nr_sequencia,
	trunc(a.dt_movimento)				dt_movimento,
	nvl(nvl(c.vl_movimento,a.vl_movimento),0)		vl_movimento,
	substr(decode(d.ie_encerramento,'N','N','S','E'),1,1) ie_tipo_lancamento,
	a.cd_conta_contabil				cd_conta_contabil,
	a.cd_classificacao					cd_classificacao,
	decode(a.ie_centro_custo,'N',null,c.cd_centro_custo)	cd_centro_custo,
	a.ie_debito_credito					ie_debito_credito,
	a.nr_seq_agrupamento				nr_documento,
	a.cd_historico					cd_historico,
	substr(replace(replace(replace(a.ds_historico || ' ' || a.ds_compl_historico,chr(13),' '),chr(10),' '),'|',''),1,250) ds_historico,
	'|'						cd_participante
from	lote_contabil d,
	ctb_movto_centro_custo c,
	ctb_mes_ref b,
	ctb_movimento_v a
where	a.nr_sequencia		= c.nr_seq_movimento(+)
and	a.nr_seq_mes_ref	= b.nr_sequencia
and	b.nr_sequencia		= d.nr_seq_mes_ref
and	d.nr_lote_contabil	= a.nr_lote_contabil
and	a.ie_tipo		= 'A'
order by dt_referencia, dt_movimento, cd_estabelecimento, nr_sequencia, tp_registro, ie_tipo_lancamento, cd_conta_contabil, cd_centro_custo;
/