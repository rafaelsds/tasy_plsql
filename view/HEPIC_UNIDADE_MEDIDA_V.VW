create or replace 
view hepic_unidade_medida_v as
	 select cd_unidade_medida cd, 
			ds_unidade_medida ds
     from 	unidade_medida
    where 	1 = 1
	order by ds;
/
