create or replace view hvs_balanco_patrimonial_v
as
select	1											tp_registro,
	'|J100'											ds_identificador,
	c.dt_referencia										dt_referencia,
	b.cd_estabelecimento									cd_estabelecimento,
	substr(decode(hvs_obter_se_multipla_origem(a.ds_origem), 'S', to_char(a.nr_seq_rubrica), a.ds_origem),1,255)	cd_aglutinacao,
	a.qt_desl_esq										ie_nivel_aglutinacao,
	decode(e.ie_tipo,'A',1,'P',2) 									cd_grupo,
	substr(rtrim(a.ds_rubrica),1,255)								ds_aglutinacao,
	nvl(a.vl_1_coluna, 0)				 					vl_conta,
	substr(ctb_obter_situacao_saldo(d.cd_conta_contabil,  a.vl_1_coluna),1,1)||'|'				ie_debito_credito,
	d.cd_classificacao										cd_classificacao,
	a.nr_seq_apres
from	ctb_grupo_conta e,
	conta_contabil d,
	ctb_mes_ref c,
	ctb_demonstrativo b,
	ecd_demo_rubrica_v a
where	a.nr_seq_demo		= b.nr_sequencia
and	b.nr_seq_mes_ref		= c.nr_sequencia
and	a.ds_origem		= d.cd_conta_contabil(+)
and	d.cd_grupo		= e.cd_grupo(+)
and	nvl(e.ie_tipo, 'X')	in ('A','P', 'X')
and	nvl(a.ds_origem, 'X')		<> 'X'
and	b.nr_seq_tipo		= 7
and	c.nr_sequencia		= 521
and	a.nr_seq_demo		= 217
order by	a.nr_seq_apres;
/