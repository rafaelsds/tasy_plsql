create or replace
view fagf_retorno_convenio_v as
select	nvl(c.vl_pago,0) + nvl(c.vl_adicional,0) vl_pago,
	to_number(fagf_obter_amenor_guia(c.nr_sequencia)) vl_naceito,
	c.vl_glosado vl_aceito,
	(nvl(c.vl_amenor,0) + nvl(c.vl_glosado,0)) vl_glosa,
	nvl(c.vl_pago,0) + nvl(c.vl_glosado,0) + nvl(c.vl_desconto,0) vl_conta,
	substr(obter_nome_convenio(b.cd_convenio),1,255) ds_convenio,
	b.cd_convenio cd_convenio,
	substr(OBTER_VALOR_DOMINIO(11,a.ie_tipo_convenio),1,255) ds_tipo_convenio,
	a.ie_tipo_convenio,
	trunc(b.dt_baixa_cr) dt_baixa_cr,
	substr(obter_empresa_estab(b.cd_estabelecimento),1,255) cd_empresa,
	e.ie_tipo_atendimento,
	substr(obter_valor_dominio(12,e.ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	(select	max(x.cd_categoria)
	from	categoria_convenio x
	where	x.cd_convenio	= a.cd_convenio
	and	x.cd_categoria	= d.cd_categoria_parametro) cd_categoria,
	(substr(obter_nome_convenio(d.cd_convenio_parametro),1,150) || ' - ' ||
		(select	max(x.ds_categoria)
		from	categoria_convenio x
		where	x.cd_convenio	= a.cd_convenio
		and	x.cd_categoria	= d.cd_categoria_parametro)) ds_categoria,
	g.dt_mesano_referencia dt_referencia
from  	protocolo_convenio g,
	atendimento_paciente e,
	conta_paciente d,
	convenio_retorno_item c,
	convenio_retorno b,
	convenio a
where 	a.cd_convenio			= b.cd_convenio
and	b.nr_sequencia			= c.nr_seq_retorno
and	c.nr_interno_conta		= d.nr_interno_conta
and	d.nr_atendimento		= e.nr_atendimento
and	b.nr_seq_protocolo		= g.nr_seq_protocolo(+);
/