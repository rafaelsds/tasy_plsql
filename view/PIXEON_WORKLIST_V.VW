Create or replace
View Pixeon_WorkList_V as
select a.nr_prescricao, 
       a.nr_acesso_dicom na_accessionnumber, 
       max(b.cd_pessoa_fisica) co_patientid, 
       max(c.nm_pessoa_fisica) na_patientname, 
       min(a.nr_seq_interno) na_password,
       substr(max(obter_exames_presc_pixeon(a.nr_prescricao, a.nr_acesso_dicom)),1,60) tx_description, 
       null na_bodypart, 
       null na_machine,
       decode(d.ie_dt_integracao_img,'S',(select to_char(max(x.dt_atualizacao),'yyyymmdd') 
                                            from prescr_proc_status x 
                                           where x.nr_prescricao = a.nr_prescricao 
									                           and x.nr_seq_prescr = a.nr_sequencia
									                           and x.ie_status_exec = d.ie_status_integracao_img), min(to_char(a.dt_prev_execucao,'yyyymmdd'))) na_studydate,
	     decode(d.ie_dt_integracao_img,'S',(select to_char(max(x.dt_atualizacao),'hh24miss') 
			                         					    from prescr_proc_status x 
								                           where x.nr_prescricao = a.nr_prescricao 
								                             and x.nr_seq_prescr = a.nr_sequencia
								                             and x.ie_status_exec = d.ie_status_integracao_img), min(to_char(a.dt_prev_execucao,'hh24miss'))) na_studytime,
	     null co_doctor, 
       max(b.cd_medico) co_doctorrequester, 
       null na_doctor, 
       max(substr(obter_nome_medico(b.cd_medico,'N'),1,60)) na_doctorrequester, 
       null na_crm, 
       max(obter_crm_medico(b.cd_medico)) na_crmrequester,
       null na_emaildoctor, 
       max(Elimina_acentuacao(substr(obter_compl_pf(b.cd_medico,1,'M'),1,50))) na_emaildoctorrequester, 
       null na_ufdoctor, 
       max(substr(obter_dados_medico(b.cd_medico,'UFCRM'),1,60)) na_ufdoctorrequester, 
       max(c.ie_sexo) na_patientsex, 
       max(to_char(c.dt_nascimento,'yyyymmdd')) na_patientbirthday, 
       to_char(max(b.qt_peso), '000.00') nu_weight, 
       to_char(max(dividir(nvl(b.qt_altura_cm,c.qt_altura_cm),100)), '0.00') nu_height, 
       max(substr(decode(nvl(d.ie_estab_pixeon,'N'),'S',obter_tipo_procedimento(a.cd_procedimento, a.ie_origem_proced,'D') || '_' || b.cd_estabelecimento, 'N',obter_tipo_procedimento(a.cd_procedimento, a.ie_origem_proced,'D')),1,60)) na_modality,
       a.nr_prescricao na_studyid, 
       max(substr(pixeon_obter_na_requirenit(b.nr_atendimento,b.cd_estabelecimento,substr(obter_nome_setor(b.cd_setor_atendimento),1,60)),1,60)) na_requirenit,
       max(to_char(a.dt_resultado,'yyyymmddhh24miss')) na_datetimerelease, 
       max(c.nr_prontuario) nr_prontuario, 
       max(decode(a.ie_suspenso,'S','DISCONTINUED','N')) ie_suspenso,
       max(substr(obter_desc_convenio(obter_convenio_atendimento(b.nr_atendimento)),1,50)) na_insurancecarrier, 
       max(substr(Obter_Plano_Atendimento(b.nr_atendimento,'D'),1,50)) na_insuranceplan, 
       max(b.nr_atendimento) na_atendimento, 
       max(b.cd_estabelecimento) na_unit, 
       max(substr(OBTER_NOME_TIPO_ATEND(OBTER_TIPO_ATENDIMENTO(b.nr_atendimento)),1,50)) na_requestingservice,
       decode(max(a.ie_urgencia),'S','1','N','0') na_priority, 
       max(b.nr_atendimento) treatmentnumber
  From pessoa_fisica c, Prescr_medica b, Prescr_procedimento a, parametro_medico d
 where a.nr_prescricao = b.nr_prescricao
   and b.cd_pessoa_fisica = c.cd_pessoa_fisica
   and b.cd_estabelecimento = d.cd_estabelecimento(+)
   and a.cd_motivo_baixa = 0
   and nvl(dt_liberacao_medico, dt_liberacao) is not null
   and a.dt_integracao is null
   and ((d.ie_status_integracao_img is null) or (a.ie_status_execucao = d.ie_status_integracao_img))
   and ((nvl(obter_select_concatenado_bv(' select ie_somente_autorizados from parametro_integracao_pacs where cd_estabelecimento = nvl(:cd_estabelecimento, cd_estabelecimento) ', 'cd_estabelecimento=' || b.cd_estabelecimento , ''),'N') = 'N') or
 	      (a.ie_autorizacao in ('L', 'A')))
   and a.cd_setor_atendimento in (select s.cd_setor_atendimento
						                        from setor_atendimento s
						                       where upper(s.nm_usuario_banco) = (select upper(user) from dual))
group by a.nr_prescricao,a.nr_sequencia, a.nr_acesso_dicom, d.ie_dt_integracao_img, d.ie_status_integracao_img
union all
select a.nr_prescricao, 
       a.nr_acesso_dicom na_accessionnumber, 
       max(b.cd_pessoa_fisica) co_patientid, 
       max(c.nm_pessoa_fisica) na_patientname, 
       min(a.nr_seq_interno) na_password,
	     substr(max(obter_exames_presc_pixeon(a.nr_prescricao, a.nr_acesso_dicom)),1,60) tx_description, 
       null na_bodypart, 
       null na_machine,
	     decode(d.ie_dt_integracao_img,'S',(select to_char(max(x.dt_atualizacao),'yyyymmdd') 
			                         						  from prescr_proc_status x 
									                         where x.nr_prescricao = a.nr_prescricao 
									                           and x.nr_seq_prescr = a.nr_sequencia
									                           and x.ie_status_exec  = d.ie_status_integracao_img), min(to_char(a.dt_prev_execucao,'yyyymmdd'))) na_studydate,
	     decode(d.ie_dt_integracao_img,'S',(select to_char(max(x.dt_atualizacao),'hh24miss') 
			                         					    from prescr_proc_status x 
								                           where x.nr_prescricao = a.nr_prescricao 
								                             and x.nr_seq_prescr = a.nr_sequencia
								                             and x.ie_status_exec = d.ie_status_integracao_img), min(to_char(a.dt_prev_execucao,'hh24miss'))) na_studytime,
	     null co_doctor, 
       max(b.cd_medico) co_doctorrequester, 
       null na_doctor, 
       max(substr(obter_nome_medico(b.cd_medico,'N'),1,60)) na_doctorrequester, 
       null na_crm, 
       max(obter_crm_medico(b.cd_medico)) na_crmrequester,
	     null na_emaildoctor, 
       max(Elimina_acentuacao(substr(obter_compl_pf(b.cd_medico,1,'M'),1,50))) na_emaildoctorrequester, 
       null na_ufdoctor, 
       max(substr(obter_dados_medico(b.cd_medico,'UFCRM'),1,60)) na_ufdoctorrequester, 
       max(c.ie_sexo) na_patientsex, 
       max(to_char(c.dt_nascimento,'yyyymmdd')) na_patientbirthday, 
       to_char(max(b.qt_peso), '000.00') nu_weight, 
       to_char(max(dividir(nvl(b.qt_altura_cm,c.qt_altura_cm),100)), '0.00') nu_height, 
       max(substr(decode(nvl(d.ie_estab_pixeon,'N'),'S',obter_tipo_procedimento(a.cd_procedimento, a.ie_origem_proced,'D') || '_' || b.cd_estabelecimento, 'N',obter_tipo_procedimento(a.cd_procedimento, a.ie_origem_proced,'D')),1,60)) na_modality,
	     a.nr_prescricao na_studyid, 
       max(substr(pixeon_obter_na_requirenit(b.nr_atendimento,b.cd_estabelecimento,substr(obter_nome_setor(b.cd_setor_atendimento),1,60)),1,60)) na_requirenit,	
       max(to_char(a.dt_resultado,'yyyymmddhh24miss')) na_datetimerelease, 
       max(c.nr_prontuario) nr_prontuario, 
       max(decode(a.ie_suspenso,'S','DISCONTINUED','N')) ie_suspenso, 
       max(substr(obter_desc_convenio(obter_convenio_atendimento(b.nr_atendimento)),1,50)) na_insurancecarrier, 
       max(substr(Obter_Plano_Atendimento(b.nr_atendimento,'D'),1,50)) na_insuranceplan, 
       max(b.nr_atendimento) na_atendimento, 
       max(b.cd_estabelecimento) na_unit, 
       max(substr(OBTER_NOME_TIPO_ATEND(OBTER_TIPO_ATENDIMENTO(b.nr_atendimento)),1,50)) na_requestingservice, 
       decode(max(a.ie_urgencia),'S','1','N','0') na_priority, 
       max(b.nr_atendimento) treatmentnumber
  From pessoa_fisica c, Prescr_medica b, Prescr_procedimento a, parametro_medico d
 where a.nr_prescricao = b.nr_prescricao
   and b.cd_pessoa_fisica = c.cd_pessoa_fisica
   and b.cd_estabelecimento = d.cd_estabelecimento(+)
   and a.dt_integracao is null
   and a.cd_motivo_baixa = 0
   and ((d.ie_status_integracao_img is null) or (a.ie_status_execucao = d.ie_status_integracao_img))
   and ((nvl(obter_select_concatenado_bv(' select ie_somente_autorizados from parametro_integracao_pacs where cd_estabelecimento = nvl(:cd_estabelecimento, cd_estabelecimento) ', 'cd_estabelecimento=' || b.cd_estabelecimento , ''),'N') = 'N') or
 	      (a.ie_autorizacao in ('L', 'A')))
   and exists(select 1 from agenda_paciente k, pixeon_agenda_liberada y    
			         where k.nr_sequencia = b.nr_seq_agenda
			           and k.cd_agenda = y.cd_agenda) --Tratamento para buscar a agenda da hemodinamica
   and a.cd_setor_atendimento in (select s.cd_setor_atendimento
					                          from setor_atendimento s
					                         where upper(s.nm_usuario_banco) = (select upper(user) from dual))
group by a.nr_prescricao, a.nr_sequencia, a.nr_acesso_dicom, d.ie_dt_integracao_img, d.ie_status_integracao_img;
/