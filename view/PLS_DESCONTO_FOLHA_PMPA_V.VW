create or replace view pls_desconto_folha_pmpa_v 
as 
select	1				TP_REGISTRO,
	substr(c.cd_matricula,1,7)	CD_MATRICULA_7,
	substr(c.cd_matricula,1,10)	CD_MATRICULA_10,
	obter_Valor_sem_virgula(a.vl_cobranca) VL_PAGO,
	substr(c.nr_seq_vinculo_empresa,1,2)NR_VINCULO_PMPA,
	a.nr_seq_cobranca		NR_SEQ_COBRANCA
from	titulo_receber_cobr		a,
	pls_mensalidade			b, 
	pls_contrato_pagador_fin	c, 
	cobranca_escritural		d, 
	pls_desc_empresa_regra		e 
where	substr(obter_dados_titulo_receber(a.nr_titulo, 'ME'),1,255) = b.nr_sequencia 
and	b.nr_seq_pagador = c.nr_seq_pagador 
and	sysdate	between nvl(c.dt_inicio_vigencia,sysdate) and nvl(c.dt_fim_vigencia,sysdate) 
and	a.nr_seq_cobranca = d.nr_sequencia 
and	e.nr_seq_empresa = d.nr_seq_empresa
group by	a.nr_seq_cobranca,
		c.cd_matricula,
		a.vl_cobranca,
		c.nr_seq_vinculo_empresa;
/
