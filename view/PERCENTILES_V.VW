CREATE OR REPLACE VIEW PERCENTILES_V AS 
SELECT
    a.nr_atendimento encounter,
    a.nr_sequencia Birth_number,
    a.qt_apgar_prim_min apgar_1,
    a.QT_APGAR_QUINTO_MIN apgar_5,
	a.QT_APGAR_DECIMO_MIN apgar_10,
    a.cd_arterial_ph           arterial_ph,
    a.cd_venous_ph            venous_ph,
    a.cd_arterial_base_excess   arterial_base_excess,
    a.dt_nascimento             nascimento,
	b.CD_ESTABELECIMENTO establishment
FROM
    nascimento a 
	inner join 
	atendimento_paciente b
	on 
	a.nr_atendimento = b.nr_atendimento
WHERE
    dt_inativacao IS NULL;
/