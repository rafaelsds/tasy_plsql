create or replace view
trans_financ_cheque_v as
select	nr_sequencia,
		cd_estabelecimento,
		cd_empresa,
		ds_transacao,
		cd_transacao || ' - ' || ds_transacao ds_trans_cod,
		ie_acao,
		cd_transacao,
		ie_situacao
from	transacao_financeira
where	ie_situacao = 'A';
/
