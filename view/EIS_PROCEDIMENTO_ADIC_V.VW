create or replace view eis_procedimento_adic_v as
select	p.cd_procedimento||' - '||substr(obter_descricao_procedimento(p.cd_procedimento,p.ie_origem_proced),1,50) ds_cod_proc_adic,
	nvl(a.dt_inicio_real,a.dt_inicio_prevista) dt_referencia,
	nvl(p.cd_medico_exec,a.cd_medico_cirurgiao) cd_medico,
	substr(obter_desc_especialidade_proc(Obter_Especialidade_Proced(p.cd_procedimento, p.ie_origem_proced)),1,200) ds_especialidade_proc,
	substr(obter_unid_setor_cirurgia(a.nr_cirurgia,'S'),1,200) ds_setor_unidade,
	SUBSTR(obter_porte_anestesico(obter_cirurgia_prescricao(p.nr_prescricao),NULL,p.cd_procedimento,p.ie_origem_proced,p.cd_convenio,p.cd_categoria),1,15) ds_porte,
	a.cd_estabelecimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento,
	p.cd_procedimento cd_procedimento,
	p.ie_origem_proced ie_origem_proced,
	p.cd_categoria cd_categoria,
	p.nr_prescricao nr_prescricao,
	p.cd_convenio cd_convenio,
	nvl(substr(obter_desc_convenio(p.cd_convenio),1,255),Obter_Desc_Expressao(327119)) ds_convenio,
	Obter_Especialidade_Proced(p.cd_procedimento, p.ie_origem_proced) cd_especialidade_proc,
	substr(obter_dados_cirurgia(a.nr_cirurgia,'CR'),1,255) cd_carater_cirurgia,
	substr(obter_valor_dominio(1016,obter_dados_cirurgia(a.nr_cirurgia,'CR')),1,255) ds_carater_cirurgia,
	obter_proc_interno(a.nr_seq_proc_interno,'CI') nr_proc_interno,
	SUBSTR(Obter_sala_cirurgia(a.nr_cirurgia),1,255) ||' '|| SUBSTR(OBTER_SALA_CIRURGIA_COMPL(a.nr_cirurgia),1,255) ds_sala_cirurgia,
	IE_STATUS_CIRURGIA,
	substr(obter_status_cirurgia(IE_STATUS_CIRURGIA),1,200) ds_status_cirurgia,
	a.CD_TIPO_ANESTESIA,
	substr(obter_valor_dominio(36,a.CD_TIPO_ANESTESIA),1,150) ds_tipo_anestesia,
	to_char(p.nr_seq_proc_interno) || ' - ' || substr(OBTER_DESC_PROC_INTERNO(p.nr_seq_proc_interno),1,255) ds_proc_interno,
	p.nr_seq_proc_interno,
	p.qt_procedimento
from	prescr_procedimento p,
	prescr_medica b,
	cirurgia a
where	a.nr_prescricao = p.nr_prescricao
and	p.nr_prescricao = b.nr_prescricao;
/