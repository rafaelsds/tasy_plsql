CREATE OR REPLACE VIEW sla_information_v
AS /* View utilizada no WCP 1029492 - slaWCP */
select	'SLA with penalty' ds_type,
			'SWP' ie_type,
			sum(decode(ie_indicador,1,1,0)) opened_defects,
			sum(decode(ie_indicador,2,1,0)) reclassified_defects,
			sum(decode(ie_indicador,3,1,0)) RESPONSE_BREACHED,
			sum(decode(ie_indicador,4,1,0)) resolution_breached,
			(100-dividir(((sum(decode(ie_indicador,3,1,0))+sum(decode(ie_indicador,4,1,0)))*100),(sum(decode(ie_indicador,1,1,0))*2))) sla_achievement,
			obter_meta_bsc_sla(2627,extract_date) qt_meta,
			trunc(extract_date, 'month') generate_date
from		sla_information_v2
where	1 = 1
and		develop_management_id <> 68
and	sla_detail in ('CSFP', 'COSP')
group by	'SWP',obter_meta_bsc_sla(2627,extract_date), trunc(extract_date, 'month')
union all
select	'SLA with contract' ds_type,
			'SWC' ie_type,
			sum(decode(ie_indicador,1,1,0)) opened_defects,
			sum(decode(ie_indicador,2,1,0)) reclassified_defects,
			sum(decode(ie_indicador,3,1,0)) RESPONSE_BREACHED,
			sum(decode(ie_indicador,4,1,0)) resolution_breached,
			(100-dividir(((sum(decode(ie_indicador,3,1,0))+sum(decode(ie_indicador,4,1,0)))*100),(sum(decode(ie_indicador,1,1,0))*2))) sla_achievement,
			obter_meta_bsc_sla(2628,extract_date) qt_meta,
			trunc(extract_date, 'month') generate_date
from		sla_information_v2
where	1 = 1
and		develop_management_id <> 68
and		sla_detail in ('CSFNP', 'COSNP', 'CSFP', 'COSP')
group by	'SWC', obter_meta_bsc_sla(2628,extract_date), trunc(extract_date, 'month')
union all
select	'All SLAs' ds_type,
			'ASLA' ie_type,
			sum(decode(ie_indicador,1,1,0)) opened_defects,
			sum(decode(ie_indicador,2,1,0)) reclassified_defects,
			sum(decode(ie_indicador,3,1,0)) RESPONSE_BREACHED,
			sum(decode(ie_indicador,4,1,0)) resolution_breached,
			(100-dividir(((sum(decode(ie_indicador,3,1,0))+sum(decode(ie_indicador,4,1,0)))*100),(sum(decode(ie_indicador,1,1,0))*2))) sla_achievement,
			obter_meta_bsc_sla(2637,extract_date) qt_meta,
			trunc(extract_date, 'month') generate_date
from		sla_information_v2 a
where	1 = 1
and		develop_management_id <> 68
group by	'ASLA', trunc(extract_date, 'month'), obter_meta_bsc_sla(2637,extract_date) 
union all
select	'Official BSC indicator' ds_type,
	'OBI' ie_type,
	OBTER_TOTAIS_SLA_INFORMATION(trunc(a.extract_date, 'month'),'OPENED_DEFECTS') OPENED_DEFECTS, 	
	sum(decode(customer_classification, philips_classification, 0, 1)) RECLASSIFIED_DEFECTS,
	OBTER_TOTAIS_SLA_INFORMATION(trunc(a.extract_date, 'month'),'RESPONSE_BREACHED') RESPONSE_BREACHED,
	OBTER_TOTAIS_SLA_INFORMATION(trunc(a.extract_date, 'month'),'RESOLUTION_BREACHED') RESOLUTION_BREACHED,
	obter_resultado_sla(trunc(extract_date,'month'),fim_mes(extract_date),null) sla_achievement,  
	obter_meta_bsc_sla(852,extract_date) qt_meta,
	trunc(extract_date, 'month') generate_date
from	sla_informations a
where	1 = 1
group by 'OBI', trunc(extract_date, 'month'), fim_mes(extract_date), obter_meta_bsc_sla(852,extract_date)
order by ds_type desc;
/