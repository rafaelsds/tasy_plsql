CREATE OR REPLACE VIEW SLA_INFORMATION_V3
AS /* View utilizada nos WCP's 1081540 - priority e 1087836 - slaPerManagementWCP */
select  '1' ie_indicador,
	'OPENED_DEFECTS' ds_indicador,
	nr_seq_ordem so_number, 
	sla_dashboard_pck.obter_status_resp(nr_seq_ordem) status_sla_response,
	a.ie_classificacao philips_classification,
	d.ie_classificacao_cliente customer_classification,
	a.ie_prioridade philips_severity,
	d.ie_prioridade_cliente customer_severity,
	man_obter_se_os_sla (d.nr_sequencia) so_sla,
	nr_seq_sla sla_id,
	nvl((100 - e.pr_desvio), 100) qt_target, 
	d.nr_seq_localizacao customer, --customer
	e.ds_titulo sla_description,
	sla_dashboard_pck.obter_min_solucao(d.nr_sequencia) sla_resolution_remaining,
	sla_dashboard_pck.obter_min_primeiro_atend(d.nr_sequencia) sla_response_remaining,
	man_obter_desc_estagio_trad(d.nr_seq_estagio, null, 'D') actual_stage,
	sla_dashboard_pck.obter_detalhe_sla(ie_tipo_sla, ie_tipo_sla_termino) sla_detail,--sla_detail,
	obter_gerencia_resp_os(nr_seq_ordem) develop_management_id,
	obter_descricao_gerencia(obter_gerencia_resp_os(nr_seq_ordem)) develop_management,
	d.dt_ordem_servico extract_date,
	qt_desvio sla_resolution_breached,
	qt_desvio_inic sla_response_breached,
	obter_desc_grupo_desenv(nr_seq_grupo_des) develop_group,
	ie_tipo_sla sla_detail_start,
	ie_tipo_sla_termino sla_detail_finish
from    man_ordem_serv_sla a,
	man_ordem_servico d,
	man_sla e
where   d.nr_sequencia = a.nr_seq_ordem
and	e.nr_sequencia = a.nr_seq_sla
and	obter_gerencia_resp_os(nr_seq_ordem) <> 68 
union--#############################################################################################
select  '2' ie_indicador,
	'RECLASSIFIED_DEFECTS' ds_indicador,
	nr_seq_ordem so_number, 
	sla_dashboard_pck.obter_status_resp(nr_seq_ordem) status_sla_response,
	a.ie_classificacao philips_classification,
	d.ie_classificacao_cliente customer_classification,
	a.ie_prioridade philips_severity,
	d.ie_prioridade_cliente customer_severity,
	man_obter_se_os_sla (d.nr_sequencia) so_sla,
	nr_seq_sla sla_id,
	nvl((100 - e.pr_desvio), 100) qt_target, 
	d.nr_seq_localizacao customer, --customer
	e.ds_titulo sla_description,
	sla_dashboard_pck.obter_min_solucao(d.nr_sequencia) sla_resolution_remaining,
	sla_dashboard_pck.obter_min_primeiro_atend(d.nr_sequencia) sla_response_remaining,
	man_obter_desc_estagio_trad(d.nr_seq_estagio, null, 'D') actual_stage,
	sla_dashboard_pck.obter_detalhe_sla(ie_tipo_sla, ie_tipo_sla_termino) sla_detail,--sla_detail,
	obter_gerencia_resp_os(nr_seq_ordem) develop_management_id,
	obter_descricao_gerencia(obter_gerencia_resp_os(nr_seq_ordem)) develop_management,
	d.dt_ordem_servico extract_date,
	qt_desvio sla_resolution_breached,
	qt_desvio_inic sla_response_breached,
	obter_desc_grupo_desenv(nr_seq_grupo_des) develop_group,
	ie_tipo_sla sla_detail_start,
	ie_tipo_sla_termino sla_detail_finish
from    man_ordem_serv_sla a,
	man_ordem_servico d,
	man_sla e
where   d.nr_sequencia = a.nr_seq_ordem
and	e.nr_sequencia = a.nr_seq_sla
and	obter_gerencia_resp_os(nr_seq_ordem) <> 68
and	d.ie_classificacao <> d.ie_classificacao_cliente
union--###############################################################################################
select  '3' ie_indicador,
	'RESPONSE_BREACHED' ds_indicador,
	nr_seq_ordem so_number, 
	sla_dashboard_pck.obter_status_resp(nr_seq_ordem) status_sla_response,
	a.ie_classificacao philips_classification,
	d.ie_classificacao_cliente customer_classification,
	a.ie_prioridade philips_severity,
	d.ie_prioridade_cliente customer_severity,
	man_obter_se_os_sla (d.nr_sequencia) so_sla,
	nr_seq_sla sla_id,
	nvl((100 - e.pr_desvio), 100) qt_target, 
	d.nr_seq_localizacao customer, --customer
	e.ds_titulo sla_description,
	sla_dashboard_pck.obter_min_solucao(d.nr_sequencia) sla_resolution_remaining,
	sla_dashboard_pck.obter_min_primeiro_atend(d.nr_sequencia) sla_response_remaining,
	man_obter_desc_estagio_trad(d.nr_seq_estagio, null, 'D') actual_stage,
	sla_dashboard_pck.obter_detalhe_sla(ie_tipo_sla, ie_tipo_sla_termino) sla_detail,--sla_detail,
	obter_gerencia_resp_os(nr_seq_ordem) develop_management_id,
	obter_descricao_gerencia(obter_gerencia_resp_os(nr_seq_ordem)) develop_management,
	d.dt_ordem_servico extract_date,
	qt_desvio sla_resolution_breached,
	qt_desvio_inic sla_response_breached,
	obter_desc_grupo_desenv(nr_seq_grupo_des) develop_group,
	ie_tipo_sla sla_detail_start,
	ie_tipo_sla_termino sla_detail_finish
from    man_ordem_serv_sla a,
	man_ordem_servico d,
	man_sla e
where   d.nr_sequencia = a.nr_seq_ordem
and	e.nr_sequencia = a.nr_seq_sla
and	obter_gerencia_resp_os(nr_seq_ordem) <> 68 
and	a.qt_desvio_inic = 1
and	not exists (	select	1 
			from	man_ordem_serv_sla_abonada x
			where	a.nr_seq_ordem = x.nr_seq_ordem_servico
			and	x.ie_tipo_abono in ('A','S'))
and  exists (	select	1
		from	man_sla_evento e
		where	e.nr_seq_ordem = d.nr_sequencia
		and	e.ie_tipo_evento = 8
		and	trunc(e.dt_atualizacao,'mm') = trunc(a.dt_inicio_sla,'mm')
		and	e.ie_prioridade = (
					select	x.ie_prioridade
					from	man_sla_evento x
					where	x.nr_sequencia = (
								select	max(y.nr_sequencia)
								from	man_sla_evento y
								where	y.nr_seq_ordem = d.nr_sequencia
								and	trunc(y.dt_atualizacao,'mm') = trunc(a.dt_inicio_sla,'mm'))))
union --###############################################################################################
select  '4' ie_indicador,
	'RESOLUTION_BREACHED' ds_indicador,
	nr_seq_ordem so_number, 
	sla_dashboard_pck.obter_status_resp(nr_seq_ordem) status_sla_response,
	a.ie_classificacao philips_classification,
	d.ie_classificacao_cliente customer_classification,
	a.ie_prioridade philips_severity,
	d.ie_prioridade_cliente customer_severity,
	man_obter_se_os_sla (d.nr_sequencia) so_sla,
	nr_seq_sla sla_id,
	nvl((100 - e.pr_desvio), 100) qt_target, 
	d.nr_seq_localizacao customer, --customer
	e.ds_titulo sla_description,
	sla_dashboard_pck.obter_min_solucao(d.nr_sequencia) sla_resolution_remaining,
	sla_dashboard_pck.obter_min_primeiro_atend(d.nr_sequencia) sla_response_remaining,
	man_obter_desc_estagio_trad(d.nr_seq_estagio, null, 'D') actual_stage,
	sla_dashboard_pck.obter_detalhe_sla(ie_tipo_sla, ie_tipo_sla_termino) sla_detail,--sla_detail,
	obter_gerencia_resp_os(nr_seq_ordem) develop_management_id,
	obter_descricao_gerencia(obter_gerencia_resp_os(nr_seq_ordem)) develop_management,
	d.dt_ordem_servico extract_date,
	qt_desvio sla_resolution_breached,
	qt_desvio_inic sla_response_breached,
	obter_desc_grupo_desenv(nr_seq_grupo_des) develop_group,
	ie_tipo_sla sla_detail_start,
	ie_tipo_sla_termino sla_detail_finish
from    man_ordem_serv_sla a,
	man_ordem_servico d,
	man_sla e
where   d.nr_sequencia = a.nr_seq_ordem
and	e.nr_sequencia = a.nr_seq_sla
and	obter_gerencia_resp_os(nr_seq_ordem) <> 68 
and	a.qt_desvio = 1
and	not exists (	select	1 
			from	man_ordem_serv_sla_abonada x
			where	a.nr_seq_ordem = x.nr_seq_ordem_servico
			and	x.ie_tipo_abono in ('A','S'))
and  exists (	select	1
		from	man_sla_evento e
		where	e.nr_seq_ordem = d.nr_sequencia
		and	e.ie_tipo_evento = 9
		and	trunc(e.dt_atualizacao,'mm') = trunc(a.dt_inicio_sla,'mm')
		and	e.ie_prioridade = (
					select	x.ie_prioridade
					from	man_sla_evento x
					where	x.nr_sequencia = (
								select	max(y.nr_sequencia)
								from	man_sla_evento y
								where	y.nr_seq_ordem = d.nr_sequencia
								and	trunc(y.dt_atualizacao,'mm') = trunc(a.dt_inicio_sla,'mm'))))
order by	1;
/
