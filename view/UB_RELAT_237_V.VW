create or replace view ub_relat_237_v as
select	a.nr_atendimento			nr_atendimento_237,
	b.nr_prescricao				nr_prescr_237,
	a.nm_usuario_atend			nm_usuario_atend_237,
	c.nm_usuario				nm_usuario_237,
	c.ds_usuario				ds_usuario_237,
	c.cd_estabelecimento			cd_estab_prescr_237,
	d.cd_estabelecimento			cd_estabelecimento_237,
	d.nm_fantasia_estab			nm_fantasia_estab_237,
	abrevia_nome_pf(a.cd_pessoa_fisica,'M') nm_paciente_237,
	null					nr_atendimento_238,
	null					nr_prescr_238,
	''					cd_pessoa_fisica_238,
	''					ds_senha_238,
	''					cd_pf_cad_238,
	null					dt_entrada_238,
	null					dt_entrega_238,
	''					nm_paciente_238,
	b.nr_prescricao				nr_prescr
from	atendimento_paciente a,
	prescr_medica b,
	usuario c,
	estabelecimento d
where	a.nr_atendimento = b.nr_atendimento
and	a.nm_usuario_atend = c.nm_usuario
and	d.cd_estabelecimento =  c.cd_estabelecimento
union
select	null					nr_atendimento_237,
	null					nr_prescr_237,
	''					nm_usuario_atend_237,
	''					nm_usuario_237,
	''					ds_usuario_237,
	null					cd_estab_prescr_237,
	null					cd_estabelecimento_237,
	''					nm_fantasia_estab_237,
	''					nm_paciente_237,
	a.nr_atendimento			nr_atendimento_238,
	b.nr_prescricao				nr_prescr_238,
	a.cd_pessoa_fisica			cd_pessoa_fisica_238,
	c.ds_senha				ds_senha_238,
	c.cd_pessoa_fisica			cd_pf_cad_238,
	'Data/hora:   '||to_char(a.dt_entrada,'dd/mm/yyyy hh24:mi:ss')	dt_entrada_238,
	'Resultado:   '|| to_char(b.dt_entrega,'dd/mm/yyyy hh24:mi:ss')	dt_entrega_238,
	abrevia_nome_pf(a.cd_pessoa_fisica,'M')	nm_paciente_238,
	b.nr_prescricao				nr_prescr
from	atendimento_paciente a,
	prescr_medica b,
	pessoa_fisica c
where	a.nr_atendimento = b.nr_atendimento
and	a.cd_pessoa_fisica = c.cd_pessoa_fisica;
/
