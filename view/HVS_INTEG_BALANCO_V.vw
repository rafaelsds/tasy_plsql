CREATE OR REPLACE VIEW HVS_INTEG_BALANCO_V
AS
select  a.cd_conta_contabil     "CODE",
        d.dt_referencia         "DATE",
        b.ds_conta_contabil     "ACCOUNT",
        sum(a.vl_debito)        "DEBIT",
        sum(a.vl_credito)       "CREDIT",
        sum(a.vl_saldo)         "SALDO",
        sum(a.vl_saldo - a.vl_movimento)                "SALDO_ANTERIOR",
        nvl(a.cd_classificacao, b.cd_classificacao)     "CD_CLASSIFICACAO",
        b.cd_empresa            "CD_EMPRESA"
from    ctb_mes_ref d,
        ctb_grupo_conta c,
        conta_contabil b,
        ctb_saldo a
where   a.cd_conta_contabil = b.cd_conta_contabil
and     d.nr_sequencia = a.nr_seq_mes_ref
and     d.cd_empresa = b.cd_empresa
and     b.cd_grupo = c.cd_grupo (+)
group by
        a.cd_conta_contabil,
        d.dt_referencia,
        b.ds_conta_contabil,
        nvl(a.cd_classificacao, b.cd_classificacao),
        b.cd_empresa;    
/
