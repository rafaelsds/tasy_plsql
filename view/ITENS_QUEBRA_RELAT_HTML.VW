CREATE OR REPLACE VIEW ITENS_QUEBRA_RELAT_HTML AS
SELECT	01 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(713989) DS, 'IE_QUEBRA_01' NM, 'DS_PORTE' VL_QUEBRA, ' ''Porte: '' || SUBSTR(Obter_Porte_Proc_Cirurgia(a.nr_cirurgia, NULL),1,100) ds_porte ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	02 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(293659) DS, 'IE_QUEBRA_02' NM, 'DS_MUNICIPIO' VL_QUEBRA, ' NVL(INITCAP(SUBSTR(obter_desc_municipio_ibge(Obter_Cod_Mun_ibge_atend(c.nr_atendimento)),1,254)),''N�o informado'') ds_municipio ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	03 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(285006) DS, 'IE_QUEBRA_03' NM, 'NM_CIRURGIAO' VL_QUEBRA, ' SUBSTR(obter_nome_pessoa_fisica(NVL(a.cd_medico_cirurgiao,''0''), NULL),1,120) nm_cirurgiaoa ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	04 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(283472) DS, 'IE_QUEBRA_26' NM, 'NM_ANESTESISTA' VL_QUEBRA, ' SUBSTR(obter_nome_pessoa_fisica(NVL(a.cd_medico_anestesista,''0''), NULL),1,120) nm_anestesista ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	05 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(310189) DS, 'IE_QUEBRA_04' NM, 'DT_PARAMETRO_QUEBRA' VL_QUEBRA, ' NVL(TO_CHAR(a.dt_inicio_real,''DD/MM/YYYY''),''N�o informado'') ' VL_TOTAL_QUEBRA 
FROM DUAL
UNION
SELECT	06 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(293568) DS, 'IE_QUEBRA_05' NM, 'DS_MOTIVO_INTERR' VL_QUEBRA, ' substr(Obter_dados_interr_cirurgia(a.nr_seq_interrupcao,''M''),1,255) ds_motivo_interr ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	07 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(300543) DS, 'IE_QUEBRA_06' NM, 'DS_TURNO' VL_QUEBRA, ' SUBSTR(Obter_Turno_Cirurgia(a.dt_inicio_real,a.cd_estabelecimento,''D''),1,150) ds_turno ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	08 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(293485) DS, 'IE_QUEBRA_07' NM, 'DS_MOTIVO_ALTA' VL_QUEBRA, ' SUBSTR(obter_desc_motivo_alta(c.cd_motivo_alta),1,60) ds_motivo_alta ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	09 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(759245) DS, 'IE_QUEBRA_24' NM, 'DS_SETOR_ATENDIMENTO' VL_QUEBRA, ' SUBSTR(obter_nome_setor(NVL(b.cd_setor_atendimento,0)),1,100) ds_setor_atendimento ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	10 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(296081) DS, 'IE_QUEBRA_25' NM, 'DS_PORTE_CIRURGIA' VL_QUEBRA, ' SUBSTR(NVL(obter_valor_dominio(1901, a.ie_porte),''Sem informa��o''),1,100) ds_porte_cirurgia ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	11 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(309493) DS, 'IE_QUEBRA_08' NM, 'DS_SETOR_INTERNACAO' VL_QUEBRA, ' SUBSTR(Obter_Unidade_Atendimento(c.nr_atendimento,''IA'',''S''),1,200) ds_setor_internacao ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	12 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(304239) DS, 'IE_QUEBRA_09' NM, 'DS_ESPEC_PROC' VL_QUEBRA, ' a.ie_carater_cirurgia, SUBSTR(obter_desc_espec_proc(Obter_Especialidade_Proced(e.cd_procedimento, e.ie_origem_proced)),1,50) ds_espec_proc ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	13 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(299472) DS, 'IE_QUEBRA_10' NM, 'DS_TIPOCIRURGIA' VL_QUEBRA, ' SUBSTR(OBTER_DESC_TIPO_CIRURGIA(a.cd_tipo_cirurgia),1,100) DS_TIPOCIRURGIA ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	14 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(713991) DS, 'IE_QUEBRA_11' NM, 'DT_MES' VL_QUEBRA, ' TO_CHAR(a.dt_inicio_real, ''MM/YYYY'') dt_mes ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	15 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(759221) DS, 'IE_QUEBRA_12' NM, 'DS_MOTIVO_CANC' VL_QUEBRA, ' SUBSTR(obter_dados_motivo_cancel(a.ie_motivo_cancelamento,''D''),1,255) ds_motivo_canc ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	16 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(294931) DS, 'IE_QUEBRA_13' NM, 'DS_ORIGEM_CONVENIO' VL_QUEBRA, ' substr(obter_origem_convenio(acc.cd_convenio),1,255) ds_origem_convenio ' VL_TOTAL_QUEBRA
FROM DUAL
UNION	
SELECT	17 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(688418) DS, 'IE_QUEBRA_14' NM, 'DS_PROCEDIMENTO' VL_QUEBRA, ' SUBSTR(DECODE(:ie_concat_codigo,''S'',a.cd_procedimento_princ||''-''||e.ds_procedimento,e.ds_procedimento),1,240) ds_procedimento ' VL_TOTAL_QUEBRA
FROM DUAL
UNION	
SELECT	18 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(10651975) DS, 'IE_QUEBRA_15' NM, 'DS_UNIDADE_CIRURGIA' VL_QUEBRA, ' NVL(b.cd_unidade_basica||'' ''||b.cd_unidade_compl,''N�o Informado'') ds_unidade_cirurgia ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	19 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(704103) DS, 'IE_QUEBRA_16' NM, 'DS_TIPO_ANESTESIA' VL_QUEBRA, ' NVL(SUBSTR(obter_valor_dominio(36,NVL(a.cd_tipo_anestesia,''NI'')),1,100), ''N�o Informado'') ds_tipo_anestesia ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	20 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(284572) DS, 'IE_QUEBRA_17' NM, 'DS_CARATER' VL_QUEBRA, ' SUBSTR(obter_valor_dominio(1016, a.ie_carater_cirurgia),1,254) ds_carater '  VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	21 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(296445) DS, 'IE_QUEBRA_18' NM, 'DS_PROC_INTERNO' VL_QUEBRA, ' substr(obter_desc_proc_interno(a.nr_seq_proc_interno),1,255) ds_proc_interno ' VL_TOTAL_QUEBRA
FROM DUAL
UNION	
SELECT	22 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(713999) DS, 'IE_QUEBRA_19' NM, 'DS_ESPEC_MED_PROC' VL_QUEBRA, '  NVL(SUBSTR(obter_espec_proc_regra(e.cd_procedimento,e.ie_origem_proced),1,90),''N�o informado'') ds_espec_med_proc ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	23 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(696048) DS, 'IE_QUEBRA_20' NM, 'DS_CONVENIO' VL_QUEBRA, ' SUBSTR(obter_nome_convenio(NVL(a.cd_convenio,0)),1,80) ds_convenio ' VL_TOTAL_QUEBRA 
FROM DUAL
UNION
SELECT	24 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(10651941) DS, 'IE_QUEBRA_22' NM, 'DS_ESPECIALIDADE_CIRURGIAO' VL_QUEBRA, ' SUBSTR(obter_especialidade_medico(a.cd_medico_cirurgiao, ''D''),1,100) ds_especialidade_cirurgiao ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	25 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(714001) DS, 'IE_QUEBRA_21' NM, 'DS_SALA_CIRURGIA' VL_QUEBRA, ' SUBSTR(obter_unid_setor_cirurgia(a.nr_cirurgia,''S''),1,120) ds_sala_cirurgia ' VL_TOTAL_QUEBRA
FROM DUAL
UNION
SELECT	26 IE_ORDEM, 'S' CD_SELECIONADO, 'N' CD_N_SELECIONADO, obter_desc_expressao(283796) DS, 'IE_QUEBRA_23' NM, 'DS_ASA' VL_QUEBRA, ' NVL(SUBSTR(obter_valor_dominio(1287,a.ie_asa_estado_paciente),1,100), ''N�o Informado'') ds_asa ' VL_TOTAL_QUEBRA
FROM DUAL;
/
