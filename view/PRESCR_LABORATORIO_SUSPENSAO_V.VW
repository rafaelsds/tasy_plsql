CREATE OR REPLACE VIEW PRESCR_LABORATORIO_SUSPENSAO_V
AS
select 
	a.NR_PRESCRICAO,
	a.NR_SEQUENCIA,
	a.CD_PROCEDIMENTO,
	p.ds_procedimento,
	a.QT_PROCEDIMENTO,
	a.DT_ATUALIZACAO,
	a.NM_USUARIO,
	a.DS_OBSERVACAO,
	a.IE_ORIGEM_PROCED,
	a.IE_URGENCIA,
	a.DS_DADO_CLINICO,
	a.IE_SUSPENSO,
	a.cd_setor_atendimento,
	b.NR_ATENDIMENTO,
 	b.CD_MEDICO,
 	b.DT_PRESCRICAO,
	b.DT_LIBERACAO,
	b.DT_LIBERACAO_MEDICO,
	b.IE_RECEM_NATO,
	g.cd_setor_atendimento cd_setor_paciente,
	decode(b.CD_RECEM_NATO,null,c.NM_PESSOA_FISICA,substr(obter_nome_pf(b.CD_RECEM_NATO),1,80) ||' RN de '||c.NM_PESSOA_FISICA) NM_PACIENTE,
	elimina_acentuacao(UPPER(decode(b.CD_RECEM_NATO,null,c.NM_PESSOA_FISICA,substr(obter_nome_pf(b.CD_RECEM_NATO),1,80) ||' RN de '||c.NM_PESSOA_FISICA))) NM_PACIENTE_SEM_ACENTO,
	c.DT_NASCIMENTO,
	c.IE_SEXO,
	c.NR_CPF,
	c.NR_PRONTUARIO,
	d.NM_PESSOA_FISICA NM_MEDICO,
	d.nr_cpf nr_cpf_medico,		/*Elemar - 27/08/2008*/
	e.NR_CRM,
	e.UF_CRM,			/*Elemar - 27/08/2008*/
	F.CD_CONVENIO,	
	F.CD_CATEGORIA,
	F.CD_USUARIO_CONVENIO,
	F.DT_VALIDADE_CARTEIRA, 	/*Elemar - 27/08/2008*/
	F.NR_DOC_CONVENIO,		/*Elemar - 27/08/2008*/
	F.IE_TIPO_GUIA,			/*Elemar - 27/08/2008*/
	V.DS_CONVENIO,
	V.CD_CGC CD_CGC_CONV,		/*Elemar - 27/08/2008*/
	V.CD_REGIONAL CD_REGIONAL_CONV,	/*Elemar - 27/08/2008*/
	A.CD_MATERIAL_EXAME,
	m.ds_material_exame,
	Y.CD_EXAME,
	y.nm_exame,
	A.DS_MATERIAL_ESPECIAL,
	nvl(a.ie_amostra,'N') ie_amostra_entregue,
	a.ds_horarios,
	a.nr_seq_exame,
	z.ds_endereco,
	z.nr_endereco,
	z.ds_complemento,
	z.ds_bairro,
	z.ds_municipio,
	z.sg_estado,
	z.nr_telefone,
	z.cd_cep,
	a.dt_prev_execucao,
	s.ds_setor_atendimento ds_setor_paciente,
	g.cd_unidade_basica || ' ' || cd_unidade_compl cd_unidade,
	obter_preco_procedimento(t.cd_estabelecimento, f.cd_convenio, f.cd_categoria,
		b.dt_prescricao, a.cd_procedimento, a.ie_origem_proced,
		f.cd_tipo_acomodacao, t.ie_tipo_atendimento, g.cd_setor_atendimento,
		b.cd_medico, null, 
		f.cd_usuario_convenio, f.cd_plano_convenio, t.ie_clinica, f.cd_empresa,
		'P') vl_procedimento,
	a.ie_executar_leito,
	b.CD_RECEM_NATO,
	substr(obter_nome_pf(b.CD_RECEM_NATO),1,80) NM_RECEM_NATO,
	y.nr_seq_grupo,
	b.cd_estabelecimento,
	a.nr_seq_proc_interno,
	substr(obter_proc_interno(a.nr_seq_proc_interno,'CI'),1,20) cd_interno_integracao,
	y.cd_exame_integracao cd_exame_integracao,
	substr(obter_desc_triagem(t.nr_seq_triagem),1,60) ds_classif_risco
FROM	setor_atendimento s,
	Procedimento p,
	CONVENIO V,
	COMPL_PESSOA_FISICA Z,
	ATEND_PACIENTE_UNIDADE G,
	ATEND_CATEGORIA_CONVENIO F,
	DUAL X,
	EXAME_LABORATORIO Y,
	MATERIAL_EXAME_LAB M,
	MEDICO E,
	ATENDIMENTO_PACIENTE T,
	PESSOA_FISICA D,
	PESSOA_FISICA C,
	PRESCR_MEDICA B,
      PRESCR_PROCEDIMENTO A
WHERE a.dt_integracao_suspenso is null
    and a.cd_procedimento		= p.cd_procedimento
    and a.ie_origem_proced		= p.ie_origem_proced
    and nvl(dt_liberacao_medico, dt_liberacao) is not null
    AND A.NR_PRESCRICAO 		= B.NR_PRESCRICAO
    AND B.CD_PESSOA_FISICA 		= C.CD_PESSOA_FISICA
    AND B.CD_PESSOA_FISICA		= Z.CD_PESSOA_FISICA (+)
    AND 1					= Z.IE_TIPO_COMPLEMENTO (+)
    AND B.CD_MEDICO 			= D.CD_PESSOA_FISICA
    AND B.CD_MEDICO 			= E.CD_PESSOA_FISICA (+)
    AND nvl(A.NR_SEQ_EXAME,obter_exame_lab_proc_int(a.nr_seq_proc_interno)) = Y.NR_SEQ_EXAME(+)
    and A.CD_MATERIAL_EXAME		= M.CD_MATERIAL_EXAME (+)
    AND G.NR_SEQ_INTERNO		= obter_atepacu_paciente(b.nr_atendimento,'A')
    AND G.CD_SETOR_ATENDIMENTO	= S.CD_SETOR_ATENDIMENTO
    AND NVL(A.IE_SUSPENSO,'N')	= 'S'
    AND A.CD_SETOR_ATENDIMENTO in
	(SELECT S.CD_SETOR_ATENDIMENTO
	FROM SETOR_ATENDIMENTO S
	WHERE S.NM_USUARIO_BANCO IN
		(SELECT USER FROM DUAL))
    AND F.NR_ATENDIMENTO  		= B.NR_ATENDIMENTO
    AND T.NR_ATENDIMENTO  		= B.NR_ATENDIMENTO
    AND F.DT_INICIO_VIGENCIA 		= 
      (SELECT MAX(W.DT_INICIO_VIGENCIA) 
       FROM ATEND_CATEGORIA_CONVENIO W
       WHERE W.NR_ATENDIMENTO  = B.NR_ATENDIMENTO)
    AND F.CD_CONVENIO           	= V.CD_CONVENIO;	
/
--DROP PUBLIC SYNONYM PRESCR_LABORATORIO_SUSPENSAO_V;
--cREATE PUBLIC SYNONYM PRESCR_LABORATORIO_SUSPENSAO_V FOR TASY.PRESCR_LABORATORIO_SUSPENSAO_V;
--GRANT SELECT ON PRESCR_LABORATORIO_SUSPENSAO_V TO LABORATORIO;