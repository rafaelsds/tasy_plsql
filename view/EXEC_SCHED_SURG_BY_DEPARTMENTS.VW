create or replace view EXEC_SCHED_SURG_BY_DEPARTMENTS as
select a.nr_cirurgia nr_surgery,
       get_main_and_addit_data_surg('GPN', null, a.nr_atendimento) nm_patient,
       a.nr_atendimento nr_attention,
       obter_sala_cirurgia(a.nr_cirurgia) ds_surgery_room,
       obter_nome_pf(a.cd_medico_cirurgiao) nm_surgeon,
       a.dt_inicio_real dt_surgery_start,
       get_main_and_addit_data_surg('GFSSD', a.nr_cirurgia, null) dt_sch_formatted_surgery,
       get_main_and_addit_data_surg('CDSS', a.nr_cirurgia, null) cd_departm_schd_surg,
       get_main_and_addit_data_surg('DDSS', a.nr_cirurgia, null) ds_departm_schd_surg,
       get_main_and_addit_data_surg('CDPS', a.nr_cirurgia, null) cd_departm_perf_surg,
       get_main_and_addit_data_surg('DDPS', a.nr_cirurgia, null) ds_departm_perf_surg
from   cirurgia a
/
