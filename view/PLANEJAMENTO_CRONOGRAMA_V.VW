CREATE OR REPLACE VIEW planejamento_cronograma_v
AS
select 	2 nr_consistencia,
	a.nr_sequencia,
	substr(obter_valor_dominio(4957,2),1,100)  ds_consistencia
from 	proj_projeto a
where	not exists ( select	1
		     from	proj_cronograma x
		     where	x.nr_seq_proj = a.nr_sequencia)
and	a.DT_INICIO_PREV <= sysdate;
/