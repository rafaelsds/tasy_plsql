create or replace view	 ptu_consulta_beneficiario_v
as
select	1				ie_tipo_registro,
	00312				cd_transacao,
	nr_sequencia			nr_sequencia,
	nr_seq_execucao 		nr_seq_execucao,
	nr_seq_guia 			nr_seq_guia,
	decode(ie_tipo_cliente,'U','UNIMED','P','PORTAL','R','PRESTADOR') ie_tipo_cliente,
	cd_unimed_executora 		cd_unimed_executora,
	cd_unimed_beneficiario 		cd_unimed_beneficiario,
	cd_unimed 			cd_unimed,
	cd_usuario_plano 		cd_usuario_plano,
	dt_nascimento 			dt_nascimento,
	nm_beneficiario 		nm_beneficiario,
	sobrenome_beneficiario		sobrenome_beneficiario,
	null				ds_fim
from	ptu_consulta_beneficiario
union
select	2				ie_tipo_registro,
	null				cd_transacao,
	nr_sequencia			nr_sequencia,
	nr_seq_execucao 		nr_seq_execucao,
	nr_seq_guia 			nr_seq_guia,
	null 				ie_tipo_cliente,
	null 				cd_unimed_executora,
	null 				cd_unimed_beneficiario,
	null 				cd_unimed,
	null 				cd_usuario_plano,
	null				dt_nascimento,
	null		 		nm_beneficiario,
	null				sobrenome_beneficiario,
	'FIM$'				ds_fim
from	ptu_consulta_beneficiario;
/
