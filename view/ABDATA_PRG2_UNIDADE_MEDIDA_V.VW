create or replace view abdata_prg2_unidade_medida_v as
select 'cm' cd_unidade_medida, 'Zentimeter' ds_unidade_medida from dual union all
select 'Fl' cd_unidade_medida, 'Flasche' ds_unidade_medida from dual union all
select 'g' cd_unidade_medida, 'Gramm' ds_unidade_medida from dual union all
select 'IE' cd_unidade_medida, 'Internationale Einheiten' ds_unidade_medida from dual union all
select 'kg' cd_unidade_medida, 'Kilogramm' ds_unidade_medida from dual union all
select 'l' cd_unidade_medida, 'Liter' ds_unidade_medida from dual union all
select 'm' cd_unidade_medida, 'Meter' ds_unidade_medida from dual union all
select 'mg' cd_unidade_medida, 'Milligramm' ds_unidade_medida from dual union all
select 'ml' cd_unidade_medida, 'Milliliter' ds_unidade_medida from dual union all
select 'mm' cd_unidade_medida, 'Millimeter' ds_unidade_medida from dual union all
select 'P' cd_unidade_medida, 'Packung' ds_unidade_medida from dual union all
select 'Sp' cd_unidade_medida, 'Spr�hst��e' ds_unidade_medida from dual union all
select 'St' cd_unidade_medida, 'St�ck' ds_unidade_medida from dual union all
select '�g' cd_unidade_medida, 'Mikrogramm' ds_unidade_medida from dual union all
select 'St' cd_unidade_medida, 'St�ck' ds_unidade_medida from dual union all
select 'Spritze' cd_unidade_medida, 'Spritze' ds_unidade_medida from dual union all
select 'Pipette' cd_unidade_medida, 'Pipette' ds_unidade_medida from dual union all
select 'Dosis' cd_unidade_medida, 'Dosis' ds_unidade_medida from dual union all
select 'Seifenst�ck' cd_unidade_medida, 'Seifenst�ck' ds_unidade_medida from dual union all
select 'Klistier' cd_unidade_medida, 'Klistier' ds_unidade_medida from dual union all
select 'Schwamm' cd_unidade_medida, 'Schwamm' ds_unidade_medida from dual union all
select 'Messl�ffel' cd_unidade_medida, 'Messl�ffel' ds_unidade_medida from dual union all
select 'Glasflasche' cd_unidade_medida, 'Glasflasche' ds_unidade_medida from dual union all
select 'Pessar' cd_unidade_medida, 'Pessar' ds_unidade_medida from dual union all
select 'Ring' cd_unidade_medida, 'Ring' ds_unidade_medida from dual union all
select 'Kompresse' cd_unidade_medida, 'Kompresse' ds_unidade_medida from dual union all
select 'Tuch' cd_unidade_medida, 'Tuch' ds_unidade_medida from dual union all
select 'Applikator' cd_unidade_medida, 'Applikator' ds_unidade_medida from dual union all
select 'H�be' cd_unidade_medida, 'H�be' ds_unidade_medida from dual union all
select 'Einzeldosis' cd_unidade_medida, 'Einzeldosis' ds_unidade_medida from dual union all
select 'Pastille' cd_unidade_medida, 'Pastille' ds_unidade_medida from dual union all
select 'Spr�hsto�' cd_unidade_medida, 'Spr�hsto�' ds_unidade_medida from dual union all
select 'Occusert' cd_unidade_medida, 'Occusert' ds_unidade_medida from dual union all
select 'qcm' cd_unidade_medida, 'Quadratzentimeter' ds_unidade_medida from dual;
/
