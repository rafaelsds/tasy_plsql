CREATE OR REPLACE VIEW RPAMH_CTB_BALANCETE_V
AS
select
	'N' ie_normal_encerramento,
	b.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.nr_seq_mes_ref,
	nvl(a.cd_classificacao, b.cd_classificacao) cd_classificacao,
	b.ds_conta_contabil,
	substr(ctb_obter_conta_apres(b.cd_conta_contabil),1,255) ds_conta_apres,
	b.cd_grupo,
	b.ie_tipo,
	a.vl_debito,
	a.vl_credito,
	a.vl_saldo,
	a.vl_movimento,
	a.vl_saldo - a.vl_movimento vl_saldo_ant,
	nvl(a.nr_nivel_conta, ctb_obter_nivel_classif_conta(nvl(a.cd_classificacao, b.cd_classificacao))) ie_nivel,
	obter_classif_ctb(nvl(a.cd_classificacao, b.cd_classificacao), 'SUPERIOR') ie_classif_superior,
	b.ie_centro_custo,
	c.ie_tipo ie_tipo_conta,
	a.cd_estabelecimento cd_estab,
	a.cd_classif_sup,
	b.ie_situacao,
	a.cd_centro_custo
from	ctb_mes_ref d,
	ctb_grupo_conta c,
	conta_contabil b,
	ctb_saldo a
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	d.nr_sequencia		= a.nr_seq_mes_ref
and	d.cd_empresa		= b.cd_empresa
and	b.cd_grupo		= c.cd_grupo (+)
union all
select
	'E' ie_normal_encerramento,
	b.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.nr_seq_mes_ref,
	nvl(a.cd_classificacao, b.cd_classificacao) cd_classificacao,
	b.ds_conta_contabil,
	substr(ctb_obter_conta_apres(b.cd_conta_contabil),1,255) ds_conta_apres,
	b.cd_grupo,
	b.ie_tipo,
	a.vl_debito - nvl(a.vl_enc_debito,0) vl_debito,
	a.vl_credito - nvl(a.vl_enc_credito,0) vl_credito,
	a.vl_saldo - a.vl_encerramento,
	a.vl_movimento - a.vl_encerramento,
	a.vl_saldo - a.vl_movimento vl_saldo_ant,
	nvl(a.nr_nivel_conta, ctb_obter_nivel_classif_conta(nvl(a.cd_classificacao, b.cd_classificacao))) ie_nivel,
	obter_classif_ctb(nvl(a.cd_classificacao, b.cd_classificacao), 'SUPERIOR') ie_classif_superior,
	b.ie_centro_custo,
	c.ie_tipo ie_tipo_conta,
	a.cd_estabelecimento cd_estab,
	a.cd_classif_sup,
	b.ie_situacao,
	a.cd_centro_custo
from	ctb_mes_ref d,
	ctb_grupo_conta c,
	conta_contabil b,
	ctb_saldo a
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	d.nr_sequencia		= a.nr_seq_mes_ref
and	d.cd_empresa		= b.cd_empresa
and	b.cd_grupo		= c.cd_grupo (+);
/