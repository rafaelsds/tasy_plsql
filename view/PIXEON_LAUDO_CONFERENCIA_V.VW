CREATE OR REPLACE 
VIEW Pixeon_Laudo_conferencia_v
AS
--view utilizada na integra��o PACS com a Pixeon para enviar a informa��o do laudo ap�s confer�ncia para ser atualizado na integra��o
select	a.nr_sequencia,
	a.dt_integracao,
	a.dt_laudo,
	a.ds_laudo,
	b.nr_acesso_dicom
from	prescr_medica d,
	prescr_procedimento b,
	laudo_paciente a
where 	d.nr_prescricao = b.nr_prescricao
and	a.nr_prescricao	= b.nr_prescricao
and	a.nr_seq_prescricao	= b.nr_sequencia
and	b.ie_status_execucao = 44
and	a.dt_liberacao is not null;

DROP PUBLIC SYNONYM Pixeon_Laudo_conferencia_v;

CREATE PUBLIC SYNONYM Pixeon_Laudo_conferencia_v FOR TASY.Pixeon_Laudo_conferencia_v;
	
GRANT SELECT ON Pixeon_Laudo_conferencia_v TO Pixeon;