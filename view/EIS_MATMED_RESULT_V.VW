create or replace
view EIS_MATMED_RESULT_V as
	select	b.ds_material,
		substr(obter_dados_material_estab(a.cd_material,a.cd_estabelecimento,'UME'),1,30) ds_unidade_medida,
		substr(obter_dados_material_estab(a.cd_material,a.cd_estabelecimento,'UME'),1,30) cd_unidade_medida_estoque,
		obter_margem(a.vl_custo_compra, a.vl_medio_fatur, 'P') pr_margem_compra,
		obter_margem(a.vl_custo_medio, a.vl_medio_fatur, 'P') pr_margem_estoque,
		obter_margem(a.vl_custo_medio, a.vl_preco_convenio, 'P') pr_margem_convenio,
		substr(obter_mat_comercial(a.cd_material,'D'),1,100) ds_material_comercial,	
		substr(obter_curva_abc(a.cd_material, '', a.dt_referencia),1,10) ie_curva_abc,
		obter_mat_estabelecimento(a.cd_estabelecimento, 0, a.cd_material, 'CM') qt_cons_medio,
		dividir(a.vl_custo_medio, vl_medio_fatur) vl_margem,
		c.cd_classe_material,
		d.cd_subgrupo_material,
		e.cd_grupo_material,
		b.ie_consignado,
		substr(obter_nome_convenio(a.cd_convenio),1,200) ds_convenio,
		substr(obter_desc_material(b.cd_material_generico),1,200) ds_material_generico,
		substr(obter_valor_dominio(29,b.ie_tipo_material),1,200) ds_tipo_material,
		b.ie_tipo_material,
		c.ds_classe_material,
		d.ds_subgrupo_material,
		e.ds_grupo_material,
		b.cd_material_generico,
		to_number(obter_mat_comercial(a.cd_material,'C'))  cd_material_comercial,
		a.*
	from	grupo_material e,
		subgrupo_material d,
		classe_material c,
		material b,
		eis_matmed_result a
	where	a.cd_material	= b.cd_material
	and	b.cd_classe_material		= c.cd_classe_material
	and	c.cd_subgrupo_material	= d.cd_subgrupo_material
	and	d.cd_grupo_material		= e.cd_grupo_material
	and	a.ie_orig_estoque		= 'E';
/