CREATE or REPLACE VIEW SUS_CIH_2008_V
AS
SELECT	0 NR_ATENDIMENTO,
	(d.cd_estabelecimento) CD_ESTABELECIMENTO,
	null CD_PESSOA_FISICA,
	sysdate DT_ENTRADA,
	0 ie_tipo_atendimento,
	sysdate DT_ALTA,
	0 cd_motivo_alta,
	0 IE_TIPO_CONVENIO,
	null NM_PACIENTE,
	0 NR_PRONTUARIO,
	sysdate DT_NASCIMENTO,
	null IE_SEXO,
	null NM_LOGRADOURO,
	0 NR_ENDERECO,
	null DS_COMPLEMENTO,
	null SG_ESTADO_PACIENTE,
	null CD_CEP,
	null CD_MUNICIPIO,
	null CD_CGC,
	null SG_ESTADO_CGC,
	null CD_CEP_CGC,
	null DS_BAIRRO_CGC,
	0 CD_PROCEDIMENTO,
	0 IE_ORIGEM_PROCED,
	null CD_CID_PRIMARIO,
	null CD_CID_SECUNDARIO,
	null cd_municipio_ibge,
	null CD_CNS,
	null CD_ANS,
	null IE_FONTE_REMUNERACAO,
	null IE_FONTE_REMUNER2,
	sysdate DT_COMPETENCIA,
	(substr(d.cd_cns, 1,8)) CD_CNES,
	null ds_procedimento,
	null cd_cnpj,
	null CD_USUARIO_CONVENIO,
	null nr_declaracao_obito,
	0 QT_DIAS_UTI,
	0 cd_convenio,
	sysdate dt_inicio_vigencia,
	null cd_categoria,
	0 qt_filhos_vivos,
	null doc_nasc1,
	null doc_nasc2,
	null doc_nasc3,
	null doc_nasc4,
	null doc_nasc5,
	1 IE_TIPO_REG_INTERFACE,
	'4.0.0.0' DS_VERSAO_INTERFACE,
	null ie_alta_uti,
	null ie_sexo_sus,
	null cd_internacional,
	null nm_usuario_atend
FROM	ESTABELECIMENTO D
WHERE	d.cd_cns is not null
union all
SELECT	A.NR_ATENDIMENTO,
	A.CD_ESTABELECIMENTO,
	A.CD_PESSOA_FISICA,
       A.DT_ENTRADA,
       A.IE_TIPO_ATENDIMENTO,
       A.DT_ALTA,
	A.CD_MOTIVO_ALTA,
	A.IE_TIPO_CONVENIO,
       B.NM_PESSOA_FISICA NM_PACIENTE,
       nvl(B.NR_PRONTUARIO,to_number(obter_prontuario_pf(a.cd_estabelecimento,a.cd_pessoa_fisica))),
       B.DT_NASCIMENTO,
       B.IE_SEXO,
	C.DS_ENDERECO NM_LOGRADOURO,
	C.NR_ENDERECO,
	C.DS_COMPLEMENTO,
	C.SG_ESTADO SG_ESTADO_PACIENTE,
	REPLACE(C.CD_CEP,'-','') CD_CEP,
	F.CD_MUNICIPIO_SINPAS CD_MUNICIPIO,
	E.CD_CGC,
	E.SG_ESTADO SG_ESTADO_CGC,
	E.CD_CEP CD_CEP_CGC,
	E.DS_BAIRRO DS_BAIRRO_CGC,
	G.CD_PROCEDIMENTO,
	G.IE_ORIGEM_PROCED,
	G.CD_CID_PRIMARIO,
	G.CD_CID_SECUNDARIO,
	C.CD_MUNICIPIO_IBGE,
	' ' CD_CNS,
	p.CD_ANS,
	DECODE(A.IE_TIPO_CONVENIO,1,'P','C') IE_FONTE_REMUNERACAO,
	nvl(v.cd_fonte_remuner_cih, DECODE(A.IE_TIPO_CONVENIO,1,'2',decode(A.IE_TIPO_CONVENIO,4,'3',6,'6',9,'9','1'))) IE_FONTE_REMUNER2,
	A.DT_ALTA_INTERNO DT_COMPETENCIA,
	(substr(d.cd_cns,1,8)) CD_CNES,
	substr(Obter_Desc_Proc_CIH(G.CD_PROCEDIMENTO, g.nr_atendimento),1,188) ds_procedimento,
	decode(A.IE_TIPO_CONVENIO,1,'',v.cd_cgc) cd_cnpj,
	t.CD_USUARIO_CONVENIO CD_USUARIO_CONVENIO,
	lpad(SUBSTR(Obter_Declaracao_Obito(a.nr_atendimento),1,8),8,'0') nr_declaracao_obito,
--	substr(to_char(Obter_Declaracao_Obito(a.nr_atendimento), '00000000'),2,9) nr_declaracao_obito,
	DECODE(SUS_Obter_Dias_UTI(a.nr_atendimento), 0, OBTER_DIAS_INTERNACAO_UTI(a.nr_atendimento), 
SUS_Obter_Dias_UTI(a.nr_atendimento)) QT_DIAS_UTI,
	t.cd_convenio,
	t.dt_inicio_vigencia,
	t.cd_categoria,
	OBTER_QT_NASCIDOS_CIH(G.CD_PROCEDIMENTO,a.nr_atendimento) qt_filhos_vivos,
	substr(OBTER_DNV_NASCIDOS_CIH(G.CD_PROCEDIMENTO,1,a.nr_atendimento),1,11) doc_nasc1,
	substr(OBTER_DNV_NASCIDOS_CIH(G.CD_PROCEDIMENTO,2,a.nr_atendimento),1,11) doc_nasc2,
	substr(OBTER_DNV_NASCIDOS_CIH(G.CD_PROCEDIMENTO,3,a.nr_atendimento),1,11) doc_nasc3,
	substr(OBTER_DNV_NASCIDOS_CIH(G.CD_PROCEDIMENTO,4,a.nr_atendimento),1,11) doc_nasc4,
	substr(OBTER_DNV_NASCIDOS_CIH(G.CD_PROCEDIMENTO,5,a.nr_atendimento),1,11) doc_nasc5,
	2 IE_TIPO_REG_INTERFACE,
	'4.0.0.0' DS_VERSAO_INTERFACE,
	substr(OBTER_SE_ALTA_UTI(a.nr_atendimento), 1,1) ie_alta_uti,
	h.ie_sexo_sus ie_sexo_sus,
	p.cd_internacional,
	a.nm_usuario_atend
FROM	procedimento h,
	pessoa_juridica p,
	convenio v,
	sus_municipio f,
	COMPL_PESSOA_FISICA C,
	ESTABELECIMENTO D,
	PESSOA_JURIDICA E,
	PESSOA_FISICA B,
	PROCEDIMENTO_PACIENTE_CIH g,
	atend_categoria_convenio t,
	ATENDIMENTO_PACIENTE A
WHERE	A.CD_PESSOA_FISICA 		= B.CD_PESSOA_FISICA
  AND	B.CD_PESSOA_FISICA 		= C.CD_PESSOA_FISICA (+)
  and	h.cd_procedimento(+)		= g.cd_procedimento
  and	h.ie_origem_proced(+)		= g.ie_origem_proced
  AND	1				= C.IE_TIPO_COMPLEMENTO (+)
  AND	A.NR_ATENDIMENTO		= G.NR_ATENDIMENTO (+)
  AND	NVL(A.IE_TIPO_CONVENIO,1)	<> 3
  AND	A.CD_ESTABELECIMENTO		= D.CD_ESTABELECIMENTO
  AND	D.CD_CGC			= E.CD_CGC
  AND	C.CD_MUNICIPIO_IBGE		= F.CD_MUNICIPIO_IBGE(+)
  and	a.nr_atendimento		= t.nr_atendimento
  and	t.cd_convenio			= v.cd_convenio
  and   v.cd_cgc			= p.cd_cgc
  and	obter_atecaco_atendimento(t.nr_atendimento) 	= t.nr_seq_interno
  and	nvl(C.CD_MUNICIPIO_IBGE,1)		<>  '999999' -- Bruna, OS61446
  and 	t.dt_inicio_vigencia= 
	(select max(dt_inicio_vigencia)
	from atend_categoria_convenio b
	where nr_atendimento 		= a.nr_atendimento)
  AND 	EXISTS 
		(SELECT	1
             	FROM		SETOR_ATENDIMENTO Y,
				ATEND_PACIENTE_UNIDADE Z
		WHERE		Z.CD_SETOR_ATENDIMENTO = Y.CD_SETOR_ATENDIMENTO
		AND		A.NR_ATENDIMENTO       = Z.NR_ATENDIMENTO
		AND		Y.CD_CLASSIF_SETOR IN (2,3,4,8));
/