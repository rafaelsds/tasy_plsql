create or replace view audc_dados_promoprev_v as	
select  c.nm_programa,
        b.dt_inclusao,
        b.dt_exclusao,
	a.cd_pessoa_fisica,
	b.nr_sequencia
from    mprev_participante a,
   	mprev_programa_partic b,
        mprev_programa c
where   b.nr_seq_participante = a.nr_sequencia
and     b.nr_seq_programa = c.nr_sequencia
and    	b.dt_inclusao <= sysdate;
/