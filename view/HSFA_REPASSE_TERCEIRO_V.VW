create or replace view HSFA_REPASSE_TERCEIRO_V as
select	substr(obter_dados_atendimento(x.nr_atendimento,'NP'),1,150) nm_paciente,
	nm_medico,
	nr_seq_procedimento,
	substr(obter_nome_terceiro(x.nr_seq_terceiro),1,150) terceiro,
	substr(obter_nome_convenio(x.cd_convenio),1,150) ds_convenio,
	x.ds_item,
	x.dt_entrada,
	x.nr_atendimento,
	sum(x.vl_faturado) vl_faturado,
	sum(x.vl_repasse) vl_repasse,
	x.vl_repasse vl_repasse_b,
	sum(x.perc_repasse)/count(*) perc_repasse,
	sum(x.vl_glosado) vl_glosado,
	sum(x.vl_amaior) vl_amaior,
	sum(x.vl_reap) vl_reap,
	x.dt_mesano_referencia,
	x.cd_convenio,
	x.nr_seq_terceiro,
	x.nr_protocolo,
	x.nr_interno_conta,
	x.nr_Seq_protocolo,
	x.ds_centro_custo,
	x.cd_setor_atendimento,
	x.cd_categoria
from(
select	c.nr_atendimento,
	c.cd_Procedimento || ' - ' || substr(obter_descricao_procedimento(c.cd_procedimento,c.ie_origem_proced),1,100) ds_item,
	f.dt_entrada,
	d.nr_seq_procedimento,
	d.nr_seq_terceiro,
	nvl(c.vl_medico, 0) vl_faturado,
	d.vl_repasse,
	(dividir(d.vl_repasse, nvl(c.vl_medico, 0))) * 100 perc_repasse,
	a.cd_convenio,
	decode(d.ie_status,'G',d.vl_repasse,0) vl_glosado,
	somente_positivo(d.vl_liberado - d.vl_repasse) vl_amaior,
	decode(d.ie_status,'U',d.vl_repasse,'A',d.vl_repasse,'D',d.vl_repasse,0) vl_reap,
	obter_nome_medico(c.cd_medico_executor,'G') nm_medico,
	a.dt_mesano_referencia,
	('PROT: ' || a.nr_seq_protocolo || ' ' || a.nr_protocolo || ' DT. REF. ' || a.dt_mesano_referencia) nr_protocolo,
	b.nr_interno_conta nr_interno_conta,
	a.nr_Seq_protocolo,
	i.cd_centro_custo || decode(c.cd_setor_atendimento,null,null,' - ') || substr(obter_nome_setor(c.cd_setor_atendimento),1,255) ds_centro_custo,
	c.cd_setor_atendimento,
	b.cd_categoria_parametro cd_categoria
from 	setor_atendimento i,
	protocolo_convenio a,
	conta_paciente b,
	atendimento_paciente f,
	procedimento_paciente c,
	procedimento_repasse d,
	terceiro_pessoa_fisica h,
	terceiro g
where	h.nr_seq_terceiro	= g.nr_sequencia
and	d.nr_seq_terceiro	= g.nr_sequencia
and	h.cd_pessoa_fisica	= c.cd_medico_executor
and	a.nr_seq_protocolo	= b.nr_seq_protocolo
and	c.nr_interno_conta 	= b.nr_interno_conta
and	d.nr_seq_procedimento 	= c.nr_sequencia
and	d.nr_seq_partic		is null
and	f.nr_atendimento 	= c.nr_atendimento 
and  	d.ie_status 		= 'A'
and	c.cd_setor_atendimento	= i.cd_setor_atendimento
union all
select	c.nr_atendimento,
	c.cd_Procedimento || ' - ' || substr(obter_descricao_procedimento(c.cd_procedimento,c.ie_origem_proced),1,100) ds_item,
	f.dt_entrada,
	d.nr_seq_procedimento,
	d.nr_seq_terceiro,
	nvl(e.vl_participante,0) vl_faturado,
	d.vl_repasse,
	(dividir(d.vl_repasse, nvl(e.vl_participante, 0))) * 100 perc_repasse,
	a.cd_convenio,
	decode(d.ie_status,'G',d.vl_repasse,0) vl_glosado,
	somente_positivo(d.vl_liberado - d.vl_repasse) vl_amaior,
	decode(d.ie_status,'U',d.vl_repasse,'A',d.vl_repasse,'D',d.vl_repasse,0) vl_reap,
	obter_nome_medico(e.cd_pessoa_fisica,'G') nm_medico,
	a.dt_mesano_referencia,
	('PROT: ' || a.nr_seq_protocolo || ' ' || a.nr_protocolo || ' DT. REF. ' || a.dt_mesano_referencia),
	b.nr_interno_conta,
	a.nr_Seq_protocolo,
	i.cd_centro_custo || decode(c.cd_setor_atendimento,null,null,' - ') || substr(obter_nome_setor(c.cd_setor_atendimento),1,255) ds_centro_custo,
	c.cd_setor_atendimento,
	b.cd_categoria_parametro cd_categoria
from 	setor_atendimento i,
	protocolo_convenio a,
	conta_paciente b,
	atendimento_paciente f,
	procedimento_participante e,
	procedimento_paciente c,
	procedimento_repasse d,
	terceiro_pessoa_fisica h,
	terceiro g
where	h.nr_seq_terceiro	= g.nr_sequencia
and	d.nr_seq_terceiro	= g.nr_sequencia
and	h.cd_pessoa_fisica	= e.cd_pessoa_fisica
and	a.nr_seq_protocolo	= b.nr_seq_protocolo
and	c.nr_interno_conta 	= b.nr_interno_conta
and	d.nr_seq_procedimento 	= c.nr_sequencia
and	f.nr_atendimento 	= c.nr_atendimento 
and	d.nr_seq_procedimento	= e.nr_sequencia
and	d.nr_seq_partic		= e.nr_seq_partic
and  	d.ie_status 		= 'A'
and	c.cd_setor_atendimento	= i.cd_setor_atendimento
union all
select	c.nr_atendimento,
	c.cd_material || ' - ' || substr(obter_desc_material(c.cd_material),1,100) ds_item,
	f.dt_entrada,
	d.nr_seq_material,
	d.nr_seq_terceiro,
	nvl(c.vl_material, 0) vl_faturado,
	d.vl_repasse,
	(dividir(d.vl_repasse,nvl(c.vl_material, 0))) * 100 perc_repasse, 
	a.cd_convenio,
	decode(d.ie_status,'G',d.vl_repasse,0) vl_glosado,
	somente_positivo(d.vl_liberado - d.vl_repasse) vl_amaior,
	decode(d.ie_status,'U',d.vl_repasse,'A',d.vl_repasse,'D',d.vl_repasse,0) vl_reap,
	obter_nome_medico(f.cd_medico_atendimento,'G') nm_medico,
	a.dt_mesano_referencia,
	('PROT: ' || a.nr_seq_protocolo || ' ' || a.nr_protocolo || ' DT. REF. ' || a.dt_mesano_referencia),
	b.nr_interno_conta,
	a.nr_Seq_protocolo,
	i.cd_centro_custo || decode(c.cd_setor_atendimento,null,null,' - ') || substr(obter_nome_setor(c.cd_setor_atendimento),1,255) ds_centro_custo,
	c.cd_setor_atendimento,
	b.cd_categoria_parametro cd_categoria
from 	setor_atendimento i,
	protocolo_convenio a,
	conta_paciente b,
	atendimento_paciente f,
	material_atend_paciente c,
	material_repasse d,
	terceiro_pessoa_fisica h,
	terceiro g
where	h.nr_seq_terceiro	= g.nr_sequencia
and	f.cd_medico_atendimento	= h.cd_pessoa_fisica
and	g.nr_sequencia		= d.nr_seq_terceiro
and	a.nr_seq_protocolo	= b.nr_seq_protocolo
and	c.nr_interno_conta	= b.nr_interno_conta
and	d.nr_seq_material	= c.nr_sequencia
and	f.nr_atendimento 	= c.nr_atendimento
and  	d.ie_status 		= 'A'
and	c.cd_setor_atendimento	= i.cd_setor_atendimento
) x
group by substr(obter_dados_atendimento(x.nr_atendimento,'NP'),1,150),
	substr(obter_nome_terceiro(x.nr_seq_terceiro),1,150),
	substr(obter_nome_convenio(x.cd_convenio),1,150),
	x.ds_item,
	x.vl_repasse,
	x.dt_entrada,
	x.nr_atendimento,
	nm_medico,
	x.dt_mesano_referencia,
	x.cd_convenio,
	nr_seq_procedimento,
	x.nr_seq_terceiro,
	x.nr_protocolo,
	x.nr_interno_conta,
	x.nr_Seq_protocolo,
	x.ds_centro_custo,
	x.cd_setor_atendimento,
	x.cd_categoria
/