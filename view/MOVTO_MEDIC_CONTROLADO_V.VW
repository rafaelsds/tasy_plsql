Create or Replace
View Movto_Medic_Controlado_v  as
select	e.nr_movimento_estoque,
	e.dt_mesano_referencia,
	e.cd_material_estoque,
	e.ie_origem_documento,
	e.nr_seq_tab_orig,
	substr(obter_desc_material(e.cd_material_estoque),1,100) ds_material,
	substr(obter_desc_redu_material(e.cd_material_estoque),1,100) ds_reduzida,
	substr(obter_desc_material(nvl(substr(obter_mat_nf_estoque(e.nr_seq_tab_orig,e.nr_sequencia_item_docto),1,100),e.cd_material)),1,100) ds_material_origem,
	substr(obter_desc_redu_material(nvl(substr(obter_mat_nf_estoque(e.nr_seq_tab_orig,e.nr_sequencia_item_docto),1,100),e.cd_material)),1,100) ds_material_origem_reduzida,
	e.nr_documento,
	e.nr_sequencia_documento,
	e.nr_sequencia_item_docto nr_seq_item,
	e.cd_fornecedor,
	e.cd_unidade_medida_estoque,
	e.cd_estabelecimento,
	e.dt_movimento_estoque,
	m.cd_classificacao,
	c.cd_dcb,
	c.ds_dcb,
	e.cd_acao,
	f.ie_tipo_requisicao,
	f.cd_operacao_estoque,
	sum(decode(f.ie_tipo_requisicao, '2', 
			decode(f.ie_entrada_saida, 'E', e.qt_estoque * -1,e.qt_estoque), 0)) qt_transf,
	sum(decode(f.ie_tipo_requisicao, '2',
			0, decode(f.ie_entrada_saida, 'E', decode(e.cd_acao,1,nvl(e.qt_estoque,0), nvl(e.qt_estoque,0) * -1), 0))) qt_entrada,
	sum(decode(f.ie_tipo_requisicao, '2',
			0, decode(f.ie_entrada_saida, 'S', decode(e.cd_acao,1,nvl(e.qt_estoque,0), nvl(e.qt_estoque,0) * -1), 0))) qt_saida,
	sum(nvl(e.qt_estoque,0)) qt_estoque,
	sum(decode(e.cd_acao,1,nvl(e.qt_estoque,0), nvl(e.qt_estoque,0) * -1)) qt_estoque_acao,
	sum(e.qt_movimento) qt_movimento,
	substr(obter_hist_medic_controlado(e.nr_movimento_estoque, 'S','N','N'),1,250) ds_historico,
	e.cd_local_estoque,
	e.nr_prescricao,
	e.nr_atendimento
from	operacao_estoque f,
	dcb_medic_controlado c,
	medic_controlado m,
	material d,
	movimento_estoque e
where	f.cd_operacao_estoque	= e.cd_operacao_estoque
and	d.cd_material 		= e.cd_material_estoque
and	e.cd_material_estoque	=  m.cd_material
and	m.nr_seq_dcb		= c.nr_sequencia
and	m.ie_situacao		= 'A'
group by	e.nr_movimento_estoque,
		e.dt_mesano_referencia,
		e.cd_material_estoque,
		e.ie_origem_documento,
		e.nr_seq_tab_orig,
		substr(obter_desc_material(e.cd_material_estoque),1,100),
		obter_desc_redu_material(e.cd_material_estoque),
		substr(obter_desc_material(nvl(substr(obter_mat_nf_estoque(e.nr_seq_tab_orig,e.nr_sequencia_item_docto),1,100),e.cd_material)),1,100),
		substr(obter_desc_redu_material(nvl(substr(obter_mat_nf_estoque(e.nr_seq_tab_orig,e.nr_sequencia_item_docto),1,100),e.cd_material)),1,100),
		e.nr_documento,
		e.nr_sequencia_documento,
		e.nr_sequencia_item_docto,
		e.cd_fornecedor,
		e.cd_unidade_medida_estoque,
		e.cd_estabelecimento,
		e.dt_movimento_estoque,
		m.cd_classificacao,
		c.cd_dcb,
		c.ds_dcb,
		e.cd_acao,
		e.nr_prescricao,
		f.ie_entrada_saida,
		f.ie_tipo_requisicao,
		f.cd_operacao_estoque,
		substr(obter_hist_medic_controlado(e.nr_movimento_estoque, 'S','N','N'),1,250),
		e.cd_local_estoque,
		e.nr_atendimento;
/
