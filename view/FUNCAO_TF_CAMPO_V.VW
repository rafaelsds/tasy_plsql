create or replace
view FUNCAO_TF_CAMPO_V as
select	'BORDERO_TIT_REC' nm_tabela,
	'NR_SEQ_TRANS_FINANC' nm_atributo,
	809 cd_funcao
from	dual
union
select	'TITULO_RECEBER_LIQ' nm_tabela,
	'NR_SEQ_TRANS_FIN' nm_atributo,
	801 cd_funcao
from	dual
union
select	'ADIANTAMENTO' nm_tabela,
	'NR_SEQ_TRANS_CAIXA' nm_atributo,
	813 cd_funcao
from	dual
union
select	'TITULO_RECEBER_LIQ' nm_tabela,
	'NR_SEQ_TRANS_CAIXA' nm_atributo,
	813 cd_funcao
from	dual
union
select	'MOVTO_TRANS_FINANC' nm_tabela,
	'NR_SEQ_TRANS_FINANC' nm_atributo,
	813 cd_funcao
from	dual
union
select	'TITULO_RECEBER_COBR' nm_tabela,
	'NR_SEQ_TRANS_FINANC' nm_atributo,
	815 cd_funcao
from	dual
order by 1,2;
/
