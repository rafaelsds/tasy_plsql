create or replace 
view eis_consumo_matmed_v as
select	a.cd_centro_custo, 
	a.cd_material_estoque cd_material,
	a.dt_mesano_referencia,
	a.cd_estabelecimento,
	a.cd_local_estoque,
	decode(nvl(o.ie_consignado,0),0,'N','S') ie_consignado,
	decode(o.ie_consumo, 'A', a.qt_estoque, 'D', a.qt_estoque * -1, 0) qt_estoque,
	nvl(decode(o.ie_consumo, 'A', a.vl_estoque, 'D', a.vl_estoque * -1, 0),0) vl_estoque,
	g.cd_grupo_material,
	g.ds_grupo_material,
	s.cd_subgrupo_material,
	s.ds_subgrupo_material,
	c.cd_classe_material,
	c.ds_classe_material,
	substr(e.ds_material,1,100) ds_material,
	substr(b.ds_centro_custo,1,100) ds_centro_custo,
	substr(l.ds_local_estoque,1,100) ds_local_estoque,
	obter_custo_medio_material(a.cd_estabelecimento, a.dt_mesano_referencia, a.cd_material_estoque) vl_custo_medio,
	substr(obter_curva_abc_estab(a.cd_estabelecimento, a.cd_material_estoque, 'N',a.dt_mesano_referencia),1,1) ie_curva_abc,
	a.cd_operacao_estoque,
	substr(o.ds_operacao,1,100) ds_operacao_estoque,
	o.ie_tipo_requisicao,
	substr(obter_valor_dominio(21,o.ie_tipo_requisicao),1,100) ds_tipo_requisicao,
	a.cd_acao,
	decode(a.cd_acao,2,wheb_mensagem_pck.get_texto(301937),wheb_mensagem_pck.get_texto(301936)) ds_acao,
	a.dt_movimento_estoque,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) nm_estabelecimento,
	e.nr_seq_familia,
	substr(obter_desc_familia_mat(e.nr_seq_familia),1,255) ds_familia
from	operacao_estoque o,
	centro_custo b,
	local_estoque l,
	material e,
	grupo_material g,
	subgrupo_material s,
	classe_material c,
	movimento_estoque_v1 a
where	a.cd_material_estoque = e.cd_material
and	a.cd_operacao_estoque = o.cd_operacao_estoque
and	a.cd_centro_custo = b.cd_centro_custo(+)
and	a.cd_local_estoque = l.cd_local_estoque
and	o.ie_consumo in ('A','D')
and 	e.cd_classe_material = c.cd_classe_material
and	c.cd_subgrupo_material = s.cd_subgrupo_material
and	s.cd_grupo_material = g.cd_grupo_material
and	a.dt_processo is not null;
/