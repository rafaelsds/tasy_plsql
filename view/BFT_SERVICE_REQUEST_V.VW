create or replace view bft_service_request_v as
select	a.nr_atendimento encounter_id,
	a.dt_solicitacao request_date,
	a.dt_liberacao release_date,
	substr(obter_med_exame_externo(b.nr_sequencia),1,255) service_description,
	b.qt_exame service_quantity,
	b.ie_urgente service_urgency,
	nvl(b.ds_justificativa,a.ds_justificativa) justification,
	a.ds_dados_clinicos clinical_data,
	a.cd_doenca icd_code,
	a.ds_exame_ant previous_exam,
	a.ds_cid icd_description,
	a.ds_solicitacao request_desc
from	pedido_exame_externo a,
	pedido_exame_externo_item b
where	a.nr_sequencia			= b.nr_seq_pedido
and	a.dt_liberacao is not null
and	a.dt_inativacao is null;
/