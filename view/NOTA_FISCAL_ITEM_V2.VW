create or replace view nota_fiscal_item_v2
as
select /*+ index (i notfiit_pk) */
	i.nr_sequencia,
	i.nr_ordem_compra,
	i.cd_material,
	i.qt_item_nf,
	i.qt_item_estoque,
	i.vl_total_item_nf,
	i.vl_unitario_item_nf,
	i.vl_desconto,
	i.vl_desc_financ,
	i.vl_desconto_rateio,
	i.vl_liquido,
	(i.vl_seguro + i.vl_frete + i.vl_despesa_acessoria) vl_rateio,
	i.cd_unidade_medida_estoque,
	i.cd_unidade_medida_compra,
	i.cd_centro_custo,
	t.tx_tributo,
	t.vl_tributo,
	(nvl(i.vl_seguro,0) + nvl(i.vl_frete,0) + nvl(i.vl_despesa_acessoria,0) + 
	nvl(i.vl_total_item_nf,0) + nvl(t.vl_tributo,0)  - 
	nvl(i.vl_desconto,0) - nvl(i.vl_desconto_rateio,0)) vl_total_nota,
	i.cd_conta_contabil,
	substr(obter_desc_conta_contabil(i.cd_conta_contabil),1,255) ds_conta_contabil,
	i.cd_local_estoque,
	substr(obter_desc_local_estoque(i.cd_local_estoque),1,100) ds_local_estoque,
	i.ds_complemento,
	i.nr_item_oci,
	substr(obter_desc_marca(nr_seq_marca),1,255) ds_marca,
	dt_entrega_ordem dt_prevista_entrega
from	nota_fiscal_item i,
	nota_fiscal_item_trib t
where	i.nr_sequencia	= t.nr_sequencia(+)
and	i.nr_item_nf		= t.nr_item_nf(+)
and	t.cd_tributo(+)	= 6;
/