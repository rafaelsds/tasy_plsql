create or replace view
anatomia_patologica_susp_ws_v as
select  b.nr_atendimento,
	a.nr_prescricao, 
	a.nr_sequencia,
	a.nr_seq_interno,
	a.nr_acesso_dicom nr_acess_number, 
	a.nr_seq_proc_interno cd_proced_tasy,
	e.cd_integracao cd_proced_integracao,
	(a.nr_seq_proc_interno||a.ie_lado) cd_proced_tasy_lado,
	d.ds_proc_exame ds_procedimento,
	a.ie_lado,
	nvl(a.ie_urgencia,'N') ie_urgencia,
	a.dt_prev_execucao,
	b.cd_setor_atendimento cd_setor_paciente,
	b.cd_estabelecimento,
	c.nm_pessoa_fisica,
	b.dt_prescricao,
	nvl(b.dt_liberacao_medico, b.dt_liberacao) dt_liberacao,
	f.nr_Sequencia cd_software_integracao,
	obter_estab_integracao(b.cd_estabelecimento, f.nr_seq_empresa) cd_estab_terceiro,
	a.cd_Setor_atendimento	cd_setor_execucao,
	b.cd_pessoa_fisica cd_paciente,
	b.cd_prescritor,
	a.nr_seq_lab,
	a.cd_material_exame
from    exame_laboratorio k,	
	sistema_integracao f,
        proc_interno_integracao e,
        pessoa_fisica c,		        	
        proc_interno d,	        
        prescr_medica b,
        prescr_procedimento a	
where   a.nr_prescricao         = b.nr_prescricao
and     b.cd_pessoa_fisica      = c.cd_pessoa_fisica
and     d.nr_sequencia          = a.nr_seq_proc_interno
and     e.nr_seq_proc_interno   = a.nr_seq_proc_interno
and     e.nr_seq_proc_interno   = d.nr_sequencia
and	k.nr_seq_exame		= a.nr_seq_exame
and	k.ie_anatomia_patologica = 'S'
and	a.nr_seq_lab		is not null
and     a.cd_motivo_baixa       = 0
and     f.nr_sequencia          = e.nr_seq_sistema_integ
and     nvl(b.dt_liberacao_medico, b.dt_liberacao) is not null
and     ((e.cd_estabelecimento    is null) or (e.cd_estabelecimento = b.cd_estabelecimento))
and 	a.dt_integracao		  is not null
and 	a.DT_INTEGRACAO_SUSPENSO  is null
and 	a.dt_suspensao		  is not null;
/	