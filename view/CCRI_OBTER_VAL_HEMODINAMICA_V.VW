CREATE OR REPLACE VIEW CCRI_OBTER_VAL_HEMODINAMICA_V
AS
SELECT
        'CALCULADO' ds_info,
        x.cd_estabelecimento,
        x.nr_atendimento,
        x.nr_interno_conta,
        x.nr_cirurgia,
        x.dt_mes_referencia,
        x.vl_tx_custo,
        x.qt_minuto,
        ROUND((x.vl_tx_custo * ROUND(x.qt_minuto/60,4)),2) vl_custo_calc,
        (x.vl_custo) vl_custo,
        x.cd_procedimento,
        x.ie_origem_proced,
        x.ds_procedimento,
		x.dt_ref,
	x.cd_estab
FROM    (
        SELECT  ccri_obter_taxa_custo(a.dt_referencia,c.cd_centro_custo,c.cd_estabelecimento,'A') vl_tx_custo,
                NVL(b.nr_min_duracao_real * SIGN(a.qt_resumo),0) qt_minuto,
                a.nr_interno_conta,
                b.nr_atendimento,
                a.vl_custo,
                TRUNC(a.dt_referencia,'mm') dt_mes_referencia,
                c.cd_estabelecimento,
                b.nr_cirurgia,
                b.cd_procedimento_princ cd_procedimento,
                b.ie_origem_proced,
                obter_desc_procedimento(b.cd_procedimento_princ,b.ie_origem_proced) ds_procedimento,
				TRUNC(a.dt_referencia,'mm') dt_ref,
		b.cd_estabelecimento cd_estab
        FROM    conta_paciente d,
                setor_atendimento c,
                cirurgia b,
                conta_paciente_resumo a
        WHERE   1=1
        AND     a.nr_cirugia = b.nr_cirurgia(+)
        AND     a.cd_setor_atendimento = c.cd_setor_atendimento
        AND     a.nr_interno_conta = d.nr_interno_conta
        AND     (a.cd_procedimento,a.ie_origem_proced) IN
                (
                SELECT  w.cd_procedimento,
                        w.ie_origem_proced
                FROM    regra_aloc_custo_setor w
                WHERE   w.cd_setor_atendimento IN (313,413)
                AND     w.cd_proc_custo = '98190000'
                AND     w.ie_origem_proc_custo = '1'
                )
        AND     d.ie_cancelamento IS NULL
        ) x
UNION ALL
SELECT
        'NAO_CALCULADO' ds_info,
        x.cd_estabelecimento,
        x.nr_atendimento,
        x.nr_interno_conta,
        x.nr_cirurgia,
        x.dt_mes_referencia,
        x.vl_tx_custo,
        x.qt_minuto,
        (x.vl_tx_custo * (x.qt_minuto/60)) vl_custo_calc,
        x.vl_custo,
        x.cd_procedimento,
        x.ie_origem_proced,
        x.ds_procedimento,
		x.dt_ref,
	x.cd_estab
FROM    (
        SELECT  b.cd_estabelecimento,
                NVL(a.dt_inicio_real,a.dt_inicio_prevista) dt_mes_referencia,
                ccri_obter_taxa_custo(NVL(a.dt_inicio_real,a.dt_inicio_prevista),b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
                a.nr_min_duracao_real qt_minuto,
                a.nr_atendimento,
                NULL nr_interno_conta,
                0 vl_custo,
                a.nr_cirurgia,
                a.cd_procedimento_princ cd_procedimento,
                a.ie_origem_proced,
                obter_desc_procedimento(a.cd_procedimento_princ,a.ie_origem_proced) ds_procedimento,
				TRUNC(NVL(a.dt_inicio_real,a.dt_inicio_prevista),'mm') dt_ref,
		b.cd_estabelecimento cd_estab
        FROM    setor_atendimento b,
                cirurgia a
        WHERE   1=1
        AND     a.cd_setor_atendimento = b.cd_setor_atendimento(+)
        AND     a.nr_cirurgia NOT IN (
                        SELECT  bb.nr_cirurgia FROM cirurgia bb,
                                conta_paciente_resumo aa
                        WHERE   1=1
                        AND     aa.nr_cirugia = bb.nr_cirurgia(+)
                        AND     (aa.cd_procedimento,aa.ie_origem_proced) IN
                                (
                                SELECT  w.cd_procedimento,
                                        w.ie_origem_proced
                                FROM    regra_aloc_custo_setor w
                                WHERE   w.cd_setor_atendimento IN (313,413)
                                AND     w.cd_proc_custo = '98190000'
                                AND     w.ie_origem_proc_custo = '1'
                                )
                        AND     TRUNC(aa.dt_referencia,'mm') = TRUNC(NVL(a.dt_inicio_real,a.dt_inicio_prevista),'mm')
                        AND     bb.cd_estabelecimento = b.cd_estabelecimento
                )
        ) x;
/