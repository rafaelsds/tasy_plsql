CREATE OR REPLACE VIEW RESUMEN_CLINICO_MEDIC_2_V AS
SELECT  a.nr_atendimento NR_ATENDIMENTO,
        SUBSTR(x.ds_material,1,240) DS_MATERIAL,
        MIN(c.dt_horario) DT_ADM,
        MAX(c.dt_fim_horario) DT_FIM_ADM,
        SUM(NVL(DECODE(NVL(d.qt_dose_adm,0),0,c.qt_dose, d.qt_dose_adm),c.qt_dose)) QT_DOSE,
        NVL(OBTER_VIA_ANVISA(b.IE_VIA_APLICACAO),b.IE_VIA_APLICACAO) IE_VIA_APLICACAO,
		Obter_Desc_via(NVL(OBTER_VIA_ANVISA(b.IE_VIA_APLICACAO),b.IE_VIA_APLICACAO)) IE_DESC_VIA_APLICACAO,
        b.ds_observacao DS_OBSERVACAO,
		CM.CD_CAT_MEDICAMENTO CD_CAT_MEDICAMENTO,
		CM.DS_MEDICAMENTO DS_MEDICAMENTO,
		x.cd_material CD_MATERIAL
FROM        material x,
        prescr_mat_alteracao d,
        prescr_mat_hor c,
        prescr_material b,
        prescr_medica a,
		CAT_MEDICAMENTO CM
WHERE        d.nr_seq_horario(+) = c.nr_sequencia
AND        c.nr_seq_material(+) = b.nr_sequencia
AND        c.nr_prescricao = b.nr_prescricao
AND        b.nr_prescricao = a.nr_prescricao
AND        b.cd_material        = x.cd_material
AND C.DT_SUSPENSAO IS NULL
AND        NVL(c.ie_situacao,'A') = 'A'
AND        NVL(c.ie_adep,'S') = 'S'
AND        b.ie_agrupador = 1
AND        NVL(a.ie_adep,'S') = 'S'
AND                NVL(a.dt_liberacao, a.dt_liberacao_medico) IS NOT NULL
AND        Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
AND 	   cm.cd_medicamento (+) = x.cd_material
AND		   EXISTS (SELECT 1 FROM prescr_item_sintoma XX WHERE XX.NR_PRESCRICAO = B.NR_PRESCRICAO)
GROUP BY a.nr_atendimento, 
	  	 a.nr_prescricao,
		 x.ds_material,
		 b.IE_VIA_APLICACAO,
		 b.ds_observacao,
		CM.CD_CAT_MEDICAMENTO,
		CM.DS_MEDICAMENTO,
		X.CD_MATERIAL;
/