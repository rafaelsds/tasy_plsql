CREATE OR REPLACE VIEW PRESCR_PROC_PACS_NAO_LAB_V
AS
select  b.cd_pessoa_fisica,
	a.nr_seq_interno,
	a.NR_PRESCRICAO,
	a.NR_SEQUENCIA,
	a.CD_PROCEDIMENTO,
	p.ds_procedimento,
	p.cd_tipo_procedimento,
	a.QT_PROCEDIMENTO,
	a.DT_ATUALIZACAO,
	a.NM_USUARIO,
	a.DS_OBSERVACAO,
	a.IE_ORIGEM_PROCED,
	a.IE_URGENCIA,
	a.DS_DADO_CLINICO,
	a.IE_SUSPENSO,
	a.cd_setor_atendimento,
	b.NR_ATENDIMENTO,
 	b.CD_MEDICO,
 	b.DT_PRESCRICAO,
	b.DT_LIBERACAO,
	b.DT_LIBERACAO_MEDICO,
	b.IE_RECEM_NATO,
	g.cd_setor_atendimento cd_setor_paciente,
	DECODE(b.CD_RECEM_NATO,NULL,c.NM_PESSOA_FISICA,SUBSTR(obter_nome_pf(b.CD_RECEM_NATO),1,80) ||' RN de '||c.NM_PESSOA_FISICA) NM_PACIENTE,
	c.DT_NASCIMENTO,
	c.IE_SEXO,
	c.NR_CPF,
	c.NR_PRONTUARIO,
	d.NM_PESSOA_FISICA NM_MEDICO,
	e.NR_CRM,
	F.CD_CONVENIO,	
	F.CD_CATEGORIA,
	F.CD_USUARIO_CONVENIO,
	V.DS_CONVENIO,
	A.CD_MATERIAL_EXAME,
	A.DS_MATERIAL_ESPECIAL,
	nvl(a.ie_amostra,'N') ie_amostra_entregue,
	a.ds_horarios,
	z.ds_endereco,
	z.nr_endereco,
	z.ds_complemento,
	z.ds_bairro,
	z.ds_municipio,
	z.sg_estado,
	z.nr_telefone,
	z.cd_cep,
	a.dt_prev_execucao,
	s.ds_setor_atendimento ds_setor_paciente,
	g.cd_unidade_basica || ' ' || cd_unidade_compl cd_unidade,
	a.ie_executar_leito,
	obter_modalidade_wtt(p.cd_tipo_procedimento) ds_modalidade,
	a.nr_acesso_dicom na_accessionnumber,
	T.IE_TIPO_ATENDIMENTO,
	i.cd_integracao cd_exame,
	p.nr_seq_grupo_rec
FROM	setor_atendimento s,
	Procedimento p,
	CONVENIO V,
	COMPL_PESSOA_FISICA Z,
	ATEND_PACIENTE_UNIDADE G,
	ATEND_CATEGORIA_CONVENIO F,
	DUAL X,
	MEDICO E,
	ATENDIMENTO_PACIENTE T,
	PESSOA_FISICA D,
	PESSOA_FISICA C,
	PRESCR_MEDICA B,
	PRESCR_PROCEDIMENTO A,
	PROC_INTERNO I
WHERE a.CD_MOTIVO_BAIXA = 0
    and a.dt_integracao is null
    and a.cd_procedimento		= p.cd_procedimento
    and a.ie_origem_proced		= p.ie_origem_proced
    and nvl(dt_liberacao_medico, dt_liberacao) is not null
    AND A.NR_PRESCRICAO 		= B.NR_PRESCRICAO
    AND B.CD_PESSOA_FISICA 		= C.CD_PESSOA_FISICA
    AND B.CD_PESSOA_FISICA		= Z.CD_PESSOA_FISICA (+)
    AND 1					= Z.IE_TIPO_COMPLEMENTO (+)
    AND B.CD_MEDICO 			= D.CD_PESSOA_FISICA
    AND B.CD_MEDICO 			= E.CD_PESSOA_FISICA (+)
    AND G.NR_SEQ_INTERNO		= obter_atepacu_paciente(b.nr_atendimento,'A')
    AND G.CD_SETOR_ATENDIMENTO	= S.CD_SETOR_ATENDIMENTO
    AND A.NR_SEQ_PROC_INTERNO	= I.NR_SEQUENCIA(+)
    AND	a.nr_seq_exame is null
    AND A.CD_SETOR_ATENDIMENTO in
	(SELECT S.CD_SETOR_ATENDIMENTO
	FROM SETOR_ATENDIMENTO S
	WHERE S.NM_USUARIO_BANCO IN
		(SELECT USER FROM DUAL))
    AND F.NR_ATENDIMENTO  		= B.NR_ATENDIMENTO
    AND T.NR_ATENDIMENTO  		= B.NR_ATENDIMENTO
    AND F.DT_INICIO_VIGENCIA 		= 
      (SELECT MAX(W.DT_INICIO_VIGENCIA) 
       FROM ATEND_CATEGORIA_CONVENIO W
       WHERE W.NR_ATENDIMENTO  = B.NR_ATENDIMENTO)
    AND F.CD_CONVENIO           	= V.CD_CONVENIO; 
    
DROP PUBLIC SYNONYM PRESCR_PROC_PACS_NAO_LAB_V;

CREATE PUBLIC SYNONYM PRESCR_PROC_PACS_NAO_LAB_V FOR TASY.PRESCR_PROC_PACS_NAO_LAB_V;

GRANT SELECT ON  TASY.PRESCR_PROC_PACS_NAO_LAB_V TO PACS;