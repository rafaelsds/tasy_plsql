CREATE or REPLACE VIEW Agenda_Consulta_v
AS
select      
	a.*,
	nvl(obter_nome_pf(b.cd_pessoa_fisica),nm_paciente) nm_pessoa_fisica,
	Nvl(qt_idade_pac, substr(obter_idade(b.dt_nascimento, sysdate, 'A'),1,3)) 	qt_anos,
      b.nr_telefone_celular,
      nvl(a.nr_telefone, b.nr_telefone_celular) nr_telefone_contato,
	b.nr_prontuario,
	substr(c.ds_status_agenda,1,100) ds_status_agenda,
      substr(obter_desc_classif_agenda(a.cd_agenda, d.cd_classif_agenda),1,40) 	ds_classif_agenda,
	d.ds_classif_agenda ds_classif_agenda_tasy,
	e.ds_convenio,
      substr(g.ds_status_paciente,1,100) ds_status_paciente,
	f.nr_seq_agenda,
	f.dt_entrada,
	f.dt_saida,
	Obter_Tempo_Espera_Atend(a.nr_atendimento) qt_min_espera_tasy,
	to_number(decode(f.dt_entrada, null, null, 
			decode(a.ie_status_agenda, 'N', Null,	
			round((nvl(f.dt_inicio_consulta, sysdate) - f.dt_entrada) * 
			1440)))) qt_min_espera,
	to_number(decode(f.dt_inicio_consulta,null, null, 
		round((nvl(f.dt_saida,sysdate) - f.dt_inicio_consulta) * 1440))) 	qt_min_Consulta,
	substr(e.ds_convenio || '  ' || obter_med_plano(a.nr_seq_plano),1,100) ds_convenio_plano,
	obter_minutos_atraso_agenda(a.nr_sequencia) nr_minutos_atraso,
       substr(Obter_Med_Classif_Paciente(
              obter_codigo_sist_orig(a.cd_pessoa_fisica, b.cd_pessoa_fisica), b.cd_pessoa_fisica),1,100) ds_classif_pac,
	substr(obter_idade(b.dt_nascimento, sysdate, 'S'),1,50) ds_idade,
	b.cd_sistema_ant cd_sistema_ant,
	d.cd_classif_tasy,
	substr(obter_nome_agenda_cons(a.cd_agenda),1,80) nm_agenda,
	substr(obter_desc_indicacao(a.nr_seq_indicacao),1,40) ds_indicacao,
	substr(obter_nome_pf(a.cd_pessoa_indicacao),1,60) nm_pessoa_indic,
	substr(obter_nome_pf(a.cd_medico),1,60) nm_medico,
	substr(obter_nome_pf(a.cd_medico_solic),1,60) nm_medico_solic
from  Convenio e,
	Med_Atendimento f,
      status_paciente_v g,
	classif_agenda_consulta_v d,
	Status_agenda_v c,
	Pessoa_fisica b,
      agenda_Consulta a      
where  a.cd_pessoa_fisica   	= b.cd_pessoa_fisica (+)
   and a.ie_status_agenda   	= c.cd_status_agenda 
   and a.ie_classif_agenda	= d.cd_classif_agenda (+)
   and a.ie_status_paciente   = g.cd_status_paciente (+)
   and a.cd_convenio        	= e.cd_convenio (+)
   and a.nr_sequencia 		= f.nr_seq_agenda (+);
/