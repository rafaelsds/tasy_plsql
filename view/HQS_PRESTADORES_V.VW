create or replace
view hqs_prestadores_v as
select	a.nr_sequencia nr_seq_prestador,
	a.ie_situacao,
	0 ie_tipo_pessoa,
	substr(pls_obter_cod_prestador(a.nr_sequencia,null),1,255) cd_prestador,
	' ' nm_fantasia,
	a.cd_pessoa_fisica cd_pf_pj,
	initcap(substr(obter_nome_pf(a.cd_pessoa_fisica),1,255)) ds_razao_social,
	pls_obter_espec_prestador(a.nr_sequencia) ds_especialidade,
	(select	max(x.ds_tipo_prestador)
	from	pls_tipo_prestador x
	where	x.nr_sequencia = a.nr_seq_tipo_prestador)	ds_tipo_prestador,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'E'),1,255) ds_endereco,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'NE'),1,255) nr_endereco,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'B'),1,255) ds_bairro, 
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'C'),1,255) ds_cidade,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'ES'),1,255) ds_estado,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'T'),1,255) nr_telefone,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'COMP'),1,255) ds_complemento,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'CEP'),1,255) ds_cep,
	substr(Sus_Obter_Desc_TipoLog(b.cd_tipo_logradouro),1,40) ds_logradouro,
	d.cd_especialidade,
	obter_crm_medico(a.cd_pessoa_fisica) cd_crm,
	a.nr_seq_tipo_prestador nr_seq_tipo_prestador
from	pls_prestador_med_espec	d,
	compl_pessoa_fisica	b,
	pessoa_fisica		c,
	pls_prestador		a
where	c.cd_pessoa_fisica	= b.cd_pessoa_fisica(+)
and	c.cd_pessoa_fisica	= a.cd_pessoa_fisica
and	a.nr_sequencia		= d.nr_seq_prestador(+)
and	sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate)
and	a.cd_pessoa_fisica is not null
union
select	a.nr_sequencia nr_seq_prestador,
	a.ie_situacao,
	1 ie_tipo_pessoa,
	substr(pls_obter_cod_prestador(a.nr_sequencia,null),1,255) cd_prestador,
	initcap(substr(obter_dados_pf_pj(null,a.cd_cgc,'F'),1,255)) nm_fantasia,
	a.cd_cgc cd_pf_pj,
	initcap(substr(obter_dados_pf_pj(null,a.cd_cgc,'N'),1,255)) ds_razao_social,
	pls_obter_espec_prestador(a.nr_sequencia) ds_especialidade,
	(select	max(x.ds_tipo_prestador)
	from	pls_tipo_prestador x
	where	x.nr_sequencia = a.nr_seq_tipo_prestador)	ds_tipo_prestador,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'E'),1,255) ds_endereco,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'NE'),1,255) nr_endereco,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'B'),1,255) ds_bairro, 
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'C'),1,255) ds_cidade,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'ES'),1,255) ds_estado,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'T'),1,255) nr_telefone,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'COMP'),1,255) ds_complemento,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'CEP'),1,255) ds_cep,
	substr(obter_dados_pf_pj(null,a.cd_cgc,'LO'),1,40) ds_logradouro,
	d.cd_especialidade,
	null cd_crm,
	a.nr_seq_tipo_prestador nr_seq_tipo_prestador
from	pls_prestador_med_espec	d,
	pls_prestador		a
where	a.nr_sequencia		= d.nr_seq_prestador(+)
and	a.cd_cgc is not null
and	sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate)
union
select	a.nr_sequencia nr_seq_prestador,
	a.ie_situacao,
	2 ie_tipo_pessoa,
	substr(pls_obter_cod_prestador(a.nr_sequencia,null),1,255) cd_prestador,
	initcap(substr(obter_dados_pf_pj(null,a.cd_cgc,'F'),1,255)) nm_fantasia,
	nvl(a.cd_cgc,a.cd_pessoa_fisica) cd_pf_pj,
	nvl(initcap(substr(obter_nome_pf(a.cd_pessoa_fisica),1,255)),
	initcap(substr(obter_dados_pf_pj(null,a.cd_cgc,'N'),1,255))) ds_razao_social,
	pls_obter_espec_prestador(a.nr_sequencia) ds_especialidade,
	(select	max(x.ds_tipo_prestador)
	from	pls_tipo_prestador x
	where	x.nr_sequencia = a.nr_seq_tipo_prestador)	ds_tipo_prestador,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'E'),1,255) ds_endereco,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'NE'),1,255) nr_endereco,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'B'),1,255) ds_bairro, 
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'C'),1,255) ds_cidade,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'ES'),1,255) ds_estado,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'T'),1,255) nr_telefone,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'COMP'),1,255) ds_complemento,
	substr(pls_obter_dados_prest_end(a.nr_sequencia,null,null,'CEP'),1,255) ds_cep,
	nvl(substr(Sus_Obter_Desc_TipoLog(b.cd_tipo_logradouro),1,40),substr(obter_dados_pf_pj(null,a.cd_cgc,'LO'),1,40)) ds_logradouro,
	d.cd_especialidade,
	decode(a.cd_cgc,null,obter_crm_medico(a.cd_pessoa_fisica),null) cd_crm,
	a.nr_seq_tipo_prestador nr_seq_tipo_prestador
from	pls_prestador_med_espec	d,
	compl_pessoa_fisica	b,
	pessoa_fisica		c,
	pls_prestador		a
where	c.cd_pessoa_fisica	= b.cd_pessoa_fisica(+)
and	c.cd_pessoa_fisica	= a.cd_pessoa_fisica
and	a.nr_sequencia		= d.nr_seq_prestador(+)
and	sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate)
order by
	ds_razao_social;
/