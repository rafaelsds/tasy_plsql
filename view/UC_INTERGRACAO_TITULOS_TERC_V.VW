create or replace
view uc_intergracao_titulos_terc_v as
/*	Header do Arquivo	*/
select	a.cd_estabelecimento,
	1 tp_registro,
	'01' ie_tipo_registro,
	a.cd_cgc,
	to_char(sysdate,'ddmmyyyy') dt_geracao,
	to_char(null) cd_funcionario,
	0 vl_comprado,
	0 vl_total,
	to_date(null) dt_emissao,
	lpad('0',27,'0') ds_zeros,
	0 nr_titulo
from	estabelecimento a
union all
/*	Detalhe		*/
select	a.cd_estabelecimento,
	2 tp_registro,
	'02' ie_tipo_registro,
	to_char(null) cd_cgc,
	to_char(null) dt_geracao,
	b.cd_funcionario,
	a.vl_saldo_titulo vl_comprado,
	0 vl_total,
	a.dt_emissao,
	'0' ds_zeros,
	a.nr_titulo
from	pessoa_fisica b,
	titulo_receber a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.ie_funcionario	= 'S'
and	a.nr_seq_terc_conta is not null
union all
/*	Trailler	*/
select	a.cd_estabelecimento,
	9 tp_registro,
	'09' ie_tipo_registro,
	to_char(null) cd_cgc,
	to_char(null) dt_geracao,
	to_char(null) cd_funcionario,
	0 vl_comprado,
	sum(a.vl_saldo_titulo) vl_total,
	to_date(null) dt_emissao,
	lpad('0',37,'0') ds_zeros,
	0 nr_titulo
from	pessoa_fisica b,
	titulo_receber a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.ie_funcionario	= 'S'
and	a.nr_seq_terc_conta is not null
group by
	a.cd_estabelecimento
/
