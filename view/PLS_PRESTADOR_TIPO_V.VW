create or replace
view pls_prestador_tipo_v as
select nr_seq_prestador, nr_seq_tipo,
       dt_inicio_vigencia_ref, dt_fim_vigencia_ref
from   pls_prestador_tipo
union all
select nr_sequencia nr_seq_prestador, nr_seq_tipo_prestador nr_seq_tipo,
       pls_util_pck.obter_dt_vigencia_default('I') dt_inicio_vigencia_ref,
       pls_util_pck.obter_dt_vigencia_default('F') dt_fim_vigencia_ref
from   pls_prestador;
/