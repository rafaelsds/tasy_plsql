create or replace view early_warning_v as
select   c.nr_atendimento,
         d.fld_nm_second_col_content,
         d.fld_nm_second_col_sub_info,
         d.fld_nm_chart_first_val || ' - ' || d.fld_nm_chart_seccond_val fld_nm_chart_tooltip,
         d.fld_nm_chart_first_val,
         d.fld_nm_chart_seccond_val,
         d.fld_nm_third_col_content,
         d.fld_nm_first_col_content,
		 d.fld_nm_sub_content,
		 d.nr_delta_view
from     (select b.nr_atendimento from early_warning b group by b.nr_atendimento) c,
         (
         select   a.nr_sequencia,
                  a.nr_atendimento,
                  null fld_nm_first_col_content,
				  substr(obter_desc_expressao_idioma(1029288,null,wheb_usuario_pck.get_nr_seq_idioma),1,255) fld_nm_second_col_content,
                  suep_algorithms_severidade_ews(a.ie_severidade,2) fld_nm_second_col_sub_info,
                  obter_pontuacao_telehealth(a.nr_atendimento, 'early_warning', 'nr_media', 'dt_pontuacao', 2) fld_nm_chart_first_val,
                  obter_pontuacao_telehealth(a.nr_atendimento, 'early_warning', 'nr_media', 'dt_pontuacao', 1) fld_nm_chart_seccond_val,
                  nvl(to_char(obter_pontuacao_telehealth(a.nr_atendimento, 'early_warning', 'nr_pontuacao', 'dt_pontuacao', 1)), '--') fld_nm_third_col_content,
                  a.dt_pontuacao,
				  suep_algorithms_severidade_ews(a.ie_severidade,1) fld_nm_sub_content,
				  get_recent_delta_telehealth(a.nr_atendimento, 'early_warning', 'nr_delta') nr_delta_view
         from     early_warning a
         ) d
where    c.nr_atendimento = d.nr_atendimento and
         d.dt_pontuacao   = (select   max(b.dt_pontuacao)
                             from     early_warning b
                             where    b.nr_atendimento = c.nr_atendimento) and
         d.nr_sequencia   = (select   max(b.nr_sequencia)
                             from     early_warning b
                             where    b.dt_pontuacao   = d.dt_pontuacao
							 and      b.nr_atendimento = d.nr_atendimento);
/
