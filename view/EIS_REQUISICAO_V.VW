create or replace
view eis_requisicao_v as
select	b.cd_estabelecimento,
	b.nr_requisicao,
	b.dt_solicitacao_requisicao,
	b.cd_pessoa_requisitante,
	substr(obter_nome_pf_pj(b.cd_pessoa_requisitante, null),1,80) nm_pessoa_requisitante,
	b.cd_operacao_estoque,
	substr(obter_desc_operacao_estoque(b.cd_operacao_estoque),1,80) ds_operacao_estoque,
	b.cd_local_estoque,
	substr(obter_desc_local_estoque(b.cd_local_estoque),1,80) ds_local_estoque,
	b.cd_centro_custo,
	substr(obter_desc_centro_custo(b.cd_centro_custo),1,80) ds_centro_custo,
	b.cd_local_estoque_destino,
	substr(obter_desc_local_estoque(b.cd_local_estoque_destino),1,80) ds_local_estoque_destino,
	b.cd_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,80) ds_setor_atendimento,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,80) ds_material,
	a.cd_conta_contabil,
	substr(obter_desc_conta_contabil(a.cd_conta_contabil),1,80) ds_conta_contabil,
	a.cd_motivo_baixa,
	substr(obter_desc_motivo_baixa_req(a.cd_motivo_baixa),1,80) ds_motivo_baixa,
	a.qt_material_requisitada,
	(a.qt_estoque * obter_custo_medio_material(b.cd_estabelecimento, trunc(b.dt_solicitacao_requisicao,'mm'), a.cd_material)) vl_material,
	c.cd_grupo_material,
	c.ds_grupo_material,
	c.cd_subgrupo_material,
	c.ds_subgrupo_material,
	c.cd_classe_material,
	c.ds_classe_material,
	trunc(b.dt_liberacao,'dd') dt_liberacao,
	trunc(b.dt_baixa,'dd') dt_baixa,
	trunc(b.dt_recebimento,'dd') dt_recebimento,
	substr(obter_nome_pf_pj(a.cd_pessoa_atende,null),1,80) nm_usuario_atendente,
	substr(obter_nome_usuario(b.nm_usuario_recebedor),1,255) nm_usuario_recebedor,
	b.cd_setor_entrega,
	obter_nome_setor(b.cd_setor_entrega) ds_setor_entrega
from	estrutura_material_v c,
	item_requisicao_material a,
	requisicao_material b
where	a.nr_requisicao = b.nr_requisicao
and	a.cd_material = c.cd_material
and	b.dt_liberacao is not null
order by b.nr_requisicao;
/