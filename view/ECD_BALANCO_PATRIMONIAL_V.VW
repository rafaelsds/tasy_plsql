create or replace view ecd_balanco_patrimonial_v
as
select	1								tp_registro,
	'|J100'								ds_identificador,
	c.dt_referencia							dt_referencia,
	a.cd_estabelecimento						cd_estabelecimento,
	a.cd_conta_contabil							cd_aglutinacao,
	ctb_obter_nivel_conta(a.cd_conta_contabil)				ie_nivel_aglutinacao,
	decode(d.ie_tipo,'A',1,'P',2) 					cd_grupo,
	b.ds_conta_contabil						ds_aglutinacao,
	nvl(sum(a.vl_saldo), 0)				 		vl_conta,
	substr(ctb_obter_situacao_saldo(a.cd_conta_contabil,  sum(a.vl_saldo)),1,1)||'|'	ie_debito_credito,
	--b.cd_classificacao							cd_classificacao
	ctb_obter_classif_conta(a.cd_conta_contabil, b.cd_classificacao, c.dt_referencia) cd_classificacao,
	b.cd_empresa
from	ctb_grupo_conta d, 
	ctb_mes_ref c,
	conta_contabil b,
	ctb_saldo a
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	a.nr_seq_mes_ref		= c.nr_sequencia
and	b.cd_empresa			= c.cd_empresa
and	b.cd_grupo				= d.cd_grupo
and	d.ie_tipo			in ('A','P')
group by	c.dt_referencia, 
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	b.cd_classificacao,
	d.ie_tipo,
	b.ds_conta_contabil,
	b.cd_empresa
order by	ie_nivel_aglutinacao;
/