CREATE OR REPLACE 
VIEW Pixeon_Laudo_integracao_v3_v
AS
select	a.nr_sequencia,
	a.dt_integracao,
	a.dt_laudo,
	a.ds_laudo,
	b.nr_acesso_dicom
from	prescr_medica d,
	prescr_procedimento b,
	laudo_paciente a,
	parametro_medico c
where 	d.nr_prescricao = b.nr_prescricao
and	a.nr_prescricao	= b.nr_prescricao
and d.cd_estabelecimento = c.cd_estabelecimento(+)
and	a.nr_seq_prescricao	= b.nr_sequencia
and	b.ie_status_execucao >= nvl(c.ie_status_exec_integr,ie_status_execucao) 
and	a.dt_liberacao is not null
and	a.dt_integracao is null;

--DROP PUBLIC SYNONYM Pixeon_Laudo_integracao_v3_v;
--CREATE PUBLIC SYNONYM Pixeon_Laudo_integracao_v3_v FOR TASY.Pixeon_Laudo_integracao_v3_v;
--GRANT SELECT ON Pixeon_Laudo_integracao_v3_v TO Pixeon;