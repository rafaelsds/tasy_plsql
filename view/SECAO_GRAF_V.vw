  CREATE OR REPLACE VIEW SECAO_GRAF_V (SECTION_ID, LINKED_DATA, SECTION_TITLE, NR_SEQ_APRESENTACAO, NM_USUARIO, NR_SEQ_PEPO, NR_CIRURGIA, NR_SEQ_MODELO, IE_MODO_VISUALIZACAO, DS_CAMINHO_IMAGEM) AS 
  SELECT 
             c.nr_sequencia section_id, 
             c.nr_seq_linked_data linked_data, 
             c.ds_nome section_title, 
             c.nr_seq_apresentacao ,
             b.nm_usuario,
             b.nr_seq_pepo,
             b.nr_cirurgia,
             b.nr_seq_modelo,
			 c.ie_modo_visualizacao,
			 c.ds_caminho_imagem
             FROM    pepo_modelo a, 
             w_flowsheet_cirurgia_pac b, 
             w_flowsheet_cirurgia_grupo c 
             WHERE a.nr_sequencia    = b.nr_seq_modelo 
             AND b.nr_sequencia      = c.nr_seq_flowsheet
			 AND nvl(c.ie_modo_visualizacao, 'VI') in ('VI', 'IN')
             ORDER BY c.nr_seq_apresentacao, c.ds_nome;
/
