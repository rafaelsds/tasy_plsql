create or replace view pls_pessoa_fisica_compl_v
as
select	/*+ INDEX (B COMPEFI_I)*/
	a.cd_atividade_sus,
	a.cd_barras_pessoa,
	a.cd_cargo,
	a.cd_cbo_sus,
	a.cd_cgc_orig_transpl,
	a.cd_cid_direta,
	a.cd_cnes,
	a.cd_curp,
	a.cd_declaracao_nasc_vivo,
	a.cd_empresa,
	a.cd_estabelecimento,
	a.cd_familia,
	a.cd_funcionario,
	a.cd_ife,
	a.cd_medico,
	a.cd_municipio_ibge,
	a.cd_nacionalidade,
	a.cd_nit,
	a.cd_perfil_ativo,
	a.cd_pessoa_cross,
	a.cd_pessoa_fisica,
	a.cd_pessoa_mae,
	a.cd_puericultura,
	a.cd_religiao,
	a.cd_rfc,
	a.cd_sistema_ant,
	a.cd_tipo_pj,
	a.cd_ult_profissao,
	a.ds_apelido,
	a.ds_categoria_cnh,
	a.ds_codigo_prof,
	a.ds_email_ccih,
	a.ds_empresa_pf,
	a.ds_fonetica,
	a.ds_fonetica_cns,
	a.ds_historico,
	a.ds_laudo_anat_patol,
	a.ds_municipio_nasc_estrangeiro,
	a.ds_observacao,
	a.ds_orgao_emissor_ci,
	a.ds_orientacao_cobranca,
	a.ds_profissao,
	a.dt_admissao_hosp,
	a.dt_adocao,
	a.dt_afastamento,
	a.dt_alta_institucional,
	a.dt_atualizacao,
	a.dt_atualizacao_nrec,
	a.dt_cadastro_original,
	a.dt_cad_sistema_ant,
	a.dt_chegada_brasil,
	a.dt_demissao_hosp,
	a.dt_emissao_cert_casamento,
	a.dt_emissao_cert_divorcio,
	a.dt_emissao_cert_nasc,
	a.dt_emissao_ci,
	a.dt_emissao_ctps,
	a.dt_fim_experiencia,
	a.dt_fim_prorrogacao,
	a.dt_geracao_pront,
	a.dt_inicio_ocup_atual,
	a.dt_integracao_externa,
	a.dt_laudo_anat_patol,
	a.dt_nascimento,
	a.dt_nascimento_ig,
	a.dt_naturalizacao_pf,
	a.dt_obito,
	a.dt_primeira_admissao,
	a.dt_revisao,
	a.dt_transplante,
	a.dt_validade_conselho,
	a.dt_validade_coren,
	a.dt_validade_rg,
	a.dt_vencimento_cnh,
	a.ie_conselheiro,
	a.ie_considera_indio,
	a.ie_consiste_nr_serie_nf,
	a.ie_coren,
	a.ie_dependencia_sus,
	a.ie_dependente,
	a.ie_doador,
	a.ie_emancipado,
	a.ie_endereco_correspondencia,
	a.ie_escolaridade_cns,
	a.ie_estado_civil,
	a.ie_fator_rh,
	a.ie_fluencia_portugues,
	a.ie_fornecedor,
	a.ie_frequenta_escola,
	a.ie_fumante,
	a.ie_funcionario,
	a.ie_gemelar,
	a.ie_grau_instrucao,
	a.ie_nasc_estimado,
	a.ie_nf_correio,
	a.ie_ocupacao_habitual,
	a.ie_perm_sms_email,
	a.ie_regra_ig,
	a.ie_revisar,
	a.ie_rh_fraco,
	a.ie_sexo,
	a.ie_situacao_conj_cns,
	a.ie_socio,
	a.ie_status_exportar,
	a.ie_status_usuario_event,
	a.ie_subtipo_sanguineo,
	a.ie_tipo_definitivo_provisorio,
	a.ie_tipo_pessoa,
	a.ie_tipo_prontuario,
	a.ie_tipo_sangue,
	a.ie_tratamento_psiquiatrico,
	a.ie_unid_med_peso,
	a.ie_vegetariano,
	a.ie_vinculo_profissional,
	a.ie_vinculo_sus,
	a.nm_abreviado,
	a.nm_pessoa_fisica,
	a.nm_pessoa_fisica_sem_acento,
	a.nm_pessoa_pesquisa,
	a.nm_primeiro_nome,
	a.nm_sobrenome_mae,
	a.nm_sobrenome_pai,
	a.nm_social,
	a.nm_usuario,
	a.nm_usuario_nrec,
	a.nm_usuario_original,
	a.nm_usuario_princ_ci,
	a.nm_usuario_revisao,
	a.nr_cartao_estrangeiro,
	a.nr_cartao_nac_sus,
	a.nr_ccm,
	a.nr_celular_numeros,
	a.nr_cep_cidade_nasc,
	a.nr_cert_casamento,
	a.nr_cert_divorcio,
	a.nr_certidao_obito,
	a.nr_cert_militar,
	a.nr_cert_nasc,
	a.nr_cnh,
	a.nr_codigo_serv_prest,
	a.nr_contra_ref_sus,
	a.nr_cpf,
	a.nr_ctps,
	a.nr_ddd_celular,
	a.nr_ddi_celular,
	a.nr_folha_cert_casamento,
	a.nr_folha_cert_div,
	a.nr_folha_cert_nasc,
	a.nr_identidade,
	a.nr_inscricao_estadual,
	a.nr_inscricao_municipal,
	a.nr_inss,
	a.nr_iss,
	a.nr_livro_cert_casamento,
	a.nr_livro_cert_divorcio,
	a.nr_livro_cert_nasc,
	a.nr_matricula_nasc,
	a.nr_pager_bip,
	a.nr_passaporte,
	a.nr_pis_pasep,
	a.nr_portaria_nat,
	a.nr_pront_dv,
	a.nr_pront_ext,
	a.nr_prontuario,
	a.nr_reg_geral_estrang,
	a.nr_registro_pls,
	a.nr_rga,
	a.nr_ric,
	a.nr_same,
	a.nr_secao,
	a.nr_seq_agencia_inss,
	a.nr_seq_cartorio_casamento,
	a.nr_seq_cartorio_divorcio,
	a.nr_seq_cartorio_nasc,
	a.nr_seq_cbo_saude,
	a.nr_seq_chefia,
	a.nr_seq_classif_pac_age,
	a.nr_seq_conselho,
	a.nr_seq_cor_cabelo,
	a.nr_seq_cor_olho,
	a.nr_seq_cor_pele,
	a.nr_seq_etnia,
	a.nr_seq_funcao_pf,
	a.nr_seq_lingua_indigena,
	a.nr_seq_nome_solteiro,
	a.nr_seq_nut_perfil,
	a.nr_seq_pais,
	a.nr_seq_perfil,
	a.nr_seq_person_name,
	a.nr_seq_tipo_beneficio,
	a.nr_seq_tipo_incapacidade,
	a.nr_seq_turno_trabalho,
	a.nr_serie_ctps,
	a.nr_spss,
	a.nr_telefone_celular,
	a.nr_termo_cert_nasc,
	a.nr_titulo_eleitor,
	a.nr_transacao_sus,
	a.nr_transplante,
	a.nr_zona,
	a.qt_altura_cm,
	a.qt_dependente,
	a.qt_dias_ig,
	a.qt_peso,
	a.qt_peso_nasc,
	a.qt_peso_um,
	a.qt_semanas_ig,
	a.sg_emissora_ci,
	a.sg_estado_nasc,
	a.uf_conselho,
	a.uf_emissora_ctps,
	b.nr_ddi_telefone,
	b.nr_ddd_telefone,
	b.nr_telefone,
	b.cd_cep,
	b.ds_endereco,
	b.nr_endereco,
	b.ds_complemento,
	b.ds_bairro,
	b.ds_municipio,
	b.cd_municipio_ibge cd_mun_ibge,
	b.sg_estado,
	substr(obter_compl_pf(a.cd_pessoa_fisica,5,'N'),1,200) nm_mae
from	pessoa_fisica a,
	compl_pessoa_fisica b
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.ie_tipo_complemento	= 1
UNION
select		a.cd_atividade_sus,
	a.cd_barras_pessoa,
	a.cd_cargo,
	a.cd_cbo_sus,
	a.cd_cgc_orig_transpl,
	a.cd_cid_direta,
	a.cd_cnes,
	a.cd_curp,
	a.cd_declaracao_nasc_vivo,
	a.cd_empresa,
	a.cd_estabelecimento,
	a.cd_familia,
	a.cd_funcionario,
	a.cd_ife,
	a.cd_medico,
	a.cd_municipio_ibge,
	a.cd_nacionalidade,
	a.cd_nit,
	a.cd_perfil_ativo,
	a.cd_pessoa_cross,
	a.cd_pessoa_fisica,
	a.cd_pessoa_mae,
	a.cd_puericultura,
	a.cd_religiao,
	a.cd_rfc,
	a.cd_sistema_ant,
	a.cd_tipo_pj,
	a.cd_ult_profissao,
	a.ds_apelido,
	a.ds_categoria_cnh,
	a.ds_codigo_prof,
	a.ds_email_ccih,
	a.ds_empresa_pf,
	a.ds_fonetica,
	a.ds_fonetica_cns,
	a.ds_historico,
	a.ds_laudo_anat_patol,
	a.ds_municipio_nasc_estrangeiro,
	a.ds_observacao,
	a.ds_orgao_emissor_ci,
	a.ds_orientacao_cobranca,
	a.ds_profissao,
	a.dt_admissao_hosp,
	a.dt_adocao,
	a.dt_afastamento,
	a.dt_alta_institucional,
	a.dt_atualizacao,
	a.dt_atualizacao_nrec,
	a.dt_cadastro_original,
	a.dt_cad_sistema_ant,
	a.dt_chegada_brasil,
	a.dt_demissao_hosp,
	a.dt_emissao_cert_casamento,
	a.dt_emissao_cert_divorcio,
	a.dt_emissao_cert_nasc,
	a.dt_emissao_ci,
	a.dt_emissao_ctps,
	a.dt_fim_experiencia,
	a.dt_fim_prorrogacao,
	a.dt_geracao_pront,
	a.dt_inicio_ocup_atual,
	a.dt_integracao_externa,
	a.dt_laudo_anat_patol,
	a.dt_nascimento,
	a.dt_nascimento_ig,
	a.dt_naturalizacao_pf,
	a.dt_obito,
	a.dt_primeira_admissao,
	a.dt_revisao,
	a.dt_transplante,
	a.dt_validade_conselho,
	a.dt_validade_coren,
	a.dt_validade_rg,
	a.dt_vencimento_cnh,
	a.ie_conselheiro,
	a.ie_considera_indio,
	a.ie_consiste_nr_serie_nf,
	a.ie_coren,
	a.ie_dependencia_sus,
	a.ie_dependente,
	a.ie_doador,
	a.ie_emancipado,
	a.ie_endereco_correspondencia,
	a.ie_escolaridade_cns,
	a.ie_estado_civil,
	a.ie_fator_rh,
	a.ie_fluencia_portugues,
	a.ie_fornecedor,
	a.ie_frequenta_escola,
	a.ie_fumante,
	a.ie_funcionario,
	a.ie_gemelar,
	a.ie_grau_instrucao,
	a.ie_nasc_estimado,
	a.ie_nf_correio,
	a.ie_ocupacao_habitual,
	a.ie_perm_sms_email,
	a.ie_regra_ig,
	a.ie_revisar,
	a.ie_rh_fraco,
	a.ie_sexo,
	a.ie_situacao_conj_cns,
	a.ie_socio,
	a.ie_status_exportar,
	a.ie_status_usuario_event,
	a.ie_subtipo_sanguineo,
	a.ie_tipo_definitivo_provisorio,
	a.ie_tipo_pessoa,
	a.ie_tipo_prontuario,
	a.ie_tipo_sangue,
	a.ie_tratamento_psiquiatrico,
	a.ie_unid_med_peso,
	a.ie_vegetariano,
	a.ie_vinculo_profissional,
	a.ie_vinculo_sus,
	a.nm_abreviado,
	a.nm_pessoa_fisica,
	a.nm_pessoa_fisica_sem_acento,
	a.nm_pessoa_pesquisa,
	a.nm_primeiro_nome,
	a.nm_sobrenome_mae,
	a.nm_sobrenome_pai,
	a.nm_social,
	a.nm_usuario,
	a.nm_usuario_nrec,
	a.nm_usuario_original,
	a.nm_usuario_princ_ci,
	a.nm_usuario_revisao,
	a.nr_cartao_estrangeiro,
	a.nr_cartao_nac_sus,
	a.nr_ccm,
	a.nr_celular_numeros,
	a.nr_cep_cidade_nasc,
	a.nr_cert_casamento,
	a.nr_cert_divorcio,
	a.nr_certidao_obito,
	a.nr_cert_militar,
	a.nr_cert_nasc,
	a.nr_cnh,
	a.nr_codigo_serv_prest,
	a.nr_contra_ref_sus,
	a.nr_cpf,
	a.nr_ctps,
	a.nr_ddd_celular,
	a.nr_ddi_celular,
	a.nr_folha_cert_casamento,
	a.nr_folha_cert_div,
	a.nr_folha_cert_nasc,
	a.nr_identidade,
	a.nr_inscricao_estadual,
	a.nr_inscricao_municipal,
	a.nr_inss,
	a.nr_iss,
	a.nr_livro_cert_casamento,
	a.nr_livro_cert_divorcio,
	a.nr_livro_cert_nasc,
	a.nr_matricula_nasc,
	a.nr_pager_bip,
	a.nr_passaporte,
	a.nr_pis_pasep,
	a.nr_portaria_nat,
	a.nr_pront_dv,
	a.nr_pront_ext,
	a.nr_prontuario,
	a.nr_reg_geral_estrang,
	a.nr_registro_pls,
	a.nr_rga,
	a.nr_ric,
	a.nr_same,
	a.nr_secao,
	a.nr_seq_agencia_inss,
	a.nr_seq_cartorio_casamento,
	a.nr_seq_cartorio_divorcio,
	a.nr_seq_cartorio_nasc,
	a.nr_seq_cbo_saude,
	a.nr_seq_chefia,
	a.nr_seq_classif_pac_age,
	a.nr_seq_conselho,
	a.nr_seq_cor_cabelo,
	a.nr_seq_cor_olho,
	a.nr_seq_cor_pele,
	a.nr_seq_etnia,
	a.nr_seq_funcao_pf,
	a.nr_seq_lingua_indigena,
	a.nr_seq_nome_solteiro,
	a.nr_seq_nut_perfil,
	a.nr_seq_pais,
	a.nr_seq_perfil,
	a.nr_seq_person_name,
	a.nr_seq_tipo_beneficio,
	a.nr_seq_tipo_incapacidade,
	a.nr_seq_turno_trabalho,
	a.nr_serie_ctps,
	a.nr_spss,
	a.nr_telefone_celular,
	a.nr_termo_cert_nasc,
	a.nr_titulo_eleitor,
	a.nr_transacao_sus,
	a.nr_transplante,
	a.nr_zona,
	a.qt_altura_cm,
	a.qt_dependente,
	a.qt_dias_ig,
	a.qt_peso,
	a.qt_peso_nasc,
	a.qt_peso_um,
	a.qt_semanas_ig,
	a.sg_emissora_ci,
	a.sg_estado_nasc,
	a.uf_conselho,
	a.uf_emissora_ctps,
	null nr_ddi_telefone,
	null nr_ddd_telefone,
	null nr_telefone,
	null cd_cep,
	null ds_endereco,
	null nr_endereco,
	null ds_complemento,
	null ds_bairro,
	null ds_municipio,
	null cd_mun_ibge,
	null sg_estado,
	substr(obter_compl_pf(a.cd_pessoa_fisica,5,'N'),1,200) nm_mae
from	pessoa_fisica a
where	not exists (	select	1
			from	compl_pessoa_fisica x
			where	a.cd_pessoa_fisica	= x.cd_pessoa_fisica
			and	x.ie_tipo_complemento	= 1);
/