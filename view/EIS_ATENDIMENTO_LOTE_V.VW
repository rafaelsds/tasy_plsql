create or replace
view eis_atendimento_lote_v as
select	a.dt_referencia,
	a.cd_estabelecimento,
	a.ie_periodo,
	a.cd_local_estoque,
        substr(obter_desc_local_estoque(a.cd_local_estoque),1,60) ds_local_estoque,
	a.cd_setor_atendimento,
	substr(obter_nome_setor(a.cd_setor_atendimento),1,120) ds_setor_atendimento,
	a.nr_seq_turno,
	substr(obter_desc_turno_disp(a.nr_seq_turno),1,255) ds_turno,
	a.nr_seq_classif,
	substr(obter_desc_classif_lote_disp(a.nr_seq_classif),1,80) ds_classificacao,
	nm_usuario_atend,
	qt_atraso_entrega,
	qt_atraso_disp,
	qt_atraso_receb,
	qt_min_atraso_entrega,
	qt_min_atraso_disp,
	qt_min_atraso_receb
from	eis_atend_lote a;
/
