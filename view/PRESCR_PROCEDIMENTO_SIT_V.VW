create or replace view prescr_procedimento_sit_v
as
select	/*+ ORDEDED */
	p.nr_atendimento,
	p.nr_prescricao,
	a.nr_sequencia_prescricao,
	a.cd_setor_atendimento,	
	p.dt_prescricao	dt_item,
	a.dt_conta,
	a.dt_entrada_unidade,
	a.cd_convenio,
	a.cd_categoria,
	a.qt_procedimento	qt_executada,
	a.qt_devolvida	qt_devolvida,
	(a.qt_procedimento - a.qt_devolvida) qt_saldo,
	a.vl_procedimento	vl_item,
	a.nm_usuario,
	i.cd_procedimento	cd_item,
	i.qt_procedimento	qt_prescrita,
	i.cd_motivo_baixa,
	i.dt_baixa,
	m.ds_procedimento	ds_item,
	m.ie_classificacao	tp_item		/* 1-AMB 2-SERVICOS 3-DIARIAS	*/, 
	decode(m.ie_classificacao,1,'PROCEDIMENTOS',2,'SERVICOS','DIARIAS') ds_tp_item,
	p.nm_usuario_original,
	substr(nvl(obter_nome_pf(p.cd_prescritor),'N�o informado'),1,60) nm_prescritor,
	substr(nvl(obter_nome_setor(p.cd_setor_atendimento),'N�o informado'),1,100) ds_setor_paciente,
	(	select	count(b.dt_fim_horario)
		from	prescr_proc_hor b
		where	b.dt_fim_horario	is not null
		and	i.nr_prescricao		= b.nr_prescricao
		and	i.nr_sequencia		= b.nr_seq_procedimento
		and	m.cd_procedimento	= b.cd_procedimento
	) qt_checados
from	prescr_medica p,
	prescr_procedimento i,
	procedimento_paciente a,
	procedimento m
where	p.nr_prescricao		= i.nr_prescricao
and	i.ie_origem_proced	= m.ie_origem_proced
and	i.cd_procedimento	= m.cd_procedimento
and	i.nr_prescricao		= a.nr_prescricao(+)
and	i.nr_sequencia		= a.nr_sequencia_prescricao(+);
/