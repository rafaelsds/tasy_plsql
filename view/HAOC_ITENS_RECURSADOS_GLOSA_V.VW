CREATE OR REPLACE VIEW HAOC_ITENS_RECURSADOS_GLOSA_V
AS SELECT
	obter_convenio_conta(a.nr_interno_conta) ds_convenio,
	a.nr_interno_conta nr_conta,
	to_number ( obter_atendimento_conta ( a.nr_interno_conta ) ) nr_atendimento,
	obter_pessoa_conta ( a.nr_interno_conta, 'D') nm_paciente,
	to_number ( obter_titulo_lote_guia ( a.nr_interno_conta, a.cd_autorizacao, a.nr_seq_lote_hist ) )  titulo,
	t.nr_nota_fiscal nr_nota_fiscal,
	a.nr_sequencia nr_guia,
	a.cd_autorizacao cd_autorizacao,
	d.dt_lote dt_lote,
	obter_nome_setor(b.cd_setor_responsavel) ds_setor,
	obter_item_conv_proc_mat(b.nr_seq_propaci, b.nr_seq_matpaci) cd_item,
	obter_dados_lote_audit_item(b.nr_sequencia,'IT') ds_item,
	b.qt_item,
	obter_dados_lote_audit_item(b.nr_sequencia,'VO') vl_original,
	b.vl_glosa_informada,
	b.vl_glosa vl_glosa_aceita,
	b.vl_amenor vl_reapresentacao,
	substr(e.ds_motivo_glosa,1,200) ds_motivo_glosa,
	substr(obter_desc_resposta_glosa( b.cd_resposta),1,200) resposta_glosa,
	c.nr_sequencia sequencia,
	obter_conv_conta(a.nr_interno_conta) cd_convenio
from	lote_audit_hist_guia a,
	lote_audit_hist_item b,
	lote_audit_hist c,
	lote_auditoria d,
	titulo_receber t,
	atendimento_paciente_v f,
	motivo_glosa e
where	a.nr_sequencia 		= b.nr_seq_guia
and	a.nr_seq_lote_hist 	= c.nr_sequencia
and	c.nr_seq_lote_audit 	= d.nr_sequencia
and	to_number(obter_titulo_lote_guia(a.nr_interno_conta, a.cd_autorizacao, a.nr_seq_lote_hist)) = t.nr_titulo(+)
and	f.nr_atendimento 	= obter_atendimento_conta (a.nr_interno_conta)
and	e.cd_motivo_glosa(+) 	= b.cd_motivo_glosa
/