CREATE OR REPLACE VIEW RESUMEN_CLINICO_PF_V AS
SELECT
		stls.cd_tipo_logradouro street_type_solic,
		cpfs.ds_endereco street_name_solic,
		nvl(cpfs.nr_endereco, cpfs.ds_compl_end) building_number_solic,
		ctas.cd_tipo_asen deliv_installation_type_solic,
		cpfs.ds_bairro deliv_installation_area_solic,
		cpfs.nr_seq_localizacao_mx precinct_solic,
		cms.cd_cat_municipio county_solic,
		ces.cd_entidade state_solic,
		cpfs.cd_cep postal_code_solic,
		cpfs.nr_seq_pais country_solic,
		stla.cd_tipo_logradouro street_type_atend,
		cpfa.ds_endereco street_name_atend,
		nvl(cpfa.nr_endereco, cpfa.ds_compl_end) building_number_atend,
		ctaa.cd_tipo_asen deliv_installation_type_atend,
		cpfa.ds_bairro deliv_installation_area_atend,
		cpfa.nr_seq_localizacao_mx precinct_atend,
		cma.cd_cat_municipio county_atend,
		cea.cd_entidade state_atend,
		cpfa.cd_cep postal_code_atend,
		cpfa.nr_seq_pais country_atend,
		stlr.cd_tipo_logradouro street_type_resp,
		cpfr.ds_endereco street_name_resp,
		nvl(cpfr.nr_endereco, cpfr.ds_compl_end) building_number_resp,
		ctar.cd_tipo_asen deliv_installation_type_resp,
		cpfr.ds_bairro deliv_installation_area_resp,
		cpfr.nr_seq_localizacao_mx precinct_resp,
		cmr.cd_cat_municipio county_resp,
		cer.cd_entidade state_resp,
		cpfr.cd_cep postal_code_resp,
		cpfr.nr_seq_pais country_resp,
		med.nr_crm nr_crm_solic,
		pfm.cd_pessoa_fisica cd_pessoa_fisica_solic,
		z.ds_given_name nm_primeiro_nome_solic,
		z.ds_component_name_1 nm_sobrenome_pai_solic,
		z.ds_component_name_2 nm_sobrenome_mae_solic,
		cpfm.nr_telefone nr_telefone_solic,
		cpfm.ds_email ds_email_solic,
		ap.nr_atendimento nr_atendimento,
		cpfs.cd_pessoa_fisica cd_pessoa_fisica,
		cpfa.cd_pessoa_fisica cd_pessoa_fisica_2,
		cpfr.cd_pessoa_fisica cd_pessoa_fisica_3
FROM	compl_pessoa_fisica cpfs,
		sus_municipio sms,
		cat_tipo_assentamento ctas,
		cat_entidade ces,
		cat_municipio cms,
		sus_tipo_logradouro stls,
		compl_pessoa_fisica cpfa,
		sus_municipio sma,
		cat_tipo_assentamento ctaa,
		cat_entidade cea,
		cat_municipio cma,
		sus_tipo_logradouro stla,
		atendimento_paciente ap,
		compl_pessoa_fisica cpfr,
		sus_municipio smr,
		cat_tipo_assentamento ctar,
		cat_entidade cer,
		cat_municipio cmr,
		sus_tipo_logradouro stlr,
		medico med,
		pessoa_fisica pfm,
		compl_pessoa_fisica cpfm,
		TABLE(pkg_name_utils.search_names_legacy('', 'main')) z
WHERE	cpfs.ie_tipo_complemento = 1
AND		cpfs.nr_seq_assentamento_mx = ctas.nr_sequencia (+)
AND		cpfs.cd_tipo_logradouro = stls.cd_tipo_logradouro (+)
AND		cpfs.cd_municipio_ibge = sms.cd_municipio_ibge (+)
AND		sms.nr_seq_municipio_mx = cms.nr_sequencia (+)
AND		sms.nr_seq_entidade_mx = ces.nr_sequencia (+)
AND 	cpfa.cd_pessoa_fisica = ap.cd_pessoa_fisica
AND		cpfa.ie_tipo_complemento = 1
AND		cpfa.nr_seq_assentamento_mx = ctaa.nr_sequencia (+)
AND		cpfa.cd_tipo_logradouro = stla.cd_tipo_logradouro (+)
AND		cpfa.cd_municipio_ibge = sma.cd_municipio_ibge (+)
AND		sma.nr_seq_municipio_mx = cma.nr_sequencia (+)
AND		sma.nr_seq_entidade_mx = cea.nr_sequencia (+)
AND 	cpfr.cd_pessoa_fisica = NVL(ap.cd_pessoa_responsavel, (SELECT CD_PESSOA_FISICA_REF x FROM compl_pessoa_fisica x WHERE x.cd_pessoa_fisica = ap.cd_pessoa_fisica AND ie_tipo_complemento = 3)) --responsavel pessoa atendimento
AND		cpfr.ie_tipo_complemento = 1
AND		cpfr.nr_seq_assentamento_mx = ctar.nr_sequencia (+)
AND		cpfr.cd_tipo_logradouro = stlr.cd_tipo_logradouro (+)
AND		cpfr.cd_municipio_ibge = smr.cd_municipio_ibge (+)
AND		smr.nr_seq_municipio_mx = cmr.nr_sequencia (+)
AND		smr.nr_seq_entidade_mx = cer.nr_sequencia (+)
AND 	med.cd_pessoa_fisica = cpfs.cd_pessoa_fisica --pf_solic
AND 	pfm.nr_seq_person_name = z.nr_sequencia (+)
AND 	pfm.cd_pessoa_fisica = cpfm.cd_pessoa_fisica (+)
AND 	cpfm.ie_tipo_complemento (+) = 1
AND 	med.cd_pessoa_fisica = pfm.cd_pessoa_fisica;
/
