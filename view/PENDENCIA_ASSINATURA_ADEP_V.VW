create or replace
view	pendencia_assinatura_adep_v as
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'PAR' ie_tipo_item,
		null nr_prescricao,
		f.nr_parecer nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		parecer_medico_req		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_parecer							= f.nr_parecer
and		c.nr_seq_perda_ganho					is null
and		c.nr_parecer							is not null
and		c.nr_seq_anamnese						is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		c.ie_tipo_pendencia						<> 'L'
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union -- Perda e Ganho
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'GP' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		atendimento_perda_ganho	f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_perda_ganho					= f.nr_sequencia
and		c.nr_seq_perda_ganho					is not null
and		c.nr_parecer							is null
and		c.nr_seq_anamnese						is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.ie_tipo_pendencia						<> 'L'
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union -- Anamnese
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'AN' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		anamnese_paciente		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_anamnese						= f.nr_sequencia
and		c.nr_seq_anamnese						is not null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.ie_tipo_pendencia						<> 'L'
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union	--  Sinais Vitais
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'SV' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		atendimento_sinal_vital f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_sinal_vital					= f.nr_sequencia
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.ie_tipo_pendencia						<> 'L'
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is not null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union	-- SAE
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'SAE' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		pe_prescricao 			f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_sae							= f.nr_sequencia
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.ie_tipo_pendencia						<> 'L'
and		c.nr_seq_sae							is not null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union	-- CID Doencas
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'CID' ie_tipo_item,
		null nr_prescricao,
		f.nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		diagnostico_doenca 		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_diag_doenca					= f.nr_seq_interno
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is not null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		c.ie_tipo_pendencia						<> 'L'
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union	-- Registro EHR
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,a.ds_item,c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'EHR' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		ehr_registro 			f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_ehr_reg						= f.nr_sequencia
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is not null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		c.ie_tipo_pendencia						<> 'L'
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union	-- Loco Regional
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'LR' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		can_loco_regional		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_loco_reg						= f.nr_sequencia
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is not null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		c.ie_tipo_pendencia						<> 'L'
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union	-- Pendencia Conta
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'CC' ie_tipo_item,
		null nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		null nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		cta_pendencia			f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.nr_seq_cta_pendencia					= f.nr_sequencia
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is not null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		c.ie_tipo_pendencia						<> 'L'
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union   --- Evolucao
select	c.nr_sequencia,
		substr(coalesce(Obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		f.dt_liberacao,
		obter_data_assinatura_digital(f.nr_seq_assinatura) dt_assinatura,
		'EP' ie_tipo_item,
		null nr_prescricao,
		f.cd_evolucao nr_seq_interno,
		null nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		evolucao_paciente 		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.nr_atendimento						= f.nr_atendimento
and		c.cd_evolucao							= f.cd_evolucao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		c.cd_evolucao							is not null
and		c.ie_tipo_pendencia						<> 'L'
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Controle de Glicemia
		substr(coalesce(obter_nome_prot_glic(g.nr_seq_prot_glic),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'CCG' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_proc_hor			f,
		prescr_procedimento		g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					in ('G','C')
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_procedimento
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Medicamentos
		substr(coalesce(obter_desc_material(g.cd_material),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		c.ie_tipo_item_adep ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_mat_hor			f,
		prescr_material			g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					in ('M','MAT')
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_material
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Procedimentos
		substr(coalesce(g.ds_rotina,obter_desc_prescr_proc(g.cd_procedimento,g.ie_origem_proced,g.nr_seq_proc_interno),
					obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'P' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_proc_hor			f,
		prescr_procedimento		g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					in ('P','L','I')
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_procedimento
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Gasoterapia
		substr(coalesce(obter_desc_gas(g.nr_seq_gas),obter_descricao_tipo_item(c.ie_tipo_item_adep),
					b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'O' ie_tipo_item,
		g.nr_prescricao,
		g.nr_sequencia nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_gasoterapia_hor	f,
		prescr_gasoterapia		g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= ('O')
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_gasoterapia
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Suporte Nutricional Enteral
		substr(coalesce(obter_desc_material(g.cd_material),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'SNE' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_mat_hor			f,
		prescr_material			g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'SNE'
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_material
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_seq_item_adep						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---Dieta Oral
		substr(coalesce(obter_desc_dieta(g.cd_dieta),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		obter_data_assinatura_digital(g.nr_seq_assinatura) dt_assinatura,
		'D' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_dieta_hor		f,
		prescr_dieta			g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'D'
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_dieta
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---Solucoes
		substr(coalesce(f.ds_solucao,obter_prim_comp_sol(f.nr_prescricao,f.nr_seq_solucao),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'SOL' ie_tipo_item,
		f.nr_prescricao,
		f.nr_seq_interno nr_seq_interno,
		c.nr_etapa_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_solucao			f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'SOL'
and		f.nr_seq_solucao						= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---SNE
		substr(coalesce(obter_desc_material(f.cd_material),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'SNE' ie_tipo_item,
		f.nr_prescricao,
		c.nr_sequencia nr_seq_interno,
		c.nr_seq_item_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_material			f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'SNE'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---NPT Adulta
		substr(coalesce('NPT Adulta',obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'NPA' ie_tipo_item,
		f.nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		c.nr_seq_item_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		nut_paciente			f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'NPA'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---NPT Adulta Protocolo
		substr(coalesce('NPT Adulta Protocolo',obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'NAN' ie_tipo_item,
		f.nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		c.nr_seq_item_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		nut_pac					f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'NAN'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		f.ie_npt_adulta							= 'S'
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---NPT Neonatal
		substr(coalesce('NPT Neonatal',obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'NPN' ie_tipo_item,
		f.nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		c.nr_seq_item_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		nut_pac					f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'NPN'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		f.ie_npt_adulta							= 'N'
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---NPT Pediatrica
		substr(coalesce(obter_desc_expressao(305336),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'NPP' ie_tipo_item,
		f.nr_prescricao,
		f.nr_sequencia nr_seq_interno,
		c.nr_seq_item_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		nut_pac					f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'NPP'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		f.ie_npt_adulta							= 'P'
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Hemocomponente
		substr(coalesce(substr(obter_desc_procedimento( f.cd_procedimento,f.ie_origem_proced),1,255),substr(Obter_desc_san_exame(f.nr_seq_exame_sangue),1,255),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'HM' ie_tipo_item,
		f.nr_prescricao,
		f.nr_seq_solic_sangue nr_seq_interno,
		c.nr_seq_item_adep nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_procedimento		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'HM'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		f.nr_seq_solic_sangue					is not null
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Gasoterapia
		substr(coalesce(substr(obter_desc_gas(f.nr_seq_gas),1,255),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'O' ie_tipo_item,
		f.nr_prescricao,
		c.nr_sequencia nr_seq_interno,
		nvl(c.nr_seq_horario,c.nr_seq_item_adep) nr_seq_horario
from	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_gasoterapia		f
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'O'
and		f.nr_sequencia							= c.nr_seq_item_adep
and		f.nr_prescricao							= c.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, ---Recomendacao
		substr(coalesce(obter_desc_tipo_recomendacao(g.cd_recomendacao),g.ds_recomendacao,
					obter_descricao_tipo_item(c.ie_tipo_item_adep),b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'R' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_rec_hor			f,
		prescr_recomendacao		g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'R'
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_recomendacao
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- Suplemento Oral
		substr(coalesce(obter_desc_material(g.cd_material),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		null dt_assinatura,
		'S' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_mat_hor			f,
		prescr_material			g
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'S'
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_material
and		g.nr_prescricao							= f.nr_prescricao
and		c.nr_seq_anamnese						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
SELECT	c.nr_sequencia, ---Leites e Derivados
		SUBSTR(coalesce(obter_desc_material(g.cd_material),obter_descricao_tipo_item(c.ie_tipo_item_adep),
					b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		NVL(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		NULL dt_liberacao,
		NULL dt_assinatura,
		'L' ie_tipo_item,
		f.nr_prescricao,
		g.nr_seq_interno,
		f.nr_sequencia nr_seq_horario
FROM	pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		prescr_mat_hor			f,
		prescr_material			g,
		prescr_leite_deriv		h
WHERE	a.nr_sequencia 							= b.nr_seq_item_pront(+)
AND		c.nr_seq_item_pront 					= a.nr_sequencia(+)
AND		c.nr_atendimento   						= e.nr_atendimento(+)
AND		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
AND		c.ie_tipo_item_adep 					= 'LD'
AND		f.nr_sequencia							= c.nr_seq_horario
AND		g.nr_sequencia							= f.nr_seq_material
AND		g.nr_prescricao							= f.nr_prescricao
AND		h.nr_sequencia							= g.nr_seq_leite_deriv
AND		h.nr_prescricao							= g.nr_prescricao
AND		c.nr_parecer							IS NULL
AND		c.nr_seq_perda_ganho					IS NULL
AND		c.nr_seq_cta_pendencia					IS NULL
AND		c.nr_seq_diag_doenca					IS NULL
AND		c.nr_seq_ehr_reg						IS NULL
AND		c.nr_seq_loco_reg						IS NULL
AND		c.nr_seq_sae							IS NULL
AND		c.nr_seq_sinal_vital					IS NULL
AND		NVL(c.ie_pendente_assinat_usuario,'N')	= 'N'
union
select	c.nr_sequencia, --- SAE
		substr(coalesce(Obter_desc_intervencoes(g.nr_seq_proc),obter_descricao_tipo_item(c.ie_tipo_item_adep),
			        b.ds_item_instituicao,a.ds_item_instituicao,obter_desc_expressao(a.cd_exp_desc_item,a.ds_item),c.ds_item),1,255) ds_item,
		nvl(b.nr_seq_apres,99999) nr_seq_apres,
		c.nm_usuario,
		c.nr_atendimento,
		b.cd_perfil,
		null dt_liberacao,
		obter_data_assinatura_digital(h.nr_seq_assinatura) dt_assinatura,
		'E' ie_tipo_item,
		h.nr_prescricao,
		h.nr_sequencia nr_seq_interno,
		f.nr_sequencia nr_seq_horario
from		pessoa_fisica 			d,
		pep_item_pendente		c,
		prontuario_item			a,
		perfil_item_pront		b,
		atendimento_paciente 	e,
		pe_prescr_proc_hor		f,
		pe_prescr_proc			g,
		pe_prescricao			h
where	a.nr_sequencia 							= b.nr_seq_item_pront(+)
and		c.nr_seq_item_pront 					= a.nr_sequencia(+)
and		c.nr_atendimento   						= e.nr_atendimento(+)
and		d.cd_pessoa_fisica 						= c.cd_pessoa_fisica
and		c.ie_tipo_item_adep 					= 'E'
and		f.nr_sequencia							= c.nr_seq_horario
and		g.nr_sequencia							= f.nr_seq_pe_proc
and		h.nr_sequencia							= g.nr_seq_prescr
and		c.nr_seq_anamnese						is null
and		c.nr_seq_item_adep						is null
and		c.nr_parecer							is null
and		c.nr_seq_perda_ganho					is null
and		c.nr_seq_cta_pendencia					is null
and		c.nr_seq_diag_doenca					is null
and		c.nr_seq_ehr_reg						is null
and		c.nr_seq_loco_reg						is null
and		c.nr_seq_sae							is null
and		c.nr_seq_sinal_vital					is null
and		nvl(c.ie_pendente_assinat_usuario,'N')	= 'N';
/
