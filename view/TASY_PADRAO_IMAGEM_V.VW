create or replace 
view Tasy_Padrao_Imagem_v as
select	a.nr_seq_legenda,
	a.nr_seq_apresent,
	b.cd_estabelecimento,
	b.cd_perfil,
	nvl(b.ie_imagem,a.ie_imagem) ie_imagem, 	       
	nvl(b.ds_legenda, a.ds_legenda) ds_legenda,
	nvl(b.ie_utilizado,'S') ie_utilizado
from	 tasy_padrao_imagem a,         
	tasy_padrao_imagem_perfil b  
where   a.nr_sequencia = b.nr_seq_padrao(+);
/