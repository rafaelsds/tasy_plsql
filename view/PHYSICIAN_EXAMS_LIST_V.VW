CREATE OR REPLACE VIEW physician_exams_list_v (
    nr_atendimento,
    nr_prescricao,
    nr_sequencia,
    nr_acesso_dicom,
    ds_exame,
    cd_pessoa_fisica,
    nm_pessoa_fisica,
    dt_baixa,
    qt_lista,
    ie_status_execucao,
    nr_prescr_proc_dit_sequencia
) AS
    SELECT DISTINCT
        prescr_med.nr_atendimento,
        prescr_proc.nr_prescricao,
        prescr_proc.nr_sequencia,
        prescr_proc.nr_acesso_dicom,
        substr(nvl(obter_desc_proc_interno(prescr_proc.nr_seq_proc_interno),obter_desc_procedimento(prescr_proc.cd_procedimento,prescr_proc
.ie_origem_proced) ),1,255) ds_exame,
        pes_fis.cd_pessoa_fisica,
        pes_fis.nm_pessoa_fisica,
        prescr_proc.dt_baixa,
        (
            SELECT
                COUNT(1)
            FROM
                lista_central_exame x
            WHERE
                x.nr_prescricao = prescr_proc.nr_prescricao
                AND x.nr_sequencia_prescricao = prescr_proc.nr_sequencia
        ) qt_lista,
        prescr_proc.ie_status_execucao,
        prescr_proc_dit.nr_sequencia AS nr_prescr_proc_dit_sequencia
    FROM
        prescr_procedimento prescr_proc,
        prescr_medica prescr_med,
        procedimento_paciente proc_pac,
        atendimento_paciente atend_pac,
        atend_categoria_convenio atend_cat_conv,
        pessoa_fisica pes_fis,
        conta_paciente conta_pac,
        prescr_proc_ditado prescr_proc_dit 
    WHERE
		prescr_proc.nr_prescricao = prescr_med.nr_prescricao
		AND (prescr_proc.nr_prescricao = proc_pac.nr_prescricao(+)
								  AND prescr_proc.nr_sequencia = proc_pac.nr_sequencia_prescricao(+))
		AND prescr_med.nr_atendimento = atend_pac.nr_atendimento(+)
		AND atend_pac.nr_atendimento = atend_cat_conv.nr_atendimento
		AND atend_pac.cd_pessoa_fisica = pes_fis.cd_pessoa_fisica
		AND proc_pac.nr_interno_conta = conta_pac.nr_interno_conta(+)
		AND prescr_proc.nr_seq_interno = prescr_proc_dit.nr_seq_prescr_proc(+)
		AND proc_pac.qt_procedimento > 0
		AND nvl(conta_pac.ie_cancelamento,'N') = 'N'
		AND atend_cat_conv.nr_seq_interno = obter_atecaco_atendimento(atend_cat_conv.nr_atendimento);
/

