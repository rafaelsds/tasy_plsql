create or replace view planificacion_familiar_nom_v as
select  0 ie_tipo_registro, 
	'clues|curpPrestador|nombrePrestador|primerApellidoPrestador|segundoApellidoPrestador|tipoPersonal|especificaTipoPersonal|cedulaProfesional|servicioAtencion|especificarServicio|'|| 
	'curpPaciente|nombre|primerApellido|segundoApellido|fechaNacimiento|entidadNacimiento|edad|claveEdad|sexo|seConsideraIndigena|spss|numeroAfiliacionSpss|prospera|'|| 
	'folioProspera|imss|numeroAfiliacionImss|issste|numeroAfiliacionIssste|otraAfiliacion|numeroOtraAfiliacion|peso|talla|tipoDificultad|gradoDificultad|origenDificultad|migrante|'||
	'fechaConsulta |relacionTemporal|descripcionDiagnostico1|primeraVezDiagnostico1|suiveCausesDiagnostico1|codigoCIEDiagnostico1|descripcionDiagnostico2|primeraVezDiagnostico2|'||
	'suiveCausesDiagnostico2|codigoCIEDiagnostico2|descripcionDiagnostico3|primeraVezDiagnostico3|suiveCausesDiagnostico3|codigoCIEDiagnostico3|primeraVezAnio|oral|inyectableMensual|'||
	'inyectableBimestral|implanteSubdermico|parcheDermico|diu|diuMedicado|quirurgico|preservativo|preservativoFemenino|otroMetodo|orientacionPF|anticoncepcionEmergencia|vasectomiaSinBisturi|'||
	'altaConAzoospermia|lineaVida|cartillaVacunacion|referido|contrarreferido|telemedicina' CABECARIO,
	null clues,
	null curpprestador,
	null nombreprestador, 
	null primerapellidoprestador, 
	null segundoapellidoprestador, 
	null tipopersonal, 
	null especificatipopersonal, 
	null cedulaprofesional, 
	null servicioatencion, 
	null especificarservicio, 
	null curppaciente, 
	null nombre, 
	null primerapellido, 
	null segundoapellido, 
	null fechanacimiento, 
	null entidadenacimiento,
	null edad, 
	null claveedad, 
	null sexo, 
	null seconsideraindigena, 
	null spss, 
	null numeroafiliacionspss, 
	null prospera, 
	null folioprospera, 
	null imss, 
	null numeroafiliacionimss, 
	null issste, 
	null numeroafiliacionissste, 
	null otraafiliacion, 
	null numerootraafiliacion,
	null peso, 
	null talla,
	null tipodificultad, 
	null gradodificultad, 
	null origendificultad,
	null migrante, 
	null fechaconsulta,
	null relaciontemporal,
	null descripciondiagnostico1,
	null primeravezdiagnostico1,
	null suivecausesdiagnostico1, 
	null codigociediagnostico1, 
	null descripciondiagnostico2, 
	null primeravezdiagnostico2, 
	null suivecausesdiagnostico2, 
	null codigociediagnostico2,
	null primeravezanio,
	null oral,
	null inyectablemensal,
	null inyectablebimestral,
	null implantesubdermico,
	null parchedermico,
	null diu,
	null diumedicado,
	null quirurgico,
	null preservativo,
	null preservativofemenino,
	null otrometodo,
	null orientacionpf,
	null anticoncepcionemergencia,
	null vasectomiasinbisturi,
	null altaconazoospermia,
	null lineavida,
	null cartillavacunacion,
	null referido,
	null contrarreferido,
	null telemedicina,
	null dt_entrada,
	null nr_atendimento
from dual 
union all 
select 
	--prestador servico-- 
	2 ie_tipo_registro, 
	'' cabecario, 
	pj.cd_internacional clues, 
	nvl(substr(upper(pj.cd_curp),1,18), 'XXXX999999XXXXXX99') curpprestador, 
	nvl(substr(upper(pj.ds_razao_social),1,50), 'XX') nombreprestador, 
	nvl(substr(upper(pj.nm_fantasia),1,50), 'XX') primerapellidoprestador, 
	nvl(substr(upper(pj.ds_nome_abrev),1,50), 'XX') segundoapellidoprestador, 
	profissional.ie_profissional  tipopersonal,
	decode(profissional.ie_profissional,'12',obter_desc_expressao(308221)) especificatipopersonal,  
	case 
		when profissional.ie_profissional in ('2','3','4','6','8')
		then nvl(lpad(nvl(pf_m.ds_codigo_prof, m.nr_crm),8,0),0) 
		else '0' 
	end as cedulaprofesional, 
	decode(b.ie_clinica,2,3,1,4,3,5,1000,6,1001,7,1002,8,1004,9,1005,13,1006,14,4,16,1007,17,1008,22,1009,23) servicioatencion,
	'' especificarservicio, 
	--dados paciente-- 
	nvl(substr(upper(pf_p.cd_curp),1,18), 'XXXX999999XXXXXX99') curppaciente, 
	substr(upper(y.ds_given_name),1,50) nombre,	 
	nvl(substr(upper(y.ds_family_name),1,50), 'XX') primerapellido,
	nvl(substr(upper(y.ds_component_name_1),1,50), 'XX') segundoapellido,
	nvl(to_char(pf_p.dt_nascimento, 'dd/mm/yyyy'), null) fechanacimiento, 
	nvl(ce.cd_entidade, '99') entidadenacimiento, 
        case 
		when obter_idade(pf_p.dt_nascimento, b.dt_entrada, 'a') is not null 
		then nvl(obter_idade_imc_nom(b.nr_atendimento,pf_p.dt_nascimento,b.dt_entrada,'IDADE'),-1) 
		else 999 
	end as edad, 
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_entrada, 'a') is not null 
		then obter_idade_imc_nom(b.nr_atendimento,pf_p.dt_nascimento,b.dt_entrada,'CLAVEEDAD') 
		else 9 
	end as claveedad, 
	decode(pf_p.ie_sexo, 'M', 1 , 'F', 2, 9) sexo, 
	decode(pf_p.nr_seq_cor_pele, 201, 1, 0) seconsideraindigena, 
	nvl2(pf_p.nr_spss, 1, 0) spss,
	case 
		when pf_p.nr_spss is not null 
		then substr(pf_p.nr_spss,1,13)
	end as numeroafiliacionspss,
	decode(n.cd_tipo_convenio_mx, 13, 1,0) prospera,
	decode(n.cd_tipo_convenio_mx, 13, elimina_caractere_especial(substr(p.nr_doc_convenio,1,16))) folioprospera,
	decode(n.cd_tipo_convenio_mx, 2, 1,0) imss,
	decode(n.cd_tipo_convenio_mx, 2, substr(p.nr_doc_convenio,1,11)) numeroafiliacionimss,
	decode(n.cd_tipo_convenio_mx, 3, 1,0) issste,
	decode(n.cd_tipo_convenio_mx, 3, substr(p.nr_doc_convenio,1,13)) numeroafiliacionissste,
	decode(n.cd_tipo_convenio_mx, 15, 1,0) otraafiliacion,
	decode(n.cd_tipo_convenio_mx, 15, substr(p.nr_doc_convenio,1,15)) numerootraafiliacion,
	nvl(nvl((select	max(qt_peso) 
		from	atendimento_sinal_vital 
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1) 
		from	atendimento_sinal_vital 
		where	qt_peso is not null 
		and	nr_atendimento	= b.nr_atendimento 
		and	nvl(ie_situacao,'A') = 'A' 
		and	nvl(ie_rn,'N')	= 'N')), pf_p.qt_peso), '999') peso, 
	nvl(nvl((select	max(qt_altura_cm) 
		from	atendimento_sinal_vital 
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1) 
		from	atendimento_sinal_vital 
		where	qt_altura_cm is not null 
		and	nr_atendimento	= b.nr_atendimento 
		and	nvl(ie_situacao,'A') = 'A'
		and	nvl(ie_rn,'N')	= 'N')), pf_p.qt_altura_cm), '999') talla,
	(select max(ptd.nr_seq_tipo_def) from pf_tipo_deficiencia ptd where ptd.cd_pessoa_fisica = b.nr_atendimento) tipodificultad, 
	case 
		when (select max(ptd.nr_seq_tipo_def) from pf_tipo_deficiencia ptd where ptd.cd_pessoa_fisica = pf_p.cd_pessoa_fisica) = 56 
		then 9 
		else decode((select max(ptd.nr_seq_tipo_def) from pf_tipo_deficiencia ptd where ptd.cd_pessoa_fisica = pf_p.cd_pessoa_fisica), 56,9, (select max(ptd.ie_grau_deficiencia) from pf_tipo_deficiencia ptd where ptd.cd_pessoa_fisica = pf_p.cd_pessoa_fisica)) 
	end as gradodificultad,
	case 
		when (select max(ptd.nr_seq_tipo_def) from pf_tipo_deficiencia ptd where ptd.cd_pessoa_fisica = pf_p.cd_pessoa_fisica) = 56 
		then 9 
		else (select max(ptd.nr_seq_causa_lesao) from pf_tipo_deficiencia ptd where ptd.cd_pessoa_fisica = pf_p.cd_pessoa_fisica)
	end as origendificultad,
	-1 migrante, 
	nvl(to_char(b.dt_entrada, 'dd/mm/yyyy'), null) fechaconsulta,
	decode((select count(*) from atendimento_paciente ap where (to_char(ap.dt_inicio_atendimento, 'yyyy') = extract(year from b.dt_entrada)) and b.nr_atendimento = ap.nr_atendimento), null,0,1) relaciontemporal,
	elimina_caractere_especial(substr(upper((select max(cd.ds_doenca_cid) 
											from diagnostico_doenca dd, cid_doenca cd 
											where dd.nr_atendimento = b.nr_atendimento 
											and dd.cd_doenca = cd.cd_doenca_cid 
											and dd.ie_classificacao_doenca = 'P')),1, 255)) descripciondiagnostico1,
	case 
		when ((select count(*) from diagnostico_doenca ddp where to_char(ddp.dt_diagnostico, 'yyyy') = extract(year from b.dt_entrada) and ddp.nr_atendimento = b.nr_atendimento and ddp.ie_classificacao_doenca = 'P') > 1)
		then 0 
		else 1 
	end as primeravezdiagnostico1,
	case
		when (select max(cd.ds_doenca_cid) from diagnostico_doenca dd, cid_doenca cd where dd.nr_atendimento = b.nr_atendimento and dd.cd_doenca = cd.cd_doenca_cid and dd.ie_classificacao_doenca = 'P') is not null
		then nvl((select decode(nvl(ie_suive_morb, ie_causa), 'S', '0', '1') from cid_doenca a2 
					where a2.cd_doenca_cid = (select max(cd_doenca) 
												from diagnostico_doenca 
												where nr_atendimento = b.nr_atendimento 
												and ie_classificacao_doenca = 'P')), 'N')
	end as suivecausesdiagnostico1,
	(select max(cd.cd_doenca_cid) from diagnostico_doenca dd, cid_doenca cd where dd.nr_atendimento = b.nr_atendimento and dd.cd_doenca = cd.cd_doenca_cid and dd.ie_classificacao_doenca = 'P') codigociediagnostico1,
	elimina_caractere_especial(substr(upper((select max(cd.ds_doenca_cid) 
											from diagnostico_doenca dd, cid_doenca cd 
											where dd.nr_atendimento = b.nr_atendimento 
											and dd.cd_doenca = cd.cd_doenca_cid 
											and dd.ie_classificacao_doenca = 'S')),1, 255)) descripciondiagnostico2, 
	case 
		when ((select count(*) from diagnostico_doenca ddp where to_char(ddp.dt_diagnostico, 'yyyy') = extract(year from b.dt_entrada) and ddp.nr_atendimento = b.nr_atendimento and ddp.ie_classificacao_doenca = 'S') > 1)
		then 0 
		else 1 
	end as primeravezdiagnostico2,
	case
		when (select max(ds_diagnostico) from diagnostico_doenca where nr_atendimento = b.nr_atendimento and ie_classificacao_doenca = 'S') is not null
		then nvl((select decode(nvl(ie_suive_morb, ie_causa), 'S', '0', '1') from cid_doenca a2 
					where a2.cd_doenca_cid = (select max(cd_doenca) 
					from diagnostico_doenca 
					where nr_atendimento = b.nr_atendimento 
					and ie_classificacao_doenca = 'S')), 'N') 
	end as suivecausesdiagnostico2,
	(select max(cd.cd_doenca_cid) from diagnostico_doenca dd, cid_doenca cd where dd.nr_atendimento = b.nr_atendimento and dd.cd_doenca = cd.cd_doenca_cid and dd.ie_classificacao_doenca = 'S') codigociediagnostico2,
	0 primeravezanio,
	case
		when hsm.ds_metodos_contrac = 2 and hsm.ds_compl_contrac = 1
		then hsm.nr_contrac_realizado
		else 0
	end as oral,
	case
		when hsm.ds_metodos_contrac = 2 and hsm.ds_compl_contrac = 2
		then '1'
		else '0'
	end as inyectablemensal,
	case
		when hsm.ds_metodos_contrac = 2 and hsm.ds_compl_contrac = 3
		then '1'
		else '0'
	end as inyectablebimestral,
	case
		when hsm.ds_metodos_contrac = 6
		then hsm.ds_compl_contrac
		else '-1'
	end as implantesubdermico,
	case
		when hsm.ds_metodos_contrac = 2 and hsm.ds_compl_contrac = 4
		then hsm.nr_contrac_realizado
		else -1
	end as parchedermico,
	case
		when hsm.ds_metodos_contrac = 1 and hsm.ds_compl_contrac = 1
		then nvl(hsm.nr_contrac_realizado, -1)
		else -1
	end as diu,
	case
		when hsm.ds_metodos_contrac = 1 and hsm.ds_compl_contrac = 2
		then nvl(hsm.nr_contrac_realizado, -1)
		else -1
	end as diumedicado,
	-1 quirurgico,
	case
		when hsm.ds_metodos_contrac = 5 and hsm.ds_compl_contrac = 1
		then hsm.nr_contrac_realizado
		else 0
	end as preservativo,
	case
		when hsm.ds_metodos_contrac = 5 and hsm.ds_compl_contrac = 2
		then hsm.nr_contrac_realizado
		else 0
	end as preservativofemenino,
	0 otrometodo,
	case
		when pf_p.ie_sexo = 'M'
		then decode(hsm.ie_orient_met_contrac,'S','1', '0')
		when pf_p.ie_sexo = 'M'
		then decode(phs.ie_orient_met_contrac,'S','1', '0')
		else '0'
	end as orientacionpf,
	case
		when hsm.ds_metodos_contrac = 5 and hsm.ds_compl_contrac = 3
		then 1
		else 0
	end as anticoncepcionemergencia,
	case 
		 when pf_p.ie_sexo = 'M' and (select max(b.ds_tipo_vasectomia)
		 	  		from historico_saude_cirurgia a, procedimento b 
					where a.cd_pessoa_fisica = b.cd_pessoa_fisica
					and a.cd_procedimento = b.cd_procedimento
					and b.cd_tipo_procedimento = 66) = 'S'
		 then 1
		 else 0
	end as vasectomiasinbisturi,
	case 
		when pf_p.ie_sexo = 'M' and (select max(b.ds_tipo_vasectomia)
					from historico_saude_cirurgia a, procedimento b 
					where a.cd_pessoa_fisica = b.cd_pessoa_fisica
					and a.cd_procedimento = b.cd_procedimento
					and b.cd_tipo_procedimento = 66) = 'N'
		then 1
		else 0
	end as altaconazoospermia,
	0 lineavida,
	0 cartillavacunacion,
	-1 referido,
	0 contrarreferido,
	0 telemedicina,
	b.dt_entrada dt_entrada,
	b.nr_atendimento nr_atendimento
from	atendimento_paciente b,
	pessoa_juridica pj,
	estabelecimento e,
	pessoa_fisica pf_m,
	pessoa_fisica pf_p,
	medico m,
	sus_municipio sm,
	cat_entidade ce,
	atend_categoria_convenio p,
	categoria_convenio cc,
	convenio n,
	cat_derechohabiencia k,
	historico_saude_mulher hsm,
	paciente_hist_social phs,
	atend_profissional profissional,
	person_name y
where 	b.nr_atendimento  		= p.nr_atendimento
and	pj.cd_cgc 			= e.cd_cgc
and     b.cd_estabelecimento 		= e.cd_estabelecimento
and 	b.cd_pessoa_fisica		= pf_p.cd_pessoa_fisica
and 	b.cd_medico_resp 		= m.cd_pessoa_fisica
and	pf_m.cd_pessoa_fisica 		= m.cd_pessoa_fisica 
and     pf_p.cd_municipio_ibge		= sm.cd_municipio_ibge (+)
and     sm.nr_seq_entidade_mx		= ce.nr_sequencia (+)
and	p.cd_convenio 			= cc.cd_convenio
and	p.cd_categoria 			= cc.cd_categoria
and	n.cd_convenio 			= cc.cd_convenio
and	n.cd_tipo_convenio_mx  	 	= k.nr_sequencia (+)
and	hsm.cd_pessoa_fisica (+) 	= b.cd_pessoa_fisica
and	phs.cd_pessoa_fisica (+) 	= b.cd_pessoa_fisica
and 	b.nr_atendimento 		= profissional.nr_atendimento
and	y.nr_sequencia 			= pf_p.nr_seq_person_name
and	y.ds_type 			= 'main'; 
/