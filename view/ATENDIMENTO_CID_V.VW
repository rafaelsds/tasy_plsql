create or replace
view ATENDIMENTO_CID_V as
select	substr(obter_nome_pessoa_fisica(a.cd_pessoa_fisica, null), 1, 100) nm_pessoa_fisica,
	a.nr_atendimento,
	d.ds_procedimento ds_diag_proced,
	d.cd_doenca_cid,
	c.cd_medico_executor cd_medico,
	substr(obter_nome_pessoa_fisica(nvl(c.cd_medico_executor, a.cd_medico_resp), null), 1, 100) nm_medico,
	f.cd_setor_atendimento,
	f.ds_setor_atendimento,
	a.dt_entrada,
	substr(obter_desc_cid(d.cd_doenca_cid),1,100) ds_doenca_cid
from	procedimento d,
	setor_atendimento f,
	procedimento_paciente c,
	atendimento_paciente a
where	a.nr_atendimento	= c.nr_atendimento
and	c.nr_interno_conta is not null
and	d.cd_procedimento	= c.cd_procedimento
and	d.ie_origem_proced	= c.ie_origem_proced
and	f.cd_setor_atendimento	= c.cd_setor_atendimento
union	all
select	substr(obter_nome_pessoa_fisica(a.cd_pessoa_fisica, null), 1, 100) nm_pessoa_fisica,
	a.nr_atendimento,
	b.ds_diagnostico ds_diag_proced,
	b.cd_doenca cd_doenca_cid,
	e.cd_medico cd_medico,
	substr(obter_nome_pessoa_fisica(e.cd_medico, null),1, 100) nm_medico,
	0 cd_setor_atendimento,
	'' ds_setor_atendimento,
	a.dt_entrada,
	substr(obter_desc_cid(b.cd_doenca),1,100) ds_doenca_cid
from	diagnostico_doenca b,
	diagnostico_medico e,
	atendimento_paciente a
where	a.nr_atendimento	= b.nr_atendimento
and	e.nr_atendimento	= b.nr_atendimento
and	e.dt_diagnostico	= b.dt_diagnostico;
/
