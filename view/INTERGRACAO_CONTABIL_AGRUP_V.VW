Create or replace view intergracao_contabil_agrup_v
as
select	dt_movimento,
	cd_conta_debito,
	cd_conta_credito,
	sum(nvl(vl_movimento,0)) vl_movimento,
	cd_historico,
	substr(ds_historico,1,500) ds_historico,
	cd_centro_custo,
	ci_integra,
	substr(ds_compl_historico,1,255) ds_compl_historico,
	nr_lote_contabil
from	movimento_contabil_v
group by	dt_movimento,
		cd_conta_debito,
		cd_conta_credito,
		cd_historico,
		substr(ds_historico,1,500),
		cd_centro_custo,
		ci_integra,
		substr(ds_compl_historico,1,255),
		nr_lote_contabil;
/