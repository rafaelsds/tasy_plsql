CREATE OR REPLACE
VIEW Eis_Mat_Med_Cirurgia_v AS
SELECT	a.*,
	SUBSTR(obter_desc_estrut_proc(NULL, NULL, NULL, a.cd_procedimento, a.ie_origem_proced),1,110) ds_procedimento,
	TO_CHAR(a.cd_procedimento) || TO_CHAR(a.ie_origem_proced) cd_proc_origem,
	SUBSTR(OBTER_DESC_MATERIAL(a.cd_material),1,255) ds_material,
	SUBSTR(OBTER_DESC_KIT(a.cd_kit_material),1,255) ds_kit,
	SUBSTR(obter_nome_pf(a.cd_medico_cirurgiao),1,60) nm_medico,
	SUBSTR(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_pessoa_fisica
FROM	EIS_MATERIAL_CIRURGIA a;
/