Create or replace View faturamento_cliente_v as
SELECT   Substr(Obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,60) ds_pessoa,
         Substr(Obter_cgc_cpf_editado(a.cd_cgc),1,30) cd_cgc,
         Substr(Obter_dados_nota_fiscal(a.nr_sequencia,31),1,40) ds_estado,
         Decode(b.cd_procedimento,69000700,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000700,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),29000600,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),19000600,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),59000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),99000006,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_custom,
         Decode(b.cd_procedimento,69000600,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),29000700,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),99000008,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000600,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_trein,
         Decode(b.cd_procedimento,69000400,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000400,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)), 99000003,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),59000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),29000400,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),19000400,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_cdu,
         Decode(b.cd_procedimento,6900700, (b.vl_total_item_nf - Nvl(b.vl_desconto,0)),69000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),19000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),99000002,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),59000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000700, (b.vl_total_item_nf - Nvl(b.vl_desconto,0)),19000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)), 2900020,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_manut, 
         Decode(b.cd_procedimento,69000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),99000005,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)), 19000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000400,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),59000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),29000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),69000855,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_lut,
         Decode(b.cd_procedimento,69000750,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000650,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_sup_adicional,
         Decode(b.cd_procedimento,69000101,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000900,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) vl_versao,
         Decode(b.cd_procedimento,69000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),69000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000200,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000300,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),99000004,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),59000400,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),29000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),19000100,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000400, (b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0)  vl_impl,
         Decode(b.cd_procedimento,69000800,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),69000850,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),85000900,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),79000800,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),39000004,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)), 79000901,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000600,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) 	vl_outros, 
         Decode(b.cd_procedimento,69000701,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),29000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),99000007,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),49000600,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),19000500,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),21000670,(b.vl_total_item_nf - Nvl(b.vl_desconto,0)),0) integracao,
         (b.vl_total_item_nf - Nvl(b.vl_desconto,0)) vl_total, 
		(SELECT Nvl(Sum(vl_repasse),0) 
              FROM   nota_fiscal_item_rep r 
              WHERE  r.nr_seq_nf = a.nr_sequencia 
              AND    b.nr_item_nf = r.nr_seq_item_nf) 	vl_repasse,
         a.cd_estabelecimento cd_estab,
         a.dt_emissao dt_nota,
         a.ie_situacao ie_status,
         a.cd_cgc cd_cnpj,
         a.nr_sequencia nr_seq_nf,
         a.cd_pessoa_fisica cd_pf,
	 c.cd_grupo_proc
FROM     nota_fiscal_item B, 
         nota_fiscal A, 
         procedimento C 
WHERE    a.nr_sequencia = b.nr_sequencia 
AND      b.cd_procedimento IS NOT NULL 
AND      c.cd_procedimento = b.cd_procedimento 
AND      c.ie_origem_proced = b.ie_origem_proced;
/
