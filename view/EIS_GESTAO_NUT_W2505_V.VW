create or replace
view eis_gestao_nut_W2505_v as
select	a.nr_sequencia,
	a.nr_seq_servico,
	a.dt_servico,
	(select	count(*)
	from	nut_atend_acomp_dieta b,
		nut_atend_acompanhante c
	where	c.nr_seq_atend_serv_dia = a.nr_sequencia
	and	b.nr_seq_atend_acomp	= c.nr_sequencia) qt_dieta_acomp
from	nut_atend_serv_dia a
/
