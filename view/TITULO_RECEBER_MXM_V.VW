CREATE OR REPLACE VIEW TITULO_RECEBER_MXM_V AS
select	'PR' ie_tipo,
	nvl(a.cd_cgc,a.cd_pessoa_fisica) cd_pessoa,
	to_char(a.nr_titulo) nr_titulo,
	substr(obter_dados_titulo_receber(a.nr_titulo,'NE'),1,20) nr_nota_fiscal,
	'34' cd_empresa_emitente,
	'02' cd_filial,
	'34' cd_empresa_pagadora,
	'NF' ie_tipo_titulo,
	to_char(a.dt_emissao,'ddmmyyyy') dt_emissao,
	to_char(a.dt_vencimento,'ddmmyyyy') dt_vencimento_original,
	to_char(a.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento_atual,
	'R' cd_moeda,
	a.vl_titulo vl_titulo,
	'BO' cd_tipo_cobranca,
	decode(a.cd_cgc,null,'04189','999') cd_conta_contabil,
	substr(replace(replace(a.ds_observacao_titulo,chr(13),''),chr(10),''),1,60) ds_observacao
from	titulo_receber a
where	a.dt_integracao_externa is null
and	a.ie_situacao	not in ('3','5')
/