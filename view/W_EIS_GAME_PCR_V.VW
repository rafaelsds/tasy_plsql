create or replace view w_eis_game_pcr_v  as 
select	distinct
	c.cd_setor_atendimento setor,
	substr(obter_nome_setor(c.cd_setor_atendimento),1,255) ds_setor_atendimento,
	e.cd_profissional,
	c.cd_unidade ds_unidade, 
	TRUNC(b.dt_Evento) dt_evento,	
	b.nr_atendimento NA,
	a.dt_acionamento_equipe	dt_acionamento,
	e.dt_chegada	dt_profissional,
	a.dt_encerramento	dt_fim,
	((e.dt_chegada - a.dt_acionamento_equipe)*24*60) qt_minutos_resp,
	dividir(((a.dt_encerramento - a.dt_acionamento_equipe)*24*60),nvl(obter_dados_PCR(a.nr_sequencia,'P'),1)) qt_minutos_atend,
	substr(obter_categoria_pcr(e.ie_tipo_evolucao),1,255) ds_categoria,
	a.ie_desfecho,
	dividir(decode(a.ie_desfecho,'R',1,0),nvl(obter_dados_PCR(a.nr_sequencia,'P'),1)) qt_desfecho_sat,
	substr(Obter_desc_desfecho_pcr(a.ie_desfecho),1,255) ds_desfecho,
	a.nr_sequencia	
from	qua_evento_paciente b,
	qua_tipo_evento d,
	qua_evento f,
	resumo_atendimento_paciente_v c,
	qua_evento_pcr a,	
	qua_evento_pcr_prof e
where	b.dt_inativacao is null
and	a.nr_seq_evento = b.nr_sequencia
and 	e.nr_seq_pcr = a.nr_sequencia
and	b.nr_atendimento = c.nr_atendimento
and	d.ie_tipo_evento = 'PCR'
and	d.nr_sequencia = f.nr_seq_tipo
and	b.nr_Seq_evento = f.nr_sequencia
and	a.dt_encerramento is not null;
/