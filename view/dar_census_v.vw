create or replace view dar_census_v as
select
	a.nr_sequencia,
    obter_dados_pf(a.cd_pessoa_fisica,'NSOC')patientFullName,
    a.nr_atendimento patientEncounterNumber,
    b.nr_pront_ext  patientLifeTimeNumber,
    a.cd_pessoa_fisica patientAccountNumber,
    obter_dados_pf(a.cd_pessoa_fisica, 'I')patientAge,
    obter_dados_pf(a.cd_pessoa_fisica, 'DN') patientDateOfBirth,
    obter_dados_pf(a.cd_pessoa_fisica, 'SE') patientGender,
    obter_valor_dominio(17, a.ie_clinica) patientType,
    (select max(y.ds_diagnostico)
            from     diagnostico_medico y
            where    y.nr_atendimento    = a.nr_atendimento
            and     y.dt_diagnostico = (select min(x.dt_diagnostico)
                          from      diagnostico_medico x
                          where  x.nr_atendimento = a.nr_atendimento)) primaryDiagnosis,
    to_char(a.dt_entrada_unidade, 'dd/mm/yyyy hh24:mi:ss') intTimeClinicalUnit,
    to_char(a.dt_saida_unidade, 'dd/mm/yyyy hh24:mi:ss') outTimeClinicalUnit,
    to_char(a.dt_entrada_unidade, 'dd/mm/yyyy') intTimeCalendarDayClinicalUnit,
    to_char(a.dt_entrada_unidade, 'yyyy') inTimeYearClinicalUnit,
    to_number(to_char(a.dt_entrada_unidade, 'MM')) inTimeMonthClinicalUnit,
    to_number(to_char(a.dt_entrada_unidade, 'dd')) InTimeDayOfMonthClinicalUnit,
    to_char(a.dt_saida_unidade, 'dd/mm/yyyy') OutTimeCalendarDayClinicalUnit,
    to_char(a.dt_saida_unidade, 'yyyy') OutTimeYear,
    to_number(to_char(a.dt_saida_unidade, 'MM')) OutTimeMonth,
    to_number(to_char(a.dt_saida_unidade, 'dd')) OutTimeDayOfMonth,
    obter_valor_dominio(12, a.ie_tipo_atendimento) AdmitType,
    substr(obter_motivo_alta_setor(a.cd_motivo_alta_setor),1,255)dischargeDisposition,
    substr(obter_desc_setor_atend(a.cd_setor_atendimento),1,255) dischargeLocation,
    obter_dias_entre_datas(a.dt_entrada_unidade,nvl(a.dt_saida_unidade,sysdate)) +1  lenghtOfStay,
    (select
       case 
         when max(ap1.dt_entrada) < (a.dt_entrada_unidade-1) then 
           0
         else 
           1
       end
     from atendimento_paciente ap1
     where ap1.cd_pessoa_fisica = b.cd_pessoa_fisica and
           ap1.dt_entrada < a.dt_entrada_unidade
    ) is24HrReAdmit,
    (select
       case 
         when trunc(max(ap1.dt_alta)) < (trunc(a.dt_entrada_unidade-30)) then 
           0
         else 
           1
       end
     from atendimento_paciente ap1
     where ap1.cd_pessoa_fisica = b.cd_pessoa_fisica and
           dt_alta < a.dt_entrada_unidade
    ) hasOpenChart,
    decode(a.dt_saida_unidade,null,'0','1') isDischarged,
    obter_desc_queixa(a.nr_seq_queixa) admitSource,
    obter_nome_estabelecimento(a.CD_SETOR_ATENDIMENTO)institutionDisplayLabel,
    substr(obter_desc_setor_atend(a.CD_SETOR_ATENDIMENTO),1,255) clinicalUnitLabel,
    obter_valor_dominio(12, a.ie_tipo_atendimento) clinicalUnitType,
    substr(obter_desc_setor_atend(a.CD_SETOR_ATENDIMENTO),1,255) transferClinicalUnitLabel,
    obter_valor_dominio(12, a.ie_tipo_atendimento) transferClinicalUnittype,
    nvl(
        (select x.temp_encounterid_ext
         from   pf_codigo_externo x
         where  x.cd_pessoa_fisica = a.cd_pessoa_fisica and rownum = 1),a.nr_atendimento )encounterId,
        a.CD_SETOR_ATENDIMENTO clinicalUnitId,
    a.nr_sequencia ptCensusId,
    a.CD_ESTABELECIMENTO_ATEND systemId,
	a.dt_atualizacao
from dar_census a,
    pessoa_fisica b
where    a.cd_pessoa_fisica = b.cd_pessoa_fisica;
/
