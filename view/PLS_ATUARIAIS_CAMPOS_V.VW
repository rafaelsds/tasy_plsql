create or replace 
view pls_atuariais_campos_v 
as
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	a.nm_tabela,
	'1' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_BENEF_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94899
union all
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	'PLS_ATU_CONTA_ITEM' nm_tabela,
	'2' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_CONTA_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94901
union all
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	'PLS_ATU_ESTIP_ITEM' nm_tabela,
	'3' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_ESTIP_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94903
union all
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	'PLS_ATU_MENS_ITEM' nm_tabela,
	'4' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_MENS_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94905
union all
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	'PLS_ATU_PREST_ITEM' nm_tabela,
	'5' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_PREST_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94907
union all
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	'PLS_ATU_PLANO_ITEM' nm_tabela,
	'6' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_PLANO_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94909
union all
select	a.nm_atributo cd_campo,
	substr(a.nm_atributo||' ('||obter_desc_expressao(v.cd_exp_label)||')',1,255) ds_campo,
	'PLS_ATU_PROCED_ITEM' nm_tabela,
	'7' ie_tipo_arquivo
from	tabela_atributo		a,
	tabela_visao_atributo	v
where	a.nm_atributo	= v.nm_atributo
and	a.nm_tabela	= 'PLS_ATU_PROCED_ITEM'
and	v.cd_exp_label is not null
and	v.nr_sequencia	= 94911;
/