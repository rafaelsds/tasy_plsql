create or replace 
	view hs_sac_boletim_ocorrencia_v as
select	b.nr_sequencia,
	b.dt_ocorrencia,
	b.cd_interno,
	b.nm_usuario,
	b.dt_envio,
	a.nr_seq_responsavel,
	c.ds_responsavel,
	b.cd_pessoa_fisica,
	substr(obter_nome_pf_pj(b.cd_pessoa_fisica,null),1,255) nm_pessoa_fisica,
	b.nr_seq_forma_reg,
	d.ds_forma_registro,
	b.ie_origem,
	a.nr_seq_classif,
	e.ds_classificacao,
	a.nr_seq_gravidade,
	f.ds_gravidade,
	a.nr_seq_tipo,
	g.ds_tipo_ocorrencia,
	a.nr_sequencia nr_seq_resposta,
	b.nm_usuario_nrec
from	sac_tipo_ocorrencia g,
	sac_gravidade f,
	sac_classif_ocorrencia e,
	sac_forma_registro d,
	sac_responsavel c,
	sac_resp_bol_ocor a,
	sac_boletim_ocorrencia b
where	b.nr_sequencia		= a.nr_seq_bo
and	a.nr_seq_responsavel	= c.nr_sequencia(+)
and	b.nr_seq_forma_reg	= d.nr_sequencia(+)
and	a.nr_seq_classif		= e.nr_sequencia(+)
and	a.nr_seq_gravidade	= f.nr_sequencia(+)
and	a.nr_seq_tipo		= g.nr_sequencia(+)
/
