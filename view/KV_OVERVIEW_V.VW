CREATE OR REPLACE VIEW KV_OVERVIEW_V AS
--Muster 1
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '01' ie_tipo,
        obter_desc_expressao(863048) ds_tipo_registro,
        obter_nome_pf(cd_medico) nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        atestado_paciente
    WHERE
        dt_inativacao IS NULL

--Muster 4
    union ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '04' ie_tipo,
        obter_desc_expressao(863050) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_transporte
    WHERE
        dt_inativacao IS NULL

--Muster 6
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '06' ie_tipo,
        obter_desc_expressao(863046) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        carta_referencia
    WHERE
        dt_inativacao is NULL

--Muster 10
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '10' ie_tipo,
        obter_desc_expressao(863047) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    from
        formulario_exame_lab
    WHERE
        dt_inativacao IS NULL

--Muster 12
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '12' ie_tipo,
        obter_desc_expressao(863051) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_home_care
    WHERE
        dt_inativacao IS NULL

--Muster 13
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '13' ie_tipo,
        obter_desc_expressao(863049) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_terapia
    WHERE
        ie_muster = 13
        AND dt_inativacao IS NULL

--Muster 14
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '14' ie_tipo,
        obter_desc_expressao(863044) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_terapia
    WHERE
        ie_muster = 14
        and dt_inativacao IS NULL

--Muster 15
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '15' ie_tipo,
        obter_desc_expressao(883284) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_audicao
    WHERE
        dt_inativacao IS NULL

--Muster 16
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '16' ie_tipo,
        obter_desc_expressao(863045) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        kv_receita_farmacia
    WHERE
        dt_inativacao IS NULL

--Muster 18
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '18' ie_tipo,
        obter_desc_expressao(863043) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_terapia
    WHERE
        ie_muster = 18
        AND dt_inativacao IS NULL

--Muster 21
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '21' ie_tipo,
        obter_desc_expressao(942948) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        kv_atestado_crianca
    WHERE
        ie_muster = 21
        AND dt_inativacao IS NULL

--Muster 26
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '26' ie_tipo,
        obter_desc_expressao(883390) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    from
        formulario_socioterapia
    WHERE
        ie_muster = 26
        AND dt_inativacao IS NULL

--Muster 27
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '27' ie_tipo,
        obter_desc_expressao(883413) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_socioterapia
    WHERE
        ie_muster = 27
        AND dt_inativacao IS NULL

--Muster 28
    UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '28' ie_tipo,
        obter_desc_expressao(883420) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        cd_pessoa_fisica
    FROM
        formulario_socioterapia
    WHERE
        ie_muster = 28
        AND dt_inativacao IS NULL

--Inpatient medical treatment plan 
	UNION ALL
    SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '40' ie_tipo,
        obter_desc_expressao(1031672) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        obter_pessoa_atendimento(nr_atendimento, 'C')  cd_pessoa_fisica
    FROM
        med_treatment_plan
    WHERE
        si_type_form = '1'
        AND dt_inativacao IS NULL
        
--Inpatient medical treatment plan (medical treatment for the elderly)
    UNION ALL
	SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '42' ie_tipo,
        obter_desc_expressao(1031693) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        obter_pessoa_atendimento(nr_atendimento, 'C')  cd_pessoa_fisica
    FROM
        med_treatment_plan
    WHERE
        si_type_form = '2'
        AND dt_inativacao IS NULL

--Inpatient medical treatment plan (when including the admission period for medical protection admission)
    UNION ALL
	SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '44' ie_tipo,
        obter_desc_expressao(1031695) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        obter_pessoa_atendimento(nr_atendimento, 'C')  cd_pessoa_fisica
    FROM
        med_treatment_plan
    WHERE
        si_type_form = '3'
        AND dt_inativacao IS NULL
	
--Community-based ward inpatient medical treatment plan (items related to home recovery support)
    UNION ALL
	SELECT
        dt_atualizacao_nrec,
        nr_sequencia,
        '45' ie_tipo,
        obter_desc_expressao(1031697) ds_tipo_registro,
        obter_dados_usuario_opcao(nm_usuario, 'NP') nm_profissional,
        dt_liberacao,
        nr_atendimento,
        obter_pessoa_atendimento(nr_atendimento, 'C')  cd_pessoa_fisica
    FROM
        med_treatment_plan
    WHERE
        si_type_form = '4'
        AND dt_inativacao IS NULL
    ORDER BY
        dt_atualizacao_nrec DESC;
/

