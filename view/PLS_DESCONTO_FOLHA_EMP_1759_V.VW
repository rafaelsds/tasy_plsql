create or replace view pls_desconto_folha_emp_1759_v
as
select	1				TP_REGISTRO,
	c.dt_remessa_retorno		DT_COMPETENCIA,
	null				CD_MATRICULA,
	null				CD_MATRICULA_REPARTICAO,
	null				CD_MATRICULA_6,
	null				CD_MATRICULA_8,
	null				CD_MATRICULA_10,
	null				CD_MATRICULA_12,
	null				CD_MATRICULA_13,
	null				VL_PAGO,
	null				VL_PENSIONISTA,
	null				VL_PAGO_POSITIVO,
	null				VL_PAGO_NEGATIVO,
	a.nr_seq_cobranca		NR_SEQ_COBRANCA,
	null				VL_TOT_PAGO,
	obter_Valor_sem_virgula(sum(b.vl_mensalidade))	VL_TOT_TITULOS,
	null				CD_CODIGO,
	null				CD_MATRICULA_REPARTICAO2,
	obter_Valor_sem_virgula(sum(b.vl_mensalidade))	VL_PAGO2,
	'mensalidade'			ESPECIE,
	null				CD_FUNCAO,
	null				IE_IDENTIFICADOR_6,
	null				VL_INCLUIR,
	null				VL_EXCLUIR,
	null				NR_PENSIONISTA,
	null				NR_SEQ_PAGADOR
from	w_pls_desconto_folha		a, 
	pls_mensalidade			b,
	cobranca_escritural		c
where	a.nr_seq_mensalidade		= b.nr_sequencia
and	a.nr_seq_cobranca 		= c.nr_sequencia
group by	c.dt_remessa_retorno,
		a.nr_seq_cobranca
union
select	2				TP_REGISTRO,
	d.dt_remessa_retorno		DT_COMPETENCIA,
	c.cd_matricula			CD_MATRICULA,
	c.cd_matricula			CD_MATRICULA_REPARTICAO,
	substr(c.cd_matricula,1,6)	CD_MATRICULA_6,
	substr(c.cd_matricula,1,8)	CD_MATRICULA_8,
	substr(c.cd_matricula,1,10)	CD_MATRICULA_10,
	substr(c.cd_matricula,1,12)	CD_MATRICULA_12,
	substr(c.cd_matricula,1,13)	CD_MATRICULA_13,
	null				VL_PAGO,
	null				VL_PENSIONISTA,
	null				VL_PAGO_POSITIVO,
	null				VL_PAGO_NEGATIVO,
	a.nr_seq_cobranca		NR_SEQ_COBRANCA,
	null				VL_TOT_PAGO,
	null				VL_TOT_TITULOS,
	pls_obter_codigo_corsan(c.cd_matricula) CD_CODIGO,
	null				CD_MATRICULA_REPARTICAO2,
	null				VL_PAGO2,
	'mensalidade'			ESPECIE,
	c.cd_profissao			CD_FUNCAO,
	null				IE_IDENTIFICADOR_6,
	obter_Valor_sem_virgula(a.vl_incluir)	VL_INCLUIR,
	obter_Valor_sem_virgula(a.vl_excluir)	VL_EXCLUIR,
	nvl(substr(c.nr_pensionista,1,2),00)	NR_PENSIONISTA,
	c.nr_seq_pagador			NR_SEQ_PAGADOR
from	w_pls_desconto_folha		a, 
	pls_mensalidade			b,
	pls_contrato_pagador_fin	c,
	cobranca_escritural		d
where	a.nr_seq_mensalidade	= b.nr_sequencia
and	b.nr_seq_pagador	= c.nr_seq_pagador
and	c.cd_matricula		= a.cd_matricula
and	a.nr_seq_cobranca 	= d.nr_sequencia;
/