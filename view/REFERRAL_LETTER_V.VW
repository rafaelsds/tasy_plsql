create or replace view REFERRAL_LETTER_V as
select	1 ie_type_admission, /*Inpatient*/
	1 ie_type_record, /*Referral letter from another hospital*/
	wheb_mensagem_pck.get_Texto(1163078) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,1,'SEQ'),1) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	a.nm_pessoa_fisica nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,1,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,1,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,1,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,1,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,1,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,1,'SEQ'),1,255) nr_Document,
	b.nr_atendimento	
FROM	setor_atendimento s,
	pep_ocupacao_unidade_v a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	a.nr_atendimento = b.nr_atendimento 
AND	a.nr_atendimento is not null
AND	b.ie_tipo_atendimento in (1,3,10,12,14)
and	get_data_external_refferal(b.nr_atendimento,1,'SEQ') > 0
union all
SELECT	8 ie_type_admission, /*Outpatient*/
	1 ie_type_record, /*Referral letter from another hospital*/
	wheb_mensagem_pck.get_Texto(1163078) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,1,'SEQ'),1) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,1,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,1,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,1,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,1,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,1,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,1,'SEQ'),1,255) nr_Document,
	b.nr_atendimento
FROM	setor_atendimento s,
	atend_paciente_unidade a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	b.nr_atendimento = a.nr_atendimento 
AND	b.dt_alta is null
AND	b.ie_tipo_atendimento in (8,7,11)
AND	a.dt_saida_unidade is null
AND	b.dt_entrada >= (sysdate - '2')
AND	nvl(s.ie_situacao,'A') = 'A'
and	get_data_external_refferal(b.nr_atendimento,1,'SEQ') > 0
union all
select	1 ie_type_admission, /*Inpatient*/
	2 ie_type_record, /*Report of the referral letter to another hospital*/
	wheb_mensagem_pck.get_Texto(1163104) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,2,'SEQ'),2) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	a.nm_pessoa_fisica nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,2,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,2,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,2,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,2,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,2,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,2,'SEQ'),1,255) nr_Document,
	b.nr_atendimento	
FROM	setor_atendimento s,
	pep_ocupacao_unidade_v a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	a.nr_atendimento = b.nr_atendimento 
AND	a.nr_atendimento is not null
AND	b.ie_tipo_atendimento in (1,3,10,12,14)
and	get_data_external_refferal(b.nr_atendimento,2,'SEQ') > 0
union all
SELECT	8 ie_type_admission, /*Outpatient*/
	2 ie_type_record, /*Report of the referral letter to another hospital*/
	wheb_mensagem_pck.get_Texto(1163104) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,2,'SEQ'),2) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,2,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,2,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,2,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,2,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,2,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,2,'SEQ'),1,255) nr_Document,
	b.nr_atendimento
FROM	setor_atendimento s,
	atend_paciente_unidade a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	b.nr_atendimento = a.nr_atendimento 
AND	b.dt_alta is null
AND	b.ie_tipo_atendimento in (8,7,11)
AND	a.dt_saida_unidade is null
AND	b.dt_entrada >= (sysdate - '2')
AND	nvl(s.ie_situacao,'A') = 'A'
and	get_data_external_refferal(b.nr_atendimento,2,'SEQ') > 0
union all
select	1 ie_type_admission, /*Inpatient*/
	3 ie_type_record, /*Referral letter that is sent to another hospital*/
	wheb_mensagem_pck.get_Texto(1163106) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,3,'SEQ'),3) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	a.nm_pessoa_fisica nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,3,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,3,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,3,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,3,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,3,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,3,'SEQ'),1,255) nr_Document,
	b.nr_atendimento	
FROM	setor_atendimento s,
	pep_ocupacao_unidade_v a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	a.nr_atendimento = b.nr_atendimento 
AND	a.nr_atendimento is not null
AND	b.ie_tipo_atendimento in (1,3,10,12,14)
and	get_data_external_refferal(b.nr_atendimento,3,'SEQ') > 0
union all
SELECT	8 ie_type_admission, /*Outpatient*/
	3 ie_type_record, /*Referral letter that is sent to another hospital*/
	wheb_mensagem_pck.get_Texto(1163106) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,3,'SEQ'),3) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,3,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,3,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,3,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,3,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,3,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,3,'SEQ'),1,255) nr_Document,
	b.nr_atendimento
FROM	setor_atendimento s,
	atend_paciente_unidade a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	b.nr_atendimento = a.nr_atendimento 
AND	b.dt_alta is null
AND	b.ie_tipo_atendimento in (8,7,11)
AND	a.dt_saida_unidade is null
AND	b.dt_entrada >= (sysdate - '2')
AND	nvl(s.ie_situacao,'A') = 'A'
and	get_data_external_refferal(b.nr_atendimento,3,'SEQ') > 0
union all
select	1 ie_type_admission, /*Inpatient*/
	4 ie_type_record, /*Report of the referral letter from another hospital*/
	wheb_mensagem_pck.get_Texto(1163107) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,4,'SEQ'),4) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	a.nm_pessoa_fisica nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,4,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,4,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,4,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,4,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,4,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,4,'SEQ'),1,255) nr_Document,
	b.nr_atendimento	
FROM	setor_atendimento s,
	pep_ocupacao_unidade_v a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	a.nr_atendimento = b.nr_atendimento 
AND	a.nr_atendimento is not null
AND	b.ie_tipo_atendimento in (1,3,10,12,14)
and	get_data_external_refferal(b.nr_atendimento,4,'SEQ') > 0
union all
SELECT	8 ie_type_admission, /*Outpatient*/
	4 ie_type_record, /*Report of the referral letter from another hospital*/
	wheb_mensagem_pck.get_Texto(1163107) ds_type_record,
	b.dt_entrada dt_hospitalization,
	get_dates_external_refferal(get_data_external_refferal(b.nr_atendimento,4,'SEQ'),4) dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	substr(get_data_external_refferal(b.nr_atendimento,4,'DSD'),1,255) ds_Medical_department_doc,
	substr(get_data_external_refferal(b.nr_atendimento,4,'DSW'),1,255) ds_Ward_Doc,
	substr(get_data_external_refferal(b.nr_atendimento,4,'NMAD'),1,255) nm_Attending_doctor,
	substr(get_data_external_refferal(b.nr_atendimento,4,'IEST'),1,255) ie_Status,
	substr(get_data_external_refferal(b.nr_atendimento,4,'DSST'),1,255) ds_Status,
	substr(get_data_external_refferal(b.nr_atendimento,4,'SEQ'),1,255) nr_Document,
	b.nr_atendimento
FROM	setor_atendimento s,
	atend_paciente_unidade a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	b.nr_atendimento = a.nr_atendimento 
AND	b.dt_alta is null
AND	b.ie_tipo_atendimento in (8,7,11)
AND	a.dt_saida_unidade is null
AND	b.dt_entrada >= (sysdate - '2')
AND	nvl(s.ie_situacao,'A') = 'A'
and	get_data_external_refferal(b.nr_atendimento,4,'SEQ') > 0
union all
/*Without Documents*/
select	1 ie_type_admission, /*Inpatient*/
	null ie_type_record,
	null ds_type_record,
	b.dt_entrada dt_hospitalization,
	null dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	a.nm_pessoa_fisica nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	null ds_Medical_department_doc,
	null ds_Ward_Doc,
	null nm_Attending_doctor,
	'N' ie_Status,
	wheb_mensagem_pck.get_Texto(1147367) ds_Status, /*Not created*/
	null nr_Document,
	b.nr_atendimento	
FROM	setor_atendimento s,
	pep_ocupacao_unidade_v a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	a.nr_atendimento = b.nr_atendimento 
AND	a.nr_atendimento is not null
AND	b.ie_tipo_atendimento in (1,3,10,12,14)
and	nvl(get_data_external_refferal(b.nr_atendimento,1,'SEQ'),0) = 0
and	nvl(get_data_external_refferal(b.nr_atendimento,2,'SEQ'),0) = 0
and	nvl(get_data_external_refferal(b.nr_atendimento,3,'SEQ'),0) = 0
and	nvl(get_data_external_refferal(b.nr_atendimento,4,'SEQ'),0) = 0
union all
SELECT	8 ie_type_admission, /*Outpatient*/
	null ie_type_record,
	null ds_type_record,
	b.dt_entrada dt_hospitalization,
	null dt_creation,
	obter_departamento_atual_atend(a.nr_atendimento) cd_Current_medical_department,
	SUBSTR(OBTER_NOME_DEPARTAMENTO_MEDICO(obter_departamento_atual_atend(a.nr_atendimento)),1,255) ds_Current_medical_department,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_Current_ward,
	SUBSTR(OBTER_NOME_SETOR(Obter_Setor_Atendimento(b.nr_atendimento)),1,255) ds_Current_ward,
	b.cd_pessoa_fisica cd_Patient_Id,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_Patient_name,
	Obter_Sexo_PF(b.cd_pessoa_fisica,'D') ie_Sex,
	obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_age,	
	obter_valor_dominio(12,obter_tipo_atendimento(a.nr_atendimento)) ds_type_admission,
	null ds_Medical_department_doc,
	null ds_Ward_Doc,
	null nm_Attending_doctor,
	'N' ie_Status,
	wheb_mensagem_pck.get_Texto(1147367) ds_Status, /*Not created */
	null nr_Document,
	b.nr_atendimento
FROM	setor_atendimento s,
	atend_paciente_unidade a,
	atendimento_paciente b
WHERE	a.cd_setor_atendimento = s.cd_setor_atendimento 
AND	b.nr_atendimento = a.nr_atendimento 
AND	b.dt_alta is null
AND	b.ie_tipo_atendimento in (8,7,11)
AND	a.dt_saida_unidade is null
AND	b.dt_entrada >= (sysdate - '2')
AND	nvl(s.ie_situacao,'A') = 'A'
and	nvl(get_data_external_refferal(b.nr_atendimento,1,'SEQ'),0) = 0
and	nvl(get_data_external_refferal(b.nr_atendimento,2,'SEQ'),0) = 0
and	nvl(get_data_external_refferal(b.nr_atendimento,3,'SEQ'),0) = 0
and	nvl(get_data_external_refferal(b.nr_atendimento,4,'SEQ'),0) = 0
order by ie_type_admission,ds_Current_medical_department,cd_Current_ward,nm_Patient_name;
/


