create or replace view
icesp_dados_entreda_oci_v as
select	a.nr_ordem_compra,
	a.dt_ordem_compra,
	a.cd_estabelecimento,
	a.dt_liberacao,
	a.dt_aprovacao,
	substr(obter_cgc_cpf_editado(a.cd_cgc_fornecedor),1,20) cd_cgc_fornecedor,
	a.cd_cgc_fornecedor cd_cgc_par,
	null cd_pessoa_par,
	d.ds_razao_social,
	substr(decode(d.nr_ddd_telefone,null,null,'(' || d.nr_ddd_telefone || ') ')|| d.nr_telefone,1,50) nr_telefone,
	to_date(c.dt_prevista_entrega) dt_entrega,
	b.cd_local_estoque,
	substr(obter_desc_local_estoque(a.cd_local_entrega),1,80) ds_local_estoque,
	e.cd_grupo_material,
	b.cd_material,
	e.ds_material,
	b.nr_item_oci,
	b.cd_unidade_medida_compra,
	c.qt_prevista_entrega,
	nvl(c.qt_real_entrega,0) qt_real_entrega,
	to_number(icesp_obter_dados_entrega_oci(c.nr_sequencia,'VU')) vl_unitario_material,
	(to_number(icesp_obter_dados_entrega_oci(c.nr_sequencia,'VU')) * c.qt_real_entrega) vl_total_material,
	to_date(c.dt_real_entrega) dt_real_entrega,
	decode(nvl(c.qt_real_entrega,0),0,'Pendente - (Re)Programar Entrega','Recebido') ds_entrega,
	substr(decode(obter_ordem_pendente_entrega(a.nr_ordem_compra,b.nr_item_oci),1,'Atrasado',0,'Entregue',3,'Parcialmente Entregue'),1,30) ie_status,
	substr(decode(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NF'),null,null,(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NF') || ' - ' || icesp_obter_dados_entrega_oci(c.nr_sequencia,'DNF'))),1,100) ds_nota_fiscal,
	substr(decode(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NRI'),null,null,(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NRI') || ' - ' || icesp_obter_dados_entrega_oci(c.nr_sequencia,'DRI'))),1,100) nr_recebimento,
	c.dt_cancelamento
from 	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c,
	pessoa_juridica d,
	estrutura_material_v e
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci
and	a.cd_cgc_fornecedor = d.cd_cgc
and	b.cd_material = e.cd_material
and	a.nr_seq_motivo_cancel is null
and	a.cd_cgc_fornecedor is not null
union all
select	a.nr_ordem_compra,
	a.dt_ordem_compra,
	a.cd_estabelecimento,
	a.dt_liberacao,
	a.dt_aprovacao,
	substr(obter_cgc_cpf_editado(d.nr_cpf),1,20) cd_cgc_fornecedor,
	null cd_cgc_par,
	a.cd_pessoa_fisica cd_pessoa_par,
	d.nm_pessoa_fisica ds_razao_social,
	substr(decode(obter_compl_pf(d.cd_pessoa_fisica,2,'DDT'),null,null,'('||obter_compl_pf(d.cd_pessoa_fisica,2,'DDT')||') ') || substr(obter_compl_pf(d.cd_pessoa_fisica,2,'T'),1,15),1,50) nr_telefone,
	to_date(c.dt_prevista_entrega) dt_entrega,
	b.cd_local_estoque,
	substr(obter_desc_local_estoque(a.cd_local_entrega),1,80) ds_local_estoque,
	e.cd_grupo_material,
	b.cd_material,
	e.ds_material,
	b.nr_item_oci,
	b.cd_unidade_medida_compra,
	c.qt_prevista_entrega,
	nvl(c.qt_real_entrega,0) qt_real_entrega,
	to_number(icesp_obter_dados_entrega_oci(c.nr_sequencia,'VU')) vl_unitario_material,
	(to_number(icesp_obter_dados_entrega_oci(c.nr_sequencia,'VU')) * c.qt_real_entrega) vl_total_material,
	to_date(c.dt_real_entrega) dt_real_entrega,
	decode(nvl(c.qt_real_entrega,0),0,'Pendente - (Re)Programar Entrega','Recebido') ds_entrega,
	substr(decode(obter_ordem_pendente_entrega(a.nr_ordem_compra,b.nr_item_oci),1,'Atrasado',0,'Entregue',3,'Parcialmente Entregue'),1,30) ie_status,
	substr(decode(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NF'),null,null,(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NF') || ' - ' || icesp_obter_dados_entrega_oci(c.nr_sequencia,'DNF'))),1,100) ds_nota_fiscal,
	substr(decode(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NRI'),null,null,(icesp_obter_dados_entrega_oci(c.nr_sequencia,'NRI') || ' - ' || icesp_obter_dados_entrega_oci(c.nr_sequencia,'DRI'))),1,100) nr_recebimento,
	c.dt_cancelamento
from 	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c,
	pessoa_fisica d,
	estrutura_material_v e
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci
and	a.cd_pessoa_fisica = d.cd_pessoa_fisica
and	b.cd_material = e.cd_material
and	a.nr_seq_motivo_cancel is null
and	a.cd_cgc_fornecedor is null;
/