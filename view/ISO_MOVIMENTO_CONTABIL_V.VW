create or replace view iso_movimento_contabil_v
as
SELECT		a.nr_lote_contabil,
		lpad(a.nr_sequencia, 7, 0) nr_sequencia,
		to_char(a.dt_movimento, 'dd/mm') dt_movimento,
		lpad(a.cd_conta_debito, 7, 0) cd_conta_debito,
		lpad(a.cd_conta_credito, 7, 0) cd_conta_credito,
		lpad(a.vl_movimento, 17, 0) vl_movimento,
		lpad(a.cd_historico, 5, 0) cd_historico,
		lpad(a.ds_compl_historico, 200, ' ') ds_compl_historico,
		lpad(b.cd_centro_custo, 42, 0) cd_centro_custo_credito,
		lpad(0, 42, 0) cd_centro_custo_debito
FROM	CTB_MOVIMENTO a,
		CTB_MOVTO_CENTRO_CUSTO b
WHERE	a.nr_sequencia = b.nr_seq_movimento(+)
AND		a.cd_conta_credito IS NOT NULL
AND		a.cd_conta_debito IS NULL
UNION
SELECT		a.nr_lote_contabil,
		lpad(a.nr_sequencia, 7, 0) nr_sequencia,
		to_char(a.dt_movimento, 'dd/mm') dt_movimento,
		lpad(a.cd_conta_debito, 7, 0) cd_conta_debito,
		lpad(a.cd_conta_credito, 7, 0) cd_conta_credito,
		lpad(a.vl_movimento, 17, 0) vl_movimento,
		lpad(a.cd_historico, 5, 0) cd_historico,
		lpad(a.ds_compl_historico, 200, ' ') ds_compl_historico,
		lpad(0, 42, 0) cd_centro_custo_credito,
		lpad(b.cd_centro_custo, 42, 0) cd_centro_custo_debito
FROM	CTB_MOVIMENTO a,
		CTB_MOVTO_CENTRO_CUSTO b
WHERE	a.nr_sequencia = b.nr_seq_movimento(+)
AND		a.cd_conta_credito IS NULL
AND		a.cd_conta_debito IS NOT NULL
UNION
SELECT		a.nr_lote_contabil,
		lpad(a.nr_sequencia, 7, 0) nr_sequencia,
		to_char(a.dt_movimento, 'dd/mm') dt_movimento,
		lpad(a.cd_conta_debito, 7, 0) cd_conta_debito,
		lpad(a.cd_conta_credito, 7, 0) cd_conta_credito,
		lpad(a.vl_movimento, 17, 0) vl_movimento,
		lpad(a.cd_historico, 5, 0) cd_historico,
		lpad(a.ds_compl_historico, 200, ' ') ds_compl_historico,
		lpad(b.cd_centro_custo, 42, 0) cd_centro_custo_credito,
		lpad(b.cd_centro_custo, 42, 0) cd_centro_custo_debito
FROM	CTB_MOVIMENTO a,
		CTB_MOVTO_CENTRO_CUSTO b
WHERE	a.nr_sequencia = b.nr_seq_movimento(+)
AND		a.cd_conta_credito IS NOT NULL
AND		a.cd_conta_debito IS NOT NULL
/
