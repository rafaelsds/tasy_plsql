CREATE OR REPLACE 
VIEW TASY_MOSAIQ_v AS 
select	Nota_ID,
	Nota_Tipo,
	Paciente_PACS,
	Paciente_RGHC,
	Paciente_Nome,
	Paciente_Sobrenome,
	Paciente_Nome||' '||Paciente_Sobrenome Nome_Completo,
	Paciente_Nascimento,
	Paciente_Sexo,
	Nota_Tipo_Descricao,
	Nota_Conteudo,
	Nota_Data_Criacao,
	--FIRST_NAME || ' ' || LAST_NAME 'nome_medico' nome_medico
	Nota_Criada_Por nome_medico
from	consulta_mosaiq
/