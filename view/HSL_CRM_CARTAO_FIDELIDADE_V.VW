CREATE OR REPLACE VIEW HSL_CRM_CARTAO_FIDELIDADE_V
as
    select 
        a.cd_pessoa_fisica,
        b.nr_seq_cartao,   
        substr(Obter_desc_cartao_fidelidade(b.NR_SEQ_CARTAO),1,60) as ds_cartao_fidelidade
    from pf_cartao_fidelidade b,
         pessoa_fisica a    
    where a.cd_pessoa_fisica = b.cd_pessoa_fisica;
/