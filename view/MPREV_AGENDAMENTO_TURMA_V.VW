Create or Replace view mprev_agendamento_turma_v
As
select	a.nr_sequencia,
	a.dt_agenda,
	a.ie_status_agenda,
	a.dt_cancelamento,
	a.dt_confirmacao,
	a.ie_forma_atendimento,
	a.ie_tipo_atendimento,
	a.ie_profissional_espec,
	a.ds_observacao,
	a.nr_minuto_duracao,
	a.nr_seq_horario_turma,
	a.nr_seq_turma,
	a.nr_seq_motivo_canc,
	b.nr_seq_grupo_coletivo,
	d.nm_grupo,
	b.nm_turma,
	b.qt_max_participante,
	b.dt_inicio dt_inicio_turma,
	b.dt_termino dt_termino_turma,
	c.nr_seq_participante,
	c.dt_entrada dt_entrada_partic,
	c.dt_saida dt_saida_partic,
	e.ie_dia_semana ie_dia_horario,
	e.dt_inicio dt_inicio_horario,
	e.hr_inicio hr_inicio_horario,
	e.hr_termino hr_final_horario,
	e.qt_repeticao qt_repet_horario,
	e.dt_fim_repeticao dt_fim_horario
from 	mprev_grupo_col_turma_hor e,
	mprev_grupo_coletivo d,
	mprev_grupo_turma_partic c,
	mprev_grupo_col_turma b,
	mprev_agendamento a
where 	a.nr_seq_turma	= b.nr_sequencia
and	b.nr_sequencia	= c.nr_seq_turma
and	b.nr_seq_grupo_coletivo = d.nr_sequencia
and	a.nr_seq_horario_turma = e.nr_sequencia(+);
/