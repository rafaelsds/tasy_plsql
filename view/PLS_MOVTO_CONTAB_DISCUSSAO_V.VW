create or replace view pls_movto_contab_discussao_v
as
select	a.ie_status,
	a.nr_seq_disc_proc,
	a.nr_seq_disc_mat,
	b.cd_conta_cred,
	b.cd_conta_deb,
	b.cd_classif_cred,
	b.cd_classif_deb,
	b.nr_seq_esquema,
	a.nr_sequencia,
	a.nr_seq_item_mensalidade,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	a.nr_seq_atualizacao,
	null nr_seq_conta_proc,
	null nr_seq_conta_mat
from	pls_movimento_contabil	a,
	pls_discussao_proc	b
where	b.nr_sequencia	= a.nr_seq_disc_proc
union all
select	a.ie_status,
	a.nr_seq_disc_proc,
	a.nr_seq_disc_mat,
	b.cd_conta_cred,
	b.cd_conta_deb,
	b.cd_classif_cred,
	b.cd_classif_deb,
	b.nr_seq_esquema,
	a.nr_sequencia,
	a.nr_seq_item_mensalidade,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	a.nr_seq_atualizacao,
	null nr_seq_conta_proc,
	null nr_seq_conta_mat
from	pls_movimento_contabil	a,
	pls_discussao_mat	b
where	b.nr_sequencia	= a.nr_seq_disc_mat;
/