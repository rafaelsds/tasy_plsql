create or replace
view eis_lab_tempos_etapa_dia_v as
select 	0 cd_setor_atendimento, /* Mapa traballho */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '15', 'D'),'dd/mm/yyyy hh24:mi:ss') - nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,15,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao))) min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	count(*) qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 15)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select  0 cd_setor_atendimento, /* Coleta do Material */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	((nvl(TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '20', 'D'),'dd/mm/yyyy hh24:mi:ss'),a.dt_coleta) 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,20,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	count(*) qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	((exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 20)) or (a.dt_coleta is not null))
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by  c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao, a.dt_coleta
union all
select 0 cd_setor_atendimento, /* Distribui��o do Material */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '25', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,25,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	count(*) qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 25)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by  c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Digita��o */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,30,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	count(*) qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by 	 c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /*  Aprova��o */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,35,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	count(*) qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and	a.nr_seq_exame is not null
group by  c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /*  Libera��o at� a Coleta */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	((a.dt_coleta - b.dt_liberacao) ) min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	count(*) qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	a.dt_coleta is not null
and  	a.nr_seq_exame is not null
group by  c.nr_seq_grupo, a.dt_coleta,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /* Libera��o at� Digita��o */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	count(*) qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and  	a.nr_seq_exame is not null
group by  c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Libera��o at� Aprova��o */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_8,
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	count(*) qt8,
	0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Libera��o at� Libera��o do exame */
	1 ie_tipo,
	c.nr_seq_grupo,
	substr(OBTER_DESC_GRUPO_EXAME_LAB(c.nr_seq_grupo),1,60) ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '40', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8,
	count(*) qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 40)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by c.nr_seq_grupo,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select b.cd_setor_atendimento, /* Mapa traballho */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '15', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,15,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	count(*) qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 15)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /* Coleta do Material */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	((nvl(TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '20', 'D'),'dd/mm/yyyy hh24:mi:ss'),a.dt_coleta) 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,20,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	count(*) qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	((exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 20)) or (a.dt_coleta is not null))
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao, a.dt_coleta
union all
select 	b.cd_setor_atendimento, /* Distribui��o do Material */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '25', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,25,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	count(*) qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 25)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /* Digita��o */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,30,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	count(*) qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /*  Aprova��o */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,35,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	count(*) qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /*  Libera��o at� a Coleta */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	((a.dt_coleta - b.dt_liberacao) ) min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	count(*) qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	a.dt_coleta is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,a.dt_coleta,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /* Libera��o at� Digita��o */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	count(*) qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /* Libera��o at� Aprova��o */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_8,
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	count(*) qt8,
	0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	b.cd_setor_atendimento, /* Libera��o at� Libera��o do exame */
	2 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	substr(obter_nome_setor(b.cd_setor_atendimento),1,60) ds_setor_atendimento,
	null ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '40', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8,
	count(*) qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 40)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_atendimento,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /* Mapa traballho */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '15', 'D'),'dd/mm/yyyy hh24:mi:ss') 
		- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,15,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao))) min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	count(*) qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 15)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select  0 cd_setor_atendimento, /* Coleta do Material */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	((nvl(TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '20', 'D'),'dd/mm/yyyy hh24:mi:ss'),a.dt_coleta)
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,20,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	count(*) qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	((exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 20)) or (a.dt_coleta is not null))
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by  c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao, a.dt_coleta
union all
select 0 cd_setor_atendimento, /* Distribui��o do Material */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '25', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,25,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	count(*) qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 25)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by  c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Digita��o */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,30,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	count(*) qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by 	 c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /*  Aprova��o */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,35,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	count(*) qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and	a.nr_seq_exame is not null
group by  c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /*  Libera��o at� a Coleta */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	((a.dt_coleta - b.dt_liberacao) ) min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	count(*) qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	a.dt_coleta is not null
and  	a.nr_seq_exame is not null
group by  c.nm_exame,a.dt_coleta,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /* Libera��o at� Digita��o */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	count(*) qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and  	a.nr_seq_exame is not null
group by  c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Libera��o at� Aprova��o */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_8,
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	count(*) qt8,
	0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Libera��o at� Aprova��o */
	4 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	null ds_setor_entrega,
	substr(c.nm_exame,1,30) nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '40', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8,
	count(*) qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 40)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by c.nm_exame,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /* Mapa traballho */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '15', 'D'),'dd/mm/yyyy hh24:mi:ss') 
		- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,15,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao))) min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	count(*) qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 15)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select  0 cd_setor_atendimento, /* Coleta do Material */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	((nvl(TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '20', 'D'),'dd/mm/yyyy hh24:mi:ss'),a.dt_coleta)
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,20,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	count(*) qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	((exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 20)) or (a.dt_coleta is not null))
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by  b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao, a.dt_coleta
union all
select 0 cd_setor_atendimento, /* Distribui��o do Material */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '25', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,25,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	count(*) qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 25)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by  b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Digita��o */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,30,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	count(*) qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by 	 b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /*  Aprova��o */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') 
				- nvl(TO_DATE(lab_obter_etapa_ant(a.nr_prescricao,a.nr_sequencia,35,1),'dd/mm/yyyy hh24:mi:ss'),b.dt_liberacao)) ) min_5,
	0 min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	count(*) qt5,
	0 qt6,
	0 qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and	a.nr_seq_exame is not null
group by  b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /*  Libera��o at� a Coleta */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	((a.dt_coleta - b.dt_liberacao) ) min_6,
	0 min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	count(*) qt6,
	0 qt7,
	0 qt8, 0 qt9
from   prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and 	a.dt_coleta is not null
and  	a.nr_seq_exame is not null
group by  b.cd_setor_entrega,a.dt_coleta,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 	0 cd_setor_atendimento, /* Libera��o at� Digita��o */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '30', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_7,
	0 min_8, 
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	count(*) qt7,
	0 qt8, 0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 30)
and  	a.nr_seq_exame is not null
group by  b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Libera��o at� Aprova��o */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '35', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_8,
	0 min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	count(*) qt8,
	0 qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 35)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao
union all
select 0 cd_setor_atendimento, /* Libera��o at� Aprova��o */
	3 ie_tipo,
	0 nr_seq_grupo,
	null ds_grupo,
	null ds_setor_atendimento,
	substr(obter_nome_setor(b.cd_setor_entrega),1,60) ds_setor_entrega,
	null nm_exame,
	trunc(b.dt_prescricao) dt_referencia,
	0 min_1,
	0 min_2,
	0 min_3,
	0 min_4,
	0 min_5,
	0 min_6,
	0 min_7,
	0 min_8,
	((TO_DATE(obter_lab_execucao_etapa(a.nr_prescricao, a.nr_sequencia, '40', 'D'),'dd/mm/yyyy hh24:mi:ss') - b.dt_liberacao) ) min_9,
	0 qt1,
	0 qt2,
	0 qt3,
	0 qt4,
	0 qt5,
	0 qt6,
	0 qt7,
	0 qt8,
	count(*) qt9
from   	prescr_medica b,
	exame_laboratorio c,
	prescr_procedimento a
where 	c.nr_seq_exame = a.nr_seq_exame
and  	a.nr_prescricao = b.nr_prescricao 
and exists 	(select 1 from prescr_proc_etapa d
		where d.nr_prescricao = a.nr_prescricao
		and   d.nr_seq_prescricao = a.nr_sequencia
		and   d.ie_etapa = 40)
and	b.dt_liberacao is not null
and  	a.nr_seq_exame is not null
group by b.cd_setor_entrega,
	 trunc(b.dt_prescricao),a.nr_prescricao, a.nr_sequencia, b.dt_liberacao;
/