create or replace
view Eis_Meta_Result_Setor_v as
select	a.cd_estabelecimento,
	a.cd_setor_atendimento,
	SUBSTR(obter_nome_setor(a.cd_setor_atendimento),1,100) ds_setor_atendimento,
	a.dt_referencia,
	sum(a.vl_Procedimento + a.vl_Material + a.Vl_terceiro) vl_hospital,
	b.vl_faturado vl_meta,
	(dividir((sum(a.vl_Procedimento + a.vl_Material + a.Vl_terceiro) - b.vl_faturado), b.vl_faturado) * 100) pr_percentual
from	EIS_META_RESULT_setor b,
	eis_resultado a
where	a.cd_setor_atendimento	= b.cd_setor_atendimento
and	a.dt_referencia = trunc(b.dt_referencia, 'month')
and 	somente_numero(a.ie_protocolo) >= 2
and 	somente_numero(a.ie_protocolo) <= 10
GROUP BY a.cd_estabelecimento,
	a.cd_setor_atendimento,
	SUBSTR(obter_nome_setor(a.cd_setor_atendimento),1,100),
	a.dt_referencia,
	b.vl_faturado;
/