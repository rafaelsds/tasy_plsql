create or replace view Lic_Memorial_Desc_v as 
SELECT  b.nr_seq_item,
	a.nr_sequencia,
	a.dt_emissao,
	a.dt_abertura_prev,
	b.qt_item,
	nvl(lic_obter_material_lic(m.nr_sequencia), obter_desc_material(to_number(obter_dados_lic_item('CD', b.nr_sequencia)))) || chr(13) || b.ds_adicional ds_item,
	(substr(obter_valor_dominio(1137, a.ie_modalidade),1,40) || ' N� ' || a.cd_processo) ie_modalidade,
	a.ds_declaracao,
	substr(obter_dados_lic_item('COMPL', b.nr_sequencia),1,100) ds_complemento,
	u.ds_unidade_medida
from	unidade_medida u,
	material_lic m,
	lic_item b,
	lic_licitacao a
where	b.nr_seq_licitacao 	= a.nr_sequencia
and	m.cd_material (+)	= b.cd_material
and	u.cd_unidade_medida = b.cd_unid_medida;
/
