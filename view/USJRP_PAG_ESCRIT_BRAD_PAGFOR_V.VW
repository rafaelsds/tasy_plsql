CREATE OR REPLACE VIEW USJRP_PAG_ESCRIT_BRAD_PAGFOR_V
AS -- Header - Sendo utilizado - USJRP
select 	0 tp_registro,
	a.cd_cgc nr_inscricao,
	upper(p.ds_razao_social) nm_empresa,
	e.dt_remessa_retorno dt_arquivo,
	e.dt_remessa_retorno hr_arquivo,
	'0' ie_tipo_fornecedor,
	'0' cd_cgc_fornecedor,
	'' nm_fornecedor,
	'' ds_endereco,
	'0' cd_cep,
	0 cd_banco_fornecedor,
	'0' cd_agencia_fornecedor,
	'0' ie_dig_agencia_fornec,
	'0' nr_conta_forneced,
	'0' nr_digito_conta,
	'0' nr_pagamento,
	'000' cd_carteira,
	0 nr_bradesco,
	0 nr_titulo,
	sysdate dt_vencimento,
	sysdate dt_emissao,
	sysdate dt_desconto,
	'0' nr_fator_vencimento,
	0 vl_documento,
	0 vl_pagamento,
	0 vl_desconto,
	0 vl_acrescimo,
	0 nr_documento,
	'' cd_serie_documento,
	sysdate dt_pagamento,
	0 cd_moeda,
	0 cd_area_empresa,
	'00000' cd_lancamento,
	0 ie_tipo_conta,
	0 qt_registros,
	0 vl_total_pagto,
	e.nr_remessa nr_remessa,
	'0' nr_bloqueto,
	'0' nr_livre_bloqueto,
	'0' nr_dig_bloqueto,
	'0' cd_moeda_bloqueto,
	0 cd_banco_bloqueto,
	0 vl_bloqueto,
	0 cd_modalidade,
	0 ie_tipo_movimento,
	e.nr_sequencia nr_seq_envio,
	'' ds_uso_empresa
from	banco_estabelecimento b,
	estabelecimento a,
	pessoa_juridica p,
	banco_escritural e
where	e.cd_estabelecimento 		= a.cd_estabelecimento
	and e.cd_estabelecimento	= b.cd_estabelecimento
	and a.cd_cgc 			= p.cd_cgc
	and e.cd_banco			= b.cd_banco
	and b.ie_tipo_relacao 		in ('EP', 'ECC')
union
-- Somente para bloquetos
select 	1 tp_registro,
	'0' nr_inscricao,
	'' nm_empresa,
	sysdate dt_arquivo,
	sysdate hr_arquivo,
	d.ie_tipo_favorecido ie_tipo_fornecedor,
	decode(d.ie_tipo_favorecido,1,substr(d.cd_favorecido,1,9) ||'0000'|| substr(d.cd_favorecido,10,2),d.cd_favorecido) cd_cgc_fornecedor,
	upper(d.nm_favorecido) nm_fornecedor,
	upper(d.ds_endereco) ||' , '|| d.nr_endereco ds_endereco,
	d.cd_cep,
	c.cd_banco cd_banco_fornecedor,
	decode(substr(d.nr_bloqueto,1,3), '237', 
		c.cd_agencia_bancaria, '00000') cd_agencia_fornecedor,
	decode(substr(d.nr_bloqueto,1,3), '237', 
		to_char(calcula_digito('Modulo11',c.cd_agencia_bancaria)), '0') ie_dig_agencia_fornec,
	decode(substr(d.nr_bloqueto,1,3), '237', c.nr_conta, '0000000') nr_conta_forneced,
	decode(substr(d.nr_bloqueto,1,3), '237', c.ie_digito_conta, '0') nr_digito_conta,
	rpad(decode(d.nr_documento,null,d.nr_titulo,to_number(d.nr_documento)),7,' ') || decode(w.ie_tipo_conta,'CS','13387','') nr_pagamento,
	decode(substr(d.nr_bloqueto,1,3), '237', substr(d.nr_bloqueto,24,2), '000') cd_carteira,
	decode(substr(d.nr_bloqueto,1,3), '237', to_number(substr(d.nr_bloqueto,26, 11)), 000) nr_bradesco,
	decode(substr(d.nr_bloqueto,1,3), '237',c.nr_titulo,000) nr_titulo,
	d.dt_vencimento_atual dt_vencimento,
	d.dt_emissao,
	d.dt_limite_antecipacao dt_desconto,
	substr(d.nr_bloqueto,6,4) nr_fator_vencimento,
	nvl(c.vl_escritural,0) vl_documento,
	(nvl(c.vl_escritural,0) - nvl(c.vl_desconto,0) + nvl(c.vl_acrescimo,0) + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_pagamento,
	c.vl_desconto,
	c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) vl_acrescimo,
	nvl(d.nr_documento,d.nr_titulo) nr_documento,
	'' cd_serie_documento,
	a.dt_remessa_retorno dt_pagamento,
	c.ie_moeda cd_moeda,
	0 cd_area_empresa,
	decode(w.ie_tipo_conta,'CS','00298','00000') cd_lancamento,
	0 ie_tipo_conta,
	0 qt_registros,
	0 vl_total_pagto,
	a.nr_remessa nr_remessa,
	d.nr_bloqueto nr_bloqueto,
	substr(d.nr_bloqueto,20, 25) nr_livre_bloqueto,
	substr(d.nr_bloqueto, 5, 1) nr_dig_bloqueto,
	substr(d.nr_bloqueto, 4, 1) cd_moeda_bloqueto,
	Campo_Numerico(substr(d.nr_bloqueto, 1, 3)) cd_banco_bloqueto,
	Campo_Numerico(substr(d.nr_bloqueto, 10, 10)) vl_bloqueto,
	31 cd_modalidade,
	0 ie_tipo_movimento,
	a.nr_sequencia nr_seq_envio,
	decode(w.ie_tipo_conta,'CS',substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPS')),1,35),
					substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPE')),1,35)) ds_uso_empresa
from	pessoa_fisica_conta w,
	pessoa_juridica_conta x,
	Estabelecimento e,
     	banco_estabelecimento b,
     	banco_escritural a,
	titulo_pagar_v2 d,
     	titulo_pagar_escrit c
where	c.nr_titulo          	= d.nr_titulo
and	a.nr_sequencia       	= c.nr_seq_escrit
and	a.cd_banco          	= b.cd_banco
and	a.cd_estabelecimento 	= b.cd_estabelecimento
and	a.cd_estabelecimento 	= e.cd_estabelecimento
and	b.ie_tipo_relacao	in ('EP', 'ECC')
and	c.ie_tipo_pagamento	= 'BLQ'
and	d.cd_pessoa_pf_pj	= w.cd_pessoa_fisica(+)
and	d.cd_pessoa_pf_pj	= x.cd_cgc(+)
union
-- Somente para Credito em Conta Corrente
select 	1 tp_registro,
	'0' nr_inscricao,
	'' nm_empresa,
	sysdate dt_arquivo,
	sysdate hr_arquivo,
	d.ie_tipo_favorecido ie_tipo_fornecedor,
	decode(d.ie_tipo_favorecido,1,substr(d.cd_favorecido,1,9) ||'0000'|| substr(d.cd_favorecido,10,2),d.cd_favorecido) cd_cgc_fornecedor,
	upper(d.nm_favorecido) nm_fornecedor,
	upper(d.ds_endereco) ||' , '|| d.nr_endereco ds_endereco,
	d.cd_cep,
	237 cd_banco_fornecedor,
	c.cd_agencia_bancaria cd_agencia_fornecedor,
	decode(c.ie_digito_agencia, null, to_char(calcula_digito('Modulo11',c.cd_agencia_bancaria)), c.ie_digito_agencia) ie_dig_agencia_fornec,
	c.nr_conta nr_conta_forneced,
	c.ie_digito_conta nr_digito_conta,
	rpad(decode(d.nr_documento,null,d.nr_titulo,to_number(d.nr_documento)),7,' ') || decode(w.ie_tipo_conta,'CS','13387','') nr_pagamento,
	'000' cd_carteira,
	0 nr_bradesco,
	0 nr_titulo,
	d.dt_vencimento_atual dt_vencimento,
	d.dt_emissao,
	d.dt_limite_antecipacao dt_desconto,
	'' nr_fator_vencimento,
	c.vl_escritural vl_documento,
	(c.vl_escritural - c.vl_desconto + c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_pagamento,
	c.vl_desconto,
	c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) vl_acrescimo,
	nvl(d.nr_documento,d.nr_titulo) nr_documento,
	'' cd_serie_documento,
	a.dt_remessa_retorno dt_pagamento,
	c.ie_moeda cd_moeda,
	0 cd_area_empresa,
	decode(w.ie_tipo_conta,'CS','00298','00000') cd_lancamento,
	0 ie_tipo_conta,
	0 qt_registros,
	0 vl_total_pagto,
	a.nr_remessa nr_remessa,
	'' nr_bloqueto,
	'' nr_livre_bloqueto,
	'' ie_dig_bloqueto,
	'' cd_moeda_bloqueto,
	0 cd_banco_bloqueto,
	0 vl_bloqueto,
	05 cd_modalidade,
	0 ie_tipo_movimento,
	a.nr_sequencia nr_seq_envio,
	decode(w.ie_tipo_conta,'CS',substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPS')),1,35),
					substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPE')),1,35)) ds_uso_empresa
from	pessoa_fisica_conta w,
	pessoa_juridica_conta x,
	Estabelecimento e,
     	banco_estabelecimento b,
     	banco_escritural a,
	titulo_pagar_v2 d,
     	titulo_pagar_escrit c
where	c.nr_titulo          	= d.nr_titulo
and	a.nr_sequencia       	= c.nr_seq_escrit
and	a.cd_banco          	= b.cd_banco
and	a.cd_estabelecimento 	= b.cd_estabelecimento
and	a.cd_estabelecimento 	= e.cd_estabelecimento
and	b.ie_tipo_relacao	in ('EP', 'ECC')
and	c.ie_tipo_pagamento	= 'CC'
and	d.cd_pessoa_pf_pj	= w.cd_pessoa_fisica(+)
and	d.cd_pessoa_pf_pj	= x.cd_cgc(+)
and	((x.ie_conta_pagamento = 'S' and x.ie_prestador_pls = 'S' and x.ie_situacao = 'A' and x.cd_cgc is not null)
or	(not exists (	select	1
			from	pessoa_juridica_conta o
			where	o.cd_cgc = d.cd_pessoa_pf_pj) and d.ie_tipo_favorecido = 2)
or	(w.ie_conta_pagamento = 'S' and w.ie_situacao = 'A' and w.cd_pessoa_fisica is not null)
or	(not exists (	select	1
			from	pessoa_fisica_conta u
			where	u.cd_pessoa_fisica = d.cd_pessoa_pf_pj) and d.ie_tipo_favorecido = 1))
union
-- Somente para DOC - mesmo titular
select 	1 tp_registro,
	'0' nr_inscricao,
	'' nm_empresa,
	sysdate dt_arquivo,
	sysdate hr_arquivo,
	d.ie_tipo_favorecido ie_tipo_fornecedor,
	decode(d.ie_tipo_favorecido,1,substr(d.cd_favorecido,1,9) ||'0000'|| substr(d.cd_favorecido,10,2),d.cd_favorecido) cd_cgc_fornecedor,
	upper(d.nm_favorecido) nm_fornecedor,
	upper(d.ds_endereco) ||' , '|| d.nr_endereco ds_endereco,
	d.cd_cep,
	c.cd_banco cd_banco_fornecedor,
	c.cd_agencia_bancaria cd_agencia_fornecedor,
	decode(c.ie_digito_agencia, null, to_char(calcula_digito('Modulo11',c.cd_agencia_bancaria)), c.ie_digito_agencia) ie_dig_agencia_fornec,
	c.nr_conta nr_conta_forneced,
	c.ie_digito_conta nr_digito_conta,
	rpad(decode(d.nr_documento,null,d.nr_titulo,to_number(d.nr_documento)),7,' ') || decode(w.ie_tipo_conta,'CS','13387','') nr_pagamento,
	'000' cd_carteira,
	0 nr_bradesco,
	d.nr_titulo nr_titulo,
	d.dt_vencimento_atual dt_vencimento,
	d.dt_emissao,
	d.dt_limite_antecipacao dt_desconto,
	'' nr_fator_vencimento,
	c.vl_escritural vl_documento,
	(c.vl_escritural - c.vl_desconto + c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_pagamento,
	c.vl_desconto,
	c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) vl_acrescimo,
	nvl(d.nr_documento,d.nr_titulo) nr_documento,
	'' cd_serie_documento,
	a.dt_remessa_retorno dt_pagamento,
	c.ie_moeda cd_moeda,
	0 cd_area_empresa,
	decode(w.ie_tipo_conta,'CS','00298','00000') cd_lancamento,
	0 ie_tipo_conta,
	0 qt_registros,
	0 vl_total_pagto,
	a.nr_remessa nr_remessa,
	'' nr_bloqueto,
	decode(e.cd_cgc,d.cd_favorecido,'D','C') || '000000' || decode(e.cd_cgc,d.cd_favorecido,'01',decode(substr(obter_dados_banco_medico(d.cd_pessoa_pf_pj, 'TC'),1,2), 'CS','06', 'CP','07','03')) || '01' nr_livre_bloqueto,
	'' ie_dig_bloqueto,
	'' cd_moeda_bloqueto,
	0 cd_banco_bloqueto,
	0 vl_bloqueto,
	03 cd_modalidade,
	0 ie_tipo_movimento,
	a.nr_sequencia nr_seq_envio,
	decode(w.ie_tipo_conta,'CS',substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPS')),1,35),
					substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPE')),1,35)) ds_uso_empresa
from	pessoa_fisica_conta w,
	pessoa_juridica_conta x,
	Estabelecimento e,
     	banco_estabelecimento b,
     	banco_escritural a,
	titulo_pagar_v2 d,
     	titulo_pagar_escrit c
where	c.nr_titulo          	= d.nr_titulo
and	a.nr_sequencia       	= c.nr_seq_escrit
and	a.cd_banco          	= b.cd_banco
and	a.cd_estabelecimento 	= b.cd_estabelecimento
and	a.cd_estabelecimento 	= e.cd_estabelecimento
and	b.ie_tipo_relacao	in ('EP', 'ECC')
and	c.ie_tipo_pagamento	= 'DOC'
and	d.cd_pessoa_pf_pj	= w.cd_pessoa_fisica(+)
and	d.cd_pessoa_pf_pj	= x.cd_cgc(+)
and	((x.ie_conta_pagamento = 'S' and x.ie_prestador_pls = 'S' and x.ie_situacao = 'A' and x.cd_cgc is not null)
or	(not exists (	select	1
			from	pessoa_juridica_conta o
			where	o.cd_cgc = d.cd_pessoa_pf_pj) and d.ie_tipo_favorecido = 2)
or	(w.ie_conta_pagamento = 'S' and w.ie_situacao = 'A' and w.cd_pessoa_fisica is not null)
or	(not exists (	select	1
			from	pessoa_fisica_conta u
			where	u.cd_pessoa_fisica = d.cd_pessoa_pf_pj) and d.ie_tipo_favorecido = 1))
Union
-- Somente para TED
select 	1 tp_registro,
	'0' nr_inscricao,
	'' nm_empresa,
	sysdate dt_arquivo,
	sysdate hr_arquivo,
	d.ie_tipo_favorecido ie_tipo_fornecedor,
	decode(d.ie_tipo_favorecido,1,substr(d.cd_favorecido,1,9) ||'0000'|| substr(d.cd_favorecido,10,2),d.cd_favorecido) cd_cgc_fornecedor,
	upper(d.nm_favorecido) nm_fornecedor,
	upper(d.ds_endereco) ||' , '|| d.nr_endereco ds_endereco,
	d.cd_cep,
	c.cd_banco cd_banco_fornecedor,
	c.cd_agencia_bancaria cd_agencia_fornecedor,
	decode(c.ie_digito_agencia, null, to_char(calcula_digito('Modulo11',c.cd_agencia_bancaria)), c.ie_digito_agencia) ie_dig_agencia_fornec,
	c.nr_conta nr_conta_forneced,
	c.ie_digito_conta nr_digito_conta,
	rpad(decode(d.nr_documento,null,d.nr_titulo,to_number(d.nr_documento)),7,' ') || decode(w.ie_tipo_conta,'CS','13387','') nr_pagamento,
	'000' cd_carteira,
	0 nr_bradesco,
	d.nr_titulo nr_titulo,
	d.dt_vencimento_atual dt_vencimento,
	d.dt_emissao,
	d.dt_limite_antecipacao dt_desconto,
	'' nr_fator_vencimento,
	c.vl_escritural vl_documento,
	(c.vl_escritural - c.vl_desconto + c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_pagamento,
	c.vl_desconto,
	c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0) vl_acrescimo,
	nvl(d.nr_documento,d.nr_titulo) nr_documento,
	'' cd_serie_documento,
	a.dt_remessa_retorno dt_pagamento,
	c.ie_moeda cd_moeda,
	0 cd_area_empresa,
	decode(c.ie_tipo_servico,'00030','00298','00000') cd_lancamento, -- decode(w.ie_tipo_conta,'CS','00298','00000') cd_lancamento,
	0 ie_tipo_conta,
	0 qt_registros,
	0 vl_total_pagto,
	a.nr_remessa nr_remessa,
	'' nr_bloqueto,
	decode(e.cd_cgc,d.cd_favorecido,'D','C') || '000000' || decode(e.cd_cgc,d.cd_favorecido,'01',decode(substr(obter_dados_banco_medico(d.cd_pessoa_pf_pj, 'TC'),1,2), 'CS','06', 'CP','07','03')) || '01' nr_livre_bloqueto,
	'' ie_dig_bloqueto,
	'' cd_moeda_bloqueto,
	0 cd_banco_bloqueto,
	0 vl_bloqueto,
	08 cd_modalidade,
	0 ie_tipo_movimento,
	a.nr_sequencia nr_seq_envio,
	decode(w.ie_tipo_conta,'CS',substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPS')),1,35),
					substr(usjrp_obter_dados_bancarios(a.nr_seq_conta_banco,d.nr_titulo,upper('RPE')),1,35)) ds_uso_empresa
from	pessoa_fisica_conta w,
	pessoa_juridica_conta x,
	Estabelecimento e,
     	banco_estabelecimento b,
     	banco_escritural a,
	titulo_pagar_v2 d,
     	titulo_pagar_escrit c
where	c.nr_titulo		= d.nr_titulo
and	a.nr_sequencia		= c.nr_seq_escrit
and	a.cd_banco		= b.cd_banco
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.cd_estabelecimento	= e.cd_estabelecimento
and	b.ie_tipo_relacao	in ('EP', 'ECC')
and	c.ie_tipo_pagamento	= 'TED'
and	d.cd_pessoa_pf_pj	= w.cd_pessoa_fisica(+)
and	d.cd_pessoa_pf_pj	= x.cd_cgc(+)
and	((x.ie_conta_pagamento = 'S' and x.ie_prestador_pls = 'S' and x.ie_situacao = 'A' and x.cd_cgc is not null)
or	(not exists (	select	1
			from	pessoa_juridica_conta o
			where	o.cd_cgc = d.cd_pessoa_pf_pj) and d.ie_tipo_favorecido = 2)
or	(w.ie_conta_pagamento = 'S' and w.ie_situacao = 'A' and w.cd_pessoa_fisica is not null)
or	(not exists (	select	1
			from	pessoa_fisica_conta u
			where	u.cd_pessoa_fisica = d.cd_pessoa_pf_pj) and d.ie_tipo_favorecido = 1))
union
-- Trailler
select	9 tp_registro,
	'0' nr_inscricao,
	'' nm_empresa,
	sysdate dt_arquivo,
	sysdate hr_arquivo,
	'0' ie_tipo_fornecedor,
	'0' cd_cgc_fornecedor,
	'' nm_fornecedor,
	'' ds_endereco,
	'0' cd_cep,
	0 cd_banco_fornecedor,
	'0' cd_agencia_fornecedor,
	'0' ie_dig_agencia_fornec,
	'0' nr_conta_forneced,
	'0' nr_digito_conta,
	'0' nr_pagamento,
	'000' cd_carteira,
	0 nr_bradesco,
	0 nr_titulo,
	sysdate dt_vencimento,
	sysdate dt_emissao,
	sysdate dt_desconto,
	'0' nr_fator_vencimento,
	0 vl_documento,
	0 vl_pagamento,
	0 vl_desconto,
	0 vl_acrescimo,
	0 nr_documento,
	'' cd_serie_documento,
	sysdate dt_pagamento,
	0 cd_moeda,
	0 cd_area_empresa,
	'00000' cd_lancamento,
	0 ie_tipo_conta,
	(count(*) + 2) qt_registros,
	sum(c.vl_escritural - c.vl_desconto + c.vl_acrescimo + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_total_pagto,
	0 nr_remessa,
	'0' nr_bloqueto,
	'0' nr_livre_bloqueto,
	'0' nr_dig_bloqueto,
	'0' cd_moeda_bloqueto,
	0 cd_banco_bloqueto,
	0 vl_bloqueto,
	0 cd_modalidade,
	0 ie_tipo_movimento,
	e.nr_sequencia nr_seq_envio,
	'' ds_uso_empresa
from	banco_escritural e,
	titulo_pagar_escrit c
where	e.nr_sequencia			= c.nr_seq_escrit
group by e.nr_sequencia;
/