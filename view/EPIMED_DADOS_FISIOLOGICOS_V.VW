create or replace view epimed_dados_fisiologicos_v as
select 	cd_pessoa_fisica PATIENTID,
		nr_atendimento VISIT_ID,
		null nr_prescricao,
		null nm_exame,
		to_char(DT_SINAL_VITAL ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE, -- Data do sinal vital
		nr_sequencia ID_EXAM,
		'QT_PAM' EXAM_CODE ,
        (qt_pa_sistolica||'/'||QT_PA_DIASTOLICA) EXAM_VALUE,
        'mmHg' UNIT_OF_MEASURE
from atendimento_sinal_vital
where ie_situacao <> 'I'
and (qt_pa_sistolica is not null
or QT_PA_DIASTOLICA is not null)
union all
select 	cd_pessoa_fisica PATIENTID,
		nr_atendimento VISIT_ID,
		null nr_prescricao,
		null nm_exame,
		to_char(DT_SINAL_VITAL ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE, -- Data do sinal vital
		nr_sequencia ID_EXAM,
		'QT_FREQ_CARDIACA' EXAM_CODE ,
        to_char(QT_FREQ_CARDIACA) EXAM_VALUE,
        'bpm' UNIT_OF_MEASURE
from atendimento_sinal_vital
where QT_FREQ_CARDIACA is not null
and ie_situacao <> 'I'
union all
select 	cd_pessoa_fisica PATIENTID,
		nr_atendimento VISIT_ID,
		null nr_prescricao,
		null nm_exame,
		to_char(DT_SINAL_VITAL ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE, -- Data do sinal vital
		nr_sequencia ID_EXAM,
		'QT_FREQ_RESP' EXAM_CODE ,
		to_char(QT_FREQ_RESP) EXAM_VALUE,
		'irpm' UNIT_OF_MEASURE
from  atendimento_sinal_vital
where QT_FREQ_RESP is not null
and ie_situacao <> 'I'
union all
select 	cd_pessoa_fisica PATIENTID,
		nr_atendimento VISIT_ID,
		null nr_prescricao,
		null nm_exame,
		to_char(DT_SINAL_VITAL ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE, -- Data do sinal vital
		nr_sequencia ID_EXAM,
		'QT_TEMP' EXAM_CODE ,
		to_char(QT_TEMP) EXAM_VALUE,
		chr(176)||'C' UNIT_OF_MEASURE
from  atendimento_sinal_vital
where QT_TEMP is not null
and ie_situacao <> 'I'
union all
select 	CD_PACIENTE PATIENTID,
		nr_atendimento VISIT_ID,
		null nr_prescricao,
		null nm_exame,
		to_char(DT_AVALIACAO ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE, -- Data da escala de glasgow
		nr_sequencia ID_EXAM,
		'QT_GLASGOW' EXAM_CODE ,
		to_char(QT_GLASGOW) EXAM_VALUE,
		'Pontos (Total)' UNIT_OF_MEASURE
from  atend_escala_indice
where QT_GLASGOW is not null
and ie_situacao <> 'I'
union all
select b.cd_pessoa_fisica PATIENTID,
       b.nr_atendimento VISIT_ID,
       b.nr_prescricao,
       elab.nm_exame,
       to_char(elri.dt_coleta ,'YYYY-MM-DD"T"HH24:MI:SS."000"') EXAM_DATE,
       elab.nr_seq_exame ID_EXAM,
       lee.cd_exame_equip EXAM_CODE ,
       lab_obter_resultado_exame(elri.nr_seq_resultado,elri.nr_sequencia) EXAM_VALUE,
	   NVL(elri.ds_unidade_medida,lab_obter_unidade_medida(elri.nr_seq_unid_med)) UNIT_OF_MEASURE
from prescr_procedimento a
inner join prescr_medica b on a.nr_prescricao = b.nr_prescricao
inner join lab_exame_equip lee on lee.nr_seq_exame = a.nr_seq_exame
inner join equipamento_lab el on el.cd_equipamento = lee.cd_equipamento and el.ds_sigla = 'EPIMED'
inner join exame_lab_resultado elr on elr.nr_prescricao = a.nr_prescricao
inner join exame_lab_result_item elri on elri.nr_seq_resultado = elr.nr_seq_resultado  and elri.nr_seq_prescr = a.nr_sequencia
inner join material_exame_lab mel on mel.nr_sequencia = elri.nr_seq_material
inner join exame_laboratorio elab on elab.nr_seq_exame = a.nr_seq_exame
where  a.ie_status_atend>=35
and  elri.nr_seq_material is not null;
/
