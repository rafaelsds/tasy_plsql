CREATE or REPLACE VIEW grafico_disp_item_html_v
AS
/* Dispositivos*/
select  a.dt_instalacao x,
        1 y,
	null y_min,
	null y_max,         
        a.nr_sequencia nr_sequencia,
	'D' code_group
from    atend_pac_dispositivo a
union  all
select nvl(a.dt_retirada, sysdate) x,
       1 y,
       null y_min,
       null y_max,
       a.nr_sequencia nr_sequencia,
	'D' code_group
from   atend_pac_dispositivo a
union all
/*Equipamentos*/
select  a.dt_inicio x,
        1 y,
	null y_min,
	null y_max,         
        a.nr_sequencia nr_sequencia,
	'E' code_group
from    equipamento_cirurgia a
union  all
select nvl(a.dt_fim, sysdate) x,
       1 y,
       null y_min,
       null y_max,
       a.nr_sequencia nr_sequencia,
	'E' code_group
from   equipamento_cirurgia a;
/