create or replace view huop_arquivo_anexo_contrato_v as
select	'1' num,
	a.nr_sequencia,
	b.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',b.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(b.ds_arquivo,'S') ds_arquivo,
	substr(b.ds_arquivo,length(b.ds_arquivo) -2,length(b.ds_arquivo)) ds_extensao,
	'S' ie_anexa_zip,
	'2' ie_tipo,
	decode(b.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	b.ds_arquivo is not null
and	o.nr_seq_interno is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'2' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,	
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_public_edital c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	b.nr_sequencia		= c.nr_seq_lic_edital
and	o.nr_seq_licitacao	= a.nr_sequencia
and	c.ds_arquivo is not null
and	o.nr_seq_interno is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'3' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,	
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_impug_edital c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	b.nr_sequencia		= c.nr_seq_lic_edital
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'4' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_errata_edital c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	b.nr_sequencia		= c.nr_seq_lic_edital
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'5' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_anexo_edital c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	c.ds_arquivo is not null
and	o.nr_seq_interno is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'6' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.nm_arquivo,'S') ds_arquivo,
	substr(c.nm_arquivo,length(c.nm_arquivo) -2,length(c.nm_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_documento_edital c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	b.nr_sequencia		= c.nr_seq_lic_edital
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.nm_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'7' ie_tipo,
	a.nr_sequencia,
	w.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',w.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(w.ds_arquivo,'S') ds_arquivo,
	substr(w.ds_arquivo,length(w.ds_arquivo) -2,length(w.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(w.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,		
	reg_lic_fornec x,
	reg_lic_fornec_docto y,
	pessoa_juridica_doc z,
	pessoa_juridica_doc_anexo w,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= x.nr_seq_licitacao
and	x.nr_sequencia		= y.nr_seq_fornec
and	y.nr_seq_tipo_docto	= z.nr_seq_tipo_docto 
and 	x.cd_cgc_fornec		= z.cd_cgc
and	z.nr_sequencia		= w.nr_seq_doc
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	w.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'8' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_anexo c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	c.ds_arquivo is not null
and	o.nr_seq_interno is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'9' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_ata c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	c.ds_arquivo is not null
and	o.nr_seq_interno is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'10' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_public_ata c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'11' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_anexo_recurso c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'12' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_resultado c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'13' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_public_result c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	c.ds_arquivo is not null
and	o.nr_seq_interno is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'14' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_homologacao c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	'15' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	'S',
	'2' ie_tipo,
	decode(c.nr_seq_tipo_anexo,5,to_char(o.dt_aprovacao,'yyyy'),to_char(o.dt_ordem_compra,'yyyy')) dt_ano_contrato,
	decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007') nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_homol_public c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	c.ds_arquivo is not null
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material = y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	distinct
	'16' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	nvl(c.ie_anexa_zip,'N'),
	'2' ie_tipo,
	(select max(to_char(o.dt_ordem_compra,'yyyy'))
	from	ordem_compra o
	where	nr_ordem_compra = somente_numero(obter_nome_arquivo(c.ds_arquivo,'S'))) dt_ano_contrato,	
	(select max(decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007'))
	from	ordem_compra o
	where	nr_ordem_compra = somente_numero(obter_nome_arquivo(c.ds_arquivo,'S'))) nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_anexo_encer c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	nvl(c.nr_seq_tipo_anexo,0) <> 5
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material	= y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
union all
select	distinct
	'16' ie_tipo,
	a.nr_sequencia,
	c.nr_seq_tipo_anexo,
	'146148' id_entidade,
	substr(obter_descricao_padrao('REG_LIC_TIPO_ANEXO','CD_TIPO_ANEXO',c.nr_seq_tipo_anexo),1,15) ie_tipo_anexo,
	obter_nome_arquivo(c.ds_arquivo,'S') ds_arquivo,
	substr(c.ds_arquivo,length(c.ds_arquivo) -2,length(c.ds_arquivo)) ds_extensao,
	nvl(c.ie_anexa_zip,'N'),
	'2' ie_tipo,
	(select max(to_char(o.dt_aprovacao,'yyyy'))
	from	ordem_compra o
	where	nr_ordem_compra = somente_numero(obter_nome_arquivo(c.ds_arquivo,'S'))) dt_ano_contrato,	
	(select max(decode(length(o.nr_seq_interno),1,lpad(o.nr_seq_interno || '0007',7,0),2,lpad(o.nr_seq_interno || '0007',7,0),o.nr_seq_interno || '0007'))
	from	ordem_compra o
	where	nr_ordem_compra = somente_numero(obter_nome_arquivo(c.ds_arquivo,'S'))) nr_contrato,
	' ' dt_assinatura
from	reg_licitacao a,
	reg_lic_edital b,
	reg_lic_anexo_encer c,
	ordem_compra o
where	a.nr_sequencia		= b.nr_seq_licitacao
and	a.nr_sequencia		= c.nr_seq_licitacao
and	o.nr_seq_licitacao	= a.nr_sequencia
and	o.nr_seq_interno is not null
and	nvl(c.nr_seq_tipo_anexo,0) = 5
and exists(
	select	1
	from	ordem_compra_item x,
		material y
	where	x.cd_material	= y.cd_material
	and	x.nr_ordem_compra = o.nr_ordem_compra
	and	y.ie_tipo_material <> 4)
/
