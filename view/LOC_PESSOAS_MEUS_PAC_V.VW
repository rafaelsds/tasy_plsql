CREATE OR REPLACE VIEW LOC_PESSOAS_MEUS_PAC_V AS
    SELECT  DISTINCT
            SUBSTR(OBTER_NOME_PF_OCULTA(D.CD_PESSOA_FISICA, WHEB_USUARIO_PCK.GET_CD_PERFIL, WHEB_USUARIO_PCK.GET_NM_USUARIO, NULL),1, 255) NM_PESSOA_FISICA,  
            C.NR_ATENDIMENTO NR_ATENDIMENTO,
            D.DT_NASCIMENTO DT_NASCIMENTO,
            NVL(OBTER_CD_PESSOA_EXTERNO(D.CD_PESSOA_FISICA), OBTER_PRONTUARIO_PF(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, D.CD_PESSOA_FISICA)) NR_PRONTUARIO,
            OBTER_NOME_MEDICO(C.CD_MEDICO_RESP, 'PMS') NM_MEDICO,
            C.CD_ESTABELECIMENTO,
            C.CD_MEDICO_RESP CD_MEDICO,
            NVL(B.CD_UNIDADE_BASICA, SUBSTR(OBTER_UNIDADE_ATENDIMENTO(C.NR_ATENDIMENTO, 'A', 'UB'), 1, 30)) CD_UNIDADE_BASICA,
            NVL(B.CD_UNIDADE_COMPL, SUBSTR(OBTER_UNIDADE_ATENDIMENTO(C.NR_ATENDIMENTO, 'A', 'UC'), 1, 30)) CD_UNIDADE_COMPL,
            D.CD_PESSOA_FISICA,
            OBTER_IDADE(D.DT_NASCIMENTO, SYSDATE, 'D') QT_IDADE,
            OBTER_NOME_SETOR(B.CD_SETOR_ATENDIMENTO) DS_SETOR_ATENDIMENTO,
            NVL(C.NR_SEQ_EPISODIO, 0) NR_SEQ_EPISODIO,
            SUBSTR(OBTER_EPISODIO_ATEND_TELA(C.NR_ATENDIMENTO), 1, 80) NR_EPISODIO
    FROM    ATENDIMENTO_PACIENTE C,
            UNIDADE_ATENDIMENTO B,
            PESSOA_FISICA D
    WHERE   B.NR_ATENDIMENTO = C.NR_ATENDIMENTO
    AND     B.NR_ATENDIMENTO IS NOT NULL
    AND     D.CD_PESSOA_FISICA = C.CD_PESSOA_FISICA
    AND     WHEB_ASSIST_PCK.GET_NIVEL_ATENCAO_PERFIL <> 'S'
    AND     PATIENT_ALREADY_DELETED(D.CD_PESSOA_FISICA) = 'N'
    AND     OBTER_SE_ACESSO_PACIENTE_LIB(D.CD_PESSOA_FISICA) = 'S'
UNION
    SELECT
            SUBSTR(OBTER_NOME_PF_OCULTA(D.CD_PESSOA_FISICA, WHEB_USUARIO_PCK.GET_CD_PERFIL, WHEB_USUARIO_PCK.GET_NM_USUARIO, NULL), 1, 255) NM_PESSOA_FISICA,
            C.NR_ATENDIMENTO NR_ATENDIMENTO,
            D.DT_NASCIMENTO DT_NASCIMENTO,
            NVL(OBTER_CD_PESSOA_EXTERNO(D.CD_PESSOA_FISICA), OBTER_PRONTUARIO_PF(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, D.CD_PESSOA_FISICA)) NR_PRONTUARIO,
            OBTER_NOME_MEDICO(B.CD_MEDICO, 'PMS') NM_MEDICO,
            NULL CD_ESTABELECIMENTO,
            B.CD_MEDICO,
            SUBSTR(OBTER_UNIDADE_ATENDIMENTO(C.NR_ATENDIMENTO, 'A', 'UB'), 1, 30) CD_UNIDADE_BASICA,
            SUBSTR(OBTER_UNIDADE_ATENDIMENTO(C.NR_ATENDIMENTO, 'A', 'UC'), 1, 30) CD_UNIDADE_COMPL,
            D.CD_PESSOA_FISICA,
            OBTER_IDADE(D.DT_NASCIMENTO, SYSDATE, 'D') QT_IDADE,
            OBTER_NOME_SETOR(OBTER_SETOR_ATENDIMENTO(C.NR_ATENDIMENTO)) DS_SETOR_ATENDIMENTO,
            OBTER_EPISODIO_ATENDIMENTO(C.NR_ATENDIMENTO) NR_SEQ_EPISODIO,
            SUBSTR(OBTER_EPISODIO_ATEND_TELA(C.NR_ATENDIMENTO), 1, 80) NR_EPISODIO
    FROM    MED_PACIENTE_ATEND C,
            MED_CLIENTE B,
            PESSOA_FISICA D
    WHERE   B.CD_PESSOA_FISICA = D.CD_PESSOA_FISICA
    AND     B.NR_SEQUENCIA = C.NR_SEQ_CLIENTE
    AND     C.NR_ATENDIMENTO IS NOT NULL
    AND     PATIENT_ALREADY_DELETED(D.CD_PESSOA_FISICA) = 'N'
    AND     OBTER_SE_ACESSO_PACIENTE_LIB(D.CD_PESSOA_FISICA) = 'S'
    AND     WHEB_ASSIST_PCK.GET_NIVEL_ATENCAO_PERFIL = 'S'
UNION
    SELECT 
            SUBSTR(OBTER_NOME_PF_OCULTA(D.CD_PESSOA_FISICA, WHEB_USUARIO_PCK.GET_CD_PERFIL, WHEB_USUARIO_PCK.GET_NM_USUARIO, NULL), 1, 255) NM_PESSOA_FISICA,
            C.NR_ATENDIMENTO NR_ATENDIMENTO,
            D.DT_NASCIMENTO DT_NASCIMENTO,
            NVL(OBTER_CD_PESSOA_EXTERNO(D.CD_PESSOA_FISICA), OBTER_PRONTUARIO_PF(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, D.CD_PESSOA_FISICA)) NR_PRONTUARIO,
            OBTER_NOME_MEDICO(C.CD_MEDICO_RESP, 'PMS') NM_MEDICO,
            C.CD_ESTABELECIMENTO,
            C.CD_MEDICO_RESP CD_MEDICO,
            NVL(A.CD_UNIDADE_BASICA, SUBSTR(OBTER_UNIDADE_ATENDIMENTO(C.NR_ATENDIMENTO, 'A', 'UB'), 1, 30)) CD_UNIDADE_BASICA,
            NVL(A.CD_UNIDADE_COMPL, SUBSTR(OBTER_UNIDADE_ATENDIMENTO(C.NR_ATENDIMENTO, 'A', 'UC'), 1, 30)) CD_UNIDADE_COMPL,
            D.CD_PESSOA_FISICA,
            OBTER_IDADE(D.DT_NASCIMENTO, SYSDATE, 'D') QT_IDADE,
            OBTER_NOME_SETOR(B.CD_SETOR_ATENDIMENTO) DS_SETOR_ATENDIMENTO,
            NVL(C.NR_SEQ_EPISODIO, 0) NR_SEQ_EPISODIO,
            SUBSTR(OBTER_EPISODIO_ATEND_TELA(C.NR_ATENDIMENTO), 1, 80) NR_EPISODIO
    FROM    PESSOA_FISICA D,
            ATENDIMENTO_PACIENTE C,
            SETOR_ATENDIMENTO B,
            ATEND_PACIENTE_UNIDADE A
    WHERE   A.CD_SETOR_ATENDIMENTO = B.CD_SETOR_ATENDIMENTO
    AND     A.DT_SAIDA_UNIDADE IS NULL
    AND     C.DT_ALTA IS NULL
    AND     A.NR_ATENDIMENTO = C.NR_ATENDIMENTO
    AND     C.CD_PESSOA_FISICA = D.CD_PESSOA_FISICA
    AND     WHEB_ASSIST_PCK.GET_NIVEL_ATENCAO_PERFIL <> 'S'
    AND     PATIENT_ALREADY_DELETED(D.CD_PESSOA_FISICA) = 'N'
    AND     OBTER_SE_ACESSO_PACIENTE_LIB(D.CD_PESSOA_FISICA) = 'S'
ORDER BY
        NM_PESSOA_FISICA;
/