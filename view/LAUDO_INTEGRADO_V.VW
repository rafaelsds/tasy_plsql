CREATE OR REPLACE 
VIEW Laudo_integrado_v
AS
select	a.nr_sequencia,
	a.dt_integracao
FROM	laudo_paciente a;	

DROP PUBLIC SYNONYM Laudo_integrado_v;

CREATE PUBLIC SYNONYM Laudo_integrado_v FOR TASY.Laudo_integrado_v;
	
GRANT SELECT, UPDATE ON Laudo_integrado_v TO Pixeon;