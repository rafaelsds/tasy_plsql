CREATE OR REPLACE VIEW movto_estoque_ajuste_prod_v
AS
select	/*+ ORDERED */
	a.nr_movimento_estoque, 
	sum(decode(b.ie_aumenta_diminui_valor,'D',
                  (a.vl_movimento * -1), a.vl_movimento)) vl_movimento,
	c.cd_operacao_estoque,
	c.dt_movimento_estoque,
	c.cd_material,
	c.nr_documento,
	c.nr_lote_contabil,
	c.cd_local_estoque
from movimento_estoque c,
	movimento_estoque_valor a,
     tipo_valor b
where	a.nr_movimento_estoque = c.nr_movimento_estoque
and	a.cd_tipo_valor	= b.cd_tipo_valor
and	b.cd_tipo_valor = 17
group by a.nr_movimento_estoque,
	c.cd_operacao_estoque,
	c.dt_movimento_estoque,
	c.cd_material,
	c.nr_documento,
	c.nr_lote_contabil,
	c.cd_local_estoque;
/