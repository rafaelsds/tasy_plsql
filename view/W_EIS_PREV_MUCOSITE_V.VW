create or replace view w_eis_prev_mucosite_v  as 
select distinct
	trunc(a.dt_avaliacao) dt_avaliacao,
	obter_setor_atendimento(a.nr_atendimento) setor,
	obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)) ds_setor_atendimento,		
	a.nr_atendimento,
	b.cd_estabelecimento,
	obter_nome_estabelecimento(b.cd_estabelecimento) ds_estabelecimento,
	decode(a.ie_toxidade,0,0,1) qt_presenca,
	obter_max_toxidade(a.nr_atendimento,a.dt_avaliacao) tox,
	substr(obter_valor_dominio(3979,obter_max_toxidade(a.nr_atendimento,a.dt_avaliacao)),1,255) ds_toxidade,
	b.nr_seq_classificacao cla,
	obter_desc_classif_atend(b.nr_seq_classificacao) ds_classificacao
from	escala_mucosite a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	((upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%ONCOLOGIA%') or
	(upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%HEMATOLOGIA%') or
	(upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%TMO%') or
	(upper(obter_desc_classif_atend(b.nr_seq_classificacao)) like '%MEDULA%')) 
and	a.dt_inativacao is null;
/