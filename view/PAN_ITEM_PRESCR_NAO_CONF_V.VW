create or replace
view pan_item_prescr_nao_conf_v as
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		b.cd_material cd_item,
		substr(c.ds_material,1,255) ds_item,
		wheb_mensagem_pck.get_texto(decode(b.ie_agrupador,1,795040, 2,795043, 8,795044, 12,795045, 16,795074, 10,795075, 11,795075, 795040)) || decode(b.ie_agrupador,11,'2','') ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
		a.cd_estabelecimento,a.nr_seq_atend,decode(b.ie_agrupador,1,'M',2,'MAT',8,'SNE',12,'SUP',16,'L',null),decode(b.ie_agrupador, 10,null,11,null, b.ie_urgencia),
		decode(b.ie_agrupador, 10,null,11,null, b.nr_ocorrencia) , decode(b.ie_agrupador,10,null,11,null,b.ie_dose_espec_agora)) ie_momento_lote,      
		cpoe_obter_dt_inicio(b.nr_seq_mat_cpoe,'M') dt_inicio
from	prescr_medica a,
		prescr_material b,
		material c,
		atendimento_paciente d
where	a.nr_prescricao 	= b.nr_prescricao
and	b.cd_material 	= c.cd_material
and d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and	b.nr_sequencia_solucao is null
and	b.nr_sequencia_dieta is null
and	b.nr_sequencia_diluicao is null
and	b.cd_kit_material is null
and	b.nr_sequencia_proc	is null
and	b.nr_seq_leite_deriv is null
and	a.dt_liberacao_medico is not null
and b.ie_agrupador in (1,2,8,12,16,10,11)
and	a.dt_suspensao is null
and	b.dt_suspensao is null
union
--Procedimentos
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(y.ds_procedimento,1,255) ds_item,
		wheb_mensagem_pck.get_texto(795046) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
		a.cd_estabelecimento,a.nr_seq_atend,'P',x.ie_urgencia,x.nr_ocorrencia,null) ie_momento_lote,
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	procedimento y,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		x.nr_prescricao = a.nr_prescricao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and		x.nr_seq_proc_interno is null
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is null
and		x.nr_seq_derivado is null
and		x.nr_seq_exame_sangue is null
and 	x.nr_Seq_origem is null
and 	x.dt_suspensao is null
and 	a.dt_suspensao is null
and		nvl(x.ie_administrar,'S') = 'S'
and	   	a.dt_liberacao_medico is not null
union
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(y.ds_procedimento,1,255) ds_item,
		wheb_mensagem_pck.get_texto(795046) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
		a.cd_estabelecimento,a.nr_seq_atend,'P',x.ie_urgencia,x.nr_ocorrencia,null) ie_momento_lote,
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	procedimento y,
		proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		w.nr_sequencia = x.nr_seq_proc_interno
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and		x.nr_prescricao = a.nr_prescricao
and d.dt_alta is null
and		nvl(w.ie_tipo,'O') not in ('G','BS')
and 	x.nr_Seq_origem is null
and		nvl(w.ie_ivc,'N') <> 'S'
and		nvl(w.ie_ctrl_glic,'NC') = 'NC'
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_prot_glic is null
and		nvl(x.ie_administrar,'S') = 'S'
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is null
and		x.nr_seq_derivado is null
and		x.nr_seq_exame_sangue is null
and 	x.dt_suspensao is null
and 	a.dt_suspensao is null
and	   	a.dt_liberacao_medico is not null
union
--CIG
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(z.ds_valor_dominio,1,255) ds_item,
		wheb_mensagem_pck.get_texto(795048) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),a.cd_estabelecimento,null) ie_momento_lote,
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	valor_dominio_v z,
		procedimento y,
		proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	z.cd_dominio = 1903
and		z.vl_dominio = w.ie_ctrl_glic
and		y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		w.nr_sequencia = x.nr_seq_proc_interno
and		x.nr_prescricao = a.nr_prescricao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and		nvl(w.ie_tipo,'O') not in ('G','BS')
and		nvl(w.ie_ivc,'N') <> 'S'
and		w.ie_ctrl_glic = 'CIG'
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_solic_sangue is null
and 	x.dt_suspensao is null
and 	a.dt_suspensao is null
and		x.nr_seq_derivado is null
and		x.nr_seq_exame_sangue is null
and	  a.dt_liberacao_medico is not null
union
--CCG
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(z.ds_protocolo,1,255) ds_item,
		wheb_mensagem_pck.get_texto(795049) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
		a.cd_estabelecimento,a.nr_seq_atend,'G',x.ie_urgencia,x.nr_ocorrencia,null) ie_momento_lote,
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	pep_protocolo_glicemia z,
		procedimento y,
		proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	z.nr_sequencia = x.nr_seq_prot_glic
and		y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		w.nr_sequencia = x.nr_seq_proc_interno
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and		x.nr_prescricao = a.nr_prescricao
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and		nvl(w.ie_tipo,'O') not in ('G','BS')
and		nvl(w.ie_ivc,'N') <> 'S'
and		w.ie_ctrl_glic = 'CCG'
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_prot_glic is not null
and		x.nr_seq_solic_sangue is null
and		x.nr_seq_derivado is null
and		x.nr_seq_exame_sangue is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		x.dt_suspensao is null
union
-- Dieta oral
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		b.cd_dieta cd_item,
		substr(obter_desc_dieta(cd_dieta),1,255) ds_item,
		wheb_mensagem_pck.get_texto(795050) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
			a.cd_estabelecimento,a.nr_seq_atend,'D',null,null,null) ie_momento_lote,
		cpoe_obter_dt_inicio(b.NR_SEQ_DIETA_CPOE,'D') dt_inicio
from	prescr_medica a,
		prescr_dieta b,
      atendimento_paciente d
where	a.nr_prescricao 	= b.nr_prescricao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		b.dt_suspensao is null
union
--Solucao
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		b.nr_seq_solucao cd_item,
		substr(ds_solucao,1,255) ds_item,
		wheb_mensagem_pck.get_texto(795051) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
		a.cd_estabelecimento,a.nr_seq_atend,'S',b.ie_urgencia,decode(nvl(b.ie_urgencia,'N'),'S',1,null),null) ie_momento_lote,
		cpoe_obter_dt_inicio(b.NR_SEQ_DIALISE_CPOE,'DI') dt_inicio
from	prescr_medica a,
		prescr_solucao b,
      atendimento_paciente d
where	a.nr_prescricao 				= b.nr_prescricao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and		nvl(b.ie_hemodialise,'N') 	= 'N'
and		b.nr_seq_dialise is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		b.dt_suspensao is null
union
--Gasoterapia
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		b.nr_sequencia cd_item,
		substr(c.ds_gas, 1,255) ds_item,
		wheb_mensagem_pck.get_texto(795052) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
			a.cd_estabelecimento,a.nr_seq_atend,'GAS',null,null,null) ie_momento_lote,
		cpoe_obter_dt_inicio(b.NR_SEQ_GAS_CPOE,'O') dt_inicio
from	prescr_medica a,
		prescr_gasoterapia b,
		gas c,
      atendimento_paciente d
where	a.nr_prescricao 	= b.nr_prescricao
and		b.nr_seq_gas		= c.nr_sequencia
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		b.dt_suspensao is null
union
--Hemoterapia
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		decode(x.nr_seq_exame_sangue,null,decode(x.ie_util_hemocomponente,null,null,'('|| substr(obter_valor_dominio(2247,x.ie_util_hemocomponente),1,20) ||') ') || nvl(z.ds_derivado,y.ds_procedimento),substr(Obter_desc_san_exame(x.nr_seq_exame_sangue),1,255)) || ' ' || substr(obter_interv_cuidados_hm(x.nr_prescricao, x.nr_sequencia, 'N'),1,255)  ds_item,
		wheb_mensagem_pck.get_texto(795053) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),a.cd_estabelecimento,null) ie_momento_lote,
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	san_derivado z,
		procedimento y,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	x.nr_seq_derivado = z.nr_sequencia(+)
and		y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		x.nr_prescricao = a.nr_prescricao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and		x.nr_seq_proc_interno is null
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is not null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		x.dt_suspensao is null
union
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(z.ds_derivado || ' ' || substr(obter_interv_cuidados_hm(x.nr_prescricao, x.nr_sequencia, 'S'),1,100),1,255) ds_item,
		wheb_mensagem_pck.get_texto(795053) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),a.cd_estabelecimento,null) ie_momento_lote,
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	san_derivado z,
		procedimento y,
		proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	z.nr_sequencia = x.nr_seq_derivado(+)
and		y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		w.nr_sequencia = x.nr_seq_proc_interno
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and		x.nr_prescricao = a.nr_prescricao
and		nvl(w.ie_tipo,'O') not in ('G','BS')
and		nvl(w.ie_ivc,'N') <> 'S'
and		nvl(w.ie_ctrl_glic,'NC') = 'NC'
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_prot_glic is null
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is not null
and		x.nr_seq_derivado is not null
and		x.nr_seq_exame_sangue is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		x.dt_suspensao is null
union
--Recomendacao
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		b.cd_recomendacao cd_item,
		substr(nvl(c.ds_tipo_recomendacao,nvl(b.ds_recomendacao,' ')),1,255) ds_item,
		wheb_mensagem_pck.get_texto(795054) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
			a.cd_estabelecimento,a.nr_seq_atend,'R',b.ie_urgencia,b.nr_ocorrencia,null) ie_momento_lote,
		cpoe_obter_dt_inicio(b.NR_SEQ_REC_CPOE,'R') dt_inicio
from	prescr_medica a,
		prescr_recomendacao b,
		tipo_recomendacao c,
      atendimento_paciente d
where	a.nr_prescricao 			= b.nr_prescricao
and		c.cd_tipo_recomendacao(+) 	= b.cd_recomendacao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		b.dt_suspensao is null
union
--Dialise
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.nr_seq_solucao cd_item,
		substr(nvl(obter_desc_prot_npt(x.nr_seq_protocolo),obter_prim_comp_sol(a.nr_prescricao,x.nr_seq_solucao)),1,255) ds_item,
		wheb_mensagem_pck.get_texto(795055) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),a.cd_estabelecimento,null) ie_momento_lote,   
		cpoe_obter_dt_inicio(b.NR_SEQ_DIALISE_CPOE,'D') dt_inicio
from	hd_prescricao b,
		prescr_solucao x,
		prescr_medica a,
      atendimento_paciente d
where	x.nr_prescricao 	= a.nr_prescricao
and     b.nr_sequencia    	= x.nr_seq_dialise
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and	   	a.dt_liberacao_medico is not null
and 	a.dt_suspensao is null
and		x.dt_suspensao is null
--IVC
union
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(nvl(w.ds_proc_exame,y.ds_procedimento),1,255) ds_item,
		wheb_mensagem_pck.get_texto(795058) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),a.cd_estabelecimento,null) ie_momento_lote,    
		cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'P') dt_inicio
from	procedimento y,
		proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and		w.nr_sequencia = x.nr_seq_proc_interno
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and		x.nr_prescricao = a.nr_prescricao
and d.dt_alta is null
and		nvl(w.ie_tipo,'O') not in ('G','BS')
and		nvl(w.ie_ivc,'N') = 'S'
and 	x.nr_seq_origem is null
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_prot_glic is null
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is null
and		x.nr_seq_derivado is null
and		x.nr_seq_exame_sangue is null
and 	x.dt_suspensao is null
and 	a.dt_suspensao is null
and	   	a.dt_liberacao_medico is not null
union
-- Atanomy pathology
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(nvl(w.ds_proc_exame,y.ds_procedimento),1,255)  ds_item,
		wheb_mensagem_pck.get_texto(795046) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
        a.cd_estabelecimento,a.nr_seq_atend,'P',x.ie_urgencia,x.nr_ocorrencia,null) ie_momento_lote,           
        cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'AP') dt_inicio
from	procedimento y,
        proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and     w.nr_sequencia = x.nr_seq_proc_interno
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and		x.nr_prescricao = a.nr_prescricao
and d.dt_alta is null
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is null
and		x.nr_seq_derivado is null
and		x.nr_seq_exame_sangue is null
and 	x.nr_Seq_origem is null
and 	x.dt_suspensao is null
and 	a.dt_suspensao is null
and		nvl(x.ie_administrar,'S') = 'S'
and	  a.dt_liberacao_medico is not null
and   x.ie_tipo_proced = 'AP'
union
-- Blood and blood components
select	a.nr_atendimento,
		a.nr_prescricao,
		a.dt_liberacao_farmacia,
		a.dt_liberacao,
		x.cd_procedimento cd_item,
		substr(nvl(w.ds_proc_exame,y.ds_procedimento),1,255)  ds_item,
		wheb_mensagem_pck.get_texto(795046) ds_tipo_item,
		prescr_obter_momento_lote(a.ie_motivo_prescricao,obter_setor_atendimento(a.nr_atendimento),
        a.cd_estabelecimento,a.nr_seq_atend,'P',x.ie_urgencia,x.nr_ocorrencia,null) ie_momento_lote,           
      cpoe_obter_dt_inicio(x.NR_SEQ_PROC_CPOE,'HM') dt_inicio      
from	procedimento y,
        proc_interno w,
		prescr_procedimento x,
		prescr_medica a,
      atendimento_paciente d
where	y.cd_procedimento = x.cd_procedimento
and		y.ie_origem_proced = x.ie_origem_proced
and     w.nr_sequencia = x.nr_seq_proc_interno
and		x.nr_prescricao = a.nr_prescricao
and   d.nr_atendimento = a.nr_atendimento
and d.cd_pessoa_fisica = a.cd_pessoa_fisica
and a.dt_prescricao between d.dt_entrada and sysdate
and d.dt_alta is null
and     x.nr_prescricao = a.nr_prescricao
and		x.nr_seq_proc_interno is not null
and		x.nr_seq_exame is null
and		x.nr_seq_solic_sangue is not null
and		x.nr_seq_derivado is not  null
and		x.nr_seq_exame_sangue is null
and 	x.nr_Seq_origem is null
and 	x.dt_suspensao is null
and 	a.dt_suspensao is null
and		nvl(x.ie_administrar,'S') = 'S'
and	    a.dt_liberacao_medico is not null;
/
