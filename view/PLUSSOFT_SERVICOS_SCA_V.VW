create or replace view plussoft_servicos_sca_v
as
	select	c.nr_contrato,
		a.nr_seq_plano nr_seq_sca,
		b.ds_plano,
		(select	max(x.vl_preco_atual)
		from	pls_plano_preco x
		where	x.nr_seq_tabela = a.nr_seq_tabela) vl_preco_atual
	from	pls_sca_regra_contrato a,
		pls_plano b,
		pls_contrato c
	where	b.nr_sequencia	= a.nr_seq_plano
	and	c.nr_sequencia	= a.nr_seq_contrato
	and 	sysdate between nvl(a.dt_inicio_vigencia, sysdate) and nvl(a.dt_fim_vigencia, sysdate)
	group by c.nr_contrato,
		a.nr_seq_plano,
		b.ds_plano,
		a.nr_seq_tabela
	order by 1,2;
/