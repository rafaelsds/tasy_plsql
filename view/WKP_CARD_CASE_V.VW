CREATE OR REPLACE
VIEW WKP_CARD_CASE_V AS 
SELECT
a.NR_SEQ_EPISODIO,
OBTER_DADOS_EPISODIO(a.NR_SEQ_EPISODIO, 'E') NR_CASE,
Substr(OBTER_NOME_TIPO_EPISODIO(b.nr_seq_tipo_episodio), 1, 100) TYPE_CASE,
SUBSTR(OBTER_DESC_CONVENIO(OBTER_CONVENIO_ATENDIMENTO(a.NR_ATENDIMENTO)),1,100) DS_INSURANCE,
'TESTE' SOCIAL_INSURANCE_CODE,
(SELECT x.CD_EXTERNO FROM CONVENIO x WHERE x.CD_CONVENIO = OBTER_CONVENIO_ATENDIMENTO(a.NR_ATENDIMENTO)) FUND_RELEVANCE,
a.dt_entrada ADMISSION_DATE_TIME,
(select max(ds_classificacao)
from classificacao_atendimento 
where  (nr_sequencia = a.nr_seq_classificacao)
and (cd_estabelecimento = a.cd_estabelecimento)
and ie_situacao = 'A') ADMISSION_REASON_1,
SUBSTR(OBTER_DESC_QUEIXA(a.NR_SEQ_QUEIXA),1,100) ADMISSION_REASON_2,
a.DT_ALTA DISCHARGE_DATE_TIME,
SUBSTR(OBTER_DESC_MOTIVO_ALTA(a.cd_motivo_alta),1,100) DISCHARGE_REASON,
SUBSTR(OBTER_DESC_FORMA_CHEGADA(a.NR_SEQ_FORMA_CHEGADA),1,100) DS_TRANSPORT_TYPE,
SUBSTR(OBTER_NOME_PF_PJ(NULL, c.CD_CGC),1,100) DS_REFERRAL_HOSPITAL,
SUBSTR(OBTER_NOME_PF_PJ(NULL, c.CD_CGC_TRANSPORTE),1,100) DS_POST_DISCHARGE,
Substr(Obter_valor_dominio(8406, b.ie_status), 1, 100) ||Obter_status_episodio(b.nr_sequencia, 'E')CASE_STATUS,
(SELECT ew.typ FROM	scoring_error_warning sew, w_xdok_ew ew WHERE sew.nr_case = b.nr_episodio 
AND	sew.cd_error_warning = ew.code AND	ew.dt_atualizacao = (SELECT  MAX(ewx.dt_atualizacao)
FROM scoring_error_warning sewx, w_xdok_ew ewx WHERE  sewx.cd_error_warning = ewx.code AND sewx.nr_case = sew.nr_case)) as DS_SCORING_STATUS,
'Requirement PFIT.05.LKF.51 not implementated' as DS_DISCHARGE_LETTER_STATUS,
'Requirement PFIT.05.LKF.50 not implementated' as DS_SURGERY_REPORT_STATUS,
'Requirement PFIT.05.LKF.23 not implementated' as DS_ICU_DOCUMENTATION_STATUS
FROM ATENDIMENTO_PACIENTE a, EPISODIO_PACIENTE b, ATENDIMENTO_TRANSF c
WHERE a.NR_SEQ_EPISODIO = b.NR_SEQUENCIA
and a.NR_ATENDIMENTO = c.NR_ATENDIMENTO(+)
/
