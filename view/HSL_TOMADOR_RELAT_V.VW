create or replace
view hsl_tomador_relat_v as
select	nr_sequencia,
	decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(nfe_elimina_caractere_especial(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc)),1,255),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','RS'),'N�O INFORMADO') nm_razao_social,
	decode(cd_cgc, '', decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_pf(cd_pessoa_fisica, 'CPF'), 1, 11),
			'C',obter_dados_tomador_conta(a.nr_sequencia,'C','CPF'),'------') , decode(obter_se_tomador_intermed(a.nr_sequencia),'S',cd_cgc,'------')) cd_cgc,		
	nvl(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(lpad(obter_dados_nfse_sp(cd_pessoa_fisica,cd_cgc,'IM'),8,'0'),1,15),'C',substr(obter_dados_tomador_conta(a.nr_sequencia,'C','IM'),1,15),'------'),'------') cd_Inscricao_Municipal,
	decode(
	substr(tiss_eliminar_caractere(elimina_acentuacao(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'COM'),1,30),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','CE'),null))), 1, 255),'',
	substr(tiss_eliminar_caractere(elimina_acentuacao(	decode(obter_se_tomador_intermed(a.nr_sequencia),'S',obter_dados_pf_pj(cd_pessoa_fisica, cd_cgc, 'EN'),'C',obter_dados_pf_pj((select  max(d.cd_pessoa_fisica)
																								from	conta_paciente c,
																									atendimento_paciente d
																								where	c.nr_interno_conta = a.nr_interno_conta
																								and	c.nr_atendimento = d.nr_atendimento), null, 'EN'),null))), 1, 255) || ' ' ||
	substr(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'NR'),1,25),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','NE'),null), 1, 25)||' - '||
	substr(tiss_eliminar_caractere(elimina_acentuacao(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'BAI'),1,30),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','B'),null))), 1, 255)  ||' - CEP: '||
	substr(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',lpad(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'CEP'),8,'0'),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','CEP'),null), 1, 15),	
	substr(tiss_eliminar_caractere(elimina_acentuacao(	decode(obter_se_tomador_intermed(a.nr_sequencia),'S',obter_dados_pf_pj(cd_pessoa_fisica, cd_cgc, 'EN'),'C',obter_dados_pf_pj((select  max(d.cd_pessoa_fisica)
																							        from	conta_paciente c,
																									atendimento_paciente d
																								where	c.nr_interno_conta = a.nr_interno_conta
																								and	c.nr_atendimento = d.nr_atendimento), null, 'EN'),null))), 1, 255) || ' ' ||
	substr(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'NR'),1,25),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','NE'),null), 1, 25)||' - '||
	substr(tiss_eliminar_caractere(elimina_acentuacao(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'COM'),1,30),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','CE'),null))), 1, 255) ||' - '||
	substr(tiss_eliminar_caractere(elimina_acentuacao(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'BAI'),1,30),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','B'),null))), 1, 255)  ||' - CEP: '||
	substr(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',lpad(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'CEP'),8,'0'),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','CEP'),null), 1, 15)
	) ds_endereco,	
	decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'CID'),1,7),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','C'),'------') ds_cidade,
	decode(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'UF'),1,2),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','UF'),'------'),'IN','EX',
	       decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'UF'),1,2),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','UF'),'------')) sg_uf,
	nvl(decode(obter_se_tomador_intermed(a.nr_sequencia),'S',substr(obter_dados_nfse_sp(cd_pessoa_fisica, cd_cgc, 'EML'),1,75),'C',obter_dados_tomador_conta(a.nr_sequencia,'C','ET'),'------'),'------') ds_email
from 	nota_fiscal a;
/