create or replace view resumo_contas_mensalidade_v
as
select	d.cd_conta_rec cd_conta,
	'C' ie_tipo_conta,
	d.vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_rec) ds_conta_contabil,
	c.dt_mesano_referencia dt_mesano_referencia,
	decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_credito,
	decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	d.ie_tipo_item	not in ('1','3','12')
UNION ALL
select	f.cd_conta_cred cd_conta,
	'C' ie_tipo_conta,
	f.vl_coparticipacao vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = f.cd_conta_cred) ds_conta_contabil,	
	c.dt_mesano_referencia dt_mesano_referencia,
	decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_credito,
	decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d,
	pls_conta e,
	pls_conta_coparticipacao f
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	d.nr_seq_conta	= e.nr_sequencia
and	e.nr_sequencia	= f.nr_seq_conta
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	d.ie_tipo_item	= '3'
UNION ALL
select	d.cd_conta_rec_antecip cd_conta,
	'C' ie_tipo_conta,
	nvl(d.vl_antecipacao,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_rec_antecip) ds_conta_contabil,
	c.dt_mesano_referencia dt_mesano_referencia,
	decode(d.vl_antecipacao,abs(d.vl_antecipacao),abs(d.vl_antecipacao),0) vl_credito,
	decode(d.vl_antecipacao,abs(d.vl_antecipacao),0,abs(d.vl_antecipacao)) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	a.dt_contabilizacao is null
UNION ALL
select	d.cd_conta_rec_antecip cd_conta,
	'C' ie_tipo_conta,
	nvl(d.vl_item,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_rec_antecip) ds_conta_contabil,
	a.dt_contabilizacao dt_mesano_referencia,
	decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_credito,
	decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
UNION ALL
select	d.cd_conta_rec cd_conta,
	'C' ie_tipo_conta,
	nvl(d.vl_pro_rata_dia,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_rec) ds_conta_contabil,
	c.dt_mesano_referencia dt_mesano_referencia,
	decode(d.vl_pro_rata_dia,abs(d.vl_pro_rata_dia),abs(d.vl_pro_rata_dia),0) vl_credito,
	decode(d.vl_pro_rata_dia,abs(d.vl_pro_rata_dia),0,abs(d.vl_pro_rata_dia)) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	d.ie_tipo_item	in ('1','12')
UNION ALL
select	d.cd_conta_deb cd_conta,
	'D' ie_tipo_conta,
	d.vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_deb) ds_conta_contabil,
	nvl(a.dt_contabilizacao,c.dt_mesano_referencia) dt_mesano_referencia,
	decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_credito,
	decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	d.ie_tipo_item	<> '3'
UNION ALL
select	f.cd_conta_deb cd_conta,
	'D' ie_tipo_conta,
	f.vl_coparticipacao,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = f.cd_conta_deb) ds_conta_contabil,	
	c.dt_mesano_referencia dt_mesano_referencia,
	decode(d.vl_item,abs(d.vl_item),0,abs(f.vl_coparticipacao)) vl_credito,
	decode(d.vl_item,abs(d.vl_item),abs(f.vl_coparticipacao),0) vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d,
	pls_conta e,
	pls_conta_coparticipacao f
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	d.nr_seq_conta	= e.nr_sequencia
and	e.nr_sequencia	= f.nr_seq_conta
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	d.ie_tipo_item	= '3'
union all /* Lepinski - in�cio contas imposto */
select  substr(pls_obter_conta_imposto('C',b.nr_sequencia,a.cd_estabelecimento),1,255) cd_conta,
	'C' ie_tipo_conta,
	nvl(e.vl_tributo,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = pls_obter_conta_imposto('C',b.nr_sequencia,a.cd_estabelecimento)) ds_conta_contabil,
	b.dt_referencia dt_mesano_referencia,
	nvl(e.vl_tributo,0) vl_credito,
	0 vl_debito
from	pls_lote_mensalidade	a,
	pls_mensalidade		b,
	titulo_receber		c,
	nota_fiscal		d,
        nota_fiscal_trib	e
where	a.nr_sequencia			= b.nr_seq_lote
and	c.nr_seq_mensalidade(+)		= b.nr_sequencia
and	d.nr_seq_mensalidade(+)		= b.nr_sequencia
and     e.nr_sequencia(+)		= d.nr_sequencia
and     e.vl_tributo <> 0
union all
select  substr(pls_obter_conta_imposto('D',b.nr_sequencia,a.cd_estabelecimento),1,255) cd_conta,
	'D' ie_tipo_conta,
	nvl(e.vl_tributo,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = pls_obter_conta_imposto('D',b.nr_sequencia,a.cd_estabelecimento)) ds_conta_contabil,
	b.dt_referencia dt_mesano_referencia,
	0 vl_credito,
	nvl(e.vl_tributo,0) vl_debito
from	pls_lote_mensalidade	a,
	pls_mensalidade		b,
	titulo_receber		c,
	nota_fiscal		d,
        nota_fiscal_trib	e
where	a.nr_sequencia			= b.nr_seq_lote
and	c.nr_seq_mensalidade(+)		= b.nr_sequencia
and	d.nr_seq_mensalidade(+)		= b.nr_sequencia
and     e.nr_sequencia(+)		= d.nr_sequencia
and     e.vl_tributo <> 0 /* Lepinski - fim contas imposto */
union all
select	d.cd_conta_rec_antecip cd_conta,
	'D' ie_tipo_conta,
	nvl(d.vl_antecipacao,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_rec_antecip) ds_conta_contabil,
	d.dt_antecipacao dt_mesano_referencia,
	0 vl_credito,
	d.vl_antecipacao vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
union all
select	d.cd_conta_deb_antecip cd_conta,
	'D' ie_tipo_conta,
	nvl(d.vl_pro_rata_dia,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_deb_antecip) ds_conta_contabil,
	c.dt_mesano_referencia dt_mesano_referencia,
	0 vl_credito,
	d.vl_pro_rata_dia vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and     a.dt_contabilizacao is not null /* Debita a conta de antecipa��o para poder creditar a receita*/
union all
select	d.cd_conta_rec cd_conta,
	'C' ie_tipo_conta,
	nvl(d.vl_antecipacao,0) vl_item,
	(select	substr(ds_conta_contabil,1,200)
	from	conta_contabil
	where	cd_conta_contabil = d.cd_conta_rec) ds_conta_contabil,
	d.dt_antecipacao dt_mesano_referencia,
	d.vl_antecipacao vl_credito,
	0 vl_debito
from	pls_lote_mensalidade a,
	pls_mensalidade b,
	pls_mensalidade_segurado c,
	pls_mensalidade_seg_item d
where	a.nr_sequencia	= b.nr_seq_lote
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	c.nr_sequencia	= d.nr_seq_mensalidade_seg;
/