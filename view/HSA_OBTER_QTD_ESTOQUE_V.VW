create or replace view hsa_obter_qtd_estoque_v
as
SELECT *
FROM (
SELECT 'Transfundido' ie_status, 
a.dt_fim_prod_doacao,
      a.ie_avaliacao_final,
	  a.nr_seq_doacao,
       a.nr_sangue,
       a.ds_derivado,
       a.ie_tipo_sangue,
       a.ie_fator_rh,
       a.nr_seq_derivado,
       a.nr_sec_saude,
       a.dt_doacao,
       b.dt_transfusao,
       b.nr_atendimento,
       b.cd_pessoa_fisica,
       SUBSTR(obter_nome_pessoa_fisica(b.cd_pessoa_fisica,''),1,255) nm_pessoa_fisica,
       a.ie_irradiado,
       a.ie_lavado,
       a.ie_filtrado,
       a.ie_aliquotado,
       a.dt_vencimento,
       NULL nr_seq_inutil,
       a.nr_seq_transfusao,
       NULL nr_seq_reserva,
       NULL nr_seq_emp_saida,
       a.dt_liberacao,QT_VOLUME,
       DECODE(Obter_Data_Maior(a.dt_vencimento,b.dt_transfusao),a.dt_vencimento,'N','S') ie_sangue_vencido,
       a.nr_sequencia, a.DT_EMPRESTIMO, san_obter_data_transf(nr_seq_transfusao) dt_trans_pac,
       SUBSTR(obter_nome_convenio(obter_convenio_atendimento(b.nr_atendimento)),1,100) ds_convenio,
       a.dt_utilizacao,
       NULL dt_inutilizacao,
       a.nr_transfusao_controle,
       a.ie_aferese, substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento       
FROM  san_transfusao b,
      san_producao_consulta_v a
WHERE nr_seq_transfusao = b.nr_sequencia
--AND   (((a.cd_estabelecimento = :cd_estabelecimento) and (:ie_restringe_estab = 'S')) or (:ie_restringe_estab = 'N'))
UNION
SELECT 'Reservado',
	a.dt_fim_prod_doacao,
     a.ie_avaliacao_final,
	 	  a.nr_seq_doacao,
       a.nr_sangue,
       a.ds_derivado,
       a.ie_tipo_sangue,
       a.ie_fator_rh,
       a.nr_seq_derivado,
       a.nr_sec_saude,
       a.dt_doacao,
       b.dt_reserva,
       b.nr_atendimento,
       b.cd_pessoa_fisica,
       SUBSTR(obter_nome_pessoa_fisica(b.cd_pessoa_fisica,''),1,100) nm_pessoa_fisica,
       a.ie_irradiado,
       a.ie_lavado,
       a.ie_filtrado,
       a.ie_aliquotado,
       a.dt_vencimento,
       NULL nr_seq_inutil,
       NULL nr_seq_transfusao,
       b.nr_sequencia nr_seq_reserva,
       NULL nr_seq_emp_saida,
       a.dt_liberacao,QT_VOLUME,
       DECODE(Obter_Data_Maior(a.dt_vencimento,b.dt_reserva),a.dt_vencimento,'N','S'),
       a.nr_sequencia, a.DT_EMPRESTIMO, san_obter_data_transf(a.nr_seq_transfusao) dt_trans_pac,
       SUBSTR(obter_nome_convenio(obter_convenio_atendimento(b.nr_atendimento)),1,100) ds_convenio,
       a.dt_utilizacao,
       NULL dt_inutilizacao,
       a.nr_transfusao_controle,
       a.ie_aferese, substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento
FROM  san_reserva b,
      san_reserva_prod c,
      san_producao_consulta_v a
WHERE c.nr_seq_reserva = b.nr_sequencia
  AND a.nr_sequencia = c.nr_seq_producao
  AND b.ie_status <> 'C'
  --AND   (((a.cd_estabelecimento = :cd_estabelecimento) and (:ie_restringe_estab = 'S')) or (:ie_restringe_estab = 'N'))
UNION
SELECT 'Reserva Canc.',
a.dt_fim_prod_doacao,
      a.ie_avaliacao_final,
	  	  a.nr_seq_doacao,
       a.nr_sangue,
       a.ds_derivado,
       a.ie_tipo_sangue,
       a.ie_fator_rh,
       a.nr_seq_derivado,
       a.nr_sec_saude,
       a.dt_doacao,
       b.dt_reserva,
       b.nr_atendimento,
       b.cd_pessoa_fisica,
       SUBSTR(obter_nome_pessoa_fisica(b.cd_pessoa_fisica,''),1,100) nm_pessoa_fisica,
       a.ie_irradiado,
       a.ie_lavado,
       a.ie_filtrado,
       a.ie_aliquotado,
       a.dt_vencimento,
       NULL nr_seq_inutil,
       NULL nr_seq_transfusao,
       b.nr_sequencia nr_seq_reserva,
       NULL nr_seq_emp_saida,
       a.dt_liberacao,QT_VOLUME,
       DECODE(Obter_Data_Maior(a.dt_vencimento,SYSDATE),a.dt_vencimento,'N','S'),
       a.nr_sequencia, a.DT_EMPRESTIMO, san_obter_data_transf(a.nr_seq_transfusao) dt_trans_pac,
       SUBSTR(obter_nome_convenio(obter_convenio_atendimento(b.nr_atendimento)),1,100) ds_convenio,
       a.dt_utilizacao,
       NULL dt_inutilizacao,
       a.nr_transfusao_controle,
       a.ie_aferese, substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento
FROM  san_reserva b,
      san_reserva_prod c,
      san_producao_consulta_v a
WHERE c.nr_seq_reserva = b.nr_sequencia
  AND a.nr_sequencia = c.nr_seq_producao
  AND b.ie_status = 'C'
  --AND   (((a.cd_estabelecimento = :cd_estabelecimento) and (:ie_restringe_estab = 'S')) or (:ie_restringe_estab = 'N'))
UNION
SELECT 'Inutilizado',
a.dt_fim_prod_doacao,
a.ie_avaliacao_final,
	  a.nr_seq_doacao,
       a.nr_sangue,
       a.ds_derivado,
       a.ie_tipo_sangue,
       a.ie_fator_rh,
       a.nr_seq_derivado,
       a.nr_sec_saude,
       a.dt_doacao,
       b.dt_inutilizacao,
       NULL,
       '',
       c.ds_motivo_inutilizacao,
       a.ie_irradiado,
       a.ie_lavado,
       a.ie_filtrado,
       a.ie_aliquotado,
       a.dt_vencimento,
       a.nr_seq_inutil,
       NULL nr_seq_transfusao,
       NULL nr_seq_reserva,
       NULL nr_seq_emp_saida,
       a.dt_liberacao,QT_VOLUME,
       DECODE(Obter_Data_Maior(a.dt_vencimento,SYSDATE),a.dt_vencimento,'N','S'),
       a.nr_sequencia, a.DT_EMPRESTIMO, san_obter_data_transf(a.nr_seq_transfusao) dt_trans_pac,
       '' ds_convenio,
       a.dt_utilizacao,
       b.dt_inutilizacao,
       a.nr_transfusao_controle,
       a.ie_aferese, substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento
FROM  san_motivo_inutil c,
      san_inutilizacao b,
      san_producao_consulta_v a
WHERE a.nr_seq_inutil = b.nr_sequencia
  AND b.nr_seq_motivo_inutil = c.nr_sequencia
 -- AND   (((a.cd_estabelecimento = :cd_estabelecimento) and (:ie_restringe_estab = 'S')) or (:ie_restringe_estab = 'N'))
UNION
SELECT 'Emprestado',
a.dt_fim_prod_doacao,
a.ie_avaliacao_final,
	  a.nr_seq_doacao,
       a.nr_sangue,
       a.ds_derivado,
       a.ie_tipo_sangue,
       a.ie_fator_rh,
       a.nr_seq_derivado,
       a.nr_sec_saude,
       a.dt_doacao,
       b.dt_emprestimo,
       NULL,
       '',
       c.ds_entidade,
       a.ie_irradiado,
       a.ie_lavado,
       a.ie_filtrado,
       a.ie_aliquotado,
       a.dt_vencimento,
       NULL nr_seq_inutil,
       NULL nr_seq_transfusao,
       NULL nr_seq_reserva,
       a.nr_seq_emp_saida,
       a.dt_liberacao,QT_VOLUME,
       DECODE(Obter_Data_Maior(a.dt_vencimento,SYSDATE),a.dt_vencimento,'N','S'),
       a.nr_sequencia, a.DT_EMPRESTIMO, san_obter_data_transf(a.nr_seq_transfusao) dt_trans_pac,
       '' ds_convenio,
       a.dt_utilizacao,
       NULL dt_inutilizacao,
       a.nr_transfusao_controle,
       a.ie_aferese, substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento
FROM  san_entidade c,
      san_emprestimo b,
      san_producao_consulta_v a
WHERE a.nr_seq_emp_saida = b.nr_sequencia
  AND b.nr_seq_entidade = c.nr_sequencia
--  AND   (((a.cd_estabelecimento = :cd_estabelecimento) and (:ie_restringe_estab = 'S')) or (:ie_restringe_estab = 'N'))
UNION
SELECT decode(a.dt_liberacao,null,'Pr� estoque', 'Estoque'),
a.dt_fim_prod_doacao,
a.ie_avaliacao_final,
	  a.nr_seq_doacao,
       a.nr_sangue,
       a.ds_derivado,
       a.ie_tipo_sangue,
       a.ie_fator_rh,
       a.nr_seq_derivado,
       a.nr_sec_saude,
       a.dt_doacao,
       a.dt_producao,
       NULL,
       '',
       '',
       a.ie_irradiado,
       a.ie_lavado,
       a.ie_filtrado,
       a.ie_aliquotado,
       a.dt_vencimento,
       NULL nr_seq_inutil,
       NULL nr_seq_transfusao,
       NULL nr_seq_reserva,
       NULL nr_seq_emp_saida,
       a.dt_liberacao,QT_VOLUME,
       DECODE(Obter_Data_Maior(a.dt_vencimento,SYSDATE),a.dt_vencimento,'N','S'),
       a.nr_sequencia, a.DT_EMPRESTIMO, san_obter_data_transf(a.nr_seq_transfusao) dt_trans_pac,
       '' ds_convenio,
       a.dt_utilizacao,
       NULL dt_inutilizacao,
       a.nr_transfusao_controle,
       a.ie_aferese, substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255) ds_estabelecimento
FROM  san_producao_consulta_v a
WHERE a.nr_seq_emp_saida IS NULL
  AND a.nr_seq_transfusao IS NULL
  AND a.nr_seq_inutil IS NULL
  --AND   (((a.cd_estabelecimento = :cd_estabelecimento) and (:ie_restringe_estab = 'S')) or (:ie_restringe_estab = 'N'))
  AND NOT EXISTS (SELECT 1 FROM san_reserva_prod x
                  WHERE x.nr_seq_producao = a.nr_sequencia
                  AND   x.ie_status <> 'N')) a
WHERE 1 = 1

/

