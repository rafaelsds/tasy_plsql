create or replace
view EXP_ISS_CTBA_V as
select	0					tp_registro,
	'H'					ds_registro,
	to_date(null)				dt_emissao,
	'0'					nr_nota_fiscal,
	''					cd_serie_nf,
	''					ie_tipo_recolhimento,
	''					ie_fora_municipio,
	''					cd_item_lista_serv,
	0					vl_total_nota,
	0					vl_tributo,
	substr(Elimina_Caracteres_Especiais(a.cd_inscricao_municipal),1,10) nr_inscricao_municipal,
	nvl(a.cd_cgc,'00000000000000')		cd_cgc_emitente,
	''					nr_cpf,
	a.ds_razao_social				ds_razao_social,
	''					ds_endereco,
	''					nr_endereco,
	''					ds_complemento,
	''					ds_bairro,
	''					ds_municipio,
	''					sg_estado,
	''					cd_cep,	
	0					tx_tributo,
	a.cd_estabelecimento			cd_estabelecimento,
	0					nr_sequencia,
	0					cd_operacao_nf,
	to_date(null)                           dt_entrada_saida
from	estabelecimento_v a
where	1=1
union
select	1					tp_registro,
	'C'					ds_registro,
	a.dt_emissao			dt_emissao,
	a.nr_nota_fiscal				nr_nota_fiscal,
	a.cd_serie_nf				cd_serie_nf,
	''					ie_tipo_recolhimento,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	''					cd_item_lista_serv,
	a.vl_mercadoria				vl_total_nota,
	0					vl_tributo,
	Elimina_Caracteres_Especiais(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'IM'),1,10)) nr_inscricao_municipal,
	nvl(a.cd_cgc,'00000000000000')		cd_cgc_emitente,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,100) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CEP')),1,08) cd_cep,
	0					tx_tributo,
	a.cd_estabelecimento			cd_estabelecimento,
	a.nr_sequencia				nr_sequencia,
	a.cd_operacao_nf,
	a.dt_entrada_saida                      dt_entrada_saida
from	nota_fiscal a
where	a.ie_situacao in ('3','9')
and	length(a.nr_nota_fiscal) < 9
and	a.dt_atualizacao_estoque is not null
and	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'S' 
union
select	2					tp_registro,
	'E'					ds_registro,
	a.dt_emissao			dt_emissao,
	a.nr_nota_fiscal				nr_nota_fiscal,
	a.cd_serie_nf				cd_serie_nf,
	(select decode(count(*),0,'N','S')
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia)	ie_tipo_recolhimento,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	(select max(nvl(cd_darf, substr(obter_codigo_darf(x.cd_tributo, a.cd_estabelecimento, a.cd_cgc,a.cd_pessoa_fisica),1,100)))
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia) cd_item_lista_serv,	
	a.vl_mercadoria				vl_total_nota,
	0					vl_tributo,
	Elimina_Caracteres_Especiais(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'IM'),1,10)) nr_inscricao_municipal,
	nvl(a.cd_cgc,'00000000000000')		cd_cgc_emitente,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,100) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CEP')),1,08) cd_cep,
	(select nvl(max(x.tx_tributo),0)
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia) tx_tributo,
	a.cd_estabelecimento			cd_estabelecimento,
	a.nr_sequencia				nr_sequencia,
	a.cd_operacao_nf,
	a.dt_entrada_saida        dt_entrada_saida
from	nota_fiscal a
where	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'S'
and	a.ie_situacao = '1'
and	length(a.nr_nota_fiscal) < 9
and	a.dt_atualizacao_estoque is not null
union
select	3					tp_registro,
	'R'					ds_registro,
	a.dt_emissao			dt_emissao,
	a.nr_nota_fiscal				nr_nota_fiscal,
	a.cd_serie_nf				cd_serie_nf,
	(select decode(count(*),0,'N','S')
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia)	ie_tipo_recolhimento,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	(select	max(nvl(cd_darf, substr(obter_codigo_darf(x.cd_tributo, a.cd_estabelecimento, a.cd_cgc_emitente,a.cd_pessoa_fisica),1,100)))
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia) cd_item_lista_serv,
	a.vl_mercadoria				vl_total_nota,
	0					vl_tributo,
	Elimina_Caracteres_Especiais(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'IM'),1,10)) nr_inscricao_municipal,
	nvl(a.cd_cgc_emitente,'00000000000000')		cd_cgc_emitente,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'N'),1,40) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc_emitente,'CEP')),1,08) cd_cep,
	(select nvl(max(x.tx_tributo),0)
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia)	tx_tributo,
	a.cd_estabelecimento			cd_estabelecimento,
	a.nr_sequencia				nr_sequencia,
	a.cd_operacao_nf,
	a.dt_entrada_saida                           dt_entrada_saida
from	nota_fiscal a
where	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'E' 
and	length(a.nr_nota_fiscal) < 9
and	a.ie_situacao = '1'
and 	obter_se_exporta_nf_entrada(a.nr_sequencia) = 'S'
and	a.dt_atualizacao_estoque is not null
union
select	4					tp_registro,
	'T'					ds_registro,
	to_date(null)				dt_emissao,
	'0'					nr_nota_fiscal,
	''					cd_serie_nf,
	''					ie_tipo_recolhimento,
	''					ie_fora_municipio,
	''					cd_item_lista_serv,
	0					vl_total_nota,
	0					vl_tributo,
	substr(Elimina_Caracteres_Especiais(a.cd_inscricao_municipal),1,10) nr_inscricao_municipal,
	nvl(a.cd_cgc,'00000000000000')		cd_cnpj,
	''					nr_cpf,
	a.ds_razao_social				ds_razao_social,
	''					ds_endereco,
	''					nr_endereco,
	''					ds_complemento,
	''					ds_bairro,
	''					ds_municipio,
	''					sg_estado,
	''					cd_cep,
	0					tx_tributo,
	a.cd_estabelecimento			cd_estabelecimento,
	0					nr_sequencia,
	0					cd_operacao_nf,
	to_date(null)                           dt_entrada_saida
from	estabelecimento_v a
where	1=1;
/
