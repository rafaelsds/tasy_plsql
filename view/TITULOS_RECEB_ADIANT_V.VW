CREATE OR REPLACE VIEW Titulos_Receb_adiant_v
AS
SELECT OBTER_NOME_FANTASIA_ESTAB(b.cd_estabelecimento) ESTABELECIMENTO,
    TRUNC(a.dt_recebimento,'dd')  dt_dia,
	SUBSTR(obter_nome_convenio(b.cd_convenio),1,40)  nm_convenio,
	b.dt_pagamento_previsto dt_pagto_prev, 
    a.vl_nota_credito vl_nt_cred,
	b.nr_nota_fiscal nr_nf,
	c.ds_tipo_recebimento ds_tipo_receb,
	TO_CHAR(x.cd_banco, '000') || ' / ' || x.cd_conta || ' - ' || x.ie_digito_conta || ' - ' 
|| DECODE(x.nr_seq_tipo_conta,1,'Inv.', 'CC') ds_conta,
	SUBSTR(obter_desc_trans_financ(a.nr_seq_trans_fin),1,35) ds_transacao,
	TRUNC(a.dt_recebimento,'dd') dt_recebimento,
	a.nr_titulo nr_titulo,
	NVL(SUM(a.vl_recebido),0) vl_recebido,
	NVL(SUM(a.vl_descontos),0) vl_descontos,
	NVL(SUM(a.vl_rec_maior),0) vl_rec_maior,
	NVL(SUM(a.vl_glosa),0) vl_glosa,
	NVL(SUM(a.vl_juros),0) vl_juros,
	NVL(SUM(a.vl_multa),0) vl_multa,
	NVL(SUM(a.vl_perdas),0) vl_perdas,
                NVL(SUM(a.vl_cambial_ativo),0) vl_cambial_ativo,
                NVL(SUM(a.vl_cambial_passivo),0) vl_cambial_passivo,
	d.nr_seq_grupo nr_seq_grupo,
	OBTER_PROD_FINANC(B.NR_TITULO,'TR','S') sub_grupo,
	d.ds_produto ds_produto,
	d.nr_sequencia NR_SEQ_PROD,
	b.cd_convenio cd_convenio,
	b.cd_portador cd_portador,
	b.cd_tipo_portador cd_tipo_portador,
	a.cd_tipo_recebimento cd_tipo_recebimento,
	b.ie_situacao	ie_situacao,
	b.ie_tipo_titulo ie_tipo_titulo,
	a.nr_seq_conta_banco nr_seq_conta_banco,
	a.nr_seq_trans_fin nr_seq_trans_fin,
	t.cd_conta_financ cd_conta_financ
FROM 	tipo_recebimento c,
	titulo_receber_v b,
        	titulo_receber_liq a,
	banco_estabelecimento_v x,
	titulo_receber_classif t,
	produto_financeiro d
WHERE 	t.nr_seq_produto = d.nr_sequencia(+)
AND	a.nr_titulo			= b.nr_titulo
AND	a.nr_titulo			= b.nr_titulo
AND b.CD_ESTABELECIMENTO <> 41
AND	a.cd_tipo_recebimento	= c.cd_tipo_recebimento
AND	a.nr_seq_conta_banco = x.nr_sequencia
AND	b.nr_titulo = t.nr_titulo(+)
AND	A.NR_SEQ_TRANS_FIN IS NOT NULL
AND	NVL(t.nr_sequencia,0)= (SELECT	NVL(MIN(z.nr_sequencia),0)
	FROM	titulo_receber_classif z
	WHERE	z.nr_titulo	= a.nr_titulo/*
		*/)
HAVING	COUNT(*) > 0
GROUP BY	b.cd_estabelecimento, TRUNC(a.dt_recebimento,'dd'),
	SUBSTR(obter_nome_convenio(b.cd_convenio),1,40),
	b.dt_pagamento_previsto,
                a.vl_nota_credito, 
	b.nr_nota_fiscal,
	c.ds_tipo_recebimento,
	TO_CHAR(x.cd_banco, '000') || ' / ' || x.cd_conta || ' - ' || x.ie_digito_conta || ' - ' 
|| DECODE(x.nr_seq_tipo_conta,1,'Inv.', 'CC'),
	SUBSTR(obter_desc_trans_financ(a.nr_seq_trans_fin),1,35),
	TRUNC(a.dt_recebimento,'dd'),
	a.nr_titulo,
	d.nr_seq_grupo,
	d.ds_produto,
	B.NR_TITULO,
	d.nr_sequencia,
	b.cd_convenio ,
	b.cd_portador ,
	b.cd_tipo_portador ,
	a.cd_tipo_recebimento ,
	b.ie_situacao	,
	b.ie_tipo_titulo ,
	a.nr_seq_conta_banco ,
	a.nr_seq_trans_fin,
	t.cd_conta_financ
UNION ALL
SELECT 	OBTER_NOME_FANTASIA_ESTAB(b.cd_estabelecimento) ESTABELECIMENTO,
     TRUNC(a.dt_recebimento,'dd')  dt_dia,
	SUBSTR(obter_nome_convenio(b.cd_convenio),1,40)  nm_convenio,
	b.dt_pagamento_previsto dt_pagto_prev,
                a.vl_nota_credito vl_nt_cred,
	b.nr_nota_fiscal nr_nf,
	c.ds_tipo_recebimento ds_tipo_receb,
	' ' ds_conta,
	SUBSTR(obter_desc_trans_financ(a.nr_seq_trans_fin),1,35) ds_transacao,
	TRUNC(a.dt_recebimento,'dd') dt_recebimento,
	a.nr_titulo nr_titulo,
	NVL(SUM(a.vl_recebido),0) vl_recebido,
	NVL(SUM(a.vl_descontos),0) vl_descontos,
	NVL(SUM(a.vl_rec_maior),0) vl_rec_maior,
	NVL(SUM(a.vl_glosa),0) vl_glosa,
	NVL(SUM(a.vl_juros),0) vl_juros,
	NVL(SUM(a.vl_multa),0) vl_multa,
	NVL(SUM(a.vl_perdas),0) vl_perdas,
                NVL(SUM(a.vl_cambial_ativo),0) vl_cambial_ativo,
                NVL(SUM(a.vl_cambial_passivo),0) vl_cambial_passivo,
	d.nr_seq_grupo nr_seq_grupo,
	OBTER_PROD_FINANC(B.NR_TITULO,'TR','S') sub_grupo,
	d.ds_produto ds_produto,
	d.nr_sequencia NR_SEQ_PROD,
	b.cd_convenio cd_convenio,
	b.cd_portador cd_portador,
	b.cd_tipo_portador cd_tipo_portador,
	a.cd_tipo_recebimento cd_tipo_recebimento,
	b.ie_situacao	ie_situacao,
	b.ie_tipo_titulo ie_tipo_titulo,
	a.nr_seq_conta_banco nr_seq_conta_banco,
	a.nr_seq_trans_fin nr_seq_trans_fin,
	t.cd_conta_financ cd_conta_financ
FROM 	tipo_recebimento c,
	titulo_receber_v b,
        	titulo_receber_liq a,
	titulo_receber_classif t,
	produto_financeiro d 
WHERE 	t.nr_seq_produto = d.nr_sequencia(+)
AND	a.nr_titulo			= b.nr_titulo
AND        b.CD_ESTABELECIMENTO <> 41
AND	a.cd_tipo_recebimento	= c.cd_tipo_recebimento
AND	b.nr_titulo = t.nr_titulo(+)
AND	a.nr_seq_conta_banco IS NULL
AND	A.NR_SEQ_TRANS_FIN IS NOT NULL
AND	NVL(t.nr_sequencia,0)= (SELECT	NVL(MIN(z.nr_sequencia),0)
		FROM	titulo_receber_classif z
		WHERE	z.nr_titulo	= a.nr_titulo/*
		*/)
HAVING	COUNT(*) > 0
GROUP BY	b.cd_estabelecimento, TRUNC(a.dt_recebimento,'dd'),
SUBSTR(obter_nome_convenio(b.cd_convenio),1,40),
b.dt_pagamento_previsto,
a.vl_nota_credito,
b.nr_nota_fiscal,
c.ds_tipo_recebimento,
SUBSTR(obter_desc_trans_financ(a.nr_seq_trans_fin),1,35),
TRUNC(a.dt_recebimento,'dd'),
a.nr_titulo,
d.nr_seq_grupo,
d.ds_produto,
B.NR_TITULO,
d.nr_sequencia,
b.cd_convenio ,
b.cd_portador ,
b.cd_tipo_portador ,
a.cd_tipo_recebimento ,
b.ie_situacao	,
b.ie_tipo_titulo ,
a.nr_seq_conta_banco ,
a.nr_seq_trans_fin,
t.cd_conta_financ 
ORDER BY 	1,2,3,4
/