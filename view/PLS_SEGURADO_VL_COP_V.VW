create or replace view pls_segurado_vl_cop_v as
select	x.nr_seq_titular,
	x.nr_sequencia,
	decode(x.nr_seq_titular, null, 'T', 'D') ie_titularidade,
	(select	count(1)
	from	pls_segurado u
	where	u.nr_sequencia	= x.nr_sequencia
	and	u.nr_seq_titular is null
	and exists(	(	select 	1 
				from	pls_conta_proc_v a,
					pls_conta_coparticipacao b
				where	a.nr_seq_segurado = u.nr_sequencia
				and  a.IE_STATUS_CONTA = 'F'
				and	b.nr_seq_conta_proc = a.nr_sequencia
				and	b.nr_seq_mensalidade_seg is null)
		union all
			(	select	1
				from 	pls_conta_mat_v a,
					pls_conta_coparticipacao b
				where 	a.nr_seq_segurado = u.nr_sequencia
				and  a.IE_STATUS_CONTA = 'F'
				and	b.nr_seq_conta_mat = a.nr_seq_segurado
				and	b.nr_seq_mensalidade_seg is null)
		union all
			(	select 	1
				from	pls_guia_plano a,
					pls_guia_plano_mat b,
					pls_guia_coparticipacao c
				where	a.nr_seq_segurado = u.nr_sequencia
				and	a.ie_status = '1'
				and	nvl(a.ie_utilizado,'N') = 'N'
				and	b.nr_seq_guia = a.nr_sequencia
				and	c.nr_seq_guia_mat = b.nr_sequencia) 
		union all
			(	select 	1
				from	pls_guia_plano a,
					pls_guia_plano_proc b,
					pls_guia_coparticipacao c
				where	a.nr_seq_segurado = u.nr_sequencia
				and	a.ie_status = '1'
				and	nvl(a.ie_utilizado,'N') = 'N'
				and 	b.nr_seq_guia = a.nr_sequencia
				and	c.nr_seq_guia_proc = b.nr_sequencia) 
				)) qt_titularidade,
	((select	count(1)
	 from		pls_conta_v	d
	 where		d.nr_seq_segurado = x.nr_sequencia
	 and		d.ie_status = 'F'
	 and		exists  (select	1
				from	pls_conta_proc_v z
				where	z.nr_seq_conta = d.nr_sequencia
				and	z.ie_status <> 'D'
				and	exists (select	1
						from	pls_conta_coparticipacao w
						where	w.nr_seq_conta_proc = z.nr_sequencia
						and	w.nr_seq_mensalidade_seg is null)
				union all 
				select	1
				from	pls_conta_mat_v z
				where	z.nr_seq_conta = d.nr_sequencia
					and 	z.ie_status <> 'D'
					and	exists (select	1
							from	pls_conta_coparticipacao w
							where	w.nr_seq_conta_mat = z.nr_sequencia
							and	w.nr_seq_mensalidade_seg is null))) 
	+
	(select	count(1)
	 from		pls_conta_v	d,
			pls_segurado	f
	 where		d.nr_seq_segurado = f.nr_sequencia
	 and		f.nr_seq_titular	= x.nr_sequencia
	 and		d.ie_status = 'F'
	 and		exists  (select	1
				from	pls_conta_proc_v z
				where	z.nr_seq_conta = d.nr_sequencia
				and	z.ie_status <> 'D'
				and	exists (select	1
						from	pls_conta_coparticipacao w
						where	w.nr_seq_conta_proc = z.nr_sequencia
						and	w.nr_seq_mensalidade_seg is null)
				union all 
				select	1
				from	pls_conta_mat_v z
				where	z.nr_seq_conta = d.nr_sequencia
					and 	z.ie_status <> 'D'
					and	exists (select	1
							from	pls_conta_coparticipacao w
							where	w.nr_seq_conta_mat = z.nr_sequencia
							and	w.nr_seq_mensalidade_seg is null))) 
	+
	(select	count(1)
	from	pls_guia_plano d
	where	d.nr_seq_segurado = x.nr_sequencia
	and	d.ie_status = '1'
	and	nvl(d.ie_utilizado,'N') = 'N'
	and	exists (	select	1
				from	pls_guia_plano_proc z
				where	z.nr_seq_guia = d.nr_sequencia
				and	exists ( 	select	1
							from	pls_guia_coparticipacao w
							where	w.nr_seq_guia_proc = z.nr_sequencia)
				union all
				select	1
				from	pls_guia_plano_mat z
				where	z.nr_seq_guia = d.nr_sequencia
				and	exists ( 	select	1
							from	pls_guia_coparticipacao w
							where	w.nr_seq_guia_mat = z.nr_sequencia)))
	+
	(select	count(1)
	from	pls_guia_plano d,
		pls_segurado	f
	where	d.nr_seq_segurado = f.nr_sequencia
	and	f.nr_seq_titular	= x.nr_sequencia
	and	d.ie_status = '1'
	and	nvl(d.ie_utilizado,'N') = 'N'
	and	exists (	select	1
				from	pls_guia_plano_proc z
				where	z.nr_seq_guia = d.nr_sequencia
				and	exists ( 	select	1
							from	pls_guia_coparticipacao w
							where	w.nr_seq_guia_proc = z.nr_sequencia)
				union all
				select	1
				from	pls_guia_plano_mat z
				where	z.nr_seq_guia = d.nr_sequencia
				and	exists ( 	select	1
							from	pls_guia_coparticipacao w
							where	w.nr_seq_guia_mat = z.nr_sequencia)))) qt_guias,
	((select nvl(sum(z.qt_procedimento), 0)
	  from	pls_conta_proc_v z
	  where	z.nr_seq_segurado = x.nr_sequencia
	  and	z.ie_status <> 'D'
	  and	exists ( select	1
			 from	pls_conta_coparticipacao w
			 where	w.nr_seq_conta_proc = z.nr_sequencia
			 and	w.nr_seq_mensalidade_seg is null))
	+
	(select nvl(sum(z.qt_procedimento), 0)
	  from	pls_conta_proc_v z,
		pls_segurado	f
	  where	z.nr_seq_segurado = f.nr_sequencia
	  and	f.nr_seq_titular	= x.nr_sequencia
	  and	z.ie_status <> 'D'
	  and	exists ( select	1
			 from	pls_conta_coparticipacao w
			 where	w.nr_seq_conta_proc = z.nr_sequencia
			 and	w.nr_seq_mensalidade_seg is null))
	+
	(select	nvl(sum(z.qt_material), 0)
	 from	pls_conta_mat_v z
	 where	z.nr_seq_segurado = x.nr_sequencia
	 and	z.ie_status <> 'D'
	 and	exists (select	1
			from	pls_conta_coparticipacao w
			where	w.nr_seq_conta_mat = z.nr_sequencia
			and	w.nr_seq_mensalidade_seg is null))
	+
	(select	nvl(sum(z.qt_autorizada),0)
	from	pls_guia_plano w,
		pls_guia_plano_proc z
	where	w.nr_seq_segurado = x.nr_sequencia
	and	w.ie_status = '1'
	and	nvl(w.ie_utilizado,'N') = 'N'
	and	z.nr_seq_guia = w.nr_sequencia
	and	exists	(	select	1
				from	pls_guia_coparticipacao n
				where	n.nr_seq_guia_proc = z.nr_sequencia))
	+
	(select	nvl(sum(z.qt_autorizada),0)
	from	pls_guia_plano w,
		pls_guia_plano_proc z,
		pls_segurado	f
	where	w.nr_seq_segurado = f.nr_sequencia
	  and	f.nr_seq_titular	= x.nr_sequencia
	and	w.ie_status = '1'
	and	nvl(w.ie_utilizado,'N') = 'N'
	and	z.nr_seq_guia = w.nr_sequencia
	and	exists	(	select	1
				from	pls_guia_coparticipacao n
				where	n.nr_seq_guia_proc = z.nr_sequencia))
	+
	(select	nvl(sum(z.qt_autorizada),0)
	from	pls_guia_plano w,
		pls_guia_plano_mat z
	where	w.nr_seq_segurado = x.nr_sequencia
	and	w.ie_status = '1'
	and	nvl(w.ie_utilizado,'N') = 'N'
	and	z.nr_seq_guia = w.nr_sequencia
	and	exists	(	select	1
				from	pls_guia_coparticipacao n
				where	n.nr_seq_guia_mat = z.nr_sequencia)))	qt_itens_consid,
	((select nvl(sum(f.VL_COPARTICIPACAO), 0)
	from	pls_conta_mat_v z,
		pls_conta_coparticipacao f
	where	z.nr_seq_segurado = x.nr_sequencia
	and  z.IE_STATUS_CONTA = 'F'
	and	f.nr_seq_conta_mat = z.nr_sequencia
	and	f.nr_seq_mensalidade_seg is null) +
	(select	nvl(sum(f.VL_COPARTICIPACAO), 0)
	from	pls_conta_proc_v z,
		pls_conta_coparticipacao f
	where	z.nr_seq_segurado = x.nr_sequencia
	and  z.IE_STATUS_CONTA = 'F'
	and	f.nr_seq_conta_proc = z.nr_sequencia
	and	f.nr_seq_mensalidade_seg is null) +
	(select	nvl(sum(f.VL_COPARTICIPACAO), 0)
	from	pls_conta_proc_v z,
		pls_conta_coparticipacao f,
		pls_segurado y
	where	z.nr_seq_segurado = y.nr_sequencia
	  and	y.nr_seq_titular	= x.nr_sequencia
	and  z.IE_STATUS_CONTA = 'F'
	and	f.nr_seq_conta_proc = z.nr_sequencia
	and	f.nr_seq_mensalidade_seg is null) +
	((select nvl(sum(f.VL_COPARTICIPACAO), 0)
	from	pls_guia_plano s,
		pls_guia_plano_mat z,
		pls_guia_coparticipacao f
	where	s.nr_seq_segurado = x.nr_sequencia
	and	s.ie_status = '1'
	and	nvl(s.ie_utilizado,'N') = 'N'
	and	z.nr_seq_guia = s.nr_sequencia
	and	f.nr_seq_guia_mat = z.nr_sequencia) +
	(select	nvl(sum(f.VL_COPARTICIPACAO), 0)
	from	pls_guia_plano s,
		pls_guia_plano_proc z,
		pls_guia_coparticipacao f
	where	s.nr_seq_segurado = x.nr_sequencia
	and	s.ie_status = '1'
	and	nvl(s.ie_utilizado,'N') = 'N'
	and 	z.nr_seq_guia = s.nr_sequencia
	and	f.nr_seq_guia_proc = z.nr_sequencia)+
	(select	nvl(sum(f.VL_COPARTICIPACAO), 0)
	from	pls_guia_plano s,
		pls_guia_plano_proc z,
		pls_guia_coparticipacao f,
		pls_segurado	y
	where	s.nr_seq_segurado = y.nr_sequencia
	  and	y.nr_seq_titular	= x.nr_sequencia
	and	s.ie_status = '1'
	and	nvl(s.ie_utilizado,'N') = 'N'
	and 	z.nr_seq_guia = s.nr_sequencia
	and	f.nr_seq_guia_proc = z.nr_sequencia))) vl_copartic,
	(select	count(1)
	from	table(pls_util_cta_pck.obter_dependentes_benef(x.nr_sequencia, 'S', 'N')) u
	where exists(	(	select 	1 
				from	pls_conta_proc_v a,
					pls_conta_coparticipacao b
				where	a.nr_seq_segurado = u.nr_seq_segurado
				and  a.IE_STATUS_CONTA = 'F'
				and	b.nr_seq_conta_proc = a.nr_sequencia
				and	b.nr_seq_mensalidade_seg is null)
		union all
			(	select	1
				from 	pls_conta_mat_v a,
					pls_conta_coparticipacao b
				where 	a.nr_seq_segurado = u.nr_seq_segurado
				and  a.IE_STATUS_CONTA = 'F'
				and	b.nr_seq_conta_mat = a.nr_seq_segurado
				and	b.nr_seq_mensalidade_seg is null)
		union all
			(	select 	1
				from	pls_guia_plano a,
					pls_guia_plano_mat b,
					pls_guia_coparticipacao c
				where	a.nr_seq_segurado = u.nr_seq_segurado
				and	a.ie_status = '1'
				and	nvl(a.ie_utilizado,'N') = 'N'
				and	b.nr_seq_guia = a.nr_sequencia
				and	c.nr_seq_guia_mat = b.nr_sequencia) 
		union all
			(	select 	1
				from	pls_guia_plano a,
					pls_guia_plano_proc b,
					pls_guia_coparticipacao c
				where	a.nr_seq_segurado = u.nr_seq_segurado
				and	a.ie_status = '1'
				and	nvl(a.ie_utilizado,'N') = 'N'
				and 	b.nr_seq_guia = a.nr_sequencia
				and	c.nr_seq_guia_proc = b.nr_sequencia) 
				)) qt_dependente
from	pls_segurado x;
/
