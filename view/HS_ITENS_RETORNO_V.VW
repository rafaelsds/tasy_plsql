create or replace view HS_ITENS_RETORNO_V as
select 	1 ie_mat_proc,
	substr(obter_desc_propaci(e.nr_Sequencia),1,255) ds_item,
	e.qt_procedimento qt_item,
	nvl(e.vl_procedimento,0) / nvl(e.qt_procedimento,1) vl_unitario,
	e.vl_procedimento vl_item,
	f.ds_motivo_glosa ds_motivo_glosa,
	d.dt_atualizacao dt_atualizacao,
	a.dt_retorno dt_retorno,
	c.nr_atendimento nr_atendimento,
	e.cd_procedimento cd_procedimento,
	0 cd_material,
	f.ie_acao_glosa ie_acao_glosa,
	Obter_Tipo_Atendimento(c.nr_atendimento) ie_tipo_atend
from 	motivo_glosa f,
	procedimento_paciente e,
	convenio_retorno_glosa d,
	conta_paciente c,
	convenio_retorno_item b,
	convenio_retorno a
where 	a.nr_sequencia		= b.nr_Seq_retorno 
and 	b.nr_interno_conta	= c.nr_interno_conta 
and 	b.nr_sequencia		= d.nr_Seq_ret_item 
and 	d.nr_Seq_propaci	= e.nr_Sequencia 
and 	d.cd_motivo_glosa	= f.cd_motivo_glosa(+) 
and 	e.vl_procedimento	> 0 
union all 
select 	2,
	substr(obter_desc_matpaci(e.nr_Sequencia),1,255),
	e.qt_material,
	e.vl_unitario,
	e.vl_material,
	f.ds_motivo_glosa,
	d.dt_atualizacao,
	a.dt_retorno,
	c.nr_atendimento,
	0,
	e.cd_material,
	f.ie_acao_glosa,
	Obter_Tipo_Atendimento(c.nr_atendimento)
from 	motivo_glosa f,
	material_atend_paciente e,
	convenio_retorno_glosa d,
	conta_paciente c,
	convenio_retorno_item b,
	convenio_retorno a
where 	a.nr_sequencia		= b.nr_Seq_retorno 
and 	b.nr_interno_conta	= c.nr_interno_conta 
and 	b.nr_sequencia		= d.nr_Seq_ret_item 
and 	d.nr_Seq_matpaci	= e.nr_Sequencia 
and 	d.cd_motivo_glosa	= f.cd_motivo_glosa(+) 
and 	e.vl_material		> 0 
union all 
select 	1,
	substr(obter_descricao_procedimento(d.cd_procedimento,d.ie_origem_proced),1,255),
	nvl(d.qt_cobrada,d.qt_glosa),
	nvl(d.vl_cobrado,d.vl_glosa) / decode(nvl(d.qt_cobrada,d.qt_glosa),0,1,nvl(d.qt_cobrada,d.qt_glosa)),
	0,
	e.ds_motivo_glosa,
	d.dt_atualizacao,
	a.dt_retorno,
	c.nr_atendimento,
	d.cd_procedimento,
	0,
	e.ie_acao_glosa,
	Obter_Tipo_Atendimento(c.nr_atendimento)
from 	motivo_glosa e,
	convenio_retorno_glosa d,
	conta_paciente c,
	convenio_retorno_item b,
	convenio_retorno a 
where 	a.nr_sequencia		= b.nr_Seq_retorno 
and 	b.nr_interno_conta	= c.nr_interno_conta 
and 	b.nr_sequencia		= d.nr_Seq_ret_item
and 	d.nr_seq_propaci 	is null 
and 	d.cd_procedimento 	is not null 
and 	d.cd_motivo_glosa	= e.cd_motivo_glosa(+) 
and 	nvl(d.vl_cobrado,d.vl_glosa) >0 
union all 
select  2,
	substr(obter_desc_material(d.cd_material),1,255), 
	nvl(d.qt_cobrada,d.qt_glosa),
	nvl(d.vl_cobrado,d.vl_glosa) / decode(nvl(d.qt_cobrada,d.qt_glosa),0,1,nvl(d.qt_cobrada,d.qt_glosa)),
	0,
	e.ds_motivo_glosa,
	d.dt_atualizacao,
	a.dt_retorno,
	c.nr_atendimento,
	0,
	d.cd_material,
	e.ie_acao_glosa,
	Obter_Tipo_Atendimento(c.nr_atendimento)
from 	motivo_glosa e,
	convenio_retorno_glosa d,	
	conta_paciente c,
	convenio_retorno_item b,
	convenio_retorno a
where 	a.nr_sequencia		= b.nr_Seq_retorno 
and 	b.nr_interno_conta	= c.nr_interno_conta 
and 	b.nr_sequencia		= d.nr_Seq_ret_item 
and 	d.nr_seq_matpaci 	is null 
and 	d.cd_material 		is not null 
and 	d.cd_motivo_glosa	= e.cd_motivo_glosa(+) 
and 	nvl(d.vl_cobrado,d.vl_glosa) > 0 
order by 1,3,4;
/