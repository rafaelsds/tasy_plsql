create or replace view hmj_laboratorio_v 
as select 	'0' cd_tipo_registro,
		'00011122'cd_registro,
		c.nr_cpf,
		a.dt_solic_prescr dt_solicitacao,
		b.dt_prev_execucao dt_realizacao,
		--b.nr_seq_exame,
		--p.cd_integracao nr_seq_exame,
		i.cd_integracao nr_seq_exame,
		' 'ie_parecer,
		' 'ds_laudo1,
		Converte_Acentuacao_Rtf_char(obter_dados_laudo_paciente(d.nr_sequencia, 'L')) ds_laudo2,
		--d.ds_laudo ds_laudo2,
		a.nr_prescricao,
		d.ds_titulo_laudo
from    pessoa_fisica c,
	prescr_medica a,
	prescr_procedimento b,
	laudo_paciente d,
	procedimento_paciente e,
	proc_interno p,
	regra_proc_interno_integra i
where  	a.nr_prescricao 	= b.nr_prescricao
and 	a.cd_pessoa_fisica 	= c.cd_pessoa_fisica
and 	b.nr_prescricao 	= d.nr_prescricao
and 	d.nr_seq_proc 	= e.nr_sequencia
and 	p.nr_sequencia 	= i.nr_seq_proc_interno
and	i.ie_tipo_integracao	= 8
and 	e.nr_sequencia_prescricao = b.nr_sequencia
and 	e.nr_prescricao 	= b.nr_prescricao
and 	p.nr_sequencia 	= b.nr_seq_proc_interno;
/
