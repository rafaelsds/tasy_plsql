create or replace
view	OBTER_DADOS_GRG_V as
select	a.nr_sequencia,
	a.nr_seq_lote_hist nr_seq_lote_hist,
	obter_nome_paciente(e.nr_atendimento) nm_paciente,
	obter_cd_usuario_conv_glosa2(e.nr_atendimento,2) cd_usuario,
	a.cd_autorizacao,
	to_number(obter_dados_propaci('CP',nr_Seq_propaci)) cd_item,
	substr(obter_desc_propaci(b.nr_seq_propaci),1,255) ds_item,
	b.qt_item,
	to_number(obter_dados_lote_audit_item(b.nr_sequencia,'BO')) vl_original,
	b.vl_pago,
	b.vl_glosa_informada,
	b.vl_glosa,
	b.vl_amenor,
	b.ds_observacao
from	lote_audit_hist_item b,
	lote_audit_hist_guia a,
	conta_paciente e
where	b.nr_Seq_guia 		= a.nr_sequencia
and	a.nr_interno_conta 	= e.nr_interno_conta
and	to_number(obter_dados_propaci('CP',nr_Seq_propaci)) is not null
union
select	a.nr_sequencia,
	a.nr_seq_lote_hist nr_seq_lote_hist,
	obter_nome_paciente(e.nr_atendimento) nm_paciente,
	obter_cd_usuario_conv_glosa2(e.nr_atendimento,2) cd_usuario,
	a.cd_autorizacao,
	to_number(obter_dados_lote_audit_item(b.nr_Sequencia,'CMGRG')) cd_item,
	substr(obter_desc_matpaci(b.nr_seq_matpaci),1,255) ds_item,
	b.qt_item,
	to_number(obter_dados_lote_audit_item(b.nr_sequencia,'BO')) vl_original,
	b.vl_pago,
	b.vl_glosa_informada,
	b.vl_glosa,
	b.vl_amenor,
	b.ds_observacao
from	lote_audit_hist_item b,
	lote_audit_hist_guia a,
	conta_paciente e
where	b.nr_Seq_guia 		= a.nr_sequencia
and	a.nr_interno_conta 	= e.nr_interno_conta
and	to_number(obter_dados_propaci('CP',nr_Seq_propaci)) is null
/