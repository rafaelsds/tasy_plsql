create or replace view tabela_atributo_ajuste_v as
SELECT 	a.nm_atributo, 
	a.ie_obrigatorio,
	a.ie_tipo_atributo,
	a.qt_tamanho,
	a.qt_decimais,
	a.vl_default,
	a.nm_tabela,
	a.nr_sequencia_criacao
FROM	tabela_atributo a,
	tabela_sistema	c
WHERE	a.ie_tipo_atributo 	not in ('FUNCTION','VISUAL')
AND	a.nm_tabela		= c.nm_tabela
AND	gerar_objeto_aplicacao(c.ds_aplicacao) = 'S'
AND	NOT EXISTS ( 	SELECT 	1
	  	  	FROM	user_tab_columns b
			WHERE	a.nm_atributo = b.column_name
			AND	a.nm_tabela   = b.table_name
			AND	b.NULLABLE    <> DECODE(a.ie_obrigatorio,'S','Y','N'));
/
