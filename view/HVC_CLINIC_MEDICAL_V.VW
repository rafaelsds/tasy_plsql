create or replace view hvc_clinic_medical_v
as
select	0 cd_tipo_registro,
	sysdate dt_geracao,
	to_char(sysdate, 'hh24mi') hr_geracao,
	null cd_convenio,
	null cd_procedimento,
	null qt_procedimento,
	null vl_procedimento,
	-87 cd_setor_atendimento,
	null dt_procedimento,
	null vl_filme,
	null vl_custo_operacional,
	null cd_senha,
	null nr_prontuario,
	null nr_atendimento,
	null dt_entrada,	
	null hr_entrada,
	null vl_materiais,
	null nm_paciente,
	null nr_guia,
	null vl_ch,
	null dt_autorizacao,
	null qt_ch,
	null  nr_matricula 
from	dual
union all
select	1 cd_tipo_registro,
	sysdate dt_geracao,
	to_char(sysdate, 'hh24mi') hr_geracao,
	a.cd_convenio,
	a.cd_procedimento,
	a.qt_procedimento,
	a.vl_procedimento,
	c.cd_setor_atendimento,
	a.dt_procedimento,
	a.vl_materiais vl_filme,
	a.vl_custo_operacional,
	a.cd_senha,
	e.nr_prontuario,
	w.nr_atendimento,
	w.dt_entrada,
	to_char(w.dt_entrada, 'hh24mi') hr_entrada,
	a.vl_materiais,
	e.nm_pessoa_fisica nm_paciente,
	a.nr_doc_convenio nr_guia,
	(obter_ch_conv_amb(i.cd_convenio_parametro, i.cd_categoria_parametro, i.cd_estabelecimento, a.cd_edicao_amb, a.dt_procedimento) * a.qt_procedimento) vl_ch,
	nvl(k.dt_autorizacao, sysdate) dt_autorizacao,
	obter_proc_paciente_valor(a.nr_sequencia,2,1) qt_ch,
	substr(obter_matricula_usuario(i.cd_convenio_parametro, a.nr_atendimento),1,20) nr_matricula 
from	tipo_acomodacao h,
	atendimento_paciente w,
	setor_atendimento c,   
	procedimento b,
	pessoa_fisica e,
	atend_paciente_unidade d,
	proc_paciente_convenio g,
	proc_paciente_valor f,
	procedimento_paciente a,
	conta_paciente i,
	autorizacao_convenio k
where 	a.cd_procedimento = b.cd_procedimento
and 	a.ie_origem_proced = b.ie_origem_proced
and 	a.cd_setor_atendimento = c.cd_setor_atendimento
and 	a.nr_seq_atepacu = d.nr_seq_interno
and 	a.nr_interno_conta = i.nr_interno_conta
and 	d.cd_tipo_acomodacao = h.cd_tipo_acomodacao(+)
and 	a.cd_medico_executor = e.cd_pessoa_fisica(+)
and 	a.nr_sequencia = f.nr_seq_procedimento(+)
and 	a.nr_sequencia = g.nr_seq_procedimento(+)
and 	a.nr_atendimento = w.nr_atendimento
and	a.nr_atendimento = k.nr_atendimento(+)
and 	2	= f.ie_tipo_valor(+)
and	a.cd_motivo_exc_conta is null
union all
select	9 cd_tipo_registro,
	null dt_geracao,
	null hr_geracao,
	null cd_convenio,
	null cd_procedimento,
	null qt_procedimento,
	null vl_procedimento,
	-87 cd_setor_atendimento,
	null dt_procedimento,
	null vl_filme,
	null vl_custo_operacional,
	null cd_senha,
	null nr_prontuario,
	null nr_atendimento,
	null dt_entrada,	
	null hr_entrada,
	null vl_materiais,
	null nm_paciente,
	null nr_guia,
	null vl_ch,
	null dt_autorizacao,
	null qt_ch,
	null  nr_matricula 
from	dual;
/
