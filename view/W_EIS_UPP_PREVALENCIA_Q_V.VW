create or replace view w_eis_UPP_prevalencia_q_v  as 
select	distinct	
	b.nr_atendimento,
	b.dt_evento,
	c.cd_setor_atendimento,
	substr(obter_ds_descricao_setor(c.cd_setor_atendimento),1,255) ds_setor,
	obter_unidade_atendimento(b.nr_atendimento,'A','U') ds_unidade,
	e.qt_ponto,	   
	substr(obter_resultado_braden_q(e.qt_ponto),1,100) ds_risco
from   	qua_evento_paciente b,
	cur_ferida c,
	atend_escala_braden_q e	   
where  	b.nr_sequencia = c.nr_seq_evento(+)
and	c.dt_inativacao is null
and	b.nr_atendimento = e.nr_atendimento
and	e.nr_sequencia = ( select max(f.nr_sequencia)
		       	   from	  atend_escala_braden_q f
			   where  f.nr_atendimento = e.nr_atendimento
			   and	  trunc(f.dt_avaliacao)<= trunc(c.dt_atualizacao))    
and	trunc(c.dt_atualizacao) >= trunc(e.dt_avaliacao);
/