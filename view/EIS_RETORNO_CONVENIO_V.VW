create or replace
view eis_retorno_convenio_v as
select	/*+ RULE */
	--obter_valor_conta_ret(a.nr_interno_conta,a.cd_autorizacao,'P',c.dt_baixa_cr,null) vl_pago, --Estes function soma todos os valores da conta.	(retirado, estava considerando o valor pago X quantidade de registros da mesma guia)
	a.vl_pago + a.vl_adicional as vl_pago,
	a.vl_amenor vl_naceito, --Estes valores de glosa devem ser com base na ultima guia do retorno, pois este � o valor que vale
	a.vl_glosado vl_aceito,	--Estes valores de glosa devem ser com base na ultima guia do retorno, pois este � o valor que vale
	(a.vl_amenor + a.vl_glosado) vl_glosa, --Estes valores de glosa devem ser com base na ultima guia do retorno, pois este � o valor que vale
	obter_valor_conpaci_guia(a.nr_interno_conta, a.cd_autorizacao, 1) vl_conta,
	substr(obter_nome_convenio(b.cd_convenio_parametro),1,255) ds_convenio,
	b.cd_convenio_parametro cd_convenio,
	substr(OBTER_VALOR_DOMINIO(11,g.ie_tipo_convenio),1,255) ds_tipo_convenio,
	g.ie_tipo_convenio ie_tipo_convenio,
	trunc(c.dt_retorno) dt_referencia,
	trunc(c.dt_baixa_cr) dt_baixa_cr,
	substr(obter_empresa_estab(b.cd_estabelecimento),1,255) cd_empresa,
	e.ie_tipo_atendimento,
	substr(obter_valor_dominio(12,ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	f.cd_categoria,
	(substr(obter_nome_convenio(b.cd_convenio_parametro),1,150) || ' - ' || f.ds_categoria) ds_categoria,
	c.cd_estabelecimento,
	a.nm_usuario nm_usuario_glosa,
	b.nr_interno_conta,
	obter_setor_atendimento(e.nr_atendimento) cd_setor_atendimento,
	substr(obter_nome_setor(obter_setor_atendimento(e.nr_atendimento)),1,255) ds_setor_atendimento,
	substr(b.nr_interno_conta||' - '||obter_nome_pf_pj(e.cd_pessoa_fisica,null),1,255) ds_conta_paciente
from 	categoria_convenio f,
	atendimento_paciente e,
	conta_paciente b,
	convenio_retorno_item a,
	convenio_retorno c,
	convenio g
where	a.nr_interno_conta 		= b.nr_interno_conta
and	c.cd_convenio			= b.cd_convenio_parametro
and	c.cd_convenio			= g.cd_convenio
and	a.nr_Seq_retorno 		= c.nr_sequencia
and	b.nr_atendimento		= e.nr_atendimento
and	c.cd_convenio			= f.cd_convenio
and	b.cd_categoria_parametro 	= f.cd_categoria
union all
/*Guias da GRG que est�o no retorno */
select	/*+ RULE */
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VP') vl_pago,
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VA') vl_naceito,
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VG') vl_aceito,
	(obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VA') +
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VG')) vl_glosa,
	0 vl_conta, --Para n�o duplicar o valor da conta, caso ela esteja no retorno e GRG
	substr(obter_nome_convenio(d.cd_convenio_parametro),1,255) ds_convenio,
	d.cd_convenio_parametro,
	substr(obter_valor_dominio(11,e.ie_tipo_convenio),1,255) ds_tipo_convenio,
	e.ie_tipo_convenio,
	trunc(a.dt_lote) dt_referencia,
	trunc(b.dt_baixa_glosa) dt_baixa_cr,
	substr(obter_empresa_estab(d.cd_estabelecimento),1,255) cd_empresa,
	f.ie_tipo_atendimento,
	substr(obter_valor_dominio(12,f.ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	g.cd_categoria,
	(substr(obter_nome_convenio(d.cd_convenio_parametro),1,150) || ' - ' || g.ds_categoria) ds_categoria,
	a.cd_estabelecimento,
	c.nm_usuario nm_usuario_glosa,
	c.nr_interno_conta,
	obter_setor_atendimento(f.nr_atendimento) cd_setor_atendimento,
	substr(obter_nome_setor(obter_setor_atendimento(f.nr_atendimento)),1,255) ds_setor_atendimento,
	substr(d.nr_interno_conta||' - '||obter_nome_pf_pj(f.cd_pessoa_fisica,null),1,255) ds_conta_paciente
from	categoria_convenio g,
	atendimento_paciente f,
	convenio e,
	conta_paciente d,
	lote_audit_hist_guia c,
	lote_audit_hist b,
	lote_auditoria a
where	d.cd_convenio_parametro		= g.cd_convenio
and	d.cd_categoria_parametro	= g.cd_categoria
and	d.nr_atendimento		= f.nr_atendimento
and	d.cd_convenio_parametro		= e.cd_convenio
and	c.nr_interno_conta		= d.nr_interno_conta
and	b.nr_sequencia			= c.nr_seq_lote_hist
and	a.nr_sequencia			= b.nr_seq_lote_audit
and	exists
	(select	1
	from	convenio_retorno_item x,
		convenio_retorno y
	where	x.nr_interno_conta		= c.nr_interno_conta
	and	x.nr_seq_retorno		= y.nr_sequencia
	and	trunc(y.dt_retorno,'mm')	= trunc(a.dt_lote,'mm'))
union all
/*Guias da GRG que n�o est�o no retorno*/
select	/*+ RULE */
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VP') vl_pago,
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VA') vl_naceito,
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VG') vl_aceito,
	(obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VA') +
	obter_valores_guia_grc(c.nr_seq_lote_hist,c.nr_interno_conta,c.cd_autorizacao,'VG')) vl_glosa,
	d.vl_conta vl_conta,
	substr(obter_nome_convenio(d.cd_convenio_parametro),1,255) ds_convenio,
	d.cd_convenio_parametro,
	substr(obter_valor_dominio(11,e.ie_tipo_convenio),1,255) ds_tipo_convenio,
	e.ie_tipo_convenio,
	trunc(a.dt_lote) dt_referencia,
	trunc(b.dt_baixa_glosa) dt_baixa_cr,
	substr(obter_empresa_estab(d.cd_estabelecimento),1,255) cd_empresa,
	f.ie_tipo_atendimento,
	substr(obter_valor_dominio(12,f.ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	g.cd_categoria,
	(substr(obter_nome_convenio(d.cd_convenio_parametro),1,150) || ' - ' || g.ds_categoria) ds_categoria,
	a.cd_estabelecimento,
	c.nm_usuario nm_usuario_glosa,
	c.nr_interno_conta,
	obter_setor_atendimento(f.nr_atendimento) cd_setor_atendimento,
	substr(obter_nome_setor(obter_setor_atendimento(f.nr_atendimento)),1,255) ds_setor_atendimento,
	substr(d.nr_interno_conta||' - '||obter_nome_pf_pj(f.cd_pessoa_fisica,null),1,255) ds_conta_paciente
from	categoria_convenio g,
	atendimento_paciente f,
	convenio e,
	conta_paciente d,
	lote_audit_hist_guia c,
	lote_audit_hist b,
	lote_auditoria a
where	d.cd_convenio_parametro		= g.cd_convenio
and	d.cd_categoria_parametro	= g.cd_categoria
and	d.nr_atendimento		= f.nr_atendimento
and	d.cd_convenio_parametro		= e.cd_convenio
and	c.nr_interno_conta		= d.nr_interno_conta
and	b.nr_sequencia			= c.nr_seq_lote_hist
and	a.nr_sequencia			= b.nr_seq_lote_audit
and	not exists
	(select	1
	from	convenio_retorno_item x,
		convenio_retorno y
	where	x.nr_interno_conta		= c.nr_interno_conta
	and	x.nr_seq_retorno		= y.nr_sequencia
	and	trunc(y.dt_retorno,'mm')	= trunc(a.dt_lote,'mm'));
/