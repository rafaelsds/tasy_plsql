create or replace view w_eis_UPP_distribuicao_v  as 
select	distinct	
	b.nr_atendimento,	   
	b.dt_evento,
	c.cd_setor_atendimento,
	substr(obter_ds_descricao_setor(c.cd_setor_atendimento),1,255) ds_setor,
	obter_unidade_atendimento(b.nr_atendimento,'A','U') ds_unidade,
	nvl(substr(obter_dados_ficha_UPP(c.nr_sequencia,'EU'),1,255),'N�o informado') ds_gravidade	
from   	qua_evento_paciente b,
	cur_ferida c,
	atend_escala_braden d	   
where  	c.ie_admitido_ferida = 'N'
and	b.nr_sequencia = c.nr_seq_evento(+)
and	c.dt_inativacao  is null
and	c.dt_alta_curativo is not null
and	b.nr_atendimento = d.nr_atendimento
and	d.nr_sequencia = ( select max(e.nr_sequencia)
		       	   from	  atend_escala_braden e
			   where  e.nr_atendimento = d.nr_atendimento
			   and	  trunc(e.dt_avaliacao)<= trunc(c.dt_atualizacao))       
and	trunc(c.dt_atualizacao) >= trunc(d.dt_avaliacao);
/