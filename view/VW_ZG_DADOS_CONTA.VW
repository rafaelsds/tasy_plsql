CREATE OR REPLACE FORCE VIEW vw_zg_dados_conta (versao,
		data_emissao_nota_fiscal,
		data_vencimento_nota_fiscal,
		remessa,
		plano_beneficiario,
		protocolo_remessa,
		data_envio_remessa,
		data_vencimento_remessa,
		data_fechamento_remessa,
		data_emissao_remessa,
		data_entrega_remessa,
		valor_total_remessa,
		data_referencia_lote,
		lote,
		fatura,
		valor_total_guia,
		sequenciadaparcial,
		prontuario,
		quantidade_guias_remessa,
		matricula_beneficiario,
		codigopessoafisica,
		nome_beneficiario,
		numero_atendimento,
		datadefechamentodaconta,
		datadeinsercaocontanoprotocolo,
		data_emissao_guia,
		data_atendimento_guia,
		horaentrada,
		data_saida_guia,
		horasaida,
		datadaaltadopaciente,
		cd_autorizacao,
		nr_guia_princ,
		senha_guia,
		tipo_guia,
		numero_nota_fiscal,
		numero_nota_fiscal_eletronica,
		titulo,
		crm_solicitante,
		nome_solicitante,
		nome_convenio,
		codigo_convenio,
		cnpj_convenio,
		registro_ans,
		ie_proc_tuss)
	AS
		SELECT
			'1.0.0'                                                                               AS versao,
			titulo.dt_emissao                                                                     AS data_emissao_nota_fiscal,
			titulo.dt_pagamento_previsto                                                          AS data_vencimento_nota_fiscal,
			protocolos.nr_seq_protocolo                                                           AS remessa,
			(SELECT ds_plano
			 FROM convenio_plano
			 WHERE cd_plano = (
				 SELECT cd_plano_convenio
				 FROM atend_categoria_convenio
				 WHERE nr_atendimento = conta.nr_atendimento AND rownum = 1)
				   AND cd_convenio = convenios.cd_convenio)                                       AS plano_beneficiario,
			protocolos.nr_seq_doc_convenio                                                        AS protocolo_remessa,
			protocolos.dt_envio                                                                   AS data_envio_remessa,
			protocolos.dt_vencimento                                                              AS data_vencimento_remessa,
			protocolos.dt_definitivo                                                              AS data_fechamento_remessa,
			protocolos.dt_definitivo                                                              AS data_emissao_remessa,
			protocolos.dt_entrega_convenio                                                        AS data_entrega_remessa,
			obter_total_protocolo(protocolos.nr_seq_protocolo)                                    AS valor_total_remessa,
			lote.dt_mesano_referencia                                                             AS data_referencia_lote,
			lote.nr_sequencia                                                                     AS lote,
			conta.nr_interno_conta                                                                AS fatura,
			conta.vl_conta                                                                        AS valor_total_guia,
			conta.nr_seq_apresentacao                                                             AS sequenciadaparcial,
			pessoa.nr_prontuario                                                                  AS prontuario,
			campo_numerico(obter_qt_conta_protocolo(protocolos.nr_seq_protocolo))                 AS quantidade_guias_remessa,
			obter_matricula_usuario(conta.cd_convenio_parametro, conta.nr_atendimento)            AS matricula_beneficiario,
			pessoa.cd_pessoa_fisica                                                               AS codigopessoafisica,
			upper(pessoa.nm_pessoa_fisica_sem_acento)                                             AS nome_beneficiario,
			conta.nr_atendimento                                                                  AS numero_atendimento,
			conta.dt_conta_definitiva                                                             AS datadefechamentodaconta,
			conta.dt_conta_protocolo                                                              AS datadeinsercaocontanoprotocolo,
			atendimento.dt_entrada                                                                AS data_emissao_guia,
			conta.dt_periodo_inicial                                                              AS data_atendimento_guia,
			conta.dt_periodo_inicial                                                              AS horaentrada,
			conta.dt_periodo_final                                                                AS data_saida_guia,
			conta.dt_periodo_final                                                                AS horasaida,
			to_char(atendimento.dt_alta, 'dd/mm/yyyy hh24:mi:ss')                                 AS datadaaltadopaciente,
			conta.cd_autorizacao                                                                  AS cd_autorizacao,
			substr(obter_guia_convenio(atendimento.nr_atendimento), 1, 200)                       AS nr_guia_princ,
			substr(obter_dados_categ_conv(conta.nr_atendimento, 'S'), 1, 20)                      AS senha_guia,
			substr(obter_valor_dominio(12, atendimento.ie_tipo_atendimento), 1, 80)               AS tipo_guia,
			trim(substr(obter_nota_conta_prot_conv(protocolos.nr_seq_protocolo, 0, 'S'), 1, 254)) AS numero_nota_fiscal,
			trim(substr(obter_nfe_conta_protocolo(protocolos.nr_seq_protocolo, 0), 1, 254))       AS numero_nota_fiscal_eletronica,
			substr(obter_titulo_conta_protocolo(protocolos.nr_seq_protocolo, 0), 1, 254)          AS titulo,
			medico.nr_crm                                                                         AS crm_solicitante,
			substr(obter_nome_medico(medico.cd_pessoa_fisica, 'P'), 1, 100)                       AS nome_solicitante,
			convenios.ds_convenio                                                                 AS nome_convenio,
			convenios.cd_convenio                                                                 AS codigo_convenio,
			convenios.cd_cgc                                                                      AS cnpj_convenio,
			c_estabelecimento.cd_ans                                                              AS registro_ans,
			parametros_convenio.ie_proc_tuss                                                      AS ie_proc_tuss
		FROM conta_paciente conta
			INNER JOIN atendimento_paciente atendimento
				ON conta.nr_atendimento = atendimento.nr_atendimento
			INNER JOIN pessoa_fisica pessoa
				ON atendimento.cd_pessoa_fisica = pessoa.cd_pessoa_fisica
			INNER JOIN protocolo_convenio protocolos
				ON protocolos.nr_seq_protocolo = conta.nr_seq_protocolo
			LEFT JOIN lote_protocolo lote
				ON lote.nr_sequencia = protocolos.nr_seq_lote_protocolo
			INNER JOIN convenio convenios
				ON convenios.cd_convenio = protocolos.cd_convenio
			INNER JOIN convenio_estabelecimento c_estabelecimento
				ON protocolos.cd_estabelecimento = c_estabelecimento.cd_estabelecimento
				   AND protocolos.cd_convenio = c_estabelecimento.cd_convenio
			LEFT JOIN (
						  SELECT
							  nr_seq_protocolo,
							  MIN(dt_pagamento_previsto) AS dt_pagamento_previsto,
							  MIN(dt_emissao)            AS dt_emissao
						  FROM titulo_receber
						  GROUP BY nr_seq_protocolo
					  ) titulo
				ON titulo.nr_seq_protocolo = conta.nr_seq_protocolo
			LEFT JOIN medico medico
				ON atendimento.cd_medico_resp = medico.cd_pessoa_fisica
			LEFT JOIN tiss_parametros_convenio parametros_convenio
				ON parametros_convenio.cd_convenio = protocolos.cd_convenio
				   AND parametros_convenio.cd_estabelecimento = protocolos.cd_estabelecimento;
/