create or replace view plussoft_taxas_contrato_v
as
	select 	b.nr_contrato,
		2 nr_seq_taxa,
		obter_valor_dominio(1930,2) ds_item
	from 	pls_regra_inscricao a,
		pls_contrato b
	where	a.nr_seq_contrato = b.nr_sequencia
	union all
	select 	b.nr_contrato,
		11 nr_seq_taxa,
		obter_valor_dominio(1930,11) ds_item
	from 	pls_regra_segurado_cart a,
		pls_contrato b
	where	a.nr_seq_contrato = b.nr_sequencia
	order by 1,2 asc;
/