create or replace view fila_email_diario_v(
  nr_sequencia, 
  data, 
  cd_versao, 
  aplicacao, 
  status, 
  nr_seq_calendario, 
  nr_seq_man_os_build_emer, 
  dt_solicit, 
  nr_seq_ordem_serv,
  nm_usuario_solit,
  ie_tipo_geracao,
  ds_motivo
  ) as
select 
  f.nr_sequencia,
  to_char(f.dt_atualizacao,'dd/mm/yyyy hh24:mi:ss') as Data, 
  f.cd_versao,
             CASE f.ie_aplicacao
                  WHEN 'F'  THEN 'HTML5 - F'
                  WHEN 'B'  THEN 'HTML5 - B'
                  WHEN 'S'  THEN 'Pacote'                  
                  WHEN 'J'  THEN 'Java Swing'
                  WHEN 'W'  THEN 'Java Web'
                  WHEN 'D'  THEN 'Delphi'
                  WHEN 'G'  THEN 'GWT'
                  ELSE f.IE_APLICACAO
             END    Aplicacao,
  CASE f.IE_STATUS_BUILD 
                  WHEN 'F'  THEN 'Finalizado'
                  WHEN 'G'  THEN 'Gerando'
                  WHEN 'A'  THEN 'Aguardando'                  
                  WHEN 'E'  THEN 'Erro'
                  ELSE f.IE_APLICACAO
             END    Status,
  f.nr_seq_calendario,
  f.nr_seq_man_os_build_emer,
  to_char(e.dt_solicit, 'dd/mm/yyyy hh24:mi:ss')          DT_SOLICIT, 
  e.nr_seq_ordem_serv, 
  e.nm_usuario_solit, 
  Decode(e.ie_tipo_geracao, 'I', 'IMEDIATO', 
                                 'F', 'FINAL DO DIA')         IE_TIPO_GERACAO,
  e.ds_motivo
  
from 
  tasy_queue_build f
  left join corp.man_os_ctrl_build_emer@whebl01_dbcorp e on f.nr_seq_man_os_build_emer = e.nr_sequencia
where 
  trunc(f.dt_atualizacao) = trunc(sysdate) 
  order by DATA ;

  /    