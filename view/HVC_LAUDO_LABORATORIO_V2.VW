CREATE OR REPLACE VIEW HVC_LAUDO_LABORATORIO_V2 as
SELECT   DISTINCT
		 b.cd_pessoa_fisica,
		 b.cd_recem_nato,
		 b.ie_recem_nato,
		 a.nr_atendimento,
   	     a.ds_tipo_atendimento,
	     a.cd_convenio || ' - ' || a.ds_convenio ds_convenio,
	     a.dt_entrada,
	     a.nm_unidade,
	     a.cd_unidade,
	     b.dt_entrada_unidade,
		obter_nome_pf(b.cd_medico)nm_medico,
	     SUBSTR(b.dt_entrega, 1, 10) AS dt_entrega,
		 MIN(b.dt_prescricao) dt_coleta,
	    c.nr_prescricao,
	    a.nr_prontuario,
	    c.cd_setor_entrega,
	    c.nr_sequencia,
	    c.cd_setor_coleta,
	    i.nr_sequencia nr_seq_grupo,
	    UPPER('RESPONSÁVEL: ' ||k.nm_pessoa_fisica||' - '||k.ds_conselho||' '||k.nr_crm||SUBSTR(obter_desc_cargo(k.cd_cargo),1,30)) ds_responsavel,
	    k.nm_pessoa_fisica nm_assinatura,
	    k.ds_conselho||' '||k.nr_crm crm_assinatura,
	    SUBSTR(obter_setor_ds_prescr(c.cd_setor_coleta),1,15) ds_setor_coleta,
	    SUBSTR(obter_setor_ds_prescr(c.cd_setor_entrega),1,15) ds_setor_entrega,
	    K.CD_PESSOA_FISICA PESSOA,
	    b.cd_medico cd_medico_solicitante,
		1 ie_tipo
FROM    	pessoa_fisica_usuario_v k,
	    grupo_exame_lab i,
	    pessoa_fisica pf,
	    exame_lab_material h,
	    exame_laboratorio d,
	    material_exame_lab f,
	    prescr_medica b,
	    prescr_procedimento c,
	    atendimento_paciente_v a
WHERE  a.nr_atendimento    = b.nr_atendimento
AND    a.cd_pessoa_fisica = pf.cd_pessoa_fisica
AND    b.nr_prescricao    = c.nr_prescricao
AND    c.nr_seq_exame    = d.nr_seq_exame
AND    c.cd_material_exame = f.cd_material_exame
AND    c.nr_seq_exame    = h.nr_seq_exame
AND    h.nr_seq_material    = f.nr_sequencia
AND    d.nr_seq_grupo = i.nr_sequencia
AND    i.cd_responsavel = k.cd_pessoa_fisica
AND    c.ie_status_atend >30
GROUP BY
	  b.ie_recem_nato,
	  b.cd_recem_nato,
	a.nr_atendimento,
    a.ds_tipo_atendimento,
    a.cd_convenio || ' - ' || a.ds_convenio,
  obter_nome_pf(b.cd_medico),
    a.dt_entrada,
    a.nm_unidade,
    a.cd_unidade,
    b.dt_entrada_unidade,
    SUBSTR(b.dt_entrega, 1, 10),
        c.nr_prescricao,
    a.nr_prontuario,
    c.cd_setor_entrega,
	c.cd_medico_solicitante,
    c.cd_setor_coleta,
   c.nr_sequencia,
    i.nr_sequencia,
    UPPER('RESPONSÁVEL: ' ||k.nm_pessoa_fisica||' - '||k.ds_conselho||' '||k.nr_crm||SUBSTR(obter_desc_cargo(k.cd_cargo),1,30)),
	    k.nm_pessoa_fisica,
	    k.ds_conselho,k.nr_crm,
    SUBSTR(obter_setor_ds_prescr(c.cd_setor_coleta),1,15),
    SUBSTR(obter_setor_ds_prescr(c.cd_setor_entrega),1,15),
    K.CD_PESSOA_FISICA,
	b.cd_pessoa_fisica,
	 b.cd_medico 
UNION
SELECT   DISTINCT
		 b.cd_pessoa_fisica,
		 b.cd_recem_nato,
		 b.ie_recem_nato,
		 a.nr_atendimento,
   	     a.ds_tipo_atendimento,
	     a.cd_convenio || ' - ' || a.ds_convenio ds_convenio,
	     a.dt_entrada,
	     a.nm_unidade,
	     a.cd_unidade,
	     b.dt_entrada_unidade,
	     obter_nome_pf(c.cd_medico_solicitante)nm_medico,
	     SUBSTR(b.dt_entrega, 1, 10) AS dt_entrega,
	       MIN(b.dt_prescricao) dt_coleta,
	    c.nr_prescricao,
	    a.nr_prontuario,
	    c.cd_setor_entrega,
	    c.nr_sequencia,
	    c.cd_setor_coleta,
	    i.nr_sequencia nr_seq_grupo,
	    UPPER('RESPONSÁVEL: ' ||k.nm_pessoa_fisica||' - '||k.ds_conselho||' '||k.nr_crm||SUBSTR(obter_desc_cargo(k.cd_cargo),1,30)) ds_responsavel,
	    k.nm_pessoa_fisica nm_assinatura,
	    k.ds_conselho||' '||k.nr_crm crm_assinatura,
	    SUBSTR(obter_setor_ds_prescr(c.cd_setor_coleta),1,15) ds_setor_coleta,
	    SUBSTR(obter_setor_ds_prescr(c.cd_setor_entrega),1,15) ds_setor_entrega,
	    K.CD_PESSOA_FISICA PESSOA,
		c.cd_medico_solicitante,
		2 ie_tipo
FROM    	pessoa_fisica_usuario_v k,
	    grupo_exame_lab i,
	    pessoa_fisica pf,
	    exame_lab_material h,
	    exame_laboratorio d,
	    material_exame_lab f,
	    prescr_medica b,
	    prescr_procedimento c,
	    atendimento_paciente_v a
WHERE  a.nr_atendimento    = b.nr_atendimento
AND    a.cd_pessoa_fisica = pf.cd_pessoa_fisica
AND    b.nr_prescricao    = c.nr_prescricao
AND    c.nr_seq_exame    = d.nr_seq_exame
AND    c.cd_material_exame = f.cd_material_exame
AND    c.nr_seq_exame    = h.nr_seq_exame
AND    h.nr_seq_material    = f.nr_sequencia
AND    d.nr_seq_grupo = i.nr_sequencia
AND    i.cd_responsavel = k.cd_pessoa_fisica
AND    c.ie_status_atend >30
AND	   c.cd_medico_solicitante IS NOT NULL
GROUP BY
	  b.ie_recem_nato,
	  b.cd_recem_nato,
	a.nr_atendimento,
    a.ds_tipo_atendimento,
    a.cd_convenio || ' - ' || a.ds_convenio,
    obter_nome_pf(c.cd_medico_solicitante),
    a.dt_entrada,
    a.nm_unidade,
    a.cd_unidade,
    b.dt_entrada_unidade,
    SUBSTR(b.dt_entrega, 1, 10),
        c.nr_prescricao,
    a.nr_prontuario,
    c.cd_setor_entrega,
	c.cd_medico_solicitante,
    c.cd_setor_coleta,
c.nr_sequencia,
    i.nr_sequencia,
    UPPER('RESPONSÁVEL: ' ||k.nm_pessoa_fisica||' - '||k.ds_conselho||' '||k.nr_crm||SUBSTR(obter_desc_cargo(k.cd_cargo),1,30)),
	    k.nm_pessoa_fisica,
	    k.ds_conselho,k.nr_crm,
    SUBSTR(obter_setor_ds_prescr(c.cd_setor_coleta),1,15),
    SUBSTR(obter_setor_ds_prescr(c.cd_setor_entrega),1,15),
    K.CD_PESSOA_FISICA,
	b.cd_pessoa_fisica
/
