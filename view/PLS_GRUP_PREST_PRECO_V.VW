create or replace view pls_grup_prest_preco_v as
select a.nr_sequencia nr_seq_grupo,
       b.nr_seq_prestador
from   pls_preco_grupo_prestador a,
       table(pls_grupos_pck.obter_prestadores_grupo( a.nr_sequencia )) b
where a.ie_situacao = 'A';
/