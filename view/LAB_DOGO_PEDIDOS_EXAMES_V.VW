create or replace View Lab_dogo_pedidos_exames_v
AS
select	a.nr_prescricao pedido,
	b.nr_sequencia item_pedido,
	c.cd_exame codigo_exame,
	c.nm_exame descricao_exame,
	b.ds_observacao observacao
from	exame_laboratorio c,
	atendimento_paciente d,
	prescr_medica a,
	prescr_procedimento b
where	a.nr_prescricao		= b.nr_prescricao
and	b.nr_seq_exame 		= c.nr_seq_exame
and	d.ie_tipo_atendimento	= 3	
and	b.cd_setor_atendimento  = 39
and	a.nr_atendimento	= d.nr_atendimento;
