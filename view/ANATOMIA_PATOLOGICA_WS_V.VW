create or replace 
view anatomia_patologica_ws_v as
select  b.nr_atendimento,
        a.nr_prescricao,
        a.nr_sequencia,
        a.nr_seq_interno,
        a.nr_acesso_dicom nr_acess_number,
        a.nr_seq_proc_interno cd_proced_tasy,
        e.cd_integracao cd_proced_integracao,
        (a.nr_seq_proc_interno||a.ie_lado) cd_proced_tasy_lado,
        d.ds_proc_exame ds_procedimento,
        a.ie_lado,
        nvl(a.ie_urgencia,'N') ie_urgencia,
        a.dt_prev_execucao,
        b.cd_setor_atendimento cd_setor_paciente,
        b.cd_estabelecimento,
        c.nm_pessoa_fisica,
        b.dt_prescricao,
        nvl(b.dt_liberacao_medico, b.dt_liberacao) dt_liberacao,
        f.nr_Sequencia cd_software_integracao,
        obter_estab_integracao(b.cd_estabelecimento, f.nr_seq_empresa) cd_estab_terceiro,
        a.cd_Setor_atendimento  cd_setor_execucao,
        b.cd_pessoa_fisica cd_paciente,
        b.cd_prescritor,
        c.ie_sexo,
	c.nm_pessoa_fisica nm_paciente,
	c.nr_prontuario,
        c.dt_nascimento,
        Obter_Idade(c.dt_nascimento, nvl(c.dt_obito,sysdate),'A') qt_idade_pac,
	Obter_Idade(c.dt_nascimento, nvl(c.dt_obito,sysdate),'MM') qt_idade_pac_meses,
        d.cd_integracao cd_integracao_proc_interno,
        b.qt_altura_cm,
        b.qt_peso,
        m.nr_crm nr_crm_prescritor,
        m.uf_crm uf_crm_prescritor,
        c.nr_cpf nr_cpf_paciente,
        a.qt_procedimento qt_prescrito,
	c.nr_identidade nr_identidade_paciente,
	c.nr_prontuario nr_prontuario_paciente,
	g.ds_endereco ds_endereco_paciente,
	g.ds_endereco,
	nvl(g.nr_endereco, g.ds_compl_end) nr_endereco_paciente,
	nvl(g.nr_endereco, g.ds_compl_end) nr_endereco,
	g.ds_bairro ds_bairro_paciente,
	g.ds_bairro,
	g.ds_municipio ds_municipio_paciente,
	g.ds_municipio,
	g.sg_estado uf_paciente,
	g.sg_estado,
	g.cd_cep cd_cep_paciente,
	g.cd_cep,
	decode(g.nr_ddd_telefone,null,g.nr_telefone,g.nr_ddd_telefone||' '||g.nr_telefone) nr_telefone_paciente,
	h.cd_convenio,
	substr(obter_categoria_convenio(h.cd_convenio,h.cd_categoria),1,100) ds_categoria_convenio,
	i.ds_convenio,
	i.cd_cgc,
	( 	select	x.cd_agenda 
		from 	agenda_paciente x
		where 	x.nr_sequencia = a.nr_seq_agenda ) cd_agenda,
	(	select	x.ds_agenda 
		from 	agenda x,
			agenda_paciente y
		where 	x.cd_agenda = y.cd_agenda
		and	y.nr_sequencia = a.nr_seq_agenda) ds_agenda,
	a.dt_resultado,
	a.ie_suspenso,
	a.cd_procedimento,
	p.cd_tipo_procedimento,
	b.cd_pessoa_fisica,	
	t.cd_estabelecimento cd_estab_atend,
	b.cd_estabelecimento cd_estab_prescr,
	t.dt_entrada,
	a.cd_setor_atendimento,	
	a.qt_procedimento,
	b.cd_medico,
	substr(obter_valor_dominio(12,t.ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	a.ds_observacao,
	a.ie_origem_proced,
	a.cd_material_exame,
	t.ie_tipo_atendimento,
	a.nr_acesso_dicom na_accessionnumber,
	obter_modalidade_wtt(p.cd_tipo_procedimento) ds_modalidade,
	nvl(n.cd_procedencia_externo, n.cd_procedencia) cd_procedencia,
	h.cd_plano_convenio,
	h.cd_plano_convenio cd_plano,
	h.cd_categoria,
	h.cd_usuario_convenio,	
	h.cd_complemento cd_compl_conv,
	h.dt_validade_carteira,	
	n.ds_procedencia ds_procedencia,	
	t.nr_seq_queixa nr_seq_motivo_atend,
	a.dt_atualizacao,
	a.nm_usuario,
	b.dt_liberacao_medico,
	a.ds_material_especial,
	nvl(a.ie_amostra,'N') ie_amostra_entregue,
	b.ie_recem_nato,
	a.nr_seq_exame,
	a.ie_executar_leito,
	a.nr_seq_proc_interno,
	j.cd_unidade_basica || ' ' || j.cd_unidade_compl cd_unidade,
	g.ds_complemento,
	g.nr_telefone,
	mp.nm_pessoa_fisica nm_medico,
	mp.nr_cpf nr_cpf_medico,		
	c.nr_cpf,	
	s.ds_setor_atendimento ds_setor_paciente,
	substr(q.ds_queixa,1,80) ds_motivo_atend,
	substr(r.ds_motivo,1,255) ds_motivo_suspensao,
	a.ds_horarios,
	a.ds_dado_clinico,
	obter_crm_medico(b.cd_medico) nr_crm,
	substr(obter_dados_medico(b.cd_medico,'UFCRM'),1,2) sg_uf_medico,
	t.ds_senha,
	c.ds_observacao ds_observacao_pf,
	(select MAX(z.ds_unidade_atend) from unidade_atendimento z where z.nr_atendimento =  b.nr_atendimento) ds_unidade_atend,
	substr(Obter_Desc_Plano_Conv(h.cd_convenio, h.cd_plano_convenio),1,255) ds_plano,
	c.dt_admissao_hosp,	
	a.nr_seq_lab,
	l.nm_contato nm_mae_paciente,
	substr(obter_cgc_estabelecimento(b.cd_estabelecimento),1,30) cd_cgc_hospital,
	substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,255) nm_hospital,	
	substr(obter_nome_convenio(h.cd_convenio),1,255) nm_convenio,
	h.nr_doc_convenio,	
	o.ds_material_exame,
  a.cd_pessoa_coleta,
	a.nr_seq_proc_int_cirur,
	a.dt_coleta,
	a.qt_peca_ap,
	a.nr_seq_amostra_princ,
	a.ds_qualidade_peca_ap,
	a.ie_forma_exame,
	a.ds_diag_provavel_ap,
	a.ds_exame_anterior_ap,
	a.ds_localizacao_lesao,
	a.ds_tempo_doenca,
	a.cd_cgc_laboratorio,
	a.qt_frasco_env
from    setor_atendimento s,
	exame_laboratorio k,
	atendimento_paciente t,
	sistema_integracao f,
        proc_interno_integracao e,
        pessoa_fisica c,
	atend_paciente_unidade j,
	material_exame_lab o,
        Medico m,
	procedencia n,
        proc_interno d,
	pessoa_fisica mp,
        procedimento p,
        prescr_medica b,	
        prescr_procedimento a,
	compl_pessoa_fisica g,
	compl_pessoa_fisica l,
	atend_categoria_convenio h,
	queixa_paciente q,
	motivo_suspensao_prescr r,
	convenio i
where   a.nr_prescricao         = b.nr_prescricao
and     b.cd_pessoa_fisica      = c.cd_pessoa_fisica
and     d.nr_sequencia          = a.nr_seq_proc_interno
and     e.nr_seq_proc_interno   = a.nr_seq_proc_interno
and     e.nr_seq_proc_interno   = d.nr_sequencia
and     p.cd_procedimento       = a.cd_procedimento
and     p.ie_origem_proced      = a.ie_origem_proced
and	h.cd_convenio		= i.cd_convenio
and 	j.nr_seq_interno	= obter_atepacu_paciente(b.nr_atendimento,'A')
and	b.nr_atendimento	= t.nr_atendimento
and	b.nr_atendimento 	= h.nr_atendimento(+)
and	t.cd_procedencia	= n.cd_procedencia
and 	j.cd_setor_atendimento	= s.cd_setor_atendimento
and	h.dt_inicio_vigencia	= (select max(w.dt_inicio_vigencia) 
				from atend_categoria_convenio w
				where w.nr_atendimento  = b.nr_atendimento)
and     b.cd_prescritor         = m.cd_Pessoa_fisica(+)
and	c.cd_pessoa_fisica	= g.cd_pessoa_fisica(+)
and	c.cd_pessoa_fisica	= l.cd_pessoa_fisica(+)
and	mp.cd_pessoa_fisica(+)	= b.cd_medico
and	k.nr_seq_exame		= a.nr_seq_exame
and	k.ie_anatomia_patologica = 'S'
and	a.cd_material_exame	= o.cd_material_exame
and	a.nr_seq_lab		is not null
and	g.ie_tipo_complemento(+) = 1
and	l.ie_tipo_complemento(+)   = 5
and     a.cd_motivo_baixa       = 0
and     f.nr_sequencia          = e.nr_seq_sistema_integ
and	a.cd_motivo_suspensao	= r.nr_sequencia(+)
and 	t.nr_seq_queixa 	= q.nr_sequencia(+)
and     nvl(b.dt_liberacao_medico, b.dt_liberacao) is not null
and     ((e.cd_estabelecimento  is null) or (e.cd_estabelecimento = b.cd_estabelecimento))
and     a.dt_integracao         is null
and     a.dt_suspensao          is null;
/