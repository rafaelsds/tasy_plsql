create or replace
view cron_proj_migr_incon_v as
select	p.nr_sequencia nr_seq_projeto,
	p.nr_seq_ordem_serv nr_seq_os_projeto,
	c.nr_sequencia nr_seq_cronograma,
	e.nr_sequencia nr_seq_atividade,
	e.ds_atividade ds_atividade,
	e.pr_etapa pr_atividade,
	e.ie_tipo_obj_proj_migr ie_tipo_atividade,
	e.cd_classificacao
from	proj_cronograma c,
	proj_cron_etapa e,
	proj_projeto p
where	c.nr_sequencia = e.nr_seq_cronograma
and	c.nr_seq_proj = p.nr_sequencia
and	p.nr_seq_gerencia = 9
and	p.nr_seq_classif = 14
and	nvl(e.ie_situacao,'A') = 'A'
and	nvl(e.ie_fase,'N') = 'N'
and	e.pr_etapa < 100
and	not exists (
		select	1
		from	proj_dependencia d
		where	d.nr_seq_projeto = p.nr_sequencia
		and	d.nr_seq_ativ_cron = e.nr_sequencia
		and	d.ie_status = 'N');
/