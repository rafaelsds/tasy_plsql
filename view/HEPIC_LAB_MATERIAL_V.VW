create or replace 
view hepic_lab_material_v as
	select  m.nr_sequencia cd_material,
			m.ds_material_exame ds_material,
			m.ie_situacao
	from    material_exame_lab m,
			material_exame_lab_int i,
			equipamento_lab e
	where   i.nr_seq_material   = m.nr_sequencia
	and     i.cd_equipamento    = e.cd_equipamento
	and     e.ds_sigla          = 'HEPIC'
	order by m.nr_sequencia;
/