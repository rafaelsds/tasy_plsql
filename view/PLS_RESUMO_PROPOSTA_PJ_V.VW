create or replace view pls_resumo_proposta_pj_v
as
select	ie_reg,
	substr(ds_descricao,1,255) ds_descricao,
	nr_seq_plano,
	vl_resumo vl_resumo,
	nr_seq_proposta
from(	select	distinct 'Produto - '||substr(pls_obter_dados_produto(a.nr_seq_plano,'N'),1,255) ds_descricao,
		1 ie_reg,
		a.nr_seq_plano,
		to_number(pls_obter_valor_proposta_pj(a.nr_seq_proposta,a.nr_seq_plano,'P')) vl_resumo,
		b.nr_sequencia nr_seq_proposta
	from	pls_proposta_beneficiario a,
		pls_proposta_adesao      b
	where	a.nr_seq_proposta = b.nr_sequencia
	and	b.ie_tipo_proposta <> 5
	and	((a.dt_cancelamento is null)
	or     (a.dt_cancelamento > b.dt_inicio_proposta))
	union
	select	'     ' || wheb_mensagem_pck.get_texto(1126915,'QT_IDADE_INICIAL='||c.qt_idade_inicial||';QT_IDADE_FINAL='||c.qt_idade_final||';QT_BENEF='||substr(pls_qt_faixa_benef_propos(c.nr_sequencia,e.nr_sequencia,d.nr_seq_plano),1,255)) ds_descricao,
		2 ie_reg,
		d.nr_seq_plano,
		substr(pls_obter_faixa_etaria_prop(d.nr_sequencia,c.nr_sequencia),1,20) * substr(pls_qt_faixa_benef_propos(c.nr_sequencia,e.nr_sequencia,d.nr_seq_plano),1,255) vl_resumo,
		e.nr_sequencia nr_seq_proposta
	from	pls_plano				a,
		pls_tabela_preco			b,
		pls_plano_preco				c,
		pls_proposta_beneficiario		d,
		pls_proposta_adesao			e
	where	e.nr_sequencia = d.nr_seq_proposta
	and	b.nr_seq_plano		= a.nr_sequencia
	and	d.nr_seq_plano		= a.nr_sequencia
	and	c.nr_Seq_tabela		= b.nr_sequencia
	and	d.nr_seq_tabela		= b.nr_sequencia
	and	e.ie_tipo_proposta <> 5
	and	substr(pls_qt_faixa_benef_propos(c.nr_sequencia,d.nr_seq_proposta,d.nr_seq_plano),1,255) is not null
	and	d.dt_cancelamento is null
	and	obter_idade_pf(d.cd_beneficiario,sysdate,'A') between c.qt_idade_inicial and c.qt_idade_final
	and	(((nvl(b.ie_preco_vidas_contrato,'N') = 'S') 
		and pls_obter_qt_vidas_proposta(e.nr_sequencia,d.nr_sequencia) between nvl(c.qt_vidas_inicial,pls_obter_qt_vidas_proposta(e.nr_sequencia,d.nr_sequencia)) and nvl(c.qt_vidas_final,pls_obter_qt_vidas_proposta(e.nr_sequencia,d.nr_sequencia)))
	or	 (nvl(b.ie_preco_vidas_contrato,'N') = 'N'))
	group by c.nr_sequencia,e.nr_sequencia,d.nr_seq_plano,c.qt_idade_final,c.qt_idade_inicial,d.nr_sequencia
	union
	select	'SCA - '||substr(pls_obter_dados_produto(a.nr_seq_plano,'N'),1,255) || ' - Qt. = '|| count(*) ds_descricao,
		3 ie_reg,
		0 nr_seq_plano,
		to_number(substr(pls_obter_valor_sca_proposta(b.nr_seq_proposta,null,a.nr_seq_plano),1,20)) vl_resumo,
		b.nr_seq_proposta nr_seq_proposta
	from	pls_sca_vinculo			a,
		pls_proposta_beneficiario	b
	where	a.nr_seq_benef_proposta	= b.nr_sequencia
	and	b.dt_cancelamento is null
	group by a.nr_seq_plano,b.nr_seq_proposta
	union
	select	wheb_mensagem_pck.get_texto(1126906) || ' - '||substr(pls_obter_desc_bonificacao(a.nr_seq_bonificacao),1,255) || ' - Qt. =  ' ||  count(*) ds_descricao, --Bonificacao
		4 ie_reg,
		0 nr_seq_plano,
		to_number(substr(pls_obter_valor_bonif_proposta(b.nr_seq_proposta,a.nr_seq_bonificacao),1,25)) vl_resumo,
		b.nr_seq_proposta nr_seq_proposta
	from	pls_bonificacao_vinculo		a,
		pls_proposta_beneficiario	b
	where	a.nr_seq_segurado_prop	= b.nr_sequencia
	and	b.dt_cancelamento is null
	group by a.nr_seq_bonificacao,b.nr_seq_proposta
	union
	select	wheb_mensagem_pck.get_texto(1126907) || ' - '||substr(pls_obter_dados_bonificacao(a.nr_sequencia,'D'),1,255) ds_descricao, --Bonificacao da proposta
		5 ie_reg,
		0 nr_seq_plano,
		to_number(substr(pls_valor_bonif_tot_prop_pj(c.nr_sequencia,a.nr_seq_bonificacao),1,15)) vl_resumo,
		c.nr_sequencia nr_seq_proposta
	from	pls_bonificacao_vinculo			a,
		pls_proposta_adesao			c
	where	a.nr_seq_proposta			= c.nr_sequencia
	group by a.nr_sequencia,c.nr_sequencia,a.nr_seq_bonificacao
	union
	select	wheb_mensagem_pck.get_texto(1126908) || ' - '||substr(pls_obter_dados_bonificacao(a.nr_sequencia,'D'),1,255) ds_descricao, --Bonificacao do contrato
		5 ie_reg,
		0 nr_seq_plano,
		to_number(substr(pls_valor_bonif_tot_prop_pj(c.nr_sequencia,a.nr_seq_bonificacao),1,15)) vl_resumo,
		c.nr_sequencia nr_seq_proposta
	from	pls_bonificacao_vinculo			a,
		pls_contrato				b,
		pls_proposta_adesao			c
	where	a.nr_seq_contrato			= b.nr_sequencia
	and	c.nr_seq_contrato			= b.nr_contrato
	group by a.nr_sequencia,c.nr_sequencia,a.nr_seq_bonificacao
	union
	select	wheb_mensagem_pck.get_texto(1126904) || ' - '||substr(pls_obter_dados_produto(c.nr_sequencia,'N'),1,255) ds_descricao, --Taxa de inscricao (Produto)
		6 ie_reg,
		0 nr_seq_plano,
		to_number(pls_valor_inscricao_prop_pj(b.nr_sequencia,c.nr_sequencia)) vl_resumo,
		b.nr_sequencia nr_seq_proposta
	from	pls_proposta_beneficiario	a,
		pls_proposta_adesao		b,
		pls_plano			c,
		pls_regra_inscricao		d
	where	a.nr_seq_proposta		= b.nr_sequencia
	and	a.nr_seq_plano			= c.nr_sequencia
	and	d.nr_seq_plano			= c.nr_sequencia
	and	nvl(a.ie_taxa_inscricao,'S')	= 'S'
	and	trunc(b.dt_inicio_proposta,'month') between trunc(nvl(d.dt_inicio_vigencia,b.dt_inicio_proposta),'month') and trunc(nvl(d.dt_fim_vigencia,b.dt_inicio_proposta),'month')
	and	d.qt_parcela_inicial		= 1
	and	((b.ie_tipo_proposta in (1,6,7) and d.ie_acao_contrato = 'A')  or 
		((b.ie_tipo_proposta in (2,8)) and (d.ie_acao_contrato = 'L')) or 
		((b.ie_tipo_proposta in (3,4,7,8)) and (d.ie_acao_contrato = 'M')) or
		(d.ie_acao_contrato is null))
	and	not exists	(select	1
				from	pls_regra_inscricao x
				where	x.nr_seq_proposta	= b.nr_sequencia)
	and	not exists(	select	1
				from	pls_regra_inscricao	x,
					pls_contrato		y
				 where	x.nr_seq_contrato	= y.nr_sequencia
				 and	b.nr_seq_contrato	= y.nr_contrato)
	union
	select	wheb_mensagem_pck.get_texto(1126905) ds_descricao, --Taxa de inscricao (Proposta)
		6 ie_reg,
		0 nr_seq_plano,
		to_number(pls_valor_inscricao_prop_pj(b.nr_sequencia,a.nr_seq_plano)) vl_resumo,
		b.nr_sequencia nr_seq_proposta
	from	pls_proposta_beneficiario	a,
		pls_proposta_adesao		b,
		pls_regra_inscricao		d
	where	a.nr_seq_proposta		= b.nr_sequencia
	and	d.nr_seq_proposta		= b.nr_sequencia
	and	nvl(a.ie_taxa_inscricao,'S')	= 'S'
	and	trunc(b.dt_inicio_proposta,'month') between trunc(nvl(d.dt_inicio_vigencia,b.dt_inicio_proposta),'month') and trunc(nvl(d.dt_fim_vigencia,b.dt_inicio_proposta),'month')
	and	d.qt_parcela_inicial		= 1
	and	((b.ie_tipo_proposta in (1,6,7) and d.ie_acao_contrato = 'A')  or 
		((b.ie_tipo_proposta in (2,8)) and (d.ie_acao_contrato = 'L')) or 
		((b.ie_tipo_proposta in (3,4,7,8)) and (d.ie_acao_contrato = 'M')) or
		(d.ie_acao_contrato is null))
	and	not exists(	select	1
				from	pls_regra_inscricao	x,
					pls_contrato		y
				where	x.nr_seq_contrato	= y.nr_sequencia
				and	b.nr_seq_contrato	= y.nr_contrato)
	group by b.nr_sequencia,a.nr_seq_plano
	union
	select	wheb_mensagem_pck.get_texto(1126902) ds_descricao, --Taxa de inscricao (Contrato)
		6 ie_reg,
		0 nr_seq_plano,
		to_number(pls_valor_inscricao_prop_pj(b.nr_sequencia,a.nr_seq_plano)) vl_resumo,
		b.nr_sequencia nr_seq_proposta
	from	pls_proposta_beneficiario	a,
		pls_proposta_adesao		b,
		pls_contrato			c,
		pls_regra_inscricao		d
	where	a.nr_seq_proposta		= b.nr_sequencia
	and	b.nr_seq_contrato		= c.nr_contrato
	and	d.nr_seq_contrato		= c.nr_sequencia
	and	nvl(a.ie_taxa_inscricao,'S')	= 'S'
	and	trunc(b.dt_inicio_proposta,'month') between trunc(nvl(d.dt_inicio_vigencia,b.dt_inicio_proposta),'month') and trunc(nvl(d.dt_fim_vigencia,b.dt_inicio_proposta),'month')
	and	d.qt_parcela_inicial		= 1
	and	((b.ie_tipo_proposta in (1,6,7) and d.ie_acao_contrato = 'A')  or 
		((b.ie_tipo_proposta in (2,8)) and (d.ie_acao_contrato = 'L')) or 
		((b.ie_tipo_proposta in (3,4,7,8)) and (d.ie_acao_contrato = 'M')) or
		(d.ie_acao_contrato is null))
	group by b.nr_sequencia,a.nr_seq_plano
	union
	select  'SCA do Contrato - ' || substr(pls_obter_dados_produto(d.nr_seq_plano,'N'),1,255) || ' - Qt. =  ' ||  count(1) ds_descricao,
		7 ie_reg,
		0 nr_seq_plano,
		pls_valor_sca_contr_prop_pj(a.nr_sequencia,d.nr_seq_plano) vl_resumo,
		a.nr_sequencia
	from	pls_sca_regra_contrato			d,
		pls_contrato				c,
		pls_proposta_beneficiario		b,
		pls_proposta_adesao			a
	where	d.nr_seq_contrato			= c.nr_sequencia
	and	c.nr_contrato				= a.nr_seq_contrato
	and	b.nr_seq_proposta			= a.nr_sequencia
	and	a.dt_inicio_proposta between nvl(dt_inicio_vigencia,a.dt_inicio_proposta) and fim_dia(nvl(dt_fim_vigencia,a.dt_inicio_proposta))
	and	((d.ie_geracao_valores = 'T' and b.nr_seq_titular is null and b.nr_seq_titular_contrato is null) or
		((d.ie_geracao_valores = 'D' and ((b.nr_seq_titular is not null) or (b.nr_seq_titular_contrato is not null)))) or
		 (d.ie_geracao_valores = 'B'))
	and	((d.nr_seq_plano_benef	= b.nr_seq_plano) or (d.nr_seq_plano_benef is null))
	and	not exists	(	select	1
					from	pls_sca_vinculo		x
					where	x.nr_seq_benef_proposta	= b.nr_sequencia
					and	d.nr_seq_plano		= x.nr_seq_plano)
	group by a.nr_sequencia,d.nr_seq_plano
	union
	select  wheb_mensagem_pck.get_texto(1126901), --Via do cartao de identificacao
		8 ie_reg,
		0 nr_seq_plano,
		to_number(sum(pls_obter_valores_propostas(a.nr_sequencia,0,'VC'))) vl_resumo,
		b.nr_sequencia nr_seq_proposta
	from	pls_proposta_beneficiario	a,
		pls_proposta_adesao		b
	where	a.nr_seq_proposta = b.nr_sequencia
	and	a.dt_cancelamento is null
	group by b.nr_sequencia
	having sum(to_number(substr(pls_obter_valores_propostas(a.nr_sequencia,0,'VC'),1,255))) > 0)
order by  nr_seq_plano desc , ie_reg;
/