create or replace view rmv_antibiograma as
select	distinct
	b.cd_material codantibiotico,
	--c.cd_medicamento,
	c.cd_microorganismo codgerme,
	c.ie_resultado sensibilidade,
	e.nr_prescricao numpedidocultura
from 	material b,
     	cih_medicamento a,
	exame_lab_result_antib c,
      	exame_lab_result_item d,
	exame_lab_resultado e
where	a.cd_medicamento 	= b.cd_medicamento
and	c.cd_medicamento  = a.cd_medicamento
and 	d.nr_seq_resultado  = c.nr_seq_resultado
and 	d.nr_sequencia	= c.nr_seq_result_item
and	d.nr_seq_resultado 	= e.nr_seq_resultado
and	c.ie_resultado 	<> 'N'
order by 	e.nr_prescricao;
/