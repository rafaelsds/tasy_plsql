create or replace view ocup_ocupacao_setores_v2
as
select  c.cd_setor_atendimento,   
	c.ds_setor_atendimento,  
	c.cd_estabelecimento_base, 
	c.cd_classif_setor,   
	c.nm_unidade_basica ||c.nm_unidade_compl nm_unidade,
	c.ds_ocup_hosp,
	c.nr_seq_apresentacao,
	count(*) nr_unidades_setor,
	sum(decode(b.ie_temporario, 'S', 1, 0)) nr_unidades_temporarias,     
	sum(decode(b.ie_status_unidade, 'P', 1, 0)) nr_unidades_ocupadas,  
	sum(decode(b.ie_status_unidade, 'M', 1, 0)) qt_unidade_acomp,     
	sum(decode(b.ie_status_unidade, 'I', 1, 0)) nr_unidades_interditadas,     
	sum(decode(b.ie_status_unidade, 'L', 1, 0)) nr_unidades_livres,     
	sum(decode(b.ie_status_unidade, 'H', 1, 0)) nr_unidades_higienizacao,
	sum(decode(b.ie_status_unidade, 'R', 1, 0)) nr_unidades_reservadas,
	sum(decode(b.ie_status_unidade, 'O', 1, 0)) qt_unidades_isolamento,
	sum(decode(b.ie_status_unidade, 'A', 1, 0)) qt_unidades_alta,
	sum(decode(b.ie_status_unidade, 'S', 1, 0)) qt_unidade_saida_inter,
	sum(decode(b.ie_status_unidade, 'C', 1, 0)) qt_unidade_chamad_manut,
	sum(decode(b.ie_status_unidade, 'E', 1, 0)) qt_unidade_manutencao,
	sum(decode(b.ie_status_unidade, 'G', 1, 0)) nr_unid_aguard_higien,	
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'P', 1, 0),0)) nr_unid_temp_ocup,   
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'P', 1, 0),0)) nr_unid_temp_ocupadas,  
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'M', 1, 0),0)) qt_unid_temp_acomp,     
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'I', 1, 0),0)) nr_unid_temp_interditadas,     
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'L', 1, 0),0)) nr_unid_temp_livres,     
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'H', 1, 0),0)) nr_unid_temp_higienizacao,
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'R', 1, 0),0)) nr_unid_temp_reservadas,
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'O', 1, 0),0)) qt_unid_temp_isolamento,
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'S', 1, 0),0)) qt_unid_temp_said_interd,
	sum(decode(b.ie_temporario, 'S', decode(b.ie_status_unidade, 'A', 1, 0),0)) qt_unid_temp_alta,
	sum(decode(b.cd_paciente_reserva, null, 0, 1)) nr_unidades_reserva,
	ie_ocup_hospitalar, c.ie_situacao,
	sum(decode(obter_se_leito_disp(b.cd_setor_atendimento, b.cd_unidade_basica, b.cd_unidade_compl), 'S', 1, 0)) qt_disponiveis,
	(sum(decode(OBTER_SE_PAC_ISOLAMENTO(b.nr_atendimento),'S',1,0))) qt_pac_isolado,
	c.nr_seq_agrupamento nr_agrupamento
from	setor_atendimento c,
	unidade_atendimento b
where	b.cd_setor_atendimento     = c.cd_setor_atendimento
  and	b.ie_situacao              = 'A'
  and	c.ie_situacao              = 'A'
  and	c.cd_classif_setor in (1,3,4,9,11,12)
group by c.cd_classif_setor,       
	c.cd_setor_atendimento,   
	c.ds_setor_atendimento,
	c.cd_estabelecimento_base,
	c.ds_ocup_hosp, c.ie_situacao,
	(c.nm_unidade_basica ||c.nm_unidade_compl),
	ie_ocup_hospitalar,
	c.nr_seq_apresentacao,
	c.nr_seq_agrupamento;
/
