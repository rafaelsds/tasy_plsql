create or replace view	 ptu_confirmacao_v40_v
as
select	00309				cd_transacao,
	1				ie_tipo_registro,
	nr_sequencia			nr_sequencia,
	nr_seq_requisicao		nr_seq_requisicao,
	nr_seq_guia			nr_seq_guia,
	decode(ie_tipo_cliente,'U','UNIMED','P','PORTAL', 'R','PRESTADOR') ie_tipo_cliente,
	cd_unimed_executora 		cd_unimed_executora,
	cd_unimed_beneficiario		cd_unimed_beneficiario,
	nr_seq_execucao   		nr_seq_execucao,
	nr_seq_origem    		nr_seq_origem,
	ie_tipo_identificador		ie_tipo_identificador,
	ie_tipo_resposta		ie_tipo_resposta,
	null				ds_fim
from	ptu_confirmacao
where	ie_enviado	= 'N'
union
select	null				cd_transacao,
	2				ie_tipo_registro,
	nr_sequencia			nr_sequencia,
	nr_seq_requisicao		nr_seq_requisicao,
	nr_seq_guia			nr_seq_guia,
	null     	   		ie_tipo_cliente,
	null 				cd_unimed_executora,
	cd_unimed_beneficiario		cd_unimed_beneficiario,
	nr_seq_execucao	   		nr_seq_execucao,
	nr_seq_origem	 		nr_seq_origem,
	null				ie_tipo_identificador,
	ie_tipo_resposta		ie_tipo_resposta,
	'FIM$'				ds_fim
from	ptu_confirmacao
where	ie_enviado	= 'N';
/