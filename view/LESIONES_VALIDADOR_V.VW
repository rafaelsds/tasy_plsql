create or replace view lesiones_validador_v as
select  --REGISTRO--
	1 ie_tipo_registro,
	'' cabecario,
	pj.cd_internacional clues,
	b.nr_atendimento folio,
	--DADOS PACIENTE--
	a.cd_curp curppaciente,
	y.ds_given_name nombre,	 
	y.ds_family_name primerapellido,
	y.ds_component_name_1 segundoapellido,
	a.dt_nascimento fechanacimiento,
        decode(i.ie_brasileiro, 'N', 0 , 'S', 1) nacioextranjero,
        decode(i.ie_brasileiro, 'S', i.cd_externo, '142 � M�XICO') paisorigen,
        decode(i.ie_brasileiro, 'S', 97, obter_dados_cat_entidade(a.cd_pessoa_fisica, 'CD_ENTIDADE')) entidadnacimiento,
	case 
		when obter_idade(a.dt_nascimento, sysdate, 'A') > 5 
		then decode(a.ie_grau_instrucao, 7, 1, 8, 3, 3, 4, 9, 5, 4, 6, 13)
		else 000
	end as escolaridad,
	case
		when obter_idade(a.dt_nascimento, sysdate, 'A') > 5 
		then decode(a.ie_grau_instrucao, 11,0, 14, 9, 1) 
		when a.ie_grau_instrucao in (7,8,3,9,4) 
		then 1
		else 000
	end as sabeleerescribir,
	case
		when obter_idade(a.dt_nascimento, b.dt_entrada, 'A') is not null 
		then obter_idade_imc_nom(b.nr_atendimento,a.dt_nascimento,b.dt_entrada,'IDADE')
	end as edad,
	case
		when obter_idade(a.dt_nascimento, b.dt_entrada, 'A') is not null 
		then obter_idade_imc_nom(b.nr_atendimento,a.dt_nascimento,b.dt_entrada,'CLAVEEDAD')
	end as claveedad,
	decode(a.ie_sexo, 'F', 2, 'M', 1) sexo,
	k.cd_der_lesiones afiliacion,
	case
		when k.cd_der_lesiones = 12 
		then decode(n.ie_classif_contabil, '3', 1, 0) 
		else 000
	end as gratuidad,
	case 
		when n.ie_classif_contabil = 3 and k.cd_der_lesiones <> 0 and k.cd_der_lesiones <> 98
		then substr(n.cd_integracao,1,16)
		else 'NO OBLIGATORIO'
	end as numeroafiliacion,
	decode(k.cd_der_lesiones, 11, n.nr_prim_digitos,0) digitoverificador,
	decode(a.nr_seq_cor_pele, 201, 1, 0) seconsideraindigena,
	decode(a.nr_seq_lingua_indigena, null, 0, 1) hablalenguaindigena,
	decode(a.nr_seq_cor_pele, 201, nvl(a.nr_seq_lingua_indigena, '-1'), '-1') cuallengua,
	nvl(b.ie_paciente_gravida, '-1') mujerfertil,
	nvl(e.ie_deficiencia, '-1') discapacidad,
	--EVENTO-- 
	c.dt_notificacao fechaevento,
	c.dt_notificacao horaevento,
	c.ie_dia_festivo diafestivo,
	decode(d.ds_local_ocorrencia, 3,2,4,3,6,4,7,5,9,6,5,8,10,9,11,10,12) sitioocurrencia,
	d.nr_seq_entidade entidadocurrencia,
	d.nr_seq_municipio municipioocurrencia,
	d.nr_seq_localidade localidadocurrencia,
	case 
		when nvl(d.nr_seq_localidade, 9999) = 9999 
		then substr(upper(d.ds_complemento),1,50)
		else 'NO OBLIGATORIO'
	end otralocalidad,
	d.nr_seq_cod_postal codigopostal,
	d.ie_possue_codigo_postal seignoracp,
	d.nr_seq_tipo_via tipovialidad,
	d.nr_seq_vialidade nombrevialidad,
	d.nr_endereco numeroexteriornumerico,
	d.nr_seq_tipo_assen tipoasentamiento,
	d.nr_seq_assentamento nombreasentamiento,
	--ATENCI�N PRE-HOSPITALARIA-- 
	decode(c.dt_tempo_translado, null, 'N', 'S') atencionprehospitalaria,
	decode(c.dt_tempo_translado, 'S', to_char(c.dt_tempo_translado, 'hh:mm')) tiempotrasladouh,
	e.ie_efeito_drogas_alcool sospechabajoefectosde,
	--CIRCUNSTANCIAS EN LAS QUE OCURRI� EL EVENTO-- 
	v.ie_tipo_violencia intencionalidad,
	case 
		when v.ie_tipo_violencia in (2,3,4) 
		then v.ie_causador_violencia
		else -1
	end as eventorepetido,
	case
		when r.ie_violencia_fisica = 1 or r.ie_violencia_sexual = 1 
		then 97
		else v.ie_agente_violencia
	end as agentelesion,
	decode(v.ie_agente_violencia, 25, substr(trim(v.ds_outro),1,50)) especifique,
	--en caso de accidente-- 
	decode(v.ie_agente_violencia, 20, v.ie_tipo_condutor, '-1') lesionadovehiculomotor,
	case
		when v.ie_tipo_condutor in (1.2)
		then v.ie_sinto_seguranca
		else -1
	end as usoequiposeguridad,
	decode(v.ie_sinto_seguranca, 1,v.ie_item_utilizado, '-1') equipoutilizado,
	decode(v.ie_item_utilizado, 4,(' '||substr(v.ds_item_utilizado,1,50)||' ')) especifiqueequipo,
	--EN CASO DE VIOLENCIA--
	nvl(substr(decode(r.ie_violencia_fisica,'s', chr(38)||'6', null)||decode(r.ie_violencia_sexual, 'S', chr(38)||'7', null)||decode(r.ie_violencia_psicologica, 'S' , chr(38)||'8', null)||decode(r.ie_violencia_fin_eco, 'S', chr(38)||'9', null)||decode(r.ie_neglicegencia_abandono,'S', chr(38)||'10', null), 2,5), '-1') as tipoviolencia , 
	--DATOS DEL AGRESOR-- 
	case 
		when v.ie_causador_violencia = 1 
		then  
		case 
			when ie_tipo_violencia in (2,3) 
			then 
			case 
				when j.ie_vinculo_pai = 'S'  then 1 
				when j.ie_vinculo_mae = 'S' then 2
				when j.ie_vinculo_filho = 'S' then 6
				when j.ie_vinculo_padastro = 'S' then 8
				when j.ie_vinculo_madastra = 'S' then 9
				when j.ie_vinculo_desconhecido = 'S' then 17
				when j.ds_outro_vinculo = 'S' then 19
			end
		end
		else -1 
	end as parentescoafetado,   
	case 
		when v.ie_tipo_violencia in (2,3)
		then j.nr_envolvido
		else -1
	end as numeroagresores,
	case
		when v.ie_tipo_violencia = 2 and j.nr_envolvido = 1 and j.ds_outro_vinculo = 'S'
		then j.ds_outro_vinculo 
	end as especifiqueparentesco,
	case
		when v.ie_tipo_violencia in (2,3)
		then j.ie_uso_alcool 
		else -1
	end as agresorbajoefectos,
		--atenci�n m�dica-- 
	b.dt_entrada fechaingreso,
	b.dt_entrada horaingreso,
	ss.ie_servico_especializado servicioatencion,
	c.ie_tipo_atendimento tipoatencion,
	v.ie_local_violencia areaanatomica,
	decode(v.ie_local_violencia, 16, substr(trim(v.ds_local_violencia),1,50), 'NO OBLIGATORIO') especifiquearea,
	v.ie_consequencia_violencia consecuenciagravedad,
	decode(v.ie_consequencia_violencia, 22, substr(trim(v.ds_outra_consequencia),1,50), 'NO OBLIGATORIO') especifiqueconsecuencia,
	b.dt_saida_real mesestadistico,
	elimina_caractere_especial(substr(upper((select max(cd.ds_doenca_cid) 
						from diagnostico_doenca dd, cid_doenca cd 
						where dd.nr_atendimento = b.nr_atendimento 
						and dd.cd_doenca = cd.cd_doenca_cid 
						and dd.ie_classificacao_doenca = 'P')),1, 255)) descripcionafeccionprincipal,
	(select max(cc1.cd_doenca)
	from 	cid_doenca cc1, diagnostico_doenca dd 
	where (cc1.cd_categoria_cid like ('%T%') or cc1.cd_categoria_cid like ('%S%') or cc1.cd_categoria_cid like ('%F%')) or (cc1.cd_doenca_cid in  ('O04','O05','O06','','O07','O20','O267','O267','O429','O468','O469','O68','O710','O713','O714','O715','O716','O717','O718','O719','O95','O9'))
	and cc1.cd_doenca_cid like dd.cd_doenca and dd.nr_atendimento = b.nr_atendimento) afeccionprincipal,
	--afecciones-- 
	substr(get_afecciones_sinan(c.nr_atendimento),1,2000) afeccionlesione,
	trim((select max(dd.cd_doenca) from diagnostico_doenca dd where dd.nr_atendimento = b.nr_atendimento and dd.ie_tipo_doenca = 'S')) afeccionreseleccionada,
	substr(trim(substr(obter_desc_cid((select max(dd.cd_doenca) from diagnostico_doenca dd where dd.nr_atendimento = b.nr_atendimento)),1, 250)),1,250) causaexterna,
	t.ds_observacao codigociecausaexterna,
	decode(v.ie_consequencia_violencia, 19, nvl(v.ie_destino, 5)) despuesatencion,
	decode(v.ie_destino, 11, substr(trim(v.ds_outro_destino),1,250), 'NO OBLIGATORIO') especifiquedestino,
	case
		when v.ie_destino = 5 and ie_envio_ministerio = 2
		then substr(xx.nr_declaracao, 1,9)
		else 'NO OBLIGATORIO'
	end as foliocertificadodefuncion,
	decode(v.ie_destino,5, v.ie_envio_ministerio, -1) ministeriopublico,
	--profesional responsable de la salud--  
	nvl(v.ie_tipo_atendimento,'-1') responsableatencion,
	aa.cd_curp curpresponsable,
        yy.ds_given_name nombreresponsable,
	yy.ds_family_name primerapellidoresponsable,
	yy.ds_component_name_1 segundoapellidoresponsable,
	nvl(aa.ds_codigo_prof, mm.nr_crm) cedulaprofesional, 
	b.dt_entrada dt_entrada,
	c.cd_paciente cd_pessoa_fisica,
	pj.cd_cgc cd_cgc,
	n.cd_cgc cd_cgc_convenio,
	c.dt_notificacao dt_notificacao
from    pessoa_fisica a,
	pessoa_fisica aa,
	atendimento_paciente b,
	notificacao_sinan c,
	sinan_dados_ocorrencia d,
	sinan_dados_complementares e,
	sinan_violencia r,
	sinan_inf_complementar t,
	nacionalidade i,
	sus_municipio s,
	cat_entidade ce,
	sinan_inf_violencia v,
	sinan_dados_agressao j,
	declaracao_obito xx,
	atend_categoria_convenio p,
	categoria_convenio m,
	convenio n,
	cat_derechohabiencia k,
	classificacao_atendimento ss,
	estabelecimento ee,
	pessoa_juridica pj,
	medico mm,
	person_name Y,
	person_name YY
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and 	y.nr_sequencia 	(+)	= a.nr_seq_person_name
and	y.ds_type (+) = 'main'
and	c.nr_atendimento	= b.nr_atendimento
and     e.nr_seq_notificacao(+)	= c.nr_sequencia
and     d.nr_seq_notificacao(+) = c.nr_sequencia
and     v.nr_seq_notificacao(+)	= c.nr_sequencia
and     j.nr_seq_notificacao(+)	= c.nr_sequencia
and     t.nr_seq_notificacao(+)	= c.nr_sequencia
and     r.nr_seq_notificacao(+)	= c.nr_sequencia
and	i.cd_nacionalidade (+) 	= a.cd_nacionalidade 
and     s.nr_seq_entidade_mx 	= ce.nr_sequencia (+)
and 	s.cd_municipio_ibge (+) = a.cd_municipio_ibge
and	xx.nr_atendimento (+) 	= b.nr_atendimento
and 	p.nr_atendimento 	= b.nr_atendimento
and	p.cd_convenio 		= m.cd_convenio 
and	p.cd_categoria 		= m.cd_categoria 
and	n.cd_convenio 		= m.cd_convenio 
and	n.cd_tipo_convenio_mx 	= k.nr_sequencia(+) 
and	aa.cd_pessoa_fisica  	= b.cd_medico_resp (+) 
and	b.nr_seq_classificacao 	= ss.nr_sequencia (+)
and 	c.nr_seq_doenca_compulsoria = 109
and	b.cd_estabelecimento 	= ee.cd_estabelecimento
and	pj.cd_cgc 		= ee.cd_cgc (+)
and	mm.cd_pessoa_fisica 	= aa.cd_pessoa_fisica
and	yy.nr_sequencia (+)	= aa.nr_seq_person_name
and	yy.ds_type (+) = 'main';
/