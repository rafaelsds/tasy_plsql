Create or replace View HMC_RELAT_PAGAR_CONTA_CTB_V as
select 	a.CD_ESTABELECIMENTO cd_estabelecimento,
		substr(OBTER_NOME_ESTABELECIMENTO(a.CD_ESTABELECIMENTO),1,255) 	ds_estabelecimento,
		a.NR_TITULO NR_TITULO,
		null cd_pf,
		a.cd_cgc cd_pj,
		j.ds_razao_social nm_pessoa,
		'1' ie_tipo_pessoa,
		a.VL_TITULO VL_TITULO,
		a.DT_EMISSAO DT_EMISSAO,
		a.DT_VENCIMENTO_ATUAL dt_venc_atual,
		a.DT_VENCIMENTO_ORIGINAL dt_venc_original,
		a.DT_CONTABIL DT_CONTABIL,
		a.DT_LIQUIDACAO DT_LIQUIDACAO,
		b.dt_baixa dt_pagamento,
		b.NR_SEQUENCIA seq_baixa,
		a.IE_ORIGEM_TITULO IE_ORIGEM_TITULO,
		substr(obter_valor_dominio(500,a.IE_ORIGEM_TITULO),1,40) ds_origem_titulo,
		substr(obter_valor_dominio(501,a.IE_SITUACAO),1,40) ds_situacao_titulo, 
		a.NR_SEQ_CLASSE nr_seq_classe,
		obter_desc_classe_cpa(a.NR_SEQ_CLASSE) ds_classe,
		b.VL_BAIXA vl_documento,
		b.VL_PAGO vl_pago,
		b.VL_DESCONTOS vl_desconto,
		b.VL_JUROS vl_juros,
		b.VL_MULTA vl_multa,
		j.CD_CONTA_CONTABIL cd_conta_contabil,   
		obter_dados_conta_contabil(j.CD_CONTA_CONTABIL, 1, 'CL') classificacao_cc,
		obter_desc_conta_contabil(j.CD_CONTA_CONTABIL) ds_cc,  
		a.NR_SEQ_NOTA_FISCAL nf  		
from 	titulo_pagar a,
		TITULO_PAGAR_BAIXA b,
		pessoa_juridica j
where	a.cd_cgc = j.cd_cgc
and		a.nr_titulo = b.nr_titulo(+)
group by a.CD_ESTABELECIMENTO, OBTER_NOME_ESTABELECIMENTO(a.CD_ESTABELECIMENTO), a.NR_TITULO, null, a.cd_cgc, j.ds_razao_social, a.VL_TITULO , a.DT_EMISSAO, a.DT_VENCIMENTO_ATUAL, a.DT_VENCIMENTO_ORIGINAL,a.DT_CONTABIL,a.DT_LIQUIDACAO, b.dt_baixa,
	b.NR_SEQUENCIA, a.IE_ORIGEM_TITULO, obter_valor_dominio(500,a.IE_ORIGEM_TITULO), obter_valor_dominio(501,a.IE_SITUACAO), a.NR_SEQ_CLASSE, obter_desc_classe_cpa(a.NR_SEQ_CLASSE), b.VL_BAIXA, b.VL_PAGO, b.VL_DESCONTOS, b.VL_JUROS, b.VL_MULTA, j.CD_CONTA_CONTABIL, obter_dados_conta_contabil(j.CD_CONTA_CONTABIL, 1, 'CL'), obter_desc_conta_contabil(j.CD_CONTA_CONTABIL), a.NR_SEQ_NOTA_FISCAL
union all
select 	a.CD_ESTABELECIMENTO cd_estabelecimento,
		substr(OBTER_NOME_ESTABELECIMENTO(a.CD_ESTABELECIMENTO),1,255) ds_estabelecimento,
		a.NR_TITULO NR_TITULO,
		f.CD_PESSOA_FISICA cd_pf,
		null cd_pj,
		f.nm_pessoa_fisica nm_pessoa,
		'2' ie_tipo_pessoa,
		a.VL_TITULO VL_TITULO,
		a.DT_EMISSAO DT_EMISSAO,
		a.DT_VENCIMENTO_ATUAL dt_venc_atual,
		a.DT_VENCIMENTO_ORIGINAL dt_venc_original,
		a.DT_CONTABIL DT_CONTABIL,
		a.DT_LIQUIDACAO DT_LIQUIDACAO,
		b.dt_baixa dt_pagamento,
		b.NR_SEQUENCIA seq_baixa,	
		a.IE_ORIGEM_TITULO IE_ORIGEM_TITULO,		
		substr(obter_valor_dominio(500,a.IE_ORIGEM_TITULO),1,40) ds_origem_titulo,
		substr(obter_valor_dominio(501,a.IE_SITUACAO),1,40) ds_situacao_titulo, 
		a.NR_SEQ_CLASSE nr_seq_classe,
		obter_desc_classe_cpa(a.NR_SEQ_CLASSE) ds_classe,
		b.VL_BAIXA vl_documento,
		b.VL_PAGO vl_pago,			 
		b.VL_DESCONTOS vl_desconto,
		b.VL_JUROS vl_juros,
		b.VL_MULTA vl_multa,
		max(c.cd_conta_contabil) cd_conta_contabil,
		obter_dados_conta_contabil(max(c.cd_conta_contabil), 1, 'CL') classificacao_cc,
		obter_desc_conta_contabil(max(c.cd_conta_contabil)) ds_cc,  
		a.NR_SEQ_NOTA_FISCAL nf  		
from 	titulo_pagar a,
		TITULO_PAGAR_BAIXA b,
		pessoa_fisica f,
		PESSOA_FISICA_CONTA_CTB c
where	a.cd_pessoa_fisica = f.cd_pessoa_fisica
and	a.nr_titulo = b.nr_titulo(+)
and	f.cd_pessoa_fisica = c.cd_pessoa_fisica(+)
group by a.CD_ESTABELECIMENTO, OBTER_NOME_ESTABELECIMENTO(a.CD_ESTABELECIMENTO), a.NR_TITULO,f.CD_PESSOA_FISICA, null, f.nm_pessoa_fisica, a.VL_TITULO , a.DT_EMISSAO, a.DT_VENCIMENTO_ATUAL, a.DT_VENCIMENTO_ORIGINAL,a.DT_CONTABIL,a.DT_LIQUIDACAO, b.dt_baixa,
	b.NR_SEQUENCIA, a.IE_ORIGEM_TITULO, obter_valor_dominio(500,a.IE_ORIGEM_TITULO), obter_valor_dominio(501,a.IE_SITUACAO),a.NR_SEQ_CLASSE, obter_desc_classe_cpa(a.NR_SEQ_CLASSE), b.VL_BAIXA, b.VL_PAGO, b.VL_DESCONTOS, b.VL_JUROS, b.VL_MULTA, a.NR_SEQ_NOTA_FISCAL
order by nm_pessoa
/