create or replace view prescr_proc_carestream_v as
select
    b.nr_atendimento,
    a.nr_prescricao,
    a.nr_sequencia nr_seq_item_prescr, --Numero do Item da Prescric?o
    a.nr_seq_interno,
    a.nr_acesso_dicom na_accessionnumber,
    b.dt_prescricao,
    nvl(b.dt_liberacao, b.dt_prescricao) dt_liberacao,
    nvl(b.dt_liberacao_medico, b.dt_prescricao) dt_liberacao_medico,
    decode(a.ie_suspenso,'S','E','I') ds_tipo_movimento,
    b.cd_pessoa_fisica,
    c.nr_prontuario,
    c.nm_pessoa_fisica nm_paciente,
    c.dt_nascimento,
    decode(c.ie_sexo,'1','M','2','F',c.ie_sexo) ie_sexo,
    c.nr_cpf,
    c.nr_identidade,
    c.qt_altura_cm,
    c.qt_peso,
    c.nr_telefone_celular,
    c.ie_estado_civil,
    w.cd_profissao,             -- cod. Profiss?o
    substr(obter_desc_profissao(w.cd_profissao),1,80) ds_profissao, -- desc profissao
    c.nr_seq_cor_pele,
    z.ds_endereco||', '||z.nr_endereco||', '||z.ds_complemento||' '||z.ds_bairro||' '||z.ds_municipio||'/'||z.sg_estado ds_endereco_compl, --Endereco Completo do Paciente / N?o Tem Cidade e UF
    substr(pacs_elimina_caract_especial(z.ds_municipio),1,255) ds_municipio,
    z.nr_endereco,
    substr(pacs_elimina_caract_especial(z.ds_bairro),1,255) ds_bairro,
    substr(pacs_elimina_caract_especial(z.ds_complemento),1,255) ds_complemento,
    z.cd_cep,
    substr(pacs_elimina_caract_especial(z.sg_estado),1,255) sg_estado,
    z.nr_telefone,
    z.ds_email,              -- email
    w.nr_telefone nr_telefone_com,          --Telefone Comercial do Paciente
    a.cd_procedimento,
    p.ds_procedimento,
    a.ie_lado,
    p.cd_tipo_procedimento,
    a.qt_procedimento,
    a.dt_atualizacao,
    a.nm_usuario,
    substr(pacs_elimina_caract_especial(a.ds_observacao),1,255) ds_observacao,
    /*substr(pacs_elimina_caract_especial(a.ds_observacao ||
           decode(a.ds_observacao, null, null, chr(10) || chr(13) || ' ') || a.ds_dado_clinico),1,255) ds_observacao,*/
    a.ie_origem_proced,
    a.ie_urgencia,
--    substr(pacs_elimina_caract_especial(a.ds_dado_clinico),1,255) ds_dado_clinico,
    substr(pacs_elimina_caract_especial(
         decode(a.ds_dado_clinico, null, a.ds_justificativa,
                a.ds_dado_clinico || chr(13) || a.ds_justificativa)),1,255) ds_dado_clinico,
    a.ie_suspenso,

    b.ie_recem_nato,

    t.CD_MEDICO_RESP,
    nvl(t.crm_medico_externo,me.nr_crm )crm_medico_atendimento,
    nvl(t.nm_medico_externo, pf.nm_pessoa_fisica) nm_medico_atendimento,
    b.cd_medico,
       -----------------------------------------------------------------------------------------------------------------------
       --                                ***     M�DICO SOLICITANTE     ***                                                 --
       --                                                                                                                   --
       --  Os crit�rios para definir o M�dico Solicitante.                                                                  --
       --    1. Caso a prescri��o seja de uma Cirurgia e a prescri��o n�o tenha sido liberada.                              --
       --       Pego o m�dico do atendimento como solicitante, obedecendo a seguinte ordem                                  --
       --       1� CRM e Nome M�dico "Externo".                                                                             --
       --       2� CRM e Nome M�dico Referido.                                                                              --
       --       3� CRM e Nome M�dico Respos�vel (Abertura do Atendimento).                                                  --
       --                                                                                                                   --
       --    2. Caso a Prescri��o n�o seja de uma Cirurgia (tem que estar liberada)                                         --
       --       Pego o m�dico da Prescri��o, obedecendo a seguinte Ordem                                                    --
       --       1� CRM do M�dico Externo                                                                                    --
       --       2� CRM do M�dico que liberou a prescri��o.                                                                  --
       --                                                                                                                   --
       -----------------------------------------------------------------------------------------------------------------------
            case
         when ((select count(*) from   cirurgia where  cirurgia.nr_prescricao = b.nr_prescricao) > 0 and b.dt_liberacao is null) then
          nvl(t.crm_medico_externo, nvl(mRef.Nr_Crm, t.cd_medico_resp))
         else
          nvl(b.crm_medico_externo, e.nr_crm)
       end nr_crm,

       case
       -- Caso a prescri��o seja de uma Cirurgia e a prescri��o n�o tenha sido liberada.
         when ((select count(*)
                from   cirurgia
                where  cirurgia.nr_prescricao = b.nr_prescricao) > 0 and b.dt_liberacao is null) then

         nvl(t.nm_medico_externo, nvl( mref.nm_guerra, me.nm_guerra))
       else
         nvl(b.nm_medico_externo, d.nm_pessoa_fisica)
         end  nm_medico, --Nome do Medico Solicitante


    nvl(pep.crm_medico_externo,i.nr_crm) nr_crm_medico_exec,           --CRM do Medico Executante
    nvl(pep.nm_medico_externo,substr(obter_nome_pf(i.cd_pessoa_fisica),1,255)) nm_medico_exec,  --Nome medico executor da prescric?o
    f.cd_convenio,                    --Codigo do Convenio
    f.cd_categoria,
    f.cd_usuario_convenio,
    b.cd_estabelecimento,             --Codigo da Unidade/Site do Hospital
    substr(pacs_elimina_caract_especial(v.ds_convenio),1,255) ds_convenio,        --Convenio
    a.cd_material_exame,
    substr(pacs_elimina_caract_especial(a.ds_material_especial),1,255) ds_material_especial,
    nvl(a.ie_amostra,'N') ie_amostra_entregue,
    a.ds_horarios,
    a.nr_seq_exame,
    substr(pacs_elimina_caract_especial(z.ds_endereco),1,255) ds_endereco,
    to_char(b.dt_prescricao, 'hh24:mi:ss') hr_prescricao,    --Hora da Prescric?o
--    a.dt_prev_execucao,
-- Incluso data prevista da cirurgia, pois nos casos de cirurgia o sistema nao gera data prevista de execucao. //Fabricio-Rogerio 27/12/2016
    case
      when a.dt_prev_execucao is null then
        (select  cc.dt_inicio_prevista
                 from cirurgia cc where cc.nr_prescricao = a.nr_prescricao)
      else
         a.dt_prev_execucao
    End  dt_prev_execucao,

    g.cd_unidade_basica || ' ' || cd_unidade_compl cd_unidade,
    a.ie_executar_leito,
    obter_modalidade_wtt(p.cd_tipo_procedimento) ds_modalidade,
    t.ie_tipo_atendimento,
    (select ds_valor_dominio from valor_dominio where cd_dominio = 12 and vl_dominio = t.ie_tipo_atendimento) ds_tipo_atendimento, -- desc tipo atendimento
    a.nr_seq_proc_interno,
    f.cd_plano_convenio,             --Codigo do Plano do Convenio
    g.cd_unidade_basica ds_unidade_atual_paciente,       --Codigo do Leito onde o Paciente se encontra
    substr(pacs_elimina_caract_especial(substr(Obter_desc_leito_unid(Obter_Atepacu_paciente(t.nr_atendimento, 'A')),1,100)),1,100) ds_leito_atual_paciente,--Descric?o do Leito onde o Paciente se encontra
    substr(pacs_elimina_caract_especial(substr(obter_desc_plano(f.cd_convenio, f.cd_plano_convenio),1,40)),1,40) ds_plano_convenio,--Descric?o do Plano do Convenio
    substr(obter_desc_plano(f.cd_convenio, f.cd_plano_convenio),1,40) ds_plano,--Plano
    (select MAX(r.dt_integracao) from laudo_paciente r where r.nr_prescricao = b.nr_prescricao)dt_integracao,--Data que foi integrado
    (select MAX(r.nm_usuario_digitacao) from laudo_paciente r where r.nr_prescricao = b.nr_prescricao)nm_usuario_digitacao,--Codigo do Usuario Digitador

    (select  x.cd_agenda
    from   agenda_paciente x
    where   x.nr_sequencia = a.nr_seq_agenda) cd_agenda,
    (select  x.ds_agenda
    from   agenda x,
      agenda_paciente y
    where   x.cd_agenda = y.cd_agenda
    and  y.nr_sequencia = a.nr_seq_agenda) ds_agenda,
    ap.dt_agenda || ap.hr_inicio data_agenda,

    -- Definic?es dos setores de atendimento
    g.cd_setor_atendimento  cd_setor_paciente, --Localizac?o do Paciente
    s.ds_setor_atendimento  ds_setor_paciente,       -- Desc setor paciente
    a.cd_setor_atendimento  cd_setor_atendimento_exec, -- Codigo do setor de Execuc?o
    obter_ds_setor_atendimento( a.cd_setor_atendimento) ds_setor_atendimento_exec, -- Descric?o do setor Executante,
    t.dt_entrada,     -- Data de entrada do paciente / Inicio do atendimento
      --  (select MAX(r.dt_prev_entrega) from laudo_paciente r where r.nr_prescricao = b.nr_prescricao)dt_prev_entrega,--Data Prevista para Entrega do Laudo
    a.dt_resultado   dt_prev_entrega,
    null ds_justificativa
    /*substr(pacs_elimina_caract_especial(
         decode(a.ds_dado_clinico, null, a.ds_justificativa,
                a.ds_dado_clinico || chr(13) || a.ds_justificativa)),1,255) ds_justificativa   */

FROM  setor_atendimento s,
      procedimento p,
      convenio v,
      compl_pessoa_fisica z,
      compl_pessoa_fisica w,
      atend_paciente_unidade g,
      atend_categoria_convenio f,
      dual x,
      medico e,
      medico i,
      medico me,
      medico                   mRef, --M�dico Referido
      atendimento_paciente t,
      atendimento_paciente t2,
      pessoa_fisica d,
      pessoa_fisica c,
      pessoa_fisica pf,
      prescr_medica b,
      prescr_procedimento a,
      agenda_paciente ap,
      PRESCR_PROCED_PROF_ADIC PEP

where   a.cd_motivo_baixa = 0
and   a.dt_integracao is null
and   a.cd_procedimento    = p.cd_procedimento
and   a.ie_origem_proced    = p.ie_origem_proced

and    (nvl(b.dt_liberacao_medico, b.dt_liberacao) is not null or
      0 < (select count(*)
             from   cirurgia aa
             where  aa.nr_prescricao = b.nr_prescricao
))

-- n?o apresentar registro que tenha erro registrado pela Carestream.
and    0 = (select count(*)
            from   PRESCR_PROC_CARE_ERRO ee
            where  a.nr_acesso_dicom = ee.nr_acesso_dicom
              and ee.dt_tratamento_erro is null
              and ee.nr_sequencia = ( select max(ef.nr_sequencia) from prescr_proc_care_erro ef
                                      where ef.nr_acesso_dicom = ee.nr_acesso_dicom
                                    )
            )

and   a.nr_prescricao     = b.nr_prescricao
and   b.cd_pessoa_fisica     = c.cd_pessoa_fisica
and   b.cd_pessoa_fisica    = z.cd_pessoa_fisica (+)
and   1        = z.ie_tipo_complemento (+)
and   b.cd_pessoa_fisica    = w.cd_pessoa_fisica (+)
and   2          = w.ie_tipo_complemento (+)
and   b.cd_medico       = d.cd_pessoa_fisica
and   b.cd_medico       = e.cd_pessoa_fisica (+)
and    a.cd_medico_exec    = i.cd_pessoa_fisica (+)
and   g.nr_seq_interno    = obter_atepacu_paciente(b.nr_atendimento,'A')
and   g.cd_setor_atendimento    = s.cd_setor_atendimento
and    Obter_Se_Integr_Proc_Interno(a.nr_seq_proc_interno, 15, null, a.ie_lado, b.cd_estabelecimento) = 'S'and   f.nr_atendimento      = b.nr_atendimento
and   t.nr_atendimento      = b.nr_atendimento
and   f.dt_inicio_vigencia     = (  select max(w.dt_inicio_vigencia)
            from atend_categoria_convenio w
            where w.nr_atendimento  = b.nr_atendimento)
and   f.cd_convenio               = v.cd_convenio
and a.nr_seq_agenda = ap.nr_sequencia(+)
and a.nr_prescricao = pep.nr_prescricao(+)
and a.nr_sequencia  = pep.nr_seq_procedimento(+)
and t2.nr_atendimento = b.nr_atendimento (+)
and  me.cd_pessoa_fisica = t2.cd_medico_resp (+)
and pf.cd_pessoa_fisica = me.cd_pessoa_fisica(+)
and    t.cd_medico_referido = mRef.Cd_Pessoa_Fisica(+)



union

select b.nr_atendimento,
       a.nr_prescricao,
       0                  nr_seq_item_prescr, --Numero do Item da Prescric?o
       to_number(cc.nr_acesso_dicom) nr_seq_interno,
       cc.nr_acesso_dicom na_accessionnumber,
       cc.dt_atualizacao  dt_prescricao,
       cc.dt_atualizacao  dt_liberacao,
       cc.dt_atualizacao  dt_liberacao_medico,

       -- O Campo "ds_tipo_movimento", define para a Carestream se � um cancelamento ou uma inser��o de exame
       -- Valoes possiveis
       -- [I] : Inser��o/Exame novo
       -- [E] : Cancelamento
       case
         when cc.dt_cancelamento is null then
          'I'
         else
          'E'
       end ds_tipo_movimento,

       c.cd_pessoa_fisica,
       c.nr_prontuario,
       c.nm_pessoa_fisica nm_paciente,
       c.dt_nascimento,
       decode(c.ie_sexo, '1', 'M', '2', 'F', c.ie_sexo) ie_sexo,
       c.nr_cpf,
       c.nr_identidade,
       c.qt_altura_cm,
       c.qt_peso,
       c.nr_telefone_celular,
       c.ie_estado_civil,
       w.cd_profissao,
       substr(obter_desc_profissao(w.cd_profissao), 1, 80) ds_profissao,
       c.nr_seq_cor_pele,
       z.ds_endereco || ', ' || z.nr_endereco || ', ' || z.ds_complemento || ' ' || z.ds_bairro || ' ' || z.ds_municipio || '/' || z.sg_estado ds_endereco_compl, --Endereco Completo do Paciente / N?o Tem Cidade e UF
       substr(pacs_elimina_caract_especial(z.ds_municipio), 1, 255) ds_municipio,
       z.nr_endereco,
       substr(pacs_elimina_caract_especial(z.ds_bairro), 1, 255) ds_bairro,
       substr(pacs_elimina_caract_especial(z.ds_complemento), 1, 255) ds_complemento,
       z.cd_cep,
       substr(pacs_elimina_caract_especial(z.sg_estado), 1, 255) sg_estado,
       z.nr_telefone,
       z.ds_email, -- email
       w.nr_telefone nr_telefone_com, --Telefone Comercial do Paciente
       a.cd_procedimento_princ cd_procedimento,
       p.ds_procedimento,
       a.ie_lado,
       p.cd_tipo_procedimento,
       1 qt_procedimento,
       a.dt_atualizacao,
       a.nm_usuario,
       substr(pacs_elimina_caract_especial(a.ds_observacao), 1, 255) ds_observacao,
       a.ie_origem_proced,
       'N' ie_urgencia,
       null ds_dado_clinico,
       case
         when cc.dt_cancelamento is null then
          'N'
         else
          'S'
       end ie_suspenso,
       b.ie_recem_nato,

       -- M�dico respons�vel pelo atendimento
       t.cd_medico_resp,
       nvl(t.crm_medico_externo, mra.nr_crm) crm_medico_atendimento,
       nvl(t.nm_medico_externo, mra.nm_guerra) nm_medico_atendimento,

       -- M�dico Solicitante
       nvl(t.cd_medico_referido, t.cd_medico_resp) cd_medico,
       nvl(t.crm_medico_externo, nvl(mr.nr_crm, mra.nr_crm)) nr_crm,
       nvl(t.nm_medico_externo, nvl(mr.nm_guerra, mra.nm_guerra)) nm_medico,

       -- M�dico Executante
       me.nr_crm    nr_crm_medico_exec,
       me.nm_guerra nm_medico_exec,

       f.cd_convenio, --Codigo do Convenio
       f.cd_categoria,
       f.cd_usuario_convenio,
       b.cd_estabelecimento,

       substr(pacs_elimina_caract_especial(v.ds_convenio), 1, 255) ds_convenio, --Convenio
       '' cd_material_exame,
       '' ds_material_especial,
       'N' ie_amostra_entregue,
       '' ds_horarios,
       null nr_seq_exame,
       substr(pacs_elimina_caract_especial(z.ds_endereco), 1, 255) ds_endereco,
       to_char(b.dt_prescricao, 'hh24:mi:ss') hr_prescricao,
       a.dt_inicio_prevista dt_prev_execucao,

       g.cd_unidade_basica || ' ' || cd_unidade_compl cd_unidade,
       '' ie_executar_leito,
       obter_modalidade_wtt(p.cd_tipo_procedimento) ds_modalidade,
       t.ie_tipo_atendimento,
       obter_valor_dominio(12, t.ie_tipo_atendimento) ds_tipo_atendimento,

       a.nr_seq_proc_interno,
       f.cd_plano_convenio, --Codigo do Plano do Convenio
       g.cd_unidade_basica ds_unidade_atual_paciente, --Codigo do Leito onde o Paciente se encontra
       substr(pacs_elimina_caract_especial(substr(Obter_desc_leito_unid(Obter_Atepacu_paciente(t.nr_atendimento, 'A')), 1, 100)), 1, 100) ds_leito_atual_paciente, --Descric?o do Leito onde o Paciente se encontra
       substr(pacs_elimina_caract_especial(substr(obter_desc_plano(f.cd_convenio, f.cd_plano_convenio), 1, 40)), 1, 40) ds_plano_convenio, --Descric?o do Plano do Convenio
       substr(obter_desc_plano(f.cd_convenio, f.cd_plano_convenio), 1, 40) ds_plano, --Plano

       (select MAX(r.dt_integracao)
        from   laudo_paciente r
        where  r.nr_prescricao = b.nr_prescricao) dt_integracao, --Data que foi integrado
       (select MAX(r.nm_usuario_digitacao)
        from   laudo_paciente r
        where  r.nr_prescricao = b.nr_prescricao) nm_usuario_digitacao, --Codigo do Usuario Digitador

       (select x.cd_agenda
        from   agenda_paciente x
        where  x.nr_sequencia = a.nr_seq_agenda) cd_agenda,
       (select x.ds_agenda
        from   agenda          x,
               agenda_paciente y
        where  x.cd_agenda = y.cd_agenda
        and    y.nr_sequencia = a.nr_seq_agenda) ds_agenda,
       ap.dt_agenda || ap.hr_inicio data_agenda,

       -- Defini��o para os setores de atendimento
       g.cd_setor_atendimento cd_setor_paciente, --Localizac?o do Paciente
       obter_ds_setor_atendimento(g.cd_setor_atendimento), -- Desc setor paciente
       a.cd_setor_atendimento cd_setor_atendimento_exec, -- Codigo do setor de Execuc?o
       obter_ds_setor_atendimento(a.cd_setor_atendimento) ds_setor_atendimento_exec, -- Descric?o do setor Executante,
       t.dt_entrada,
       a.dt_inicio_prevista dt_prev_entrega,
       null ds_justificativa

from   cirurgia_carestream cc

inner  join cirurgia a
on     cc.nr_cirurgia = a.nr_cirurgia

inner  join prescr_medica b
on     cc.nr_prescricao = b.nr_prescricao

-- Pessoa_fisica com o "Alias [C] = Paciente"
inner  join pessoa_fisica c
on     b.cd_pessoa_fisica = c.cd_pessoa_fisica

-- Complemento para o paciente "Endere�o Comercial".
left   outer join compl_pessoa_fisica w
on     b.cd_pessoa_fisica = w.cd_pessoa_fisica
and    2 = w.ie_tipo_complemento

-- Complemento para o paciente "Endereco Residencial"
left   outer join compl_pessoa_fisica z
on     b.cd_pessoa_fisica = z.cd_pessoa_fisica
and    1 = z.ie_tipo_complemento

inner  join procedimento p
on     a.cd_procedimento_princ = p.cd_procedimento
and    a.ie_origem_proced = p.ie_origem_proced

left   outer join atendimento_paciente t
on     b.nr_atendimento = t.nr_atendimento

-- Liga��o com a tabela de m�dico, representando o M�dico Respons�vel [MRA]
left   outer join medico mra
on     t.cd_medico_resp = mra.cd_pessoa_fisica

-- Liga��o com a tabela de m�dico, respresentando o M�dico Referido  [MR]
left   outer join medico mr
on     t.cd_medico_referido = mr.cd_pessoa_fisica

-- Liga��o com a tabela de m�dico, representando o M�dico Executante [ME]
left   outer join medico me
on     a.cd_medico_cirurgiao = me.cd_pessoa_fisica

left   outer join atend_categoria_convenio f
on     b.nr_atendimento = f.nr_atendimento

inner  join convenio v
on     f.cd_convenio = v.cd_convenio

inner  join atend_paciente_unidade g
on     g.nr_seq_interno = obter_atepacu_paciente(b.nr_atendimento, 'A')

left   outer join agenda_paciente ap
on     a.nr_seq_agenda = ap.nr_sequencia

where  0 = 0

and    f.dt_inicio_vigencia = (select max(w.dt_inicio_vigencia)
                               from   atend_categoria_convenio w
                               where  w.nr_atendimento = b.nr_atendimento)



and    0 = (select count(*)
            from   PRESCR_PROC_CARE_ERRO ee
            where  cc.nr_acesso_dicom = ee.nr_acesso_dicom
            and    ee.dt_tratamento_erro is null
            and    ee.nr_sequencia = (select max(ef.nr_sequencia)
                                      from   prescr_proc_care_erro ef
                                      where  ef.nr_acesso_dicom = ee.nr_acesso_dicom));





/