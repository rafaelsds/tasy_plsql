create or replace view cus_paciente_cc_1_v
as
select 	a.cd_estabelecimento,
	a.dt_referencia,
	s.cd_centro_custo cd_centro_controle, 
	sum(a.nr_pac_dia) qt_distribuicao
from 	eis_censo_diario_v2 a,
	setor_atendimento s
where 	a.cd_setor_atendimento = s.cd_setor_atendimento
and 	substr(obter_nome_setor(a.cd_setor_atendimento),1,200) is not null 
and 	ie_periodo = 'M'
group by a.cd_estabelecimento,
	 a.dt_referencia,
	 s.cd_centro_custo;
/
