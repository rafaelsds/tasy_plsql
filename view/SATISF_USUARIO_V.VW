create or replace
view satisf_usuario_v as
/*select 	a.nm_pessoa_fisica nm_pessoa_fisica,
	a.cd_pessoa_fisica,
	count(z.nr_sequencia) qt_satisf,
	1 cd_opcao,
	c.nr_sequencia nr_seq_grupo_des,
	-1 nr_seq_grupo_sup,
	c.nr_seq_gerencia nr_seq_gerencia,
	z.dt_fim_real
from 	pessoa_fisica a,
	usuario b,
	grupo_desenvolvimento c,
	(	select	distinct e.nr_sequencia,
			e.nr_seq_grupo_des,
			f.nm_usuario_exec,
			e.dt_fim_real
		from 	man_ordem_servico e,
			man_ordem_servico_exec f
		where	e.nr_sequencia = f.nr_seq_ordem
		and	f.nm_usuario_exec = (	select	max(k.nm_usuario_exec) nm_usuario_exec
						from	man_ordem_servico_exec k
						where	k.nr_seq_ordem = e.nr_sequencia
						and exists (	select	1
								from	usuario_grupo_des l
								where	k.nm_usuario_exec = l.nm_usuario_grupo	)	)
		and	e.ie_grau_satisfacao in ('R', 'P')
		and	e.ie_classificacao in ('S', 'E')
		and	e.nr_seq_grupo_des is not null
		and	e.nr_seq_gerencia_insatisf is null	) z
where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	z.nr_seq_grupo_des = c.nr_sequencia
and	z.nm_usuario_exec = b.nm_usuario
group by a.nm_pessoa_fisica, a.cd_pessoa_fisica, c.nr_sequencia, c.nr_seq_gerencia, z.dt_fim_real
union all*/
select	a.nm_pessoa_fisica nm_pessoa_fisica,
	a.cd_pessoa_fisica,
	count(z.nr_sequencia) qt_satisf,
	2 cd_opcao,
	c.nr_sequencia nr_seq_grupo_des ,
	-1 nr_seq_grupo_sup,
	c.nr_seq_gerencia nr_seq_gerencia,
	z.dt_fim_real
from	pessoa_fisica a,
	usuario b,
	grupo_desenvolvimento c,
		(	select	distinct e.nr_sequencia,
				e.nr_seq_grupo_des,
				f.nm_usuario_exec,
				e.dt_fim_real
			from 	man_ordem_servico e,
				man_ordem_servico_exec f
			where	e.nr_sequencia = f.nr_seq_ordem
			and	f.nm_usuario_exec = (	select	max(k.nm_usuario_exec) nm_usuario_exec
							from	man_ordem_servico_exec k
							where	k.nr_seq_ordem = e.nr_sequencia
							and exists (	select	1
									from	usuario_grupo_des l
									where	k.nm_usuario_exec = l.nm_usuario_grupo	)	)
			and	exists (	select 	1
						from 	grupo_desenvolvimento 
						where 	nr_seq_gerencia = e.nr_seq_gerencia_insatisf	)
			and	e.ie_grau_satisfacao in ('R', 'P')
			and	e.nr_seq_grupo_des is not null
			and	e.nr_seq_gerencia_insatisf is not null	) z
where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	z.nr_seq_grupo_des = c.nr_sequencia
and	z.nm_usuario_exec = b.nm_usuario
group by a.nm_pessoa_fisica, a.cd_pessoa_fisica, c.nr_sequencia, c.nr_seq_gerencia, z.dt_fim_real
union all
/*select 	a.nm_pessoa_fisica nm_pessoa_fisica,
	a.cd_pessoa_fisica,
	count(z.nr_sequencia) qt_satisf,
	1 cd_opcao,
	-1 nr_seq_grupo_des,
	c.nr_sequencia nr_seq_grupo_sup,
	c.nr_seq_gerencia_sup nr_seq_gerencia,
	z.dt_fim_real
from 	pessoa_fisica a,
	usuario b,
	grupo_suporte c,
	(	select	distinct e.nr_sequencia,
			e.nr_seq_grupo_sup,
			f.nm_usuario_exec,
			e.dt_fim_real
		from 	man_ordem_servico e,
			man_ordem_servico_exec f
		where	e.nr_sequencia = f.nr_seq_ordem
		and	f.nm_usuario_exec = (	select	max(k.nm_usuario_exec) nm_usuario_exec
							from	man_ordem_servico_exec k
							where	k.nr_seq_ordem = e.nr_sequencia
							and exists (	select	1
									from	usuario_grupo_sup l
									where	k.nm_usuario_exec = l.nm_usuario_grupo	)	)
		and	e.ie_grau_satisfacao in ('R', 'P')
		and	e.ie_classificacao in ('D')
		and	e.nr_seq_grupo_sup is not null
		and	e.nr_seq_gerencia_insatisf is null	) z
where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	z.nr_seq_grupo_sup = c.nr_sequencia
and	z.nm_usuario_exec = b.nm_usuario
group by a.nm_pessoa_fisica, a.cd_pessoa_fisica, c.nr_sequencia, c.nr_seq_gerencia_sup, z.dt_fim_real
union all*/
select	a.nm_pessoa_fisica nm_pessoa_fisica,
	a.cd_pessoa_fisica,
	count(z.nr_sequencia) qt_satisf,
	2 cd_opcao,
	-1 nr_seq_grupo_des,
	c.nr_sequencia nr_seq_grupo_sup,
	c.nr_seq_gerencia_sup nr_seq_gerencia,
	z.dt_fim_real
from	pessoa_fisica a,
	usuario b,
	grupo_suporte c,
		(	select	distinct e.nr_sequencia,
				e.nr_seq_grupo_sup,
				f.nm_usuario_exec,
				e.dt_fim_real
			from 	man_ordem_servico e,
				man_ordem_servico_exec f
			where	e.nr_sequencia = f.nr_seq_ordem
			and	f.nm_usuario_exec = (	select	max(k.nm_usuario_exec) nm_usuario_exec
							from	man_ordem_servico_exec k
							where	k.nr_seq_ordem = e.nr_sequencia
							and exists (	select	1
									from	usuario_grupo_sup l
									where	k.nm_usuario_exec = l.nm_usuario_grupo	)	)
			and	exists (	select 	1
						from 	grupo_suporte 
						where 	nr_seq_gerencia_sup = e.nr_seq_gerencia_insatisf	)
			and	e.ie_grau_satisfacao in ('R', 'P')
			and	e.nr_seq_grupo_sup is not null
			and	e.nr_seq_gerencia_insatisf is not null	) z
where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
and	z.nr_seq_grupo_sup = c.nr_sequencia
and	z.nm_usuario_exec = b.nm_usuario
group by a.nm_pessoa_fisica, a.cd_pessoa_fisica, c.nr_sequencia, c.nr_seq_gerencia_sup, z.dt_fim_real
order by nm_pessoa_fisica;
/
