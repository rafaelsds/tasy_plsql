create or replace 
view eis_banco_sangue_v as
select	1 ie_tipo, /* Empr�stimo entrada */
	trunc(a.dt_producao) dt_referencia,
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh ds_tipo_sangue,
	count(*) qt_total_1,
	0 qt_total_2,
	0 qt_total_3,
	0 qt_total_4,
	0 qt_total_5,
	0 qt_total_6,
	0 qt_total_7,
	'' ds_motivo_inutilizacao,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_producao a,
	san_derivado b
where	a.nr_seq_derivado	= b.nr_sequencia
and	nr_seq_emp_ent	is not null 
group by trunc(a.dt_producao),
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento	
union
select	2 ie_tipo, /* Empr�stimo sa�da */
	trunc(c.dt_emprestimo) dt_referencia,
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	c.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh ds_tipo_sangue,
	0,
	count(*) qt_total_2,
	0,0,0,0,0,
	'',
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_producao a,
	san_derivado b,
	san_emprestimo c
where	a.nr_seq_derivado 	= b.nr_sequencia
and	a.nr_seq_emp_saida	= c.nr_sequencia	
and	nr_seq_emp_saida	is not null 
group by trunc(c.dt_emprestimo),
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	c.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento		
union
select	3 ie_tipo, /* Reserva */
	trunc(b.dt_atualizacao) dt_referencia,
	d.ds_derivado,
	c.ie_tipo_sangue,
	c.ie_fator_rh,
	b.nm_usuario,
	c.ie_tipo_sangue||c.ie_fator_rh ds_tipo_sangue,
	0,0,
	count(*) qt_total_3,
	0,0,0,0,'',
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_reserva a,
	san_reserva_prod b,
	san_producao c,
	san_derivado d
where	b.nr_seq_reserva	= a.nr_sequencia
and	b.nr_seq_producao	= c.nr_sequencia
and	c.nr_seq_derivado	= d.nr_sequencia
and	b.ie_status	= 'R'
group by trunc(b.dt_atualizacao),
	d.ds_derivado,
	c.ie_tipo_sangue,
	c.ie_fator_rh,
	b.nm_usuario,
	c.ie_tipo_sangue||c.ie_fator_rh	,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento	
union
select	4 ie_tipo, /* Transfus�o */
	trunc(c.dt_transfusao) dt_referencia,
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	c.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh ds_tipo_sangue,
	0,0,0,
	count(*) qt_total_4,
	0,0,0,'',
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_producao a,
	san_derivado b,
	san_transfusao c
where	a.nr_seq_derivado 	= b.nr_sequencia
and	a.nr_seq_transfusao	= c.nr_sequencia
and	c.ie_status <> 'C'
group by	trunc(c.dt_transfusao),
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	c.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento	
union
select	5 ie_tipo, /* Inutiliza��o */
	trunc(c.dt_inutilizacao) dt_referencia,
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh ds_tipo_sangue,
	0,0,0,0,
	count(*) qt_total_5,
	0 qt_total_6,
	0 qt_total_7,
	d.ds_motivo_inutilizacao,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_producao a,
	san_derivado b,
	san_inutilizacao c,
	san_motivo_inutil d 
where	a.nr_seq_derivado	= b.nr_sequencia
and	a.nr_seq_inutil	= c.nr_sequencia
and	c.nr_seq_motivo_inutil = d.nr_sequencia
and	a.nr_seq_inutil is not null
group by trunc(c.dt_inutilizacao),
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh,
	d.ds_motivo_inutilizacao,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento	
union
select	6 ie_tipo, /* Produ��o */
	trunc(a.dt_producao) dt_referencia,
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh ds_tipo_sangue,
	0 qt_total_1,
	0 qt_total_2,
	0 qt_total_3,
	0 qt_total_4,
	0 qt_total_5,
	count(*) qt_total_6,
	0 qt_total_7,
	'',
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_producao a,
	san_derivado b
where	a.nr_seq_derivado	= b.nr_sequencia
and	a.nr_seq_emp_ent is not null
and	a.nr_seq_emp_saida is not null
group by trunc(a.dt_producao),
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento	
union
select	7 ie_tipo, /* Empr�stimo entrada */
	trunc(dt_emprestimo) dt_referencia,
	b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh ds_tipo_sangue,
	0 qt_total_1,
	0 qt_total_2,
	0 qt_total_3,
	0 qt_total_4,
	0 qt_total_5,
	0 qt_total_6,
	count(*) qt_total_7,
	'',
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	a.cd_estabelecimento
from	san_producao a,
	san_derivado b,
	san_emprestimo c
where	a.nr_seq_derivado	= b.nr_sequencia
and 	a.nr_seq_emp_ent 	= c.nr_sequencia
and	nr_seq_emp_ent	is not null 
group by b.ds_derivado,
	a.ie_tipo_sangue,
	a.ie_fator_rh,
	a.nm_usuario,
	a.ie_tipo_sangue||a.ie_fator_rh,
	trunc(dt_emprestimo),
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80),
	a.cd_estabelecimento	;
/
