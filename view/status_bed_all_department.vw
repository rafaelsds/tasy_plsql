CREATE OR REPLACE VIEW status_bed_all_department AS 
select
   a.* 
from
   (
      WITH dominio_sexo AS 
      (
         SELECT
            'F' sexo_pf,
            'Feminino' descricao 
         FROM
            dual 
         UNION
         SELECT
            'M',
            'Masculino' 
         FROM
            dual 
         UNION
         SELECT
            'L',
            'Livre' 
         FROM
            dual 
         UNION
         SELECT
            'A',
            'Livre Ambos' 
         FROM
            dual 
         UNION
         SELECT
            'f',
            'Livre Feminino' 
         FROM
            dual 
         UNION
         SELECT
            'm',
            'Livre Masculino' 
         FROM
            dual 
      )
,
      atendimentos_previsao_alta AS 
      (
         SELECT
            ap.cd_setor_desejado cd_setor_atendimento,
            ap.NR_ATENDIMENTO,
            ap.DT_PREVISTO_ALTA 
         FROM
            atendimento_paciente ap 
         WHERE
            ap.dt_previsto_alta BETWEEN trunc(sysdate) AND trunc(sysdate) + 2 
      )
,
      internacao_programada AS 
      (
         SELECT
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') dt_prevista_internacao,
            count(1) nu_pacientes 
         FROM
            gestao_vaga g,
            atendimento_paciente ap,
            setor_atendimento d 
         WHERE
            ap.NR_ATENDIMENTO = g.NR_ATENDIMENTO 
            AND d.CD_SETOR_ATENDIMENTO = ap.CD_SETOR_DESEJADO 
            AND g.ie_status = 'A' 				/* aguardando*/
            AND g.dt_solicitacao BETWEEN trunc(sysdate) AND trunc(sysdate) + 2 
         GROUP BY
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') 
      )
,
      internacao_prog_transf_in AS 
      (
         SELECT
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') dt_prev_trans_interna,
            count(1) nu_pacientes 
         FROM
            gestao_vaga g,
            atendimento_paciente ap,
            setor_atendimento d 
         WHERE
            ap.NR_ATENDIMENTO = g.NR_ATENDIMENTO 
            AND d.CD_SETOR_ATENDIMENTO = ap.CD_SETOR_DESEJADO 
            AND ie_solicitacao = 'TI' 				/* transferencia interna*/
            AND g.dt_solicitacao BETWEEN trunc(sysdate) AND trunc(sysdate) + 2 
         GROUP BY
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') 
      )
,
      internacao_prog_transf_out AS 
      (
         SELECT
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') dt_prev_trans_externa,
            count(1) nu_pacientes 
         FROM
            gestao_vaga g,
            atendimento_paciente ap,
            setor_atendimento d 
         WHERE
            ap.NR_ATENDIMENTO = g.NR_ATENDIMENTO 
            AND d.CD_SETOR_ATENDIMENTO = ap.CD_SETOR_DESEJADO 
            AND ie_solicitacao = 'TE' 				/* transferencia externa */
            AND g.dt_solicitacao BETWEEN trunc(sysdate) AND trunc(sysdate) + 2 
         GROUP BY
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') 
      )
,
      internacao_hospitalizada AS 
      (
         SELECT
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') dt_hospitalization,
            count(1) nu_pacientes 
         FROM
            gestao_vaga g,
            atendimento_paciente ap,
            setor_atendimento d 
         WHERE
            ap.NR_ATENDIMENTO = g.NR_ATENDIMENTO 
            AND d.CD_SETOR_ATENDIMENTO = ap.CD_SETOR_DESEJADO 
            AND ie_solicitacao = 'F' 				/* transferencia externa */
            AND g.dt_solicitacao BETWEEN trunc(sysdate) AND trunc(sysdate) + 2 
         GROUP BY
            d.cd_setor_atendimento,
            trunc(g.dt_prevista, 'DD') 
      )
,
      dados_dia AS 
      (
         SELECT
            d.cd_setor_atendimento,
            d.ds_setor_atendimento,
            nvl(round(sum(decode(ua.NR_ATENDIMENTO, NULL, 0, 1)) / nvl(sum(1), 0), 4), 0)*100 || '%' RT_TAXA_OCUPACAO,
            lpad(nvl(sum(1), 0), 3, '0') nu_leitos,
            lpad(nvl(sum(decode(ua.nr_atendimento, NULL, 0, 1)), 0), 3, '0') NU_HOSPITALIZATION,
            lpad(sum(decode(sexo_pf, 'M', 1, 0)), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(sum(decode(sexo_pf, 'F', 1, 0)), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(sum(decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1))), 5, '0') DS_EMPYT_BED,
            lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'M', 1, 0), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0) ) , 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'F', 1, 0), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0) ) , 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) ), 5, '0') DS_EMPYT_BED_TODAY,
            lpad(nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_PACIENTES_AGENDA_ALTA,
            lpad(nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_TRNSF_OUT,
            lpad(nvl(sum(decode(trunc(hosp.dt_hospitalization, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(hosp.dt_hospitalization, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_HOSPIT_TODAY,
            lpad(nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate), decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate), decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_TRNSF_TODAY 
         FROM
            unidade_atendimento ua,
            dominio_sexo ds,
            setor_atendimento d,
            (
               select
                  * 
               from
                  atendimentos_previsao_alta ap 
               WHERE
                  trunc(ap.dt_previsto_alta) = trunc(sysdate)
            )
            ap,
            (
               select
                  * 
               from
                  internacao_programada ip 
               WHERE
                  trunc(ip.dt_prevista_internacao) = trunc(sysdate)
            )
            ip,
            (
               select
                  * 
               from
                  internacao_prog_transf_in ptin 
               WHERE
                  trunc(ptin.dt_prev_trans_interna) = trunc(sysdate)
            )
            ptin,
            (
               select
                  * 
               from
                  internacao_prog_transf_out ptout 
               WHERE
                  trunc(ptout.dt_prev_trans_externa) = trunc(sysdate)
            )
            ptout,
            (
               select
                  * 
               from
                  internacao_hospitalizada hosp 
               WHERE
                  trunc(hosp.dt_hospitalization) = trunc(sysdate)
            )
            hosp 
         WHERE
            ds.sexo_pf = ua.IE_SEXO_PACIENTE 
            AND d.cd_setor_atendimento = ua.CD_SETOR_ATENDIMENTO 
            AND ap.NR_ATENDIMENTO ( + ) = ua.NR_ATENDIMENTO 
            AND ap.CD_SETOR_ATENDIMENTO ( + ) = ua.CD_SETOR_ATENDIMENTO 
            AND ua.CD_SETOR_ATENDIMENTO = ip.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = ptin.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = ptout.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = hosp.cd_setor_atendimento ( + ) 
         GROUP BY
            d.cd_setor_atendimento,
            d.ds_setor_atendimento 
      )
,
      dados_amanha AS 
      (
         SELECT
            d.cd_setor_atendimento,
            d.ds_setor_atendimento,
            nvl(round(sum(decode(ua.NR_ATENDIMENTO, NULL, 0, 1)) / nvl(sum(1), 0), 4), 0)*100 || '%' RT_TAXA_OCUPACAO,
            lpad(nvl(sum(1), 0), 3, '0') nu_leitos,
            lpad(nvl(sum(decode(ua.nr_atendimento, NULL, 0, 1)), 0), 3, '0') NU_HOSPITALIZATION,
            lpad(sum(decode(sexo_pf, 'M', 1, 0)), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(sum(decode(sexo_pf, 'F', 1, 0)), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(sum(decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1))), 5, '0') DS_EMPYT_BED,
            lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'M', 1, 0), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0) ) , 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'F', 1, 0), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0) ) , 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) ), 5, '0') DS_EMPYT_BED_TODAY,
            lpad(nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_PACIENTES_AGENDA_ALTA,
            lpad(nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_TRNSF_OUT,
            lpad(nvl(sum(decode(trunc(hosp.dt_hospitalization, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(hosp.dt_hospitalization, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_HOSPIT_TODAY,
            lpad(nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 1, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_TRNSF_TODAY 
         FROM
            unidade_atendimento ua,
            dominio_sexo ds,
            setor_atendimento d,
            (
               select
                  * 
               from
                  atendimentos_previsao_alta ap 
               WHERE
                  trunc(ap.dt_previsto_alta) = trunc(sysdate) + 1
            )
            ap,
            (
               select
                  * 
               from
                  internacao_programada ip 
               WHERE
                  trunc(ip.dt_prevista_internacao) = trunc(sysdate) + 1
            )
            ip,
            (
               select
                  * 
               from
                  internacao_prog_transf_in ptin 
               WHERE
                  trunc(ptin.dt_prev_trans_interna) = trunc(sysdate) + 1
            )
            ptin,
            (
               select
                  * 
               from
                  internacao_prog_transf_out ptout 
               WHERE
                  trunc(ptout.dt_prev_trans_externa) = trunc(sysdate) + 1
            )
            ptout,
            (
               select
                  * 
               from
                  internacao_hospitalizada hosp 
               WHERE
                  trunc(hosp.dt_hospitalization) = trunc(sysdate) + 1
            )
            hosp 
         WHERE
            ds.sexo_pf = ua.IE_SEXO_PACIENTE 
            AND d.cd_setor_atendimento = ua.CD_SETOR_ATENDIMENTO 
            AND ap.NR_ATENDIMENTO ( + ) = ua.NR_ATENDIMENTO 
            AND ap.CD_SETOR_ATENDIMENTO ( + ) = ua.CD_SETOR_ATENDIMENTO 
            AND ua.CD_SETOR_ATENDIMENTO = ip.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = ptin.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = ptout.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = hosp.cd_setor_atendimento ( + ) 
         GROUP BY
            d.cd_setor_atendimento,
            d.ds_setor_atendimento 
      )
,
      dados_depois_amanha AS 
      (
         SELECT
            d.cd_setor_atendimento,
            d.ds_setor_atendimento,
            nvl(round(sum(decode(ua.NR_ATENDIMENTO, NULL, 0, 1)) / nvl(sum(1), 0), 4), 0)*100 || '%' RT_TAXA_OCUPACAO,
            lpad(nvl(sum(1), 0), 3, '0') nu_leitos,
            lpad(nvl(sum(decode(ua.nr_atendimento, NULL, 0, 1)), 0), 3, '0') NU_HOSPITALIZATION,
            lpad(sum(decode(sexo_pf, 'M', 1, 0)), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(sum(decode(sexo_pf, 'F', 1, 0)), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(sum(decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1))), 5, '0') DS_EMPYT_BED,
            lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'M', 1, 0), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0) ) , 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'F', 1, 0), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0) ) , 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad( nvl(sum(decode(ua.nr_atendimento, NULL, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) - (nvl(sum(decode(trunc(ip.dt_prevista_internacao, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) + nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 0, decode(sexo_pf, 'M', 0, 1)), 0)), 0) ), 5, '0') DS_EMPYT_BED_TODAY,
            lpad(nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ap.DT_PREVISTO_ALTA, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_PACIENTES_AGENDA_ALTA,
            lpad(nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ptout.dt_prev_trans_externa, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_TRNSF_OUT,
            lpad(nvl(sum(decode(trunc(hosp.dt_hospitalization, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(hosp.dt_hospitalization, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_HOSPIT_TODAY,
            lpad(nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'M', 1, 0), 0)), 0), 5, '0') || CHR(9) || ' | ' || CHR(9) || lpad(nvl(sum(decode(trunc(ptin.dt_prev_trans_interna, 'DD'), trunc(sysdate) + 2, decode(sexo_pf, 'F', 1, 0), 0)), 0), 5, '0') DS_SCHDL_TRNSF_TODAY 
         FROM
            unidade_atendimento ua,
            dominio_sexo ds,
            setor_atendimento d,
            (
               select
                  * 
               from
                  atendimentos_previsao_alta ap 
               WHERE
                  trunc(ap.dt_previsto_alta) = trunc(sysdate) + 2
            )
            ap,
            (
               select
                  * 
               from
                  internacao_programada ip 
               WHERE
                  trunc(ip.dt_prevista_internacao) = trunc(sysdate) + 2
            )
            ip,
            (
               select
                  * 
               from
                  internacao_prog_transf_in ptin 
               WHERE
                  trunc(ptin.dt_prev_trans_interna) = trunc(sysdate) + 2
            )
            ptin,
            (
               select
                  * 
               from
                  internacao_prog_transf_out ptout 
               WHERE
                  trunc(ptout.dt_prev_trans_externa) = trunc(sysdate) + 2
            )
            ptout,
            (
               select
                  * 
               from
                  internacao_hospitalizada hosp 
               WHERE
                  trunc(hosp.dt_hospitalization) = trunc(sysdate) + 2
            )
            hosp 
         WHERE
            ds.sexo_pf = ua.IE_SEXO_PACIENTE 
            AND d.cd_setor_atendimento = ua.CD_SETOR_ATENDIMENTO 
            AND ap.NR_ATENDIMENTO ( + ) = ua.NR_ATENDIMENTO 
            AND ap.CD_SETOR_ATENDIMENTO ( + ) = ua.CD_SETOR_ATENDIMENTO 
            AND ua.CD_SETOR_ATENDIMENTO = ip.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = ptin.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = ptout.cd_setor_atendimento ( + ) 
            AND ua.CD_SETOR_ATENDIMENTO = hosp.cd_setor_atendimento ( + ) 
         GROUP BY
            d.cd_setor_atendimento,
            d.ds_setor_atendimento 
      )
      SELECT
         dh.cd_setor_atendimento || '- ' || dh.ds_setor_atendimento DS_DEPARTAMENTO,
         max(dh.RT_TAXA_OCUPACAO) RT_TAXA_OCUPACAO,
         max(dh.nu_leitos) NU_LEITOS,
         max(dh.NU_HOSPITALIZATION) NU_HOSPITALIZATION,
         max(dh.DS_EMPYT_BED) DS_EMPYT_BED,
         max(dh.DS_EMPYT_BED_TODAY) DS_EMPYT_BED_TODAY,
         max(dh.DS_PACIENTES_AGENDA_ALTA) DS_PACIENTES_AGENDA_ALTA,
         max(dh.DS_SCHDL_TRNSF_OUT) DS_SCHDL_TRNSF_OUT,
         max(dh.DS_SCHDL_HOSPIT_TODAY) DS_SCHDL_HOSPIT_TODAY,
         max(dh.DS_SCHDL_TRNSF_TODAY) DS_SCHDL_TRNSF_TODAY,
         max(da.DS_EMPYT_BED_TODAY) DS_EMPTY_BED_TOMORROW,
         max(da.DS_PACIENTES_AGENDA_ALTA) DS_SCHDL_DISCHARGED_TOMORROW,
         max(da.DS_SCHDL_TRNSF_OUT) DS_SCHDL_TRNSF_OUT_TOMORROW,
         max(da.DS_SCHDL_HOSPIT_TODAY) DS_SCHDL_ADMSSION_TOMORROW,
         max(da.DS_SCHDL_TRNSF_TODAY) DS_SCHDL_TRNSF_IN_TOMORROW,
         max(dda.DS_EMPYT_BED_TODAY) DS_EMPTY_BEDS_AFTER_TOMORROW,
         max(dda.DS_PACIENTES_AGENDA_ALTA) DS_SCHDL_DISCHRGD_DA_TOMORROW,
         max(dda.DS_SCHDL_TRNSF_OUT) DS_SCHDL_TRNSF_OT_D1_TOMORROW,
         max(dda.DS_SCHDL_HOSPIT_TODAY) DS_SCHDL_HOSPIT_DAY_A_TOMORROW,
         max(dda.DS_SCHDL_TRNSF_TODAY) DS_SCHDL_TRNSF_IN_D1_TOMORROW 
      FROM
         dados_depois_amanha dda,
         dados_dia dh,
         dados_amanha da 
      WHERE
         da.cd_setor_atendimento = dh.cd_setor_atendimento 
         AND dda.cd_setor_atendimento = da.cd_setor_atendimento 
         AND dda.cd_setor_atendimento = dh.cd_setor_atendimento 
      GROUP BY
         dh.cd_setor_atendimento,
         dh.ds_setor_atendimento 
   )
   a;
/
