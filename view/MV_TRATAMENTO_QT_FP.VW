CREATE OR REPLACE VIEW mv_tratamento_qt_fp AS
SELECT
    nr_seq_loco_regional AS diagnostico_clinico_id,
    obter_desc_protocolo(paciente_setor.cd_protocolo) AS protocolo,
    obter_desc_protocolo_medic(paciente_setor.nr_seq_medicacao, paciente_setor.cd_protocolo) AS esquema_protocolo,
    obter_valor_dominio(1163, ie_finalidade) AS finalidade,
    nr_ciclos AS ciclos,
    (
        SELECT COUNT(*)
        FROM (
            SELECT paciente_atendimento.nr_ciclo
            FROM paciente_atendimento
            WHERE paciente_atendimento.nr_seq_paciente = paciente_setor.nr_seq_paciente
            HAVING COUNT(paciente_atendimento.ds_dia_ciclo_real) = (
                SELECT COUNT(DISTINCT TRIM(REGEXP_SUBSTR(paciente_protocolo_medic.ds_dias_aplicacao, '[^,]+', 1, sessao.column_value))) AS nr_sessoes
                FROM 
                    paciente_protocolo_medic,
                    TABLE(CAST(MULTISET(SELECT LEVEL FROM dual CONNECT BY LEVEL <= LENGTH (REGEXP_REPLACE(paciente_protocolo_medic.ds_dias_aplicacao, '[^,]+'))  + 1) AS sys.odcinumberlist)) sessao
                WHERE paciente_protocolo_medic.nr_seq_paciente = paciente_setor.nr_seq_paciente
                )
            GROUP BY paciente_atendimento.nr_ciclo
            )
        ) AS ciclos_finalizados,
    (
        SELECT MIN(paciente_atendimento.dt_prevista)
        FROM paciente_atendimento
        WHERE paciente_atendimento.nr_seq_paciente = paciente_setor.nr_seq_paciente
        ) AS dt_inicio,
    (
        SELECT MAX(paciente_atendimento.dt_prevista)
        FROM paciente_atendimento
        WHERE paciente_atendimento.nr_seq_paciente = paciente_setor.nr_seq_paciente
        ) AS dt_fim,
    (
        SELECT MAX(paciente_atendimento.dt_prevista)
        FROM paciente_atendimento
        WHERE paciente_atendimento.nr_seq_paciente = paciente_setor.nr_seq_paciente
        ) AS dt_ult_aplicacao,
    paciente_setor.nr_seq_paciente AS programacao_tratamento_id,
    NULL AS ordem_todos_protocolos,
    NULL AS ordem_protocolos_iguais,
    NULL AS dt_corrente,
    NULL AS vigente
FROM
    paciente_setor,
    pep_pac_ci
WHERE pep_pac_ci.cd_pessoa_fisica = paciente_setor.cd_pessoa_fisica
AND pep_pac_ci.ie_situacao = 'A'
AND pep_pac_ci.ie_tipo_consentimento = 'R';
/
