create or replace view detecciones_nom_v as
select 
	0 ie_tipo_registro,
	'clues|curpPrestador|nombrePrestador|primerApellidoPrestador|segundoApellidoPrestador|tipoPersonal|especificaTipoPersonal|cedulaProfesional|servicioAtencion|especificarServicio|curpPaciente|'||
	'nombre|primerApellido|segundoApellido|fechaNacimiento|entidadNacimiento|edad|claveEdad|sexo|seConsideraIndigena|spss|numeroAfiliacionSpss|prospera|folioProspera|imss|numeroAfiliacionImss|'||
	'issste|numeroAfiliacionIssste|otraAfiliacion|numeroOtraAfiliacion|peso|talla|tipoDificultad|gradoDificultad|origenDificultad|migrante|fechaDeteccion|diabetesMellitus|'||
	'hipertensionArterial|obesidad|dislipidemias|depresion|alteracionMemoria|sintomaticoRespiratorio|alcoholismo|tabaquismo|farmacos|incontinenciaUrinaria|caida60yMas|riesgoFractura|vih|gonorrea|'||
	'itsSecretoras|itsUlcerosas|itsTumorales|sifilis|sospechaSindromeTurner|violenciaFamiliar|hiperplasiaProstatica|tirasEmbarazadasSanas|'||
	'tirasDeteccion|tirasControl|reactivosAntigenoProstatico|cartillaVacunacion|telemedicina' CABECARIO,
	null clues,
	null curpprestador,
	null nombreprestador,
	null primerapellidoprestador,
	null segundoapellidoprestador,
	null tipopersonal,
	null especificatipopersonal,
	null cedulaprofesional,
	null servicioatencion,
	null especificarservicio,
	null curppaciente,
	null nombre,
	null primerapellido,
	null segundoapellido,
	null fechanacimiento,
	null entidadnacimiento,
	null edad,
	null claveedad,
	null sexo,
	null seconsideraindigena,
	null spss,
	null numeroafiliacionspss,
	null prospera,
	null folioprospera,
	null imss,
	null numeroafiliacionimss,
	null issste,
	null numeroafiliacionissste,
	null otraafiliacion,
	null numerootraafiliacion,
	null peso,
	null talla,
	null tipodificultad,
	null gradodificultad,
	null origendificultad,
	null migrante,
	null fechadeteccion,
	null diabetesmellitus,
	null hipertensionarterial,
	null obesidad,
	null dislipidemias,
	null depresion,
	null alteracionmemoria,
	null sintomaticorespiratorio,
	null alcoholismo,
	null tabaquismo,
	null farmacos,
	null incontinenciaurinaria,
	null caida60ymas,
	null riesgofractura,
	null vih,
	null gonorrea,
	null itssecretoras,
	null itsulcerativas,
	null itstumorales,
	null sifilis,
	null sospechasindrometurner,
	null violenciafamiliar,
	null hiperplasiaprostatica,
	null tirasembarazadassanas,
	null tirasdeteccion,
	null tirascontrol,
	null reactivosantigenoprostatico,
	null cartillavacunacion,
	null telemedicina,
	null dt_entrada,
	null nr_atendimento
from dual
union all
select
	--prestador servico--	
	2 ie_tipo_registro,
	'' cabecario,	   
	pj.cd_internacional clues,
	nvl(substr(upper(pj.cd_curp),1,18), 'XXXX999999XXXXXX99') curpprestador,
	nvl(substr(upper(pj.ds_razao_social),1,50), 'XX') nombreprestador,
	nvl(substr(upper(pj.nm_fantasia),1,50), 'XX') primerapellidoprestador,
	nvl(substr(upper(pj.ds_nome_abrev),1,50), 'XX') segundoapellidoprestador,
	profissional.ie_profissional  tipopersonal,
	decode(profissional.ie_profissional,'12',obter_desc_expressao(308221)) especificatipopersonal, 
	case 
		when profissional.ie_profissional in ('2','3','4','6','8')
		then nvl(lpad(nvl(pf_m.ds_codigo_prof, m.nr_crm),8,0),0) 
		else '0' 
	end as cedulaprofesional, 
	decode(b.ie_clinica,2,3,1,4,3,5,1000,6,1001,7,1002,8,1004,9,1005,13,1006,14,4,16,1007,17,1008,22,1009,23) servicioatencion,
	null especificarservicio,
	--dados paciente--	
	nvl(substr(upper(pf_p.cd_curp),1,18), 'XXXX999999XXXXXX99') curppaciente,
	substr(upper((y.ds_given_name)),1,50) nombre,	 
	nvl(substr(upper((y.ds_family_name)),1,50), 'XX') primerapellido,
	nvl(substr(upper((y.ds_component_name_1)),1,50), 'XX') segundoapellido,
	to_char(pf_p.dt_nascimento, 'dd/mm/yyyy') fechanacimiento,
	obter_dados_cat_entidade(pf_p.cd_pessoa_fisica, 'CD_ENTIDADE') entidadnacimiento,
	case
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') is not null 
		then nvl(obter_idade_imc_nom(b.nr_atendimento,pf_p.dt_nascimento,b.dt_inicio_atendimento,'IDADE'),-1)
		else 999
	end as edad,
	case
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') is not null 
		then obter_idade_imc_nom(b.nr_atendimento,pf_p.dt_nascimento,b.dt_inicio_atendimento,'CLAVEEDAD')
		else 9
	end as claveedad,
	decode(pf_p.ie_sexo, 'M', 1 , 'F', 2, 9) sexo,
	decode(pf_p.nr_seq_cor_pele, 201, 1, 0) seconsideraindigena,
	-- --derechohaniencia--
	nvl2(pf_p.nr_spss, 1, 0) spss,
	case 
		when pf_p.nr_spss is not null 
		then substr(pf_p.nr_spss,1,13)
	end as numeroafiliacionspss,
	decode(n.cd_tipo_convenio_mx, 13, 1,0) prospera,
	decode(n.cd_tipo_convenio_mx, 13, elimina_caractere_especial(substr(p.nr_doc_convenio,1,16))) folioprospera,
	decode(n.cd_tipo_convenio_mx, 2, 1,0) imss,
	decode(n.cd_tipo_convenio_mx, 2, substr(p.nr_doc_convenio,1,11)) numeroafiliacionimss,
	decode(n.cd_tipo_convenio_mx, 3, 1,0) issste,
	decode(n.cd_tipo_convenio_mx, 3, substr(p.nr_doc_convenio,1,13)) numeroafiliacionissste,
	decode(n.cd_tipo_convenio_mx, 15, 1,0) otraafiliacion,
	decode(n.cd_tipo_convenio_mx, 15, substr(p.nr_doc_convenio,1,15)) numerootraafiliacion,
	---- outros dados--
	nvl(nvl((select	max(qt_peso)		
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_peso is not null
					and	nr_atendimento	= b.nr_atendimento
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(ie_rn,'N')	= 'N')), pf_p.qt_peso), '999') peso,
	nvl(nvl((select	max(qt_altura_cm)		
		from	atendimento_sinal_vital	
		where	nr_sequencia = (select	nvl(max(nr_sequencia),-1)
					from	atendimento_sinal_vital
					where	qt_altura_cm is not null
					and	nr_atendimento	= b.nr_atendimento
					and	nvl(ie_situacao,'A') = 'A'
					and	nvl(ie_rn,'N')	= 'N')), pf_p.qt_altura_cm), '999') talla,								
	(select max(decode(ptd.nr_seq_tipo_def, 48,1,49,2,50,3,51,4,52,5,53,6,54,7,55,8,56,9)) 
	 from pf_tipo_deficiencia ptd 
	where pf_p.cd_pessoa_fisica = ptd.cd_pessoa_fisica and ptd.dt_inativacao is null and ptd.dt_liberacao is not null) as tipodificultad,
	 -- gradodificultad
	(select max(decode(ptd.nr_seq_tipo_def,56,9,ptd.ie_grau_deficiencia))
	from pf_tipo_deficiencia ptd 
	where pf_p.cd_pessoa_fisica = ptd.cd_pessoa_fisica
	and ptd.dt_inativacao is null and ptd.dt_liberacao is not null) as gradodificultad,
	-- origendificultad 
	(select max(case 
		when ptd.nr_seq_tipo_def = 56 
		then 9
		else decode(ptd.nr_seq_causa_lesao,6,1,8,2,9,3,10,4,11,5,12,6,13,9) 
		end) 
	from pf_tipo_deficiencia ptd 
	where pf_p.cd_pessoa_fisica = ptd.cd_pessoa_fisica
	and ptd.dt_inativacao is null and ptd.dt_liberacao is not null) as origendificultad,
	-1 migrante,
	nvl(to_char((select max(ddp.dt_diagnostico) from diagnostico_doenca ddp where ddp.nr_atendimento = b.nr_atendimento and ddp.ie_classificacao_doenca = 'P'), 'dd/mm/yyyy'), null) fechadeteccion,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 20 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'DIABT' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as diabetesmellitus,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 20 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'HIPAS' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as hipertensionarterial,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 20 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'OBSDA' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as obesidad,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 20 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'LIPDM' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as dislipidemias,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 60 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'DPRSS' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as depresion,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 60 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'ALTME' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as alteracionmemoria,
	nvl((select decode(ddp.ie_sintomatico, 'S', 0, 1) from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'STRSP' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento), '-1') sintomaticorespiratorio,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'ALCOL' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as alcoholismo,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'TABAC' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as tabaquismo,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'FARMA' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as farmacos,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'INCUR' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as incontinenciaurinaria,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 60 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'SINDC' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as caida60ymas,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 50 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'RISFO' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as riesgofractura,
	case 
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 50 and (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'HIV' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as vih,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'GRREA' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as gonorrea,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where (cc1.cd_doenca in ('A54', 'N74') or cc1.cd_doenca like '%A59%' or cc1.cd_doenca like '%A70%') and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento)
				then 0
				else 1
			end 
		else -1
	end as itssecretoras,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.cd_doenca in ('B081','B977')  and cc1.cd_doenca_cid like ddp.cd_doenca and b.nr_atendimento = ddp.nr_atendimento)
				then 0
				else 1
			end 
		else -1
	end as itsulcerativas,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.cd_doenca in ('B97.7', 'A58', 'A60') and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento)
				then 0
				else 1
			end 
		else -1
	end as itstumorales,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'SIFIL' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as sifilis,
	case 
		when (select max(ddp.cd_doenca) from diagnostico_doenca ddp where b.nr_atendimento = ddp.nr_atendimento) is not null
		then 
			case 
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'SINDT' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end 
		else -1
	end as sospechasindrometurner,
	case
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 15 and pf_p.ie_sexo = 'F'
		then 
			case
				when exists (select 1 from sinan_inf_violencia siv, notificacao_sinan nf where siv.nr_seq_notificacao = nf.nr_sequencia and nf.nr_atendimento = b.nr_atendimento and ie_tipo_violencia = 2)
				then 0
				else 1
			end
		else -1
	end as violenciafamiliar,
	case
		when obter_idade(pf_p.dt_nascimento, b.dt_inicio_atendimento, 'A') >= 15 and pf_p.ie_sexo = 'M'
		then 
			case
				when exists (select 1 from cid_doenca cc1, diagnostico_doenca ddp where cc1.ie_doenca_cronica_mx = 'HIPPR' and cc1.cd_doenca_cid like ddp.cd_doenca and ddp.nr_atendimento = b.nr_atendimento) 
				then 0
				else 1
			end
		else -1
	end as hiperplasiaprostatica,
	0 tirasembarazadassanas,
	0 tirasdeteccion,
	0 tirascontrol,
	0 reactivosantigenoprostatico,
	0 cartillavacunacion,
	0 telemedicina,
	b.dt_entrada dt_entrada,
	b.nr_atendimento nr_atendimento
from   	pessoa_juridica pj,	
	estabelecimento e,
	pessoa_fisica pf_m,
	pessoa_fisica pf_p,
	medico m,
	sus_municipio sm,
	cat_entidade ce,	   
	atend_categoria_convenio p,
	categoria_convenio cc,
	convenio n,
	cat_derechohabiencia k,
	atendimento_paciente b,	   
	atend_profissional profissional,
	person_name y	
where 	b.nr_atendimento  	= p.nr_atendimento
and	pj.cd_cgc 		= e.cd_cgc 
and     b.cd_estabelecimento 	= e.cd_estabelecimento 
and 	b.cd_pessoa_fisica	= pf_p.cd_pessoa_fisica
and 	b.cd_medico_resp 	= m.cd_pessoa_fisica
and	pf_m.cd_pessoa_fisica 	= m.cd_pessoa_fisica 
and     pf_p.cd_municipio_ibge	= sm.cd_municipio_ibge (+)  
and     sm.nr_seq_entidade_mx	= ce.nr_sequencia (+) 
and	p.cd_convenio 		= cc.cd_convenio 
and	p.cd_categoria 		= cc.cd_categoria 
and	n.cd_convenio 		= cc.cd_convenio 
and	n.cd_tipo_convenio_mx  	= k.nr_sequencia (+)
and 	b.nr_atendimento 	= profissional.nr_atendimento 
and 	y.nr_sequencia 		= pf_p.nr_seq_person_name
and	y.ds_type		= 'main';
/