create or replace
view TISS_OPM_EXECUTADO_V as
select	'2.01.01' ds_versao,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,60) ds_procedimento,
	sum(a.qt_material) qt_material,
	substr(TISS_OBTER_ORIGEM_PRECO_MAT(a.ie_origem_preco, a.cd_material, b.cd_estabelecimento, b.cd_convenio_parametro, b.cd_categoria_parametro, a.cd_tab_preco_material, a.dt_conta,a.cd_setor_atendimento,'X',null,a.nr_sequencia, a.cd_material_tuss),1,20) cd_edicao_amb,
	lpad(decode(a.CD_material_CONVENIO,'0', TO_CHAR(a.CD_material), null, TO_CHAR(a.CD_material), a.CD_material_convenio),10,'0') cd_procedimento_convenio,
	substr(TISS_OBTER_ORIGEM_PRECO_MAT(a.ie_origem_preco, a.cd_material, b.cd_estabelecimento, b.cd_convenio_parametro, b.cd_categoria_parametro, a.cd_tab_preco_material, a.dt_conta,a.cd_setor_atendimento,'R',null,a.nr_sequencia, a.cd_material_tuss),1,20) cd_edicao_relat,
	a.vl_unitario,
	sum(a.vl_material) vl_material,
	nvl(a.nr_doc_convenio, obter_desc_expressao(778770)) cd_autorizacao,
	a.nr_interno_conta,
	a.ie_tiss_tipo_guia_desp,
	nvl(a.cd_cgc_prestador, g.cd_cgc) cd_cgc_prestador
from	estabelecimento g,
	classe_material f,
	material e,
	material_atend_paciente a,
	conta_paciente b
where	b.nr_interno_conta				= a.nr_interno_conta
and	a.cd_motivo_exc_conta				is null
and	a.cd_material					= e.cd_material
and	e.cd_classe_material				= f.cd_classe_material
and	f.ie_tipo_classe				in ('O','P','M')
and	b.cd_estabelecimento				= g.cd_estabelecimento
group 	by a.cd_material,
	substr(obter_desc_material(a.cd_material),1,60),
	substr(TISS_OBTER_ORIGEM_PRECO_MAT(a.ie_origem_preco, a.cd_material, b.cd_estabelecimento, b.cd_convenio_parametro, b.cd_categoria_parametro, a.cd_tab_preco_material, a.dt_conta,a.cd_setor_atendimento,'X',null,a.nr_sequencia, a.cd_material_tuss),1,20),
	lpad(decode(a.CD_material_CONVENIO,'0', TO_CHAR(a.CD_material), null, TO_CHAR(a.CD_material), a.CD_material_convenio),10,'0'),
	substr(TISS_OBTER_ORIGEM_PRECO_MAT(a.ie_origem_preco, a.cd_material, b.cd_estabelecimento, b.cd_convenio_parametro, b.cd_categoria_parametro, a.cd_tab_preco_material, a.dt_conta,a.cd_setor_atendimento,'R',null,a.nr_sequencia, a.cd_material_tuss),1,20),
	a.vl_unitario,
	nvl(a.nr_doc_convenio, obter_desc_expressao(778770)),
	a.nr_interno_conta,
	a.ie_tiss_tipo_guia_desp,
	nvl(a.cd_cgc_prestador, g.cd_cgc);
/
