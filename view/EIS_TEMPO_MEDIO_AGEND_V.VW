create or replace
view Eis_tempo_medio_agend_v as 
select	a.cd_agenda,
	substr(obter_nome_agenda(a.cd_agenda),1,100) ds_agenda,
	b.cd_pessoa_fisica,
	b.nm_paciente,
	a.cd_setor_exclusivo,
	substr(obter_nome_setor(a.cd_setor_exclusivo),1,100) ds_setor_exclusivo,
	b.dt_agendamento,
	b.dt_final_agendamento,
	trunc(dt_agenda) dt_referencia,
	(dt_final_agendamento - dt_agendamento) * 1440 hr_diferenca,
	a.cd_estabelecimento
from	agenda a,
	agenda_paciente b
where	a.cd_agenda = b.cd_agenda
and	b.ie_status_agenda <> 'L'
and	a.cd_tipo_agenda = 2
and	b.dt_final_agendamento is not null;
/