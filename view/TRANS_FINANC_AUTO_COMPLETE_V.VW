create or replace view
TRANS_FINANC_AUTO_COMPLETE_V as
select	nr_sequencia,
		cd_estabelecimento,
		cd_empresa,
		ds_transacao,
		cd_transacao || ' - ' || ds_transacao ds_trans_cod,
		ie_acao,
		cd_transacao,
		ie_situacao
from	transacao_financeira
where	nvl(ie_banco,'N') <> 'N'
and		ie_situacao = 'A'
and		nvl(ie_controle_banc, 'S') = 'S'
union all
select	nr_sequencia,
		cd_estabelecimento,
		cd_empresa,
		ds_transacao,
		cd_transacao || ' - ' || ds_transacao ds_trans_cod,
		ie_acao,
		cd_transacao,
		ie_situacao
from	transacao_financeira a
where	nvl(ie_banco,'N') <> 'N'
and		ie_situacao = 'I'
and		nvl(ie_controle_banc, 'S') = 'S'
and		exists(	select	1
				from	banco_saldo x,
						movto_trans_financ y
				where	x.nr_sequencia = y.nr_seq_saldo_banco
				and		y.nr_seq_trans_financ = a.nr_sequencia);
/