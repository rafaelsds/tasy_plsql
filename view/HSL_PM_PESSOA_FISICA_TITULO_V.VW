create or replace
view hsl_pm_pessoa_fisica_titulo_v as
select	a.*,
	c.ds_titulo
from	pf_titulo c,
	medico b,
	pessoa_fisica_titulo a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	a.nr_seq_titulo	= c.nr_sequencia(+)
/