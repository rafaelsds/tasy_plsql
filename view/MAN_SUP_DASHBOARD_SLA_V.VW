/*
Esta View n�o utiliza as regras de contrato de SLA.
Utiliza regra determinada pelo time de suporte, sendo que cadas OS, no momento
que chega, deve ser triada em no maximo 30 minutos.
*/
create or replace view man_sup_dashboard_sla_v
as
	select	t.*,
		man_obter_min_com (1, t.dt_ordem, t.dt_triagem) || ' min' min_aguardando_triagem,
		case	when	man_obter_min_com (1, t.dt_ordem, t.dt_triagem) <= 30
				then 1
			else 3
		end nr_atraso
	from	(
			select	a.nr_sequencia,
				a.nr_seq_gerencia_sup nr_seq_gerencia,
				a.nr_seq_grupo_sup,
				a.dt_ordem_servico dt_ordem,
				a.ds_localizacao,
				(
					select	nvl (min (a2.dt_atualizacao), sysdate)
					from	man_ordem_serv_estagio a2
					where	a2.nr_seq_ordem = a.nr_sequencia
					and	a2.nr_sequencia =
						(
							select	min (a1.nr_sequencia)
							from	man_ordem_serv_estagio a1
							where	a1.nr_seq_estagio <> 231
							and	a1.nr_seq_ordem = a2.nr_seq_ordem
						)
				)
				dt_triagem,
				a.ds_dano_breve,
				substr(obter_desc_expressao(b.cd_exp_grupo, b.ds_grupo), 1, 255) ds_grupo_des,
				man_obter_sla_leg(a.nr_sequencia) ie_sla_leg
			from man_ordem_servico_v a,
				grupo_suporte b
			where	a.nr_seq_grupo_sup = b.nr_sequencia
			and	a.ie_classificacao = 'E'
			and	a.ie_terceiro        = 'S'
			and	a.ie_status_ordem   <> 3
			and	a.nr_seq_grupo_sup  <> 85
				/*-- n�o foi triada ainda*/
				/*--/**/
			and not exists
				(
					select	1
					from	man_ordem_serv_estagio a1
					where	a1.nr_seq_estagio <> 231
					and	a1.nr_seq_ordem = a.nr_sequencia
				)
				/*
				31	Suporte - An�lise
				121	Aguardando Conex�o
				151	Retorno Cliente
				311	Aguardando Aux�lio Externo
				861	Em conex�o
				1341	Aguardando Inf. do Cliente - Suporte
				1371	Aguardando Aux�lio Interno - Grupo
				1431	Aguardando Aux�lio Interno - Analista
				1821	Suporte n�vel 2
				2113	Triada
				231	Aguardando Triagem
				*/
			and a.nr_seq_estagio in (31, 121, 151, 311, 861, 1341, 1371, 1431, 1821, 2113, 231)
		) t;
/
