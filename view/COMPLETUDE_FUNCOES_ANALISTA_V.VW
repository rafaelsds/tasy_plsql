create or replace view completude_funcoes_analista_v as
select distinct
		a.cd_funcao,
		obter_nome_funcao(a.cd_funcao) nm_funcao,
		a.nr_sequencia nr_seq_ord_serv,
		a.ds_dano_breve,
		b.nr_sequencia nr_seq_ger_wheb,
		e.nr_sequencia nr_seq_grup_des,
		d.nr_sequencia nr_seq_mod_tasy,
		e.ds_grupo,
		b.ds_gerencia,
		d.nm_modulo,
		f.qt_min_prev,
		a.ie_status_ordem,
		f.nr_sequencia nr_seq_ativ_prev,
		f.nm_usuario_prev
from 	man_ordem_servico a,
		gerencia_wheb b,
		funcao c,
		modulo_tasy d,
		grupo_desenvolvimento e,
		man_ordem_ativ_prev f
where	a.nr_seq_grupo_des = e.nr_sequencia
and		e.nr_seq_gerencia(+) = b.nr_sequencia
and 	a.cd_funcao = c.cd_funcao
and 	c.nr_seq_modulo = d.nr_sequencia
and 	((a.ds_dano_breve like '(Analyst)%') or 
		 (a.ds_dano_breve like '(Component)%'))
and		f.nr_seq_ordem_serv(+) = a.nr_sequencia
/