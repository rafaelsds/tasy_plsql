create or replace 
view eis_indicador_mentor_v as 
select decode(obter_informacoes_pendencia(c.nr_sequencia),'S',1,0) qt_executadas,
       decode(d.ie_executar,'S',1,0) qt_executada_aut,
       substr(obter_informacoes_pendencia(c.nr_sequencia, 'D'),1,255) dt_execucao,
       c.dt_atualizacao_nrec,
       e.nr_atendimento,     
       obter_convenio_atendimento(e.nr_atendimento) cd_convenio,
       substr(obter_nome_convenio(obter_convenio_atendimento(e.nr_atendimento)),1,150) ds_convenio,
       eis_obter_setor_atend_data(e.nr_atendimento, c.dt_atualizacao_nrec) cd_setor_atendimento,
       obter_nome_setor(eis_obter_setor_atend_data(e.nr_atendimento, c.dt_atualizacao_nrec) ) ds_setor_atendimento,
       obter_sexo_pf(x.cd_pessoa_fisica,'C') ie_sexo, 
       obter_sexo_pf(x.cd_pessoa_fisica,'D') ds_sexo, 
       substr(obter_unidade_atend_data(e.nr_atendimento, c.dt_atualizacao_nrec),1,255) ds_unidade,
       substr(obter_idade(obter_data_nascto_pf(x.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria,
       e.cd_pessoa_fisica cd_paciente,
       a.ie_tipo_pendencia,
       substr(obter_desc_pendencia_gqa(a.ie_tipo_pendencia),1,255) tipo_pendencia,        
       a.ds_pendencia,
       a.nr_sequencia nr_pendencia,
	  b.ds_regra,
       b.nr_sequencia nr_regra,
       substr(d.DS_ACAO,1,255) ds_acao,
       d.nr_sequencia nr_acao,
       substr(obter_informacoes_pendencia(c.nr_sequencia, 'PE'),1,255) nm_executor_previsto,
       x.cd_pessoa_fisica cd_executor_previsto,
       substr(obter_informacoes_pendencia(c.nr_sequencia, 'P'),1,255) nm_usuario_execucao,
       substr(obter_informacoes_pendencia(c.nr_sequencia, 'PF'),1,255) usuario_execucao,
       to_number(obter_dif_data(c.dt_atualizacao_nrec,nvl(to_date(obter_informacoes_pendencia(c.nr_sequencia, 'D'), 'dd/mm/yyyy hh24:mi:ss'),c.dt_atualizacao_nrec),'TM')) intervalo
from   gqa_pendencia a,
       gqa_pendencia_regra b, 
	  gqa_pend_pac_acao c,
	  gqa_acao d,
	  gqa_pendencia_pac e,
       (select cd_pessoa_fisica cd_pessoa_fisica, nvl(ppa.nr_seq_superior, ppa.nr_seq_superior_saps) nr_seq_superior, ppad.nr_seq_pend_pac_acao
        from   gqa_pend_pac_acao_dest ppad, gqa_pend_pac_acao ppa
        where  ppad.nr_seq_pend_pac_acao = ppa.nr_sequencia) x       
where  b.nr_seq_pendencia = a.nr_sequencia
and    d.nr_seq_pend_regra = b.nr_sequencia
and    c.nr_seq_regra_acao = d.nr_sequencia
and    e.nr_sequencia = c.nr_seq_pend_pac
and    decode(c.nr_seq_proc, null, decode(c.nr_seq_proc_saps, null, null, c.nr_seq_superior_saps), c.nr_seq_superior) is null
and    (x.nr_seq_superior = c.nr_sequencia
        or     x.nr_seq_pend_pac_acao = c.nr_sequencia);
/