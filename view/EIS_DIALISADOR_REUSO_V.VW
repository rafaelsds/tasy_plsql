Create or replace view EIS_DIALISADOR_REUSO_V
AS
select	m.ds_modelo,
	m.nr_sequencia nr_seq_modelo,
	substr(obter_nome_pf(d.cd_pessoa_fisica),1,50) nm_paciente,
	d.cd_pessoa_fisica,
	substr(obter_nome_estabelecimento(u.cd_estabelecimento),1,50) ds_estabelecimento,
	substr(hd_obter_unid_atual_dialisador(d.nr_sequencia,'D'),1,50) ds_unidade,
	hd_obter_unid_atual_dialisador(d.nr_sequencia,'C') nr_seq_unidade,
	d.ie_motivo_descarte,
	substr(obter_valor_dominio(1917, d.ie_motivo_descarte),1,50) ds_motivo_descarte,
	d.dt_descarte,
	u.cd_estabelecimento,
	obter_empresa_estab(u.cd_estabelecimento) cd_empresa
from	hd_modelo_dialisador m,
	hd_pac_renal_cronico p,
	hd_unidade_dialise u,
	hd_dializador d
where	u.nr_sequencia = hd_obter_unid_atual_dialisador(d.nr_sequencia,'C')
and	p.cd_pessoa_fisica = d.cd_pessoa_fisica 
and	m.nr_sequencia = d.nr_seq_modelo
and	p.dt_fim is null;
/	
