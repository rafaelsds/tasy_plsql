CREATE OR replace VIEW TWS_WSUITE_TERMO_USO_V AS
SELECT nr_sequencia,
	ie_situacao,
	nm_termo,
	ds_termo,
	ie_origem_termo,
	dt_atualizacao
FROM wsuite_termo_uso;
/