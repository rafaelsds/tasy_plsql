create or replace
view procedimento_cirurgia_v1
as
select	a.nr_cirurgia,
	a.cd_pessoa_fisica,
	a.dt_inicio_real,
	a.cd_medico_cirurgiao,
	a.cd_procedimento_princ,
	a.ie_origem_proced,
	a.nr_seq_proc_interno,
	1 qt_procedimento
from	cirurgia a
where	a.ie_status_cirurgia = '2'
union all
select	c.nr_cirurgia,
	c.cd_pessoa_fisica,
	c.dt_inicio_real,
	nvl(a.cd_medico_exec,c.cd_medico_cirurgiao),
	a.cd_procedimento,
	a.ie_origem_proced,
	a.nr_seq_proc_interno,
	a.qt_procedimento		 
from	prescr_procedimento a,
	cirurgia c
where	a.nr_prescricao = c.nr_prescricao;
/