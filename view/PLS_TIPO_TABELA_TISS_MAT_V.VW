create or replace
view 	pls_tipo_tabela_tiss_mat_v as
select	m.nr_sequencia nr_seq_material,
	null ie_tipo_despesa,
	(select t.cd_tabela_xml
	from	tiss_tipo_tabela t
	where	t.nr_sequencia = r.nr_seq_tiss_tabela) cd_tabela,
	2 nr_prioridade,
	r.cd_estabelecimento,
	p.nr_sequencia nr_seq_regra,
	r.nr_sequencia nr_seq_regra_princ
from	pls_regra_tabela_tiss 	r,
	pls_regra_tab_tiss_proc	p,
	pls_material		m
where	r.ie_situacao 		= 'A'
and	p.nr_seq_regra_tab_tiss	= r.nr_sequencia
and	p.ie_situacao		= 'A'
and	p.nr_seq_material	is not null
and	m.nr_sequencia		= p.nr_seq_material
union all
select	null nr_seq_material,
	p.ie_tipo_despesa_mat ie_tipo_despesa,
	(select t.cd_tabela_xml
	from	tiss_tipo_tabela t
	where	t.nr_sequencia = r.nr_seq_tiss_tabela) cd_tabela,
	1 nr_prioridade,
	r.cd_estabelecimento,
	p.nr_sequencia nr_seq_regra,
	r.nr_sequencia nr_seq_regra_princ
from	pls_regra_tabela_tiss 	  r,
	pls_regra_tab_tiss_proc	  p
where	r.ie_situacao 		= 'A'
and	p.nr_seq_regra_tab_tiss = r.nr_sequencia
and	p.ie_situacao 		= 'A'
and	p.ie_tipo_despesa_mat	is not null;
/
