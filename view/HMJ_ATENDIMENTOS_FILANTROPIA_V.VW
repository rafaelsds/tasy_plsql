create or replace view HMJ_ATENDIMENTOS_FILANTROPIA_V as
SELECT	1 qt,
		a.nr_atendimento,
		substr(obter_nome_tipo_atend(a.ie_tipo_atendimento),1,30) ds_tipo_atendimento,
		a.ie_tipo_atendimento ie_tipo_atendimento,
		a.dt_entrada dt_entrada,
		a.dt_alta dt_alta,
		b.cd_convenio cd_convenio,
		substr(obter_valor_dominio(11,obter_tipo_convenio(b.cd_convenio)),1,100) ds_tipo_convenio,
		obter_tipo_convenio(b.cd_convenio) cd_tipo_convenio,
		substr(obter_nome_setor(c.cd_setor_atendimento),1,50) ds_setor_atendimento,
		cd_setor_atendimento cd_setor_atendimento,
		'A' ie_periodo
FROM	atendimento_paciente a,
		atend_categoria_convenio b,
		atend_paciente_unidade c
WHERE   a.nr_atendimento = b.nr_atendimento
and		b.nr_atendimento = c.nr_atendimento
and		c.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento, 'P')
and		b.nr_seq_interno = obter_atecaco_atendimento(a.nr_atendimento)
union all
SELECT	1 qt,
		a.nr_atendimento,
		substr(obter_nome_tipo_atend(a.ie_tipo_atendimento),1,30) ds_tipo_atendimento,
		a.ie_tipo_atendimento ie_tipo_atendimento,
		a.dt_entrada dt_entrada,
		a.dt_alta dt_alta,
		b.cd_convenio cd_convenio,
		substr(obter_valor_dominio(11,obter_tipo_convenio(b.cd_convenio)),1,100) ds_tipo_convenio,
		obter_tipo_convenio(b.cd_convenio) cd_tipo_convenio,
		substr(obter_nome_setor(c.cd_setor_atendimento),1,50) ds_setor_atendimento,
		cd_setor_atendimento cd_setor_atendimento,
		'M' ie_periodo
FROM	atendimento_paciente a,
		atend_categoria_convenio b,
		atend_paciente_unidade c
WHERE   a.nr_atendimento = b.nr_atendimento
and		b.nr_atendimento = c.nr_atendimento
and		c.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento, 'P')
and		b.nr_seq_interno = obter_atecaco_atendimento(a.nr_atendimento);
/

