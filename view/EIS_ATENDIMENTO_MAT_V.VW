create or replace
view Eis_Atendimento_Mat_v as
select	a.*,
	obter_nome_setor(a.cd_setor_solicitante) ds_setor_solicitante,
	obter_desc_estrut_mat(a.cd_grupo_material, null, null, null) ds_grupo_material,
	obter_desc_estrut_mat(null, a.cd_subgrupo_material, null, null) ds_subgrupo_material,
	obter_nome_pessoa_fisica(a.cd_medico, null) nm_medico,
	decode(a.cd_centro_custo, null, 'N�o Informado', 
	'(' || a.cd_centro_custo || ') ' || obter_desc_centro_custo(a.cd_centro_custo)) ds_centro_custo,
	obter_desc_local_estoque(a.cd_local_estoque) ds_local_estoque,
	obter_desc_local_estoque(a.cd_local_destino) ds_local_destino,
	obter_valor_dominio(29, a.ie_tipo_material) ds_tipo_material,
	obter_valor_dominio(33, a.ie_origem_inf) ds_origem_informacao,
	obter_Eis_Tipo_Baixa_mat(nvl(a.cd_motivo_baixa,0), a.ie_ocorrencia) ds_motivo_baixa,
	decode(a.ie_ocorrencia, 'P','Prescri��o','Requisi��o') ds_ocorrencia,
	trunc(dt_referencia, 'MONTH') dt_mensal
from	Eis_Atendimento_Mat a;
/