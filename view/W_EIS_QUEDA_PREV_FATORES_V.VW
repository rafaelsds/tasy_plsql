create or replace view w_eis_queda_prev_fatores_v  as 
select	distinct	
	obter_unidade_atendimento(b.nr_atendimento,'A','S') ds_Setor_Queda,	
	b.dt_avaliacao,
	f.cd_empresa,
	c.nr_seq_item,
	a.cd_estabelecimento,
	obter_unidade_atendimento(b.nr_atendimento,'A','U') ds_unidade,	
	b.nr_sequencia,
	c.ie_resultado,
	substr(Obter_desc_result_queda(c.nr_seq_item,'F'),1,255) ds_fator,
	to_number(obter_dados_ficha_queda(null,'TA',b.dt_avaliacao,null)) qt_total,
	substr(obter_resultado_escala_eif(b.nr_sequencia,'T'),1,255) ds_pontos,	
	b.nr_atendimento	
from	escala_eif b,
	escala_eif_item c,
	eif_escala d,
	estabelecimento f,
	atendimento_paciente a
where	a.nr_atendimento = b.nr_atendimento
and	d.nr_sequencia = b.nr_seq_escala
and	b.dt_inativacao is null 
and	upper(d.ds_escala) like upper('%queda%')
and	a.cd_estabelecimento = f.cd_estabelecimento
and	b.nr_sequencia = c.nr_seq_escala(+);
/
