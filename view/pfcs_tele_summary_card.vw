create or replace view pfcs_tele_summary_card as
	select	indi.nr_sequencia nr_seq_indicator,
			ppd.nr_seq_operational_level cd_establishment,
			pdb.cd_department,
			to_number(pfcs_get_indicator_rule(indi.nr_sequencia, nvl(0,0), nvl(ppd.nr_seq_operational_level,0),'ORDER')) nr_seq_ord
	  from	pfcs_indicator indi
	inner join pfcs_panel_detail ppd on (ppd.nr_seq_indicator = indi.nr_sequencia and ppd.ie_situation = 'A')
	left join pfcs_detail_bed pdb on (ppd.nr_sequencia = pdb.nr_seq_detail)
	 where	indi.ie_classification = 'TL'
	   and	indi.nr_sequencia in (40,46,160,128,50)
	union all
	select	51 nr_seq_indicator,
			ppd.nr_seq_operational_level cd_establishment,
			pdb.cd_department,
			to_number(pfcs_get_indicator_rule(51, nvl(0,0), nvl(ppd.nr_seq_operational_level,0),'ORDER')) nr_seq_ord
	  from	pfcs_panel_detail ppd
	inner join pfcs_detail_patient pdp on (ppd.nr_sequencia = pdp.nr_seq_detail)
	left join pfcs_detail_bed pdb on (ppd.nr_sequencia = pdb.nr_seq_detail)
	 where	ppd.nr_seq_indicator = 46 -- Patient to review get data from this indicator
	   and	ppd.nr_sequencia = pdb.nr_seq_detail
	   and	ppd.ie_situation = 'A'
	   and pfcs_telemetry_config_pck.get_patient_review(pdp.dt_monitor_entrance) = 'S'
	union all
	select	144 nr_seq_indicator,
			ppd.nr_seq_operational_level cd_establishment,
			pdb.cd_department,
			to_number(pfcs_get_indicator_rule(144, nvl(0,0), nvl(ppd.nr_seq_operational_level,0),'ORDER')) nr_seq_ord
	  from	pfcs_panel_detail ppd
	inner join pfcs_detail_patient pdp on (ppd.nr_sequencia = pdp.nr_seq_detail)
	left join pfcs_detail_bed pdb on (ppd.nr_sequencia = pdb.nr_seq_detail)
	 where	ppd.nr_seq_indicator = 46 -- Patient to review get data from this indicator
	   and	ppd.nr_sequencia = pdb.nr_seq_detail
	   and	ppd.ie_situation = 'A'	   
	   and pfcs_telemetry_config_pck.get_transition_review(pdp.qt_red_alarms, pdp.qt_yellow_alarms, pdp.qt_edi_score, pdp.dt_monitor_entrance) = 'S'
	   and pfcs_telemetry_config_pck.get_discharge_orders(ppd.nr_seq_operational_level, pdp.nr_seq_encounter) = 'N';
/
