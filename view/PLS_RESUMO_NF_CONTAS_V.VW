create or replace view pls_resumo_nf_contas_v
as
select	cd_conta,
	sum(vl_credito) vl_credito,
	sum(vl_debito) vl_debito,
	dt_referencia
from	(
	select	d.cd_conta_rec cd_conta,
		decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_credito,
		decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_debito,
		trunc(f.dt_lote,'month') dt_referencia
	from	pls_mensalidade_seg_item	d,
		pls_mensalidade_segurado	c,
		pls_mensalidade			b,
		pls_lote_mensalidade		a,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.ie_tipo_item	<> '3'
	and	not exists (	select	1
				from	pls_regra_pro_rata_dia x
				where	x.ie_tipo_item_mensalidade	= d.ie_tipo_item)
	and	c.dt_mesano_referencia = trunc(f.dt_lote,'month')
	UNION ALL
	select	f.cd_conta_cred cd_conta,
		decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_credito,
		decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_debito,
		trunc(h.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_conta			e,
		pls_conta_coparticipacao	f,
		pls_lote_nf_mens_item		g,
		pls_lote_nf_mensalidade		h
	where	g.nr_seq_lote	= h.nr_sequencia
	and	b.nr_sequencia	= g.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.nr_seq_conta	= e.nr_sequencia
	and	e.nr_sequencia	= f.nr_seq_conta
	and	d.ie_tipo_item	= '3'
	and	c.dt_mesano_referencia = trunc(h.dt_lote,'month')
	UNION ALL
	select	d.cd_conta_rec_antecip cd_conta,
		decode(d.vl_antecipacao,abs(d.vl_antecipacao),abs(d.vl_antecipacao),0) vl_credito,
		decode(d.vl_antecipacao,abs(d.vl_antecipacao),0,abs(d.vl_antecipacao)) vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.vl_antecipacao <> 0
	and	a.dt_contabilizacao is null
	and	c.dt_mesano_referencia = trunc(f.dt_lote,'month')
	UNION ALL
	select	d.cd_conta_rec_antecip cd_conta,
		decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_credito,
		decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	a.dt_contabilizacao = trunc(f.dt_lote,'month')
	UNION ALL
	select	d.cd_conta_rec cd_conta,
		decode(d.vl_pro_rata_dia,abs(d.vl_pro_rata_dia),abs(d.vl_pro_rata_dia),0) vl_credito,
		decode(d.vl_pro_rata_dia,abs(d.vl_pro_rata_dia),0,abs(d.vl_pro_rata_dia)) vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.vl_pro_rata_dia <> 0
	and	exists (select	1
			from	pls_regra_pro_rata_dia x
			where	x.ie_tipo_item_mensalidade	= d.ie_tipo_item)
	and	c.dt_mesano_referencia = trunc(f.dt_lote,'month')
	UNION ALL
	select	d.cd_conta_deb cd_conta,
		decode(d.vl_item,abs(d.vl_item),0,abs(d.vl_item)) vl_credito,
		decode(d.vl_item,abs(d.vl_item),abs(d.vl_item),0) vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.ie_tipo_item	<> '3'
	and	nvl(a.dt_contabilizacao,c.dt_mesano_referencia) = trunc(f.dt_lote,'month')
	UNION ALL
	select	f.cd_conta_deb cd_conta,
		decode(d.vl_item,abs(d.vl_item),0,abs(f.vl_coparticipacao)) vl_credito,
		decode(d.vl_item,abs(d.vl_item),abs(f.vl_coparticipacao),0) vl_debito,
		trunc(h.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_conta			e,
		pls_conta_coparticipacao	f,
		pls_lote_nf_mens_item		g,
		pls_lote_nf_mensalidade		h
	where	g.nr_seq_lote	= h.nr_sequencia
	and	b.nr_sequencia	= g.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.nr_seq_conta	= e.nr_sequencia
	and	e.nr_sequencia	= f.nr_seq_conta
	and	d.ie_tipo_item	= '3'
	and	c.dt_mesano_referencia = trunc(h.dt_lote,'month')
	union all /* Lepinski - in�cio contas imposto */
	select  substr(pls_obter_conta_imposto('C',b.nr_sequencia,a.cd_estabelecimento),1,255) cd_conta,
		nvl(e.vl_tributo,0) vl_credito,
		0 vl_debito,
		trunc(g.dt_lote,'month')
	from	pls_lote_mensalidade	a,
		pls_mensalidade		b,
		titulo_receber		c,
		nota_fiscal		d,
		nota_fiscal_trib	e,
		pls_lote_nf_mens_item	f,
		pls_lote_nf_mensalidade	g
	where	f.nr_seq_lote	= g.nr_sequencia
	and	b.nr_sequencia	= f.nr_seq_mensalidade
	and	a.nr_sequencia			= b.nr_seq_lote
	and	c.nr_seq_mensalidade(+)		= b.nr_sequencia
	and	d.nr_seq_mensalidade(+)		= b.nr_sequencia
	and     e.nr_sequencia(+)		= d.nr_sequencia
	and     e.vl_tributo <> 0
	and	b.dt_referencia = trunc(g.dt_lote,'month')
	union all
	select  substr(pls_obter_conta_imposto('D',b.nr_sequencia,a.cd_estabelecimento),1,255) cd_conta,
		0 vl_credito,
		nvl(e.vl_tributo,0) vl_debito,
		trunc(g.dt_lote,'month')
	from	pls_lote_mensalidade	a,
		pls_mensalidade		b,
		titulo_receber		c,
		nota_fiscal		d,
		nota_fiscal_trib	e,
		pls_lote_nf_mens_item	f,
		pls_lote_nf_mensalidade	g
	where	f.nr_seq_lote	= g.nr_sequencia
	and	b.nr_sequencia	= f.nr_seq_mensalidade
	and	a.nr_sequencia			= b.nr_seq_lote
	and	c.nr_seq_mensalidade(+)		= b.nr_sequencia
	and	d.nr_seq_mensalidade(+)		= b.nr_sequencia
	and     e.nr_sequencia(+)		= d.nr_sequencia
	and     e.vl_tributo <> 0
	and	b.dt_referencia = trunc(g.dt_lote,'month')	/* Lepinski - fim contas imposto */
	union all
	select	d.cd_conta_rec_antecip cd_conta,
		0 vl_credito,
		d.vl_antecipacao vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.vl_antecipacao <> 0
	and	d.dt_antecipacao = trunc(f.dt_lote,'month')
	union all
	select	d.cd_conta_deb_antecip cd_conta,
		0 vl_credito,
		d.vl_pro_rata_dia vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and     a.dt_contabilizacao is not null 
	and	d.vl_pro_rata_dia <> 0
	and	c.dt_mesano_referencia = trunc(f.dt_lote,'month')
	union all
	select	d.cd_conta_rec cd_conta,
		d.vl_antecipacao vl_credito,
		0 vl_debito,
		trunc(f.dt_lote,'month')
	from	pls_lote_mensalidade		a,
		pls_mensalidade			b,
		pls_mensalidade_segurado	c,
		pls_mensalidade_seg_item	d,
		pls_lote_nf_mens_item		e,
		pls_lote_nf_mensalidade		f
	where	e.nr_seq_lote	= f.nr_sequencia
	and	b.nr_sequencia	= e.nr_seq_mensalidade
	and	a.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	d.vl_antecipacao <> 0
	and	d.dt_antecipacao = trunc(f.dt_lote,'month')
)
group by cd_conta, dt_referencia;
/