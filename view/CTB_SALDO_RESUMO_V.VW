create or replace
view CTB_SALDO_RESUMO_V as
select	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) nm_estabelecimento,
	ctb_obter_mes_ref(a.nr_seq_mes_ref) dt_referencia,
	decode(c.ie_tipo, 'A', vl_saldo, 0) vl_ativo,
	decode(c.ie_tipo, 'P', vl_saldo, 0) vl_passivo,
	decode(c.ie_tipo, 'C', vl_saldo, 0) vl_receita,
	decode(c.ie_tipo, 'D', vl_saldo, 0) vl_despesa,
	(decode(c.ie_tipo, 'A', vl_saldo, 0) + decode(c.ie_tipo, 'D', vl_saldo, 0)) vl_ativo_mais_despesa,
	(decode(c.ie_tipo, 'P', vl_saldo, 0) + decode(c.ie_tipo, 'C', vl_saldo, 0)) vl_passivo_mais_receita,
	(decode(c.ie_tipo, 'A', vl_saldo, 0) + decode(c.ie_tipo, 'D', vl_saldo, 0)) -
	(decode(c.ie_tipo, 'P', vl_saldo, 0) + decode(c.ie_tipo, 'C', vl_saldo, 0)) vl_diferenca,
	decode(c.ie_tipo, 'A', vl_saldo_ant, 0) vl_ativo_ant,
	decode(c.ie_tipo, 'P', vl_saldo_ant, 0) vl_passivo_ant,
	decode(c.ie_tipo, 'C', vl_saldo_ant, 0) vl_receita_ant,
	decode(c.ie_tipo, 'D', vl_saldo_ant, 0) vl_despesa_ant,
	(decode(c.ie_tipo, 'A', vl_saldo_ant, 0) + decode(c.ie_tipo, 'D', vl_saldo_ant, 0)) vl_ativo_mais_despesa_ant,
	(decode(c.ie_tipo, 'P', vl_saldo_ant, 0) + decode(c.ie_tipo, 'C', vl_saldo_ant, 0)) vl_passivo_mais_receita_ant,
	(decode(c.ie_tipo, 'A', vl_saldo_ant, 0) + decode(c.ie_tipo, 'D', vl_saldo_ant, 0)) -
	(decode(c.ie_tipo, 'P', vl_saldo_ant, 0) + decode(c.ie_tipo, 'C', vl_saldo_ant, 0)) vl_diferenca_ant,
	decode(c.ie_tipo, 'A', vl_debito, 0) vl_ativo_deb,
	decode(c.ie_tipo, 'P', vl_debito, 0) vl_passivo_deb,
	decode(c.ie_tipo, 'C', vl_debito, 0) vl_receita_deb,
	decode(c.ie_tipo, 'D', vl_debito, 0) vl_despesa_deb,
	0 vl_ativo_mais_despesa_deb,
	0 vl_passivo_mais_receita_deb,
	0 vl_diferenca_deb,
	decode(c.ie_tipo, 'A', vl_credito, 0) vl_ativo_cred,
	decode(c.ie_tipo, 'P', vl_credito, 0) vl_passivo_cred,
	decode(c.ie_tipo, 'C', vl_credito, 0) vl_receita_cred,
	decode(c.ie_tipo, 'D', vl_credito, 0) vl_despesa_cred,
	0 vl_ativo_mais_despesa_cred,
	0 vl_passivo_mais_receita_cred,
	0 vl_diferenca_cred,
	a.nr_seq_mes_ref,
	a.cd_estabelecimento,
	a.cd_empresa
from	ctb_grupo_conta c,
	conta_contabil b,
	ctb_balancete_v a
where	(CTB_Obter_Nivel_Classif_Conta(substr(obter_dados_conta_contabil(a.cd_conta_contabil, a.cd_estabelecimento, 'CL'),1,40)) = 1)
and	a.ie_normal_encerramento = 'N'
and	a.cd_conta_contabil = b.cd_conta_contabil
and	b.cd_grupo = c.cd_grupo
order by	ctb_obter_mes_ref(a.nr_seq_mes_ref);
/
