create or replace view San_Exame_v as
select	c.nr_seq_exame,
	b.nr_sequencia,
	a.dt_doacao dt_exame,
	nvl(ds_resultado, nvl(to_char(vl_resultado), ie_resultado)) ds_resultado,
	1 nr_ocor, 
	1 nr_doac, 
	0 nr_res, 
	0 nr_transf,
	1 ie_exame,
	substr(obter_nome_pf(a.CD_PESSOA_FISICA),1,255) nm_paciente,
	a.nr_atendimento nr_atendimento
from san_exame_realizado c,
     san_exame_lote b,
     san_doacao a
where a.nr_sequencia = b.nr_seq_doacao
  and b.nr_sequencia = c.nr_seq_exame_lote
  and somente_numero(a.nr_sangue) <> 0
union
select	c.nr_seq_exame, 
	b.nr_sequencia,
	a.dt_transfusao,
	nvl(ds_resultado, nvl(to_char(vl_resultado), ie_resultado)) ds_resultado,
	1 nr_ocor, 
	0, 
	0, 
	1 nr_transf,
	2 ie_exame,
	substr(obter_nome_pf(a.CD_PESSOA_FISICA),1,255) nm_paciente,
	a.nr_atendimento nr_atendimento
from san_exame_realizado c,
     san_exame_lote b,
     san_transfusao a
where a.nr_sequencia = b.nr_seq_transfusao
  and b.nr_sequencia = c.nr_seq_exame_lote
  and b.nr_seq_producao is null
union
select	c.nr_seq_exame, 
	b.nr_sequencia,
	a.dt_transfusao,
	nvl(ds_resultado, nvl(to_char(vl_resultado), ie_resultado)) ds_resultado,
	1 nr_ocor, 
	0, 
	0, 
	1 nr_transf,
	2 ie_exame,
	substr(nvl(obter_nome_pf(san_obter_doador_producao(d.nr_sequencia)),obter_desc_expressao(892968)) ,1,255) nm_paciente,
	a.nr_atendimento nr_atendimento
from san_exame_realizado c,
     san_exame_lote b,
     san_producao d,
     san_transfusao a
where a.nr_sequencia = d.nr_seq_transfusao
  and d.nr_sequencia = b.nr_seq_producao
  and b.nr_sequencia = c.nr_seq_exame_lote
  and b.ie_origem = 'T'
union
select	c.nr_seq_exame, 
	b.nr_sequencia,
	a.dt_cirurgia + 0.1,
	nvl(ds_resultado, nvl(to_char(vl_resultado), ie_resultado)) ds_resultado,
	1 nr_ocor, 
	0, 
	1 nr_res, 
	0,
	3 ie_exame,
	substr(obter_nome_pf(a.CD_PESSOA_FISICA),1,255) nm_paciente,
	a.nr_atendimento nr_atendimento
from san_exame_realizado c,
     san_exame_lote b,
     san_reserva a
where a.nr_sequencia = b.nr_seq_reserva
  and b.nr_sequencia = c.nr_seq_exame_lote
  and b.nr_seq_res_prod is null
union
select	c.nr_seq_exame, 
	b.nr_sequencia,
	a.dt_cirurgia + 0.1,
	nvl(ds_resultado, nvl(to_char(vl_resultado), ie_resultado)) ds_resultado,
	1 nr_ocor, 
	0, 
	1 nr_res, 
	0,
	3 ie_exame,
	substr(nvl(obter_nome_pf(san_obter_doador_producao(d.nr_seq_producao)),obter_desc_expressao(892968)) ,1,255) nm_paciente,
	a.nr_atendimento nr_atendimento
from san_exame_realizado c,
     san_exame_lote b,
     san_reserva_prod d,
     san_reserva a
where a.nr_sequencia = d.nr_seq_reserva
  and b.nr_seq_res_prod = d.nr_sequencia
  and b.nr_sequencia = c.nr_seq_exame_lote;
/