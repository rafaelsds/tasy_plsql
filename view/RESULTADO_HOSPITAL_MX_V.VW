create or replace view resultado_hospital_mx_v as 
select a.cd_serie_nf serie,
	nvl(a.nr_nota_fiscal, 'NO TIENE') factura,
	upper(substr(obter_valor_dominio(1056,a.ie_situacao),1,20)) situacao,
	obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc) cliente,
	obter_paciente_conta(a.nr_interno_conta,'D') paciente,
	upper(obter_convenio_conta(a.nr_interno_conta))
	|| ' - '
	|| obter_plano_atendimento(obter_atendimento_conta(a.nr_interno_conta),'D') convenio_cuenta,
	a.nr_nfe_imp ds_uuid,
	a.dt_emissao dt_referencia,
	a.dt_emissao_nfe fh_emision_fe,
	a.dt_cancelamento fh_cancelacion,
	a.dt_cancelamento_nfe fh_cancelacion_fe,
    decode(a.ie_acao_nf,1, decode(b.ie_data_contab_nf, 'ES', a.dt_entrada_saida, 'ATU', a.dt_atualizacao, 'EST', a.dt_atualizacao_estoque, 'OPE', decode(obter_regra_contab_nf_operacao(a.nr_sequencia), 'ES',
	a.dt_entrada_saida, 'ATU', a.dt_atualizacao, 'EST', a.dt_atualizacao_estoque, a.dt_emissao), a.dt_emissao), decode(b.ie_data_contab_est_nf, 'ES', a.dt_entrada_saida, 'ATU', a.dt_atualizacao, 'EST',
	a.dt_atualizacao_estoque, 'OPE', decode(obter_regra_contab_nf_operacao(a.nr_sequencia), 'ES', a.dt_entrada_saida, 'ATU', a.dt_atualizacao, 'EST', a.dt_atualizacao_estoque, a.dt_emissao), a.dt_emissao)) fh_contabilizacion,
	nvl(decode(a.ie_situacao, 1, decode(a.nr_nfe_imp, null, 0, 1), 1) * obter_dados_iva(a.nr_sequencia,0) * decode(a.ie_acao_nf,1,1,-1) * decode(a.cd_serie_nf,'BBT',-1,1), 0) importe_0,
	nvl(decode(a.ie_situacao, 1, decode(a.nr_nfe_imp, null, 0, 1), 1) * obter_dados_iva(a.nr_sequencia,16) * decode(a.ie_acao_nf,1,1,-1) * decode(a.cd_serie_nf,'BBT',-1,1), 0) importe_16,
	nvl(decode(a.ie_situacao, 1, decode(a.nr_nfe_imp, null, 0, 1), 1) * obter_vl_total_trib_nota(a.nr_sequencia,'IVA') * decode(a.ie_acao_nf,1,1,-1) * decode(a.cd_serie_nf,'BBT',-1,1), 0) iva,
	nvl(decode(a.ie_situacao, 1, decode(a.nr_nfe_imp, null, 0, 1), 1) * a.vl_total_nota * decode(a.ie_acao_nf,1,1,-1) * decode(a.cd_serie_nf,'BBT',-1,1), 0) total,
	a.nr_sequencia,
	nvl(decode(a.ie_situacao,2,
	(select obter_protocolo_conpaci(max(nr_interno_conta))
	from conta_paciente
	where nr_seq_conta_origem = a.nr_interno_conta
	and ie_cancelamento       = 'E'
	), obter_protocolo_conpaci(a.nr_interno_conta)), 0)
	|| ',' nr_protocolo
from 	nota_fiscal a,
	parametro_estoque b
where 	a.ie_tipo_nota in ('SF','SE','SD')
and 	a.cd_estabelecimento = b.cd_estabelecimento
order by 1,2;
/