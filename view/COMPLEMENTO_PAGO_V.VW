create or replace
view complemento_pago_v
as
select b.nr_sequencia sequencia,
  Retorna_data_complemento_pago(b.nr_sequencia, d.nr_titulo, d.nr_sequencia) fecha,
  subst_virgula_ponto_adic_zero(campo_mascara_casas(Decode(elimina_acentuacao(b.ds_observacao), 'Cancelacion de pago', 1, Nvl(d.vl_recebido_estrang, d.vl_recebido)), 2)) monto,
  Decode(Obter_desc_sigla_moeda(d.cd_moeda), 'MXN', NULL, Subst_virgula_ponto_adic_zero( Obter_cotacao_moeda_fin(d.cd_moeda, d.dt_recebimento))) tipo, 
	lpad(nvl(cd_integracao_externa, 99),2,'0') forma, 
	Nvl(Obter_desc_sigla_moeda(d.cd_moeda), 'MXN') moneda, 	
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFC'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'RFC'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFC')) rfc_emisor,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'B'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'B'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'B')) nom_banco,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'C'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'C'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'C')) cta_ordenante,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFCB'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'RFCB'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFCB')) rfc_beneficiario,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'CONTAB'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'CONTAB'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'CONTAB')) cta_beneficiario	
from nota_fiscal  a,
  nota_fiscal  b,
  titulo_receber c,
  titulo_receber_liq d,
  tipo_recebimento e
where	a.nr_sequencia = b.nr_sequencia_ref
and	c.nr_seq_nf_saida = a.nr_sequencia
and	c.nr_titulo = d.nr_titulo
and	d.nr_sequencia = b.nr_seq_baixa_tit
and	d.cd_tipo_recebimento = e.cd_tipo_recebimento
union
select a.nr_sequencia sequencia,
  to_char(g.dt_recebimento, 'yyyy-mm-dd') || 'T' || to_char(g.dt_recebimento, 'hh24:mi:ss') fecha,
	subst_virgula_ponto_adic_zero(campo_mascara_casas(g.vl_recebimento, 2)) monto,
	decode(Obter_Desc_sigla_Moeda(d.cd_moeda),'MXN',null,subst_virgula_ponto_adic_zero(obter_cotacao_moeda_fin(d.cd_moeda,d.dt_recebimento))) tipo,
	lpad(nvl(cd_integracao_externa, 99),2,'0') forma,
	nvl(obter_desc_sigla_moeda(d.cd_moeda),'MXN') moneda,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFC'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'RFC'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFC')) rfc_emisor,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'B'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'B'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'B')) nom_banco,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'C'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'C'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'C')) cta_ordenante,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFCB'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'RFCB'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFCB')) rfc_beneficiario,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'CONTAB'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'CONTAB'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'CONTAB')) cta_beneficiario
from nota_fiscal a,
	nota_fiscal_item b,
	titulo_receber c,
	titulo_receber_liq d,
	tipo_recebimento e,
	convenio_ret_receb f,
	convenio_receb g    
where a.nr_sequencia = b.nr_sequencia
and b.nr_titulo is not null 
and b.nr_seq_tit_rec is not null 
and b.nr_titulo = c.nr_titulo
and c.nr_titulo = d.nr_titulo
and b.nr_seq_tit_rec = d.nr_sequencia
and d.cd_tipo_recebimento = e.cd_tipo_recebimento
and d.nr_seq_retorno = f.nr_seq_retorno
and g.nr_sequencia = f.nr_seq_receb
union
select distinct a.nr_sequencia sequencia,
  to_char(d.dt_recebimento, 'yyyy-mm-dd') || 'T' || to_char(d.dt_recebimento, 'hh24:mi:ss') fecha,
	(
    select subst_virgula_ponto_adic_zero(campo_mascara_casas(sum(nvl(trl.vl_recebido_estrang, trl.vl_recebido)), 2))     
from nota_fiscal nf,
      nota_fiscal_item nfi,
      titulo_receber tr,
      titulo_receber_liq trl
    where nf.nr_sequencia = a.nr_sequencia
    and nf.nr_sequencia = nfi.nr_sequencia
    and nfi.nr_titulo = tr.nr_titulo
    and tr.nr_titulo = trl.nr_titulo
  ) monto,
	decode(Obter_Desc_sigla_Moeda(d.cd_moeda),'MXN',null,subst_virgula_ponto_adic_zero(obter_cotacao_moeda_fin(d.cd_moeda,d.dt_recebimento))) tipo,
	lpad(nvl(cd_integracao_externa, 99),2,'0') forma,
	nvl(obter_desc_sigla_moeda(d.cd_moeda),'MXN') moneda,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFC'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'RFC'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFC')) rfc_emisor,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'B'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'B'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'B')) nom_banco,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'C'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'C'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'C')) cta_ordenante,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFCB'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'RFCB'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'RFCB')) rfc_beneficiario,
	decode(obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'CONTAB'),null,obter_dados_transf_mx(d.nr_titulo,d.nr_sequencia,'CONTAB'),obter_dados_control_mx(d.nr_titulo,d.nr_sequencia,'CONTAB')) cta_beneficiario
from nota_fiscal a,
	nota_fiscal_item b,
	titulo_receber c,
	titulo_receber_liq d,
	tipo_recebimento e
where a.nr_sequencia = b.nr_sequencia
and b.nr_titulo is not null 
and b.nr_seq_tit_rec is not null 
and b.nr_titulo = c.nr_titulo
and d.nr_seq_retorno is null
and c.nr_titulo = d.nr_titulo
and b.nr_seq_tit_rec = d.nr_sequencia
and d.cd_tipo_recebimento = e.cd_tipo_recebimento;
/
