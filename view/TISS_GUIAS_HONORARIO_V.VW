create or replace
view 	TISS_GUIAS_HONORARIO_V as
select	'2.01.01' ds_versao,
	c.cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4') ie_tipo_atendimento,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5') ie_tipo_saida,
	e.cd_medico_executor,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	substr(TISS_OBTER_GRAU_PARTIC(e.ie_funcao_medico),1,25) ds_funcao_medico,
	a.ie_tipo_atend_tiss,
	e.cd_especialidade,
	decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao) CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador) cd_cgc_honorario,
	substr(nvl(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro), decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao)),1,255) cd_autorizacao_princ,
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40) cd_tipo_acomodacao
from 	regra_honorario g,
	atend_categoria_convenio t,
	procedimento_paciente e,
	motivo_alta d,
	conta_paciente_guia c,
	atendimento_paciente b,
	conta_paciente a 
where	a.nr_atendimento		= b.nr_atendimento
and	a.nr_interno_conta		= c.nr_interno_conta
and	b.cd_motivo_alta		= d.cd_motivo_alta(+)
and	e.nr_interno_conta		= c.nr_interno_conta
and	e.nr_doc_convenio		= c.cd_autorizacao
and	e.ie_responsavel_credito	= g.cd_regra(+)
and	b.nr_atendimento		= t.nr_atendimento
and	obter_atecaco_atendimento(b.nr_atendimento) = t.nr_seq_interno
and	((TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				e.ie_tiss_tipo_guia) = '6') or
	 (TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				e.ie_tiss_tipo_guia_honor) = '6'))
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null
group	by c.cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U'),
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4'),
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5'),
	e.cd_medico_executor,
	obter_tipo_protocolo(a.nr_seq_protocolo),
	substr(TISS_OBTER_GRAU_PARTIC(e.ie_funcao_medico),1,25),
	a.ie_tipo_atend_tiss,
	e.cd_especialidade,
	decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao),
	a.cd_convenio_parametro,
	nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador),
	substr(nvl(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro), decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao)),1,255),
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40)
union
select	'2.01.01' ds_versao,
	'N�o Informada' cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4') ie_tipo_atendimento,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5') ie_tipo_saida,
	e.cd_medico_executor,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	substr(TISS_OBTER_GRAU_PARTIC(e.ie_funcao_medico),1,25) ds_funcao_medico,
	a.ie_tipo_atend_tiss,
	e.cd_especialidade,
	null CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador) cd_cgc_honorario,
	substr(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro),1,255) cd_autorizacao_princ,
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40) cd_tipo_acomodacao
from 	regra_honorario g,
	atend_categoria_convenio t,
	procedimento_paciente e,
	motivo_alta d,
	atendimento_paciente b,
	conta_paciente a 
where	a.nr_atendimento		= b.nr_atendimento
and	b.cd_motivo_alta		= d.cd_motivo_alta(+)
and	a.nr_interno_conta		= e.nr_interno_conta
and	e.ie_responsavel_credito	= g.cd_regra(+)
and	b.nr_atendimento		= t.nr_atendimento
and	nvl(e.nr_doc_convenio, 'N�o Informada') = 'N�o Informada'
and	obter_atecaco_atendimento(b.nr_atendimento)	= t.nr_seq_interno
and	((TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				e.ie_tiss_tipo_guia) = '6') or
	 (TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				e.ie_tiss_tipo_guia_honor) = '6'))
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null
group 	by decode(b.ie_carater_inter_sus,'1','E','U'),
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4'),
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5'),
	e.cd_medico_executor,
	obter_tipo_protocolo(a.nr_seq_protocolo),
	substr(TISS_OBTER_GRAU_PARTIC(e.ie_funcao_medico),1,25),
	a.ie_tipo_atend_tiss,
	e.cd_especialidade,
	a.cd_convenio_parametro,
	nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador),
	substr(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro),1,255),
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40)
union
select	'2.01.01' ds_versao,
	'N�o Informada' cd_autorizacao,
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4') ie_tipo_atendimento,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5') ie_tipo_saida,
	f.cd_pessoa_fisica cd_medico_executor,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	substr(TISS_OBTER_GRAU_PARTIC(f.ie_funcao),1,25) ds_funcao_medico,
	a.ie_tipo_atend_tiss,
	f.cd_especialidade,
	null CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	nvl(f.cd_cgc,nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador)) cd_cgc_honorario,
	substr(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro),1,255) cd_autorizacao_princ,
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40) cd_tipo_acomodacao
from 	regra_honorario g,
	atend_categoria_convenio t,
	motivo_alta d,
	procedimento_participante f,
	procedimento_paciente e,
	atendimento_paciente b,
	conta_paciente a
where	a.nr_atendimento		= b.nr_atendimento
and	b.cd_motivo_alta		= d.cd_motivo_alta(+)
and	a.nr_interno_conta		= e.nr_interno_conta
and	f.ie_responsavel_credito	= g.cd_regra(+)
and	f.nr_sequencia			= e.nr_sequencia
and	b.nr_atendimento		= t.nr_atendimento
and	obter_atecaco_atendimento(b.nr_atendimento)			  = t.nr_seq_interno
and	nvl(nvl(f.nr_doc_honor_conv, e.nr_doc_convenio), 'N�o Informada') = 'N�o Informada'
and	TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				f.ie_tiss_tipo_guia) = '6'
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null
group 	by decode(b.ie_carater_inter_sus,'1','E','U'),
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4'),
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5'),
	f.cd_pessoa_fisica,
	obter_tipo_protocolo(a.nr_seq_protocolo),
	substr(TISS_OBTER_GRAU_PARTIC(f.ie_funcao),1,25),
	a.ie_tipo_atend_tiss,
	f.cd_especialidade,
	a.cd_convenio_parametro,
	nvl(f.cd_cgc,nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador)),
	substr(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro),1,255),
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40)
union
select	'2.01.01' ds_versao,
	nvl(f.NR_DOC_HONOR_CONV,c.cd_autorizacao),
	decode(b.ie_carater_inter_sus,'1','E','U') DS_CARATER_INTERNACAO,
	'A' ds_tipo_saida, 
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4') ie_tipo_atendimento,
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5') ie_tipo_saida,
	f.cd_pessoa_fisica cd_medico_executor,
	obter_tipo_protocolo(a.nr_seq_protocolo) ie_tipo_protocolo,
	substr(TISS_OBTER_GRAU_PARTIC(f.ie_funcao),1,25) ds_funcao_medico,
	a.ie_tipo_atend_tiss,
	f.cd_especialidade,
	decode(nvl(f.NR_DOC_HONOR_CONV,c.cd_autorizacao), 'N�o Informada', null, nvl(f.NR_DOC_HONOR_CONV,c.cd_autorizacao)) CD_AUTORIZACAO_TAG,
	a.cd_convenio_parametro cd_convenio,
	nvl(f.cd_cgc,nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador)) cd_cgc_honorario,
	substr(nvl(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro), decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao)),1,255) cd_autorizacao_princ,
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40) cd_tipo_acomodacao
from 	regra_honorario g,
	atend_categoria_convenio t,
	motivo_alta d,
	procedimento_participante f,
	procedimento_paciente e,
	conta_paciente_guia c,
	atendimento_paciente b,
	conta_paciente a
where	a.nr_atendimento		= b.nr_atendimento
and	b.cd_motivo_alta		= d.cd_motivo_alta(+)
and	a.nr_interno_conta		= e.nr_interno_conta
and	f.ie_responsavel_credito	= g.cd_regra(+)
and	f.nr_sequencia			= e.nr_sequencia
and	e.nr_interno_conta		= c.nr_interno_conta
and	b.nr_atendimento		= t.nr_atendimento
and	obter_atecaco_atendimento(b.nr_atendimento)	= t.nr_seq_interno
and	nvl(e.nr_doc_convenio, f.nr_doc_honor_conv)	= c.cd_autorizacao
and	TISS_OBTER_GUIA_PROC(b.ie_tipo_atendimento, 
				e.cd_procedimento, 
				e.ie_origem_proced,
				g.ie_entra_conta,
				g.ie_repassa_medico,
				f.ie_tiss_tipo_guia) = '6'
and	nvl(e.nr_seq_proc_pacote, e.nr_sequencia) = e.nr_sequencia
and	e.cd_motivo_exc_conta	is null
group 	by  nvl(f.NR_DOC_HONOR_CONV, c.cd_autorizacao),
	decode(b.ie_carater_inter_sus,'1','E','U'),
	decode(b.ie_tipo_atendimento, 1, '7', 3, '4', 7, '6',8,'4'),
	a.nr_interno_conta,
	a.nr_seq_protocolo,
	a.nr_atendimento,
	b.dt_entrada,
	decode(nvl(d.ie_obito,'N'),'S','6','5'),
	f.cd_pessoa_fisica,
	obter_tipo_protocolo(a.nr_seq_protocolo),
	substr(TISS_OBTER_GRAU_PARTIC(f.ie_funcao),1,25),
	a.ie_tipo_atend_tiss,
	f.cd_especialidade,
	decode(nvl(f.NR_DOC_HONOR_CONV,c.cd_autorizacao), 'N�o Informada', null, nvl(f.NR_DOC_HONOR_CONV,c.cd_autorizacao)),
	a.cd_convenio_parametro,
	nvl(f.cd_cgc,nvl(e.cd_cgc_honorario_tiss, e.cd_cgc_prestador)),
	substr(nvl(TISS_OBTER_GUIA_PRINCIPAL(b.nr_atendimento, a.cd_convenio_parametro), decode(c.cd_autorizacao, 'N�o Informada', null, c.cd_autorizacao)),1,255),
	substr(tiss_obter_tipo_acomod(t.cd_tipo_acomodacao),1,40);
/
