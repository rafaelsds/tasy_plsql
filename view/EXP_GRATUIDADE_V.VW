create or replace view 
exp_gratuidade_v as
select	1 tp_registro,
	lpad(nvl(d.nr_prontuario,0),6,0) nr_prontuario,
	'TSY' ds_sistema_org,
	substr(obter_dados_estab(b.cd_estabelecimento,3),1,3) ds_unidade_atend,
	substr(d.nm_pessoa_fisica,1,50) nm_paciente,
	lpad(nvl(d.nr_cpf,'0'),11,'0') nr_cpf,
	d.nr_cartao_nac_sus nr_cns,
	d.nr_identidade,
	substr(obter_compl_pf(d.cd_pessoa_fisica,1,'EDN'),1,50) ds_endereco,
	substr(obter_compl_pf(d.cd_pessoa_fisica,1,'B'),1,30) ds_bairro,
	substr(obter_compl_pf(d.cd_pessoa_fisica,1,'DM'),1,30) ds_municipio,
	substr(obter_compl_pf(d.cd_pessoa_fisica,1,'UF'),1,2) sg_estado,
	substr(obter_compl_pf(d.cd_pessoa_fisica,1,'CEP'),1,8) cd_cep,
	substr(nvl(obter_dados_pf(d.cd_pessoa_fisica,'NDR'),obter_compl_pf(d.cd_pessoa_fisica,5,'N')),1,50) nm_mae,	
	to_char(to_date(obter_dados_pf(d.cd_pessoa_fisica,'DN'),'dd/mm/yyyy'),'yyyymmdd') dt_nascimento,
	a.ie_clinica,
	substr(gratuidade_gerar_campo_proc(c.nr_sequencia),1,50) nr_sequencia,
	substr(lpad(sus_obter_conversao_amb_grat(c.cd_procedimento,c.ie_origem_proced),10,'0'),1,10) cd_procedimento_sus,
	substr(obter_nome_convenio(b.cd_convenio_parametro),1,35) ds_convenio,
	substr(to_char(a.dt_entrada,'yyyymmdd'),1,8) dt_entrada,
	substr(to_char(a.dt_entrada,'hhmi'),1,4) hr_entrada,
	'  ' ie_classif_social,
	substr(gratuidade_obter_campo_local(c.nr_sequencia),1,1) cd_local_atend,
	substr(obter_conversao_meio_ext_ipe(obter_cgc_conv(b.cd_convenio_parametro),'GRATUIDADE','DS_OBS_NOTA',b.cd_convenio_parametro),1,1) ds_obs_nota,
	substr(obter_codigo_usuario_conv(a.nr_atendimento),1,30) cd_usuario_convenio,
	substr(nvl(obter_conversao_meio_ext_ipe(obter_cgc_conv(b.cd_convenio_parametro),'GRATUIDADE','CD_CGC',b.cd_convenio_parametro),obter_cgc_conv(b.cd_convenio_parametro)),1,14) cd_cgc,
	substr(obter_dados_pf(d.cd_pessoa_fisica,'SE'),1,1) ie_sexo,
	substr(obter_cid_atendimento(a.nr_atendimento,'P'),1,4) cd_doenca,
	substr(obter_cid_atendimento(a.nr_atendimento,'S'),1,4) cd_doenca_sec,
	substr(to_char(a.dt_alta,'yyyymmdd'),1,8) dt_alta,
	nvl(e.cd_externo,a.cd_motivo_alta) cd_motivo_alta,
	lpad(nvl(substr(obter_declaracao_obito(a.nr_atendimento),1,11),'0'),11,'0') nr_declaracao,
	substr(to_char(b.dt_mesano_referencia,'mmyyyy'),1,6) dt_competencia,
	obter_dados_categ_conv(a.nr_atendimento,'G') nr_doc_convenio,
	c.cd_setor_atendimento,
	b.dt_periodo_inicial,
	b.dt_periodo_final,
	c.dt_procedimento,
	b.dt_mesano_referencia,
	f.dt_mesano_referencia dt_filtro,
	b.dt_conta_definitiva,
	substr(d.nm_pessoa_fisica,1,50) ds_ordenacao,
	a.ie_tipo_atendimento,
	sus_obter_aihunif_conta(b.nr_interno_conta) nr_aih
from	motivo_alta e,
        protocolo_convenio f, 
	pessoa_fisica d,
	atendimento_paciente a,
	conta_paciente b,
	procedimento_paciente c
where	a.cd_motivo_alta = e.cd_motivo_alta(+)
and	a.cd_pessoa_fisica = d.cd_pessoa_fisica
and     b.nr_seq_protocolo = f.nr_seq_protocolo
and	a.nr_atendimento = b.nr_atendimento 
and	b.nr_interno_conta = c.nr_interno_conta
and	c.cd_motivo_exc_conta is null
and	c.nr_seq_proc_interno is not null
and	a.dt_alta is not null
and	b.ie_status_acerto = 2
and	b.ie_cancelamento is null
and	nvl(gratuidade_obter_se_exporta(c.nr_sequencia,a.ie_tipo_atendimento),'N') = 'S';
/
