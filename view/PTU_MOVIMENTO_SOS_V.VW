Create or replace view ptu_movimento_sos_v
as 
select	d.nr_sequencia			nr_seq_benef,
	a.nr_sequencia			nr_seq_lote,
	d.cd_unimed			cd_unimed,
	c.cd_empresa_origem 		cd_empresa,
	d.cd_familia			cd_familia,
	d.cd_dependencia		cd_dependencia,
	d.nm_beneficiario 		nm_benef,
	d.dt_nascimento			dt_nascimento,
	d.ie_sexo			ie_sexo,
	d.ie_estado_civil		ie_estado_civil,
	d.dt_inclusao 			dt_inclusao_sos,
	d.dt_exclusao 			dt_exclusao_sos,
	1				ie_tipo_cliente,
	e.ds_endereco			ds_endereco,
	e.ds_complemento		ds_complemento,
	e.ds_bairro			ds_bairro,
	e.nm_municipio			ds_cidade,
	e.sg_uf				sg_uf,
	e.cd_cep			cd_cep,
	e.nr_ddd			nr_ddd,
	e.nr_fone			nr_telefone,
	' '				nm_pessoa_contato,
	b.cd_unimed_origem		cd_unimed_origem,
	substr(d.cd_usuario_plano,13,1) nr_digito_verificador	
from	ptu_movimento_benef_compl	e,
	ptu_mov_produto_benef		d,
	ptu_mov_produto_empresa		c,
	ptu_movimentacao_produto	b,
	ptu_mov_produto_lote		a
where	e.nr_seq_beneficiario(+)	= d.nr_sequencia
and	d.nr_seq_empresa		= c.nr_sequencia
and	c.nr_seq_mov_produto		= b.nr_sequencia
and	b.nr_seq_lote			= a.nr_sequencia;
/
