create or replace view	pls_nome_pessoa_atend_v
as
select	a.cd_pessoa_fisica 	cd_pessoa_fisica,
	null 			cd_cgc,
	null	 		nm_fantasia,
	a.nm_pessoa_fisica	nm_pessoa_fisica,
	b.nr_telefone		nr_telefone,
	a.nr_telefone_celular	nr_telefone_celular
from	pessoa_fisica a,
	compl_pessoa_fisica b
where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica 
and	b.ie_tipo_complemento 	= 1
union
select	null		 	cd_pessoa_fisica,
	cd_cgc 			cd_cgc,
	nm_fantasia 		nm_fantasia,
	null			nm_pessoa_fisica,
	nr_telefone		nr_telefone,
	null			nr_telefone_celular
from	pessoa_juridica;
/