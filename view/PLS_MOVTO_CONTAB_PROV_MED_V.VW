create or replace view pls_movto_contab_prov_med_v
as
select	substr((select	ds_valor_dominio
		from 	valor_dominio
		where 	cd_dominio	= 5260
		and	vl_dominio	= a.ie_status),1,255) ds_status,
	a.ie_status,
	a.nr_seq_conta_proc,
	null nr_seq_conta_mat,
	b.cd_conta_provisao_cred,
	b.cd_conta_provisao_deb,
	b.cd_classif_prov_cred,
	b.cd_classif_prov_deb,
	b.nr_seq_esquema_prov nr_seq_esquema,
	b.cd_conta_glosa_cred,
	b.cd_conta_glosa_deb,
	b.cd_classif_glosa_cred,
	b.cd_classif_glosa_deb,
	b.nr_seq_esquema_glosa,
	a.nr_sequencia,
	b.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_provisao_cred),1,255) ds_conta_cred,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_provisao_deb),1,255) ds_conta_deb,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_glosa_cred),1,255) ds_conta_glosa_cred,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_glosa_deb),1,255) ds_conta_glosa_deb,
	substr((select	ds_valor_dominio
		from 	valor_dominio
		where 	cd_dominio	= 3418
		and	vl_dominio	= b.ie_ato_cooperado),1,255) ds_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	b.nr_seq_conta,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_proc		b
where	a.nr_seq_conta_proc	= b.nr_sequencia
and	a.nr_seq_resumo is null
union all
select	substr((select	ds_valor_dominio
		from 	valor_dominio
		where 	cd_dominio	= 5260
		and	vl_dominio	= a.ie_status),1,255) ds_status,
	a.ie_status,
	null nr_seq_conta_proc,
	a.nr_seq_conta_mat,
	b.cd_conta_provisao_cred,
	b.cd_conta_provisao_deb,
	b.cd_classif_prov_cred,
	b.cd_classif_prov_deb,
	b.nr_seq_esquema_prov nr_seq_esquema,
	b.cd_conta_glosa_cred,
	b.cd_conta_glosa_deb,
	b.cd_classif_glosa_cred,
	b.cd_classif_glosa_deb,
	b.nr_seq_esquema_glosa,
	a.nr_sequencia,
	b.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_provisao_cred),1,255) ds_conta_cred,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_provisao_deb),1,255) ds_conta_deb,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_glosa_cred),1,255) ds_conta_glosa_cred,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = b.cd_conta_glosa_deb),1,255) ds_conta_glosa_deb,
	substr((select	ds_valor_dominio
		from 	valor_dominio
		where 	cd_dominio	= 3418
		and	vl_dominio	= b.ie_ato_cooperado),1,255) ds_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	b.nr_seq_conta,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_mat		b
where	a.nr_seq_conta_mat	= b.nr_sequencia
and	a.nr_seq_resumo is null
union all
select	substr((select	ds_valor_dominio
		from 	valor_dominio
		where 	cd_dominio	= 5260
		and	vl_dominio	= a.ie_status),1,255) ds_status,
	a.ie_status,
	r.nr_seq_conta_proc,
	r.nr_seq_conta_mat,
	r.cd_conta_prov_cred cd_conta_provisao_cred,
	r.cd_conta_prov_deb cd_conta_provisao_deb,
	r.cd_classif_prov_cred,
	r.cd_classif_prov_deb,
	r.nr_seq_esquema_prov nr_seq_esquema,
	r.cd_conta_glosa_cred,
	r.cd_conta_glosa_deb,
	r.cd_classif_glosa_cred,
	r.cd_classif_glosa_deb,
	r.nr_seq_esquema_glosa,
	a.nr_sequencia,
	r.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = r.cd_conta_prov_cred),1,255) ds_conta_cred,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = r.cd_conta_prov_deb),1,255) ds_conta_deb,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = r.cd_conta_glosa_cred),1,255) ds_conta_glosa_cred,
	substr((select	ds_conta_contabil
		from	conta_contabil
		where	cd_conta_contabil = r.cd_conta_glosa_deb),1,255) ds_conta_glosa_deb,
	substr((select	ds_valor_dominio
		from 	valor_dominio
		where 	cd_dominio	= 3418
		and	vl_dominio	= r.ie_ato_cooperado),1,255) ds_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	r.nr_seq_conta,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_medica_resumo r
where	a.nr_seq_resumo		= r.nr_sequencia;
/