create or replace
view EIS_REPASSE_V as
select	a.dt_mesano_referencia,
	sum(a.vl_repasse) vl_repasse,
	sum(a.vl_liberado) vl_liberado,
	a.nm_medico,
	a.cd_medico,
	a.cd_estabelecimento,
	a.nr_seq_terceiro,
	substr(obter_nome_terceiro(a.nr_seq_terceiro),1,254) nm_terceiro,
	a.cd_convenio,
	substr(obter_nome_convenio(a.cd_convenio),1,255) ds_convenio
from	(
	select	x.dt_mesano_referencia,
		a.vl_repasse,
		nvl(a.vl_liberado, 0) vl_liberado,
		substr(obter_nome_pf_pj(a.cd_medico, null),1,100) nm_medico,
		a.cd_medico,
		x.cd_estabelecimento,
		x.nr_seq_terceiro,
		c.cd_convenio_parametro cd_convenio
	from	conta_paciente c,
		procedimento_paciente b,
		repasse_terceiro x,
		procedimento_repasse a
	where	x.nr_repasse_terceiro	= a.nr_repasse_terceiro
	and	a.nr_seq_procedimento	= b.nr_sequencia
	and	b.nr_interno_conta	= c.nr_interno_conta
	union all
	select	x.dt_mesano_referencia,
		a.vl_repasse,
		nvl(a.vl_liberado, 0) vl_liberado,
		substr(obter_nome_pf_pj(a.cd_medico, null),1,100) nm_medico,
		a.cd_medico,
		x.cd_estabelecimento,
		x.nr_seq_terceiro,
		c.cd_convenio_parametro cd_convenio
	from	conta_paciente c,
		material_atend_paciente b,
		repasse_terceiro x,
		material_repasse a
	where	a.nr_repasse_terceiro	= x.nr_repasse_terceiro
	and	a.nr_seq_material	= b.nr_sequencia
	and	b.nr_interno_conta	= c.nr_interno_conta
	union all
	select	b.dt_mesano_referencia,
		a.vl_repasse,
		a.vl_repasse vl_liberado,
		substr(obter_nome_pf_pj(a.cd_medico, null),1,100) nm_medico,
		a.cd_medico,
		b.cd_estabelecimento,
		b.nr_seq_terceiro,
		nvl(a.cd_convenio,b.cd_convenio) cd_convenio
	from	repasse_terceiro b,
		Repasse_Terceiro_Item a
	where	a.nr_repasse_terceiro	= b.nr_repasse_terceiro
	) a
group by a.nm_medico,
	a.cd_medico,
	a.dt_mesano_referencia,
	a.cd_estabelecimento,
	a.nr_seq_terceiro,
	substr(obter_nome_terceiro(a.nr_seq_terceiro),1,254),
	a.cd_convenio
order by a.nm_medico;
/