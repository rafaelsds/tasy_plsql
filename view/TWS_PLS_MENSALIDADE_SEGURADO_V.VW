create or replace view tws_pls_mensalidade_segurado_v as 
select	nr_sequencia,
	dt_atualizacao,
	nr_seq_segurado,
	vl_mensalidade,
	nr_seq_mensalidade,
	dt_mesano_referencia,
	nr_parcela,
	vl_coparticipacao 
from 	pls_mensalidade_segurado;
/