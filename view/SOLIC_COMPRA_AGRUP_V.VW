Create or replace View solic_compra_agrup_v as
select	distinct 
	nr_solic_compra,
	nr_cot_compra
from(
	select	x.nr_cot_compra,
		x.nr_solic_compra
	from	cot_compra_item x
	union all
	select	x.nr_cot_compra ,
		x.nr_solic_compra
	from	cot_compra_solic_agrup x);
/