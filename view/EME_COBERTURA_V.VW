Create or Replace View eme_cobertura_v as
select	a.nr_sequencia,
	a.cd_contrato, 
	a.nm_contrato,
	a.dt_contrato,
	a.dt_cancelamento,
	substr(obter_tipo_cobertura(a.nr_sequencia),1,60) ds_cobertura, 
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,100) ds_contratado,
	a.cd_convenio,
	a.cd_categoria,
	a.cd_tipo_acomodacao,
	a.cd_estabelecimento
from	eme_tipo_servico b,
	eme_tipo_cobertura d,
	eme_contrato a
where	a.nr_seq_cobertura = d.nr_sequencia
and	d.ie_pessoa_fisica = 'N';
/
