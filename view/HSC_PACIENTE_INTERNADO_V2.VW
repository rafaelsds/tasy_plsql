CREATE OR REPLACE VIEW 	HSC_PACIENTE_INTERNADO_V2
AS
SELECT 	a.nr_atendimento,
	a.nr_atend_original,
        a.dt_entrada,
        a.cd_pessoa_fisica,
        a.cd_procedencia,
        a.ie_tipo_atendimento,
        a.cd_medico_resp,
        a.dt_alta,
	a.cd_motivo_alta,
	a.dt_alta_interno,
	a.ie_permite_visita,
	a.ie_clinica,
	a.nr_seq_classificacao,
        b.dt_entrada_unidade,
        b.dt_saida_unidade,
	b.dt_saida_interno,
        b.cd_setor_atendimento,
        b.cd_unidade_basica,
        b.cd_unidade_compl,
        (b.cd_unidade_basica ||' '||b.cd_unidade_compl) cd_unidade,
        b.cd_tipo_acomodacao cd_tipo_acomod_unid,
        c.cd_tipo_acomodacao cd_tipo_acomod_conv,
	b.nr_seq_interno nr_seq_atepacu,	
        s.ds_setor_atendimento,
        m.nm_pessoa_fisica nm_medico,
	p.nm_pessoa_fisica nm_paciente,
	p.nr_prontuario,
	p.dt_nascimento,
	p.cd_religiao,
	c.cd_convenio,
	c.cd_categoria,
	c.cd_usuario_convenio,
	c.nr_doc_convenio,
	v.ds_convenio,
	t.ds_categoria,
	s.cd_classif_setor,
	a.ie_carater_inter_sus,
	a.nr_seq_indicacao,
	a.cd_estabelecimento,
	a.nr_seq_tipo_acidente,
	a.ie_paciente_isolado,
	a.dt_saida_real,
	a.nm_usuario_saida
from  	atend_paciente_unidade b,
	atendimento_paciente a,
	setor_atendimento s,
	pessoa_fisica m,
	pessoa_fisica p,
	atend_categoria_convenio c,
	categoria_convenio t,
	convenio v
where (a.nr_atendimento = b.nr_atendimento)
  and (c.nr_atendimento = b.nr_atendimento)
  and (b.cd_setor_atendimento  = s.cd_setor_atendimento)
  and (a.cd_medico_resp	= m.cd_pessoa_fisica)
  and (a.cd_pessoa_fisica	= p.cd_pessoa_fisica)
  and (c.nr_seq_interno	= obter_atecaco_atendimento(a.nr_atendimento))
  and (c.cd_convenio           = v.cd_convenio)
  and (c.cd_convenio           = t.cd_convenio)
  and (c.cd_categoria          = t.cd_categoria)
 /
