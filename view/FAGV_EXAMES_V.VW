CREATE OR REPLACE VIEW FAGV_EXAMES_V
AS select 	a.nr_atendimento,
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,100) nm_paciente,
	substr(OBTER_SEXO_PF(a.cd_pessoa_fisica,'D'),1,100) ds_sexo,
	substr(OBTER_PRONTUARIO_PACIENTE(a.cd_pessoa_fisica),1,10) nr_prontuario,
	OBTER_DATA_NASCTO_PF(a.cd_pessoa_fisica) dt_nascimento,
	'('||substr(obter_telefone_pf(a.cd_pessoa_fisica,'9'),1,2)||') '||substr(obter_telefone_pf(a.cd_pessoa_fisica,'9'),3,4)||'-'||substr(obter_telefone_pf(a.cd_pessoa_fisica,'9'),7,4) nr_telefone_celular,
	OBTER_IDADE_PF(a.cd_pessoa_fisica,sysdate,'A') ds_anos,
	substr(OBTER_CONVENIO_ATENDIMENTO(a.nr_atendimento),1,5) cd_convenio,
                substr(OBTER_NOME_CONVENIO(OBTER_CONVENIO_ATENDIMENTO(a.nr_atendimento)),1,100)  ds_convenio,
	d.dt_entrada_unidade,
	substr(obter_setor_atepacu(obter_atepacu_paciente(a.nr_atendimento, 'P'),1),1,100) ds_setor_entrada,
	--substr(OBTER_PROC_PRINCIPAL(A.NR_ATENDIMENTO, OBTER_CONVENIO_ATENDIMENTO(a.nr_atendimento), A.IE_TIPO_ATENDIMENTO,0,'D'),1,240) DS_PROC_PRINCIPAL,
	obter_Select_concatenado_bv
		('select  substr(obter_descricao_procedimento(c.cd_procedimento, c.ie_origem_proced),1,254)
		from	  prescr_medica b,
			  prescr_procedimento c
		where	  b.nr_prescricao = c.nr_prescricao
		and	  b.nr_prescricao = (select max(d.nr_prescricao)
			  		     from   prescr_medica d
					     where  d.nr_atendimento = :nr_atendimento)
		and	  b.nr_atendimento = :nr_atendimento', 'nr_atendimento='||a.nr_atendimento, ', ') DS_PROC_PRINCIPAL,
	substr(OBTER_CODIGO_USUARIO_CONV(a.nr_atendimento),1,30) cd_usuario_convenio,
	substr(OBTER_PLANO_ATENDIMENTO(a.nr_atendimento,'D'),1,200) ds_plano,      
	substr(OBTER_GUIA_CONVENIO(a.nr_atendimento),1,20) nr_guia_atend,
	a.ds_senha,
	substr(OBTER_CLINICA_ATENDIMENTO(a.ie_clinica),1,100)  ds_clinica,
	S.NM_UNIDADE_BASICA || ' ' ||S.NM_UNIDADE_COMPL  nm_unidade,
	s.nm_unidade_basica,
	s.nm_unidade_compl,
	substr(obter_compl_pf(a.cd_pessoa_fisica, 1,'ES'),1,255) ds_endereco,
	nvl(obter_compl_pf(a.cd_pessoa_fisica, 3,'N'), 'mesmo') nm_responsavel,
	nvl(obter_compl_pf(a.cd_pessoa_fisica, 3,'ES'),'mesmo') ds_end_responsavel,
	substr(obter_compl_pf(p.cd_pessoa_fisica, 1, 'CI'),1,254) ds_cidade,
	to_date((select obter_dia_entrega_proc(trunc(b.dt_entrada),c.cd_procedimento, c.ie_origem_proced) dt_resultado
	 from 	prescr_medica a,
				atendimento_paciente b,
				prescr_procedimento c
			where 	b.nr_atendimento = d.nr_atendimento
			and 	a.nr_prescricao = (	select 	max(x.nr_prescricao)
							from 	prescr_medica x,
								prescr_procedimento y
							where 	x.nr_atendimento = d.nr_atendimento
							and 	x.nr_prescricao = y.nr_prescricao
							and 	y.dt_resultado is not null )
	and a.nr_atendimento = b.nr_atendimento
	and c.nr_prescricao = a.nr_prescricao
	and c.dt_resultado is not null
	and rownum <= 1),'dd/mm/yyyy') DT_RESULTADO
from 	atendimento_paciente a,
	pessoa_fisica p,
	atend_paciente_unidade d,
	setor_atendimento s
where 	p.cd_pessoa_fisica = a.cd_pessoa_fisica
and 	d.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento, 'A')
and	(d.cd_setor_atendimento	= s.cd_setor_atendimento);
/