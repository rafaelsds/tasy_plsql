CREATE OR REPLACE VIEW REG_TC_EVIDENCE_V AS 
select	x.nr_seq_pr,
	x.nr_seq_tc,
	y.nr_seq_tc_item,
	x.nr_sequencia nr_seq_tc_evidence,
	y.nr_sequencia nr_seq_evidence_item,
	y.ds_item_description ds_evidence_item,
	obter_desc_expressao_idioma(f.cd_exp_funcao, f.ds_funcao, 5) ds_function,
	z.nr_seq_service_order,
	decode(y.ie_result, 'P', 'Passed', 'B', 'Blocked', 'F', 'Failed') ie_result,
	obter_valor_dominio_idioma(7676,	(	select	q.ie_severidade
							from	man_ordem_serv_analise_vv q
							where	q.nr_seq_ordem_serv = z.nr_seq_service_order
						), 5) ie_severidade,
	decode((	select	r.ie_status_ordem
			from	man_ordem_servico r 
			where	r.nr_sequencia = z.nr_seq_service_order
		), 1, 'Open', 2, 'In progress', 3, 'Closed') ie_status_ordem,
	(	select	max(t.nr_sequencia)
		from	man_doc_erro t
		where	t.nr_seq_ordem = z.nr_seq_service_order
	) nr_seq_defect,
	x.ds_version,
	y.dt_execution,
	y.nr_seq_ciclo,
	y.ds_item_description || decode(ds_funcao, null, '', '(') || obter_desc_expressao_idioma(f.cd_exp_funcao, f.ds_funcao, 5) || decode(ds_funcao, null, '', ')') ds_tc_item,
	y.ds_result ds_resultado,
	y.ds_exec_result,    
	obter_os_reg_evidence_item(y.nr_sequencia) ds_os_falha
from	reg_tc_pendencies x,
	reg_tc_evidence_item y,
	reg_tc_so_pendencies z,
	funcao f
where	y.nr_seq_ect = x.nr_sequencia
and	f.cd_funcao (+) = x.cd_funcao
and	y.nr_sequencia = z.nr_seq_ev_item (+)
and 	exists (	select	1
			from	reg_product_requirement_v t
			where	t.nr_seq_product_requirement = x.nr_seq_pr
		);
/