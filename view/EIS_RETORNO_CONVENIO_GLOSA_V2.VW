
create or replace
view eis_retorno_convenio_glosa_v2 as
select	substr(obter_nome_convenio(b.cd_convenio_parametro),1,255) ds_convenio,
	b.cd_convenio_parametro cd_convenio,
	null ds_tipo_convenio,
	null ie_tipo_convenio,
	trunc(c.dt_retorno) dt_referencia,
	trunc(c.dt_baixa_cr) dt_baixa_cr,
	substr(obter_empresa_estab(b.cd_estabelecimento),1,255) cd_empresa,
	e.ie_tipo_atendimento,
	substr(obter_valor_dominio(12,ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	f.cd_categoria,
	(substr(obter_nome_convenio(b.cd_convenio_parametro),1,150) || ' - ' || f.ds_categoria) ds_categoria,
	g.cd_motivo_glosa,
	nvl(h.ds_motivo_glosa,'N�o Informado') ds_motivo_glosa,
	g.cd_setor_atendimento,
	g.cd_setor_responsavel,
	substr(obter_dados_setor(g.cd_setor_atendimento,'DS'),1,255) ds_setor_atendimento,
	substr(obter_dados_setor(g.cd_setor_responsavel,'DS'),1,255) ds_setor_responsavel,
	to_number(obter_dados_ret_movto_glosa(g.nr_sequencia, 6)) vl_cobrado_unit,
	g.vl_cobrado vl_cobrado,
	to_number(obter_dados_ret_movto_glosa(g.nr_sequencia, 7)) vl_pago,
	nvl(vl_pago_digitado,to_number(obter_dados_ret_movto_glosa(g.nr_sequencia, 3))) vl_total_pago,
	g.vl_repasse_item vl_repasse_item,
	g.vl_glosa
from 	motivo_glosa h,
	categoria_convenio f,
	atendimento_paciente e,
	conta_paciente b,
	convenio_retorno_glosa g,
	convenio_retorno_item a,
	convenio_retorno c
where	a.nr_interno_conta 	= b.nr_interno_conta
and	c.cd_convenio		= b.cd_convenio_parametro
and	a.nr_Seq_retorno 		= c.nr_sequencia
and	b.nr_atendimento		= e.nr_atendimento
and	c.cd_convenio		= f.cd_convenio
and	b.cd_categoria_parametro 	= f.cd_categoria
and	g.nr_seq_ret_item		= a.nr_sequencia
and	g.cd_motivo_glosa		= h.cd_motivo_glosa(+)
union all
select	null ds_convenio,
	null cd_convenio,
	substr(OBTER_VALOR_DOMINIO(11,d.ie_tipo_convenio),1,255) ds_tipo_convenio,
	d.ie_tipo_convenio ie_tipo_convenio,
	trunc(c.dt_retorno) dt_referencia,
	trunc(c.dt_baixa_cr) dt_baixa_cr,
	substr(obter_empresa_estab(b.cd_estabelecimento),1,255) cd_empresa,
	e.ie_tipo_atendimento,
	substr(obter_valor_dominio(12,ie_tipo_atendimento),1,255) ds_tipo_atendimento,
	f.cd_categoria,
	(substr(obter_nome_convenio(b.cd_convenio_parametro),1,150) || ' - ' || f.ds_categoria) ds_categoria,
	g.cd_motivo_glosa,
	nvl(h.ds_motivo_glosa,'N�o Informado') ds_motivo_glosa,
	g.cd_setor_atendimento,
	g.cd_setor_responsavel,
	substr(obter_dados_setor(g.cd_setor_atendimento,'DS'),1,255) ds_setor_atendimento,
	substr(obter_dados_setor(g.cd_setor_responsavel,'DS'),1,255) ds_setor_responsavel,
	to_number(obter_dados_ret_movto_glosa(g.nr_sequencia, 6)) vl_cobrado_unit,
	g.vl_cobrado vl_cobrado,
	to_number(obter_dados_ret_movto_glosa(g.nr_sequencia, 7)) vl_pago,
	nvl(vl_pago_digitado,to_number(obter_dados_ret_movto_glosa(g.nr_sequencia, 3))) vl_total_pago,
	g.vl_repasse_item vl_repasse_item,
	g.vl_glosa
from 	motivo_glosa h,
	categoria_convenio f,
	convenio d,
	atendimento_paciente e,
	conta_paciente b,
	convenio_retorno_glosa g,
	convenio_retorno_item a,
	convenio_retorno c
where	b.cd_convenio_parametro	= d.cd_convenio
and	c.cd_convenio		= d.cd_convenio
and	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_Seq_retorno 		= c.nr_sequencia
and	b.nr_atendimento		= e.nr_atendimento
and	c.cd_convenio		= f.cd_convenio
and	b.cd_categoria_parametro 	= f.cd_categoria
and	g.nr_seq_ret_item		= a.nr_sequencia
and	g.cd_motivo_glosa		= h.cd_motivo_glosa(+);
/