create or replace view
HDM_INDIC_CARE_SUSPENSION_V as
/* Esta view ir� conter a jun��o de todas as tabelas de fato e dimens�o do modelo de encaminhamentos (susp atend) */
SELECT	/* Dados dia */
	c.nr_day_of_week,
	c.nr_day,
	c.si_holiday,
	c.si_weekend,
	c.dt_complete_date dt_day,
	c.ds_day_of_week,
	/* Dados m�s */
	b.nr_year,       
	b.nr_month,
	b.nr_semester,
	b.nr_quarter,  
	b.dt_complete_date dt_month,
	/* Dados grupo paciente */ 
	a.nr_seq_patient_group,
	d.si_sex,
	d.ds_sex,
	d.nm_age_range,
	d.ds_participant,
	d.si_participant,
	/* Dados programa */
	e.nr_sequencia nr_seq_dm_program,
	e.nr_seq_program nr_seq_program, 
	e.nm_program,
	e.nm_program_module nm_program_module,
	/* Dados detalhes suspens�o */
	k.si_status,
	k.ds_status,
	k.si_reason,
	k.ds_reason,
	k.si_reason_kind,
	k.ds_reason_kind,
	/* Dados fato */
	a.nr_days_duration,
	(a.nr_days_duration / COUNT(1) OVER (PARTITION BY a.nr_sequencia)) qt_day_by_fact,
	a.nr_sequencia nr_seq_fact,
	a.nr_dif_person,
	a.ds_unique
FROM	hdm_indic_dm_program e,
	hdm_indic_ft_suspension_pr f,
	hdm_indic_dm_susp_details k,
	hdm_indic_dm_patient_group d,
	hdm_indic_dm_month b,
	hdm_indic_dm_day c,
	hdm_indic_ft_suspension a,
	pessoa_fisica pf
WHERE	1 = 1
AND	f.nr_seq_dimension = e.nr_sequencia
AND	a.nr_sequencia = f.nr_seq_fact
AND	a.nr_seq_susp_detail = k.nr_sequencia
AND	a.nr_seq_patient_group = d.nr_sequencia
AND	b.nr_sequencia = a.nr_seq_month
AND	c.nr_sequencia = a.nr_seq_day
AND	pf.cd_pessoa_fisica = a.nr_dif_person(+);
/