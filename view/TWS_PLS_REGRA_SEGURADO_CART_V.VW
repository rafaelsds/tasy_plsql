CREATE OR replace VIEW TWS_PLS_REGRA_SEGURADO_CART_V AS 
select  nr_sequencia,
		dt_atualizacao,
		cd_estabelecimento,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		ie_renovacao,
		nr_via_inicial,
		nr_via_final,
		nr_seq_contrato,
		nr_seq_plano,
		vl_via_adicional,
		nr_seq_intercambio
from    pls_regra_segurado_cart;
/