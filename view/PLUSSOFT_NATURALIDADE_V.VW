create or replace view plussoft_naturalidade_v
as
	select cd_municipio_ibge id_naturalidade, 
	       ds_municipio ds_naturalidade 
	from   sus_municipio 
	order by 2;
/
