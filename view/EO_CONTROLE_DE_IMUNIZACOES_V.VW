create or replace
view eo_controle_de_imunizacoes_v
as
select	1 nr_seq,
	x.nr_atendimento,
	x.cd_pessoa_fisica,
	substr(nvl(obter_nome_pf(x.cd_pessoa_fisica),obter_dados_atendimento(x.nr_atendimento,'NP')),1,100) nm_pf,
	substr(nvl(obter_idade_pf(x.cd_pessoa_fisica,sysdate,'A'),obter_idade_pf(obter_dados_atendimento(x.nr_atendimento,'CP'),sysdate,'A')),1,3) ds_idade_pf,
	substr(nvl(obter_faixa_etaria_pf(x.cd_pessoa_fisica),obter_faixa_etaria_pf(obter_dados_atendimento(x.nr_atendimento,'CP'))),1,10) ds_faixa_etaria,
	substr(nvl(obter_sexo_pf(x.cd_pessoa_fisica,'C'),obter_sexo_pf(obter_dados_atendimento(x.nr_atendimento,'CP'),'C')),1,15) ie_sexo,
	substr(nvl(obter_sexo_pf(x.cd_pessoa_fisica,'D'),obter_sexo_pf(obter_dados_atendimento(x.nr_atendimento,'CP'),'D')),1,15) ds_sexo,	
	x.nr_seq_vacina,
	substr(obter_desc_vacina(x.nr_seq_vacina),1,255) ds_vacina,	
	x.ie_dose,
	substr(obter_valor_dominio(1018, x.ie_dose),1,150) ds_dose,
	x.cd_procedimento,
	substr(nvl(obter_descricao_procedimento(x.cd_procedimento,x.ie_origem_proced),'N�o informado'),1,100) ds_procedimento,
	x.ie_via_aplicacao,
	substr(nvl(obter_via_aplicacao(x.ie_via_aplicacao,'D'),'N�o informado'),1,150) ds_via_aplicacao,
	x.cd_profissional,
	substr(obter_nome_pf(x.cd_profissional),1,100) ds_profissional,
	x.cd_setor_atendimento,
	substr(nvl(obter_nome_setor(x.cd_setor_atendimento),'N�o informado'),1,100) nm_setor_atendimento,
	x.nr_seq_topografia,
	substr(nvl(obter_desc_topografia(x.nr_seq_topografia),'N�o informado'),1,100) ds_topografia,
	x.ie_executado,
	decode(nvl(x.ie_executado,'N'),'N','N�o executado', 'Executado') ds_executado,	
	x.qt_dose,
	x.dt_vacina,
	x.dt_vacina dt_filtro
from  	paciente_vacina x
where	x.dt_vacina is not null
order by	substr(nvl(obter_nome_pf(x.cd_pessoa_fisica),obter_dados_atendimento(x.nr_atendimento,'NP')),1,100);
/
