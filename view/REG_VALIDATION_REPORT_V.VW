CREATE OR REPLACE VIEW REG_VALIDATION_REPORT_V AS 
SELECT  P.DS_VERSION ,
        V.Crs_Id ,
        V.Crs_Id
        ||' - '
        ||V.Ds_Customer_Requirement DS_CRS ,
        V.Ds_Crs_Title ds_title ,
        V.Ct_Validation_Id ,
        v.ds_tc ,
        V.ds_titulo ,
        V.Nr_Customer_Requirement nr_seq_cr ,
        V.Nr_Seq_Caso_Teste nr_seq_tc ,
        V.Ds_Titulo_Formatado ,
        V.Ds_Pre_Condicao ,
        P.nr_sequencia nr_seq_pend ,
        I.nr_sequencia nr_seq_pend_item ,
        I.Nr_Seq_Cycle ,
        null pr_aderencia ,
        V.ds_funcionalidade ,
        I.Ds_Item_Description ,
        V.Nr_Ordenacao ,
        I.Ds_Evidence_Path ,
        I.Ds_Item_Result ,
        I.Dt_Execution ,
        I.Ie_Severity ,
        DECODE(I.Ie_Severity,1,'S5',2,'S4',3,'S3',4,'S4',5,'S5') ds_severity ,
        DECODE(i.IE_RESULT,'P','Passed','F','Failed','B','Blocked') ds_result ,
        OBTER_OS_REG_VALIDATION_ITEM(O.Nr_Seq_Validation_Item) ds_os_falha ,
        null DS_ULTIMO_CICLO
FROM    Reg_Test_Case_Validation_V v ,
        Reg_Validation_Pendencies p ,
        Reg_Validation_Pend_Item i ,
        Reg_Validation_So_Pend o
WHERE   V.Nr_Seq_Caso_Teste = P.Nr_Seq_Tc
AND     P.Nr_Sequencia = I.Nr_Seq_Pend_Tc
AND     I.Nr_Seq_Pend_Item = V.Nr_Seq_Acao_Teste
AND     I.Nr_Sequencia = O.Nr_Seq_Validation_Item (+);
/
