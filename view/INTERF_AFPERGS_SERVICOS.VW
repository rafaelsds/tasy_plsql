create or replace view INTERF_AFPERGS_SERVICOS as
select	1 tp_registro,
	0 ie_registro,
	'Cabe�alho' ds_registro,
	b.nr_seq_protocolo,
	to_char(sysdate,'DDMMYYYY') dt_emissao,
	0 cd_profissional,
	to_char(b.dt_mesano_referencia,'MMYYYY') dt_mesano_referencia,
	substr(obter_cgc_estabelecimento(max(b.cd_estabelecimento)),1,14) cd_cgc,
	substr(obter_razao_social(obter_cgc_estabelecimento(max(b.cd_estabelecimento))),1,40) ds_razao_social,
	0 nr_registro,
	''	dt_atendimento,
	'' 	cd_usuario_convenio,
	''	cd_item,
	''	cd_categoria,
	0	tp_tabela,
	0	vl_total_item,
	0	qt_total_reg,
	0	vl_total_protocolo
from	protocolo_convenio b,
	w_interf_conta_cab a
where	a.nr_seq_protocolo = b.nr_seq_protocolo
group by b.nr_seq_protocolo,
	to_char(b.dt_mesano_referencia,'MMYYYY') 
union all
select	2 tp_registro,
	1 ie_registro,
	'Movimento' ds_registro,
	a.nr_seq_protocolo,
	''	dt_emissao,
	0	cd_profissional,
	'' 	dt_mesano_referencia,
	'' 	cd_cgc,
	'' 	ds_razao_social,
	to_number(lpad(rownum,4,0)) nr_registro,
	substr(to_char(obter_data_entrada(a.nr_atendimento),'DDMMYYYY'),1,8) dt_atendimento,
	max(to_char(substr(OBTER_CD_USUARIO_CONV_GLOSA(Obter_Convenio_Atendimento(a.nr_atendimento),a.NR_ATENDIMENTO),1,10))) cd_usuario_convenio,
	substr(replace(nvl(nvl(Obter_Codigo_Brasindice(1,C.CD_ITEM,sysdate,obter_conv_conta(a.nr_interno_conta)),
	(Obter_Codigo_Item_Conv(obter_conv_conta(a.nr_interno_conta),ie_tipo_item,cd_item,ie_origem_proced,'N',0,null,0,a.nr_atendimento))),c.cd_item),';',''),1,20)  cd_item,		
	nvl(a.CD_CATEGORIA_CONVENIO,'0') cd_categoria,
	decode(c.ie_origem_proced,1,1,2) tp_tabela,            
	to_number(substr(somente_numero(campo_mascara_virgula(c.vl_total_item)),1,20))vl_total_item,
	0	qt_total_reg,
	0	vl_total_protocolo
from	w_interf_conta_cab a,
	w_interf_conta_item c
where	c.nr_interno_conta = a.nr_interno_conta
group by 	a.nr_seq_protocolo,
	substr(to_char(obter_data_entrada(a.nr_atendimento),'DDMMYYYY'),1,8),
	substr(replace(nvl(nvl(Obter_Codigo_Brasindice(1,C.CD_ITEM,sysdate,obter_conv_conta(a.nr_interno_conta)),
	(Obter_Codigo_Item_Conv(obter_conv_conta(a.nr_interno_conta),ie_tipo_item,cd_item,ie_origem_proced,'N',0,null,0,a.nr_atendimento))),c.cd_item),';',''),1,20) ,
	nvl(a.CD_CATEGORIA_CONVENIO,'0') ,
	to_number(lpad(rownum,4,0)),
	decode(c.ie_origem_proced,1,1,2) ,            
	to_number(substr(somente_numero(campo_mascara_virgula(c.vl_total_item)),1,20))
union all
select	3 tp_registro,
	2 ie_registro,
	'Rodap�' ds_registro,
	a.nr_seq_protocolo,
	''	dt_emissao,
	0	cd_profissional,
	'' 	dt_mesano_referencia,
	'' 	cd_cgc,
	'' 	ds_razao_social,
	0	nr_registro,
	''	 dt_atendimento,
	''	cd_usuario_convenio,
	''	cd_item,
	''	cd_categoria,
	0	tp_tabela,            
	0	vl_total_item,
	count(*) qt_total_reg,
	to_number(replace(substr(campo_mascara_virgula(sum(c.vl_total_item)),1,50),',','')) vl_total_protocolo
from	w_interf_conta_cab a,
	w_interf_conta_item c
where	c.nr_interno_conta = a.nr_interno_conta
group by a.nr_seq_protocolo;
/

