CREATE OR REPLACE VIEW ea_hist_paciente_unidade_v
AS
SELECT --Dados Histórico
	x.dt_historico dt_inicio,
	x.dt_fim_historico dt_fim,
	x.nr_atendimento,
	c.dt_entrada,
	SUBSTR(obter_nome_pf(c.cd_pessoa_fisica),1,100) nm_paciente,
	x.ie_status_unidade,
	SUBSTR(obter_valor_dominio(82,x.ie_status_unidade),1,200) ds_status_unidade,
	x.cd_motivo_interdicao,
	SUBSTR(obter_desc_mot_inter(x.cd_motivo_interdicao),1,200) ds_motiv_interditado,
	x.cd_motivo_manutencao,
	SUBSTR(obter_desc_mot_manut(x.cd_motivo_manutencao),1,255) ds_motiv_manutencao,
	x.ds_motivo_reserva,
	x.ie_situacao,
	x.ie_temporario,
	x.nr_seq_unidade,
	x.nm_usuario_nrec,
	x.nm_usuario,
	--Dados Unidade/Quarto
	a.nr_seq_interno,
	a.cd_unidade_basica,
	a.cd_unidade_compl,
	--Dados Setor
	b.cd_setor_atendimento,
	b.ds_setor_atendimento,
	b.nm_unidade_basica nm_unid_basic_setor,
	b.nm_unidade_compl nm_unid_compl_setor
FROM  	atendimento_paciente c,
	setor_atendimento b,
	unidade_atendimento a,
	unidade_atend_hist x
WHERE  1 = 1
AND  	a.cd_setor_atendimento = b.cd_setor_atendimento
AND  	x.nr_atendimento = c.nr_atendimento(+)
AND  	x.nr_seq_unidade = a.nr_seq_interno
ORDER BY SUBSTR(obter_nome_pf(c.cd_pessoa_fisica),1,100)
/
