CREATE OR REPLACE VIEW NASCIMENTOS_VALIDADOR_V AS
SELECT 	b.dt_nascimento, a.nr_atendimento,
	a.cd_pessoa_fisica,
	b.nr_sequencia NR_SEQ_NASCIMENTO,
	obter_dados_cat_clues(d.cd_internacional,'CD_ESTADO') CEDOCVE,
	b.nr_dnv FOLIO,	
	(CASE WHEN (TO_NUMBER(TO_CHAR(b.dt_nascimento,'yyyy')) < 2010) THEN 'ANTERIOR'
	     WHEN (TO_NUMBER(TO_CHAR(b.dt_nascimento,'yyyy')) BETWEEN 2010 AND 2014) THEN '2010'
	     ELSE '2015'
	END) TIPO_FORMATO,
	z.ds_given_name C_04,
	z.ds_family_name C_05,
	z.ds_component_name_1 C_06,
	pf.cd_curp C_07,
	obter_dados_cat_entidade(pf.cd_pessoa_fisica,'CD_ENTIDADE') C_08,
	obter_dados_cat_municipio(pf.cd_municipio_ibge,'CD_CAT_MUNICIPIO') C_09,
	pf.dt_nascimento C_10,
	obter_idade(pf.dt_nascimento,SYSDATE,'A') C_11,
	DECODE(pf.nr_seq_cor_pele,NULL,'2','1') C_12,
	DECODE(pf.nr_seq_lingua_indigena,NULL,'2','1') C_13,
	SUBSTR(obter_dados_cat_lingua_indig(pf.nr_seq_lingua_indigena,'CD_LINGUA_INDIGENA_MF'),1,4) C_14,
	DECODE(pf.ie_estado_civil,'1','12','2','11','3','13','4','16','5','14','6','16','7','15','9','88','99') C_15,
	LPAD(NVL(cpf.cd_tipo_logradouro,'99'),2,'0') C_16,
	cpf.ds_endereco C_17,
	cpf.nr_endereco C_18,
	'' C_19,
	substr(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'TIPO_BAIRRO','C'),'99'),1,2) C_20,
	substr(NVL(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'TIPO_BAIRRO','D'),'SIN INFORMACIA�N'),1,60) C_21,
	SUBSTR(trim(NVL(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'CODIGO_POSTAL','D'),'99999')),1,5) C_22,
	substr(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'ESTADO_PROVINCI','C'),'99'),1,2) C_23,
	substr(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'MUNICIPIO','C'),'999'),1,3) C_24,
	substr(nvl(get_info_end_endereco(cpf.nr_seq_pessoa_endereco,'LOCALIDADE_AREA','C'),'9999'),1,4) C_25,
	cpf.nr_telefone C_26,
	NVL(NVL(ag.qt_gestacoes,hsm.qt_gestacoes),0)+1 C_27,
	NVL(NVL(ag.qt_filhos_mortos,hsm.qt_filhos_mortos),'99') C_28,
	NVL(NVL(par.qt_filhos_vivos,ag.qt_filhos_vivos),hsm.qt_filhos_vivos) C_29,
	NVL(hsm.qt_filhos_vivos,'99') C_30,
	par.ie_cond_ult_nasc C_31,
	DECODE(par.ie_cond_ult_nasc,'2','0','3','0',DECODE(par.ie_ult_filho_vivo,'S','1','N','2','9')) C_32,
	'99/99/9999' C_33,
	b.nr_sequencia C_34,
	par.ie_pre_natal C_35,
	DECODE(par.ie_pre_natal,'N','0',(CASE	WHEN ((h.dt_prim_consulta-h.dt_ultima_menstruacao) < 90) THEN '1'
					WHEN ((h.dt_prim_consulta-h.dt_ultima_menstruacao) BETWEEN 91 AND 180) THEN '2'
					WHEN ((h.dt_prim_consulta-h.dt_ultima_menstruacao) BETWEEN 181 AND 270) THEN '3'
					ELSE '9' END)) C_36,
	DECODE(par.ie_pre_natal,'N','0',par.qt_consultas) C_37,
	DECODE(dco.nr_declaracao,NULL,'2','1') C_38,
	NVL(dco.nr_declaracao,'999999999') C_39,
	obter_convenio_atend_mx(a.nr_atendimento,1,'CD_DER_NACIMIENTO') C_40,
	pf.nr_spss C_41,
	DECODE(NVL(obter_convenio_atend_mx(a.nr_atendimento,1,'CD_DER_NACIMIENTO'),'01'),'01','00', obter_convenio_atend_mx(a.nr_atendimento,2,'CD_DER_NACIMIENTO')) C_42,
	DECODE(pf.ie_grau_instrucao,'1','01','2','03','3','05','4','07','5','10','6','10','7','02','8','05','9','06','10','03','11','01','12','10','13','10','14','88','15','08','99') C_43,
	SUBSTR(NVL(obter_desc_cargo(pf.cd_cargo),'SIN INFORMACION'),1,40) C_44,
	DECODE((SELECT w.cd_externo FROM cargo w WHERE w.cd_cargo = pf.cd_cargo),'01','0','02','0','03','0','04','0',(SELECT NVL(w.cd_externo,'99') FROM cargo w WHERE w.cd_cargo = pf.cd_cargo)) C_45,
	DECODE(pf.cd_cargo,NULL,'2','1') C_46, --926411
	b.dt_nascimento FECH_NACH,
	b.dt_nascimento HORA_NACH,
	b.ie_sexo SEXOH,
	NVL(b.qt_sem_ig_total, b.qt_sem_ig) GESTACH,
	b.qt_altura TALLAH,
	NVL(b.qt_peso_sala_parto, b.qt_peso) PESOH,
	NVL(b.qt_apgar_quinto_min,'99') APGARH,
	NVL(es.qt_pontuacao,'99') SILVERMAN,
	DECODE(NVL(vtr.ie_tipo_vacina,'N'),'B','1','2') BCG,
	DECODE(NVL(vtr.ie_tipo_vacina,'N'),'H','1','2') HEP_B,
	DECODE(b.ie_vitamina_a,'S','1','2') VIT_A,
	DECODE(b.ie_vitamina_k,'S','1','2') VIT_K,
	'0' TAM_MET,
	DECODE(NVL(vtr.ie_tipo_vacina,'N'),'A','1','2') TAM_AUD,
	DECODE(NVL(par.qt_feto,'1'),'1','1','2','2','3') PRODUCTO,
	DECODE(obter_inf_doenca_cid_atend(b.nr_atend_rn,1,'MAL'),'ANMRN','S','N') IE_MALFORMACAO_1,
	nvl(obter_inf_doenca_cid_atend(b.nr_atend_rn,1,'CD'),'0000') CVE_CIE,
	SUBSTR(nvl(obter_inf_doenca_cid_atend(b.nr_atend_rn,1,'DS'),'NINGUNA APARENTE'),1,40) ACELRN,
	DECODE(obter_inf_doenca_cid_atend(b.nr_atend_rn,2,'MAL'),'ANMRN','S','N') IE_MALFORMACAO_2,
	nvl(obter_inf_doenca_cid_atend(b.nr_atend_rn,2,'CD'),'7777') CVE_CIE2,
	SUBSTR(nvl(obter_inf_doenca_cid_atend(b.nr_atend_rn,2,'DS'),'NO APLICA'),1,40) ACELRN2,
	par.ie_parto_normal,
	par.ie_parto_forceps,
	par.ie_parto_cesaria,
	DECODE(par.ie_parto_forceps,'S','1','2') FORCEPS,
	SUBSTR(par.ds_observacao,1,25) ESPECIFIQUE,
	NVL(d.ie_tipo_inst_saude,'99') INST_NAC,
	obter_dados_cat_clues(d.cd_internacional,'DS_ESTAB_SAUDE') UNIMED,
	obter_dados_cat_clues(d.cd_internacional,'CD_CLUES') CLUES,
	obter_dados_cat_prof_parto(par.nr_seq_prof_parto,'CD_ATEND_PARTO') ATENDIO,
	SUBSTR((SELECT obter_nome_pf(x.cd_pessoa_fisica) FROM parto_participante x WHERE nr_atendimento = a.nr_atendimento),1,25) ATEN_OTRO,
	obter_dados_cat_tipo_via(d.nr_seq_tipo_logradouro,'CD_TIPO_VIA') TIPOVIAL_NAC,
	SUBSTR(d.ds_endereco||' '||d.nr_endereco||' '||d.ds_bairro,1,80) CALLE_NAC,
	SUBSTR(NVL(d.nr_endereco,'9999999999'),1,10) NUMEXT_NAC,
	SUBSTR(d.ds_complemento,1,5) NUMINT_NAC,
	obter_dados_cat_tipo_assent(d.nr_seq_tipo_asen,'CD_TIPO_ASEN') TIPOASEN_NAC,
	obter_dados_cat_tipo_assent(d.nr_seq_tipo_asen,'NM_ASSENTAMENTO') NOMASEN_NAC,
	obter_dados_cat_clues(d.cd_internacional,'CD_POSTAL') CODPOS_NAC,
	obter_dados_cat_clues(d.cd_internacional,'CD_ESTADO') ENT_NAC,
	obter_dados_cat_clues(d.cd_internacional,'CD_MUNICIPIO') MPO_NAC,
	obter_dados_cat_clues(d.cd_internacional,'CD_LOCALIDADE') LOC_NAC, 
	y.ds_given_name NOMBRE_C,
	y.ds_family_name PATERNOC,
	y.ds_component_name_1 MATERNOC,
	obter_dados_cat_cert_parto(par.nr_seq_cert_parto,'CD_CERT_PARTO') CERT_POR,
	SUBSTR(NVL(obter_nome_pf(b.cd_pediatra),'SIN INFORMACION'),1,25) OTROMEDICO,
	SUBSTR(NVL(b.cd_pediatra,'SIN INFORMACION'),1,20) CED_MEDC,
	SUBSTR(d.ds_razao_social,1,50) UNIMED_33_1,
	LPAD(NVL(obter_dados_cat_clues(d.cd_internacional,'CD_CLUES'),'99999999999'),11,'0') CLUES_33_2,
	NVL(obter_dados_cat_tipo_via(d.nr_seq_tipo_logradouro,'CD_TIPO_VIA'),'99') TIPOVIAL_CERT,
	SUBSTR(d.ds_endereco||' '||d.nr_endereco||' '||d.ds_bairro,1,80) CALLE_CERT,
	SUBSTR(NVL(d.nr_endereco,'9999999999'),1,10) NUMEXT_CERT,
	SUBSTR(d.ds_complemento,1,5) NUMINT_CERT,
	obter_dados_cat_tipo_assent(d.nr_seq_tipo_asen,'CD_TIPO_ASEN') TIPOASEN_CERT,
	obter_dados_cat_tipo_assent(d.nr_seq_tipo_asen,'NM_ASSENTAMENTO') NOMASEN_CERT,
	obter_dados_cat_clues(d.cd_internacional,'CD_POSTAL') CODPOS_CERT,
	obter_dados_cat_clues(d.cd_internacional,'CD_ESTADO') ENT_CERT,
	obter_dados_cat_clues(d.cd_internacional,'CD_MUNICIPIO') MPO_CERT,
	obter_dados_cat_clues(d.cd_internacional,'CD_LOCALIDADE') LOC_CERT,
	SUBSTR((d.nr_ddi_telefone||d.nr_ddd_telefone||d.nr_telefone),1,12) TEL_CERT,
	TO_CHAR(b.dt_atualizacao,'dd/mm/yyyy') FECH_CERT,
	'ADMIN' USER_ALTA,
	TO_CHAR(b.dt_entrega_dnv,'dd/mm/yyyy') FECH_ALTA,
	'' USER_CAMB, 
	b.dt_nascimento FECH_CAMB,
	NVL(obter_dados_cat_lugar_cert_nac(b.nr_seq_local_cert_nasc,'CD_LUG_CERT_NASC'),'9') IDCAPTURA,
	apm.nr_atendimento_mae NR_ATENDIMENTO_MAE,
	d.cd_cgc CD_PESSOA_JURIDICA,
	pf.cd_pessoa_fisica CD_PESSOA_FISICA_MAE,
	b.nr_atendimento NR_ATENDIMENTO_RN,
	ped.cd_pessoa_fisica cd_pediatra,
	a.cd_pessoa_fisica CD_PESSOA_FISICA_RN
FROM   	atendimento_paciente a,
	nascimento b,
	estabelecimento c,
	pessoa_juridica d,
	pessoa_fisica pf,
	compl_pessoa_fisica cpf,
	historico_saude_mulher hsm,
	atendimento_gravidez ag,
	parto par,
	med_pac_pre_natal h,
	declaracao_obito dco,
	person_name z,
	escala_silverman es,
	atend_vacina_teste avt,
	vacina_teste_rn vtr,
	atendimento_paciente apm,
	pessoa_fisica ped,
	person_name Y
WHERE	a.nr_atendimento 	= b.nr_atendimento
AND	b.ie_tipo_nascimento    = 10
AND	a.cd_estabelecimento	= c.cd_estabelecimento
AND	c.cd_cgc		= d.cd_cgc
AND	a.cd_pessoa_fisica	= pf.cd_pessoa_fisica
AND	a.cd_pessoa_fisica	= cpf.cd_pessoa_fisica
AND	cpf.ie_tipo_complemento (+)	= 1
AND	a.cd_pessoa_fisica 	= hsm.cd_pessoa_fisica(+)
AND	a.nr_atendimento 	= ag.nr_atendimento(+)
AND	a.nr_atendimento	= par.nr_atendimento(+)
AND	a.nr_atendimento	= h.nr_atendimento(+)
AND	a.nr_atendimento 	= dco.nr_atendimento(+) 
AND     pf.nr_seq_person_name	= z.nr_sequencia(+)
AND	a.nr_atendimento	= es.nr_atendimento(+)
AND	a.nr_atendimento	= avt.nr_atendimento(+)
AND	avt.nr_seq_teste_vacina	= vtr.nr_sequencia(+)
AND	a.nr_atendimento	= apm.nr_atendimento_mae(+)
AND	b.cd_pediatra		= ped.cd_pessoa_fisica(+)
AND 	ped.nr_seq_person_name	= y.nr_sequencia(+)
AND	y.ds_type  (+) = 'main'
AND	z.ds_type  (+) = 'main';
/