Create or replace
View setor_atendimento_v as
SELECT	cd_setor_atendimento,
		substr(obter_desc_expressao(cd_exp_setor_atend, ds_setor_atendimento),1,100) ds_setor_atendimento,
		cd_classif_setor,
		cd_estabelecimento_base,
		cd_estabelecimento,
		ie_situacao
FROM 	setor_atendimento;
/