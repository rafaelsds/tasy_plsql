create or replace 
view UFI_TITULO_RECEBER_V
as 
select	a.nr_titulo,
	a.dt_emissao,		
	a.cd_cgc,
	substr(rpad(b.ds_razao_social,20,' '),1,20) ds_razao_social,
	a.dt_pagamento_previsto,
	a.vl_saldo_titulo vl_saldo_titulo, 
	nvl(OBTER_VALOR_TRIB_TIT_REC(a.nr_titulo,'O'),0) vl_iss,
	nvl(OBTER_VALOR_TRIB_TIT_REC(a.nr_titulo,'IR'),0) vl_irrf,
	nvl(OBTER_VALOR_TRIB_TIT_REC(a.nr_titulo,'INSS'),0) vl_inss,
	nvl(OBTER_VALOR_TRIB_TIT_REC(a.nr_titulo,'COFINS'),0) vl_cofins,
	nvl(OBTER_VALOR_TRIB_TIT_REC(a.nr_titulo,'PIS'),0) vl_pis,	
	nvl(OBTER_VALOR_TRIB_TIT_REC(a.nr_titulo,'CSLL'),0) vl_csll,
	a.ds_observacao_titulo ds_observacao	
from	pessoa_juridica b,
	titulo_receber a
where	a.ie_situacao	<> '3'
and	a.cd_cgc	= b.cd_cgc;
/
