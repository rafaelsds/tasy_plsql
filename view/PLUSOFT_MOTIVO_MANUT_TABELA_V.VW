create or replace view plusoft_motivo_manut_tabela_v
as
	select	nr_sequencia,
		ds_motivo   
	from	pls_motivo_man_tab_preco
	where	ie_situacao = 'A'
	order by 1;
/
