create or replace
view EIS_GPI_PROJETO_ENCERRADO_V as
select	a.nr_sequencia,
	a.nm_projeto,
	a.nr_seq_tipo,
	a.cd_pf_gestor,
	a.cd_gestor_funcional,
	a.cd_setor_atendimento,
	a.cd_centro_custo,
	substr(obter_nome_pf(a.cd_pf_gestor),1,255) nm_pf_gestor,
	substr(obter_nome_pf(a.cd_gestor_funcional),1,255) nm_gestor_funcional,
	trunc(gpi_obter_dt_conclusao_proj(a.nr_sequencia, a.nr_seq_estagio)) dt_referencia,
	substr(obter_nome_setor(a.cd_setor_Atendimento),1,255) ds_setor_atendimento
from	gpi_estagio b,
	gpi_projeto a
where	a.nr_seq_estagio	= b.nr_sequencia
and	b.ie_tipo_estagio	= 'C';
/