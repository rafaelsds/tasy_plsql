create or replace
view hsl_pm_medico_area_atuacao_v as
select	a.*,
	c.ds_area_atuacao
from	area_atuacao_medica c,
	medico b,
	medico_area_atuacao a
where	a.cd_pessoa_fisica		= b.cd_pessoa_fisica
and	a.nr_seq_area_atuacao	= c.nr_sequencia;
/