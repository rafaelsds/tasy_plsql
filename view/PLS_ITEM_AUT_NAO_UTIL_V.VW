create or replace view pls_item_aut_nao_util_v as
select	'P'				ie_tipo,
	'Procedimento'			ds_tipo,
	null				nr_seq_material,
	null				ds_material,
	ca.ie_origem_proced		ie_origem_proced,
	ca.cd_procedimento		cd_procedimento,
	p.ds_procedimento		ds_procedimento,
	p.cd_procedimento || ' - ' ||
		p.ds_procedimento	ds_item,
        c.nr_seq_guia			nr_seq_guia,
	c.nr_sequencia			nr_seq_conta
from	pls_conta_v		c,
	table(pls_conta_autor_pck.obter_dados(c.nr_seq_guia,'P', c.cd_estabelecimento)) ca,
	procedimento		p
where	c.nr_seq_guia		is not null
and	ca.qt_autorizada	> 0
and	ca.qt_utilizada		= 0
and	p.ie_origem_proced	= ca.ie_origem_proced
and	p.cd_procedimento	= ca.cd_procedimento
union all
select	'P'				ie_tipo,
	'Procedimento'			ds_tipo,
	null				nr_seq_material,
	null				ds_material,
	ca.ie_origem_proced		ie_origem_proced,
	ca.cd_procedimento		cd_procedimento,
	proc.ds_procedimento		ds_procedimento,
	proc.cd_procedimento || ' - ' ||
		proc.ds_procedimento	ds_item,
	conta.nr_seq_guia_conv		nr_seq_guia,
	conta.nr_sequencia		nr_seq_conta	
from	pls_conta_imp conta,
	table(pls_conta_autor_pck.obter_dados(conta.nr_seq_guia_conv,'P', conta.cd_estabelecimento)) ca,
	procedimento proc
where	conta.nr_seq_guia_conv is not null
and	ca.qt_autorizada	> 0 
and	ca.qt_utilizada		= 0
and	proc.ie_origem_proced	= ca.ie_origem_proced
and	proc.cd_procedimento	= ca.cd_procedimento
union all
select	'M' 				ie_tipo,
	'Material' 			ds_tipo,
	m.nr_sequencia 			nr_seq_material,
	m.ds_material 			ds_material,
	null 				ie_origem_proced, 
	null 				cd_procedimento, 
	null 				ds_procedimento,
	m.nr_sequencia || ' - ' || 
		m.ds_material 		ds_item,
        c.nr_seq_guia			nr_seq_guia,
	c.nr_sequencia			nr_seq_conta
from	pls_conta_v	c,
	table(pls_conta_autor_pck.obter_dados(c.nr_seq_guia,'M', c.cd_estabelecimento)) ca,
	pls_material	m
where	c.nr_seq_guia		is not null
and	ca.qt_autorizada	> 0
and	ca.qt_utilizada		= 0
and	m.nr_sequencia		= ca.nr_seq_material
union all
select	'M'				ie_tipo,
	'Material'			ds_tipo,
	mat.nr_sequencia		nr_seq_material,
	mat.ds_material			ds_material,
	null				ie_origem_proced,
	null				cd_procedimento,
	null				ds_procedimento,
	mat.nr_sequencia || ' - ' || 
		mat.ds_material		ds_item,
	conta.nr_seq_guia_conv		nr_seq_guia,
	conta.nr_sequencia		nr_seq_conta
from	pls_conta_imp conta,
	table(pls_conta_autor_pck.obter_dados(conta.nr_seq_guia_conv,'M', conta.cd_estabelecimento)) ca,
	pls_material mat
where	conta.nr_seq_guia_conv is not null
and	ca.qt_autorizada	> 0
and	ca.qt_utilizada		= 0 
and	mat.nr_sequencia	= ca.nr_seq_material
union all
select	'P'				ie_tipo,
	'Procedimento'			ds_tipo,
	null				nr_seq_material,
	null				ds_material,
        ca.ie_origem_proced		ie_origem_proced,
	ca.cd_procedimento		cd_procedimento,
	p.ds_procedimento		ds_procedimento,
	p.cd_procedimento || ' - ' || 
		p.ds_procedimento 	ds_item,
        c.nr_seq_guia			nr_seq_guia,
	c.nr_sequencia			nr_seq_conta
from	pls_conta_v	c,
	table(pls_conta_autor_pck.obter_dados(c.nr_seq_guia,'D', c.cd_estabelecimento)) ca,
	procedimento  p
where	c.nr_seq_guia		is not null
and	ca.qt_autorizada	> 0
and	ca.qt_utilizada		= 0
and	p.ie_origem_proced	= ca.ie_origem_proced
and	p.cd_procedimento 	= ca.cd_procedimento
union all
select	'P'				ie_tipo,
	'Procedimento'			ds_tipo,
	null				nr_seq_material,
	null				ds_material,
	ca.ie_origem_proced		ie_origem_proced,
	ca.cd_procedimento		cd_procedimento,
	proc.ds_procedimento		ds_procedimento,
	proc.cd_procedimento || ' - ' || 
		proc.ds_procedimento	ds_item,
	conta.nr_seq_guia_conv		nr_seq_guia,
	conta.nr_sequencia		nr_seq_conta
from	pls_conta_imp conta,
	table(pls_conta_autor_pck.obter_dados(conta.nr_seq_guia_conv,'D', conta.cd_estabelecimento)) ca,
	procedimento proc
where	conta.nr_seq_guia_conv is not null
and	ca.qt_autorizada	> 0 
and	ca.qt_utilizada		= 0
and	proc.ie_origem_proced	= ca.ie_origem_proced
and	proc.cd_procedimento	= ca.cd_procedimento;
/
