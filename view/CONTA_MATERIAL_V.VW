CREATE OR REPLACE VIEW CONTA_MATERIAL_V AS
 Select nvl(a.cd_grupo_material,0) cd_grupo_material,
   nvl(a.cd_subgrupo_material,0) cd_subgrupo_material,
   nvl(a.cd_classe_material,0) cd_classe_material,
   nvl(a.cd_material,0) cd_material,
   a.cd_conta_receita,
   a.cd_conta_estoque,
   a.cd_conta_despesa,
   a.cd_conta_passag_direta,
   a.cd_setor_atendimento,
   a.ie_tipo_atendimento,
   a.ie_classif_convenio,
   f.ds_conta_contabil ds_conta_receita,
   g.ds_conta_contabil ds_conta_estoque,
   h.ds_conta_contabil ds_conta_despesa,
   b.ds_grupo_material,
   c.ds_subgrupo_material,
   d.ds_classe_material,
   e.ds_material
from parametros_conta_contabil a,
   grupo_material b,
   subgrupo_material c,
   classe_material d,
   material e,
   conta_contabil f,
   conta_contabil g,
   conta_contabil h
where      a.cd_grupo_material is not null
and        a.cd_grupo_material     = b.cd_grupo_material
and        a.cd_subgrupo_material  = c.cd_subgrupo_material (+)
and        a.cd_classe_material    = d.cd_classe_material (+)
and        a.cd_material           = e.cd_material (+)
and        a.cd_conta_receita      = f.cd_conta_contabil(+)
and        a.cd_conta_estoque      = g.cd_conta_contabil(+)
and        a.cd_conta_despesa      = h.cd_conta_contabil(+);
/
