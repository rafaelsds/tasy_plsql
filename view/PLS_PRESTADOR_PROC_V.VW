create or replace 
view 	pls_prestador_proc_v as
select	0	nr_prioridade,
	a.ie_liberar	ie_liberado,
	a.nr_seq_prestador,
	b.ie_origem_proced,
	b.cd_procedimento,
	a.dt_inicio_vigencia,
	a.dt_fim_vigencia,
	a.ie_tipo_guia,
	a.nr_seq_prestador_exec,
	a.nr_seq_tipo_prestador,
	a.cd_prestador_princ,
	a.ie_carater_internacao,
	a.nr_seq_tipo_atendimento,
	decode(a.ie_internado, 
		null,'T',
		'N', 'T', 
		'A', 'N', 
		'S', 'S') ie_internado,
	a.nr_seq_plano,
	a.nr_seq_grupo_produto,
	a.nr_seq_tipo_prest_prot,
	a.nr_seq_cbo_saude,
	a.cd_especialidade
from	pls_prestador_proc	a,
	procedimento	b
where	a.cd_area_procedimento		is null
and	a.cd_especialidade_proc		is null
and	a.cd_grupo_proc			is null
and 	a.nr_seq_grupo_serv		is null
and	a.cd_procedimento		is null
and	a.ie_origem_proced		is null
union all
select	1	nr_prioridade,
	a.ie_liberar	ie_liberado,
	a.nr_seq_prestador,
	b.ie_origem_proced,
	b.cd_procedimento,
	a.dt_inicio_vigencia,
	a.dt_fim_vigencia,
	a.ie_tipo_guia,
	a.nr_seq_prestador_exec,
	a.nr_seq_tipo_prestador,
	a.cd_prestador_princ,
	a.ie_carater_internacao,
	a.nr_seq_tipo_atendimento,
	decode(a.ie_internado, 
		null,'T',
		'N', 'T', 
		'A', 'N', 
		'S', 'S') ie_internado,
	a.nr_seq_plano,
	a.nr_seq_grupo_produto,
	a.nr_seq_tipo_prest_prot,
	a.nr_seq_cbo_saude,
	a.cd_especialidade
from	pls_prestador_proc	a,
	pls_grupo_servico_tm 	b
where	a.cd_area_procedimento		is null
and	a.cd_especialidade_proc		is null
and	a.cd_grupo_proc			is null
and	a.cd_procedimento		is null
and	a.ie_origem_proced		is null
and	a.nr_seq_grupo_serv		is not null
and	b.nr_seq_grupo_servico 		= a.nr_seq_grupo_serv
union all	
	select	case 	
		when 	a.cd_area_procedimento is not null	then 2
		when 	a.cd_especialidade_proc is not null	then 3
		when 	a.cd_grupo_proc is not null		then 4
	end	nr_prioridade,
	a.ie_liberar	ie_liberado,
	a.nr_seq_prestador,
	b.ie_origem_proced,
	b.cd_procedimento,
	a.dt_inicio_vigencia,
	a.dt_fim_vigencia,
	a.ie_tipo_guia,
	a.nr_seq_prestador_exec,
	a.nr_seq_tipo_prestador,
	a.cd_prestador_princ,
	a.ie_carater_internacao,
	a.nr_seq_tipo_atendimento,
	decode(a.ie_internado, 
		null,'T',
		'N', 'T', 
		'A', 'N', 
		'S', 'S') ie_internado,
	a.nr_seq_plano,
	a.nr_seq_grupo_produto,
	a.nr_seq_tipo_prest_prot,
	a.nr_seq_cbo_saude,
	a.cd_especialidade
from	pls_prestador_proc	a,
	table(pls_grupos_pck.obter_procs_estrutura (	null,
							null,
							null,
							a.cd_grupo_proc,
							a.cd_especialidade_proc,
							a.cd_area_procedimento,
							null,
							null)) b
where	a.cd_procedimento		is null
and	a.ie_origem_proced		is null
and	(a.cd_area_procedimento		is not null
	or a.cd_especialidade_proc	is not null
	or a.cd_grupo_proc		is not null)
union all
select	5	nr_prioridade,
	a.ie_liberar	ie_liberado,
	a.nr_seq_prestador,
	b.ie_origem_proced,
	b.cd_procedimento,
	a.dt_inicio_vigencia,
	a.dt_fim_vigencia,
	a.ie_tipo_guia,
	a.nr_seq_prestador_exec,
	a.nr_seq_tipo_prestador,
	a.cd_prestador_princ,
	a.ie_carater_internacao,
	a.nr_seq_tipo_atendimento,
	decode(a.ie_internado, 
		null,'T',
		'N', 'T', 
		'A', 'N', 
		'S', 'S') ie_internado,
	a.nr_seq_plano,
	a.nr_seq_grupo_produto,
	a.nr_seq_tipo_prest_prot,
	a.nr_seq_cbo_saude,
	a.cd_especialidade
from	pls_prestador_proc	a,
	procedimento	b
where	a.ie_origem_proced		is not null
and	a.cd_procedimento		is not null
and	a.ie_origem_proced		= b.ie_origem_proced
and	a.cd_procedimento		= b.cd_procedimento;
/
