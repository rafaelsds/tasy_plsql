create or replace 
view hepic_material_v as
	SELECT 	a.cd_material, 
			a.ds_material, 
			a.nr_registro_anvisa,
			a.nr_registro_ms, 
			d.cd_medicamento cd_brasindice,
			tiss_obter_cod_brasindice (d.cd_apresentacao, d.cd_laboratorio, d.cd_medicamento) cd_tiss,
			obter_valor_ultima_compra ('2',360,a.cd_material,'','') valor_ult_compra,
			obter_ult_custo_medio_mat ('2', a.cd_material) custo_medio
     FROM 	material a,
			classe_material b,
			subgrupo_material c,
			material_brasindice d
    WHERE 	a.cd_classe_material = b.cd_classe_material
      AND 	b.cd_subgrupo_material = c.cd_subgrupo_material
      AND 	a.cd_material = d.cd_material(+);
/