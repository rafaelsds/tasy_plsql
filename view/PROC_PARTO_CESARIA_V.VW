CREATE OR REPLACE VIEW Proc_Parto_Cesaria_V
AS
select a.nr_sequencia,
	a.nr_atendimento,
	a.nr_interno_conta,
	a.cd_medico_executor,
	nvl(decode(a.cd_procedimento, 45080186, 1, 45080097, 1, 35001011, 1, 35006013, 1, 35025018, 1, 0),0) qt_parto,
	nvl(decode(a.cd_procedimento, 45080194, 1, 45080020, 1, 35009012, 1, 35026014, 1, 0),0) qt_cesaria,
	a.cd_motivo_exc_conta 
from 	procedimento_paciente a
where a.cd_procedimento in (45080186, 45080097, 35001011, 35006013, 35025018, 45080194, 45080020, 35009012, 35026014);
/
