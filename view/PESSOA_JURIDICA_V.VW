create or replace view pessoa_juridica_v as 
select	a.*,
	b.ds_tipo_pessoa,
	a.ds_endereco || ', n� ' || a.nr_endereco || ', ' || a.ds_bairro || '' || a.ds_complemento || ', ' || a.ds_municipio || ' - ' || a.sg_estado ds_end_compl,
	a.ds_endereco || ', n� ' ||a.nr_endereco || ' ' || a.ds_complemento ds_end,
	a.ds_municipio || ' - ' || a .sg_estado ds_cid_est
from	tipo_pessoa_juridica	b,
	pessoa_juridica		a
where	a.cd_tipo_pessoa	= b.cd_tipo_pessoa;
/