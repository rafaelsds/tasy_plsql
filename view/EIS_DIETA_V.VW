Create or Replace View Eis_Dieta_v as
select	a.*,
	obter_valor_dominio(004, a.ie_sexo) ds_sexo,
	obter_valor_dominio(069, a.ie_faixa_etaria) ds_faixa_etaria,
	obter_nome_setor(a.cd_setor_atendimento) ds_setor_atendimento,
	obter_nome_dieta(a.cd_dieta)nm_dieta,
	obter_valor_dominio(099, a.cd_refeicao) ds_refeicao,
	substr(obter_desc_tipo_acomod(a.cd_tipo_acomodacao),1,255) ds_tipo_acomodacao,
	substr(obter_nome_convenio(a.cd_convenio),1,255) nm_convenio,
	obter_empresa_estab(a.cd_estabelecimento) cd_empresa,
	obter_classif_dieta(a.cd_dieta) cd_classif_dieta,
	substr(obter_desc_classif_dieta(a.cd_dieta),1,120) ds_classif_dieta,
	b.nr_atendimento,
	substr(obter_pessoa_atendimento(b.nr_atendimento,'N'),1,255) nm_pessoa_fisica
from	eis_dieta a,
	mapa_dieta b
where	a.nr_seq_mapa = b.nr_sequencia(+);
/