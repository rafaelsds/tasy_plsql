CREATE OR REPLACE VIEW LAUDO_PACIENTE_PACS_V
AS
SELECT	NR_SEQUENCIA,
	DS_LAUDO,
	DT_LIBERACAO,
	DS_ARQUIVO,
	IE_FORMATO
FROM	LAUDO_PACIENTE;
/
