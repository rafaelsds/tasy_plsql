CREATE OR REPLACE VIEW hsl_receita_caixa_v
AS
select	distinct
	c.nr_recibo,
	c.dt_Recebimento dt_recibo,
	SUBSTR(x.nr_nota_fiscal,1,100) nr_rps,
	n.vl_total_nota vl_rps,
	SUBSTR(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_paciente,
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
			titulo_receber_liq y
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= y.nr_seq_caixa_rec) vl_recebido,
	c.vl_especie vl_especie,
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao = '1501') vl_cheque_avista,
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao = '1502') vl_cheque_pre,
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506')) vl_credito,
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1507','1508')) vl_debito,
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie vl_tot_receb,
	(((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie) / 	
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ)) pr_proporcao,
	(c.vl_especie * 	(((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie) / 	
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ))) vl_pr_especie,
	((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao = '1501')* 	(((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie) / 	
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ))) vl_pr_cheque_avista,
	((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao = '1502') * 	(((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie) / 	
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ))) vl_pr_cheque_pre,
	((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506'))* 	(((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie) / 	
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ))) vl_pr_credito,
	((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1507','1508'))* 	(((	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ
		and	t.cd_transacao in('1503','1504','1505','9099','1506','1507','1508','1501','1502')) + c.vl_especie) / 	
	(	select	NVL(SUM(m.vl_transacao),0)
		from	caixa_Receb x,
				movto_trans_financ m,
				transacao_financeira t,
				transacao_financeira tr 
		where	x.nr_recibo	= c.nr_recibo
		and	x.nr_sequencia	= m.nr_seq_caixa_rec
		and	t.ie_caixa		= 'M' 
		and	t.nr_sequencia	= m.nr_seq_trans_financ
		and	tr.nr_sequencia	= x.nr_seq_trans_financ))) vl_pr_debito,
	p.cd_convenio_calculo,
	p.cd_categoria_calculo,
	c.dt_cancelamento,
	c.dt_recebimento,
	c.nr_seq_trans_financ,
	nvl(x.cd_estabelecimento,p.cd_estabelecimento) cd_estabelecimento,
	n.nr_seq_grupo_prod,
	x.nr_titulo
from	Caixa_Receb c,
	Movto_Trans_Financ m,
	transacao_financeira t,
	transacao_financeira tr,
	titulo_receber x,
	nota_fiscal n,
	conta_paciente p,
	atendimento_paciente a
where	c.nr_sequencia		= m.nr_seq_caixa_rec
and	a.nr_atendimento		= p.nr_atendimento
and	t.nr_sequencia		= m.nr_seq_trans_financ
and	tr.nr_sequencia		= c.nr_seq_trans_financ
and	x.nr_titulo(+)		= m.nr_seq_titulo_receber
and	n.nr_interno_conta(+)	= x.nr_interno_conta
and	p.nr_interno_conta(+)	= x.nr_interno_conta
order by 1;
/