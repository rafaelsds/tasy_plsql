create or replace
view TISS_DADOS_PACIENTE_V as
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	a.nr_atendimento,
	to_number(null) nr_seq_agenda,
	TISS_OBTER_CARTEIRINHA(b.cd_convenio, a.cd_estabelecimento, a.ie_tipo_atendimento, b.cd_usuario_convenio, b.cd_complemento) cd_usuario_convenio,	
	substr(obter_desc_plano(b.cd_convenio, b.cd_plano_convenio),1,40) ds_plano,
	b.dt_validade_carteira,
	--substr(nvl(tiss_obter_nome_abreviado(b.cd_convenio, a.cd_estabelecimento, a.cd_pessoa_fisica), obter_nome_pf(a.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(TISS_OBTER_TITULAR_CONV_RN(a.nr_atendimento, null, b.cd_convenio, a.cd_estabelecimento),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(a.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	a.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	nvl(c.ds_tarja_cartao,b.ds_tarja_cartao) ds_tarja_cartao,
	tiss_obter_se_atend_rn(a.nr_atendimento) ie_atendimento_rn
from	atend_categoria_convenio b,
	atendimento_paciente a,
	autorizacao_convenio c
where	a.nr_atendimento	= b.nr_atendimento
and	b.nr_seq_interno	= obter_atecaco_atendimento(b.nr_atendimento)
and	c.nr_atendimento	= a.nr_atendimento
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	c.nr_seq_agenda,
	TISS_OBTER_CARTEIRINHA(a.cd_convenio, obter_estab_agenda(a.cd_agenda), a.ie_tipo_atendimento, a.cd_usuario_convenio, null) cd_usuario_convenio,	
	substr(obter_desc_plano(a.cd_convenio, a.cd_plano),1,40) ds_plano,
	a.dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, obter_estab_agenda(a.cd_agenda), a.cd_pessoa_fisica), obter_nome_pf(a.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(a.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	a.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	tiss_obter_se_autor_rn(null, c.nr_sequencia,c.nr_seq_agenda, null) ie_atendimento_rn
from	agenda_paciente a,
	autorizacao_convenio c
where	c.nr_seq_agenda		= a.nr_sequencia
union
select	'2.01.01' ds_versao,
	'AP' ie_origem,
	a.nr_atendimento,
	to_number(null) nr_seq_agenda,
	b.cd_usuario_convenio,
	substr(obter_desc_plano(b.cd_convenio, b.cd_plano_convenio),1,40) ds_plano,
	b.dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(b.cd_convenio, a.cd_estabelecimento, c.cd_pessoa_fisica), c.nm_pessoa_fisica),1,150),
	c.nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	to_number(null) nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	c.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	null ds_tarja_cartao,
	tiss_obter_se_atend_rn(a.nr_atendimento) ie_atendimento_rn
from	pessoa_fisica c,
	atend_categoria_convenio b,
	atendimento_paciente a
where	b.nr_seq_interno	= obter_atecaco_atendimento(b.nr_atendimento)
and	a.nr_atendimento	= b.nr_atendimento
and	a.cd_pessoa_fisica	= c.cd_pessoa_fisica
union
select	'2.01.01' ds_versao,
	'MED' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_seq_agenda,
	a.cd_usuario_convenio,
	substr(c.ds_plano,1,40) ds_plano,
	a.dt_validade_carteira,
	substr(obter_nome_pf(d.cd_pessoa_fisica),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(d.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	to_number(null) nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	d.cd_pessoa_fisica,
	b.nr_sequencia nr_seq_med_guia,
	a.nr_atendimento nr_med_atendimento,
	null ds_tarja_cartao,
	'N' ie_atendimento_rn
from	med_cliente d,
	med_plano c,
	med_faturamento b,
	med_atendimento a
where	a.nr_atendimento	= b.nr_atendimento
and	a.nr_seq_plano	= c.nr_sequencia
and	a.nr_seq_cliente	= d.nr_sequencia
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	c.nr_seq_agenda,
	TISS_OBTER_CARTEIRINHA(a.cd_convenio,  obter_estab_agenda(a.cd_agenda), a.ie_tipo_atendimento, a.cd_usuario_convenio, a.cd_complemento) cd_usuario_convenio,
	substr(obter_desc_plano(a.cd_convenio, obter_plano_cod_usuario_conv(a.cd_convenio, a.cd_usuario_convenio)), 1, 40) ds_plano,
	a.dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, obter_estab_agenda(a.cd_agenda), a.cd_pessoa_fisica), obter_nome_pf(a.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(a.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	a.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	tiss_obter_se_autor_rn(null, c.nr_sequencia, null,c.nr_seq_agenda_consulta) ie_atendimento_rn
from	agenda_consulta a,
	autorizacao_convenio c
where	c.nr_seq_agenda_consulta = a.nr_sequencia
union
select	'2.01.01' ds_versao,
	'GV' ie_origem,
	to_number(null) nr_atendimento,
	a.nr_sequencia,
	a.ds_cod_usuario,
	substr(obter_desc_plano(a.cd_convenio, obter_plano_cod_usuario_conv(a.cd_convenio, a.ds_cod_usuario)), 1, 40) ds_plano,
	a.dt_validade,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, a.cd_estabelecimento, a.cd_pessoa_fisica), obter_nome_pf(a.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(a.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	a.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	tiss_obter_se_autor_rn(null, c.nr_sequencia, null, null) ie_atendimento_rn
from	gestao_vaga a,
	autorizacao_convenio c
where	c.nr_seq_gestao	= a.nr_sequencia
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_sequencia,
	a.cd_usuario_convenio,
	substr(obter_desc_plano(a.cd_convenio, a.cd_plano_convenio),1,40) ds_plano,
	a.dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, a.cd_estabelecimento, c.cd_pessoa_fisica), obter_nome_pf(c.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(c.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	c.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	tiss_obter_se_autor_rn(a.ie_cod_usuario_mae_resp, c.nr_sequencia, c.nr_seq_agenda, c.nr_seq_agenda_consulta) ie_atendimento_rn
from	autorizacao_convenio_tiss a,
	autorizacao_convenio c
where	c.nr_sequencia			= a.nr_sequencia_autor
and	c.nr_atendimento		is null
and	c.nr_seq_agenda			is null
and	c.nr_seq_agenda_consulta	is null
and	c.nr_seq_gestao			is null
and	c.nr_seq_paciente_setor		is null
and	c.cd_pessoa_fisica 		is not null
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_sequencia,
	TISS_OBTER_CARTEIRINHA(a.cd_convenio, a.cd_estabelecimento, null, a.cd_usuario_convenio, null) cd_usuario_convenio,	
	substr(obter_desc_plano(a.cd_convenio, a.cd_plano_convenio),1,40) ds_plano,
	a.dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, a.cd_estabelecimento, d.cd_pessoa_fisica), obter_nome_pf(d.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(d.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	d.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	tiss_obter_se_autor_rn(a.ie_cod_usuario_mae_resp, c.nr_sequencia, c.nr_seq_agenda, c.nr_seq_agenda_consulta) ie_atendimento_rn
from	paciente_setor d,
	autorizacao_convenio_tiss a,
	autorizacao_convenio c
where	c.nr_sequencia			= a.nr_sequencia_autor
and	c.nr_seq_paciente_setor		= d.nr_seq_paciente
and	c.nr_atendimento		is null
and	c.nr_seq_agenda			is null
and	c.nr_seq_agenda_consulta	is null
and	c.nr_seq_gestao			is null
and	c.nr_seq_paciente_setor		is not null
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_sequencia,
	TISS_OBTER_CARTEIRINHA(a.cd_convenio, c.cd_estabelecimento, null, a.cd_usuario_convenio, null) cd_usuario_convenio,	
	substr(obter_desc_plano(a.cd_convenio, a.cd_plano),1,40) ds_plano,
	null dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, c.cd_estabelecimento, d.cd_pessoa_fisica), obter_nome_pf(d.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(d.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	d.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	'N' ie_atendimento_rn
from	paciente_setor d,
	paciente_setor_convenio a,
	autorizacao_convenio c
where	c.nr_seq_paciente_setor		= a.nr_seq_paciente
and	c.nr_seq_paciente_setor		= d.nr_seq_paciente
and	c.nr_atendimento		is null
and	c.nr_seq_agenda			is null
and	c.nr_seq_agenda_consulta	is null
and	c.nr_seq_gestao			is null
and	c.nr_seq_paciente_setor		is not null
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_sequencia,
	TISS_OBTER_CARTEIRINHA(a.cd_convenio, a.cd_estabelecimento, null, a.cd_usuario_convenio, null) cd_usuario_convenio,
	substr(obter_desc_plano(a.cd_convenio, a.cd_plano_convenio),1,40) ds_plano,
	null dt_validade_carteira,
	substr(nvl(tiss_obter_nome_abreviado(a.cd_convenio, c.cd_estabelecimento, c.cd_pessoa_fisica), obter_nome_pf(c.cd_pessoa_fisica)),1,150) nm_pessoa_fisica,
	substr(obter_dados_pf(c.cd_pessoa_fisica, 'CNS'),1,20) nr_cartao_nac_sus,
	to_number(null) nr_seq_protocolo,
	c.nr_sequencia nr_sequencia_autor,
	to_number(null) nr_interno_conta,
	c.cd_pessoa_fisica,
	to_number(null) nr_seq_med_guia,
	to_number(null) nr_med_atendimento,
	c.ds_tarja_cartao ds_tarja_cartao,
	tiss_obter_se_autor_rn(a.ie_cod_usuario_mae_resp, c.nr_sequencia, null, null) ie_atendimento_rn
from	autorizacao_convenio_tiss a,
	autorizacao_convenio c
where	c.nr_atendimento		is null
and	c.nr_sequencia			= a.nr_sequencia_autor(+)
and	c.nr_seq_agenda			is null
and	c.nr_seq_agenda_consulta	is null
and	c.nr_seq_gestao			is null
and	c.nr_seq_rxt_tratamento		is not null;
/
