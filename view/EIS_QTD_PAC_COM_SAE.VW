create or replace view eis_Qtd_pac_com_sae as
SELECT	obter_unidade_atendimento(a.nr_atendimento,'A','CS') cd_setor_atendimento,
	obter_unidade_atendimento(a.nr_atendimento,'A','S') ds_setor_atendimento,
	dt_entrada
FROM	atendimento_paciente a
WHERE	IE_TIPO_ATENDIMENTO = 1
AND	EXISTS(	SELECT	1
		FROM	pe_prescricao x
		WHERE	x.nr_atendimento = a.nr_atendimento
		AND		trunc(x.dt_prescricao - a.dt_entrada)  <= 24)
GROUP BY obter_unidade_atendimento(a.nr_atendimento,'A','CS'),
		obter_unidade_atendimento(a.nr_atendimento,'A','S'),
		dt_entrada;
/