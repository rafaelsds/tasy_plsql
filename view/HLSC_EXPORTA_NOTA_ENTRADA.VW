CREATE OR REPLACE VIEW hlsc_exporta_nota_entrada AS 
SELECT	'01' tp_registro,
	n.nr_sequencia,
	n.dt_emissao,
	e.cd_cgc,
	e.cd_estabelecimento,
	NULL cd_cfop,
	NULL nr_nota_fiscal,
	NULL cd_serie_nf,
	NULL dt_entrada_saida,
	NULL vl_total_nota,
	NULL sg_estado,
	NULL ie_tipo_frete,
	NULL vl_mercadoria,
	NULL nr_inscricao_estadual,
	NULL nr_inscricao_municipal,
	NULL ds_observacao,
	NULL nr_danfe,
	NULL cd_tributo,
	NULL vl_base_calculo,
	NULL vl_tributo,
	0 ie_fixo,
	(SELECT MIN(n.dt_emissao) FROM nota_fiscal x WHERE n.nr_sequencia = x.nr_sequencia)dt_inicial,
	(SELECT MAX(n.dt_emissao) FROM nota_fiscal x WHERE n.nr_sequencia = x.nr_sequencia)dt_final
FROM	nota_fiscal n,
	estabelecimento_v e
WHERE	1 = 1
AND	n.cd_estabelecimento = e.cd_estabelecimento
AND	SUBSTR(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'E'
AND	n.dt_atualizacao_estoque IS NOT NULL
UNION ALL
SELECT	'02',
	n.nr_sequencia,
	n.dt_emissao,
	n.cd_cgc_emitente,
	n.cd_estabelecimento,
	t.cd_cfop,
	n.nr_nota_fiscal,
	n.cd_serie_nf,
	n.dt_entrada_saida,
	n.vl_total_nota,
	p.sg_estado,
	NVL(f.ie_tipo_frete,'C'),
	n.vl_mercadoria,
	p.nr_inscricao_estadual,
	p.nr_inscricao_municipal,
	n.ds_observacao,
	n.nr_danfe,
	NULL,
	NULL,
	NULL,
	NULL,
	(SELECT MIN(n.dt_emissao) FROM nota_fiscal x WHERE n.nr_sequencia = x.nr_sequencia)dt_inicial,
	(SELECT MAX(n.dt_emissao) FROM nota_fiscal x WHERE n.nr_sequencia = x.nr_sequencia)dt_final
FROM	nota_fiscal n,
	pessoa_juridica p,
	nota_fiscal_transportadora f,
	natureza_operacao t
WHERE	1 = 1
AND	p.cd_cgc = n.cd_cgc_emitente
AND	n.nr_sequencia = f.nr_seq_nota(+)
AND	t.cd_natureza_operacao = n.cd_natureza_operacao
AND	SUBSTR(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'E'
AND	n.dt_atualizacao_estoque IS NOT NULL
UNION ALL
SELECT	'03',
	n.nr_sequencia,
	n.dt_emissao,
	NULL,
	n.cd_estabelecimento,
	NULL,
	NULL,
	NULL,
	NULL,
	n.vl_total_nota,
	NULL,
	NULL,
	n.vl_mercadoria,
	NULL,
	NULL,
	NULL,
	NULL,
	t.cd_tributo,
	t.vl_base_calculo,
	t.vl_tributo,
	NULL,
	(SELECT MIN(n.dt_emissao) FROM nota_fiscal x WHERE n.nr_sequencia = x.nr_sequencia)dt_inicial,
	(SELECT MAX(n.dt_emissao) FROM nota_fiscal x WHERE n.nr_sequencia = x.nr_sequencia)dt_final
FROM	nota_fiscal n,
	nota_fiscal_trib t
WHERE	1 = 1
AND	t.nr_sequencia = n.nr_sequencia
AND	SUBSTR(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'E'
AND	n.dt_atualizacao_estoque IS NOT NULL