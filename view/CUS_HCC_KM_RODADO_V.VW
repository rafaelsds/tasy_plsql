create or replace
view cus_hcc_km_rodado_v as
select	trunc(a.DT_REGISTRO,'dd') dt_referencia,
	b.cd_estab_contabil,
	a.cd_centro_custo cd_centro_controle,
	sum(qt_contador) qt_distribuicao
from	man_equip_contador a,
	man_equipamento b
where	a.nr_seq_equipamento = b.nr_sequencia
group by 	trunc(a.DT_REGISTRO,'dd'),
	b.cd_estab_contabil,
	a.cd_centro_custo;
/
