create or replace
view hmlb_proc_transf_rel_fatura_v
as
SELECT	a.nr_sequencia nr_seq_transfusao,
	c.qt_procedimento qt_pacote,
	to_char(c.cd_procedimento_convenio) cd_procedimento,
	obter_desc_proc_convenio(c.nr_sequencia,'0') ds_procedimento,
	c.vl_procedimento/c.qt_procedimento vl_procedimento
FROM	san_transfusao a,
	san_producao b,
	procedimento_paciente c,
	pessoa_fisica d
WHERE	a.nr_sequencia = b.nr_seq_transfusao
AND	b.nr_seq_propaci = c.nr_sequencia
AND	a.cd_pessoa_fisica =  d.cd_pessoa_fisica
UNION all
SELECT	a.nr_sequencia nr_seq_transfusao,
	c.qt_procedimento qt_pacote,
	to_char(c.cd_procedimento_convenio) cd_procedimento,
	obter_desc_proc_convenio(c.nr_sequencia,'0') ds_procedimento,
	c.vl_procedimento/c.qt_procedimento vl_procedimento
FROM	san_transfusao a,
	san_producao b,
	procedimento_paciente c,
	pessoa_fisica d,
	san_exame_lote f,
	san_exame_realizado g
WHERE	a.nr_sequencia = b.nr_seq_transfusao
AND	g.nr_seq_propaci = c.nr_sequencia
AND	a.cd_pessoa_fisica =  d.cd_pessoa_fisica
AND	b.nr_sequencia = f.nr_seq_producao
AND	f.nr_sequencia = g.nr_seq_exame_lote
UNION all
select	a.nr_sequencia nr_seq_transfusao,
	nvl(x.qt_lancamento,1) qt_pacote,
	to_char(coalesce(x.cd_procedimento,x.cd_material,x.nr_seq_proc_interno)) cd_procedimento,
	substr(decode(Obter_desc_proc_mat(x.cd_material, x.cd_procedimento, x.ie_origem_proced),'N�o Encontrado', obter_desc_proc_interno(x.nr_seq_proc_interno),Obter_desc_proc_mat(x.cd_material, x.cd_procedimento, x.ie_origem_proced)),1,254) ds_procedimento,
	San_obter_preco_proced(y.cd_estabelecimento,
			a.nr_atendimento,
			x.cd_procedimento,
			x.ie_origem_proced,
			x.nr_seq_proc_interno) vl_procedimento
from	regra_lanc_automatico y,
	regra_lanc_aut_pac x,
	san_transfusao a,
	san_producao b
where	x.nr_seq_regra = y.nr_sequencia
and	a.nr_sequencia = b.nr_seq_transfusao
and	nvl(y.ie_situacao,'A') = 'A'
and	y.cd_estabelecimento = a.cd_estabelecimento
and	((y.cd_convenio = obter_convenio_atendimento(a.nr_atendimento)) or (y.cd_convenio is null))
and	y.nr_seq_proc_interno = obter_proc_tab_interno_hemo(b.nr_sequencia)
and not exists(	select	1
		from	procedimento_paciente c
		where	(c.nr_seq_proc_interno = x.nr_seq_proc_interno or c.cd_procedimento = x.cd_procedimento)
		and 	c.cd_motivo_exc_conta is null 
		and	exists(	select	1
				from	procedimento_paciente d
				where	(d.nr_sequencia = b.nr_seq_propaci or d.nr_seq_transfusao = a.nr_sequencia)
				and	d.nr_sequencia = c.nr_seq_proc_princ))
and not exists(	select	1
		from	material_atend_paciente c
		where	c.cd_material = x.cd_material
		and 	c.cd_motivo_exc_conta is null
		and	exists(	select	1
				from	procedimento_paciente d
				where	(d.nr_sequencia = b.nr_seq_propaci or d.nr_seq_transfusao = a.nr_sequencia)
				and	d.nr_sequencia = c.nr_seq_proc_princ))
union all
SELECT	a.nr_sequencia nr_seq_transfusao,
	San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'Q') qt_pacote,
	to_char(San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'C')) cd_procedimento,
	Obter_Desc_Procedimento(San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'C'),San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'O')) ds_procedimento,
	San_obter_preco_proced(a.cd_estabelecimento,
			a.nr_atendimento,
			San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'C'),
			San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'O'),
			San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'I')) vl_procedimento
FROM	san_transfusao a,
	san_producao b
WHERE	a.nr_sequencia = b.nr_seq_transfusao
and	San_obter_proc_derivado(0,b.nr_sequencia,null,a.nr_atendimento,'C') is not null
and not exists(	select	1
		from	procedimento_paciente c
		where	b.nr_seq_propaci = c.nr_sequencia)
UNION all
SELECT	a.nr_sequencia nr_seq_transfusao,
	San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'Q') qt_pacote,
	to_char(San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'C')) cd_procedimento,
	Obter_Desc_Procedimento(San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'C'),San_obter_proc_derivado(1,b.nr_sequencia,null,a.nr_atendimento,'O')) ds_procedimento,
	San_obter_preco_proced(a.cd_estabelecimento,
			a.nr_atendimento,
			San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'C'),
			San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'O'),
			San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'I')) vl_procedimento
FROM	san_exame d,
	san_transfusao a,
	san_producao b,
	san_exame_lote f,
	san_exame_realizado g
WHERE	a.nr_sequencia = b.nr_seq_transfusao
AND	b.nr_sequencia = f.nr_seq_producao
AND	f.nr_sequencia = g.nr_seq_exame_lote
and	g.nr_seq_exame = d.nr_sequencia
and	San_obter_proc_derivado(1,g.nr_seq_exame,g.nr_seq_exame_lote,a.nr_atendimento,'C') is not null
and	nvl(d.ie_situacao,'A')	= 'A'
and	(((g.vl_resultado is not null) or (g.ds_resultado is not null)) or (nvl(d.ie_conta_sem_result,'N') = 'S'))
and	((nvl(d.ie_gerar_conta_amostra,'N') = 'N')
or 	exists (select	1
		from	san_reserva r
		where	r.nr_sequencia = b.nr_seq_reserva
		and	substr(san_obter_se_amostra_conta(r.cd_pessoa_fisica),1,1) = 'S'))
and not exists(	select	1
		from	procedimento_paciente c
		where	g.nr_seq_propaci = c.nr_sequencia);
/