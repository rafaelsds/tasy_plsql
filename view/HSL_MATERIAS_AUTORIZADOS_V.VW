create or replace
view HSL_MATERIAS_AUTORIZADOS_V
as
select	a.nr_sequencia_autor nr_sequencia_autor,
	substr(obter_nome_pf_pj(null,a.cd_cgc_fabricante),1,255) ds_fornecedor,
	a.nr_sequencia cd_material,
	substr(obter_desc_material(a.cd_material),1,100) ds_material,
	nvl(a.qt_solicitada,0) qt_solicitada,
	nvl(a.vl_unitario,0) vl_unitario,
	nvl((a.qt_solicitada*a.vl_unitario),0) tt_vl_solicitado,
	obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material) qt_utilizada,
	nvl(a.vl_unitario,0) vl_unitario_utilizado,
	(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material)*nvl(a.vl_unitario,0)) tt_vl_utilizado,
	'Utilizado' ds_status,
	a.qt_autorizada qt_autorizada,
	nvl((a.qt_autorizada*a.vl_unitario),0) vl_total_autor
from	material_autorizado a
where	nvl(qt_autorizada,0)-nvl(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material),0)=0
and	nvl(qt_autorizada,0) > 0
union all
select	a.nr_sequencia_autor nr_sequencia_autor,
	substr(obter_nome_pf_pj(null,a.cd_cgc_fabricante),1,255) ds_fornecedor,
	a.nr_sequencia cd_material,
	substr(obter_desc_material(a.cd_material),1,100) ds_material,
	nvl(a.qt_solicitada,0) qt_solicitada,
	nvl(a.vl_unitario,0) vl_unitario,
	nvl((a.qt_solicitada*a.vl_unitario),0) tt_vl_solicitado,
	obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material) qt_utilizada,
	nvl(a.vl_unitario,0) vl_unitario_utilizado,
	(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material)*nvl(a.vl_unitario,0)) tt_vl_utilizado,
	'Utilizado a Maior' ds_status,
	a.qt_autorizada,
	nvl((a.qt_autorizada*a.vl_unitario),0) vl_total_autor
from	material_autorizado a
where	nvl(qt_autorizada,0)-nvl(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material),0) < 0
union all
select	a.nr_sequencia_autor nr_sequencia_autor,
	substr(obter_nome_pf_pj(null,a.cd_cgc_fabricante),1,255) ds_fornecedor,
	a.nr_sequencia cd_material,
	substr(obter_desc_material(a.cd_material),1,100) ds_material,
	nvl(a.qt_solicitada,0) qt_solicitada,
	nvl(a.vl_unitario,0) vl_unitario,
	nvl((a.qt_solicitada*a.vl_unitario),0) tt_vl_solicitado,
	obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material) qt_utilizada,
	nvl(a.vl_unitario,0) vl_unitario_utilizado,
	(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material)*nvl(a.vl_unitario,0)) tt_vl_utilizado,
	'Utilizado a Menor' ds_status,
	a.qt_autorizada,
	nvl((a.qt_autorizada*a.vl_unitario),0) vl_total_autor
from	material_autorizado a
where	nvl(qt_autorizada,0)-nvl(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material),0) > 0
union all
select	a.nr_sequencia_autor nr_sequencia_autor,
	substr(obter_nome_pf_pj(null,a.cd_cgc_fabricante),1,255) ds_fornecedor,
	a.nr_sequencia cd_material,
	substr(obter_desc_material(a.cd_material),1,100) ds_material,
	nvl(a.qt_solicitada,0) qt_solicitada,
	nvl(a.vl_unitario,0) vl_unitario,
	nvl((a.qt_solicitada*a.vl_unitario),0) tt_vl_solicitado,
	obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material) qt_utilizada,
	nvl(a.vl_unitario,0) vl_unitario_utilizado,
	(obter_qt_util_mat_autor(a.nr_sequencia,a.nr_atendimento,obter_convenio_atendimento(a.nr_atendimento),a.cd_material)*nvl(a.vl_unitario,0)) tt_vl_utilizado,
	'Utilizado Sem Pr�' ds_status,
	a.qt_autorizada,
	nvl((a.qt_autorizada*a.vl_unitario),0) vl_total_autor
from	material_autorizado a
where	nvl(qt_autorizada,0) =0
order by 	ds_material,cd_material;
/