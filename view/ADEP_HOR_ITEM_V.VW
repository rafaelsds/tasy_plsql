create or replace
view adep_hor_item_v
as
select	1 nr_seq_apresent,
	'D' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	null nr_seq_item,
	c.cd_refeicao cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'D'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	'' cd_unidade_medida,
	'' ds_intervalo,
	0 qt_dose,
	null nr_seq_lote,
	a.nr_atendimento
from	prescr_dieta_hor c,
	prescr_medica a
where	c.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'S'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	2 nr_seq_apresent,
	'S' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'S'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	b.ie_agrupador = 12
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(a.ie_adep,'S') = 'S'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	3 nr_seq_apresent,
	'M' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	--substr(obter_status_hor_prescr(c.nr_sequencia,'M'),1,1) ie_status,
	decode(	c.dt_recusa,null,
		substr(obter_status_hor_medic(c.dt_fim_horario,c.dt_suspensao,c.ie_dose_especial,c.nr_seq_processo,c.nr_seq_area_prep),1,15),
			substr(obter_status_recusa_pac(c.dt_fim_horario,c.dt_suspensao),1,15)) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	b.ie_agrupador = 1
and	nvl(a.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	4 nr_seq_apresent,
	'MAT' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'M'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	b.ie_agrupador = 2
and	nvl(a.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	a.dt_liberacao is not null
and	obter_se_material_adep(a.cd_estabelecimento,b.cd_material) = 'S'
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	decode(b.nr_seq_exame, null, 5, 8) nr_seq_apresent,
	decode(obter_se_ctrl_glic_proc(b.nr_seq_proc_interno), 'S', 'G',decode(b.nr_seq_exame, null, 'P', 'L')) ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_procedimento) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'P'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	'' cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	0 qt_dose,
	null nr_seq_lote,
	a.nr_atendimento
from	prescr_proc_hor c,
	prescr_procedimento b,
	prescr_medica a
where	c.nr_seq_procedimento = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	b.nr_seq_solic_sangue is null
and	b.nr_seq_derivado is null
--and	((b.nr_seq_proc_interno is null) or (obter_ctrl_glic_proc(b.nr_seq_proc_interno) = 'NC'))
and	nvl(a.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	6 nr_seq_apresent,
	'R' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	nvl(to_char(b.cd_recomendacao),b.ds_recomendacao) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'R'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	'' cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	0 qt_dose,
	null nr_seq_lote,
	a.nr_atendimento
from	prescr_rec_hor c,
	prescr_recomendacao b,
	prescr_medica a
where	c.nr_seq_recomendacao = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'S'
and	nvl(c.ie_horario_especial,'N') = 'N'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	7 nr_seq_apresent,
	'E' ie_tipo_item,
	a.nr_sequencia nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.nr_seq_proc) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'E'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	'' cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	0 qt_dose,
	null nr_seq_lote,
	a.nr_atendimento
from	pe_prescr_proc_hor c,
	pe_prescr_proc b,
	pe_prescricao a
where	c.nr_seq_pe_proc = b.nr_sequencia
and	c.nr_seq_pe_prescr = b.nr_seq_prescr
and	b.nr_seq_prescr = a.nr_sequencia
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(b.ie_adep,'N') in ('S','M')
and	a.dt_liberacao is not null
union
select	8 nr_seq_apresent, --Material associado procedimentos
	'IA' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'M'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_adep,'S') = 'S'
and	b.ie_agrupador = 5
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(a.ie_adep,'S') = 'S'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	9 nr_seq_apresent, --SUporte nutricional enteral
	'SNE' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'M'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_adep,'S') = 'S'
and	b.ie_agrupador = 8
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(a.ie_adep,'S') = 'S'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	10 nr_seq_apresentacao, --Solu��es
	'SOL' ie_tipo_item,	
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'M'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
and	nvl(c.ie_adep,'S') = 'S'
and	b.ie_agrupador = 4
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(a.ie_adep,'S') = 'S'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
union
select	12 nr_seq_apresent,
	'LD' ie_tipo_item,
	a.nr_prescricao nr_prescricao,
	b.nr_sequencia nr_seq_item,
	to_char(b.cd_material) cd_item,
	c.nr_sequencia nr_seq_horario,
	c.dt_horario dt_horario,
	substr(obter_status_hor_prescr(c.nr_sequencia,'S'),1,1) ie_status,
	c.dt_fim_horario dt_adm,
	c.dt_suspensao dt_susp,
	b.cd_unidade_medida,
	substr(Obter_desc_intervalo(b.cd_intervalo),1,40) ds_intervalo,
	b.qt_dose,
	c.nr_seq_lote nr_seq_lote,
	a.nr_atendimento
from	prescr_mat_hor c,
	prescr_material b,
	prescr_medica a
where	c.nr_seq_material = b.nr_sequencia
and	c.nr_prescricao = b.nr_prescricao
and	b.nr_prescricao = a.nr_prescricao
--and	nvl(c.ie_situacao,'A') = 'A'
and	b.ie_agrupador = 16
and	nvl(c.ie_horario_especial,'N') = 'N'
and	nvl(a.ie_adep,'S') = 'S'
and	a.dt_liberacao is not null
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S';
/
