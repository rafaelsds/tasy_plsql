create or replace 
view hepic_disp_proc_invasivo_v as
select a.nr_atendimento, a.dt_atualizacao_nrec dt_registro,
          a.dt_atualizacao, a.nm_usuario,
          a.nm_usuario_nrec usuario_atualizacao, dt_instalacao,
          qt_hora_permanencia, a.dt_retirada_prev, qt_hora_perm_curat,
          dt_retir_prev_curat, cd_profissional,
          d.nm_pessoa_fisica profissional, a.qt_tentativas,
          a.qt_disp_utilizados, a.ie_bronco, a.ie_inconsciente, a.ie_sne,
          a.ie_sucesso, c.nr_seq_disp_pac, c.nr_seq_proc_interno,
          c.cd_setor_atendimento, e.ds_proc_exame, ds_setor_atendimento,
          0 cd_material, ' ' ds_material
     from atend_pac_dispositivo a,
          atend_pac_disp_proc c,
          pessoa_fisica d,
          proc_interno e,
          setor_atendimento f
    where a.nr_sequencia = c.nr_sequencia
      and a.cd_profissional = d.cd_pessoa_fisica
      and c.nr_seq_proc_interno = e.nr_sequencia
      and c.cd_setor_atendimento = f.cd_setor_atendimento
   union all
   select a.nr_atendimento, a.dt_atualizacao_nrec dt_registro,
          a.dt_atualizacao, a.nm_usuario,
          a.nm_usuario_nrec usuario_atualizacao, dt_instalacao,
          qt_hora_permanencia, a.dt_retirada_prev, qt_hora_perm_curat,
          dt_retir_prev_curat, cd_profissional,
          d.nm_pessoa_fisica profissional, a.qt_tentativas,
          a.qt_disp_utilizados, a.ie_bronco, a.ie_inconsciente, a.ie_sne,
          a.ie_sucesso, 0 nr_seq_disp_pac, 0 nr_seq_proc_interno,
          0 cd_setor_atendimento, ' ', ' ' ds_setor_atendimento,
          b.cd_material, e.ds_material
     from atend_pac_dispositivo a,
          atend_pac_disp_mat b,
          pessoa_fisica d,
          material e
    where a.nr_sequencia = b.nr_seq_disp_pac
      and a.cd_profissional = d.cd_pessoa_fisica
      and b.cd_material = e.cd_material;
/