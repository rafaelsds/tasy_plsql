create or replace
view cpoe_material_hemo_item_v as

	select * 
	from (
		select	
			nr_sequencia nr_sequencia,
			cd_interv_hem1 cd_interv_hem,
			cd_mat_hem1 cd_mat_hem,
			cd_unid_medida_dose_hem1 cd_unid_medida_dose_hem,
			ds_hor_hem1 ds_hor_hem,
			dt_inicio_hem1 dt_inicio_hem,
			dt_prim_hor_hem1 dt_prim_hor_hem,
			hr_prim_hor_hem1 hr_prim_hor_hem,
			ie_adm_mat_hem1 ie_adm_mat_hem,
			ie_urgencia_mat_hem1 ie_urgencia_mat_hem,
			ie_via_mat_hem1 ie_via_mat_hem,
			qt_dose_hem1 qt_dose_hem
		from CPOE_hemoterapia
		where	cd_interv_hem1 is not null 
		union all
		select	
			nr_sequencia nr_sequencia,
			cd_interv_hem2 cd_interv_hem,
			cd_mat_hem2 cd_mat_hem,
			cd_unid_medida_dose_hem2 cd_unid_medida_dose_hem,
			ds_hor_hem2 ds_hor_hem,
			dt_inicio_hem2 dt_inicio_hem,
			dt_prim_hor_hem2 dt_prim_hor_hem,
			hr_prim_hor_hem2 hr_prim_hor_hem,
			ie_adm_mat_hem2 ie_adm_mat_hem,
			ie_urgencia_mat_hem2 ie_urgencia_mat_hem,
			ie_via_mat_hem2 ie_via_mat_hem,
			qt_dose_hem2 qt_dose_hem
		from CPOE_hemoterapia		
		where	cd_interv_hem2 is not null 
		union all
		select	
			nr_sequencia nr_sequencia,		
			cd_interv_hem3 cd_interv_hem,
			cd_mat_hem3 cd_mat_hem,
			cd_unid_medida_dose_hem3 cd_unid_medida_dose_hem,
			ds_hor_hem3 ds_hor_hem,
			dt_inicio_hem3 dt_inicio_hem,
			dt_prim_hor_hem3 dt_prim_hor_hem,
			hr_prim_hor_hem3 hr_prim_hor_hem,
			ie_adm_mat_hem3 ie_adm_mat_hem,
			ie_urgencia_mat_hem3 ie_urgencia_mat_hem,
			ie_via_mat_hem3 ie_via_mat_hem,
			qt_dose_hem3 qt_dose_hem
		from CPOE_hemoterapia
		where	cd_interv_hem3 is not null 
		union all
		select	
			nr_sequencia nr_sequencia,
			cd_interv_hem4 cd_interv_hem,
			cd_mat_hem4 cd_mat_hem,
			cd_unid_medida_dose_hem4 cd_unid_medida_dose_hem,
			ds_hor_hem4 ds_hor_hem,
			dt_inicio_hem4 dt_inicio_hem,
			dt_prim_hor_hem4 dt_prim_hor_hem,
			hr_prim_hor_hem4 hr_prim_hor_hem,
			ie_adm_mat_hem4 ie_adm_mat_hem,
			ie_urgencia_mat_hem4 ie_urgencia_mat_hem,
			ie_via_mat_hem4 ie_via_mat_hem,
			qt_dose_hem4 qt_dose_hem
		from CPOE_hemoterapia
		where	cd_interv_hem4 is not null 
		union all
		select	
			nr_sequencia nr_sequencia,
			cd_interv_hem5 cd_interv_hem,
			cd_mat_hem5 cd_mat_hem,
			cd_unid_medida_dose_hem5 cd_unid_medida_dose_hem,
			ds_hor_hem5 ds_hor_hem,
			dt_inicio_hem5 dt_inicio_hem,
			dt_prim_hor_hem5 dt_prim_hor_hem,
			hr_prim_hor_hem5 hr_prim_hor_hem,
			ie_adm_mat_hem5 ie_adm_mat_hem,
			ie_urgencia_mat_hem5 ie_urgencia_mat_hem,
			ie_via_mat_hem5 ie_via_mat_hem,
			qt_dose_hem5 qt_dose_hem
		from CPOE_hemoterapia
		where	cd_interv_hem5 is not null 			
	)order by nr_sequencia;
/