create or replace view pls_cp_cta_segurado_plano_v as
select	a.nr_sequencia,
	a.nr_seq_contrato,
	a.nr_seq_plano,
	c.ie_preco,
	c.ie_tipo_contratacao,
	c.ie_acomodacao,
	d.nr_seq_tipo_acomodacao,
	d.nr_seq_categoria,
	null nr_seq_grupo,
	c.ie_franquia
from	pls_segurado	a,
	pls_contrato	b,
	pls_plano 	c,
	pls_plano_acomodacao d
where 	b.nr_sequencia(+) = a.nr_seq_contrato
and	c.nr_sequencia = a.nr_seq_plano
and	d.nr_seq_plano(+)= c.nr_sequencia;
/