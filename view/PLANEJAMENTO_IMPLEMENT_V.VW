CREATE OR REPLACE VIEW planejamento_implement_v
AS
select 	3 nr_consistencia,
	a.nr_sequencia,
	substr(obter_valor_dominio(4957,3),1,100)  ds_consistencia
from 	proj_projeto a
where	not exists ( select	1
		     from	proj_documento x
		     where	x.nr_seq_proj = a.nr_sequencia
		     and	nr_seq_tipo_documento = 14)
and	a.DT_INICIO_PREV <= sysdate;
/