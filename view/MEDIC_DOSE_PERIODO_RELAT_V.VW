create or replace view medic_dose_periodo_relat_v
as select  substr(decode(obter_ficha_tecnica_medic(a.cd_material), 0, obter_desc_material(obter_mat_generico(a.cd_material)), obter_princ_ativo_matmed(a.cd_material)),1,255) ds_princ_ativo,
            		substr(obter_desc_material(a.cd_material), 1, 255) ds_medicamento,
            		a.ds_potencia ds_potencia,
		(select ds_nome_amigavel from forma_dosagem_ger where cd_codigo_ifa = a.ds_forma_medicamento) ds_form,
	    decode(a.qt_dose_manha, null, to_char(a.qt_dose_manha), converte_fracao_dose(b.cd_unidade_medida_consumo, a.qt_dose_manha)) qt_manha,
	    decode(a.qt_dose_almoco, null, to_char(a.qt_dose_almoco), converte_fracao_dose(b.cd_unidade_medida_consumo, a.qt_dose_almoco)) qt_almoco,
		decode(a.qt_dose_tarde, null, to_char(a.qt_dose_tarde), converte_fracao_dose(b.cd_unidade_medida_consumo, a.qt_dose_tarde)) qt_tarde,
		decode(a.qt_dose_noite, null, to_char(a.qt_dose_noite), converte_fracao_dose(b.cd_unidade_medida_consumo, a.qt_dose_noite)) qt_noite,
		a.ds_unidade_medida ds_unidade_medida,
		a.ds_observacao ds_observacao,
		a.ds_motivo ds_motivo,
		a.ds_horario_especial ds_intervalo,
		a.nr_seq_versao nr_seq_versao,
		a.ie_classificacao cd_classificacao,
		a.ds_descricao_complementar ds_complementar,
		a.cd_material cd_material,
		a.ds_orientacao ds_orientacao,
		a.nr_sequencia nr_sequencia
	from	paciente_medic_uso a,
          		material b
	where 	b.cd_material      = a.cd_material
	and   	((nvl(a.qt_dose_manha, 0) != 0
	or    	nvl(a.qt_dose_almoco, 0)  != 0
	or    	nvl(a.qt_dose_tarde, 0)   != 0
	or	nvl(a.qt_dose_noite, 0)   != 0)
	and   	a.ds_horario_especial   is null)
	union all
    	select	'' ds_princ_ativo,
	                substr(a.ds_medicamento, 1, 255) ds_medicamento,
	                a.ds_potencia ds_potencia,
	                (select ds_nome_amigavel from forma_dosagem_ger where cd_codigo_ifa = a.ds_forma_medicamento) ds_form,
	                to_char(a.qt_dose_manha) qt_manha,
	                to_char(a.qt_dose_almoco) qt_almoco,
	                to_char(a.qt_dose_tarde) qt_tarde,
	                to_char(a.qt_dose_noite) qt_noite,
            		a.cd_unidade_medida_plano cd_unidade_medida,
	                a.ds_observacao ds_observacao,
	                a.ds_motivo ds_motivo,
	                a.ds_horario_especial ds_intervalo,
	                a.nr_seq_versao nr_seq_versao,
	                a.ie_classificacao cd_classificacao,
	                a.ds_descricao_complementar ds_complementar,
	                a.cd_material cd_material,
	                a.ds_orientacao ds_orientacao,
					a.nr_sequencia nr_sequencia
	from  	paciente_medic_uso a
	where 	a.cd_material         is null
	and   	((nvl(a.qt_dose_manha, 0) != 0
	or	nvl(a.qt_dose_almoco, 0)  != 0
	or	nvl(a.qt_dose_tarde, 0)   != 0
	or	nvl(a.qt_dose_noite, 0)   != 0)
  	and   	a.ds_horario_especial   is null);
/
