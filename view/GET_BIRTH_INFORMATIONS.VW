create or replace view get_birth_informations as
		SELECT 	NVL2(z.ds_given_name, trim(substr(z.ds_given_name,1,40)), 'SIN INFORMACIÓN') C_04,
		NVL2(z.ds_family_name, trim(substr(z.ds_family_name,1,20)), 'SIN INFORMACIÓN') C_05,
		NVL2(z.ds_component_name_1, trim(substr(z.ds_component_name_1,1,20)), 'SIN INFORMACIÓN')C_06,
		NVL(b.cd_curp,'SIN INFORMACIÓN') C_07,
		lpad(NVL(obter_dados_cat_entidade(b.cd_pessoa_fisica,'CD_ENTIDADE'),'99'),2,'0') C_08,
		NVL(obter_dados_cat_municipio(b.cd_municipio_ibge,'CD_CAT_MUNICIPIO'),'999') C_09,
		NVL(TO_CHAR(b.dt_nascimento,'dd/mm/yyyy'),'99/99/9999') C_10,
		decode(b.dt_nascimento,null,'99',obter_idade(b.dt_nascimento,SYSDATE,'A')) C_11,
		DECODE(obter_se_indigina(b.nr_seq_cor_pele), 'S', 1, 2) C_12, 
		DECODE(b.nr_seq_lingua_indigena,null,'2','1') C_13,
		lpad(NVL(obter_dados_cat_lingua_indig(b.nr_seq_lingua_indigena,'CD_LINGUA_INDIGENA_MF'),'9999'),4,'0') C_14,
		DECODE(b.ie_estado_civil,'1','12','2','11','3','13','4','16','5','14','6','16','7','15','9','88','99') C_15,
		lpad(substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'TIPO_LOGRAD','C'),'99'),1,2),2,'0') C_16,		
		substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'RUA_VIALIDADE','D'),'SIN INFORMACIÓN'),1,80) C_17,
		substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'NUMERO','D'),'9999999999'),1,10) C_18,
		substr(get_info_end_endereco(c.nr_seq_pessoa_endereco,'COMPLEMENTO','D'),1,5) C_19,
		lpad(substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'TIPO_BAIRRO','C'),'99'),1,2),2,'0') C_20,
		substr(NVL(get_info_end_endereco(c.nr_seq_pessoa_endereco,'TIPO_BAIRRO','D'),'SIN INFORMACIÓN'),1,60) C_21,
		SUBSTR(trim(NVL(get_info_end_endereco(c.nr_seq_pessoa_endereco,'CODIGO_POSTAL','D'),'99999')),1,5) C_22,
		lpad(substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'ESTADO_PROVINCI','C'),'99'),1,2),2,'0') C_23,
		lpad(substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'MUNICIPIO','C'),'999'),1,3),3,'0') C_24,
		lpad(substr(nvl(get_info_end_endereco(c.nr_seq_pessoa_endereco,'LOCALIDADE_AREA','C'),'9999'),1,4),4,'0') C_25,
		substr(nvl(obter_telefone_pf_html5(b.cd_pessoa_fisica, 13),'999999999999999'),1,15) C_26,
		NVL(f.qt_gestacoes,0)+1 C_27,
		NVL(f.qt_natimortos,'99') C_28,
		(select decode(count(*),0,99,count(*)) from	nascimento x where	x.nr_atendimento= a.nr_atendimento and x.ie_unico_nasc_vivo = 'S') C_29,
		nvl(f.qt_filhos_vivos + (select count(n.nr_sequencia) from nascimento n where n.nr_atendimento = a.nr_atendimento and n.dt_inativacao is null and n.dt_liberacao is not null),'99') C_30,
		NVL(f.ie_cond_ult_nasc,'9') C_31,
		DECODE(f.ie_cond_ult_nasc,'2','0','3','0',DECODE(f.ie_ult_filho_vivo,'S','1','N','2','9')) C_32,
		'99/99/9999' C_33,
		NVL(g.nr_sequencia,'1') C_34,
		DECODE(f.ie_pre_natal,'S','1','2') C_35,
		DECODE(f.ie_pre_natal,'N','0', (
							CASE	
								WHEN (f.qt_sem_ig_ini_pre_natal < 14) THEN '1'
								WHEN (f.qt_sem_ig_ini_pre_natal < 27) THEN '2'
								WHEN (f.qt_sem_ig_ini_pre_natal < 42) THEN '3'
								ELSE '9' 
								END)) C_36,					
		DECODE(nvl(f.ie_pre_natal,'N'),'N','0',nvl(f.qt_consultas,0)) C_37,
		DECODE(OBTER_DECLARACAO_OBITO(a.nr_atendimento),NULL,'1','2') C_38,
		substr(DECODE(OBTER_DECLARACAO_OBITO(a.nr_atendimento),NULL,'',NVL(OBTER_DECLARACAO_OBITO(a.nr_atendimento), '999999999')),1,9) C_39,
		LPAD(NVL(obter_convenio_atend_mx(a.nr_atendimento,1,'CD_DER_NACIMIENTO'),'01'),2,'0') C_40,
		decode(obter_convenio_atend_mx(a.nr_atendimento,1,'CD_DER_NACIMIENTO'), '07', '', lpad(b.nr_spss,18,'0')) C_41,
		lpad(DECODE(NVL(obter_convenio_atend_mx(a.nr_atendimento,1,'CD_DER_NACIMIENTO'),'01'),'01','00', obter_convenio_atend_mx(a.nr_atendimento,2,'CD_DER_NACIMIENTO')), 2, '0') C_42,
		DECODE(b.ie_grau_instrucao,'1','01','2','03','3','05','4','07','5','10','6','10','7','02','8','05','9','06','10','03','11','01','12','10','13','10','14','88','15','08','99') C_43,
		SUBSTR(NVL(obter_desc_cargo(b.cd_cargo),'SIN INFORMACIÓN'),1,40) C_44,
		nvl((SELECT max(t.cd_ocupacao_nascimento) FROM cargo w, cat_ocupacao_hab t WHERE w.cd_cargo = b.cd_cargo and t.cd_ocupacao = w.cd_externo),'99') C_45,
		DECODE((SELECT max(t.cd_ocupacao_nascimento) FROM cargo w, cat_ocupacao_hab t WHERE w.cd_cargo = b.cd_cargo and t.cd_ocupacao = w.cd_externo),'01','0','02','0','03','0','04','0',
			DECODE(b.cd_cargo,NULL,'2','1')) C_46,
	a.nr_atendimento nr_atendimento,
	g.nr_sequencia nr_seq_nascimento
FROM	atendimento_paciente a,
		pessoa_fisica b,
		compl_pessoa_fisica c,
		parto f,
		nascimento g,
		person_name z
WHERE a.cd_pessoa_fisica	= b.cd_pessoa_fisica
AND 	b.nr_seq_person_name	= z.nr_sequencia
and		z.ds_type = 'main'
AND		a.cd_pessoa_fisica	= c.cd_pessoa_fisica
AND		c.ie_tipo_complemento	= 1
AND		a.nr_atendimento	= f.nr_atendimento
AND		a.nr_atendimento	= g.nr_atendimento(+);
/