CREATE OR REPLACE VIEW SUBSECAO_GRAF_V AS
  SELECT
                c.nr_seq_linked_data LINKED_DATA,
                c.ds_nome SECTION_TITLE,
                d.nr_sequencia ROW_ID,
                d.ds_informacao ROW_DESCRIPTION,
                d.nm_atributo ROW_FIELD,
                d.nm_tabela ROW_TABLE,
                d.nr_seq_apresentacao ,
                c.nr_sequencia id_secao,
                d.ds_total_dose,
                d.ds_unidade_medida,
                d.ds_material,
                d.ds_informacao_complementar,
                d.ds_linha_complementar,
                d.ie_temporario,
                d.ie_timeout_sv,
                d.nr_altura,
                d.vl_min,
                d.vl_max,
                d.vl_med,
				d.ds_cor,
				d.ie_symbol_legend,
				d.ie_style_legend,
				d.ie_tipo,
				d.ie_tipo_agrupamento,
				d.cd_tipo_escala,
				d.vl_intervalo_escala,
				d.ds_cor_escala
             FROM pepo_modelo a,
                  w_flowsheet_cirurgia_pac b,
                  w_flowsheet_cirurgia_grupo c,
                  w_flowsheet_cirurgia_info d
             WHERE
                a.NR_SEQUENCIA           = b.nr_seq_modelo
                AND b.nr_sequencia       = c.nr_seq_flowsheet
                AND c.nr_sequencia       = d.nr_seq_grupo
                AND nvl(d.ie_visivel, 'S') = 'S'
             ORDER BY d.nr_seq_apresentacao, d.ds_informacao;
/
