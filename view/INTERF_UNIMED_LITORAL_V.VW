CREATE OR REPLACE VIEW INTERF_UNIMED_LITORAL_V
AS
SELECT	0					TP_REGISTRO,
	C.NR_SEQ_PROTOCOLO			NR_SEQ_PROTOCOLO,
	SYSDATE					DT_ENVIO,
	A.CD_CGC_HOSPITAL			CD_PRESTADOR,
	'1' || TO_CHAR(A.DT_REMESSA,'YYYYMM')	NR_LOTE,
	B.NR_INTERNO_CONTA			NR_DOCUMENTO,
	C.NR_DOC_CONVENIO			NR_DOC_ORIGEM,
	SUBSTR(C.CD_USUARIO_CONVENIO,1,3)	NR_UNIMED_ORIGEM,
	TO_CHAR(D.DT_MESANO_REFERENCIA,'YYYYMM') DT_COMPETENCIA,
	DECODE(C.IE_CARATER_INTER,'5','E', DECODE(C.IE_TIPO_ATENDIMENTO,8,'A',1,'I',' ')) IE_TIPO_DOCUMENTO,
	TO_CHAR(C.DT_ENTRADA,'DDMMYYYY')	DT_EMISSAO,
	TO_CHAR(C.DT_ENTRADA,'DDMMYYYY')	DT_ENTRADA,
	TO_CHAR(C.DT_ALTA,'DDMMYYYY')		DT_ALTA,
	TO_CHAR(C.DT_ENTRADA,'HHMI')		HR_ENTRADA,
	TO_CHAR(C.DT_ALTA,'HHMI')		HR_ALTA,
	C.CD_USUARIO_CONVENIO			CD_USUARIO_CONVENIO,
	C.NM_PACIENTE				NM_PACIENTE,
	C.CD_PLANO_CONVENIO			CD_PLANO,
	C.UF_CRM_MEDICO_RESP			UF_MEDICO_SOLIC,
	C.NR_CRM_MEDICO_RESP			CD_MEDICO_SOLIC,
	C.CD_CID_PRINCIPAL			CD_CID_PRINCIPAL,
	C.CD_CID_PRINCIPAL			CD_CID_DEFINITIVO,
	C.CD_MOTIVO_ALTA			IE_TIPO_ALTA,
	2					IE_TIPO_OBITO,
	' '					IE_TIPO_TRANSFERENCIA,
	' '					UF_HOSPITAL,
	' '					CD_HOSPITAL,
	C.IE_CARATER_INTER			IE_CARATER_INTER,
	C.CD_PROC_PRINCIPAL			CD_PROC_PRINCIPAL,
	C.CD_TIPO_ACOMODACAO			CD_TIPO_ACOMODACAO,
	C.IE_CLINICA				IE_CLINICA,
	(SELECT NVL(MAX(QT_GEMELAR_VIVOS),0) FROM NASCIMENTO WHERE NR_ATENDIMENTO = C.NR_ATENDIMENTO) QT_NASC_VIVOS,
	(SELECT NVL(MAX(QT_GEMELAR_MORTOS),0) FROM NASCIMENTO WHERE NR_ATENDIMENTO = C.NR_ATENDIMENTO) QT_NASC_MORTOS,
	0					QT_NASC_VIVOS_PAT,
	' '					CD_CID_VIVOS1,
	' '					CD_CID_VIVOS2,
	B.CD_MEDICO_EXECUTOR			CD_MEDICO_EXECUTOR,
	B.CD_FUNCAO_EXECUTOR			IE_TIPO_PARTICIPACAO,
	SUBSTR(B.CD_ITEM,1,7)			CD_ITEM,
	B.QT_ITEM				QT_ITEM,
	B.VL_TOTAL_ITEM				VL_ITEM,
	B.IE_VIA_ACESSO				IE_CIRURGIA_MULT,
	B.CD_SENHA_GUIA				CD_SENHA,
	B.QT_FILME				QT_FILME,
	B.VL_FILME				VL_FILME,	
	B.VL_CUSTO_OPER				VL_CUSTO_OPER,
	B.VL_HONORARIO				VL_HONORARIO,
	C.IE_SEXO				IE_SEXO,
	C.DT_NASCIMENTO				DT_NASCIMENTO,
	(SELECT MAX(CD_MOEDA) FROM COTACAO_MOEDA_CONVENIO WHERE CD_CONVENIO = A.CD_CONVENIO AND NVL(IE_SITUACAO,'A') = 'A') CD_MOEDA,
	0					VL_GLOSA
FROM	CONTA_PACIENTE D,
	W_INTERF_CONTA_HEADER A,
	W_INTERF_CONTA_ITEM B,
	W_INTERF_CONTA_CAB C
WHERE	A.NR_SEQ_PROTOCOLO	= C.NR_SEQ_PROTOCOLO
AND	B.NR_INTERNO_CONTA	= C.NR_INTERNO_CONTA
AND	C.NR_INTERNO_CONTA	= D.NR_INTERNO_CONTA
UNION ALL
SELECT	1					TP_REGISTRO,
	A.NR_SEQ_PROTOCOLO			NR_SEQ_PROTOCOLO,
	SYSDATE					DT_ENVIO,
	' '					CD_PRESTADOR,
	' '					NR_LOTE,
	999999999				NR_DOCUMENTO,
	' '					NR_DOC_ORIGEM,
	' '					NR_UNIMED_ORIGEM,
	TO_CHAR(SYSDATE,'DDMMYY')		DT_COMPETENCIA,
	' '					IE_TIPO_DOCUMENTO,
	TO_CHAR(SYSDATE,'DDMMYYYY')		DT_EMISSAO,
	TO_CHAR(SYSDATE,'DDMMYYYY')		DT_ENTRADA,
	TO_CHAR(SYSDATE,'DDMMYYYY')		DT_ALTA,
	TO_CHAR(SYSDATE,'HHMI')			HR_ENTRADA,
	TO_CHAR(SYSDATE,'HHMI')			HR_ALTA,
	' '					CD_USUARIO_CONVENIO,
	' '					NM_PACIENTE,
	' '					CD_PLANO,
	' '					UF_MEDICO_SOLIC,
	' '					CD_MEDICO_SOLIC,
	' '					CD_CID_PRINCIPAL,
	' '					CD_CID_DEFINITIVO,
	0					IE_TIPO_ALTA,
	0					IE_TIPO_OBITO,
	' '					IE_TIPO_TRANSFERENCIA,
	' '					UF_HOSPITAL,
	' '					CD_HOSPITAL,
	' '					IE_CARATER_INTER,
	' '					CD_PROC_PRINCIPAL,
	0					CD_TIPO_ACOMODACAO,
	0					IE_CLINICA,
	0					QT_NASC_VIVOS,
	0					QT_NASC_MORTOS,
	0					QT_NASC_VIVOS_PAT,
	' '					CD_CID_VIVOS1,
	' '					CD_CID_VIVOS2,
	' '					CD_MEDICO_EXECUTOR,
	0					IE_TIPO_PARTICIPACAO,
	' '					CD_ITEM,
	0					QT_ITEM,
	0					VL_ITEM,
	' '					IE_CIRURGIA_MULT,
	' '					CD_SENHA,
	0					QT_FILME,
	0					VL_FILME,	
	0					VL_CUSTO_OPER,
	0					VL_HONORARIO,
	' '					IE_SEXO,
	SYSDATE					DT_NASCIMENTO,
	0					CD_MOEDA,
	0					VL_GLOSA
FROM	W_INTERF_CONTA_TRAILLER A;
/