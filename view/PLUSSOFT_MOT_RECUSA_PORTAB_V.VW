create or replace view plussoft_mot_recusa_portab_v
as
	select  nr_sequencia	nr_seq_motivo,
		ds_motivo
	from	pls_portab_motivo_recusa
	where	nvl(ie_situacao,'A') = 'A'
	order by 1;
/
