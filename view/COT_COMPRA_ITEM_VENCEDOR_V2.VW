CREATE OR REPLACE VIEW COT_COMPRA_ITEM_VENCEDOR_V2
/* esta view foi criada para substituar a COT_COMPRA_ITEM_VENCEDOR_V
devido a altera��o das PK'S das tabelas d3e cota��o*/
AS
SELECT	I.NR_SEQUENCIA,
	i.nr_cot_compra,
	i.nr_item_cot_compra,
	DECODE(Obter_Venc_Cot_Fornec_item(i.nr_cot_compra, i.nr_item_cot_compra), i.nr_sequencia, 'S','N') ie_vencedor,
	DECODE(Obter_Venc_Cot_Fornec_item(i.nr_cot_compra, i.nr_item_cot_compra),
			i.nr_sequencia, i.vl_presente, 0) vl_vencedor
FROM	COT_COMPRA_FORN_ITEM I
WHERE	I.VL_UNITARIO_MATERIAL > 0;
/