CREATE OR REPLACE VIEW Eis_Tipo_Convenio_v
AS 
SELECT campo_numerico(VL_DOMINIO)	cd_tipo_convenio,
       substr(DS_VALOR_DOMINIO,1,60) ds_tipo_convenio
FROM VALOR_DOMINIO
WHERE CD_DOMINIO = 11;