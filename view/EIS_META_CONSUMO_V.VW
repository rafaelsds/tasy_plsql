create or replace
view eis_meta_consumo_v as
select	a.dt_referencia,
	a.cd_estabelecimento,
	a.cd_grupo_material,
	SUBSTR(obter_desc_grupo_mat(a.cd_grupo_material),1,80) ds_grupo_material,
	a.vl_consumo vl_meta,
	SUM(b.vl_estoque) vl_consumo,
	(dividir((SUM(b.vl_estoque) - a.vl_consumo), a.vl_consumo) * 100) pr_percentual
from	eis_meta_consumo a,
	eis_consumo_matmed_v b
where	a.cd_estabelecimento = b.cd_estabelecimento
and	a.cd_grupo_material = b.cd_grupo_material
and	a.dt_referencia = b.dt_mesano_referencia
group by
	a.dt_referencia,
	a.cd_estabelecimento,
	a.cd_grupo_material,
	a.vl_consumo;
/