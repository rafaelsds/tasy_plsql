create or replace view hmcc_relat_78946_v as
select	a.nm_exame,
	nvl(x.cd_unidade_medida, a.ds_unidade_medida) ds_unidade_medida,
	substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 1),1,255) || ' a ' || substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 2),1,255) vl_referencia,
	substr(obter_desc_material_exame_lab(b.cd_material_exame),1,255) ds_material,
	obter_metodo_exame_lab(e.nr_seq_metodo,2) ds_metodo,
	e.ds_resultado	ds_resultado_varchar,
	null		ds_resultado_number,
	a.nr_seq_grupo,
	c.nr_atendimento,
	b.dt_resultado,
	a.nr_seq_exame
from	exame_laboratorio a,
	prescr_procedimento b,
	prescr_medica c,
	lab_unidade_medida x,
	exame_lab_resultado d,
	exame_lab_result_item e
where	a.nr_seq_exame = b.nr_seq_exame
and	b.nr_prescricao = c.nr_prescricao
and	a.nr_seq_unid_med = x.nr_sequencia
and	d.nr_seq_resultado = e.nr_seq_resultado
and	e.nr_seq_prescr = b.nr_sequencia
and	d.nr_prescricao = b.nr_prescricao
and	d.nr_prescricao = c.nr_prescricao
and	b.ie_status_atend >= 35
and	(a.ie_formato_resultado = 'D' or a.ie_formato_resultado = 'DD')
union
select	a.nm_exame,
	nvl(x.cd_unidade_medida, a.ds_unidade_medida) ds_unidade_medida,
	substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 1),1,255) || ' a ' || substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 2),1,255) vl_referencia,
	substr(obter_desc_material_exame_lab(b.cd_material_exame),1,255) ds_material,
	obter_metodo_exame_lab(e.nr_seq_metodo,2) ds_metodo,
	e.ds_resultado || ' ' || e.qt_resultado	ds_resultado_varchar,
	null					ds_resultado_number,
	a.nr_seq_grupo,
	c.nr_atendimento,
	b.dt_resultado,
	a.nr_seq_exame
from	exame_laboratorio a,
	prescr_procedimento b,
	prescr_medica c,
	lab_unidade_medida x,
	exame_lab_resultado d,
	exame_lab_result_item e
where	a.nr_seq_exame = b.nr_seq_exame
and	b.nr_prescricao = c.nr_prescricao
and	a.nr_seq_unid_med = x.nr_sequencia
and	d.nr_seq_resultado = e.nr_seq_resultado
and	e.nr_seq_prescr = b.nr_sequencia
and	d.nr_prescricao = b.nr_prescricao
and	d.nr_prescricao = c.nr_prescricao
and	b.ie_status_atend >= 35
and	a.ie_formato_resultado = 'DV'
union
select	a.nm_exame,
	nvl(x.cd_unidade_medida, a.ds_unidade_medida) ds_unidade_medida,
	substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 1),1,255) || ' a ' || substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 2),1,255) vl_referencia,
	substr(obter_desc_material_exame_lab(b.cd_material_exame),1,255) ds_material,
	obter_metodo_exame_lab(e.nr_seq_metodo,2) ds_metodo,
	''		ds_resultado_varchar,
	e.qt_resultado	ds_resultado_number,
	a.nr_seq_grupo,
	c.nr_atendimento,
	b.dt_resultado,
	a.nr_seq_exame
from	exame_laboratorio a,
	prescr_procedimento b,
	prescr_medica c,
	lab_unidade_medida x,
	exame_lab_resultado d,
	exame_lab_result_item e
where	a.nr_seq_exame = b.nr_seq_exame
and	b.nr_prescricao = c.nr_prescricao
and	a.nr_seq_unid_med = x.nr_sequencia
and	d.nr_seq_resultado = e.nr_seq_resultado
and	e.nr_seq_prescr = b.nr_sequencia
and	d.nr_prescricao = b.nr_prescricao
and	d.nr_prescricao = c.nr_prescricao
and	b.ie_status_atend >= 35
and	a.ie_formato_resultado = 'V'
union
select	a.nm_exame,
	nvl(x.cd_unidade_medida, a.ds_unidade_medida) ds_unidade_medida,
	substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 1),1,255) || ' a ' || substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 2),1,255) vl_referencia,
	substr(obter_desc_material_exame_lab(b.cd_material_exame),1,255) ds_material,
	obter_metodo_exame_lab(e.nr_seq_metodo,2) ds_metodo,
	''					ds_resultado_varchar,
	nvl(e.qt_resultado, e.pr_resultado)	ds_resultado_number,
	a.nr_seq_grupo,
	c.nr_atendimento,
	b.dt_resultado,
	a.nr_seq_exame
from	exame_laboratorio a,
	prescr_procedimento b,
	prescr_medica c,
	lab_unidade_medida x,
	exame_lab_resultado d,
	exame_lab_result_item e
where	a.nr_seq_exame = b.nr_seq_exame
and	b.nr_prescricao = c.nr_prescricao
and	a.nr_seq_unid_med = x.nr_sequencia
and	d.nr_seq_resultado = e.nr_seq_resultado
and	e.nr_seq_prescr = b.nr_sequencia
and	d.nr_prescricao = b.nr_prescricao
and	d.nr_prescricao = c.nr_prescricao
and	b.ie_status_atend >= 35
and	a.ie_formato_resultado = 'VP'
union
select	a.nm_exame,
	nvl(x.cd_unidade_medida, a.ds_unidade_medida) ds_unidade_medida,
	substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 1),1,255) || ' a ' || substr(lab_obter_valor_referencia(c.nr_prescricao, b.nr_sequencia, b.nr_seq_exame, 2),1,255) vl_referencia,
	substr(obter_desc_material_exame_lab(b.cd_material_exame),1,255) ds_material,
	obter_metodo_exame_lab(e.nr_seq_metodo,2) ds_metodo,
	''		ds_resultado_varchar,
	e.pr_resultado	ds_resultado_number,
	a.nr_seq_grupo,
	c.nr_atendimento,
	b.dt_resultado,
	a.nr_seq_exame
from	exame_laboratorio a,
	prescr_procedimento b,
	prescr_medica c,
	lab_unidade_medida x,
	exame_lab_resultado d,
	exame_lab_result_item e
where	a.nr_seq_exame = b.nr_seq_exame
and	b.nr_prescricao = c.nr_prescricao
and	a.nr_seq_unid_med = x.nr_sequencia
and	d.nr_seq_resultado = e.nr_seq_resultado
and	e.nr_seq_prescr = b.nr_sequencia
and	d.nr_prescricao = b.nr_prescricao
and	d.nr_prescricao = c.nr_prescricao
and	b.ie_status_atend >= 35
and	a.ie_formato_resultado = 'P'
order by 1;
/