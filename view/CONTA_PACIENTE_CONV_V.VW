CREATE OR REPLACE VIEW Conta_Paciente_Conv_V
AS
select	a.*,
		Obter_Nome_Item_Convenio(a.cd_convenio,a.ie_proc_mat,a.cd_item,
						a.ie_origem_proced,'N', null, null, null, null) ds_item_convenio,
		nvl(b.cd_unidade_convenio,a.ds_unidade) ds_unidade_medida,
		Obter_Grupo_Item_Convenio(a.cd_convenio,a.ie_proc_mat,a.cd_item,
						a.ie_origem_proced, null, null, null) cd_grupo_convenio
from		conta_paciente_v a,
		conversao_unidade_medida b
where		a.cd_convenio			= b.cd_convenio(+)
and		a.ds_unidade			= b.cd_unidade_medida(+);
/
