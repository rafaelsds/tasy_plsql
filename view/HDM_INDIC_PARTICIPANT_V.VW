create or replace view
HDM_INDIC_PARTICIPANT_V as
/* Esta view ir� conter a jun��o de todas as tabelas de fato e dimens�o do modelo de participantes */
SELECT	/* Dados m�s */
	b.nr_year,       
	b.nr_month,
	b.nr_semester,
	b.nr_quarter,  
	b.dt_complete_date dt_month,
	/* Dados grupo paciente */ 
	a.nr_seq_patient_group,
	d.si_sex,
	d.ds_sex,
	d.nm_age_range,
	d.ds_participant,
	d.si_participant,
	/* Dados programa */
	e.nr_sequencia nr_seq_dm_program,
	e.nr_seq_program nr_seq_program, 
	e.nm_program,
	e.nm_program_module nm_program_module, 
	/* Dados campanha */
	g.nr_sequencia nr_seq_campaign,
	g.nm_campaign, 
	/* Dados grupo atividade */
	i.nr_sequencia nr_seq_dm_group,
	i.nm_group, 
	i.nm_class nm_group_class, 
	i.nr_seq_turma,
	/* Patologia */
	n.nr_sequencia nr_seq_disease_risk,
	n.nm_disease_risk, 
	/* Dados detalhes paciente */
	k.si_situation,
	k.si_status,
	k.ds_situation,
	k.ds_status,
	/* Dados profissional*/
	p.nm_responsability,
	p.nm_professional,
	p.nm_team,
	/* Dados regra */
	r.nm_rule,
	/* Dados fato */
	a.nr_sequencia nr_seq_fact,
	a.si_beneficiary,
	a.ds_unique
FROM	hdm_indic_dm_search_rule r,
	hdm_indic_dm_risk_disease n,
	hdm_indic_dm_program e,
	hdm_indic_dm_campaign g,
	hdm_indic_dm_activ_group i,
	hdm_indic_dm_professional p,
	hdm_indic_ft_partic_sr s,
	hdm_indic_ft_partic_rd o,
	hdm_indic_ft_partic_pr f,
	hdm_indic_ft_partic_cp h,
	hdm_indic_ft_partic_ag j,
	hdm_indic_ft_partic_pf q,
	hdm_indic_dm_partic_det k,
	hdm_indic_dm_patient_group d,
	hdm_indic_dm_month b,
	hdm_indic_ft_partic a
WHERE	1 = 1
AND	f.nr_seq_dimension = e.nr_sequencia
AND	h.nr_seq_dimension = g.nr_sequencia
AND	j.nr_seq_dimension = i.nr_sequencia
AND	q.nr_seq_dimension = p.nr_sequencia
AND	s.nr_seq_dimension = r.nr_sequencia
AND	o.nr_seq_dimension = n.nr_sequencia
AND	a.nr_sequencia = f.nr_seq_fact
AND	a.nr_sequencia = h.nr_seq_fact
AND	a.nr_sequencia = j.nr_seq_fact
AND	a.nr_sequencia = q.nr_seq_fact
AND	a.nr_sequencia = o.nr_seq_fact
AND	a.nr_sequencia = s.nr_seq_fact
AND	a.nr_seq_partic_det = k.nr_sequencia
AND	a.nr_seq_patient_group = d.nr_sequencia
AND	b.nr_sequencia = a.nr_seq_month;
/