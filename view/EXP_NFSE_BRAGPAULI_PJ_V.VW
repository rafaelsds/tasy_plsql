CREATE OR REPLACE 
VIEW EXP_NFSE_BRAGPAULI_PJ_V AS
select	nr_sequencia						nr_nota_fiscal,
	'C'							tp_registro,
	n.cd_serie_nf						cd_serie_nf,
	n.dt_emissao						dt_emissao,
	decode(n.ie_situacao,1,1,2)					ie_situacao_nota,	        
	lpad(elimina_caracteres_especiais(n.vl_total_nota),12,0)		vl_total_nota,
	lpad(elimina_caractere_especial(obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'), 
	obter_procedimento_nfse(n.nr_sequencia,'O')),'CD')),10,' ') 		cd_servico,
	2							ie_tipo_toma_prest,
	decode (upper(elimina_caracteres_especiais(obter_dados_pf_pj(null, cd_cgc, 'CI'))),
	(upper(elimina_caracteres_especiais(obter_dados_pf_pj(null,(obter_dados_nota_fiscal(n.nr_sequencia,2)),'CI')))),'S','N') ds_local_tomador,
	substr(obter_nome_pf_pj(n.cd_pessoa_fisica,n.cd_cgc),1,75) 	nm_tomador,
	decode(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'IM'),1,10),'X'),'X','Isento',
	substr(obter_dados_pf_pj(null,n.cd_cgc,'IM'),1,10))	nr_insc_munic_tomador,
	'  '							ds_digito_insc_municipal,
	n.cd_cgc							cd_cgc_tomador,
	decode(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'IE'),1,20),'X'),'X','S','N') ds_insc_estad_tomador,
	substr(obter_dados_pf_pj(null,n.cd_cgc,'IE'),1,20)	nr_insc_estad_tomador,
	nvl(to_char(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'CEP'),1,15),'N/D',
	to_char(somente_numero(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CEP'),1,15))),
	substr(obter_compl_pj(n.cd_cgc,1,'CEP'),1,15)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'CEP'),1,15))),
	to_char(somente_numero(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CEP'),1,15)))) cd_cep_tomador,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'LO'),1,5)		ds_tipo_logradouro,
	lpad(' ',5,' ')						ds_titulo_logradouro,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'R'),1,50)		ds_logradouro_tomador,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'CO'),1,40)		ds_complemento_tomador,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'NR'),1,10)		nr_endereco_tomador,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'B'),1,50)		ds_bairro_tomador,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'UF'),1,2)		sg_uf_tomador,
	substr(obter_dados_pf_pj(NULL,n.cd_cgc,'CI'),1,50)		ds_municipio_tomador,
	'D'							ds_local_prest_servico,
	n.cd_estabelecimento					cd_estabelecimento
from	nota_fiscal n,
	operacao_nota o
where	n.cd_operacao_nf = o.cd_operacao_nf
and	o.ie_servico = 'S'
and	n.cd_cgc is not null
and	n.cd_pessoa_fisica is null
and	(substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'S');
/
