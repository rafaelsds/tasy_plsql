create or replace
view ccih_infeccoes_v
as
select	c.nr_atendimento,
	e.dt_infeccao,
	obter_nome_pessoa_fisica(f.cd_pessoa_fisica,'') nm_paciente,
	nvl(a.ds_cirurgia,c.ds_diagnostico_int) ds_diagnostico,
	d.ds_clinica,
	y.ds_topografia,
	y.cd_topografia,
	obter_nome_medico(nvl(a.cd_medico_cirurgiao,c.cd_medico),'G') nm_guerra,
	k.ds_clinica espec_medico,
	d.ie_ordem_ss,
	nvl(a.cd_medico_cirurgiao,c.cd_medico) cd_medico_executor,
	k.cd_clinica cd_clinica_medico,
	d.cd_clinica cd_clinica_proc,
	b.cd_tipo_cirurgia,
	c.dt_cancelamento,
	e.ds_observacao
from	cih_cirurgia a,
	cih_tipo_cirurgia b,
	cih_ficha_ocorrencia c,
	cih_clinica d,
	cih_local_infeccao e,
	cih_clinica k,
	atendimento_paciente f,
	cih_caso_infeccao g,
	cih_topografia y
where	a.cd_tipo_cirurgia	= b.cd_tipo_cirurgia(+)
and	c.nr_ficha_ocorrencia	= a.nr_ficha_ocorrencia(+)
and	g.cd_caso_infeccao	= e.cd_caso_infeccao
and	k.cd_clinica		= c.cd_clinica
and	trunc(c.dt_ficha)	= trunc(a.dt_cirurgia(+))
and	y.cd_topografia		= e.cd_topografia
and	d.cd_clinica		= e.cd_clinica
and	g.cd_caso_infeccao	= e.cd_caso_infeccao
and	f.nr_atendimento	= c.nr_atendimento
and	e.nr_ficha_ocorrencia	= c.nr_ficha_ocorrencia;
/