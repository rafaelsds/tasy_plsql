create or replace view sentry_score_v as
select   c.nr_atendimento,
		 d.fld_nm_chart_first_val,
		 d.fld_nm_chart_second_val,
         d.fld_nm_second_col_content,
         suep_severidade_sentry(d.fld_nm_third_col_content, 'T') fld_nm_second_col_sub_info,
         nvl(to_char(d.fld_nm_third_col_content), '--') fld_nm_third_col_content,
         null fld_nm_first_col_content,
		 d.fld_nm_chart_first_val || ' - ' || d.fld_nm_chart_second_val fld_nm_chart_tooltip,
         suep_severidade_sentry(d.fld_nm_third_col_content, 'C') fld_nm_sub_content,
		 d.nr_delta_view
from     (select b.nr_atendimento from sentry_score b group by b.nr_atendimento) c,
         (
         select   a.nr_sequencia,
                  a.nr_atendimento,
                  a.dt_pontuacao fld_nm_second_col_content,
                  obter_pontuacao_sentry_score(a.nr_atendimento, 1) fld_nm_third_col_content,
                  a.dt_pontuacao,
				  obter_pontuacao_sentry_score(a.nr_atendimento, 2) fld_nm_chart_first_val,
				  obter_pontuacao_sentry_score(a.nr_atendimento, 1) fld_nm_chart_second_val,
				  obter_pontuacao_sentry_score(a.nr_atendimento, 1) -
				  obter_pontuacao_sentry_score(a.nr_atendimento, 2) nr_delta_view
         from     sentry_score a
         ) d
where    c.nr_atendimento = d.nr_atendimento and
         d.dt_pontuacao   = (select   max(b.dt_pontuacao)
                             from     sentry_score b
                             where    b.nr_atendimento = c.nr_atendimento) and
         d.nr_sequencia   = (select   max(b.nr_sequencia)
                             from     sentry_score b
                             where    b.dt_pontuacao   = d.dt_pontuacao);
/
