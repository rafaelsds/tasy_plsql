create or replace view eis_alergia_v  as 
select	distinct 
	obter_setor_atendimento(e.nr_atendimento) cd_setor_atendimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
	substr(obter_nome_convenio(obter_convenio_atendimento(a.nr_atendimento)),1,150) ds_convenio,
	obter_nome_setor(obter_setor_atendimento(e.nr_atendimento)) ds_setor_atendimento,
	obter_sexo_pf(a.cd_pessoa_fisica,'C') ie_sexo,
	obter_sexo_pf(a.cd_pessoa_fisica,'D') ds_sexo,
	e.cd_pessoa_fisica,
	substr(obter_idade(obter_data_nascto_pf(a.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria,
	trunc(e.dt_registro) dt_avaliacao,
	f.cd_empresa,
	a.cd_estabelecimento,
	a.nr_atendimento,
	a.cd_pessoa_fisica cd_paciente,
	substr(Obter_desc_alergeno(e.nr_seq_tipo),1,80) ds_alergeno,
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_paciente,
	e.nr_seq_tipo
from 	paciente_alergia e,
	estabelecimento f,
	atendimento_paciente a
where 	a.nr_atendimento= e.nr_atendimento
and	a.cd_estabelecimento = f.cd_estabelecimento
and	e.dt_liberacao is not null
and	e.dt_inativacao is null
and	nvl(ie_nega_alergias,'N') = 'N';
/
