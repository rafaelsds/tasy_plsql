CREATE OR REPLACE VIEW UNMD_ETIQUETA_APOIO_EXAMES_V3 AS
SELECT DISTINCT
usjc_lab_se_exame_urgente(c.nr_prescricao,b.nr_seq_grupo,h.nr_sequencia)ie_urgencia,
d.nr_atendimento,
    c.dt_prescricao,
                a.nr_sequencia,
                a.nr_controle_exame NR_SEQ_AMOSTRA,
                A.NR_SEQ_LAB,
                c.nr_prescricao,
                g.nr_seq_material,
                h.ds_material_exame,
                '('|| trim(to_char(r.nr_amostra,'09')) ||')' nr_amostra,
                TO_CHAR (SYSDATE, 'DD/MM/YYYY HH24:MI') dt_conferencia,
                (case when nvl(c.ie_recem_nato, 'N') = 'N' then obter_nome_pf (d.cd_pessoa_fisica)
                      when c.ie_recem_nato = 'S' then obter_nome_pf(c.cd_recem_nato)
                 end)  nm_pessoa_fisica,
--                obter_nome_pf(c.cd_recem_nato) nm_recem_nato,
--                obter_nome_pf (d.cd_pessoa_fisica) nm_pessoa_fisica,
--                (obter_idade_pf (d.cd_pessoa_fisica, SYSDATE, 'A')
--                 || 'A-'
--                 || obter_sexo_pf (d.cd_pessoa_fisica, 'C')) ds_anos,
                (case when nvl(c.ie_recem_nato, 'N') = 'N' then (obter_idade_pf (d.cd_pessoa_fisica, SYSDATE, 'A') || 'A-'|| obter_sexo_pf (d.cd_pessoa_fisica, 'C'))
                      when c.ie_recem_nato = 'S' then (obter_idade_pf (c.cd_recem_nato, SYSDATE, 'A') || 'A-'|| obter_sexo_pf (c.cd_recem_nato, 'C'))
                 end)  ds_anos,
                 'DN:'||obter_data_nascto_pf(d.cd_pessoa_fisica) ||'-'|| obter_sexo_pf (d.cd_pessoa_fisica, 'C') dt_nasc,
                 DECODE (i.cd_estabelecimento,
                           22,  'JAC',
                           23,  'JLG',
                           204, 'TIV',
                           24,  'SAT',
                           26,  'CAR',
                           4,   'MAT',
                           61,  'UBA',
                           2,   'PAS',
                           131, 'PAJ',
                           216, 'VIL',
                           20,  'SDL',
                            1,  'SDH'
                          )|| ' '|| LPAD (a.nr_prescricao, 7, '0') nr_protocolo,
                f.nr_seq_frasco,
                DECODE (ds_frasco,
                        '',
      h.ds_material_exame,
                        ds_frasco) ds_frasco,
                     trim(a.nr_controle_exame) cd_barras,
OBTER_SELECT_CONCATENADO_BV('
SELECT DISTINCT B.CD_EXAME
           FROM prescr_procedimento a,
                exame_laboratorio b,
                prescr_medica c,
                atendimento_paciente d,
                frasco_exame_lab e,
                exame_lab_frasco f,
                exame_lab_material g,
                material_exame_lab h,
                estabelecimento i,
                pessoa_juridica j,
                prescr_proc_mat_item p
          WHERE a.nr_prescricao = c.nr_prescricao
            AND c.nr_atendimento = d.nr_atendimento
            AND a.nr_seq_exame = b.nr_seq_exame
            AND f.nr_seq_exame(+) = b.nr_seq_exame
            AND f.nr_seq_frasco = e.nr_sequencia(+)
            AND h.nr_sequencia = g.nr_seq_material
            AND g.nr_seq_exame = a.nr_seq_exame
            AND h.cd_material_exame = a.cd_material_exame
            AND i.cd_estabelecimento = c.cd_estabelecimento
            AND i.cd_cgc = j.cd_cgc
            AND f.nr_seq_material = g.nr_seq_material
            AND a.ie_suspenso = ''N''
            AND a.cd_motivo_baixa = 0
            AND a.nr_prescricao=p.nr_prescricao
            AND a.nr_sequencia = p.nr_seq_prescr
            AND c.nr_prescricao = :P1
            AND trim(a.nr_controle_exame)=:P2
            AND nvl(b.nr_seq_grupo_imp,0)=nvl(:P3,0)
            ORDER BY B.CD_EXAME','P1='|| C.NR_PRESCRICAO ||';P2='|| trim(a.nr_controle_exame) ||';P3='|| b.nr_seq_grupo_imp ,';') DS_EXAMES,
            DECODE(MAX (NVL (nr_etiqueta, 1)),1,1,MAX (NVL (nr_etiqueta, 1))) nr_etiquetas, '' NM_FANTASIA,
            nvl(b.nr_seq_grupo_imp,0) NR_SEQ_GRUPO_IMP,
            B.NR_SEQ_GRUPO,
            a.ie_status_atend,
            c.cd_estabelecimento
           FROM prescr_procedimento a,
                exame_laboratorio b,
                prescr_medica c,
                atendimento_paciente d,
                frasco_exame_lab e,
                exame_lab_frasco f,
                exame_lab_material g,
                material_exame_lab h,
                pessoa_juridica j,
                estabelecimento i,
                estabelecimento t,
                prescr_proc_mat_item p,
                prescr_proc_material r
          WHERE a.nr_prescricao = c.nr_prescricao
            AND c.nr_atendimento = d.nr_atendimento
            AND a.nr_seq_exame = b.nr_seq_exame
            AND b.nr_seq_exame = f.nr_seq_exame(+)
            AND e.nr_sequencia(+) = f.nr_seq_frasco
            AND h.nr_sequencia = g.nr_seq_material
            AND g.nr_seq_exame = a.nr_seq_exame
            AND h.cd_material_exame = a.cd_material_exame
            AND i.cd_estabelecimento = c.cd_estabelecimento
            AND i.cd_cgc = j.cd_cgc
            AND f.nr_seq_material = g.nr_seq_material
            AND a.ie_suspenso = 'N'
            AND a.cd_motivo_baixa = 0
            AND d.cd_estabelecimento=t.cd_estabelecimento
            AND a.nr_prescricao=p.nr_prescricao
            AND a.nr_sequencia = p.nr_seq_prescr
            AND p.nr_seq_prescr_proc_mat=r.nr_sequencia
            AND a.nr_prescricao not in(select distinct pr.nr_prescricao
                                         from prescr_proc_mat_item pr, prescr_procedimento po
                                        where pr.nr_prescricao = po.nr_prescricao
                                          AND pr.nr_seq_prescr = po.nr_sequencia
                                          and a.nr_prescricao=po.nr_prescricao
                                          AND pr.ie_status = 10
                                          AND po.ie_suspenso = 'N'
                                          AND po.cd_motivo_baixa = 0)
GROUP BY
usjc_lab_se_exame_urgente(c.nr_prescricao,b.nr_seq_grupo,h.nr_sequencia),
c.dt_prescricao,
d.cd_pessoa_fisica,
a.nr_sequencia,
a.nr_controle_exame,
A.NR_SEQ_LAB,
c.nr_prescricao, g.nr_seq_material, '('|| trim(to_char(r.nr_amostra,'09')) ||')', h.ds_material_exame,
                TO_CHAR (SYSDATE, 'DD/MM/YYYY HH24:MI'),
                (case when nvl(c.ie_recem_nato, 'N') = 'N' then obter_nome_pf (d.cd_pessoa_fisica)
                      when c.ie_recem_nato = 'S' then obter_nome_pf(c.cd_recem_nato)
                 end),
--                obter_nome_pf(c.cd_recem_nato),
--                obter_nome_pf (d.cd_pessoa_fisica),
--                (   obter_idade_pf (d.cd_pessoa_fisica, SYSDATE, 'A')
--                 || 'A-'
--                 || obter_sexo_pf (d.cd_pessoa_fisica, 'C')
--                ),
                (case when nvl(c.ie_recem_nato, 'N') = 'N' then (obter_idade_pf (d.cd_pessoa_fisica, SYSDATE, 'A') || 'A-'|| obter_sexo_pf (d.cd_pessoa_fisica, 'C'))
                      when c.ie_recem_nato = 'S' then (obter_idade_pf (c.cd_recem_nato, SYSDATE, 'A') || 'A-'|| obter_sexo_pf (c.cd_recem_nato, 'C'))
                 end),
                DECODE (i.cd_estabelecimento,
                           22,  'JAC',
                           23,  'JLG',
                           204, 'TIV',
                           24,  'SAT',
                           26,  'CAR',
                           4,   'MAT',
                           61,  'UBA',
                           2,   'PAS',
                           131, 'PAJ',
                           216, 'VIL',
                           20,  'SDL',
                            1,  'SDH'
                          )
                || ' '
                || LPAD (a.nr_prescricao, 7, '0'),
                f.nr_seq_frasco,
                DECODE (ds_frasco,
                        '', h.ds_material_exame,
                        ds_frasco
                       ),
                     trim(to_char(p.nr_seq_prescr_proc_mat,'0999999999')),
OBTER_SELECT_CONCATENADO_BV('
SELECT DISTINCT B.CD_EXAME
           FROM prescr_procedimento a,
                exame_laboratorio b,
                prescr_medica c,
                atendimento_paciente d,
                frasco_exame_lab e,
                exame_lab_frasco f,
                exame_lab_material g,
                material_exame_lab h,
                estabelecimento i,
                pessoa_juridica j,
                prescr_proc_mat_item p
          WHERE a.nr_prescricao = c.nr_prescricao
            AND c.nr_atendimento = d.nr_atendimento
            AND a.nr_seq_exame = b.nr_seq_exame
            AND f.nr_seq_exame(+) = b.nr_seq_exame
            AND f.nr_seq_frasco = e.nr_sequencia(+)
            AND h.nr_sequencia = g.nr_seq_material
            AND g.nr_seq_exame = a.nr_seq_exame
            AND h.cd_material_exame = a.cd_material_exame
            AND i.cd_estabelecimento = c.cd_estabelecimento
            AND i.cd_cgc = j.cd_cgc
            AND f.nr_seq_material = g.nr_seq_material
            AND a.ie_suspenso = ''N''
            AND a.cd_motivo_baixa = 0
            AND a.nr_prescricao=p.nr_prescricao
            AND a.nr_sequencia = p.nr_seq_prescr
            AND c.nr_prescricao = :P1
            AND trim(a.nr_controle_exame)=:P2
            AND nvl(b.nr_seq_grupo_imp,0)=nvl(:P3,0)
            ORDER BY B.CD_EXAME','P1='|| C.NR_PRESCRICAO ||';P2='|| trim(a.nr_controle_exame) ||';P3='|| b.nr_seq_grupo_imp ,';'),
            t.nm_fantasia_estab,
            nvl(b.nr_seq_grupo_imp,0),
            B.NR_SEQ_GRUPO,
            a.ie_status_atend,
            c.cd_estabelecimento,
    d.nr_atendimento
       ORDER BY nr_seq_material, nr_seq_frasco
;
/
