create or replace view pls_interf_mens_superc_koch_v
as
select	DT_MESANO_REFERENCIA,
	nr_cpf_titular,
	ie_titularidade,
	nr_cpf_dependente,
	cd_usuario_plano,
	to_number(Campo_Mascara_virgula(vl_mensalidade)) vl_mensalidade,
	substr(to_char(decode(vl_coparticipacao,0,'0,00',Campo_Mascara_virgula(vl_coparticipacao))),1,255) vl_coparticipacao,
	nr_seq_titular,
	nm_usuario
from  (	select	c.DT_MESANO_REFERENCIA,
		d.nr_cpf nr_cpf_titular,
		'T' ie_titularidade,
		d.nr_cpf nr_cpf_dependente,
		e.cd_usuario_plano,
		nvl(c.vl_mensalidade,0) - nvl(c.vl_coparticipacao,0) vl_mensalidade,
		nvl(c.vl_coparticipacao,0) vl_coparticipacao,
		b.nr_sequencia nr_seq_titular,
		a.nm_usuario
	from	pessoa_fisica		d,
		w_pls_interface_mens	a,
		pls_segurado		b,
		pls_mensalidade_segurado c,
		pls_segurado_carteira	e
	where	b.cd_pessoa_fisica	= d.cd_pessoa_fisica
	and	e.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_seq_mensalidade_seg = c.nr_sequencia	
	and	b.nr_seq_titular is null
	union all
	select	c.DT_MESANO_REFERENCIA,
		(	select	max(x.nr_cpf)
			from	pessoa_fisica		x,
				pls_segurado		y
			where	y.cd_pessoa_fisica	= x.cd_pessoa_fisica
			and	y.nr_sequencia		= b.nr_seq_titular) nr_cpf_titular,
		'D' ie_titularidade,
		d.nr_cpf nr_cpf_dependente,
		e.cd_usuario_plano,
		nvl(c.vl_mensalidade,0) - nvl(c.vl_coparticipacao,0) vl_mensalidade,
		nvl(c.vl_coparticipacao,0) vl_coparticipacao,
		b.nr_seq_titular nr_seq_titular,
		a.nm_usuario
	from	pessoa_fisica		d,
		w_pls_interface_mens	a,
		pls_segurado		b,
		pls_mensalidade_segurado c,
		pls_segurado_carteira	e
	where	b.cd_pessoa_fisica	= d.cd_pessoa_fisica
	and	e.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_seq_segurado	= b.nr_sequencia
	and	a.nr_seq_mensalidade_seg = c.nr_sequencia
	and	b.nr_seq_titular is not null
)
order by nr_seq_titular, ie_titularidade desc;
/
