CREATE
	OR replace VIEW SCOLA_EXAME_PERMIT_APAC_V AS

	SELECT c.nr_seq_exame
	,l.cd_exame
	,l.nm_exame
FROM exame_laboratorio l
	,exame_lab_convenio c
	,sus_procedimento p
	,sus_procedimento_registro r
WHERE l.nr_seq_exame = c.nr_seq_exame
	AND c.cd_procedimento = p.cd_procedimento
	AND p.cd_procedimento = r.cd_procedimento
	AND r.cd_registro = 7
	AND c.cd_procedimento >= 500000000
/