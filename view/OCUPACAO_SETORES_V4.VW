create or replace view ocupacao_setores_v4 as
	select	1 cd_tipo_informacao,
		cd_classif_setor,
		cd_setor_atendimento,
		ds_setor_atendimento,
		nm_unidade,
		nr_unidades_setor,
		nr_unidades_ocupadas,
		nr_unidades_temporarias,
		(nr_unidades_ocupadas - nr_unid_temp_ocup) nr_unid_ocup,
		nr_unidades_reservadas,
		nr_unidades_higienizacao,
		nr_unidades_livres,
		nr_unid_temp_ocup,
		qt_unidade_acomp,
		qt_unidade_saida_inter,
		qt_unidades_isolamento,
		nr_unidades_interditadas,
		qt_unidades_alta,
		qt_unidade_chamad_manut,
		qt_unidade_manutencao,
		(nr_unidades_ocupadas - nr_unidades_higienizacao) qt_ocupadas,
		(nr_unidades_livres + qt_unidades_alta + nr_unidades_higienizacao) qt_livres,
		((nr_unidades_livres + qt_unidades_alta + nr_unidades_higienizacao) -
		 (nr_unid_temp_livres + qt_unid_temp_alta + nr_unid_temp_higienizacao)) qt_livres_sem_temp,
		(nr_unidades_setor - nr_unidades_temporarias) nr_unidades_normais,
		Obter_percetual_ocupacao (
			nr_unidades_setor,
			nr_unidades_temporarias,
			nr_unidades_ocupadas,
			qt_unidade_acomp,
			nr_unidades_interditadas,
			nr_unidades_livres,
			nr_unidades_higienizacao,
			nr_unidades_reservadas,
			qt_unidades_isolamento,
			qt_unidades_alta,
			nr_unid_temp_ocup,
			nr_unid_temp_ocupadas,
			qt_unid_temp_acomp,
			nr_unid_temp_interditadas,
			nr_unid_temp_livres,
			nr_unid_temp_higienizacao,
			nr_unid_temp_reservadas,
			qt_unid_temp_isolamento,
			qt_unid_temp_alta,
			qt_unidade_manutencao,
			'X') pr_ocupacao,
		dividir((nr_unidades_ocupadas + qt_unidade_acomp) * 100,nr_unidades_setor) pr_ocupacao_acomp,
		((dividir((nr_unidades_setor - qt_disponiveis), nr_unidades_setor) * 100)) pr_ocupacao_total,
		dividir((nr_unidades_ocupadas + qt_unidade_acomp + qt_unidades_isolamento + nr_unidades_interditadas +
			 nr_unidades_reservadas + nr_unid_temp_ocup) * 100 , (nr_unidades_setor + nr_unid_temp_ocup)) pr_ocupacao_total2,
		dividir(nr_unidades_ocupadas * 100, (nr_unidades_setor - nr_unidades_temporarias)) pr_olf,
		dividir((nr_unidades_ocupadas + nr_unid_temp_ocup) * 100,
		(nr_unidades_setor - nr_unidades_interditadas)) pr_ohr, qt_pac_isolado,
		(nr_unidades_setor - nr_unidades_interditadas) nr_unidades_tot_int,
		Obter_agrupamento_setor(cd_setor_atendimento) nr_seq_agrupamento
	from	ocupacao_setores_v2
	where	ie_ocup_hospitalar in ('S','M','T') and ie_situacao = 'A'
	union
	select	2,
		'0',
		0,
		'     ' || substr(obter_valor_dominio(1, cd_classif_setor),1,200) ds_setor_atendimento,
		'' nm_unidade,
		sum(nr_unidades_setor),
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,nr_unidades_ocupadas)) nr_unidades_ocupadas,
		sum(nr_unidades_temporarias),
		sum(nr_unidades_ocupadas - nr_unid_temp_ocup) nr_unid_ocup,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,nr_unidades_reservadas)) nr_unidades_reservadas,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,nr_unidades_higienizacao)) nr_unidades_higienizacao,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,nr_unidades_livres)) nr_unidades_livres,
		sum(nr_unid_temp_ocup),
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,qt_unidade_acomp)) qt_unidade_acomp,
		sum(DECODE(ie_ocup_hospitalar,'M',0,'T',0,qt_unidade_saida_inter)) qt_unidade_saida_inter,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,qt_unidades_isolamento)) qt_unidades_isolamento,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,nr_unidades_interditadas)) nr_unidades_interditadas,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,qt_unidades_alta)) qt_unidades_alta,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,qt_unidade_chamad_manut)) qt_unidade_chamad_manut,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,qt_unidade_manutencao)) qt_unidade_manutencao,
		(sum(nr_unidades_ocupadas) - sum(nr_unidades_higienizacao)) qt_ocupadas,
		(sum(nr_unidades_livres) + sum(qt_unidades_alta) + sum(nr_unidades_higienizacao)) qt_livres,
		((sum(nr_unidades_livres) + sum(qt_unidades_alta) + sum(nr_unidades_higienizacao)) -
		(sum(nr_unid_temp_livres) + sum(qt_unid_temp_alta) + sum(nr_unid_temp_higienizacao))) qt_livres_sem_temp,
		SUM(DECODE(ie_ocup_hospitalar,'M',0,'T',0,nr_unidades_setor - nr_unidades_temporarias)) nr_unidades_normais,
		Obter_percetual_ocupacao (
				sum(nr_unidades_setor),
				sum(nr_unidades_temporarias),
				sum(nr_unidades_ocupadas),
				sum(qt_unidade_acomp),
				sum(nr_unidades_interditadas),
				sum(nr_unidades_livres),
				sum(nr_unidades_higienizacao),
				sum(nr_unidades_reservadas),
				sum(qt_unidades_isolamento),
				sum(qt_unidades_alta),
				sum(nr_unid_temp_ocup),
				sum(nr_unid_temp_ocupadas),
				sum(qt_unid_temp_acomp),
				sum(nr_unid_temp_interditadas),
				sum(nr_unid_temp_livres),
				sum(nr_unid_temp_higienizacao),
				sum(nr_unid_temp_reservadas),
				sum(qt_unid_temp_isolamento),
				sum(qt_unid_temp_alta),
				sum(qt_unidade_manutencao),
				'X') pr_ocupacao,
		dividir((SUM(nr_unidades_ocupadas) + SUM(qt_unidade_acomp)) * 100,SUM(nr_unidades_setor)) pr_ocupacao_acomp,
		((dividir((sum(nr_unidades_setor) - sum(qt_disponiveis)), sum(nr_unidades_setor)) * 100)) pr_ocupacao_total,
		dividir((sum(nr_unidades_ocupadas) + sum(qt_unidade_acomp) + sum(qt_unidades_isolamento) + sum(nr_unidades_interditadas) +
			 sum(nr_unidades_reservadas) + sum(nr_unid_temp_ocup)) * 100 ,
			(sum(nr_unidades_setor) + sum(nr_unid_temp_ocup))),
		dividir(sum(nr_unidades_ocupadas * 100), sum(nr_unidades_setor - nr_unidades_temporarias)) pr_olf,
		dividir(sum((nr_unidades_ocupadas + nr_unid_temp_ocup) * 100),
		sum(nr_unidades_setor - nr_unidades_interditadas)) pr_ohr, sum(qt_pac_isolado) qt_pac_isolado,
		sum(nr_unidades_setor - nr_unidades_interditadas) NR_UNIDADES_TOT_INT,
		0 nr_seq_agrupamento
	from	ocupacao_setores_v2
	where	ie_ocup_hospitalar in ('S','M','T') and ie_situacao = 'A'
	group by substr(obter_valor_dominio(1, cd_classif_setor),1,200);
/
