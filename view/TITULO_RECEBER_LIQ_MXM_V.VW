CREATE OR REPLACE VIEW TITULO_RECEBER_LIQ_MXM_V AS
select	'BR' ie_tipo,
	nvl(a.cd_cgc,a.cd_pessoa_fisica) cd_pessoa,
	nvl(a.nr_titulo_externo,to_char(a.nr_titulo)) nr_titulo,
	b.nr_sequencia nr_seq_baixa,
	nvl(a.nr_nota_fiscal,substr(obter_dados_titulo_receber(a.nr_titulo,'NF'),1,20)) nr_nota_fiscal,
	'34' cd_empresa_emitente,
	'00' cd_filial,
	'34' cd_empresa_pagadora,
	'NF' ie_tipo_titulo,
	to_char(a.dt_emissao,'ddmmyyyy') dt_emissao,
	to_char(a.dt_vencimento,'ddmmyyyy') dt_vencimento_original,
	to_char(a.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento_atual,
	'R' cd_moeda,
	a.vl_titulo vl_titulo,
	'BO' cd_tipo_cobranca,
	decode(a.cd_cgc,null,'04189','999') cd_conta_contabil,
	substr(replace(replace(a.ds_observacao_titulo,chr(13),''),chr(10),''),1,60) ds_observacao,
	to_char(b.dt_recebimento,'ddmmyyyy') dt_baixa,
	b.vl_recebido vl_baixa,
	c.cd_integracao_externa cd_conta_ext,
	nvl(d.cd_integracao_externa,'BOR') ie_forma_pagto,
	'00000000' vl_desconto,
	to_char(to_date(null),'ddmmyyyy') dt_desconto,
	decode(a.vl_saldo_titulo,0,'T','P') ie_total_parcial
from	tipo_recebimento d,
	banco_estabelecimento c,
	titulo_receber_liq b,
	titulo_receber a
where	a.nr_titulo		= b.nr_titulo
and	b.cd_tipo_recebimento	= d.cd_tipo_recebimento
and	b.nr_seq_conta_banco	= c.nr_sequencia(+)	
and	a.dt_integracao_externa is null
and	a.ie_situacao		<> '3'
AND	d.cd_tipo_recebimento	not in (9,11)
and	b.nr_sequencia		= 	(select	max(x.nr_sequencia)
					from	titulo_receber_liq x
					where	x.nr_titulo	= a.nr_titulo
					and	x.cd_tipo_recebimento not in (9,11));
/
