create or replace view rentabilidade_conta_v
as
select		a.nr_atendimento,
		a.nr_interno_conta,
		a.nr_seq_proc_pacote,
		a.cd_convenio,
		a.cd_setor_atendimento,
		a.ie_proc_mat,
		a.cd_item,
		a.ds_item,
		a.dt_item,
		a.qt_item,
		a.vl_item,
		a.vl_original,
		b.ds_convenio,
		c.ds_setor_atendimento,
		d.cd_pessoa_fisica cd_paciente,
		d.ie_tipo_atendimento,
		e.nm_pessoa_fisica nm_paciente,
		a.nr_seq_protocolo,
		a.dt_mesano_protocolo dt_mesano_referencia,
		a.cd_item cd_proced_pacote,
		a.ds_item ds_proced_pacote,
		d.cd_estabelecimento,
		substr(obter_nome_pf(a.CD_MEDICO_EXECUTOR),1,50) nm_medico_executor,
		a.ie_emite_conta,
		a.ie_origem_proced,
		a.nr_seq_proc_interno,
		a.nr_seq_exame,
		a.vl_custo_operacional,
		a.vl_medico,
		a.vl_filme,
		a.ie_cancelamento
from		setor_atendimento c,
		convenio b,
		pessoa_fisica e,
		atendimento_paciente d,
		protocolo_convenio_item_v a
where		a.cd_convenio			= b.cd_convenio
and		a.cd_setor_atendimento		= c.cd_setor_atendimento
and		a.nr_atendimento		= d.nr_atendimento
and		d.cd_pessoa_fisica		= e.cd_pessoa_fisica;
/