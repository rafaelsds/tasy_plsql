create or replace view pls_movto_contab_desp_interc_v
as
select	a.ie_status,
	a.nr_seq_conta_proc,
	null nr_seq_conta_mat,
	b.cd_conta_cred,
	b.cd_conta_deb,
	b.cd_classif_cred,
	b.cd_classif_deb,
	b.nr_seq_esquema,
	a.nr_sequencia,
	b.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	'Conta' ds_conta_glosa,
	b.ie_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_proc		b
where	a.nr_seq_conta_proc	= b.nr_sequencia
and	ie_conta_glosa	= 'C'
union all
select	a.ie_status,
	a.nr_seq_conta_proc,
	a.nr_seq_conta_mat,
	b.cd_conta_cred,
	b.cd_conta_deb,
	b.cd_classif_cred,
	b.cd_classif_deb,
	b.nr_seq_esquema,
	a.nr_sequencia,
	b.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	'Conta' ds_conta_glosa,
	b.ie_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_mat		b
where	a.nr_seq_conta_mat	= b.nr_sequencia
and	ie_conta_glosa	= 'C'
union all
select	a.ie_status,
	a.nr_seq_conta_proc,
	null nr_seq_conta_mat,
	b.cd_conta_glosa_cred cd_conta_cred,
	b.cd_conta_glosa_deb cd_conta_deb,
	b.cd_classif_glosa_cred cd_classif_cred,
	b.cd_classif_glosa_deb cd_classif_deb,
	b.nr_seq_esquema_glosa nr_seq_esquema,
	a.nr_sequencia,
	b.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	'Glosa' ds_conta_glosa,
	b.ie_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_proc		b
where	a.nr_seq_conta_proc	= b.nr_sequencia
and	ie_conta_glosa	= 'G'
union all
select	a.ie_status,
	a.nr_seq_conta_proc,
	a.nr_seq_conta_mat,
	b.cd_conta_glosa_cred cd_conta_cred,
	b.cd_conta_glosa_deb cd_conta_deb,
	b.cd_classif_glosa_cred cd_classif_cred,
	b.cd_classif_glosa_deb cd_classif_deb,
	b.nr_seq_esquema_glosa nr_seq_esquema,
	a.nr_sequencia,
	b.nr_seq_grupo_ans,
	a.nr_seq_item_mensalidade,
	'Glosa' ds_conta_glosa,
	b.ie_ato_cooperado,
	a.nr_seq_conta_copartic,
	a.nr_seq_conta_pos_estab,
	a.nr_seq_atualizacao
from	pls_movimento_contabil	a,
	pls_conta_mat		b
where	a.nr_seq_conta_mat	= b.nr_sequencia
and	ie_conta_glosa	= 'G';
/