create or replace view conta_paciente_movto_v2
as
select	'23' ie_emite_conta,
	d.nr_interno_conta,
	substr(Obter_Unidade_Atendimento(d.nr_atendimento,'IAA','SU'),1,200) ds_setor_unidade
from  	conta_paciente d;
/
