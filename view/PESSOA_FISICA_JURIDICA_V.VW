CREATE OR REPLACE VIEW Pessoa_fisica_juridica_v
AS
select	b.cd_pessoa_fisica cd_codigo,
	b.nm_pessoa_fisica nm_pessoa,
	'F' ie_tipo_pessoa
from	pessoa_fisica b
union
select	b.cd_cgc cd_codigo,
	b.ds_razao_social nm_pessoa,
	'J' ie_tipo_pessoa
from	pessoa_juridica b
order by 2;
/