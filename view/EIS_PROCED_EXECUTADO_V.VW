create or replace
View Eis_Proced_Executado_v as
select	a.*,
	b.cd_tipo_procedimento,
	'('||b.cd_procedimento || ') ' || substr(b.ds_procedimento,1,40) ds_procedimento,
	(select ds_setor_atendimento from setor_atendimento where cd_setor_atendimento = a.cd_setor_atendimento) ds_setor_atendimento,
        (select max(nm_pessoa_fisica) from pessoa_fisica where cd_pessoa_fisica = a.cd_medico_prescr) nm_medico_prescr,
        (select max(nm_pessoa_fisica) from pessoa_fisica where cd_pessoa_fisica = a.cd_medico_solicitante) nm_medico_solic,
	(select max(nm_pessoa_fisica) from pessoa_fisica where cd_pessoa_fisica = a.cd_medico_executor) nm_medico_executor,
        (select max(nm_pessoa_fisica) from pessoa_fisica where cd_pessoa_fisica = a.cd_tecnico) nm_tecnico,
        (select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 12 and vl_dominio = a.ie_tipo_atendimento) ds_tipo_atendimento,
        (select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 95 and vl_dominio = b.cd_tipo_procedimento) ds_tipo_procedimento,
        (select ds_convenio from convenio where cd_convenio = a.cd_convenio) ds_convenio,
	substr((select ds_convenio from convenio where cd_convenio = a.cd_convenio) || ' - ' || (select ds_categoria from categoria_convenio where cd_convenio = a.cd_convenio and cd_categoria = a.cd_categoria),1,200) ds_categoria,
	substr(obter_desc_conta_contabil (a.cd_conta_contabil),1,255) ds_conta_contabil,
	substr(obter_desc_espec_medica(a.cd_especialidade),1,40) ds_especialidade_med,
	(select	max(ds_grupo_receita) from grupo_receita where nr_sequencia = a.nr_seq_grupo_rec) ds_grupo_rec,
	(select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 11 and vl_dominio = a.ie_tipo_convenio) ds_tipo_conv,
	(select	max(ds_proc_exame) from	proc_interno where nr_sequencia	= a.nr_seq_proc_interno) ds_proc_interno,
	substr(ds_proc_interno,1,250) ds_procedimento_int,
	(select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 1372 and vl_dominio = a.ie_lado) ds_lado,
	(select	decode(nvl(x.ie_razao_fantasia,'R'),'R', y.ds_razao_social, 'F',  y.nm_fantasia, 'E', x.nm_fantasia_estab) 
		from estabelecimento x, pessoa_juridica y  where x.cd_cgc = y.cd_cgc and x.cd_estabelecimento = a.cd_estabelecimento) ds_estabelecimento,
	(select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 17 and vl_dominio = a.ie_clinica) ds_clinica,
	(select	ds_procedencia from procedencia where cd_procedencia = a.cd_procedencia) ds_procedencia,
	(select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 1763 and vl_dominio = a.ie_complexidade_sus) ds_complexidade_sus,
	obter_atendimento_conta(a.nr_interno_conta)  nr_atendimento,
	substr(obter_idade_pf(a.cd_pessoa_fisica,sysdate,'E'),1,60) ds_idade,
	(select	substr(ds_valor_dominio,1,254) from valor_dominio where cd_dominio = 4 and vl_dominio = 
							(select	max(ie_sexo) from pessoa_fisica where cd_pessoa_fisica	= a.cd_pessoa_fisica)) ds_sexo,	
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'DM'),1,200) ds_municipio_ibge,
	(select max(nm_pessoa_fisica) from pessoa_fisica where cd_pessoa_fisica = a.cd_pessoa_fisica) nm_paciente,
	(select	max(cd_area_procedimento) from estrutura_procedimento_v x where x.cd_procedimento = a.cd_procedimento and x.ie_origem_proced	= a.ie_origem_proced) cd_area_proced,
	(select	max(ds_area_procedimento) from estrutura_procedimento_v x where x.cd_procedimento = a.cd_procedimento and x.ie_origem_proced	= a.ie_origem_proced) ds_area_proced,
	(select  max(ds_agrupamento) from agrupamento_setor b where b.nr_sequencia = a.nr_seq_agrup_classif) ds_agrup_classif,
	(select max(nm_pessoa_fisica) from pessoa_fisica where cd_pessoa_fisica = a.cd_medico_laudo) nm_medico_laudo,
	SUBSTR(obter_descricao_padrao('ESPECIALIDADE_PROC','DS_ESPECIALIDADE',obter_especialidade_proced(b.cd_procedimento,b.ie_origem_proced)),1,80) ds_especialidade,
	obter_especialidade_proced(b.cd_procedimento,b.ie_origem_proced) cd_especialidade_proc,
	lpad(a.dt_hora_exame,2,0) || ':00' ds_hora_exame
from	procedimento b,
	eis_proced_executado a
where	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= b.ie_origem_proced;
/
