create or replace view ctb_orc_empresarial_v 
as
select  b.dt_referencia,
        a.cd_centro_custo,
        a.cd_conta_contabil,
        a.cd_estabelecimento,
        b.cd_empresa,
        sum(a.vl_orcado) vl_orcado,
        sum(a.vl_realizado) vl_realizado
from    ctb_orcamento a,
        ctb_mes_ref b
where   a.nr_seq_mes_ref = b.nr_sequencia
group by b.dt_referencia, a.cd_conta_contabil, a.cd_centro_custo, a.cd_estabelecimento, b.cd_empresa
UNION ALL
select  b.dt_referencia,
        a.cd_centro_custo,
        a.cd_conta_contabil,
        a.cd_estabelecimento,
        b.cd_empresa,
        sum(a.vl_orcado),
        sum(a.vl_realizado)
from    ctb_mes_ref b,
        ctb_orcamento_capex a
where   a.nr_seq_mes_ref = b.nr_sequencia
group by b.dt_referencia, a.cd_centro_custo, a.cd_conta_contabil, a.cd_estabelecimento, b.cd_empresa
order by 1
/