CREATE OR REPLACE 
VIEW EIS_ESCALA_SAPS_3  
AS 	SELECT	DISTINCT
	eis_obter_setor_atend_data(a.nr_atendimento, e.dt_avaliacao) cd_setor_atendimento,
	SUBSTR(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
	SUBSTR(obter_nome_convenio(obter_convenio_atendimento(a.nr_atendimento)),1,150) ds_convenio,
	obter_nome_setor(eis_obter_setor_atend_data(a.nr_atendimento, e.dt_avaliacao) ) ds_setor_atendimento,
	obter_sexo_pf(a.cd_pessoa_fisica,'C') ie_sexo,
	obter_sexo_pf(a.cd_pessoa_fisica,'D') ds_sexo,
	obter_nome_pessoa_fisica(e.CD_PROFISSIONAL, NULL) nm_medico,
	e.CD_PROFISSIONAL, 
	a.cd_medico_resp,
	SUBSTR(obter_idade(obter_data_nascto_pf(a.cd_pessoa_fisica),SYSDATE,'E'),1,10) ie_faixa_etaria,
	SUBSTR(obter_unidade_atend_data(a.nr_atendimento, e.dt_avaliacao),1,255) ds_unidade,
	TRUNC(e.dt_avaliacao) dt_avaliacao,
	e.QT_PONTUACAO,
	e.QT_PONTUACAO|| ' ' || wheb_mensagem_pck.GET_TEXTO(1061984) ds_pontos,	
	f.cd_empresa,
	a.cd_estabelecimento,	
	a.nr_atendimento,
	a.cd_pessoa_fisica cd_paciente,
	e.pr_risco,
	'Risco de '||e.pr_risco||'%' ds_risco,
	e.pr_risco_america_sul,
	'Risco de '||e.pr_risco_america_sul||'%' ds_risco_sul
FROM 	ESCALA_SAPS3 e,
	estabelecimento f,
	atendimento_paciente a
WHERE 	a.nr_atendimento= e.nr_atendimento
AND	a.cd_estabelecimento = f.cd_estabelecimento
AND	e.dt_liberacao IS NOT NULL;
/