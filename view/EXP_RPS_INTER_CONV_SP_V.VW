CREATE OR REPLACE 
VIEW EXP_RPS_INTER_CONV_SP_V AS
select	1					tp_registro,
	a.cd_inscricao_municipal		nr_inscricao_municipal,
	''					ds_tipo_rps,
	''					cd_serie_nf,
	'1'					nr_nota_fiscal,
	null				dt_emissao,
	''					ie_situacao,
	''					ie_situacao_nf,
	0					vl_servico,
	0					vl_descontos,
	''					cd_servico,
	0					vl_aliquota,
	''					ie_iss_retido,
	0					ie_cpf_cnpj,
	''					cd_cpf_cnpj,
	''					nr_inscricao_munic_tomador,
	''					nr_inscricao_estad_tomador,
	''					nm_tomador,
	''					ds_tipo_logradouro,
	''					ds_endereco,
	''					nr_endereco,
	''					ds_complemento,
	''					ds_bairro,
	''					ds_municipio,
	''					sg_estado,
	''					cd_cep,
	''					ds_email,
	' '					ds_servicos,
	''					ds_observacao,
	0					vl_total_servicos,
	0					vl_total_descontos,
	0					qt_linhas,
	0					nr_sequencia,
	0					cd_operacao_nf,
	a.cd_estabelecimento,
	''					ds_natureza_operacao,
	0					vl_deducoes,
	0					vl_total_deducoes,
	0					vl_nota_fiscal,
	0					vl_total_nota_fiscal,
	''					cd_pessoa_fisica,
	0					IndCNPJ,
	''					CNPJ_Intermed,
	''					INSC_MUNICIPAL_Intermed,
	''					DS_EMAIL_Intermed
from	estabelecimento a
union
select	2						tp_registro,
	''							nr_inscricao_municipal,
	'RPS'						ds_tipo_rps,
	n.cd_serie_nf				cd_serie_nf,
	n.nr_nota_fiscal			nr_nota_fiscal,
	trunc(n.dt_emissao)			dt_emissao,
	decode( n.ie_situacao,1, 
					decode(nvl((	select	count(*) 
									from	nota_fiscal_trib a, 
											tributo c 
									where	a.cd_tributo 	 = c.cd_tributo 
									and	c.ie_tipo_tributo	= 'ISS' 
									and 	a.nr_sequencia  	= n.nr_sequencia),0),0,'I','T'), 
			3, decode(n.ie_status_envio,null, 
					decode(nvl((	select	count(*) 
									from	nota_fiscal_trib a, 
											tributo c 
									where	a.cd_tributo 	 = c.cd_tributo 
									and	c.ie_tipo_tributo	= 'ISS' 
									and 	a.nr_sequencia  	= n.nr_sequencia),0),0,'I','T'),'C'),'C') ie_situacao,
	n.ie_situacao				ie_situacao_nf,
	decode(n.ie_situacao,1, n.vl_mercadoria, 3, decode(n.ie_status_envio, null, n.vl_mercadoria, 0), 0) vl_servico,
	n.vl_descontos				vl_descontos,
	nvl(o.nr_codigo_serv_prest,lpad(nvl(substr(elimina_caractere_especial(obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'), obter_procedimento_nfse(n.nr_sequencia,'O')),'CD')),1,5),obter_dados_pf_pj_estab(n.cd_estabelecimento, null, n.cd_cgc_emitente, 'ATIV')),5,'0')) cd_servico,
	0	vl_aliquota,
	decode(obter_dados_pf_pj(null,obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia)),'CDM'), 355030, '3', decode(substr(obter_se_nf_retem_iss(n.nr_sequencia),1,1),'S','1','2')) ie_iss_retido,	
	decode(substr(obter_dados_pf_pj(n.cd_pessoa_fisica,n.cd_cgc,'UF'),1,2),'IN',3, decode(n.cd_pessoa_fisica,null,2,decode(obter_cpf_pessoa_fisica(n.cd_pessoa_fisica),null,3,1))) ie_cpf_cnpj,			
	decode(substr(obter_dados_pf_pj(n.cd_pessoa_fisica,n.cd_cgc,'UF'),1,2),'IN','00000000000000', decode(n.cd_pessoa_fisica,null,n.cd_cgc, decode(obter_cpf_pessoa_fisica(n.cd_pessoa_fisica),null,'00000000000000',obter_cpf_pessoa_fisica(n.cd_pessoa_fisica)))) cd_cpf_cnpj,
	decode(elimina_acentuacao(elimina_caracteres_especiais(upper(substr(obter_dados_pf_pj(null,n.cd_cgc,'CI'),1,50)))),'SAOPAULO',	substr(obter_dados_pf_pj(null,n.cd_cgc,'IM'),1,8),'00000000') nr_inscricao_munic_tomador,
	substr(obter_dados_pf_pj(null,n.cd_cgc,'IE'),1,20) nr_inscricao_estad_tomador,
	substr(obter_nome_pf_pj(n.cd_pessoa_fisica,n.cd_cgc),1,75) nm_tomador,
	'Rua'					ds_tipo_logradouro,
	nvl(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'E'),1,40),'N/D', substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'EN'),1,50), substr(obter_compl_pj(n.cd_cgc,1,'E'),1,40)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'EN'),1,50)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'EN'),1,50)) ds_endereco,
	nvl(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'NR'),1,10),'N/D', substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'NR'),1,10),substr(obter_compl_pj(n.cd_cgc,1,'NR'),1,10)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'NR'),1,10)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'NR'),1,10)) nr_endereco,
	nvl(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'CO'),1,30),'N/D', substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CO'),1,30), substr(obter_compl_pj(n.cd_cgc,1,'CO'),1,30)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'CO'),1,30)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CO'),1,30)) ds_complemento,
	nvl(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'B'),1,30),'N/D', substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'B'),1,30), substr(obter_compl_pj(n.cd_cgc,1,'B'),1,30)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'B'),1,30)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'B'),1,30)) ds_bairro,
	nvl(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'CI'),1,50),'N/D', substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CI'),1,50), substr(obter_compl_pj(n.cd_cgc,1,'CI'),1,50)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'CI'),1,50)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CI'),1,50)) ds_municipio,
	nvl(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'UF'),1,2),'N/D', substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'UF'),1,2), substr(obter_compl_pj(n.cd_cgc,1,'UF'),1,2)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'UF'),1,2)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'UF'),1,2)) sg_estado,
	nvl(to_char(decode(n.cd_pessoa_fisica,null,decode(substr(obter_compl_pj(n.cd_cgc,1,'CEP'),1,15),'N/D', to_char(somente_numero(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CEP'),1,15))), substr(obter_compl_pj(n.cd_cgc,1,'CEP'),1,15)), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'CEP'),1,15))), to_char(somente_numero(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CEP'),1,15)))) cd_cep,
	nvl(decode(n.cd_pessoa_fisica,null,substr(obter_compl_pj(n.cd_cgc,1,'M'),1,80), substr(obter_compl_pf(n.cd_pessoa_fisica, 1, 'M'),1,60)), substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'M'),1,60)) ds_email,
	substr(obter_descricao_rps(n.cd_estabelecimento, n.nr_sequencia, 'DS_SERVICOS'), 1, 1000) ds_servicos,
	n.ds_observacao				ds_observacao,
	0					vl_total_servicos,
	0					vl_total_descontos,
	0					qt_linhas,
	n.nr_sequencia				nr_sequencia,
	n.cd_operacao_nf 				cd_operacao_nf,
	n.cd_estabelecimento,
	substr(obter_desc_natureza_operacao(n.cd_natureza_operacao),1,100) ds_natureza_operacao,
	nvl(Obter_Valor_tipo_Tributo_Nota(n.nr_sequencia, 'V', 'ISS'),0) vl_deducoes,
	0					vl_total_deducoes,
	decode(n.ie_situacao,1,n.vl_total_nota,0) vl_nota_fiscal,
	0					vl_total_nota_fiscal,
	n.cd_pessoa_fisica,
	0					IndCNPJ,
	''					CNPJ_Intermed,
	''					INSC_MUNICIPAL_Intermed,
	''					DS_EMAIL_Intermed	
from	operacao_nota o,
	nota_fiscal n
where	exists(
	select	1
	from	w_nota_fiscal x
	where	x.nr_seq_nota_fiscal = n.nr_sequencia)
and	o.cd_operacao_nf = n.cd_operacao_nf
and	n.vl_total_nota > 0
and obter_convenio_nf(n.nr_sequencia) is null
union
select	2						tp_registro,
	''							nr_inscricao_municipal,
	'RPS'						ds_tipo_rps,
	n.cd_serie_nf				cd_serie_nf,
	n.nr_nota_fiscal			nr_nota_fiscal,
	trunc(n.dt_emissao)			dt_emissao,
	decode( n.ie_situacao,1, 
					decode(nvl((	select	count(*) 
									from	nota_fiscal_trib a, 
											tributo c 
									where	a.cd_tributo 	 = c.cd_tributo 
									and	c.ie_tipo_tributo	= 'ISS' 
									and 	a.nr_sequencia  	= n.nr_sequencia),0),0,'I','T'), 
			3, decode(n.ie_status_envio,null, 
					decode(nvl((	select	count(*) 
									from	nota_fiscal_trib a, 
											tributo c 
									where	a.cd_tributo 	 = c.cd_tributo 
									and	c.ie_tipo_tributo	= 'ISS' 
									and 	a.nr_sequencia  	= n.nr_sequencia),0),0,'I','T'),'C'),'C') ie_situacao,
	n.ie_situacao				ie_situacao_nf,
	decode(n.ie_situacao,1, n.vl_mercadoria, 3, decode(n.ie_status_envio, null, n.vl_mercadoria, 0), 0) vl_servico,
	n.vl_descontos				vl_descontos,
	nvl(o.nr_codigo_serv_prest,lpad(nvl(substr(elimina_caractere_especial(obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'), obter_procedimento_nfse(n.nr_sequencia,'O')),'CD')),1,5),obter_dados_pf_pj_estab(n.cd_estabelecimento, null, n.cd_cgc_emitente, 'ATIV')),5,'0')) cd_servico,
	0	vl_aliquota,
	decode(obter_dados_pf_pj(null,obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia)),'CDM'), 355030, '3', decode(substr(obter_se_nf_retem_iss(n.nr_sequencia),1,1),'S','1','2')) ie_iss_retido,	
	3 ie_cpf_cnpj,			
	'00000000000000' cd_cpf_cnpj,
	rpad(' ',  8, ' ')  nr_inscricao_munic_tomador,
	rpad(' ',  12, ' ') nr_inscricao_estad_tomador,
	rpad(' ',  75, ' ') nm_tomador,
	rpad(' ',  3, ' ')	ds_tipo_logradouro,
	rpad(' ',  50, ' ') ds_endereco,
	rpad(' ',  10, ' ') nr_endereco,
	rpad(' ',  30, ' ') ds_complemento,
	rpad(' ',  30, ' ') ds_bairro,
	rpad(' ',  50, ' ') ds_municipio,
	rpad(' ',  2, ' ')  sg_estado,
	rpad(' ',  8, ' ')  cd_cep,
	rpad(' ',  75, ' ') ds_email,
	substr(obter_descricao_rps(n.cd_estabelecimento, n.nr_sequencia, 'DS_SERVICOS'), 1, 955) || ' ARE N.O 12.017, PROCESSO 2013-0.224.393-0' ds_servicos,
	n.ds_observacao				ds_observacao,
	0					vl_total_servicos,
	0					vl_total_descontos,
	0					qt_linhas,
	n.nr_sequencia				nr_sequencia,
	n.cd_operacao_nf 				cd_operacao_nf,
	n.cd_estabelecimento,
	substr(obter_desc_natureza_operacao(n.cd_natureza_operacao),1,100) ds_natureza_operacao,
	nvl(Obter_Valor_tipo_Tributo_Nota(n.nr_sequencia, 'V', 'ISS'),0) vl_deducoes,
	0					vl_total_deducoes,
	decode(n.ie_situacao,1,n.vl_total_nota,0) vl_nota_fiscal,
	0					vl_total_nota_fiscal,
	n.cd_pessoa_fisica,
	0					IndCNPJ,
	''					CNPJ_Intermed,
	''					INSC_MUNICIPAL_Intermed,
	''					DS_EMAIL_Intermed	
from	operacao_nota o,
	nota_fiscal n
where	exists(
	select	1
	from	w_nota_fiscal x
	where	x.nr_seq_nota_fiscal = n.nr_sequencia)
and	o.cd_operacao_nf = n.cd_operacao_nf
and	n.vl_total_nota > 0
and obter_convenio_nf(n.nr_sequencia) is not null
union
select	5				tp_registro,
	''					nr_inscricao_municipal,
	''					ds_tipo_rps,
	''					cd_serie_nf,
	n.nr_nota_fiscal	nr_nota_fiscal,
	null				dt_emissao,
	''					ie_situacao,
	''					ie_situacao_nf,
	0					vl_servico,
	0					vl_descontos,
	''					cd_servico,
	0					vl_aliquota,
	''					ie_iss_retido,
	0					ie_cpf_cnpj,
	''					cd_cpf_cnpj,
	''					nr_inscricao_munic_tomador,
	''					nr_inscricao_estad_tomador,
	''					nm_tomador,
	''					ds_tipo_logradouro,
	''					ds_endereco,
	''					nr_endereco,
	''					ds_complemento,
	''					ds_bairro,
	''					ds_municipio,
	''					sg_estado,
	''					cd_cep,
	''					ds_email,
	' '					ds_servicos,
	''					ds_observacao,
	0					vl_total_servicos,
	0					vl_total_descontos,
	0					qt_linhas,
	0					nr_sequencia,
	0 					cd_operacao_nf,
	n.cd_estabelecimento,
	''					ds_natureza_operacao,
	0					vl_deducoes,
	0					vl_total_deducoes,
	0					vl_nota_fiscal,
	0					vl_total_nota_fiscal,
	''					cd_pessoa_fisica,
	2					IndCNPJ,
	lpad(nvl(n.cd_cgc,substr(obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia)),1,14)),14,'0') CNPJ_Intermed,
	lpad(substr(decode(obter_dados_pf_pj(null,nvl(n.cd_cgc,obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia))),'CDM'), 355030, obter_dados_pf_pj(null,nvl(n.cd_cgc,obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia))),'IM'), '0'),1,8),8,'0') INSC_MUNICIPAL_Intermed,
	rpad(substr((SELECT x.ds_email from pessoa_juridica_estab x where x.cd_cgc = nvl(n.cd_cgc,obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia))) and x.cd_estabelecimento = n.cd_estabelecimento),1,75),75,' ') DS_EMAIL_Intermed	
from	nota_fiscal n
where	1 = 1
and	exists(
	select	1
	from	w_nota_fiscal x
	where	x.nr_seq_nota_fiscal = n.nr_sequencia)
and	n.vl_total_nota > 0
and obter_convenio_nf(n.nr_sequencia) is not null
--and	obter_dados_pf_pj(null,obter_cgc_convenio(obter_convenio_nf(n.nr_sequencia)),'CDM') = 355030
union
select	9					tp_registro,
	''					nr_inscricao_municipal,
	'RPS'					ds_tipo_rps,
	''					cd_serie_nf,
	'999999'				nr_nota_fiscal,
	null					dt_emissao,
	''					ie_situacao,
	''					ie_situacao_nf,
	0					vl_servico,
	0					vl_descontos,
	''					cd_servico,
	0					vl_aliquota,
	''					ie_iss_retido,
	0					ie_cpf_cnpj,
	''					cd_cpf_cnpj,
	''					nr_inscricao_munic_tomador,
	''					nr_inscricao_estad_tomador,
	''					nm_tomador,
	''					ds_tipo_logradouro,
	''					ds_endereco,
	''					nr_endereco,
	''					ds_complemento,
	''					ds_bairro,
	''					ds_municipio,
	''					sg_estado,
	''					cd_cep,
	''					ds_email,
	' '					ds_servicos,
	''					ds_observacao,
	sum(decode(n.ie_situacao,1, n.vl_mercadoria, 3, decode(n.ie_status_envio, null, n.vl_mercadoria, 0), 0)) vl_total_servicos,
	sum(n.vl_descontos)			vl_total_descontos,
	0					qt_linhas,
	0					nr_sequencia,
	0 					cd_operacao_nf,
	n.cd_estabelecimento,
	''					ds_natureza_operacao,
	0					vl_deducoes,
	sum(nvl(Obter_Valor_tipo_Tributo_Nota(n.nr_sequencia, 'V', 'ISS'),0))  vl_total_deducoes,
	0					vl_nota_fiscal,
	sum(nvl(decode(n.ie_situacao,1,n.vl_total_nota,0),0)) vl_total_nota_fiscal,
	''					cd_pessoa_fisica,
	0					IndCNPJ,		
	''					CNPJ_Intermed,
	''					INSC_MUNICIPAL_Intermed,
	''					DS_EMAIL_Intermed	
from	nota_fiscal n
where	exists(
	select	1
	from	w_nota_fiscal x
	where	x.nr_seq_nota_fiscal = n.nr_sequencia)
and	n.vl_total_nota > 0	
group by n.cd_estabelecimento;
/
