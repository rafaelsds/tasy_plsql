create or replace 
view ageint_check_list_pac_item_v as
select	distinct
	1 ordem,
	a.ds_grupo ds_item_padrao,
	a.nr_sequencia nr_seq_grupo,	
	c.nr_seq_check_list nr_seq_checklist,
	null nr_seq_item_checklist,
	null ds_resultado,
	null ds_item,
	a.ds_grupo ds_grupo,
	d.nr_seq_ageint,
	d.dt_liberacao,
	a.ds_email_destino,
	null ie_bloq_agendamento
from	ageint_check_list_pac_item c,
		ageint_item_check_list b,
		ageint_grupo_check_list a,
		ageint_check_list_paciente d
where	c.nr_seq_item_check_list = b.nr_sequencia
and	b.nr_seq_grupo_check_list = a.nr_sequencia
and	c.nr_seq_check_list = d.nr_sequencia
union
select	distinct
	2 ordem,
	'       ' || b.ds_item ds_item_padrao,
	a.nr_sequencia nr_seq_grupo,	
	c.nr_seq_check_list nr_seq_checklist,
	c.nr_sequencia nr_seq_item_checklist,
	ds_resultado ds_resultado,
	b.ds_item,
	a.ds_grupo ds_grupo,
	d.nr_seq_ageint,
	d.dt_liberacao,
	'' ds_email_destino,
	nvl(b.ie_bloqueia_agendamento, 'N') ie_bloq_agendamento
from	ageint_check_list_pac_item c,
		ageint_item_check_list b,
		ageint_grupo_check_list a,
		ageint_check_list_paciente d
where	c.nr_seq_item_check_list = b.nr_sequencia
and	b.nr_seq_grupo_check_list = a.nr_sequencia
and	c.nr_seq_check_list = d.nr_sequencia;
/
