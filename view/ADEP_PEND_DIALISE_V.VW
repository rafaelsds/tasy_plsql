create or replace
view adep_pend_dialise_v
as
select	15 nr_seq_apresent,
		'DI' ie_tipo_item,
		'Di�lise' ds_tipo_item,
		'Di�lises' ds_tipo_item_plural,
		'N' ie_laboratorio,
		a.cd_estabelecimento,
		a.nr_atendimento,
		a.nr_prescricao,
		a.cd_prescritor,
		obter_nome_pf(a.cd_prescritor) nm_prescritor,
		a.dt_prescricao,
		a.dt_inicio_prescr,
		a.dt_validade_prescr,
		a.dt_liberacao,
		null dt_lib_horario,
		a.cd_setor_atendimento cd_setor_prescr,
		substr(obter_se_setor_processo_gedipa(a.cd_setor_atendimento),1,1) ie_setor_processo_gedipa,
		-1 nr_seq_item,
		null ie_acm_sn,
		null ie_di,
		to_char(b.nr_seq_solucao) cd_item,
		null ie_origem_proced,
		null nr_seq_proc_interno,	
		substr(nvl(obter_desc_prot_npt(b.nr_seq_protocolo),obter_prim_comp_sol(a.nr_prescricao,b.nr_seq_solucao)),1,240) ds_item,
		null nr_agrupamento,
		b.cd_intervalo cd_intervalo,
		'' ds_diluente,
		null ds_topografia,
		'N' ie_assoc_adep,
		null nr_seq_area_prep,	
		null nr_seq_lote,
		null nr_seq_processo,
		c.nr_sequencia nr_seq_horario,
		b.dt_status,
		null qt_pend,
		b.cd_unidade_medida cd_um,
		b.qt_dosagem qt_dose,
		b.ie_tipo_dosagem,
		'' ie_via,
		'' cd_pessoa_evento,
		'' nm_pessoa_evento,
		a.dt_liberacao_medico,
		'' ie_urgencia,
		obter_local_estoque_setor(a.cd_setor_atendimento, a.cd_estabelecimento) cd_local_estoque,
		to_number(null) nr_seq_superior,
		null ie_item_superior,
		null ds_justificativa,
		null nr_sequencia_proc,
		null cd_local_estoque_mat,
		obter_local_estoque_setor(Obter_Unidade_Atendimento(a.nr_atendimento,'IAA','CS'), a.cd_estabelecimento) cd_local_estoque_aux,
		Obter_Unidade_Atendimento(a.nr_atendimento,'IAA','CS') cd_setor_aux,
		null ie_conferencia,
		null ie_higienizacao,
		null ie_preparo,
		'' ds_observacao,
		'N' ie_medicacao_paciente
from	hd_prescricao c,
		prescr_solucao b,
		prescr_medica a
where	c.nr_sequencia = b.nr_seq_dialise
and		c.nr_prescricao = b.nr_prescricao
and		b.nr_prescricao = a.nr_prescricao
and		nvl(a.ie_adep,'S') = 'S'
and     c.ie_tipo_dialise in ('H','P')
and     b.ie_status not in ('T', 'V')
and		b.nr_seq_dialise is not null;
/