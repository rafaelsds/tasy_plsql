create or replace view hcc_bpa_v as
select	1 				tp_registro,
	'BPA' 				descricao_reg,
	a.dt_mesano_referencia		dt_mesano_referencia,
	a.nr_seq_protocolo		nr_seq_protocolo,
	a.qt_linhas			qt_linhas,
	a.qt_folha_bpa			qt_folha_bpa,
	a.cd_dominio			cd_dominio,
	a.nm_orgao_responsavel		nm_orgao_responsavel,
	a.cd_orgao_responsavel		cd_orgao_responsavel,
	a.cd_cgc_responsavel		cd_cgc_responsavel,
	a.nm_orgao_responsavel		nm_orgao_destino,
	a.ie_orgao_destino		ie_orgao_destino,
	a.ds_versao			ds_versao,
	sysdate				dt_competencia,
	0				cd_ups,
	''				cd_cns_medico_exec,
	''				cd_cbo,
	sysdate				dt_procedimento,
	0				nr_folha_bpa,
	0				nr_linha_folha,
	0				cd_procedimento,
	''				cd_cns_paciente,
	''				ie_sexo_pac,
	''				cd_municipio_ibge,
	''				cd_cid_proc,
	0				nr_idade_pac,
	0				qt_procedimento,
	''				cd_carater_atendimento,
	''				ds_autorizacao,
	''				ds_origem_inf,
	''				nm_paciente,
	sysdate				dt_nascimento,
	''				ie_tipo_bpa,
	0				cd_procedimento_amb
from	w_susbpa_interf a
group by 		a.dt_mesano_referencia,
			a.nr_seq_protocolo,
			a.qt_linhas,
			a.qt_folha_bpa,
			a.cd_dominio,
			a.nm_orgao_responsavel,
			a.cd_orgao_responsavel,
			a.cd_cgc_responsavel,
			a.nm_orgao_responsavel,
			a.ie_orgao_destino,
			a.ds_versao
union all
select	2 				tp_registro,
	'BPA' 				descricao_reg,
	a.dt_mesano_referencia		dt_mesano_referencia,
	a.nr_seq_protocolo		nr_seq_protocolo,
	a.qt_linhas			qt_linhas,
	a.qt_folha_bpa			qt_folha_bpa,
	0				cd_dominio,
	''				nm_orgao_responsavel,
	''				cd_orgao_responsavel,
	''				cd_cgc_responsavel,
	''				nm_orgao_destino,
	''				ie_orgao_destino,
	''				ds_versao,
	a.dt_competencia		dt_competencia,
	a.cd_ups			cd_ups,
	a.cd_cns_medico_exec		cd_cns_medico_exec,
	a.cd_cbo			cd_cbo,
	a.dt_procedimento		dt_procedimento,
	a.nr_folha_bpa			nr_folha_bpa,
	a.nr_linha_folha		nr_linha_folha,
	a.cd_procedimento		cd_procedimento,
	a.cd_cns_paciente		cd_cns_paciente,
	a.ie_sexo_pac			ie_sexo_pac,
	a.cd_municipio_ibge		cd_municipio_ibge,
	a.cd_cid_proc			cd_cid_proc,
	a.nr_idade_pac			nr_idade_pac,
	a.qt_procedimento		qt_procedimento,
	a.cd_carater_atendimento	cd_carater_atendimento,
	a.ds_autorizacao		ds_autorizacao,
	a.ds_origem_informacao		ds_origem_inf,
	a.nm_paciente			nm_paciente,
	a.dt_nascimento			dt_nascimento,
	a.ie_tipo_bpa			ie_tipo_bpa,
	sus_obter_conversao(a.cd_procedimento,
			    7)	cd_procedimento_amb
from	w_susbpa_interf a;
/