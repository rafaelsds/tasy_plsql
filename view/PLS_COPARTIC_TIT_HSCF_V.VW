create or replace view pls_copartic_tit_hscf_v
as
select	distinct 1												TP_REGISTRO,
		a.nm_usuario											NM_USUARIO,
		d.cd_funcionario || ' '									CD_PESSOA_FISICA,
		obter_Valor_sem_virgula(nvl(a.vl_agrupado,0) + 
					nvl((	select 	sum(x.vl_agrupado)
							from	w_pls_interface_mens 	x,
									pls_segurado			y
							where 	x.nr_seq_segurado = y.nr_sequencia
							and	y.nr_seq_titular = b.nr_sequencia
							and	x.nm_usuario	= a.nm_usuario ),0)) || ' '	VL_COPARTICIPACAO,
		d.nm_pessoa_fisica || ' '								NM_PESSOA_FISICA
from	w_pls_interface_mens a,
		pls_segurado		 b,
		pessoa_fisica		 d
where	a.nr_seq_segurado	 = b.nr_sequencia
and		b.cd_pessoa_fisica	 = d.cd_pessoa_fisica
and		b.nr_seq_titular is null
union
select	distinct 1													TP_REGISTRO,
		a.nm_usuario												NM_USUARIO,
		(	select	max(x.cd_funcionario)
			from	pessoa_fisica	x,
					pls_segurado	y
			where	x.cd_pessoa_fisica	= y.cd_pessoa_fisica
			and		y.nr_sequencia		= b.nr_seq_titular) || ' '	CD_PESSOA_FISICA,
		obter_Valor_sem_virgula(sum(nvl(a.vl_agrupado,0))) || ' '	VL_COPARTICIPACAO,
		(	select	max(x.nm_pessoa_fisica)
			from	pessoa_fisica	x,
					pls_segurado	y
			where	x.cd_pessoa_fisica	= y.cd_pessoa_fisica
			and		y.nr_sequencia		= b.nr_seq_titular) || ' '	NM_PESSOA_FISICA
from	w_pls_interface_mens a,
		pls_segurado		 b,
		pessoa_fisica		 d
where	a.nr_seq_segurado	 = b.nr_sequencia
and		b.cd_pessoa_fisica	 = d.cd_pessoa_fisica
and		b.nr_seq_titular is not null
and not exists(	select	1
				from	w_pls_interface_mens x,
						pls_segurado		 y
				where	x.nr_seq_segurado	= y.nr_sequencia
				and		y.nr_sequencia		= b.nr_seq_titular
				and		x.nm_usuario = a.nm_usuario)
group by a.nm_usuario,
		 b.nr_seq_titular
order by nm_usuario;
/