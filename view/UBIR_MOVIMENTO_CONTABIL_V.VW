create or replace
view UBIR_MOVIMENTO_CONTABIL_V as
select	1 						tp_registro,
	a.nr_lote_contabil					nr_lote_contabil,
	obter_empresa_estab(a.cd_estabelecimento)		cd_empresa,
	a.cd_estabelecimento				cd_estabelecimento,
	to_char(a.dt_referencia,'YYYYMM') 			dt_lancamento,
	to_char(sysdate,'dd/mm/yyyy') 			dt_geracao,
	nvl(a.vl_debito,a.vl_credito) 				vl_total_lote,
	0						vl_debito,
	0						vl_Credito,
	a.ds_observacao					ds_observacao,
	0						nr_sequencia,
	''						dt_movimento,
	0						nr_documento,
	0						vl_movimento,
	''						cd_conta_debito,
	''						cd_conta_credito,
	0						cd_historico,
	''						ds_compl_historico
from	lote_contabil a
union all
select	2 tp_registro,
	a.nr_lote_contabil				nr_lote_contabil,
	obter_empresa_estab(a.cd_estabelecimento) 	cd_empresa,
	a.cd_estabelecimento,
	to_char(a.dt_referencia,'YYYYMM') 		dt_lancamento,
	to_char(sysdate,'dd/mm/yyyy')			dt_geracao,
	nvl(a.vl_debito,a.vl_credito) 			vl_total_lote,
	0 					vl_debito,
	0 					vl_credito,
	a.ds_observacao,
	b.nr_sequencia				nr_lancamento,
	to_char(b.dt_movimento,'dd/mm/yyyy') 		dt_movimento,
	b.nr_documento,
	b.vl_movimento,
	b.cd_conta_debito,
	b.cd_conta_credito,
	b.cd_historico,
	b.ds_compl_historico
from	lote_contabil a,
	movimento_contabil_v b
where	a.nr_lote_contabil	= b.nr_lote_contabil
union all
select	9 tp_registro,
	a.nr_lote_contabil				nr_lote_contabil,
	obter_empresa_estab(a.cd_estabelecimento)	cd_empresa,
	a.cd_estabelecimento			cd_estabelecimento,
	to_char(a.dt_referencia,'YYYYMM') 		dt_lancamento,
	to_char(sysdate,'dd/mm/yyyy') 			dt_geracao,
	nvl(a.vl_debito,a.vl_credito) 			vl_total_lote,
	nvl(a.vl_debito,0)				vl_debito,
	nvl(a.vl_credito,0)				vl_credito,
	a.ds_observacao					ds_observacao,
	0						nr_sequencia,
	''						dt_movimento,
	0,
	0						vl_movimento,
	''						cd_conta_debito,
	''						cd_conta_credito,
	0						cd_historico,
	''						ds_compl_historico
from	lote_contabil a
order by nr_lote_contabil,tp_registro;
/