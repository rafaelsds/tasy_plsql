CREATE OR REPLACE 
VIEW UDI_OBTER_SALDO_PORCENT AS
 select	a.nr_Seq_protocolo nr_Seq_protocolo,
	a.cd_convenio cd_convenio,
	a.dt_mesano_referencia dt_mesano_referencia,
	a.dt_vencimento dt_vencimento,
	max((	select	max(t.nr_titulo)
		from	titulo_receber t
		where	t.nr_Seq_protocolo = a.nr_Seq_protocolo)) nr_titulo,
	max((	select	max(t.dt_pagamento_previsto)
		from	titulo_Receber t
		where	t.nr_seq_protocolo = a.nr_Seq_protocolo)) dt_pagamento_previsto,
	substr(obter_nome_convenio(a.cd_convenio),1,255) ds_convenio,
	nvl(max((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)/*
			*/vl_protocolo,
	nvl(max((	select	nvl(sum(x.vl_pago),0)
		from	convenio_retorno_item x,
			conta_paciente y
		where	x.nr_interno_conta = y.nr_interno_conta/*
		*/and	y.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_pago,
	nvl(sum((select	nvl(sum(j.vl_saldo_titulo),0)
		from	titulo_receber j
		where	j.nr_seq_protocolo = a.nr_seq_protocolo)),0)/*
		*/vl_amenor,
		nvl(max((select	nvl(sum(obter_valores_guia_grc(q.nr_seq_lote_hist,q.nr_interno_conta,q.cd_autorizacao,'VA')),0)
			from	lote_audit_hist_guia q ,
				conta_paciente t
			where	q.nr_interno_conta = t.nr_interno_conta
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)
			 vl_recurso,
	nvl(sum((	select 	sum(x.vl_pago)
			from 	convenio_retorno_item x,
				conta_paciente t,
				convenio_retorno s
			where 	x.nr_interno_conta = t.nr_interno_conta
			and	x.nr_seq_retorno   = s.nr_Sequencia
			and	s.nr_seq_tipo      = 3
			and 	t.nr_Seq_protocolo = a.nr_Seq_protocolo
			and 	x.nr_seq_retorno > (	select 	min(j.nr_seq_retorno)
							from 	convenio_retorno_item j,
								conta_paciente f
							where 	j.nr_interno_conta = f.nr_interno_conta
							and 	f.nr_seq_protocolo = a.nr_seq_protocolo))),0) vl_rec_recurso,
	nvl(sum((	select	sum(x.vl_glosado)
			from	convenio_Retorno_item x,
				conta_paciente t
			where	x.nr_interno_conta = t.nr_interno_conta/*
			*/
			and	x.ie_glosa = 'S'
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
				nvl(sum((select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
										from	lote_audit_hist_guia r
										where	r.nr_interno_conta = c.nr_interno_conta)),0) vl_glosa_aceita,
	nvl(max((	select	nvl(sum(obter_total_protocolo(x.nr_Seq_protocolo)),0)
			from	protocolo_convenio x,
				titulo_receber d
			where	x.nr_Seq_protocolo = d.nr_Seq_protocolo)),0) vl_tot_prot,
	nvl(max((	select	nvl(sum(obter_valores_guia_grc(q.nr_seq_lote_hist,q.nr_interno_conta,q.cd_autorizacao,'VA')),0)
			from	lote_audit_hist_guia q ,
				conta_paciente t
			where	q.nr_interno_conta = t.nr_interno_conta
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) vl_recursar,
	nvl(max((	select	nvl(sum(g.vl_saldo_titulo),0)
		from	titulo_receber g,
			protocolo_convenio f
		where	f.nr_Seq_protocolo = a.nr_seq_protocolo
		and	f.nr_seq_protocolo = g.nr_seq_protocolo
		and	g.dt_pagamento_previsto < sysdate)),0) vl_vencido,
	nvl(max((	select	nvl(sum(g.vl_saldo_titulo),0)
		from	titulo_receber g,
			protocolo_convenio f
		where	f.nr_Seq_protocolo = a.nr_seq_protocolo
		and	f.nr_seq_protocolo = g.nr_seq_protocolo
		and	g.dt_pagamento_previsto > sysdate)),0) vl_vencer,
	nvl(max((select	sum(q.vl_amenor)
	from	convenio_retorno_item q,
		conta_paciente c
	where	c.nr_interno_conta = q.nr_interno_conta
	and	c.nr_seq_protocolo = a.nr_seq_protocolo
	and	q.vl_pago > 0
	and	not exists	(	select	f.nr_seq_retorno
					from	convenio_retorno_item f,
						conta_paciente g
					where	f.nr_interno_conta 	= g.nr_interno_conta
					and	q.nr_interno_conta	= f.nr_interno_conta
					and	g.nr_seq_protocolo 	= a.nr_Seq_protocolo
					and	f.nr_seq_retorno 	> q.nr_seq_retorno))),0) +
						nvl(max((	select	sum(x.vl_glosado)
								from	convenio_Retorno_item x,
									conta_paciente t
								where	x.nr_interno_conta = t.nr_interno_conta
								and	x.ie_glosa = 'S'
								and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
									nvl(max((	select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
											from	lote_audit_hist_guia r,
												conta_paciente c
											where	r.nr_interno_conta = c.nr_interno_conta
											and	c.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_glosado,
	dividir(nvl(max((	select	nvl(sum(g.vl_saldo_titulo),0)
		from	titulo_receber g,
			protocolo_convenio f
		where	f.nr_Seq_protocolo = a.nr_seq_protocolo
		and	f.nr_seq_protocolo = g.nr_seq_protocolo
		and	g.dt_pagamento_previsto < sysdate)),0),nvl(max((	select	nvl(sum(obter_total_protocolo(x.nr_Seq_protocolo)),0)
			from	protocolo_convenio x,
				titulo_receber d
			where	x.nr_Seq_protocolo = d.nr_Seq_protocolo)),0)) * 100 vl_percent_vencido,
	dividir(nvl(max((	select	nvl(sum(g.vl_saldo_titulo),0)
		from	titulo_receber g,
			protocolo_convenio f
		where	f.nr_Seq_protocolo = a.nr_seq_protocolo
		and	f.nr_seq_protocolo = g.nr_seq_protocolo
		and	g.dt_pagamento_previsto > sysdate)),0),nvl(max((	select	nvl(sum(obter_total_protocolo(x.nr_Seq_protocolo)),0)
			from	protocolo_convenio x,
				titulo_receber d
			where	x.nr_Seq_protocolo = d.nr_Seq_protocolo)),0)) * 100 vl_percent_vencer,
	dividir(nvl(sum((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0),nvl(max((	select	nvl(sum(obter_total_protocolo(x.nr_Seq_protocolo)),0)
			from	protocolo_convenio x,
				titulo_receber d
			where	x.nr_Seq_protocolo = d.nr_Seq_protocolo)),0)) * 100 vl_percent_tot,
	dividir(sum((	select	nvl(sum(x.vl_recebido),0)
		from	titulo_receber_liq x,
			titulo_receber y
		where	x.nr_titulo = y.nr_titulo/*
		*/and	y.nr_Seq_protocolo = a.nr_seq_protocolo)),nvl(sum((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)) * 100 vl_percent_pago,
	dividir(nvl(max((select	nvl(sum(x.vl_saldo_titulo),0)
		from	titulo_receber x
		where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0),nvl(max((	select	nvl(max(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)) * 100 vl_percent_sald_dev,
	dividir(nvl(sum((	select	nvl(sum(b.vl_amenor),0)
			from	convenio_retorno_item b,
				conta_paciente c
			where	b.nr_interno_conta = c.nr_interno_conta/*
			*/and	c.nr_seq_protocolo = a.nr_Seq_protocolo)),0),nvl(sum((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)) * 100 vl_percent_rec,
	dividir(nvl(sum((	select	nvl(sum(m.vl_recebido),0)
		from	titulo_receber_liq m,
			lote_audit_hist_guia p,
			conta_paciente c
		where	m.nr_Seq_lote_hist_guia = p.nr_sequencia/*
		*/and	p.nr_interno_conta 	= c.nr_interno_conta
		and	c.nr_seq_protocolo 	= a.nr_Seq_protocolo)),0),nvl(sum((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)) * 100 vl_percent_rec_recurs,
	dividir(nvl(max(obter_total_protocolo(a.nr_Seq_protocolo)),0) - sum((	select	nvl(sum(x.vl_pago),0)
										from	convenio_retorno_item x,
											conta_paciente y
										where	x.nr_interno_conta = y.nr_interno_conta/*
										*/and	y.nr_Seq_protocolo = a.nr_seq_protocolo)) -
			nvl(max((	select	nvl(sum(x.vl_glosado),0)
					from	convenio_Retorno_item x,
						conta_paciente t
					where	x.nr_interno_conta = t.nr_interno_conta/*
					*/and	x.ie_glosa = 'S'
					and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0),nvl(sum((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)) * 100 vl_percent_glosa,
	dividir(nvl(sum((	select	nvl(sum(t.vl_amenor),0)
		from	convenio_retorno_item t,
			conta_paciente r
		where	t.nr_interno_conta 	= r.nr_interno_conta/*
		*/and	r.nr_Seq_protocolo	= a.nr_Seq_protocolo
		and	t.ie_glosa		= 'S')),0),nvl(sum((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0)) * 100 vl_percent_glosado,
	/*nvl(max((	select	nvl(sum(obter_total_protocolo(a.nr_Seq_protocolo)),0)
			from	protocolo_convenio x
			where	x.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) -
					nvl(max((	select	nvl(sum(x.vl_pago),0)
		from	convenio_retorno_item x,
			conta_paciente y
		where	x.nr_interno_conta = y.nr_interno_conta
		and	y.nr_Seq_protocolo = a.nr_seq_protocolo)),0) -
			nvl(sum((	select	sum(x.vl_glosado)
					from	convenio_Retorno_item x,
						conta_paciente t
					where	x.nr_interno_conta = t.nr_interno_conta
					and	x.ie_glosa = 'S'
					and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) -
						nvl(sum((	select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
								from	lote_audit_hist_guia r
								where	r.nr_interno_conta = c.nr_interno_conta)),0) vl_amenor_ret,*/
	 nvl(max((	select	nvl(sum(j.vl_saldo_titulo),0)
			from	titulo_receber j
			where	j.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_amenor_ret,						
	nvl(max((	select	nvl(sum(t.vl_recebido),0)
		from	titulo_receber_liq t,
			titulo_receber s
		where	s.nr_titulo	 = t.nr_titulo
		and	s.nr_Seq_protocolo = a.nr_seq_protocolo)),0) vl_tit_rec_liq,
		decode(obter_se_negativo(nvl(sum((	select	nvl(max(j.vl_saldo_titulo),0)
							from	titulo_receber j
							where	j.nr_seq_protocolo = a.nr_seq_protocolo)),0)-
								nvl(sum((	select	sum(x.vl_glosado)
										from	convenio_Retorno_item x,
											conta_paciente t
										where	x.nr_interno_conta = t.nr_interno_conta
										and	x.ie_glosa = 'S'
										and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
											nvl(sum((	select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
													from	lote_audit_hist_guia r
													where	r.nr_interno_conta = c.nr_interno_conta)),0)),'S',0,
														nvl(sum((	select	nvl(max(j.vl_saldo_titulo),0)
																from	titulo_receber j
																where	j.nr_seq_protocolo = a.nr_seq_protocolo)),0)-
																	nvl(sum((	select	sum(x.vl_glosado)
																			from	convenio_Retorno_item x,
																				conta_paciente t
																			where	x.nr_interno_conta = t.nr_interno_conta/*
																									*/
																			and	x.ie_glosa = 'S'
			and	t.nr_Seq_protocolo = a.nr_Seq_protocolo)),0) +
				nvl(sum((select 	nvl(sum(obter_valores_guia_grc(r.nr_seq_lote_hist,r.nr_interno_conta,r.cd_autorizacao,'VG')),0)
										from	lote_audit_hist_guia r
										where	r.nr_interno_conta = c.nr_interno_conta)),0)) vl_recursar_analitico
from	protocolo_convenio a,
	conta_paciente c
where	exists(select 1 from titulo_Receber f where f.nr_Seq_protocolo = a.nr_Seq_protocolo)
and	c.nr_seq_protocolo = a.nr_seq_protocolo
group by a.cd_convenio,
	a.nr_Seq_protocolo,
	a.dt_mesano_referencia,
	a.dt_vencimento
/