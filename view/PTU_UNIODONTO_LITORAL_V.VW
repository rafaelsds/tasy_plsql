/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Gerar o arquivo do Uniodonto da interface 2345
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace 
view ptu_uniodonto_litoral_v as
select	a.nr_sequencia			nr_seq_lote,
	substr(e.cd_unimed,2,4)		cd_unimed,
	e.cd_usuario_plano		cd_usuario_plano,
	e.nm_beneficiario		nm_beneficiario,
	e.dt_nascimento			dt_nascimento,
	e.ie_sexo			ie_sexo,
	e.CD_CGC_CPF			nr_cpf,
	' '				nr_pis_pasep,
	e.nr_seq_segurado		nr_seq_segurado,
	d.ds_endereco			ds_endereco,
	' '				nr_endereco,
	d.ds_complemento		ds_complemento,
	d.ds_bairro			ds_bairro,
	d.cd_cep			cd_cep,
	d.nm_municipio			nm_municipio,
	d.sg_uf				sg_uf,
	d.nr_ddd			nr_ddd,
	d.nr_fone			nr_telefone,
	e.cd_plano			cd_plano
from	ptu_mov_produto_benef		e,
	ptu_movimento_benef_compl	d,
	ptu_mov_produto_empresa		c,
	ptu_movimentacao_produto	b,
	ptu_mov_produto_lote		a
where	d.nr_seq_beneficiario		= e.nr_sequencia
and	e.nr_seq_empresa		= c.nr_sequencia
and	c.nr_seq_mov_produto		= b.nr_sequencia
and	b.nr_seq_lote			= a.nr_sequencia;
/
