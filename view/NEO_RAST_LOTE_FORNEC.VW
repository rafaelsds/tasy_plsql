create or replace
view	NEO_RAST_LOTE_FORNEC as
select	b.dt_prescricao dt_utilizado,
	p.cd_pessoa_fisica,
	upper(p.nm_pessoa_fisica) nm_pessoa_fisica, 
	a.cd_local_estoque,
	substr(nvl(obter_desc_local_estoque(a.cd_local_estoque),'Sem Local de Estoque'),1,255) ds_local_estoque,
	e.ds_grupo_material,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,255) ds_material,
	sum(x.qt_material) qt_material,
	substr(obter_dados_material(a.cd_material,'UMC'),1,3) cd_unid_medida,
	l.ds_lote_fornec,
	l.dt_validade,
	substr(obter_desc_marca(l.nr_seq_marca),1,100) ds_marca,
	'' nm_manip,
	'' nm_alt,
	''nome_medico,
	substr(obter_medico_prescricao(a.nr_prescricao,'N'),1,100) nm_medico,
	'Prescricao' ie_origem
from	prescr_material a,
	prescr_medica b,
	pessoa_fisica p,
	material_lote_fornec l,
	material_atend_paciente x,
	estrutura_material_v e
where	b.cd_pessoa_fisica = p.cd_pessoa_fisica
and	a.nr_prescricao = b.nr_prescricao
and	a.nr_prescricao = x.nr_prescricao
and	a.nr_sequencia = x.nr_sequencia_prescricao
and	x.cd_material = e.cd_material
and	x.nr_seq_lote_fornec = l.nr_sequencia
group by 	b.dt_prescricao,
	p.cd_pessoa_fisica,
	p.nm_pessoa_fisica,
	a.cd_local_estoque,
	e.ds_grupo_material,
	a.cd_material,
	l.ds_lote_fornec,
	l.ds_lote_fornec,
	l.dt_validade,
	l.nr_seq_marca,
	a.nr_prescricao
union
select	b.dt_devolucao,
	p.cd_pessoa_fisica,
	upper(p.nm_pessoa_fisica),
	a.cd_local_estoque,
	substr(nvl(obter_desc_local_estoque(a.cd_local_estoque),'Sem Local de Estoque'),1,255) ds_local_estoque,
	e.ds_grupo_material,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,255) ds_material,
	sum(a.qt_material) qt_material,
	substr(obter_dados_material(a.cd_material,'UMC'),1,30) cd_unid_medida,
	l.ds_lote_fornec,
	l.dt_validade,
	substr(obter_desc_marca(l.nr_seq_marca),1,100) ds_marca,
	substr(obter_pessoa_fisica_usuario(a.nm_usuario_nrec,'N'),1,100) nm_manip,
	'' nome_medico,
	'' nm_alt,
	'' nm_medico,
	'Devolu��o' ie_origem
from	pessoa_fisica p,
 	atendimento_paciente c,
	devolucao_material_pac b,
	item_devolucao_material_pac a,
	estrutura_material_v e,
	material_lote_fornec l
where	c.cd_pessoa_fisica = p.cd_pessoa_fisica
and	b.nr_atendimento = c.nr_atendimento
and	a.nr_devolucao = b.nr_devolucao
and	a.cd_material = e.cd_material
and	a.nr_seq_lote_fornec = l.nr_sequencia
group by	b.dt_devolucao,
	p.cd_pessoa_fisica,
	p.nm_pessoa_fisica,
	a.cd_local_estoque,
	e.ds_grupo_material,
	a.cd_material,
	l.ds_lote_fornec,
	l.ds_lote_fornec,
	l.dt_validade,
	l.nr_seq_marca,
	substr(obter_pessoa_fisica_usuario(a.nm_usuario_nrec,'N'),1,100)
union
select	dt_atendimento,
	p.cd_pessoa_fisica,
	upper(p.nm_pessoa_fisica),
	a.cd_local_estoque,
	substr(nvl(obter_desc_local_estoque(a.cd_local_estoque),'Sem Local de Estoque'),1,255) ds_local_estoque,
	e.ds_grupo_material,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,255) ds_material,
	sum(decode(a.qt_material, 0 , c.qt_movimento, a.qt_material)) qt_material,
	substr(obter_dados_material(a.cd_material,'UMC'),1,3) cd_unid_medida,
	l.ds_lote_fornec,
	l.dt_validade,
	substr(obter_desc_marca(l.nr_seq_marca),1,100) ds_marca,
	substr(obter_pessoa_fisica_usuario(a.nm_usuario_original,'N'),1,100) nm_manip,
	substr(obter_pessoa_fisica_usuario(a.nm_usuario,'N'),1,100) nm_alt,
	obter_nome_medico(b.cd_medico_resp,'G') nome_medico,
	'' nm_medico, 
	'Execu��o' ie_origem
from	pessoa_fisica p,
	atendimento_paciente b,
	movimento_estoque c,
	material_atend_paciente a,
	material_lote_fornec l,
	estrutura_material_v e
where	b.cd_pessoa_fisica = p.cd_pessoa_fisica
and	a.cd_material = c.cd_material
and	a.nr_atendimento = c.nr_atendimento
and	a.nr_sequencia = c.nr_seq_tab_orig
and	c.cd_local_estoque <> 11
and	a.nr_atendimento = b.nr_atendimento
and	a.nr_seq_lote_fornec = l.nr_sequencia
and	a.nr_sequencia_prescricao is null
and	e.cd_material = a.cd_material
group by	dt_atendimento,
	p.cd_pessoa_fisica,
	p.nm_pessoa_fisica,
	a.cd_local_estoque,
	b.cd_medico_resp,
	e.ds_grupo_material,
	a.cd_material,
	l.ds_lote_fornec,
	l.ds_lote_fornec,
	l.dt_validade,
	l.nr_seq_marca,
	a.nm_usuario,
	substr(obter_pessoa_fisica_usuario(a.nm_usuario_original,'N'),1,100)
/