create or replace view hs_totalizador_por_periodo as
select	nr_atendimento, dt_entrada_unidade, tp_periodo, ds_periodo, ds_proc_exame, sum(qt_procedimento) qt_procedimento
from
	(
		select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, qt_procedimento, 1 tp_periodo, 'Manhã' ds_periodo
		from
			(
				select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, 1 qt_procedimento, to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(dt_entrada_unidade, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') hr_entrada_unidade
				from
					(
						select  pp.nr_atendimento, pp.dt_entrada_unidade, 'Sala de Recuperação Anestésica' ds_proc_exame, sum(pp.qt_procedimento) qt_procedimento
						from    procedimento_paciente pp
						where   pp.nr_seq_proc_interno = 25250
						and 	pp.cd_setor_atendimento in ( 4, 117 )
						group by pp.nr_atendimento, pp.dt_entrada_unidade
						having sum(pp.qt_procedimento) > 0
					) tmp_tabela
			) tmp_tabela
		where 	hr_entrada_unidade between to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 07:00:00', 'dd/mm/yyyy hh24:mi:ss') and to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 12:59:59', 'dd/mm/yyyy hh24:mi:ss')
		union
		select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, qt_procedimento, 2 tp_periodo, 'Tarde' ds_periodo
		from
			(
				select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, 1 qt_procedimento, to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(dt_entrada_unidade, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') hr_entrada_unidade
				from
					(
						select  pp.nr_atendimento, pp.dt_entrada_unidade, 'Sala de Recuperação Anestésica' ds_proc_exame, sum(pp.qt_procedimento) qt_procedimento
						from    procedimento_paciente pp
						where   pp.nr_seq_proc_interno = 25250
						and 	pp.cd_setor_atendimento in ( 4, 117 )
						group by pp.nr_atendimento, pp.dt_entrada_unidade
						having sum(pp.qt_procedimento) > 0
					) tmp_tabela
			) tmp_tabela
		where 	hr_entrada_unidade between to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 13:00:00', 'dd/mm/yyyy hh24:mi:ss') and to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 18:59:59', 'dd/mm/yyyy hh24:mi:ss')
		union
		select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, qt_procedimento, 3 tp_periodo, 'Noite' ds_periodo
		from
			(
				select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, 1 qt_procedimento, to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(dt_entrada_unidade, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') hr_entrada_unidade
				from
					(
						select  pp.nr_atendimento, pp.dt_entrada_unidade, 'Sala de Recuperação Anestésica' ds_proc_exame, sum(pp.qt_procedimento) qt_procedimento
						from    procedimento_paciente pp
						where   pp.nr_seq_proc_interno = 25250
						and 	pp.cd_setor_atendimento in ( 4, 117 )
						group by pp.nr_atendimento, pp.dt_entrada_unidade
						having sum(pp.qt_procedimento) > 0
					) tmp_tabela
			) tmp_tabela
		where 	hr_entrada_unidade between to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 19:00:00', 'dd/mm/yyyy hh24:mi:ss') and to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 23:59:59', 'dd/mm/yyyy hh24:mi:ss')
		union
		select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, qt_procedimento, 3 tp_periodo, 'Noite' ds_periodo
		from
			(
				select	nr_atendimento, dt_entrada_unidade, ds_proc_exame, 1 qt_procedimento, to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(dt_entrada_unidade, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') hr_entrada_unidade
				from
					(
						select  pp.nr_atendimento, pp.dt_entrada_unidade, 'Sala de Recuperação Anestésica' ds_proc_exame, sum(pp.qt_procedimento) qt_procedimento
						from    procedimento_paciente pp
						where   pp.nr_seq_proc_interno = 25250
						and 	pp.cd_setor_atendimento in ( 4, 117 )
						group by pp.nr_atendimento, pp.dt_entrada_unidade
						having sum(pp.qt_procedimento) > 0
					) tmp_tabela
			) tmp_tabela
		where 	hr_entrada_unidade between to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 00:00:00', 'dd/mm/yyyy hh24:mi:ss') and to_date(to_char(sysdate, 'dd/mm/yyyy') || ' 06:59:59', 'dd/mm/yyyy hh24:mi:ss')
	) tmp_tabela
group by	nr_atendimento, dt_entrada_unidade, tp_periodo, ds_periodo, ds_proc_exame
order by	tp_periodo;