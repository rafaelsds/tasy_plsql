CREATE or REPLACE VIEW grafico_dispositivos_html_v
AS
/* Dispositivo*/
select substr(b.ds_dispositivo,1,80) NAME,
       '' description,
       null max_scale,
       null  min_scale,
       substr(Obter_Cor_Grafico_Dispositivo(nr_seq_dispositivo,'S'),1,30) COLOR,
       a.dt_instalacao, 
       'lines' STYLE,
       null shape,
       10 qt_size,       
       null point_shape,
       null fill_position,
       'true' shaded,
       0 fixed_scale,       
       (sysdate + 0.1) end_date,
       'D' code_group,
       a.nr_sequencia code_chart,
       b.nr_seq_apres,
	   a.dt_retirada dt_fim,
	   nvl(a.ie_sucesso,'S') ie_sucesso,
	   a.nr_atendimento nr_Atendimento,
	   a.nr_cirurgia nr_cirurgia,
	   a.nr_seq_pepo nr_seq_pepo,
	nvl(b.ie_permite_disp_alta,'N') ie_permite_disp_alta
from   dispositivo b, 
       atend_pac_dispositivo a
where a.nr_seq_dispositivo = b.nr_sequencia
and   obter_se_exibe_graf_disp(a.nr_sequencia) = 'S'
Union all /*Equipamentos*/
select substr(d.ds_equipamento,1,80) NAME,
        '' description,
       null max_scale,
       null  min_scale,
       substr(obter_dados_equipamento(c.cd_equipamento,'COR','S'),1,30) COLOR,
       c.dt_inicio dt_instalacao,
       'lines' STYLE,
       null shape,
       10 qt_size,       
       null point_shape,
       null fill_position,
       'true' shaded,
       0 fixed_scale,       
       (sysdate + 0.1) end_date,
       'E' code_group,
       c.nr_sequencia code_chart,
       1 nr_seq_apres,
	   c.dt_fim dt_fim,
	   'S' ie_sucesso,
	   c.nr_atendimento nr_Atendimento,
	   c.nr_cirurgia nr_cirurgia,
	   c.nr_seq_pepo nr_seq_pepo,
	null ie_permite_disp_alta
from  equipamento_cirurgia c,
       equipamento d
where  c.cd_equipamento = d.cd_equipamento
and    nvl(obter_dados_equipamento(c.cd_equipamento,'GD'),'S') = 'S'
and    c.dt_inicio is not null
order by nr_seq_apres desc, dt_instalacao desc;
/