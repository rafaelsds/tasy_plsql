Create or replace view HSP_EIS_OCUPACAO_SETOR_V
AS
SELECT 		a.cd_estabelecimento,
		a.cd_unidade_basica,
		a.cd_unidade_compl,
		A.DT_REFERENCIA,
		A.CD_SETOR_ATENDIMENTO,
 		S.DS_SETOR_ATENDIMENTO,
		A.IE_PERIODO,
		S.CD_CLASSIF_SETOR,
		(sum(DECODE(a.IE_SITUACAO,'P',NR_PACIENTES,1))) NR_UNIDADES_SETOR,
		-- Troquei as duas linhas abaixo pela linha acima em 11/02/2008 - Com Ricardo
		--(sum(DECODE(a.IE_SITUACAO,'P',NR_PACIENTES,0)) + 
		--		max(DECODE(a.IE_SITUACAO,'L',1,decode(a.IE_SITUACAO, 'I', 1, 0)))) NR_UNIDADES_SETOR,
		--dividir(SUM(decode(Obter_Se_Leito_Disp(a.cd_setor_atendimento, a.cd_unidade_basica, a.cd_unidade_compl), 'S', 1, 0)),2) qt_disponiveis,
		--Troquei pela linha abaixo
		SUM(decode(Obter_Se_Leito_Disp(a.cd_setor_atendimento, a.cd_unidade_basica, a.cd_unidade_compl), 'S', 1, 0)) qt_disponiveis,
		SUM(decode(Obter_Se_Leito_Disp(a.cd_setor_atendimento, a.cd_unidade_basica, a.cd_unidade_compl), 'N', 1, 0)) qt_ind,
		decode(Obter_Se_Unidade_Temp(a.cd_setor_atendimento, a.dt_referencia, a.cd_unidade_basica, a.cd_unidade_compl), 'N',
	        	sum(DECODE(a.IE_SITUACAO,'P',NR_PACIENTES,0)),
			sum(decode(a.cd_pessoa_fisica, null, 0, NR_PACIENTES))) NR_LEITOS_OCUPADOS,
		decode(Obter_Se_Unidade_Temp(a.cd_setor_atendimento, a.dt_referencia, a.cd_unidade_basica, a.cd_unidade_compl), 'N',
	        	sum(decode(a.cd_pessoa_fisica, null, 1, DECODE(a.IE_SITUACAO,'L',nr_pacientes,0))),
			--sum(decode(a.cd_pessoa_fisica, null, 1, DECODE(a.IE_SITUACAO,'L',1,0))), Dalcastagne em 11/06/2008 - Mudei pela linha acima OS 92512
			sum(decode(a.cd_pessoa_fisica, null, 1, 0))) NR_LEITOS_Livres,
        	SUM(NR_ADMISSOES) NR_ADMISSOES,
        	SUM(NR_ALTAS) NR_ALTAS,
        	SUM(NR_OBITOS) NR_OBITOS,
        	SUM(NR_TRANSF_ENTRADA) NR_TRANSF_ENTRADA,
        	SUM(NR_TRANSF_SAIDA)  NR_TRANSF_SAIDA,
       	avg(DECODE(A.IE_PERIODO,'M', To_Number(TO_CHAR(LAST_DAY(A.DT_REFERENCIA),'DD')),1)) NR_DIAS_PERIODO,
		nvl(ie_ocup_hospitalar,'S') ie_ocup_Hospitalar,
		substr(Obter_Se_Unidade_Temp(a.cd_setor_atendimento, a.dt_referencia, a.cd_unidade_basica, a.cd_unidade_compl),1,1) ie_temp,
		a.IE_SITUACAO,
		sum(decode(Obter_Se_Unidade_Temp(a.cd_setor_atendimento, a.dt_referencia, a.cd_unidade_basica, a.cd_unidade_compl), 'S', 1, 0)) 		
nr_unidades_temporarias,     
		sum(decode(a.ie_situacao, 'P', 1, 0)) nr_unidades_ocupadas,  
		sum(decode(a.ie_situacao, 'M', 1, 0)) qt_unidade_acomp,     
		sum(decode(a.ie_situacao, 'I', 1, 0)) nr_unidades_interditadas,     
		sum(decode(a.ie_situacao, 'L', 1, 0)) nr_unidades_livres,     
		sum(decode(a.ie_situacao, 'H', 1, 0)) nr_unidades_higienizacao,
		sum(decode(a.ie_situacao, 'R', 1, 0)) nr_unidades_reservadas,
		sum(decode(a.ie_situacao, 'O', 1, 0)) qt_unidades_isolamento,
		sum(decode(a.ie_situacao, 'A', 1, 0)) qt_unidades_alta,
		sum(decode(Obter_Se_Unidade_Temp(a.cd_setor_atendimento, a.dt_referencia, a.cd_unidade_basica, a.cd_unidade_compl), 'S', 		
decode(a.ie_situacao, 'P', 1, 0),0)) nr_unid_temp_ocup,
		a.cd_tipo_acomodacao
FROM   		SETOR_ATENDIMENTO S,
        	hsp_ocupacao_hospitalar A
WHERE 		A.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO
GROUP BY 	a.cd_estabelecimento,
		A.DT_REFERENCIA,
		a.cd_unidade_basica,
		a.cd_unidade_compl,
		A.CD_SETOR_ATENDIMENTO,
	       	S.DS_SETOR_ATENDIMENTO,
		A.IE_PERIODO,
		S.CD_CLASSIF_SETOR,
		a.cd_tipo_acomodacao,
		a.ie_temp,
		nvl(ie_ocup_hospitalar,'S'),
		a.IE_SITUACAO;
/