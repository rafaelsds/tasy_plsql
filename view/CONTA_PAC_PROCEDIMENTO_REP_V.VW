create or replace view conta_pac_procedimento_rep_v
as
select  c.nr_atendimento,
        substr(obter_nome_pf(c.cd_pessoa_fisica),1,40) nm_pessoa_fisica,
        c.dt_entrada,
        substr(obter_nome_medico(a.cd_medico, 'N'),1,100) nm_medico,
	b.cd_procedimento cd_item,
        f.ds_procedimento ds_item,
        substr(obter_valor_dominio(1129, a.ie_status),1,50) ds_status,
        a.nr_repasse_terceiro,
        a.vl_repasse,
        a.vl_liberado,         
        a.dt_contabil,
        a.dt_contabil_titulo,
        a.nr_sequencia,
        substr(obter_nome_terceiro(g.nr_sequencia),1,60) ds_terceiro,
        a.cd_regra,
        a.dt_liberacao,
        b.dt_conta,
        a.nr_seq_criterio,
	g.cd_cgc,
	c.cd_pessoa_fisica,
	a.cd_medico,
	b.nr_interno_conta,
	212 ie_emite_conta,
	b.nr_seq_proc_pacote,
	d.nr_seq_protocolo,
	b.vl_procedimento vl_procedimento,
	b.qt_procedimento qt_procedimento
from    procedimento f,
        atendimento_paciente c,
        terceiro g,
        procedimento_repasse a,
        procedimento_paciente b,
	conta_paciente d
where   a.nr_seq_procedimento   = b.nr_sequencia
and     c.nr_atendimento        = b.nr_atendimento
and     f.cd_procedimento       = b.cd_procedimento
and     f.ie_origem_proced      = b.ie_origem_proced
and     g.nr_sequencia          = a.nr_seq_terceiro
and	b.nr_interno_conta	= d.nr_interno_conta;
/
