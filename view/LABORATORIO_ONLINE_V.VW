create or replace view laboratorio_online_v as
select 10 cd_tipo_registro,
	 Lab_obter_amostra_prescr(f.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia) ds_amostra,
	 ' ' ie_reservado,
	 'N' ie_repeticao,
	 ' ' qt_diluicao,
	 ' ' cd_agrupamento,
	 ' ' ie_reservado2,
	 max(b.dt_atualizacao) dt_coleta,
	 decode(a.ie_urgencia, 'S', 'U', 'R') ie_prioridade,
	 nvl(d.cd_material_integracao, d.cd_material_exame) cd_material,
	 ' ' cd_instrumento,
	 a.nr_prescricao,
	 a.cd_setor_atendimento ie_origem,
 	 SUBSTR(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8','',c.nr_seq_grupo_imp, nr_seq_lab, f.ie_status_envio, 'CETUS', null,nr_seq_lote_externo), 1, 160) ds_exames,
--	 SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160) ds_exames,
	 '' nm_paciente,
	 '0' dt_idade,
	 sysdate dt_nascimento,
	 '' ie_sexo,
	 0  ie_cor,
	 '' cd_atributo,
	 '' ds_valor,
	 '10' ie_digito,
	 ' ' ds_sigla,
	 e.cd_setor_atendimento cd_setor_prescr
from 	material_exame_lab d,
	lab_parametro f,
	exame_laboratorio c,
	prescr_medica e,
	prescr_proc_etapa b,
	atendimento_paciente h,
	prescr_procedimento a
where a.nr_seq_exame = c.nr_seq_exame
  and ((a.nr_seq_lab is not null) or (f.ie_padrao_amostra = 'PM'))
  and f.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
  and a.cd_material_exame = d.cd_material_exame
  and a.nr_prescricao 	= b.nr_prescricao(+)
  and a.nr_prescricao 	= e.nr_prescricao
  and a.nr_sequencia 	= b.nr_seq_prescricao(+)
  and f.ie_status_envio(+) = b.ie_etapa
  and f.cd_estabelecimento = e.cd_estabelecimento
  and a.nr_prescricao 	= e.nr_prescricao
  and e.nr_atendimento = h.nr_atendimento
  and a.dt_integracao is null
  and LAB_Obter_Codigo_Exame_Equip('CETUS',a.nr_seq_exame,h.ie_tipo_atendimento,a.ie_urgencia,d.nr_sequencia,'CEX') is not null
  and length(SUBSTR(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8','',c.nr_seq_grupo_imp, nr_seq_lab, f.ie_status_envio, 'CETUS', null,nr_seq_lote_externo), 1, 160)) > 0
--  and length(SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160)) > 0
  and Obter_Equipamento_Exame(a.nr_seq_exame,null,'CETUS') is not null
group by  Lab_obter_amostra_prescr(f.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia),
	nvl(d.cd_material_integracao, d.cd_material_exame),
	 decode(a.ie_urgencia, 'S', 'U', 'R'),
	 a.nr_prescricao,
	 a.cd_setor_atendimento,
 	 SUBSTR(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8','',c.nr_seq_grupo_imp, nr_seq_lab, f.ie_status_envio, 'CETUS', null,nr_seq_lote_externo), 1, 160),
--	 SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160),
	 e.cd_setor_atendimento
union
select 10 cd_tipo_registro,
	 Lab_obter_amostra_prescr(f.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia) ds_amostra,
	 ' ' ie_reservado,
	 'N' ie_repeticao,
	 ' ' qt_diluicao,
	 ' ' cd_agrupamento,
	 ' ' ie_reservado2,
	 max(b.dt_atualizacao) dt_coleta,
	 decode(a.ie_urgencia, 'S', 'U', 'R') ie_prioridade,
	 nvl(d.cd_material_integracao, d.cd_material_exame) cd_material,
	 ' ' cd_instrumento,
	 a.nr_prescricao,
	 a.cd_setor_atendimento ie_origem,
 	 SUBSTR(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8','',c.nr_seq_grupo_imp, nr_seq_lab, f.ie_status_envio, 'CETUS', null,nr_seq_lote_externo), 1, 160) ds_exames,
--	 SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160) ds_exames,
	 '' nm_paciente,
	 '0' dt_idade,
	 sysdate dt_nascimento,
	 '' ie_sexo,
	 0  ie_cor,
	 '' cd_atributo,
	 '' ds_valor,
	 '10' ie_digito,
	 ' ' ds_sigla,
	 e.cd_setor_atendimento cd_setor_prescr
from 	material_exame_lab d,
	lab_parametro f,
	exame_laboratorio c,
	prescr_medica e,
	prescr_proc_etapa b,
	atendimento_paciente h,
	prescr_procedimento a
where a.nr_seq_exame = c.nr_seq_exame
  and a.cd_material_exame = d.cd_material_exame
  and e.cd_estabelecimento = f.cd_estabelecimento
  and ((a.nr_seq_lab is not null) or (f.ie_padrao_amostra = 'PM'))
  and f.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
  and a.nr_prescricao 	= b.nr_prescricao(+)
  and a.nr_prescricao 	= e.nr_prescricao
  and a.nr_sequencia 	= b.nr_seq_prescricao(+)
  and f.ie_status_envio(+) = b.ie_etapa
  and f.cd_estabelecimento = e.cd_estabelecimento
  and e.nr_atendimento = h.nr_atendimento
  and a.dt_integracao is null
  and Obter_Equipamento_Exame(a.nr_seq_exame,null,'CETUS') is not null
  and LAB_Obter_Codigo_Exame_Equip('CETUS',a.nr_seq_exame,h.ie_tipo_atendimento,a.ie_urgencia,d.nr_sequencia,'CEX') is not null
  and length(SUBSTR(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8','',c.nr_seq_grupo_imp, nr_seq_lab, f.ie_status_envio, 'CETUS', null,nr_seq_lote_externo), 1, 160)) > 0
--  and length(SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160)) > 0
group by   Lab_obter_amostra_prescr(f.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia),
	nvl(d.cd_material_integracao, d.cd_material_exame),
	 decode(a.ie_urgencia, 'S', 'U', 'R'),
	 a.nr_prescricao,
	 a.cd_setor_atendimento,
	SUBSTR(Obter_Exames_Prescr_Lab_integr(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8','',c.nr_seq_grupo_imp, nr_seq_lab, f.ie_status_envio, 'CETUS', null,nr_seq_lote_externo), 1, 160),
--	SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, c.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160),
	 e.cd_setor_atendimento
union
select	distinct
	 11 cd_tipo_registro,
	 Lab_obter_amostra_prescr(f.ie_padrao_amostra,a.nr_prescricao,a.nr_seq_exame,a.nr_sequencia) ds_amostra,
	 '' ie_reservado,
	 '' ie_repeticao,
	 '' qt_diluicao,
	 '' cd_agrupamento,
	 '' ie_reservado2,
	 sysdate dt_coleta,
	 '' ie_prioridade,
	 '' cd_material,
	 '' cd_instrumento,
	 a.nr_prescricao,
	 0 ie_origem,
	 '' ds_exames,
	 c.nm_pessoa_fisica nm_paciente,
	 obter_idade(c.dt_nascimento, b.dt_prescricao, 'A') dt_idade,
	 c.dt_nascimento,
	 c.ie_sexo,
	 c.nr_seq_cor_pele ie_cor,
	 '' cd_atributo,
	 '' ds_valor,
	 '11' ie_digito,
	 '',
	 b.cd_setor_atendimento cd_setor_prescr
from  material_exame_lab e,
	lab_parametro f,
	exame_laboratorio d,
	pessoa_fisica c,
	prescr_medica b,
	atendimento_paciente h,
	prescr_procedimento a
where a.nr_prescricao = b.nr_prescricao
  and b.cd_pessoa_fisica = c.cd_pessoa_fisica
  and f.cd_estabelecimento = b.cd_estabelecimento
  and a.nr_seq_exame = d.nr_seq_exame
  and ((a.nr_seq_lab is not null) or ( f.ie_padrao_amostra = 'PM'))
  and f.ie_padrao_amostra NOT IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
  and a.cd_material_exame = e.cd_material_exame
  and b.nr_atendimento = h.nr_atendimento
  and a.dt_integracao is null
  and LAB_Obter_Codigo_Exame_Equip('CETUS',a.nr_seq_exame,h.ie_tipo_atendimento,a.ie_urgencia,e.nr_sequencia,'CEX') is not null
  and length(SUBSTR(obter_exames_prescr_lab(a.nr_prescricao, a.cd_setor_atendimento, a.cd_material_exame, d.nr_seq_grupo, NULL, 'CI8;' || f.ie_status_envio, '', 0), 1, 160)) > 0
  and Obter_Equipamento_Exame(a.nr_seq_exame,null,'CETUS') is not null
union
select	distinct
	 11 cd_tipo_registro,
 	 lab_obter_caracteres_amostra(g.cd_barras,f.ie_padrao_amostra)  ds_amostra,
	 '' ie_reservado,
	 '' ie_repeticao,
	 '' qt_diluicao,
	 '' cd_agrupamento,
	 '' ie_reservado2,
	 sysdate dt_coleta,
	 '' ie_prioridade,
	 '' cd_material,
	 '' cd_instrumento,
	 a.nr_prescricao,
	 0 ie_origem,
	 '' ds_exames,
	 c.nm_pessoa_fisica nm_paciente,
	 obter_idade(c.dt_nascimento, b.dt_prescricao, 'A') dt_idade,
	 c.dt_nascimento,
	 c.ie_sexo,
	 c.nr_seq_cor_pele ie_cor,
	 '' cd_atributo,
	 '' ds_valor,
	 '11' ie_digito,
	 '',
	 b.cd_setor_atendimento cd_setor_prescr
from  material_exame_lab e,
	lab_parametro f,
	exame_laboratorio d,
	pessoa_fisica c,
	prescr_medica b,
	prescr_procedimento a,
	atendimento_paciente h,
	prescr_proc_mat_item g
where a.nr_prescricao = b.nr_prescricao
  and b.cd_pessoa_fisica = c.cd_pessoa_fisica
  and g.nr_prescricao = a.nr_prescricao
  and g.nr_seq_prescr = a.nr_sequencia
  and f.cd_estabelecimento = b.cd_estabelecimento
  and a.nr_seq_exame = d.nr_seq_exame
  and f.ie_padrao_amostra IN ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
  and a.cd_material_exame = e.cd_material_exame
  and b.nr_atendimento = h.nr_atendimento
 and g.dt_integracao is null
 and LAB_Obter_Codigo_Exame_Equip('CETUS',a.nr_seq_exame,h.ie_tipo_atendimento,a.ie_urgencia,e.nr_sequencia,'CEX') is not null
  and length(substr(obter_exames_lab_integr_item(a.nr_prescricao, a.cd_setor_atendimento,'CI8','',f.ie_status_envio,'CETUS', g.nr_seq_prescr_proc_mat ,a.nr_seq_lote_externo,b.cd_estabelecimento), 1, 160)) > 0
  and obter_equipamento_exame(a.nr_seq_exame,null,'CETUS') is not null
union
select 10 cd_tipo_registro,
 	 lab_obter_caracteres_amostra(g.cd_barras,f.ie_padrao_amostra)  ds_amostra,
	 ' ' ie_reservado,
	 'N' ie_repeticao,
	 ' ' qt_diluicao,
	 ' ' cd_agrupamento,
	 ' ' ie_reservado2,
	 max(a.dt_coleta) dt_coleta,
	 decode(a.ie_urgencia, 'S', 'U', 'R') ie_prioridade,
	 nvl(d.cd_material_integracao, d.cd_material_exame) cd_material,
	 ' ' cd_instrumento,
	 a.nr_prescricao,
	 a.cd_setor_atendimento ie_origem,
 	 substr(obter_exames_lab_integr_item(a.nr_prescricao, a.cd_setor_atendimento,'CI8','',f.ie_status_envio,'CETUS', g.nr_seq_prescr_proc_mat ,a.nr_seq_lote_externo,e.cd_estabelecimento), 1, 160) ds_exames,
	 '' nm_paciente,
	 '0' dt_idade,
	 sysdate dt_nascimento,
	 '' ie_sexo,
	 0  ie_cor,
	 '' cd_atributo,
	 '' ds_valor,
	 '10' ie_digito,
	 ' ' ds_sigla,
	 e.cd_setor_atendimento cd_setor_prescr
from 	material_exame_lab d,
	lab_parametro f,
	exame_laboratorio c,
	prescr_medica e,
	atendimento_paciente h,
	prescr_procedimento a,
	prescr_proc_mat_item g
where a.nr_seq_exame = c.nr_seq_exame
  and f.ie_padrao_amostra IN  ('AMO9','AMO10','AMO11','AM10F','AM11F','AMO13')
  and a.cd_material_exame = d.cd_material_exame
  and a.nr_prescricao 	= e.nr_prescricao
  and f.cd_estabelecimento = e.cd_estabelecimento
  and a.nr_prescricao 	= e.nr_prescricao
  and g.dt_integracao is null
  and e.nr_atendimento = h.nr_atendimento
   and g.nr_prescricao = a.nr_prescricao
  and g.nr_seq_prescr = a.nr_sequencia
  and LAB_Obter_Codigo_Exame_Equip('CETUS',a.nr_seq_exame,h.ie_tipo_atendimento,a.ie_urgencia,d.nr_sequencia,'CEX') is not null
  and length(substr(obter_exames_lab_integr_item(a.nr_prescricao, a.cd_setor_atendimento,'CI8','',f.ie_status_envio,'CETUS', g.nr_seq_prescr_proc_mat ,a.nr_seq_lote_externo,e.cd_estabelecimento), 1, 160)) > 0
  and obter_equipamento_exame(a.nr_seq_exame,null,'CETUS') is not null
group by   	 g.cd_barras,
 	 lab_obter_caracteres_amostra(g.cd_barras,f.ie_padrao_amostra),
	nvl(d.cd_material_integracao, d.cd_material_exame),
	 decode(a.ie_urgencia, 'S', 'U', 'R'),
	 a.nr_prescricao,
	 a.cd_setor_atendimento,
 	 substr(obter_exames_lab_integr_item(a.nr_prescricao, a.cd_setor_atendimento,'CI8','',f.ie_status_envio,'CETUS', g.nr_seq_prescr_proc_mat ,a.nr_seq_lote_externo,e.cd_estabelecimento), 1, 160),
	 e.cd_setor_atendimento;
/