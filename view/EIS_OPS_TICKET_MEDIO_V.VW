create or replace view eis_ops_ticket_medio_v
as
select	dt_mes_competencia,
	cd_estabelecimento,
	qt_segurado,
	vl_receitas,
	dividir(vl_receitas, qt_segurado) vl_ticket_receita,
	vl_custo,
	dividir(vl_custo, qt_segurado) vl_ticket_custo
FROM	(
	select	pkg_date_utils.start_of(d.dt_mes_competencia,'MONTH') dt_mes_competencia,
		a.cd_estabelecimento,
		sum(nvl(a.vl_mensalidade,0) + nvl(a.vl_faturado,0) + nvl(a.vl_taxa,0)) vl_receitas,
		sum(nvl(a.vl_conta,0) + nvl(a.vl_reembolso,0) + nvl(a.vl_ressarcir,0) + nvl(a.vl_recurso,0) - nvl(a.vl_coparticipacao,0)) vl_custo,
		count(distinct a.nr_seq_segurado) qt_segurado
	from	pls_ar_dados_v a,
		pls_ar_lote d,
		pls_segurado s
	where	d.nr_sequencia = a.nr_seq_lote
	and	a.nr_seq_segurado = s.nr_sequencia
	group by pkg_date_utils.start_of(d.dt_mes_competencia,'MONTH'), a.cd_estabelecimento
	);
/
