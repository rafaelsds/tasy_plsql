CREATE OR REPLACE VIEW SUS_PREVIA_PROTOCOLO_AIH_V as
select	sum(decode(nvl(p.ie_doc_executor,5),3,nvl(s.vl_matmed,0)+ nvl(s.vl_sadt,0),
	6,nvl(s.vl_matmed,0)+ nvl(s.vl_sadt,0),0)) vl_terceiros_hosp,
	sum(decode(nvl(p.ie_doc_executor,5),5,nvl(s.vl_matmed,0)+ nvl(s.vl_sadt,0),
	1,nvl(s.vl_matmed,0)+ nvl(s.vl_sadt,0),0)) vl_proprio_hosp,
	sum(decode(sus_obter_procconta_rateio_aih(p.nr_sequencia),'N',
	decode(nvl(p.ie_doc_executor,5),3,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),6,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),0),0)) vl_terceiros_prof,
	sum(decode(sus_obter_procconta_rateio_aih(p.nr_sequencia),'N',
	decode(nvl(p.ie_doc_executor,5),5,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),
	1,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),0),0)) vl_proprio_prof,
	sum(decode(sus_obter_procconta_rateio_aih(p.nr_sequencia),'S',
	decode(nvl(p.ie_doc_executor,5),3,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),6,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),0),0)) vl_terceiro_rat,
	sum(decode(sus_obter_procconta_rateio_aih(p.nr_sequencia),'S',
	decode(nvl(p.ie_doc_executor,5),5,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),
	1,nvl(s.vl_medico,0) + nvl(s.vl_ato_anestesista,0),0),0)) vl_proprio_rat,
	substr(Sus_Obter_Estrut_Proc(p.cd_procedimento,p.ie_origem_proced,'CD','F'),1,200) ds_forma_organizacao,
	substr(Sus_Obter_Estrut_Proc(p.cd_procedimento,p.ie_origem_proced,'CD','S'),1,200) ds_subgrupo,
	substr(Sus_Obter_Estrut_Proc(p.cd_procedimento,p.ie_origem_proced,'CD','G'),1,200) ds_grupo,
	sum(nvl(s.vl_medico,0)+ nvl(s.vl_matmed,0) + nvl(s.vl_sadt,0) + nvl(s.vl_ato_anestesista,0)) vl_total,
	c.nr_seq_protocolo,
	c.nr_interno_conta
from	procedimento_paciente p,
	sus_valor_proc_paciente s,
	conta_paciente c
where	p.nr_interno_conta	= c.nr_interno_conta
and	s.nr_sequencia	= p.nr_sequencia
and	p.ie_origem_proced	= 7
and	p.cd_motivo_exc_conta is null
group by Sus_Obter_Estrut_Proc(p.cd_procedimento,p.ie_origem_proced,'CD','F'),
	Sus_Obter_Estrut_Proc(p.cd_procedimento,p.ie_origem_proced,'CD','S'),
	Sus_Obter_Estrut_Proc(p.cd_procedimento,p.ie_origem_proced,'CD','G'),
	c.nr_seq_protocolo,
	c.nr_interno_conta;
/
