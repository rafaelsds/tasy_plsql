create or replace 
view  ADEP_Inconsistencias_v as
select  1 ie_opcao,  
		1 ie_prioridade,
		nr_seq_apresent,
		ds_tipo_item,
		substr(ds_item,1,80) ds_item,
		dt_horario,
		ie_tipo_item,
		nr_prescricao,
		cd_item,
		nr_atendimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		dt_validade_prescr,
		null dt_prev_prox_etapa,
		null dt_controle,
		null dt_retirada_prev,
		null ie_tipo_solucao,
		substr(obter_nome_setor(cd_setor_prescr),1,80) ds_setor_origem,
		substr(obter_nome_setor(obter_setor_atendimento(nr_atendimento)),1,80) ds_setor_atual
from	adep_pend_v
where	dt_horario < sysdate
and		ie_tipo_item <> 'MAP'
and		ADEP_obter_se_consiste_item(ie_tipo_item, obter_perfil_ativo, cd_item, ie_origem_proced, nr_seq_proc_interno) = 'S'
and		dt_liberacao is not null
union all
select	2 ie_opcao,  
		1 ie_prioridade,
		a.nr_seq_apresent,
		a.ds_tipo_item,
		substr(a.ds_item,1,80),
		a.dt_horario,
		a.ie_tipo_item,
		a.nr_prescricao,
		null,
		a.nr_atendimento,
		null,
		null,
		a.dt_validade_prescr,
		null,
		null,
		null,
		null,
		substr(Obter_nome_setor(b.cd_setor_atendimento),1,80) ds_setor_origem,
		substr(Obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,80) ds_setor_atual
from	adep_glic_pend_v a,
		prescr_medica b
where	dt_horario < sysdate
and		a.nr_prescricao = b.nr_prescricao
and		a.dt_liberacao is not null
union all
select  3 ie_opcao,  
		2 ie_prioridade,
		a.ie_tipo_solucao nr_seq_apresent,
		'SNE' ds_tipo_item,
		substr(a.ds_solucao,1,80) ds_item,
		a.dt_status dt_horario,
		'SNE' ie_tipo_item,
		a.nr_prescricao,		
		null,
		a.nr_atendimento,
		null,
		null,
		a.dt_validade_prescr,
		null,
		null,
		null,
		ie_tipo_solucao,
		substr(Obter_nome_setor(b.cd_setor_atendimento),1,80) ds_setor_origem,
		substr(Obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,80) ds_setor_atual
from	adep_sol_prescr_v  a,
		prescr_medica b
where	dt_susp_prescr is null
and		ie_suspenso <> 'S'
and		nvl(ie_acm,'N') <> 'S'
and		a.dt_liberacao is not null
and		a.ie_status_solucao <> 'T'
and		a.dt_validade_prescr > sysdate 
and		a.nr_prescricao = b.nr_prescricao
and		nvl(a.ie_adep,'N') = 'S'
and		dt_status < sysdate
and		ie_tipo_solucao = 2
and		nvl(obter_ult_evento_sne(a.nr_prescricao, a.nr_seq_solucao),0) not in (1,3)
and		obter_se_consiste_min_pausa(a.nr_prescricao, a.nr_seq_solucao) = 'S'
and		adep_obter_se_consiste_item('S', obter_perfil_ativo, null, null, null) = 'S'
union all
select  4 ie_opcao,  
		2 ie_prioridade,
		a.ie_tipo_solucao nr_seq_apresent,
		'Solu��o' ds_tipo_item,
		--substr(a.ds_solucao||' - '||DECODE((NVL(a.nr_etapas,0) - (obter_etapas_adep_sol(1,a.nr_prescricao,a.nr_seq_solucao) + NVL(nr_etapas_suspensa,0))), 0, 'Existem etapas a serem terminadas', 'A etapa '||(obter_etapas_adep_sol(1,a.nr_prescricao,a.nr_seq_solucao) + NVL(a.nr_etapas_suspensa,0))||' esta em andamento e deve ser trocada.'),1,200) ds_item,
		substr(a.ds_solucao||decode(obter_etapas_adep_sol(1,a.nr_prescricao,a.nr_seq_solucao),0,' - A solu��o deve ser iniciada.',' - '||DECODE((NVL(a.nr_etapas,0) - (obter_etapas_adep_sol(1,a.nr_prescricao,a.nr_seq_solucao) + NVL(nr_etapas_suspensa,0))), 0, 'Existem etapas a serem terminadas', 'A etapa '||(obter_etapas_adep_sol(1,a.nr_prescricao,a.nr_seq_solucao) + NVL(a.nr_etapas_suspensa,0))||' esta em andamento e deve ser trocada.')),1,200) ds_item,	
		a.dt_status dt_horario,
		'SOL' ie_tipo_item,
		a.nr_prescricao,
		null,
		a.nr_atendimento,
		null,
		null,
		a.dt_validade_prescr,
		a.dt_prev_prox_etapa,
		null,
		null,
		a.ie_tipo_solucao,
		substr(Obter_nome_setor(b.cd_setor_atendimento),1,80) ds_setor_origem,
		substr(Obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,80) ds_setor_atual
from    adep_sol_prescr_v a,
		prescr_medica b
where   a.dt_susp_prescr is null
and     a.ie_suspenso <> 'S'
and     nvl(a.ie_acm,'N') <> 'S'
and     a.dt_liberacao is not null
and     a.dt_status < sysdate
and     a.nr_prescricao = b.nr_prescricao
and     ie_tipo_solucao = 1
and		nvl(a.ie_adep,'N') = 'S'
and		ADEP_obter_se_consiste_item('SOL', obter_perfil_ativo,null,null, null) = 'S'
union all
select  5 ie_opcao,  
		3 ie_prioridade,
		3 nr_seq_apresent,
		'CCG' ds_tipo_item,
		substr(obter_nome_prot_glic(c.nr_seq_prot_glic),1,80)||' - Confirma��o da Adm pendente' ds_item,
		a.dt_controle dt_horario,
		'CCG' ie_tipo_item,
		b.nr_prescricao,
		null,
		b.nr_atendimento,
		null,
		null,
		b.dt_validade_prescr,
		null,
		a.dt_controle,
		null,
		null,
		substr(Obter_nome_setor(b.cd_setor_atendimento),1,80) ds_setor_origem,
		substr(Obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,80) ds_setor_atual
from	atendimento_glicemia a,
		atend_glicemia c,
		prescr_medica b
where	a.nr_atendimento 	= b.nr_atendimento
and		c.nr_sequencia		= a.nr_seq_glicemia
and		c.nr_atendimento	= a.nr_atendimento
and		c.nr_atendimento	= b.nr_atendimento
and		c.nr_prescricao		= b.nr_prescricao
and		b.dt_validade_prescr 	> sysdate
and		((nvl(a.qt_ui_insulina_calc,0) > 0) or
		 (nvl(a.qt_ui_insulina_adm,0) > 0) or
		 (nvl(a.qt_ui_insulina_int_calc,0) > 0) or
		 (nvl(a.qt_ui_insulina_int_adm,0) > 0) or
		 (nvl(a.qt_glicose,0) > 0) or
		 (nvl(a.qt_glicose_adm,0) > 0))
and		nvl(ie_adm_confirmada,'N') <> 'S'
and		nvl(ie_status_glic, 'P') = 'N'
and     nvl(b.ie_adep,'N') = 'S'
and		ADEP_obter_se_consiste_item('CCG', obter_perfil_ativo, null, null,null) = 'S'
and     dt_liberacao is not null
union all
select  6 ie_opcao,  
		4 ie_prioridade,
		4 nr_seq_apresent,
		'Dispositivo' ds_tipo_item,
		substr(obter_descricao_padrao('DISPOSITIVO','DS_DISPOSITIVO',a.NR_SEQ_DISPOSITIVO),1,80) ds_item,
		a.DT_RETIRADA_PREV dt_horario,
		'Dispositivo' ie_tipo_item,
		to_number(null) nr_prescricao,
		null,
		a.nr_atendimento,
		null,
		null,
		null,
		null,
		null,
		a.dt_retirada_prev,
		null,
		substr(obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,80) ds_setor_origem,
		substr(obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,80) ds_setor_atual
from    atend_pac_dispositivo a
where   a.dt_retirada is null
and     a.dt_retirada_prev is not null;
/