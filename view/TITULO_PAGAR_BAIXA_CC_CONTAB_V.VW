Create or replace 
VIEW Titulo_Pagar_Baixa_cc_Contab_V
AS
select 	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	nvl(b.vl_baixa, a.vl_baixa) vl_transacao,
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_BAIXA' ds_atributo,
	a.nr_bordero,
	a.nr_seq_escrit,
	b.cd_centro_custo,
	b.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	b.nr_sequencia nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa_cc b,
	titulo_pagar_baixa a
where 	a.nr_titulo	= b.nr_titulo(+)
and	a.nr_sequencia	= b.nr_Seq_baixa(+)
and	nvl(b.vl_baixa, a.vl_baixa) <> 0
union all
select 	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	nvl(b.vl_desconto, a.vl_descontos),
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_DESCONTOS',
	a.nr_bordero,
	a.nr_seq_escrit,
	b.cd_centro_custo,
	b.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	b.nr_sequencia nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa_cc b,
	titulo_pagar_baixa a
where 	a.nr_titulo	= b.nr_titulo(+)
and	a.nr_sequencia	= b.nr_Seq_baixa(+)
and	nvl(b.vl_desconto, a.vl_descontos) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	a.vl_outras_deducoes,
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_OUTRAS_DEDUCOES',
	a.nr_bordero,
	a.nr_seq_escrit,
	a.cd_centro_custo,
	a.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	null nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa a
where 	a.vl_outras_deducoes <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	nvl(b.vl_juros, a.vl_juros),
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_JUROS',
	a.nr_bordero,
	a.nr_seq_escrit,
	b.cd_centro_custo,
	b.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	b.nr_sequencia nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa_cc b,
	titulo_pagar_baixa a
where 	a.nr_titulo	= b.nr_titulo(+)
and	a.nr_sequencia	= b.nr_Seq_baixa(+)
and 	nvl(b.vl_juros, a.vl_juros) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	nvl(b.vl_multa, a.vl_multa),
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_MULTA',
	a.nr_bordero,
	a.nr_seq_escrit,
	b.cd_centro_custo,
	b.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	b.nr_sequencia nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa_cc b,
	titulo_pagar_baixa a
where 	a.nr_titulo	= b.nr_titulo(+)
and	a.nr_sequencia	= b.nr_Seq_baixa(+)
and 	nvl(b.vl_multa, a.vl_multa) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	nvl(b.vl_outros_acrescimos, a.vl_outros_acrescimos),
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_OUTROS_ACRESCIMOS',
	a.nr_bordero,
	a.nr_seq_escrit,
	b.cd_centro_custo,
	b.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	b.nr_sequencia nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa_cc b,
	titulo_pagar_baixa a
where 	a.nr_titulo	= b.nr_titulo(+)
and	a.nr_sequencia	= b.nr_Seq_baixa(+)
and 	nvl(b.vl_outros_acrescimos, a.vl_outros_acrescimos) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	nvl(b.vl_pago,a.vl_pago),
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_PAGO',
	a.nr_bordero,
	a.nr_seq_escrit,
	b.cd_centro_custo,
	b.cd_conta_contabil,
	a.dt_baixa,
	a.nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	b.nr_sequencia nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa_cc b,
	titulo_pagar_baixa a
where 	a.nr_titulo	= b.nr_titulo(+)
and	a.nr_sequencia	= b.nr_seq_baixa(+)
and	nvl(b.vl_pago, a.vl_pago) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	a.vl_inss,
	a.nr_seq_conta_banco,
	nr_seq_trans_fin,
	'VL_INSS',
	nr_bordero,
	nr_seq_escrit,
	cd_centro_custo,
	cd_conta_contabil,
	dt_baixa,
	nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	null nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa a
where 	nvl(a.vl_inss,0) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	a.vl_imposto_munic,
	a.nr_seq_conta_banco,
	a.nr_seq_trans_fin,
	'VL_IMPOSTO_MUNIC',
	nr_bordero,
	nr_seq_escrit,
	cd_centro_custo,
	cd_conta_contabil,
	dt_baixa,
	nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	null nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa a
where 	nvl(a.vl_imposto_munic,0) <> 0
union all
select	a.nr_sequencia nr_seq_baixa,
	a.nr_lote_contabil,
	a.nr_titulo,
	a.vl_imposto vl_transacao,
	a.nr_seq_conta_banco,
	nr_seq_trans_fin,
	'VL_IMPOSTO_BAIXA',
	nr_bordero,
	nr_seq_escrit,
	cd_centro_custo,
	cd_conta_contabil,
	dt_baixa,
	nr_seq_movto_trans_fin,
	a.cd_tipo_baixa,
	null nr_seq_baixa_cc,
	a.cd_moeda,
	a.nr_seq_baixa_origem
from 	titulo_pagar_baixa a
where 	nvl(a.vl_imposto,0) <> 0;
/