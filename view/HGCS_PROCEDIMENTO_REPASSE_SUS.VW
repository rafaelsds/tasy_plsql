Create or Replace View HGCS_PROCEDIMENTO_REPASSE_SUS As
/* tr�s o valor do procedimento repassado ao terceiro segundo a tabela SUS, e a quantidade de auxiliares */
select	a.cd_pessoa_fisica,
	a.nr_sequencia nr_seq_terceiro,
	b.nr_repasse_terceiro,
	sum(c.vl_repasse) vl_proc_repasse,
	c.ie_status ie_status_proc_rep,
	d.cd_convenio,
	d.nr_interno_conta,
	d.nr_atendimento,
	d.nr_sequencia nr_seq_propaci,
	d.dt_procedimento,
	sum(c.vl_liberado) vl_liberado,
	(sum(c.vl_liberado) * 100) / sum(c.vl_repasse) pr_liberado,
	decode(	(select	count(*)
		from	procedimento_paciente x
		where	x.nr_sequencia		= d.nr_sequencia
		and	x.cd_medico_executor	= a.cd_pessoa_fisica), 0, 'N', 'S') ie_cirurgiao,
	decode(	(select	count(*)
		from	funcao_medico y,
			procedimento_participante x
		where	x.nr_sequencia		= d.nr_sequencia
		and	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
		and	x.ie_funcao		= y.cd_funcao
		and	y.ie_ind_equipe_sus	= 2), 0, 'N', 'S') ie_prim_auxiliar,
	decode(	(select	count(*)
		from	funcao_medico y,
			procedimento_participante x
		where	x.nr_sequencia		= d.nr_sequencia
		and	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
		and	x.ie_funcao		= y.cd_funcao
		and	y.ie_ind_equipe_sus	= 3), 0, 'N', 'S') ie_seg_auxiliar,
	max(e.vl_sp) vl_sus_proc,
	(select	count(*)
	from	funcao_medico y,
		procedimento_participante x
	where	x.nr_sequencia		= d.nr_sequencia
	and	x.ie_funcao		= y.cd_funcao
	and	y.ie_ind_equipe_sus	= 2) qt_prim_auxiliar,
	(select	count(*)
	from	funcao_medico y,
		procedimento_participante x
	where	x.nr_sequencia		= d.nr_sequencia
	and	x.ie_funcao		= y.cd_funcao
	and	y.ie_ind_equipe_sus	= 3) qt_seg_auxiliar
from	sus_preco e,
	procedimento_paciente d,
	procedimento_repasse c,
	repasse_terceiro b,
	terceiro a
where	a.nr_sequencia		= b.nr_seq_terceiro
and	b.nr_repasse_terceiro	= c.nr_repasse_terceiro
and	c.nr_seq_procedimento	= d.nr_sequencia
and	d.cd_procedimento		= e.cd_procedimento
and	d.ie_origem_proced	= e.ie_origem_proced
group by	a.cd_pessoa_fisica,
	a.nr_sequencia,
	b.nr_repasse_terceiro,
	c.ie_status,
	d.cd_convenio,
	d.nr_interno_conta,
	d.nr_atendimento,
	d.nr_sequencia,
	d.dt_procedimento;
/