create or replace
view titulo_receber_contrato_v as
select	b.nr_titulo,
	b.dt_emissao,
	b.dt_vencimento,
	b.dt_contabil,
	b.vl_titulo,
	b.vl_saldo_titulo,
	b.nr_documento,
	b.dt_liquidacao,
	b.ie_situacao,
	b.nr_seq_contrato
from	titulo_receber b
where	b.nr_seq_contrato	is not null
union
select	b.nr_titulo,
	b.dt_emissao,
	b.dt_vencimento,
	b.dt_contabil,
	b.vl_titulo,
	b.vl_saldo_titulo,
	b.nr_documento,
	b.dt_liquidacao,
	b.ie_situacao,
	a.nr_seq_contrato
from	titulo_receber b,
	titulo_receber_classif a
where	b.nr_titulo	= a.nr_titulo
and	a.nr_seq_contrato	is not null;
/