create or replace
view adep_gestao_solucao_v
as
select	a.nr_atendimento,
	a.cd_pessoa_fisica,
	p.nm_pessoa_fisica,	
	obter_setor_atendimento(a.nr_atendimento) cd_setor_atendimento,
	substr(obter_nome_setor(obter_setor_atendimento(a.nr_atendimento)),1,100) ds_setor_atendimento,
	a.nr_prescricao,
	a.cd_prescritor,
	t.nm_pessoa_fisica nm_prescritor,
	a.dt_prescricao,
	a.dt_inicio_prescr,
	a.dt_validade_prescr,
	a.dt_liberacao,
	c.ie_tipo_solucao ie_tipo_solucao,
	b.nr_seq_solucao,
	b.ie_acm ie_acm,
	b.ie_se_necessario ie_se_necessario,
	substr(nvl(b.ds_solucao,obter_prim_comp_sol(b.nr_prescricao,b.nr_seq_solucao)),1,240) ds_solucao,
	substr(obter_componentes_solucao(b.nr_prescricao,b.nr_seq_solucao,'N', null, null),1,2000) ds_componentes,
	'IV' ie_via,
	b.ie_bomba_infusao ie_bi,
	b.ie_tipo_dosagem ie_unidade_veloc,
	b.qt_tempo_aplicacao qt_tempo_solucao,
	b.nr_etapas_suspensa,
	obter_etapas_adep_sol(1,b.nr_prescricao,b.nr_seq_solucao) nr_etapas_adep,
	b.nr_etapas nr_etapas,
	b.qt_volume qt_volume_etapa,
	b.qt_hora_fase qt_tempo_etapa,
	b.qt_solucao_total qt_volume_original,
	b.qt_dosagem qt_velocidade_original,
	nvl(b.qt_volume_suspenso,0) qt_volume_suspenso,
	obter_dados_solucao(1,b.nr_prescricao,b.nr_seq_solucao,'VI') qt_volume_infundido,
	obter_dados_solucao(1,b.nr_prescricao,b.nr_seq_solucao,'VD') qt_volume_desprezado,
	obter_dados_solucao(1,b.nr_prescricao,b.nr_seq_solucao,'VR') qt_volume_pendente,
	nvl(obter_dados_solucao(1,b.nr_prescricao,b.nr_seq_solucao,'VA'),b.qt_dosagem) qt_velocidade_atual,
	b.ie_status,
	obter_desc_radio_group(597225, b.ie_status) ds_status,
	b.dt_status,
	obter_data_prev_term_sol(b.nr_prescricao,b.nr_seq_solucao,1) dt_prev_termino,
	c.nr_sequencia nr_seq_evento,
	c.ie_alteracao ie_evento,
	c.dt_alteracao dt_evento,
	c.cd_pessoa_fisica cd_pessoa_evento,
	substr(obter_nome_pf(c.cd_pessoa_fisica),1,60) nm_pessoa_evento,
	c.ie_evento_valido
from	pessoa_fisica t,
	pessoa_fisica p,
	prescr_solucao_evento c,
	prescr_solucao b,
	prescr_medica a
where	b.nr_seq_solucao = c.nr_seq_solucao(+)
and	b.nr_prescricao = c.nr_prescricao(+)
and	b.nr_prescricao = a.nr_prescricao
and	a.cd_pessoa_fisica = p.cd_pessoa_fisica 
and	a.cd_prescritor	= t.cd_pessoa_fisica
and	a.nr_atendimento is not null;
/