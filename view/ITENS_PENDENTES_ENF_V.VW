create or replace
view itens_pendentes_enf_v
as
SELECT   'M' ie_tipo_item,
	 substr(Obter_Desc_Material(a.cd_material),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_material a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND	 a.ie_agrupador = 1
AND      nvl(a.ie_suspenso,'N') <> 'S'
UNION
SELECT   'S' ie_tipo_item,
	 substr(b.ds_solucao,1,255) ds,
	 substr(b.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(c.cd_prescritor),1,80) nm_prescritor
FROM     prescr_material a,
	 prescr_solucao b,
         prescr_medica c
WHERE    a.nr_prescricao = c.nr_prescricao
AND      b.nr_prescricao = c.nr_prescricao
AND      a.nr_prescricao = b.nr_prescricao
AND	 a.nr_sequencia_solucao = b.nr_seq_solucao
AND      nvl(a.ie_suspenso,'N') <> 'S'
UNION
SELECT   'P' ie_tipo_item,
	 substr(obter_desc_proced_prescr(a.nr_prescricao, a.nr_sequencia),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_procedimento a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND      nvl(a.ie_suspenso,'N') <> 'S'
AND	 a.nr_seq_solic_sangue is null
AND	 a.nr_seq_derivado is null
AND	 a.nr_seq_exame is null
and      ((a.nr_seq_prot_glic is null) or
          (exists (select 1 from pep_protocolo_glicemia c where c.nr_sequencia = a.nr_seq_prot_glic and c.ie_tipo = 'I')))
UNION
SELECT   'L' ie_tipo_item,
	 substr(obter_desc_exame_lab(a.nr_seq_exame, null, a.cd_procedimento, a.ie_origem_proced),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_procedimento a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND      nvl(a.ie_suspenso,'N') <> 'S'
AND	 a.nr_seq_solic_sangue is null
AND	 a.nr_seq_derivado is null
AND	 a.nr_seq_exame is not null
UNION
SELECT   'R' ie_tipo_item,
	 substr(Obter_recomendacao_medic(a.nr_sequencia,a.nr_prescricao),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_recomendacao a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND      nvl(a.ie_suspenso,'N') <> 'S'
UNION
SELECT   'SNE' ie_tipo_item,
	 substr(Obter_Desc_Material(a.cd_material),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_material a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND	 a.ie_agrupador = 8
AND      nvl(a.ie_suspenso,'N') <> 'S'
UNION
SELECT   'SUPL' ie_tipo_item,
	 substr(Obter_Desc_Material(a.cd_material),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_material a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND	 a.ie_agrupador = 12
AND      nvl(a.ie_suspenso,'N') <> 'S'
UNION
SELECT   'HM' ie_tipo_item,
	 substr(obter_desc_proced_prescr(a.nr_prescricao, a.nr_sequencia),1,255) ds,
	 substr(a.ds_horarios,1,255) ds_horarios,
         b.nr_prescricao,
         substr(obter_nome_pf(b.cd_prescritor),1,80) nm_prescritor
FROM     prescr_procedimento a,
         prescr_medica b
WHERE    a.nr_prescricao = b.nr_prescricao
AND      nvl(a.ie_suspenso,'N') <> 'S'
AND	 a.nr_seq_solic_sangue is not null
AND	 a.nr_seq_derivado is not null
AND	 a.nr_seq_exame is null
and      ((a.nr_seq_prot_glic is null) or
          (exists (select 1 from pep_protocolo_glicemia c where c.nr_sequencia = a.nr_seq_prot_glic and c.ie_tipo = 'I')))
ORDER BY ie_tipo_item
/
