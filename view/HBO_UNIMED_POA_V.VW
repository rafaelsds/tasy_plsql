CREATE OR REPLACE VIEW
HBO_UNIMED_POA_V AS
select	/* Header */
	1 				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'00101146' 			cd_prestador,
	c.nr_seq_protocolo 		nr_seq_lote,
	0				nr_interno_conta,
	0				cd_serie_documento,
	sysdate 			dt_geracao,
	' ' 				nr_documento,
	'0' 				cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0 				cd_motivo_alta,
	' ' 				cd_cid,
	sysdate 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	
					dt_ano_guia,
	0 				nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao,
	' ' 				nm_beneficiario,
	' ' 				ie_sexo,
	sysdate 			dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	0				cd_procedimento,
	0 				qt_procedimento_autor,
	sysdate 			dt_procedimento,
	' ' 				ie_urgencia,
	' '	 			ie_adicional_urgencia,
	0 				qt_procedimento,
	0 				vl_procedimento,
	' ' 				ie_tipo_nascimento,
	0 				nr_seq_autorizacao,
	0		 		ie_tipo_percentual,
	0 				ie_tipo_insumo,
	0 				cd_insumo,
	0		 		qt_insumo,
	sysdate 			dt_insumo,
	0 				qt_cobrada,
	0	 			vl_cobrado,
	0 				cd_pacote,
	sysdate 			dt_emissao,
	0	 			qt_reg_nota,
 	0 				qt_reg_doc,
 	0 				qt_reg_proc,
	0 				qt_reg_insumo,
	0 				vl_total_lote,
	0				nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	' '				nr_guia_prestador
from	w_interf_conta_header c
union
select	/* Nota */
	2	 			tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'00101146'		 	cd_prestador,
	0 				nr_seq_lote,
	0				nr_interno_conta,
	0				cd_serie_documento,
	sysdate	 			dt_geracao,
	' ' 				nr_documento,
	'0'	 			cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0	 			cd_motivo_alta,
	' ' 				cd_cid,
	sysdate	 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	dt_ano_guia,
	0		 		nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao,
	' '	 			nm_beneficiario,
	' '	 			ie_sexo,
	sysdate 			dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	0				cd_procedimento,
	0 				qt_procedimento_autor,
	sysdate 			dt_procedimento,
	' '	 			ie_urgencia,
	' ' 				ie_adicional_urgencia,
	0	 			qt_procedimento,
	0		 		vl_procedimento,
	' ' 				ie_tipo_nascimento,
	0 				nr_seq_autorizacao,
	0		 		ie_tipo_percentual,
	0 				ie_tipo_insumo,
	0 				cd_insumo,
	0	 			qt_insumo,
	sysdate 			dt_insumo,
	0	 			qt_cobrada,
	0		 		vl_cobrado,
	0 				cd_pacote,
	sysdate 			dt_emissao,
 	0		 		qt_reg_nota,
 	0 				qt_reg_doc,
 	0 				qt_reg_proc,
	0	 			qt_reg_insumo,
	0 				vl_total_lote,
	0				nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	' '				nr_guia_prestador
from	w_interf_conta_header c
union
select	/* Documento  */
	3	 			tp_registro,
	b.nr_seq_protocolo		nr_seq_protocolo,
	decode(b.ie_tipo_atendimento, 3, substr(a.cd_interno,1,8), 
		somente_numero(substr(hdjb_obter_regra_prestador(b.ie_tipo_atendimento,d.ie_tipo_guia, b.nr_seq_protocolo, b.cd_medico_resp),1,8))) cd_prestador,
	0 				nr_seq_lote,
	b.nr_interno_conta		nr_interno_conta,
	1				cd_serie_documento,
	sysdate 			dt_geracao,
	substr(b.nr_doc_convenio,1,8)	nr_documento,
	substr(b.cd_usuario_convenio,1,4) cd_unid_carteira,
	substr(b.cd_usuario_convenio,5,13) nr_carteira,
	sysdate				dt_referencia,
	b.cd_motivo_alta		cd_motivo_alta,
	substr(b.cd_cid_principal,1,4)	cd_cid,
	b.dt_entrada			dt_entrada,
	b.dt_alta			dt_alta,
	decode(d.ie_tipo_guia,'E',null,'C',null,'A', null,to_char(trunc(b.dt_periodo_inicial,'year'),'yyyy')) 
					dt_ano_guia,
	decode(d.ie_tipo_guia,'E',00000000,'A',00000000,somente_numero(substr(b.nr_doc_convenio,1,8))) 
					nr_guia,
	decode(b.ie_tipo_atendimento,7,'02',
		decode(d.ie_tipo_guia,'IC','04','I','05','IO','06','AI','03','08')) cd_classe_nota,
	b.cd_tipo_acomodacao		cd_acomodacao,	
	substr(b.nm_paciente,1,40)	nm_beneficiario,
	b.ie_sexo			ie_sexo,
	b.dt_nascimento			dt_nascimento,
	b.dt_validade_carteira		 dt_validade_carteira,
	nvl(b.cd_setor_atendimento,0) 	cd_setor_atendimento,
	0				cd_procedimento,
	0 				qt_procedimento_autor,
	sysdate 			dt_procedimento,
	' '	 			ie_urgencia,
	' ' 				ie_adicional_urgencia,
	0 				qt_procedimento,
	0 				vl_procedimento,
	' '	 			ie_tipo_nascimento,
	0				nr_seq_autorizacao,
	0 				ie_tipo_percentual,
	0 				ie_tipo_insumo,
	0 				cd_insumo,
	0 				qt_insumo,
	sysdate 			dt_insumo,
	0 				qt_cobrada,
	0	 			vl_cobrado,
	0 				cd_pacote,
	sysdate 			dt_referencia,
 	0 				qt_reg_nota,
 	0 				qt_reg_doc,
 	0 				qt_reg_proc,
	0 				qt_reg_insumo,
	0 				vl_total_lote,
	0				nr_seq_pacote,	
	somente_numero(substr(obter_dados_protocolo(a.nr_seq_protocolo,'DC'),1,8)) nr_lote,
	(SELECT sum(qt_nasc_vivos) FROM parto WHERE NR_ATENDIMENTO = b.NR_ATENDIMENTO) qt_nasc_vivos_termo,
	(SELECT sum(qt_nasc_mortos) FROM parto WHERE NR_ATENDIMENTO = b.NR_ATENDIMENTO) qt_nasc_mortos,
	decode(b.ie_tipo_nascimento,3,1,0) qt_nasc_vivos_premat,
	decode(b.ie_tipo_nascimento,8,1,0) qt_obito_nasc_precoc,
	decode(b.ie_tipo_nascimento,7,1,0) qt_obito_nasc_tardio,
	b.cd_motivo_alta 		ie_tipo_saida,
	substr(hdjb_obter_dados_tiss_diag(b.nr_atendimento, '1'),1,1) ie_tipo_doenca,
	somente_numero(substr(hdjb_obter_dados_tiss_diag(b.nr_atendimento, '3'),1,2)) qt_tempo_doenca,
	substr(hdjb_obter_dados_tiss_diag(b.nr_atendimento,'2'),1,1) ie_tipo_tempo_doenca,
	0 				ie_acidente,
	DECODE(b.cd_cid_secundario,null,substr(b.cd_cid_principal,1,4), substr(b.cd_cid_secundario,1,4))
					cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	substr(d.nm_medico_solicitante,1,70) 
					nm_prof_solicitante,
	decode(b.ie_tipo_atendimento, 7, '05', b.ie_tipo_atendimento) ie_tipo_atendimento,
	D.IE_TIPO_GUIA			ie_tipo_consulta,
	' '				ds_observacoes,
	decode(b.ie_tipo_nascimento,1,'S','N') ie_gestacao,
	decode(b.ie_tipo_nascimento,2,'S','N') ie_aborto,
	decode(b.ie_tipo_nascimento,6,'S','N') ie_trans_gravidez,
	'N'				ie_compl_puerperio, 	
	'N'				ie_sala_parto,
	'N'				ie_compl_neonat,
	(SELECT max('S') FROM nascimento WHERE NR_ATENDIMENTO = b.NR_ATENDIMENTO and qt_peso < 2.5) 
					ie_baixo_peso,
	(SELECT decode(ie_parto_cesaria,null,'N',ie_parto_cesaria) FROM parto WHERE NR_ATENDIMENTO = b.NR_ATENDIMENTO)	
					ie_parto_cesareo,
	(SELECT decode(ie_parto_normal,null,'N',ie_parto_normal) FROM parto WHERE NR_ATENDIMENTO = b.NR_ATENDIMENTO)
					ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	'T'				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	to_char(b.nr_atendimento)	nr_guia_prestador
FROM	w_interf_conta_header 	a,
	w_interf_conta_autor 	d,
	W_INTERF_CONTA_CAB 	b
where	b.nr_seq_protocolo	= a.nr_seq_protocolo
and	b.nr_interno_conta	= d.nr_interno_conta(+)
and	nvl(d.nr_sequencia,0) =
	(select nvl(min(y.nr_sequencia),0)
		from w_interf_conta_autor y
		where b.nr_interno_conta = y.nr_interno_conta)
union
select	/*Procedimentos*/
	4	 			tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'00101146'			cd_prestador,
	0 				nr_seq_lote,
	d.nr_interno_conta		nr_interno_conta,
	0				cd_serie_documento,
	sysdate 			dt_geracao,
	' ' 				nr_documento,
	'0' 				cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0 				cd_motivo_alta,
	' ' 				cd_cid,
	sysdate 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	
					dt_ano_guia,
	0 				nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao, 
	' ' 				nm_beneficiario,
	' '	 			ie_sexo,
	sysdate 			Dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	somente_numero(nvl(d.cd_item_convenio, d.cd_item)) 
					cd_procedimento,
	d.qt_item 			qt_procedimento_autor,
	d.dt_item			dt_procedimento,
	decode(c.ie_carater_inter,'20','S','5','S','N')	
					ie_urgencia,
	'N' 				ie_adicional_urgencia,
	d.qt_item			qt_procedimento,
	d.vl_total_item			vl_procedimento,
	c.ie_tipo_nascimento		ie_tipo_nascimento,
	somente_numero(substr(d.cd_senha_guia,1,8))	
					nr_seq_autorizacao,
	decode(d.pr_funcao_participante,50,1,20,2,40,4,100,05,00) 
					ie_tipo_percentual,
	0 				ie_tipo_insumo,
	0 				cd_insumo,
	0 				qt_insumo,
	sysdate 			dt_insumo,
	0 				qt_cobrada,
	0 				vl_cobrado,
	0 				cd_pacote,
	sysdate 			dt_emissao,
 	0 				qt_reg_nota,			
 	0 				qt_reg_doc,
 	0	 			qt_reg_proc,
	0 				qt_reg_insumo,
	0 				vl_total_lote,
	0				nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	substr(obter_dados_pf_pj(Null, CD_CGC_PRESTADOR, 'N'),1,30) 
					nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	substr(d.nm_medico_executor,1,70) nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	to_char(c.nr_atendimento)	nr_guia_prestador
from	w_interf_conta_item d,
	w_interf_conta_cab c	
where   c.nr_interno_conta = d.nr_interno_conta
and	d.nr_seq_proc_pacote is null
and	nvl(d.cd_grupo_gasto,0) not in (10,11,12,20)
and 	d.ie_tipo_item = 1
union 
select	/* Insumos  (Taxas e diarias)  */
	5	 			tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'00101146'			cd_prestador,
	0 				nr_seq_lote,
	d.nr_interno_conta		nr_interno_conta,
	0				cd_serie_documento,
	sysdate 			dt_geracao,
	' ' 				nr_documento,
	'0' 				cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0 				cd_motivo_alta,
	' ' 				cd_cid,
	sysdate 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	dt_ano_guia,
	0 				nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao, 
	' ' 				nm_beneficiario,
	' '	 			ie_sexo,
	sysdate 			Dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	0				cd_procedimento,
	0		 		qt_procedimento_autor,
	sysdate				dt_procedimento,
	' '				ie_urgencia,
	' ' 				ie_adicional_urgencia,
	0				qt_procedimento,
	0 				vl_procedimento,
	' '				ie_tipo_nascimento,
	0				nr_seq_autorizacao,
	0				ie_tipo_percentual,
	decode(d.cd_item,99901013,99,99901315,99,to_number(d.cd_grupo_gasto))	ie_tipo_insumo,
	somente_numero(nvl(d.cd_item_convenio,d.cd_item))	
					cd_insumo,
	d.qt_item			qt_insumo,
	d.dt_item			dt_insumo,
	d.qt_item			qt_cobrada,
	d.vl_total_item			vl_cobrado,
	0 				cd_pacote,
	sysdate 			dt_emissao,
 	0 				qt_reg_nota,			
 	0 				qt_reg_doc,
 	0	 			qt_reg_proc,
	0 				qt_reg_insumo,
	0 				vl_total_lote,
	0				nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	to_char(c.nr_atendimento)	nr_guia_prestador
from	w_interf_conta_item 	d,
	w_interf_conta_cab 	c	
where   c.nr_interno_conta = d.nr_interno_conta
and	d.nr_seq_proc_pacote is null
and	nvl(d.cd_grupo_gasto,0) in (10,11,12,20)
and 	d.ie_tipo_item = 1
union
select	/* Insumos (Materiais e Medicamentos) */
	5				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'00101146'			cd_prestador,
	0 				nr_seq_lote,
	d.nr_interno_conta		nr_interno_conta,
	0				cd_serie_documento,
	sysdate	 			dt_geracao,
	' ' 				nr_documento,
	'0' 				cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0 				cd_motivo_alta,
	' ' 				cd_cid,
	sysdate	 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	dt_ano_guia,
	0 				nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao, 	
	' ' 				nm_beneficiario,
	' ' 				ie_sexo,
	sysdate 			dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	0				cd_procedimento,
	0 				qt_procedimento_autor,
	sysdate 			dt_procedimento,
	' ' 				ie_urgencia,
	' ' 				ie_adicional_urgencia,
	0 				qt_procedimento,
	0 				vl_procedimento,
	' ' 				ie_tipo_nascimento,
	0 				nr_seq_autorizacao,
	0 				ie_tipo_percentual,
	to_number(d.cd_grupo_gasto)	ie_tipo_insumo,
	somente_numero(nvl(d.cd_item_convenio,d.cd_item))
					cd_insumo,
	sum(d.qt_item) 			qt_insumo,
	d.dt_item			dt_insumo,
	sum(d.qt_item) 			qt_cobrada,
	sum(d.vl_total_item) 		vl_cobrado,
	0	 			cd_pacote,
	sysdate 			dt_emissao,
 	0 				qt_reg_nota,	
 	0 				qt_reg_doc,
 	0 				qt_reg_proc,
	0 				qt_reg_insumo,
	0 				vl_total_lote,
	0				nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	' '				nr_guia_prestador
from	w_interf_conta_item 	d,
	w_interf_conta_cab 	c	
where   c.nr_interno_conta = d.nr_interno_conta
and	d.nr_seq_proc_pacote is null
and 	d.ie_tipo_item = 2
group by 5,
	c.nr_seq_protocolo,
	'00101146',
	d.nr_interno_conta,
	to_char(trunc(sysdate,'year'),'yyyy'),
	d.dt_item,
	to_number(d.cd_grupo_gasto),
	somente_numero(nvl(d.cd_item_convenio,d.cd_item))
having	sum(d.qt_item) > 0
union
select	/* Pacote */
	6 				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'00101146'			cd_prestador,
	0 				nr_seq_lote,
	d.nr_interno_conta		nr_interno_conta,
	0				cd_serie_documento,
	sysdate 			dt_geracao,
	' ' 				nr_documento,
	'0' 				cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0 				cd_motivo_alta,
	' ' 				cd_cid,
	sysdate 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	
					dt_ano_guia,
	0 				nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao, 	
	' ' 				nm_beneficiario,
	' ' 				ie_sexo,
	sysdate 			dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	0				cd_procedimento,
	0 				qt_procedimento_autor,
	sysdate 			dt_procedimento,
	' ' 				ie_urgencia,
	' ' 				ie_adicional_urgencia,
	0 				qt_procedimento,
	0 				vl_procedimento,
	' ' 				ie_tipo_nascimento,
	0 				nr_seq_autorizacao,
	0 				ie_tipo_percentual,	
	0 				ie_tipo_insumo,	
	0 				cd_insumo,
	0 				qt_insumo,
	sysdate 			dt_insumo,
	0 				qt_cobrada,
	0 				vl_cobrado,
	somente_numero(nvl(d.cd_item_convenio,d.cd_item))
					cd_pacote,
	d.dt_item			dt_emissao,
 	0 				qt_reg_nota,	
 	0 				qt_reg_doc,
 	0 				qt_reg_proc,
	0 				qt_reg_insumo,
	0 				vl_total_lote,
	d.nr_seq_proc_pacote		nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	d.qt_item			qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	' '				nr_guia_prestador
from	w_interf_conta_item 	d,
	w_interf_conta_cab 	c	
where   c.nr_interno_conta = d.nr_interno_conta
and	d.nr_seq_proc_pacote is not null
union
select	/* Trailler */
	7 				tp_registro,
	c.nr_seq_protocolo		nr_seq_protocolo,
	'0'	 			cd_prestador,
	0 				nr_seq_lote,
	9999999999				nr_interno_conta,
	0				cd_serie_documento,
	sysdate 			dt_geracao,
	' ' 				nr_documento,
	'0' 				cd_unid_carteira,
	' ' 				nr_carteira,
	sysdate 			dt_referencia,
	0 				cd_motivo_alta,
	' ' 				cd_cid,
	sysdate 			dt_entrada,
	sysdate 			dt_alta,
	to_char(trunc(sysdate,'year'),'yyyy')	
					dt_ano_guia,
	0 				nr_guia,
	'0' 				cd_classe_nota,
	0 				cd_acomodacao, 
	' ' 				nm_beneficiario,
	' ' 				ie_sexo,
	sysdate 			dt_nascimento,
	sysdate				dt_validade_carteira,
	0 				cd_setor_atendimento,
	0				cd_procedimento,
	0 				qt_procedimento_autor,
	sysdate 			dt_procedimento,
	' ' 				ie_urgencia,
	' ' 				ie_adicional_urgencia,
	0 				qt_procedimento,
	0 				vl_procedimento,
	' ' 				ie_tipo_nascimento,
	0 				nr_seq_autorizacao,
	0 				ie_tipo_percentual,
	0 				ie_tipo_insumo,	
	0 				cd_insumo,
	0 				qt_insumo,
	sysdate 			dt_insumo,
	0 				qt_cobrada,
	0 				vl_cobrado,
	0 				cd_pacote,
	sysdate 			dt_emissao,
 	2 				qt_reg_nota,
 	3 				qt_reg_doc,
 	4 				qt_reg_proc,
	5 				qt_reg_insumo,
	hbo_obter_total_protocolo(c.nr_seq_protocolo) vl_total_lote,
	0				nr_seq_pacote,
	0				nr_lote,
	0 				qt_nasc_vivos_termo,
	0				 qt_nasc_mortos,
	0 				qt_nasc_vivos_premat,
	0 				qt_obito_nasc_precoc,
	0 				qt_obito_nasc_tardio,
	0		 		ie_tipo_saida,
	' '				ie_tipo_doenca,
	0				qt_tempo_doenca,
	' '				ie_tipo_tempo_doenca,
	0 				ie_acidente,
	' ' 				cd_cid1,
	' ' 				cd_cid2,
	' ' 				cd_cid3,
	' '				nm_prof_solicitante,
	'00'				ie_tipo_atendimento,
	' '				ie_tipo_consulta,
	' '				ds_observacoes,
	' '				ie_gestacao,
	' '  				ie_aborto,
	' '				ie_trans_gravidez,
	' '				ie_compl_puerperio, 	
	' '				ie_sala_parto,
	' '				ie_compl_neonat,
	' '				ie_baixo_peso,
	' '				ie_parto_cesareo,
	' ' 				ie_parto_normal,
	' ' 				ie_obito_mulher,
	' '				ie_nasc_vivos,
	' '				ie_cid_obito,
	0				nr_decl_obito,
	' '				ie_tipo_faturamento,
	'000'				cd_cobertura,
	' '				nm_prest_exec,
	'00'				cd_tab_medica,
	sysdate				hr_final,
	' ' 				nm_prof_exec,
	' '				ie_tec_utilizada,
	' ' 				ie_tipo_perc,
	0				qt_pacote,
	0				nr_linha_ptu,
	' '				ie_nasc_vivos2,
	' '				ie_nasc_vivos3,
	' '				ie_nasc_vivos4,
	' '				ie_nasc_vivos5,
	0				ie_regime_internacao,
	' '				ds_indic_clinica,
	' '				nr_guia_prestador
from	w_interf_conta_trailler c
group by c.nr_seq_protocolo;
/