create or replace view tws_mensalidade_benef_v as
select	a.nr_sequencia nr_seq_mensalidade,
	e.nr_titulo,
	a.dt_referencia,
	b.dt_mesano_referencia,
	b.vl_mensalidade,
	b.vl_coparticipacao,
	e.dt_vencimento,
	e.dt_pagamento_previsto,
	c.nr_sequencia nr_seq_segurado,
	c.nr_seq_titular,
	d.dt_mesano_referencia dt_referencia_lote,
	e.ie_situacao,
	a.nr_seq_pagador,
	a.ds_mensagem_quitacao,
	a.ds_observacao,
	a.vl_mensalidade vl_mensalidade_total,
	a.vl_coparticipacao vl_coparticipacao_total,
	nf.ds_link_rps ds_link_nf,
	nf.nr_nota_fiscal,
	nf.nr_sequencia nr_seq_nota_fiscal
from	pls_mensalidade		a,
	pls_mensalidade_segurado b,
	pls_segurado		c,
	pls_lote_mensalidade	d,
	titulo_receber		e,
	nota_fiscal		nf
where	b.nr_seq_mensalidade	= a.nr_sequencia
and	b.nr_seq_segurado	= c.nr_sequencia
and	d.nr_sequencia		= a.nr_seq_lote
and	e.nr_seq_mensalidade	= a.nr_sequencia
and	a.nr_sequencia		= nf.nr_seq_mensalidade(+)
and	((d.ie_visualizar_portal = 'S') or (d.ie_visualizar_portal is null))
and	e.ie_situacao in ('1','2','4','6')
and	a.ie_cancelamento is null;
/