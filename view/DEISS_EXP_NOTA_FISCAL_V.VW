create or replace
view DEISS_EXP_NOTA_FISCAL_V as
select	1 					tp_registro,
	'H'					ds_registro,
	substr(somente_numero(a.cd_cgc),1,14)	cd_cnpj_declarante,
	substr(somente_numero(a.cd_inscricao_municipal),1,8) nr_inscricao_municipal,
	a.cd_estabelecimento			cd_estabelecimento,
	'0'					nr_nota_fiscal,
	to_date(null)				dt_emissao,
	to_date(null)				dt_emissao_nota,
	0					cd_operacao_nf,
	''					cd_serie_nf,
	''					ie_receb_emit_nf,
	''					cd_cpf_cnpj_tomador_prestador,
	0					vl_mercadoria,
	''					ie_tipo_recolhimento,
	''					nm_tomador_prestador,
	''					ds_municipio_tomador_prestador,
	''					sg_estado_tomador_prestador,
	0					nr_sequencia,
	0					tx_imposto,
	0 					vl_imposto
from	estabelecimento_v a
where	1=1
union
select	2					tp_registro,
	'N'					ds_registro,
	''					cd_cnpj_declarante,
	'' 					nr_inscricao_municipal,
	n.cd_estabelecimento			cd_estabelecimento,
	n.nr_nota_fiscal				nr_nota_fiscal,
	n.dt_emissao				dt_emissao,
	n.dt_emissao				dt_emissao_nota,
	n.cd_operacao_nf				cd_operacao_nf,
	substr(n.cd_serie_nf,1,5) 			cd_serie_nf,
	decode(substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1), 'S', 'E', 'R') ie_receb_emit_nf,
	lpad(decode(n.cd_cgc, null, substr(obter_dados_pf(n.cd_pessoa_fisica, 'CPF'),1,11), n.cd_cgc),14,'0') cd_cpf_cnpj_tomador_prestador,
	n.vl_mercadoria				vl_mercadoria,
	'P'					ie_tipo_recolhimento,
	replace(substr(obter_nome_pf_pj(n.cd_pessoa_fisica, n.cd_cgc),1,85), chr(39), ' ') nm_tomador_prestador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica,n.cd_cgc,'CI'),1,30) ds_municipio_tomador_prestador,
	substr(obter_dados_pf_pj(n.cd_pessoa_fisica,n.cd_cgc,'UF'),1,2) sg_estado_tomador_prestador,
	n.nr_sequencia,
	0					tx_imposto,
	0 					vl_imposto
from	nota_fiscal n
where	n.ie_situacao in ('1')
and	substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'S'
union
select	3					tp_registro,
	'I'					ds_registro,
	''					cd_cnpj_declarante,
	'' 					nr_inscricao_municipal,
	n.cd_estabelecimento			cd_estabelecimento,
	n.nr_nota_fiscal				nr_nota_fiscal,
	n.dt_emissao				dt_emissao,
	n.dt_emissao				dt_emissao_nota,
	n.cd_operacao_nf				cd_operacao_nf,
	''			 		cd_serie_nf,
	decode(substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1), 'S', 'E', 'R') ie_receb_emit_nf,
	''					cd_cpf_cnpj_tomador_prestador,
	n.vl_mercadoria				vl_mercadoria,
	''					ie_tipo_recolhimento,
	''					nm_tomador_prestador,
	''					ds_municipio_tomador_prestador,
	''					sg_estado_tomador_prestador,
	n.nr_sequencia,
	nvl(Obter_Valor_tipo_Tributo_Nota(n.nr_sequencia, 'X', 'ISS'),0) tx_imposto,
	nvl(Obter_Valor_tipo_Tributo_Nota(n.nr_sequencia, 'V', 'ISS'),0) vl_imposto
from	nota_fiscal n
where	n.ie_situacao in ('1')
and	substr(obter_se_nota_entrada_saida(n.nr_sequencia),1,1) = 'S'
union
select	9 					tp_registro,
	'T'					ds_registro,
	''					cd_cnpj_declarante,
	''					nr_inscricao_municipal,
	a.cd_estabelecimento			cd_estabelecimento,
	'9999999999999999'			nr_nota_fiscal,
	to_date(null)				dt_emissao,
	to_date(null)				dt_emissao_nota,
	0					cd_operacao_nf,
	''					cd_serie_nf,
	''					ie_receb_emit_nf,
	''					cd_cpf_cnpj_tomador_prestador,
	0					vl_mercadoria,
	''					ie_tipo_recolhimento,
	''					nm_tomador_prestador,
	''					ds_municipio_tomador_prestador,
	''					sg_estado_tomador_prestador,
	9999999999999999			nr_sequencia,
	0					tx_imposto,
	0 					vl_imposto
from	estabelecimento_v a
where	1=1;
/