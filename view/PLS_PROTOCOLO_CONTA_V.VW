create or replace view pls_protocolo_conta_v as
select	a.nr_sequencia,
	pls_obter_seq_codigo_coop(a.nr_seq_congenere, null) cd_congenere,
	a.nr_seq_congenere,
	a.dt_protocolo,
	a.dt_mes_competencia,
	a.dt_lib_pagamento,
	a.nr_seq_prot_referencia,
	a.nr_seq_periodo_pgto,
	a.nr_seq_prestador,
	a.ie_tipo_protocolo,
	a.ie_status,
	a.nr_seq_lote_conta,
	a.dt_integracao,
	a.ie_tipo_importacao,
	a.ie_origem_protocolo,
	a.dt_recebimento,
	a.cd_versao_tiss,
	nvl(a.ie_guia_fisica,'N') ie_guia_fisica,
	a.nr_protocolo_prestador,
	nvl(a.qt_contas_informadas, 0) qt_contas_informadas,
	nvl(a.vl_cobrado_manual, 0) vl_cobrado_manual,
	a.ie_situacao,
	a.cd_estabelecimento,
	nvl(a.ie_apresentacao,'A') ie_apresentacao,
	a.nr_seq_prestador_imp_ref nr_seq_prestador_imp ,
	trunc(a.dt_mes_competencia,'month') dt_mes_competencia_trunc,
	(select	max(b.dt_geracao_analise)
	 from	pls_lote_protocolo_conta	b
	 where b.nr_sequencia = a.nr_seq_lote_conta) dt_geracao_analise_lote,
	(select	max(b.ie_status)
	 from	pls_lote_protocolo_conta	b
	 where b.nr_sequencia = a.nr_seq_lote_conta) ie_status_lote,
	 pls_obter_local_prest_prot(a.nr_seq_prestador, 'uf') sg_estado,
	 pls_obter_local_prest_prot(a.nr_seq_prestador, 'cidade_ibge') cd_municipio_ibge,
	ie_tipo_guia,
	dt_protocolo_imp,
	a.nm_usuario_nrec,
	a.dt_atualizacao_nrec,
	a.nr_seq_segurado nr_seq_segurado_prot,
	a.nr_seq_mot_reembolso,
	a.ie_tipo_atendimento ie_tipo_atendimento_reemb
from	pls_protocolo_conta		a;
/