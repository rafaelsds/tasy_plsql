create or replace
view conta_paciente_desconto_v3
as
select	sum(b.vl_desconto) vl_desconto,
	'22' ie_emite_conta,
	substr(obter_desc_estrut_conta(b.cd_estrutura),1,200) ds_desconto,
	a.nr_interno_conta
from	conta_paciente_desconto a,
	conta_paciente_desc_item b
where	a.nr_sequencia = b.nr_seq_desconto
group by substr(obter_desc_estrut_conta(b.cd_estrutura),1,200),
	 a.nr_interno_conta;
/