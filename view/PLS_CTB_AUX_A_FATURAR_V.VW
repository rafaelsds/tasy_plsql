create or replace view pls_ctb_aux_a_faturar_v as
select	substr(pls_obter_dados_segurado(c.nr_seq_segurado,'CR'),1,30) cd_beneficiario,
	substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_beneficiario,
	obter_valor_dominio(1669, d.ie_preco) ds_preco,--modalidade contratação
	substr(obter_valor_dominio(2157, d.ie_regulamentacao),1,255) ds_regulamentacao,
	substr(obter_valor_dominio(1666, d.ie_tipo_contratacao),1,255) ds_tipo_contratacao,
	substr(obter_valor_dominio(1665, d.ie_segmentacao),1,255) ds_segmentacao,
	substr(obter_valor_dominio(3418,(	select	b.ie_ato_cooperado
						from	pls_conta_proc b
						where	e.nr_seq_conta_proc = b.nr_sequencia
						and	b.nr_seq_conta = c.nr_sequencia
						union all
						select	b.ie_ato_cooperado
						from	pls_conta_mat b
						where	e.nr_seq_conta_mat = b.nr_sequencia
						and	b.nr_seq_conta = c.nr_sequencia)),1,255) ds_ato_cooperado,
	b.vl_provisao vl_provisao,
	nvl(b.cd_conta_deb_provisao, b.cd_conta_deb_ndc) cd_conta_deb_provisao,
	nvl(b.cd_conta_cred_provisao, b.cd_conta_cred_ndc) cd_conta_cred_provisao,
	p.dt_mes_competencia dt_mes_competencia,
	c.nr_sequencia nr_conta_medica,
	t.nr_contrato,
	substr(decode(p.ie_tipo_protocolo,'I',pls_obter_nome_congenere(p.nr_seq_congenere),pls_obter_dados_prestador(nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec),'N')),1,255) nm_prestador,
	substr(decode(p.ie_tipo_protocolo,'I',pls_obter_cnpj_congenere(p.nr_seq_congenere),nvl(pls_obter_dados_prestador(nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec),'CGC'),
		pls_obter_dados_prestador(nvl(c.nr_seq_prestador,c.nr_seq_prestador_exec),'CPF'))),1,14) nr_cpf_cnpj,
	e.ie_status_faturamento,
	nvl(e.ie_situacao,'A') ie_situacao,
	e.nr_seq_lote_fat,
	l.dt_mesano_referencia,
	e.nr_sequencia nr_seq_conta_pos,
	s.nr_seq_congenere
from	pls_protocolo_conta 		p,
	pls_conta 			c,
	pls_conta_pos_estabelecido 	e,
	pls_conta_pos_estab_contab 	b,
	pls_segurado 			s,
	pls_plano 			d,
	pls_contrato 			t,
	pls_lote_faturamento 		l
where	p.nr_sequencia 		= c.nr_seq_protocolo
and	c.nr_sequencia 		= e.nr_seq_conta
and	e.nr_sequencia 		= b.nr_seq_conta_pos
and	s.nr_sequencia 		= c.nr_seq_segurado
and	d.nr_sequencia 		= s.nr_seq_plano
and	s.nr_seq_contrato 	= t.nr_sequencia(+)
and	e.nr_seq_lote_fat 	= l.nr_sequencia(+);
/