create or replace view material_comercial_v 
as
select 
	a.*,
	a.ds_material ds_material_coml,
	a.cd_material cd_material_coml
from material a
where a.ie_tipo_material <> 3
union
select 
	a.*,
	b.ds_material ds_material_coml,
	b.cd_material cd_material_coml
from Material b, Material a
where a.ie_tipo_material = 3
  and b.cd_material = 
	(select min(cd_material)
	from material c
	where c.cd_material_generico = a.cd_material)
union
select 
	a.*,
	a.ds_material ds_material_coml,
	a.cd_material cd_material_coml
from Material a
where a.ie_tipo_material = 3
  and not exists
	(select 1
	from material c
	where c.cd_material_generico = a.cd_material);
/