create or replace
view os_revisao_migracao_usuario_v as
select	p.nr_sequencia nr_seq_projeto,
	o.nr_sequencia nr_seq_os,
	e.nm_usuario_exec nm_executor,
	o.dt_ordem_servico dt_os,
	p.nr_seq_grupo_des nr_seq_grupo_desenv,
	o.cd_funcao cd_funcao
from	man_ordem_servico_exec e,
	proj_ordem_servico r,
	proj_projeto p,
	man_ordem_servico o
where	e.nr_seq_ordem = o.nr_sequencia
and	r.nr_seq_proj = p.nr_sequencia
and	r.nr_seq_ordem = o.nr_sequencia
and	r.ie_tipo_ordem = 'R'
and	p.nr_seq_gerencia = 9
and	p.nr_seq_classif = 14
and	o.ie_status_ordem in ('1','2')
and	obter_se_usuario_acao_exec_os(o.nr_sequencia, e.nm_usuario_exec) = 'S'
and	o.nr_seq_estagio in (
		select	a.nr_seq_estagio
		from	man_estagio_usuario a
		where	a.nm_usuario_acao = e.nm_usuario_exec);
/