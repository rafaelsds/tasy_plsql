CREATE OR REPLACE VIEW execucao_v
AS
select	nr_consistencia,
	nr_sequencia,
	ds_consistencia
from	execucao_mpo_v
union
select	nr_consistencia,
	nr_sequencia,
	ds_consistencia
from	execucao_treinamento_v
union
select	nr_consistencia,
	nr_sequencia,
	ds_consistencia
from	execucao_manual_v
union
select	nr_consistencia,
	nr_sequencia,
	ds_consistencia
from	execucao_oficial_v
union
select	nr_consistencia,
	nr_sequencia,
	ds_consistencia
from	execucao_final_v
union
select	nr_consistencia,
	nr_sequencia,
	ds_consistencia
from	execucao_equipes_v;
/