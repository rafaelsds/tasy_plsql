create or replace view pls_copel_carga_muc_v 
as
select	a.nr_seq_lote,
	lpad(substr(h.cd_usuario_plano, 1, 9), 9, 0) cd_usuario_plano,
	lpad(substr(
		decode(i.cd_material, null, (	select max(j.cd_ean)
						from 	brasindice_preco j
						where 	j.cd_tuss	= i.cd_material_ops
					),  (	select max(substr(l.cd_ean, 1, 20))
						from 	material j,
							material_brasindice k,
							brasindice_preco l
						where 	j.cd_material	= i.cd_material
						and	j.cd_material	= k.cd_material
						and 	k.cd_medicamento	= l.cd_medicamento
						and	k.cd_laboratorio	= l.cd_laboratorio
						and  	k.cd_apresentacao  	= l.cd_apresentacao
						and  	l.dt_inicio_vigencia  	= (  	select  max(m.dt_inicio_vigencia)
											from 	brasindice_preco  m
											where  	m.cd_medicamento  = k.cd_medicamento
											and  	m.cd_laboratorio  = k.cd_laboratorio
											and  	m.cd_apresentacao = k.cd_apresentacao))
			), 1, 13), 13, 0) codigo_ean,
	to_char(e.dt_final, 'yyyymmdd') dt_final,
	lpad(substr(round(n.pr_subsidio), 1, 3), 3, 0) pr_subsidio,
	lpad(substr(replace(campo_mascara(e.qt_posologia, 2), '.', ''), 1, 8), 8, 0) qt_posologia,
	lpad(substr(e.qt_dias_tratamento, 1, 3), 3, 0) qt_dias_tratamento,
	lpad(substr(e.nr_sequencia, 1, 8), 8, 0) nr_sequencia,
	lpad(substr(n.cd_subsidio, 1, 2), 2, 0) cd_subsidio
from	pls_repasse_sca_fornecedor	a,
	pls_repasse_sca_plano		b,
	pls_repasse_sca_benef		c,
	pls_sca_vinculo			d,
	pls_sca_vinculo_med		e,
	pls_plano_fornecedor		f,
	pls_segurado			g,
	pls_segurado_carteira		h,
	pls_material			i,
	pls_sca_tipo_medicamento	n
where	a.nr_sequencia		= b.nr_seq_repasse_fornecedor 
and	b.nr_sequencia		= c.nr_seq_repasse_plano 
and	d.nr_seq_segurado	= c.nr_seq_segurado 
and	d.nr_seq_plano		= b.nr_seq_sca
and	d.nr_sequencia		= e.nr_seq_sca_vinculo
and	f.nr_seq_plano		= b.nr_seq_sca
and	f.nr_seq_prestador	= a.nr_seq_prestador
and	c.nr_seq_segurado	= g.nr_sequencia
and	g.nr_sequencia		= h.nr_seq_segurado
and	d.dt_liberacao		is not null
and	d.nr_seq_segurado	is not null
and	f.nr_seq_prestador	is not null 
and	e.nr_seq_medicamento	= i.nr_sequencia
and	n.nr_sequencia		= e.nr_seq_tipo_medicamento;
/