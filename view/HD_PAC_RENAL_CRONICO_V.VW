create or replace
view hd_pac_renal_cronico_v as
select	a.cd_pessoa_fisica,
	a.cd_estabelecimento,
	a.ie_pode_pesar,
	a.nr_seq_ponto_acesso,
	a.nr_seq_maquina,
	a.qt_peso_ideal,
	substr(hd_obter_hemodialise_atual(a.cd_pessoa_fisica, 'A'),1,200) 	nr_seq_dialise_atual,
	substr(hd_obter_hemodialise_atual(a.cd_pessoa_fisica, 'U'),1,200) 	nr_seq_dialise_ultima,
	substr(hd_obter_unidade_prc(a.cd_pessoa_fisica,'C'),1,200) 		nr_seq_unid_dial
from hd_pac_renal_cronico a;
/