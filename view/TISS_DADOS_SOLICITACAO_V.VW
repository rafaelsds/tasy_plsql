create or replace
view TISS_DADOS_SOLICITACAO_V as
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	a.nr_atendimento,
	a.nr_seq_agenda,
	TISS_OBTER_DATA_AUTOR(a.nr_sequencia) dt_autorizacao,
	a.ie_carater_int_tiss ie_carater_inter_sus,
	a.ds_indicacao,
	a.nr_sequencia,
	c.cd_cgc,
	d.ds_razao_social,
	d.cd_cnes,
	a.qt_dia_autorizado,
	substr(tiss_obter_tipo_acomod(a.cd_tipo_acomodacao),1,40) cd_tipo_acomod_autor,
	a.nr_sequencia nr_sequencia_autor,
	a.ie_tipo_autorizacao,
	nvl(g.cd_ans, d.cd_ans)cd_ans,
	TISS_OBTER_GUIA_PRIC_AUTOR(x.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GO') cd_autorizacao,
	nvl(a.ie_tipo_internacao_tiss,substr(TISS_OBTER_CLINICA(b.ie_clinica, b.nr_seq_classificacao),1,3)) ie_tipo_internacao,
	nvl(a.ie_regime_internacao,substr(tiss_obter_regime_atend(b.nr_atendimento, c.cd_convenio),1,5)) ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', a.cd_convenio, a.ds_observacao, b.ie_tipo_atendimento, null,'N',0, b.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(a.cd_convenio, b.cd_estabelecimento, a.nm_usuario) || ' ' || substr(a.ds_observacao,1,255)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	a.CD_SENHA,
	a.DT_FIM_VIGENCIA,
	a.qt_dia_solicitado,
	a.NM_RESPONSAVEL,
	a.DT_ENTRADA_PREVISTA,
	e.ie_interno,
	a.cd_convenio,
	substr(TISS_OBTER_CODIGO_PRESTADOR(c.cd_convenio, b.cd_estabelecimento, null, nvl(a.cd_cgc_prestador, x.cd_cgc), to_number(null), 'CI',NULL,b.ie_tipo_atendimento,null),1,20) cd_interno,
	TISS_OBTER_GUIA_PRIC_AUTOR(x.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GP') cd_autorizacao_princ,
	tiss_obter_tipo_guia_autor(a.nr_sequencia) ie_tiss_tipo_guia,
	a.ie_previsao_uso_quimio,
	a.ie_previsao_uso_opme,
	a.ie_tiss_tipo_acidente,
	a.cd_autorizacao_prest,
	0 nr_seq_pedido_exame_ext,
	a.cd_validacao_tiss,
	a.cd_ausencia_cod_valid,
	a.ie_tiss_tipo_etapa_autor
from	estagio_autorizacao e,
	pessoa_juridica d,
	estabelecimento x,
	convenio c,
	atendimento_paciente b,
	autorizacao_convenio a,
	convenio_estabelecimento g
where	a.nr_atendimento	= b.nr_atendimento
and 	c.cd_convenio = g.cd_convenio
and 	g.cd_estabelecimento = a.cd_estabelecimento
and	a.cd_convenio		= c.cd_convenio
and	c.cd_cgc		= d.cd_cgc
and	x.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_estagio	= e.nr_sequencia
and	a.nr_seq_agenda	is null
and	a.nr_seq_agenda_consulta is null
and	a.nr_seq_rxt_tratamento		is null
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	a.nr_seq_agenda,
	TISS_OBTER_DATA_AUTOR(a.nr_sequencia) dt_autorizacao,
	a.ie_carater_int_tiss ie_carater_inter_sus,
	a.ds_indicacao,
	a.nr_sequencia,
	c.cd_cgc,
	d.ds_razao_social,
	d.cd_cnes,
	a.qt_dia_autorizado,
	substr(tiss_obter_tipo_acomod(a.cd_tipo_acomodacao),1,40) cd_tipo_acomod_autor,
	a.nr_sequencia nr_sequencia_autor,
	a.ie_tipo_autorizacao,
	nvl(p.cd_ans, d.cd_ans) cd_ans,
	TISS_OBTER_GUIA_PRIC_AUTOR(f.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GO') cd_autorizacao,
	nvl(a.ie_tipo_internacao_tiss,substr(TISS_OBTER_CLINICA(nvl(g.ie_clinica, g.ie_clinica), g.nr_seq_classificacao),1,3)) ie_tipo_internacao,
	nvl(a.ie_regime_internacao,substr(tiss_obter_regime_atend(a.nr_atendimento, a.cd_convenio),1,5)) ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', a.cd_convenio, a.ds_observacao, b.ie_tipo_atendimento, null,'N',0, f.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(a.cd_convenio, f.cd_estabelecimento, a.nm_usuario) || ' ' || substr(a.ds_observacao,1,255)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	a.CD_SENHA,
	a.DT_FIM_VIGENCIA,
	a.qt_dia_solicitado,
	a.NM_RESPONSAVEL,
	a.DT_ENTRADA_PREVISTA,
	e.ie_interno,
	a.cd_convenio,
	substr(Obter_Valor_Conv_Estab(c.cd_convenio, f.cd_estabelecimento, 'CD_INTERNO'),1,15) cd_interno,
	TISS_OBTER_GUIA_PRIC_AUTOR(f.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GP') cd_autorizacao_princ,
	tiss_obter_tipo_guia_autor(a.nr_sequencia) ie_tiss_tipo_guia,
	a.ie_previsao_uso_quimio,
	a.ie_previsao_uso_opme,
	a.ie_tiss_tipo_acidente,
	a.cd_autorizacao_prest,
	0 nr_seq_pedido_exame_ext,
	a.cd_validacao_tiss,
	a.cd_ausencia_cod_valid,
	a.ie_tiss_tipo_etapa_autor
from	atendimento_paciente g,
	estagio_autorizacao e,
	pessoa_juridica d,
	convenio c,
	agenda f,
	agenda_paciente b,
	autorizacao_convenio a,
	convenio_estabelecimento p
where	a.nr_seq_agenda		= b.nr_sequencia
and c.cd_convenio = p.cd_convenio
and p.cd_estabelecimento = a.cd_estabelecimento
and	b.cd_agenda		= f.cd_agenda
and	a.cd_convenio		= c.cd_convenio
and	c.cd_cgc		= d.cd_cgc
and	a.nr_seq_estagio	= e.nr_sequencia
and	a.nr_atendimento	= g.nr_atendimento(+)
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	a.nr_seq_agenda,
	TISS_OBTER_DATA_AUTOR(a.nr_sequencia) dt_autorizacao,
	a.ie_carater_int_tiss ie_carater_inter_sus,
	a.ds_indicacao,
	a.nr_sequencia,
	c.cd_cgc,
	d.ds_razao_social,
	d.cd_cnes,
	a.qt_dia_autorizado,
	substr(tiss_obter_tipo_acomod(a.cd_tipo_acomodacao),1,40) cd_tipo_acomod_autor,
	a.nr_sequencia nr_sequencia_autor,
	a.ie_tipo_autorizacao,
	nvl(p.cd_ans, d.cd_ans) cd_ans,
	TISS_OBTER_GUIA_PRIC_AUTOR(f.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GO') cd_autorizacao,
	nvl(a.ie_tipo_internacao_tiss,substr(TISS_OBTER_CLINICA(g.ie_clinica, g.nr_seq_classificacao),1,3)) ie_tipo_internacao,
	nvl(a.ie_regime_internacao,substr(tiss_obter_regime_atend(a.nr_atendimento, a.cd_convenio),1,5)) ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', a.cd_convenio, a.ds_observacao, null, null,'N',0, f.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(a.cd_convenio, f.cd_estabelecimento, a.nm_usuario) || ' ' || substr(a.ds_observacao,1,255)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	a.CD_SENHA,
	a.DT_FIM_VIGENCIA,
	a.qt_dia_solicitado,
	a.NM_RESPONSAVEL,
	a.DT_ENTRADA_PREVISTA,
	e.ie_interno,
	a.cd_convenio,
	substr(Obter_Valor_Conv_Estab(c.cd_convenio, f.cd_estabelecimento, 'CD_INTERNO'),1,15) cd_interno,
	TISS_OBTER_GUIA_PRIC_AUTOR(f.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GP') cd_autorizacao_princ,
	tiss_obter_tipo_guia_autor(a.nr_sequencia) ie_tiss_tipo_guia,
	a.ie_previsao_uso_quimio,
	a.ie_previsao_uso_opme,
	a.ie_tiss_tipo_acidente,
	a.cd_autorizacao_prest,
	0 nr_seq_pedido_exame_ext,
	a.cd_validacao_tiss,
	a.cd_ausencia_cod_valid,
	a.ie_tiss_tipo_etapa_autor
from	atendimento_paciente g,
	estagio_autorizacao e,
	pessoa_juridica d,
	convenio c,
	agenda f,
	agenda_consulta b,
	autorizacao_convenio a,
	convenio_estabelecimento p
where	a.nr_seq_agenda_consulta 	= b.nr_sequencia
and c.cd_convenio = p.cd_convenio
and p.cd_estabelecimento = a.cd_estabelecimento
and	b.cd_agenda		= f.cd_agenda
and	a.cd_convenio		= c.cd_convenio
and	c.cd_cgc			= d.cd_cgc
and	a.nr_seq_estagio		= e.nr_sequencia
and	a.nr_atendimento		= g.nr_atendimento(+)
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_seq_agenda,
	TISS_OBTER_DATA_AUTOR(a.nr_sequencia) dt_autorizacao,
	a.ie_carater_int_tiss ie_carater_inter_sus,
	a.ds_indicacao,
	a.nr_sequencia,
	c.cd_cgc,
	d.ds_razao_social,
	d.cd_cnes,
	a.qt_dia_autorizado,
	substr(tiss_obter_tipo_acomod(a.cd_tipo_acomodacao),1,40) cd_tipo_acomod_autor,
	a.nr_sequencia nr_sequencia_autor,
	a.ie_tipo_autorizacao,
	nvl(g.cd_ans, d.cd_ans)cd_ans,
	TISS_OBTER_GUIA_PRIC_AUTOR(b.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GO') cd_autorizacao,
	a.ie_tipo_internacao_tiss ie_tipo_internacao,
	a.ie_regime_internacao ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', a.cd_convenio, a.ds_observacao, null, null,'N',0, b.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(a.cd_convenio, b.cd_estabelecimento, a.nm_usuario) || ' ' || substr(a.ds_observacao,1,255)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	a.CD_SENHA,
	a.DT_FIM_VIGENCIA,
	a.qt_dia_solicitado,
	a.NM_RESPONSAVEL,
	a.DT_ENTRADA_PREVISTA,
	e.ie_interno,
	a.cd_convenio,
	substr(Obter_Valor_Conv_Estab(c.cd_convenio, b.cd_estabelecimento, 'CD_INTERNO'),1,15) cd_interno,
	TISS_OBTER_GUIA_PRIC_AUTOR(b.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GP') cd_autorizacao_princ,
	tiss_obter_tipo_guia_autor(a.nr_sequencia) ie_tiss_tipo_guia,
	a.ie_previsao_uso_quimio,
	a.ie_previsao_uso_opme,
	a.ie_tiss_tipo_acidente,
	a.cd_autorizacao_prest,
	0 nr_seq_pedido_exame_ext,
	a.cd_validacao_tiss,
	a.cd_ausencia_cod_valid,
	a.ie_tiss_tipo_etapa_autor
from	estagio_autorizacao e,
	pessoa_juridica d,
	convenio c,
	autorizacao_convenio_tiss b,
	autorizacao_convenio a,
	convenio_estabelecimento g
where	a.nr_sequencia		= b.nr_sequencia_autor
and c.cd_convenio = g.cd_convenio
and g.cd_estabelecimento = a.cd_estabelecimento
and	a.cd_convenio		= c.cd_convenio
and	c.cd_cgc			= d.cd_cgc
and	a.nr_seq_estagio		= e.nr_sequencia
and	a.nr_atendimento		is null
and	a.nr_seq_agenda			is null
and	a.nr_seq_agenda_consulta	is null
and	a.nr_seq_gestao			is null
and	a.nr_seq_paciente_setor		is null
and	a.nr_seq_rxt_tratamento		is null
and	a.cd_pessoa_fisica 		is not null
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	to_number(null) nr_atendimento,
	to_number(null) nr_seq_agenda,
	TISS_OBTER_DATA_AUTOR(a.nr_sequencia) dt_autorizacao,
	a.ie_carater_int_tiss ie_carater_inter_sus,
	a.ds_indicacao,
	a.nr_sequencia,
	c.cd_cgc,
	d.ds_razao_social,
	d.cd_cnes,
	a.qt_dia_autorizado,
	substr(tiss_obter_tipo_acomod(a.cd_tipo_acomodacao),1,40) cd_tipo_acomod_autor,
	a.nr_sequencia nr_sequencia_autor,
	a.ie_tipo_autorizacao,
	nvl(g.cd_ans, d.cd_ans)cd_ans,
	TISS_OBTER_GUIA_PRIC_AUTOR(a.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GO') cd_autorizacao,
	a.ie_tipo_internacao_tiss ie_tipo_internacao,
	a.ie_regime_internacao ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', a.cd_convenio, a.ds_observacao, null, null,'N',0, a.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(a.cd_convenio, a.cd_estabelecimento, a.nm_usuario) || ' ' || substr(a.ds_observacao,1,255)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	a.CD_SENHA,
	a.DT_FIM_VIGENCIA,
	a.qt_dia_solicitado,
	a.NM_RESPONSAVEL,
	a.DT_ENTRADA_PREVISTA,
	e.ie_interno,
	a.cd_convenio,
	substr(Obter_Valor_Conv_Estab(c.cd_convenio, a.cd_estabelecimento, 'CD_INTERNO'),1,15) cd_interno,
	TISS_OBTER_GUIA_PRIC_AUTOR(a.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GP') cd_autorizacao_princ,
	tiss_obter_tipo_guia_autor(a.nr_sequencia) ie_tiss_tipo_guia,
	a.ie_previsao_uso_quimio,
	a.ie_previsao_uso_opme,
	a.ie_tiss_tipo_acidente,
	a.cd_autorizacao_prest,
	0 nr_seq_pedido_exame_ext,
	a.cd_validacao_tiss,
	a.cd_ausencia_cod_valid,
	a.ie_tiss_tipo_etapa_autor
from	estagio_autorizacao e,
	pessoa_juridica d,
	convenio c,	
	autorizacao_convenio a,
	convenio_estabelecimento g
where	a.cd_convenio		= c.cd_convenio
and c.cd_convenio = g.cd_convenio
and g.cd_estabelecimento = a.cd_estabelecimento
and	c.cd_cgc			= d.cd_cgc
and	a.nr_seq_estagio		= e.nr_sequencia
and	a.nr_atendimento		is null
and	a.nr_seq_agenda			is null
and	a.nr_seq_agenda_consulta	is null
and	a.nr_seq_gestao			is null
and	a.nr_seq_rxt_tratamento		is null
and	a.cd_estabelecimento		is not null
and	a.nr_seq_paciente_setor		is not null
and	a.cd_pessoa_fisica 		is not null
union
select	'2.01.01' ds_versao,
	'AC' ie_origem,
	a.nr_atendimento nr_atendimento,
	to_number(null) nr_seq_agenda,
	TISS_OBTER_DATA_AUTOR(a.nr_sequencia) dt_autorizacao,
	a.ie_carater_int_tiss ie_carater_inter_sus,
	a.ds_indicacao,
	a.nr_sequencia,
	c.cd_cgc,
	d.ds_razao_social,
	d.cd_cnes,
	a.qt_dia_autorizado,
	substr(tiss_obter_tipo_acomod(a.cd_tipo_acomodacao),1,40) cd_tipo_acomod_autor,
	a.nr_sequencia nr_sequencia_autor,
	a.ie_tipo_autorizacao,
	nvl(g.cd_ans, d.cd_ans)cd_ans,
	TISS_OBTER_GUIA_PRIC_AUTOR(a.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GO') cd_autorizacao,
	a.ie_tipo_internacao_tiss ie_tipo_internacao,
	a.ie_regime_internacao ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', a.cd_convenio, a.ds_observacao, null, null,'N',0, a.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(a.cd_convenio, a.cd_estabelecimento, a.nm_usuario) || ' ' || substr(a.ds_observacao,1,255)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	a.CD_SENHA,
	a.DT_FIM_VIGENCIA,
	a.qt_dia_solicitado,
	a.NM_RESPONSAVEL,
	a.DT_ENTRADA_PREVISTA,
	e.ie_interno,
	a.cd_convenio,
	substr(Obter_Valor_Conv_Estab(c.cd_convenio, a.cd_estabelecimento, 'CD_INTERNO'),1,15) cd_interno,
	TISS_OBTER_GUIA_PRIC_AUTOR(a.cd_estabelecimento,a.cd_convenio,a.nr_sequencia,'GP') cd_autorizacao_princ,
	tiss_obter_tipo_guia_autor(a.nr_sequencia) ie_tiss_tipo_guia,
	a.ie_previsao_uso_quimio,
	a.ie_previsao_uso_opme,
	a.ie_tiss_tipo_acidente,
	a.cd_autorizacao_prest,
	0 nr_seq_pedido_exame_ext,
	a.cd_validacao_tiss,
	a.cd_ausencia_cod_valid,
	a.ie_tiss_tipo_etapa_autor
from	estagio_autorizacao e,
	pessoa_juridica d,
	convenio c,	
	autorizacao_convenio a,
	convenio_estabelecimento g
where	a.cd_convenio			= c.cd_convenio
and c.cd_convenio = g.cd_convenio
and g.cd_estabelecimento = a.cd_estabelecimento
and	c.cd_cgc			= d.cd_cgc
and	a.nr_seq_estagio		= e.nr_sequencia
--and	a.nr_atendimento		is null
and	a.nr_seq_agenda			is null
and	a.nr_seq_agenda_consulta	is null
and	a.nr_seq_gestao			is null
and	a.cd_estabelecimento		is not null
and	a.nr_seq_rxt_tratamento		is not null
and	a.cd_pessoa_fisica 		is not null
union
select	'2.01.01' ds_versao,
	'EX' ie_origem,
	a.nr_atendimento,
	null,
	a.dt_solicitacao dt_autorizacao,
	SUBSTR(TISS_OBTER_CARATER_INTERN (B.IE_CARATER_INTER_SUS),1,1) ie_carater_inter_sus,
	a.ds_dados_clinicos ds_indicacao,
	0 nr_sequencia,
	d.cd_cgc cd_cgc,
	e.ds_razao_social ds_razao_social,
	e.cd_cnes,
	0 qt_dia_autorizado,
	null cd_tipo_acomod_autor,
	0 nr_sequencia_autor,
	'3' ie_tipo_autorizacao,
	nvl(g.cd_ans, e.cd_ans)cd_ans,
	c.nr_doc_convenio cd_autorizacao,
	null  ie_tipo_internacao,
	null ie_regime_internacao,
	substr(nvl(tiss_obter_regra_campo(4, 'DS_OBSERVACAO', c.cd_convenio, a.ds_justificativa, b.ie_tipo_atendimento, c.cd_categoria,'N',0, b.cd_estabelecimento, null, null, null, null), tiss_obter_obs_solic_spsadt(c.cd_convenio, b.cd_estabelecimento, a.nm_usuario)),1,254) ds_observacao,
	to_number(null) nr_interno_conta,
	c.CD_SENHA,
	c.DT_INICIO_VIGENCIA,
	0 qt_dia_solicitado,
	null NM_RESPONSAVEL,
	null DT_ENTRADA_PREVISTA,
	'1' ie_interno,
	c.cd_convenio,
	substr(TISS_OBTER_CODIGO_PRESTADOR(c.cd_convenio, b.cd_estabelecimento, null, e.cd_cgc, to_number(null), 'CI',NULL,b.ie_tipo_atendimento,null),1,20) cd_interno,
	null cd_autorizacao_princ,
	'4' ie_tiss_tipo_guia,
	null ie_previsao_uso_quimio,
	null ie_previsao_uso_opme,
	null ie_tiss_tipo_acidente,
	null cd_autorizacao_prest,
	a.nr_sequencia nr_seq_pedido_exame_ext,
	'' cd_validacao_tiss,
	'' cd_ausencia_cod_valid,
	'' ie_tiss_tipo_etapa_autor
from	PEDIDO_EXAME_EXTERNO A,
	ATENDIMENTO_PACIENTE B,
	ATEND_CATEGORIA_CONVENIO C,
	CONVENIO D,
	PESSOA_JURIDICA E,
	ESTABELECIMENTO F,
	AUTORIZACAO_CONVENIO P,
    CONVENIO_ESTABELECIMENTO G
where	b.nr_atendimento	= c.nr_atendimento
and d.cd_convenio = g.cd_convenio
and g.cd_estabelecimento = p.cd_estabelecimento
and	obter_atecaco_atendimento(b.nr_atendimento) 	= c.nr_seq_interno
and	a.nr_atendimento	= b.nr_atendimento
and	d.cd_convenio		= c.cd_convenio
and	d.cd_cgc		= e.cd_cgc
and	f.cd_estabelecimento	= b.cd_estabelecimento;
/
