create or replace
view PROCEDIMENTO_REPASSE_PERDAS_V as
select /*+ CHOOSE */
       a.cd_conta_contabil, 
       a.cd_medico, 
       a.dt_atualizacao, 
       a.dt_atualizacao_nrec, 
       a.dt_contabil, 
       a.dt_contabil_titulo, 
       a.ie_desc_caixa, 
       a.ie_repasse_calc, 
       a.nm_usuario, 
       a.nm_usuario_nrec, 
       a.nr_interno_conta_est, 
       a.nr_repasse_terceiro, 
       a.nr_seq_criterio, 
       a.nr_seq_lote_audit_hist, 
       a.nr_seq_motivo_des, 
       a.nr_seq_partic, 
       a.nr_seq_ret_glosa, 
       a.nr_seq_terceiro, 
       a.nr_seq_trans_fin, 
       a.nr_seq_trans_fin_rep_maior, 
       a.vl_original_repasse, 
       a.vl_liberado, 
       a.vl_repasse, 
       a.dt_liberacao, 
       a.nr_lote_contabil, 
       a.nr_seq_origem, 
       a.cd_regra, 
       a.nr_sequencia, 
       substr(a.ds_observacao,1,255) ds_observacao, 
       a.NR_PROCESSO_AIH, 
       a.NM_USUARIO_LIB, 
       a.IE_ESTORNO, 
       a.NR_SEQ_REGRA_ITEM, 
       a.NR_SEQ_PROCEDIMENTO, 
		   b.nr_interno_conta, 
       b.dt_procedimento, 
       substr(obter_nome_convenio(b.cd_convenio),1,80) ds_convenio, 
       trunc(to_date(substr(obter_descricao_padrao('PROTOCOLO_CONVENIO','DT_MESANO_REFERENCIA',T.NR_SEQ_PROTOCOLO),1,10),'dd/mm/yy')) dt_mesano_protoc, 
       c.cd_procedimento, 
       c.ds_procedimento, 
       substr(OBTER_ATENDIMENTO_RN(d.nr_atendimento,b.nr_sequencia, null),1,50) nr_atendimento, 
       e.cd_pessoa_fisica cd_paciente, 
       substr(OBTER_PACIENE_RN(d.nr_atendimento,b.nr_sequencia, null),1,255) nm_pessoa_fisica, 
       substr(obter_valor_dominio(1129, a.ie_status),1,50) ds_status, 
       substr(obter_desc_trans_financ(a.nr_seq_trans_fin),1,100) ds_transacao, 
       substr(obter_dados_retorno_convenio(a.nr_seq_item_retorno),1,100) ds_retorno, 
       t.nr_seq_protocolo, 
       t.nr_protocolo, 
       b.nr_nf_prestador, 
       b.qt_procedimento, 
       a.ie_status, 
       a.nr_seq_item_retorno, 
       substr(obter_nome_pf_pj(a.cd_medico, null),1,100) nm_medico, 
       substr(obter_nome_tipo_atend(d.ie_tipo_atendimento),1,80) ds_tipo_atendimento, 
       to_char(t.dt_mesano_referencia, 'dd/mm/yyyy') dt_mesano_referencia, 
       b.nr_seq_proc_crit_repasse, 
       nvl(a.ie_analisado, 'N') ie_analis, 
       substr(obter_funcao_medico(nvl(j.IE_FUNCAO, b.ie_funcao_medico)),1,255) ds_funcao_med, 
       substr(obter_nome_setor(b.cd_setor_atendimento),1,255) ds_setor_atendimento, 
       substr(obter_descricao_padrao('MOTIVO_ALT_REPASSE','DS_MOTIVO',A.NR_SEQ_MOTIVO_DES),1,255) ds_motivo_desvinc, 
       substr(Sus_Obter_Desc_Carater_Int(d.ie_carater_inter_sus),1,50) ds_carater_inter_sus, 
       substr(obter_nome_pf_pj(b.cd_medico_req, null),1,100) nm_medico_req, 
       b.NM_USUARIO_ORIGINAL, 
       substr(obter_nome_pf_pj(d.cd_medico_referido,null),1,255) nm_medico_referido, 
       (substr(obter_titulo_conta_protocolo(0,t.NR_INTERNO_CONTA),1,100)||substr(obter_titulo_conta_protocolo(t.nr_seq_protocolo,0),1,100)) ds_titulo, 
       to_date(obter_dados_laudo_procedimento(b.nr_sequencia,'DL')) dt_liberacao_laudo, 
       substr(obter_nota_fiscal_repasse(b.nr_interno_conta, 'N', p.nr_seq_protocolo, p.nr_seq_lote_protocolo),1,255) nr_nota_fiscal, 
       obter_dt_emissao_nf(b.nr_interno_conta) dt_emissao_nota_fiscal, 
       somente_numero(obter_nota_fiscal_repasse(b.nr_interno_conta, 'S', p.nr_seq_protocolo, p.nr_seq_lote_protocolo)) nr_seq_nota_fiscal, 
       f.cd_unidade_basica, 
       substr(obter_valor_dominio(11,g.ie_tipo_convenio),1,255) ds_tipo_conv, 
		   b.vl_procedimento, 
		   substr(obter_nome_pf_pj(b.cd_medico_executor, null),1,80) nm_medico_executor, 
		   substr(obter_nome_pf(Obter_Medico_Laudo_Repasse(b.nr_sequencia)),1,80) nm_medico_laudo, 
       nvl(to_date(obter_dados_conta_paciente(b.nr_interno_conta,'DVNF'),'dd/mm/yyyy'),to_date(obter_dados_protocolo(t.nr_seq_protocolo,'DVNF'),'dd/mm/yyyy')) dt_vencimento_nf, 
       substr(obter_nome_terceiro(a.nr_seq_terceiro),1,255) nm_terceiro, 
       (select max(w.dt_definitivo) from protocolo_convenio w where w.nr_seq_protocolo = t.nr_seq_protocolo) dt_definitivo, 
       a.vl_desp_cartao 
       ,null vl_repasse_cheque, d.ds_observacao ds_obs_atend, d.dt_entrada dt_entrada_atend 
from	protocolo_convenio p,
	convenio g,
       atend_paciente_unidade f, 
       procedimento c, 
       pessoa_fisica e, 
       conta_paciente t, 
       atendimento_paciente d, 
       procedimento_participante j, 
       procedimento_paciente b, 
       procedimento_repasse a 
where  a.nr_seq_procedimento   = b.nr_sequencia 
  and  b.cd_procedimento       = c.cd_procedimento 
  and  b.ie_origem_proced      = c.ie_origem_proced 
  and  b.nr_atendimento        = d.nr_atendimento 
  and  d.cd_pessoa_fisica      = e.cd_pessoa_fisica 
  and  b.nr_interno_conta      = t.nr_interno_conta 
  and  b.nr_seq_atepacu        = f.nr_seq_interno 
  and  t.cd_convenio_parametro = g.cd_convenio
  and	t.nr_seq_protocolo 	= p.nr_seq_protocolo(+)
  and  a.ie_status             = 'P'
  and  a.nr_repasse_terceiro   is null 
  and  a.nr_seq_procedimento   = j.nr_sequencia(+) 
  and  a.nr_seq_partic         = j.nr_seq_partic(+) 
  and  a.nr_repasse_terceiro   is null ;
/