create or replace view eis_escala_braden_q_v  as 
SELECT	DISTINCT	
	EIS_OBTER_SETOR_ATEND_DATA(a.nr_atendimento, e.dt_avaliacao) cd_setor_atendimento,
	SUBSTR(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
	SUBSTR(obter_nome_convenio(obter_convenio_atendimento(a.nr_atendimento)),1,150) ds_convenio,
	obter_nome_setor(EIS_OBTER_SETOR_ATEND_DATA(a.nr_atendimento, e.dt_avaliacao) ) ds_setor_atendimento,
	obter_sexo_pf(a.cd_pessoa_fisica,'C') ie_sexo,
	obter_sexo_pf(a.cd_pessoa_fisica,'D') ds_sexo,
	obter_nome_pessoa_fisica(e.cd_pessoa_fisica, NULL) nm_medico,
	a.cd_pessoa_fisica,
	a.cd_medico_resp,
	SUBSTR(obter_idade(obter_data_nascto_pf(a.cd_pessoa_fisica),SYSDATE,'E'),1,10) ie_faixa_etaria,
	SUBSTR(obter_unidade_atend_data(a.nr_atendimento, e.dt_avaliacao),1,255) ds_unidade,
	e.dt_avaliacao,
	e.qt_ponto qt_ponto,
	f.cd_empresa,
	a.cd_estabelecimento,
	substr(obter_resultado_braden_q(qt_ponto),1,100) ds_gradacao,
	e.nr_atendimento,
	a.cd_pessoa_fisica cd_paciente,
   	substr(obter_turno_data(dt_avaliacao),1,70) ie_turno
FROM 	atend_escala_braden_q e,
	estabelecimento f,
	atendimento_paciente a
WHERE 	a.nr_atendimento= e.nr_atendimento
AND	a.cd_estabelecimento = f.cd_estabelecimento
AND	e.dt_liberacao IS NOT NULL;
/