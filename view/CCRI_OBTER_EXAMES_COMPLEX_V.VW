CREATE OR REPLACE VIEW CCRI_OBTER_EXAMES_COMPLEX_V
AS
select  'PROC' ds_info,
        ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
        trunc(a.dt_referencia,'mm') dt_mes_referencia,
        a.nr_interno_conta,
        c.nr_atendimento,
        a.vl_custo,
        a.qt_resumo,
        b.cd_centro_custo,
        b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
        coalesce(
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
                        and x.cd_setor_atendimento is null
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
                        and x.cd_setor_atendimento is null
            )
            ,
            0
        ) qt_fator
from    conta_paciente c,
        setor_atendimento b,
        conta_paciente_resumo a
where   1=1
and     a.nr_interno_conta = c.nr_interno_conta
and     a.cd_setor_atendimento = b.cd_setor_atendimento
and     c.ie_cancelamento is null
and     (a.cd_procedimento,a.ie_origem_proced) in
        (
        select x.cd_procedimento,x.ie_origem_proced
        from ficha_tecnica x
        where x.cd_procedimento is not null
        and x.ie_origem_proced is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
and     (a.cd_procedimento,a.ie_origem_proced,a.cd_setor_atendimento) not in
        (
        select x.cd_procedimento,x.ie_origem_proced,x.cd_setor_atendimento
        from ficha_tecnica x
        where x.cd_procedimento is not null
        and x.ie_origem_proced is not null
        and x.cd_setor_atendimento is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
and     (a.nr_seq_proc_interno) not in
        (
        select x.nr_seq_proc_interno
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) not in
        (select x.nr_seq_proc_interno,x.cd_setor_atendimento
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and x.cd_setor_atendimento is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
union all
select  'PROC_SETOR' ds_info,
        ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
        trunc(a.dt_referencia,'mm') dt_mes_referencia,
        a.nr_interno_conta,
        c.nr_atendimento,
        a.vl_custo,
        a.qt_resumo,
        b.cd_centro_custo,
        b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
        coalesce(
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            )
            ,
            0
        ) qt_fator
from    conta_paciente c,
        setor_atendimento b,
        conta_paciente_resumo a
where   1=1
and     a.nr_interno_conta = c.nr_interno_conta
and     a.cd_setor_atendimento = b.cd_setor_atendimento
and     c.ie_cancelamento is null
and     (a.cd_procedimento,a.ie_origem_proced,a.cd_setor_atendimento) in
        (
        select x.cd_procedimento,x.ie_origem_proced,x.cd_setor_atendimento
        from ficha_tecnica x
        where x.cd_procedimento is not null
        and x.ie_origem_proced is not null
        and x.cd_setor_atendimento is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
and     (a.nr_seq_proc_interno) not in
        (
        select x.nr_seq_proc_interno
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) not in
        (select x.nr_seq_proc_interno,x.cd_setor_atendimento
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and x.cd_setor_atendimento is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
union all
select  'PROC_INT' ds_info,
        ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
        trunc(a.dt_referencia,'mm') dt_mes_referencia,
        a.nr_interno_conta,
        c.nr_atendimento,
        a.vl_custo,
        a.qt_resumo,
        b.cd_centro_custo,
        b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
        coalesce(
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            )
            ,
            0
        ) qt_fator
from    conta_paciente c,
        setor_atendimento b,
        conta_paciente_resumo a
where   1=1
and     a.nr_interno_conta = c.nr_interno_conta
and     a.cd_setor_atendimento = b.cd_setor_atendimento
and     c.ie_cancelamento is null
and     (a.nr_seq_proc_interno) in
        (
        select x.nr_seq_proc_interno
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) not in
        (select x.nr_seq_proc_interno,x.cd_setor_atendimento
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and x.cd_setor_atendimento is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
union all
select  'PROC_INT_SETOR' ds_info,
        ccri_obter_taxa_custo(a.dt_referencia,b.cd_centro_custo,b.cd_estabelecimento,'A') vl_tx_custo,
        trunc(a.dt_referencia,'mm') dt_mes_referencia,
        a.nr_interno_conta,
        c.nr_atendimento,
        a.vl_custo,
        a.qt_resumo,
        b.cd_centro_custo,
        b.cd_estabelecimento,b.cd_centro_custo_receita,b.cd_centro_controle,
        coalesce(
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = a.nr_seq_proc_interno
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            and x.cd_setor_atendimento = a.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = a.cd_procedimento
            and x.ie_origem_proced = a.ie_origem_proced
            )
            ,
            0
        ) qt_fator
from    conta_paciente c,
        setor_atendimento b,
        conta_paciente_resumo a
where   1=1
and     a.nr_interno_conta = c.nr_interno_conta
and     a.cd_setor_atendimento = b.cd_setor_atendimento
and     c.ie_cancelamento is null
and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) in
        (select x.nr_seq_proc_interno,x.cd_setor_atendimento
        from ficha_tecnica x
        where x.nr_seq_proc_interno is not null
        and x.cd_setor_atendimento is not null
        and exists (
                select 1
                from ficha_tecnica_componente y
                where x.nr_ficha_tecnica = y.nr_ficha_tecnica
                and y.ie_tipo_componente = '3'
                and y.cd_estabelecimento = 21
                and y.cd_centro_controle = 3111
                )
        )
union all
select  'NAO_CALCULADO' ds_info,
        ccri_obter_taxa_custo(bb.dt_procedimento,cc.cd_centro_custo,cc.cd_estabelecimento,'A') vl_tx_custo,
        trunc(bb.dt_procedimento,'mm') dt_mes_referencia,
        bb.nr_interno_conta,
        aa.nr_atendimento,
        0 vl_custo,
        bb.qt_procedimento qt_resumo,
        cc.cd_centro_custo,
        cc.cd_estabelecimento,cc.cd_centro_custo_receita,cc.cd_centro_controle,
        coalesce(
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = bb.nr_seq_proc_interno
            and x.cd_setor_atendimento = bb.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.nr_seq_proc_interno = bb.nr_seq_proc_interno
                        and x.cd_setor_atendimento is null
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = bb.cd_procedimento
            and x.ie_origem_proced = bb.ie_origem_proced
            and x.cd_setor_atendimento = bb.cd_setor_atendimento
            )
            ,
            (
            select  y.qt_fixa
            from    ficha_tecnica_componente y,
                    ficha_tecnica x
            where 1=1
            and x.nr_ficha_tecnica = y.nr_ficha_tecnica
            and x.cd_procedimento = bb.cd_procedimento
            and x.ie_origem_proced = bb.ie_origem_proced
                        and x.cd_setor_atendimento is null
            )
            ,
            0
        ) qt_fator
from    centro_custo dd,
        setor_atendimento cc,
        procedimento_paciente bb,
        atendimento_paciente_v aa
where   1=1
and     aa.nr_atendimento = bb.nr_atendimento
and     bb.cd_setor_atendimento = cc.cd_setor_atendimento
and     cc.cd_centro_custo = dd.cd_centro_custo
and     bb.cd_motivo_exc_conta is null
and     dd.cd_centro_custo = 3111
and     dd.cd_estabelecimento = 21
and     not exists  (
                    select  c.nr_interno_conta
                    from    conta_paciente c,
                            setor_atendimento b,
                            conta_paciente_resumo a
                    where   1=1
                    and     c.nr_interno_conta = bb.nr_interno_conta
                    and     a.nr_interno_conta = c.nr_interno_conta
                    and     a.cd_setor_atendimento = b.cd_setor_atendimento
                    and     c.ie_cancelamento is null
                    and     trunc(a.dt_referencia,'mm') = trunc(bb.dt_procedimento,'mm')
                    and     (a.cd_procedimento,a.ie_origem_proced) in
                            (
                            select  x.cd_procedimento,x.ie_origem_proced
                            from    ficha_tecnica x
                            where   x.cd_procedimento is not null
                            and     x.ie_origem_proced is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    and     (a.cd_procedimento,a.ie_origem_proced,a.cd_setor_atendimento) not in
                            (
                            select  x.cd_procedimento,x.ie_origem_proced,x.cd_setor_atendimento
                            from    ficha_tecnica x
                            where   x.cd_procedimento is not null
                            and     x.ie_origem_proced is not null
                            and     x.cd_setor_atendimento is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    and     (a.nr_seq_proc_interno) not in
                            (
                            select  x.nr_seq_proc_interno
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) not in
                            (select x.nr_seq_proc_interno,x.cd_setor_atendimento
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     x.cd_setor_atendimento is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    union all
                    select  c.nr_interno_conta
                    from    conta_paciente c,
                            setor_atendimento b,
                            conta_paciente_resumo a
                    where   1=1
                    and     c.nr_interno_conta = bb.nr_interno_conta
                    and     a.nr_interno_conta = c.nr_interno_conta
                    and     a.cd_setor_atendimento = b.cd_setor_atendimento
                    and     c.ie_cancelamento is null
                    and     trunc(a.dt_referencia,'mm') = trunc(bb.dt_procedimento,'mm')
                    and     (a.cd_procedimento,a.ie_origem_proced,a.cd_setor_atendimento) in
                            (
                            select  x.cd_procedimento,x.ie_origem_proced,x.cd_setor_atendimento
                            from    ficha_tecnica x
                            where   x.cd_procedimento is not null
                            and     x.ie_origem_proced is not null
                            and     x.cd_setor_atendimento is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    and     (a.nr_seq_proc_interno) not in
                            (
                            select  x.nr_seq_proc_interno
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) not in
                            (
                            select  x.nr_seq_proc_interno,x.cd_setor_atendimento
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     x.cd_setor_atendimento is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    union all
                    select  c.nr_interno_conta
                    from    conta_paciente c,
                            setor_atendimento b,
                            conta_paciente_resumo a
                    where   1=1
                    and     c.nr_interno_conta = bb.nr_interno_conta
                    and     a.nr_interno_conta = c.nr_interno_conta
                    and     a.cd_setor_atendimento = b.cd_setor_atendimento
                    and     c.ie_cancelamento is null
                    and     trunc(a.dt_referencia,'mm') = trunc(bb.dt_procedimento,'mm')
                    and     (a.nr_seq_proc_interno) in
                            (
                            select  x.nr_seq_proc_interno
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) not in
                            (
                            select  x.nr_seq_proc_interno,x.cd_setor_atendimento
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     x.cd_setor_atendimento is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    union all
                    select  c.nr_interno_conta
                    from    conta_paciente c,
                            setor_atendimento b,
                            conta_paciente_resumo a
                    where   1=1
                    and     c.nr_interno_conta = bb.nr_interno_conta
                    and     a.nr_interno_conta = c.nr_interno_conta
                    and     a.cd_setor_atendimento = b.cd_setor_atendimento
                    and     c.ie_cancelamento is null
                    and     trunc(a.dt_referencia,'mm') = trunc(bb.dt_procedimento,'mm')
                    and     (a.nr_seq_proc_interno, a.cd_setor_atendimento) in
                            (
                            select  x.nr_seq_proc_interno,x.cd_setor_atendimento
                            from    ficha_tecnica x
                            where   x.nr_seq_proc_interno is not null
                            and     x.cd_setor_atendimento is not null
                            and     exists (
                                            select  1
                                            from    ficha_tecnica_componente y
                                            where   x.nr_ficha_tecnica = y.nr_ficha_tecnica
                                            and     y.ie_tipo_componente = '3'
                                            and     y.cd_estabelecimento = dd.cd_estabelecimento
                                            and     y.cd_centro_controle = dd.cd_centro_custo
                                            )
                            )
                    );
/