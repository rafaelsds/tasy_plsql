Create or replace view  pls_conta_incons_ocorrencia_v
as
select	'I' ie_tipo,
		nr_sequencia,
		substr(pls_obter_dados_insconsitencia(nr_seq_inconsistencia, 'C'),1,10) nr_codigo,
		substr(pls_obter_dados_insconsitencia(nr_seq_inconsistencia, 'D'),1,255) ds_descricao_incons_ocor,
		substr(ds_observacao,1,255) ds_observacao,
		null nr_regra_oco,
		nr_seq_conta nr_seq_conta,
		nr_seq_procedimento nr_seq_proc,
		nr_seq_material	nr_seq_mat,
		null nr_ocorrencia,
		'S' ie_fechar_conta
from	ptu_intercambio_consist
union
select	'O' ie_tipo,
		a.nr_sequencia,
		substr(pls_obter_dados_ocorrencia(a.nr_seq_ocorrencia,'C'),1,255) nr_codigo,
		substr(pls_obter_dados_ocorrencia(a.nr_seq_ocorrencia,'D'),1,255) ds_descricao_incons_ocor,
		substr(pls_obter_dados_ocorrencia(a.nr_seq_ocorrencia,'DO'),1,255) ds_observacao,
		a.nr_seq_regra	nr_regra_ocor,
		a.nr_seq_conta	nr_seq_conta,
		a.nr_seq_proc	nr_seq_proc,
		a.nr_seq_mat	nr_seq_mat,
		a.nr_seq_ocorrencia nr_ocorrencia,
		b.ie_fechar_conta ie_fechar_conta		
from	pls_ocorrencia_benef	a,
		pls_ocorrencia		b
where	a.nr_seq_ocorrencia = b.nr_sequencia
and		nr_seq_guia_plano is null
and		nr_seq_requisicao is null
and		a.ie_situacao = 'A'
union	
select	'G' ie_tipo,
		a.nr_sequencia,
		substr(tiss_obter_motivo_glosa(nr_seq_motivo_glosa,'C'),1,10) cd_codigo,
		substr(tiss_obter_motivo_glosa(nr_seq_motivo_glosa,'D'),1,255) ds_descricao_glosa_ocor,
		substr(ds_observacao,1,255) ds_observacao,
		0 nr_regra_ocor,
		a.nr_seq_conta 		nr_seq_conta,
		a.nr_seq_conta_proc	nr_seq_proc,
		a.nr_seq_conta_mat	nr_seq_mat,
		a.nr_seq_ocorrencia	nr_ocorrencia,
		a.ie_fechar_conta	ie_fechar_conta		
from	pls_conta_glosa	a
where	a.ie_situacao = 'A';
/