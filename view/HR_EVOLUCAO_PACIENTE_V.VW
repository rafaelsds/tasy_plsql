create or replace view HR_evolucao_paciente_v as
select	a.*,
	a.ie_tipo_evolucao ie_funcao
from	funcao_parametro p,
	evolucao_paciente a
where	((dt_liberacao is not null) or (nvl(vl_parametro, vl_parametro_padrao) = 'N'))
and	p.cd_funcao		= 0
and	p.nr_sequencia	= 37;
/
