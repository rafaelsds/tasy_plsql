create or replace view pls_desconto_folha_corsan_v
as
select	a.nr_seq_cobranca,
	1 tp_registro,
	nvl(f.ie_empresa,'0001') cd_empresa,
	e.nr_cpf,
	9101 cd_matricula,
	to_char(g.dt_remessa_retorno,'yyyymm') dt_arquivo,
	lpad(replace(replace(campo_mascara_virgula(a.vl_cobranca),'.',''),',',''),16,'0') vl_cobranca
from	titulo_receber_cobr a,
	titulo_receber b,
	pls_mensalidade c,
	pls_contrato_pagador_fin d,
	pessoa_fisica e,
	pls_desc_empresa f,
	cobranca_escritural g
where	b.nr_titulo	= a.nr_titulo
and	c.nr_sequencia	= b.nr_seq_mensalidade
and	d.nr_sequencia	= c.nr_seq_pagador_fin
and	e.cd_pessoa_fisica = b.cd_pessoa_fisica
and	f.nr_sequencia	= d.nr_seq_empresa
and	g.nr_sequencia	= a.nr_seq_cobranca;
/