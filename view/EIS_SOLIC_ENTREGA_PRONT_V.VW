create or replace
view Eis_Solic_Entrega_Pront_v as
select	a.dt_solicitacao dt_referencia,
		substr(OBTER_PRONTUARIO_PACIENTE(a.cd_pessoa_fisica),1,254) nr_prontuario,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,254) nm_paciente,
		substr(obter_nome_pf_pj(a.cd_pessoa_solicitante, null),1,254) nm_pessoa_solic,
		a.dt_solicitacao,
		a.dt_prevista,
		a.nr_seq_motivo,
		substr(obter_descricao_padrao('SAME_SOLIC_MOTIVO','DS_MOTIVO',a.NR_SEQ_MOTIVO),1,80) ds_motivo,
		a.dt_entrega,	
		SUBSTR(obter_nome_pf(b.cd_pessoa_fisica),1,120) nm_pessoa_entrega,
		a.cd_estabelecimento,
		a.ie_status,
		substr(obter_valor_dominio(1313,a.ie_status),1,100) ds_status,
		a.ie_tipo_solicitacao,
		substr(obter_valor_dominio(1322,a.ie_tipo_solicitacao),1,100) ds_tipo_solicitacao
from	same_solic_pront a,
		same_copia_prontuario b
where	a.nr_sequencia = b.nr_seq_solic;
/
