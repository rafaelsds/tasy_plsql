create or replace view tws_tipo_documentacao_v as
select nr_sequencia,
	dt_atualizacao,
	ie_situacao,
	ie_inclusao_ops
from tipo_documentacao;
/