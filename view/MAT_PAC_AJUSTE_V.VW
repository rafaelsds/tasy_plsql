create or replace view mat_pac_ajuste_v
as
select		to_char(a.dt_conta,'dd/mm/yyyy') dt_conta, 
		a.cd_material cd_item, 
		a.cd_unidade_medida, 
		a.vl_unitario, 
/*		a.dt_acerto_conta,    os - 147986 - diego */
		a.nr_interno_conta, 
		sum(a.qt_material) qt_item, 
		sum(a.qt_material) qt_real, 
		min(a.dt_atendimento) dt_atendimento, 
		'0' cd_medico_executor,
		'Nenhum' nm_medico,
		a.nr_atendimento,
		a.cd_setor_atendimento,	
		a.cd_material,
		c.ds_material ds_item,
		a.ie_auditoria,
		max(a.dt_conta) dt_conta_item,
		a.nr_doc_convenio,
		a.ie_valor_informado
from  	conta_paciente b, 
		material_atend_paciente a,
		material c
where 	a.cd_motivo_exc_conta is null
and 		a.nr_interno_conta = b.nr_interno_conta
and 		b.ie_status_acerto = 1
and		a.cd_material = c.cd_material
group by 	to_char(a.dt_conta,'dd/mm/yyyy'), 
		a.cd_material, 
		a.cd_unidade_medida, 
		a.vl_unitario, 
--		a.dt_acerto_conta, 
		a.nr_interno_conta,
		a.nr_atendimento,
		a.cd_setor_atendimento,	
		a.cd_material,
		c.ds_material,
		a.ie_auditoria,
		a.nr_doc_convenio,
		a.ie_valor_informado
having 	sum(a.qt_material) <> 0;
/
