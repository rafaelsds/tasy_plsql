CREATE OR REPLACE VIEW W_RESULT_EVOLUCAO_TUMOR_GRID_V ( NM_USUARIO, 
NR_SEQ_LOCALIZACAO, DT_RESULTADO, DS_RESULTADO, NR_SEQUENCIA, 
IE_ORDEM ) AS SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT1),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DECODE(IE_ORDEM,'-6','0',DS_RESULT1) ds_resultado,  
nr_sequencia ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT2),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT2 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT3),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT3 ds_resultado,  
nr_sequencia  ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT4),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT4 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT5),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT5 ds_resultado,  
nr_sequencia ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT6),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT6 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT7),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT7 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT8),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT8 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT9),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT9 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT10),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT10 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT11),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT11 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT12),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT12 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT13),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT13 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT14),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT14 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT15),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT15 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT16),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT16 ds_resultado,  
nr_sequencia ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT17),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT17 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT18),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT18 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT19),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT19 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT20),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT20 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT21),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT21 ds_resultado,  
nr_sequencia  ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT22),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT22 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT23),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT23 ds_resultado,  
nr_sequencia ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT24),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT24 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT25),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT25 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT26),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT26 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT27),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT27 ds_resultado,  
nr_sequencia ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT28),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT28 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT29),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT29 ds_resultado,  
nr_sequencia,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6)  
UNION ALL  
SELECT  a.nm_usuario,  
a.nr_seq_localizacao,  
(SELECT	TRUNC(TO_DATE(MAX(DS_RESULT30),'dd/mm/yyyy hh24:mi:ss'),'DD') FROM	w_recist_registro x WHERE a.nm_usuario = x.nm_usuario  AND ie_ordem  = -3) dt_resultado,  
DS_RESULT30 ds_resultado,  
nr_sequencia ,  
IE_ORDEM  
FROM     w_recist_registro a  
WHERE    ie_ordem IN (-4,-5,-6);
/