create or replace view pls_segurado_conta_imp_v as
select	a.nr_sequencia,
	a.nr_seq_plano,
	pls_obter_conv_cat_segurado(a.nr_sequencia, 1) cd_convenio,
	pls_obter_conv_cat_segurado(a.nr_sequencia, 2) cd_categoria,
	obter_idade(b.dt_nascimento, sysdate, 'A') qt_idade_anos,
	obter_idade(b.dt_nascimento, sysdate, 'M') qt_idade_meses,
	a.ie_local_cadastro,
	a.ie_tipo_segurado,
	nvl(a.ie_pcmso,'N') ie_pcmso,
	b.ie_sexo,
	c.ie_preco,
	a.nr_seq_contrato,
	d.nr_contrato,
	a.nr_seq_intercambio,
	c.ie_regulamentacao,
	b.dt_nascimento,
	pls_obter_dt_primeira_util_seg(a.nr_sequencia) dt_primeira_utilizacao,
	d.dt_contrato,
	a.dt_contratacao,
	a.nr_seq_titular,
	a.nr_seq_grupo_coop,
	a.cd_matricula_estipulante,
	c.ie_acomodacao,
	a.nr_seq_grupo_intercambio,
	a.nr_seq_congenere
from	pls_segurado	a,
	pessoa_fisica	b,
	pls_plano	c,
	pls_contrato	d
where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
and	c.nr_sequencia(+)	= a.nr_seq_plano
and	d.nr_sequencia(+)	= a.nr_seq_contrato;
/