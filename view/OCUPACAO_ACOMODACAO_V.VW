CREATE OR REPLACE VIEW Ocupacao_Acomodacao_V
AS
SELECT  /*+ ORDERED */
	a.cd_tipo_acomodacao,   
	a.ds_tipo_acomodacao,   
      	c.cd_setor_atendimento,
      	c.ds_setor_atendimento,
	c.cd_estabelecimento_base,
	COUNT(*) qt_unidade,
      	sum(DECODE(B.IE_TEMPORARIO, 'S', 1, 0)) qt_UNIDADE_TEMP,     
	sum(DECODE(B.IE_STATUS_UNIDADE, 'P', 1, 0)) qt_UNIDADE_OCUPADA,     
	sum(DECODE(B.IE_STATUS_UNIDADE, 'M', 1, 0)) qt_UNIDADE_ACOMP,     
	sum(DECODE(B.IE_STATUS_UNIDADE, 'L', 1, 0)) qt_UNIDADE_LIVRE,     
	sum(DECODE(B.IE_STATUS_UNIDADE, 'I', 1, 0)) qt_UNIDADE_INTERDITADA,     
	sum(DECODE(B.IE_STATUS_UNIDADE, 'H', 1, 0)) qt_UNIDADE_HIGIENIZACAO,
	sum(DECODE(B.IE_STATUS_UNIDADE, 'R', 1, 0)) qt_UNIDADE_RESERVADA,
	sum(DECODE(B.IE_TEMPORARIO, 'S', 
	    DECODE(B.IE_STATUS_UNIDADE, 'P', 1, 0),0)) qt_UNID_TEMP_OCUP,
	sum(DECODE(B.IE_STATUS_UNIDADE, 'A', 1, 0)) QT_UNIDADES_ALTA
FROM 	SETOR_ATENDIMENTO C,
     	UNIDADE_ATENDIMENTO B,
	TIPO_ACOMODACAO A
WHERE B.CD_SETOR_ATENDIMENTO     = C.CD_SETOR_ATENDIMENTO
  AND B.IE_SITUACAO              = 'A'
  AND A.CD_TIPO_ACOMODACAO	   = B.CD_TIPO_ACOMODACAO
  and C.CD_CLASSIF_SETOR IN (3,4)
GROUP BY 	a.cd_tipo_acomodacao,   
		a.ds_tipo_acomodacao,
		c.cd_setor_atendimento,
		c.ds_setor_atendimento,
		c.cd_estabelecimento_base;
/
