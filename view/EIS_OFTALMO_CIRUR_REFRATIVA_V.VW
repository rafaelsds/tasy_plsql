create or replace
view EIS_OFTALMO_CIRUR_REFRATIVA_V as
SELECT nr_cirurgia,
       IE_TIPO_CIRURGIA,
       SUBSTR(obter_valor_dominio(3882, ie_tipo_cirurgia),1,200) ds_cirurgia,
       nr_sequencia,
       dt_registro dt_referencia,
       SUBSTR(OBTER_DADOS_CIRURGIA(nr_cirurgia,'AT'),1,255) nr_atendimento,
        SUBSTR(obter_nome_pf(cd_profissional),1,255) nm_profissional,
       cd_profissional,
       SUBSTR(OBTER_DESC_CONVENIO(OBTER_CD_CONVENIO_CIRURGIA(nr_cirurgia)),1,255) ds_convenio,
       SUBSTR(OBTER_CD_CONVENIO_CIRURGIA(nr_cirurgia),1,255) cd_convenio,
       SUBSTR(OBTER_IDADE(OBTER_DADOS_PF(OBTER_PESSOA_FISICA_CIRURGIA(nr_cirurgia),'DN'),SYSDATE,'A'),1,255) ds_idade,
       SUBSTR(OBTER_IDADE(OBTER_DADOS_PF(OBTER_PESSOA_FISICA_CIRURGIA(nr_cirurgia),'DN'),SYSDATE,'A'),1,255) cd_idade,
       trim(SUBSTR(OBTER_IDADE(OBTER_DADOS_PF(OBTER_PESSOA_FISICA_CIRURGIA(nr_cirurgia),'DN'),SYSDATE,'E'),1,255)) ie_faixa_etaria,
       NVL(IE_WFO,'N') IE_WFO,
       NVL(IE_FCAT,'N') IE_FCAT,
       NVL(IE_DPCAT,'N') IE_DPCAT,
       NVL(IE_WAVE_FRONT,'N') IE_WAVE_FRONT,
       NVL(IE_TOPOLYZER,'N') IE_TOPOLYZER,
       NVL(IE_OCULYZER,'N') IE_OCULYZER,
       SUBSTR(OBTER_MEDICO_CIRURGIA(nr_cirurgia,'D'),1,255) nm_medico_cirurgiao,
       SUBSTR(OBTER_MEDICO_CIRURGIA(nr_cirurgia,'C'),1,255) cd_medico_cirurgiao
FROM OFT_CIRURGIA_REFRATIVA
WHERE IE_SITUACAO = 'A';
/