CREATE OR REPLACE VIEW execucao_oficial_v
AS
select	4 nr_consistencia,
	a.nr_sequencia,
	substr(obter_valor_dominio(4958,4),1,255)ds_consistencia
from	proj_projeto a
where	exists (    select 	1
		    from	proj_cronograma x,
		    		proj_cron_etapa y
		    where	y.nr_seq_cronograma = x.nr_sequencia
		    and		a.nr_sequencia 	    = x.nr_seq_proj
		    and		y.cd_funcao	   is not null);
/		    