create or replace view tws_mprev_campanha_v as 
select 	nr_sequencia,
	dt_atualizacao,
	nm_campanha,
	dt_inicio,
	dt_termino 
from 	mprev_campanha;
/