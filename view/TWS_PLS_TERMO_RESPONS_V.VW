create or replace view tws_pls_termo_respons_v as
select	nr_sequencia,
	dt_atualizacao,
	cd_estabelecimento,
	ie_situacao,
	ds_termo,
	nm_termo,
	ie_aplicacao
from	pls_termo_responsabilidade;
/