create or replace view conta_financeira_v as
select	cd_conta_financ,
	ds_conta_financ,
	ie_oper_fluxo,
	dt_atualizacao,
	nm_usuario,
	cd_conta_superior,
	cd_conta_apres,
	substr(obter_ds_conta_financ(cd_conta_financ,'S'),1,60) ds_conta_estrut,
	cd_conta_apres || ' ' || ds_conta_financ ds_conta_classif,
	ds_cor,
	ds_cor_fundo,
	ie_situacao,
	cd_estabelecimento,
	ds_cor_html,
	cd_conta_financ_global
from 	conta_financeira;
/