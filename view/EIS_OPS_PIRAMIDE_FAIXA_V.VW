create or replace view eis_ops_piramide_faixa_v
as
select	substr(pls_obter_seq_faixa_etaria(ds_idade,nr_seq_faixa,'D'), 1,30) ds_faixa_etaria,
	ie_tipo_segurado,
	pkg_date_utils.start_of(dt_mes,'MONTH') dt_competencia,
	pkg_date_utils.start_of(dt_mes,'MONTH') dt_mes_competencia_param,
	cd_estabelecimento,
	sum(qt_masculino) qt_masculino,
	sum(qt_feminino) * -1 qt_feminino
from	(
	select	a.ds_idade,
		a.nr_seq_faixa,
		a.ie_tipo_segurado,
		sum(a.qt_masculino) qt_masculino,
		sum(a.qt_feminino) qt_feminino,
		a.cd_estabelecimento,
		b.dt_mes
	from	(
		select	s.nr_sequencia,
			trunc(pkg_date_utils.get_DiffDate(p.dt_nascimento, sysdate, 'YEAR')) ds_idade,
			ie_tipo_segurado,
			decode(p.ie_sexo, 'M', 1, 0) qt_masculino,
			decode(p.ie_sexo, 'F', 1, 0) qt_feminino,
			(select	max(a.nr_sequencia)
			from 	pls_faixa_etaria a, 
				pls_faixa_etaria_item b 
			where 	a.nr_sequencia = b.nr_seq_faixa_etaria 
			and 	a.ie_tipo_faixa_etaria = 'IG' 
			and 	a.ie_situacao = 'A'
			and	a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento) nr_seq_faixa,
			s.cd_estabelecimento,
			pkg_date_utils.start_of(s.dt_contratacao,'MONTH') dt_contratacao,
			pkg_date_utils.start_of(s.dt_rescisao,'MONTH') dt_rescisao
		from	pls_segurado s,
			pessoa_fisica p
		where	s.cd_pessoa_fisica = p.cd_pessoa_fisica
		and	dt_liberacao is not null
		) a,
		(select	dt_mes
		from	mes_v
		where	dt_mes > pkg_date_utils.start_of(pkg_date_utils.add_month(sysdate, -12), 'MONTH')) b
	where	ds_idade is not null
	and	a.dt_contratacao <= b.dt_mes
	and	nvl(a.dt_rescisao,b.dt_mes) >= b.dt_mes
	group by a.ds_idade, a.nr_seq_faixa, a.ie_tipo_segurado, a.cd_estabelecimento, b.dt_mes
	order by a.ds_idade desc
	)
group by substr(pls_obter_seq_faixa_etaria(ds_idade,nr_seq_faixa,'D'),1,30),
	 ie_tipo_segurado,
	 cd_estabelecimento,
	 pkg_date_utils.start_of(dt_mes,'MONTH')
order by ds_faixa_etaria desc;
/