create or replace view eis_escala_snap_snappe_v  as
select	nvl(eis_obter_setor_atend_data(a.nr_atendimento, e.dt_avaliacao),a.cd_setor_atendimento) cd_setor_atendimento,
	substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,80) ds_estabelecimento,
	obter_convenio_atendimento(a.nr_atendimento) cd_convenio,
	substr(obter_nome_convenio(obter_convenio_atendimento(a.nr_atendimento)),1,150) ds_convenio,
	obter_nome_setor(nvl(eis_obter_setor_atend_data(a.nr_atendimento, e.dt_avaliacao),a.cd_setor_atendimento)) ds_setor_atendimento,
	obter_sexo_pf(a.cd_pessoa_fisica,'C') ie_sexo,
	obter_sexo_pf(a.cd_pessoa_fisica,'D') ds_sexo,
	obter_nome_pessoa_fisica(e.cd_profissional, null) nm_medico,
	e.cd_profissional,
	substr(obter_idade(obter_data_nascto_pf(a.cd_pessoa_fisica),sysdate,'E'),1,10) ie_faixa_etaria,
	obter_unidade_atendimento(a.nr_atendimento,'A','U') ds_unidade,
	e.dt_avaliacao,
	e.qt_snap_ii,
	e.qt_snappe_ii,
	f.cd_empresa,
	a.cd_estabelecimento,
	a.nr_atendimento,
	e.pr_mortalidade
from 	escala_snapii_snappeii e,
	estabelecimento f,
	atendimento_paciente_v a
where 	a.nr_atendimento= e.nr_atendimento
and	a.cd_estabelecimento = f.cd_estabelecimento
and	e.dt_liberacao is not null
and	e.dt_inativacao is null;
/
