create or replace
VIEW EIS_QUA_DOC_ELAB_V
as
select	to_char(trunc(sysdate, 'yy'), 'yyyy') cd_ano,
	somente_numero(to_char(trunc(sysdate, 'yy'), 'yyyy')) nr_ano,
	cd_estabelecimento,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '01', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_jan,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '02', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_fev,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '03', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_mar,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '04', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_abr,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '05', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_mai,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '06', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_jun,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '07', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_jul,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '08', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_ago,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '09', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_set,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '10', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_out,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '11', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_nov,
	nvl(max(decode(to_char(dt_referencia, 'mm'), '12', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_dez,
	dividir(SUM(dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100) , SUM(DECODE(dividir(QT_DOC_ELABORADO, QT_DOCUMENTO), 0, 0, 1))) qt_media,
	cd_setor_atendimento
from 	eis_qua_doc_elab
where	trunc(dt_referencia, 'yy') = trunc(sysdate, 'yy')
group	by to_char(trunc(sysdate, 'yy'), 'yyyy'),
	somente_numero(to_char(trunc(sysdate, 'yy'), 'yyyy')),
	cd_setor_atendimento,cd_estabelecimento
union all
SELECT	TO_CHAR(TRUNC(SYSDATE, 'yy'), 'yyyy') cd_ano,
	somente_numero(TO_CHAR(TRUNC(SYSDATE, 'yy'), 'yyyy')) nr_ano,
	avg(cd_estabelecimento),
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '01',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '01',SUM(QT_DOCumento),0)) * 100),0) qt_jan,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '02',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '02',SUM(QT_DOCumento),0)) * 100),0) qt_fev,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '03',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '03',SUM(QT_DOCumento),0)) * 100),0) qt_mar,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '04',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '04',SUM(QT_DOCumento),0)) * 100),0) qt_abr,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '05',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '05',SUM(QT_DOCumento),0)) * 100),0) qt_mai,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '06',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '06',SUM(QT_DOCumento),0)) * 100),0) qt_jun,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '07',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '07',SUM(QT_DOCumento),0)) * 100),0) qt_jul,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '08',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '08',SUM(QT_DOCumento),0)) * 100),0) qt_ago,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '09',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '09',SUM(QT_DOCumento),0)) * 100),0) qt_set,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '10',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '10',SUM(QT_DOCumento),0)) * 100),0) qt_out,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '11',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '11',SUM(QT_DOCumento),0)) * 100),0) qt_nov,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '12',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '12',SUM(QT_DOCumento),0)) * 100),0) qt_dez,
	nvl(SUM(dividir(SUM(QT_DOC_ELABORADO), SUM(qt_documento)) * 100) / COUNT(*),0) qt_media,
	-1
FROM 	eis_qua_doc_elab
WHERE	TRUNC(dt_referencia, 'yy') = TRUNC(SYSDATE, 'yy')
GROUP	BY TO_CHAR(TRUNC(SYSDATE, 'yy'), 'yyyy'),
	somente_numero(TO_CHAR(TRUNC(SYSDATE, 'yy'), 'yyyy')),
	cd_estabelecimento,
	dt_referencia
union all				
SELECT	TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy') cd_ano,
	somente_numero(TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy')) nr_ano,
	cd_estabelecimento,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '01', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_jan,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '02', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_fev,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '03', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_mar,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '04', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_abr,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '05', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_mai,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '06', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_jun,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '07', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_jul,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '08', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_ago,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '09', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_set,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '10', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_out,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '11', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_nov,
	NVL(MAX(DECODE(TO_CHAR(dt_referencia, 'mm'), '12', dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100,0)),0) qt_dez,
	dividir(SUM(dividir(QT_DOC_ELABORADO, QT_DOCUMENTO) * 100) , SUM(DECODE(dividir(QT_DOC_ELABORADO, QT_DOCUMENTO), 0, 0, 1))) qt_media,
	cd_setor_atendimento
FROM 	eis_qua_doc_elab
WHERE	(TRUNC(dt_referencia, 'yy')) = TRUNC((TRUNC(SYSDATE, 'yy')-1),'yy')
GROUP	BY TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy'),
	somente_numero(TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy')),
	cd_estabelecimento, cd_setor_atendimento
union all
SELECT	TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy') cd_ano,
	somente_numero(TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy')) nr_ano,
	avg(cd_estabelecimento),
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '01',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '01',SUM(QT_DOCumento),0)) * 100),0) qt_jan,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '02',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '02',SUM(QT_DOCumento),0)) * 100),0) qt_fev,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '03',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '03',SUM(QT_DOCumento),0)) * 100),0) qt_mar,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '04',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '04',SUM(QT_DOCumento),0)) * 100),0) qt_abr,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '05',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '05',SUM(QT_DOCumento),0)) * 100),0) qt_mai,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '06',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '06',SUM(QT_DOCumento),0)) * 100),0) qt_jun,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '07',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '07',SUM(QT_DOCumento),0)) * 100),0) qt_jul,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '08',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '08',SUM(QT_DOCumento),0)) * 100),0) qt_ago,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '09',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '09',SUM(QT_DOCumento),0)) * 100),0) qt_set,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '10',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '10',SUM(QT_DOCumento),0)) * 100),0) qt_out,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '11',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '11',SUM(QT_DOCumento),0)) * 100),0) qt_nov,
	nvl(SUM(dividir(DECODE(TO_CHAR(dt_referencia, 'mm'), '12',SUM(QT_DOC_ELABORADO),0) , DECODE(TO_CHAR(dt_referencia, 'mm'), '12',SUM(QT_DOCumento),0)) * 100),0) qt_dez,
	nvl(SUM(dividir(SUM(QT_DOC_ELABORADO), SUM(qt_documento)) * 100) / COUNT(*),0) qt_media,
	-1
FROM 	eis_qua_doc_elab
WHERE	(TRUNC(dt_referencia, 'yy')) = TRUNC((TRUNC(SYSDATE, 'yy')-1),'yy')
GROUP	BY TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy'),
	somente_numero(TO_CHAR(TRUNC(SYSDATE, 'yy')-1, 'yyyy')),
	cd_estabelecimento,
	dt_referencia
union all
select	'Meta' cd_ano,
	0,
	cd_estabelecimento,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'P') qt_jan,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'P') qt_fev,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'P') qt_mar,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'P') qt_abr,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'P') qt_mai,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'P') qt_jun,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'P') qt_jul,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'P') qt_ago,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'P') qt_set,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'P') qt_out,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'P') qt_nov,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'P') qt_dez,
	0 qt_media,
	0 cd_setor_atendimento
from 	estabelecimento
group by cd_estabelecimento
union all
select	'Bench' cd_ano,
	1,
	cd_estabelecimento,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'B') qt_jan,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'B') qt_fev,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'B') qt_mar,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'B') qt_abr,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'B') qt_mai,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 1, 'B') qt_jun,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'B') qt_jul,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'B') qt_ago,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'B') qt_set,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'B') qt_out,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'B') qt_nov,
	Eis_Obter_Meta_Indicador(cd_estabelecimento, 451, to_number(to_char(trunc(sysdate, 'yy'), 'yyyy')), 2, 'B') qt_dez,
	0 qt_media,
	0 cd_setor_atendimento
from 	estabelecimento
group by cd_estabelecimento;
/