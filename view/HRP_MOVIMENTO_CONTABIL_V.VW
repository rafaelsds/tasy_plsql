create or replace view hrp_movimento_contabil_v
as
select	1 tp_registro,
	a.nr_sequencia nr_seq_movimento,
	a.nr_seq_mes_ref,
	c.cd_tipo_lote_contabil,
	c.nr_lote_contabil,
	a.cd_conta_debito,
	a.cd_conta_credito,
	a.cd_historico,
	nvl(b.vl_movimento, a.vl_movimento) vl_movimento,
	a.dt_movimento,
	a.ds_compl_historico,
	a.nm_usuario,
	a.ie_debito_credito,
	decode(a.ie_debito_credito, 'D', b.cd_centro_custo, null) cd_centro_debito,
	decode(a.ie_debito_credito, 'D', decode(nvl(b.cd_centro_custo, 0), 0, null, b.vl_movimento)) vl_movto_centro_debito,
	decode(a.ie_debito_credito, 'C', b.cd_centro_custo, null) cd_centro_credito,
	decode(a.ie_debito_credito, 'C', decode(nvl(b.cd_centro_custo, 0), 0, null, b.vl_movimento)) vl_movto_centro_credito
from	lote_contabil c,
	ctb_movto_centro_custo b,
	ctb_movimento_v a
where	a.nr_sequencia		= b.nr_seq_movimento(+)
and	a.nr_lote_contabil		= c.nr_lote_contabil;
/