create or replace view HSC_CONTAS_REPASSE_PENDENTE_V as
select	d.nr_seq_terceiro,
	b.nr_titulo,
	e.nr_atendimento,
	c.nr_sequencia,
	d.vl_repasse vl_item,
	h.ds_convenio,
	'Procedimento' ds_tipo,
	e.cd_convenio_parametro,
	e.cd_estabelecimento,
	e.dt_acerto_conta,
	d.ie_status
from	titulo_receber b,
	procedimento_repasse d,
	procedimento_paciente c,
	convenio h,
	conta_paciente e
where	e.nr_interno_conta	= b.nr_interno_conta
and	d.nr_repasse_terceiro	is null
and	c.nr_sequencia		= d.nr_seq_procedimento
and	e.nr_interno_conta	= c.nr_interno_conta
and	(h.ie_tipo_convenio	= 1 or
	exists
	(select	1
	from	convenio_retorno_item x
	where	x.nr_interno_conta	= e.nr_interno_conta))
and	e.cd_convenio_parametro	= h.cd_convenio
union
select	d.nr_seq_terceiro,
	b.nr_titulo,
	e.nr_atendimento,
	c.nr_sequencia,
	d.vl_repasse vl_item,
	h.ds_convenio,
	'Procedimento' ds_tipo,
	e.cd_convenio_parametro,
	e.cd_estabelecimento,
	e.dt_acerto_conta,
	d.ie_status
from	titulo_receber b,
	procedimento_repasse d,
	procedimento_paciente c,
	convenio h,
	conta_paciente e
where	e.nr_seq_protocolo	= b.nr_seq_protocolo
and	d.nr_repasse_terceiro	is null
and	c.nr_sequencia		= d.nr_seq_procedimento
and	e.nr_interno_conta	= c.nr_interno_conta
and	(h.ie_tipo_convenio	= 1 or
	exists
	(select	1
	from	convenio_retorno_item x
	where	x.nr_interno_conta	= e.nr_interno_conta))
and	e.cd_convenio_parametro	= h.cd_convenio
and	e.ie_cancelamento	is null
union
select	d.nr_seq_terceiro,
	b.nr_titulo,
	e.nr_atendimento,
	c.nr_sequencia,
	d.vl_repasse vl_item,
	h.ds_convenio,
	'Material' ds_tipo,
	e.cd_convenio_parametro,
	e.cd_estabelecimento,
	e.dt_acerto_conta,
	d.ie_status
from	titulo_receber b,
	material_repasse d,
	material_atend_paciente c,
	convenio h,
	conta_paciente e
where	e.nr_interno_conta	= b.nr_interno_conta
and	d.nr_repasse_terceiro	is null
and	c.nr_sequencia		= d.nr_seq_material
and	e.nr_interno_conta	= c.nr_interno_conta
and	(h.ie_tipo_convenio	= 1 or
	exists
	(select	1
	from	convenio_retorno_item x
	where	x.nr_interno_conta	= e.nr_interno_conta))
and	e.cd_convenio_parametro	= h.cd_convenio
union
select	d.nr_seq_terceiro,
	b.nr_titulo,
	e.nr_atendimento,
	c.nr_sequencia,
	d.vl_repasse vl_item,
	h.ds_convenio,
	'Material' ds_tipo,
	e.cd_convenio_parametro,
	e.cd_estabelecimento,
	e.dt_acerto_conta,
	d.ie_status
from	titulo_receber b,
	material_repasse d,
	material_atend_paciente c,
	convenio h,
	conta_paciente e
where	e.nr_seq_protocolo	= b.nr_seq_protocolo
and	d.nr_repasse_terceiro	is null
and	c.nr_sequencia		= d.nr_seq_material
and	e.nr_interno_conta	= c.nr_interno_conta
and	(h.ie_tipo_convenio	= 1 or
	exists
	(select	1
	from	convenio_retorno_item x
	where	x.nr_interno_conta	= e.nr_interno_conta))
and	e.cd_convenio_parametro	= h.cd_convenio
and	e.ie_cancelamento	is null;
/