CREATE OR REPLACE VIEW HISTORICO_PADRAO_EMPRESA_V AS
SELECT a.cd_historico,
       a.ds_historico,
       a.dt_atualizacao,
       a.nm_usuario,
       a.cd_empresa,
       a.dt_atualizacao_nrec,
       a.nm_usuario_nrec,
       a.cd_sistema_contabil,
       a.ie_situacao,
       a.cd_historico_ref,
       a.nr_seq_grupo_emp
  FROM historico_padrao a
 WHERE a.nr_seq_grupo_emp IS NULL
UNION
SELECT a.cd_historico,
       a.ds_historico,
       a.dt_atualizacao,
       a.nm_usuario,
       b.cd_empresa,
       a.dt_atualizacao_nrec,
       a.nm_usuario_nrec,
       a.cd_sistema_contabil,
       a.ie_situacao,
       a.cd_historico_ref,
       a.nr_seq_grupo_emp
  FROM historico_padrao a,
       grupo_empresa_v b
 WHERE a.nr_seq_grupo_emp IS NOT NULL
   AND a.nr_seq_grupo_emp = b.nr_seq_grupo_empresa;
/