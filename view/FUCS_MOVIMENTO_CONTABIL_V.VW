create or replace
view fucs_movimento_contabil_v as
select	001									tp_registro,
	0									nr_seq_movimento,
	a.nr_lote_contabil								nr_lote_contabil,
	min(a.dt_movimento)							dt_movimento,
	max(x.dt_referencia)								dt_geracao_lote,
	count(*)									qt_total_lancamento,
	sum(a.vl_movimento)							vl_total_movimento,
	max(x.nm_usuario)								nm_usuario,
	sum(a.vl_movimento)							vl_total_conta,
	0									vl_total_historico,
	'' 									ds_sistema,
	to_char(max(x.dt_referencia),'mm')						dt_referencia,
	'0'									ie_tipo_lancamento,
	''									cd_conta_contabil,
	''									ds_referencia_1,
	0									cd_historico,
	0									vl_movimento,
	0									vl_fasb,
	0									vl_cmcac,
	0									vl_presente,
	''									cd_conta_contra_partida,
	''									ds_referencia_2,
	''									ds_historico,
	x.cd_tipo_lote_contabil,
	x.nr_seq_mes_ref
from	lote_contabil x,
	ctb_movimento_v a
where	a.nr_lote_contabil	= x.nr_lote_contabil 
group by	a.nr_lote_contabil, x.cd_tipo_lote_contabil, x.nr_seq_mes_ref
union all
select	distinct
	002									tp_registro,
	b.nr_sequencia								nr_seq_movimento,						
	b.nr_lote_contabil								nr_lote_contabil,
	b.dt_movimento								dt_movimento,
	null									dt_geracao_lote,
	0									qt_total_lancamento,
	0									vl_total_movimento,
	b.nm_usuario								nm_usuario,
	0									vl_total_conta,
	0									vl_total_historico,
	''									ds_sistema,
	to_char(b.dt_movimento,'mm')							dt_referencia,		
	substr(fucs_obter_se_deb_cred_cpt(b.nr_sequencia),1,2)				ie_tipo_lancamento,
substr(obter_dados_conta_contabil(nvl(b.cd_conta_debito,b.cd_conta_credito),b.cd_estabelecimento,'SC') ||
nvl(ctb_obter_dados_centro_custo(nvl(d.cd_centro_custo,c.cd_centro_custo),'S'),'03001001'),1,20)	cd_conta_contabil,
	''									ds_referencia_1,
	b.cd_historico								cd_historico,
	nvl(c.vl_movimento,b.vl_movimento)						vl_movimento,
	0									vl_fasb,
	0									vl_cmcac,
	0									vl_presente,
	substr(obter_dados_conta_contabil(	decode(nvl(b.cd_conta_debito,'0'), '0',b.cd_conta_debito,b.cd_conta_credito), b.cd_estabelecimento,'SC') ||
	nvl(ctb_obter_dados_centro_custo(nvl(d.cd_centro_custo,c.cd_centro_custo),'S'),'03001001'),1,20) cd_conta_contra_partida,
	substr(to_char(sysdate,'ddmmyy') || b.nr_lote_contabil,1,16)		ds_referencia_2,
	b.ds_compl_historico							ds_historico,
	a.cd_tipo_lote_contabil,
	b.nr_seq_mes_ref
from	conta_contabil d,
	ctb_movto_centro_custo c,
	ctb_movimento b,
	lote_contabil a
where	a.nr_lote_contabil	= b.nr_lote_contabil
and	b.nr_sequencia		= c.nr_seq_movimento(+)
and	nvl(b.cd_conta_debito,b.cd_conta_credito)	= d.cd_conta_contabil
union all
select	distinct
	003									tp_registro,
	c.nr_sequencia								nr_seq_movimento,
	c.nr_lote_contabil								nr_lote_contabil,
	c.dt_movimento								dt_movimento,
	null									dt_geracao_lote,
	0									qt_total_lancamento,
	0									vl_total_movimento,
	c.nm_usuario								nm_usuario,
	0									vl_total_conta,
	0									vl_total_historico,
	''									ds_sistema,
	to_char(c.dt_movimento,'mm')							dt_referencia,
	'0'									ie_tipo_lancamento,
	''									cd_conta_contabil,
	''									ds_referencia_1,
	c.cd_historico								cd_historico,
	c.vl_movimento								vl_movimento,
	0									vl_fasb,
	0									vl_cmcac,
	0									vl_presente,
	''									cd_conta_contra_partida,
	''									ds_referencia_2,
	''									ds_historico,
	a.cd_tipo_lote_contabil,
	c.nr_seq_mes_ref
from	ctb_movimento c,
	lote_contabil a
where	a.nr_lote_contabil	= c.nr_lote_contabil;
/