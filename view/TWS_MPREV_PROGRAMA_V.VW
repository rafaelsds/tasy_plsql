create or replace view tws_mprev_programa_v as 
select 	nr_sequencia,
	dt_atualizacao,
	dt_inicio_programa,
	nm_programa,
	dt_exclusao
from 	mprev_programa;
/