create or replace
view ISSDIGITAL_NOVA_LIMA_V as
select	distinct
	0					tp_registro,
	to_char(sysdate, 'ddmmyyyy')		dt_geracao_arquivo,
	rpad(obter_dados_pf_pj(null, a.cd_cgc,'IM'),10, ' ' )	cd_inscricao_municipal,		
	a.cd_cgc				cd_cgc,
	a.ds_razao_social			ds_razao_social,
	1					nr_arquivo,
	'0202'					nr_versao_arquivo,
	'P'					ie_envio_arquivo,
	'ISSDigital'				nm_sistema_envio,
	null					nr_linha,
	''					cd_cgc_emitente,
	''					ie_prestador,
	null					dt_competencia,
	null					nr_nota_fiscal_inicial,
	''					cd_serie_nf,
	null					nr_nota_fiscal_final,
	null					dt_dia,
	''					ie_tipo_lancamento,
	null					vl_total_nota,
	''					cd_atividade,
	''					ie_tipo_escrituracao,
	a.cd_estabelecimento,
	null					dt_emissao
from	estabelecimento_v a
union
select	distinct
	1 					tp_registro,
	to_char(sysdate, 'ddmmyyyy')			dt_geracao_arquivo,
	rpad(obter_dados_pf_pj(null, a.cd_cgc,'IM'),10,' ')	cd_inscricao_municipal,
	''					cd_cgc,
	''					ds_razao_social,
	1					nr_arquivo,
	''					nr_versao_arquivo,
	''					ie_envio_arquivo,
	''					nm_sistema_envio,
	null					nr_linha,
	lpad(decode(a.cd_cgc,null,obter_dados_pf(a.cd_pessoa_fisica,'CPF'),a.cd_cgc),14,0) cd_cgc_emitente,
	'P'					ie_prestador,
	to_char(a.dt_emissao, 'YYYYMM')		dt_competencia,
	lpad(nvl(a.nr_nota_fiscal, 0), 8, 0)		nr_nota_fiscal_inicial,
	a.cd_serie_nf				cd_serie_nf,
	lpad(nvl(a.nr_nota_fiscal, 0), 8, 0)		nr_nota_fiscal_final,
	to_char(a.dt_emissao, 'dd')			dt_dia,
	decode(a.ie_situacao,9, 'C', 3, 'C', decode(obter_se_nf_retem_iss(a.nr_sequencia), 'N', 'I', 'S', 'T', 'C'))	ie_tipo_lancamento,
	a.vl_total_nota				vl_total_nota,
	'003020008'				cd_atividade,
	'N' 					ie_tipo_escrituracao,
	a.cd_estabelecimento,
	a.dt_emissao	
from	nota_fiscal a
where	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'S'
and	ie_situacao <> 2
union
select	distinct
	9 					tp_registro,
	''					dt_geracao_arquivo,
	''					cd_inscricao_municipal,		
	''					cd_cgc,
	''					ds_razao_social,
	1					nr_arquivo,
	''					nr_versao_arquivo,
	''					ie_envio_arquivo,
	''					nm_sistema_envio,
	''					nr_linha,
	''					cd_cgc_emitente,
	''					ie_prestador,
	null					dt_competencia,
	null					nr_nota_fiscal_inicial,
	''					cd_serie_nf,
	null					nr_nota_fiscal_final,
	null					dt_dia,
	''					ie_tipo_lancamento,
	null					vl_total_nota,
	''					cd_atividade,
	''					ie_tipo_escrituracao,
	a.cd_estabelecimento,
	null					dt_emissao
from	estabelecimento_v a;
/