create or replace view pessoa_doc_valid_v as
select     pdoc.cd_pessoa_fisica,
           pdoc.ds_arquivo,
           pdoc.nr_seq_documento,
           pdoc.dt_atualizacao_nrec,
           pdoc.nm_usuario_nrec,
           pdoc.dt_validade,
           tpdoc.ie_finalidade
   from  pessoa_documentacao pdoc,
         tipo_documentacao   tpdoc
   where pdoc.nr_seq_documento = tpdoc.nr_sequencia
   and pdoc.dt_validade is not null
   and  trunc(pdoc.dt_validade) >= trunc(sysdate)
   and tpdoc.ie_finalidade ='I';
/