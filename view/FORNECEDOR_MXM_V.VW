create or replace view FORNECEDOR_MXM_V as
/* Fornecedor - tipo de registro 1 */
select	1 ie_tipo_registro,
	'02' ie_tipo_registro_1,
	b.cd_cgc cd_fornecedor,
	'J' ie_tipo_pessoa,
	b.ds_razao_social nm_fornecedor,
	to_char(b.dt_atualizacao,'ddmmyyyy') dt_nascimento,
	b.cd_cgc nr_cpf_cnpj,
	b.nr_inscricao_estadual nr_identificacao,
	b.ds_endereco ds_endereco,
	b.ds_bairro ds_bairro,
	b.cd_cep cd_cep,
	b.ds_municipio ds_cidade,
	b.sg_estado sg_estado,
	b.ds_email ds_email,
	b.nr_telefone nr_telefone,
	b.nr_fax nr_fax,
	'A' ie_ativo,
	''ie_tipo_registro_2,
	'' cd_empresa,
	'' cd_filial,
	'' cd_moeda,
	'' cd_conta_contabil,
	'' ie_tipo_registro_3,
	'' cd_grupo_pag,
	'' ie_tipo_registro_4,
	'' cd_conta,
	'' cd_banco,
	'' cd_agencia,
	'' nr_conta,
	a.dt_emissao dt_emissao,
	to_char(null) cd_pessoa_fisica,
	a.cd_cgc cd_cgc
from	pessoa_juridica b,
	titulo_pagar a
where	a.cd_cgc	= b.cd_cgc
and	b.dt_integracao_externa is null
union all
select	1 ie_tipo_registro,
	'02' ie_tipo_registro_1,
	b.cd_pessoa_fisica cd_fornecedor,
	'F' ie_tipo_pessoa,
	b.nm_pessoa_fisica nm_fornecedor,
	to_char(b.dt_nascimento,'ddmmyyyy') dt_nascimento,
	b.nr_cpf nr_cpf_cnpj,
	b.nr_identidade nr_identificacao,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'EN'),1,255) ds_endereco,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'B'),1,255) ds_bairro,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'CEP'),1,255) cd_cep,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'CI'),1,255) ds_cidade,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'UF'),1,255) sg_estado,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'M'),1,255) ds_email,
	substr(obter_compl_pf(a.cd_pessoa_fisica,1,'T'),1,255) nr_telefone,
	'' nr_fax,
	'A' ie_ativo,
	''ie_tipo_registro_2,
	'' cd_empresa,
	'' cd_filial,
	'' cd_moeda,
	'' cd_conta_contabil,
	'' ie_tipo_registro_3,
	'' cd_grupo_pag,
	'' ie_tipo_registro_4,
	'' cd_conta,
	'' cd_banco,
	'' cd_agencia,
	'' nr_conta,
	a.dt_emissao dt_emissao,
	a.cd_pessoa_fisica cd_pessoa_fisica,
	to_char(null) cd_cgc 
from	pessoa_fisica b,
	titulo_pagar a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.dt_integracao_externa is null
union all
/* Conta contabil - tipo de registro 2 */
select	2 ie_tipo_registro,
	'' ie_tipo_registro_1,
	'' cd_fornecedor,
	'' ie_tipo_pessoa,
	'' nm_fornecedor,
	'' dt_nascimento,
	'' nr_cpf_cnpj,
	'' nr_identificacao,
	'' ds_endereco,
	'' ds_bairro,
	'' cd_cep,
	'' ds_cidade,
	'' sg_estado,
	'' ds_email,
	'' nr_telefone,
	'' nr_fax,
	'' ie_ativo,
	'07'ie_tipo_registro_2,
	'34' cd_empresa,
	'02' cd_filial,
	'R' cd_moeda,
	b.cd_conta_contabil,
	'' ie_tipo_registro_3,
	'' cd_grupo_pag,
	'' ie_tipo_registro_4,
	'' cd_conta,
	'' cd_banco,
	'' cd_agencia,
	'' nr_conta,
	a.dt_emissao dt_emissao,
	to_char(null) cd_pessoa_fisica,
	a.cd_cgc cd_cgc
from	pessoa_juridica b,
	titulo_pagar a
where	a.cd_cgc	= b.cd_cgc
and	b.dt_integracao_externa is null
union all
select	2 ie_tipo_registro,
	'' ie_tipo_registro_1,
	'' cd_fornecedor,
	'' ie_tipo_pessoa,
	'' nm_fornecedor,
	'' dt_nascimento,
	'' nr_cpf_cnpj,
	'' nr_identificacao,
	'' ds_endereco,
	'' ds_bairro,
	'' cd_cep,
	'' ds_cidade,
	'' sg_estado,
	'' ds_email,
	'' nr_telefone,
	'' nr_fax,
	'' ie_ativo,
	'07'ie_tipo_registro_2,
	'34' cd_empresa,
	'02' cd_filial,
	'R' cd_moeda,
	d.cd_conta_contabil,
	'' ie_tipo_registro_3,
	'' cd_grupo_pag,
	'' ie_tipo_registro_4,
	'' cd_conta,
	'' cd_banco,
	'' cd_agencia,
	'' nr_conta,
	a.dt_emissao dt_emissao,
	a.cd_pessoa_fisica cd_pessoa_fisica,
	to_char(null) cd_cgc 
from	pessoa_fisica_conta_ctb d,
	pessoa_fisica b,
	titulo_pagar a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	a.cd_pessoa_fisica	= d.cd_pessoa_fisica
and	nvl(d.ie_tipo_conta,'R')	= 'R'
and	b.dt_integracao_externa is null;
/