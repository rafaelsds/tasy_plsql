CREATE OR REPLACE VIEW AUDIO_VIDEO_NOTIFICATIONS_V AS
WITH BASE_DATA AS (
    SELECT DISTINCT
		   T.NR_SEQUENCIA,
           T.CD_SETOR_ATENDIMENTO,
           T.CD_UNIDADE_BASICA,
           T.CD_UNIDADE_COMPL,
           T.CD_ESTABELECIMENTO,
           T.CD_VIDEO_SERVER,
           T.NR_PRIORIDADE,
           T.DS_ANEXOS,
           T.NM_SERVER,
           T.DT_CRIACAO,
           T.DT_VIDEO_ASSESSMENT,
           T.NM_USUARIOS_DESTINO,
           T.DS_TITULO,
           T.CD_PROFILES_DESTINO,
           T.CD_GRUPOS_DESTINO,
           T.IE_NOTIFICACAO_PASSIVA,
           T.IE_NOTIFICACAO_CHAMADA,
           CASE
               WHEN CD_PROFILES_DESTINO IS NULL AND CD_SETORES_DESTINO IS NULL THEN (
                   SELECT SUBSTR(
                                  REGEXP_REPLACE(
                                          LISTAGG(SA.CD_SETOR_ATENDIMENTO, ';')
                                                  WITHIN GROUP ( ORDER BY SA.CD_SETOR_ATENDIMENTO ),
                                          '([^;]+)(;\1)*(;|$)', '\1\3'
                                      ), 1, 2000)
                   FROM SETOR_ATENDIMENTO SA
                   WHERE CD_ESTABELECIMENTO = T.CD_ESTABELECIMENTO
               )
               ELSE T.CD_SETORES_DESTINO
               END CD_SETORES_DESITNO
    FROM (
        SELECT VSN.NR_SEQUENCIA,
               VSN.CD_SETOR_ATENDIMENTO,
               VSN.CD_UNIDADE_BASICA        "CD_UNIDADE_BASICA",
               VSN.CD_UNIDADE_COMPL         "CD_UNIDADE_COMPL",
               SA.CD_ESTABELECIMENTO,
               VSN.CD_VIDEO_SERVER,
               CASE
                   WHEN VSN.IE_PRIORITY = 'N' THEN 3
                   WHEN VSN.IE_PRIORITY = 'U' THEN 2
                   WHEN VSN.IE_PRIORITY = 'E' THEN 1
               END                                                            "NR_PRIORIDADE",
				'[{"code":423,"method":"externalAccess","params":{"ie_read":"S"' ||
				',"nr_sequencia":'  		|| '"' || VSN.NR_SEQUENCIA      || '"' ||'}, "messageCode": 1141415},' ||
				'{"code":423,"method":"externalAccess","params":{"ie_read":"N"' ||
				',"cd_setor_atendimento":' 	|| '"' || VSN.CD_SETOR_ATENDIMENTO 	|| '"' ||
				',"cd_unidade_basica":' 	|| '"' || VSN.CD_UNIDADE_BASICA 	|| '"' ||
				',"cd_unidade_compl":'  	|| '"' || VSN.CD_UNIDADE_COMPL  	|| '"' ||
				',"cd_video_server":'   	|| '"' || VSN.CD_VIDEO_SERVER   	|| '"' ||
				',"nr_sequencia":'  		|| '"' || VSN.NR_SEQUENCIA      	|| '"' ||'}, "messageCode": 1137505}]' "DS_ANEXOS",
               VS.NM_SERVER,
               VSN.DT_NOTIFICATION                                                "DT_CRIACAO",
               VSN.DT_VIDEO_ASSESSMENT                                            "DT_VIDEO_ASSESSMENT",
               ''                                                                 "NM_USUARIOS_DESTINO",
               EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1028451, NVL(GET_ESTABLISHMENT_LOCALE(VNT.CD_ESTABELECIMENTO), 'pt_BR')) "DS_TITULO",
               REGEXP_REPLACE(
                       LISTAGG(VNT.CD_PERFIL, ';') WITHIN GROUP ( ORDER BY VNT.CD_PERFIL ),
                       '([^;]+)(;\1)*(;|$)',
                       '\1\3'
                   )                                                              "CD_PROFILES_DESTINO",
               ''                                                                 "CD_GRUPOS_DESTINO",
               REGEXP_REPLACE(
                       LISTAGG(VNT.CD_SETOR_ATENDIMENTO, ';') WITHIN GROUP ( ORDER BY VNT.CD_SETOR_ATENDIMENTO ),
                       '([^;]+)(;\1)*(;|$)',
                       '\1\3'
                   )                                                              "CD_SETORES_DESTINO",
               'S'                                                                IE_NOTIFICACAO_PASSIVA,
               'S'                                                                IE_NOTIFICACAO_CHAMADA
        FROM VIDEO_SERVER_NOTIFICATION VSN
            LEFT JOIN VIDEO_SERVER VS ON VSN.CD_VIDEO_SERVER = VS.NR_SEQUENCIA
            LEFT JOIN VIDEO_NOTIFICATION_TARGET VNT ON VSN.NR_SEQUENCIA = VNT.NR_SEQ_SERVER_NOT
            LEFT JOIN SETOR_ATENDIMENTO SA ON VSN.CD_SETOR_ATENDIMENTO = SA.CD_SETOR_ATENDIMENTO

        WHERE VSN.DT_NOTIFICATION >= SYSDATE - 1       
        GROUP BY VS.NM_SERVER,
                 VSN.CD_VIDEO_SERVER,
                 VNT.CD_ESTABELECIMENTO,
                 SA.CD_ESTABELECIMENTO,
                 VSN.CD_SETOR_ATENDIMENTO,
                 VSN.CD_UNIDADE_BASICA,
                 VSN.DT_NOTIFICATION,
                 VSN.DT_VIDEO_ASSESSMENT,
                 VSN.CD_UNIDADE_COMPL,
                 VSN.IE_PRIORITY,
				         VSN.NR_SEQUENCIA,
                 NVL2(VNT.CD_PERFIL, 'NOT NULL', 'NULL')

    ) T
),
     SPECIFIC_PATIENT AS (
         SELECT BD.DS_TITULO,
                CASE
                    WHEN PF.NM_PESSOA_FISICA IS NULL THEN
                        REPLACE(REPLACE(REPLACE(REPLACE(
                                                        EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029990, NVL(GET_ESTABLISHMENT_LOCALE(BD.CD_ESTABELECIMENTO), 'pt_BR')),
                                                        '#@nm_estabelecimento#@', EST.NM_FANTASIA_ESTAB),
                                                '#@nm_setor#@', TRIM(SA.DS_SETOR_ATENDIMENTO)),
                                        '#@cd_unidade_basica#@', TRIM(BD.CD_UNIDADE_BASICA)),
                                '#@cd_unidade_compl#@', TRIM(BD.CD_UNIDADE_COMPL))
                    ELSE
                            REPLACE(REPLACE(REPLACE(REPLACE(
                                                            EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029990, NVL(GET_ESTABLISHMENT_LOCALE(BD.CD_ESTABELECIMENTO), 'pt_BR')),
                                                            '#@nm_estabelecimento#@', EST.NM_FANTASIA_ESTAB),
                                                    '#@nm_setor#@', TRIM(SA.DS_SETOR_ATENDIMENTO)),
                                            '#@cd_unidade_basica#@', TRIM(BD.CD_UNIDADE_BASICA)),
                                    '#@cd_unidade_compl#@', TRIM(BD.CD_UNIDADE_COMPL))
                            || ' ' || REPLACE(EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029992,
                                                                                 NVL(GET_ESTABLISHMENT_LOCALE(EST.CD_ESTABELECIMENTO), 'pt_BR')),
                                              '#@nm_pessoa_fisica#@', PF.NM_PESSOA_FISICA)
                    END "DS_CONTEUDO",
                BD.DT_CRIACAO,
                BD.DT_VIDEO_ASSESSMENT,
                BD.NM_USUARIOS_DESTINO,
                BD.CD_PROFILES_DESTINO,
                BD.CD_GRUPOS_DESTINO,
                BD.CD_SETORES_DESITNO,
                BD.IE_NOTIFICACAO_PASSIVA,
                BD.IE_NOTIFICACAO_CHAMADA,
                BD.DS_ANEXOS,
                BD.NR_PRIORIDADE,
				BD.NR_SEQUENCIA
         FROM BASE_DATA BD
             INNER JOIN ESTABELECIMENTO EST ON BD.CD_ESTABELECIMENTO = EST.CD_ESTABELECIMENTO
             INNER JOIN SETOR_ATENDIMENTO SA ON BD.CD_SETOR_ATENDIMENTO = SA.CD_SETOR_ATENDIMENTO
             LEFT JOIN UNIDADE_ATENDIMENTO UA ON SA.CD_SETOR_ATENDIMENTO = UA.CD_SETOR_ATENDIMENTO
             AND BD.CD_UNIDADE_BASICA = UA.CD_UNIDADE_BASICA
             AND BD.CD_UNIDADE_COMPL = UA.CD_UNIDADE_COMPL
             LEFT JOIN ATENDIMENTO_PACIENTE AP ON UA.NR_ATENDIMENTO = AP.NR_ATENDIMENTO
             LEFT JOIN PESSOA_FISICA PF ON AP.CD_PESSOA_FISICA = PF.CD_PESSOA_FISICA
         WHERE BD.CD_VIDEO_SERVER IS NULL
     ),
     TREATMENT_UNIT AS (
         SELECT BD.DS_TITULO,
                CASE
                    WHEN PF.NM_PESSOA_FISICA IS NULL THEN
                        REPLACE(REPLACE(REPLACE(REPLACE(
                                                        EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029990, NVL(GET_ESTABLISHMENT_LOCALE(BD.CD_ESTABELECIMENTO), 'pt_BR')),
                                                        '#@nm_estabelecimento#@', EST.NM_FANTASIA_ESTAB),
                                                '#@nm_setor#@', TRIM(SA.DS_SETOR_ATENDIMENTO)),
                                        '#@cd_unidade_basica#@', TRIM(BD.CD_UNIDADE_BASICA)),
                                '#@cd_unidade_compl#@', TRIM(BD.CD_UNIDADE_COMPL))
                    ELSE
                            REPLACE(REPLACE(REPLACE(REPLACE(
                                                            EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029990, NVL(GET_ESTABLISHMENT_LOCALE(BD.CD_ESTABELECIMENTO), 'pt_BR')),
                                                            '#@nm_estabelecimento#@', EST.NM_FANTASIA_ESTAB),
                                                    '#@nm_setor#@', TRIM(SA.DS_SETOR_ATENDIMENTO)),
                                            '#@cd_unidade_basica#@', TRIM(BD.CD_UNIDADE_BASICA)),
                                    '#@cd_unidade_compl#@', TRIM(BD.CD_UNIDADE_COMPL))
                            || ' ' || REPLACE(EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029992,
                                                                                 NVL(GET_ESTABLISHMENT_LOCALE(BD.CD_ESTABELECIMENTO), 'pt_BR')),
                                              '#@nm_pessoa_fisica#@', PF.NM_PESSOA_FISICA)
                    END "DS_CONTEUDO",
                BD.DT_CRIACAO,
                BD.DT_VIDEO_ASSESSMENT,
                BD.NM_USUARIOS_DESTINO,
                BD.CD_PROFILES_DESTINO,
                BD.CD_GRUPOS_DESTINO,
                BD.CD_SETORES_DESITNO,
                BD.IE_NOTIFICACAO_PASSIVA,
                BD.IE_NOTIFICACAO_CHAMADA,
                BD.DS_ANEXOS,
                BD.NR_PRIORIDADE,
				BD.NR_SEQUENCIA
         FROM BASE_DATA BD
             INNER JOIN ESTABELECIMENTO EST ON BD.CD_ESTABELECIMENTO = EST.CD_ESTABELECIMENTO
             INNER JOIN VIDEO_SERVER_UNIDADE_ATEND VA ON BD.CD_VIDEO_SERVER = VA.CD_VIDEO_SERVER
             INNER JOIN SETOR_ATENDIMENTO SA ON VA.CD_SETOR_ATENDIMENTO = SA.CD_SETOR_ATENDIMENTO
             INNER JOIN UNIDADE_ATENDIMENTO UA ON VA.CD_SETOR_ATENDIMENTO = UA.CD_SETOR_ATENDIMENTO
             AND VA.CD_UNIDADE_BASICA = UA.CD_UNIDADE_BASICA
             AND VA.CD_UNIDADE_COMPL = UA.CD_UNIDADE_COMPL
             LEFT JOIN ATENDIMENTO_PACIENTE AP ON UA.NR_ATENDIMENTO = AP.NR_ATENDIMENTO
             LEFT JOIN PESSOA_FISICA PF ON AP.CD_PESSOA_FISICA = PF.CD_PESSOA_FISICA
         WHERE BD.CD_VIDEO_SERVER IS NOT NULL
           AND BD.CD_UNIDADE_BASICA IS NOT NULL
     ),
     MULTIPLE_PATIENT AS (
         SELECT BD.DS_TITULO,
                REPLACE(
                        EXPRESSAO_PCK.OBTER_DESC_EXPRESSAO(1029994, NVL(GET_ESTABLISHMENT_LOCALE(BD.CD_ESTABELECIMENTO), 'pt_BR')),
                        '#@nm_servidor#@', BD.NM_SERVER) "DS_CONTEUDO",
                BD.DT_CRIACAO,
                BD.DT_VIDEO_ASSESSMENT,
                BD.NM_USUARIOS_DESTINO,
                BD.CD_PROFILES_DESTINO,
                BD.CD_GRUPOS_DESTINO,
                BD.CD_SETORES_DESITNO,
                BD.IE_NOTIFICACAO_PASSIVA,
                BD.IE_NOTIFICACAO_CHAMADA,
                BD.DS_ANEXOS,
                BD.NR_PRIORIDADE,
				BD.NR_SEQUENCIA
         FROM BASE_DATA BD
         WHERE BD.CD_VIDEO_SERVER IS NOT NULL
               AND BD.CD_UNIDADE_BASICA IS NULL
     )
SELECT "DS_TITULO",
       "DS_CONTEUDO",
       "DT_CRIACAO",
       "NM_USUARIOS_DESTINO",
       "CD_PROFILES_DESTINO",
       "CD_GRUPOS_DESTINO",
       "CD_SETORES_DESITNO",
       "IE_NOTIFICACAO_PASSIVA",
       "IE_NOTIFICACAO_CHAMADA",
       "DS_ANEXOS",
       "NR_PRIORIDADE",
	     "NR_SEQUENCIA"
FROM TREATMENT_UNIT
UNION ALL
SELECT "DS_TITULO",
       "DS_CONTEUDO",
       "DT_CRIACAO",
       "NM_USUARIOS_DESTINO",
       "CD_PROFILES_DESTINO",
       "CD_GRUPOS_DESTINO",
       "CD_SETORES_DESITNO",
       "IE_NOTIFICACAO_PASSIVA",
       "IE_NOTIFICACAO_CHAMADA",
       "DS_ANEXOS",
       "NR_PRIORIDADE",
	     "NR_SEQUENCIA"
FROM MULTIPLE_PATIENT
UNION ALL
SELECT DISTINCT "DS_TITULO",
       "DS_CONTEUDO",
       "DT_CRIACAO",
       "NM_USUARIOS_DESTINO",
       "CD_PROFILES_DESTINO",
       "CD_GRUPOS_DESTINO",
       "CD_SETORES_DESITNO",
       "IE_NOTIFICACAO_PASSIVA",
       "IE_NOTIFICACAO_CHAMADA",
       "DS_ANEXOS",
       "NR_PRIORIDADE",
	"NR_SEQUENCIA"
FROM SPECIFIC_PATIENT;
/
