create or replace
view nut_atend_serv_v as 
select	a.*,
		substr(obter_desc_nut_servico(a.nr_seq_servico),1,50) ds_servico,
		b.nr_seq_motivo,
		substr(obter_desc_motivo_inconsist(b.nr_seq_motivo),1,255) ds_motivo_inconsist
from	nut_atend_serv_dia a,
		nut_atend_serv_inconsist b
where	b.nr_seq_serv_dia(+) = a.nr_sequencia;
/
