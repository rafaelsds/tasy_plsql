create or replace view pls_mens_relat_385_v
as
select	trunc(d.dt_referencia, 'month') dt_mes_referencia,
	sum(a.vl_item) vl_mensalidade,
	decode(a.ie_tipo_item, '3', sum(a.vl_item), 0) vl_coparticipacao,
	decode(a.ie_tipo_item, '3', 0, sum(decode(e.dt_contabilizacao, null, a.vl_item, a.vl_pro_rata_dia))) vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_tipo_lanc_adic		f,
	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e
where	f.nr_sequencia(+)	= a.nr_seq_tipo_lanc
and	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	((a.ie_tipo_item not in ('20','11')) or (a.ie_tipo_item = '20' and f.ie_considera_receita = 'S'))
and	nvl(e.ie_mensalidade_mes_anterior,'N')	= 'N'
group by	trunc(d.dt_referencia,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL
select	trunc(a.dt_antecipacao,'month') dt_mes_referencia,
	sum(a.vl_antecipacao) vl_mensalidade,
	0 vl_coparticipacao,
	sum(a.vl_antecipacao) vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_tipo_lanc_adic		f,
	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e
where	f.nr_sequencia(+)	= a.nr_seq_tipo_lanc
and	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	e.ie_status = '2'
and	e.dt_contabilizacao is not null
and	((a.ie_tipo_item not in ('20','11')) or (a.ie_tipo_item = '20' and f.ie_considera_receita = 'S'))
and	nvl(e.ie_mensalidade_mes_anterior,'N')	= 'N'
group by	trunc(a.dt_antecipacao,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL
select	trunc(b.dt_mesano_referencia,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,
	sum(decode(e.dt_contabilizacao, null, a.vl_antecipacao,0)) vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e,
	pls_tipo_lanc_adic		f
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	f.nr_sequencia(+)	= a.nr_seq_tipo_lanc
and	e.ie_status = '2'
and	((a.ie_tipo_item not in ('20','11','3')) or (a.ie_tipo_item = '20' and f.ie_considera_receita = 'S'))
and	nvl(e.ie_mensalidade_mes_anterior,'N') = 'N'
group by	trunc(b.dt_mesano_referencia,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL
select	trunc(e.dt_contabilizacao,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	sum(a.vl_item) vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	e.ie_status = '2'
and	a.ie_tipo_item <> '11'
and	nvl(e.ie_mensalidade_mes_anterior,'N')	= 'N'
group by	trunc(e.dt_contabilizacao,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL
select	trunc(d.dt_mesano_referencia,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	sum(a.vl_tributo) vl_imposto,
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	null ie_tipo_vinculo_operadora,
	c.ie_cancelamento,
	null ie_tipo_item,
	d.dt_geracao_nf
from	nota_fiscal_trib	a,
	nota_fiscal		b,
	pls_mensalidade		c,
	pls_lote_mensalidade	d
where	b.nr_sequencia		= a.nr_sequencia
and	c.nr_sequencia		= b.nr_seq_mensalidade
and	d.nr_sequencia		= c.nr_seq_lote
and	d.ie_status = '2'
and	nvl(d.ie_mensalidade_mes_anterior,'N') = 'N'
group by	trunc(d.dt_mesano_referencia,'month'),
		c.ie_cancelamento,
		d.dt_geracao_nf
UNION ALL
select	trunc(d.dt_referencia,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	0 vl_antecipacao,
	sum(a.vl_item) vl_cancelado,
	0 vl_imposto,
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_tipo_lanc_adic		f,
	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	f.nr_sequencia(+)	= a.nr_seq_tipo_lanc
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	e.ie_status = '2'
and	d.ie_cancelamento = 'C'
and	e.dt_contabilizacao is null
and	nvl(e.ie_mensalidade_mes_anterior,'N') = 'N'
and	((a.ie_tipo_item <> '20') or (a.ie_tipo_item = '20' and f.ie_considera_receita = 'S'))
group by	trunc(d.dt_referencia,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL
select	trunc(e.dt_mesano_referencia,'month') dt_mes_referencia,
	sum(a.vl_item) vl_mensalidade,
	decode(a.ie_tipo_item,'3',sum(a.vl_item),0) vl_coparticipacao,
	decode(a.ie_tipo_item,'3',0,sum(a.vl_item)) vl_liq_sem_copartic,
	0 vl_antecipacao,
	decode(d.ie_cancelamento,'C',sum(a.vl_item),0),
	0 vl_imposto,
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	decode(d.ie_cancelamento,null,sum(decode(a.ie_tipo_item,'20',a.vl_item,0)),0) vl_manual,
	decode(d.ie_cancelamento,null,sum(decode(a.ie_tipo_item,'11',a.vl_item,0)),0) vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	e.ie_status = '2'
and	nvl(e.ie_mensalidade_mes_anterior,'N') = 'S'
group by	trunc(e.dt_mesano_referencia,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL	
select	trunc(a.dt_antecipacao,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,	
	0 vl_antec_pro_rata,
	sum(decode(e.dt_contabilizacao,null,a.vl_antecipacao,0)) vl_antecipacao_rata_ant,
	0 vl_manual,
	0 vl_carteirinha,
	c.ie_tipo_vinculo_operadora,
	d.ie_cancelamento,
	a.ie_tipo_item,
	e.dt_geracao_nf
from	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_segurado			c,
	pls_mensalidade			d,
	pls_lote_mensalidade		e,
	pls_tipo_lanc_adic		f
where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
and	c.nr_sequencia		= b.nr_seq_segurado
and	d.nr_sequencia		= b.nr_seq_mensalidade
and	e.nr_sequencia		= d.nr_seq_lote
and	f.nr_sequencia(+)	= a.nr_seq_tipo_lanc
and	e.ie_status = '2'
and	d.ie_cancelamento is null
and	nvl(e.ie_mensalidade_mes_anterior,'N') = 'N'
and	((a.ie_tipo_item not in ('20','11')) or (a.ie_tipo_item = '20' and f.ie_considera_receita = 'S'))
group by	trunc(a.dt_antecipacao,'month'),
		a.ie_tipo_item,
		c.ie_tipo_vinculo_operadora,
		d.ie_cancelamento,
		e.dt_geracao_nf
UNION ALL
select	trunc(c.dt_mesano_referencia,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,	
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	decode(nvl(e.ie_considera_despesa,'N'),'S',abs(d.vl_item),d.vl_item) vl_manual,
	0 vl_carteirinha,
	f.ie_tipo_vinculo_operadora,
	b.ie_cancelamento,
	d.ie_tipo_item,
	a.dt_geracao_nf
from	pls_mensalidade_seg_item 	d,
	pls_tipo_lanc_adic		e,
	pls_mensalidade_segurado 	c,
	pls_segurado			f,
	pls_mensalidade			b,
	pls_lote_mensalidade		a
where	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	d.nr_seq_tipo_lanc	= e.nr_sequencia
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	f.nr_sequencia	= c.nr_seq_segurado
and	a.nr_sequencia	= b.nr_seq_lote
and	d.ie_tipo_item	= '20'
and	e.ie_considera_receita = 'N'
and	((b.ie_cancelamento is null) or (nvl(e.ie_considera_despesa,'N') = 'S'))
and	a.ie_status	= '2'
and	nvl(a.ie_mensalidade_mes_anterior,'N')	= 'N'
UNION ALL
select	trunc(c.dt_mesano_referencia,'month') dt_mes_referencia,
	0 vl_mensalidade,
	0 vl_coparticipacao,
	0 vl_liq_sem_copartic,
	0 vl_antecipacao,
	0 vl_cancelado,
	0 vl_imposto,	
	0 vl_antec_pro_rata,
	0 vl_antecipacao_rata_ant,
	0 vl_manual,
	sum(d.vl_item) vl_carteirinha,
	e.ie_tipo_vinculo_operadora,
	b.ie_cancelamento,
	d.ie_tipo_item,
	a.dt_geracao_nf
from	pls_mensalidade_seg_item d,
	pls_mensalidade_segurado c,
	pls_segurado		e,
	pls_mensalidade		b,
	pls_lote_mensalidade	a
where	c.nr_sequencia	= d.nr_seq_mensalidade_seg
and	e.nr_sequencia	= c.nr_seq_segurado
and	b.nr_sequencia	= c.nr_seq_mensalidade
and	a.nr_sequencia	= b.nr_seq_lote
and	d.ie_tipo_item	= '11'
and	b.ie_cancelamento is null
and	a.ie_status	= '2'
and	nvl(a.ie_mensalidade_mes_anterior,'N')	= 'N'
group by	trunc(c.dt_mesano_referencia,'month'),
		d.ie_tipo_item,
		e.ie_tipo_vinculo_operadora,
		b.ie_cancelamento,
		a.dt_geracao_nf;
/