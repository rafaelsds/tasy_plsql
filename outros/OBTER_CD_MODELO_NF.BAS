create or replace
function obter_cd_modelo_nf(	nr_sequencia_p		number)
 		    	return varchar2 is

cd_modelo_nf_w		varchar2(80);			
			
begin

select	cd_modelo_nf
into	cd_modelo_nf_w
from	modelo_nota_fiscal
where	nr_sequencia = nr_sequencia_p;

return	cd_modelo_nf_w;

end obter_cd_modelo_nf;
/
