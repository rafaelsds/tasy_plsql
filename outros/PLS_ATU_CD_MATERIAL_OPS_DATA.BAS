create or replace
procedure pls_atu_cd_material_ops_data is 

tb_sequencia_w		pls_util_cta_pck.t_number_table;
nr_seq_ultimo_reg_w	number(15);
nr_seq_inicial_w	number(15);
nr_seq_final_w		number(15);

cursor c01(	nr_seq_inicial_pc	number,
		nr_seq_final_pc		number) is
	select	nr_sequencia
	from	pls_material
	where	nr_sequencia between nr_seq_inicial_pc and nr_seq_final_pc;
begin

pls_util_cta_pck.ie_grava_log_w := 'N';
wheb_usuario_pck.set_ie_executar_trigger('N');
wheb_usuario_pck.set_nm_usuario('tasy');

select	max(nr_sequencia),
	min(nr_sequencia)
into	nr_seq_ultimo_reg_w,
	nr_seq_inicial_w
from	pls_material;

nr_seq_final_w := nr_seq_inicial_w + 500;

-- menor igual a um number de 10
while (nr_seq_final_w <= nr_seq_ultimo_reg_w or nr_seq_inicial_w <= nr_seq_ultimo_reg_w) loop

	-- Abrimos o cursor
	open c01(nr_seq_inicial_w, nr_seq_final_w);

	-- abrimos um loop para iterar enquanto ouverem registro do cursor.
	loop

	-- Limpamos os registros das listas para que n�o seja processado o mesmo registro mais de uma vez.
	tb_sequencia_w.delete;

	-- Executamos o fetch de um n�mero determinado de linhas do cursor nas listas definidas, uma para cada campo da consulta.
	fetch c01 bulk collect into tb_sequencia_w limit 2000;

	-- Quando o cursor n�o tiver mais linhas ent�o sai do loop.
	exit when tb_sequencia_w.count = 0;

	-- executamos as opera��es necess�rias.
	forall	i in tb_sequencia_w.first..tb_sequencia_w.last 
		-- faz disparar a trigger respons�vel por resolver tudo
		update 	pls_material set 
			cd_material_ops_number = pls_util_pck.converte_para_numero(cd_material_ops,null),
			dt_referencia = nvl(dt_limite_utilizacao, dt_exclusao)
		where	nr_sequencia = tb_sequencia_w(i);
	commit;
	
	end loop;
	
	-- Ao terminar fechamos o cursor.
	close c01;
	
	nr_seq_inicial_w := nr_seq_final_w + 1;
	nr_seq_final_w := nr_seq_inicial_w + 500;
end loop;

pls_util_cta_pck.ie_grava_log_w := 'S';

end pls_atu_cd_material_ops_data;
/
