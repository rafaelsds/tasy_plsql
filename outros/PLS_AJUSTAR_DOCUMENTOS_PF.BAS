create or replace
procedure pls_ajustar_documentos_pf
			( cd_estabelecimento_p	number) is


nr_seq_tipo_documento_w		number(10);
cd_pessoa_fisica_w		varchar2(20);
nr_seq_tipo_doc_w		number(10); --sequ�ncia da tabela pls_tipo_documento_pf
dt_atualizao_w			date;
dt_atualizao_nrec_w		date;
nm_usuario_w			varchar2(30);
nm_usuario_nrec_w		varchar2(15);
nr_seq_documento_w		number(10);
nr_seq_doc_tipo_w		number(10);
cd_estabelecimento_w		number(4);

Cursor C01 is
	select	a.nr_seq_tipo_documento,
		a.cd_pessoa_fisica,
		a.dt_atualizacao,
		a.nm_usuario,
		nvl(a.dt_atualizacao_nrec,sysdate),
		a.nm_usuario_nrec,
		a.nr_sequencia
	from	pls_documento_pf	a,
		pls_segurado		b
	where	a.cd_pessoa_fisica is not null
	and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_seq_tipo_doc_pf is null
	and	a.nr_seq_tipo_documento is not null;
		
begin

open C01;
loop
fetch C01 into	
	nr_seq_tipo_documento_w,
	cd_pessoa_fisica_w,
	dt_atualizao_w,
	nm_usuario_w,
	dt_atualizao_nrec_w,
	nm_usuario_nrec_w,
	nr_seq_documento_w;
exit when C01%notfound;
	begin
	
	select	max(nr_sequencia)
	into	nr_seq_doc_tipo_w
	from	pls_tipo_documento_pf
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	nr_seq_tipo_documento	= nr_seq_tipo_documento_w;
	
	if	(nvl(nr_seq_doc_tipo_w,0) = 0) then
		
		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		select	pls_tipo_documento_pf_seq.nextval
		into	nr_seq_tipo_doc_w
		from	dual;
		
		insert into pls_tipo_documento_pf	
			(	nr_sequencia, cd_estabelecimento , dt_atualizacao , 
				nm_usuario, dt_atualizacao_nrec , nm_usuario_nrec,
				nr_seq_tipo_documento, dt_registro , nm_usuario_registro,
				cd_pessoa_fisica	)
		values	(	nr_seq_tipo_doc_w, nvl(cd_estabelecimento_w,cd_estabelecimento_p) , dt_atualizao_w, 
				nm_usuario_w , dt_atualizao_nrec_w, nm_usuario_nrec_w,
				nr_seq_tipo_documento_w , dt_atualizao_w , nm_usuario_w,
				cd_pessoa_fisica_w	);
			
		update	pls_documento_pf
		set	nr_seq_tipo_doc_pf	= nr_seq_tipo_doc_w
		where	nr_sequencia		= nr_seq_documento_w;
	else
		update	pls_documento_pf
		set	nr_seq_tipo_doc_pf	= nr_seq_doc_tipo_w
		where	nr_sequencia		= nr_seq_documento_w;
		
	end if;
			
	end;
end loop;
close C01;

commit;

end pls_ajustar_documentos_pf;
/
