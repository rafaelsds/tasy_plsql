create or replace
procedure pls_ajustar_parcela_sca_mens is 

Cursor C01 is
	select	a.nr_sequencia nr_seq_mens_item,
		nvl(a.nr_parcela_sca, 0) nr_parcela_sca,
		trunc(months_between(b.dt_mesano_referencia, trunc(d.dt_inicio_vigencia, 'month'))) + 1 nr_parcela_sca_correta,
		b.nr_seq_segurado,
		d.dt_inicio_vigencia,
		b.dt_mesano_referencia
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c,
		pls_sca_vinculo d
	where	c.nr_sequencia = b.nr_seq_mensalidade
	and	b.nr_sequencia = a.nr_seq_mensalidade_seg
	and	d.nr_sequencia = a.nr_seq_vinculo_sca
	and	c.ie_cancelamento is null
	and	d.dt_inicio_vigencia is not null
	and	a.ie_tipo_item = '15';

begin

for r_c01_w in c01 loop
	begin
	
	if	(r_c01_w.nr_parcela_sca_correta <> r_c01_w.nr_parcela_sca) and
		(r_c01_w.nr_parcela_sca_correta > 0) then
		update	pls_mensalidade_seg_item
		set	nr_parcela_sca	= r_c01_w.nr_parcela_sca_correta
		where	nr_sequencia	= r_c01_w.nr_seq_mens_item;
	end if;
	
	end;
end loop;

commit;

end pls_ajustar_parcela_sca_mens;
/