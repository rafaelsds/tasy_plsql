create or replace
procedure pls_alterar_vias is


nr_via_w	number(10);
nr_seq_cartao_w	number(10);

Cursor C01 is
	select	a.nr_via_solicitacao,
		a.nr_sequencia
	from	pls_segurado_carteira		a,
		pls_segurado_cart_estagio	b
	where	b.nr_seq_cartao_seg	= a.nr_sequencia;

begin

open C01;
loop
fetch C01 into	
	nr_via_w,
	nr_seq_cartao_w;
exit when C01%notfound;
	begin
	update pls_segurado_cart_estagio
	set	nr_via	= nr_via_w
	where	nr_seq_cartao_seg	= nr_seq_cartao_w;
	end;
end loop;
close C01;

commit;

end pls_alterar_vias;
/
