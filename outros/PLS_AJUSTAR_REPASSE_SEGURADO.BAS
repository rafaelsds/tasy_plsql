create or replace
procedure pls_ajustar_repasse_segurado is


nr_seq_segurado_w	number(10);
ie_tipo_repasse_w	varchar2(1);
ie_preco_w		varchar2(2);

Cursor C01 is
	select	nr_seq_segurado
	from	pls_segurado_repasse
	where	ie_tipo_repasse is null;

begin

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	
	select	max(b.ie_preco)
	into	ie_preco_w
	from	pls_segurado	a,
		pls_plano	b
	where	a.nr_seq_plano	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_segurado_w;
	
	if	(ie_preco_w = 1) then
		ie_tipo_repasse_w	:= 'P';
	else	
		ie_tipo_repasse_w	:= 'C';
	end if;

	update	pls_segurado_repasse
	set	ie_tipo_repasse	= ie_tipo_repasse_w
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	end;
end loop;
close C01;

end pls_ajustar_repasse_segurado;
/
