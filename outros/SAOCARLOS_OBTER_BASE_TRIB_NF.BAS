create or replace
function saocarlos_obter_base_trib_nf(nr_sequencia_p number,
					nr_sequencia_nf_p number)
					return number is
vl_base_trib_item_w number(13,2);

begin
select 	nvl(max(vl_base_calculo),0)
into	vl_base_trib_item_w
from	nota_fiscal_item_trib
where 	nr_sequencia = nr_sequencia_p
and	nr_sequencia_nf = nvl(nr_sequencia_nf_p, nr_sequencia_nf);

return vl_base_trib_item_w;

end saocarlos_obter_base_trib_nf;
/