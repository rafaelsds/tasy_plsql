create or replace
procedure pls_ajustar_fat_proc_pos_estab is 

nr_seq_conta_pos_w		number(15);
nr_seq_conta_proc_w		number(15);
nr_seq_fat_proc_w		number(15);
nr_seq_conta_mat_w		number(15);
nr_seq_fat_mat_w		number(15);


Cursor 	C01 is
	select	a.nr_seq_conta_proc,
		a.nr_sequencia
	from	pls_fatura_proc a;

Cursor 	C02 is
	select	a.nr_seq_conta_mat,
		a.nr_sequencia
	from	pls_fatura_mat a;
	
begin
open C01;
loop
fetch C01 into	
	nr_seq_conta_proc_w,
	nr_seq_fat_proc_w;
exit when C01%notfound;
	begin	
	select	max(a.nr_sequencia)
	into	nr_seq_conta_pos_w
	from	pls_conta_pos_estabelecido a
	where	a.nr_seq_conta_proc 	= nr_seq_conta_proc_w
	and	((a.ie_situacao		= 'A') or (a.ie_situacao	is null));
	
	update	pls_fatura_proc
	set	nr_seq_conta_pos_estab 	= nr_seq_conta_pos_w
	where	nr_sequencia		= nr_seq_fat_proc_w;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_conta_mat_w,
	nr_seq_fat_mat_w;
exit when C02%notfound;
	begin
	select	max(a.nr_sequencia)
	into	nr_seq_conta_pos_w
	from	pls_conta_pos_estabelecido a
	where	a.nr_seq_conta_mat 	= nr_seq_conta_mat_w
	and	((a.ie_situacao		= 'A') or (a.ie_situacao	is null));
	
	update	pls_fatura_mat
	set	nr_seq_conta_pos_estab 	= nr_seq_conta_pos_w
	where	nr_sequencia		= nr_seq_fat_mat_w;
	end;
end loop;
close C02;

commit;

end pls_ajustar_fat_proc_pos_estab;
/