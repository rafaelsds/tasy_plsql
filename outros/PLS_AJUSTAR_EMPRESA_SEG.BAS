create or replace
procedure pls_ajustar_empresa_seg is

nr_seq_segurado_w		number(10);
nr_seq_contr_inter_w		number(10);
ie_tipo_vinculo_w		varchar2(1);
cd_operadora_empresa_w		varchar2(10);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_contrato,
		'C'
	from	pls_segurado
	where	cd_operadora_empresa is null
	and	nr_seq_contrato is not null
	union
	select	nr_sequencia,
		nr_seq_intercambio,
		'I'
	from	pls_segurado
	where	cd_operadora_empresa is null
	and	nr_seq_intercambio is not null;


begin

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w,
	nr_seq_contr_inter_w,
	ie_tipo_vinculo_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_vinculo_w = 'C') then
		begin
		select	max(cd_operadora_empresa)
		into	cd_operadora_empresa_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contr_inter_w;
		exception
		when others then
			cd_operadora_empresa_w	:= null;
		end;
	elsif	(ie_tipo_vinculo_w = 'I') then
		begin
		select	max(cd_operadora_empresa)
		into	cd_operadora_empresa_w
		from	pls_intercambio
		where	nr_sequencia	= nr_seq_contr_inter_w;
		exception
		when others then
			cd_operadora_empresa_w	:= null;
		end;
	end if;	
	
	if	(cd_operadora_empresa_w is not null) then
		update	pls_segurado
		set	cd_operadora_empresa	= cd_operadora_empresa_w
		where	nr_sequencia		= nr_seq_segurado_w;
	end if;
	end;
end loop;
close C01;

commit;

end pls_ajustar_empresa_seg;
/
