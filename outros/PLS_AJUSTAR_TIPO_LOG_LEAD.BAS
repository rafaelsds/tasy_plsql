create or replace
procedure pls_ajustar_tipo_log_lead
			(	cd_estabelecimento_p	number) is 
			
nr_seq_solicitacao_w		number(10);
nr_seq_tipo_logradouro_w	number(10);
cd_tipo_logradouro_w		number(10);
cd_tipo_logradouro_sus_w	varchar2(10);
ds_tipo_logradouro_w		varchar2(80);

Cursor C01 is
	select	nr_sequencia
	from	pls_solicitacao_comercial
	where	cd_estabelecimento	=  cd_estabelecimento_p
	and	nr_seq_tipo_logradouro is not null;


begin

open C01;
loop
fetch C01 into	
	nr_seq_solicitacao_w;
exit when C01%notfound;
	begin
	
	select	nr_seq_tipo_logradouro
	into	nr_seq_tipo_logradouro_w
	from	pls_solicitacao_comercial
	where	nr_sequencia	= nr_seq_solicitacao_w;
	
	select	cd_tipo_logradouro,
		ds_tipo_logradouro
	into	cd_tipo_logradouro_w,
		ds_tipo_logradouro_w
	from	cns_tipo_logradouro
	where	nr_sequencia	= nr_seq_tipo_logradouro_w;
	
	begin
	select	max(cd_tipo_logradouro)
	into	cd_tipo_logradouro_sus_w
	from	sus_tipo_logradouro
	where	to_number(cd_tipo_logradouro)	= nr_seq_tipo_logradouro_w;
	exception
	when others then
		cd_tipo_logradouro_sus_w	:= null;
	end;
	
	if	(cd_tipo_logradouro_sus_w is null) then
		begin
		select	max(cd_tipo_logradouro)
		into	cd_tipo_logradouro_sus_w
		from	sus_tipo_logradouro
		where	upper(ds_tipo_logradouro)	= upper(ds_tipo_logradouro_w);
		exception
		when others then
			cd_tipo_logradouro_sus_w	:= null;
		end;
	end if;
	
	if	(cd_tipo_logradouro_sus_w is not null) then
		update	pls_solicitacao_comercial
		set	CD_TIPO_LOGRADOURO	= cd_tipo_logradouro_sus_w
		where	nr_sequencia		= nr_seq_solicitacao_w;
	end if;
	
	end;
end loop;
close C01;

commit;

end pls_ajustar_tipo_log_lead;
/
