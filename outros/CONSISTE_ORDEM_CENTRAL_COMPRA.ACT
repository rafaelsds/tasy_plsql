create or replace
procedure consiste_ordem_central_compra(	nr_ordem_p		in	varchar2,
					nm_usuario_p		in	varchar2,
					ds_erro_p			out	varchar2) is

nr_item_oci_w				number(5);
qt_material_w				number(13,4);
qt_material_entrega_w			number(13,4);
ds_erro_w				varchar2(2000);
ds_erro_ww				varchar2(2000) := ' ';
cd_item_erro_w				varchar(255) := '';
qt_item_compra_w				number(3);
vl_total_ordem_w				number(15,2);
vl_total_venc_w				number(15,2);
cd_material_w				number(6);
ds_material_w				varchar2(255);
cd_centro_custo_w				number(08,0);
cd_local_estoque_w			number(04,0);
ie_aprovacao_ext_w			varchar2(255);
qt_inativo_w				number(05,0);
qt_condicao_inativo_w			number(3);
qt_reg_w					number(10);
cd_conta_contabil_w			varchar2(20);
vl_minimo_nf_w				number(15,2);
qt_validade_vencida_w			number(5);
cd_estabelecimento_w			number(4);
ie_consiste_validade_anvisa_w		varchar2(1);
ie_consiste_valid_marca_mat_w		varchar2(1);
ds_erro_validade_w				varchar2(2000);
qt_dia_entrega_urgente_w			number(05,0);
qt_entrega_w				number(05,0);
nr_seq_marca_w				number(10);
dt_validade_anvisa_marca_w			date;
nr_seq_subgrupo_compra_w			number(10);
ie_liberado_w				varchar2(1);
ie_consiste_regra_w			varchar2(1) := 'N';
ie_consiste_revisao_w			varchar2(15);
ie_consiste_fornec_adiant_w			varchar2(15);
ie_marcar_urgente_oc_w			varchar2(1);
ie_forma_revisao_w				varchar2(15);
nr_seq_licitacao_w				number(10);
nr_seq_reg_lic_item_w			number(10);

cd_cgc_fornecedor_w			varchar2(14);
cd_centro_custo_ordem_w			number(8);
cd_local_entrega_ordem_w			number(4);
ie_tipo_local_w				varchar2(5);
ie_tipo_ordem_w				varchar2(15);
qt_existe_regra_cc_usuario_w		number(4);
nr_seq_reg_preco_w			number(10);
ie_vigente_w				varchar2(1);
nr_solic_compra_w				number(10);
qt_existe_w				number(10);
qt_forn_adiant_aberto_w			number(10);
cd_estab_transf_w				number(4);
cd_local_transf_w				number(4);
qt_estoque_disp_atend_w			number(13,4);
cd_unidade_medida_compra_w		varchar2(30);
qt_material_estoque_w			number(13,4);
ie_situacao_mat_w				varchar2(1);
ie_vigente_anvisa_w  			material_estab.ie_vigente_anvisa%type;
ie_vigente_anvisa_marca_w		material_marca.ie_vigente_anvisa%type;

cursor	c01 is
select	nvl(a.cd_centro_custo,0),
	nvl(a.cd_local_estoque,0),
	b.cd_material,
	substr(adiciona_zeros_esquerda(to_char(a.nr_item_oci),5) || ' - (' || adiciona_zeros_esquerda(to_char(a.cd_material),6) || ') ' || b.ds_material,1,255),
	a.nr_item_oci,
	a.qt_material,
	a.nr_seq_marca,
	a.nr_seq_reg_lic_item,
	a.cd_unidade_medida_compra,
	nvl(b.ie_situacao,'A')
from	material b,
	ordem_compra_item a
where	a.cd_material		= b.cd_material
and	a.nr_ordem_compra	= nr_ordem_p;
	
begin
delete	ordem_compra_consist
where	nr_ordem_compra = nr_ordem_p;

select	count(*)
into	qt_item_compra_w 
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_p;

select	nvl(sum(vl_vencimento),0) vl_total_venc_w
into	vl_total_venc_w
from	ordem_compra_venc
where	nr_ordem_compra = nr_ordem_p;

select	nvl(sum(obter_valor_liquido_ordem(nr_ordem_compra)),0),
	max(cd_estabelecimento),
	nvl(max(nr_seq_subgrupo_compra),0),
	max(ie_tipo_ordem),
	max(cd_local_entrega),
	max(cd_centro_custo),
	nvl(max(cd_cgc_fornecedor),0)
into	vl_total_ordem_w,
	cd_estabelecimento_w,
	nr_seq_subgrupo_compra_w,
	ie_tipo_ordem_w,
	cd_local_entrega_ordem_w,
	cd_centro_custo_ordem_w,
	cd_cgc_fornecedor_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_p;

select	nvl(ie_consiste_validade_anvisa,'N'),
	nvl(ie_consiste_validade_marca_mat,'N'),
	qt_dia_entrega_urgente
into	ie_consiste_validade_anvisa_w,
	ie_consiste_valid_marca_mat_w,
	qt_dia_entrega_urgente_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

open c01;
loop
fetch c01 into
	cd_centro_custo_w,
	cd_local_estoque_w,
	cd_material_w,
	ds_material_w,
	nr_item_oci_w,
	qt_material_w,
	nr_seq_marca_w,
	nr_seq_reg_lic_item_w,
	cd_unidade_medida_compra_w,
	ie_situacao_mat_w;
exit when c01%notfound;
	begin
	if	(cd_centro_custo_w	= 0) and
		(cd_local_estoque_w	= 0) then
		begin
		ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(279737) || substr(ds_material_w,1,100) || WHEB_MENSAGEM_PCK.get_texto(279739),1,255);		
		
		gravar_ordem_compra_consist(nr_ordem_p, '02', ds_material_w, 'C',  null, nm_usuario_p);
		end;
	end if;

	select	nvl(sum(qt_prevista_entrega), 0)
	into	qt_material_entrega_w
	from	ordem_compra_item_entrega
	where	nr_ordem_compra	= nr_ordem_p
	and	nr_item_oci	= nr_item_oci_w;

	if	(qt_material_w	<> qt_material_entrega_w) then
		begin
		ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(279737) || substr(ds_material_w,1,100) || WHEB_MENSAGEM_PCK.get_texto(279740),1,255);
		gravar_ordem_compra_consist(nr_ordem_p, '06', ds_material_w, 'C',  null, nm_usuario_p);
		end;
	end if;
	
	select	max(qt_item)
	into	qt_reg_w
	from(
		select	count(*) qt_item,
			dt_prevista_entrega
		from	ordem_compra_item_entrega
		where	nr_ordem_compra = nr_ordem_p
		and	nr_item_oci = nr_item_oci_w
		and	dt_cancelamento is null
		group by dt_prevista_entrega);

	if	(qt_reg_w > 1) then
		begin
		ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(279737) || substr(ds_material_w,1,100) || WHEB_MENSAGEM_PCK.get_texto(279742),1,255); 	
		gravar_ordem_compra_consist(nr_ordem_p, '07', ds_material_w, 'C',  null, nm_usuario_p);
		end;
	end if;
	
	/* consiste se validade do registro da anvisa vencida*/
	if	(ie_consiste_validade_anvisa_w = 'S') then
		select	count(*)
		into	qt_validade_vencida_w
		from	material_estab
		where	cd_material = cd_material_w
		and	cd_estabelecimento = cd_estabelecimento_w
		and	dt_validade_reg_anvisa is not null
		and	dt_validade_reg_anvisa < sysdate;
		
		select	max(nvl(ie_vigente_anvisa,'N'))
		into 	ie_vigente_anvisa_w
		from	material_estab
		where	cd_material 		= cd_material_w
		and		cd_estabelecimento 	= cd_estabelecimento_w;
		
		if	(qt_validade_vencida_w > 0) and
			(ie_vigente_anvisa_w = 'N') then
			begin
			ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(279737) || substr(ds_material_w,1,100) || WHEB_MENSAGEM_PCK.get_texto(279743),1,255);
			gravar_ordem_compra_consist(nr_ordem_p, '08', ds_material_w, 'C',  null, nm_usuario_p);
			end;
		end if;
	end if;
	
	/*Consiste a data de validade da marca do material*/	
	if	(ie_consiste_valid_marca_mat_w = 'S') then

		select	max(dt_validade_reg_anvisa),
				max(ie_vigente_anvisa)
		into	dt_validade_anvisa_marca_w,
				ie_vigente_anvisa_marca_w
		from	material_marca
		where	cd_material = cd_material_w
		and	nr_sequencia = nr_seq_marca_w;
		
		if	((nvl(ie_vigente_anvisa_marca_w, 'X') = 'N') or 
			(nvl(dt_validade_anvisa_marca_w, sysdate) < trunc(sysdate))) then
			begin
			ds_erro_w :=	substr(WHEB_MENSAGEM_PCK.get_texto(279745) || substr(obter_desc_marca(nr_seq_marca_w),1,50) || WHEB_MENSAGEM_PCK.get_texto(279746) || substr(ds_material_w,1,100) || WHEB_MENSAGEM_PCK.get_texto(279748),1,255);
			gravar_ordem_compra_consist(nr_ordem_p, '09', ds_material_w, 'C',  null, nm_usuario_p);
			end;
		end if;	
	end if;

	/*consistir dados na material_estab*/
	select	count(*)
	into	qt_reg_w
	from	material_estab
	where	cd_material		= cd_material_w
	and	cd_estabelecimento 	= cd_estabelecimento_w;

	if	(qt_reg_w = 0) then
		begin
		ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(279737) || substr(ds_material_w,1,100) || WHEB_MENSAGEM_PCK.get_texto(279749),1,255);
		gravar_ordem_compra_consist(nr_ordem_p, '11', ds_material_w, 'C',  null, nm_usuario_p);
		end;
	end if;	
	end;
end loop;
close c01;

select	nvl(max(qt),0)
into	qt_reg_w
from	(select	count(*) qt,
		trunc(dt_vencimento,'dd')
	from	ordem_compra_venc
	where	nr_ordem_compra = nr_ordem_p
	having	count(trunc(dt_vencimento,'dd')) > 1
	group by trunc(dt_vencimento,'dd'));

if	(qt_reg_w > 0) then
	begin
	ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(279751),1, 255);
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

if	(qt_dia_entrega_urgente_w is not null) then
	begin
	select	count(*)
	into	qt_entrega_w
	from	ordem_compra_item_entrega b,
		ordem_compra_item a
	where 	a.nr_ordem_compra 	= b.nr_ordem_compra(+)
	and 	a.nr_item_oci		= b.nr_item_oci(+)
	and	dt_prevista_entrega	<= trunc(sysdate) + qt_dia_entrega_urgente_w
	and 	a.nr_ordem_compra 	= nr_ordem_p;

	if	(qt_entrega_w > 0) and
		(ie_marcar_urgente_oc_w = 'S') then
		update	ordem_compra
		set	ie_urgente = 'S'
		where	nr_ordem_compra = nr_ordem_p;
	end if;
	end;	
end if;

if	(qt_item_compra_w = 0) then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279752);
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if; 

if	(vl_total_ordem_w <> vl_total_venc_w)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279753);
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	count(*)
into	qt_inativo_w
from	pessoa_juridica b,
	ordem_compra a
where	a.nr_ordem_compra	= nr_ordem_p
and	a.cd_cgc_fornecedor	= b.cd_cgc
and	b.ie_situacao		<> 'A';

if	(qt_inativo_w > 0)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279754);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	count(*)
into	qt_condicao_inativo_w
from	condicao_pagamento b,
	ordem_compra a
where	a.nr_ordem_compra	= nr_ordem_p
and	a.cd_condicao_pagamento	= b.cd_condicao_pagamento
and	b.ie_situacao		<> 'A';

if	(qt_condicao_inativo_w > 0)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279755);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	count(*)
into	qt_reg_w
from	ordem_compra a
where	a.nr_ordem_compra	= nr_ordem_p
and	cd_cgc_fornecedor is not null
and	cd_pessoa_fisica is not null;

if	(qt_reg_w > 0)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279757);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	count(*)
into	qt_reg_w
from	ordem_compra a
where	a.nr_ordem_compra	= nr_ordem_p
and	cd_cgc_fornecedor is null
and	cd_pessoa_fisica is null;

if	(qt_reg_w > 0)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279758);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	count(*),
	max(a.cd_conta_contabil)
into	qt_reg_w,
	cd_conta_contabil_w
from	conta_contabil b, 
	ordem_compra_item a
where	a.nr_ordem_compra	= nr_ordem_p
and	a.cd_conta_contabil	= b.cd_conta_contabil
and	b.ie_tipo		= 'T';	

if	(qt_reg_w > 0)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279759) || cd_conta_contabil_w || WHEB_MENSAGEM_PCK.get_texto(279760);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	nvl(max(a.cd_conta_contabil), 'X')
into	cd_conta_contabil_w
from	ordem_compra b,
	ordem_compra_item a
where	a.nr_ordem_compra	= b.nr_ordem_compra
and	a.nr_ordem_compra	= nr_ordem_p
and	(substr(obter_se_conta_vigente(a.cd_conta_contabil, b.dt_ordem_compra),1,1) = 'N');

if	(cd_conta_contabil_w <> 'X') then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279759) || cd_conta_contabil_w || WHEB_MENSAGEM_PCK.get_texto(279761);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

select	count(*), 
	max(a.cd_centro_custo)
into	qt_reg_w,
	cd_centro_custo_w
from	centro_custo b, 
	ordem_compra_item a
where	a.nr_ordem_compra	= nr_ordem_p
and	a.cd_centro_custo	= b.cd_centro_custo
and	b.ie_tipo		= 'T';
	
if	(qt_reg_w > 0)	then
	begin
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279762) || cd_centro_custo_w || WHEB_MENSAGEM_PCK.get_texto(279763);	
	gravar_ordem_compra_consist(nr_ordem_p, '01', ds_erro_w, 'C',  null, nm_usuario_p);
	end;
end if;

ds_erro_p := substr(ds_erro_w,1,255);
commit;
end consiste_ordem_central_compra;
/