create or replace
procedure ajuste_lic_doc_padrao is

qt_existe_w		number(5);

begin

select	count(*)
into	qt_existe_w
from	lic_documentacao_padrao
where	ds_texto is not null;

Exec_Sql_Dinamico('Juan',' ALTER TABLE LIC_DOCUMENTACAO_PADRAO ADD DS_TEXTO_VAR LONG ');

if	(qt_existe_w = 0) then
	Exec_Sql_Dinamico('Juan',' UPDATE LIC_DOCUMENTACAO_PADRAO SET DS_TEXTO_VAR = DS_TEXTO ');
end if;

Exec_Sql_Dinamico('Juan',' ALTER TABLE LIC_DOCUMENTACAO_PADRAO DROP COLUMN DS_TEXTO ');
Exec_Sql_Dinamico('Juan',' ALTER TABLE LIC_DOCUMENTACAO_PADRAO RENAME COLUMN DS_TEXTO_VAR TO DS_TEXTO ');

commit;

end ajuste_lic_doc_padrao;
/