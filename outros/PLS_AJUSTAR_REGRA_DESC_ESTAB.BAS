create or replace
procedure pls_ajustar_regra_desc_estab is

nr_seq_plano_w			number(10);
cd_estab_plano_w		number(10);
nr_seq_contrato_w		number(10);
cd_estab_contrato_w		number(10);
nr_seq_proposta_w		number(10);
cd_estab_proposta_w		number(10);

Cursor C01 is
	select	nr_seq_plano
	from	pls_regra_desconto
	where	nr_seq_plano is not null
	and	cd_estabelecimento is null;
	
Cursor C02 is
	select	nr_seq_contrato
	from	pls_regra_desconto
	where	nr_seq_contrato is not null
	and	cd_estabelecimento is null;

Cursor C03 is
	select	nr_seq_proposta
	from	pls_regra_desconto
	where	nr_seq_proposta is not null
	and	cd_estabelecimento is null;

begin

open C01;
loop
fetch C01 into	
	nr_seq_plano_w;
exit when C01%notfound;
	begin
	
	select	max(cd_estabelecimento)
	into	cd_estab_plano_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_w;
	
	if	(cd_estab_plano_w is not null) then
		update	pls_regra_desconto
		set	cd_estabelecimento	= cd_estab_plano_w
		where	nr_seq_plano		= nr_seq_plano_w
		and	cd_estabelecimento is null;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_contrato_w;
exit when C02%notfound;
	begin
	
	select	max(cd_estabelecimento)
	into	cd_estab_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
	
	if	(cd_estab_contrato_w is not null) then
		update	pls_regra_desconto
		set	cd_estabelecimento	= cd_estab_contrato_w
		where	nr_seq_contrato		= nr_seq_contrato_w
		and	cd_estabelecimento is null;
	end if;
	
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_proposta_w;
exit when C03%notfound;
	begin
	
	select	max(cd_estabelecimento)
	into	cd_estab_proposta_w
	from	pls_proposta_adesao
	where	nr_sequencia	= nr_seq_proposta_w;
	
	if	(cd_estab_proposta_w is not null) then
		update	pls_regra_desconto
		set	cd_estabelecimento	= cd_estab_proposta_w
		where	nr_seq_proposta		= nr_seq_proposta_w
		and	cd_estabelecimento is null;
	end if;
	
	end;
end loop;
close C03;

commit;

end pls_ajustar_regra_desc_estab;
/
