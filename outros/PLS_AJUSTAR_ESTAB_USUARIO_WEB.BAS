create or replace
procedure pls_ajustar_estab_usuario_web
			(	
				nm_usuario_p		Varchar2) is 

				
qt_reg_prest_w		Number(15);
qt_reg_estip_w		Number(15);
qt_reg_benef_w		Number(15);
nr_seq_contrato_w	Number(10);
nr_seq_estip_w		Number(10);
nr_seq_seg_w		Number(10);
nr_seq_segurado_w	Number(10);
cd_estabelecimento_w	Number(4);
nr_seq_prestador_w	Number(10);
nr_seq_user_prest_w	Number(10);
			
				
Cursor C01 is
	select 	nr_sequencia,
		nr_seq_prestador	
	from 	pls_usuario_web;
	
Cursor C02 is
	select	nr_sequencia,
		nr_seq_contrato
	from	pls_estipulante_web;
	
Cursor C03 is
	select	nr_sequencia,
		nr_seq_segurado
	from	pls_segurado_web;
	
begin

select 	count(nr_sequencia)
into	qt_reg_prest_w
from 	pls_usuario_web;


select	count(nr_sequencia)
into	qt_reg_estip_w
from	pls_estipulante_web;

select	count(nr_sequencia)
into	qt_reg_benef_w
from	pls_segurado_web;


/* Adicionar estabelecimento aos usu�rios prestadores do portal do plano de sa�de */
if	(qt_reg_prest_w > 0) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_user_prest_w,
		nr_seq_prestador_w;
	exit when C01%notfound;
		begin
			select	nvl(cd_estabelecimento,1)
			into	cd_estabelecimento_w
			from	pls_prestador
			where	nr_sequencia	= nr_seq_prestador_w;	
			
			if	(cd_estabelecimento_w is not null) then
				update 	pls_usuario_web 
				set 	cd_estabelecimento = cd_estabelecimento_w
				where	nr_sequencia = nr_seq_user_prest_w;
			end if;
		end;
	end loop;
	close C01;	
end if; 

/* Adicionar estabelecimento aos usu�rios estipulantes do portal do plano de sa�de */
if	(qt_reg_estip_w > 0) then
	open C02;
	loop
	fetch C02 into	
		nr_seq_estip_w,
		nr_seq_contrato_w;
	exit when C02%notfound;
		begin
			select	nvl(cd_estabelecimento,1)
			into	cd_estabelecimento_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_w;
						
			if	(cd_estabelecimento_w is not null) then
				update 	pls_estipulante_web 
				set 	cd_estabelecimento = cd_estabelecimento_w
				where	nr_sequencia = nr_seq_estip_w;
			end if;
		end;
	end loop;
	close C02;
end if;

/* Adicionar estabelecimento aos usu�rios benefici�rios do portal do plano de sa�de */
if	(qt_reg_benef_w > 0) then
	open C03;
	loop
	fetch C03 into	
		nr_seq_seg_w,
		nr_seq_segurado_w;
	exit when C03%notfound;
		begin
			select	max(b.cd_estabelecimento)
			into	cd_estabelecimento_w
			from	pls_segurado a,
				pls_contrato b
			where	a.nr_seq_contrato	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_segurado_w;
			
			if	(cd_estabelecimento_w is not null) then
				update 	pls_segurado_web 
				set 	cd_estabelecimento = cd_estabelecimento_w
				where	nr_sequencia = nr_seq_seg_w;
			end if;
		end;
	end loop;
	close C03;
end if;

commit;

end pls_ajustar_estab_usuario_web;
/