create or replace
procedure pls_lib_interface_a560_v70 is 

begin

begin
insert into interface (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2747, 'A560 - Carga para Nota de D�bito 7.0', 'C:\', sysdate, 'wcbernardino', 'N', 'PTU7.0', 'select ds_conteudo from    w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_sequencia', null, '', '', '');
exception
when others then
	null;
end;
--'

begin
insert into interface_reg (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2747, 1, 'Conte�do geral - Procedure', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into interface_atributo (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2747, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 300, sysdate, 'wcbernardino', null, '', ' ', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

commit;

end pls_lib_interface_a560_v70;
/