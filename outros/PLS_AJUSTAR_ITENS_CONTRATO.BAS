create or replace
procedure pls_ajustar_itens_contrato
		(	cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 
			
cd_perfil_w		number(5);
nr_seq_item_w		number(10);
qt_registros_w		number(10);
			
Cursor C01 is
	select	cd_perfil
	from	funcao_perfil
	where	cd_funcao = 1202;
	
Cursor C02 is
	select	nr_sequencia
	from	pls_itens_contrato_benef
	where	ie_situacao	= 'A';
	
Cursor C03 is
	select	nr_sequencia
	from	pls_itens_contrato_regra
	where	ie_situacao	= 'A';

begin

open C01;
loop
fetch C01 into	
	cd_perfil_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_item_w;
	exit when C02%notfound;
		begin
		
		select	count(*)
		into	qt_registros_w
		from	pls_item_contr_benef_lib
		where	cd_perfil	= cd_perfil_w
		and	nr_seq_item	= nr_seq_item_w;
		
		if	(qt_registros_w = 0) then
			pls_copiar_item_contr_benef(nr_seq_item_w,cd_perfil_w,cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		end;
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_item_w;	
	exit when C03%notfound;
		begin
		
		select	count(*)
		into	qt_registros_w
		from	pls_item_contr_regra_lib
		where	cd_perfil	= cd_perfil_w
		and	nr_seq_item	= nr_seq_item_w;
		
		if	(qt_registros_w = 0) then
			pls_copiar_item_contr_regra(nr_seq_item_w,cd_perfil_w,cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		end;
	end loop;
	close C03;
	
	end;
end loop;
close C01;

commit;

end pls_ajustar_itens_contrato;
/
