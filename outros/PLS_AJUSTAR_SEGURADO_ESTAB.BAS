create or replace
procedure pls_ajustar_segurado_estab is

nr_seq_segurado_w	number(10);
cd_estab_contrato_w	number(10);
cd_estab_plano_w	number(10);
cd_estab_intercambio_w	number(10);
cd_estab_congenere_w	number(10);
ie_tipo_segurado_w	varchar2(2);
cd_estab_pericia_w	number(10);

Cursor C01 is
	select	a.cd_estabelecimento,
		b.nr_sequencia,
		c.cd_estabelecimento,
		b.ie_tipo_segurado,
		d.cd_estabelecimento,
		e.cd_estabelecimento
	from	pls_intercambio		e,
		pls_congenere		d,
		pls_plano		c,
		pls_segurado		b,
		pls_contrato		a
	where	b.nr_seq_contrato	= a.nr_sequencia(+)
	and	b.nr_seq_plano		= c.nr_sequencia(+)
	and	b.nr_seq_congenere	= d.nr_sequencia(+)
	and	b.nr_seq_intercambio	= e.nr_sequencia(+);
	
begin

open C01;
loop
fetch C01 into	
	cd_estab_contrato_w,
	nr_seq_segurado_w,
	cd_estab_plano_w,
	ie_tipo_segurado_w,
	cd_estab_congenere_w,
	cd_estab_intercambio_w;
exit when C01%notfound;
	begin
	
	if	(ie_tipo_segurado_w in ('A','B')) then
		update	pls_segurado
		set	cd_estabelecimento	=  nvl(cd_estab_contrato_w,cd_estab_plano_w)
		where	nr_sequencia		= nr_seq_segurado_w;
	elsif	(ie_tipo_segurado_w = 'P') then
	
		begin
		select	cd_estabelecimento
		into	cd_estab_pericia_w
		from	pls_pericia_medica
		where	nr_seq_segurado	= nr_seq_segurado_w;
		exception
		when others then
			cd_estab_pericia_w	:= cd_estab_plano_w;
		end;
		
		update	pls_segurado
		set	cd_estabelecimento	=  nvl(cd_estab_pericia_w,cd_estab_plano_w)
		where	nr_sequencia		= nr_seq_segurado_w;
		
	elsif	(ie_tipo_segurado_w = 'I') then
		update	pls_segurado
		set	cd_estabelecimento	= nvl(cd_estab_congenere_w,cd_estab_plano_w)
		where	nr_sequencia		= nr_seq_segurado_w;
	elsif	(ie_tipo_segurado_w = 'T') then
		update	pls_segurado
		set	cd_estabelecimento	= nvl(cd_estab_intercambio_w,cd_estab_plano_w)
		where	nr_sequencia		= nr_seq_segurado_w;
	end if;	
	end;
end loop;
close C01;

commit;

end pls_ajustar_segurado_estab;
/
