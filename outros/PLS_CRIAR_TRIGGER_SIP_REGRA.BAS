create or replace
procedure pls_criar_trigger_sip_regra is

ds_trigger_w			clob;
ds_corpo_trigger_w		clob;

ds_select_campos_w		clob;
ds_select_variaveis_w		clob;
ds_select_w			clob;

ds_variaveis_w			clob;
ds_montar_campo_w		clob;
ds_estrutura_valor_campo_w	clob;
ds_enter_w			varchar2(10) := chr(10);

qt_caracteres_w			integer;
qt_loops_w			integer;
ub				integer := 0;

type campos is record (ds		Varchar2(32760));
type Vetor is table of  campos index by binary_integer;
ds_script_vetor			Vetor;

ret_val				pls_integer;
cursor1				pls_integer;
v_text				dbms_sql.varchar2s;
ds_linha_w			Varchar2(256);

ds_script_w			clob;

cursor C01 is
select	column_name nm_atributo,
	data_type ie_tipo_atributo,
	data_length qt_tamanho 
from	user_tab_columns
where	upper(table_name) = upper('sip_item_assist_regra_nv')
and	upper(column_name) not in (	upper('nm_usuario'),
					upper('nm_usuario_nrec'),
					upper('dt_atualizacao'),
					upper('dt_atualizacao_nrec'),
					upper('nr_seq_item_assist'),
					upper('nr_sequencia'),
					upper('ie_situacao'),
					upper('ds_controle_regra_duplic'))
order by 1;

begin

ds_variaveis_w := '';
ds_montar_campo_w := '';
ds_select_campos_w := '';
ds_select_variaveis_w := '';
ds_estrutura_valor_campo_w := '';


for	r_C01_w in C01 loop
	-- Criar as vari�veis da Trigger
	if	(r_C01_w.qt_tamanho is null) then
		ds_variaveis_w := ds_variaveis_w||r_C01_w.nm_atributo||'_w	'||r_C01_w.ie_tipo_atributo||';'||ds_enter_w;
	else
		ds_variaveis_w := ds_variaveis_w||r_C01_w.nm_atributo||'_w	'||r_C01_w.ie_tipo_atributo||'('||r_C01_w.qt_tamanho||');'||ds_enter_w;
	end if;

	--Montar os campos do select principal da Trigger;
	-- Se for a primeira linha n�o coloca v�rgula
	if	(ds_select_campos_w is null) then
		ds_select_campos_w := ds_select_campos_w||':new.'||r_C01_w.nm_atributo;
	else
		ds_select_campos_w := ds_select_campos_w||', '||ds_enter_w||':new.'||r_C01_w.nm_atributo;
	end if;

	--Montar as vari�veis do select principal da Trigger;
	-- Se for a primeira linha n�o coloca v�rgula
	if	(ds_select_variaveis_w  is null) then
		ds_select_variaveis_w := ds_select_variaveis_w||r_C01_w.nm_atributo||'_w';
	else
		ds_select_variaveis_w := ds_select_variaveis_w||', '||ds_enter_w||r_C01_w.nm_atributo||'_w';
	end if;

	--Motar o recebimento de valor para o campo ds_controle_regra_duplic
	-- Se for a primeira linha n�o coloca o ||
	if	(ds_estrutura_valor_campo_w is null) then
		ds_estrutura_valor_campo_w := 	ds_estrutura_valor_campo_w||ds_enter_w||
						''''||r_C01_w.nm_atributo||'='||''''||'||'||r_C01_w.nm_atributo||'_w'||'||'||''';''';
	else
		ds_estrutura_valor_campo_w := 	ds_estrutura_valor_campo_w||ds_enter_w||'||'||
						''''||r_C01_w.nm_atributo||'='||''''||'||'||r_C01_w.nm_atributo||'_w'||'||'||''';''';
	end if;
end loop;

--Criar o select principal da trigger;
if	(ds_select_campos_w is not null) and
	(ds_select_variaveis_w is not null) then

	ds_select_w	:=	'	select	'||ds_select_campos_w||ds_enter_w||
				'	into	'||ds_select_variaveis_w||ds_enter_w||
				'	from	dual ;'||ds_enter_w;
end if;

-- Criar o recebimento do campo ds_controle_regra_duplic
if	(ds_estrutura_valor_campo_w is not null) then
	ds_montar_campo_w :=	' :new.ds_controle_regra_duplic := '||ds_estrutura_valor_campo_w||';';
end if;

ds_corpo_trigger_w	:=	ds_enter_w||ds_select_w||ds_enter_w||ds_montar_campo_w||ds_enter_w;

ds_trigger_w := ' create or replace trigger sip_item_assist_regra_nv_bi '||ds_enter_w||
		' before update or insert on sip_item_assist_regra_nv '||ds_enter_w||
		' for each row '||ds_enter_w||
		' declare '||ds_enter_w||
		ds_variaveis_w||ds_enter_w||
		' begin '||ds_enter_w||
		ds_corpo_trigger_w||ds_enter_w||
		' end; '||ds_enter_w;

ds_script_vetor(1).ds := substr(ds_trigger_w,1,32000);
ds_script_vetor(2).ds := substr(ds_trigger_w,32001,32000);

qt_caracteres_w := nvl(length(ds_script_vetor(1).ds),0) + nvl(length(ds_script_vetor(2).ds),0);

qt_loops_w	:= round(qt_caracteres_w / 256) + 1;

for j in 1..qt_loops_w loop
	begin
	ds_linha_w := dbms_lob.substr(ds_trigger_w,256,((j*256)-255));

	if	(length(trim(ds_linha_w)) > 0) then
		ub := ub+1;
		v_text(ub) := ds_linha_w;
	end if;
	end;
end loop;

for l in 1..ub loop
	ds_script_w := ds_script_w||v_text(ub);
end loop;

cursor1 := dbms_sql.open_cursor;
dbms_sql.parse(cursor1, v_text, 1, ub, null, dbms_sql.native);
ret_val := dbms_sql.execute(cursor1);
dbms_sql.close_cursor(cursor1);

end pls_criar_trigger_sip_regra;
/