create or replace 
type person_name_dev_row as object(
	cd_pessoa_fisica varchar2(10), 
	nm_pessoa_fisica varchar2(200)
);
/

create or replace 
type person_name_dev_table as table of person_name_dev_row;
/
