create or replace type tipo_val_tab_amb as object
(
	vl_procedimento		number(15,4),
	vl_custo_operacional	number(15,4),
	vl_medico		number(15,4),               
	vl_filme		number(15,4),                
	nr_porte_anestesico	number(2),     
	vl_anestesista		number(15,2),          
	nr_auxiliares		number(2),           
	vl_auxiliares_amb	number(15,4),
	qt_filme		number(15,4)
);
/