create or replace type philips_json_list as object (
 
  list_data p_json_value_array,
  constructor function philips_json_list return self as result,
  constructor function philips_json_list(str varchar2) return self as result,
  constructor function philips_json_list(str clob) return self as result,
  constructor function philips_json_list(cast philips_json_value) return self as result,
  
  member procedure append(self in out nocopy philips_json_list, elem philips_json_value, position pls_integer default null),
  member procedure append(self in out nocopy philips_json_list, elem varchar2, position pls_integer default null),
  member procedure append(self in out nocopy philips_json_list, elem number, position pls_integer default null),
  member procedure append(self in out nocopy philips_json_list, elem boolean, position pls_integer default null),
  member procedure append(self in out nocopy philips_json_list, elem philips_json_list, position pls_integer default null),

  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem philips_json_value),
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem varchar2),
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem number),
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem boolean),
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem philips_json_list),

  member function count return number,
  member procedure remove(self in out nocopy philips_json_list, position pls_integer),
  member procedure remove_first(self in out nocopy philips_json_list),
  member procedure remove_last(self in out nocopy philips_json_list),
  member function get(position pls_integer) return philips_json_value,
  member function head return philips_json_value,
  member function last return philips_json_value,
  member function tail return philips_json_list,


  member function to_char(spaces boolean default true, chars_per_line number default 0) return varchar2,
  member procedure to_clob(self in philips_json_list, buf in out nocopy clob, spaces boolean default false, chars_per_line number default 0, erase_clob boolean default true),
  member procedure print(self in philips_json_list, spaces boolean default true, chars_per_line number default 8192, jsonp varchar2 default null), --32512 is maximum
  member procedure htp(self in philips_json_list, spaces boolean default false, chars_per_line number default 0, jsonp varchar2 default null),


  member function path(json_path varchar2, base number default 1) return philips_json_value,

  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem philips_json_value, base number default 1),
  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem varchar2  , base number default 1),
  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem number    , base number default 1),
  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem boolean   , base number default 1),
  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem philips_json_list , base number default 1),


  member procedure path_remove(self in out nocopy philips_json_list, json_path varchar2, base number default 1),

  member function to_json_value return philips_json_value
 
  
) not final;
/

create or replace type body philips_json_list as

  constructor function philips_json_list return self as result as
  begin
    self.list_data := p_json_value_array();
    return;
  end;

  constructor function philips_json_list(str varchar2) return self as result as
  begin
    self := philips_json_parser.parse_list(str);
    return;
  end;
  
  constructor function philips_json_list(str clob) return self as result as
  begin
    self := philips_json_parser.parse_list(str);
    return;
  end;

  constructor function philips_json_list(cast philips_json_value) return self as result as
    x number;
  begin
    x := cast.object_or_array.getobject(self);
    return;
  end;


  member procedure append(self in out nocopy philips_json_list, elem philips_json_value, position pls_integer default null) as
    indx pls_integer;
    insert_value philips_json_value := NVL(elem, philips_json_value);
  begin
    if(position is null or position > self.count) then --end of list
      indx := self.count + 1;
      self.list_data.extend(1);
      self.list_data(indx) := insert_value;
    elsif(position < 1) then --new first
      indx := self.count;
      self.list_data.extend(1);
      for x in reverse 1 .. indx loop
        self.list_data(x+1) := self.list_data(x);
      end loop;
      self.list_data(1) := insert_value;
    else
      indx := self.count;
      self.list_data.extend(1);
      for x in reverse position .. indx loop
        self.list_data(x+1) := self.list_data(x);
      end loop;
      self.list_data(position) := insert_value;
    end if;

  end;

  member procedure append(self in out nocopy philips_json_list, elem varchar2, position pls_integer default null) as
  begin
    append(philips_json_value(elem), position);
  end;
  
  member procedure append(self in out nocopy philips_json_list, elem number, position pls_integer default null) as
  begin
    if(elem is null) then
      append(philips_json_value(), position);
    else
      append(philips_json_value(elem), position);
    end if;
  end;
  
  member procedure append(self in out nocopy philips_json_list, elem boolean, position pls_integer default null) as
  begin
    if(elem is null) then
      append(philips_json_value(), position);
    else
      append(philips_json_value(elem), position);
    end if;
  end;

  member procedure append(self in out nocopy philips_json_list, elem philips_json_list, position pls_integer default null) as
  begin
    if(elem is null) then
      append(philips_json_value(), position);
    else
      append(elem.to_json_value, position);
    end if;
  end;
  
 member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem philips_json_value) as
    insert_value philips_json_value := NVL(elem, philips_json_value);
    indx number;
  begin
    if(position > self.count) then 
      indx := self.count + 1;
      self.list_data.extend(1);
      self.list_data(indx) := insert_value;
    elsif(position < 1) then 
      null;
    else
      self.list_data(position) := insert_value;
    end if;
  end;
  
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem varchar2) as
  begin
    replace(position, philips_json_value(elem));
  end;
  
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem number) as
  begin
    if(elem is null) then
      replace(position, philips_json_value());
    else
      replace(position, philips_json_value(elem));
    end if;
  end;
  
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem boolean) as 
  begin
    if(elem is null) then
      replace(position, philips_json_value());
    else
      replace(position, philips_json_value(elem));
    end if;
  end;
  
  member procedure replace(self in out nocopy philips_json_list, position pls_integer, elem philips_json_list) as 
  begin
    if(elem is null) then
      replace(position, philips_json_value());
    else
      replace(position, elem.to_json_value);
    end if;
  end;

  member function count return number as
  begin
    return self.list_data.count;
  end;
  
  member procedure remove(self in out nocopy philips_json_list, position pls_integer) as
  begin
    if(position is null or position < 1 or position > self.count) then return; end if;
    for x in (position+1) .. self.count loop
      self.list_data(x-1) := self.list_data(x);
    end loop;
    self.list_data.trim(1);
  end;
  
  member procedure remove_first(self in out nocopy philips_json_list) as 
  begin
    for x in 2 .. self.count loop
      self.list_data(x-1) := self.list_data(x);
    end loop;
    if(self.count > 0) then 
      self.list_data.trim(1);
    end if;
  end;
  
  member procedure remove_last(self in out nocopy philips_json_list) as
  begin
    if(self.count > 0) then 
      self.list_data.trim(1);
    end if;
  end;
  
  member function get(position pls_integer) return philips_json_value as
  begin
    if(self.count >= position and position > 0) then
      return self.list_data(position);
    end if;
    return null; 
  end;
  
  member function head return philips_json_value as
  begin
    if(self.count > 0) then
      return self.list_data(self.list_data.first);
    end if;
    return null; 
  end;
  
  member function last return philips_json_value as
  begin
    if(self.count > 0) then
      return self.list_data(self.list_data.last);
    end if;
    return null; 
  end;
  
  member function tail return philips_json_list as
    t philips_json_list;
  begin
    if(self.count > 0) then
      t := philips_json_list(self.list_data);
      t.remove(1);
      return t;
    else return philips_json_list(); end if;
  end;

  member function to_char(spaces boolean default true, chars_per_line number default 0) return varchar2 as
  begin
    if(spaces is null) then
      return philips_json_printer.pretty_print_list(self, line_length => chars_per_line);
    else 
      return philips_json_printer.pretty_print_list(self, spaces, line_length => chars_per_line);
    end if;
  end;

  member procedure to_clob(self in philips_json_list, buf in out nocopy clob, spaces boolean default false, chars_per_line number default 0, erase_clob boolean default true) as
  begin
    if(spaces is null) then	
      philips_json_printer.pretty_print_list(self, false, buf, line_length => chars_per_line, erase_clob => erase_clob);
    else 
      philips_json_printer.pretty_print_list(self, spaces, buf, line_length => chars_per_line, erase_clob => erase_clob);
    end if;
  end;

  member procedure print(self in philips_json_list, spaces boolean default true, chars_per_line number default 8192, jsonp varchar2 default null) as --32512 is the real maximum in sqldeveloper
    my_clob clob;
  begin
    my_clob := empty_clob();
    dbms_lob.createtemporary(my_clob, true);
    philips_json_printer.pretty_print_list(self, spaces, my_clob, case when (chars_per_line>32512) then 32512 else chars_per_line end);
    philips_json_printer.dbms_output_clob(my_clob, philips_json_printer.newline_char, jsonp);
    dbms_lob.freetemporary(my_clob);  
  end;
  
  member procedure htp(self in philips_json_list, spaces boolean default false, chars_per_line number default 0, jsonp varchar2 default null) as 
    my_clob clob;
  begin
    my_clob := empty_clob();
    dbms_lob.createtemporary(my_clob, true);
    philips_json_printer.pretty_print_list(self, spaces, my_clob, chars_per_line);
    philips_json_printer.htp_output_clob(my_clob, jsonp);
    dbms_lob.freetemporary(my_clob);  
  end;

  member function path(json_path varchar2, base number default 1) return philips_json_value as
    cp philips_json_list := self;
  begin
    return philips_json_ext.get_json_value(philips_json(cp), json_path, base);
  end path;



  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem philips_json_value, base number default 1) as
    objlist philips_json;
    jp philips_json_list := philips_json_ext.parsePath(json_path, base); 
  begin
    while(jp.head().get_number() > self.count) loop
      self.append(philips_json_value());
    end loop;
    
    objlist := philips_json(self);
    philips_json_ext.put(objlist, json_path, elem, base);
    self := objlist.get_values;
  end path_put;
  
  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem varchar2, base number default 1) as
    objlist philips_json;
    jp philips_json_list := philips_json_ext.parsePath(json_path, base); 
  begin
    while(jp.head().get_number() > self.count) loop
      self.append(philips_json_value());
    end loop;
    
    objlist := philips_json(self);
    philips_json_ext.put(objlist, json_path, elem, base);
    self := objlist.get_values;
  end path_put;
  
  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem number, base number default 1) as
    objlist philips_json;
    jp philips_json_list := philips_json_ext.parsePath(json_path, base); 
  begin
    while(jp.head().get_number() > self.count) loop
      self.append(philips_json_value());
    end loop;
    
    objlist := philips_json(self);
  
    if(elem is null) then 
      philips_json_ext.put(objlist, json_path, philips_json_value, base);
    else 
      philips_json_ext.put(objlist, json_path, elem, base);
    end if;
    self := objlist.get_values;
  end path_put;

  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem boolean, base number default 1) as
    objlist philips_json;
    jp philips_json_list := philips_json_ext.parsePath(json_path, base); 
  begin
    while(jp.head().get_number() > self.count) loop
      self.append(philips_json_value());
    end loop;
    
    objlist := philips_json(self);
    if(elem is null) then 
      philips_json_ext.put(objlist, json_path, philips_json_value, base);
    else 
      philips_json_ext.put(objlist, json_path, elem, base);
    end if;
    self := objlist.get_values;
  end path_put;

  member procedure path_put(self in out nocopy philips_json_list, json_path varchar2, elem philips_json_list, base number default 1) as
    objlist philips_json;
    jp philips_json_list := philips_json_ext.parsePath(json_path, base); 
  begin
    while(jp.head().get_number() > self.count) loop
      self.append(philips_json_value());
    end loop;
    
    objlist := philips_json(self);
    if(elem is null) then 
      philips_json_ext.put(objlist, json_path, philips_json_value, base);
    else 
      philips_json_ext.put(objlist, json_path, elem, base);
    end if;
    self := objlist.get_values;
  end path_put;
  
  member procedure path_remove(self in out nocopy philips_json_list, json_path varchar2, base number default 1) as
    objlist philips_json := philips_json(self);
  begin
    philips_json_ext.remove(objlist, json_path, base);
    self := objlist.get_values;
  end path_remove;
  

  member function to_json_value return philips_json_value as
  begin
    return philips_json_value(sys.anydata.convertobject(self));
  end;

 
end;
/