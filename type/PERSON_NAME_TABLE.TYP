create or replace 
type person_name_row as object(	nr_sequencia number(10), 
				ds_given_name varchar2(128), 
				ds_component_name_1 varchar2(128), 
				ds_component_name_2 varchar2(128), 
				ds_component_name_3 varchar2(128), 
				ds_family_name varchar2(128), 
				ds_type varchar2(15));
/

create or replace 
type person_name_row_score as object(
   nr_sequencia number(10),
   ds_given_name varchar2(128),
   ds_component_name_1 varchar2(128),
   ds_component_name_2 varchar2(128),
   ds_component_name_3 varchar2(128),
   ds_family_name varchar2(128),
   ds_type varchar2(15),
   nr_score number(5));
/

create or replace type person_name_table as table of person_name_row_score;
/
