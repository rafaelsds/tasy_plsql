create or replace 
type get_chart_row as object(
	code_group			varchar2(15),
	ie_union 				number(10),
	name				varchar2(255),
	comercial_name			varchar2(255),
	ie_medic_nao_inform		varchar2(15),
	description			varchar2(255),
	color				varchar2(15),
	style 				varchar2(30),
	shape 				varchar2(30),
	qt_size 				number(5),
	fill_color 				varchar2(15),
	shaded 				varchar2(15),
	fixed_scale 			number(2),
	init_date 				date,
	end_date 			date,
	CODE_CHART 			number(10),
	seq_apresentacao 			number(10),
	nr_cirurgia 			number(10),
	nr_seq_pepo 			number(10),
	nm_usuario 			varchar2(15),
	nm_tabela			varchar2(50),
	nm_atributo 			varchar2(50),
	ie_sinal_vital 			varchar2(15),
	nr_seq_superior 			number(10),
	end_of_administration 		varchar2(15),
	cd_material 			number(6),
	cd_unid_medida_adm 		varchar2(30),
	ie_modo_adm 			varchar2(15),
	nr_seq_agente 			number(10),
	ds_dosagem 			varchar2(15),
	ie_tipo				varchar2(15)
);
/

create or replace 
type get_chart_table as table of get_chart_row;
/