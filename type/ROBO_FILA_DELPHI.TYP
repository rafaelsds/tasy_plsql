create or replace
procedure robo_fila_delphi is

nr_seq_ordem_w			number(10);
nr_ordem_serv_w			number(10);
ie_prioridade_w			varchar2(10);
ie_prioridade_sup_w		number(3);
dt_ordem_serv_w			date;
nm_usuario_w			varchar2(20);
nr_seq_prevista_w 			number(10);
nr_seq_ativ_prev_w 		number(10);
qt_registro_w			number(10);
nr_seq_ordem_antiga_w 		number(10);
nr_seq_fila_w			number(10);
nm_usuario_fila_w			varchar2(15);
qt_min_prev_w			number(15);
nr_seq_lp_w			number(10);
dt_prevista_ww			date;
/*dados para tabela da fila*/
dt_referencia_w			date	:= trunc(sysdate,'dd');
qt_integrantes_w			number(10)	:= 0;
qt_min_capac_prod_diaria_w		number(15)	:= 0;
qt_min_previsto_dia_w		number(15)	:= 0;
nr_seq_exec_w			number(10);
dt_prev_os_w			date;
ie_status_w			varchar2(15);
nr_seq_gerencia_w			number(10);
ie_atualiza_dt_prev_w		varchar2(1);
qt_pacote_w			number(10);
nr_seq_severidade_w		man_ordem_servico.nr_seq_severidade_wheb%type;
nr_sequencia_w			fila_execucao_os.nr_sequencia%type;
ds_curva_abc_w			varchar2(10);
nr_seq_severidade_score_w		number(10);
ie_classificacao_w			varchar2(10);
qt_pontuacao_w			number(15);
qt_dias_os_w			number(15);
ie_prioridade_cliente_w		varchar2(20);
ie_considera_grupo_w		cadastro_fila.ie_considera_grupo%type;
ie_calc_score_w			cadastro_fila.ie_calc_score%type;
ie_capacidade_skill_w		cadastro_fila.ie_capacidade_skill%type;
IE_PACOTE_ENTREGA_w		cadastro_fila.IE_PACOTE_ENTREGA%type;

qt_recurso_w			fila_execucao_os.qt_recurso%type;

cursor c01 is
select 	1 nr_seq_ordem,
	a.nr_sequencia,
	c.ie_prioridade_desen,
	a.dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.dt_prevista,
	c.nr_sequencia nr_seq_ativ_prev,
	decode(a.ie_classificacao,'E',nvl(a.nr_seq_severidade_wheb,0),0) nr_seq_severidade,
	substr(nvl(man_obter_curva_abc(a.nr_seq_cliente),'C'),1,5) ds_curva_abc,
	nvl(a.nr_seq_severidade,a.nr_seq_severidade_wheb) nr_seq_severidade_score,
	a.ie_classificacao,
	nvl(a.ie_prioridade_cliente,a.ie_prioridade) ie_prioridade,
	c.ie_prioridade_sup
from   	man_ordem_servico a,
	man_ordem_servico_exec b,
	man_ordem_ativ_prev c
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
and	(c.ie_prioridade_desen > 1 or c.ie_prioridade_sup > 1)
and	a.nr_seq_estagio not in (2,9,791,1234,41,1071,1341) --desenvolvimento aguardando programa��o
union
select 	2 nr_seq_ordem,
	a.nr_sequencia,
	c.ie_prioridade_desen,
	a.dt_ordem_servico dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.dt_prevista,
	c.nr_sequencia nr_seq_ativ_prev,
	nvl(a.nr_seq_severidade_wheb,0) nr_seq_severidade,
	substr(nvl(man_obter_curva_abc(a.nr_seq_cliente),'C'),1,5) ds_curva_abc,
	nvl(a.nr_seq_severidade,a.nr_seq_severidade_wheb) nr_seq_severidade_score,
	a.ie_classificacao,
	nvl(a.ie_prioridade_cliente,a.ie_prioridade) ie_prioridade,
	c.ie_prioridade_sup
from   	man_ordem_servico a,
    	man_ordem_servico_exec b,
	man_ordem_ativ_prev c
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
and	(c.ie_prioridade_desen = 1 or c.ie_prioridade_sup = 1)
and	a.nr_seq_estagio not in (2,9,791,1234,41,1071,1341) --desenvolvimento aguardando programa��o
--and	a.nr_seq_estagio in (1191) --desenvolvimento aguardando programa��o
and	a.ie_classificacao = 'E'
union
select 	3 nr_seq_ordem,
	a.nr_sequencia,
	c.ie_prioridade_desen,
	a.dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.dt_prevista,
	c.nr_sequencia nr_seq_ativ_prev,
	999 nr_seq_severidade,
	substr(nvl(man_obter_curva_abc(a.nr_seq_cliente),'C'),1,5) ds_curva_abc,
	nvl(a.nr_seq_severidade,a.nr_seq_severidade_wheb) nr_seq_severidade_score,
	a.ie_classificacao,
	nvl(a.ie_prioridade_cliente,a.ie_prioridade) ie_prioridade,
	c.ie_prioridade_sup
from   	man_ordem_servico a,
    	man_ordem_servico_exec b,
	man_ordem_ativ_prev c
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
and	(c.ie_prioridade_desen = 1 or c.ie_prioridade_sup = 1)
and	a.ie_classificacao <> 'E' --defeito
and	a.nr_seq_estagio not in (2,9,791,1234,41,1071,1341) --desenvolvimento aguardando programa��o
order by nr_seq_severidade, nr_seq_ordem, ie_prioridade_desen desc, ie_prioridade_sup desc, dt_ordem_servico;

cursor c02 is
select 	a.nm_usuario_lista
from	lista_usuario_os a
where	nr_seq_fila = nr_seq_fila_w
and	ie_situacao	= 'A'
order by a.nm_usuario_lista;

cursor c03 is
select	nr_sequencia,
	nr_seq_gerencia,
	nm_usuario_fila,
	nvl(ie_atualiza_dt_prev,'N'),
	nvl(ie_considera_grupo,'N'),
	nvl(ie_calc_score,'N'),
	nvl(ie_capacidade_skill,'N'),
	nvl(IE_PACOTE_ENTREGA,'N')
from	cadastro_fila
where	ie_situacao	= 'A';

cursor c04 is
select	a.nr_sequencia,
	a.nr_seq_ordem_serv,
	a.nr_seq_exec
from	fila_execucao_os a
where	a.nr_seq_fila	= nr_seq_fila_w
and	a.dt_referencia	= dt_referencia_w
order by a.dt_prevista, a.nr_seq_exec;

cursor c05 is
select	a.nr_sequencia,
	a.nr_seq_ordem_serv,
	a.nr_seq_exec
from	fila_execucao_os a
where	a.nr_seq_fila	= nr_seq_fila_w
and	a.dt_referencia	= dt_referencia_w
order by nr_ordem, qt_score desc, dt_ordem_servico;

cursor c06 is
select	a.nr_sequencia,
	a.nr_seq_ordem_serv,
	a.nr_seq_exec
from	fila_execucao_os a
where	a.nr_seq_fila	= nr_seq_fila_w
and	a.dt_referencia	= dt_referencia_w;

cursor c07 is
select	a.nr_sequencia,
	a.nr_seq_ordem_serv,
	a.nr_seq_exec
from	fila_execucao_os a
where	a.nr_seq_fila	= nr_seq_fila_w
and	a.dt_referencia	= dt_referencia_w
order by	decode(IE_PACOTE_ENTREGA_w,'S',dt_prev_entrega,null), qt_recurso, nr_seq_exec;

vet04	c04%rowtype;
c05_w 	c05%rowtype;
c06_w 	c06%rowtype;
c07_w 	c07%rowtype;

begin
delete	fila_execucao_os
where	dt_referencia	= dt_referencia_w;

wheb_usuario_pck.set_nm_usuario('Edilson');

select	y.nr_sequencia
into	nr_sequencia_w
from (
select	nr_sequencia
from	fila_execucao_os
order by 1 desc) y
where	rownum = 1;

nr_seq_exec_w	:= 0;
nr_sequencia_w	:= nvl(nr_sequencia_w,0);

open c03;
loop
fetch c03 into
	nr_seq_fila_w,
	nr_seq_gerencia_w,
	nm_usuario_fila_w,
	ie_atualiza_dt_prev_w,
	ie_considera_grupo_w,
	ie_calc_score_w,
	ie_capacidade_skill_w,
	IE_PACOTE_ENTREGA_w;
exit when c03%notfound;
	begin
	nr_seq_exec_w	:= 0;

	if	(ie_calc_score_w = 'S') then
		begin
		open c01;
		loop
		fetch c01 into
			nr_seq_ordem_w,
			nr_ordem_serv_w,
			ie_prioridade_w,
			dt_ordem_serv_w,
			qt_min_prev_w,
			nr_seq_lp_w,
			dt_prev_os_w,
			nr_seq_ativ_prev_w,
			nr_seq_severidade_w,
			ds_curva_abc_w,
			nr_seq_severidade_score_w,
			ie_classificacao_w,
			ie_prioridade_cliente_w,
			ie_prioridade_sup_w;
		exit when c01%notfound;
			begin
			qt_pontuacao_w	:= des_obter_score_ordem_serv(nr_ordem_serv_w);

			nr_seq_exec_w	:= nr_seq_exec_w + 1;
			nr_sequencia_w	:= nr_sequencia_w + 1;

			insert into fila_execucao_os(
				nr_sequencia,
				dt_referencia,
				nr_seq_ordem_serv,
				nr_seq_fila,
				nr_seq_exec,
				dt_prevista,
				qt_min_prev,
				dt_prev_entrega,
				nr_ordem,
				nr_prioridade,
				nr_seq_ativ_prev,
				dt_ordem_servico,
				qt_score)
			values(	nr_sequencia_w,
				dt_referencia_w,
				nr_ordem_serv_w,
				nr_seq_fila_w,
				nr_seq_exec_w,
				dt_prev_os_w,
				qt_min_prev_w,
				decode(dt_prev_os_w,null,null,obter_proxima_versao2(dt_prev_os_w)),
				nr_seq_ordem_w,
				ie_prioridade_w,
				nr_seq_ativ_prev_w,
				dt_ordem_serv_w,
				qt_pontuacao_w);
			end;
		end loop;
		close c01;

		nr_seq_exec_w	:= 0;

		open c05;
		loop
		fetch c05 into
			c05_w;
		exit when c05%notfound;
			begin

			nr_seq_exec_w	:= nr_seq_exec_w + 1;

			update	fila_execucao_os
			set	nr_seq_exec	= nr_seq_exec_w
			where	nr_seq_fila	= nr_seq_fila_w
			and	dt_referencia	= dt_referencia_w
			and	nr_seq_exec	= c05_w.nr_seq_exec
			and	nr_seq_ordem_serv	= c05_w.nr_seq_ordem_serv;
			end;
		end loop;
		close c05;
		end;
	else
		open c01;
		loop
		fetch c01 into
			nr_seq_ordem_w,
			nr_ordem_serv_w,
			ie_prioridade_w,
			dt_ordem_serv_w,
			qt_min_prev_w,
			nr_seq_lp_w,
			dt_prev_os_w,
			nr_seq_ativ_prev_w,
			nr_seq_severidade_w,
			ds_curva_abc_w,
			nr_seq_severidade_score_w,
			ie_classificacao_w,
			ie_prioridade_cliente_w,
			ie_prioridade_sup_w;
		exit when c01%notfound;
			begin
			nr_seq_exec_w	:= nr_seq_exec_w + 1;
			nr_sequencia_w	:= nr_sequencia_w + 1;
			insert into fila_execucao_os(
				nr_sequencia,
				dt_referencia,
				nr_seq_ordem_serv,
				nr_seq_fila,
				nr_seq_exec,
				dt_prevista,
				qt_min_prev,
				dt_prev_entrega,
				nr_ordem,
				nr_prioridade,
				nr_seq_ativ_prev,
				dt_ordem_servico)
			values(	nr_sequencia_w,
				dt_referencia_w,
				nr_ordem_serv_w,
				nr_seq_fila_w,
				nr_seq_exec_w,
				dt_prev_os_w,
				qt_min_prev_w,
				decode(dt_prev_os_w,null,null,obter_proxima_versao2(dt_prev_os_w)),
				nr_seq_ordem_w,
				ie_prioridade_w,
				nr_seq_ativ_prev_w,
				dt_ordem_serv_w);

			end;
		end loop;
		close c01;
	end if;

	/*'
	Deve ser a �ltima ordena��o a ser realizada
	Ordena a execu��o dentro do pacote conforme quantidade de recursos com o skill da OS
	'*/
	if	(ie_capacidade_skill_w = 'S') then
		begin
		open c06;
		loop
		fetch c06 into
			c06_w;
		exit when c06%notfound;
			begin
			select	count(1)
			into	qt_recurso_w
			from	(
			select	a.nm_usuario_lista
			from	lista_usuario_os a,
				man_ordem_serv_skill b,
				skill_desenv_usuario c
			where	a.nm_usuario_lista = c.nm_usuario_des
			and	b.nr_seq_skill = c.nr_seq_skill 
			and	b.nr_seq_ordem_serv = c06_w.nr_seq_ordem_serv
			and	a.nr_seq_fila = nr_seq_fila_w
			and	ie_situacao = 'A'
			and	ie_calculo_prev = 'S'
			and	not exists(	select	1
					from	man_ordem_servico o,
						man_ordem_serv_skill x
					where	o.nr_sequencia = x.nr_seq_ordem_serv
					and	o.nr_sequencia = b.nr_seq_ordem_serv
					and	not exists(	select	1
							from	skill_desenv_usuario y
							where	y.nm_usuario_des = a.nm_usuario_lista
							and	x.nr_seq_skill = y.nr_seq_skill
							and	(nvl(o.nr_seq_complex,0) <> 4 or decode(o.nr_seq_complex,4,'A','B') = decode(y.ie_nivel_skill,1,'A','B'))))
			group by	a.nm_usuario_lista);

			update	fila_execucao_os
			set	qt_recurso	= qt_recurso_w
			where	nr_seq_fila	= nr_seq_fila_w
			and	dt_referencia	= dt_referencia_w
			and	nr_seq_exec	= c06_w.nr_seq_exec
			and	nr_seq_ordem_serv	= c06_w.nr_seq_ordem_serv;
			end;
		end loop;
		close c06;
		
		nr_seq_exec_w	:= 0;

		open c07;
		loop
		fetch c07 into
			c07_w;
		exit when c07%notfound;
			begin
			nr_seq_exec_w	:= nr_seq_exec_w + 1;

			update	fila_execucao_os
			set	nr_seq_exec	= nr_seq_exec_w
			where	nr_seq_fila	= nr_seq_fila_w
			and	dt_referencia	= dt_referencia_w
			and	nr_seq_exec	= c07_w.nr_seq_exec
			and	nr_seq_ordem_serv	= c07_w.nr_seq_ordem_serv;
			end;
		end loop;
		close c07;
		end;
	/*reorganiza a sequencia da fila conforme data prevista para as ger�ncias que trabalham com pacotes de entrega*/
	elsif	(ie_atualiza_dt_prev_w = 'N') then
		begin
		nr_seq_exec_w	:= 0;
		open c04;
		loop
		fetch c04 into
			vet04;
		exit when c04%notfound;
			begin
			nr_seq_exec_w	:= nr_seq_exec_w + 1;

			update	fila_execucao_os
			set	nr_seq_exec	= nr_seq_exec_w
			where	nr_seq_fila	= nr_seq_fila_w
			and	dt_referencia	= dt_referencia_w
			and	nr_seq_exec	= vet04.nr_seq_exec
			and	nr_seq_ordem_serv	= vet04.nr_seq_ordem_serv;
			end;
		end loop;
		close c04;
		end;
	end if;

	/*rotina para gerar datas de entrega e colocar a primeira os do dia para o programador */
	if	(to_char(sysdate,'hh24') = '01') then
		select	count(*)
		into	qt_registro_w
		from	fila_execucao_os
		where	dt_referencia = dt_referencia_w
		and	nr_seq_fila	= nr_seq_fila_w
		and	dt_prevista is null;

		if	(qt_registro_w > 0) then
			gerar_data_entrega_os(nr_seq_gerencia_w, nr_seq_fila_w);
		end if;

		open c02;
		loop
		fetch c02 into
			nm_usuario_w;
		exit when c02%notfound;
			begin
			nr_ordem_serv_w	:= 0;

			begin
			select	nvl(max(a.nr_sequencia),0)
			into	nr_ordem_serv_w
			from	man_ordem_ativ_prev b,
				man_ordem_servico a
			where	a.nr_sequencia	= b.nr_seq_ordem_serv
			and	b.nm_usuario_prev	= nm_usuario_w
			and	b.nm_usuario_nrec	= nm_usuario_fila_w
			and	trunc(b.dt_prevista)	= trunc(sysdate-1)
			and	nvl(b.pr_atividade,0)	= 0
			and   	b.dt_real is null
			and	not exists(	select	1
					from	man_ordem_serv_ativ y
					where	y.nr_seq_ordem_serv	= a.nr_sequencia
					and	y.nm_usuario_exec	= nm_usuario_w)
			and	exists(	select	1
					from	man_ordem_servico_exec y
					where	y.nr_seq_ordem	= a.nr_sequencia
					and	y.nm_usuario_exec	= nm_usuario_fila_w
					and	y.dt_fim_execucao is not null)
			and	exists(	select	1
					from	man_ordem_servico_exec y
					where	y.nr_seq_ordem	= a.nr_sequencia
					and	y.nm_usuario_exec	= nm_usuario_w
					and	y.dt_fim_execucao is null);

			/* retira �ltima os passada pela fila que o usu�rio n�o trabalhou */
			if	(nr_ordem_serv_w <> 0) then

				delete	man_ordem_ativ_prev a
				where	nr_seq_ordem_serv	= nr_ordem_serv_w
				and	trunc(dt_prevista)	= trunc(sysdate-1)
				and	a.nm_usuario_prev	= nm_usuario_w
				and	a.nm_usuario_nrec	= nm_usuario_fila_w
				and   	a.dt_real is null
				and	nvl(a.pr_atividade,0)	= 0
				and	not exists(	select	1
						from	man_ordem_serv_ativ y
						where	y.nr_seq_ordem_serv	= a.nr_seq_ordem_serv
						and	y.nm_usuario_exec	= nm_usuario_w)
				and	exists(	select	1
						from	man_ordem_servico_exec y
						where	y.nr_seq_ordem	= a.nr_seq_ordem_serv
						and	y.nm_usuario_exec	= nm_usuario_fila_w
						and	y.dt_fim_execucao is not null)
				and	exists(	select	1
						from	man_ordem_servico_exec y
						where	y.nr_seq_ordem	= a.nr_seq_ordem_serv
						and	y.nm_usuario_exec	= nm_usuario_w
						and	y.dt_fim_execucao is null);

				delete	man_ordem_servico_exec
				where	nr_seq_ordem	= nr_ordem_serv_w
				and	nm_usuario_exec	= nm_usuario_w;


				update	man_ordem_servico_exec
				set	dt_fim_execucao	= null,
					nm_usuario	= nm_usuario_fila_w,
					dt_atualizacao	= sysdate
				where	nr_seq_ordem	= nr_ordem_serv_w
				and	nm_usuario_exec	= nm_usuario_fila_w;

			end if;
			end;

			select	count(*)
			into	qt_registro_w
			from  	man_ordem_ativ_prev a
			where 	a.nm_usuario_prev = nm_usuario_w
			and	trunc(a.dt_prevista) = trunc(sysdate)
			and   	a.dt_real is null;

			if	(qt_registro_w = 0) then
				fila_busca_nova_os(nm_usuario_w, nr_ordem_serv_w);
			end if;
			end;
		end loop;
		close c02;
		nr_ordem_serv_w	:= null;
	end if;

	end;
end loop;
close c03;

commit;

end robo_fila_delphi;
/
